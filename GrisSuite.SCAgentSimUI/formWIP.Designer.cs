namespace GrisSuite
{
    partial class formWIP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formWIP));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtStartedAt = new System.Windows.Forms.TextBox();
            this.txtStopedAt = new System.Windows.Forms.TextBox();
            this.listCalls = new System.Windows.Forms.ListView();
            this.Method = new System.Windows.Forms.ColumnHeader();
            this.Counter = new System.Windows.Forms.ColumnHeader();
            this.ColAvgDuration = new System.Windows.Forms.ColumnHeader();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Telefin.Properties.Resources.head_refresh;
            this.pictureBox1.Location = new System.Drawing.Point(10, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 44);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(249, 14);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(249, 41);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Started at:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Stoped at:";
            // 
            // txtStartedAt
            // 
            this.txtStartedAt.Location = new System.Drawing.Point(113, 16);
            this.txtStartedAt.Name = "txtStartedAt";
            this.txtStartedAt.ReadOnly = true;
            this.txtStartedAt.Size = new System.Drawing.Size(128, 20);
            this.txtStartedAt.TabIndex = 5;
            // 
            // txtStopedAt
            // 
            this.txtStopedAt.Location = new System.Drawing.Point(113, 44);
            this.txtStopedAt.Name = "txtStopedAt";
            this.txtStopedAt.ReadOnly = true;
            this.txtStopedAt.Size = new System.Drawing.Size(128, 20);
            this.txtStopedAt.TabIndex = 6;
            // 
            // listCalls
            // 
            this.listCalls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Method,
            this.Counter,
            this.ColAvgDuration});
            this.listCalls.Location = new System.Drawing.Point(10, 70);
            this.listCalls.Name = "listCalls";
            this.listCalls.Size = new System.Drawing.Size(314, 149);
            this.listCalls.TabIndex = 7;
            this.listCalls.UseCompatibleStateImageBehavior = false;
            this.listCalls.View = System.Windows.Forms.View.Details;
            // 
            // Method
            // 
            this.Method.Text = "Method";
            this.Method.Width = 180;
            // 
            // Counter
            // 
            this.Counter.Text = "Counter";
            this.Counter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ColAvgDuration
            // 
            this.ColAvgDuration.Text = "Avg Duration";
            this.ColAvgDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ColAvgDuration.Width = 70;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // formWIP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 231);
            this.Controls.Add(this.listCalls);
            this.Controls.Add(this.txtStopedAt);
            this.Controls.Add(this.txtStartedAt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formWIP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Work In Progress";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.formWIP_FormClosed);
            this.Shown += new System.EventHandler(this.formWIP_Shown);
            this.Load += new System.EventHandler(this.formWIP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStartedAt;
        private System.Windows.Forms.TextBox txtStopedAt;
        private System.Windows.Forms.ListView listCalls;
        private System.Windows.Forms.ColumnHeader Method;
        private System.Windows.Forms.ColumnHeader Counter;
        private System.Windows.Forms.ColumnHeader ColAvgDuration;
        private System.Windows.Forms.Timer timer1;
    }
}
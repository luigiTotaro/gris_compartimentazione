using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace GrisSuite
{
    public partial class formXml2Table : Form
    {
        private XmlDocument _xDoc;
        private DataTable _dt;

        public formXml2Table()
        {
            InitializeComponent();

            txtFile.Text = @"C:\my\Projects\Telefin\Definizioni_Periferiche\xml\TT10210d.xml";
            txtXPath.Text = "/type/stream/item"; 
            // /type/stream/item/field 
            // /type/stream/item/field/value 
        }


        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            try
            {
                _xDoc = new XmlDocument();
                _xDoc.Load(txtFile.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void btnGetNodes_Click(object sender, EventArgs e)
        {
            try
            {
                if (_xDoc != null)
                {
                    _dt = LocalUtil.GetTableFormXML(_xDoc, txtXPath.Text);

                    dataGridView1.DataSource = _dt;
                    dataGridView1.AutoGenerateColumns = true;
                    dataGridView1.Refresh();

                    if (_dt.Rows.Count == 0)
                    {
                        MessageBox.Show(this, "Zero nodes found", "Xml2Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show(this, "Document not loaded", "Xml2Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GrisSuite
{
    public partial class formConfigTest : Form
    {

        private string _loadedConfig = string.Empty;

        public formConfigTest()
        {
            InitializeComponent();
        }

        private void btnLoadDeviceDef_Click(object sender, EventArgs e)
        {
            formLoadDefinitions f = new formLoadDefinitions();

            f.ShowDialog(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            formXml2Table f = new formXml2Table();
            f.Show();
        }

        private void btnSaveTest_Click(object sender, EventArgs e)
        {
            try
            {
                
                saveFileDialog1.Filter = "Test Config File (*.xtest)|*.xtest|Tutti i file (*.*)|*.*";
                string defaultFile = _loadedConfig;
                int i = 1;

                if (defaultFile == string.Empty)
                {
                    while (File.Exists(string.Format("test{0}.xtest", i)))
                    {
                        i++;
                    }
                    defaultFile = string.Format("test{0}.xtest", i);
                }

                saveFileDialog1.FileName = defaultFile;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    testPropertyBindingSource.EndEdit();

                    testDS.WriteXml(saveFileDialog1.FileName);
                    _loadedConfig = saveFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnLoadTest_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = "Test Config File (*.xtest)|*.xtest|Tutti i file (*.*)|*.*";
                openFileDialog1.FileName = "";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    testDS.Clear();
                    testDS.ReadXml(openFileDialog1.FileName);
                    _loadedConfig = openFileDialog1.FileName;
                }

                if (testPropertyBindingSource.Count == 0)
                {
                    testPropertyBindingSource.AddNew();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {
            try
            {

                formWIP wip = new formWIP();
                wip.DsTest = testDS;
                wip.ShowDialog(this);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void btnFillDB_Click(object sender, EventArgs e)
        {

        }


    }


}
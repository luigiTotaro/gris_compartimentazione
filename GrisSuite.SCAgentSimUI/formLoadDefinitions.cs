using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telefin;

namespace GrisSuite
{
    public partial class formLoadDefinitions : Form
    {
        private DefinitionDS _ds = new DefinitionDS();

        public formLoadDefinitions()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                if (Directory.Exists(txtFolder.Text))
                {
                    string[] files = Directory.GetFiles(txtFolder.Text, "*d.xml");

                    // set progress 
                    progressBar1.Minimum = 0;
                    progressBar1.Maximum = files.Length - 1;
                    progressBar1.Visible = true;
                    comboBox1.Visible = false;
                    _ds = new DefinitionDS();

                    if (File.Exists("Definition.xml"))
                    {
                        File.Delete("Definition.xml");
                    }

                    for (int i = 0; i < files.Length; i++)
                    {
                        string file = files[i];
                        lblFile.Text = file;
                        Application.DoEvents();

                        StreamReader sr = new StreamReader(file,System.Text.ASCIIEncoding.ASCII);
                        xDoc.Load(sr);

                        LoadDeviceType(_ds,xDoc);

                        progressBar1.Value = i;
                        Application.DoEvents();
                    }

                    _ds.WriteXml("Definition.xml");

                    progressBar1.Visible = false;
                    comboBox1.Visible = true;

                    LoadComboTables();

                    MessageBox.Show("Acquisione conclusa con successo", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void LoadComboTables()
        {
            comboBox1.Items.Clear();
            foreach (DataTable table in _ds.Tables)
            {
            comboBox1.Items.Add(table.TableName);
            }
            comboBox1.SelectedIndex = 0;
        }

        private DefinitionDS LoadDeviceType(DefinitionDS dsDefinition, XmlDocument xDoc)
        {
            DefinitionDS ds = new DefinitionDS();
            
            ds.DeviceType.DeviceTypeCodeColumn.AutoIncrementSeed = dsDefinition.DeviceType.Count +1;
            ds.StreamType.Columns[0].AutoIncrementSeed = dsDefinition.StreamType.Rows.Count + 1;
            ds.FieldType.Columns[0].AutoIncrementSeed = dsDefinition.FieldType.Rows.Count + 1;
            ds.ValueType.Columns[0].AutoIncrementSeed = dsDefinition.ValueType.Rows.Count + 1;

            DataTable xdtDeviceType = LocalUtil.GetTableFormXML(xDoc,"/type", "code", "name", "version");

            foreach (DataRow xrDeviceType in xdtDeviceType.Rows)
            {
                DefinitionDS.DeviceTypeRow deviceTypeRow = ds.DeviceType.AddDeviceTypeRow(LocalUtil.IsNullString(xrDeviceType["code"]), 
                                                                                            LocalUtil.IsNullString(xrDeviceType["name"]), 
                                                                                            LocalUtil.IsNullString(xrDeviceType["version"]));

                string getStreams = "/type/stream/item";

                DataTable xdtStreamType = LocalUtil.GetTableFormXML(xDoc, getStreams, "id", "name", "visible");

                foreach (DataRow xrStreamType in xdtStreamType.Rows)
                {
                    DefinitionDS.StreamTypeRow streamTypeRow = ds.StreamType.AddStreamTypeRow(deviceTypeRow, 
                                                                                                LocalUtil.IsNullInt(xrStreamType["id"]), 
                                                                                                LocalUtil.IsNullString(xrStreamType["name"]), 
                                                                                                LocalUtil.IsNullBool(xrStreamType["visible"]));
                    string getFields = string.Format("/type/stream/item[@id=\"{0}\"]/field",xrStreamType["id"]);

                    DataTable xdtFieldType = LocalUtil.GetTableFormXML(xDoc, getFields, "id", "name", "capacity", "type","len","format","factor","visible");
                    foreach (DataRow xrFieldType in xdtFieldType.Rows)
                    {
                        DefinitionDS.FieldTypeRow fieldTypeRow = ds.FieldType.AddFieldTypeRow(streamTypeRow,
                                                                                                LocalUtil.IsNullInt(xrFieldType["id"]),
                                                                                                LocalUtil.IsNullString(xrFieldType["name"]),
                                                                                                LocalUtil.IsNullInt(xrFieldType["capacity"]),
                                                                                                LocalUtil.IsNullString(xrFieldType["type"]),
                                                                                                LocalUtil.IsNullString(xrFieldType["len"]),
                                                                                                LocalUtil.IsNullString(xrFieldType["format"]),
                                                                                                LocalUtil.IsNullString(xrFieldType["factor"]),
                                                                                                LocalUtil.IsNullBool(xrFieldType["visible"]));

                        string getValues = string.Format("/type/stream/item[@id=\"{0}\"]/field[@id=\"{1}\"]/value", xrStreamType["id"], xrFieldType["id"]);

                        DataTable xdtValueType = LocalUtil.GetTableFormXML(xDoc, getValues, "type","mask","sev","des","dep","equal","from","to");

                        foreach (DataRow xrValueType in xdtValueType.Rows)
                        {
                            DefinitionDS.ValueTypeRow valueTypeRow = ds.ValueType.AddValueTypeRow(fieldTypeRow,
                                                                                                    LocalUtil.IsNullString(xrValueType["type"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["mask"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["sev"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["des"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["dep"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["equal"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["from"]),
                                                                                                    LocalUtil.IsNullString(xrValueType["to"]));
                        }
                    }
                }
            }

            _ds.Merge(ds);

            return ds;


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                dataGridView1.DataSource = _ds;
                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataMember = comboBox1.SelectedItem.ToString();

                dataGridView1.Refresh();

                lblReportCount.Text = string.Format("{0} Records",_ds.Tables[dataGridView1.DataMember].Rows.Count);

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formLoadDefinitions_Load(object sender, EventArgs e)
        {
            progressBar1.Visible = false;
            comboBox1.Visible = true;

            if (File.Exists("Definition.xml"))
            {
                _ds = new DefinitionDS();
                _ds.ReadXml("Definition.xml");
                LoadComboTables();
                comboBox1.Visible = true;
            }

        }

    }
}
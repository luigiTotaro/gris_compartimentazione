namespace GrisSuite
{
    partial class formConfigTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formConfigTest));
            this.btnLoadDeviceDef = new System.Windows.Forms.Button();
            this.btnSaveTest = new System.Windows.Forms.Button();
            this.btnFillDB = new System.Windows.Forms.Button();
            this.btnLoadTest = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgSimulations = new System.Windows.Forms.DataGridView();
            this.simulationIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numStationsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numServersInStationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numDevicesInServerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percUpdateDevicesStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percUpdateStreamsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percUpdateStreamsFieldsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percUpdateDevicesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindSimulation = new System.Windows.Forms.BindingSource(this.components);
            this.testDS = new GrisSuite.TestDS();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUpdateCountMin = new System.Windows.Forms.TextBox();
            this.testPropertyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtIntervalSeconds = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnStartTest = new System.Windows.Forms.Button();
            this.txtUpdateCountMax = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgSimulations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindSimulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testPropertyBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadDeviceDef
            // 
            this.btnLoadDeviceDef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadDeviceDef.Location = new System.Drawing.Point(404, 392);
            this.btnLoadDeviceDef.Name = "btnLoadDeviceDef";
            this.btnLoadDeviceDef.Size = new System.Drawing.Size(118, 23);
            this.btnLoadDeviceDef.TabIndex = 0;
            this.btnLoadDeviceDef.Text = "Device Definition";
            this.btnLoadDeviceDef.UseVisualStyleBackColor = true;
            this.btnLoadDeviceDef.Click += new System.EventHandler(this.btnLoadDeviceDef_Click);
            // 
            // btnSaveTest
            // 
            this.btnSaveTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveTest.Location = new System.Drawing.Point(825, 55);
            this.btnSaveTest.Name = "btnSaveTest";
            this.btnSaveTest.Size = new System.Drawing.Size(94, 23);
            this.btnSaveTest.TabIndex = 1;
            this.btnSaveTest.Text = "Salva Test";
            this.btnSaveTest.UseVisualStyleBackColor = true;
            this.btnSaveTest.Click += new System.EventHandler(this.btnSaveTest_Click);
            // 
            // btnFillDB
            // 
            this.btnFillDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFillDB.Location = new System.Drawing.Point(825, 84);
            this.btnFillDB.Name = "btnFillDB";
            this.btnFillDB.Size = new System.Drawing.Size(94, 23);
            this.btnFillDB.TabIndex = 2;
            this.btnFillDB.Text = "Popola DB";
            this.btnFillDB.UseVisualStyleBackColor = true;
            this.btnFillDB.Visible = false;
            this.btnFillDB.Click += new System.EventHandler(this.btnFillDB_Click);
            // 
            // btnLoadTest
            // 
            this.btnLoadTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadTest.Location = new System.Drawing.Point(825, 26);
            this.btnLoadTest.Name = "btnLoadTest";
            this.btnLoadTest.Size = new System.Drawing.Size(94, 23);
            this.btnLoadTest.TabIndex = 3;
            this.btnLoadTest.Text = "Carica Test";
            this.btnLoadTest.UseVisualStyleBackColor = true;
            this.btnLoadTest.Click += new System.EventHandler(this.btnLoadTest_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Simulazioni";
            // 
            // dgSimulations
            // 
            this.dgSimulations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSimulations.AutoGenerateColumns = false;
            this.dgSimulations.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgSimulations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSimulations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.simulationIdDataGridViewTextBoxColumn,
            this.numStationsDataGridViewTextBoxColumn,
            this.numServersInStationDataGridViewTextBoxColumn,
            this.numDevicesInServerDataGridViewTextBoxColumn,
            this.percUpdateDevicesStatusDataGridViewTextBoxColumn,
            this.percUpdateStreamsDataGridViewTextBoxColumn,
            this.percUpdateStreamsFieldsDataGridViewTextBoxColumn,
            this.percUpdateDevicesDataGridViewTextBoxColumn});
            this.dgSimulations.DataSource = this.bindSimulation;
            this.dgSimulations.Location = new System.Drawing.Point(13, 26);
            this.dgSimulations.Name = "dgSimulations";
            this.dgSimulations.Size = new System.Drawing.Size(806, 356);
            this.dgSimulations.TabIndex = 5;
            // 
            // simulationIdDataGridViewTextBoxColumn
            // 
            this.simulationIdDataGridViewTextBoxColumn.DataPropertyName = "SimulationId";
            this.simulationIdDataGridViewTextBoxColumn.HeaderText = "Id";
            this.simulationIdDataGridViewTextBoxColumn.Name = "simulationIdDataGridViewTextBoxColumn";
            // 
            // numStationsDataGridViewTextBoxColumn
            // 
            this.numStationsDataGridViewTextBoxColumn.DataPropertyName = "NumStations";
            this.numStationsDataGridViewTextBoxColumn.HeaderText = "Stations";
            this.numStationsDataGridViewTextBoxColumn.Name = "numStationsDataGridViewTextBoxColumn";
            // 
            // numServersInStationDataGridViewTextBoxColumn
            // 
            this.numServersInStationDataGridViewTextBoxColumn.DataPropertyName = "NumServersInStation";
            this.numServersInStationDataGridViewTextBoxColumn.HeaderText = "Servers In Station";
            this.numServersInStationDataGridViewTextBoxColumn.Name = "numServersInStationDataGridViewTextBoxColumn";
            // 
            // numDevicesInServerDataGridViewTextBoxColumn
            // 
            this.numDevicesInServerDataGridViewTextBoxColumn.DataPropertyName = "NumDevicesInServer";
            this.numDevicesInServerDataGridViewTextBoxColumn.HeaderText = "Devices In Server";
            this.numDevicesInServerDataGridViewTextBoxColumn.Name = "numDevicesInServerDataGridViewTextBoxColumn";
            // 
            // percUpdateDevicesStatusDataGridViewTextBoxColumn
            // 
            this.percUpdateDevicesStatusDataGridViewTextBoxColumn.DataPropertyName = "PercUpdateDevicesStatus";
            this.percUpdateDevicesStatusDataGridViewTextBoxColumn.HeaderText = "% Update DevStatus";
            this.percUpdateDevicesStatusDataGridViewTextBoxColumn.Name = "percUpdateDevicesStatusDataGridViewTextBoxColumn";
            // 
            // percUpdateStreamsDataGridViewTextBoxColumn
            // 
            this.percUpdateStreamsDataGridViewTextBoxColumn.DataPropertyName = "PercUpdateStreams";
            this.percUpdateStreamsDataGridViewTextBoxColumn.HeaderText = "% Update Streams";
            this.percUpdateStreamsDataGridViewTextBoxColumn.Name = "percUpdateStreamsDataGridViewTextBoxColumn";
            // 
            // percUpdateStreamsFieldsDataGridViewTextBoxColumn
            // 
            this.percUpdateStreamsFieldsDataGridViewTextBoxColumn.DataPropertyName = "PercUpdateStreamsFields";
            this.percUpdateStreamsFieldsDataGridViewTextBoxColumn.HeaderText = "% Update Fields";
            this.percUpdateStreamsFieldsDataGridViewTextBoxColumn.Name = "percUpdateStreamsFieldsDataGridViewTextBoxColumn";
            // 
            // percUpdateDevicesDataGridViewTextBoxColumn
            // 
            this.percUpdateDevicesDataGridViewTextBoxColumn.DataPropertyName = "PercUpdateDevices";
            this.percUpdateDevicesDataGridViewTextBoxColumn.HeaderText = "% Update Devices";
            this.percUpdateDevicesDataGridViewTextBoxColumn.Name = "percUpdateDevicesDataGridViewTextBoxColumn";
            // 
            // bindSimulation
            // 
            this.bindSimulation.DataMember = "Simulation";
            this.bindSimulation.DataSource = this.testDS;
            // 
            // testDS
            // 
            this.testDS.DataSetName = "TestDS";
            this.testDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 397);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Update Count Min";
            // 
            // txtUpdateCountMin
            // 
            this.txtUpdateCountMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtUpdateCountMin.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testPropertyBindingSource, "UpdateCountMin", true));
            this.txtUpdateCountMin.Location = new System.Drawing.Point(108, 393);
            this.txtUpdateCountMin.Name = "txtUpdateCountMin";
            this.txtUpdateCountMin.Size = new System.Drawing.Size(37, 20);
            this.txtUpdateCountMin.TabIndex = 7;
            this.txtUpdateCountMin.Text = "1";
            // 
            // testPropertyBindingSource
            // 
            this.testPropertyBindingSource.DataMember = "TestProperty";
            this.testPropertyBindingSource.DataSource = this.testDS;
            // 
            // txtIntervalSeconds
            // 
            this.txtIntervalSeconds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtIntervalSeconds.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testPropertyBindingSource, "IntervalSeconds", true));
            this.txtIntervalSeconds.Location = new System.Drawing.Point(326, 393);
            this.txtIntervalSeconds.Name = "txtIntervalSeconds";
            this.txtIntervalSeconds.Size = new System.Drawing.Size(32, 20);
            this.txtIntervalSeconds.TabIndex = 9;
            this.txtIntervalSeconds.Text = "10";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(227, 397);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Intervallo(secondi)";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnStartTest
            // 
            this.btnStartTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartTest.Location = new System.Drawing.Point(825, 113);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(94, 23);
            this.btnStartTest.TabIndex = 10;
            this.btnStartTest.Text = "Start";
            this.btnStartTest.UseVisualStyleBackColor = true;
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // txtUpdateCountMax
            // 
            this.txtUpdateCountMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtUpdateCountMax.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.testPropertyBindingSource, "UpdateCountMax", true));
            this.txtUpdateCountMax.Location = new System.Drawing.Point(184, 393);
            this.txtUpdateCountMax.Name = "txtUpdateCountMax";
            this.txtUpdateCountMax.Size = new System.Drawing.Size(37, 20);
            this.txtUpdateCountMax.TabIndex = 13;
            this.txtUpdateCountMax.Text = "1";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(151, 397);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Max";
            // 
            // formConfigTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 423);
            this.Controls.Add(this.txtUpdateCountMax);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnStartTest);
            this.Controls.Add(this.txtIntervalSeconds);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUpdateCountMin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgSimulations);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoadTest);
            this.Controls.Add(this.btnFillDB);
            this.Controls.Add(this.btnSaveTest);
            this.Controls.Add(this.btnLoadDeviceDef);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "formConfigTest";
            this.Text = "STLC1000 Test Centralizzazione";
            ((System.ComponentModel.ISupportInitialize)(this.dgSimulations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindSimulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testPropertyBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadDeviceDef;
        private System.Windows.Forms.Button btnSaveTest;
        private System.Windows.Forms.Button btnFillDB;
        private System.Windows.Forms.Button btnLoadTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgSimulations;
        private System.Windows.Forms.BindingSource bindSimulation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUpdateCountMin;
        private TestDS testDS;
        private System.Windows.Forms.TextBox txtIntervalSeconds;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnStartTest;
        private System.Windows.Forms.BindingSource testPropertyBindingSource;
        private System.Windows.Forms.TextBox txtUpdateCountMax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn simulationIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numStationsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numServersInStationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numDevicesInServerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percUpdateDevicesStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percUpdateStreamsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percUpdateStreamsFieldsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percUpdateDevicesDataGridViewTextBoxColumn;

    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telefin;

namespace GrisSuite
{
    public partial class formWIP : Form
    {
        private TestDS _dsTest = null;
        
        public formWIP()
        {
            InitializeComponent();
        }

        public TestDS DsTest
        {
            get { return _dsTest; }
            set { _dsTest = value; }
        }

        private void formWIP_Load(object sender, EventArgs e)
        {
            if (_dsTest == null)
                throw new Exception("Set DsTest and retry");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (Simulator.IsRunning)
            {
                Simulator.Stop();
                txtStopedAt.Text = DateTime.Now.ToString();
                pictureBox1.Enabled = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (Simulator.IsRunning)
                Simulator.Stop();

            this.Close();
        }

        private void formWIP_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Simulator.IsRunning)
                Simulator.Stop();

        }

        private void formWIP_Shown(object sender, EventArgs e)
        {
            try
            {
                txtStartedAt.Text = DateTime.Now.ToString();
                Application.DoEvents();
                Simulator.ServerStarted += new ServerStatedEventHandler(Simulator_ServerStarted);
                timer1.Enabled = true;
                Simulator.Start(_dsTest);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error on simulation start:" + ex.Message);
            }
        }


        void Simulator_ServerStarted(object sender, EventArgsTelefinServerStarted e)
        {
            this.Text = string.Format("Work in Progress - N.Server:{0}", e.ServerStared);
            Application.DoEvents();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                listCalls.SuspendLayout();
                listCalls.Items.Clear();
                foreach (MethodCallStatistics m in Simulator.MethodCalls)
                {
                    listCalls.Items.Add(new ListViewItem(new string[] { m.Name, m.Count.ToString(), ((int)(m.Duration / m.Count)).ToString() }));
                    //listCalls.Items.Add(new ListViewItem(new string[] { m.Name, m.Count.ToString(), m.Duration.ToString() }));
                }
            }
            finally
            {
                listCalls.ResumeLayout();
            }
        }
    }
}
using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GrisSuite.Data;


namespace GrisSuite
{
    public partial class formLoadReportList : Form
    {
        private dsConfig _ds = new dsConfig();

        public formLoadReportList()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {

            ImpExpRegionList irl = new ImpExpRegionList();

            this.Cursor = Cursors.WaitCursor;
            try
            {
                DataSet ds = irl.ImportFileToDataSet(txtFile.Text);
                _ds.Merge(ds);
                LoadComboTables();
                this.Cursor = Cursors.Default;
                MessageBox.Show("Acquisione conclusa con successo", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void LoadComboTables()
        {
            comboBox1.Items.Clear();
            foreach (DataTable table in _ds.Tables)
            {
            comboBox1.Items.Add(table.TableName);
            }
            comboBox1.SelectedIndex = 0;
        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                dataGridView1.DataSource = _ds;
                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataMember = comboBox1.SelectedItem.ToString();

                dataGridView1.Refresh();

                lblReportCount.Text = string.Format("{0} Records",_ds.Tables[dataGridView1.DataMember].Rows.Count);

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formLoadDefinitions_Load(object sender, EventArgs e)
        {
            progressBar1.Visible = false;
            comboBox1.Visible = true;

            _ds = new dsConfig();
            LoadComboTables();

        }

    }
}
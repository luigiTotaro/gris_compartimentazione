namespace GrisSuite.Util.AutoConfigGUI
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose ( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.gbxParams = new System.Windows.Forms.GroupBox();
			this.cboEcho = new System.Windows.Forms.ComboBox();
			this.lblEcho = new System.Windows.Forms.Label();
			this.cboRS = new System.Windows.Forms.ComboBox();
			this.lblRS = new System.Windows.Forms.Label();
			this.cboAddress = new System.Windows.Forms.ComboBox();
			this.lblAddress = new System.Windows.Forms.Label();
			this.btnFile = new System.Windows.Forms.Button();
			this.txtFile = new System.Windows.Forms.TextBox();
			this.lblFile = new System.Windows.Forms.Label();
			this.cboOperation = new System.Windows.Forms.ComboBox();
			this.lblOperation = new System.Windows.Forms.Label();
			this.btnGo = new System.Windows.Forms.Button();
			this.chkEnableParams = new System.Windows.Forms.CheckBox();
			this.lblAutoConfig = new System.Windows.Forms.Label();
			this.sfileConfig = new System.Windows.Forms.SaveFileDialog();
			this.ofileConfig = new System.Windows.Forms.OpenFileDialog();
			this.cboPort = new System.Windows.Forms.ComboBox();
			this.lblPort = new System.Windows.Forms.Label();
			this.gbxParams.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbxParams
			// 
			this.gbxParams.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
						| System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.gbxParams.Controls.Add(this.cboPort);
			this.gbxParams.Controls.Add(this.lblPort);
			this.gbxParams.Controls.Add(this.cboEcho);
			this.gbxParams.Controls.Add(this.lblEcho);
			this.gbxParams.Controls.Add(this.cboRS);
			this.gbxParams.Controls.Add(this.lblRS);
			this.gbxParams.Controls.Add(this.cboAddress);
			this.gbxParams.Controls.Add(this.lblAddress);
			this.gbxParams.Controls.Add(this.btnFile);
			this.gbxParams.Controls.Add(this.txtFile);
			this.gbxParams.Controls.Add(this.lblFile);
			this.gbxParams.Controls.Add(this.cboOperation);
			this.gbxParams.Controls.Add(this.lblOperation);
			this.gbxParams.Location = new System.Drawing.Point(12, 35);
			this.gbxParams.Name = "gbxParams";
			this.gbxParams.Size = new System.Drawing.Size(416, 189);
			this.gbxParams.TabIndex = 0;
			this.gbxParams.TabStop = false;
			this.gbxParams.Text = "Parametri";
			// 
			// cboEcho
			// 
			this.cboEcho.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboEcho.FormattingEnabled = true;
			this.cboEcho.Items.AddRange(new object[] {
            "Echo on",
            "Echo off"});
			this.cboEcho.Location = new System.Drawing.Point(87, 162);
			this.cboEcho.Name = "cboEcho";
			this.cboEcho.Size = new System.Drawing.Size(121, 21);
			this.cboEcho.TabIndex = 10;
			// 
			// lblEcho
			// 
			this.lblEcho.AutoSize = true;
			this.lblEcho.Location = new System.Drawing.Point(6, 165);
			this.lblEcho.Name = "lblEcho";
			this.lblEcho.Size = new System.Drawing.Size(32, 13);
			this.lblEcho.TabIndex = 9;
			this.lblEcho.Text = "Echo";
			// 
			// cboRS
			// 
			this.cboRS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboRS.FormattingEnabled = true;
			this.cboRS.Items.AddRange(new object[] {
            "RS232",
            "RS485/RS422"});
			this.cboRS.Location = new System.Drawing.Point(87, 135);
			this.cboRS.Name = "cboRS";
			this.cboRS.Size = new System.Drawing.Size(121, 21);
			this.cboRS.TabIndex = 8;
			// 
			// lblRS
			// 
			this.lblRS.AutoSize = true;
			this.lblRS.Location = new System.Drawing.Point(6, 138);
			this.lblRS.Name = "lblRS";
			this.lblRS.Size = new System.Drawing.Size(28, 13);
			this.lblRS.TabIndex = 7;
			this.lblRS.Text = "Tipo";
			// 
			// cboAddress
			// 
			this.cboAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboAddress.FormattingEnabled = true;
			this.cboAddress.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
			this.cboAddress.Location = new System.Drawing.Point(87, 81);
			this.cboAddress.Name = "cboAddress";
			this.cboAddress.Size = new System.Drawing.Size(121, 21);
			this.cboAddress.TabIndex = 6;
			// 
			// lblAddress
			// 
			this.lblAddress.AutoSize = true;
			this.lblAddress.Location = new System.Drawing.Point(6, 84);
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Size = new System.Drawing.Size(69, 13);
			this.lblAddress.TabIndex = 5;
			this.lblAddress.Text = "Indirizzo Perif";
			// 
			// btnFile
			// 
			this.btnFile.Location = new System.Drawing.Point(366, 53);
			this.btnFile.Name = "btnFile";
			this.btnFile.Size = new System.Drawing.Size(34, 23);
			this.btnFile.TabIndex = 4;
			this.btnFile.Text = "...";
			this.btnFile.UseVisualStyleBackColor = true;
			this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
			// 
			// txtFile
			// 
			this.txtFile.Location = new System.Drawing.Point(87, 55);
			this.txtFile.Name = "txtFile";
			this.txtFile.ReadOnly = true;
			this.txtFile.Size = new System.Drawing.Size(273, 20);
			this.txtFile.TabIndex = 3;
			// 
			// lblFile
			// 
			this.lblFile.AutoSize = true;
			this.lblFile.Location = new System.Drawing.Point(6, 58);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(56, 13);
			this.lblFile.TabIndex = 2;
			this.lblFile.Text = "Config File";
			// 
			// cboOperation
			// 
			this.cboOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboOperation.FormattingEnabled = true;
			this.cboOperation.Items.AddRange(new object[] {
            "LETTURA",
            "SCRITTURA"});
			this.cboOperation.Location = new System.Drawing.Point(87, 28);
			this.cboOperation.Name = "cboOperation";
			this.cboOperation.Size = new System.Drawing.Size(121, 21);
			this.cboOperation.TabIndex = 1;
			this.cboOperation.SelectedIndexChanged += new System.EventHandler(this.cboOperation_SelectedIndexChanged);
			// 
			// lblOperation
			// 
			this.lblOperation.AutoSize = true;
			this.lblOperation.Location = new System.Drawing.Point(6, 31);
			this.lblOperation.Name = "lblOperation";
			this.lblOperation.Size = new System.Drawing.Size(61, 13);
			this.lblOperation.TabIndex = 0;
			this.lblOperation.Text = "Operazione";
			// 
			// btnGo
			// 
			this.btnGo.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
			this.btnGo.Location = new System.Drawing.Point(353, 236);
			this.btnGo.Name = "btnGo";
			this.btnGo.Size = new System.Drawing.Size(75, 49);
			this.btnGo.TabIndex = 1;
			this.btnGo.Text = "Avvio";
			this.btnGo.UseVisualStyleBackColor = true;
			this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
			// 
			// chkEnableParams
			// 
			this.chkEnableParams.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
			this.chkEnableParams.AutoSize = true;
			this.chkEnableParams.Location = new System.Drawing.Point(327, 12);
			this.chkEnableParams.Name = "chkEnableParams";
			this.chkEnableParams.Size = new System.Drawing.Size(101, 17);
			this.chkEnableParams.TabIndex = 2;
			this.chkEnableParams.Text = "Abilita Parametri";
			this.chkEnableParams.UseVisualStyleBackColor = true;
			this.chkEnableParams.CheckedChanged += new System.EventHandler(this.chkEnableParams_CheckedChanged);
			// 
			// lblAutoConfig
			// 
			this.lblAutoConfig.AutoSize = true;
			this.lblAutoConfig.Location = new System.Drawing.Point(18, 261);
			this.lblAutoConfig.Name = "lblAutoConfig";
			this.lblAutoConfig.Size = new System.Drawing.Size(102, 13);
			this.lblAutoConfig.TabIndex = 3;
			this.lblAutoConfig.Text = "Auto Configurazione";
			// 
			// cboPort
			// 
			this.cboPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboPort.FormattingEnabled = true;
			this.cboPort.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
			this.cboPort.Location = new System.Drawing.Point(87, 108);
			this.cboPort.Name = "cboPort";
			this.cboPort.Size = new System.Drawing.Size(121, 21);
			this.cboPort.TabIndex = 12;
			// 
			// lblPort
			// 
			this.lblPort.AutoSize = true;
			this.lblPort.Location = new System.Drawing.Point(6, 111);
			this.lblPort.Name = "lblPort";
			this.lblPort.Size = new System.Drawing.Size(67, 13);
			this.lblPort.TabIndex = 11;
			this.lblPort.Text = "Porta Seriale";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(440, 297);
			this.Controls.Add(this.lblAutoConfig);
			this.Controls.Add(this.chkEnableParams);
			this.Controls.Add(this.btnGo);
			this.Controls.Add(this.gbxParams);
			this.Icon = ( (System.Drawing.Icon)( resources.GetObject("$this.Icon") ) );
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.gbxParams.ResumeLayout(false);
			this.gbxParams.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox gbxParams;
		private System.Windows.Forms.Button btnGo;
		private System.Windows.Forms.CheckBox chkEnableParams;
		private System.Windows.Forms.Label lblOperation;
		private System.Windows.Forms.ComboBox cboOperation;
		private System.Windows.Forms.Button btnFile;
		private System.Windows.Forms.TextBox txtFile;
		private System.Windows.Forms.Label lblFile;
		private System.Windows.Forms.ComboBox cboAddress;
		private System.Windows.Forms.Label lblAddress;
		private System.Windows.Forms.ComboBox cboRS;
		private System.Windows.Forms.Label lblRS;
		private System.Windows.Forms.ComboBox cboEcho;
		private System.Windows.Forms.Label lblEcho;
		private System.Windows.Forms.Label lblAutoConfig;
		private System.Windows.Forms.SaveFileDialog sfileConfig;
		private System.Windows.Forms.OpenFileDialog ofileConfig;
		private System.Windows.Forms.ComboBox cboPort;
		private System.Windows.Forms.Label lblPort;
	}
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.IO;

namespace GrisSuite.Util.AutoConfigGUI
{
	public partial class MainForm : Form
	{
		private bool _bSTLCManagerServiceStopped;

		public MainForm ()
		{
			InitializeComponent();

			this.cboOperation.SelectedIndex = 0;
			this.cboAddress.SelectedIndex = 0;
			this.cboPort.SelectedIndex = 0;
			this.cboRS.SelectedIndex = 0;
			this.cboEcho.SelectedIndex = 0;

			this.chkEnableParams.Checked = false;
			this.EnableParameters(false);
		}

		private void btnFile_Click ( object sender, EventArgs e )
		{
			if ( this.cboOperation.SelectedItem.ToString() == "LETTURA" )
			{
				this.sfileConfig.Filter = "File di testo|*.txt";
				if ( DialogResult.OK == this.sfileConfig.ShowDialog() )
				{
					this.txtFile.Text = this.sfileConfig.FileName;
				}
			}
			else 
			{
				this.ofileConfig.Filter = "File di testo|*.txt";
				if ( DialogResult.OK == this.ofileConfig.ShowDialog() )
				{
					this.txtFile.Text = this.ofileConfig.FileName;
				}
			}
		}

		private void btnGo_Click ( object sender, EventArgs e )
		{
			string autoConfigPath = Application.ExecutablePath.Replace("AutoConfigGUI", "AutoConfigurator");

			if ( !File.Exists(autoConfigPath) ) MessageBox.Show(this, "L'interfaccia grafica deve risiedere nella stessa cartella in cui si trova l'applicativo AutoConfigurator.exe", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);

			if ( this.chkEnableParams.Checked )
			{
				if ( this.txtFile.Text.Length > 0 && System.IO.Path.IsPathRooted(this.txtFile.Text) )
				{
					// args: null autoconfigurazione      
					//       READ                       READ        Legge configurazione da dispositivo e genera file 
					//                                  WRITE       Scrive configurazione da file e scrive in EEProm dispositivo 
					//       FILE                                   Nome del file contenente la configurazione
					//       Addr                       00          indirizzo periferica
					//       2,9600,N,8,1,T,T           2           porta seriale
					//                                  9600        baud rate
					//                                  N(E)(O)     parit� N=nessuna, E Even, O Odd
					//                                  8           data
					//                                  1           stop bit 0=nessuno, 1 = 1 stop bit, 1.5, 2
					//                                  T           T=232, F=485/422
					//                                  T           T=Echo on, F= Echo Off
					//		gestione dei servizi		Y			Y=yes, N=no
					//

					// AutoConfigurator R File.cds 04 3,9600,N,8,1,F,T Y
					string args = "{0} {1} {2} {3},9600,N,8,1,{4} Y";
					string varArgs = "";

					switch ( this.cboRS.SelectedItem.ToString() )
					{
						case "RS232":
							{
								varArgs += "T,";
								break;
							}
						case "RS485/RS422":
							{
								varArgs += "F,";
								break;
							}
					}

					switch ( this.cboEcho.SelectedItem.ToString() )
					{
						case "Echo on":
							{
								varArgs += "T";
								break;
							}
						case "Echo off":
							{
								varArgs += "F";
								break;
							}
					}

					// Cristian 25-03-2008
					//if ( !this.StopSTLCServices() )
					//{
					//    MessageBox.Show(this, "Impossibile arrestare il servizio \"STLC Manager Service\" o \"STLC Supervisor Server Service\".", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
					//    return;
					//}

					//Thread.Sleep(10000);

					Process proc = new Process();
					try
					{
						proc.StartInfo.FileName = autoConfigPath;
						proc.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
						proc.StartInfo.Arguments =
							string.Format(
								args,
								( this.cboOperation.SelectedItem.ToString() == "LETTURA" ? "R" : "W" ),
								string.Format("\"{0}\"", this.txtFile.Text),
								this.cboAddress.SelectedItem.ToString(),
								this.cboPort.SelectedItem.ToString(),
								varArgs);
						proc.Start();

						Clipboard.Clear();
						Clipboard.SetData(DataFormats.StringFormat, string.Format("\"{0}\" {1}", autoConfigPath, proc.StartInfo.Arguments));
						MessageBox.Show(this, string.Format("Comando eseguito: \"{0}\" {1} e copiato in clipboard!", autoConfigPath, proc.StartInfo.Arguments), "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					catch ( Exception exc )
					{
						MessageBox.Show(this,exc.Message + Environment.NewLine + ( exc.StackTrace ?? "" ), "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					finally
					{
						proc.Dispose();

						//Thread.Sleep(10000);

						//// Cristian 25-03-2008
						//if ( !this.StartSTLCServices() )
						//{
						//    MessageBox.Show(this, "Impossibile avviare il servizio \"STLC Manager Service\".", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
						//}
					}
				}
				else 
				{
					MessageBox.Show(this, "Specificare un path valido per il file di configurazione.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else 
			{
				try
				{
					using ( Process proc = new Process() )
					{
						proc.StartInfo.FileName = autoConfigPath;
						proc.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
						proc.Start();
					}
				}
				catch ( Exception exc )
				{
					MessageBox.Show(this, exc.Message, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void chkEnableParams_CheckedChanged ( object sender, EventArgs e )
		{
			this.EnableParameters(this.chkEnableParams.Checked);
		}

		private void EnableParameters ( bool enable )
		{
			if ( enable )
			{
				this.gbxParams.Enabled = true;
				this.lblAutoConfig.Text = "";
			}
			else
			{
				this.gbxParams.Enabled = false;
				this.lblAutoConfig.Text = "Auto Configurazione";
			}
		}

		public bool StartSTLCServices ()
		{
			if ( _bSTLCManagerServiceStopped )
			{
				try
				{
					using ( ServiceController oServiceControl = new ServiceController("STLCManagerService") )
					{
						if ( oServiceControl != null )
						{
							if ( oServiceControl.Status == ServiceControllerStatus.Stopped )
							{
								oServiceControl.Start();
								_bSTLCManagerServiceStopped = false;

								do
								{
									Thread.Sleep(500);
									oServiceControl.Refresh();
								} while ( oServiceControl.Status != ServiceControllerStatus.Running );

								return true;
							}
						}
					}
				}
				catch ( Exception )
				{
					return false;
				}
			}

			return false;
		}

		public bool StopSTLCServices ()
		{
			try
			{
				using ( ServiceController oServiceControl = new ServiceController("STLCManagerService") )
				{
					if ( oServiceControl != null )
					{
						if ( oServiceControl.Status == ServiceControllerStatus.Running )
						{
							oServiceControl.Stop();
							_bSTLCManagerServiceStopped = true;

							do
							{
								Thread.Sleep(500);
								oServiceControl.Refresh();
							} while ( oServiceControl.Status != ServiceControllerStatus.Stopped );
						}
					}
				}

				using ( ServiceController oServiceControl = new ServiceController("SPVServerDaemon") )
				{
					if ( oServiceControl != null )
					{
						if ( oServiceControl.Status == ServiceControllerStatus.Running )
						{
							oServiceControl.Stop();

							do
							{
								Thread.Sleep(500);
								oServiceControl.Refresh();
							} while ( oServiceControl.Status != ServiceControllerStatus.Stopped );
						}
					}
				}

				return true;
			}
			catch ( Exception )
			{
				return false;
			}
		}

		private void cboOperation_SelectedIndexChanged ( object sender, EventArgs e )
		{
			this.txtFile.Text = "";
		}
	}
}
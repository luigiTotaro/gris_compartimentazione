/**
 * Interfaccia per la gestione delle classi che implementano gli endpoint esposti. Tutte le 
 * classi che implementano questa interfaccia sono responsabili della della gestione di un endpoint
 * @interface iEndpoint
 */
'use strict';
const config = require('../../config.js');



class iEndpoint {

    constructor(express_instance) {
        this.attach_endpoint(express_instance);
    }

    attach_endpoint(express_instance) {
        throw 'abstract method';
    }

    processMessage(data) {
		

	}   


}


module.exports = iEndpoint;
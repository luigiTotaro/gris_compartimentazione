'use strict';
const iEndpoint = require('../iEndpoint.js');
const logger = require('../../utils/logger.js'); 
const config = require('../../../config.js');
const broker = require('../../broker/broker.js');
const CommonDAO = require('../../database/CommonDAO.js');

class GetDeviceCommand extends iEndpoint {


    constructor(express_instance) {
        super(express_instance);
    
    }


    attach_endpoint(express_instance) {

        express_instance.post('/device_command/', function(request, response) {
           

            let requestIp=request.connection.remoteAddress;
            console.log(requestIp);
            if (config.http.allowedIP.indexOf(requestIp)==-1)
            {
                response.status(200).json({
                    status : 'Webapp Agent not Available'
                });
                return;
            }

            if(!request.body.data) {
                response.status(200).json({
                    status : 'parametro "data" obbligatorio'
                });
                return;
            }


            let data = request.body.data;

            var DestinationRegionId=data.DestinationRegionId;

            logger.log("info", "[device_command] ricevuto - RegID: " + DestinationRegionId + " - NodeID: " + data.DestinationNodeId + " - DeviceID: " + data.DeviceId + " - Comando: " + data.IdComando);
            //ho un broker con quel region ID?
            //il nodeId di destinazione potrebbe non essere giusto, magari � un node id attaccato a un agent che ha come nodeid principale un altro,
            //perci� quell'agent non avr� un subscriber in grado di rispondere a questo messaggio. Devo cercare il nodeId principale (la maggior parte delle volte sar� lo stesso...)

            CommonDAO.getMainNodeId(data.DestinationNodeId, (err, result) => {
			
                let newNodeId=data.DestinationNodeId;
                if(err)
                {
                    logger.log("info", "Non riesco a recuperare il NodeId principale relativo al NodeId della richiesta - Uso quello della richiesta");
                }
                else
                {
                    if (result.length==0)
                    {
                        logger.log("info", "Non esiste un NodeId principale relativo al NodeId della richiesta - Uso quello della richiesta");
                    }
                    else
                    {
                        newNodeId = result[0].nodid;
                        logger.log("info", "Recuperato il NodeId principale relativo al NodeId della richiesta: " + newNodeId + " - Uso questo per il comando");
                    }
                }

                try {
                    broker.publish(DestinationRegionId, 'PUBLISHER_COMMAND', newNodeId, data);
                } catch(e) {
                    response.status(200).json({
                        status : e
                    });
                    return;
                }
    
                response.status(200).json({
                    status : "Comando Inviato, in attesa della risposta dal Device"
                });
                
        
            });            

            

    
        });
        
        
    }

}

module.exports = GetDeviceCommand;
'use strict';
const iEndpoint = require('../iEndpoint.js');
const logger = require('../../utils/logger.js'); 
const config = require('../../../config.js');
const broker = require('../../broker/broker.js');

class GetTest extends iEndpoint {


    constructor(express_instance) {
        super(express_instance);
    
    }


    attach_endpoint(express_instance) {

        express_instance.get('/test/', function(request, response) {
           
            let requestIp=request.connection.remoteAddress;
            console.log(requestIp);
            if (config.http.allowedIP.indexOf(requestIp)!=-1)
            {
                response.status(200).json({
                    state : 'Webapp Agent Online'
                });
            }
            else
            {
                response.status(200).json({
                    state : 'Webapp Agent Not Available'
                });
            }

    
        });
        
        
    }

}

module.exports = GetTest;
'use strict';
var express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

module.exports = (() => {

    let _init = (express_instance) => {

        // for parsing application/json
        express_instance.use(bodyParser.json({limit:'50mb'})); 
        // for parsing application/x-www-form-urlencoded
        express_instance.use(bodyParser.urlencoded({ 
           
            limit: '50mb'
        })); 
		
        express_instance.use('/html', express.static(__dirname + '/../html'));
        
        const endpoint_path = path.normalize(path.join(__dirname, 'endpoints', 'implementations'));

        fs.readdirSync(endpoint_path).forEach(file => {
             
            console.log(file);
			let class_def = require(path.join(endpoint_path, file));
            new class_def(express_instance);

        });

    };

    return {
        init: _init
    }
    
})();
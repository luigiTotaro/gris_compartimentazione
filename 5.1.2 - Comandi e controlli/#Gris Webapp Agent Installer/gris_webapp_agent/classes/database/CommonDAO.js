"use strict";
const db_conn = require('./db_connection.js');

module.exports = {
	


	/** 
 	* @function 'getSTLCData' - Recupera i dati dalla tabella stlc_parameters
 	* @param {callback} on_complete - function(err, result)
 	*/	
	updateCommandHistory: function(message,on_complete) {

		var id = message.commandReplyId;
		var result= message.result;
		var serverId = message.serverId;
		var deviceId = message.deviceId;

		if (serverId==null) //nessun controllo, eseguo subito
		{
			var ret;
			//var changedUserString = result.replace("'","''"); 
			var changedUserString = result.replace(/'/g, "''")
			var query="UPDATE command_history SET response='"+changedUserString+"', DateReceived=CURRENT_TIMESTAMP, RealReplyDate=CURRENT_TIMESTAMP WHERE Command_historyID="+id+" AND RealReplyDate IS NULL";		
			db_conn.update(query, (err, result) => {
				if (err) ret=null;
				else ret = result;
				on_complete(err, ret)
				return;
			}); 			
		}
		else
		{
			//prima parte, quel device appartiene a quel server id? (possibile problema di 2 Mqtt agent sullo stesso nodo)
			var query="SELECT COUNT(*) as conto FROM devices where DevID="+deviceId+" AND SrvID="+serverId;		
			db_conn.select(query, (err, res) => {
				if (err)
				{
					//errore, non faccio niente
					on_complete("Errore nel controllo associazione ServerID - DeviceID", null);
					return;
				}
				else
				{
					//l'ha trovato?
					if (res != null)
					{
						if (res[0].conto==0)
						{
							//il device id non � associato al server
							on_complete("Il Device oggetto di questa risposta non � associato al Server indicato", null);
							return;
						}
						else
						{
							//tutto ok, device e server sono associati
							//secondo controllo, ci possono essere pi� webapp agent attivi che ricevono la medesima risposta. aspetto un intervallo casuale e poi provo l'update
							var ret;
							//var changedUserString = result.replace("'","''"); 
							var changedUserString = result.replace(/'/g, "''")
							var query="UPDATE command_history SET response='"+changedUserString+"', DateReceived=CURRENT_TIMESTAMP, RealReplyDate=CURRENT_TIMESTAMP WHERE Command_historyID="+id+" AND RealReplyDate IS NULL";		
							db_conn.update(query, (err, result) => {
								if (err) ret=null;
								else ret = result;
								on_complete(err, ret)
					
							}); 


						}
					}
					else
					{
						//errore, non faccio niente
						on_complete("Nessuna associazione ServerID - DeviceID", null);
						return;
					}


				}

			}); 		
		}

	},

	getBrokerInfos: function(on_complete) {

		var ret;
		var query="SELECT * FROM regions_ext";		
		db_conn.select(query, (err, result) => {
			if (err) ret=null;
			else ret = result;
			on_complete(err, ret)

		}); 
	},

	getMainNodeId: function(nodeId, on_complete) {

		var ret;
		var query="select nodid from servers where srvid in (select max(srvid) from devices where nodid="+nodeId+")";		
		db_conn.select(query, (err, result) => {
			if (err) ret=null;
			else ret = result;
			on_complete(err, ret)

		}); 
	},	

}
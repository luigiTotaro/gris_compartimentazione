/**
 * Interfaccia per la gestione delle classi che implementano i publisher verso il broker. Tutte le 
 * classi che implementano questa interfaccia sono responsabili della sottoscrizione a determinati topic
 * verso il broker
 * @interface iPublisher
 */

'use strict';

const config = require('../../../config.js');
const path = require('path');
const fs = require('fs');


class iPublisher {

	constructor(opts) {
		
		this.opts = opts;
		
		if(!this.opts.topic) {
			throw 'parametro "topic" obbligatorio';
		}

		if(!this.opts.key) {
			throw 'parametro "key" obbligatorio';
		}

	}


	get_key() {

		return this.opts.key;
	
	}



	publish(client, extTopic, message) {
		
		if(client.constructor.name !== 'MqttClient') {
			throw 'parametro client non di tipo MqttClient';
		}

		let _msg = null;

		if(typeof message === 'object') {
			_msg = JSON.stringify(message);
		} else if (typeof message === 'string') {
			_msg = message;
		} else {
			throw 'tipo di messaggio ' + (typeof message) + ' non gestito';
		}
		

		client.publish(this.opts.topic+"_"+extTopic, _msg, config.broker_publish_ops, (err) => {
		});


	}



}

module.exports = iPublisher;












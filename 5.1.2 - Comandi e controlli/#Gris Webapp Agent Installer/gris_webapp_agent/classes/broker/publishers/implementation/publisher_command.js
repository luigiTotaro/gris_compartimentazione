"use strict";

const iPublisher = require('../iPublisher.js');
const logger = require('../../../utils/logger.js'); 

class publisher_command extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_COMMAND',
	    	topic: '/command'
	    });
	}

	publish(client, extTopic, message) {

		//console.log("PUBLISHER_COMMAND Invio messaggio.....");
		logger.log("info", "Invio messaggio come Publisher di command");
		super.publish(client, extTopic, message);
	}
	

}

module.exports = publisher_command;
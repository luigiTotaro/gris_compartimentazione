"use strict";

const iSubscriber = require('../iSubscriber.js');
const CommonDAO = require('../../../database/CommonDAO.js');
const logger = require('../../../utils/logger.js');


class subscriber_commandReply extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/commandReply'
	    });
	}

	on_message(topic, message, client) {
		//il client qui non serve, ma viene restituito per eventuali controlli su chi ha spedito il messaggio
		var msgStr= message.toString();
		var msg = JSON.parse(msgStr);
		logger.log("info", "Ricevuto messaggio come Subscriber di commandReply: "+msgStr);
		CommonDAO.updateCommandHistory(msg, (err) => {
			logger.log("info", "Ritornato dall'update Command History con err: " + err);
		})
	}

}

module.exports = subscriber_commandReply;
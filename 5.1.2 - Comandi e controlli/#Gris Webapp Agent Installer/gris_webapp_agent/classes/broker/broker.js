"use strict";
const config = require('../../config.js');
const mqtt = require('mqtt');
const fs = require("fs");
const path = require('path');
const iSubscriber = require('./subscribers/iSubscriber.js');
const iPublisher = require('./publishers/iPublisher.js');
const logger = require('../utils/logger.js');

/**
 * Modulo per la gestione della connessione e degli eventi di sottoscrizione/pubblicazione verso un broker
 * @module classes/broker
 */
module.exports = (() => {

		
	var clientArray = [];

	var _subscribers = [];
	//var _subscribers = {};
	var _publishers = [];
	var _regionID = '';


	var _register_subscribers = () => {
		
		// caricamento di tutti le classi presenti nel folder 'subscribers/implementation'
		const subscribers_class_path = path.normalize(path.join(__dirname, 'subscribers', 'implementation'));

		fs.readdirSync(subscribers_class_path).forEach(file => {

			try {

				let cl = require(path.join(subscribers_class_path, file));

				
				//per ognuno dei client, registro la classe
				for (var elemento in clientArray)
				{
					//console.log(elemento + '-' + clientArray[elemento].options.host + '');

					let instance = new cl();

					if(!(instance instanceof iSubscriber)) {
						throw 'la classe ' + file + ' deve essere una istanza di iSubscriber';
					}
	
					instance.register(clientArray[elemento]);
					if(!_subscribers[elemento])
					{
						_subscribers[elemento] = {};
					}
					_subscribers[elemento][instance.get_topic()] = instance;
				}
				
/*
				let instance = new cl();

				if(!(instance instanceof iSubscriber)) {
					throw 'la classe ' + file + ' deve essere una istanza di iSubscriber';
				}

				instance.register(_client);
				_subscribers[instance.get_topic()] = instance;
*/			
			} catch(e) {

				console.log(e, 'nel file', file);

			}

		});

	};



	var _register_publishers = () => {

		// caricamento di tutti le classi presenti nel folder 'publishers/implementation'
		const publishers_class_path = path.normalize(path.join(__dirname, 'publishers', 'implementation'));

		fs.readdirSync(publishers_class_path).forEach(file => {

			try {
				
				//non lo faccio per ogni client, verrà scelto dopo in fase di pubblicazione
				let cl = require(path.join(publishers_class_path, file));

				let instance = new cl();

				if(!(instance instanceof iPublisher)) {
					throw 'la classe ' + file + ' deve essere una istanza di iPublisher';
				}
				
				_publishers[instance.get_key()] = instance;

			
			} catch(e) {

				console.log(e, 'nel file', file);

			}

		});

	};

 
 	/** 
 	* @function 'connect' - connessione verso il broker. instanzia gli eventi tipici del 
	 * protocollo mqtt (error/connect/ecc...)
	 * @param {callback} events.on_connect - function chiamata UNA SOLA VOLTA appena sento la prima connessione al broker...
	 * @param {callback} events.on_offline - function chiamata UNA SOLA VOLTA appena sento il primo evento offline...
 	*/
	var _connect = (RegID, events) => {

		let has_run_first_connection_callback = false;
		let has_run_first_offline_callback = false;


		config.broker_srv_opts.clientId = RegID + "_" + config.webappAgentIdentificationId;
		//logger.log("info","mi connetto al broker " + config.broker_srv_opts.host);
		clientArray[RegID] = mqtt.connect(config.broker_srv_opts);
		//console.log("Nuovo Client creato - Region: " + RegID + " - host: " + config.broker_srv_opts.host);

		clientArray[RegID].on('error', (err) => {
			logger.log('error', err);
		});

		clientArray[RegID].on('offline', function(e) {
			logger.log('warn','OFFLINE: '+ this.options.host);
			if(has_run_first_offline_callback === false) {
				has_run_first_offline_callback = true;
				if(events && events.on_offline) {
					events.on_offline();
				}			
			}
//			logger.log('warn','broker offline - RegionID: ' + self.RegID);
//			console.log("Region: " + self.RegID + " - host: " + self.options.host);
		});

		clientArray[RegID].on('connect', function(e) {
			logger.log('info','CONNESSO: '+ this.options.host);
			if(has_run_first_connection_callback === false) {
				has_run_first_connection_callback = true;
				if(events && events.on_connect) {
					events.on_connect();
				}			
			}
//			logger.log('info','broker connesso - RegionID: ' + self.RegID);
//			console.log("Region: " + self.RegID + " - host: " + self.options.host);
		});

		clientArray[RegID].on('reconnect', function(e) {
			logger.log('warn','RECONNECT: '+ this.options.host);
//			logger.log('warn','riconnessione al broker - RegionID: ' + self.RegID);
//			console.log("Region: " + self.RegID + " - host: " + self.options.host);
		});

		clientArray[RegID].on('message', function(topic, message) {
			// delego la gestione dell'evento al metodo 'on_message' della
			// rispettiva classe (che estende da iSubscriber) caricata dinamicamente
			// all'avvio.
			
			//dal client id devo togliere il webappAgentIdentificationId
			let myRegId=this.options.clientId.split("_")[0];
			_subscribers[myRegId][topic].on_message(topic, message, this);
		});

		//_register_subscribers();
		//_register_publishers();
	};






	var _publish = (RegID, key, extTopic, message) => {

  		if(!message) {
			logger.log('error','message vuoto');  
			throw 'Errore di sistema: 1002';
		}
		  
		//esiste la connessione verso quel region ID ed è attiva?
		//let trovato=false;
		if (!clientArray[RegID])
		{
			logger.log('error','Connessione verso il Broker di compartimento non disponibile: ' + RegID);
			throw 'Connessione verso il Broker di compartimento non disponibile';
		}
		
		let myClient=clientArray[RegID];

		/*
		for (let i=0;i<clientArray.length;i++)
		{
			if (clientArray[i].RegID==RegID)
			{
				trovato=true;
				myClient=clientArray[i];
				break;
			}
		}
		if (!trovato){
			throw 'Connessione verso il Broker con Region ID: ' + RegID + ' non trovata';
		}
		*/
		
		//è connesso?
		if (myClient.connected==false)
		{
			logger.log('error','Broker di compartimento al momento Offline: ' + RegID);
			throw 'Broker di compartimento al momento Offline';
		}

  		if(!_publishers[key]) {
			logger.log('error','handler per il publish con chiave "' + key + '" mancante');  
			throw 'Errore di sistema: 1001';
  		}

  		_publishers[key].publish(myClient, extTopic, message);

	};

	var _registerSub_Pub = () => {

		console.log("Register Sub & Pub");
		_register_subscribers();
		_register_publishers();
	};
	



	return {
		connect:_connect,
		publish:_publish,
		registerSub_Pub:_registerSub_Pub,
		regionID: _regionID
	}

})();



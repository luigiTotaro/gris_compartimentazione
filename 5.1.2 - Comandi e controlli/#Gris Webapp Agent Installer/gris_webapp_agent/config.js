var path = require('path');
var fs = require('fs');

 
/**
 * modulo di configurazione contiene i parametri di inizializzazione di tutta l'applicazione.
 * Questo modulo deve accentrare tutte le opzioni di configurazione
 * @module config
 * @param {json} broker_srv_opts - impostazione per la connessione al broker mqtt
 * @param {string} broker_srv_opts.port - porta di connessione al broker mqtt
 * @param {string} broker_srv_opts.host - indirizzo di connessione al broker mqtt
 * @param {string} broker_srv_opts.clean - true/false indica se si vuole eseguire una connessione 'clean' verso il broker MQTT {@link https://www.hivemq.com/docs/hivemq/latest/}
 * @param {string} broker_srv_opts.clientId - id del client verso il broker. è obbligatorio se il parametro clean è 'true'
 * @param {int} broker_srv_opts.protocolVersion - versione del protocollo MQTT (di default è 4)
 * @param {string} broker_srv_opts.key - contenuto della chiave privata in formato .pem del client. il certificato viene emesso dal server e conservato sul server in un trust store
 * @param {string} broker_srv_opts.cert - contenuto del certificato in formato .pem del client. il certificato viene emesso dal server e conservato sul server in un trust store
 * @param {string} broker_srv_opts.rejectUnauthorized - impostare su false in caso di certificati autoprodotti
 * @param {string} broker_srv_opts.protocol - tipo di protocollo 'mqtts','mqtt'. {@link https://www.npmjs.com/package/mqtt#connect} 
 * @param {string} broker_srv_opts.username - nome utente
 * @param {string} broker_srv_opts.password - password
 * @param {int} broker_srv_opts.reconnectPeriod - delta temporale prima di ritentare una connessione
 * @param {int} broker_srv_opts.connectTimeout - timeout di connessione (allo scadere il client genera un evento 'reconnect')
 * @param {json} broker_subscribe_ops - opzioni utilizzate dal client per la sottoscrizione ad un topic
 * @param {int} broker_subscribe_ops.qos - qos da applicare nella sottoscrizione 
 * @param {json} broker_publish_ops - opzioni utilizzate dal client per la pubblicazione verso un topic
 * @param {int} broker_publish_ops.qos - qos da applicare nella sottoscrizione 

 * @param {json} database - opzioni per la connessione verso un database mssql
 * @param {boolean} database.start_local_sql_browser - true/false, indica se far partire il servizio SQL Browser perima di connettersi al db
 * @param {string} database.user - nome utente
 * @param {string} database.password - password
 * @param {string} database.server - indirizzo ip del server
 * @param {string} database.database - nome del database
 * @param {int} database.pool.max - numero massimo di connessioni verso il pool
 * @param {int} database.pool.min - numero minimo di connessioni verso il pool
 * @param {int} database.pool.idleTimeoutMillis - tempo di inattività prima di chiudere una connessione
 * @param {int} database.options.encrypt - crypt della connessione (true se Windows Azure)
 * @param {json} logfile - opzioni per il logging su file secondo le modalità della libreria winston {@link https://github.com/winstonjs/winston}
 * @param {string} logfile.file - nome del file (oppure nome con percorso completo)
 * @param {int} logfile.maxsize - massima dimensione del file di log prima di effettuare una rotazione

  */
module.exports = {

	broker_srv_opts_defaults: {
		port: '8883',
		host: '192.170.5.81',
		//clean:false,
		protocolVersion: 4,
		//key: fs.readFileSync(path.join(__dirname, 'certs', 'client-key.pem')),
		//cert: fs.readFileSync(path.join(__dirname, 'certs', 'client-cert.pem')),
		//rejectUnauthorized: false,
		protocol: 'mqtt',
		clientId: 'client_' + (new Date()).getTime(),
		username: 'hivemq-user3',
		password: 'user3',
		reconnectPeriod: 10000,
		connectTimeout: 20000
	},


	broker_subscribe_ops: {
		qos:2
	},


	broker_publish_ops: {
		qos:2
	},

	webappAgentIdentificationId: "1", //da cambiare per ogni webappa agent che si connette contemporaneamente ai broker

	http: {
		port: 8916,
		allowedIP: [
			"127.0.0.1",
			"10.211.55.2"
		]
    },

	database: {
		//SVILUPPO, verso il db sulla 40
		
		start_local_sql_browser: false,
		user: 'GrisRestServiceUser',
		password: '1234',
	    //server: '192.170.5.21',
	    server: '10.10.10.40',

		database: 'GrisSuite.DB.Server',

	    options: {
	        encrypt: false 
	    },
	    
		
		// PRODUZIONE
		/*
		start_local_sql_browser: true,
	    user: 'sa',
	    password: 'StLc!000',
	    server: '127.0.0.1',
	    database: 'telefin',

	    options: {
	        encrypt: false,
			instanceName: 'SQLEXPRESS',
			trustedConnection: true,
			localAddress: '127.0.0.1'
	    },
	    */

	    pool: {
	        max: 10,
	        min: 5,
	        idleTimeoutMillis: 10000
	    }

	},


	logfile: {
		file: 'log.txt',
		level: 'info', //debug, info, warn, error
		maxsize: 100*1024*5,
		maxfiles: 1
	},

	testMode: false

}
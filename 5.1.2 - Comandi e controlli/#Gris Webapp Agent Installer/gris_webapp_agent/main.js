'use strict';
const db_conn = require('./classes/database/db_connection.js');
const config = require('./config.js');
const logger = require('./classes/utils/logger.js');
const commonDao = require('./classes/database/CommonDAO.js');
const broker = require('./classes/broker/broker.js');
var path = require('path');
var fs = require('fs');

var express = require('express');
var app = express();
const endpoints = require('./classes/endpoints.js');

let brokerInfos=new Array();


/**
 * main module. entry point per il servizio agent
 * avviare con: node main.js 
 * @module main
 */


db_conn.connect((err) => {

    if(err) {
        console.log(err);
        process.exit();
    }

	start(); //faccio partire il servizio REST comunque
	getBrokerInfos(); //faccio partire le varie connessioni ai broker

});



function getBrokerInfos()
{
	//devo prendere dal db tutte le informazioni sui broker e connettermi a tutti
	commonDao.getBrokerInfos((err, result) => {
			
		if(err)
		{
			//non mi posso connettere ai broker
			console.log(err);
			process.exit();
		}
		else
		{

			brokerInfos=result;
			
			//per test limito i broker a 1, 2 o 3
			//brokerInfos.push(result[0]);
			//brokerInfos.push(result[1]);
			//brokerInfos.push(result[2]);

			brokerConnections(0); //primo elemento
		}

	});



}


function brokerConnections(index)
{
	
	/*
	//per debug, connessione a 2 broker diversi
	if (brokerInfos[index].RegID=='281474976645122')
	{
		config.broker_srv_opts=new Object();

		config.broker_srv_opts.port= '8883';
		config.broker_srv_opts.host= '192.170.5.81';
		config.broker_srv_opts.clean=false;
		config.broker_srv_opts.protocolVersion= 3;
		config.broker_srv_opts.key= fs.readFileSync(path.join(__dirname, 'certs', 'client-key.pem'));
		config.broker_srv_opts.cert= fs.readFileSync(path.join(__dirname, 'certs', 'client-cert.pem'));
		config.broker_srv_opts.rejectUnauthorized= false;
		config.broker_srv_opts.protocol= 'mqtts';
		config.broker_srv_opts.clientId= 'client_' + (new Date()).getTime();
		config.broker_srv_opts.username= 'hivemq-user3';
		config.broker_srv_opts.password= 'user3';
		config.broker_srv_opts.reconnectPeriod= 10000;
		config.broker_srv_opts.connectTimeout= 20000;
	}
	else
	{
		config.broker_srv_opts=new Object();

		config.broker_srv_opts.port= '1883';
		config.broker_srv_opts.host= 'test.mosquitto.org';
		config.broker_srv_opts.clean=false;
		config.broker_srv_opts.protocolVersion= 4;
		config.broker_srv_opts.protocol= 'mqtt';
		config.broker_srv_opts.clientId= 'client_' + (new Date()).getTime();
		config.broker_srv_opts.reconnectPeriod= 10000;
		config.broker_srv_opts.connectTimeout= 20000;		
	}
	*/

	config.broker_srv_opts=new Object();

	config.broker_srv_opts.clean=config.broker_srv_opts_defaults.clean;
	config.broker_srv_opts.protocolVersion= config.broker_srv_opts_defaults.protocolVersion;
	config.broker_srv_opts.key= config.broker_srv_opts_defaults.key;
	config.broker_srv_opts.cert= config.broker_srv_opts_defaults.cert;
	config.broker_srv_opts.rejectUnauthorized= config.broker_srv_opts_defaults.rejectUnauthorized;
	config.broker_srv_opts.protocol= config.broker_srv_opts_defaults.protocol;
	config.broker_srv_opts.clientId= config.broker_srv_opts_defaults.clientId;
	config.broker_srv_opts.reconnectPeriod= config.broker_srv_opts_defaults.reconnectPeriod;
	config.broker_srv_opts.connectTimeout= config.broker_srv_opts_defaults.connectTimeout;	


	config.broker_srv_opts.host = brokerInfos[index].BrokerIP;
	config.broker_srv_opts.username = brokerInfos[index].Username;
	config.broker_srv_opts.password = brokerInfos[index].Password;
	config.broker_srv_opts.port = brokerInfos[index].Port;
	
	logger.log('info','Connessione al Broker - Broker IP: '+config.broker_srv_opts.host+ " - Broker Port: " + config.broker_srv_opts.port + " - Broker Username: " + config.broker_srv_opts.username + " - RegionID: " + brokerInfos[index].RegID);
	try
	{
		
		broker.connect(brokerInfos[index].RegID,{
			on_connect: () => {
				//console.log("Connesso");
				/*
				index++;
				if (index==brokerInfos.length)
				{
					//ho finito, non faccio niente
					//start();
				}
				else if (index<brokerInfos.length)
				{
					brokerConnections(index++);
				}
				*/
			},
			on_offline: () => {
				//console.log("Offline");
				/*
				index++;
				if (index==brokerInfos.length)
				{
					//ho finito, non faccio niente
					//start();
				}
				else if (index<brokerInfos.length)
				{
					brokerConnections(index++);
				}
				*/

			}
		});	

		index++;
		if (index==brokerInfos.length)
		{
			broker.registerSub_Pub();
		}
		else if (index<brokerInfos.length)
		{
			brokerConnections(index++);
		}

	}
	catch(e) {
		console.log(e);
	}
	
	

}



function start()
{
    app.listen(config.http.port, '0.0.0.0', function() {
        //console.log(this);
        //console.log(`STARTED ON ${config.http.port}`);
        logger.log("info", `STARTED ON ${config.http.port}`);
    });
    
    endpoints.init(app);

}
	




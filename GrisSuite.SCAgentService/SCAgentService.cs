using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using GrisSuite.Common;
using GrisSuite.Properties;
using GrisSuite.SCAgent;
using Timer = System.Timers.Timer;

namespace GrisSuite.SCAgentService
{
	public partial class SCAgentService : ServiceBase
	{
		private int _errorCounter;
		private readonly int _maxErrors;
		private bool _firstSending = true;
		private readonly int _intervalSendDeviceStatusToCentral = 300000;
		private DateTime _lastStatusCompleteSending = DateTime.MinValue;
		private Timer _timerSendDeviceStatusToCentral;
		private Timer _timerSendEvents;
		private Timer _timerSyncTime;
        private Timer _startupTimer;
		private readonly Agent _agent;
		private int _sendInProgress;
		private int _sendEventsInProgress;
		private int _syncTimeInProgress;
		internal static readonly string LogSource = "GrisSuite.SCAgent";
		internal static readonly string LogFile = "GrisSuite";

		public SCAgentService()
		{
			try
			{
				LocalUtil.WriteDebugLogToFile("SCAgentService Begin Constructor");

				this.InitializeComponent();

                GrisSuite.AppCfg.AppCfg.Default.LogAllCfg();

				this.CanPauseAndContinue = true;
				this.CanShutdown = true;
				this.InitMyEventLog();

				this._maxErrors = Settings.Default.MaxErrors;

				this._agent = new Agent();

				this._intervalSendDeviceStatusToCentral = (Settings.Default.IntervalSeconds*1000);
                this._timerSendDeviceStatusToCentral = new Timer(this._intervalSendDeviceStatusToCentral);
                this._timerSendDeviceStatusToCentral.Elapsed += this.TimerSendDeviceStatusToCentral_Elapsed;
                LocalUtil.WriteDebugLogToFile(string.Format("Timer SendDeviceStatusToCentral seconds: {0}", Settings.Default.IntervalSeconds));

				this._timerSendEvents = new Timer(Settings.Default.IntervalSendEventsSeconds*1000);
				this._timerSendEvents.Elapsed += this.TimerSendEvents_Elapsed;
				LocalUtil.WriteDebugLogToFile(string.Format("Timer SendEvents seconds: {0}", Settings.Default.IntervalSendEventsSeconds));

                if (Settings.Default.IntervalSyncTimeSeconds != 0)
                {
                    this._timerSyncTime = new Timer(Settings.Default.IntervalSyncTimeSeconds * 1000);
                    this._timerSyncTime.Elapsed += this.TimerSyncTime_Elapsed;
                    LocalUtil.WriteDebugLogToFile(string.Format("Timer SyncTime seconds: {0}", Settings.Default.IntervalSyncTimeSeconds));
                }
                else
                    LocalUtil.WriteDebugLogToFile("Timer SyncTime disabled");

				LocalUtil.WriteDebugLogToFile("SCAgentService End Constructor");
			}
			catch (Exception ex)
			{
				this.WriteLogError("Error on SCAgentService Constructor", ex);

				if (this._timerSendDeviceStatusToCentral != null)
				{
					this._timerSendDeviceStatusToCentral.Stop();
				}

				if (this._timerSendEvents != null)
				{
					this._timerSendEvents.Stop();
				}

				if (this._timerSyncTime != null)
				{
					this._timerSyncTime.Stop();
				}
			}
		}

		internal void InitMyEventLog()
		{
			// Registrazione della sorgente di log 
			if (!EventLog.SourceExists(LogSource))
			{
				EventLog.CreateEventSource(LogSource, LogFile);
			}
			this.MyEventLog.Log = LogFile;
			this.MyEventLog.Source = LogSource;
		}

        private void TimerSendDeviceStatusToCentral_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.SendDeviceStatusToCentral();
        }

	    private void SendDeviceStatusToCentral()
	    {
            if (this._agent.GetParkingServerStatus() != ServerParkingData.ParkingKind.PreviouslyParkedServer)
            {
                this.WriteLogInfo("Send Device Status to Central: no action (ParkedSever).", null);
                return;
            }

            this.WriteLogInfo("Send Device Status to Central", null);

	        if (Interlocked.CompareExchange(ref this._sendInProgress, 1, 0) == 0)
	        {
	            try
	            {
	                this._agent.UpdateServerFromEnvironment();

	                if (this._firstSending)
	                {
	                    this.SendStatusComplete();
	                }
	                else
	                {
	                    this.SendStatusUpdate();
	                }
	            }
	            catch (Exception ex)
	            {
	                this._errorCounter++;
	                if (this._errorCounter > 1)
	                {
	                    this.WriteLogError("Consecutive Error #{0}", ex, this._errorCounter);
	                }
	                else
	                {
	                    this.WriteLogError(ex.Message, null);
	                }

	                if (this._errorCounter >= this._maxErrors)
	                {
	                    this._errorCounter = 0;
	                    this._timerSendDeviceStatusToCentral.Interval = this._intervalSendDeviceStatusToCentral;
	                    this.WriteLogError("Consecutive max errors ({0}) reached, reset config and status.", null, this._maxErrors);
	                    this.ResetConfigAndStatus();
	                }

	                this._timerSendDeviceStatusToCentral.Interval = this._intervalSendDeviceStatusToCentral*this._errorCounter;
	            }
	            finally
	            {
	                this._sendInProgress = 0;
	            }
	        }
	    }

	    private void TimerSendEvents_Elapsed(object sender, ElapsedEventArgs e)
	    {
	        this.SendEvents();
	    }

	    private void SendEvents()
	    {
            this.WriteLogInfo("Send Events", null);

	        if (Interlocked.CompareExchange(ref this._sendEventsInProgress, 1, 0) == 0)
	        {
	            try
	            {
	                this._agent.SendEvents();
	            }
	            catch (Exception ex)
	            {
	                this.WriteLogError("Error on SendEvents", ex);
	            }
	            finally
	            {
	                this._sendEventsInProgress = 0;
	            }
	        }
	    }

	    private void TimerSyncTime_Elapsed(object sender, ElapsedEventArgs e)
	    {
	        this.SyncTime();
	    }

	    private void SyncTime()
	    {
            this.WriteLogInfo("Sync Time", null);

	        if (Interlocked.CompareExchange(ref this._syncTimeInProgress, 1, 0) == 0)
	        {
	            try
	            {
	                this._agent.SyncronizeTime();
	            }
	            catch (Exception ex)
	            {
	                this.WriteLogError("Error on SyncronizeTime", ex);
	            }
	            finally
	            {
	                this._syncTimeInProgress = 0;
	            }
	        }
	    }

	    protected override void OnStart(string[] args)
		{
		    if (Settings.Default.StartupDelaySeconds > 0)
            {
                LocalUtil.WriteDebugLogToFile(String.Format("Startup Delay Seconds set to: {0} seconds", Settings.Default.StartupDelaySeconds.ToString()));

                this._startupTimer = new Timer { Interval = (Settings.Default.StartupDelaySeconds * 1000) };
                this._startupTimer.Elapsed += _startupTimer_Elapsed;

                this._startupTimer.Start();
            }
            else
            {
                this.StartMonitoring();
            }
		}

        void _startupTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this._startupTimer != null)
            {
                this._startupTimer.Stop();
                this._startupTimer.Dispose();
            }

            this.StartMonitoring();
        }

	    private void StartMonitoring()
	    {
            this.WriteLogInfo("Monitoring started", null);

	        this._firstSending = true;
	        this._timerSendDeviceStatusToCentral.Start();
	        this._timerSendEvents.Start();
	        this._timerSyncTime.Start();

            // Chiamiamo manualmente i tre metodi usati nei timer, perch� i timer sono appena stati avviati, quindi non eseguiranno nulla fino al primo tick, dopo il tempo di intervallo
            this.SyncTime();
	        this.SendDeviceStatusToCentral();
            this.SendEvents();
        }

	    private void StopMonitoring()
	    {
            this.WriteLogInfo("Monitoring stopped", null);

            this._timerSendDeviceStatusToCentral.Stop();
            this._timerSendEvents.Stop();
            this._timerSyncTime.Stop();
	    }

	    protected override void OnStop()
	    {
            this.WriteLogInfo("OnStop: shutting down...", null);

            this.OnShutdown();
	    }

		protected override void OnContinue()
		{
            this.StartMonitoring();
		}

		protected override void OnPause()
		{
            this.StopMonitoring();
		}

		protected override void OnShutdown()
		{
			this._timerSendDeviceStatusToCentral.Stop();
			this._timerSendDeviceStatusToCentral.Dispose();
			this._timerSendDeviceStatusToCentral = null;

			this._timerSendEvents.Stop();
			this._timerSendEvents.Dispose();
			this._timerSendEvents = null;

			this._timerSyncTime.Stop();
			this._timerSyncTime.Dispose();
			this._timerSyncTime = null;
		}

		private void SendStatusComplete()
		{
            this.WriteLogInfo("Send Status Complete", null);

			try
			{
				this._agent.WriteDebugLog("FirstSending Begin");
				if (this._agent.TrySqlConnect())
				{
                    Guid rc = Guid.Empty;

					if (Settings.Default.SendConfigAndStatusAtServiceStart)
					{
						this.WriteLogInfo("Calling ResetConfigAndStatus()");
						rc = this.ResetConfigAndStatus();
					}

					if (Settings.Default.SendEventsAtServiceStart)
					{
						this.WriteLogInfo("Calling SendEvents()");
						rc = this._agent.SendEvents();
					}

					if (Settings.Default.SendParametersAtServiceStart)
					{
						this.WriteLogInfo("Calling SendSTLCParameters()");
						rc = this._agent.SendSTLCParameters();
					}

				    if ((!Settings.Default.SendEventsAtServiceStart) && (rc != Guid.Empty))
					{
                        // L'invio della segnalazione del server come attivo e con centralizzazione avviene anche durante l'invio Eventi,
                        // quindi evitiamo l'invio doppio se � gi� stato mandato sopra nel SendEvents()
                        // Segnaliamo il server come correttamente funzionante, se inserito nei parcheggiati
                        // La segnalazione avviene solamente se siamo riusciti a centralizzare, quindi con Guid valorizzato
                        this.WriteLogInfo("Calling SignalParkedServer()");
                        this._agent.SignalParkedServer(this._agent.GetParkingServerStatus());
					}

				    this._agent.WriteDebugLog("FirstSending End");
				}
				else
				{
					throw new Exception("Error on TrySqlConnect");
				}
			}
			finally
			{
				this._lastStatusCompleteSending = DateTime.Now; // inizializzato indipendentemente dall'esito dell'operazione
				this._firstSending = false;
				this._errorCounter = 0;
				this._timerSendDeviceStatusToCentral.Interval = this._intervalSendDeviceStatusToCentral;
			}
		}

		private void SendStatusUpdate()
		{
            this.WriteLogInfo("Send Status Update", null);

			Guid rcSendConfig = Guid.Empty;
			Guid rcSendStatus;
			if (Settings.Default.SendConfigChanges)
			{
				rcSendConfig = this._agent.SendConfigToCentral(true);
			}

			this._agent.SendAcks();

            if (Settings.Default.PeriodicallySendAllStatus && (DateTime.Now > this._lastStatusCompleteSending.AddMinutes(Settings.Default.SendAllStatusMinutes)))
                {
				// inizializzato indipendentemente dall'esito dell'operazione (per evitare il reinvio continuo)
				this._lastStatusCompleteSending = DateTime.Now;
				rcSendStatus = this._agent.SendDeviceStatusToCentral(false); // invia tutto
			}
			else
			{
				// invia solo le righe che sono cambiate rispetto alle colonne definite in configurazione
				rcSendStatus = this._agent.SendDeviceStatusToCentral(true);
			}

			if (rcSendConfig == Guid.Empty && rcSendStatus == Guid.Empty && Settings.Default.SendIsAliveIfNoChange)
			{
				this._agent.SendIsAlive();
			}

			this._errorCounter = 0;
			this._timerSendDeviceStatusToCentral.Interval = this._intervalSendDeviceStatusToCentral;
		}

		private Guid ResetConfigAndStatus()
		{
			this._agent.ClearCache();
			this._agent.SendConfigToCentral();
			this._agent.SendDeviceStatusToCentral();
			this._agent.SendAcks();
		    return this._agent.SendIsAlive();
		}

        private void WriteLogInfo(string text, params object[] args)
        {
            string sMessage = text;
            if (args != null)
            {
                sMessage = string.Format(text, args);
            }

            LocalUtil.WriteDebugLogToFile(sMessage); 
        }

		private void WriteLogError(string text, Exception ex, params object[] args)
		{
            string sMessage = text;
            if (args != null)
            {
                sMessage = string.Format(text, args);
            }

            if (ex != null)
            {
                sMessage += "\r\nError: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace;
            }

            LocalUtil.WriteDebugLogToFile(sMessage); 
            EventLog el = this.EventLog;
			if (!string.IsNullOrEmpty(this.MyEventLog.Source))
			{
				el = this.MyEventLog;
			}

            if (ex == null) {
				el.WriteEntry(sMessage, EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, (short) GrisEventCategory.SystemEvents);
            }
			else if (ex is SqlException)
			{
				el.WriteEntry(sMessage, EventLogEntryType.Error, (int) GrisDatabaseEvents.QueryError, (short) GrisEventCategory.DatabaseEvents);
			}
			else
			{
				el.WriteEntry(sMessage, EventLogEntryType.Error, (int) GrisSystemEvents.Error, (short) GrisEventCategory.SystemEvents);
			}
		}

		protected override void OnCustomCommand(int command)
		{
			switch (command)
			{
				case (int) ScagentActionEnum.SendConfigToCentralComplete:
				{
					this._agent.SendConfigToCentral();
					break;
				}
				case (int) ScagentActionEnum.SendConfigToCentralOnlyChanges:
				{
					this._agent.SendConfigToCentral(true);
					break;
				}
				case (int) ScagentActionEnum.SendDeviceStatusToCentralComplete:
				{
					this._agent.SendDeviceStatusToCentral();
					break;
				}
				case (int) ScagentActionEnum.SendDeviceStatusToCentralOnlyChanges:
				{
					this._agent.SendDeviceStatusToCentral(true);
					break;
				}
				case (int) ScagentActionEnum.SendDeviceStatusAndConfigToCentral:
				{
					this._agent.SendConfigToCentral();
					this._agent.SendDeviceStatusToCentral();
					break;
				}
			}
		}
	}
}
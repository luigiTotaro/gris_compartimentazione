using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace GrisSuite.SCAgentService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            //this.serviceProcessInstaller1.Username = ".\\TempServiceUser";
            //this.serviceProcessInstaller1.Password = "password";      

            base.Install(stateSaver);
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            base.Uninstall(savedState);
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }

    }
}
using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using GrisSuite.SCAgent;

namespace GrisSuite.SCAgentService
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            bool test = false;
            bool testEvents = false;
            bool testSyncTime = false;
            bool sendConfigAndStatus = false;
            bool sendParameters = false;
            bool createLogSource = false;
            bool testAcks = false;
            bool testLicense = false;
            bool resetConfigAndStatus = false;
            bool signalParkedServer = false;

            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "/t":
                        test = true;
                        break;
                    case "/te":
                        testEvents = true;
                        break;
                    case "/ta":
                        testAcks = true;
                        break;
                    case "/tt":
                        testSyncTime = true;
                        break;
                    case "/s":
                        sendConfigAndStatus = true;
                        break;
                    case "/sp":
                        sendParameters = true;
                        break;
                    case "/cl":
                        createLogSource = true;
                        break;
                    case "/tl":
                        testLicense = true;
                        break;
                    case "/reset":
                        resetConfigAndStatus = true;
                        break;
                    case "/sps":
                        signalParkedServer = true;
                        break;
                }
            }

            if (test)
            {
                string step = string.Empty;
                const string mgsInfo = "{0} TEST: {1}, GUID: {2}";
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    a.UpdateServerFromEnvironment();

                    a.SendIsAlive();

                    step = "SendConfigToCentral Complete";
                    Guid returnGuid = a.SendConfigToCentral();
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SendConfigToCentral Only Change";
                    returnGuid = a.SendConfigToCentral(true);
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SendDeviceStatusToCentral Complete";
                    returnGuid = a.SendDeviceStatusToCentral();
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SendDeviceStatusToCentral Only Change 1/2";
                    returnGuid = a.SendDeviceStatusToCentral(true);
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SendDeviceStatusToCentral Only Change 2/2";
                    returnGuid = a.SendDeviceStatusToCentral(true);
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SendEvents";
                    returnGuid = a.SendEvents();
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SyncronizeTime";
                    bool syncTime = a.SyncronizeTime();
                    Console.WriteLine(syncTime ? string.Format(mgsInfo, "OK", step, Guid.Empty) : string.Format(mgsInfo, "Failed", step, Guid.Empty));

                    step = "SendSTLCParameters";
                    returnGuid = a.SendSTLCParameters();
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    step = "SendAcks";
                    returnGuid = a.SendAcks();
                    Console.WriteLine(mgsInfo, "OK", step, returnGuid);

                    a.SendIsAlive();

                    Console.WriteLine(syncTime ? "TEST SUPERATO" : "TEST FALLITO");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TEST FALLITO, STEP:{0} MESSAGE:{1}", step, ex.Message);
                }
            }
            else if (testEvents)
            {
                string step = string.Empty;
                const string mgsInfo = "TEST EVENTS {0} OK, GUID: {1}";
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    step = "SendEvents";
                    Guid returnGuid = a.SendEvents();
                    Console.WriteLine(mgsInfo, step, returnGuid);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TEST EVENTS {0} ERROR:{1}", step, ex.Message);
                }
            }
            else if (testAcks)
            {
                string step = string.Empty;
                const string mgsInfo = "TEST ACKS {0} OK, GUID: {1}";
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    step = "SendAcks";
                    Guid returnGuid = a.SendAcks();
                    Console.WriteLine(mgsInfo, step, returnGuid);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TEST ACKS {0} ERROR:{1}", step, ex.Message);
                }
            }
            else if (testSyncTime)
            {
                string step = string.Empty;
                const string mgsInfo = "TEST SyncronizeTime {0}";
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    step = "SyncronizeTime";
                    Console.WriteLine(a.SyncronizeTime() ? string.Format(mgsInfo, "OK") : string.Format(mgsInfo, "Failed"));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TEST SyncronizeTime STEP:{0} ERROR:{1}", step, ex.Message);
                }
            }
            else if (sendConfigAndStatus)
            {
                string step = string.Empty;
                Guid returnGuid = Guid.Empty;
                const string mgsInfo = "SendConfigAndStatus {0} OK, GUID: {1}";
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    step = "SendDeviceStatusToCentral ClearCache";
                    a.ClearCache();
                    Console.WriteLine(mgsInfo, step, returnGuid);

                    step = "SendConfigToCentral";
                    returnGuid = a.SendConfigToCentral();
                    Console.WriteLine(mgsInfo, step, returnGuid);

                    step = "SendDeviceStatusToCentral Complete";
                    returnGuid = a.SendDeviceStatusToCentral(false);
                    Console.WriteLine(mgsInfo, step, returnGuid);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SendConfigAndStatus {0} ERROR:{1}", step, ex.Message);
                }
            }
            else if (sendParameters)
            {
                string step = string.Empty;
                const string mgsInfo = "Send STLC Parameters {0} OK, GUID: {1}";
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    step = "SendSTLCParameters";
                    Guid returnGuid = a.SendSTLCParameters();
                    Console.WriteLine(mgsInfo, step, returnGuid);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Send STLC Parameters {0} ERROR:{1}", step, ex.Message);
                }
            }
            else if (createLogSource)
            {
                CreateLogSource();
            }
            else if (testLicense)
            {
                string step = string.Empty;
                try
                {
                    step = "Create SCAgent";
                    Agent a = new Agent();

                    step = "CheckLicense";
                    byte returnValue = a.CheckLicense();
                    string returnString = "N/A";

                    switch (returnValue)
                    {
                        case 0x00:
                            returnString = "Not licensed";
                            break;
                        case 0x01:
                            returnString = "Licensed";
                            break;
                        case 0xFF:
                            returnString = "No license required";
                            break;
                    }

                    Console.WriteLine("Check License {0} OK, Value: {1} ({2})", step, returnValue, returnString);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Check License {0} ERROR:{1}", step, ex.Message);
                }
            }
            else if (resetConfigAndStatus)
            {
                try
                {
                    Agent a = new Agent();

                    a.ClearCache();
                    a.SendConfigToCentral();
                    a.SendDeviceStatusToCentral();
                    a.SendAcks();
                    Guid returnGuid = a.SendIsAlive();

                    Console.WriteLine("Reset Config and Status OK, GUID: {0}", returnGuid.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Reset Config and Status ERROR:{0}", ex.Message);
                }
            }
            else if (signalParkedServer)
            {
                try
                {
                    Agent a = new Agent();

                    Guid returnGuid = a.SignalParkedServer();

                    Console.WriteLine("Signal Parked Server OK, GUID: {0}", returnGuid.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Signal Parked Server ERROR:{0}", ex.Message);
                }
            }
            else if (Environment.UserInteractive)
            {
                StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
                standardOutput.AutoFlush = true;
                Console.SetOut(standardOutput);

                Console.WriteLine(AppDomain.CurrentDomain.FriendlyName + " se lanciato a linea di comando accetta le seguenti opzioni:");
                Console.WriteLine("/t  = Test di tutti i metodi del servizio");
                Console.WriteLine("/tt = Sincronizzazione data e ora");
                Console.WriteLine("/te = Invio tabella Events");
                Console.WriteLine("/ta = Invio tabella Device_ack");
                Console.WriteLine("/s  = Invio configurazione e stato");
                Console.WriteLine("/sp = Invio della tabella stlc_parameters");
                Console.WriteLine("/tl = Testa stato licenza per il server");
                Console.WriteLine("/reset = Resetta configurazione e status sul server (come avvio servizio)");
                Console.WriteLine("/sps = Segnala il server come parcheggiato (in base alla configurazione)");
                Console.WriteLine("/cl  = Crea la log source {0} nel log {1}", SCAgentService.LogSource, SCAgentService.LogFile);
            }
            else
            {
                ServiceBase[] ServicesToRun = new ServiceBase[] { new SCAgentService() };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static void CreateLogSource()
        {
            try
            {
                // Registrazione della sorgente di log 
                if (!EventLog.SourceExists(SCAgentService.LogSource))
                {
                    EventLog.CreateEventSource(SCAgentService.LogSource, SCAgentService.LogFile);
                    Console.WriteLine("OK LogSource {0} in log {1} creato.", SCAgentService.LogSource, SCAgentService.LogFile);
                }
                else
                {
                    Console.WriteLine("LogSource {0} gi� esistente.", SCAgentService.LogSource);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore nella creazione della LogSource, Error:{0}", ex.Message);
            }
        }
    }
}
@echo off
@if exist GRIS_PRE rd /s /q GRIS_PRE
@robocopy GRIS\ GRIS_PRE\ /MIR /NFL /R:1 /W:1


@copy ..\GrisSuite.GRIS\Reports\*.rdlc GRIS_PRE\Reports


@if exist FDS rd /s /q FDS
@if not exist FDS\FDS.B2O md FDS\FDS.B2O
@if not exist FDS\FDSUI md FDS\FDSUI
@if not exist FDS\FDSUI\WorkingXap md FDS\FDSUI\WorkingXap

@copy "..\GrisSuite.FDSUI\Bin\Release\*" FDS\FDSUI
@cd FDS\FDSUI
@"c:\Program Files\7-Zip\7z.exe" x -oWorkingXap GrisSuite.FDS.UI.xap
@cd WorkingXap
@del "ServiceReferences.ClientConfig"
@del "ServiceReferences.PRO.ClientConfig"
@ren "ServiceReferences.PreProd.ClientConfig" "ServiceReferences.ClientConfig"
@"c:\Program Files\7-Zip\7z.exe" a -r -tzip GrisSuite.FDS.UI.xap
@cd ..
@move /y "WorkingXap\GrisSuite.FDS.UI.xap" .
@cd ..
@cd ..

@copy "..\GrisSuite.FDS.B2O\bin\Release\*" "FDS\FDS.B2O"
@copy /y "FDS\FDSUI\*.xap" "GRIS_PRE\ClientBin"

@rd /s /q FDS


@if exist WebMap rd /s /q WebMap
@if not exist WebMap md WebMap
@if not exist WebMap\WorkingXap md WebMap\WorkingXap

@copy "..\GrisSuite.Web.Map\Bin\Release\*" WebMap
@cd WebMap
@"c:\Program Files\7-Zip\7z.exe" x -oWorkingXap GrisSuite.Web.Map.xap
@cd WorkingXap
@del "ServiceReferences.ClientConfig"
@del "ServiceReferences.PRO.ClientConfig"
@ren "ServiceReferences.PreProd.ClientConfig" "ServiceReferences.ClientConfig"
@"c:\Program Files\7-Zip\7z.exe" a -r -tzip GrisSuite.Web.Map.xap
@cd ..
@move /y "WorkingXap\GrisSuite.Web.Map.xap" .
@cd ..

@copy /y "WebMap\GrisSuite.Web.Map.xap" "GRIS_PRE\ClientBin"

@rd /s /q WebMap


@del /q GRIS_PRE\web*.config
@del /q GRIS_PRE\GrisBuild.proj

pause
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GrisSuite.Alerts
{
    public class DevicesFilters 
    {
        [XmlElement(ElementName = "deviceType")]
        public List<DeviceType> DeviceTypes { get; set; }
    }

    public class DeviceType
    {
          [XmlAttribute(AttributeName="code")]
          public string Code { get; set; }
          [XmlElement(ElementName = "stream")]
          public List<Stream> Streams { get; set; }
      }

    public class Stream
    {
        private List<Field> _fields;
        private int[] _fieldIds;

        [XmlAttribute(AttributeName="id")]
        public int Id { get; set; }
        [XmlAttribute(AttributeName="name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "field")]
        public List<Field> Fields 
        {
            get { return _fields; }
            set 
            { 
                _fields = value;
                _fieldIds = new int[_fields.Count];
                
                for (int i = 0; i<_fields.Count;i++)
                {
                    _fieldIds[i] = _fields[i].Id;
                }
            }
        }

        public Field FindField(int id)
        {
            if (_fieldIds.Length != _fields.Count)
                this.Fields = _fields;

            int i = Array.IndexOf(_fieldIds, id);
            if (i >= 0)
                return _fields[i];
            else
            {
                return null;
            }
        }
    }

    public class Field
    {
        [XmlAttribute(AttributeName="id")]
        public int Id { get; set; }
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }
		[XmlAttribute(AttributeName = "storeDescription")]
		public bool StoreDescription { get; set; }
	}

}

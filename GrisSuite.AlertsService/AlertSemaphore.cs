﻿using System;
using GrisSuite.Data;
using GrisSuite.Data.dsSemaphoresTableAdapters;

namespace GrisSuite.Alerts
{
    internal enum SemaphoreState
    {
        Unknown,
        Done,
        Error
    }

    internal class SemaphoreStatus
    {
        public SemaphoreState Status { get; private set; }
        public Exception InnerException { get; private set; }

        public SemaphoreStatus(SemaphoreState status, Exception exception)
        {
            this.Status = status;
            this.InnerException = exception;
        }
    }

    internal class AlertSemaphore
    {
        private const string SEMAPHORE_NAME = "AlertService";

        internal static SemaphoreStatus Lock()
        {
            try
            {
                QueriesSemaphores q = new QueriesSemaphores();
                if (q.SemaphoreLock(SEMAPHORE_NAME, Environment.MachineName) > 0)
                {
                    return new SemaphoreStatus(SemaphoreState.Done, null);
                }

                return new SemaphoreStatus(SemaphoreState.Error, null);
            }
            catch (Exception ex)
            {
                return new SemaphoreStatus(SemaphoreState.Unknown, ex);
            }
        }

        internal static SemaphoreStatus Unlock()
        {
            try
            {
                QueriesSemaphores q = new QueriesSemaphores();
                if (q.SemaphoreUnlock(SEMAPHORE_NAME, Environment.MachineName) > 0)
                {
                    return new SemaphoreStatus(SemaphoreState.Done, null);
                }

                return new SemaphoreStatus(SemaphoreState.Error, null);
            }
            catch (Exception ex)
            {
                return new SemaphoreStatus(SemaphoreState.Unknown, ex);
            }
        }

        internal static AlertLockInfo GetLockInfo()
        {
            try
            {
                SemaphoresTableAdapter ta = new SemaphoresTableAdapter();
                if (ta.GetData(SEMAPHORE_NAME).Rows.Count > 0)
                {
                    return new AlertLockInfo((dsSemaphores.SemaphoresRow) ta.GetData(SEMAPHORE_NAME).Rows[0], null);
                }

                return new AlertLockInfo(null, null);
            }
            catch (Exception ex)
            {
                return new AlertLockInfo(null, ex);
            }
        }

        internal static bool? IsMostRecentCacheLogMessageOlderThan(int timespanMinutes)
        {
            try
            {
                QueriesSemaphores q = new QueriesSemaphores();
                if ((byte)q.IsMostRecentCacheLogMessageOlderThan(timespanMinutes) != 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    internal class AlertLockInfo
    {
        internal bool IsValid { get; private set; }
        internal bool IsMine { get; private set; }
        internal DateTime LastUpdate { get; private set; }
        internal Exception InnerException { get; private set; }

        internal AlertLockInfo(dsSemaphores.SemaphoresRow smRow, Exception exception)
        {
            if (smRow != null)
            {
                this.IsValid = true;
                this.IsMine = (Environment.MachineName.Equals(smRow.HostName, StringComparison.OrdinalIgnoreCase));
                this.LastUpdate = smRow.LastUpdate;
                this.InnerException = exception;
            }
            else
            {
                this.IsValid = false;
                this.IsMine = false;
                this.LastUpdate = DateTime.MaxValue;
                this.InnerException = exception;
            }
        }
    }
}
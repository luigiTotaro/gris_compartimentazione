﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using GrisSuite.Alerts.Properties;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.dsCacheLogTableAdapters;
using GrisSuite.Data.dsLogTableAdapters;

namespace GrisSuite.Alerts
{
    internal class CacheDatasetHelper
    {
        private readonly EventLog _eventLog;

        public CacheDatasetHelper(EventLog eventLog)
        {
            this._eventLog = eventLog;
        }

        private dsCacheLog CreateCacheDeviceStatus(Guid logID, string hostSender, DateTime created, string messageType, string mac, string hostReceiver, dsDeviceStatus dsStatus, DevicesFilters filters)
        {
            QueriesCacheLog qta = new QueriesCacheLog();
            dsCacheLog dsCache = new dsCacheLog();

            dsCacheLog.LogMessagesRow cacheLogMessageRow = dsCache.LogMessages.NewLogMessagesRow();
            cacheLogMessageRow.LogID = logID;
            cacheLogMessageRow.HostSender = hostSender;
            cacheLogMessageRow.Created = created;
            cacheLogMessageRow.MessageType = messageType;
            cacheLogMessageRow.MAC = mac;
            cacheLogMessageRow.HostReceiver = hostReceiver;
            dsCache.LogMessages.AddLogMessagesRow(cacheLogMessageRow);

            foreach (dsDeviceStatus.DeviceStatusRow statusRow in dsStatus.DeviceStatus)
            {
                dsCacheLog.DeviceStatusRow cacheStatusRow = dsCache.DeviceStatus.NewDeviceStatusRow();
                cacheStatusRow.DevID = statusRow.DevID;
                cacheStatusRow.LogID = logID;
                cacheStatusRow.SevLevel = statusRow.SevLevel;
                cacheStatusRow.Description = statusRow.Description;
                cacheStatusRow.LogID = logID;
                cacheStatusRow.Offline = statusRow.Offline;
                cacheStatusRow.DevType = qta.GetDeviceType(cacheStatusRow.DevID, created);
                dsCache.DeviceStatus.AddDeviceStatusRow(cacheStatusRow);
            }

            DeviceType devTypeFilter = null;
            long lastDevId = 0;
            string devType = string.Empty;
            foreach (dsDeviceStatus.StreamsRow streamRow in dsStatus.Streams)
            {
                if (lastDevId != streamRow.DevID)
                {
                    devType = qta.GetDeviceType(streamRow.DevID, created) ?? string.Empty;
                    devTypeFilter = filters.DeviceTypes.Find(x => x.Code.Equals(devType));
                }

                lastDevId = streamRow.DevID;

                if (devTypeFilter != null && devTypeFilter.Streams.Find(x => x.Id.Equals(streamRow.StrID)) != null)
                {
                    dsCacheLog.StreamsRow cacheStreamRow = dsCache.Streams.NewStreamsRow();
                    cacheStreamRow.DevID = streamRow.DevID;
                    cacheStreamRow.StrID = streamRow.StrID;
                    cacheStreamRow.LogID = logID;
                    cacheStreamRow.Name = streamRow.Name;
                    cacheStreamRow.Visible = streamRow.Visible;
                    cacheStreamRow.DateTime = streamRow.DateTime;
                    cacheStreamRow.SevLevel = streamRow.SevLevel;
                    cacheStreamRow.Processed = 1;
                    cacheStreamRow.DevType = devType;
                    dsCache.Streams.AddStreamsRow(cacheStreamRow);
                }
            }

            int lastStrId = 0;
            Stream strFilter = null;
            lastDevId = 0;
            devType = string.Empty;
            foreach (dsDeviceStatus.StreamFieldsRow streamFieldRow in dsStatus.StreamFields)
            {
                Field fldFilter = null;

                if (lastDevId != streamFieldRow.DevID)
                {
                    devType = qta.GetDeviceType(streamFieldRow.DevID, created) ?? string.Empty;
                    devTypeFilter = filters.DeviceTypes.Find(x => x.Code.Equals(devType));
                    strFilter = null;
                }

                if ((lastDevId != streamFieldRow.DevID || lastStrId != streamFieldRow.StrID) && devTypeFilter != null)
                {
                    strFilter = devTypeFilter.Streams.Find(x => x.Id.Equals(streamFieldRow.StrID));
                }

                lastDevId = streamFieldRow.DevID;
                lastStrId = streamFieldRow.StrID;

                if (strFilter != null)
                {
                    fldFilter = strFilter.FindField(streamFieldRow.FieldID); // strFilter.Fields.Find(x => x.Id.Equals(streamFieldRow.FieldID));
                }

                if (fldFilter != null)
                {
                    if (streamFieldRow.ReferenceRow != null)
                    {
                        dsCacheLog.ReferenceRow cacheReferenceRow = dsCache.Reference.NewReferenceRow();
                        cacheReferenceRow.ReferenceID = streamFieldRow.ReferenceID;
                        cacheReferenceRow.Value = streamFieldRow.ReferenceRow.Value;
                        cacheReferenceRow.DateTime = streamFieldRow.ReferenceRow.DateTime;
                        cacheReferenceRow.Visible = streamFieldRow.ReferenceRow.Visible;
                        cacheReferenceRow.LogID = logID;
                        cacheReferenceRow.DeltaDevID = streamFieldRow.ReferenceRow.DeltaDevID;
                        cacheReferenceRow.DeltaStrID = streamFieldRow.ReferenceRow.DeltaStrID;
                        cacheReferenceRow.DeltaFieldID = streamFieldRow.ReferenceRow.DeltaFieldID;
                        cacheReferenceRow.DeltaArrayID = streamFieldRow.ReferenceRow.DeltaArrayID;
                        cacheReferenceRow.DevType = devType;
                        dsCache.Reference.AddReferenceRow(cacheReferenceRow);
                    }

                    dsCacheLog.StreamFieldsRow cacheStreamFieldRow = dsCache.StreamFields.NewStreamFieldsRow();
                    cacheStreamFieldRow.DevID = streamFieldRow.DevID;
                    cacheStreamFieldRow.StrID = streamFieldRow.StrID;
                    cacheStreamFieldRow.FieldID = streamFieldRow.FieldID;
                    cacheStreamFieldRow.ArrayID = streamFieldRow.ArrayID;
                    cacheStreamFieldRow.LogID = logID;
                    cacheStreamFieldRow.Name = streamFieldRow.Name;
                    cacheStreamFieldRow.SevLevel = streamFieldRow.SevLevel;
                    cacheStreamFieldRow.Value = streamFieldRow.Value;
                    cacheStreamFieldRow.Visible = streamFieldRow.Visible;
                    cacheStreamFieldRow.DevType = devType;
                    if (!streamFieldRow.IsReferenceIDNull())
                    {
                        cacheStreamFieldRow.ReferenceID = streamFieldRow.ReferenceID;
                    }

                    if (fldFilter.StoreDescription)
                    {
                        cacheStreamFieldRow.Description = streamFieldRow.Description;
                    }

                    dsCache.StreamFields.AddStreamFieldsRow(cacheStreamFieldRow);
                }
            }

            return dsCache;
        }

        private dsCacheLog CreateCacheConfig(Guid logID, string hostSender, DateTime created, string messageType, dsConfig dsConfig)
        {
            dsCacheLog dsCache = new dsCacheLog();

            dsCacheLog.LogMessagesRow cacheLogMessageRow = dsCache.LogMessages.NewLogMessagesRow();
            cacheLogMessageRow.LogID = logID;
            cacheLogMessageRow.HostSender = hostSender;
            cacheLogMessageRow.Created = created;
            cacheLogMessageRow.MessageType = messageType;
            dsCache.LogMessages.AddLogMessagesRow(cacheLogMessageRow);

            foreach (dsConfig.DevicesRow devRow in dsConfig.Devices)
            {
                dsCacheLog.DevicesRow cacheDeviceRow = dsCache.Devices.NewDevicesRow();
                cacheDeviceRow.DevID = devRow.DevID;
                cacheDeviceRow.LogID = logID;
                cacheDeviceRow.Type = devRow.Type;
                dsCache.Devices.AddDevicesRow(cacheDeviceRow);
            }

            return dsCache;
        }

        public void ImportLogMessage(ref int importedRowCount, ref int importErrorsCount, DevicesFilters filters, int commandTimeout, ref DateTime? processedFrom, ref string lastErrorMessage)
        {
            LogMessageTableAdapter lmtAdapter = new LogMessageTableAdapter {CommandTimeout = commandTimeout};

            string step = "Get Data Not Analyzed (SP: tf_GetLogsForCache)";
            DebugLog.WriteStart(step);
            dsLog.LogMessageDataTable logMessageTable = lmtAdapter.GetDataNotAnalyzed();
            DebugLog.WriteEnd(step + " Rows loaded: {0}:", logMessageTable.Rows.Count);

            importedRowCount = 0;
            importErrorsCount = 0;

            Stopwatch timer = new Stopwatch();
            timer.Start();

            if (logMessageTable.Rows.Count > 0)
            {
                processedFrom = ((dsLog.LogMessageRow) logMessageTable.Rows[0]).Created;
            }
            else
            {
                processedFrom = null;
            }

            foreach (dsLog.LogMessageRow logRow in logMessageTable)
            {
                dsDeviceStatus dsStatus = new dsDeviceStatus();
                dsConfig dsConfig = new dsConfig();
                dsCacheLogDataSetAdaper dsa = new dsCacheLogDataSetAdaper();

                try
                {
                    dsCacheLog dsCache = null;

                    if (Settings.Default.UnpackAndFillCacheFromLogMsg)
                    {
                        if (logRow.MessageType.EndsWith("dsDeviceStatus", true, CultureInfo.CurrentCulture))
                        {
                            Util.FillDataSetFromCompressed(dsStatus, logRow.Message);
                            dsCache = this.CreateCacheDeviceStatus(logRow.LogID, logRow.HostSender, logRow.Created, logRow.MessageType, (logRow.IsMACNull() ? string.Empty : logRow.MAC), (logRow.IsHostReceiverNull() ? string.Empty : logRow.HostReceiver), dsStatus, filters);
                        }
                        else if (logRow.MessageType.EndsWith("dsConfig", true, CultureInfo.CurrentCulture))
                        {
                            Util.FillDataSetFromCompressed(dsConfig, logRow.Message);
                            dsCache = this.CreateCacheConfig(logRow.LogID, logRow.HostSender, logRow.Created, logRow.MessageType, dsConfig);
                        }
                    }

                    using (var cn = dsa.Connection)
                    {
                        cn.Open();
                        dsa.Transaction = cn.BeginTransaction(Settings.Default.CacheWriteIsolationLevel);

                        try
                        {
                            if (dsCache != null)
                            {
                                dsa.Update(dsCache);
                            }

                            lmtAdapter.UpdateLogMessageAnalyzed((byte) AnalyzedEnum.Analyzed, logRow.LogID);
                            dsa.Transaction.Commit();
                        }
                        catch
                        {
                            if (dsa.Transaction != null)
                            {
                                dsa.Transaction.Rollback();
                            }
                            throw;
                        }
                        finally
                        {
                            if (cn.State != ConnectionState.Closed)
                            {
                                cn.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    lastErrorMessage = this.HandleImportException(lmtAdapter, logRow, ex);
                    string stepException = string.Format("LogMessageRow: {0}", logRow.LogID);
                    this.WriteLog("Step: {0} Error: {1}, ImportException: {2}", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.Error, EventLogEntryType.Error, stepException, ex.Message, lastErrorMessage);
                    importErrorsCount++;
                    continue;
                }

                importedRowCount++;

                if (importedRowCount%100 == 0)
                {
                    DebugLog.Write("Parsed {0} cache log rows of {1} at {2:dd/MM/yyyy HH.mm.ss}", importedRowCount, logMessageTable.Rows.Count, DateTime.Now);
                }
            }

            timer.Stop();

            if (importedRowCount > 0)
            {
                DebugLog.Write("Import cache log messages complete. Rows parsed: {0} in {1:0.00} seconds. Milliseconds by row: {2}", importedRowCount, timer.Elapsed.TotalSeconds, timer.ElapsedMilliseconds / importedRowCount);
            }

            QueriesTableAdapter qm = new QueriesTableAdapter { CommandTimeout = commandTimeout };
            step = "Move to Log History (SP: tf_UpdMoveToLogHistory)";
            DebugLog.WriteStart(step);
            qm.MoveToLogHistory(processedFrom);
            DebugLog.WriteEnd(step);
        }

        private string HandleImportException(LogMessageTableAdapter ad, dsLog.LogMessageRow row, Exception ex)
        {
            try
            {
                ad.UpdateLogMessageAnalyzed((byte) AnalyzedEnum.Wrong, row.LogID);
                return ex.Message;
            }
            catch (SqlException sqlEx)
            {
                // prova ad aggiornare ma se ha problemi su sql non da errore la riga resta comunque in stato da analizare
                return sqlEx.Message;
            }
        }

        private void WriteLog(string text, GrisEventCategory category, int eventId, EventLogEntryType entryType, params object[] args)
        {
            var message = args == null ? text : string.Format(text, args);
            this._eventLog.WriteEntry(message, entryType, eventId, (short)category);
            DebugLog.Write(message, entryType);
        }
    }
}
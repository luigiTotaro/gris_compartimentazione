﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using GrisSuite.Alerts.Properties;

namespace GrisSuite.Alerts
{
    public class DebugLog
    {

        private static readonly Stack<Stopwatch> Watchs = new Stack<Stopwatch>();

        public static void Write(string text, EventLogEntryType entryType, params object[] args)
        {
            if (!Settings.Default.EnableDebugLog) return;
                
            var message = (args == null ? text : string.Format(text, args));
            var filename = GetFileName();
            WriteLine(filename, String.Format("{0}: {1}{2}<br/>", DateTime.Now, TypePrefix(entryType), HtmlNewLine(message)));
                
        }

        private static string TypePrefix(EventLogEntryType entryType)
        {
            var prefix = "";
            switch (entryType)
            {
                case EventLogEntryType.Error:
                    Watchs.Clear(); // in caso di errore annullo tutti i timer
                    prefix = @"<b><font color=""red"">ERROR:</font></b>";
                    break;
                case EventLogEntryType.Warning:
                    prefix = @"<b><font color=""orange"">WARNING:</font></b>";
                    break;
            }
            return prefix;
        }

        private static void WriteLine(String filename, String text) {
            try
            {
                using (TextWriter wr = new StreamWriter(filename, true))
                {
                    wr.WriteLine(text);
                    wr.Close();
                }

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(AlertsService.LogSource, "Error on write debug log:" + ex.Message + Environment.NewLine + ex.StackTrace,
                    EventLogEntryType.Warning);
            }

        }


        public static void Write(string text, params object[] args)
        {
            Write(text, EventLogEntryType.Information, args);
        }

        public static void WriteStart(string text, params object[] args)
        {
            Watchs.Push(Stopwatch.StartNew());
            var suffix = string.Format(" starting on {0:dd/MM/yyyy HH.mm.ss}",DateTime.Now);
            Write(text + suffix, args);

        }

        public static void WriteEnd(string text, params object[] args)
        {
            var suffix = string.Format(" ended at {0:dd/MM/yyyy HH.mm.ss} ", DateTime.Now);
            if (Watchs.Count > 0) {
                var w = Watchs.Pop();
                w.Stop();
                var t = w.Elapsed;
                suffix += string.Format(" duration: {0}:{1}:{2}.{3:000}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
            }
            Write(text + suffix, args);    
        }

        public static string HtmlNewLine(string input)
        {
            return !String.IsNullOrEmpty(input) ? HttpUtility.HtmlEncode(input).Replace("\n", "<br/>") : string.Empty;
        }

        private static string GetFileName()
        {
            string sfile;

            if (string.IsNullOrEmpty(Settings.Default.DebugLogFolder)) {
                sfile = AppDomain.CurrentDomain.BaseDirectory + "\\" + AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".log.htm");
            }
            else {
                sfile = Path.Combine(Settings.Default.DebugLogFolder, AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".log.htm"));
            }

            if (!File.Exists(sfile)) {
                WriteLogHeader(sfile);
                return sfile;
            }

            var f = new FileInfo(sfile);
            var debugLogMaxSize = Settings.Default.DebugLogMaxSize;

            if (f.Length <= debugLogMaxSize || debugLogMaxSize <= 0) return sfile;

            var sfileOld = sfile.Replace(".log.htm", "_old.log.htm");

            if (File.Exists(sfileOld)) {
                File.Delete(sfileOld);
            }

            f.MoveTo(sfileOld);

            WriteLogHeader(sfile);
            return sfile;
        }

        private static void WriteLogHeader(String filename)
        {
            WriteLine(filename, @"<html><style>html {color: #222;font-size: 1em;line-height: 1.4; font-family:verdana,arial;}</style><body><h1><font color=""green"">GrisSuite Allerts Service Log</font></h1>");
        }
 
    }
}
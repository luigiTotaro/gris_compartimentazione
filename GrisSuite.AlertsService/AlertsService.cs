﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using GrisSuite.Alerts.Properties;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.dsRulesTableAdapters;
using GrisSuite.Data.dsCacheLogTableAdapters;

namespace GrisSuite.Alerts
{
    internal sealed partial class AlertsService : ServiceBase
    {
        private Timer _timer;
        private readonly int _numIntervalsLockedBeforeUnlock;
        internal static readonly string LogSource = "GrisSuite.Alerts";
        internal static readonly string LogFile = "GrisSuite";
        private const int TOO_LONG_CACHE_MESSAGES_TIMESPAN_MINUTES = 15;

        public AlertsService()
        {
            try
            {
                this.InitializeComponent();
                this.CanPauseAndContinue = true;
                this.CanShutdown = true;
                // Registrazione della sorgente di log 
                if (!EventLog.SourceExists(LogSource))
                {
                    EventLog.CreateEventSource(LogSource, LogFile);
                }

                this.MyEventLog.Source = LogSource;
                this.MyEventLog.Log = LogFile;

                this._numIntervalsLockedBeforeUnlock = Settings.Default.NumIntervalsLockedBeforeUnlock;
                this._timer = new Timer(Settings.Default.IntervalSeconds*1000);
                this._timer.Elapsed += this.Timer_Elapsed;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(string.Format("Error on constructor ({0})", ex.Message), EventLogEntryType.Error, (int) GrisSystemEvents.ErrorOnCostructor, (short) GrisEventCategory.SystemEvents);
                throw;
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int importErrorsCount = 0;
            int importCount = 0;
            string lastErrorMessage = string.Empty;

            string step = "VerifyLock";
            SemaphoreStatus lockSemaphoreStatus = AlertSemaphore.Lock();
            if (lockSemaphoreStatus.Status == SemaphoreState.Done)
            {
                try
                {
                    DateTime? processedFrom = null;

                    QueriesCacheLog qry = new QueriesCacheLog {CommandTimeout = Settings.Default.CommandTimeoutSeconds};

                    CacheDatasetHelper cacheDatasetHelper = new CacheDatasetHelper(this.MyEventLog);

                    step = string.Format("Pre process logs cache, remove rows from cache_log_messages older than {0} days (SP: tf_UpdCachePreProcess)", Settings.Default.CacheDays);
                    DebugLog.WriteStart(step);
                    qry.CachePreProcess(DateTime.Now, Settings.Default.CacheDays);
                    DebugLog.WriteEnd(step);

                    step = "Populate logs cache";
                    DebugLog.WriteStart(step);
                    cacheDatasetHelper.ImportLogMessage(ref importCount, ref importErrorsCount, Settings.Default.DevicesFilters, Settings.Default.CommandTimeoutSeconds, ref processedFrom, ref lastErrorMessage);
                    DebugLog.WriteEnd(step);

                    step = "Post process logs cache (SP: tf_UpdCachePostProcess)";
                    DebugLog.WriteStart(step);
                    qry.CachePostProcess(processedFrom);
                    DebugLog.WriteEnd(step);

                    QueriesTableAdapter qta = new QueriesTableAdapter {CommandTimeout = Settings.Default.CommandTimeoutSeconds};

                    step = "Clean Alerts Devices (SP: gris_UpdCleanAlertsDevices)";
                    DebugLog.WriteStart(step);
                    qta.CleanAlertsDevices();
                    DebugLog.WriteEnd(step);

                    step = "Execute Rules";
                    DebugLog.WriteStart(step);
                    // In questa chiamata, non ci sono timeout per i comandi passati, quindi prende i default dal Dataset
                    dsRules.ExecuteRules(Settings.Default.CommandTimeoutSeconds);
                    DebugLog.WriteEnd(step);

                    step = "Refresh Alerts (SP: gris_UpdRefreshAlerts)";
                    DebugLog.WriteStart(step);
                    qta.RefreshAlerts();
                    DebugLog.WriteEnd(step);

                    DebugLog.Write("Alerts refreshed successfully.");
                    
                }
                catch (SqlException sqlEx)
                {
                    this.WriteLog("Step: {0} Error: {1}", GrisEventCategory.DatabaseEvents, (int) GrisDatabaseEvents.QueryError, EventLogEntryType.Error, step, sqlEx.Message);
                }
                catch (Exception ex)
                {
                    this.WriteLog("Step: {0} Error: {1}", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.Error, EventLogEntryType.Error, step, ex.Message);
                }
                finally
                {
                    this.SemaphoreUnlock();
                }
            }
            else if (lockSemaphoreStatus.Status == SemaphoreState.Error)
            {
                // Se siamo qui, non siamo riusciti ad acquisire il lock, cioè c'è una riga che indica il lock (nostro o di un altro server)
                AlertLockInfo lockInfo = AlertSemaphore.GetLockInfo();

                // IsValid indica se c'è almeno una riga nella tabella semaphores
                if (lockInfo.IsValid)
                {
                    if (lockInfo.IsMine)
                    {
                        // La riga di lock è impostata dal server corrente
                        if (DateTime.Now.Subtract(lockInfo.LastUpdate).TotalMilliseconds > (this._timer.Interval * this._numIntervalsLockedBeforeUnlock))
                        {
                            // Verifichiamo se ci sono righe scodate recenti o meno. Se non ci sono, nessuno sta scodando, ne' questa istanza, ne' altre, quindi occorre rilasciare il lock
                            bool? isMostRecentCacheLogMessageOlderThan = AlertSemaphore.IsMostRecentCacheLogMessageOlderThan(TOO_LONG_CACHE_MESSAGES_TIMESPAN_MINUTES);

                            if (isMostRecentCacheLogMessageOlderThan.HasValue)
                            {
                                if (isMostRecentCacheLogMessageOlderThan.Value)
                                {
                                    // Il server corrente potrebbe risultare in lock, se non è riuscito a scrivere l'unlock per server offline o altri problemi, quindi, lo forziamo
                                    this.SemaphoreUnlock(string.Format("Unlock forced after {0} elapsed events locked by same server ({1} seconds), because there are no cache_log_messages newer than {2} minutes.", this._numIntervalsLockedBeforeUnlock, (Settings.Default.IntervalSeconds*this._numIntervalsLockedBeforeUnlock), TOO_LONG_CACHE_MESSAGES_TIMESPAN_MINUTES));
                                }
                                else
                                {
                                    // Il lock è del servizio corrente, stiamo scodando, ma è scaduto il tempo di lavoro
                                    this.WriteLog("Re-enter on elapsed event (every {0} seconds). Lock already set by same server, queue processing is taking too much time to complete ({1} seconds).", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.ReenterOnTimerWarning, EventLogEntryType.Error, Settings.Default.IntervalSeconds, (Settings.Default.IntervalSeconds*this._numIntervalsLockedBeforeUnlock));
                                }
                            }
                            else
                            {
                                this.WriteLog("Unable to retrieve cache_log_messages information from database.", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.DBSemaphoreLockWarning, EventLogEntryType.Error);
                            }
                        }
                        else
                        {
                            this.WriteLog("Re-enter on elapsed event (every {0} seconds). Lock already set by same server.", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.ReenterOnTimerWarning, EventLogEntryType.Warning, Settings.Default.IntervalSeconds);
                        }
                    }
                    else
                    {
                        if (DateTime.Now.Subtract(lockInfo.LastUpdate).TotalMilliseconds > (this._timer.Interval*this._numIntervalsLockedBeforeUnlock))
                        {
                            // Verifichiamo se ci sono righe scodate recenti o meno. Se non ci sono, nessuno sta scodando, ne' questa istanza, ne' altre, quindi occorre rilasciare il lock
                            bool? isMostRecentCacheLogMessageOlderThan = AlertSemaphore.IsMostRecentCacheLogMessageOlderThan(TOO_LONG_CACHE_MESSAGES_TIMESPAN_MINUTES);

                            if (isMostRecentCacheLogMessageOlderThan.HasValue)
                            {
                                if (isMostRecentCacheLogMessageOlderThan.Value)
                                {
                                    // Il lock era stato impostato dall'altro server, che però, pare non essere più in grado di scodare messaggi. Forziamo l'unlock, che è a prescindere dal nome del server
                                    this.SemaphoreUnlock(string.Format("Unlock forced after {0} elapsed events locked by other server ({1} seconds), because there are no cache_log_messages newer than {2} minutes.", this._numIntervalsLockedBeforeUnlock, (Settings.Default.IntervalSeconds*this._numIntervalsLockedBeforeUnlock), TOO_LONG_CACHE_MESSAGES_TIMESPAN_MINUTES));
                                }
                                else
                                {
                                    // Il lock non è del servizio corrente, che sta scodando, ma è scaduto il tempo di lavoro, considerato comunque normale
                                    DebugLog.Write("Operation locked by other server. Nothing to do.");
                                }
                            }
                            else
                            {
                                this.WriteLog("Unable to retrieve cache_log_messages information from database.", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.DBSemaphoreLockWarning, EventLogEntryType.Error);
                            }
                        }
                        else
                        {
                            // Questo è il caso normale in cui lavora l'altro server
                            DebugLog.Write("Operation locked by other server. Nothing to do.");
                        }
                    }
                }
                else
                {
                    this.WriteLog("Unable to get lock information from database. Exception: {0}", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.DBSemaphoreLockWarning, EventLogEntryType.Warning, lockInfo.InnerException);
                }
            }
            else if (lockSemaphoreStatus.Status == SemaphoreState.Unknown)
            {
                this.WriteLog("Lock state unknown. Exception: {0}", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.DBSemaphoreLockWarning, EventLogEntryType.Error, lockSemaphoreStatus.InnerException.Message);
            }
        }

        private void SemaphoreUnlock(string doneMessage = "Lock released.")
        {
            var unlockSemaphoreStatus = AlertSemaphore.Unlock();
            switch (unlockSemaphoreStatus.Status)
            {
                case SemaphoreState.Done:
                    DebugLog.Write(doneMessage);
                    break;
                case SemaphoreState.Error:
                    this.WriteLog("Unlock failed.", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.DBSemaphoreUnlockWarning, EventLogEntryType.Error);
                    break;
                case SemaphoreState.Unknown:
                    this.WriteLog("Unlock failed. Exception: {0}", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.DBSemaphoreUnlockWarning, EventLogEntryType.Error, unlockSemaphoreStatus.InnerException);
                    break;
            }
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            this._timer.Start();
            this.WriteLog("Service started", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.OkInfo, EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            base.OnStop();
            this._timer.Stop();
            this.WriteLog("Service stopped", GrisEventCategory.SystemEvents, (int) GrisSystemEvents.OkInfo, EventLogEntryType.Information);

            this.SemaphoreUnlock();
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            this._timer.Start();
        }

        protected override void OnPause()
        {
            base.OnPause();
            this._timer.Stop();

            this.SemaphoreUnlock();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            this._timer.Stop();
            this._timer.Dispose();
            this._timer = null;

            this.SemaphoreUnlock();
        }

        private void WriteLog(string text, GrisEventCategory category, int eventId, EventLogEntryType entryType, params object[] args)
        {
            var message = args == null ? text : string.Format(text, args);
            this.MyEventLog.WriteEntry(message, entryType, eventId, (short) category);
            DebugLog.Write(message, entryType);
        }
    }
}
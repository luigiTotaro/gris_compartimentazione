﻿using System;
using System.ServiceProcess;
using System.Diagnostics;

namespace GrisSuite.Alerts
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            var createLogSource = false;

            foreach (var arg in args)
            {
                switch (arg)
                {
                    case "/cl":
                        createLogSource = true;
                        break;
                }
            }

            if (createLogSource)
            {
                CreateLogSource();
            }
            else if (Environment.UserInteractive)
            {
                Console.WriteLine(AppDomain.CurrentDomain.FriendlyName + " se lanciato a linea di comando accetta le seguenti opzioni:");
                Console.WriteLine("/cl  = Crea la log source {0} nel log {1}", AlertsService.LogSource, AlertsService.LogFile);
            }
            else
            {
                var servicesToRun = new ServiceBase[] {new AlertsService()};
                ServiceBase.Run(servicesToRun);
            }
        }

        private static void CreateLogSource()
        {
            try
            {
                // Registrazione della sorgente di log 
                if (!EventLog.SourceExists(AlertsService.LogSource))
                {
                    EventLog.CreateEventSource(AlertsService.LogSource, AlertsService.LogFile);
                    Console.WriteLine("OK LogSource {0} in log {1} creato.", AlertsService.LogSource, AlertsService.LogFile);
                }
                else
                {
                    Console.WriteLine("LogSource {0} già esistente.", AlertsService.LogSource);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore nella creazione della LogSource, Error:{0}", ex.Message);
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace GrisSuite
{
    class Program
    {
        static void Main(string[] args)
        {
            string importClass = string.Empty;
            string command = string.Empty;
            string filename = string.Empty;

            if (args.Length >= 1)
            {
                string[] s = args[0].Trim().Split('.');
                if (s.Length == 2)
                {
                    importClass = s[0].Trim();
                    command = s[1].Trim();
                }

            }

            if (args.Length >= 2)
            {
                filename = args[1].Trim();
            }

            if (importClass == string.Empty || command  == string.Empty || filename == string.Empty) 
            {
                Console.WriteLine("Telefin Import Export utility");
                Console.WriteLine("TImpExp command filename");
                Console.WriteLine("Valid commmand \"ImpExpRegionList.Import\" = Import Regions,Zones,Nodes from a xml file");
                Console.WriteLine("Valid commmand \"ImpExpRegionList.Export\" = Export Regions,Zones,Nodes to a xml file");
            }
            else 
            {
                try
                {
                    Assembly a = AppDomain.CurrentDomain.Load("GrisSuite.ImpExp");



                    if (string.Compare(command, "Import", true)==0)
                    {
                        IImport importer = (IImport)a.CreateInstance("GrisSuite." + importClass);
                        importer.ImportFileToDataSet(filename);
                    }
                    else if (string.Compare(command, "Export", true)==0)
                    {
                        IExport exporter = (IExport)a.CreateInstance("GrisSuite." + importClass);
                        exporter.ExportToFile(filename);
                    }

                    Console.WriteLine(String.Format("{0}.{1} completed sucessfully.",importClass,command));

                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("Error on {0}.{1} : {2}", importClass,command,ex.Message));
                }

            }
         }
    }
}

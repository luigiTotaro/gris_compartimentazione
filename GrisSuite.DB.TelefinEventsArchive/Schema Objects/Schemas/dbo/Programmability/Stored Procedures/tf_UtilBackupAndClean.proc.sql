﻿-- Esegue il backup del database e poi pulisce la tabella di events_archiveL2 
-- @FileNameBase = Path e prefisso del file di backup da creare, Es.: e:\TelefinEventsArchiveBackup\TelefinEventArchive
-- La procedura completa il @FileNameBase con la data di centralizzazione minore e maggione degli eventi contenuti 
-- nella tabella history e con l'estensione .bak 
CREATE PROCEDURE [dbo].[tf_UtilBackupAndClean]
	@FileNameBase VARCHAR(255)
	AS

	DECLARE @FileName VARCHAR(MAX)
	DECLARE @MinDate DateTime
	DECLARE @MaxDate DateTime
	DECLARE @Result INT

	IF EXISTS (SELECT EventArchiveID FROM events_archiveL2)
	BEGIN
		IF OBJECT_ID('tempdb..##TelefinEventsArchiveMutex') IS NULL 
		BEGIN
			BEGIN TRY
				CREATE TABLE ##TelefinEventsArchiveMutex (id int);
				
				-- trova la data minima e la massima
				SELECT @MinDate = MIN(Centralized), @MaxDate = Max(Centralized)
					FROM dbo.events_archiveL2
					
				-- crea il nome del file di backup
				SET @FileName = @FileNameBase 
							+ REPLACE(REPLACE(REPLACE(CONVERT(varchar(20),@MinDate, 120),' ',''),'-',''),':','')
							+ 'to' 
							+ REPLACE(REPLACE(REPLACE(CONVERT(varchar(20),@MaxDate, 120),' ',''),'-',''),':','')
							+ '.bak'

				DECLARE @res INT
				

				-- fai backup
				BACKUP DATABASE TelefinEventsArchive TO DISK = @FileName  WITH FORMAT;

				-- tronca la tabella di storico
				TRUNCATE TABLE events_archiveL2;


			END TRY
			BEGIN CATCH
				IF OBJECT_ID('tempdb..##TelefinEventsArchiveMutex') IS NOT NULL DROP TABLE ##TelefinEventsArchiveMutex
				RAISERROR('Errore in fase di backup e pulizia del database',16,1) 
			END CATCH		
			IF OBJECT_ID('tempdb..##TelefinEventsArchiveMutex') IS NOT NULL DROP TABLE ##TelefinEventsArchiveMutex
		END
		ELSE
		BEGIN
			RAISERROR('Backup e pulizia del database già in corso.',16,1) 
		END
	END
	ELSE
	BEGIN
		PRINT 'Nessuna riga nella tabella dbo.events_archiveL2'
	END


RETURN
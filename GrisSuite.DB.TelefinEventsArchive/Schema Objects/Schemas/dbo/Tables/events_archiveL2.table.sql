﻿CREATE TABLE [dbo].[events_archiveL2] (
    [EventArchiveID] UNIQUEIDENTIFIER NOT NULL,
    [EventID]        UNIQUEIDENTIFIER NOT NULL,
    [DevID]          BIGINT           NOT NULL,
    [EventData]      VARBINARY (MAX)  NOT NULL,
    [EventCategory]  TINYINT          NOT NULL,
    [Created]        DATETIME         NULL,
    [Requested]      DATETIME         NOT NULL,
    [ToBeDeleted]    BIT              NOT NULL,
    [Centralized]    DATETIME         NOT NULL,
    [EventDevId]     INT              NULL
);




﻿CREATE TABLE [dbo].[events_devices] (
    [EventDevId]         INT           IDENTITY (1, 1) NOT NULL,
    [CreationDate]       DATETIME      NOT NULL,
    [DevID]              BIGINT        NOT NULL,
    [NodID]              BIGINT        NULL,
    [SrvID]              BIGINT        NULL,
    [Name]               VARCHAR (64)  NULL,
    [Type]               VARCHAR (16)  NULL,
    [SN]                 VARCHAR (16)  NULL,
    [Addr]               VARCHAR (32)  NULL,
    [ServerName]         VARCHAR (64)  NULL,
    [ServerHost]         VARCHAR (64)  NULL,
    [ServerFullHostName] VARCHAR (256) NULL,
    [ServerIP]           VARCHAR (16)  NULL,
    [ServerVersion]      VARCHAR (32)  NULL,
    [ServerMAC]          VARCHAR (16)  NULL,
    [NodeName]           VARCHAR (64)  NULL
);


/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * mibs.cpp - Modulo implementazione mibs in base al tipo periferica.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 11/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "utility.h"
#include "mibs.h"

namespace subagent {

	/**
	 *  Crea istanza della classe principale MIBs per la gestione delle mibs delle periferiche
	 *  poste sotto diagnistica come da configurazione servizio.
	 *
	 *	@param[in]  pConfig  configurazione servizio.
	 */
	MIBs::MIBs (ServiceConfig* pConfig)
	{
		try {
			pAIMCTS0 = NULL;
			pAIMCTSN = NULL;
			pDT13000 = NULL;
			pDT13100 = NULL;
			pTA74000 = NULL;

			AIMCTS0_ThreadCrashed = false;
			AIMCTSN_ThreadCrashed = false;
			DT13100_ThreadCrashed = false;
			DT13000_ThreadCrashed = false;
			TA74000_ThreadCrashed = false;
			UpdateRegKeyFailed    = false;

			if (!pConfig->getDevicesConfig()->empty()) {

				for(list<DevConfig>::iterator it=pConfig->getDevicesConfig()->begin(); it != pConfig->getDevicesConfig()->end(); it++) {
					DevConfig dconfig = *it;
					
					if (dconfig.getEnabled()) {
						switch (this->deviceType(dconfig.getType())) {
							case DEVICE_AIMCTS0:
								if (this->pAIMCTS0 == NULL) {
									this->pAIMCTS0 = new diagAIMCTS0(pConfig->getDatabaseConfig(), &dconfig);
									this->deviceLogInfo(&dconfig, "device under diagnostics");
								}
								else {
									this->deviceLogError(&dconfig, "device type already configurated");
								}
								break;
							case DEVICE_AIMCTSN:
								if (this->pAIMCTSN == NULL) {
									this->pAIMCTSN = new diagAIMCTSN(pConfig->getDatabaseConfig(), &dconfig);
									this->deviceLogInfo(&dconfig, "device under diagnostics");
								}
								else {
									this->deviceLogError(&dconfig, "device type already configurated");
								}
								break;
							case DEVICE_DT13000:
								if (this->pDT13000 == NULL) {
									this->pDT13000 = new diagDT13000(pConfig->getDatabaseConfig(), &dconfig);
									this->deviceLogInfo(&dconfig, "device under diagnostics");
								}
								else {
									this->deviceLogError(&dconfig, "device type already configurated");
								}
								break;
							case DEVICE_DT13100:
								if (this->pDT13100 == NULL) {
									this->pDT13100 = new diagDT13100(pConfig->getDatabaseConfig(), &dconfig);
									this->deviceLogInfo(&dconfig, "device under diagnostics");
								}
								else {
									this->deviceLogError(&dconfig, "device type already configurated");
								}
								break;
							case DEVICE_TA74000:
								if (this->pTA74000 == NULL) {
									this->pTA74000 = new diagTA74000(pConfig->getDatabaseConfig(), &dconfig);
									this->deviceLogInfo(&dconfig, "device under diagnostics");
								}
								else {
									this->deviceLogError(&dconfig, "device type already configurated");
								}
								break;
							default:
								this->deviceLogError(&dconfig, "device type unknown");
						}
					}
					else {
						this->deviceLogInfo(&dconfig, "device disabled");
					}
				}
			}
			else {
				snmp_log(LOG_INFO, "No device configurated.\n");
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "MIBs initialization failed.\n");
		}
	}

	/**
	 *  Distruttore istanza
	 */
	MIBs::~MIBs (void)
	{
		if (pAIMCTS0)  delete pAIMCTS0;
		if (pAIMCTSN)  delete pAIMCTSN;
		if (pDT13000)  delete pDT13000;
		if (pDT13100)  delete pDT13100;
		if (pTA74000)  delete pTA74000;

		pAIMCTS0 = NULL;
		pAIMCTSN = NULL;
		pDT13000 = NULL;
		pDT13100 = NULL;
		pTA74000 = NULL;
	}

	/**
	 *  Restituisce il codice della periferica dato il tipo riportato in formato stringa
	 *
	 *  @param[in]  type  stringa identificativa della periferica
	 *
	 *  @return  codice tipo periferica, DEVICE_UNKNOWN se periferica non gestita
	 */
	MIBs::device_t MIBs::deviceType (string& type)
	{
		if (type == "AIMCTS0")  return DEVICE_AIMCTS0;
		if (type == "AIMCTSN")  return DEVICE_AIMCTSN;
		if (type == "DT13000")  return DEVICE_DT13000;
		if (type == "DT13100")  return DEVICE_DT13100;
		if (type == "TA74000")  return DEVICE_TA74000;

		return DEVICE_UNKNOWN;
	}

	/**
	 *  Stampa messaggio di errore sul log di sistema
	 *
	 *  @param[in]  pdev  descrittore periferica
	 *  @param[in]  msg   messaggio da registrare
	 */
	void MIBs::deviceLogError (DevConfig *pdev, string msg)
	{
		snmp_log(LOG_ERR, "%s (%s - %s - %s): %s.\n", pdev->getName().c_str(), pdev->getType().c_str(), pdev->getPath().c_str(), pdev->getID().c_str(), msg.c_str());
	}

	/**
	 *  Stampa messaggio informativo sul log di sistema
	 *
	 *  @param[in]  pdev  descrittore periferica
	 *  @param[in]  msg   messaggio da registrare
	 */
	void MIBs::deviceLogInfo (DevConfig *pdev, string msg)
	{
		snmp_log(LOG_INFO, "%s (%s - %s - %s): %s.\n", pdev->getName().c_str(), pdev->getType().c_str(), pdev->getPath().c_str(), pdev->getID().c_str(), msg.c_str());
	}

	/**
	 *  Verifica se tutti i processi attivati sono in esecuzione.
	 *
	 *  @return  TRUE se tutti i processi sono attivi.
	 */
	bool MIBs::threadsRunning (void)
	{
		bool running = true;

		if (pAIMCTS0) {
			if (!pAIMCTS0->check()) {
				snmp_log(LOG_ERR, "AIMCTS0: thread cheshed.\n");
				this->AIMCTS0_ThreadCrashed = true;
				running = false;
			}
			else {
				this->AIMCTS0_ThreadCrashed = false;
			}
		}

		if (pAIMCTSN) {
			if (!pAIMCTSN->check()) {
				if (!this->AIMCTSN_ThreadCrashed) {
					snmp_log(LOG_ERR, "AIMCTSN: thread cheshed.\n");
					this->AIMCTSN_ThreadCrashed = true;
				}
				running = false;
			}
			else {
				this->AIMCTSN_ThreadCrashed = false;
			}
		}

		if (pDT13000) {
			if (!pDT13000->check()) {
				snmp_log(LOG_ERR, "DT13000: thread cheshed.\n");
				this->DT13000_ThreadCrashed = true;
				running = false;
			}
			else {
				this->DT13000_ThreadCrashed = false;
			}
		}

		if (pDT13100) {
			if (!pDT13100->check()) {
				snmp_log(LOG_ERR, "DT13100: thread cheshed.\n");
				this->DT13100_ThreadCrashed = true;
				running = false;
			}
			else {
				this->DT13100_ThreadCrashed = false;
			}
		}

		if (pTA74000) {
			if (!pTA74000->check()) {
				snmp_log(LOG_ERR, "TA74000: thread cheshed.\n");
				this->TA74000_ThreadCrashed = true;
				running = false;
			}
			else {
				this->TA74000_ThreadCrashed = false;
			}
		}

		if (running) {
			int retcode;

			if ((retcode=UpdateLastAutoCheck()) != applyException::SYS_NO_ERROR) {
				snmp_log(LOG_ERR, "Update LastAutoCheck key register failed (error code %d).\n", retcode);
				this->UpdateRegKeyFailed = true;
				running = false;
			}
			else {
				this->UpdateRegKeyFailed = false;
			}
		}

		return running;
	}
}
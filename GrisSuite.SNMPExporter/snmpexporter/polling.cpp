/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * polling.cpp - Implementazione classe per la gestione delle attivita periodiche.
 *
 * @author Riccardo Venturini - Paolo Colli
 * @version 1.0.0.0 11/05/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include <windows.h>
#include <iostream>

#include "net-snmp/net-snmp-config.h"
#include "net-snmp/library/snmp_logging.h"
#include "polling.h"


// Definzione tempi di polling in sec
#define STEP_POLLING_TIME      1
#define DEFAULT_POLLING_TIME   5 

using namespace subagent;

Polling::Polling() {
	this->init(DEFAULT_POLLING_TIME);
}

Polling::Polling(unsigned int pollingTime) {
	this->init(pollingTime);
}

Polling::~Polling() {
	delete this->pollingThread;
}

void Polling::init(unsigned int pollingTime) {
	setPollingTime(pollingTime);
	this->pollingThread = new Thread(this);

	if (this->pollingThread == NULL) {
		snmp_log(LOG_ERR, "Error creating thread.\n");
	}
}

void Polling::start() {
	if (this->pollingThread && !this->pollingThread->start()) {
		snmp_log(LOG_ERR, "Start %s thread failure (%d).\n", this->getPollingName().c_str(), this->error());
	}
}

void Polling::stop() {
	this->pollingThread->exit();
	this->pollingThread->join(WAIT_FOR_END_THREAD);
}

void Polling::setPollingTime(unsigned int pollingTime)
{
	this->pollingTime = pollingTime;
}

unsigned int Polling::getPollingTime() const
{
	return this->pollingTime;
}

void Polling::setPollingName(string& name)
{
	if (this->pollingThread) {
		pollingThread->setName(name);
	}
}

string& Polling::getPollingName()
{
	static string name = "";
	return this->pollingThread ? this->pollingThread->getName() : name;
}

int Polling::error()
{
	return this->pollingThread ? this->pollingThread->error() : 0;
}


bool Polling::check () {
	return this->pollingThread ? this->pollingThread->verifylife(this->pollingTime*2) : false;
}

bool Polling::running () {
	return this->pollingThread && this->pollingThread->running();
}

/**
 *  Metodo di implementazione attivita' periodiche di aggiornamento tabella.
 */
void Polling::Run() {
	while (this->pollingThread->running()) {
		this->Update();
		
		this->pollingThread->refreshlife();

		unsigned int pollingTime = this->pollingTime;
		while (pollingTime && this->pollingThread->running()) {
			if (STEP_POLLING_TIME < pollingTime) {
				Sleep(STEP_POLLING_TIME*1000);
				pollingTime -= STEP_POLLING_TIME;
			}
			else {
				Sleep(pollingTime);
				pollingTime = 0;
			}
		}
	}
}

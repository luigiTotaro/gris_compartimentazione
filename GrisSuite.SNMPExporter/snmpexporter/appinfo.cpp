/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 * Usa Net-SNMP 5.6.1.1
 *
 * app_info.cpp - Modulo di definizione informazioni generali applicazione.
 *
 * @author Enrico Alborali, Paolo Colli
 * @version 1.0.0.0 27/06/2012
 * @copyright 2012 Telefin S.p.A.
 */

// Inclusioni standard
#include <stdafx.h>
#include "appinfo.h"

using namespace std;

// Variabili globali applicazione
const string	app_vendor    ("Telefin S.p.A.");
const string	app_web       ("www.telefin.it");
const string	app_email     ("info@telefin.it");
const string	app_name      ("SNMPExporterService");
const string	app_code      ("US20010");
const string	app_version   ("1.0.2.0");
const string	app_datetime  ("2012/11/09 11:10");

const string	mib_subagent_full_version = app_name + " version " + app_version + " build " + app_datetime;

// Direttori file di configurazione servizio
string  app_config_path ("");


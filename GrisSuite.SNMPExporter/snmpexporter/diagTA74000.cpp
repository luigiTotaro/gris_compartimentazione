/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagTA74000.h - Modulo di gestione diagnostica alimentatore TA74000.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "diagTA74000.h"
#include "alarmTA74000.h"

using namespace std;
using namespace mib;
using namespace mib_hwTA74000;

namespace subagent {

	/**
	 *  Tabella oggetti mib da salvare su file
	 */
	int diagTA74000::mibMaskToSave[] = {
		scalar::locnodeAlarmsMask_id ,
		scalar::nodesAlarmsMask_id   ,
		scalar::nullID
	};


	/**
	 *  Costruttore istanza classe diagnostica periferica TA74000
	 *
	 *  @param[in]  dbcfg   dati configurazione database
	 *  @param[in]  devcfg  dati configurazione periferica
	 */
	diagTA74000::diagTA74000 (DBConfig* dbcfg, DevConfig* devcfg) : diagDEVICE (devcfg)
	{
		// Carica funzione di gestione scrittura oggetti mib
		this->pMib->scalarObj.SetCallback(mibObjectWritten, this);
		this->pMib->trapsEnaTableObj.SetRowAction(mibTrapsControl, this);

		// Attiva gestione dati mib
		this->pMib->SetRunning();
	
		// Crea istanza classe di consultazione dati DB
		this->pDatabase = new dbTA74000(dbcfg, devcfg);

		this->threadStart();
	}

	/**
	 *  Distruttore istanza
	 */
	diagTA74000::~diagTA74000 (void)
	{
		delete this->pDatabase;
	}

	/**
	 *  Metodo gestione aggiornamento oggetti mib
	 */
	void diagTA74000::Update (void)
	{
		this->pDatabase->dbLock();

		if (this->threadRunning()) {
			this->pDatabase->updateMIB();
		}

		this->pDatabase->dbUnlock();
	}

	/**
	 *  Metodo di gestione post-scrittura parametri scalari
	 *
	 *  @param[in]  id   identificativo oggetto scalare scritto
	 *  @param[in]  obj  istanza della classe che opera sulla mib
	 *
	 *  @ereturn    true se scrittura valida.
	 */
	static bool mibObjectWritten (mib_hwTA74000::scalar::scalar_id_t id, void* obj)
	{
		diagTA74000* This = static_cast<diagTA74000*>(obj);

		try {
			switch (id) {
				case scalar::devCommand_id:  return This->mibDeviceCommand();
				default:                     return true;
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when writing mib object.\n", This->deviceType.c_str());
		}
		return false;
	}

	/**
	 *  Metodo di gestione manipolazione righe tabella abilitazione traps.
	 *
	 *  @param[in]  row  riga tabella su cui agire
	 *  @param[in]  act  azione eseguita
	 *  @param[in]  obj  istanza della classe che opera sulla mib
	 *
	 *  @ereturn    true se riga trattata correttamente.
	 */
	static bool mibTrapsControl (netsnmp_tdata_row* row, RowAction act, void* obj)
	{
		diagTA74000* This = static_cast<diagTA74000*>(obj);

		try {
			int32_tt count = This->pMib->scalarObj.trapsNumber().get();

			switch (act) {
				case ROW_CHECK_TO_CREATE:
					break;

				case ROW_CREATED:
					count++;
					This->pMib->scalarObj.trapsNumber().set(count);
					break;

				case ROW_CHECK_TO_DESTROY:
					if (count <= 0)  return false;
					break;

				case ROW_DESTROYED:
					count--;
					This->pMib->scalarObj.trapsNumber().set(count);
					break;

				default:
					return false;
			}
			return true;
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when creating row in table enabled traps.\n", This->deviceType.c_str());
		}
		return false;
	}

}



/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagDEVICE.h - Modulo implementazione classe generica di gestione diagnostica.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 11/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef DIAGDEVICE_H
#define DIAGDEVICE_H

#include <string>

#include "srvconfig.h"
#include "appinfo.h"
#include "polling.h"
#include "traps.h"

using namespace std;

namespace subagent {

	/**
	 *  Classe template di gestione parte comune a tutte le mibs
	 */
	template <typename MIBTYPE>
	class diagDEVICE : public Polling {

	protected:
		MIBTYPE*    pMib;

		string      deviceType;
		string      trapConfigFile;
		string      maskConfigFile;

		static int  mibMaskToSave[];

	public:

		/**
		 *  Costruttore istanza classe generica di gestione diagnostica periferica
		 *
		 *  @param[in]   devcfg  configurazione device
		 */
		diagDEVICE (DevConfig* devcfg);

		/**
		 *  Distruttore istanza
		 */
		~diagDEVICE (void);

		/**
		 *  Verifica lo stato di funzionamento del thread di gestione diagnostica periferica
		 *
		 *  true se thread attivo
		 */
		void threadStart (void);

		/**
		 *  Verifica lo stato di funzionamento del thread di gestione diagnostica periferica
		 *
		 *  true se thread attivo
		 */
		bool threadRunning (void);

		/**
		 *  Verifica l'esistenza del file
		 *
		 *  @param[in]   fname  nome file da verificare
		 *
		 *  @return      true se file esistente
		 */
		bool exists (string fname);

		/**
		 *  Gestione scrittura dell'oggetto "devCommand" per la gestione della configurazione mib.
		 *
		 *  @return  true se comando eseguito.
		 */
		bool mibDeviceCommand (void);

		/**
		 *  Carica maschere di abilitazione allarmi come da file di configurazione
		 */
		void loadMaskAlarms (void);

		/**
		 *  Carica maschere di abilitazione allarmi come da file di configurazione
		 */
		void storeMaskAlarms (void);

	};


	/**
	 *  Costruttore istanza classe generica di gestione diagnostica periferiva
	 *
	 *  @param[in]   devcfg  configurazione device
	 */
	template <typename MIBTYPE>
	diagDEVICE<MIBTYPE>::diagDEVICE (DevConfig* devcfg)
	{
		try {
			deviceType = "DEVICE";
			deviceType = devcfg->getType();

			// Inizializzo MIB custom 
			pMib = MIBTYPE::Instance();

			// Definisce nome file di configurazione mib
			trapConfigFile = app_config_path + "\\" + deviceType + ".trap.conf";
			maskConfigFile = app_config_path + "\\" + deviceType + ".mask.conf";

			// Imposta versione subagent
			pMib->scalarObj.subagentVersion().set(mib_subagent_full_version);

			// Carica configurazione maschere attivazione allarmi
			if (this->exists(maskConfigFile)) {
				this->loadMaskAlarms();
			}

			// Carica configurazione traps
			CTraps<MIBTYPE>* trapConfig = new CTraps<MIBTYPE>;
			if (this->exists(trapConfigFile)) {
				trapConfig->load(trapConfigFile);
			}
			else {
				trapConfig->load();
			}
			delete trapConfig;

			// Attiva thread di polling per aggiornamento mib
			this->setPollingName(devcfg->getType());
			this->setPollingTime(devcfg->getPollingTime());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: initialization failed.\n", deviceType.c_str());
		}
	}

	/**
	 *  Distruttore istanza
	 */
	template <typename MIBTYPE>
	diagDEVICE<MIBTYPE>::~diagDEVICE (void)
	{
		this->stop();
		if (pMib) delete pMib;
	}

	/**
	 *  Verifica lo stato di funzionamento del thread di gestione diagnostica periferica
	 *
	 *  true se thread attivo
	 */
	template <typename MIBTYPE>
	void diagDEVICE<MIBTYPE>::threadStart (void)
	{
		this->start();
	}

	/**
	 *  Verifica lo stato di funzionamento del thread di gestione diagnostica periferica
	 *
	 *  true se thread attivo
	 */
	template <typename MIBTYPE>
	bool diagDEVICE<MIBTYPE>::threadRunning (void)
	{
		return this->running();
	}

	/**
	 *  Verifica l'esistenza del file
	 *
	 *  @param[in]   fname  nome file da verificare
	 *
	 *  @return      true se file esistente
	 */
	template <typename MIBTYPE>
	bool diagDEVICE<MIBTYPE>::exists (string fname)
	{
		HANDLE           handle;
		WIN32_FIND_DATA  fd;

		if ((handle=FindFirstFile(fname.c_str(),&fd)) != INVALID_HANDLE_VALUE) {
			FindClose(handle);
			return true;
		}
		return false;
	}

	/**
	 *  Gestione scrittura dell'oggetto "devCommand" per la gestione della configurazione mib.
	 *
	 *  @return  true se comando eseguito.
	 */
	template <typename MIBTYPE>
	bool diagDEVICE<MIBTYPE>::mibDeviceCommand (void)
	{
		CTraps<MIBTYPE>* ctrap = new CTraps<MIBTYPE>;
		bool             rcode = true;

		switch(this->pMib->scalarObj.devCommand().get()) {

			case commandSaveConfig:
				ctrap->store(this->trapConfigFile);
				this->storeMaskAlarms();
				break;

			case commandUndoConfig:
				ctrap->load(this->trapConfigFile);
				this->loadMaskAlarms();
				break;

			default:
				return false;
		}
		this->pMib->scalarObj.devCommand().set(commandNULL);

		delete ctrap;
		return rcode;
	}

	/**
	 *  Carica maschere di abilitazione allarmi come da file di configurazione
	 */
	template <typename MIBTYPE>
	void diagDEVICE<MIBTYPE>::loadMaskAlarms (void)
	{
		MIBTYPE*  pMib;
		cline     cln;

		pMib = MIBTYPE::Instance();

		ifstream in(this->maskConfigFile.c_str());

		if (!in) {
			snmp_log(LOG_ERR, "%s : error opening file for reading.\n", this->maskConfigFile.c_str());
			return;
		}

		pMib->scalarObj.lock();
		try {
			while (!in.eof()) {
				in >> cln;

				int* pObjID = this->mibMaskToSave;

				while (*pObjID != scalar::nullID) {
					if (cln.getKey() == pMib->scalarObj.Key((scalar::scalar_id_t)(*pObjID))) {
						(*static_cast<bits*>(pMib->scalarObj.Item((scalar::scalar_id_t)(*pObjID)))).set(cln.getbValue());
						break;
					}
					pObjID++;
				}
			}
			in.close();
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : error reading file (%s - %d).\n", this->maskConfigFile.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s : error reading file.\n", this->maskConfigFile.c_str());
		}

		pMib->scalarObj.unlock();
	}

	/**
	 *  Carica maschere di abilitazione allarmi come da file di configurazione
	 */
	template <typename MIBTYPE>
	void diagDEVICE<MIBTYPE>::storeMaskAlarms (void)
	{
		MIBTYPE*  pMib;
		
		pMib = MIBTYPE::Instance();
		
		ofstream out(this->maskConfigFile.c_str());
		
		if (!out) {
			snmp_log(LOG_ERR, "%s : error opening file for writing.\n", this->maskConfigFile.c_str());
			return;
		}
		
		try {
			string    soid;
			string    skey;
			uint8_tt  bval [BITS_DATA_SIZE];
			
			int* pObjID = this->mibMaskToSave;

			while (*pObjID != scalar::nullID) {
				soid = pMib->scalarObj.OID((scalar::scalar_id_t)(*pObjID));
				skey = pMib->scalarObj.Key((scalar::scalar_id_t)(*pObjID));

				(*static_cast<bits*>(pMib->scalarObj.Item((scalar::scalar_id_t)(*pObjID)))).get(bval);

				out << cline(soid, skey, ASN_BIT_STR, bval);

				pObjID++;
			}
			out.close();
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : error writing file (%s - %d).\n", this->maskConfigFile.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s : error writing file.\n", this->maskConfigFile.c_str());
		}
	}

}

#endif
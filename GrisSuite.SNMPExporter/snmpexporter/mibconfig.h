/*
 *  config.h
 *  IP-PBX
 *
 *  Created by Paolo Colli on 18/04/12.
 *  Copyright 2012 Telefin SpA. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <string>
#include "mib_objects.h"

using namespace std;
using namespace mib;

namespace subagent {

	class cline {
		string    soid;
		string    skey;
		int       itype;
		long      lvalue;
		string    svalue;
		uint8_tt  bvalue [BITS_DATA_SIZE];

	public:
		cline();
		cline(string soid, string skey, int itype, long lvalue);
		cline(string soid, string skey, int itype, string svalue);
		cline(string soid, string skey, int itype, uint8_tt* bvalue);

		void      clear();
		string    getOID();
		string    getKey();
		int       getType();
		long      getlValue();
		string    getsValue();
		uint8_tt* getbValue();

		friend ostream &operator<<(ostream &stream, cline &cln);
		friend istream &operator>>(istream &stream, cline &cln);

	};

	ostream &operator<<(ostream &stream, cline &cln);
	istream &operator>>(istream &stream, cline &cln);

}

#endif

/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmAIMCTS0.h - Modulo implementazione classe gestione allarmi periferica AIMCTS0.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */
#ifndef ALARMAIMCTS0_H
#define ALARMAIMCTS0_H

#include "hwAIMCTS0.h"
#include "alarmDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwAIMCTS0;

namespace subagent {

	class alarmAIMCTS0 : public alarmDEVICE<hwAIMCTS0> {

		integer  deviceType;   // tipo periferica

	public:
		alarmAIMCTS0  (void);
		alarmAIMCTS0  (bits& flags, bits& mask);
		~alarmAIMCTS0 (void);

		void setDeviceType (int32_tt type);
		bool checkItem     (void);
		void notifyAlarm   (void);

	private:
		void init       (void);
		void updateItem (void);
	};
}

#endif


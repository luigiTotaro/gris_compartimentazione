/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * exception.cpp - Modulo di gestione eccezioni.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include <iostream>
#include "exception.h"

using namespace std;
using namespace subagent;

/**
 *  Costruttote base istanza della classe applyException. Inizializza dati ai valori di default.
 */
applyException::applyException() {
	sDecription = "";
	iCode = 0;
}

/**
 *  Costruttore istanza della classe applyException.
 *
 *  @param[in]   description  descrizione eccezione da gestire
 *  @param[in]   code         codice eccezione da gestire
 */
applyException::applyException(string description, int code) {
	sDecription = description;
	iCode = code;
}

/**
 *  Restituisce la descrizione dell'eccezione
 *
 *  @return      puntatore alla descrizione eccezione.
 */
string& applyException::Description() {
	return sDecription;
}

/**
 *  Restituisce il codice dell'eccezione
 *
 *  @return      codice eccezione.
 */
int applyException::Code() {
	return iCode;
}


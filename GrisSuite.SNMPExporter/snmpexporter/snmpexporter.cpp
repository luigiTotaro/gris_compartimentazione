/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* Usa Net-SNMP 5.6.1.1
*
* snmpexporter.cpp - Modulo principale.
*
* @author Enrico Alborali, Paolo Colli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

// Inclusioni standard
#include "stdafx.h"

// Inclusioni Net-SNMP
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <net-snmp/library/fd_event_manager.h>

// Inclusioni MIB Net-SNMP
#include "notification-log-mib/notification_log.h"
#include "tlstm-mib/snmpTlstmCertToTSNTable/snmpTlstmCertToTSNTable.h"
#include "mibII/vacm_conf.h"

// Inclusioni custom
#include "appinfo.h"
#include "mibs.h"
#include "srvConfig.h"

// Inclusione per applicazione Windows
#ifdef WIN32
#include <windows.h>

// Inclusione per servizio Windows
#ifdef WIN32SERVICE
#include <net-snmp/library/winservice.h>
#endif
#endif


static int  keep_running = 0;
static int  stderrlog    = 0;

static subagent::ServiceConfig*  pConfig = NULL; 
static subagent::MIBs*           pMibs   = NULL;

// Definizioni relative a servizio Windows
#define SNMPEXPORTER_RUNNING 1
#define SNMPEXPORTER_STOPPED 0
int exporter_status = SNMPEXPORTER_STOPPED;
extern "C" LPCTSTR  app_name_long = _T(app_name.c_str());

void          StopSNMPExporterService(void);
int           SNMPExporterMain (int argc, TCHAR * argv[]);
int __cdecl   _tmain           (int argc, TCHAR * argv[]);

/**
* Funzione che stampa i parametri di utilizzo dell'applicazione
*/
static void usage(void)
{
    cerr << endl;
	cerr << "Usage:  snmpexporter [-register] [-quiet] [OPTIONS]" << endl;
    cerr << "        snmpexporter [-unregister] [-quiet]" << endl;
    cerr << endl;
    cerr << "  -h            display this usage message" << endl;
	cerr << "  -v            display version" << endl;
    cerr << "  -register     register as a Windows service" << endl;
    cerr << "                 (followed by -quiet to prevent message popups)" << endl;
    cerr << "                 (followed by the startup parameter list)" << endl;
    cerr << "                 Note that some parameters are not relevant when running as a service" << endl;
    cerr << "  -unregister   unregister as a Windows service" << endl;
    cerr << "                 (followed -quiet to prevent message popups)" << endl;
    cerr << endl;
}

/**
* Funzione per stampare la versione dell'applicazione
*/
static void version(void)
{
	cout << endl;
	cout << app_vendor << endl;
	cout << "Web:    " << app_web << endl;
    cout << "Email:  " << app_email << endl;
	cout << mib_subagent_full_version << endl;
	cout << "Built with Net-SNMP version " << netsnmp_get_version() << endl;
	cout << endl;
}

/**
 * This function checks for packets arriving on the SNMP port and
 * processes them(snmp_read) if some are found, using the select().
 *
 * @return  Returns a positive integer if packets were processed, and -1 if an
 * error was found.
 *
 */
static int subagent_check_and_process(void)
{
#define SELECT_TIMEOUT	3  // second

    int             numfds;
    fd_set          fdset;
    struct timeval  timeout = { SELECT_TIMEOUT, 0 }, *tvp = &timeout;
    int             count;
    int             fakeblock = 0;

    numfds = 0;
    FD_ZERO(&fdset);
    snmp_select_info(&numfds, &fdset, tvp, &fakeblock);

    count = select(numfds, &fdset, NULL, NULL, tvp);

    if (count > 0) {
        /*
         * packets found, process them 
         */
        snmp_read(&fdset);
    } else
        switch (count) {
        case 0:
            snmp_timeout();
            break;
        case -1:
            if (errno != EINTR) {
                snmp_log_perror("select");
            }
            return -1;
        default:
            snmp_log(LOG_ERR, "select returned %d\n", count);
            return -1;
        }                       /* endif -- count>0 */

    /*
     * see if persistent store needs to be saved
     */
    snmp_store_if_needed();

    /*
     * Run requested alarms.  
     */
    run_alarms();

    netsnmp_check_outstanding_agent_requests();

    return count;
}

/**
* Funzione per la gestione del Term Signal
*/
RETSIGTYPE term_handler(int sig)
{
	keep_running = 0;
}

/**
* Main function
* @param *argc
* @param *argv[]
* @returns 0	Always succeeds.  (?)
*/
int SNMPExporterMain(int argc, TCHAR * argv[])
{
    char  options[128] = "aAc:CdD::efF:g:hHI:L:m:M:no:O:PqsS:tu:vx:-:";
    int   arg;

    // Attivo la gestione dei Term Signal
    signal(SIGTERM, term_handler);
    signal(SIGINT, term_handler);

    // Now process options normally.  
    while ((arg = getopt(argc, argv, options)) != EOF) {
        switch (arg) {
			case '-':
				/*
				if (strcasecmp(optarg, "help") == 0) {
					usage();
					exit(0);
				}
				if (strcasecmp(optarg, "version") == 0) {
					version();
					exit(0);
				}

				handle_long_opt(optarg);
				*/
				break;
			case 'h':
				usage();
				exit(0);
			case 'v':
				version();
				exit(0);
			default:
				fprintf(stderr, "invalid option: -%c\n", arg);
				usage();
				exit(1);
				break;
        }
    }

	// Inizializzo socket
    SOCK_STARTUP;
	
	// Inizializzo log
    snmp_log_syslogname(app_name.c_str());

    if (stderrlog) {
		version();
		snmp_enable_stderrlog();
	}
	else if (!snmp_get_do_logging()) {
        snmp_enable_syslog();
    }

	// Imposto come AgentX SubAgent
	netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_AGENT_ROLE, 1);
    netsnmp_ds_toggle_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_AGENT_NO_ROOT_ACCESS);
    netsnmp_ds_toggle_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_AGENT_NO_CONNECTION_WARNINGS);
	
	snmp_log(LOG_INFO, "Starting...\n");

    // Inizializzo la libreria agent Net-SNMP
	init_agent(app_name.c_str());

	// Carica configurazione da file XML
	pConfig = new subagent::ServiceConfig();
	pConfig->xmlLoadConfig();

	// Inizializzo MIB custom 
	pMibs = new subagent::MIBs(pConfig);

	// Inizializzo SNMP
	init_snmp(app_name.c_str());

    exporter_status = SNMPEXPORTER_RUNNING;
	
	// In case we recevie a request to stop (kill -TERM or kill -INT) 
	keep_running = 1;

	snmp_log(LOG_INFO, "Exporter is up and running.\n");
	    
	// Main loop
	while(keep_running) {
		// if you use select(), see snmp_select_info() in snmp_api(3) 
		//     --- OR ---  
		// agent_check_and_process(1); // 0 == don't block

		subagent_check_and_process();

		pMibs->threadsRunning();
	}
	
	// Chiusura
    snmp_log(LOG_INFO, "Service stopped.\n");
	snmp_shutdown(app_name.c_str());
    exporter_status = SNMPEXPORTER_STOPPED;
    snmp_disable_log();
    SOCK_CLEANUP;

	if (pMibs  )  delete pMibs;
	if (pConfig)  delete pConfig;

    return 0;
}

/**
* Main function for Windows
* Parse command line arguments for startup options,
* to start as service or console mode application in windows.
* Invokes appropriate startup functions depending on the 
* parameters passed
*/
int __cdecl _tmain(int argc, TCHAR * argv[])
{
    /*
     * Define Service Name and Description, which appears in windows SCM 
     */
    LPCTSTR lpszServiceName			= _T(app_name.c_str());                                             /* Service Registry Name */
    LPCTSTR lpszServiceDisplayName	= _T("STLC SNMP Exporter Service");                                 /* Display Name */
    LPCTSTR lpszServiceDescription	= _T("SNMP exporter service for STLC1000 monitoring database.");    /* Service Description */

	InputParams     InputOptions;

    int             nRunType = RUN_AS_CONSOLE;
    int             quiet = 0;

    nRunType = ParseCmdLineForServiceOption(argc, argv, &quiet);

    switch (nRunType) {
		// Register as service
		case REGISTER_SERVICE:
			InputOptions.Argc = argc;
			InputOptions.Argv = argv;
			exit (RegisterService(lpszServiceName, lpszServiceDisplayName, lpszServiceDescription, &InputOptions, quiet));
			break;
		// Unregister service
		case UN_REGISTER_SERVICE:
			exit (UnregisterService(lpszServiceName, quiet));
			exit(0);
			break;
		// Run as service
		case RUN_AS_SERVICE:
			// Register Stop Function 
			RegisterStopFunction(StopSNMPExporterService);
			// Run exporter main as service
			app_config_path.assign(SERVICE_MODE_CONFIG_PATH);
			return RunAsService(SNMPExporterMain);
			break;
		// Run in console mode
		default:
			// Usa stderr per il log
			stderrlog = 1;
			// Run exporter main
			app_config_path.assign(CONSOLE_MODE_CONFIG_PATH);
			return SNMPExporterMain(argc, argv);
			break;
	}
}

/**
* Function to stop SNMP Exporter Service
* WARNING: This portion is still not working
*/
void StopSNMPExporterService(void)
{
    // Shut Down Service
    term_handler(1);
    // Wait till trap receiver is completely stopped 
    while (exporter_status != SNMPEXPORTER_STOPPED) {
        Sleep(100);
    }
}

/*
 *  config.cpp
 *  IP-PBX
 *
 *  Created by Paolo Colli on 18/04/12.
 *  Copyright 2012 Telefin SpA. All rights reserved.
 *
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include "exception.h"
#include "mibconfig.h"

using namespace std;
using namespace subagent;

/**
 *  Costruttote base istanza della classe cline. Inizializza dati ai valori di default.
 */
cline::cline() {
	this->clear();
}

/**
 *  Costruttote istanza della classe cline. Imposta dati per oggetto INTEGER
 *
 *  @param[in]   soid    OID oggetto
 *  @param[in]   skey    chiave di descrizione oggetto
 *  @param[in]   itype   tipo oggetto
 *  @param[in]   lvalue  valore oggetto intero
 */
cline::cline(string soid, string skey, int itype, long lvalue) {
	this->clear();
	this->soid = soid;
	this->skey = skey;
	this->itype = itype;
	this->lvalue = lvalue;
}

/**
 *  Costruttote istanza della classe cline. Imposta dati per oggetto STRING
 *
 *  @param[in]   soid    OID oggetto
 *  @param[in]   skey    chiave di descrizione oggetto
 *  @param[in]   itype   tipo oggetto
 *  @param[in]   svalue  valore oggetto stringa
 */
cline::cline(string soid, string skey, int itype, string svalue) {
	this->clear();
	this->soid = soid;
	this->skey = skey;
	this->itype = itype;
	this->svalue = svalue;
}

/**
 *  Costruttote istanza della classe cline. Imposta dati per oggetto BITS
 *
 *  @param[in]   soid    OID oggetto
 *  @param[in]   skey    chiave di descrizione oggetto
 *  @param[in]   itype   tipo oggetto
 *  @param[in]   bvalue  valore oggetto bits
 */
cline::cline(string soid, string skey, int itype, uint8_tt* bvalue) {
	this->clear();
	this->soid = soid;
	this->skey = skey;
	this->itype = itype;
	memcpy(this->bvalue, bvalue, sizeof(this->bvalue));
}

/**
 *  Azzera dati classe.
 */
void cline::clear() {
	this->soid = "";
	this->skey = "";
	this->itype = 0;
	this->lvalue = 0;
	this->svalue = "";
	memset(this->bvalue, 0, sizeof(this->bvalue));
}

/**
 *  Metodi per la restituzione dei valori oggetti
 *
 *  @return     ritorna il valore dell'oggetto.
 */
string cline::getOID() {
	return this->soid;
}

string cline::getKey() {
	return this->skey;
}

int cline::getType() {
	return this->itype;
}

long cline::getlValue() {
	return this->lvalue;
}

string cline::getsValue() {
	return this->svalue;
}

uint8_tt* cline::getbValue() {
	return this->bvalue;
}

/**
 *  Inseritore della classe.
 */
ostream &subagent::operator<<(ostream &stream, cline &cln) {
	stream << cln.soid << " : " << cln.skey << " = ";

	switch (cln.itype) {
		case ASN_INTEGER:
			stream << "INTEGER : " << cln.lvalue << endl;
			break;

		case ASN_OCTET_STR:
			stream << "STRING : \"" << cln.svalue << "\"" << endl;
			break;

		case ASN_BIT_STR:
			stream << "Hex-STRING :";
			for (int i=0; i < sizeof(cln.bvalue)/sizeof(cln.bvalue[0]); i++) {
				stream << " " << hex << setw(2) << setfill('0') << uppercase << (int)cln.bvalue[i];
			}
			stream << endl;
			break;
			
		default:
			throw applyException("Unknown object type.", applyException::SYS_WRITE_CONFIG_FILE_FAIL);
	}

	return stream;
}

/**
 *  Estrattore della classe.
 */
istream &subagent::operator>>(istream &stream, cline &cln) {
	string stype;
	string stmp;
	long lpos;
	char str [100];
	size_t found;

	cln.clear();

	while (!stream.eof()) {
		char c = stream.peek();

		if (c == '\n' || c == '\r' || c == ' ')
			stream.ignore(1);
		else
			break;
	}

	if (stream.eof()) {
		// Raggiunta fine del file
		return stream;
	}

	lpos = stream.tellg();

	stream >> cln.soid >> stmp;

	if (stream.eof() || cln.soid == "" || stmp != ":") {
		stream.seekg(lpos);
		stream.getline(str, sizeof (str));
		throw applyException(str, applyException::SYS_READ_CONFIG_FILE_FAIL);
	}

	stream >> cln.skey >> stmp;

	if (stream.eof() || cln.skey == "" || stmp != "=") {
		stream.seekg(lpos);
		stream.getline(str, sizeof (str));
		throw applyException(str, applyException::SYS_READ_CONFIG_FILE_FAIL);
	}

	stream >> stype >> stmp;

	if (stream.eof() || stype == "" || stmp != ":") {
		stream.seekg(lpos);
		stream.getline(str, sizeof (str));
		throw applyException(str, applyException::SYS_READ_CONFIG_FILE_FAIL);
	}

	if (stype == "INTEGER") {
		cln.itype = ASN_INTEGER;
		stream >> cln.lvalue;
	} else if (stype == "STRING") {
		cln.itype = ASN_OCTET_STR;
		stream.getline(str, sizeof (str));

		cln.svalue = str;

		found = cln.svalue.find('"');
		if (found == string::npos) {
			stream.seekg(lpos);
			stream.getline(str, sizeof (str));
			throw applyException(str, applyException::SYS_READ_CONFIG_FILE_FAIL);
		}
		cln.svalue.erase(0, found + 1);

		found = cln.svalue.rfind('"');
		if (found == string::npos) {
			stream.seekg(lpos);
			stream.getline(str, sizeof (str));
			throw applyException(str, applyException::SYS_READ_CONFIG_FILE_FAIL);
		}

		cln.svalue.erase(found, cln.svalue.length() - found);
	} else if (stype == "Hex-STRING") {
		cln.itype = ASN_BIT_STR;
		for (int i=0; i < sizeof(cln.bvalue)/sizeof(cln.bvalue[0]); i++) {
			int data;
			stream >> hex >> data;
			cln.bvalue[i] = data;
		}
	} else {
		stream.seekg(lpos);
		stream.getline(str, sizeof (str));
		throw applyException(str, applyException::SYS_READ_CONFIG_FILE_FAIL);
	}

	return stream;
}


/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbAIMCTS0.h - Modulo acquisizione dati periferica AIMCTSN da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

#include <SQLAPI.h> // main SQLAPI++ header
#include "hwAIMCTS0.h"
#include "diagDEVICE.h"
#include "reference.h"

using namespace std;
using namespace mib_hwAIMCTS0;


namespace subagent{


	class dbAIMCTS0 : public reference {
		hwAIMCTS0* pMib;
		DBConfig*  dbConfig;

		ElencoMIB generalInfo;
		ElencoMIB severityInfo;
		ElencoMIB infoStation;		
		ElencoMIB streamInfo;		
		ElencoMIB cpusts;
		ElencoMIB ctssTable;
		ElencoMIB modulesTable;
		ElencoMIB consolesTable;
		ElencoMIB acdcsTable;
		ElencoMIB dcdcsTable;

		int32_tt  severity;
		bits      ctssAlarmsMask_New;
		bits      ctssAlarmsMask_Old;
		bits      cpustsAlarmsMask_New;
		bits      cpustsAlarmsMask_Old;
		bits      modulesAlarmsMask_New;
		bits      modulesAlarmsMask_Old;
		bits      consolesAlarmsMask_New;
		bits      consolesAlarmsMask_Old;
		bits      acdcsAlarmsMask_New;
		bits      acdcsAlarmsMask_Old;
		bits      dcdcsAlarmsMask_New;
		bits      dcdcsAlarmsMask_Old;

	public:
		// Costruttore istanza
		dbAIMCTS0 (DBConfig* dbcfg, DevConfig* devcfg);
		// Distruttore istanza
		~dbAIMCTS0 (void);
	
		void maxSeverity (int32_tt severity);
		void updateMIB   (void);

		void sendToMIB(ElementoMIB &el, int row, Conversion& value);
	};

}
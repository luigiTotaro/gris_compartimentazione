/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * polling.h - Implementazione classe gestione configurazione servizio.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 09/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef SERVCONFIG_H
#define SERVCONFIG_H

#include <string>
#include <list>

using namespace std;

namespace subagent {

	// Classe di definizione configurazione database
	class DBConfig {
	private:
		string name;
		string type;
		string version;
		string host;
		string user;
		string password;
		string hostname;
		bool   persistent;

	public:
		DBConfig (void);

		void xmlLoadConfig (void* source);

		string& getName       (void) { return name;       }
		string& getType       (void) { return type;       }
		string& getVersion    (void) { return version;    }
		string& getHost       (void) { return host;       }
		string& getUser       (void) { return user;       }
		string& getPassword   (void) { return password;   }
		string& getHostname   (void) { return hostname;   }
		bool    getPersistent (void) { return persistent; }
	};

	// Classe di definizione configurazione periferica
	class DevConfig {

	// Struttura di identificazione posizione
	typedef struct {
		unsigned int  id;
		string        name;
		bool          enabled;

		void clear (void) {this->id=0; this->enabled=false; this->name="";}
	} location_t;

	private:
		location_t   region;
		location_t   zone;
		location_t   node;
		location_t   device;

		string       path;
		string       dbID;
		string       type;
		bool         enabled;
		unsigned int polling_time;

	public:
		DevConfig (void);

		void clear (void);

		void xmlLoadRegion (void* source);
		void xmlLoadZone   (void* source);
		void xmlLoadNode   (void* source);
		void xmlLoadDevice (void* source);
		void dbIdentifier  (void);

		string&      getPath        (void) { return path;         }
		string&      getName        (void) { return device.name;  }
		string&      getID          (void) { return dbID;         }
		string&      getType        (void) { return type;         }
		bool         getEnabled     (void) { return enabled;      }
		unsigned int getPollingTime (void) { return polling_time; }
	};

	// Classe di definizione configurazione servizio
	class ServiceConfig {

	private:
		string      xmlFileName;
		string      softName;
		string      softVersion;
		DBConfig    dbConfig;

		list<DevConfig>  devConfigData;

	public:
		ServiceConfig(void);
		~ServiceConfig(void);

		void xmlLoadConfig(void);

		string*          getSoftwareName    (void) { return &softName;    }
		string*          getSoftwareVersion (void) { return &softVersion; }
		DBConfig*        getDatabaseConfig  (void) { return &dbConfig;    }
		list<DevConfig>* getDevicesConfig   (void) { return &devConfigData; }

	private:
		void xmlLoadInfoConfig    (void* source);
		void xmlLoadDevicesConfig (void* source);
	};

}
#endif
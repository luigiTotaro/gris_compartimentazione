/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagDT13100.h - Modulo di gestione diagnostica alimentatore DT13100.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef DIAGDT13100_H
#define DIAGDT13100_H

#include "hwDT13100.h"
#include "dbDT13100.h"
#include "srvConfig.h"
#include "diagDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwDT13100;

namespace subagent {

	class diagDT13100 : public diagDEVICE<hwDT13100> {

		dbDT13100* pDatabase;

	public:
		diagDT13100  (DBConfig* dbcfg, DevConfig* devcfg);
		~diagDT13100 (void);

		void Update (void);

	private:
		friend bool mibObjectWritten (mib_hwDT13100::scalar::scalar_id_t id, void* obj);
		friend bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
	};

static bool mibObjectWritten (mib_hwDT13100::scalar::scalar_id_t id, void* obj);
static bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
}

#endif
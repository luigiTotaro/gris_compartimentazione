/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbDT13000.cpp - Modulo acquisizione dati periferica DT13000 da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

/*
USE Telefin;
SELECT * FROM stream_fields WHERE DevID=284249597739009 AND StrID=2 AND FieldID=2 ORDER BY ArrayID;
*/

#include "alarmDT13000.h"
#include "dbDT13000.h"

using namespace std; 
using namespace subagent;



namespace subagent{

	dbDT13000::dbDT13000 (DBConfig* dbcfg, DevConfig* devcfg){

		string DevID = devcfg->getID();
		int i;

		this->pMib     = hwDT13000::Instance();
		this->dbConfig = dbcfg;

		// Copia la maschera degli allarmi attivi, copia necessaria per gestire le variazione della maschera
		this->genstsAlarmsMask_New = this->pMib->scalarObj.genstsAlarmsMask();

		// Query informazioni generali
		generalInfo.sqlQuery    = "SELECT devices.Name as DeviceName, nodes.Name as NodeName, devices.Addr as Addr FROM devices, nodes WHERE devices.DevID=" + DevID + " AND devices.NodID=nodes.NodID";
		generalInfo.n_elementi  = 3;
		generalInfo.elementi    = new ElementoMIB[generalInfo.n_elementi];

		i = 0;
		generalInfo.elementi[i].OID			= "infoDeviceName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter = "DeviceName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoStationName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "NodeName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoDeviceID.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "Addr";
		generalInfo.elementi[i].Operation	= OPERATION_INTEGER;

		// Query severit� periferica
		severityInfo.sqlQuery   = "SELECT * FROM device_status WHERE DevID=" + DevID;
		severityInfo.n_elementi = 1;
		severityInfo.elementi   = new ElementoMIB[severityInfo.n_elementi];
		
		i = 0;
		severityInfo.elementi[i].OID		= "devSeverity.0";
		severityInfo.elementi[i].Type		= ASN_INTEGER;
		severityInfo.elementi[i].DBparameter= "SevLevel";
		severityInfo.elementi[i].Operation	= OPERATION_INTEGER;

		// Query dati periferica
		streamInfo.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " ORDER BY StrID, FieldID";
		streamInfo.n_elementi	= 5;
		streamInfo.elementi		= new ElementoMIB[streamInfo.n_elementi];

		i = 0;
		streamInfo.elementi[i].OID			= "genstsSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "genstsFlags.0";
		streamInfo.elementi[i].Type			= ASN_BIT_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_BUFCONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;
		streamInfo.elementi[i].Data[0].Mask     = 0x87;

		i++;
		streamInfo.elementi[i].OID			= "invltSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "invltValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "btcurrentValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

	}
		 
	dbDT13000::~dbDT13000 (void)
	{
	}

	/**
	 *  Calcola la massima severit�
	 */
	void dbDT13000::maxSeverity (int32_tt severity)
	{
		switch (this->severity) {
			case severityUNKNOWN:
			case severityNORMAL:
				switch (severity) {
					case severityUNKNOWN: this->severity = severityUNKNOWN;  break;
					case severityWARNING: this->severity = severityWARNING;  break;
					case severityERROR  : this->severity = severityERROR;    break;
				}
				break;

			case severityWARNING:
				if (severity == severityERROR)
					this->severity = severityERROR;
				break;

			case severityERROR:
				break;
		}
	}

	void dbDT13000::updateMIB (void)
	{
		this->genstsAlarmsMask_Old = this->genstsAlarmsMask_New;
		this->genstsAlarmsMask_New = this->pMib->scalarObj.genstsAlarmsMask();

		this->DBconnect(this->dbConfig);
		this->sql_value(this->generalInfo, true);
		this->sql_value(this->severityInfo);
		this->sql_value(this->streamInfo);
		this->DBdisconnect();
	}

	void dbDT13000::sendToMIB(ElementoMIB &el, int row, Conversion& value){

#ifdef DB_DEBUG
		cout << "    " << el.OID;
		switch(value.getType()){
			case VINTEGER:
				cout << " --> INT: " << value.getInteger().get() << endl;
				break;
			case VDISPLAYSTRING:
				{
				string s;
				value.getString().get(s);
				cout << " --> STR: " << s << endl;
				}
				break;
			case VBITS:
				cout << " --> BIT: ";
				for(int cb=0; cb<BITS_DATA_SIZE; cb++)
					cout << hex <<(int)value.getBits()[cb] << " ";
				cout << dec << endl;
				break;
			default:
				cout << endl;
		}
#endif		

		try
		{
			if (el.OID.find("infoStationName")!=string::npos){
				pMib->scalarObj.infoStationName()=value.getString();
			}
			else if(el.OID.find("infoDeviceName")!=string::npos){
				pMib->scalarObj.infoDeviceName()=value.getString();
			}
			else if(el.OID.find("infoDeviceID")!=string::npos){
				pMib->scalarObj.infoDeviceID()=value.getInteger();
			}
			else if(el.OID.find("devSeverity")!=string::npos){
				if(pMib->scalarObj.devSeverity()!=value.getInteger()) {
					pMib->scalarObj.devSeverity()=value.getInteger();

					traps<hwDT13000>* trap = new traps<hwDT13000>(pMib->trapSystemSeverityKey(), pMib->trapSystemSeverityOID());
					pMib->trapSystemSeverityNotify();
					delete trap;
				}
			}
			else if(el.OID.find("genstsSeverity")!=string::npos){
				pMib->scalarObj.genstsSeverity()=value.getInteger();
			}
			else if(el.OID.find("genstsFlags")!=string::npos){
				alarmDT13000* alarm = new alarmDT13000(value.getBits(),this->genstsAlarmsMask_New);
				if (alarm->changed(pMib->scalarObj.genstsFlags(),this->genstsAlarmsMask_Old)) {
					alarm->setDeviceID(pMib->scalarObj.infoDeviceID());
					alarm->setSeverity(pMib->scalarObj.genstsSeverity());
					alarm->setSourceOID(pMib->scalarObj.OID(scalar::genstsFlags_id));
					alarm->notifyAlarm();
				}
				delete alarm;
				pMib->scalarObj.genstsFlags()=value.getBits();
			}
			else if(el.OID.find("invltSeverity")!=string::npos){
				pMib->scalarObj.invltSeverity()=value.getInteger();
			}
			else if(el.OID.find("invltValue")!=string::npos){
				pMib->scalarObj.invltValue()=value.getString();
			}
			else if(el.OID.find("btcurrentValue")!=string::npos){
				pMib->scalarObj.btcurrentValue()=value.getString();
			}
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : set mib parameter (%s - %d).\n", el.OID.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when writing mib values.\n", el.OID.c_str());
		}
		
	}

};

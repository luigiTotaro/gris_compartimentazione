/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * thread.h - Implementazione classe di gestione creazione e controllo thread.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef THREAD_H
#define THREAD_H

#include <string>
#include <windows.h>

using namespace std;

namespace subagent {

	/*!
	@class
	@abstract    Creazione e gestione thread.
	@discussion  
	 */

	// Timeout di attesa chiusura regolare del thread, successivamente comanda il kill
	#define WAIT_FOR_END_THREAD		5000

	class Runnable {
	public:
		virtual void Run() = 0;
	};

	class Thread {
	private:
		Runnable*               pObject;          // Funzione che deve essere eseguita dal thread 
		bool                    Terminated;       // Flag di thread terminato
		bool                    Suspended;        // Flag di thread sospeso
		unsigned long           SuspendCount;     // Contatore delle sospensioni
		HANDLE                  Handle;           // Handle del thread
		LPSECURITY_ATTRIBUTES   ThreadAttributes; // Struttura di parametri di sicurezza
		DWORD                   StackSize;        // Dimensione iniziale dello Stack per il Thread
		LPTHREAD_START_ROUTINE  StartAddress;     // Funzione di start del Thread
		LPVOID                  Parameter;        // Puntatore ai parametri da passare al Thread
		DWORD                   CreationFlags;    // Flag di creazione del Thread
		DWORD                   ThreadId;         // ID del Thread
		DWORD                   ExitCode;         // Codice di uscita del thread
		int                     LastErrorCode;    // Codice ultimo errore
		DWORD                   LifeSignal;       // Segnale di vita del thread
		string                  Name;             // Nome del thread

	public:
		Thread(Runnable* pObj, DWORD flags = 0);
		~Thread();

		bool  start       (void);
		bool  close       (void);
		bool  kill        (void);
		bool  exit        (void);
		bool  join        (int timeout = INFINITE);
		bool  suspened    (void);
		bool  resume      (void);
		int   error       (void);
		bool  running     (void);
		void  refreshlife (void);
		bool  verifylife  (int lifetime);

		void    setName (string& name);
		string& getName (void);

	private:
		int exceptionCode (DWORD err);
	};

}

#endif
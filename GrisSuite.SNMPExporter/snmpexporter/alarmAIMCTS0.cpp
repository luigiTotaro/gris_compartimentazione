/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmAIMCTS0.cpp - Modulo implementazione classe gestione allarmi periferica AIMCTS0.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "alarmAIMCTS0.h"

using namespace std;
using namespace mib;
using namespace mib_hwAIMCTS0;

namespace subagent {

	/**
	 *  Unique IDentification per gli allarmi periferica
	 */
	uint32_tt alarmDEVICE<hwAIMCTS0>::UID = 0; 


	/**
	 *  Inizializza oggetti al valore di default
	 */
	void alarmAIMCTS0::init (void)
	{
		try {
			this->deviceType.defval();
		}
		catch (...) {
			throw applyException("alarmAIMCTS0::init: critical error.\n", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Costruttore di default istanza classe gestione allarmi periferica AIMCTS0
	 */
	alarmAIMCTS0::alarmAIMCTS0 (void) : alarmDEVICE ()
	{
		this->init();
	}

	/**
	 *  Costruttore istanza classe gestione allarmi periferica AIMCTS0
	 *
	 *  @param[in]  flags  flags di segnalazione stato periferica
	 *  @param[in]  mask   maschera allarmi periferica
	 */
	alarmAIMCTS0::alarmAIMCTS0 (bits& flags, bits& mask) : alarmDEVICE (flags, mask)
	{
		this->init();
	}

	/**
	 *  Distruttore istanza
	 */
	alarmAIMCTS0::~alarmAIMCTS0 (void)
	{
	}

	/**
	 *  Imposta tipo periferica che ha generato l'allarme
	 *
	 *  @param[in]  type  codice tipo periferica
	 */
	void alarmAIMCTS0::setDeviceType (int32_tt type)
	{
		this->deviceType.set(type);
	}

	/**
	 *  Aggiorna riga tabella con dati allarme
	 */
	void alarmAIMCTS0::updateItem (void)
	{
		try {
			if (this->action == INS_ACTION) {
				alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);
				entry.alarmDeviceType = this->deviceType;
			}
		}
		catch (...) {
			throw applyException("alarmAIMCTS0::updateItem: critical error.\n", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Verifica corrispondenza oggetto con tabella allarmi
	 *
	 *  @return true se l'oggetto corrisponde con allarme selezionato
	 */
	bool alarmAIMCTS0::checkItem (void)
	{
		alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);

		return this->deviceType == entry.alarmDeviceType && this->deviceID == entry.alarmDeviceID;
	}

	/**
	 *  Inserisce allarme in tabella ed invia notifica al server di diagnostica. 
	 */
	void alarmAIMCTS0::notifyAlarm (void)
	{
		this->selectItem();
		dynamic_cast<alarmDEVICE*>(this)->updateItem();
		this->updateItem();
		this->notify();
	}
}



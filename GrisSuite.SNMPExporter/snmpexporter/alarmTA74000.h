/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmTA74000.h - Modulo implementazione classe gestione allarmi periferica TA74000.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */
#ifndef ALARMTA74000_H
#define ALARMTA74000_H

#include "hwTA74000.h"
#include "alarmDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwTA74000;

namespace subagent {

	class alarmTA74000 : public alarmDEVICE<hwTA74000> {

	public:
		alarmTA74000 (bits& flags, bits& mask);

	};
}

#endif


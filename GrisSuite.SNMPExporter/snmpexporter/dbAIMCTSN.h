/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbAIMCTSN.h - Modulo acquisizione dati periferica AIMCTSN da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

#ifndef DBAIMCTSN_H
#define DBAIMCTSN_H

#include <SQLAPI.h> // main SQLAPI++ header
#include "hwAIMCTSN.h"
#include "diagDEVICE.h"
#include "reference.h"

using namespace std;
using namespace mib_hwAIMCTSN;


namespace subagent{


	class dbAIMCTSN : public reference {
		hwAIMCTSN* pMib;
		DBConfig*  dbConfig;

		ElencoMIB generalInfo;
		ElencoMIB severityInfo;
		ElencoMIB streamInfo;
		ElencoMIB cpusts;
		ElencoMIB modulesTable;
		ElencoMIB consolesTable;
		ElencoMIB tonitelsTable;
		ElencoMIB acdcsTable;
		ElencoMIB dcdcsTable;

		int32_tt  severity;
		bits      genstsAlarmsMask_New;
		bits      genstsAlarmsMask_Old;
		bits      cpustsAlarmsMask_New;
		bits      cpustsAlarmsMask_Old;
		bits      modulesAlarmsMask_New;
		bits      modulesAlarmsMask_Old;
		bits      consolesAlarmsMask_New;
		bits      consolesAlarmsMask_Old;
		bits      tonitelsAlarmsMask_New;
		bits      tonitelsAlarmsMask_Old;
		bits      acdcsAlarmsMask_New;
		bits      acdcsAlarmsMask_Old;
		bits      dcdcsAlarmsMask_New;
		bits      dcdcsAlarmsMask_Old;

	public:
		// Costruttore istanza
		dbAIMCTSN (DBConfig* dbcfg, DevConfig* devcfg);
		// Distruttore istanza
		~dbAIMCTSN (void);
	
		void maxSeverity (int32_tt severity);
		void updateMIB   (void);

		void sendToMIB(ElementoMIB &el, int row, Conversion& value);
	};

}
#endif
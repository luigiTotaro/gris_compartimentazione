/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbDT13100.cpp - Modulo acquisizione dati periferica DT13100 da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

/*
USE Telefin;
SELECT * FROM stream_fields WHERE DevID=284249597739009 AND StrID=2 AND FieldID=2 ORDER BY ArrayID;
*/

#include "alarmDT13100.h"
#include "dbDT13100.h"

using namespace std; 
using namespace subagent;



namespace subagent{

	dbDT13100::dbDT13100 (DBConfig* dbcfg, DevConfig* devcfg){

		string DevID = devcfg->getID();
		int i;

		this->pMib     = hwDT13100::Instance();
		this->dbConfig = dbcfg;

		// Copia la maschera degli allarmi attivi, copia necessaria per gestire le variazione della maschera
		this->genstsAlarmsMask_New = this->pMib->scalarObj.genstsAlarmsMask();

		// Query informazioni generali
		generalInfo.sqlQuery    = "SELECT devices.Name as DeviceName, nodes.Name as NodeName, devices.Addr as Addr FROM devices, nodes WHERE devices.DevID=" + DevID + " AND devices.NodID=nodes.NodID";
		generalInfo.n_elementi  = 3;
		generalInfo.elementi    = new ElementoMIB[generalInfo.n_elementi];

		i = 0;
		generalInfo.elementi[i].OID			= "infoDeviceName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter = "DeviceName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoStationName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "NodeName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoDeviceID.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "Addr";
		generalInfo.elementi[i].Operation	= OPERATION_INTEGER;

		// Query severit� periferica
		severityInfo.sqlQuery   = "SELECT * FROM device_status WHERE DevID=" + DevID;
		severityInfo.n_elementi = 1;
		severityInfo.elementi   = new ElementoMIB[severityInfo.n_elementi];
		
		i = 0;
		severityInfo.elementi[i].OID		= "devSeverity.0";
		severityInfo.elementi[i].Type		= ASN_INTEGER;
		severityInfo.elementi[i].DBparameter= "SevLevel";
		severityInfo.elementi[i].Operation	= OPERATION_INTEGER;

		// Query dati periferica
		streamInfo.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " ORDER BY StrID, FieldID";
		streamInfo.n_elementi	= 24;
		streamInfo.elementi		= new ElementoMIB[streamInfo.n_elementi];

		i = 0;
		streamInfo.elementi[i].OID			= "genstsSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "genstsFlags.0";
		streamInfo.elementi[i].Type			= ASN_BIT_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_BUFCONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;
		streamInfo.elementi[i].Data[0].Length   = 4;

		i++;
		streamInfo.elementi[i].OID			= "invltSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "invltValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "invltWarningUpperThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;
		
		i++;
		streamInfo.elementi[i].OID			= "invltErrorUpperThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "4";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;
		
		i++;
		streamInfo.elementi[i].OID			= "invltWarningLowerThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;
		
		i++;
		streamInfo.elementi[i].OID			= "invltErrorLowerThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "3";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;
		
		i++;
		streamInfo.elementi[i].OID			= "b1vltSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "3";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;
		
		i++;
		streamInfo.elementi[i].OID			= "b1vltValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "3";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;
		
		i++;
		streamInfo.elementi[i].OID			= "b2vltSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "4";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "b2vltValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "4";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "bvltWarningUpperThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "6";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "bvltErrorUpperThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "8";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "bvltWarningLowerThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "5";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "bvltErrorLowerThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "7";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "btcurrentValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "5";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "tempSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "6";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "tempValue.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "6";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "tempWarningUpperThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "10";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "tempErrorUpperThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "12";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "tempWarningLowerThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "9";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "tempErrorLowerThreshold.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "2";
		streamInfo.elementi[i].FieldID		= "11";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "infoFirmwareVersion.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "3";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;
	}
		 
	dbDT13100::~dbDT13100 (void)
	{
	}

	/**
	 *  Calcola la massima severit�
	 */
	void dbDT13100::maxSeverity (int32_tt severity)
	{
		switch (this->severity) {
			case severityUNKNOWN:
			case severityNORMAL:
				switch (severity) {
					case severityUNKNOWN: this->severity = severityUNKNOWN;  break;
					case severityWARNING: this->severity = severityWARNING;  break;
					case severityERROR  : this->severity = severityERROR;    break;
				}
				break;

			case severityWARNING:
				if (severity == severityERROR)
					this->severity = severityERROR;
				break;

			case severityERROR:
				break;
		}
	}

	void dbDT13100::updateMIB (void)
	{
		this->genstsAlarmsMask_Old = this->genstsAlarmsMask_New;
		this->genstsAlarmsMask_New = this->pMib->scalarObj.genstsAlarmsMask();

		this->DBconnect(this->dbConfig);
		this->sql_value(this->generalInfo, true);
		this->sql_value(this->severityInfo);
		this->sql_value(this->streamInfo);
		this->DBdisconnect();

	}

	void dbDT13100::sendToMIB(ElementoMIB &el, int row, Conversion& value){

#ifdef DB_DEBUG
		cout << "    " << el.OID;
		switch(value.getType()){
			case VINTEGER:
				cout << " --> INT: " << value.getInteger().get() << endl;
				break;
			case VDISPLAYSTRING:
				{
				string s;
				value.getString().get(s);
				cout << " --> STR: " << s << endl;
				}
				break;
			case VBITS:
				cout << " --> BIT: ";
				for(int cb=0; cb<BITS_DATA_SIZE; cb++)
					cout << hex <<(int)value.getBits()[cb] << " ";
				cout << dec << endl;
				break;
			default:
				cout << endl;
		}
#endif		

		try
		{
			if (el.OID.find("infoStationName")!=string::npos){
				pMib->scalarObj.infoStationName()=value.getString();
			}
			else if(el.OID.find("infoDeviceName")!=string::npos){
				pMib->scalarObj.infoDeviceName()=value.getString();
			}
			else if(el.OID.find("infoDeviceID")!=string::npos){
				pMib->scalarObj.infoDeviceID()=value.getInteger();
			}
			else if(el.OID.find("devSeverity")!=string::npos){
				if(pMib->scalarObj.devSeverity()!=value.getInteger()) {
					pMib->scalarObj.devSeverity()=value.getInteger();

					traps<hwDT13100>* trap = new traps<hwDT13100>(pMib->trapSystemSeverityKey(), pMib->trapSystemSeverityOID());
					pMib->trapSystemSeverityNotify();
					delete trap;
				}
			}
			else if(el.OID.find("genstsSeverity")!=string::npos){
				pMib->scalarObj.genstsSeverity()=value.getInteger();
			}
			else if(el.OID.find("genstsFlags")!=string::npos){
				alarmDT13100* alarm = new alarmDT13100(value.getBits(),this->genstsAlarmsMask_New);
				if (alarm->changed(pMib->scalarObj.genstsFlags(),this->genstsAlarmsMask_Old)) {
					alarm->setDeviceID(pMib->scalarObj.infoDeviceID());
					alarm->setSeverity(pMib->scalarObj.genstsSeverity());
					alarm->setSourceOID(pMib->scalarObj.OID(scalar::genstsFlags_id));
					alarm->notifyAlarm();
				}
				delete alarm;
				pMib->scalarObj.genstsFlags()=value.getBits();
			}
			else if(el.OID.find("invltSeverity")!=string::npos){
				pMib->scalarObj.invltSeverity()=value.getInteger();
			}
			else if(el.OID.find("invltValue")!=string::npos){
				pMib->scalarObj.invltValue()=value.getString();
			}
			else if(el.OID.find("btcurrentValue")!=string::npos){
				pMib->scalarObj.btcurrentValue()=value.getString();
			}
			else if(el.OID.find("invltWarningUpperThreshold")!=string::npos){
				pMib->scalarObj.invltWarningUpperThreshold()=value.getString();
			}
			else if(el.OID.find("invltErrorUpperThreshold")!=string::npos){
				pMib->scalarObj.invltErrorUpperThreshold()=value.getString();
			}
			else if(el.OID.find("invltWarningLowerThreshold")!=string::npos){
				pMib->scalarObj.invltWarningLowerThreshold()=value.getString();
			}
			else if(el.OID.find("invltErrorLowerThreshold")!=string::npos){
				pMib->scalarObj.invltErrorLowerThreshold()=value.getString();
			}
			else if(el.OID.find("b1vltSeverity")!=string::npos){
				pMib->scalarObj.b1vltSeverity()=value.getInteger();
			}
			else if(el.OID.find("b1vltValue")!=string::npos){
				pMib->scalarObj.b1vltValue()=value.getString();
			}
			else if(el.OID.find("b2vltSeverity")!=string::npos){
				pMib->scalarObj.b2vltSeverity()=value.getInteger();
			}
			else if(el.OID.find("b2vltValue")!=string::npos){
				pMib->scalarObj.b2vltValue()=value.getString();
			}
			else if(el.OID.find("bvltWarningUpperThreshold")!=string::npos){
				pMib->scalarObj.bvltWarningUpperThreshold()=value.getString();
			}
			else if(el.OID.find("bvltErrorUpperThreshold")!=string::npos){
				pMib->scalarObj.bvltErrorUpperThreshold()=value.getString();
			}
			else if(el.OID.find("bvltWarningLowerThreshold")!=string::npos){
				pMib->scalarObj.bvltWarningLowerThreshold()=value.getString();
			}
			else if(el.OID.find("bvltErrorLowerThreshold")!=string::npos){
				pMib->scalarObj.bvltErrorLowerThreshold()=value.getString();
			}
			else if(el.OID.find("btcurrentValue")!=string::npos){
				pMib->scalarObj.btcurrentValue()=value.getString();
			}
			else if(el.OID.find("tempSeverity")!=string::npos){
				pMib->scalarObj.tempSeverity()=value.getInteger();
			}
			else if(el.OID.find("tempValue")!=string::npos){
				pMib->scalarObj.tempValue()=value.getString();
			}
			else if(el.OID.find("tempWarningUpperThreshold")!=string::npos){
				pMib->scalarObj.tempWarningUpperThreshold()=value.getString();
			}
			else if(el.OID.find("tempErrorUpperThreshold")!=string::npos){
				pMib->scalarObj.tempErrorUpperThreshold()=value.getString();
			}
			else if(el.OID.find("tempWarningLowerThreshold")!=string::npos){
				pMib->scalarObj.tempWarningLowerThreshold()=value.getString();
			}
			else if(el.OID.find("tempErrorLowerThreshold")!=string::npos){
				pMib->scalarObj.tempErrorLowerThreshold()=value.getString();
			}
			else if(el.OID.find("infoFirmwareVersion")!=string::npos){
				pMib->scalarObj.infoFirmwareVersion()=value.getString();
			}
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : set mib parameter (%s - %d).\n", el.OID.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when writing mib values.\n", el.OID.c_str());
		}
		
	}

};

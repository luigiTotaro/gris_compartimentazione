/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * mibs.h - Modulo implementazione mibs in base al tipo periferica.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 11/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef MIBS_H
#define MIBS_H

#include <string>

#include "srvconfig.h"
#include "diagAIMCTS0.h"
#include "diagAIMCTSN.h"
#include "diagDT13000.h"
#include "diagDT13100.h"
#include "diagTA74000.h"

using namespace std;

namespace subagent {

	/**
	 *  Classe gestione Mibs in base alla configurazione del servizio
	 */
	class MIBs {

		typedef enum {
			DEVICE_UNKNOWN ,
			DEVICE_AIMCTS0 ,
			DEVICE_AIMCTSN ,
			DEVICE_DT13000 ,
			DEVICE_DT13100 ,
			DEVICE_TA74000
		} device_t;

	private:
		diagAIMCTS0*  pAIMCTS0;
		diagAIMCTSN*  pAIMCTSN;
		diagDT13000*  pDT13000;
		diagDT13100*  pDT13100;
		diagTA74000*  pTA74000;

		bool AIMCTS0_ThreadCrashed;
		bool AIMCTSN_ThreadCrashed;
		bool DT13100_ThreadCrashed;
		bool DT13000_ThreadCrashed;
		bool TA74000_ThreadCrashed;
		bool UpdateRegKeyFailed;
		
	public:
		MIBs  (ServiceConfig* Config);
		~MIBs (void);

		bool threadsRunning (void);

	private:
		device_t deviceType     (string& type);
		void     deviceLogError (DevConfig* pdev, string msg);
		void     deviceLogInfo  (DevConfig* pdev, string msg);

	};
}

#endif
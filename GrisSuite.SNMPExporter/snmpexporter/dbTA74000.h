/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbTA74000.h - Modulo acquisizione dati periferica TA74000 da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

#include <SQLAPI.h> // main SQLAPI++ header
#include "hwTA74000.h"
#include "diagDEVICE.h"
#include "reference.h"

using namespace std;
using namespace mib_hwTA74000;


namespace subagent{


	class dbTA74000 : public reference {
		hwTA74000* pMib;
		DBConfig*  dbConfig;

		ElencoMIB generalInfo;
		ElencoMIB severityInfo;
		ElencoMIB streamInfo;
		ElencoMIB nodesInfo;

		int32_tt  severity;
		bits      locnodeAlarmsMask_New;
		bits      locnodeAlarmsMask_Old;
		bits      nodesAlarmsMask_New;
		bits      nodesAlarmsMask_Old;

	public:
		// Costruttore istanza
		dbTA74000 (DBConfig* dbcfg, DevConfig* devcfg);
		// Distruttore istanza
		~dbTA74000 (void);
	
		void maxSeverity (int32_tt severity);
		void setMibFlags (bits& flags);
		void updateMIB   (void);

		void sendToMIB(ElementoMIB &el, int row, Conversion& value);
	};

}
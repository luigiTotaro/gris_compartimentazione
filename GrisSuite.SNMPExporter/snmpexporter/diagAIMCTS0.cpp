/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportazione dati da DB STLC1000
 *
 * diagAIMCTS0.cpp - Modulo di gestione diagnostica CTS di stazione.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "diagAIMCTS0.h"
#include "alarmAIMCTS0.h"

using namespace std;
using namespace mib;
using namespace mib_hwAIMCTS0;
using namespace subagent;

namespace subagent {

	/**
	 *  Tabella oggetti mib da salvare su file
	 */
	int diagAIMCTS0::mibMaskToSave[] = {
		scalar::ctssAlarmsMask_id     ,
		scalar::cpustsAlarmsMask_id   ,
		scalar::modulesAlarmsMask_id  ,
		scalar::consolesAlarmsMask_id ,
		scalar::acdcsAlarmsMask_id    ,
		scalar::dcdcsAlarmsMask_id    ,
		scalar::nullID
	};


	/**
	 *  Costruttore istanza classe diagnostica periferica AIMCTS0
	 *
	 *  @param[in]  dbcfg   dati configurazione database
	 *  @param[in]  devcfg  dati configurazione periferica
	 */
	diagAIMCTS0::diagAIMCTS0 (DBConfig* dbcfg, DevConfig* devcfg) : diagDEVICE (devcfg)
	{
		// Carica funzione di gestione scrittura oggetti mib
		this->pMib->scalarObj.SetCallback(mibObjectWritten, this);
		this->pMib->trapsEnaTableObj.SetRowAction(mibTrapsControl, this);

		// Attiva gestione dati mib
		this->pMib->SetRunning();

		// Crea istanza classe di consultazione dati DB
		this->pDatabase = new dbAIMCTS0(dbcfg, devcfg);

		this->threadStart();
	}

	/**
	 *  Distruttore istanza
	 */
	diagAIMCTS0::~diagAIMCTS0 (void)
	{
		delete this->pDatabase;
	}

	/**
	 *  Metodo gestione aggiornamento oggetti mib
	 */
	void diagAIMCTS0::Update (void)
	{
		this->pDatabase->dbLock();

		if (this->threadRunning()) {
			this->pDatabase->updateMIB();
		}

		this->pDatabase->dbUnlock();
	}

	/**
	 *  Metodo di gestione post-scrittura parametri scalari
	 *
	 *  @param[in]  id   identificativo oggetto scalare scritto
	 *  @param[in]  obj  istanza della classe che opera sulla mib
	 *
	 *  @ereturn    true se scrittura valida.
	 */
	static bool mibObjectWritten (mib_hwAIMCTS0::scalar::scalar_id_t id, void* obj)
	{
		diagAIMCTS0* This = static_cast<diagAIMCTS0*>(obj);

		try {
			switch (id) {
				case scalar::devCommand_id:  return This->mibDeviceCommand();
				default:                     return true;
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when writing mib object.\n", This->deviceType.c_str());
		}
		return false;
	}

	/**
	 *  Metodo di gestione manipolazione righe tabella abilitazione traps.
	 *
	 *  @param[in]  row  riga tabella su cui agire
	 *  @param[in]  act  azione eseguita
	 *  @param[in]  obj  istanza della classe che opera sulla mib
	 *
	 *  @ereturn    true se riga trattata correttamente.
	 */
	static bool mibTrapsControl (netsnmp_tdata_row* row, RowAction act, void* obj)
	{
		diagAIMCTS0* This = static_cast<diagAIMCTS0*>(obj);

		try {
			int32_tt count = This->pMib->scalarObj.trapsNumber().get();

			switch (act) {
				case ROW_CHECK_TO_CREATE:
					break;

				case ROW_CREATED:
					count++;
					This->pMib->scalarObj.trapsNumber().set(count);
					break;

				case ROW_CHECK_TO_DESTROY:
					if (count <= 0)  return false;
					break;

				case ROW_DESTROYED:
					count--;
					This->pMib->scalarObj.trapsNumber().set(count);
					break;

				default:
					return false;
			}
			return true;
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when creating row in table enabled traps.\n", This->deviceType.c_str());
		}
		return false;
	}
}



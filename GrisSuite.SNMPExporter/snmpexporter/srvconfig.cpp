/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * polling.cpp - Implementazione classe gestione configurazione servizio.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 09/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include <iostream>
#include <sstream>
#include <string>

#include "net-snmp/net-snmp-config.h"
#include "net-snmp/library/snmp_logging.h"

#include "XMLInterface.h"
#include "XMLParser.h"
#include "appinfo.h"
#include "srvconfig.h"


namespace subagent {

	DBConfig::DBConfig (void)
	{
		this->name       = "";
		this->type       = "";
		this->version    = "";
		this->host       = "";
		this->user       = "";
		this->password   = "";
		this->hostname   = "";
		this->persistent = false;
	}

	void DBConfig::xmlLoadConfig (void* source)
	{
		try {
			XML_ELEMENT* db_element   = NULL;
			XML_ELEMENT* conn_element = NULL;
			XML_ELEMENT* sch_element  = NULL;

			if ((db_element=XMLGetFirst((XML_SOURCE*)source,"database")) == NULL) {
				snmp_log(LOG_ERR, "XML file, database node missing.\n");
			}
			else if ((conn_element=XMLGetNext(db_element->child,"connection",-1)) == NULL) {
				snmp_log(LOG_ERR, "XML file, connection node missing.\n");
			}
			else if ((sch_element=XMLGetNext(conn_element->child,"schema",-1)) == NULL) {
				snmp_log(LOG_ERR, "XML file, schema node missing.\n");
			}
			else {
				this->name       = XMLGetValue     (conn_element, "name"       );
				this->type       = XMLGetValue     (conn_element, "type"       );
				this->version    = XMLGetValue     (conn_element, "version"    );
				this->host       = XMLGetValue     (conn_element, "host"       );
				this->user       = XMLGetValue     (conn_element, "user"       );
				this->password   = XMLGetValue     (conn_element, "password"   );
				this->hostname   = XMLGetValue     (sch_element , "name"       );
				this->persistent = XMLGetValueBool (conn_element, "persistent" );
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load database configuration failed (critical error).\n");
		}
	}

	DevConfig::DevConfig (void)
	{
		this->clear();
	}

	void DevConfig::clear (void)
	{
		this->region.clear();
		this->zone.clear();
		this->node.clear();
		this->device.clear();

		this->dbID         = "";
		this->type         = "";
		this->enabled      = false;
		this->polling_time = 0;
	}

	void DevConfig::xmlLoadRegion (void* source)
	{
		try {
			XML_ELEMENT* element = (XML_ELEMENT*)source;
			char*        s;

			this->region.clear();

			this->region.id      = XMLGetValueUInt32 (element, "RegID");
			this->region.enabled = XMLGetValueBool   (element, "enabled");
			if ((s=XMLGetValue(element, "name")) != NULL)
				this->region.name = s;
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load region information failed (critical error).\n");
		}
	}

	void DevConfig::xmlLoadZone (void* source)
	{
		try {
			XML_ELEMENT* element = (XML_ELEMENT*)source;
			char*        s;

			this->zone.clear();

			this->zone.id      = XMLGetValueUInt32 (element, "ZonID");
			this->zone.enabled = XMLGetValueBool   (element, "enabled");
			if ((s=XMLGetValue(element, "name")) != NULL)
				this->zone.name = s;
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load zone information failed (critical error).\n");
		}
	}

	void DevConfig::xmlLoadNode (void* source)
	{
		try {
			XML_ELEMENT* element = (XML_ELEMENT*)source;
			char*        s;

			this->node.clear();

			this->node.id      = XMLGetValueUInt32 (element, "NodID");
			this->node.enabled = XMLGetValueBool   (element, "enabled");
			if ((s=XMLGetValue(element, "name")) != NULL)
				this->node.name = s;
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load node information failed (critical error).\n");
		}
	}

	void DevConfig::xmlLoadDevice (void* source)
	{
		try {
			XML_ELEMENT* element = (XML_ELEMENT*)source;
			char*        s;

			this->device.clear();

			this->device.id      = XMLGetValueUInt32 (element, "DevID");
			this->device.enabled = XMLGetValueBool   (element, "enabled");
			if ((s=XMLGetValue(element, "name")) != NULL)
				this->device.name = s;

			this->type         = XMLGetValue       ((XML_ELEMENT*)source, "type"         );
			this->polling_time = XMLGetValueUInt32 ((XML_ELEMENT*)source, "polling_time" );

			this->dbIdentifier();
			this->enabled = this->region.enabled && this->zone.enabled && this->node.enabled && this->device.enabled;
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load device configuration failed (critical error).\n");
		}
	}

    /// <summary>
    /// Calcola l'identificativo della periferica per accedere al database.
    /// </summary>
    void DevConfig::dbIdentifier (void)
	{
	#define ulong unsigned __int64

		ulong id = (ulong)(this->region.id | ((ulong)this->zone.id << 16) | ((ulong)this->node.id << 32) | ((ulong)this->device.id << 48));

		ostringstream oss;
		oss << this->region.id << "." << this->zone.id << "." << this->node.id << "." << this->device.id;
		this->path = oss.str();
		oss.seekp(ios_base::beg);

		oss << id;
		this->dbID = oss.str();
    }

	ServiceConfig::ServiceConfig (void)
	{
		this->xmlFileName = app_config_path + "\\SNMPExporterConfig.xml";

		this->softName    = "";
		this->softVersion = "";
	}

	ServiceConfig::~ServiceConfig (void)
	{
		if (!this->devConfigData.empty()) {
			this->devConfigData.clear();
		}
	}

	void ServiceConfig::xmlLoadConfig (void)
	{
		XML_SOURCE* cfg_source = NULL;
		int         ret_code;

		try {
			// Inizializza modulo XML e cestino
			XMLInterfaceInit();

			// Crea, apre e legge file XML
			if ((cfg_source=XMLCreate((char*)xmlFileName.c_str())) == NULL) {
				snmp_log(LOG_ERR, "Create XML structure failed.\n");
			}
			else if ((ret_code=XMLOpen(cfg_source,NULL,'R')) != XML_NO_ERROR) {
				snmp_log(LOG_ERR, "Open XML file failed (%d).\n", ret_code);
			}
			else if ((ret_code=XMLRead(cfg_source, NULL)) != XML_NO_ERROR) {
				snmp_log(LOG_ERR, "Read XML file failed (%d).\n", ret_code);
			}
			else {
				// Carica dati di configurazione
				xmlLoadInfoConfig(cfg_source);
				xmlLoadDevicesConfig(cfg_source);
				dbConfig.xmlLoadConfig(cfg_source);
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load service configuration failed (critical error).\n");
		}

		// Chiude e disalloca sorgente XML
		if (cfg_source) {
			XMLClose(cfg_source);
			XMLFree(cfg_source);
		}
		// Chiude modulo e svuota cestino XML
		XMLInterfaceClear();
	}

	void ServiceConfig::xmlLoadInfoConfig (void* source)
	{
		try {
			XML_ELEMENT* element = NULL;

			if ((element=XMLGetFirst((XML_SOURCE*)source,"telefin")) == NULL) {
				snmp_log(LOG_ERR, "XML file, telefin node missing.\n");
			}
			else {
				this->softName    = XMLGetValue(element, "software");
				this->softVersion = XMLGetValue(element, "version");
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load generic information failed (critical error).\n");
		}
	}

	void ServiceConfig::xmlLoadDevicesConfig (void* source)
	{
		try {
			XML_ELEMENT* system_element = NULL;
			XML_ELEMENT* region_element = NULL;
			XML_ELEMENT* zone_element   = NULL;
			XML_ELEMENT* node_element   = NULL;
			XML_ELEMENT* device_element = NULL;

			DevConfig  device;

			if ((system_element=XMLGetFirst((XML_SOURCE*)source,"system")) == NULL) {
				snmp_log(LOG_ERR, "XML file, system node missing.\n");
			}
			else {
				// cicla tutte le regioni
				for (region_element=XMLGetChild(system_element,"region"); region_element != NULL; region_element=region_element->next) { //region_element=XMLGetNext(region_element,"region",-1)) {
					device.xmlLoadRegion(region_element);

					for (zone_element=XMLGetChild(region_element,"zone"); zone_element != NULL; zone_element=zone_element->next) { //zone_element=XMLGetNext(zone_element,"zone",-1)) {
						device.xmlLoadZone(zone_element);

						for (node_element=XMLGetChild(zone_element,"node"); node_element != NULL; node_element=node_element->next) { //node_element=XMLGetNext(node_element,"node",-1)) {
							device.xmlLoadNode(node_element);

							for (device_element=XMLGetChild(node_element,"device"); device_element != NULL; device_element=device_element->next) { //device_element=XMLGetNext(device_element,"device",-1)) {
								device.xmlLoadDevice(device_element);

								this->devConfigData.push_back(device);
							}
						}
					}
				}
			}
		}
		catch (...) {
			snmp_log(LOG_ERR, "XML file: load generic information failed (critical error).\n");
		}
	}

}
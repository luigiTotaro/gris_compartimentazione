/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbTA74000.cpp - Modulo acquisizione dati periferica TA74000 da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

/*
USE Telefin;
SELECT * FROM stream_fields WHERE DevID=284249597739009 AND StrID=2 AND FieldID=2 ORDER BY ArrayID;
*/

#include <sstream>
#include <iostream>
#include <iomanip>

#include "alarmTA74000.h"
#include "dbTA74000.h"

using namespace std; 
using namespace subagent;



namespace subagent{

	dbTA74000::dbTA74000 (DBConfig* dbcfg, DevConfig* devcfg){

		string DevID = devcfg->getID();
		int i;

		this->pMib     = hwTA74000::Instance();
		this->dbConfig = dbcfg;

		// Copia la maschera degli allarmi attivi, copia necessaria per gestire le variazione della maschera
		this->locnodeAlarmsMask_New = this->pMib->scalarObj.locnodeAlarmsMask();
		this->nodesAlarmsMask_New   = this->pMib->scalarObj.nodesAlarmsMask();

		// Query informazioni generali
		generalInfo.sqlQuery    = "SELECT devices.Name as DeviceName, nodes.Name as NodeName, devices.Addr as Addr FROM devices, nodes WHERE devices.DevID=" + DevID + " AND devices.NodID=nodes.NodID";
		generalInfo.n_elementi  = 3;
		generalInfo.elementi    = new ElementoMIB[generalInfo.n_elementi];

		i = 0;
		generalInfo.elementi[i].OID			= "infoDeviceName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter = "DeviceName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoStationName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "NodeName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoDeviceID.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "Addr";
		generalInfo.elementi[i].Operation	= OPERATION_INTEGER;

		// Query severit� periferica
		severityInfo.sqlQuery   = "SELECT * FROM device_status WHERE DevID=" + DevID;
		severityInfo.n_elementi = 1;
		severityInfo.elementi   = new ElementoMIB[severityInfo.n_elementi];
		
		i = 0;
		severityInfo.elementi[i].OID		= "devSeverity.0";
		severityInfo.elementi[i].Type		= ASN_INTEGER;
		severityInfo.elementi[i].DBparameter= "SevLevel";
		severityInfo.elementi[i].Operation	= OPERATION_INTEGER;

		// Query dati periferica (nodo locale)
		streamInfo.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND (StrID=1 OR StrID=3) ORDER BY StrID, FieldID, ArrayID";
		streamInfo.n_elementi	= 13;
		streamInfo.elementi		= new ElementoMIB[streamInfo.n_elementi];

		i = 0;
		streamInfo.elementi[i].OID			= "locnodeID.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine1ID.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine1Status.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine2ID.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine2Status.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine3ID.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "2";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine3Status.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "2";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine4ID.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "3";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];

		i++;
		streamInfo.elementi[i].OID			= "locnodeLine4Status.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "2";
		streamInfo.elementi[i].ArrayID		= "3";
		streamInfo.elementi[i].Operation	= OPERATION_UINT16CONVERSION;
		streamInfo.elementi[i].DataLen      = 1;
		streamInfo.elementi[i].Data			= new ConversionData[1];
		streamInfo.elementi[i].Data[0].Position = 2;

		i++;
		streamInfo.elementi[i].OID			= "locnodeSeverity.0";
		streamInfo.elementi[i].Type			= ASN_INTEGER;
		streamInfo.elementi[i].DBparameter	= "SevLevel";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "6";
		streamInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i++;
		streamInfo.elementi[i].OID			= "locnodeFlags.0";
		streamInfo.elementi[i].Type			= ASN_BIT_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "1";
		streamInfo.elementi[i].FieldID		= "6";
		streamInfo.elementi[i].Operation	= OPERATION_BUFCONVERSION;
		streamInfo.elementi[i].DataLen      = 2;
		streamInfo.elementi[i].Data			= new ConversionData[2];
		streamInfo.elementi[i].Data[0].Position = 2;
		streamInfo.elementi[i].Data[0].Length   = 2;
		streamInfo.elementi[i].Data[0].Mask     = 0x3F;
		streamInfo.elementi[i].Data[1].Position = 4;
		streamInfo.elementi[i].Data[1].Length   = 2;
		streamInfo.elementi[i].Data[1].Mask     = 0xFF;

		i++;
		streamInfo.elementi[i].OID			= "infoFirmwareCode.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "3";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		streamInfo.elementi[i].OID			= "infoFirmwareVersion.0";
		streamInfo.elementi[i].Type			= ASN_OCTET_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "3";
		streamInfo.elementi[i].FieldID		= "1";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;


		// Query dati periferica (nodo locale)
		nodesInfo.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=2 AND FieldID=0 ORDER BY ArrayID";
		nodesInfo.fltQuery.DBparameter    = "Value";
		nodesInfo.fltQuery.Position       = 36;
		nodesInfo.fltQuery.Length         = 1;
		nodesInfo.fltQuery.ParameterValue = "1,2,3,4,5,6,7,8,9,A,B,C,D,E,F";

		nodesInfo.n_elementi	= 11;
		nodesInfo.elementi		= new ElementoMIB[nodesInfo.n_elementi];

		i = 0;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeID";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeSeverity";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "SevLevel";
		nodesInfo.elementi[i].Operation	    = OPERATION_INTEGER;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine1ID";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 4;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine1Status";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 6;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine2ID";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 8;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine2Status";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 10;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine3ID";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 12;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine3Status";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 14;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine4ID";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 16;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeLine4Status";
		nodesInfo.elementi[i].Type			= ASN_INTEGER;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_UINT16CONVERSION;
		nodesInfo.elementi[i].DataLen       = 1;
		nodesInfo.elementi[i].Data			= new ConversionData[1];
		nodesInfo.elementi[i].Data[0].Position = 18;

		i++;
		nodesInfo.elementi[i].OID			= "nodesTable.nodeFlags";
		nodesInfo.elementi[i].Type			= ASN_BIT_STR;
		nodesInfo.elementi[i].DBparameter	= "Value";
		nodesInfo.elementi[i].Operation	    = OPERATION_BUFCONVERSION;
		nodesInfo.elementi[i].DataLen       = 2;
		nodesInfo.elementi[i].Data			= new ConversionData[2];
		nodesInfo.elementi[i].Data[0].Position = 36;
		nodesInfo.elementi[i].Data[0].Length   = 2;
		nodesInfo.elementi[i].Data[0].Mask     = 0x3F;
		nodesInfo.elementi[i].Data[1].Position = 38;
		nodesInfo.elementi[i].Data[1].Length   = 2;
		nodesInfo.elementi[i].Data[1].Mask     = 0xFF;
	}
		 
	dbTA74000::~dbTA74000 (void)
	{
	}

	/**
	 *  Calcola la massima severit�
	 */
	void dbTA74000::maxSeverity (int32_tt severity)
	{
		switch (this->severity) {
			case severityUNKNOWN:
			case severityNORMAL:
				switch (severity) {
					case severityUNKNOWN: this->severity = severityUNKNOWN;  break;
					case severityWARNING: this->severity = severityWARNING;  break;
					case severityERROR  : this->severity = severityERROR;    break;
				}
				break;

			case severityWARNING:
				if (severity == severityERROR)
					this->severity = severityERROR;
				break;

			case severityERROR:
				break;
		}
	}

	/**
	 *  Imposta i flags di errore che sono stati aggiunti a livello di mibs per gestire dipendenze
	 *
	 *  @param[in/out]  flags  puntatore a flags periferica
	 */
	void dbTA74000::setMibFlags (bits &flags)
	{
		// Gestione segnalazione di errore lato EAST
		if (!flags.get(devEastSideDisabled)) {
			if (!flags.get(devEastSideActive))  flags.set(devEastSideError);
			else                                flags.clr(devEastSideError);

			if (!flags.get(devRxEastSide))  flags.set(devRxEastSideError);
			else                            flags.clr(devRxEastSideError);
		}
		else {
			flags.clr(devEastSideError);
			flags.clr(devRxEastSideError);
		}

		// Gestione segnalazione di errore lato WEST
		if (!flags.get(devWestSideDisabled)) {
			if (!flags.get(devWestSideActive))  flags.set(devWestSideError);
			else                                flags.clr(devWestSideError);

			if (!flags.get(devRxWestSide))  flags.set(devRxWestSideError);
			else                            flags.clr(devRxWestSideError);
		}
		else {
			flags.clr(devWestSideError);
			flags.clr(devRxWestSideError);
		}

		// Gestione segnalazione di errore del LOOP
		if (!flags.get(devLoopActive) && flags.get(devLoopEnabled))  flags.set(devLoopError);
		else                                                         flags.clr(devLoopError);

		// Inversione bit di stato tensioni
		flags.chg(devM12TensionError);
		flags.chg(devP12TensionError);
	}

	void dbTA74000::updateMIB (void)
	{
		netsnmp_tdata_row* pRow;
		int32_tt           number;

		this->locnodeAlarmsMask_Old = this->locnodeAlarmsMask_New;
		this->locnodeAlarmsMask_New = this->pMib->scalarObj.locnodeAlarmsMask();

		this->nodesAlarmsMask_Old = this->nodesAlarmsMask_New;
		this->nodesAlarmsMask_New = this->pMib->scalarObj.nodesAlarmsMask();

		this->DBconnect(this->dbConfig);
		this->sql_value(this->generalInfo, true);
		this->sql_value(this->severityInfo);
		this->sql_value(this->streamInfo);

		this->sql_value(this->nodesInfo);
		for(this->severity=severityNORMAL,pRow=pMib->nodesTableObj.NextRow(NULL),number=0; pRow != NULL; pRow=pMib->nodesTableObj.NextRow(pRow),number++) {
			this->maxSeverity(pMib->nodesTableObj.Entry(pRow).nodeSeverity.get());
		}
		pMib->scalarObj.nodesNumber().set(number);
		pMib->scalarObj.nodesSeverity().set(this->severity);
		
		this->DBdisconnect();
	}

	void dbTA74000::sendToMIB(ElementoMIB &el, int row, Conversion& value){

#ifdef DB_DEBUG
		cout << "    " << el.OID;
		switch(value.getType()){
			case VINTEGER:
				cout << " --> INT: " << value.getInteger().get() << endl;
				break;
			case VDISPLAYSTRING:
				{
				string s;
				value.getString().get(s);
				cout << " --> STR: " << s << endl;
				}
				break;
			case VBITS:
				cout << " --> BIT: ";
				for(int cb=0; cb<BITS_DATA_SIZE; cb++)
					cout << hex <<(int)value.getBits()[cb] << " ";
				cout << dec << endl;
				break;
			default:
				cout << endl;
		}
#endif
		
		try
		{
			if (el.OID.find("infoStationName")!=string::npos){
				pMib->scalarObj.infoStationName()=value.getString();
			}
			else if(el.OID.find("infoDeviceName")!=string::npos){
				pMib->scalarObj.infoDeviceName()=value.getString();
			}
			else if(el.OID.find("infoDeviceID")!=string::npos){
				pMib->scalarObj.infoDeviceID()=value.getInteger();
			}
			else if(el.OID.find("devSeverity")!=string::npos){
				if(pMib->scalarObj.devSeverity()!=value.getInteger()) {
					pMib->scalarObj.devSeverity()=value.getInteger();

					traps<hwTA74000>* trap = new traps<hwTA74000>(pMib->trapSystemSeverityKey(), pMib->trapSystemSeverityOID());
					pMib->trapSystemSeverityNotify();
					delete trap;
				}
			}
			else if(el.OID.find("infoFirmwareCode")!=string::npos){
				pMib->scalarObj.infoFirmwareCode()=value.getString();
			}
			else if(el.OID.find("infoFirmwareVersion")!=string::npos){
				pMib->scalarObj.infoFirmwareVersion()=value.getString();
			}
			else if(el.OID.find("locnodeID")!=string::npos){
				pMib->scalarObj.locnodeID()=value.getInteger();
			}
			else if(el.OID.find("locnodeSeverity")!=string::npos){
				pMib->scalarObj.locnodeSeverity()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine1ID")!=string::npos){
				pMib->scalarObj.locnodeLine1ID()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine1Status")!=string::npos){
				pMib->scalarObj.locnodeLine1Status()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine2ID")!=string::npos){
				pMib->scalarObj.locnodeLine2ID()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine2Status")!=string::npos){
				pMib->scalarObj.locnodeLine2Status()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine3ID")!=string::npos){
				pMib->scalarObj.locnodeLine3ID()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine3Status")!=string::npos){
				pMib->scalarObj.locnodeLine3Status()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine4ID")!=string::npos){
				pMib->scalarObj.locnodeLine4ID()=value.getInteger();
			}
			else if(el.OID.find("locnodeLine4Status")!=string::npos){
				pMib->scalarObj.locnodeLine4Status()=value.getInteger();
			}
			else if(el.OID.find("locnodeFlags")!=string::npos){
				this->setMibFlags(value.getBits());
				alarmTA74000* alarm = new alarmTA74000(value.getBits(),this->locnodeAlarmsMask_New);
				if (alarm->changed(pMib->scalarObj.locnodeFlags(),this->locnodeAlarmsMask_Old)) {
					ostringstream oss;
					oss << pMib->scalarObj.infoDeviceID().get() << "." << setfill('0') << setw(2) << pMib->scalarObj.locnodeID().get();
					alarm->setDeviceID(oss.str());
					alarm->setSeverity(pMib->scalarObj.locnodeSeverity());
					alarm->setSourceOID(pMib->scalarObj.OID(scalar::locnodeFlags_id));
					alarm->notifyAlarm();
				}
				delete alarm;
				pMib->scalarObj.locnodeFlags()=value.getBits();
			}

			/* TABELLA NODI */
			if(el.OID.find("nodesTable")!=string::npos){

				netsnmp_tdata_row* pRow = pMib->nodesTableObj.Row(row);		
				nodesTable::entry_t& Entry = pMib->nodesTableObj.Entry(pRow);

				if(el.OID.find("nodeID")!=string::npos){
					Entry.nodeID=value.getInteger();
				}
				else if(el.OID.find("nodeSeverity")!=string::npos){
					Entry.nodeSeverity=value.getInteger();
				}
				else if(el.OID.find("nodeLine1ID")!=string::npos){
					Entry.nodeLine1ID=value.getInteger();
				}
				else if(el.OID.find("nodeLine1Status")!=string::npos){
					Entry.nodeLine1Status=value.getInteger();
				}
				else if(el.OID.find("nodeLine2ID")!=string::npos){
					Entry.nodeLine2ID=value.getInteger();
				}
				else if(el.OID.find("nodeLine2Status")!=string::npos){
					Entry.nodeLine2Status=value.getInteger();
				}
				else if(el.OID.find("nodeLine3ID")!=string::npos){
					Entry.nodeLine3ID=value.getInteger();
				}
				else if(el.OID.find("nodeLine3Status")!=string::npos){
					Entry.nodeLine3Status=value.getInteger();
				}
				else if(el.OID.find("nodeLine4ID")!=string::npos){
					Entry.nodeLine4ID=value.getInteger();
				}
				else if(el.OID.find("nodeLine4Status")!=string::npos){
					Entry.nodeLine4Status=value.getInteger();
				}
				else if(el.OID.find("nodeFlags")!=string::npos){
					this->setMibFlags(value.getBits());
					alarmTA74000* alarm = new alarmTA74000(value.getBits(),this->nodesAlarmsMask_New);
					if (alarm->changed(Entry.nodeFlags,this->nodesAlarmsMask_Old)) {
						ostringstream oss;
						oss << pMib->scalarObj.infoDeviceID().get() << "." << setfill('0') << setw(2) << Entry.nodeID.get();
						alarm->setDeviceID(oss.str());
						alarm->setSeverity(Entry.nodeSeverity);
						alarm->setSourceOID(pMib->nodesTableObj.OID(pRow, nodesTable::nodeFlags));
						alarm->notifyAlarm();
					}
					delete alarm;
					Entry.nodeFlags=value.getBits();
				}
			}
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : set mib parameter (%s - %d).\n", el.OID.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when writing mib values.\n", el.OID.c_str());
		}
		
	}

};

/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmTA74000.cpp - Modulo implementazione classe gestione allarmi periferica TA74000.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "alarmTA74000.h"

using namespace std;
using namespace mib;
using namespace mib_hwTA74000;

namespace subagent {

	/**
	 *  Unique IDentification per gli allarmi periferica
	 */
	uint32_tt alarmDEVICE<hwTA74000>::UID = 0; 

	/**
	 *  Costruttore istanza classe gestione allarmi periferica TA74000
	 *
	 *  @param[in]  flags  flags di segnalazione stato periferica
	 *  @param[in]  mask   maschera allarmi periferica
	 */
	alarmTA74000::alarmTA74000 (bits& flags, bits& mask) : alarmDEVICE (flags, mask)
	{
	}

}



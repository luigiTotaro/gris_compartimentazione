/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagAIMCTSN.h - Modulo di gestione diagnostica CTS di stazione.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef DIAGAIMCTSN_H
#define DIAGAIMCTSN_H

#include "hwAIMCTSN.h"
#include "dbAIMCTSN.h"
#include "srvConfig.h"
#include "diagDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwAIMCTSN;

namespace subagent {

	class diagAIMCTSN : public diagDEVICE<hwAIMCTSN> {

		dbAIMCTSN* pDatabase;

	public:
		diagAIMCTSN  (DBConfig* dbcfg, DevConfig* devcfg);
		~diagAIMCTSN (void);

		void Update (void);

	private:
		friend bool mibObjectWritten (mib_hwAIMCTSN::scalar::scalar_id_t id, void* obj);
		friend bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
	};

static bool mibObjectWritten (mib_hwAIMCTSN::scalar::scalar_id_t id, void* obj);
static bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
}

#endif
/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbDT13100.h - Modulo acquisizione dati periferica DT13100 da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

#include <SQLAPI.h> // main SQLAPI++ header
#include "hwDT13100.h"
#include "diagDEVICE.h"
#include "reference.h"

using namespace std;
using namespace mib_hwDT13100;


namespace subagent{


	class dbDT13100 : public reference {
		hwDT13100* pMib;
		DBConfig*  dbConfig;

		ElencoMIB generalInfo;
		ElencoMIB severityInfo;
		ElencoMIB streamInfo;

		int32_tt  severity;
		bits      genstsAlarmsMask_New;
		bits      genstsAlarmsMask_Old;

	public:
		// Costruttore istanza
		dbDT13100 (DBConfig* dbcfg, DevConfig* devcfg);
		// Distruttore istanza
		~dbDT13100 (void);
	
		void maxSeverity (int32_tt severity);
		void updateMIB   (void);

		void sendToMIB(ElementoMIB &el, int row, Conversion& value);
	};

}
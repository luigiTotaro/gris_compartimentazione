//==============================================================================
// Telefin SNMP Exporter Service
//------------------------------------------------------------------------------
// Utility.h    modulo di definizione funzioni di utility
//				Funzioni estratte dal modulo SYSKernel.cpp
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//				Paolo Colli (paolo.colli@telefin.it)
//------------------------------------------------------------------------------
// Version history:
// - Versione derivata da SYSKernel 0.55 del 2008
//==============================================================================

#ifndef UTILITY_H
#define UTILITY_H

#include <windows.h>

//==============================================================================
// Funzioni per la gestione del registro di Windows
//------------------------------------------------------------------------------

// Funzione per creare una nuova chiave di registro
int           SYSCreateRegKey       (  void * keybase, char * keypath );
// Funzione per ottenere l'handler di una chiave di registro
void        * SYSOpenRegKey         ( void * keybase, char * keypath, bool write_enable  );
// Funzione per chiudere un handler di una chiave di registro
int           SYSCloseRegKey        ( void * keyhandler );
// Funzione per eliminare una chiave di registro
int           SYSDeleteRegKey       ( void * keyhandler );
// Funzione per impostare un valore in una chiave di registro
int           SYSSetRegValue        ( void * keyhandler, char * name, void * value, int type, int size );
// Funzione per ottenere un valore di una chiave di registro
void        * SYSGetRegValue        ( void * keyhandler, char * name );

// Funzione di aggiornamento segnale di vita su chiave di registro
int	UpdateLastAutoCheck( void );

#endif
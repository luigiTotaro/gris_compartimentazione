/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* Usa Net-SNMP 5.6.1.1
*
* app_info.h - Modulo di definizione informazioni generali applicazione.
*
* @author Enrico Alborali, Paolo Colli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

// Inclusioni standard
#include <stdafx.h>
#include <string>

using namespace std;

// Variabili globali applicazione
extern const string	app_vendor;
extern const string	app_web;
extern const string	app_email;
extern const string	app_name;
extern const string	app_code;
extern const string	app_version;
extern const string	app_datetime;

extern const string	mib_subagent_full_version;

// Direttori file di configurazione servizio
#define CONSOLE_MODE_CONFIG_PATH	"."
#define SERVICE_MODE_CONFIG_PATH	"C:\\Program Files\\Telefin\\SNMPExporter"

extern string  app_config_path;

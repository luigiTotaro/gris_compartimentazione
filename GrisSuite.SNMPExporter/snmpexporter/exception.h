/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * exception.h - Modulo di gestione eccezioni.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>

using namespace std;


namespace subagent {

	/*!
	@class
	@abstract    Segnalazione tipo eccezione
	@discussion  Segnala gli estremi dell'eccezione occorsa tramite descrizione e codice.
	 */
	class applyException {
		string sDecription;
		int iCode;

	public:
		applyException();
		applyException(string description, int code);

		string& Description();
		int Code();

		/*!
		 @enum 
		 @abstract   Codici eccezioni trattare dall'applicazione
		 @discussion 
		 */
		enum Code {
			SYS_NO_ERROR,
			SYS_GENERIC_ERROR,
			SYS_CREATE_MIB_ISTANCE_FAIL,
			SYS_OPEN_CONFIG_FILE_FAIL,
			SYS_READ_CONFIG_FILE_FAIL,
			SYS_WRITE_CONFIG_FILE_FAIL,
			SYS_QSSE_INVALID_HANDLE,
			SYS_QSSE_ACCESS_DENIED,
			SYS_QSSE_INSUFFICIENT_BUFFER,
			SYS_QSSE_INVALID_PARAMETER,
			SYS_QSSE_INVALID_LEVEL,
			SYS_QSSE_SHUTDOWN_IN_PROGRESS,
			SYS_SERVICE_QUERY_FAILURE,
			SYS_INVALID_THREAD_HANDLE,
			SYS_THREAD_WAIT_FAILED,
			SYS_THREAD_WAIT_TIMEOUT,
			SYS_CRITICAL_ERROR,
			SYS_REGKEY_WRITE_FAILURE,
			SYS_INVALID_REGKEY_PATH,
			SYS_REGKEY_DELETE_FAILURE,
			SYS_REGKEY_CLOSE_FAILURE,
			SYS_INVALID_REGKEY_HANDLER,
			SYS_REGKEY_SETVALUE_FAILURE,
			SYS_INVALID_REGKEY_VALUE,
			SYS_INVALID_REGKEY_VALUENAME
		};
	};

}

#endif
/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* reference.h - Modulo acquisizione dati da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

#ifndef REFERENCE_H
#define REFERENCE_H

#include <SQLAPI.h> // main SQLAPI++ header
#include "mib_objects.h"
#include "diagDEVICE.h"

using namespace std;
using namespace mib;


namespace subagent{

	class ConversionData {
	public:
		int Position;	// Posizione del carattere nella stringa
		int Length;		// Numero caratteri da convertire
		int Mask;		// Maschera da applicare
		int Factor;		// Fattore di moltiplicazione per eseguire lo shift dei bit

		ConversionData (void)
		{
			this->Position = 0;
			this->Length   = 2;
			this->Mask     = 0;
			this->Factor   = 1;
		}
	};

	class Filter {
	public:
		string DBparameter;     // Parametro su cui operare il filtro
		int    Position;	    // Posizione del carattere nella stringa
		int    Length;		    // Numero caratteri da convertire
		string ParameterValue;  // Valori ammessi 

		Filter (void)
		{
			this->DBparameter = "";
			this->Position = 0;
			this->Length   = 2;
			this->ParameterValue = "";
		}
	};

	class ElementoMIB {
	public:
		// Object ID del parametro
		string OID;		
		// Tipo di dato
		int Type;
		// Colonna della tabella contenente il dato
		string DBparameter;
		// Parametri di ricerca nella tabella
		string StrID;
		string FieldID;
		string ArrayID;

		// Operazione da eseguire sul risultato
		#define OPERATION_NONE					0
		#define OPERATION_INTEGER				1
		#define OPERATION_BUFCONVERSION			2
		#define OPERATION_UINT16CONVERSION		3
		#define OPERATION_INT16CONVERSION		4
		#define OPERATION_SLOT					5

		int Operation;

		// Numero char risultato (per padding)
		int N_ByteRes;
		// Numero di elementi di conversione
		int DataLen;
		// Informazioni necessarie alla conversione
		ConversionData* Data;
		// tabella univoca di appartenenza
		string Table;

		ElementoMIB(void){			
			this->N_ByteRes  = 0;
			this->DataLen    = 0;
			this->Data       = NULL;
			this->Table      = "";
		}

		~ElementoMIB(void){
			if (this->Data)  delete [] this->Data;
		}
	};

	class ElencoMIB {
	public:
		// Query SQL per ottenere le informazioni
		string sqlQuery;
		// Filtro di selezione righe
		Filter fltQuery;
		// Numero di elementi dell'array
		int n_elementi;
		// Array di elementi MIB
		ElementoMIB* elementi;

		ElencoMIB(void)
		{
			this->elementi = NULL;
		}

		~ElencoMIB(void){
			if (this->elementi)  delete [] this->elementi;
		}
	};


	class bvalue {
	public:
		int        size;
		uint8_tt*  pointer;

		bvalue(int size){
			this->size    = 0;
			this->pointer = NULL;
			if (size > 0) {
				this->size = size;
				this->pointer = new uint8_tt[this->size];
			}
		}
		~bvalue(){
			delete [] this->pointer;
		}
		void append(int size){
			if (size > 0) {
				if (this->size) {
					uint8_tt* pdata = this->pointer;

					this->pointer = new uint8_tt[this->size + size];
					memcpy(this->pointer, pdata, this->size);
					this->size += size;

					delete [] pdata;
				}
				else {
					this->size = size;
					this->pointer = new uint8_tt[this->size];
				}
			}
		}
	};


	class Conversion {
	public:
		// Tipo valore ottenuto dalla conversione
		#define VINTEGER		1
		#define VDISPLAYSTRING	2
		#define VBITS			3

	private:
		string   strValue;
		bvalue*  bufValue;
		int		 intValue;

		int            type;
		integer*       pInteger;
		displaystring* pString;
		bits*          pBits;

	public:
		Conversion (void);
		Conversion (const char* value);
		~Conversion (void);

		void convert (ElementoMIB& el);
		void str2hex (string& s, int m, int f);
		void str2int (string& s, int m, int f);

		int            getType    (void);
		integer&       getInteger (void);
		displaystring& getString  (void);
		bits&          getBits    (void);
	};

	
	class reference {

	private:

		DBConfig*     dbcfg;
		SAConnection  con;

	public:
		// Costruttore istanza
		reference (void);
		// Distruttore istanza
		~reference (void);

		void dbLock   (void);
		void dbUnlock (void);

		bool DBconnect    (DBConfig* dbcfg);
		bool DBdisconnect (void);

		void sql_value(ElencoMIB& el, bool logerr = false);
		bool sql_filter(ElencoMIB& el, SACommand& cmd);
		bool isRightRow(ElementoMIB& element, SACommand& cmd);

		virtual void sendToMIB(ElementoMIB &el, int index, Conversion& value) = 0;
	};


}

#endif

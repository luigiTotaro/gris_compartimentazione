/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmDT13100.cpp - Modulo implementazione classe gestione allarmi periferica DT13100.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "alarmDT13100.h"

using namespace std;
using namespace mib;
using namespace mib_hwDT13100;

namespace subagent {

	/**
	 *  Unique IDentification per gli allarmi periferica
	 */
	uint32_tt alarmDEVICE<hwDT13100>::UID = 0; 

	/**
	 *  Costruttore istanza classe gestione allarmi periferica DT13100
	 *
	 *  @param[in]  flags  flags di segnalazione stato periferica
	 *  @param[in]  mask   maschera allarmi periferica
	 */
	alarmDT13100::alarmDT13100 (bits& flags, bits& mask) : alarmDEVICE (flags, mask)
	{
	}

}



/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmDT13100.h - Modulo implementazione classe gestione allarmi periferica DT13100.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */
#ifndef ALARMDT13100_H
#define ALARMDT13100_H

#include "hwDT13100.h"
#include "alarmDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwDT13100;

namespace subagent {

	class alarmDT13100 : public alarmDEVICE<hwDT13100> {

	public:
		alarmDT13100 (bits& flags, bits& mask);

	};
}

#endif


/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagTA74000.h - Modulo di gestione diagnostica alimentatore TA74000.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef DIAGTA74000_H
#define DIAGTA74000_H

#include "hwTA74000.h"
#include "dbTA74000.h"
#include "srvConfig.h"
#include "diagDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwTA74000;

namespace subagent {

	class diagTA74000 : public diagDEVICE<hwTA74000> {

		dbTA74000* pDatabase;

	public:
		diagTA74000  (DBConfig* dbcfg, DevConfig* devcfg);
		~diagTA74000 (void);

		void Update (void);

	private:
		friend bool mibObjectWritten (mib_hwTA74000::scalar::scalar_id_t id, void* obj);
		friend bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
	};

static bool mibObjectWritten (mib_hwTA74000::scalar::scalar_id_t id, void* obj);
static bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
}

#endif
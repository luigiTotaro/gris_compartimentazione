/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * polling.h - Implementazione classe per la gestione delle attivita periodiche.
 *
 * @author Riccardo Venturini - Paolo Colli
 * @version 1.0.0.0 11/05/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef POLLING_H
#define	POLLING_H

#include <string>
#include "thread.h"

using namespace std;

namespace subagent {

	class Polling : public Runnable {
	public:
		Polling();
		Polling(unsigned int pollingTime);

		virtual ~Polling();
		
		void setPollingTime(unsigned int pollingTime);
		unsigned int getPollingTime() const;
		
		void setPollingName(string& name);
		string& getPollingName();

		void Run();
		void start();
		void stop();
		bool check();
		bool running();
		
		virtual void Update() = 0 ;
		
		
	private:
		Thread* pollingThread;
		unsigned int pollingTime;
		
		void init(unsigned int pollingTime);
		int error();
	};

}

#endif	/* POLLING_H */


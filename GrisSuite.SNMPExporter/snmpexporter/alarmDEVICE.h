/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmDEVICE.h - Modulo implementazione classe generica di gestione allarmi periferica.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef ALARMDEVICE_H
#define ALARMDEVICE_H

#include <time.h>
#include <iostream>
#include <string>
#include <sstream>

#include "mib_objects.h"
#include "exception.h"
#include "traps.h"

using namespace std;
using namespace mib;


namespace subagent {

	/**
	 *  Classe template di gestione parte comune a tutte le periferiche
	 */
	typedef enum {
		NO_ACTION  ,
		INS_ACTION ,
		CHG_ACTION ,
		DEL_ACTION
	} action_t;

	template <typename MIBTYPE>
	class alarmDEVICE {

	protected:
		static uint32_tt    UID;	      // Unique IDentification, al reset � inizializzato a zero ed incrementato ad ogni allarme

		MIBTYPE*            pMib;         // puntatore descrittore mib
		netsnmp_tdata_row*  pRow;         // puntatore riga tabella allarmi

		action_t  action;                 // identifica l'azione da esseguire sulla tabella allarmi

		mibtm          mibtime;     // data/ora sistema nel formato mibtm

		bits           mask;        // maschera allarmi (quali flags di stato sono da considerare allarmi)
		bits           flags;       // flags di segnalazione stato periferica
		displaystring  deviceID;    // identificativo periferica
		integer        severity;    // grado di severit� allarme
		displaystring  sourceOID;   // OID sorgente che ha generato allarme

	public:

		alarmDEVICE (void);
		alarmDEVICE (bits& flags, bits& mask);
		~alarmDEVICE (void);

		bool changed      (bits& oldflg);
		bool changed      (bits& oldflg, bits& oldmsk);
		void setAlarms    (bits& flags, bits& mask);
		void setDeviceID  (displaystring& devID);
		void setDeviceID  (integer& devID);
		void setDeviceID  (string& devID);
		void setSeverity  (integer& severity);
		void setSourceOID (string& sourceOID);

		virtual bool checkItem   (void);
		virtual void notifyAlarm (void);

	private:
		void   init    (void);
		mibtm& setTime (void);

	//protected:
	public:
		void selectItem (void);
		void updateItem (void);
		void notify     (void);

	};


	/**
	 *  Inizializza oggetti al valore di default
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::init (void)
	{
		try {
			this->pMib = MIBTYPE::Instance();
			this->pRow = NULL;

			this->action = NO_ACTION;

			this->deviceID.defval();
			this->severity.defval();
			this->flags.defval();
			this->sourceOID.defval();

			this->setTime();
		}
		catch (...) {
			throw applyException("alarmDEVICE::init: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Restituisce data/ora sistema nel formato mibtm
	 *
	 *  @return  struttura mibtm
	 */
	template <typename MIBTYPE>
	mibtm& alarmDEVICE<MIBTYPE>::setTime (void)
	{
		try {
			time_t  t     = time(NULL);
			tm*     ltime = localtime(&t);

			this->mibtime.tm_year   = ltime->tm_year + 1900;
			this->mibtime.tm_mon    = ltime->tm_mon + 1;
			this->mibtime.tm_mday   = ltime->tm_mday;
			this->mibtime.tm_hour   = ltime->tm_hour;
			this->mibtime.tm_min    = ltime->tm_min;
			this->mibtime.tm_sec    = ltime->tm_sec;
			this->mibtime.tm_dsec   = 0;
			this->mibtime.tm_gmtoff = 0; 
		}
		catch (...) {
			throw applyException("alarmDEVICE::time: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
		return this->mibtime;
	}

	/**
	 *  Costruttore di default istanza della classe, inizializza oggetti al valore di default
	 */
	template <typename MIBTYPE>
	alarmDEVICE<MIBTYPE>::alarmDEVICE (void)
	{
		this->init();
		this->pMib->alarmsTableObj.lock();
	}

	/**
	 *  Costruttore istanza della classe, definisce allarmi attivi
	 *
	 *  @param[in]  flags  flags di segnalazione stato periferica
	 *  @param[in]  mask   maschera allarmi periferica
	 */
	template <typename MIBTYPE>
	alarmDEVICE<MIBTYPE>::alarmDEVICE (bits& flags, bits& mask)
	{
		this->init();
		this->setAlarms(flags, mask);
		this->pMib->alarmsTableObj.lock();
	}

	/**
	 *  Distruttore istanza
	 */
	template <typename MIBTYPE>
	alarmDEVICE<MIBTYPE>::~alarmDEVICE (void)
	{
		this->pMib->alarmsTableObj.unlock();
	}

	/**
	 *  Verifica se lo stato di allarme delle periferica � cambiato rispetto al precedente controllo
	 *
	 *  @param[in]  oldflg  precedente valore dei flags di allarme
	 *
	 *  @return     true se flags cambiati.
	 */
	template <typename MIBTYPE>
	bool alarmDEVICE<MIBTYPE>::changed (bits& oldflg)
	{
		try {
			for (int i=0; i < BITS_DATA_SIZE; i++) {
				if ((oldflg[i] & this->mask[i]) ^ this->flags[i])
					return true;
			}
		}
		catch (...) {
			throw applyException("alarmDEVICE::changes: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
		return false;
	}

	/**
	 *  Verifica se lo stato di allarme delle periferica � cambiato rispetto al precedente controllo
	 *
	 *  @param[in]  oldflg  precedente valore dei flags di allarme
	 *  @param[in]  oldmsk  precedente valore della maschera di attivazione allarmi
	 *
	 *  @return     true se flags cambiati.
	 */
	template <typename MIBTYPE>
	bool alarmDEVICE<MIBTYPE>::changed (bits& oldflg, bits& oldmsk)
	{
		try {
			for (int i=0; i < BITS_DATA_SIZE; i++) {
				if ((oldflg[i] & oldmsk[i]) ^ this->flags[i])
					return true;
			}
		}
		catch (...) {
			throw applyException("alarmDEVICE::changes: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
		return false;
	}

	/**
	 *  Imposta allarmi attivi.
	 *
	 *  @param[in]  flags  flags di segnalazione stato periferica
	 *  @param[in]  mask   maschera allarmi periferica
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::setAlarms (bits& flags, bits& mask)
	{
		try {
			this->flags = flags;
			this->mask  = mask;

			// Applica la maschera degli allarmi e verifica se vi � almeno un allarme attivo
			this->action = DEL_ACTION;
			for (int i=0; i < BITS_DATA_SIZE; i++) {
				this->flags[i] &= this->mask[i];
				if (this->flags[i])
					this->action = INS_ACTION;
			}
		}
		catch (...) {
			throw applyException("alarmDEVICE::setAlarms: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Imposta identificativo periferica
	 *
	 *  @param[in]  devID  identificativo periferica
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::setDeviceID (displaystring& devID)
	{
		this->deviceID = devID; 
	}

	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::setDeviceID (integer& devID)
	{
		int32_tt intID = devID.get();
		ostringstream oss;
		oss << intID;
		string strID = oss.str();
		this->deviceID.set(strID);
	}

	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::setDeviceID (string& devID)
	{
		this->deviceID.set(devID);
	}

	/**
	 *  Imposta codice severit� allarme
	 *
	 *  @param[in]  severity  codice severit� allarme
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::setSeverity (integer& severity)
	{
		this->severity = severity;
	}

	/**
	 *  Imposta OID sorgente che ha generato l'allarme
	 *
	 *  @param[in]  sourceOID  OID sorgente allarme
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::setSourceOID (string& sourceOID)
	{
		this->sourceOID.set(sourceOID);
	}

	/**
	 *  Verifica corrispondenza oggetto con tabella allarmi
	 *
	 *  @return true se l'oggetto corrisponde con allarme selezionato
	 */
	template <typename MIBTYPE>
	bool alarmDEVICE<MIBTYPE>::checkItem (void)
	{
		alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);

		return this->deviceID == entry.alarmDeviceID;
	}

	/**
	 *  Seleziona la riga della tabella allarmi su cui operare.
	 *  In uscita this->pRow punta alla riga tabella
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::selectItem (void)
	{
		try {
			while (this->pRow=pMib->alarmsTableObj.NextRow(this->pRow)) {
				if (this->checkItem()) {
					// trovata riga, trasforma l'azione di inserimento in MODIFICA
					if (this->action == INS_ACTION)
						this->action = CHG_ACTION;
					return;
				}
			}
			// periferica non presente in tabella
			if (this->action == INS_ACTION) {
				// INSERIMENTO: crea riga
				this->UID++;
				this->pRow = pMib->alarmsTableObj.Row(this->UID);
				alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);
				entry.alarmDeviceID.set(this->deviceID);
			}
			else {
				// CANCELLAZIONE: annulla azione, la riga da cancellare non esiste
				this->action = NO_ACTION;
			}
		}
		catch (...) {
			throw applyException("alarmDEVICE::selectItem: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Aggiorna riga tabella con dati allarme
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::updateItem (void)
	{
		try {
			alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);

			switch (this->action) {

				case NO_ACTION:
					break;

				case INS_ACTION:
					entry.alarmDeviceID = this->deviceID;
					entry.alarmOID      = this->sourceOID;
				case CHG_ACTION:
				case DEL_ACTION:
					entry.alarmSeverity = this->severity;
					entry.alarmFlags    = this->flags;
					entry.alarmDateTime.set(this->mibtime);
					break;
			}
		}
		catch (...) {
			throw applyException("alarmDEVICE::updateItem: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Notifica allarme. Invia trap 
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::notify (void)
	{
		try {
			traps<MIBTYPE>* trap = NULL;

			switch (this->action) {

				case INS_ACTION:
					trap = new traps<MIBTYPE>(pMib->trapAlarmInsertedKey(), pMib->trapAlarmInsertedOID());
					pMib->trapAlarmInsertedNotify(this->pRow);
					delete trap;
					break;

				case CHG_ACTION:
					trap = new traps<MIBTYPE>(pMib->trapAlarmChangedKey(), pMib->trapAlarmChangedOID());
					pMib->trapAlarmChangedNotify(this->pRow);
					delete trap;
					break;

				case DEL_ACTION:
					trap = new traps<MIBTYPE>(pMib->trapAlarmDeletedKey(), pMib->trapAlarmDeletedOID());
					pMib->trapAlarmDeletedNotify(this->pRow);
					delete trap;
					// Inviata la trap elimina la riga dalla tabella allarmi
					pMib->alarmsTableObj.DelRow(this->pRow);
					break;
			}
		}
		catch (...) {
			throw applyException("alarmDEVICE::notify: critical error.", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Inserisce allarme in tabella ed invia notifica al server di diagnostica. 
	 */
	template <typename MIBTYPE>
	void alarmDEVICE<MIBTYPE>::notifyAlarm (void)
	{
		this->selectItem();
		this->updateItem();
		this->notify();
	}

}

#endif
/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * traps.cpp - Gestione trap: crea un'istanza della classe trap (apertura sezione, invio trap, chiusura sezione).
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */
#include "traps.h"
#include "hwAIMCTS0.h"
#include "hwAIMCTSN.h"
#include "hwDT13000.h"
#include "hwDT13100.h"
#include "hwTA74000.h"

namespace subagent {

	mutex traps<mib_hwAIMCTS0::hwAIMCTS0>::trapMutex;
	mutex traps<mib_hwAIMCTSN::hwAIMCTSN>::trapMutex;
	mutex traps<mib_hwDT13000::hwDT13000>::trapMutex;
	mutex traps<mib_hwDT13100::hwDT13100>::trapMutex;
	mutex traps<mib_hwTA74000::hwTA74000>::trapMutex;

}



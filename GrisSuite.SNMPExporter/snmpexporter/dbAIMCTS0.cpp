/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* dbAIMCTS0.cpp - Modulo acquisizione dati periferica AIMCTSN da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

#ifndef DBAIMCTS0_H
#define DBAIMCTS0_H

#include "alarmAIMCTS0.h"
#include "dbAIMCTS0.h"

using namespace std; 
using namespace subagent;

namespace subagent{

	dbAIMCTS0::dbAIMCTS0 (DBConfig* dbcfg, DevConfig* devcfg){

		string DevID=devcfg->getID();
		int i;
		this->pMib = hwAIMCTS0::Instance();
		this->dbConfig=dbcfg;

		// Copia la maschera degli allarmi attivi, copia necessaria per gestire le variazione della maschera
		this->ctssAlarmsMask_New     = this->pMib->scalarObj.ctssAlarmsMask();
		this->cpustsAlarmsMask_New   = this->pMib->scalarObj.cpustsAlarmsMask();
		this->modulesAlarmsMask_New  = this->pMib->scalarObj.modulesAlarmsMask();
		this->consolesAlarmsMask_New = this->pMib->scalarObj.consolesAlarmsMask();
		this->acdcsAlarmsMask_New    = this->pMib->scalarObj.acdcsAlarmsMask();
		this->dcdcsAlarmsMask_New    = this->pMib->scalarObj.dcdcsAlarmsMask();

		generalInfo.sqlQuery	= "SELECT devices.Name as DeviceName, nodes.Name as NodeName FROM devices, nodes WHERE devices.DevID=" + DevID + " AND devices.NodID=nodes.NodID";
		severityInfo.sqlQuery	= "SELECT * FROM device_status WHERE DevID=" + DevID;
		streamInfo.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=3 ORDER BY StrID, FieldID, ArrayID;";
		cpusts.sqlQuery			= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=2 AND FieldID=0 ORDER BY StrID, FieldID, ArrayID;";
		ctssTable.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=1 AND FieldID=0 ORDER BY ArrayID";
		modulesTable.sqlQuery	= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=2 AND FieldID=1 ORDER BY ArrayID";

		consolesTable.sqlQuery	= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=2 AND FieldID=2 ORDER BY ArrayID";
		consolesTable.fltQuery.DBparameter    = "Value";
		consolesTable.fltQuery.Position       = 16;
		consolesTable.fltQuery.ParameterValue = "30,31,32,33,37,38,39,3A,40";

		acdcsTable.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=2 AND FieldID=3 ORDER BY ArrayID";
		acdcsTable.fltQuery.DBparameter    = "Value";
		acdcsTable.fltQuery.Position       = 0;
		acdcsTable.fltQuery.ParameterValue = "31";

		dcdcsTable.sqlQuery		= "SELECT * FROM stream_fields WHERE DevID=" + DevID + " AND StrID=2 AND FieldID=3 ORDER BY ArrayID";
		dcdcsTable.fltQuery.DBparameter    = "Value";
		dcdcsTable.fltQuery.Position       = 0;
		dcdcsTable.fltQuery.ParameterValue = "32";

		generalInfo.n_elementi		= 2;
		severityInfo.n_elementi		= 1;
		streamInfo.n_elementi		= 2;
		cpusts.n_elementi			= 3;
		ctssTable.n_elementi		= 3;
		modulesTable.n_elementi		= 5;
		consolesTable.n_elementi	= 5;
		acdcsTable.n_elementi		= 7;
		dcdcsTable.n_elementi		= 22;

		generalInfo.elementi	= new ElementoMIB[generalInfo.n_elementi];
		severityInfo.elementi	= new ElementoMIB[severityInfo.n_elementi];
		streamInfo.elementi		= new ElementoMIB[streamInfo.n_elementi];
		cpusts.elementi			= new ElementoMIB[cpusts.n_elementi];
		modulesTable.elementi	= new ElementoMIB[modulesTable.n_elementi];
		consolesTable.elementi	= new ElementoMIB[consolesTable.n_elementi];
		ctssTable.elementi		= new ElementoMIB[ctssTable.n_elementi];
		acdcsTable.elementi		= new ElementoMIB[acdcsTable.n_elementi];
		dcdcsTable.elementi		= new ElementoMIB[dcdcsTable.n_elementi];

		i=0;
		generalInfo.elementi[i].OID			= "infoDeviceName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter = "DeviceName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i++;
		generalInfo.elementi[i].OID			= "infoStationName.0";
		generalInfo.elementi[i].Type		= ASN_OCTET_STR;
		generalInfo.elementi[i].DBparameter	= "NodeName";
		generalInfo.elementi[i].Operation	= OPERATION_NONE;

		i=0;
		severityInfo.elementi[i].OID		= "devSeverity.0";
		severityInfo.elementi[i].Type		= ASN_INTEGER;
		severityInfo.elementi[i].DBparameter= "SevLevel";
		severityInfo.elementi[i].Operation	= OPERATION_INTEGER;

		i=0;
		streamInfo.elementi[i].OID			= "infoModel.0";
		streamInfo.elementi[i].Type			= ASN_BIT_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "3";
		streamInfo.elementi[i].FieldID		= "0";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;		
		i++;
		streamInfo.elementi[i].OID			= "infoAIMVersion.0";
		streamInfo.elementi[i].Type			= ASN_BIT_STR;
		streamInfo.elementi[i].DBparameter	= "Value";
		streamInfo.elementi[i].StrID		= "3";
		streamInfo.elementi[i].FieldID		= "4";
		streamInfo.elementi[i].Operation	= OPERATION_NONE;

		i=0;
		cpusts.elementi[i].OID				= "cpustsType.0";
		cpusts.elementi[i].Type				= ASN_INTEGER;
		cpusts.elementi[i].DBparameter		= "Value";
		cpusts.elementi[i].StrID			= "2";
		cpusts.elementi[i].FieldID			= "0";
		cpusts.elementi[i].Operation		= OPERATION_UINT16CONVERSION;	
		cpusts.elementi[i].DataLen			= 1;
		cpusts.elementi[i].Data				= new ConversionData[1];
		i++;
		cpusts.elementi[i].OID				= "cpustsSeverity.0";
		cpusts.elementi[i].Type				= ASN_INTEGER;
		cpusts.elementi[i].DBparameter		= "SevLevel";
		cpusts.elementi[i].Operation		= OPERATION_INTEGER;
		i++;
		cpusts.elementi[i].OID				= "cpustsFlags.0";
		cpusts.elementi[i].Type				= ASN_BIT_STR;
		cpusts.elementi[i].DBparameter		= "Value";
		cpusts.elementi[i].StrID			= "2";
		cpusts.elementi[i].FieldID			= "0";
		cpusts.elementi[i].Operation		= OPERATION_BUFCONVERSION;
		cpusts.elementi[i].DataLen			= 1;
		cpusts.elementi[i].Data				= new ConversionData[1];
		cpusts.elementi[i].Data[0].Position	= 2;
		cpusts.elementi[i].Data[0].Length	= 6;

		// Ctss
		i=0;
		ctssTable.elementi[i].OID				= "ctssTable.ctsSeverity";
		ctssTable.elementi[i].Type				= ASN_INTEGER;
		ctssTable.elementi[i].DBparameter		= "SevLevel";
		ctssTable.elementi[i].Operation			= OPERATION_INTEGER;
		i++;
		ctssTable.elementi[i].OID				= "ctssTable.ctsNumber";
		ctssTable.elementi[i].Type				= ASN_INTEGER;
		ctssTable.elementi[i].DBparameter		= "Value";
		ctssTable.elementi[i].Operation			= OPERATION_BUFCONVERSION;
		ctssTable.elementi[i].DataLen			= 1;
		ctssTable.elementi[i].Data				= new ConversionData[1];
		ctssTable.elementi[i].Data[0].Position	= 0;	
		ctssTable.elementi[i].Data[0].Length	= 2;		
		i++;
		ctssTable.elementi[i].OID				= "ctssTable.ctsFlags";
		ctssTable.elementi[i].Type				= ASN_BIT_STR;
		ctssTable.elementi[i].DBparameter		= "Value";
		ctssTable.elementi[i].Operation			= OPERATION_BUFCONVERSION;
		ctssTable.elementi[i].DataLen			= 1;
		ctssTable.elementi[i].Data				= new ConversionData[1];
		ctssTable.elementi[i].Data[0].Position	= 2;
		ctssTable.elementi[i].Data[0].Length 	= 8;

		// Modules
		i=0;
		modulesTable.elementi[i].OID				= "modulesTable.moduleType";
		modulesTable.elementi[i].Type				= ASN_INTEGER;
		modulesTable.elementi[i].DBparameter		= "Value";
		modulesTable.elementi[i].Operation			= OPERATION_UINT16CONVERSION;
		modulesTable.elementi[i].DataLen			= 1;
		modulesTable.elementi[i].Data				= new ConversionData[1];
		modulesTable.elementi[i].Data[0].Position	= 4;
		i++;
		modulesTable.elementi[i].OID				= "modulesTable.moduleSeverity";
		modulesTable.elementi[i].Type				= ASN_INTEGER;
		modulesTable.elementi[i].DBparameter		= "SevLevel";
		modulesTable.elementi[i].Operation			= OPERATION_INTEGER;
		i++;
		modulesTable.elementi[i].OID				= "modulesTable.modulePath";
		modulesTable.elementi[i].Type				= ASN_OCTET_STR;
		modulesTable.elementi[i].DBparameter		= "Value";
		modulesTable.elementi[i].Operation			= OPERATION_SLOT;
		modulesTable.elementi[i].DataLen			= 3;
		modulesTable.elementi[i].Data				= new ConversionData[3];
		modulesTable.elementi[i].Data[0].Position	= 0;	
		modulesTable.elementi[i].Data[0].Mask		= 112;
		modulesTable.elementi[i].Data[0].Factor		= -16;
		modulesTable.elementi[i].Data[1].Position	= 0;	
		modulesTable.elementi[i].Data[1].Mask		= 15;
		modulesTable.elementi[i].Data[1].Factor		= 1;	
		modulesTable.elementi[i].Data[2].Position	= 2;	
		modulesTable.elementi[i].Data[2].Mask		= 96;
		modulesTable.elementi[i].Data[2].Factor		= -32;
		i++;
		modulesTable.elementi[i].OID				= "modulesTable.moduleSubtype";
		modulesTable.elementi[i].Type				= ASN_INTEGER;
		modulesTable.elementi[i].DBparameter		= "Value";
		modulesTable.elementi[i].Operation			= OPERATION_UINT16CONVERSION;
		modulesTable.elementi[i].DataLen			= 1;
		modulesTable.elementi[i].Data				= new ConversionData[1];
		modulesTable.elementi[i].Data[0].Position	= 2;
		modulesTable.elementi[i].Data[0].Mask		= 31;
		i++;
		modulesTable.elementi[i].OID				= "modulesTable.moduleFlags";
		modulesTable.elementi[i].Type				= ASN_BIT_STR;
		modulesTable.elementi[i].DBparameter		= "Value";
		modulesTable.elementi[i].Operation			= OPERATION_BUFCONVERSION;
		modulesTable.elementi[i].DataLen			= 1;
		modulesTable.elementi[i].Data				= new ConversionData[1];
		modulesTable.elementi[i].Data[0].Position	= 6;
		modulesTable.elementi[i].Data[0].Length 	= 6;

		// Consoles
		i=0;
		consolesTable.elementi[i].OID				= "consolesTable.consoleType";
		consolesTable.elementi[i].Type				= ASN_INTEGER;
		consolesTable.elementi[i].DBparameter		= "Value";
		consolesTable.elementi[i].Operation			= OPERATION_UINT16CONVERSION;
		consolesTable.elementi[i].DataLen			= 1;
		consolesTable.elementi[i].Data				= new ConversionData[1];
		consolesTable.elementi[i].Data[0].Position	= 16;
		i++;
		consolesTable.elementi[i].OID				= "consolesTable.consoleSeverity";
		consolesTable.elementi[i].Type				= ASN_INTEGER;
		consolesTable.elementi[i].DBparameter		= "SevLevel";
		consolesTable.elementi[i].Operation			= OPERATION_INTEGER;
		i++;
		consolesTable.elementi[i].OID				= "consolesTable.consolePath";
		consolesTable.elementi[i].Type				= ASN_OCTET_STR;
		consolesTable.elementi[i].DBparameter		= "Value";
		consolesTable.elementi[i].Operation			= OPERATION_SLOT;
		consolesTable.elementi[i].DataLen			= 3;
		consolesTable.elementi[i].Data				= new ConversionData[3];
		consolesTable.elementi[i].Data[0].Position	= 0;	
		consolesTable.elementi[i].Data[0].Mask		= 112;
		consolesTable.elementi[i].Data[0].Factor	= -16;
		consolesTable.elementi[i].Data[1].Position	= 0;	
		consolesTable.elementi[i].Data[1].Mask		= 15;
		consolesTable.elementi[i].Data[1].Factor	= 1;	
		consolesTable.elementi[i].Data[2].Position	= 2;	
		consolesTable.elementi[i].Data[2].Mask		= 96;
		consolesTable.elementi[i].Data[2].Factor	= -32;
		i++;
		consolesTable.elementi[i].OID				= "consolesTable.consoleNumber";
		consolesTable.elementi[i].Type				= ASN_OCTET_STR;
		consolesTable.elementi[i].DBparameter		= "Value";
		consolesTable.elementi[i].Operation			= OPERATION_UINT16CONVERSION;
		consolesTable.elementi[i].N_ByteRes			= 5;
		consolesTable.elementi[i].DataLen			= 3;
		consolesTable.elementi[i].Data				= new ConversionData[3];
		consolesTable.elementi[i].Data[0].Position	= 18;	
		consolesTable.elementi[i].Data[0].Mask		= 127;
		consolesTable.elementi[i].Data[0].Factor	= 1000;
		consolesTable.elementi[i].Data[1].Position	= 20;	
		consolesTable.elementi[i].Data[1].Mask		= 127;
		consolesTable.elementi[i].Data[1].Factor	= 8;
		consolesTable.elementi[i].Data[2].Position	= 22;	
		consolesTable.elementi[i].Data[2].Mask		= 112;
		consolesTable.elementi[i].Data[2].Factor	= -16;
		i++;
		consolesTable.elementi[i].OID				= "consolesTable.consoleFlags";
		consolesTable.elementi[i].Type				= ASN_BIT_STR;
		consolesTable.elementi[i].DBparameter		= "Value";
		consolesTable.elementi[i].Operation			= OPERATION_BUFCONVERSION;
		consolesTable.elementi[i].DataLen			= 1;
		consolesTable.elementi[i].Data				= new ConversionData[1];
		consolesTable.elementi[i].Data[0].Position	= 24;
		consolesTable.elementi[i].Data[0].Length	= 4;

		// ACDC
		i = 0;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcType";
		acdcsTable.elementi[i].Type				= ASN_INTEGER;
		acdcsTable.elementi[i].DBparameter		= "Value";
		acdcsTable.elementi[i].Operation		= OPERATION_UINT16CONVERSION;
		acdcsTable.elementi[i].DataLen			= 1;
		acdcsTable.elementi[i].Data				= new ConversionData[1];
		i++;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcSeverity";
		acdcsTable.elementi[i].Type				= ASN_INTEGER;
		acdcsTable.elementi[i].DBparameter		= "SevLevel";
		acdcsTable.elementi[i].Operation		= OPERATION_INTEGER;
		i++;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcID";
		acdcsTable.elementi[i].Type				= ASN_INTEGER;
		acdcsTable.elementi[i].DBparameter		= "Value";
		acdcsTable.elementi[i].Operation		= OPERATION_UINT16CONVERSION;
		acdcsTable.elementi[i].DataLen			= 1;
		acdcsTable.elementi[i].Data				= new ConversionData[1];
		acdcsTable.elementi[i].Data[0].Position	= 2;
		i++;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcFlags";
		acdcsTable.elementi[i].Type				= ASN_BIT_STR;
		acdcsTable.elementi[i].DBparameter		= "Value";
		acdcsTable.elementi[i].Operation		= OPERATION_BUFCONVERSION;
		acdcsTable.elementi[i].DataLen			= 1;
		acdcsTable.elementi[i].Data				= new ConversionData[1];
		acdcsTable.elementi[i].Data[0].Position	= 4;	
		acdcsTable.elementi[i].Data[0].Length	= 6;
		i++;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcTension";
		acdcsTable.elementi[i].Type				= ASN_INTEGER;
		acdcsTable.elementi[i].DBparameter		= "Value";
		acdcsTable.elementi[i].Operation		= OPERATION_INT16CONVERSION;
		acdcsTable.elementi[i].Table			= "acdcsTable";
		acdcsTable.elementi[i].DataLen			= 2;
		acdcsTable.elementi[i].Data				= new ConversionData[2];
		acdcsTable.elementi[i].Data[0].Position	= 12;
		acdcsTable.elementi[i].Data[0].Factor	= 256;
		acdcsTable.elementi[i].Data[1].Position	= 14;
		i++;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcCurrent";
		acdcsTable.elementi[i].Type				= ASN_INTEGER;
		acdcsTable.elementi[i].DBparameter		= "Value";
		acdcsTable.elementi[i].Operation		= OPERATION_INT16CONVERSION;
		acdcsTable.elementi[i].Table			= "acdcsTable";
		acdcsTable.elementi[i].DataLen			= 2;
		acdcsTable.elementi[i].Data				= new ConversionData[2];
		acdcsTable.elementi[i].Data[0].Position	= 16;
		acdcsTable.elementi[i].Data[0].Factor	= 256;
		acdcsTable.elementi[i].Data[1].Position	= 18;
		i++;
		acdcsTable.elementi[i].OID				= "acdcsTable.acdcShutdown";
		acdcsTable.elementi[i].Type				= ASN_INTEGER;
		acdcsTable.elementi[i].DBparameter		= "Value";
		acdcsTable.elementi[i].Operation		= OPERATION_INT16CONVERSION;
		acdcsTable.elementi[i].Table			= "acdcsTable";
		acdcsTable.elementi[i].DataLen			= 2;
		acdcsTable.elementi[i].Data				= new ConversionData[2];
		acdcsTable.elementi[i].Data[0].Position	= 20;
		acdcsTable.elementi[i].Data[0].Factor	= 256;
		acdcsTable.elementi[i].Data[1].Position	= 22;

		// DCDC
		i = 0;
		dcdcsTable.elementi[i].OID				= "dcdcsTable.dcdcType";
		dcdcsTable.elementi[i].Type				= ASN_INTEGER;
		dcdcsTable.elementi[i].DBparameter		= "Value";
		dcdcsTable.elementi[i].Operation		= OPERATION_UINT16CONVERSION;
		dcdcsTable.elementi[i].DataLen			= 1;
		dcdcsTable.elementi[i].Data				= new ConversionData[1];
		i++;
		dcdcsTable.elementi[i].OID				= "dcdcsTable.dcdcSeverity";
		dcdcsTable.elementi[i].Type				= ASN_INTEGER;
		dcdcsTable.elementi[i].DBparameter		= "SevLevel";
		dcdcsTable.elementi[i].Operation		= OPERATION_INTEGER;
		i++;
		dcdcsTable.elementi[i].OID					= "dcdcsTable.dcdcID";
		dcdcsTable.elementi[i].Type				= ASN_INTEGER;
		dcdcsTable.elementi[i].DBparameter		= "Value";
		dcdcsTable.elementi[i].Operation		= OPERATION_UINT16CONVERSION;
		dcdcsTable.elementi[i].DataLen			= 1;
		dcdcsTable.elementi[i].Data				= new ConversionData[1];
		dcdcsTable.elementi[i].Data[0].Position	= 2;
		i++;
		dcdcsTable.elementi[i].OID					= "dcdcsTable.dcdcFlags";
		dcdcsTable.elementi[i].Type				= ASN_BIT_STR;
		dcdcsTable.elementi[i].DBparameter		= "Value";
		dcdcsTable.elementi[i].Operation		= OPERATION_BUFCONVERSION;
		dcdcsTable.elementi[i].DataLen			= 1;
		dcdcsTable.elementi[i].Data				= new ConversionData[1];
		dcdcsTable.elementi[i].Data[0].Position	= 4;	
		dcdcsTable.elementi[i].Data[0].Length	= 6;	
		i++;
		string dcdcsTable_elements[18] = {
			"dcdcsTable.dcdcTensionIN1",
			"dcdcsTable.dcdcTensionIN2",
			"dcdcsTable.dcdcTensionBUS",
			"dcdcsTable.dcdcTensionBATT",
			"dcdcsTable.dcdcTensionV24",
			"dcdcsTable.dcdcTensionV12P",
			"dcdcsTable.dcdcTensionV12M",
			"dcdcsTable.dcdcTensionV5",
			"dcdcsTable.dcdcTensionV3",
			"dcdcsTable.dcdcCurrentIN1",
			"dcdcsTable.dcdcCurrentIN2",
			"dcdcsTable.dcdcCurrentBATT",
			"dcdcsTable.dcdcCurrentV24",
			"dcdcsTable.dcdcCurrentV12P",
			"dcdcsTable.dcdcCurrentV12M",
			"dcdcsTable.dcdcCurrentV5",
			"dcdcsTable.dcdcCurrentV3",
			"dcdcsTable.dcdcTemperature"
		};
		int byte=12;
		for(int counter=i; counter < i+18; counter++){
			dcdcsTable.elementi[counter].OID				= dcdcsTable_elements[counter-i];
			dcdcsTable.elementi[counter].Type				= ASN_INTEGER;
			dcdcsTable.elementi[counter].DBparameter		= "Value";
			dcdcsTable.elementi[counter].Operation			= OPERATION_INT16CONVERSION;
			dcdcsTable.elementi[counter].Table				= "dcdcsTable";
			dcdcsTable.elementi[counter].DataLen			= 2;
			dcdcsTable.elementi[counter].Data				= new ConversionData[2];
			dcdcsTable.elementi[counter].Data[0].Position	= byte;
			byte += 2;
			dcdcsTable.elementi[counter].Data[0].Factor	= 256;
			dcdcsTable.elementi[counter].Data[1].Position	= byte;
			byte += 2;
		}

	}
		 
	dbAIMCTS0::~dbAIMCTS0 (void)
	{
	}

	/**
	 *  Calcola la massima severit�
	 */
	void dbAIMCTS0::maxSeverity (int32_tt severity)
	{
		switch (this->severity) {
			case severityUNKNOWN:
			case severityNORMAL:
				switch (severity) {
					case severityUNKNOWN: this->severity = severityUNKNOWN;  break;
					case severityWARNING: this->severity = severityWARNING;  break;
					case severityERROR  : this->severity = severityERROR;    break;
				}
				break;

			case severityWARNING:
				if (severity == severityERROR)
					this->severity = severityERROR;
				break;

			case severityERROR:
				break;
		}
	}

	void dbAIMCTS0::updateMIB (void)
	{
		netsnmp_tdata_row* pRow;
		int32_tt           number;

		this->ctssAlarmsMask_Old     = this->ctssAlarmsMask_New;
		this->ctssAlarmsMask_New     = this->pMib->scalarObj.ctssAlarmsMask();

		this->cpustsAlarmsMask_Old   = this->cpustsAlarmsMask_New;
		this->cpustsAlarmsMask_New   = this->pMib->scalarObj.cpustsAlarmsMask();

		this->modulesAlarmsMask_Old  = this->modulesAlarmsMask_New;
		this->modulesAlarmsMask_New  = this->pMib->scalarObj.modulesAlarmsMask();

		this->consolesAlarmsMask_Old = this->consolesAlarmsMask_New;
		this->consolesAlarmsMask_New = this->pMib->scalarObj.consolesAlarmsMask();

		this->acdcsAlarmsMask_Old    = this->acdcsAlarmsMask_New;
		this->acdcsAlarmsMask_New    = this->pMib->scalarObj.acdcsAlarmsMask();

		this->dcdcsAlarmsMask_Old    = this->dcdcsAlarmsMask_New;
		this->dcdcsAlarmsMask_New    = this->pMib->scalarObj.dcdcsAlarmsMask();

		this->DBconnect(this->dbConfig);
		this->sql_value(this->generalInfo, true);
		this->sql_value(this->severityInfo);
		this->sql_value(this->streamInfo);

		this->sql_value(this->ctssTable);
		for(this->severity=severityNORMAL,pRow=pMib->ctssTableObj.NextRow(NULL),number=0; pRow != NULL; pRow=pMib->ctssTableObj.NextRow(pRow),number++) {
			this->maxSeverity(pMib->ctssTableObj.Entry(pRow).ctsSeverity.get());
		}
		pMib->scalarObj.ctssNumber().set(number);
		pMib->scalarObj.ctssSeverity().set(this->severity);

		if ((pRow=pMib->ctssTableObj.NextRow(NULL)) != NULL)
			pMib->scalarObj.infoStationID()=pMib->ctssTableObj.Entry(pRow).ctsNumber;

		this->sql_value(this->cpusts);

		this->sql_value(this->modulesTable);
		for(this->severity=severityNORMAL,pRow=pMib->modulesTableObj.NextRow(NULL),number=0; pRow != NULL; pRow=pMib->modulesTableObj.NextRow(pRow),number++) {
			this->maxSeverity(pMib->modulesTableObj.Entry(pRow).moduleSeverity.get());
		}
		pMib->scalarObj.modulesNumber().set(number);
		pMib->scalarObj.modulesSeverity().set(this->severity);

		this->sql_value(this->consolesTable);
		for(this->severity=severityNORMAL,pRow=pMib->consolesTableObj.NextRow(NULL),number=0; pRow != NULL; pRow=pMib->consolesTableObj.NextRow(pRow),number++) {
			this->maxSeverity(pMib->consolesTableObj.Entry(pRow).consoleSeverity.get());
		}
		pMib->scalarObj.consolesNumber().set(number);
		pMib->scalarObj.consolesSeverity().set(this->severity);

		this->sql_value(this->acdcsTable);
		for(this->severity=severityNORMAL,pRow=pMib->acdcsTableObj.NextRow(NULL),number=0; pRow != NULL; pRow=pMib->acdcsTableObj.NextRow(pRow),number++) {
			this->maxSeverity(pMib->acdcsTableObj.Entry(pRow).acdcSeverity.get());
		}
		pMib->scalarObj.acdcsNumber().set(number);
		pMib->scalarObj.acdcsSeverity().set(this->severity);

		this->sql_value(this->dcdcsTable);
		for(this->severity=severityNORMAL,pRow=pMib->dcdcsTableObj.NextRow(NULL),number=0; pRow != NULL; pRow=pMib->dcdcsTableObj.NextRow(pRow),number++) {
			this->maxSeverity(pMib->dcdcsTableObj.Entry(pRow).dcdcSeverity.get());
		}
		pMib->scalarObj.dcdcsNumber().set(number);
		pMib->scalarObj.dcdcsSeverity().set(this->severity);
		
		this->DBdisconnect();
	}

	void dbAIMCTS0::sendToMIB(ElementoMIB &el, int row, Conversion& value){

#ifdef DB_DEBUG
		cout << "    " << el.OID;
		switch(value.getType()){
			case VINTEGER:
				cout << " --> INT: " << value.getInteger().get() << endl;
				break;
			case VDISPLAYSTRING:
				{
				string s;
				value.getString().get(s);
				cout << " --> STR: " << s << endl;
				}
				break;
			case VBITS:
				cout << " --> BIT: ";
				for(int cb=0; cb<BITS_DATA_SIZE; cb++)
					cout << hex <<(int)value.getBits()[cb] << " ";
				cout << dec << endl;
				break;
			default:
				cout << endl;
		}
#endif

		try
		{
			/* SCALARI */
			if(el.OID.find("infoDeviceName")!=string::npos){
				pMib->scalarObj.infoDeviceName()=value.getString();
			}
			else if(el.OID.find("infoStationName")!=string::npos){
				pMib->scalarObj.infoStationName()=value.getString();
			}
			else if(el.OID.find("infoModel")!=string::npos){
				pMib->scalarObj.infoModel()=value.getString();
			}
			else if(el.OID.find("infoAIMVersion")!=string::npos){
				pMib->scalarObj.infoAIMVersion()=value.getString();
			}

			else if(el.OID.find("devSeverity")!=string::npos){
				if(pMib->scalarObj.devSeverity()!=value.getInteger()) {
					pMib->scalarObj.devSeverity()=value.getInteger();

					traps<hwAIMCTS0>* trap = new traps<hwAIMCTS0>(pMib->trapSystemSeverityKey(), pMib->trapSystemSeverityOID());
					pMib->trapSystemSeverityNotify();
					delete trap;
				}
			}

			else if(el.OID.find("cpustsSeverity")!=string::npos){
				pMib->scalarObj.cpustsSeverity()=value.getInteger();
			}
			else if(el.OID.find("cpustsType")!=string::npos){
				pMib->scalarObj.cpustsType()=value.getInteger();
			}
			else if(el.OID.find("cpustsFlags")!=string::npos){
				alarmAIMCTS0* alarm = new alarmAIMCTS0(value.getBits(),this->cpustsAlarmsMask_New);
				if (alarm->changed(pMib->scalarObj.cpustsFlags(),this->cpustsAlarmsMask_Old)) {
					alarm->setDeviceType(deviceCPU);
					alarm->setDeviceID(pMib->scalarObj.infoStationID());
					alarm->setSeverity(pMib->scalarObj.cpustsSeverity());
					alarm->setSourceOID(pMib->scalarObj.OID(scalar::cpustsFlags_id));
					alarm->notifyAlarm();
				}
				delete alarm;
				pMib->scalarObj.cpustsFlags()=value.getBits();
			}

			/* TABELLA CTSS */
			if(el.OID.find("ctssTable")!=string::npos){

				netsnmp_tdata_row* pRow = pMib->ctssTableObj.Row(row);		
				ctssTable::entry_t& Entry = pMib->ctssTableObj.Entry(pRow);

				if(el.OID.find("ctsSeverity")!=string::npos){
					Entry.ctsSeverity=value.getInteger();
				}
				else if(el.OID.find("ctsNumber")!=string::npos){
					Entry.ctsNumber=value.getInteger();
				}
				else if(el.OID.find("ctsFlags")!=string::npos){
					alarmAIMCTS0* alarm = new alarmAIMCTS0(value.getBits(),this->ctssAlarmsMask_New);
					if (alarm->changed(Entry.ctsFlags,this->ctssAlarmsMask_Old)) {
						alarm->setDeviceType(devicePOD);
						alarm->setDeviceID(Entry.ctsNumber);
						alarm->setSeverity(Entry.ctsSeverity);
						alarm->setSourceOID(pMib->ctssTableObj.OID(pRow, ctssTable::ctsFlags));
						alarm->notifyAlarm();
					}
					delete alarm;
					Entry.ctsFlags=value.getBits();
				}
			}

			/* TABELLA MODULES */
			if(el.OID.find("modulesSeverity")!=string::npos){
				pMib->scalarObj.modulesSeverity()=value.getInteger();
			}
			else if(el.OID.find("modulesNumber")!=string::npos){
				pMib->scalarObj.modulesNumber()=value.getInteger();
			}

			else if(el.OID.find("modulesTable")!=string::npos){

				netsnmp_tdata_row* pRow = pMib->modulesTableObj.Row(row);		
				modulesTable::entry_t& Entry = pMib->modulesTableObj.Entry(pRow);

				if(el.OID.find("moduleSeverity")!=string::npos){
					Entry.moduleSeverity=value.getInteger();
				}
				else if(el.OID.find("modulePath")!=string::npos){
					Entry.modulePath=value.getString();
				}
				else if(el.OID.find("moduleType")!=string::npos){
					Entry.moduleType=value.getInteger();
				}
				else if(el.OID.find("moduleSubtype")!=string::npos){
					Entry.moduleSubtype=value.getInteger();
				}
				else if(el.OID.find("moduleFlags")!=string::npos){
					alarmAIMCTS0* alarm = new alarmAIMCTS0(value.getBits(),this->modulesAlarmsMask_New);
					if (alarm->changed(Entry.moduleFlags,this->modulesAlarmsMask_Old)) {
						alarm->setDeviceType(devicePOD);
						alarm->setDeviceID(Entry.modulePath);
						alarm->setSeverity(Entry.moduleSeverity);
						alarm->setSourceOID(pMib->modulesTableObj.OID(pRow, modulesTable::moduleFlags));
						alarm->notifyAlarm();
					}
					delete alarm;
					Entry.moduleFlags=value.getBits();
				}
			}

			/* TABELLA CONSOLES */
			if(el.OID.find("consolesTable")!=string::npos){

				netsnmp_tdata_row* pRow = pMib->consolesTableObj.Row(row);		
				consolesTable::entry_t& Entry = pMib->consolesTableObj.Entry(pRow);

				if(el.OID.find("consoleSeverity")!=string::npos){
					Entry.consoleSeverity=value.getInteger();
				}
				else if(el.OID.find("consolePath")!=string::npos){
					Entry.consolePath=value.getString();
				}
				else if(el.OID.find("consoleType")!=string::npos){
					Entry.consoleType=value.getInteger();
				}
				else if(el.OID.find("consoleNumber")!=string::npos){
					Entry.consoleNumber=value.getString();
				}
				else if(el.OID.find("consoleFlags")!=string::npos){
					alarmAIMCTS0* alarm = new alarmAIMCTS0(value.getBits(),this->consolesAlarmsMask_New);
					if (alarm->changed(Entry.consoleFlags,this->consolesAlarmsMask_Old)) {
						string sPath;
						string sNum;
						
						Entry.consolePath.get(sPath);
						Entry.consoleNumber.get(sNum);

						alarm->setDeviceType(deviceCONSOLE);
						alarm->setDeviceID(sNum + "(" + sPath + ")");
						alarm->setSeverity(Entry.consoleSeverity);
						alarm->setSourceOID(pMib->consolesTableObj.OID(pRow, consolesTable::consoleFlags));
						alarm->notifyAlarm();
					}
					delete alarm;
					Entry.consoleFlags=value.getBits();
				}
			}

			/* TABELLA ACDCS */
			if(el.OID.find("acdcsTable")!=string::npos){

				netsnmp_tdata_row* pRow = pMib->acdcsTableObj.Row(row);		
				acdcsTable::entry_t& Entry = pMib->acdcsTableObj.Entry(pRow);

				if(el.OID.find("acdcSeverity")!=string::npos){
					Entry.acdcSeverity=value.getInteger();
				}
				else if(el.OID.find("acdcType")!=string::npos){
					Entry.acdcType=value.getInteger();
				}
				else if(el.OID.find("acdcID")!=string::npos){
					Entry.acdcID=value.getInteger();
				}
				else if(el.OID.find("acdcFlags")!=string::npos){
					alarmAIMCTS0* alarm = new alarmAIMCTS0(value.getBits(),this->acdcsAlarmsMask_New);
					if (alarm->changed(Entry.acdcFlags,this->acdcsAlarmsMask_Old)) {
						alarm->setDeviceType(deviceACDC);
						alarm->setDeviceID(Entry.acdcID);
						alarm->setSeverity(Entry.acdcSeverity);
						alarm->setSourceOID(pMib->acdcsTableObj.OID(pRow, acdcsTable::acdcFlags));
						alarm->notifyAlarm();
					}
					delete alarm;
					Entry.acdcFlags=value.getBits();
				}
				else if(el.OID.find("acdcTension")!=string::npos){
					Entry.acdcTension=value.getInteger();
				}
				else if(el.OID.find("acdcCurrent")!=string::npos){
					Entry.acdcCurrent=value.getInteger();
				}
				else if(el.OID.find("acdcShutdown")!=string::npos){
					Entry.acdcShutdown=value.getInteger();
				}
			}

			/* TABELLA DCDCS */
			if(el.OID.find("dcdcsTable")!=string::npos){

				netsnmp_tdata_row* pRow = pMib->dcdcsTableObj.Row(row);		
				dcdcsTable::entry_t& Entry = pMib->dcdcsTableObj.Entry(pRow);

				if(el.OID.find("dcdcSeverity")!=string::npos){
					Entry.dcdcSeverity=value.getInteger();
				}
				else if(el.OID.find("dcdcType")!=string::npos){
					Entry.dcdcType=value.getInteger();
				}
				else if(el.OID.find("dcdcID")!=string::npos){
					Entry.dcdcID=value.getInteger();
				}
				else if(el.OID.find("dcdcFlags")!=string::npos){
					alarmAIMCTS0* alarm = new alarmAIMCTS0(value.getBits(),this->dcdcsAlarmsMask_New);
					if (alarm->changed(Entry.dcdcFlags,this->dcdcsAlarmsMask_Old)) {
						alarm->setDeviceType(deviceDCDC);
						alarm->setDeviceID(Entry.dcdcID);
						alarm->setSeverity(Entry.dcdcSeverity);
						alarm->setSourceOID(pMib->dcdcsTableObj.OID(pRow, dcdcsTable::dcdcFlags));
						alarm->notifyAlarm();
					}
					delete alarm;
					Entry.dcdcFlags=value.getBits();
				}
				else if(el.OID.find("dcdcTensionIN1")!=string::npos){
					Entry.dcdcTensionIN1=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionIN2")!=string::npos){
					Entry.dcdcTensionIN2=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionBUS")!=string::npos){
					Entry.dcdcTensionBUS=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionBATT")!=string::npos){
					Entry.dcdcTensionBATT=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionV24")!=string::npos){
					Entry.dcdcTensionV24=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionV12P")!=string::npos){
					Entry.dcdcTensionV12P=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionV12M")!=string::npos){
					Entry.dcdcTensionV12M=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionV5")!=string::npos){
					Entry.dcdcTensionV5=value.getInteger();
				}
				else if(el.OID.find("dcdcTensionV3")!=string::npos){
					Entry.dcdcTensionV3=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentIN1")!=string::npos){
					Entry.dcdcCurrentIN1=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentIN2")!=string::npos){
					Entry.dcdcCurrentIN2=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentBATT")!=string::npos){
					Entry.dcdcCurrentBATT=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentV24")!=string::npos){
					Entry.dcdcCurrentV24=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentV12P")!=string::npos){
					Entry.dcdcCurrentV12P=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentV12M")!=string::npos){
					Entry.dcdcCurrentV12M=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentV5")!=string::npos){
					Entry.dcdcCurrentV5=value.getInteger();
				}
				else if(el.OID.find("dcdcCurrentV3")!=string::npos){
					Entry.dcdcCurrentV3=value.getInteger();
				}
				else if(el.OID.find("dcdcTemperature")!=string::npos){
					Entry.dcdcTemperature=value.getInteger();
				}
			}
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : set mib parameter (%s - %d).\n", el.OID.c_str(), ex.Description().c_str(), ex.Code());
	
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s: critical error when writing mib values.\n", el.OID.c_str());

		}
		
	}

};
#endif
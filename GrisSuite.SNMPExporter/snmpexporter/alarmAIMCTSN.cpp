/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmAIMCTSN.cpp - Modulo implementazione classe gestione allarmi periferica AIMCTSN.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#include "alarmAIMCTSN.h"

using namespace std;
using namespace mib;
using namespace mib_hwAIMCTSN;

namespace subagent {

	/**
	 *  Unique IDentification per gli allarmi periferica
	 */
	uint32_tt alarmDEVICE<hwAIMCTSN>::UID = 0; 


	/**
	 *  Inizializza oggetti al valore di default
	 */
	void alarmAIMCTSN::init (void)
	{
		try {
			this->deviceType.defval();
		}
		catch (...) {
			throw applyException("alarmAIMCTSN::init: critical error.\n", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Costruttore di default istanza classe gestione allarmi periferica AIMCTSN
	 */
	alarmAIMCTSN::alarmAIMCTSN (void) : alarmDEVICE ()
	{
		this->init();
	}

	/**
	 *  Costruttore istanza classe gestione allarmi periferica AIMCTSN
	 *
	 *  @param[in]  flags  flags di segnalazione stato periferica
	 *  @param[in]  mask   maschera allarmi periferica
	 */
	alarmAIMCTSN::alarmAIMCTSN (bits& flags, bits& mask) : alarmDEVICE (flags, mask)
	{
		this->init();
	}

	/**
	 *  Distruttore istanza
	 */
	alarmAIMCTSN::~alarmAIMCTSN (void)
	{
	}

	/**
	 *  Imposta tipo periferica che ha generato l'allarme
	 *
	 *  @param[in]  type  codice tipo periferica
	 */
	void alarmAIMCTSN::setDeviceType (int32_tt type)
	{
		this->deviceType.set(type);
	}

	/**
	 *  Aggiorna riga tabella con dati allarme
	 */
	void alarmAIMCTSN::updateItem (void)
	{
		try {
			if (this->action == INS_ACTION) {
				alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);
				entry.alarmDeviceType = this->deviceType;
			}
		}
		catch (...) {
			throw applyException("alarmAIMCTSN::updateItem: critical error.\n", applyException::SYS_CRITICAL_ERROR);
		}
	}

	/**
	 *  Verifica corrispondenza oggetto con tabella allarmi
	 *
	 *  @return true se l'oggetto corrisponde con allarme selezionato
	 */
	bool alarmAIMCTSN::checkItem (void)
	{
		alarmsTable::entry_t& entry = pMib->alarmsTableObj.Entry(this->pRow);

		return this->deviceType == entry.alarmDeviceType && this->deviceID == entry.alarmDeviceID;
	}

	/**
	 *  Inserisce allarme in tabella ed invia notifica al server di diagnostica. 
	 */
	void alarmAIMCTSN::notifyAlarm (void)
	{
		this->selectItem();
		dynamic_cast<alarmDEVICE*>(this)->updateItem();
		this->updateItem();
		this->notify();
	}
}



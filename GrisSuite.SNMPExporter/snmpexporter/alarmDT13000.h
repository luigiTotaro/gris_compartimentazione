/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmDT13000.h - Modulo implementazione classe gestione allarmi periferica DT13000.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */
#ifndef ALARMDT13000_H
#define ALARMDT13000_H

#include "hwDT13000.h"
#include "alarmDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwDT13000;

namespace subagent {

	class alarmDT13000 : public alarmDEVICE<hwDT13000> {

	public:
		alarmDT13000 (bits& flags, bits& mask);

	};
}

#endif


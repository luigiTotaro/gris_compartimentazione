/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagDT13000.h - Modulo di gestione diagnostica alimentatore DT13000.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef DIAGDT13000_H
#define DIAGDT13000_H

#include "hwDT13000.h"
#include "dbDT13000.h"
#include "srvConfig.h"
#include "diagDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwDT13000;

namespace subagent {

	class diagDT13000 : public diagDEVICE<hwDT13000> {

		dbDT13000* pDatabase;

	public:
		diagDT13000  (DBConfig* dbcfg, DevConfig* devcfg);
		~diagDT13000 (void);

		void Update (void);

	private:
		friend bool mibObjectWritten (mib_hwDT13000::scalar::scalar_id_t id, void* obj);
		friend bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
	};

static bool mibObjectWritten (mib_hwDT13000::scalar::scalar_id_t id, void* obj);
static bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
}

#endif
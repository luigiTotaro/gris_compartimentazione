/*
 *  thread.cpp
 *  IP-PBX
 *
 *  Created by Paolo Colli on 16/04/12.
 *  Copyright 2012 Telefin SpA. All rights reserved.
 *
 */
#include "exception.h"
#include "thread.h"

using namespace std;
using namespace subagent;


/**
 *  Funzione statica per l'attivazione di un nuovo thread.
 *
 *  @param[in]   pObj  puntatpre oggetto che implementa il metodo Run.
 *
 *  @return      0
 */
static DWORD WINAPI StartThread ( LPVOID pObj )
{
	Runnable* pf = static_cast<Runnable*> (pObj);

	pf->Run();

	return 0;
}

/**
 *  Costruttore istanza della classe Thread.
 *
 *  @param[in]   pObj   puntatpre oggetto che implementa il metodo Run.
 *  @param[in]   flags  flags di creazione thread.
 */
Thread::Thread ( Runnable* pObj, DWORD flags )
{
	this->pObject           = pObj;
	this->Terminated        = false;
	this->Suspended         = false;
	this->SuspendCount      = 0;
	this->Handle            = NULL;
	this->ThreadAttributes	= NULL;
	this->StackSize         = 0;
	this->StartAddress		= StartThread;
	this->Parameter			= this->pObject;
	this->CreationFlags		= flags;
	this->ThreadId			= 0;
	this->LastErrorCode     = 0;
	this->LifeSignal        = 0;
	this->Name              = "";
}

/**
 *  Distruttore istanza della classe Thread.
 */
Thread::~Thread ( void )
{
	if (this->Handle != NULL) {
		if (!this->exit() || !this->join(WAIT_FOR_END_THREAD))
			this->kill();

		this->close();
	}
}

/**
 *  Istanzia e attiva il thread.
 *
 *  @return  In caso di errore ritorna false. LastErrorCode segnala il codice di errore.
 */
bool Thread::start ( void )
{
	try {
		this->refreshlife();

		this->Handle = CreateThread( this->ThreadAttributes,
									 this->StackSize       ,
									 this->StartAddress    ,
									 this->Parameter       ,
									 this->CreationFlags   ,
									 &this->ThreadId       );
		if ( this->Handle == NULL ) {
			// Creazione fallita
			this->LastErrorCode = this->exceptionCode(GetLastError());
			return false;
		}
		else {
			// Creazione riuscita
			if ( this->CreationFlags == CREATE_SUSPENDED ) {
				this->Suspended     = true;
				this->SuspendCount  = 1;
			}
			else {
				this->Suspended     = false;
				this->SuspendCount  = 0;
			}
			this->Terminated = false;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	return true;
}


/**
 *  Chiude l'oggetto Handle.
 *
 *  @return  In caso di errore ritorna false. LastErrorCode segnala il codice di errore.
 */
bool Thread::close ( void )
{
	try {
		if ( this->Handle == NULL ) {
			if (!CloseHandle(this->Handle)) {
				this->LastErrorCode = this->exceptionCode(GetLastError());
				return false;
			}
		}
		else {
			this->LastErrorCode = applyException::SYS_INVALID_THREAD_HANDLE;
			return false;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	this->Handle = NULL;
	return true;
}


/**
 *  Funzione per terminare thread.
 *  Questa funzione deve essere usata solo in casi estremi perch� lo stack non
 *  viene disallocato ed eventuali DLL non vengono avvisate.
 *
 *  @return  In caso di errore restituisce false. LastErrorCode segnala il codice di errore.
 */
bool Thread::kill ( void )
{
	try {
		BOOL code;

		if ( this->Handle != NULL ) {
			// Recupero l'exitcode
			code = GetExitCodeThread(this->Handle, &(this->ExitCode));
			if ( !code ) {
				// Funzione fallita	
				this->LastErrorCode = this->exceptionCode(GetLastError());
				return false;
			}
			else {
				code = TerminateThread(this->Handle, this->ExitCode);
				if ( !code ) {
					// Terminazione fallita
					this->LastErrorCode = this->exceptionCode(GetLastError());
					return false;
				}
				else {
					// Terminazione esguita con successo
					this->Terminated = true;
					this->Suspended  = true;
				}
			}
		}
		else {
			this->LastErrorCode = applyException::SYS_INVALID_THREAD_HANDLE;
			return false;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	return true;
}


/**
 *  Funzione per uscire da thread.
 *  L'uso di questa funzione � preferibile rispetto a kill perch�
 *  esegue la disallocazione dello stack del thread e avvisa eventuali DLL.
 *
 *  @return  In caso di errore restituisce false. LastErrorCode segnala il codice di errore.
 */
bool Thread::exit ( void )
{
	try {
		BOOL  code;

		if ( this->Handle != NULL ) {
			// Recupero l'exitcode
			code = GetExitCodeThread(this->Handle, &(this->ExitCode));

			// Comanda chiusura del thread
			this->Terminated = true;

			if ( !code ) {
				this->LastErrorCode = this->exceptionCode(GetLastError());
				return false;
			}
		}
		else {
			this->LastErrorCode = applyException::SYS_INVALID_THREAD_HANDLE;
			return false;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	return true;
}


/**
 *  Funzione per attendere la fine di un thread (con timeout specificato)
 *
 *  @return  In caso di errore restituisce false. LastErrorCode segnala il codice di errore.
 */
bool Thread::join ( int timeout )
{
	try {
		DWORD code;

		if ( this->Handle != NULL )
		{
			code = WaitForSingleObject( this->Handle, timeout );
			if ( code == WAIT_FAILED ) {
				this->LastErrorCode = applyException::SYS_THREAD_WAIT_FAILED;
				return false;
			}
			if ( code == WAIT_TIMEOUT ) {
				this->LastErrorCode = applyException::SYS_THREAD_WAIT_TIMEOUT;
				return false;
			}
		}
		else {
			this->LastErrorCode = applyException::SYS_INVALID_THREAD_HANDLE;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	return true;
}


/**
 *  Funzione per ripristinare thread.
 *  Questa funzione decrementa il contatore delle sospensioni di un thread.
 *  Quando il contatore va a zero il thread viene ripristinato.
 *
 *  @return  In caso di errore restituisce false. LastErrorCode segnala il codice di errore
 */
bool Thread::resume ( void )
{
	try {
		DWORD code;

		if ( this->Handle != NULL ) {
			code = ResumeThread(this->Handle);
			if ( code == (DWORD)-1 ) {
				// Rispristino fallito
				this->LastErrorCode = this->exceptionCode(GetLastError());
				return false;
			}
			else {
				// Rispristino eseguito con successo
				this->SuspendCount = (code <= 0)? 0 : (code-1);
				if ( this->SuspendCount == 0 ) {
					// Thread ripristinato
					this->Suspended = false;
				}
			}
		}
		else {
			this->LastErrorCode = applyException::SYS_INVALID_THREAD_HANDLE;
			return false;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	return true;
}

/**
 *  Funzione per sospendere thread.
 *  Questa funzione sospende il thread specificato nella struttura <thread>.
 *  Se il thread � gi� sospeso incrementa il contatore delle sospensioni.
 *
 *  @return  In caso di errore restituisce false. LastErrorCode segnala il codice di errore
 */
bool Thread::suspened ( void )
{
	try
	{
		DWORD code;

		if ( this->Handle != NULL ) {
			code = SuspendThread(this->Handle);
			if ( code == (DWORD)-1 ) {
				// Sospensione fallita
				this->LastErrorCode = this->exceptionCode(GetLastError());
				return false;
			}
			else
			{
				this->SuspendCount = code+1;
				this->Suspended    = true;
			}
		}
		else {
			this->LastErrorCode = applyException::SYS_INVALID_THREAD_HANDLE;
			return false;
		}
	}
	catch (...) {
		this->LastErrorCode = applyException::SYS_CRITICAL_ERROR;
		return false;
	}
	return true;
}


/**
 *  Ritorna il codice dell'ultimo errore occorso.
 *
 *  @return      codice ultimo errore.
 */
int Thread::error ( void )
{
	return this->LastErrorCode;
}


/**
 *  Indica se deve essere terminato il thread.
 *
 *  @return      true se il thread deve vivere, false se deve terminare.
 */
bool Thread::running ( void )
{
	return !this->Terminated;
}


/**
 *  Rinfresca segnale di vita.
 */
void Thread::refreshlife ( void )
{
	this->LifeSignal = GetTickCount();
}


/**
 *  Verifica il segnale di vita del thread.
 *  Se � trascorso un periodo maggiore di lifetime dall'ultimo aggiornamento il thread � ritenuto non funzionante
 *
 *  @param[in]   lifetime  tempo massimo consentito per aggiornamento segnale di vita
 *
 *  @return      true se il thread in funzione, false se bloccato.
 */
bool Thread::verifylife ( int lifetime )
{
	DWORD nowtickcount = GetTickCount();

	if (nowtickcount < this->LifeSignal) {
		// overflow del contatore
		this->LifeSignal = nowtickcount;
	}

	DWORD elapsedtime = nowtickcount - this->LifeSignal;

	return (elapsedtime < (DWORD)lifetime*1000);
}


/**
 *  Decodifica errore di sistema in codice eccezione applicazione.
 *
 *  @param[in]   err  codice errore di sistema
 *
 *  @return      codice eccezione
 */
int Thread::exceptionCode ( DWORD err )
{
	switch (err) {
		case ERROR_INVALID_HANDLE      : return applyException::SYS_QSSE_INVALID_HANDLE;
		case ERROR_ACCESS_DENIED       : return applyException::SYS_QSSE_ACCESS_DENIED;
		case ERROR_INSUFFICIENT_BUFFER : return applyException::SYS_QSSE_INSUFFICIENT_BUFFER;
		case ERROR_INVALID_PARAMETER   : return applyException::SYS_QSSE_INVALID_PARAMETER;
		case ERROR_INVALID_LEVEL       : return applyException::SYS_QSSE_INVALID_LEVEL;
		case ERROR_SHUTDOWN_IN_PROGRESS: return applyException::SYS_QSSE_SHUTDOWN_IN_PROGRESS;
	}
	return applyException::SYS_SERVICE_QUERY_FAILURE;
}


/**
 *  Imposta il nome del thread.
 *
 *  @param[in]   name  nome thread
 */
void Thread::setName ( string& name )
{
	this->Name = name;
}


/**
 *  Restituisce il nome del thread
 *
 *  @return      nome thread
 */
string& Thread::getName ( void )
{
	return this->Name;
}
/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * alarmAIMCTSN.h - Modulo implementazione classe gestione allarmi periferica AIMCTSN.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 17/07/2012
 * @copyright 2012 Telefin S.p.A.
 */
#ifndef ALARMAIMCTSN_H
#define ALARMAIMCTSN_H

#include "hwAIMCTSN.h"
#include "alarmDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwAIMCTSN;

namespace subagent {

	class alarmAIMCTSN : public alarmDEVICE<hwAIMCTSN> {

		integer  deviceType;   // tipo periferica

	public:
		alarmAIMCTSN  (void);
		alarmAIMCTSN  (bits& flags, bits& mask);
		~alarmAIMCTSN (void);

		void setDeviceType (int32_tt type);
		bool checkItem     (void);
		void notifyAlarm   (void);

	private:
		void init       (void);
		void updateItem (void);
	};
}

#endif


/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * traps.h - Gestione trap: crea un'istanza della classe trap (apertura sezione, invio trap, chiusura sezione).
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef TRAPS_H
#define TRAPS_H

#include <iostream>
#include <string>
#include <fstream>
#include <list>

#include "mib_objects.h"
#include "mibconfig.h"
#include "exception.h"

using namespace std;


namespace subagent {

	/**
	 *  Inizializza tabella di gestione traps
	 */
	template <typename MIBTYPE>
	class CTraps {
	
	public:
		/**
		 *  Carica tabella di abilitazione traps a valori di default.
		 */
		void load (void);

		/**
		 *  Carica tabella di abilitazione traps come da file di configurazione
		 *
		 *  @param[in]   fname  identificativo file di configurazione tabella traps abilitate
		 */
		void load (string fname);

		/**
		 *  Memorizza tabella di abilitazione traps nel file di configurazione
		 *
		 *  @param[in]   fname  identificativo file di configurazione tabella traps abilitate
		 */
		void store (string fname);
	};


	/**
	 *  Carica tabella di abilitazione traps a valori di default.
	 */
	template <typename MIBTYPE>
	void CTraps<MIBTYPE>::load (void)
	{
		try {
			MIBTYPE* pMib;
			netsnmp_tdata_row* row;

			pMib = MIBTYPE::Instance();

			pMib->trapsEnaTableObj.lock();
			
			row = pMib->trapsEnaTableObj.Row(1);
			
			pMib->trapsEnaTableObj.Entry(row).trapIdentify.set("all");
			pMib->trapsEnaTableObj.Entry(row).trapIPaddress.set("localhost");
			pMib->trapsEnaTableObj.Entry(row).trapCommunity.set("public");
			
			pMib->scalarObj.trapsNumber().set(1);
			
			pMib->trapsEnaTableObj.unlock();
		} catch (...) {
			snmp_log(LOG_ERR, "Traps initialization failed.\n");
		}
	}

	/**
	 *  Carica tabella di abilitazione traps come da file di configurazione
	 *
	 *  @param[in]   fname  identificativo file di configurazione tabella traps abilitate
	 */
	template <typename MIBTYPE>
	void CTraps<MIBTYPE>::load (string fname)
	{
		netsnmp_tdata_row* row;
		MIBTYPE*           pMib;
		cline              cln;
		long               num;

		pMib = MIBTYPE::Instance();

		ifstream in(fname.c_str());

		if (!in) {
			snmp_log(LOG_ERR, "%s : error opening file for reading.\n", fname.c_str());
			return;
		}

		pMib->scalarObj.lock();
		pMib->trapsEnaTableObj.lock();
		try {
			while (!in.eof()) {
				in >> cln;
				
				size_t pos = cln.getOID().rfind('.');
				if (pos == string::npos)
					continue;
				
				long idx = atol(cln.getOID().substr(pos + 1).data());
				if (idx == 0)
					continue;
				
				row = pMib->trapsEnaTableObj.Row(idx);
				if (row != NULL) {
					trapsEnaTable::column_id_t column = pMib->trapsEnaTableObj.NextCol(trapsEnaTable::nullID);
					
					while (column != trapsEnaTable::nullID) {
						if (cln.getKey() == pMib->trapsEnaTableObj.Key(row, column) && cln.getType() == pMib->trapsEnaTableObj.Type(column)) {
							switch (column) {
								case trapsEnaTable::trapIdentify:
									pMib->trapsEnaTableObj.Entry(row).trapIdentify.set(cln.getsValue());
									break;
								case trapsEnaTable::trapIPaddress:
									pMib->trapsEnaTableObj.Entry(row).trapIPaddress.set(cln.getsValue());
									break;
								case trapsEnaTable::trapCommunity:
									pMib->trapsEnaTableObj.Entry(row).trapCommunity.set(cln.getsValue());
									break;
								case trapsEnaTable::trapRowStatus:
									pMib->trapsEnaTableObj.Entry(row).trapRowStatus.set(cln.getlValue());
									break;
							}
						}
						column = pMib->trapsEnaTableObj.NextCol(column);
					}
				}
			}
			in.close();
			
			// Conta il numero di righe per configurare trapsNumber
			num = 0;
			row = NULL;
			while ((row = pMib->trapsEnaTableObj.NextRow(row)) != NULL) {
				num++;
			}
			
			pMib->scalarObj.trapsNumber().set(num);
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : error reading file (%s - %d).\n", fname.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s : error reading file.\n", fname.c_str());
		}
		
		pMib->scalarObj.unlock();
		pMib->trapsEnaTableObj.unlock();
	}

	/**
	 *  Memorizza tabella di abilitazione traps nel file di configurazione
	 *
	 *  @param[in]   fname  identificativo file di configurazione tabella traps abilitate
	 */
	template <typename MIBTYPE>
	void CTraps<MIBTYPE>::store (string fname)
	{
		netsnmp_tdata_row* row;
		MIBTYPE*           pMib;
		
		pMib = MIBTYPE::Instance();
		
		ofstream out(fname.c_str());
		
		if (!out) {
			snmp_log(LOG_ERR, "%s : error opening file for writing.\n", fname.c_str());
			return;
		}
		
		pMib->trapsEnaTableObj.lock();
		try {
			string   soid;
			string   skey;
			string   sval;
			int32_tt ival;
			
			row = pMib->trapsEnaTableObj.NextRow(NULL);
			while (row != NULL) {
				trapsEnaTable::column_id_t column = pMib->trapsEnaTableObj.NextCol(trapsEnaTable::nullID);
				
				while (column != trapsEnaTable::nullID) {
					soid = pMib->trapsEnaTableObj.OID(row, column);
					skey = pMib->trapsEnaTableObj.Key(row, column);
					sval.clear();
					
					switch (column) {
						case trapsEnaTable::trapIdentify:
							pMib->trapsEnaTableObj.Entry(row).trapIdentify.get(sval);
							out << cline(soid, skey, ASN_OCTET_STR, sval);
							break;
						case trapsEnaTable::trapIPaddress:
							pMib->trapsEnaTableObj.Entry(row).trapIPaddress.get(sval);
							out << cline(soid, skey, ASN_OCTET_STR, sval);
							break;
						case trapsEnaTable::trapCommunity:
							pMib->trapsEnaTableObj.Entry(row).trapCommunity.get(sval);
							out << cline(soid, skey, ASN_OCTET_STR, sval);
							break;
						case trapsEnaTable::trapRowStatus:
							ival = pMib->trapsEnaTableObj.Entry(row).trapRowStatus.get();
							out << cline(soid, skey, ASN_INTEGER, ival);
							break;
					}
					column = pMib->trapsEnaTableObj.NextCol(column);
				}
				row = pMib->trapsEnaTableObj.NextRow(row);
			}
			out.close();
		}
		catch (applyException ex) {
			snmp_log(LOG_ERR, "%s : error writing file (%s - %d).\n", fname.c_str(), ex.Description().c_str(), ex.Code());
		}
		catch (...) {
			snmp_log(LOG_ERR, "%s : error writing file.\n", fname.c_str());
		}
		
		pMib->trapsEnaTableObj.unlock();
	}



	/**
	 *  Gestione invio traps
	 */
	template <typename MIBTYPE>
	class traps {

	private:
		static mutex trapMutex;

		list<void*> SessionsList;

	public:
		/**
		 *  Costruttote base istanza della classe traps. Crea sessione per l'invio delle traps
		 *
		 *  @param[in]   tkey  nome notifica da inviare
		 *  @param[in]   toid  oid notifica da inviare
		 */
		traps(string tkey, string toid);

		/**
		 *  Distruttore istanza. Chiude tutte le sessioni aperte ed elimina lista.
		 */
		~traps();

		/**
		 *  Crea una sesssione di trasmissione notifiche (funzione derivata da create_trap_session).
		 *
		 *  @param[in]   sink      identificativo destinatario della trap
		 *  @param[in]   sinkport  numero porta 
		 *  @param[in]   com       community
		 *  @param[in]   version   versione SNMP
		 *  @param[in]   pdutype   tipo PDU (Protocol Data Unit)
		 *
		 *  @return      descrittore sessione creata, NULL in caso di errore
		 */
		void* createSession(string sink, u_short sinkport, string com, int version, int pdutype);

		/**
		 *  Elimina la sessione indicata.
		 *
		 *  @param[in]   ss  descrittore sessione da eliminare
		 */
		void deleteSession(void* ss);
	};


	/**
	 *  Costruttote base istanza della classe traps. Crea sessione per l'invio delle traps
	 *
	 *  @param[in]   tkey  nome notifica da inviare
	 *  @param[in]   toid  oid notifica da inviare
	 */
	template <typename MIBTYPE>
	traps<MIBTYPE>::traps(string tkey, string toid) {
		try {
			trapMutex.lock();
			
			MIBTYPE*           pMib = MIBTYPE::Instance();
			netsnmp_tdata_row* row  = NULL;

			while ((row = pMib->trapsEnaTableObj.NextRow(row)) != NULL) {
				string trap;
				string sink;
				string com;

				pMib->trapsEnaTableObj.Entry(row).trapIdentify.get(trap);
				pMib->trapsEnaTableObj.Entry(row).trapIPaddress.get(sink);
				pMib->trapsEnaTableObj.Entry(row).trapCommunity.get(com);
				
				if (sink.length() != 0) {
					if (trap == "all") {
						void* ss = createSession(sink, 0, com, SNMP_VERSION_2c, SNMP_MSG_TRAP2);
						if (ss != NULL)
							SessionsList.push_back(ss);
					} else if (trap == (*trap.data() == '.' ? toid : tkey)) {
						void* ss = createSession(sink, 0, com, SNMP_VERSION_2c, SNMP_MSG_TRAP2);
						if (ss != NULL)
							SessionsList.push_back(ss);
					}
				}
			}
		} catch (...) {
			snmp_log(LOG_ERR, "Error creating trap session (%s - %s).\n", tkey.c_str(), toid.c_str());
		}
	}

	/**
	 *  Distruttore istanza. Chiude tutte le sessioni aperte ed elimina lista.
	 */
	template <typename MIBTYPE>
	traps<MIBTYPE>::~traps() {
		try {
			while (!SessionsList.empty()) {
				deleteSession(SessionsList.front());
				SessionsList.pop_front();
			}
		} catch (...) {
			snmp_log(LOG_ERR, "Error deleting trap session.\n");
		}
		trapMutex.unlock();
	}

	/**
	 *  Crea una sesssione di trasmissione notifiche (funzione derivata da create_trap_session).
	 *
	 *  @param[in]   sink      identificativo destinatario della trap
	 *  @param[in]   sinkport  numero porta 
	 *  @param[in]   com       community
	 *  @param[in]   version   versione SNMP
	 *  @param[in]   pdutype   tipo PDU (Protocol Data Unit)
	 *
	 *  @return      descrittore sessione creata, NULL in caso di errore
	 */
	template <typename MIBTYPE>
	void* traps<MIBTYPE>::createSession(string sink, u_short sinkport, string com, int version, int pdutype) {
		netsnmp_transport *t;
		netsnmp_session session, *sesp;

		memset(&session, 0, sizeof (netsnmp_session));
		session.version = version;

		session.community = (u_char *) com.data();
		session.community_len = com.length();

		/*
		 * for informs, set retries to default
		 */
		if (SNMP_MSG_INFORM == pdutype) {
			session.timeout = SNMP_DEFAULT_TIMEOUT;
			session.retries = SNMP_DEFAULT_RETRIES;
		}

		/*
		 * if the sink is localhost, bind to localhost, to reduce open ports.
		 */
		if ((netsnmp_ds_get_string(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_CLIENT_ADDR) == NULL) &&
				(sink == "localhost") || (sink == "127.0.0.1"))
			session.localname = strdup("localhost");

		t = netsnmp_tdomain_transport_full("snmptrap", sink.c_str(), 0, NULL, NULL /*sinkport*/);
		if (t != NULL) {
			sesp = snmp_add(&session, t, NULL, NULL);

			if (sesp) {
				if (add_trap_session(sesp, pdutype, (pdutype == SNMP_MSG_INFORM), version))
					return sesp;
			}
		}
		/*
		 * diagnose snmp_open errors with the input netsnmp_session pointer 
		 */
		snmp_sess_perror("snmp subagent: createSession", &session);
		snmp_log(LOG_ERR, "createSession failed (%s).\n", sink.c_str());

		if (session.localname != NULL) {
			free(session.localname);
		}

		return NULL;
	}

	/**
	 *  Elimina la sessione indicata.
	 *
	 *  @param[in]   ss  descrittore sessione da eliminare
	 */
	template <typename MIBTYPE>
	void traps<MIBTYPE>::deleteSession(void* ss) {
		struct snmp_session* sesp = (snmp_session*) ss;

		if (sesp->localname) {
			free(sesp->localname);
			sesp->localname = NULL;
		}

		snmp_close(sesp);

		if (!remove_trap_session(sesp)) {
			snmp_sess_perror("snmp subagent: deletesession", sesp);
			snmp_log(LOG_ERR, "deleteSession failed.\n");
		}
	}

}

#endif


/**
* Telefin SNMP Exporter Service
*
* SNMP AgentX SubAgent per esportszione dati da DB STLC1000
* 
*
* reference.cpp - Modulo acquisizione dati da DB.
*
* @author Enrico Alborali, Paolo Colli, Magda Bendazzoli
* @version 1.0.0.0 27/06/2012
* @copyright 2012 Telefin S.p.A.
*/

// Inclusioni standard
#include "stdafx.h"
#include "reference.h"
#include <math.h>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std; 
using namespace subagent;

namespace subagent{

	/**
	 *  Mutex per sincrionizzazione accesso al DB.
	 */
	static mutex dbMutex; // = new mutex;

	/**
	* Costruttore della classe reference.
	*/
	reference::reference(){
	}

	/**
	* Distruttore della classe reference.
	*/	 
	reference::~reference(){
	}

	/**
	 *  Occupa accesso al DB. Se occupato attende.
	 */
	void reference::dbLock (void)
	{
		dbMutex.lock();
	}

	/**
	 *  Libera DB.
	 */
	void reference::dbUnlock (void)
	{
		dbMutex.unlock();
	}

	// Applica il filtro alle righe ottenute dalla query.
	//  @return  true se riga valida.
	bool reference::sql_filter (ElencoMIB& el, SACommand& cmd)
	{
		bool filter = true;

		try {
			if (!el.fltQuery.DBparameter.empty()) {
				string sValue = (const char*)cmd.Field(el.fltQuery.DBparameter.c_str()).asString();
				if (el.fltQuery.ParameterValue.find(sValue.substr(el.fltQuery.Position,el.fltQuery.Length)) == string::npos)
					filter = false;
			}
		}
		catch(...) {
			filter = false;
		}
		return filter;
	}

	// cerco la riga con i parametri StrID, FieldID corretti (se popolati).
	// @return 1 se la riga � corretta.
	bool reference::isRightRow(ElementoMIB& element, SACommand& cmd){

		if(!element.StrID.empty()) {					
			if(element.StrID!=(const char*)cmd.Field("StrID").asString())
				return false;

			if(!element.FieldID.empty()) {
				if(element.FieldID!=(const char*)cmd.Field("FieldID").asString())
					return false;

				if(!element.ArrayID.empty()) {
					if(element.ArrayID!=(const char*)cmd.Field("ArrayID").asString())
						return false;
				}
			}
		}
		return true;
	}

	/**
	* Connessione al DB.
	* @return true se operazione riuscita. 
	*/

	bool reference::DBconnect(DBConfig* dbcfg){
		try
		{
			this->dbcfg = dbcfg;

			if(!(this->dbcfg->getPersistent() && this->con.isConnected())){

				string databasename=this->dbcfg->getHost() +"@"+ this->dbcfg->getHostname();
				string user="";
				string password="";
				
				if(this->dbcfg->getUser().find("WindowsAuthentication")==string::npos){
					user=this->dbcfg->getUser();
					password=this->dbcfg->getPassword();
				}
				
				this->con.Connect(
					databasename.c_str(),	// database name
					user.c_str(),			// user name
					password.c_str(),		// password
					SA_SQLServer_Client);
			}

			if(this->con.isConnected())
				return true;
			else
				return false;
		}
		catch(SAException &x)
		{	
			try
			{
				this->con.Disconnect();				
			}
			catch(SAException &)
			{
			}

			snmp_log(LOG_ERR, "%s: Database reading.\n", (const char*)x.ErrText());
			printf("%s\n", (const char*)x.ErrText());
			return false;
		}
		
	}

	/**
	* Disconnessione dal DB.
	* @return true se operazione riuscita. 
	*/

	bool reference::DBdisconnect (void){
		try
		{
			if(!this->dbcfg->getPersistent() && this->con.isConnected())
				this->con.Disconnect();
			
			if(this->con.isConnected())
				return false;
			else
				return true;	
		}
		catch(SAException &x)
		{	
			try
			{
				this->con.Disconnect();				
			}
			catch(SAException &)
			{
			}

			snmp_log(LOG_ERR, "%s: Database reading.\n", (const char*)x.ErrText());
			printf("%s\n", (const char*)x.ErrText());
			return false;
		}
			
	}

	/**
	* Esegue la query.
	* @param[in] el elenco di elementi della mib da ricavare tramite la query
	* @logerr[in] se a true indica che il ritorno nullo della query � un errore
	*/
	void reference::sql_value(ElencoMIB& el, bool logerr){
	    
		try
		{			
			if(!this->con.isConnected())
				this->DBconnect(this->dbcfg);

			SACommand cmd;
			cmd.setConnection(&this->con);

			cmd.setCommandText(el.sqlQuery.c_str());
			
			bool queryExecuted = false;
			int row = 1;

			cmd.Execute();
			while (cmd.FetchNext()) {
				queryExecuted = true;
				if (sql_filter(el,cmd)) {

					for (int i=0; i < el.n_elementi; i++) {
						if (isRightRow(el.elementi[i], cmd)) {
							Conversion value((const char*)cmd.Field(el.elementi[i].DBparameter.c_str()).asString());
							value.convert(el.elementi[i]);
							this->sendToMIB(el.elementi[i], row, value);
						}
					}
					row++;
				}
			}
			cmd.Close();
			if (!queryExecuted && logerr) {
				snmp_log(LOG_WARNING, "Query data unavailable (%s).\n", (const char*)el.sqlQuery.c_str());
			}
		}
		catch(SAException &x)
		{

			try
			{
				// on error rollback changes
				con.Rollback();
			}
			catch(SAException &)
			{
			}

			snmp_log(LOG_ERR, "%s: Database reading.\n", (const char*)x.ErrText());
			printf("%s\n", (const char*)x.ErrText());
		}

	}

	/**
	* Costruttore della classe conversion. 
	*/
	Conversion::Conversion (void)
	{
		this->strValue = "";
		this->bufValue = NULL;
		this->intValue = 0;
		
		this->type     = 0;
		this->pInteger = NULL;
		this->pString  = NULL;
		this->pBits    = NULL;
	}


	/**
	* Costruttore della classe conversion. 
	*/
	Conversion::Conversion (const char* value)
	{
		this->strValue = value;
		this->bufValue = NULL;
		this->intValue = 0;
		
		this->type     = 0;
		this->pInteger = NULL;
		this->pString  = NULL;
		this->pBits    = NULL;
	}


	/**
	* Distruttore della classe conversion. 
	*/
	Conversion::~Conversion (void)
	{
		if (this->bufValue)  delete this->bufValue;
		if (this->pInteger)  delete this->pInteger;
		if (this->pString)   delete this->pString;
		if (this->pBits)     delete this->pBits;
	}

	/**
	* Conversione da stringa ad esadecimale: inserisce il risultato nel buffer this->bufValue.
	* @param[in] s stringa contenente l'informazione da convertire
	* @param[in] m maschera da applicare
	* @param[in] f fattore da applicare
	*/
	void Conversion::str2hex (string& s, int m, int f)
	{
		if((s.length() % 2) == 0) {
			int pos  = 0;
			int size = s.length()/2;

			if (this->bufValue) {
				pos = this->bufValue->size;
				this->bufValue->append(size);
			}
			else
				this->bufValue = new bvalue(size);

			for(int i=0; i < size; i++, pos++){	
				unsigned int x;
				std::stringstream ss;
				ss << std::hex << s.substr(2*i,2);
				ss >> x;
				
				if (m)  x &= m;

				if (f > 0)       x *= f;
				else if (f < 0)  x /= abs(f);

				this->bufValue->pointer[pos] = (uint8_tt)x;
			}
		}
	}

	/**
	* Conversione da stringa ad intero: inserisce il risultato in this->intValue.
	* @param[in] s stringa contenente l'informazione da convertire
	* @param[in] m maschera da applicare
	* @param[in] f fattore da applicare
	*/
	void Conversion::str2int (string& s, int m, int f)
	{
		if((s.length() % 2) == 0) {
			int count = s.length() / 2;

			for(int i=0; i < count; i++){	
				unsigned int x;
				std::stringstream ss;
				ss << std::hex << s.substr(2*i,2);
				ss >> x;
				
				if (m)  x &= m;

				if (f > 0)       x *= f;
				else if (f < 0)  x /= abs(f);

				this->intValue += x;
			}
		}
	}

	/**
	* Conversione della stringa ottenuta dalla query (this->strValue) nel formato appropriato.
	* @param[in] el elenco di elementi della mib da ricavare tramite la query	
	*/
	void Conversion::convert (ElementoMIB& el)
	{
		switch (el.Operation) {

			case OPERATION_INTEGER:
				this->type     = VINTEGER;
				this->pInteger = new integer;
				this->pInteger->set(atoi(this->strValue.c_str()));
				break;

			case OPERATION_BUFCONVERSION:
				for (int i=0; i < el.DataLen; i++) {
					string s = this->strValue.substr(el.Data[i].Position, el.Data[i].Length);
					this->str2hex(s, el.Data[i].Mask, el.Data[i].Factor);
				}
				if(el.Type == ASN_INTEGER){
					this->type     = VINTEGER;
					this->pInteger = new integer;
					this->pInteger->set(static_cast<int>(this->bufValue->pointer[0]));
				}else if (el.Type == ASN_BIT_STR){
					this->type  = VBITS;
					this->pBits = new bits;
					this->pBits->set(this->bufValue->pointer, this->bufValue->size);					
				}
				break;

			case OPERATION_UINT16CONVERSION:
				for (int i=0; i < el.DataLen; i++) {
					string s = this->strValue.substr(el.Data[i].Position, el.Data[i].Length);
					this->str2int(s, el.Data[i].Mask, el.Data[i].Factor);
				}
				if(el.Type == ASN_INTEGER){
					this->type     = VINTEGER;
					this->pInteger = new integer;
					this->pInteger->set(this->intValue);
				}else if (el.Type == ASN_OCTET_STR){
					this->type    = VDISPLAYSTRING;
					this->pString = new displaystring;
					
					ostringstream oss;
					oss << setfill('0') << setw(el.N_ByteRes) << this->intValue;
					this->pString->set(oss.str());					
				}
				break;


			case OPERATION_INT16CONVERSION:
				for (int i=0; i < el.DataLen; i++) {
					string s = this->strValue.substr(el.Data[i].Position, el.Data[i].Length);
					this->str2int(s, el.Data[i].Mask, el.Data[i].Factor);
				}
				if(el.Type == ASN_INTEGER){
					this->type     = VINTEGER;
					this->pInteger = new integer;
					this->pInteger->set((__int16)this->intValue);
				}
				break;

			case OPERATION_SLOT:
				for (int i=0; i < el.DataLen; i++) {
					string s = this->strValue.substr(el.Data[i].Position, el.Data[i].Length);
					this->str2hex(s, el.Data[i].Mask, el.Data[i].Factor);
				}
				if(el.Type == ASN_OCTET_STR){
					this->type    = VDISPLAYSTRING;
					this->pString = new displaystring;
					
					ostringstream oss;
					oss << setfill('0') << setw(1) << (int)this->bufValue->pointer[0] << "." << setw(2) << (int)this->bufValue->pointer[1] << "."  << setw(1) << (int)this->bufValue->pointer[2];
					this->pString->set(oss.str());
				}
				break;

			case OPERATION_NONE:
				this->type    = VDISPLAYSTRING;
				this->pString = new displaystring;
				this->pString->set(this->strValue);
				break;
		}
	}

	/**
	* Ritorna la tipologia di dato voluta (e convertita) per l'elemento MIB.
	*/
	int Conversion::getType (void)
	{
		return this->type;
	}

	/**
	* Ritorna il valore intero  ottenuto dal risultato opportunamente convertito della query.
	*/
	integer& Conversion::getInteger (void)
	{
		return *this->pInteger;
	}

	/**
	* Ritorna la stringa ottenuta dal risultato opportunamente convertito della query.
	*/
	displaystring& Conversion::getString (void)
	{
		return *this->pString;
	}

	/**
	* Ritorna lo stream esadecimale ottenuto dal risultato opportunamente convertito della query.
	*/
	bits& Conversion::getBits (void)
	{
		return *this->pBits;
	}

};
/**
 * Telefin SNMP Exporter Service
 *
 * SNMP AgentX SubAgent per esportszione dati da DB STLC1000
 *
 * diagAIMCTS0.h - Modulo di gestione diagnostica CTS di stazione.
 *
 * @author Paolo Colli
 * @version 1.0.0.0 03/07/2012
 * @copyright 2012 Telefin S.p.A.
 */

#ifndef DIAGAIMCTS0_H
#define DIAGAIMCTS0_H

#include "hwAIMCTS0.h"
#include "dbAIMCTS0.h"
#include "srvConfig.h"
#include "diagDEVICE.h"

using namespace std;
using namespace mib;
using namespace subagent;
using namespace mib_hwAIMCTS0;

namespace subagent {

	class diagAIMCTS0 : public diagDEVICE<hwAIMCTS0> {
		
		dbAIMCTS0* pDatabase;

	public:
		diagAIMCTS0  (DBConfig* dbcfg, DevConfig* devcfg);
		~diagAIMCTS0 (void);

		void Update (void);

	private:
		friend bool mibObjectWritten (mib_hwAIMCTS0::scalar::scalar_id_t id, void* obj);
		friend bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
	};

static bool mibObjectWritten (mib_hwAIMCTS0::scalar::scalar_id_t id, void* obj);
static bool mibTrapsControl  (netsnmp_tdata_row* row, RowAction act, void* obj);
}

#endif
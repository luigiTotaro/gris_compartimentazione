//==============================================================================
// Telefin SNMP Exporter Service
//------------------------------------------------------------------------------
// Utility.cpp  modulo di definizione funzioni di utility
//				Funzioni estratte dal modulo SYSKernel.cpp
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//				Paolo Colli (paolo.colli@telefin.it)
//------------------------------------------------------------------------------
// Version history: vedere in utility.h
//==============================================================================
#include "UtilityLibrary.h"
#include "exception.h"
#include "utility.h"

#define SNMP_APPLIC_SERVICE_KEY					"SYSTEM\\CurrentControlSet\\Services\\SNMPExporterService"
#define SNMP_APPLIC_LASTAUTOCHECK_VALUE_NAME	"LastAutoCheck"

using namespace subagent;

//==============================================================================
/// Funzione per creare una nuova chiave di registro
/// <keybase> specifica l'handler di una delle chiavi aperte da cui partire. Se
/// il parametro � nullo viene automaticamente utilizzato HKEY_LOCAL_MACHINE.
/// <kaypath> specifica il percorso e il nome della nuova chiave di registro che
/// si vuole creare (es: "SYSTEM\\CurrentControlSet\\Control\\ProductOptions").
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSCreateRegKey( void * keybase, char * keypath )
{
  int   ret_code = applyException::SYS_NO_ERROR;
  HKEY  hk      ; // Handler della chiave di registro
  DWORD dwDisp  ;
  TCHAR szBuf[MAX_PATH];

  if ( keypath != NULL )
  {
    // --- Preparo la stringa del percorso e nome chiave di registro ---
		if ( keybase == NULL )
    {
      keybase = (void*)HKEY_LOCAL_MACHINE;
    }
    wsprintf( szBuf, keypath );
    // --- Provo a scrivere la chiave ---
    if ( RegCreateKeyEx( (HKEY)keybase, szBuf,
         0, NULL, REG_OPTION_NON_VOLATILE,
		 KEY_WRITE, NULL, &hk, &dwDisp ) == ERROR_SUCCESS )
    {
      ret_code = SYSCloseRegKey( (void*)hk );
	}
	else
    {
      ret_code = applyException::SYS_REGKEY_WRITE_FAILURE;
    }
  }
  else
  {
    ret_code = applyException::SYS_INVALID_REGKEY_PATH;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una chiave di registro
/// <keybase> specifica l'handler di una delle chiavi aperte da cui partire. Se
/// il parametro � nullo viene automaticamente utilizzato HKEY_LOCAL_MACHINE.
/// <kaypath> specifica il percorso e il nome della chiave di registro che si
/// vuole eliminare (es: "SYSTEM\\CurrentControlSet\\Control\\ProductOptions").
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSDeleteRegKey( void * keybase, char * keypath )
{
  int ret_code = applyException::SYS_NO_ERROR;

  if ( keypath != NULL )
  {
    if ( keybase == NULL )
    {
	  keybase = (void*)HKEY_LOCAL_MACHINE;
	}
    if ( RegDeleteKey( (HKEY)keybase, keypath ) != ERROR_SUCCESS )
    {
      ret_code = applyException::SYS_REGKEY_DELETE_FAILURE;
    }
  }
  else
  {
    ret_code = applyException::SYS_INVALID_REGKEY_PATH;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere l'handler di una chiave di registro
/// <keybase> specifica l'handler di una delle chiavi aperte da cui partire. Se
/// il parametro � nullo viene automaticamente utilizzato HKEY_LOCAL_MACHINE.
/// <kaypath> specifica il percorso e il nome della chiave di registro che si
/// vuole ottenere (es: "SYSTEM\\CurrentControlSet\\Control\\ProductOptions").
/// <write_enable> indica se aprire l'handle in scrittura o in lettura
///
/// In caso di errore restituisce NULL.
///
/// \date [27.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void * SYSOpenRegKey( void * keybase, char * keypath, bool write_enable )
{
	void  * keyhandler = NULL ;
	HKEY    hk                ; // Handler della chiave di registro
	TCHAR   szBuf[MAX_PATH]   ;
	unsigned long samDesired  ;

	if ( keypath != NULL )
	{
		// --- Preparo la stringa del percorso e nome chiave di registro ---
		if ( keybase == NULL )
		{
			keybase = (void*)HKEY_LOCAL_MACHINE;
		}
		wsprintf( szBuf, keypath );
		// --- Provo a scrivere la chiave ---
		if (write_enable)
		{
			samDesired = KEY_WRITE;
		}
		else
		{
			samDesired = KEY_QUERY_VALUE;
		}
		if ( RegOpenKeyEx ( (HKEY)keybase, keypath, 0, samDesired, &hk ) == ERROR_SUCCESS )
		{
			keyhandler = (void*)hk;
		}
	}
  return keyhandler;
}

//==============================================================================
/// Funzione per chiudere un handler di una chiave di registro
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSCloseRegKey( void * keyhandler )
{
  int ret_code = applyException::SYS_NO_ERROR;

  if ( keyhandler != NULL )
  {
    if ( RegCloseKey( (HKEY)keyhandler ) != ERROR_SUCCESS )
    {
      ret_code = applyException::SYS_REGKEY_CLOSE_FAILURE;
	}
  }
  else
  {
    ret_code = applyException::SYS_INVALID_REGKEY_HANDLER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per impostare un valore in una chiave di registro
/// <type> � il tipo di valore della chiave di registro e pu� essere:
/// REG_BINARY: qualsiaisi tipo di dato binario
/// REG_DWORD: un numero a 32 bit
/// REG_DWORD_LITTLE_ENDIAN: un numero a 32 bit formato little-endian
/// REG_DWORD_BIG_ENDIAN: un numero a 32 bit formato big-endian
/// REG_EXPAND_SZ: una stringa NULL-terminata che contiene riferimenti non
///                espansi a variabili di ambiente (es: "%PATH%")
/// REG_LINK: un link simbolico unicode.
/// REG_MULTI_SZ: un array di stringhe NULL-terminate terminato da due NULL
/// REG_NONE: tipo non definito
/// REG_RESOURCE_LIST: una lista di risorse per un driver
/// REG_SZ: una stringa NULL-terminata
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSSetRegValue( void * keyhandler, char * name, void * value, int type, int size )
{
  int   ret_code = applyException::SYS_NO_ERROR;
  long  fun_code = 0;

  if ( keyhandler != NULL )
	{                                        
    if ( name != NULL )
    {
      if ( value != NULL && size > 0 )
      {
        fun_code = RegSetValueEx( (HKEY)keyhandler, (LPCTSTR)name, (DWORD)0,
                                  (DWORD)type, (CONST BYTE*)value, (DWORD)size );
        if ( fun_code != ERROR_SUCCESS )
        {
          ret_code = applyException::SYS_REGKEY_SETVALUE_FAILURE;
		}
      }
      else
      {
        ret_code = applyException::SYS_INVALID_REGKEY_VALUE;
      }
    }
    else
    {
      ret_code = applyException::SYS_INVALID_REGKEY_VALUENAME;
    }
  }
  else
  {
    ret_code = applyException::SYS_INVALID_REGKEY_HANDLER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere un valore di una chiave di registro
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [19.07.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void * SYSGetRegValue( void * keyhandler, char * name )
{
  void  * value         = NULL;
  DWORD   buffer_size   = 1024;
  char  * buffer[1024]        ;
  char  * value_buffer  = NULL;

  if ( keyhandler != NULL )
  {
    if ( name != NULL )
    {
      if ( RegQueryValueEx( (HKEY)keyhandler, (LPCTSTR)name, NULL, NULL, (LPBYTE)buffer, &buffer_size ) == ERROR_SUCCESS )
      {
        value_buffer = (char*)malloc( sizeof(char) * buffer_size ); // -MALLOC
        if ( value_buffer != NULL )
        {
          memcpy( value_buffer, buffer, sizeof(char) * buffer_size );
          value = (void*)value_buffer;
        }
      }
	}
  }

  return value;
}

//==============================================================================
/// Funzione per scrivere su chiave di registro lo UnixTime corrente
///
/// \date [27.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	UpdateLastAutoCheck( void )
{
	int                ret_code	= applyException::SYS_NO_ERROR;
	void*              key      = NULL;
	char*              value    = NULL;
	unsigned __int32   utime    = 0;
	int                size     = 0;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SNMP_APPLIC_SERVICE_KEY, true );
	if ( key != NULL )
	{
		utime = (unsigned __int32) ULGetUnixTime();
		size = 4;
		ret_code = SYSSetRegValue( key, SNMP_APPLIC_LASTAUTOCHECK_VALUE_NAME, &utime, REG_DWORD, size );
		SYSCloseRegKey( key );
	}
	else
	{
		ret_code = applyException::SYS_INVALID_REGKEY_PATH;
	}
	return ret_code;
}


echo
echo "*** AIMCTS0 ***"
echo
env MIBDIRS="+./" mib2c -c mib2c.telefin.cpp.conf TLF-AIMCTS0-MIB::hwAIMCTS0
echo
echo "*** AIMCTSN ***"
echo
env MIBDIRS="+./" mib2c -c mib2c.telefin.cpp.conf TLF-AIMCTSN-MIB::hwAIMCTSN
echo
echo "*** DT13000 ***"
echo
env MIBDIRS="+./" mib2c -c mib2c.telefin.cpp.conf TLF-DT13000-MIB::hwDT13000
echo
echo "*** DT13100 ***"
echo
env MIBDIRS="+./" mib2c -c mib2c.telefin.cpp.conf TLF-DT13100-MIB::hwDT13100
echo
echo "*** TA74000 ***"
echo
env MIBDIRS="+./" mib2c -c mib2c.telefin.cpp.conf TLF-TA74000-MIB::hwTA74000
echo
echo
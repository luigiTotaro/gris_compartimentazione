/*
 * Note: this file originally auto-generated by mib2c using
 *       : mib2c.telefin.cpp.conf 00000 2012-04-11 10:00:00Z dts12 $
 */
#ifndef HWDT13000_H
#define HWDT13000_H

#include "stdafx.h"

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "hwDT13000_alarmsTable.h"
#include "hwDT13000_trapsEnaTable.h"
#include "hwDT13000_scalar.h"

using namespace mib;

namespace mib_hwDT13000 {

	// Classe di definizione mib
	class hwDT13000 : virtual public state {

	// Inclusione di tutti gli oggetti che compongono la mib
	public:
		scalar scalarObj;
		alarmsTable alarmsTableObj;
		trapsEnaTable trapsEnaTableObj;

	// Creazione classe Singleton
	public:
		static hwDT13000* Instance();

	private:
		static hwDT13000* instance;

	// Costruttore istanza
	protected:
		hwDT13000 (void);

	// Definizione trap 
	private:
		const static oid snmptrap_oid[];

	public:
		std::string trapSystemSeverityKey	(void);
		std::string trapSystemSeverityOID	(void);
		int      trapSystemSeverityNotify (void);
  
		std::string trapAlarmInsertedKey	(void);
		std::string trapAlarmInsertedOID	(void);
		int      trapAlarmInsertedNotify (netsnmp_tdata_row* row);
  
		std::string trapAlarmChangedKey	(void);
		std::string trapAlarmChangedOID	(void);
		int      trapAlarmChangedNotify (netsnmp_tdata_row* row);
  
		std::string trapAlarmDeletedKey	(void);
		std::string trapAlarmDeletedOID	(void);
		int      trapAlarmDeletedNotify (netsnmp_tdata_row* row);
  
	};

}

#endif /* HWDT13000_H */

/*
 * Note: this file originally auto-generated by mib2c using
 *		: mib2c.telefin.cpp.conf 00000 2012-04-11 10:00:00Z dts12 $
 */

#include "stdafx.h"

#include <iostream>

#include "hwAIMCTSN.h"

using namespace std;
using namespace mib;

namespace mib_hwAIMCTSN {

	/**
	 *  OID oggetto che identifica le trap snmp
	 */
	const oid hwAIMCTSN::snmptrap_oid[] = {1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0};

	/**
	 *  Class singleton: crea istanza per la gestione della mib.
	 */
	hwAIMCTSN* hwAIMCTSN::instance = NULL;
	hwAIMCTSN* hwAIMCTSN::Instance()
	{
		if (instance == NULL) {
			instance = new(nothrow) hwAIMCTSN;
		}
		return instance;
	}

	hwAIMCTSN::hwAIMCTSN (void)
	{
	}

	/**
	 *  Restituisce chiave di identificazione trap
	 */
	std::string hwAIMCTSN::trapSystemSeverityKey (void)
	{
		return "trapSystemSeverity";
	}
 
	/**
	 *  Restituisce OID associata alla trap
	 */
	std::string hwAIMCTSN::trapSystemSeverityOID (void)
	{
		return ".1.3.6.1.4.1.34208.1.5.3.990.3.1";
	}
 
	/**
	 *  Metodo per la notifica della trap
	 */
	int hwAIMCTSN::trapSystemSeverityNotify (void)
	{
		netsnmp_variable_list  *var_list = NULL;
		oid trapSystemSeverity_oid[] = { 1,3,6,1,4,1,34208,1,5,3,990,3,1 };
		oid infoStationName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,6, 0 };
		oid infoDeviceName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,5, 0 };
		oid infoStationID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,7, 0 };
		oid devSeverity_oid[] = { 1,3,6,1,4,1,34208,1,5,3,1, 0 };

		/*
		 * Set the snmpTrapOid.0 value
		 */
		snmp_varlist_add_variable(&var_list,
			snmptrap_oid, OID_LENGTH(snmptrap_oid),
			ASN_OBJECT_ID,
			(const u_char*)trapSystemSeverity_oid, sizeof(trapSystemSeverity_oid)
			);

		/*
		 * Add any objects from the trap definition
		 */
		snmp_varlist_add_variable(&var_list,
			infoStationName_oid, OID_LENGTH(infoStationName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoStationName().pointer(), scalarObj.infoStationName().length()
			);
		snmp_varlist_add_variable(&var_list,
			infoDeviceName_oid, OID_LENGTH(infoDeviceName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoDeviceName().pointer(), scalarObj.infoDeviceName().length()
			);
		snmp_varlist_add_variable(&var_list,
			infoStationID_oid, OID_LENGTH(infoStationID_oid),
			ASN_INTEGER,
			(const u_char*)scalarObj.infoStationID().pointer(), scalarObj.infoStationID().length()
			);
		snmp_varlist_add_variable(&var_list,
			devSeverity_oid, OID_LENGTH(devSeverity_oid),
			ASN_INTEGER,
			(const u_char*)scalarObj.devSeverity().pointer(), scalarObj.devSeverity().length()
			);

		/*
		 * Send the trap to the list of configured destinations and clean up
		 */
		send_v2trap( var_list );
		snmp_free_varbind( var_list );

		return SNMP_ERR_NOERROR;
	}

	/**
	 *  Restituisce chiave di identificazione trap
	 */
	std::string hwAIMCTSN::trapAlarmInsertedKey (void)
	{
		return "trapAlarmInserted";
	}
 
	/**
	 *  Restituisce OID associata alla trap
	 */
	std::string hwAIMCTSN::trapAlarmInsertedOID (void)
	{
		return ".1.3.6.1.4.1.34208.1.5.3.990.3.2";
	}
 
	/**
	 *  Metodo per la notifica della trap
	 */
	int hwAIMCTSN::trapAlarmInsertedNotify (netsnmp_tdata_row* row)
	{
		netsnmp_variable_list  *var_list = NULL;
		oid trapAlarmInserted_oid[] = { 1,3,6,1,4,1,34208,1,5,3,990,3,2 };
		oid infoStationName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,6, 0 };
		oid infoDeviceName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,5, 0 };
		oid alarmUID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,1, *row->oid_index.oids };
		oid alarmDateTime_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,2, *row->oid_index.oids };
		oid alarmDeviceType_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,3, *row->oid_index.oids };
		oid alarmDeviceID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,4, *row->oid_index.oids };
		oid alarmSeverity_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,5, *row->oid_index.oids };
		oid alarmFlags_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,6, *row->oid_index.oids };
		oid alarmOID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,7, *row->oid_index.oids };
		alarmsTable::entry_t* entry = (alarmsTable::entry_t*)netsnmp_tdata_row_entry(row);

		/*
		 * Set the snmpTrapOid.0 value
		 */
		snmp_varlist_add_variable(&var_list,
			snmptrap_oid, OID_LENGTH(snmptrap_oid),
			ASN_OBJECT_ID,
			(const u_char*)trapAlarmInserted_oid, sizeof(trapAlarmInserted_oid)
			);

		/*
		 * Add any objects from the trap definition
		 */
		snmp_varlist_add_variable(&var_list,
			infoStationName_oid, OID_LENGTH(infoStationName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoStationName().pointer(), scalarObj.infoStationName().length()
			);
		snmp_varlist_add_variable(&var_list,
			infoDeviceName_oid, OID_LENGTH(infoDeviceName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoDeviceName().pointer(), scalarObj.infoDeviceName().length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmUID_oid, OID_LENGTH(alarmUID_oid),
			ASN_COUNTER,
			(const u_char*)entry->alarmUID.pointer(), entry->alarmUID.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDateTime_oid, OID_LENGTH(alarmDateTime_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmDateTime.pointer(), entry->alarmDateTime.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDeviceType_oid, OID_LENGTH(alarmDeviceType_oid),
			ASN_INTEGER,
			(const u_char*)entry->alarmDeviceType.pointer(), entry->alarmDeviceType.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDeviceID_oid, OID_LENGTH(alarmDeviceID_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmDeviceID.pointer(), entry->alarmDeviceID.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmSeverity_oid, OID_LENGTH(alarmSeverity_oid),
			ASN_INTEGER,
			(const u_char*)entry->alarmSeverity.pointer(), entry->alarmSeverity.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmFlags_oid, OID_LENGTH(alarmFlags_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmFlags.pointer(), entry->alarmFlags.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmOID_oid, OID_LENGTH(alarmOID_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmOID.pointer(), entry->alarmOID.length()
			);

		/*
		 * Send the trap to the list of configured destinations and clean up
		 */
		send_v2trap( var_list );
		snmp_free_varbind( var_list );

		return SNMP_ERR_NOERROR;
	}

	/**
	 *  Restituisce chiave di identificazione trap
	 */
	std::string hwAIMCTSN::trapAlarmChangedKey (void)
	{
		return "trapAlarmChanged";
	}
 
	/**
	 *  Restituisce OID associata alla trap
	 */
	std::string hwAIMCTSN::trapAlarmChangedOID (void)
	{
		return ".1.3.6.1.4.1.34208.1.5.3.990.3.3";
	}
 
	/**
	 *  Metodo per la notifica della trap
	 */
	int hwAIMCTSN::trapAlarmChangedNotify (netsnmp_tdata_row* row)
	{
		netsnmp_variable_list  *var_list = NULL;
		oid trapAlarmChanged_oid[] = { 1,3,6,1,4,1,34208,1,5,3,990,3,3 };
		oid infoStationName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,6, 0 };
		oid infoDeviceName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,5, 0 };
		oid alarmUID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,1, *row->oid_index.oids };
		oid alarmDateTime_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,2, *row->oid_index.oids };
		oid alarmDeviceType_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,3, *row->oid_index.oids };
		oid alarmDeviceID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,4, *row->oid_index.oids };
		oid alarmSeverity_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,5, *row->oid_index.oids };
		oid alarmFlags_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,6, *row->oid_index.oids };
		oid alarmOID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,7, *row->oid_index.oids };
		alarmsTable::entry_t* entry = (alarmsTable::entry_t*)netsnmp_tdata_row_entry(row);

		/*
		 * Set the snmpTrapOid.0 value
		 */
		snmp_varlist_add_variable(&var_list,
			snmptrap_oid, OID_LENGTH(snmptrap_oid),
			ASN_OBJECT_ID,
			(const u_char*)trapAlarmChanged_oid, sizeof(trapAlarmChanged_oid)
			);

		/*
		 * Add any objects from the trap definition
		 */
		snmp_varlist_add_variable(&var_list,
			infoStationName_oid, OID_LENGTH(infoStationName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoStationName().pointer(), scalarObj.infoStationName().length()
			);
		snmp_varlist_add_variable(&var_list,
			infoDeviceName_oid, OID_LENGTH(infoDeviceName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoDeviceName().pointer(), scalarObj.infoDeviceName().length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmUID_oid, OID_LENGTH(alarmUID_oid),
			ASN_COUNTER,
			(const u_char*)entry->alarmUID.pointer(), entry->alarmUID.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDateTime_oid, OID_LENGTH(alarmDateTime_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmDateTime.pointer(), entry->alarmDateTime.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDeviceType_oid, OID_LENGTH(alarmDeviceType_oid),
			ASN_INTEGER,
			(const u_char*)entry->alarmDeviceType.pointer(), entry->alarmDeviceType.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDeviceID_oid, OID_LENGTH(alarmDeviceID_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmDeviceID.pointer(), entry->alarmDeviceID.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmSeverity_oid, OID_LENGTH(alarmSeverity_oid),
			ASN_INTEGER,
			(const u_char*)entry->alarmSeverity.pointer(), entry->alarmSeverity.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmFlags_oid, OID_LENGTH(alarmFlags_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmFlags.pointer(), entry->alarmFlags.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmOID_oid, OID_LENGTH(alarmOID_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmOID.pointer(), entry->alarmOID.length()
			);

		/*
		 * Send the trap to the list of configured destinations and clean up
		 */
		send_v2trap( var_list );
		snmp_free_varbind( var_list );

		return SNMP_ERR_NOERROR;
	}

	/**
	 *  Restituisce chiave di identificazione trap
	 */
	std::string hwAIMCTSN::trapAlarmDeletedKey (void)
	{
		return "trapAlarmDeleted";
	}
 
	/**
	 *  Restituisce OID associata alla trap
	 */
	std::string hwAIMCTSN::trapAlarmDeletedOID (void)
	{
		return ".1.3.6.1.4.1.34208.1.5.3.990.3.4";
	}
 
	/**
	 *  Metodo per la notifica della trap
	 */
	int hwAIMCTSN::trapAlarmDeletedNotify (netsnmp_tdata_row* row)
	{
		netsnmp_variable_list  *var_list = NULL;
		oid trapAlarmDeleted_oid[] = { 1,3,6,1,4,1,34208,1,5,3,990,3,4 };
		oid infoStationName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,6, 0 };
		oid infoDeviceName_oid[] = { 1,3,6,1,4,1,34208,1,5,3,3,5, 0 };
		oid alarmUID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,1, *row->oid_index.oids };
		oid alarmDateTime_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,2, *row->oid_index.oids };
		oid alarmDeviceType_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,3, *row->oid_index.oids };
		oid alarmDeviceID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,4, *row->oid_index.oids };
		oid alarmSeverity_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,5, *row->oid_index.oids };
		oid alarmFlags_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,6, *row->oid_index.oids };
		oid alarmOID_oid[] = { 1,3,6,1,4,1,34208,1,5,3,9,1,1,7, *row->oid_index.oids };
		alarmsTable::entry_t* entry = (alarmsTable::entry_t*)netsnmp_tdata_row_entry(row);

		/*
		 * Set the snmpTrapOid.0 value
		 */
		snmp_varlist_add_variable(&var_list,
			snmptrap_oid, OID_LENGTH(snmptrap_oid),
			ASN_OBJECT_ID,
			(const u_char*)trapAlarmDeleted_oid, sizeof(trapAlarmDeleted_oid)
			);

		/*
		 * Add any objects from the trap definition
		 */
		snmp_varlist_add_variable(&var_list,
			infoStationName_oid, OID_LENGTH(infoStationName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoStationName().pointer(), scalarObj.infoStationName().length()
			);
		snmp_varlist_add_variable(&var_list,
			infoDeviceName_oid, OID_LENGTH(infoDeviceName_oid),
			ASN_OCTET_STR,
			(const u_char*)scalarObj.infoDeviceName().pointer(), scalarObj.infoDeviceName().length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmUID_oid, OID_LENGTH(alarmUID_oid),
			ASN_COUNTER,
			(const u_char*)entry->alarmUID.pointer(), entry->alarmUID.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDateTime_oid, OID_LENGTH(alarmDateTime_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmDateTime.pointer(), entry->alarmDateTime.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDeviceType_oid, OID_LENGTH(alarmDeviceType_oid),
			ASN_INTEGER,
			(const u_char*)entry->alarmDeviceType.pointer(), entry->alarmDeviceType.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmDeviceID_oid, OID_LENGTH(alarmDeviceID_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmDeviceID.pointer(), entry->alarmDeviceID.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmSeverity_oid, OID_LENGTH(alarmSeverity_oid),
			ASN_INTEGER,
			(const u_char*)entry->alarmSeverity.pointer(), entry->alarmSeverity.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmFlags_oid, OID_LENGTH(alarmFlags_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmFlags.pointer(), entry->alarmFlags.length()
			);
		snmp_varlist_add_variable(&var_list,
			alarmOID_oid, OID_LENGTH(alarmOID_oid),
			ASN_OCTET_STR,
			(const u_char*)entry->alarmOID.pointer(), entry->alarmOID.length()
			);

		/*
		 * Send the trap to the list of configured destinations and clean up
		 */
		send_v2trap( var_list );
		snmp_free_varbind( var_list );

		return SNMP_ERR_NOERROR;
	}

}


/*
 * Note: this file originally auto-generated by mib2c using
 *		: mib2c.telefin.cpp.conf 00000 2012-04-11 10:00:00Z dts12 $
 */
#ifndef HWAIMCTSN_TONITELSTABLE_H
#define HWAIMCTSN_TONITELSTABLE_H

#include "stdafx.h"

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "mib_objects.h"

using namespace mib;

namespace mib_hwAIMCTSN {

	// Classe di definizione oggetto tonitelsTable
	class tonitelsTable : virtual public state, public mutex {

	public:
		// Costruttore istanza
		tonitelsTable (void);

		// Distruttore istanza
		~tonitelsTable (void);

		// Identificativi colonne tabella
		typedef enum {
			nullID = 0
			, tonitelIndex
			, tonitelSeverity
			, tonitelPathLAM
			, tonitelPathLAS
			, tonitelPathLBM
			, tonitelPathLBS
			, tonitelType
			, tonitelNumber
			, tonitelFlags
		} column_id_t;

	
		// Struttura dati di definizione oggetti in tabella
		typedef struct {
			// object index
			integer tonitelIndex;

			/* object value */
			integer tonitelSeverity;
			displaystring tonitelPathLAM;
			displaystring tonitelPathLAS;
			displaystring tonitelPathLBM;
			displaystring tonitelPathLBS;
			integer tonitelType;
			displaystring tonitelNumber;
			bits tonitelFlags;

		} entry_t;

		// Puntatore alla tabella
		netsnmp_tdata* TableData;

		// Metodi di interfacciamento alla tabella
		netsnmp_tdata_row* Row     (
									 integer tonitelIndex
								   );
		netsnmp_tdata_row* GetRow  (
									 integer tonitelIndex
								   );
		void               ClrRow  (netsnmp_tdata_row* row);
		void               DelRow  (netsnmp_tdata_row* row);
		netsnmp_tdata_row* NextRow (netsnmp_tdata_row* row);
		column_id_t        NextCol (column_id_t column);
		int                Type    (column_id_t column);
		size_t             Size    (column_id_t column);
		std::string        Key     (netsnmp_tdata_row* row, column_id_t column);
		std::string        OID     (netsnmp_tdata_row* row, column_id_t column);
		entry_t&           Entry   (netsnmp_tdata_row* row);

		// Metodi di gestione tabella
		void Initialize (void);

		netsnmp_tdata_row* CreateEntry (netsnmp_tdata* table_data
									   ,integer tonitelIndex
									   );
		void RemoveEntry  (netsnmp_tdata* table_data, netsnmp_tdata_row* row);
		void RangeEntry   (entry_t* entry);
		void DefaultEntry (entry_t* entry);

	};

// Valori per oggetto tonitelSeverity
#define severityINFO -255
#define severityNORMAL 0
#define severityWARNING 1
#define severityERROR 2
#define severityDISABLE 9
#define severityUNKNOWN 255

// Valori per oggetto tonitelType
#define phoneCTS 48
#define phoneDigitelConsoleDM 49
#define phoneDigitelConsoleDCO 50
#define phoneDigitelConsoleDOTE 51
#define phoneAlongTrack 52
#define phonePlatform 53
#define phoneOffice 54
#define phoneMultiLine 55
#define phoneISDNConsoleDM 56
#define phoneISDNConsoleDCO 57
#define phoneISDNConsoleDOTE 58
#define phoneISDNConsoleStandard 64

// Valori per oggetto tonitelFlags
#define tonitelResetDone 1
#define tonitelTrazCallActive 2
#define tonitelMovCallActive 3
#define tonitelDoorOpen 4
#define tonitelBrokenHandset 5
#define tonitelHandsetBadPosition 6
#define tonitelPTTBlockedClosed 7
#define tonitelNotReacheableFromSlaveLineB 12
#define tonitelNotReacheableFromMasterLineB 13
#define tonitelNotReacheableFromSlaveLineA 14
#define tonitelNotReacheableFromMasterLineA 15

}

#endif /* HWAIMCTSN_TONITELSTABLE_H */

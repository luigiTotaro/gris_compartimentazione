﻿using System;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using System.Windows.Threading;
using GrisSuite.Web.Client.Cache;
using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.Classes;
using GrisSuite.Web.Map.MapService;
using GrisSuite.Web.Map.StatusCountersService;
using GrisSuite.Web.Map.UserControls;

namespace GrisSuite.Web.Map
{
	public partial class MainPage
	{
		private readonly StatusCountersServiceClient serviceCounter;
		private readonly MapServiceClient mapServiceClient;
		private GrisMap currentMap;
		private const byte CLASSIFICATION_STATUS_FILTER_ID = 4; // Classificazione "Filtro per stato"
		private const int ALL_STATUS = -2;
		private readonly DispatcherTimer hoverTimer;
		private readonly DispatcherTimer delayedWaitingAnimationTimer;
		private readonly DispatcherTimer autoRefreshTimer;
		private readonly DispatcherTimer delayedAutoRefreshTimer;

		private DateTime? hoverTriggered;
		private DateTime? unhoverTriggered;
		private ElementEventArgs hoverElement;
		private byte lastClassificationId = 1;
		private int lastFilterSystemID;
		private byte lastHideNotActiveNodes;

		private readonly object locker = new object();
		private Guid? lastObjectStatusid;

		private ObservableCollection<GrisMapLayer> currentMapLayers;

		private readonly string updateProgressDivJsFunctionName;
		private readonly string hideProgressDivJsFunctionName;
		private double _increaseQuota;
		private bool _selectedRepositionExecuted;

		private readonly IsolatedStorageSettings _userSettings;
		private readonly CacheUtility<SerializableMapElements> _mapElementsCache;

		private readonly WaitingHandleCollection _waitingHandles = new WaitingHandleCollection();

		private readonly int autoRefreshTimeoutSeconds;

		public MainPage(string updateProgressDivJsFunctionName,
			string hideProgressDivJsFunctionName,
			string htmlControlHostId,
			int autoRefreshTimeoutSeconds)
		{
			try
			{
				this._userSettings = IsolatedStorageSettings.ApplicationSettings;
			}
			catch (IsolatedStorageException)
			{
				// Eccezione "Operation not permitted" che avviene su FireFox
			}

			this.InitializeComponent();

			this.autoRefreshTimeoutSeconds = autoRefreshTimeoutSeconds;

			this.hoverTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 2)};
			this.hoverTimer.Tick += this.hoverTimer_Tick;

			this.delayedWaitingAnimationTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 1, 0)};
			this.delayedWaitingAnimationTimer.Tick += this.delayedWaitingAnimationTimer_Tick;

			if (this.autoRefreshTimeoutSeconds > 0)
			{
				this.autoRefreshTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, this.autoRefreshTimeoutSeconds)};
				this.autoRefreshTimer.Tick += this.autoRefreshTimer_Tick;
				this.autoRefreshTimer.Start();

				this.delayedAutoRefreshTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 1)};
				this.delayedAutoRefreshTimer.Tick += this.delayedAutoRefreshTimer_Tick;
			}

			this.serviceCounter = ProxiesFactory.GetStatusCountersService();
			this.mapServiceClient = ProxiesFactory.GetMapService();

			this._mapElementsCache = new CacheUtility<SerializableMapElements>();
			this._mapElementsCache.NotEnoughFreeSpace += this.MapElementsCache_NotEnoughFreeSpace;

			if (!string.IsNullOrEmpty(updateProgressDivJsFunctionName))
			{
				this.updateProgressDivJsFunctionName = updateProgressDivJsFunctionName;
			}

			if (!string.IsNullOrEmpty(hideProgressDivJsFunctionName))
			{
				this.hideProgressDivJsFunctionName = hideProgressDivJsFunctionName;
			}

			this.serviceCounter.GetRegionCountersCompleted += (sender, e) =>
			{
				if ((e.Error == null) && (e.Result != null))
				{
					this.RegionName.Text = e.Result.Name;
					this.RegionCounterChart.LoadData(e.Result.Counters);
				}

				this.HideWaitingAnimation(this.serviceCounter);
				//Debug.WriteLine("GetRegionCountersAsync End	{0}:	{1}", Thread.CurrentThread.ManagedThreadId, DateTime.Now);
			};

			this.serviceCounter.GetZoneCountersCompleted += (sender, e) =>
			{
				if ((e.Error == null) && (e.Result != null))
				{
					this.RegionName.Text = e.Result.Name;
					this.RegionCounterChart.LoadData(e.Result.Counters);
				}

				this.HideWaitingAnimation(this.serviceCounter);
			};

			this.mapServiceClient.GetMapLayersByClassificationCompleted += (sender, e) =>
			{
				if ((e.Error == null) && (e.Result != null))
				{
					this.currentMapLayers = e.Result;

					if (this.PanelClassification.SelectedClassificationId == CLASSIFICATION_STATUS_FILTER_ID)
					{
						if (this.RegionCounterChart.SelectedStatus == null)
						{
							this.LoadGrisMapByStatus(ALL_STATUS);
						}
						else
						{
							this.LoadGrisMapByStatus((int) this.RegionCounterChart.SelectedStatus);
						}
					}
					else
					{
						this.RegionCounterChart.SelectedStatus = null;
						this.LoadGrisMapByLayers();
					}
				}

				this.HideWaitingAnimation(this.mapServiceClient);

				//Debug.WriteLine("GetMapLayersByClassificationAsync End	{0}:	{1}", Thread.CurrentThread.ManagedThreadId, DateTime.Now);
			};

			if (this._userSettings != null && this._userSettings.Contains("selectedObject"))
			{
				var parms = this._userSettings["selectedObject"].ToString().Split('|');

				try
				{
					long objectId;
					GrisMapElementType objectType;
					if (parms.Length == 2 && long.TryParse(parms[0], out objectId) &&
					    (objectType = (GrisMapElementType) Enum.Parse(typeof (GrisMapElementType), parms[1], true)) != GrisMapElementType.None)
					{
						this.RememberSelectedObject(objectId, objectType);
					}
				}
				catch (ArgumentException)
				{
					this.RememberSelectedObject(-1, GrisMapElementType.None);
				}
			}

			//Debug.WriteLine("GetMapLayersByClassificationAsync Start	{0}:	{1}", Thread.CurrentThread.ManagedThreadId, DateTime.Now);
			this.mapServiceClient.GetMapLayersByClassificationAsync(1);
			this.StartWaitingAnimation(this.mapServiceClient);

			//Debug.WriteLine("GetRegionCountersAsync Start	{0}:	{1}", Thread.CurrentThread.ManagedThreadId, DateTime.Now);
			this.serviceCounter.GetRegionCountersAsync(null);
			this.StartWaitingAnimation(this.serviceCounter);

			this.hoverTimer.Start();
		}

		#region Isolated Storage Space Management

		private void MapElementsCache_NotEnoughFreeSpace(object sender, NotEnoughSpaceEventArgs e)
		{
			this.IncreasePanelShow.Begin();
			this._increaseQuota = e.RequiredSpace*1.8;
			this.tbIncreaseQuotaText.Text = string.Format(this.tbIncreaseQuotaText.Text, ((this._increaseQuota/1000000).ToString("0.#") + " MB"));
		}

		private void btnIncreaseQuota_Click(object sender, RoutedEventArgs e)
		{
			if (this._mapElementsCache != null)
			{
				bool success = this._mapElementsCache.IncreaseQuota((long) Math.Ceiling(this._increaseQuota));
				if (success)
				{
					this.currentMap.CacheMapElements();
				}
			}
			this.IncreasePanelHide.Begin();
		}

		private void btnNotIncreaseQuota_Click(object sender, RoutedEventArgs e)
		{
			if (this._userSettings != null)
			{
				this._userSettings["UseCache"] = false;
			}

			this.IncreasePanelHide.Begin();
		}

		#endregion

		private void StartWaitingAnimation(object waitingHandle)
		{
			if (!string.IsNullOrEmpty(this.updateProgressDivJsFunctionName))
			{
				if (this._waitingHandles.IsEmpty())
				{
					if (!this.delayedWaitingAnimationTimer.IsEnabled)
					{
						this.delayedWaitingAnimationTimer.Start();
					}
				}

				this._waitingHandles.Add(waitingHandle);
			}
		}

		private void ShowWaitingAnimation()
		{
			ScriptObject script = (ScriptObject) HtmlPage.Window.GetProperty(this.updateProgressDivJsFunctionName);
			script.InvokeSelf();
		}

		private void HideWaitingAnimation(object waitingHandle)
		{
			if (!string.IsNullOrEmpty(this.hideProgressDivJsFunctionName))
			{
				this._waitingHandles.Remove(waitingHandle);

				if (this._waitingHandles.IsEmpty())
				{
					this.delayedWaitingAnimationTimer.Stop();

					ScriptObject script = (ScriptObject) HtmlPage.Window.GetProperty(this.hideProgressDivJsFunctionName);
					script.InvokeSelf();
				}
			}
		}

		private void delayedWaitingAnimationTimer_Tick(object sender, EventArgs e)
		{
			this.ShowWaitingAnimation();
			this.delayedWaitingAnimationTimer.Stop();
		}

		private void hoverTimer_Tick(object sender, EventArgs e)
		{
			lock (this.locker)
			{
				if (this.hoverTriggered == null && this.unhoverTriggered == null)
				{
					return;
				}
				if (this.hoverTriggered != null && this.unhoverTriggered == null)
				{
					//System.Diagnostics.Debug.WriteLine(string.Format("Hover:{0}", hoverTriggered.ToString()));
					this.LoadStatusInfo(this.hoverElement);
				}
				else if (this.hoverTriggered == null)
				{
					//System.Diagnostics.Debug.WriteLine(string.Format("Unhover:{0}", unhoverTriggered.ToString()));
					this.UnloadStatusInfo();
				}
				else
				{
					//System.Diagnostics.Debug.WriteLine(string.Format("Hover:{0},Unhover:{1}", hoverTriggered.ToString(), unhoverTriggered.ToString()));
					if (this.hoverTriggered >= this.unhoverTriggered)
					{
						this.LoadStatusInfo(this.hoverElement);
					}
					else
					{
						this.UnloadStatusInfo();
					}
				}

				this.hoverElement = null;
				this.hoverTriggered = null;
				this.unhoverTriggered = null;
			}
		}

		private void LoadStatusInfo(ElementEventArgs e)
		{
			if (this.lastObjectStatusid != e.ObjectStatusId)
			{
				if (e.ObjectTypeId == GrisMapElementType.Region)
				{
					this.serviceCounter.GetRegionCountersAsync(e.ObjectStatusId);
					this.StartWaitingAnimation(this.serviceCounter);
				}
				else
				{
					this.serviceCounter.GetZoneCountersAsync(e.ObjectStatusId);
					this.StartWaitingAnimation(this.serviceCounter);
				}
				this.lastObjectStatusid = e.ObjectStatusId;
			}
		}

		private void UnloadStatusInfo()
		{
			this.serviceCounter.GetRegionCountersAsync(null);
			this.StartWaitingAnimation(this.serviceCounter);
			this.lastObjectStatusid = null;
		}

		private void ClassificationPanel_Click(object sender, ClassificationClickEventArgs e)
		{
			this.lastClassificationId = e.Classification.Id;
			this.lastFilterSystemID = e.FilterSystemId;
			this.lastHideNotActiveNodes = e.HideNotActiveNodes;

			if (this._waitingHandles != null)
			{
				this._waitingHandles.Clear();
			}

			this.LoadMapLayersByClassificationAsync(this.lastClassificationId);
		}

		private void LoadMapLayersByClassificationAsync(byte classificationId)
		{
			this.mapServiceClient.GetMapLayersByClassificationAsync(classificationId);
			this.StartWaitingAnimation(this.mapServiceClient);
		}

		private void LoadGrisMapByLayers()
		{
			this.StartWaitingAnimation(this.mapServiceClient);

			if (this.MapPlaceholder.Child is GrisMap)
			{
				(this.MapPlaceholder.Child as GrisMap).Dispose();
			}

			this.currentMap = new GrisMap(this._selectedRepositionExecuted);
			this.currentMap.ElementHover += this.map_ElementHover;
			this.currentMap.ElementUnhover += this.map_ElementUnhover;
			this.currentMap.LayersImported += this.map_LayersImported;
			this.currentMap.LayersLoaded += this.map_LayersLoaded;
			this.currentMap.ElementClick += this.map_ElementClick;
			this.currentMap.SelectedRepositionExecuted += this.map_SelectedRepositionExecuted;
			this.currentMap.MapSelectionChanged += this.currentMap_MapSelectionChanged;
			this.currentMap.LoadGrisMap(this.currentMapLayers, null, this.lastClassificationId, this.lastFilterSystemID, this.lastHideNotActiveNodes,
				this._userSettings, this._mapElementsCache);
			this.MapPlaceholder.Child = this.currentMap;
			this.PanelClassification.Visibility = Visibility.Visible;
		}

		private void map_ElementClick(object sender, ElementEventArgs e)
		{
			if (e.ObjectTypeId == GrisMapElementType.Node)
			{
				this.StartWaitingAnimation(e);

				if (e.Status != GrisStatus.Maintenance)
				{
					HtmlPage.Window.CreateInstance("AreaRedirect", new object[] {"node", e.ObjectId.ToString()});
				}
			}
		}

		private void currentMap_MapSelectionChanged(object sender, SelectionEventArgs e)
		{
			if (e.IsSelection)
			{
				this.RememberSelectedObject(e.ObjectId, e.ObjectTypeId);
			}
			else
			{
				this.RememberSelectedObject(-1, GrisMapElementType.None);
			}
		}

		private void map_LayersImported(object sender, EventArgs e)
		{
		}

		private void map_LayersLoaded(object sender, EventArgs e)
		{
			this.HideWaitingAnimation(this.mapServiceClient);
		}

		private void map_SelectedRepositionExecuted(object sender, SelectionEventArgs e)
		{
			if (e != null)
			{
				this._selectedRepositionExecuted = this.currentMap.IsSelectedRepositionExecuted;
				this.RememberSelectedObject(e.ObjectId, e.ObjectTypeId);
			}
		}

		private void LoadGrisMapByStatus(int sevLevel)
		{
			this.StartWaitingAnimation(this.mapServiceClient);

			if (this.MapPlaceholder.Child is GrisMap)
			{
				(this.MapPlaceholder.Child as GrisMap).Dispose();
			}

			this.currentMap = new GrisMap(this._selectedRepositionExecuted);
			this.currentMap.ElementHover += this.map_ElementHover;
			this.currentMap.ElementUnhover += this.map_ElementUnhover;
			this.currentMap.LayersImported += this.map_LayersImported;
			this.currentMap.LayersLoaded += this.map_LayersLoaded;
			this.currentMap.ElementClick += this.map_ElementClick;
			this.currentMap.SelectedRepositionExecuted += this.map_SelectedRepositionExecuted;
			this.currentMap.MapSelectionChanged += this.currentMap_MapSelectionChanged;
			this.currentMap.LoadGrisMap(this.currentMapLayers, (GrisStatus) Enum.Parse(typeof (GrisStatus), sevLevel.ToString(), true),
				this.lastClassificationId, this.lastFilterSystemID, this.lastHideNotActiveNodes, this._userSettings, this._mapElementsCache);
			this.MapPlaceholder.Child = this.currentMap;
			this.PanelClassification.Visibility = Visibility.Visible;
		}

		private void RegionCounterChart_Click(object sender, LineChartClickEventArgs e)
		{
			if (this.currentMapLayers != null && this.currentMap != null)
			{
				if (this._waitingHandles != null)
				{
					this._waitingHandles.Clear();
				}

				if (this.PanelClassification.SelectedClassificationId == CLASSIFICATION_STATUS_FILTER_ID)
				{
					if (e.Deselected)
					{
						this.LoadGrisMapByStatus(ALL_STATUS);
					}
					else
					{
						if (this.RegionCounterChart.SelectedStatus.HasValue)
						{
							this.LoadGrisMapByStatus(this.RegionCounterChart.SelectedStatus.Value);
						}
					}
				}
				else
				{
					this.PanelClassification.SelectedClassificationId = CLASSIFICATION_STATUS_FILTER_ID;

					this.LoadMapLayersByClassificationAsync(CLASSIFICATION_STATUS_FILTER_ID);
				}
			}
		}

		private void map_ElementHover(object sender, ElementEventArgs e)
		{
			lock (this.locker)
			{
				this.hoverTriggered = DateTime.Now;
				this.hoverElement = e;
			}
		}

		private void map_ElementUnhover(object sender, ElementEventArgs e)
		{
			lock (this.locker)
			{
				this.unhoverTriggered = DateTime.Now;
			}
		}

		private void PanelClassification_Refresh(object sender, EventArgs e)
		{
			if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
			{
				if ((this.currentMap != null) && (this.currentMap.UsePositionCache) && (this._mapElementsCache != null))
				{
					this._mapElementsCache.EmptyApplicationStore();
				}
			}

			this.LoadMapLayersByClassificationAsync(this.lastClassificationId);
		}

		private void autoRefreshTimer_Tick(object sender, EventArgs e)
		{
			this.autoRefreshTimer.Stop();
			this.delayedAutoRefreshTimer.Start();
		}

		private void delayedAutoRefreshTimer_Tick(object sender, EventArgs e)
		{
			// Nel caso ci siano operazioni pendenti, aspetta il delay di delayedAutoRefreshTimer, prima di ritentare il refresh
			if ((this._waitingHandles != null) && (this._waitingHandles.IsEmpty()))
			{
				this.delayedAutoRefreshTimer.Stop();

				this._waitingHandles.Clear();

				this.LoadMapLayersByClassificationAsync(this.lastClassificationId);

				this.autoRefreshTimer.Start();
			}
		}

		private void RememberSelectedObject(long objectId, GrisMapElementType objectType)
		{
			HtmlDocument doc = HtmlPage.Document;
			HtmlElement hid = doc.GetElementById("ctl00_cphMainArea_hidSelectedObject");

			if (objectId > 0)
			{
				string objectString = string.Format("{0}|{1}", objectId, objectType);
				if (this._userSettings != null)
				{
					this._userSettings["selectedObject"] = objectString;
				}
				if (hid != null)
				{
					hid.SetAttribute("value", objectString);
				}
			}
			else
			{
				if (this._userSettings != null)
				{
					this._userSettings.Remove("selectedObject");
				}
				if (hid != null)
				{
					hid.SetAttribute("value", "");
				}
			}
		}
	}
}
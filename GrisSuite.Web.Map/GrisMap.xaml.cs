﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;

using GrisSuite.Web.Client.Cache;
using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.MapService;
using Infragistics.Silverlight.Map;

namespace GrisSuite.Web.Map
{
	public partial class GrisMap : UserControl, IDisposable
	{
		# region private fields
		private Collection<GrisMapLayer> _grisLayers;
		private GrisStatus? _statusFilter;
		private byte _classificationId;
		private bool _repositionFromQuerystring;
		private long _areaId;
		private int _layersLoaded;
		private int _layersImported;
		private int _layersToLoad;
		private bool _usePositionCache = true;
		private GrisMapElementType _areaType;
		private IsolatedStorageSettings _userSettings;
		private CacheUtility<SerializableMapElements> _mapElementsCache;
		private SerializableMapElements _cachedMapElements;
		private int _filterSystemID;
		private byte _hideNotActiveNodes;
		# endregion

		# region Events
		public event EventHandler<ElementEventArgs> ElementClick;
		public event EventHandler<ElementEventArgs> ElementHover;
		public event EventHandler<ElementEventArgs> ElementUnhover;
		public event EventHandler<SelectionEventArgs> MapSelectionChanged;

		public event EventHandler LayersImported;
		public event EventHandler LayersLoaded;
		public event EventHandler<SelectionEventArgs> SelectedRepositionExecuted;
		# endregion

		public bool IsSelectedRepositionExecuted { get; private set; }

		public GrisMap()
		{
			this.InitializeComponent();

			this.theMap.WindowZoomMaximum = 25;
			this.theMap.WindowZoomMinimum = 0;

#if (GRISSINO)
		    this.theMap.WindowZoomMaximum = 10;
		    this.theMap.WindowZoomMinimum = 6;

		    foreach (var element in this.theMap.LogicalChildren)
		    {
		        if (element.Name.Equals("theMapNavigationPane"))
		        {
		            element.Visibility = Visibility.Collapsed;
                    break;
		        }
		    }
#endif

			if (HtmlPage.Document.QueryString.ContainsKey("Aid") && HtmlPage.Document.QueryString.ContainsKey("Atype"))
			{
				this._repositionFromQuerystring = true;
				long.TryParse(HtmlPage.Document.QueryString["Aid"], out this._areaId);
				this._areaType = (GrisMapElementType)Enum.Parse(typeof(GrisMapElementType), HtmlPage.Document.QueryString["Atype"], true);
			}

			if (HtmlPage.Document.QueryString.ContainsKey("Upc"))
			{
				if (!Boolean.TryParse(HtmlPage.Document.QueryString["Upc"], out this._usePositionCache))
				{
					this._usePositionCache = true;
				}
			}
		}

		public GrisMap(bool selectedRepositionExecuted)
			: this()
		{
			this.IsSelectedRepositionExecuted = selectedRepositionExecuted;
		}

		public void Dispose()
		{

			while (this.theMap.Layers.Count > 0)
			{
				theMap.Layers[0].Brushes = null;
				theMap.Layers.RemoveAt(0);
			}
		}

		public bool UsePositionCache
		{
			get
			{
				if (this._userSettings != null)
				{
					bool positionCacheDisabled = (this._userSettings.Contains("UseCache") && !Convert.ToBoolean(this._userSettings["UseCache"]));
					return (this._usePositionCache && this._mapElementsCache != null && !positionCacheDisabled);
				}

				return false;
			}
		}

		public void LoadGrisMap(Collection<GrisMapLayer> grisLayers, GrisStatus? statusFilter, byte classificationId, int filterSystemID, byte hideNotActiveNodes, IsolatedStorageSettings userSettings = null, CacheUtility<SerializableMapElements> mapElementsCache = null)
		{
			this._classificationId = classificationId;
			this._grisLayers = grisLayers;
			this._statusFilter = statusFilter;
			this._layersLoaded = 0;
			this._layersToLoad = grisLayers.Count;
			this._userSettings = userSettings;
			this._mapElementsCache = mapElementsCache;
			this._filterSystemID = filterSystemID;
			this._hideNotActiveNodes = hideNotActiveNodes;

			if (this.UsePositionCache)
			{
				try
				{
					this._cachedMapElements = this._mapElementsCache != null ? this._mapElementsCache.LoadDataCustom() : this._cachedMapElements = null;
				}
				catch (Exception)
				{
					//TODO: log silverlight
					this._cachedMapElements = null;
				}
			}

			foreach (var grisLayer in this._grisLayers)
			{
				bool addLayer = false;
				bool addReader = false;
				GrisMapLayer layer = grisLayer;
				MapLayer mapLayer = this.theMap.Layers.SingleOrDefault(x => x.LayerName == layer.LayerName);

				if (mapLayer == null)
				{
					mapLayer = new MapLayer();
					addLayer = true;
				}

				ShapeFileReader reader = mapLayer.Reader as ShapeFileReader;
				if (reader == null)
				{
					reader = new ShapeFileReader();
					addReader = true;
				}

				DataMapping.Converter converter = new DataMapping.Converter();
				string dataMappingString = "Name=Name;Guid=Guid;Id=Id;";
				reader.DataMapping = converter.ConvertFromString(dataMappingString) as DataMapping;
				reader.Uri = string.IsNullOrEmpty(grisLayer.ShapefileUri) ? "shapefiles/empty" : grisLayer.ShapefileUri;
				if (addReader)
				{
					mapLayer.Reader = reader;
				}

				mapLayer.IsSensitive = false;
				mapLayer.FillMode = MapFillMode.None;
				mapLayer.Fill = (Brush)Application.Current.Resources["BackgroundMappa"];
				mapLayer.SymbolType = MapSymbolType.None;
				mapLayer.ValueTemplate = grisLayer.ShowElementsSymbols ? mapLayer.ValueTemplate : null;

				mapLayer.Imported += this.mapLayer_Imported;
				mapLayer.VisibleFromScale = grisLayer.VisibleFromScale;
				mapLayer.VisibleToScale = grisLayer.VisibleToScale;

				mapLayer.IsVisible = true;

				if (addLayer)
				{
					mapLayer.LayerName = grisLayer.LayerName;
					this.theMap.Layers.Add(mapLayer);
				}
			}
		}

		private void mapLayer_Imported(object sender, MapLayerImportEventArgs e)
		{
			if (e.Action == MapLayerImportAction.End)
			{
				GrisLayer2Map mapper;
				MapLayer mapLayer = (MapLayer)sender;
				GrisMapLayer grisLayer = this._grisLayers.SingleOrDefault(x => x.LayerName == mapLayer.LayerName);

				if (mapLayer != null && grisLayer != null)
				{
					this.RemoveEmptyElement(mapLayer);

					if (this._repositionFromQuerystring)
					{
						mapper = new GrisLayer2Map(this.theMap, grisLayer, mapLayer, _classificationId, this._filterSystemID, this._hideNotActiveNodes, this._cachedMapElements, this._areaId, this._areaType);
						mapper.SelectedElementFound += SelectedElementFound;
					}
					else
					{
						mapper = new GrisLayer2Map(this.theMap, grisLayer, mapLayer, _classificationId, this._filterSystemID, this._hideNotActiveNodes, this._cachedMapElements);
					}
					mapper.LayerLoaded += LayerLoaded;


					if (this._statusFilter == null)
					{
						mapper.AddElements();
					}
					else if (grisLayer.MapLayerType == GrisMapLayerType.RegionLayer)
					{
						mapper.AddElements();
					}
					else if (grisLayer.MapLayerType == GrisMapLayerType.StatusFilterLayer)
					{
						mapper.AddElementsByStatus((GrisStatus)this._statusFilter);
					}
				}

				this._layersImported++;
				if (this.LayersImported != null && this._layersImported >= this._layersToLoad)
				{
					# region Posizionamento su elemento selezionato

					if (this._cachedMapElements != null && this._areaId > 0)
					{
						//TODO: se sulla querystring venisse passato l'objectstatusid la ricerca sarebbe molto più veloce
						var selectedElements = (from selEl in this._cachedMapElements.Values
												where selEl.Id == this._areaId && selEl.TypeId == this._areaType
												select selEl).ToList();

						if (!this.IsSelectedRepositionExecuted)
						{
							if (selectedElements.Count() > 0)
							{
								var selectedElement = selectedElements[0];
								this.RestoreMapRect(selectedElement.ToMapElement());
								this.IsSelectedRepositionExecuted = true;
								if (this.SelectedRepositionExecuted != null) this.SelectedRepositionExecuted(this, new SelectionEventArgs { IsSelection = true, ObjectId = selectedElement.Id, ObjectTypeId = selectedElement.TypeId });

								this.LayersImported(this, null);
								return;
							}
						}
					}
					else if ((this._areaType == GrisMapElementType.Italia) && (this._areaId == 0))
					{
						this.theMap_WindowRectChanged(this, new MapWindowRectChangedEventArgs(new Rect(0, 0, 0, 0), new Point(0, 0), 1.0));
					}

					this.RestoreMapRect();
					this.LayersImported(this, null);

					# endregion
				}

#if (GRISSINO)
                if (this._areaId == 0)
                {
                    // Visualizzazione statica sul compartimento di Bari
                    this.RestoreMapRect(new Rect(1530336.09001466, -5149197.52634507, 553763.753165928, 323490.325807763));
                }
#endif
			}
		}

		void LayerLoaded(object sender, EventArgs e)
		{
			this._layersLoaded++;
			if (this.LayersLoaded != null && this._layersLoaded >= this._layersToLoad)
			{
				this.LayersLoaded(this, null);
				this.CacheMapElements();
			}
		}

		public void CacheMapElements()
		{
			if (this.UsePositionCache)
			{
				if (this._cachedMapElements == null) this._cachedMapElements = new SerializableMapElements(3000);
				//la cache non viene più ricostruita ogni volta per permettere di richiedere al db i soli elementi da visualizzare (problemi di switch tra classificazioni)
				//this._cachedMapElements.Clear();

				// popolo gli elementi da salvare
				foreach (var layer in this.theMap.Layers)
				{
					foreach (var element in layer.Elements)
					{
						if (element.GetProperty("Status") != null) this._cachedMapElements[new Guid(element.GetProperty("Guid").ToString())] = element; //il cast diretto va in errore con alcuni Guid
					}
				}

				this._mapElementsCache.SaveDataCustom(this._cachedMapElements);
			}
		}

		void SelectedElementFound(object sender, SelectedElementFoundEventArgs e)
		{
			if (!this.IsSelectedRepositionExecuted)
			{
				this.FitToElement(e.MapElement);
				this.IsSelectedRepositionExecuted = true;
				if (this.SelectedRepositionExecuted != null) this.SelectedRepositionExecuted(this, null);
			}
		}

		public static IEnumerable<MapElement> FindElementByGuid(MapElementCollection elements, Guid objectStatusId)
		{
			return elements.FindElement("Guid", new Regex(objectStatusId.ToString(), RegexOptions.IgnoreCase));
		}

		private void FitToElement(MapElement elementToFit)
		{
			if (!(elementToFit is SymbolElement))
			{
				this.theMap.WindowFit(elementToFit);

				if ((elementToFit.HasCustomProperty("TypeId")) && ((GrisMapElementType)elementToFit.GetProperty("TypeId") == GrisMapElementType.Zone))
				{
					this.theMap.WindowZoom = this.theMap.WindowZoom - 0.5; //zoomma leggermente meno per non tagliare gli elementi alle estremità    
				}
			}
		}

		private void RemoveEmptyElement(MapLayer mapLayer)
		{
			if (mapLayer.Elements.Count == 1 && mapLayer.Elements[0].Name == "(empty)")
			{
				mapLayer.Elements.RemoveAt(0);
			}
		}

		private void theMap_WindowRectChanged(object sender, MapWindowRectChangedEventArgs e)
		{
			if (e.WindowScale == 1.0 && this.ElementClick != null)
			{
				// Simulo un click su un elemento virtuale Italia
				ElementEventArgs ec = new ElementEventArgs();
				ec.ObjectId = 0;
				ec.ObjectStatusId = Guid.Empty;
				ec.ObjectTypeId = GrisMapElementType.Region;
				ec.Name = "Italia";
				this.ElementClick(this, ec);
			}

			this.SaveMapRect();
		}

		private void theMap_ElementClick(object sender, MapElementClickEventArgs e)
		{
			this.FitToElement(e.Element);

			if (this.ElementClick != null && e.Element.HasCustomProperty("Id") && e.Element.HasCustomProperty("TypeId") &&
				e.Element.HasCustomProperty("Guid"))
			{
				ElementEventArgs ec = new ElementEventArgs();
				ec.ObjectId = (long)e.Element.GetProperty("Id");
				ec.ObjectTypeId = (GrisMapElementType)e.Element.GetProperty("TypeId");
				ec.ObjectStatusId = (Guid)e.Element.GetProperty("Guid");
				ec.Status = (GrisStatus)e.Element.GetProperty("Status");
				ec.Name = e.Element.Name;
				this.ElementClick(this, ec);
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void Layer_ElementPrerender(object sender, MapElementRenderEventArgs e)
		{
			MapElement me = e.Element;
			SymbolElement se = me as SymbolElement;
			if (se != null &&
				(se.HasCustomProperty("FromCache") && !Convert.ToBoolean(se.GetProperty("FromCache"))) &&
				(se.HasCustomProperty("OffsetX") || se.HasCustomProperty("OffsetY")) &&
				(!se.HasCustomProperty("WindowScale") ||
				 (double)se.GetProperty("WindowScale") != this.theMap.WindowScale))
			{
				Point OriginalSymbolOrigin;
				se.SetProperty("WindowScale", this.theMap.WindowScale);
				if (!se.HasCustomProperty("OriginalSymbolOrigin"))
				{
					OriginalSymbolOrigin = se.SymbolOrigin;
					se.SetProperty("OriginalSymbolOrigin", se.SymbolOrigin);
				}
				else
				{
					OriginalSymbolOrigin = (Point)se.GetProperty("OriginalSymbolOrigin");
				}
				double offsetX = se.HasCustomProperty("OffsetX") ? (double)se.GetProperty("OffsetX") : 0;
				double offsetY = se.HasCustomProperty("OffsetY") ? (double)se.GetProperty("OffsetY") : 0;

				se.SymbolOrigin = new Point(OriginalSymbolOrigin.X - ((offsetX * 2000) / this.theMap.WindowScale),
											OriginalSymbolOrigin.Y - ((offsetY * 2000) / this.theMap.WindowScale));
			}

			if (me != null && me.HasCustomProperty("CaptionFromScale"))
			{
				double captionFromScale = (double)me.GetProperty("CaptionFromScale");
				me.Caption = Math.Round(this.theMap.WindowScale, 6) >= captionFromScale ? me.Name : string.Empty;
				if (Math.Round(this.theMap.WindowScale, 6) <= 2)
				{
					me.Layer.Foreground = new SolidColorBrush(Colors.Black);
					me.Layer.FontWeight = FontWeights.Bold;
				}
				else
				{
					me.Layer.Foreground = new SolidColorBrush(Color.FromArgb(255, 169, 169, 169));
					me.Layer.FontWeight = FontWeights.Normal;
				}
			}
		}

		public void SaveMapRect()
		{
			if (this._userSettings != null) this._userSettings["WindowRect"] = this.theMap.WindowRect;
		}

		public void RestoreMapRect()
		{
			if (this._userSettings != null && this._userSettings.Contains("WindowRect")) this.RestoreMapRect((Rect)this._userSettings["WindowRect"]);
		}

		public void RestoreMapRect(MapElement mapElement)
		{
			this.RestoreMapRect(mapElement.WorldRect);

			if ((mapElement.HasCustomProperty("TypeId")) && ((GrisMapElementType)mapElement.GetProperty("TypeId") == GrisMapElementType.Zone))
			{
				this.theMap.WindowZoom = this.theMap.WindowZoom - 0.5; //zoomma leggermente meno per non tagliare gli elementi alle estremità    
			}
		}

		public void RestoreMapRect(Rect worldRect)
		{
			try
			{
				this.theMap.WindowRect = worldRect;
			}
			catch (Exception)
			{
				// "Cannot create viewport matrix"
			}
		}

		private void station_Hover(object sender, ElementEventArgs e)
		{
			if (this.ElementHover != null)
			{
				this.ElementHover(this, e);
			}
		}

		private void station_Unhover(object sender, ElementEventArgs e)
		{
			if (this.ElementUnhover != null)
			{
				this.ElementUnhover(this, e);
			}
		}

		private void station_Loaded(object sender, RoutedEventArgs e)
		{
			UserControls.Station station = sender as UserControls.Station;
			if (station != null)
			{
				station.FilterSystemId = this._filterSystemID;
			}
		}

		private void station_Click(object sender, ElementEventArgs e)
		{
			if (this.ElementClick != null) this.ElementClick(this, e);
		}

		private void theMap_ElementHover(object sender, MapElementHoverEventArgs e)
		{
			if (this.ElementHover != null && e.Element.HasCustomProperty("Id") && e.Element.HasCustomProperty("TypeId") &&
				e.Element.HasCustomProperty("Guid"))
			{
				ElementEventArgs ec = new ElementEventArgs
				{
					ObjectId = (long)e.Element.GetProperty("Id"),
					ObjectTypeId = (GrisMapElementType)e.Element.GetProperty("TypeId"),
					ObjectStatusId = (Guid)e.Element.GetProperty("Guid"),
					Status = (GrisStatus)e.Element.GetProperty("Status"),
					Name = e.Element.Name
				};
				this.ElementHover(this, ec);
			}
		}

		private void theMap_ElementUnhover(object sender, MapElementHoverEventArgs e)
		{
			if (this.ElementUnhover != null && e.Element.HasCustomProperty("Id") &&
				e.Element.HasCustomProperty("TypeId") && e.Element.HasCustomProperty("Guid"))
			{
				ElementEventArgs ec = new ElementEventArgs
				{
					ObjectId = (long)e.Element.GetProperty("Id"),
					ObjectTypeId = (GrisMapElementType)e.Element.GetProperty("TypeId"),
					ObjectStatusId = (Guid)e.Element.GetProperty("Guid"),
					Status = (GrisStatus)e.Element.GetProperty("Status"),
					Name = e.Element.Name
				};
				this.ElementUnhover(this, ec);
			}
		}

		private void theMap_MapMouseLeftButtonUp(object sender, MapMouseButtonEventArgs e)
		{
			var evArgs = new SelectionEventArgs
							{
								IsSelection = (e.Element != null), // il click sulle stazioni non viene considerato selezione
								ObjectId = (e.Element != null) ? (long)e.Element.GetProperty("Id") : 0,
								ObjectTypeId = (e.Element != null) ? (GrisMapElementType)e.Element.GetProperty("TypeId") : GrisMapElementType.None
							};
			if (MapSelectionChanged != null) this.MapSelectionChanged(this, evArgs);
		}

		private void theMap_ZoomRectChanged(object sender, MapRectChangedEventArgs e)
		{
			if (MapSelectionChanged != null) this.MapSelectionChanged(this, new SelectionEventArgs());
		}
	}

	public class ElementEventArgs : EventArgs
	{
		public long ObjectId { get; set; }
		public Guid ObjectStatusId { get; set; }
		public GrisMapElementType ObjectTypeId { get; set; }
		public string Name { get; set; }
		public GrisStatus Status { get; set; }
	}

	public class SelectionEventArgs : EventArgs
	{
		public bool IsSelection { get; set; }
		public long ObjectId { get; set; }
		public GrisMapElementType ObjectTypeId { get; set; }
	}
}
﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GrisSuite.Web.Map.Classes
{
	class WaitingHandleCollection
	{
		private object _sync = new object();
		private ICollection<object> _internalHandles = new Collection<object>(); //permette elementi duplicati

		public void Add ( object waitingHandle )
		{
			lock ( this._sync )
			{
				this._internalHandles.Add(waitingHandle);
			}
		}

		public void Remove ( object waitingHandle )
		{
			lock ( this._sync )
			{
				if ( this._internalHandles.Contains(waitingHandle) ) this._internalHandles.Remove(waitingHandle);
			}
		}

		public bool IsEmpty ()
		{
			lock ( this._sync )
			{
                //System.Diagnostics.Debug.WriteLine("+++++++++++WaitingHandleCollection-----------------");
                //foreach (var item in this._internalHandles)
                //{
                //    System.Diagnostics.Debug.WriteLine(item.ToString());
                //}
                //System.Diagnostics.Debug.WriteLine("-----------WaitingHandleCollection-----------------");

				return this._internalHandles.Count == 0;
			}
		}

        public void Clear ()
        {
            lock ( this._sync )
            {
                this._internalHandles.Clear();
            }
        }
	}
}

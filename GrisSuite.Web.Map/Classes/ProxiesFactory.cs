﻿
using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows;

using GrisSuite.Web.Map.MapService;
using GrisSuite.Web.Map.StatusCountersService;

namespace GrisSuite.Web.Map.Classes
{
	public static class ProxiesFactory
	{
		public static MapServiceClient GetMapService ()
		{
#if (DEBUG)
            return new MapService.MapServiceClient();
#else
            MapService.MapServiceClient ms = new MapService.MapServiceClient();
            ServiceEndpoint ep = ms.Endpoint;
            ep.Address = new EndpointAddress(AdaptUrl(ms.Endpoint.Address.ToString()));
            return new MapService.MapServiceClient("CustomBinding_IMapService1", ep.Address);
#endif
        }

		public static StatusCountersServiceClient GetStatusCountersService ()
		{

#if (DEBUG)
            return new StatusCountersService.StatusCountersServiceClient();
#else
            StatusCountersService.StatusCountersServiceClient ms = new StatusCountersService.StatusCountersServiceClient();
            ServiceEndpoint ep = ms.Endpoint;
            ep.Address = new EndpointAddress(AdaptUrl(ms.Endpoint.Address.ToString()));
            return new StatusCountersService.StatusCountersServiceClient("CustomBinding_IStatusCountersService1", ep.Address);
#endif
        }

        private static string AdaptUrl(string originalUriString)
        {
            Uri originalUri = new Uri(originalUriString);

            string part1 = Application.Current.Host.Source.GetComponents(UriComponents.AbsoluteUri, UriFormat.UriEscaped);
            part1 = part1.Substring(0, part1.IndexOf("ClientBin", 0,StringComparison.InvariantCultureIgnoreCase)-1);
            string part2 = originalUri.GetComponents(UriComponents.PathAndQuery, UriFormat.UriEscaped);

            return part1 + part2;
        }
	}
}

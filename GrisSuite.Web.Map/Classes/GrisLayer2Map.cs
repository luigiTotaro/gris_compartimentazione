﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Collections.ObjectModel;
using Infragistics.Silverlight.Map;
using System.Collections.Generic;
using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.MapService;
using System.Globalization;
using GrisSuite.Web.Client.Cache;

namespace GrisSuite.Web.Map
{
	public class GrisLayer2Map
	{
		private XamWebMap Map;
		private Rect WorldBounds;
		private double xMin = double.MaxValue;
		private double xMax = double.MinValue;
		private double yMin = double.MaxValue;
		private double yMax = double.MinValue;
		private byte ClassificationId;
		private SerializableMapElements _cacheBuffer;
		private DateTime? _mostRecentPositionUpdate;
		private int FilterSystemID;
		private byte HideNotActiveNodes;

		public GrisMapLayer CurrentGrisMapLayer { get; set; }
		public MapLayer CurrentMapLayer { get; set; }

		private long selectedElementId;
		private GrisMapElementType selectedElementType;

		//public delegate void SelectedElementFoundHandler ( object sender, SelectedElementFoundEventArgs e );
		public event EventHandler<SelectedElementFoundEventArgs> SelectedElementFound;
		public event EventHandler LayerLoaded;

		public GrisLayer2Map(XamWebMap map,
			GrisMapLayer grisMapLayer,
			MapLayer mapLayer,
			byte classificationId,
			int filterSystemID,
			byte hideNotActiveNodes,
			SerializableMapElements cacheBuffer)
			: this(map, grisMapLayer, mapLayer, classificationId, filterSystemID, hideNotActiveNodes, cacheBuffer, -1, default(GrisMapElementType))
		{
		}

		public GrisLayer2Map(XamWebMap map,
			GrisMapLayer grisMapLayer,
			MapLayer mapLayer,
			byte classificationId,
			int filterSystemID,
			byte hideNotActiveNodes,
			SerializableMapElements cacheBuffer,
			long selectedElementId,
			GrisMapElementType selectedElementType)
		{
			this.Map = map;
			this.CurrentGrisMapLayer = grisMapLayer;
			this.CurrentMapLayer = mapLayer;
			this.ClassificationId = classificationId;
			this.selectedElementId = selectedElementId;
			this.selectedElementType = selectedElementType;
			this._cacheBuffer = cacheBuffer;
			this.FilterSystemID = filterSystemID;
			this.HideNotActiveNodes = hideNotActiveNodes;

			if (this._cacheBuffer != null)
			{
				var minimumDate = new DateTime(1900, 1, 1);
				this._mostRecentPositionUpdate = this._cacheBuffer.Values.Max(elem => elem.PositionDate);
				this._mostRecentPositionUpdate = (this._mostRecentPositionUpdate < minimumDate) ? minimumDate : this._mostRecentPositionUpdate;
			}
		}

		public void AddElements()
		{
			//Debug.WriteLine("AddElements Start	{0}:	{1}", this.CurrentGrisMapLayer.LayerName, DateTime.Now);
			MapServiceClient sc = Classes.ProxiesFactory.GetMapService();
			sc.GetMapElementsByLayerCompleted += this.sc_GetMapElementsByLayerCompleted;
			sc.GetMapElementsByLayerAsync(this.CurrentGrisMapLayer.Id, this.ClassificationId, this._mostRecentPositionUpdate, this.FilterSystemID,
				this.HideNotActiveNodes);
		}

		public void AddElementsByStatus(GrisStatus grisStatus)
		{
			//Debug.WriteLine("AddElementsByStatus Start	{0}:	{1}", this.CurrentGrisMapLayer.LayerName, DateTime.Now);
			MapServiceClient sc = Classes.ProxiesFactory.GetMapService();
			sc.GetMapElementsByStatusCompleted += this.sc_GetMapElementsByStatusCompleted;
			sc.GetMapElementsByStatusAsync((int) grisStatus, this._mostRecentPositionUpdate, this.FilterSystemID, this.HideNotActiveNodes);
		}

		private void sc_GetMapElementsByLayerCompleted(object sender, GetMapElementsByLayerCompletedEventArgs e)
		{
			if ((e.Error == null) && (e.Result != null))
			{
				//Debug.WriteLine("AddElements Returned	{0}:	{1}", this.CurrentGrisMapLayer.LayerName, DateTime.Now);
				ObservableCollection<GrisMapElement> elements = e.Result;
				this.LoadLayer(elements);
				//Debug.WriteLine("AddElements End	{0}:	{1}", this.CurrentGrisMapLayer.LayerName, DateTime.Now);
			}
		}

		private void sc_GetMapElementsByStatusCompleted(object sender, GetMapElementsByStatusCompletedEventArgs e)
		{
			if ((e.Error == null) && (e.Result != null))
			{
				//Debug.WriteLine("AddElementsByStatus Returned	{0}:	{1}", this.CurrentGrisMapLayer.LayerName, DateTime.Now);
				ObservableCollection<GrisMapElement> elements = e.Result;
				this.LoadLayer(elements);
				//Debug.WriteLine("AddElementsByStatus End	{0}:	{1}", this.CurrentGrisMapLayer.LayerName, DateTime.Now);
			}
		}

		private void LoadLayer(ObservableCollection<GrisMapElement> elements)
		{
			GrisMapElement selectedElement = null;
			MapElement selectedMapElement = null;

			MapElementCollection mapNodes = new MapElementCollection();
			MapElementCollection mapZones = new MapElementCollection();
			MapElementCollection mapRegions = new MapElementCollection();

			#region Diagnostic

			TimeSpan currentIteration = TimeSpan.Zero;
			TimeSpan totIteration = TimeSpan.Zero;
			TimeSpan minIteration = TimeSpan.MaxValue;
			TimeSpan maxIteration = TimeSpan.MinValue;
			int iterationCounter = 0;
			DateTime iterationTime = DateTime.Now;

			#endregion

			#region Elements Initialization

			foreach (GrisMapElement element in elements)
			{
				Rect newWorldRect = Rect.Empty;
				MapElement mapElement = null;

				if (this.CurrentMapLayer != null)
				{
					mapElement = GrisMap.FindElementByGuid(this.CurrentMapLayer.Elements, element.ObjectStatusId).FirstOrDefault();
				}

				if (mapElement != null)
				{
					// SurfaceElements (Compartimenti), già inclusi nel layer
					UpdateWorldBounds(UnprojectFromMap(mapElement.WorldRect));
					newWorldRect = mapElement.WorldRect;

					if (element.MapElementType == GrisMapElementType.Region)
					{
						SurfaceElement surfaceElement = (SurfaceElement) mapElement;
						if ((this.FilterSystemID == 0) && (this.CurrentGrisMapLayer.ShowElementsStatus))
						{
							surfaceElement.Fill = this.GetStatusBrush(element.Status, true);
						}

						if (this.CurrentGrisMapLayer.ShowElementsTooltips)
						{
							if (this.FilterSystemID > 0)
							{
								surfaceElement.ToolTip = string.Format("{0}", element.Name);
							}
							else
							{
								surfaceElement.ToolTip = string.Format("{0} ({1})", element.Name, element.StatusDescription);
							}
						}
					}
				}
				else
				{
					// PathElements (Linee) e SymbolElements (Stazioni)
					SerializableMapElement tempMapElement = null;

					if (this._cacheBuffer != null && this._cacheBuffer.TryGetValue(element.ObjectStatusId, out tempMapElement) &&
					    tempMapElement.PositionDate >= element.PositionDate)
					{
						mapElement = tempMapElement;
					}

					switch (element.MapElementType)
					{
						case GrisMapElementType.Node:
						{
							mapElement = this.BuildNodeElement(element, mapElement, out newWorldRect);
							if (mapElement == null)
							{
								continue;
							}
							break;
						}
						case GrisMapElementType.Zone:
						{
							mapElement = this.BuildZoneElement(element, mapElement, out newWorldRect);
							if (mapElement == null)
							{
								continue;
							}
							break;
						}
						default:
						{
							throw new Exception("GrisMapElementType non gestito per l'elemento con ObjectStatusId:" + element.ObjectStatusId);
						}
					}
				}

				mapElement.Name = element.Name;
				mapElement.SetProperty("Status", (GrisStatus) Enum.Parse(typeof (GrisStatus), element.Status.ToString(), true));
				mapElement.SetProperty("ClassificationValue", element.GroupName);
				mapElement.SetProperty("Guid", element.ObjectStatusId);
				mapElement.SetProperty("WithServer", element.WithServer);
				mapElement.SetProperty("Id", element.ObjectId);
				mapElement.SetProperty("TypeId", element.MapElementType);
				mapElement.SetProperty("PositionDate", element.PositionDate);
				mapElement.SetProperty("IsNodeAccessibleToUser", element.IsNodeAccessibleToUser);
				mapElement.SetProperty("CustomColor", element.CustomColor);

				if (element.CaptionFromScale != 0)
				{
					mapElement.SetProperty("CaptionFromScale", element.CaptionFromScale);
				}
				if (element.MapOffsetX != 0)
				{
					mapElement.SetProperty("OffsetX", element.MapOffsetX);
				}
				if (element.MapOffsetY != 0)
				{
					mapElement.SetProperty("OffsetY", element.MapOffsetY);
				}

				if (element.MapElementType == GrisMapElementType.Node)
				{
					mapNodes.Add(mapElement);
				}
				if (element.MapElementType == GrisMapElementType.Zone)
				{
					mapZones.Add(mapElement);
				}
				if (element.MapElementType == GrisMapElementType.Region)
				{
					mapRegions.Add(mapElement);
				}

				Rect worldRect = mapElement.WorldRect;
				worldRect = newWorldRect;
				mapElement.WorldRect = worldRect;

				// se è stato indicato un elemento su cui fare il fit della finestra sollevo l'evento
				if (this.SelectedElementFound != null && this.selectedElementId != -1 && this.selectedElementId == element.ObjectId &&
				    this.selectedElementType == element.MapElementType)
				{
					selectedElement = element;
					selectedMapElement = mapElement;
				}

				#region Diagnostic

				currentIteration = DateTime.Now - iterationTime;
				if (currentIteration > maxIteration)
				{
					maxIteration = currentIteration;
				}
				if (currentIteration < minIteration)
				{
					minIteration = currentIteration;
				}
				totIteration += currentIteration;
				iterationTime += currentIteration;
				iterationCounter++;

				#endregion
			}

			#endregion

			#region Diagnostic

			//Debug.WriteLine("Totale elementi: {0}, Media caricamento: {1}, Minor tempo di caricamento: {2}, Maggior tempo di caricamento {3} (millisecondi)", iterationCounter, totIteration.TotalMilliseconds / iterationCounter, minIteration.TotalMilliseconds, maxIteration.TotalMilliseconds);

			#endregion

			if (this.xMax >= this.xMin && this.yMax >= this.yMin)
			{
				this.WorldBounds = new Rect(this.xMin, this.yMin, this.xMax - this.xMin, this.yMax - this.yMin);
				this.WorldBounds = this.ProjectToMap(this.WorldBounds);
				this.CurrentMapLayer.WorldRect = this.WorldBounds;

				MapElementCollection mapElements = new MapElementCollection();

				//Debug.WriteLine("Inizio riorganizzazione elementi {0}", DateTime.Now);
				// l'ordine di Add è significativo in questo modo i nodes vengono visualizzati sempre sopra le zones
				foreach (var mapElement in mapNodes)
				{
					mapElements.Add(mapElement);
				}

				foreach (var mapElement in mapZones)
				{
					mapElements.Add(mapElement);
				}

				foreach (var mapElement in mapRegions)
				{
					mapElements.Add(mapElement);
				}
				//Debug.WriteLine("Fine riorganizzazione elementi {0}", DateTime.Now);

				this.CurrentMapLayer.SmartOverlap = 0.5;
				this.CurrentMapLayer.SmartFade = 0.5;
				this.CurrentMapLayer.FontFamily = new FontFamily("Arial");
				this.CurrentMapLayer.FontSize = 12;
				this.CurrentMapLayer.FontWeight = FontWeights.Normal;
				this.CurrentMapLayer.Foreground = new SolidColorBrush(Color.FromArgb(255, 169, 169, 169));

				//Debug.WriteLine("Inizio posizionamento elementi sul layer {0}", DateTime.Now);
				this.CurrentMapLayer.Elements = mapElements;
				//Debug.WriteLine("Fine collocaposizionamentomento elementi sul layer {0}", DateTime.Now);
				this.CurrentMapLayer.IsSensitive = this.CurrentGrisMapLayer.IsSensitive;

				if (this.SelectedElementFound != null && selectedMapElement != null)
				{
					this.SelectedElementFound(this, new SelectedElementFoundEventArgs(selectedElement, selectedMapElement));
				}
			}

			if (this.LayerLoaded != null)
			{
				this.LayerLoaded(this.CurrentMapLayer, null);
			}
		}

		private MapElement FindMapElementByGuid(MapElementCollection mec, Guid id)
		{
			IEnumerable<MapElement> mecFound = GrisMap.FindElementByGuid(mec, id);
			foreach (MapElement me in mecFound)
			{
				return me;
			}
			return null;
		}

		private Brush GetStatusBrush(Status status, bool backgroundVersion)
		{
			string resourceName = "BrushUnknown" + (backgroundVersion ? "BG" : "");
			switch (status)
			{
				case Status.Ok:
					resourceName = "BrushOk" + (backgroundVersion ? "BG" : "");
					break;
				case Status.Warning:
					resourceName = "BrushWarning" + (backgroundVersion ? "BG" : "");
					break;
				case Status.Error:
					resourceName = "BrushError" + (backgroundVersion ? "BG" : "");
					break;
				case Status.Maintenance:
					resourceName = "BrushMaintenance" + (backgroundVersion ? "BG" : "");
					break;
			}
			return (Brush) Application.Current.Resources[resourceName];
		}

		private MapPolylineCollection ProjectToMap(MapPolylineCollection mpc)
		{
			MapPolylineCollection mpcOut = new MapPolylineCollection();
			foreach (MapPolyline mp in mpc)
			{
				mpcOut.Add(ProjectToMap(mp));
			}
			return mpcOut;
		}

		private MapPolyline ProjectToMap(IList<Point> points)
		{
			foreach (Point point in points)
			{
				UpdateWorldBounds(point);
			}
			return this.Map.MapProjection.ProjectToMap(points);
		}

		private MapPolyline UnprojectFromMap(IList<Point> points)
		{
			var polyline = this.Map.MapProjection.UnprojectFromMap(points);
			foreach (Point point in polyline)
			{
				UpdateWorldBounds(point);
			}
			return polyline;
		}

		private Point ProjectToMap(Point point)
		{
			if (this.Map == null)
			{
				return new Point(0, 0);
			}

			UpdateWorldBounds(point);

			return this.Map.MapProjection.ProjectToMap(point);
		}

		private Point UnprojectFromMap(Point point)
		{
			if (this.Map == null)
			{
				return new Point(0, 0);
			}
			Point retPoint = this.Map.MapProjection.UnprojectFromMap(point);

			UpdateWorldBounds(retPoint);

			return retPoint;
		}

		private Rect ProjectToMap(Rect rc)
		{
			Point leftTop = this.Map.MapProjection.ProjectToMap(new Point(rc.Left, rc.Top));
			Point rightButtom = this.Map.MapProjection.ProjectToMap(new Point(rc.Right, rc.Bottom));

			return new Rect(leftTop, rightButtom);
		}

		private Rect UnprojectFromMap(Rect rc)
		{
			Point leftTop = this.Map.MapProjection.UnprojectFromMap(new Point(rc.Left, rc.Top));
			Point rightButtom = this.Map.MapProjection.UnprojectFromMap(new Point(rc.Right, rc.Bottom));

			return new Rect(leftTop, rightButtom);
		}

		private void UpdateWorldBounds(Point point)
		{
			if (point.X < this.xMin)
			{
				this.xMin = point.X;
			}
			if (point.Y < this.yMin)
			{
				this.yMin = point.Y;
			}
			if (point.X > this.xMax)
			{
				this.xMax = point.X;
			}
			if (point.Y > this.yMax)
			{
				this.yMax = point.Y;
			}
		}

		private void UpdateWorldBounds(Rect rect)
		{
			this.UpdateWorldBounds(new Point(rect.Left, rect.Top));
			this.UpdateWorldBounds(new Point(rect.Right, rect.Bottom));
		}

		private object ParsePoints(string points)
		{
			string pts = points.Trim();
			if (pts.StartsWith("point"))
			{
				return this.ParsePoint(pts);
			}

			if (pts.StartsWith("polyline"))
			{
				MapPolylineCollection mpc = new MapPolylineCollection();
				pts = pts.Substring("polyline".Length + 1);
				string[] parts = pts.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
				foreach (string part in parts)
				{
					MapPolyline mp = new MapPolyline();
					string[] aPts = part.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
					foreach (string pt in aPts)
					{
						mp.Add(this.ParsePoint(pt));
					}
					mpc.Add(mp);
				}
				return mpc;
			}
			return null;
		}

		private Point ParsePoint(string point)
		{
			string pts = point.Trim();
			if (pts.StartsWith("point"))
			{
				pts = pts.Substring("point".Length + 1);
			}

			string[] pt = pts.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
			if (pt.Length > 1)
			{
				double y = double.Parse(pt[0], CultureInfo.InvariantCulture);
				double x = double.Parse(pt[1], CultureInfo.InvariantCulture);

				return new Point(x, y);
			}

			return new Point(0, 0);
		}

		# region Metodi di costruzione dei MapElement

		private MapElement BuildNodeElement(GrisMapElement element, MapElement cacheElement, out Rect newWorldRect)
		{
			SymbolElement mapElement = null;

			newWorldRect = Rect.Empty;

			if (cacheElement != null)
			{
				mapElement = (SymbolElement) cacheElement;

				if (!string.IsNullOrEmpty(element.MapPoints))
				{
					mapElement.SymbolOrigin = this.ProjectToMap((Point) this.ParsePoints(element.MapPoints));
				}
				else
				{
					this.UnprojectFromMap(mapElement.SymbolOrigin);
				}
				newWorldRect = mapElement.WorldRect;

				mapElement.SetProperty("FromCache", true);
			}
			else
			{
				if (string.IsNullOrEmpty(element.MapPoints))
				{
					Debug.WriteLine("Mancano i dati per posizionamento geografico di " + element.Name);
					return null;
				}

				mapElement = new SymbolElement {SymbolOrigin = this.ProjectToMap((Point) this.ParsePoints(element.MapPoints))};
				newWorldRect = mapElement.WorldRect;

				mapElement.SymbolSize = this.CurrentMapLayer.SymbolSize;
				mapElement.ShadowFill = new SolidColorBrush(Colors.Transparent);

				mapElement.SetProperty("FromCache", false);
			}

			return mapElement;
		}

		private MapElement BuildZoneElement(GrisMapElement element, MapElement cacheElement, out Rect newWorldRect)
		{
			PathElement mapElement = null;
			newWorldRect = Rect.Empty;

			if (cacheElement != null)
			{
				MapPolylineCollection lines = null;

				mapElement = (PathElement) cacheElement;

				if (!string.IsNullOrEmpty(element.MapPoints))
				{
					lines = this.ProjectToMap((MapPolylineCollection) this.ParsePoints(element.MapPoints));
					mapElement.Polylines = lines;
				}
				else
				{
					lines = mapElement.Polylines;
					this.UnprojectFromMap((from line in lines from point in line select point).ToList());
				}

				newWorldRect = lines.GetWorldRect();

				if (this.CurrentGrisMapLayer.ShowElementsStatus)
				{
					if (this.FilterSystemID > 0)
					{
						mapElement.Fill = this.GetStatusBrush(Status.Maintenance, false);
					}
					else
					{
						mapElement.Fill = this.GetStatusBrush(element.Status, false);
					}
				}

				if (this.CurrentGrisMapLayer.ShowElementsTooltips)
				{
					if (this.FilterSystemID > 0)
					{
						mapElement.ToolTip = string.Format("{0}", element.Name);
					}
					else
					{
						mapElement.ToolTip = string.Format("{0} ({1})", element.Name, element.StatusDescription);
					}
				}

				mapElement.SetProperty("FromCache", true);
			}
			else
			{
				if (string.IsNullOrEmpty(element.MapPoints))
				{
					Debug.WriteLine("Mancano i dati per posizionamento geografico di " + element.Name);
					return null;
				}

				MapPolylineCollection lines = this.ProjectToMap((MapPolylineCollection) this.ParsePoints(element.MapPoints));
				mapElement = new PathElement {Polylines = lines};
				newWorldRect = lines.GetWorldRect();

				mapElement.StrokeThickness = 5;
				mapElement.ShadowFill = new SolidColorBrush(Colors.Black);

				if (this.CurrentGrisMapLayer.ShowElementsStatus)
				{
					if (this.FilterSystemID > 0)
					{
						mapElement.Fill = this.GetStatusBrush(Status.Maintenance, false);
					}
					else
					{
						mapElement.Fill = this.GetStatusBrush(element.Status, false);
					}
				}

				if (this.CurrentGrisMapLayer.ShowElementsTooltips)
				{
					if (this.FilterSystemID > 0)
					{
						mapElement.ToolTip = string.Format("{0}", element.Name);
					}
					else
					{
						mapElement.ToolTip = string.Format("{0} ({1})", element.Name, element.StatusDescription);
					}
				}

				mapElement.SetProperty("FromCache", false);
			}

			return mapElement;
		}

		# endregion
	}

	public class SelectedElementFoundEventArgs : EventArgs
	{
		public GrisMapElement Element { get; private set; }
		public MapElement MapElement { get; private set; }

		public SelectedElementFoundEventArgs(GrisMapElement element, MapElement mapElement)
		{
			this.Element = element;
			this.MapElement = mapElement;
		}
	}
}
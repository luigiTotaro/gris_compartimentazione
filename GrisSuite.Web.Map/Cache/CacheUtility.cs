﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;
using BrainTechLLC;


namespace GrisSuite.Web.Client.Cache
{

    public class CacheUtility<T> where T : ICustomBinarySerializable
    {
        private long _quotaToAdd;

        public readonly string CacheFilename = typeof(T).Name;
        public long UncompressedSize { get; private set; }
        public long CompressedSize { get; private set; }
        public double ElapsedMilliseconds { get; private set; }

        public event EventHandler<NotEnoughSpaceEventArgs> NotEnoughFreeSpace;

        public bool CacheExists()
        {
            using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
                return isolatedStorageFile.FileExists(CacheFilename);
        }

        public void SaveData(T data)
        {
            DateTime start = DateTime.Now;
            using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                DeleteData();

                using (IsolatedStorageFileStream fileStream = isolatedStorageFile.CreateFile(CacheFilename))
                {
                    DataContractSerializer s = new DataContractSerializer(typeof(T));
                    MemoryStream ms = new MemoryStream();
                    s.WriteObject(ms, data);
                    byte[] b = ms.ToArray();
                    UncompressedSize = b.Length;

                    byte[] compressed = b.Compress();
                    CompressedSize = compressed.Length;
                    fileStream.Write(compressed, 0, compressed.Length);
                    ms.Dispose();
                }
            }
            DateTime end = DateTime.Now;
            TimeSpan elapsed = end - start;
            this.ElapsedMilliseconds = elapsed.TotalMilliseconds;
        }

        public void SaveDataCustom(T data)
        {
            DateTime start = DateTime.Now;

            this.DeleteData();
            this.TryToSaveData(this.SerializeObject(data));

            DateTime end = DateTime.Now;
            TimeSpan elapsed = end - start;
            this.ElapsedMilliseconds = elapsed.TotalMilliseconds;
        }

        private byte[] SerializeObject(T objectToSerialize)
        {
            CustomBinaryFormatter binaryFormatter = new CustomBinaryFormatter();

            binaryFormatter.Register<T>(1);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, objectToSerialize);
                memoryStream.Seek(0, 0);
                byte[] buffer = memoryStream.ToArray();
                UncompressedSize = buffer.Length;
                byte[] compressed = buffer.Compress();
                CompressedSize = compressed.Length;

                return compressed;
            }
        }

        private bool TryToSaveData(byte[] buffer)
        {
            const int FREE_OFFSET = 5120; // 5 Kb di tolleranza
            using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream isStream = isolatedStorageFile.CreateFile(CacheFilename))
                {
                    bool mustAskForSpace = (buffer.Length > isolatedStorageFile.AvailableFreeSpace + FREE_OFFSET);
                    if (mustAskForSpace)
                    {
                        if (this.NotEnoughFreeSpace != null)
                        {
                            this._quotaToAdd = (buffer.Length + FREE_OFFSET - isolatedStorageFile.AvailableFreeSpace);
                            var eventArgs = new NotEnoughSpaceEventArgs(buffer, this._quotaToAdd);
                            this.NotEnoughFreeSpace(this, eventArgs);

                            if (eventArgs.SpaceGranted) this.TryToSaveData(buffer);
                        }
                    }
                    else
                    {
                        isStream.Write(buffer, 0, buffer.Length);
                        return true;
                    }
                }
            }

            return false;
        }

        private void DeleteData()
        {
            using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (CacheExists()) isolatedStorageFile.DeleteFile(CacheFilename);
            }
        }

        public T LoadData()
        {
            DateTime start = DateTime.Now;
            T typeData = default(T);

            using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (CacheExists())
                {
                    using (IsolatedStorageFileStream fileStream = isolatedStorageFile.OpenFile(CacheFilename, FileMode.Open))
                    {
                        this.CompressedSize = (int)fileStream.Length;
                        byte[] compressed = new byte[fileStream.Length];
                        fileStream.Read(compressed, 0, (int)fileStream.Length);
                        byte[] decompressed = compressed.Decompress();
                        this.UncompressedSize = decompressed.Length;
                        DataContractSerializer s = new DataContractSerializer(typeof(T));
                        MemoryStream ms = new MemoryStream(decompressed);
                        typeData = (T)s.ReadObject(ms);
                        ms.Dispose();
                    }
                }
            }
            DateTime end = DateTime.Now;
            TimeSpan elapsed = end - start;
            this.ElapsedMilliseconds = elapsed.TotalMilliseconds;
            return typeData;
        }

        public T LoadDataCustom()
        {
            DateTime start = DateTime.Now;
            T typeData = default(T);

            using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (CacheExists())
                {
                    using (IsolatedStorageFileStream isFileStream = isolatedStorageFile.OpenFile(CacheFilename, FileMode.Open))
                    {
                        if (isFileStream.Length > 0)
                        {
                            byte[] compressed = new byte[isFileStream.Length];
                            this.CompressedSize = (int)isFileStream.Length;
                            isFileStream.Read(compressed, 0, (int)isFileStream.Length);
                            byte[] decompressed = compressed.Decompress();
                            this.UncompressedSize = decompressed.Length;
                            CustomBinaryFormatter f = new CustomBinaryFormatter();
                            f.Register<T>(1);
                            using (MemoryStream memoryStream = new MemoryStream(decompressed))
                            {
                                memoryStream.Seek(0, 0);
                                typeData = (T)f.Deserialize(memoryStream);
                                memoryStream.Dispose();
                            }
                        }
                    }
                }
            }
            DateTime end = DateTime.Now;
            TimeSpan elapsed = end - start;
            this.ElapsedMilliseconds = elapsed.TotalMilliseconds;
            return typeData;
        }

        public bool IncreaseQuota()
        {
            if (this._quotaToAdd > 0) return this.IncreaseQuota(this._quotaToAdd);
            return false;
        }

        public bool IncreaseQuota(long additionalQuota)
        {
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    return store.IncreaseQuotaTo(store.Quota + additionalQuota);
                }
            }
            catch (IsolatedStorageException)
            {
                // TODO: Log
            }

            return false;
        }

        public void EmptyApplicationStore()
        {
            try
            {
                this.DeleteData();

                using (IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication())
                {

                    isolatedStorageFile.Remove();
                }
            }
            catch (IsolatedStorageException)
            {
                // TODO: Log
            }
        }
    }

    public class NotEnoughSpaceEventArgs : EventArgs
    {
        public readonly byte[] TargetBuffer;
        public readonly long RequiredSpace;
        public bool SpaceGranted { get; set; }

        public NotEnoughSpaceEventArgs(byte[] targetBuffer, long requiredSpace)
        {
            this.TargetBuffer = targetBuffer;
            this.RequiredSpace = requiredSpace;
        }
    }
}
﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Linq;

using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.MapService;
using Infragistics.Silverlight.Map;

namespace GrisSuite.Web.Client.Cache
{
	[Serializable]
	public sealed class SerializableMapElements : Dictionary<Guid, SerializableMapElement>, ICustomBinarySerializable
	{
		public SerializableMapElements () {}

		public SerializableMapElements ( int capacity ) : base (capacity) {}
		
		public void WriteDataTo ( BinaryWriter writer )
		{
			if ( this.Count > 0 )
			{
				writer.Write(this.Count);
				foreach ( var element in this )
				{
					writer.Write((int)element.Value.TypeId);
					writer.Write(element.Key.ToByteArray());
					element.Value.WriteDataTo(writer);
				}
			}
		}

		public void SetDataFrom ( BinaryReader reader )
		{
			this.Clear();
			int count = reader.ReadInt32();
			for ( int index = 0; index < count; index++ )
			{
				GrisMapElementType elementType = (GrisMapElementType) reader.ReadInt32();
				Guid guid = new Guid(reader.ReadBytes(16));
				SerializableMapElement sMapElement = null;

				switch ( elementType )
				{
					case GrisMapElementType.Node:
						{
							sMapElement = new SerializableSymbolElement();
							break;
						}
					case GrisMapElementType.Zone:
						{
							sMapElement = new SerializablePathElement();
							break;
						}
					case GrisMapElementType.Region:
						{
							sMapElement = new SerializableSurfaceElement();
							break;
						}
				}

				if ( sMapElement != null )
				{
					sMapElement.SetDataFrom(reader);
					this.Add(guid, sMapElement);
				}
			}
		}
	}

	[Serializable]
	public sealed class SerializableSurfaceElement : SerializableMapElement
	{
		public SerializableSurfaceElement () { }

		public SerializableSurfaceElement ( SurfaceElement surfaceElement )
			: base(surfaceElement)
		{
			if ( surfaceElement == null ) throw new ArgumentException("Il parametro 'symbolElement' della classe 'SerializableMapElement' non è inizializzato.");
		}

		public static implicit operator SerializableSurfaceElement ( SurfaceElement surfaceElement )
		{
			return new SerializableSurfaceElement(surfaceElement);
		}

		public static implicit operator MapElement ( SerializableSurfaceElement serializableSurfaceElement )
		{
			return serializableSurfaceElement.ToMapElement();
		}
	}

	[Serializable]
	public sealed class SerializableSymbolElement : SerializableMapElement
	{
		private SymbolElement _internalMapElement;

		public SerializablePoint SymbolOrigin { get; set; }
		public double SymbolSize { get; set; }

		public SerializableSymbolElement () { }

		public SerializableSymbolElement ( SymbolElement symbolElement )
			: base(symbolElement)
		{
			if ( symbolElement == null ) throw new ArgumentException("Il parametro 'symbolElement' della classe 'SerializableMapElement' non è inizializzato.");
			this._internalMapElement = symbolElement;

			this.SymbolOrigin = symbolElement.SymbolOrigin;
			this.SymbolSize = symbolElement.SymbolSize;
			this.ShadowFill = symbolElement.ShadowFill;
		}

		public override MapElement ToMapElement ()
		{
			this._internalMapElement = (SymbolElement) base.ToMapElement();
			this._internalMapElement.SymbolOrigin = this.SymbolOrigin;
			this._internalMapElement.SymbolSize = this.SymbolSize;
			this._internalMapElement.ShadowFill = this.ShadowFill;

			return this._internalMapElement;
		}

		public override void WriteDataTo ( BinaryWriter writer )
		{
			base.WriteDataTo(writer);
			this.SymbolOrigin.WriteDataTo(writer);
			writer.Write(this.SymbolSize);
		}

		public override void SetDataFrom ( BinaryReader reader )
		{
			base.SetDataFrom(reader);

			if ( this.SymbolOrigin == null ) this.SymbolOrigin = new SerializablePoint(new Point(0, 0));
			this.SymbolOrigin.SetDataFrom(reader);
			this.SymbolSize = reader.ReadDouble();
		}

		public static implicit operator SerializableSymbolElement ( SymbolElement symbolElement )
		{
			return new SerializableSymbolElement(symbolElement);
		}

		public static implicit operator MapElement ( SerializableSymbolElement serializableSymbolElement )
		{
			return serializableSymbolElement.ToMapElement();
		}
	}

	[Serializable]
	public sealed class SerializablePathElement : SerializableMapElement
	{
		private PathElement _internalMapElement;

		public MapPolylineCollection Polylines { get; set; }
		public double StrokeThickness { get; set; }

		public SerializablePathElement () { }

		public SerializablePathElement ( PathElement pathElement )
			: base(pathElement)
		{
			if ( pathElement == null ) throw new ArgumentException("Il parametro 'pathElement' del costruttore della classe 'SerializableMapElement' non è inizializzato.");
			this._internalMapElement = pathElement;

			this.Polylines = pathElement.Polylines;
			this.StrokeThickness = pathElement.StrokeThickness;
		}

		public override MapElement ToMapElement ()
		{
			this._internalMapElement = (PathElement) base.ToMapElement();
			this._internalMapElement.Polylines = this.Polylines;
			this._internalMapElement.StrokeThickness = this.StrokeThickness;

			return this._internalMapElement;
		}

		public override void WriteDataTo ( BinaryWriter writer )
		{
			base.WriteDataTo(writer);

			writer.Write(this.StrokeThickness);
			writer.Write(this.Polylines.Count);
			foreach ( var polyline in this.Polylines ) ( (SerializablePolyline) polyline ).WriteDataTo(writer);
		}

		public override void SetDataFrom ( BinaryReader reader )
		{
			base.SetDataFrom(reader);

			this.StrokeThickness = reader.ReadDouble();
			this.Polylines = new MapPolylineCollection();
			int count = reader.ReadInt32();
			for ( int index = 0; index < count; index++ )
			{
				SerializablePolyline polyline = new SerializablePolyline(null);
				polyline.SetDataFrom(reader);
				this.Polylines.Add(polyline);
			}
		}

		public static implicit operator SerializablePathElement ( PathElement pathElement )
		{
			return new SerializablePathElement(pathElement);
		}

		public static implicit operator MapElement ( SerializablePathElement serializablePathElement )
		{
			return serializablePathElement.ToMapElement();
		}
	}

	# region Classe base
	[Serializable]
	public abstract class SerializableMapElement : ICustomBinarySerializable
	{
		private MapElement _internalMapElement;
		
		public Brush Fill { get; set; }
		public string Tooltip { get; set; }
		public Rect WorldRect { get; set; }
		public string Name { get; set; }
		public GrisStatus Status { get; set; }
		public string ClassificationValue { get; set; }
		public Guid Guid { get; set; }
		public bool WithServer { get; set; }
		public long Id { get; set; }
		public GrisMapElementType TypeId { get; set; }
		public double CaptionFromScale { get; set; }
		public double OffsetX { get; set; }
		public double OffsetY { get; set; }
		public Brush ShadowFill { get; set; }
		public DateTime PositionDate { get; set; }

		protected SerializableMapElement () { }

		protected SerializableMapElement ( MapElement mapElement )
		{
			if ( mapElement == null ) throw new ArgumentException("Il parametro 'mapElement' del costruttore non è inizializzato.");
			this._internalMapElement = mapElement;

			this.Name = mapElement.Name;
			this.Status = (GrisStatus) mapElement.GetProperty("Status");
			this.ClassificationValue = mapElement.GetProperty("ClassificationValue").ToString();
			this.Guid = (Guid) mapElement.GetProperty("Guid");
			this.WithServer = (bool) mapElement.GetProperty("WithServer");
			this.Id = (long) mapElement.GetProperty("Id");
			this.TypeId = (GrisMapElementType) mapElement.GetProperty("TypeId");
			this.PositionDate = (DateTime) mapElement.GetProperty("PositionDate");
			this.Fill = mapElement.Fill;
			this.WorldRect = mapElement.WorldRect;
			this.ShadowFill = mapElement.ShadowFill;
			
			this.Tooltip =( mapElement.ToolTip is string ) ? mapElement.ToolTip.ToString() : default(string);
			if ( mapElement.GetProperty("CaptionFromScale") != null ) this.CaptionFromScale = (double) mapElement.GetProperty("CaptionFromScale");
			if ( mapElement.GetProperty("OffsetX") != null ) this.OffsetX = (double) mapElement.GetProperty("OffsetX");
			if ( mapElement.GetProperty("OffsetY") != null ) this.OffsetY = (double) mapElement.GetProperty("OffsetY");
		}
		
		public virtual MapElement ToMapElement()
		{
			if ( this._internalMapElement == null && this.TypeId != GrisMapElementType.None )
			{
				switch ( this.TypeId )
				{
					case GrisMapElementType.Node:
						{
							this._internalMapElement = new SymbolElement();
							break;
						}
					case GrisMapElementType.Zone:
						{
							this._internalMapElement = new PathElement();
							break;
						}
					default:
						{
							this._internalMapElement = new SurfaceElement();
							break;
						}
				}
			}
			
			if ( this._internalMapElement != null )
			{
				this._internalMapElement.Name = this.Name;
				this._internalMapElement.SetProperty("Status", this.Status);
				this._internalMapElement.SetProperty("ClassificationValue", this.ClassificationValue);
				this._internalMapElement.SetProperty("Guid", this.Guid);
				this._internalMapElement.SetProperty("WithServer", this.WithServer);
				this._internalMapElement.SetProperty("Id", this.Id);
				this._internalMapElement.SetProperty("TypeId", this.TypeId);
				this._internalMapElement.SetProperty("PositionDate", this.PositionDate);
				this._internalMapElement.Fill = this.Fill;
				this._internalMapElement.WorldRect = this.WorldRect;
				this._internalMapElement.ShadowFill = this.ShadowFill;

				if ( !string.IsNullOrEmpty(this.Tooltip) ) this._internalMapElement.ToolTip = this.Tooltip;
				if ( this.CaptionFromScale != 0 ) this._internalMapElement.SetProperty("CaptionFromScale", this.CaptionFromScale);
				if ( this.OffsetX != 0 ) this._internalMapElement.SetProperty("OffsetX", this.OffsetX);
				if ( this.OffsetY != 0 ) this._internalMapElement.SetProperty("OffsetY", this.OffsetY);				
			}

			return this._internalMapElement;
		}

		public virtual void WriteDataTo ( BinaryWriter writer )
		{
			//NOTA IMPORTANTE: tenere sincronizzato l'ordine di scrittura e lettura delle proprietà
			writer.Write(this.Name);
			writer.Write((int) this.Status);
			writer.Write(this.ClassificationValue);
			writer.Write(this.Guid.ToByteArray());
			writer.Write(this.WithServer);
			writer.Write(this.Id);
			writer.Write((int) this.TypeId);
			writer.Write(this.CaptionFromScale);
			writer.Write(this.OffsetX);
			writer.Write(this.OffsetY);

			writer.Write(this.PositionDate.Year);
			writer.Write(this.PositionDate.Month);
			writer.Write(this.PositionDate.Day);
			writer.Write(this.PositionDate.Hour);
			writer.Write(this.PositionDate.Minute);
			writer.Write(this.PositionDate.Second);
			writer.Write(this.PositionDate.Millisecond);

			writer.Write(this.WorldRect.X);
			writer.Write(this.WorldRect.Y);
			writer.Write(this.WorldRect.Width);
			writer.Write(this.WorldRect.Height);

			writer.Write(this.Tooltip ?? "");

			var color = ( this.Fill != null ) ? ((SolidColorBrush)this.Fill).Color : Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
			writer.Write(color.A);
			writer.Write(color.R);
			writer.Write(color.G);
			writer.Write(color.B);

			color = ( this.ShadowFill != null ) ? ( (SolidColorBrush) this.ShadowFill ).Color : Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
			writer.Write(color.A);
			writer.Write(color.R);
			writer.Write(color.G);
			writer.Write(color.B);
		}

		public virtual void SetDataFrom ( BinaryReader reader )
		{
			//NOTA IMPORTANTE: tenere sincronizzato l'ordine di scrittura e lettura delle proprietà
			this.Name = reader.ReadString();
			this.Status = (GrisStatus)reader.ReadInt32();
			this.ClassificationValue = reader.ReadString();
			this.Guid = new Guid(reader.ReadBytes(16));
			this.WithServer = reader.ReadBoolean();
			this.Id = reader.ReadInt64();
			this.TypeId = (GrisMapElementType) reader.ReadInt32();
			this.CaptionFromScale = reader.ReadDouble();
			this.OffsetX = reader.ReadDouble();
			this.OffsetY = reader.ReadDouble();

			this.PositionDate = new DateTime(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());

			this.WorldRect = new Rect(reader.ReadDouble(), reader.ReadDouble(), reader.ReadDouble(), reader.ReadDouble()); //x, y, width, height

			string tooltip = reader.ReadString();
			if ( !string.IsNullOrEmpty(tooltip) ) this.Tooltip = tooltip;

			var color = Color.FromArgb(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), reader.ReadByte()); //alpha, red, green, blue
			if ( color != Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF) ) this.Fill = new SolidColorBrush(color);

			color = Color.FromArgb(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
			if ( color != Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF) ) this.ShadowFill =  new SolidColorBrush(color);
		}

		public static implicit operator SerializableMapElement ( MapElement mapElement )
		{
			if ( mapElement is SymbolElement )
			{
				return new SerializableSymbolElement((SymbolElement) mapElement);				
			}
			if ( mapElement is PathElement )
			{
				return new SerializablePathElement((PathElement) mapElement);
			}
			if ( mapElement is SurfaceElement )
			{
				return new SerializableSurfaceElement((SurfaceElement) mapElement);
			}

			return null;
		}

		public static implicit operator MapElement ( SerializableMapElement serializableMapElement )
		{
			return serializableMapElement.ToMapElement();
		}
	}
	# endregion

	# region Classi d'appoggio
	[Serializable]
	public sealed class SerializablePolyline : ICustomBinarySerializable
	{
		public IList<SerializablePoint> Points;

		public SerializablePolyline ( MapPolyline polyline )
		{
			if ( polyline != null ) this.Points = polyline.ToArray().Select(pt => new SerializablePoint(pt)).ToList();
		}

		public MapPolyline ToMapPolyline ()
		{
			if ( this.Points != null )
			{
				var polyline = new MapPolyline();
				foreach ( var point in Points ) polyline.Add(point);
				return polyline;				
			}

			return null;
		}

		public static implicit operator SerializablePolyline ( MapPolyline polyline )
		{
			return new SerializablePolyline(polyline);
		}

		public static implicit operator MapPolyline ( SerializablePolyline spolyline )
		{
			return spolyline.ToMapPolyline();
		}

		public void WriteDataTo ( BinaryWriter writer )
		{
			writer.Write(this.Points.Count);
			foreach ( var point in Points ) point.WriteDataTo(writer);
		}

		public void SetDataFrom ( BinaryReader reader )
		{			
			int count = reader.ReadInt32();
			this.Points = new List<SerializablePoint>(1000);
			for ( int index = 0; index < count; index++ )
			{
				SerializablePoint spoint = new SerializablePoint(new Point(0, 0));
				spoint.SetDataFrom(reader);
				this.Points.Add(spoint);
			}
		}
	}

	[Serializable]
	public sealed class SerializablePoint : ICustomBinarySerializable
	{
		public double X { get; set; }
		public double Y { get; set; }

		public SerializablePoint ( Point point )
		{
			this.X = point.X;
			this.Y = point.Y;
		}

		public Point ToPoint ()
		{
			return new Point(this.X, this.Y);
		}

		public static implicit operator SerializablePoint ( Point point )
		{
			return new SerializablePoint(point);
		}

		public static implicit operator Point ( SerializablePoint spoint )
		{
			return spoint.ToPoint();
		}

		public void WriteDataTo ( BinaryWriter writer )
		{
			writer.Write(this.X);
			writer.Write(this.Y);
		}

		public void SetDataFrom ( BinaryReader reader )
		{
			this.X = reader.ReadDouble();
			this.Y = reader.ReadDouble();
		}
	}
	# endregion
}

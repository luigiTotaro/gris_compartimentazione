﻿
using System.IO;

namespace GrisSuite.Web.Client.Cache
{
    public interface ICustomBinarySerializable
    {
        void WriteDataTo(BinaryWriter _Writer);
        void SetDataFrom(BinaryReader _Reader);
    }
}

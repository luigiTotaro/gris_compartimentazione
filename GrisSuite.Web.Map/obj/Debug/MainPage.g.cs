﻿#pragma checksum "C:\Users\federicopici\Documents\GrisSuite_Export_Sorgenti\GrisSuite.Web.Map\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "78C89A727727AF16A734AE7FEE2B13EA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.UserControls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace GrisSuite.Web.Map {
    
    
    public partial class MainPage : System.Windows.Controls.UserControl {
        
        internal System.Windows.Media.Animation.Storyboard IncreasePanelShow;
        
        internal System.Windows.Media.Animation.Storyboard IncreasePanelHide;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Border MapPlaceholder;
        
        internal GrisSuite.Web.Map.UserControls.ClassificationsPanel PanelClassification;
        
        internal GrisSuite.Web.Controls.SL.GrisPanel IncreaseQuotaPanel;
        
        internal System.Windows.Controls.TextBlock tbIncreaseQuotaText;
        
        internal System.Windows.Controls.Button btnIncreaseQuota;
        
        internal System.Windows.Controls.Button btnNotIncreaseQuota;
        
        internal System.Windows.Controls.TextBlock RegionName;
        
        internal GrisSuite.Web.Map.UserControls.LineChart RegionCounterChart;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/GrisSuite.Web.Map;component/MainPage.xaml", System.UriKind.Relative));
            this.IncreasePanelShow = ((System.Windows.Media.Animation.Storyboard)(this.FindName("IncreasePanelShow")));
            this.IncreasePanelHide = ((System.Windows.Media.Animation.Storyboard)(this.FindName("IncreasePanelHide")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.MapPlaceholder = ((System.Windows.Controls.Border)(this.FindName("MapPlaceholder")));
            this.PanelClassification = ((GrisSuite.Web.Map.UserControls.ClassificationsPanel)(this.FindName("PanelClassification")));
            this.IncreaseQuotaPanel = ((GrisSuite.Web.Controls.SL.GrisPanel)(this.FindName("IncreaseQuotaPanel")));
            this.tbIncreaseQuotaText = ((System.Windows.Controls.TextBlock)(this.FindName("tbIncreaseQuotaText")));
            this.btnIncreaseQuota = ((System.Windows.Controls.Button)(this.FindName("btnIncreaseQuota")));
            this.btnNotIncreaseQuota = ((System.Windows.Controls.Button)(this.FindName("btnNotIncreaseQuota")));
            this.RegionName = ((System.Windows.Controls.TextBlock)(this.FindName("RegionName")));
            this.RegionCounterChart = ((GrisSuite.Web.Map.UserControls.LineChart)(this.FindName("RegionCounterChart")));
        }
    }
}

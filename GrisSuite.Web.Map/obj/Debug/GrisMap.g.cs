﻿#pragma checksum "C:\Users\federicopici\Documents\GrisSuite_Export_Sorgenti\GrisSuite.Web.Map\GrisMap.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3B47CCC8EE7E485D59C5559C4468B2E1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Infragistics.Silverlight.Map;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace GrisSuite.Web.Map {
    
    
    public partial class GrisMap : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Infragistics.Silverlight.Map.XamWebMap theMap;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer00;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer01;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer02;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer03;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer04;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer05;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer06;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer07;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer08;
        
        internal Infragistics.Silverlight.Map.MapLayer Layer09;
        
        internal Infragistics.Silverlight.Map.MapNavigationPane theMapNavigationPane;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/GrisSuite.Web.Map;component/GrisMap.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.theMap = ((Infragistics.Silverlight.Map.XamWebMap)(this.FindName("theMap")));
            this.Layer00 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer00")));
            this.Layer01 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer01")));
            this.Layer02 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer02")));
            this.Layer03 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer03")));
            this.Layer04 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer04")));
            this.Layer05 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer05")));
            this.Layer06 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer06")));
            this.Layer07 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer07")));
            this.Layer08 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer08")));
            this.Layer09 = ((Infragistics.Silverlight.Map.MapLayer)(this.FindName("Layer09")));
            this.theMapNavigationPane = ((Infragistics.Silverlight.Map.MapNavigationPane)(this.FindName("theMapNavigationPane")));
        }
    }
}

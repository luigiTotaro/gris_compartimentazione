﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Infragistics.Silverlight.Map;
using GrisSuite.Web.Map.StatusCountersService;
using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.Classes;

namespace GrisSuite.Web.Map.UserControls
{
    public partial class StationInfo : UserControl
    {
        private readonly StatusCountersServiceClient StatusCounterService;

        public Guid Id
        {
            get { return (Guid)this.GetValue(IdProperty); }
            set { this.SetValue(IdProperty, value); }
        }

        public static readonly DependencyProperty IdProperty = DependencyProperty.Register("Id", typeof(Guid), typeof(StationInfo),
                                                                                           new PropertyMetadata(Guid.Empty, PropertiesChangedCallback));

        public string Title
        {
            get { return (string)this.GetValue(TitleProperty); }
            set { this.SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(StationInfo),
                                                                                              new PropertyMetadata("", PropertiesChangedCallback));

        public bool WithServer
        {
            get { return (bool)this.GetValue(WithServerProperty); }
            set { this.SetValue(WithServerProperty, value); }
        }

        public static readonly DependencyProperty StatusProperty = DependencyProperty.Register("Status", typeof(GrisVisualState), typeof(StationInfo),
                                                                                               new PropertyMetadata(new GrisVisualState(),
                                                                                                                    PropertiesChangedCallback));

        public GrisVisualState Status
        {
            get { return (GrisVisualState)this.GetValue(StatusProperty); }
            set { this.SetValue(StatusProperty, value); }
        }

        public static readonly DependencyProperty WithServerProperty = DependencyProperty.Register("WithServer", typeof(bool), typeof(StationInfo),
                                                                                                   new PropertyMetadata(true,
                                                                                                                        PropertiesChangedCallback));

        public string ClassificationValue
        {
            get { return (string)this.GetValue(ClassificationValueProperty); }
            set { this.SetValue(ClassificationValueProperty, value); }
        }

        public static readonly DependencyProperty ClassificationValueProperty = DependencyProperty.Register("ClassificationValue", typeof(string),
                                                                                                            typeof(StationInfo),
                                                                                                            new PropertyMetadata("",
                                                                                                                                 PropertiesChangedCallback));

		public int FilterSystemId
		{
			get { return (int)this.GetValue(FilterSystemIdProperty); }
			set { this.SetValue(FilterSystemIdProperty, value); }
		}

		public static readonly DependencyProperty FilterSystemIdProperty = DependencyProperty.Register("FilterSystemId", typeof(int), typeof(StationInfo),
																						   new PropertyMetadata(0, PropertiesChangedCallback));

        private static void PropertiesChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            StationInfo stationInfo = dependencyObject as StationInfo;
            if (stationInfo != null)
            {
                if (eventArgs.Property == TitleProperty)
                {
                    stationInfo.tbNodeName.Text = (eventArgs.NewValue != null) ? eventArgs.NewValue.ToString() : string.Empty;
                }

                if (eventArgs.Property == StatusProperty && eventArgs.NewValue != null && stationInfo.WithServer)
                {
                    GrisVisualState grisStatus = (GrisVisualState)eventArgs.NewValue;

                    switch (grisStatus.Status)
                    {
                        case GrisStatus.Maintenance:
                        {
                            stationInfo.tbServerStatus.Visibility = Visibility.Visible;
                            stationInfo.tbServerStatus.Text = string.Empty;
                            break;
                        }
                    }
                }

                if (eventArgs.Property == WithServerProperty)
                {
                    bool withServer = (eventArgs.NewValue != null) ? (bool)eventArgs.NewValue : true;
                    stationInfo.tbServerStatus.Visibility = withServer ? Visibility.Collapsed : Visibility.Visible;
                    stationInfo.tbServerStatus.Text = string.Empty;
                }

                if (eventArgs.Property == ClassificationValueProperty)
                {
                    stationInfo.tbNodeRelevance.Text = (eventArgs.NewValue != null) ? eventArgs.NewValue.ToString() : string.Empty;
                }
            }
        }

        public StationInfo()
        {
            this.InitializeComponent();

            this.StatusCounterService = ProxiesFactory.GetStatusCountersService();
            this.StatusCounterService.GetNodeSystemsCountersByObjectStatusIdCompleted +=
                this.StatusCounterService_GetNodeSystemsCountersByObjectStatusIdCompleted;
            this.StatusCounterService.GetSTLCInfoEntitiesCompleted += this.StatusCounterService_GetSTLCInfoEntitiesCompleted;

            MapElement el = this.DataContext as MapElement;
            if (el != null)
            {
                this.tbNodeName.Text = el.Name;
            }
        }

        private void StatusCounterService_GetNodeSystemsCountersByObjectStatusIdCompleted(object sender,
                                                                                          GetNodeSystemsCountersByObjectStatusIdCompletedEventArgs e)
        {
            if (e != null && e.Error == null && e.Result != null && !string.IsNullOrEmpty(e.Result.Name))
            {
                NodeSystemsCounters nsc = e.Result;
                SystemCounters altrePeriferiche = nsc.SystemsCounters.SingleOrDefault(x => x.SystemId == (int)GrisSystems.AltrePeriferiche);
                nsc.SystemsCounters.Remove(altrePeriferiche);

                this.DataContext = nsc;
            }
        }

        private void StatusCounterService_GetSTLCInfoEntitiesCompleted(object sender, GetSTLCInfoEntitiesCompletedEventArgs e)
        {
            this.listSTLCInfos.DataContext = null;

            if (e != null && e.Error == null && e.Result != null && e.Result.Count >= 0)
            {
                this.listSTLCInfos.ItemsSource = e.Result;
            }
        }

        public void LoadData()
        {
			this.StatusCounterService.GetNodeSystemsCountersByObjectStatusIdAsync(this.Id, this.FilterSystemId);
            this.StatusCounterService.GetSTLCInfoEntitiesAsync(this.Id);
        }

        private void VisualStateObject_Loaded(object sender, RoutedEventArgs e)
        {
            VisualStateObject vso = (VisualStateObject)sender;
            vso.Status = new GrisVisualState((GrisStatus)vso.DataContext);
        }

        private void pnlNodeDetail_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }
    }
}
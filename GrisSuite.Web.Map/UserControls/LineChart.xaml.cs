﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GrisSuite.Web.Map.StatusCountersService;


namespace GrisSuite.Web.Map.UserControls
{
	public partial class LineChart : UserControl
	{
        public delegate void ClickHandler(object sender, LineChartClickEventArgs e);
        public event ClickHandler Click;
        private Button _selectedButton;
        private int _totalCount = 0;
        

        public LineChart()
		{
			// Required to initialize variables
			InitializeComponent();
        }

        private StatusCounter FindCounter(ObservableCollection<StatusCounter> sc, Status status)
        {
            if (sc != null)
                for (int i = 0; i < sc.Count; i++)
                    if (sc[i].Status == status)
                        return (sc[i].Counter > 0) ? sc[i] : null;

            return null;
        }

        public void ResetLayout()
        {
            SetButtons(null, null, null, null, null);
            btnSx.Template = (ControlTemplate)this.Resources["ButtonAlone"];
            Grid.SetColumnSpan(btnSx, 5);
        }


        public void LoadData(ObservableCollection<StatusCounter> scc )
        {
            this.DataContext = scc;
           
            List<ButtonDynaProperties> btnDynaList = new List<ButtonDynaProperties>();
            
            _totalCount = 0;
            ResetLayout();

            StatusCounter sc = FindCounter(scc, Status.Ok);
            if (sc != null)
            {
                btnDynaList.Add(new ButtonDynaProperties { Background = (Brush)Application.Current.Resources["BrushOk"], Text = sc.Label, Counter = sc.Counter, Status = sc.Status });
                _totalCount += sc.Counter;
            }

            sc = FindCounter(scc, Status.Warning);
            if (sc != null)
            {
                btnDynaList.Add(new ButtonDynaProperties { Background = (Brush)Application.Current.Resources["BrushWarning"], Text = sc.Label, Counter = sc.Counter, Status = sc.Status });
                _totalCount += sc.Counter;
            }

            sc = FindCounter(scc, Status.Error);
            if (sc != null)
            {
                btnDynaList.Add(new ButtonDynaProperties { Background = (Brush)Application.Current.Resources["BrushError"], Text = sc.Label, Counter = sc.Counter, Status = sc.Status });
                _totalCount += sc.Counter;
            }


            sc = FindCounter(scc, Status.Unknown);
            if (sc != null)
            {
                btnDynaList.Add(new ButtonDynaProperties { Background = (Brush)Application.Current.Resources["BrushUnknown"], Text = sc.Label, Counter = sc.Counter, Status = sc.Status });
                _totalCount += sc.Counter;
            }

            sc = FindCounter(scc, Status.Maintenance);
            if (sc != null)
            {
                btnDynaList.Add(new ButtonDynaProperties { Background = (Brush)Application.Current.Resources["BrushMaintenance"], Text = sc.Label, Counter = sc.Counter, Status = sc.Status });
                _totalCount += sc.Counter;
            }

            btnSx.Template = (ControlTemplate)this.Resources[((btnDynaList.Count <= 1) ? "ButtonAlone" : "ButtonSx")];
            if (btnDynaList.Count <= 1)
            {
                btnSx.Template = (ControlTemplate)this.Resources["ButtonAlone"];
                Grid.SetColumnSpan(btnSx, 5);
            }
            else
            { 
                btnSx.Template = (ControlTemplate)this.Resources["ButtonSx"];
                Grid.SetColumnSpan(btnSx, 1);
            }

            switch (btnDynaList.Count)
            {
                case 1:
                    SetButtons(btnDynaList[0], null, null, null, null);
                    break;
                case 2:
                    SetButtons(btnDynaList[0], null, null, null, btnDynaList[1]);
                    break;
                case 3:
                    SetButtons(btnDynaList[0], btnDynaList[1], null, null, btnDynaList[2]);
                    break;
                case 4:
                    SetButtons(btnDynaList[0], btnDynaList[1], btnDynaList[2], null, btnDynaList[3]);
                    break;
                case 5:
                    SetButtons(btnDynaList[0], btnDynaList[1], btnDynaList[2], btnDynaList[3], btnDynaList[4]);
                    break;
            }
        }

        private void FillButtonWithDynaProperties(Button btn, ButtonDynaProperties dyna)
        {
            if (dyna != null)
            {
                GridLine.ColumnDefinitions[int.Parse(btn.Tag.ToString())].Width = new GridLength(dyna.Counter, GridUnitType.Star);
                btn.Content = dyna.Counter;
                btn.Background = dyna.Background;
                btn.DataContext = dyna;
                ToolTipService.SetPlacement(btn, System.Windows.Controls.Primitives.PlacementMode.Top);
                ToolTipService.SetToolTip(btn, string.Format("Stazioni in stato {0}: {1} su {2}.  (Totale calcolato sulle stazioni con collettore STLC1000 attivo)", dyna.Text, dyna.Counter, _totalCount));
            }
            else
            {
                GridLine.ColumnDefinitions[int.Parse(btn.Tag.ToString())].Width = new GridLength(0, GridUnitType.Auto);
                btn.Content = string.Empty;
                btn.Background = new SolidColorBrush(Colors.Transparent);
                ToolTipService.SetPlacement(btn, System.Windows.Controls.Primitives.PlacementMode.Top); 
                ToolTipService.SetToolTip(btn, string.Empty);
            }
        }

        private void SetButtons(ButtonDynaProperties sx, ButtonDynaProperties mid1, ButtonDynaProperties mid2, ButtonDynaProperties mid3, ButtonDynaProperties dx)
        {
            FillButtonWithDynaProperties(btnSx, sx);
            FillButtonWithDynaProperties(btnMid1, mid1);
            FillButtonWithDynaProperties(btnMid2, mid2);
            FillButtonWithDynaProperties(btnMid3, mid3);
            FillButtonWithDynaProperties(btnDx, dx);
             
            GridLine.ColumnDefinitions[0].MinWidth = (sx != null && sx.Counter > 0) ? 10 : 0;
            GridLine.ColumnDefinitions[1].MinWidth = (mid1 != null && mid1.Counter > 0) ? 5 : 0;
            GridLine.ColumnDefinitions[2].MinWidth = (mid2 != null && mid2.Counter > 0) ? 5 : 0;
            GridLine.ColumnDefinitions[3].MinWidth = (mid3 != null && mid3.Counter > 0) ? 5 : 0;
            GridLine.ColumnDefinitions[4].MinWidth = (dx != null && dx.Counter > 0) ? 10 : 0;
        }

        public bool IsTextWidthCompatible(Button btn)
        {
            if (btn.Content == null) return true;

            TextBlock measureBox = new TextBlock();
            measureBox.Text = btn.Content.ToString();
            measureBox.FontSize = btn.FontSize;
            measureBox.FontFamily = btn.FontFamily;

            return (btn.ActualWidth < measureBox.ActualWidth);
        }

        public int? SelectedStatus
        {
            get
            {
                if (_selectedButton != null )
                {
                    ButtonDynaProperties dyna = _selectedButton.DataContext as ButtonDynaProperties;
                    if (dyna != null)
                    {
                        return (int)dyna.Status;
                    }
                }
                return null;
            }
            set
            {
                if (value == null)
                {
                    SelectButton(_selectedButton);
                }
                else
                {
                    int status = (int)value;
                    if (TrySelectButtonByStatus(btnSx, status))
                        return;
                    if (TrySelectButtonByStatus(btnMid1, status))
                        return;
                    if (TrySelectButtonByStatus(btnMid2, status))
                        return;
                    if (TrySelectButtonByStatus(btnMid3, status))
                        return;
                    if (TrySelectButtonByStatus(btnDx, status))
                        return;
                }
            }
        }

        private bool TrySelectButtonByStatus(Button btn, int status)
        {
            ButtonDynaProperties dyna = btn.DataContext as ButtonDynaProperties;
            if (dyna != null && (int)dyna.Status == status)
            {
                SelectButton(btn);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SelectButton(Button selected)
        {
        	string stateName;
            if (_selectedButton != selected)
            {
            	stateName = "Deselected";
                _selectedButton = selected;
            }
            else
            {
				stateName = "Selected";
				_selectedButton = null;
            }

			VisualStateManager.GoToState(btnSx, stateName, true);
			VisualStateManager.GoToState(btnMid1, stateName, true);
			VisualStateManager.GoToState(btnMid2, stateName, true);
			VisualStateManager.GoToState(btnMid3, stateName, true);
			VisualStateManager.GoToState(btnDx, stateName, true);

			if ( selected != null ) VisualStateManager.GoToState(selected, "Selected", true);
		}

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            SelectButton(btn);
            if (this.Click != null)
            {
                ButtonDynaProperties dyna = (ButtonDynaProperties)btn.DataContext;
                this.Click(this, new LineChartClickEventArgs { Status = dyna.Status, Deselected = (_selectedButton == null) });
            }
        }

        private void GridLine_LayoutUpdated(object sender, EventArgs e)
        {
            if (IsTextWidthCompatible(btnSx)) btnSx.Content = string.Empty;
            if (IsTextWidthCompatible(btnMid1)) btnMid1.Content = string.Empty;
            if (IsTextWidthCompatible(btnMid2)) btnMid2.Content = string.Empty;
            if (IsTextWidthCompatible(btnMid3)) btnMid3.Content = string.Empty;
            if (IsTextWidthCompatible(btnDx)) btnDx.Content = string.Empty;  
        }

        private class ButtonDynaProperties
        {
            internal Brush Background { get; set; }
            internal string Text { get; set; }
            internal int Counter { get; set; }
            internal Status Status { get; set; }
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext != null)
            {
                LoadData((ObservableCollection<StatusCounter>)DataContext);
            }

        }
	}

    public class LineChartClickEventArgs : RoutedEventArgs
    {
        public Status Status { get; set; }
        public bool Deselected { get; set; }
    }
}
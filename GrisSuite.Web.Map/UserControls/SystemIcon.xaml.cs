﻿using System.Windows;
using System.Windows.Controls;
using GrisSuite.Web.Controls.SL;

namespace GrisSuite.Web.Map.UserControls
{
	public partial class SystemIcon : UserControl
	{
		public SystemIcon()
		{
			this.InitializeComponent();
		}

		public string SystemName
		{
			get { return (string)this.GetValue(SystemNameProperty); }
			set { this.SetValue(SystemNameProperty, value); }
		}

		public GrisSystems GrisSystem
		{
			get { return (GrisSystems)this.GetValue(GrisSystemProperty); }
			set { this.SetValue(GrisSystemProperty, value); }
		}

		// Using a DependencyProperty as the backing store for GrisSystem.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty GrisSystemProperty = DependencyProperty.Register("GrisSystem", typeof(GrisSystems),
																								   typeof(SystemIcon),
																								   new PropertyMetadata(GrisSystems.AltrePeriferiche,
																														PropertiesChangedCallback));

		public static readonly DependencyProperty SystemNameProperty = DependencyProperty.Register("SystemName", typeof(string), typeof(SystemIcon),
																								   new PropertyMetadata("", PropertiesChangedCallback));

		private static void PropertiesChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
		{
			SystemIcon systemIcon = dependencyObject as SystemIcon;
			if (systemIcon != null)
			{
				if (eventArgs.Property == GrisSystemProperty)
				{
					GrisSystems system = (GrisSystems)eventArgs.NewValue;
					switch (system)
					{
						case GrisSystems.DiffusioneSonora:
							systemIcon.SetSystem(systemIcon.IconDiffusioneSonora.Name);
							break;
						case GrisSystems.InformazioneVisiva:
							systemIcon.SetSystem(systemIcon.IconInformazioneVisiva.Name);
							break;
						case GrisSystems.Illuminazione:
							systemIcon.SetSystem(systemIcon.IconIlluminazione.Name);
							break;
						case GrisSystems.Telefonia:
							systemIcon.SetSystem(systemIcon.IconTelefonia.Name);
							break;
						case GrisSystems.Rete:
							systemIcon.SetSystem(systemIcon.IconRete.Name);
							break;
						case GrisSystems.ErogazioneEnergia:
							systemIcon.SetSystem(systemIcon.IconErogazioneEnergia.Name);
							break;
						case GrisSystems.FDS:
							systemIcon.SetSystem(systemIcon.IconFDS.Name);
							break;
						case GrisSystems.Diagnostica:
							systemIcon.SetSystem(systemIcon.IconDiagnostica.Name);
							break;
						case GrisSystems.Facilities:
							systemIcon.SetSystem(systemIcon.IconFacilities.Name);
							break;
						case GrisSystems.MonitoraggioVPNVerde:
							systemIcon.SetSystem(systemIcon.IconMonitoraggioVPNVerde.Name);
							break;
                        case GrisSystems.WebRadio:
                            systemIcon.SetSystem(systemIcon.IconWebRadio.Name);
                            break;
                        case GrisSystems.Orologi:
                            systemIcon.SetSystem(systemIcon.IconOrologi.Name);
                            break;
                        case GrisSystems.Ascensori:
                            systemIcon.SetSystem(systemIcon.IconAscensori.Name);
                            break;
                        default:
							systemIcon.SetSystem(systemIcon.IconAltrePeriferiche.Name);
							break;
					}
				}
			}
		}

		private void SetSystem(string controlName)
		{
			this.IconDiffusioneSonora.Visibility = (this.IconDiffusioneSonora.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconInformazioneVisiva.Visibility = (this.IconInformazioneVisiva.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconIlluminazione.Visibility = (this.IconIlluminazione.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconTelefonia.Visibility = (this.IconTelefonia.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconRete.Visibility = (this.IconRete.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconErogazioneEnergia.Visibility = (this.IconErogazioneEnergia.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconFDS.Visibility = (this.IconFDS.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconDiagnostica.Visibility = (this.IconDiagnostica.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconFacilities.Visibility = (this.IconFacilities.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
			this.IconMonitoraggioVPNVerde.Visibility = (this.IconMonitoraggioVPNVerde.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
            this.IconWebRadio.Visibility = (this.IconWebRadio.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
            this.IconOrologi.Visibility = (this.IconOrologi.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
            this.IconAscensori.Visibility = (this.IconAscensori.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
            this.IconAltrePeriferiche.Visibility = (this.IconAltrePeriferiche.Name == controlName) ? Visibility.Visible : Visibility.Collapsed;
		}
	}
}
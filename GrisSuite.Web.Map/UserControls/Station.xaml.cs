﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Devcorp.Controls.Design;
using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.MapService;
using Infragistics.Silverlight.Map;

namespace GrisSuite.Web.Map.UserControls
{
    public partial class Station
    {
        public delegate void LayersLoadedHandler(object sender, EventArgs e);

        public event EventHandler<ElementEventArgs> Hover;
        public event EventHandler<ElementEventArgs> Unhover;
        public event EventHandler<ElementEventArgs> Click;

        private MapElement mapElement;
        private Size stationInfoDetailSize = new Size(300, 150);
        private Boolean dataReady;

		public int FilterSystemId
		{
			get { return (int)this.GetValue(FilterSystemIdProperty); }
			set { this.SetValue(FilterSystemIdProperty, value); }
		}

		public static readonly DependencyProperty FilterSystemIdProperty = DependencyProperty.Register("FilterSystemId", typeof(int), typeof(Station),
																						   new PropertyMetadata(0, PropertiesChangedCallback));

		private static void PropertiesChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
		{
		}
		
		public Station()
        {
            this.InitializeComponent();
        }

        private void StationControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.mapElement = this.DataContext as MapElement;

            if (this.mapElement is PathElement)
            {
                this.Visibility = Visibility.Collapsed;
                return;
            }

            if (this.mapElement != null)
            {
                if (this.mapElement.HasCustomProperty("Status") && this.mapElement.GetProperty("Status") is GrisStatus)
                {
                    GrisStatus gs = (GrisStatus)this.mapElement.GetProperty("Status");

                    if (this.mapElement.HasCustomProperty("CustomColor"))
                    {
                        Color customColor = ColorSpaceHelper.GetColorFromHexString((string)this.mapElement.GetProperty("CustomColor"));

                        this.StationControl.Status = new GrisVisualState(gs, customColor);
                    }
                    else
                    {
                        this.StationControl.Status = new GrisVisualState(gs);
                    }

                    SymbolElement se = this.mapElement as SymbolElement;
                    if (se != null)
                    {
                        this.Height = se.SymbolSize;
                        this.Width = se.SymbolSize;
                    }

                    if (this.mapElement.HasCustomProperty("ClassificationValue") &&
                        this.mapElement.HasCustomProperty("Guid"))
                    {
                        this.StationInfoDetail.ClassificationValue =
                            this.mapElement.GetProperty("ClassificationValue").ToString();
                        this.StationInfoDetail.Id = (Guid)this.mapElement.GetProperty("Guid");
                        this.StationInfoDetail.WithServer = (bool)this.mapElement.GetProperty("WithServer");

                        if (this.mapElement.HasCustomProperty("CustomColor"))
                        {
                            Color customColor = ColorSpaceHelper.GetColorFromHexString((string)this.mapElement.GetProperty("CustomColor"));

                            this.StationInfoDetail.Status = new GrisVisualState(gs, customColor);
                        }
                        else
                        {
                            this.StationInfoDetail.Status = new GrisVisualState(gs);
                        }

                        this.StationInfoDetail.Title = this.mapElement.Name;
                        this.dataReady = true;
                    }
                }
            }
        }

        private void StationControl_Click(object sender, EventArgs e)
        {
            if (this.Click != null && this.mapElement != null && this.mapElement.HasCustomProperty("Id") &&
                this.mapElement.HasCustomProperty("TypeId") && this.mapElement.HasCustomProperty("Guid")
                // Disattiva il click sulle stazioni prive di STLC1000 e non attive
                &&
                !((this.mapElement.HasCustomProperty("WithServer") && (!(bool)this.mapElement.GetProperty("WithServer"))) &&
                  ((this.mapElement.HasCustomProperty("Status") && (this.mapElement.GetProperty("Status") is GrisStatus) &&
                    ((GrisStatus)this.mapElement.GetProperty("Status")) == GrisStatus.Maintenance)))
                &&
                !(((this.mapElement.HasCustomProperty("Status") && (this.mapElement.GetProperty("Status") is GrisStatus) &&
                    ((GrisStatus)this.mapElement.GetProperty("Status")) == GrisStatus.Maintenance)) &&
                  ((this.mapElement.HasCustomProperty("IsNodeAccessibleToUser") && !((bool)this.mapElement.GetProperty("IsNodeAccessibleToUser"))))))
            {
                ElementEventArgs ec = new ElementEventArgs
                                      {
                                          ObjectId = (long)this.mapElement.GetProperty("Id"),
                                          ObjectTypeId = (GrisMapElementType)this.mapElement.GetProperty("TypeId"),
                                          ObjectStatusId = (Guid)this.mapElement.GetProperty("Guid"),
                                          Name = this.mapElement.Name
                                      };

                this.Click(this, ec);
            }
        }

        private void StationControl_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!this.StationPopup.IsOpen && this.dataReady)
            {
	            this.StationInfoDetail.FilterSystemId = this.FilterSystemId;
                this.StationInfoDetail.LoadData();
                this.StationPopup.IsOpen = true;

                FrameworkElement win = Application.Current.RootVisual as FrameworkElement;
                Point pos = e.GetPosition(null);

                const double offset = 15;
                this.StationPopup.HorizontalOffset = offset;
                this.StationPopup.VerticalOffset = offset;

                if ((this.stationInfoDetailSize.Width > 0 && this.stationInfoDetailSize.Height > 0) && (win != null))
                {
                    if (pos.X + this.stationInfoDetailSize.Width > win.ActualWidth)
                    {
                        this.StationPopup.HorizontalOffset = -this.stationInfoDetailSize.Width - offset;
                    }

                    if (pos.Y + this.stationInfoDetailSize.Height > win.ActualHeight)
                    {
                        this.StationPopup.VerticalOffset = -this.stationInfoDetailSize.Height - offset;
                    }
                }
            }

            this.mapElement = this.DataContext as MapElement;
            if (this.Hover != null && this.mapElement != null && this.mapElement.HasCustomProperty("Id") &&
                this.mapElement.HasCustomProperty("TypeId") && this.mapElement.HasCustomProperty("Guid"))
            {
                ElementEventArgs ec = new ElementEventArgs
                                      {
                                          ObjectId = (long)this.mapElement.GetProperty("Id"),
                                          ObjectTypeId = (GrisMapElementType)this.mapElement.GetProperty("TypeId"),
                                          ObjectStatusId = (Guid)this.mapElement.GetProperty("Guid"),
                                          Name = this.mapElement.Name
                                      };
                this.Hover(this, ec);
            }
        }

        private void StationControl_MouseLeave(object sender, MouseEventArgs e)
        {
            this.StationPopup.IsOpen = false;

            this.mapElement = this.DataContext as MapElement;
            if (this.Unhover != null && this.mapElement != null && this.mapElement.HasCustomProperty("Id") &&
                this.mapElement.HasCustomProperty("TypeId") && this.mapElement.HasCustomProperty("Guid"))
            {
                ElementEventArgs ec = new ElementEventArgs
                                      {
                                          ObjectId = (long)this.mapElement.GetProperty("Id"),
                                          ObjectTypeId = (GrisMapElementType)this.mapElement.GetProperty("TypeId"),
                                          ObjectStatusId = (Guid)this.mapElement.GetProperty("Guid"),
                                          Name = this.mapElement.Name
                                      };
                this.Unhover(this, ec);
            }
        }

        private void StationInfoDetail_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.stationInfoDetailSize = e.NewSize;
        }
    }
}
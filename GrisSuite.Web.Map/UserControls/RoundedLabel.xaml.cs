﻿
using System.Windows;

namespace GrisSuite.Web.Map.UserControls
{
	public partial class RoundedLabel
	{
		public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string),
																						  typeof(RoundedLabel),
																						  new PropertyMetadata(
																							"",
																							TextPropertyChangedCallback
																						  ));

		public string Text
		{
			get
			{
				return GetValue(TextProperty).ToString();
			}
			set
			{
				this.SetValue(TextProperty, value);
			}
		}

		private static void TextPropertyChangedCallback ( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs )
		{
			if ( eventArgs.Property == TextProperty && dependencyObject is RoundedLabel ) ( (RoundedLabel)dependencyObject ).SetText(eventArgs.NewValue.ToString());
		}
		
		private void SetText ( string text )
		{
			this.tbText.Text = text;
		}

		public RoundedLabel ()
		{
			InitializeComponent();
		}
	}
}

﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using GrisSuite.Web.Map.Classes;
using GrisSuite.Web.Map.MapService;

namespace GrisSuite.Web.Map.UserControls
{
	public partial class ClassificationsPanel : UserControl
	{
		// Numero minino di secondi entro cui è possibile effettuare un refresh
		private const int MINIMUM_DELAY_REFRESH_SECONDS = 5;

		public delegate void ClickHandler(object sender, ClassificationClickEventArgs e);

		public event ClickHandler Click;
		public event EventHandler Refresh;

		private ObservableCollection<Classification> currentClassifications;
		private Classification selectedClassification;
		private int selectedClassificationIndex = -1;

		private ObservableCollection<GrisSystem> currentSystems;
		private int lastClickedFilterSystemid;
		private byte lastCheckedHideNotActiveNodes;

		private readonly DispatcherTimer delayedRefreshButtonEnabledTimer;

		#region Visual state names

		private const string NormalState = "Normal";
		private const string MouseOverState = "MouseOver";

		#endregion

		public ClassificationsPanel()
		{
			this.InitializeComponent();

			this.delayedRefreshButtonEnabledTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, MINIMUM_DELAY_REFRESH_SECONDS, 0)};
			this.delayedRefreshButtonEnabledTimer.Tick += this.delayedRefreshButtonEnabledTimer_Tick;

			MapServiceClient mapService = ProxiesFactory.GetMapService();
			mapService.GetClassificationsCompleted += this.mapService_GetClassificationsCompleted;
			mapService.GetClassificationsAsync();
			mapService.GetSystemsCompleted += this.mapService_GetSystemsCompleted;
			mapService.GetSystemsAsync();

			this.MouseEnter += (o, e) => VisualStateManager.GoToState(this, MouseOverState, true);
			this.MouseLeave += (o, e) => VisualStateManager.GoToState(this, NormalState, true);
		}

		private void mapService_GetSystemsCompleted(object sender, GetSystemsCompletedEventArgs e)
		{
			if ((e.Error == null) && (e.Result != null))
			{
				this.currentSystems = e.Result;
				this.cboSystems.ItemsSource = null;
				this.cboSystems.ItemsSource = this.currentSystems;
                if (this.currentSystems.Count > 0)
                {
                    this.cboSystems.SelectedIndex = 0;
                }
			}
		}

		private void mapService_GetClassificationsCompleted(object sender, GetClassificationsCompletedEventArgs e)
		{
			if ((e.Error == null) && (e.Result != null))
			{
				this.currentClassifications = e.Result;
				if (this.currentClassifications.Count > 0)
				{
					this.SelectedClassificationIndex = 0;
				}
			}
		}

		private void SetClassificationChecked(int index)
		{
			this.cboSystems.Visibility = Visibility.Collapsed;
			this.chkShowNotActive.Visibility = Visibility.Collapsed;
			this.stpSystems.Visibility = Visibility.Collapsed;
			this.stpShowNotActive.Visibility = Visibility.Collapsed;

			for (int i = 0; i < this.currentClassifications.Count; i++)
			{
				this.currentClassifications[i].IsChecked = (i == index);
				if (this.currentClassifications[i].IsChecked)
				{
					this.selectedClassificationIndex = i;
					this.selectedClassification = this.currentClassifications[i];

					if (this.currentClassifications[i].CanFilterBySystemId)
					{
						this.cboSystems.Visibility = Visibility.Visible;
						this.chkShowNotActive.Visibility = Visibility.Visible;
						this.stpSystems.Visibility = Visibility.Visible;
						this.stpShowNotActive.Visibility = Visibility.Visible;
					}
				}
			}

			this.buttonList.ItemsSource = null;
			this.buttonList.ItemsSource = this.currentClassifications;
		}

		public Classification SelectedClassification
		{
			get { return this.selectedClassification; }
			set
			{
				for (int i = 0; i < this.currentClassifications.Count; i++)
				{
					if (this.currentClassifications[i] == value)
					{
						this.SetClassificationChecked(i);
					}
				}
			}
		}

		public byte? SelectedClassificationId
		{
			get
			{
				if (this.selectedClassification != null)
				{
					return this.selectedClassification.Id;
				}

				return null;
			}
			set
			{
				for (int i = 0; i < this.currentClassifications.Count; i++)
				{
					if (this.currentClassifications[i].Id == value)
					{
						this.SetClassificationChecked(i);
					}
				}
			}
		}

		public int SelectedFilterSystemId
		{
			get
			{
				if (this.cboSystems.Items.Count > 0)
				{
					return ((GrisSystem) this.cboSystems.SelectedItem).SystemId;
				}

				return 0;
			}
		}

		public byte CheckedHideNotActiveNodes
		{
			get
			{
				// Su interfaccia si mostrano le stazioni prive di stato (bianche),
				// mentre nel codice e nei parametri, si gestisce il "nascondi stazioni non attive",
				// quindi logiche invertite
				if (this.chkShowNotActive.IsChecked.HasValue)
				{
					return this.chkShowNotActive.IsChecked.Value ? (byte) 0 : (byte) 1;
				}

				return 1;
			}
		}

		public int SelectedClassificationIndex
		{
			get { return this.selectedClassificationIndex; }
			set { this.SetClassificationChecked(value); }
		}

		private void ClassificationButton_Click(object sender, RoutedEventArgs e)
		{
			RadioButton button = (RadioButton) sender;
			Classification c = (Classification) button.DataContext;
			this.SetClassificationChecked(this.currentClassifications.IndexOf(c));

			int filterSystemId = 0;
			byte hideNotActiveNodes = 0;
			if (c.CanFilterBySystemId)
			{
				filterSystemId = this.SelectedFilterSystemId;
				this.lastClickedFilterSystemid = this.SelectedFilterSystemId;

				hideNotActiveNodes = this.CheckedHideNotActiveNodes;
				this.lastCheckedHideNotActiveNodes = this.CheckedHideNotActiveNodes;
			}

			if (this.Click != null)
			{
				ClassificationClickEventArgs ea = new ClassificationClickEventArgs
				{
					Classification = c,
					FilterSystemId = filterSystemId,
					HideNotActiveNodes = hideNotActiveNodes
				};
				this.Click(this, ea);
			}
		}

		private void mainBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
		}

		private void FullScreen_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Host.Content.IsFullScreen = !Application.Current.Host.Content.IsFullScreen;
			this.SetFullsceenButton();
		}

		private void SetFullsceenButton()
		{
			this.FullScreen.Visibility = Application.Current.Host.Content.IsFullScreen ? Visibility.Collapsed : Visibility.Visible;
			this.NormalScreen.Visibility = Application.Current.Host.Content.IsFullScreen ? Visibility.Visible : Visibility.Collapsed;
		}

		private void LayoutRoot_LayoutUpdated(object sender, EventArgs e)
		{
			this.SetFullsceenButton();
		}

		private void Refresh_Click(object sender, RoutedEventArgs e)
		{
			this.RefreshButton.IsEnabled = false;
			this.delayedRefreshButtonEnabledTimer.Start();

			if (this.Refresh != null)
			{
				this.Refresh(this, null);
			}
		}

		private void delayedRefreshButtonEnabledTimer_Tick(object sender, EventArgs e)
		{
			this.RefreshButton.IsEnabled = true;
			this.delayedRefreshButtonEnabledTimer.Stop();
		}

		private void cboSystems_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.RaiseClick();
		}

		private void chkShowNotActive_Checked(object sender, RoutedEventArgs e)
		{
			this.RaiseClick();
		}

		private void chkShowNotActive_Unchecked(object sender, RoutedEventArgs e)
		{
			this.RaiseClick();
		}

		private void RaiseClick()
		{
			if (this.Click != null)
			{
				if ((this.SelectedClassification != null) && (this.SelectedClassification.CanFilterBySystemId) && (this.SelectedFilterSystemId >= 0) &&
				    ((this.lastClickedFilterSystemid != this.SelectedFilterSystemId) || (this.lastCheckedHideNotActiveNodes != this.CheckedHideNotActiveNodes)))
				{
					this.lastClickedFilterSystemid = this.SelectedFilterSystemId;
					this.lastCheckedHideNotActiveNodes = this.CheckedHideNotActiveNodes;

					ClassificationClickEventArgs ea = new ClassificationClickEventArgs
					{
						Classification = this.SelectedClassification,
						FilterSystemId = this.SelectedFilterSystemId,
						HideNotActiveNodes = this.CheckedHideNotActiveNodes
					};
					this.Click(this, ea);
				}
			}
		}
	}

	public class ClassificationClickEventArgs : EventArgs
	{
		public Classification Classification { get; set; }
		public int FilterSystemId { get; set; }
		public byte HideNotActiveNodes { get; set; }
	}
}
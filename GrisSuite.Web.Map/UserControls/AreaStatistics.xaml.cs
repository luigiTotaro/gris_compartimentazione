﻿using System;
using System.Windows;
using System.Windows.Media;
using GrisSuite.Web.Controls.SL;
using GrisSuite.Web.Map.StatusCountersService;

namespace GrisSuite.Web.Map.UserControls
{
    public partial class AreaStatistics
    {
        private const string FORCED_SEV_LEVEL_MESSAGE_1 = "* Il valore è stato forzato, perché contempla problemi con almeno un Pannello Zone";

        private const string FORCED_SEV_LEVEL_MESSAGE_2 = "* Il valore è stato forzato, perché il collettore/i STLC1000 della stazione è non raggiungibile";

        private const string FORCED_SEV_LEVEL_MESSAGE_4 = "* Il valore è stato forzato, perché tutti gli elementi dipendenti da quello selezionato sono non raggiungibili";

        private const string FORCED_SEV_LEVEL_MESSAGE_8 = "* Il valore è stato forzato, perché i dati raccolti dal collettore non sono completi";

        private const string PLATINUM_MESSAGE = "Il calcolo della media non considera mai le stazioni platinum.";

        private const string COLLETTORE_STLC_NON_PRESENTE = "Collettore STLC1000 non presente.";

        private const string FORMULA_DESCRIPTION = "";

        public AreaStatistics()
        {
            this.InitializeComponent();
        }

        public void LoadData()
        {
            this.txbFormulaDescription.Text = FORMULA_DESCRIPTION;

            if ((this.DataContext != null) && (this.DataContext is AreaCountersByEntityState))
            {
                this.ChangeDetailsVisibility(true);

                AreaCountersByEntityState data = (AreaCountersByEntityState) this.DataContext;

                this.txbPercentageError.Text = string.Format("{0:F0}%", Math.Round(data.TotErrorPercentage*100));
                this.txbPercentageWarning.Text = string.Format("{0:F0}%", Math.Round(data.TotWarningPercentage*100));
                this.txbPercentageOk.Text = string.Format("{0:F0}%", Math.Round(data.TotOkPercentage*100));

                if (data.SevLevel == (int) GrisStatus.Maintenance)
                {
                    this.ChangeDetailsVisibility(false);
                }
                else
                {
                    this.UpdateMeanSevLevel(data.AvgValue, data.SevLevel, data.ForcedSevLevel, data.IsWithServerObject, data.ObjectTypeId);
                }
            }
            else
            {
                this.ChangeDetailsVisibility(false);
            }
        }

        public void ChangeDetailsVisibility(bool showDetails)
        {
            this.brdHideTitle.Opacity = (showDetails ? 0 : 1);
            this.brdHideGrid.Opacity = (showDetails ? 0 : 1);
        }

        private void UpdateMeanSevLevel(decimal avgValue, int sevLevel, byte forcedLevel, bool isWithServerObject, int objectTypeId)
        {
            if (this.txbSevLevel.Text != null)
            {
                if (isWithServerObject)
                {
                    this.txbNote2.Text = PLATINUM_MESSAGE;
                }
                else
                {
                    this.txbNote2.Text = string.Format("{0} {1}", PLATINUM_MESSAGE, COLLETTORE_STLC_NON_PRESENTE);
                }

                this.txbSevLevel.Text = string.Format("{0:0.##}{1}", avgValue, (forcedLevel > 0 ? "*" : string.Empty));

                switch (forcedLevel)
                {
                    case 0:
                        this.txbNote.Text = string.Empty;
                        break;
                    case 1:
                        this.txbNote.Text = FORCED_SEV_LEVEL_MESSAGE_1;
                        break;
                    case 2:
                        this.txbNote.Text = FORCED_SEV_LEVEL_MESSAGE_2;
                        break;
                    case 4:
                        this.txbNote.Text = FORCED_SEV_LEVEL_MESSAGE_4;
                        break;
                    case 8:
                        this.txbNote.Text = FORCED_SEV_LEVEL_MESSAGE_8;
                        break;
                    default:
                        this.txbNote.Text = string.Empty;
                        break;
                }

                this.pthTriangleOk.Visibility = Visibility.Collapsed;
                this.pthTriangleWarning.Visibility = Visibility.Collapsed;
                this.pthTriangleError.Visibility = Visibility.Collapsed;

                switch (sevLevel)
                {
                    case 0:
                        this.brdSevLevel.Background = (SolidColorBrush) Application.Current.Resources["BrushOk"];
                        this.pthTriangleOk.Visibility = Visibility.Visible;
                        break;
                    case 1:
                        this.brdSevLevel.Background = (SolidColorBrush) Application.Current.Resources["BrushWarning"];
                        this.pthTriangleWarning.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        this.brdSevLevel.Background = (SolidColorBrush) Application.Current.Resources["BrushError"];
                        this.pthTriangleError.Visibility = Visibility.Visible;
                        break;
                    case 9:
                        this.brdSevLevel.Background = (SolidColorBrush) Application.Current.Resources["BrushMaintenance"];
                        break;
                    case 255:
                        this.brdSevLevel.Background = (SolidColorBrush) Application.Current.Resources["BrushUnknown"];
                        break;
                    default:
                        this.brdSevLevel.Background = (SolidColorBrush) Application.Current.Resources["BrushUnknown"];
                        break;
                }
            }
        }

        public static readonly DependencyProperty PercentualeOggettiTitleProperty = DependencyProperty.Register("PercentualeOggettiTitle", typeof (string), typeof (AreaStatistics),
                                                                                                                new PropertyMetadata("", PropertiesChangedCallback));

        public string PercentualeOggettiTitle
        {
            get { return (string) this.GetValue(PercentualeOggettiTitleProperty); }
            set { this.SetValue(PercentualeOggettiTitleProperty, value); }
        }

        private static void PropertiesChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            AreaStatistics areaStatistics = dependencyObject as AreaStatistics;
            if (areaStatistics != null)
            {
                if (eventArgs.Property == PercentualeOggettiTitleProperty)
                {
                    areaStatistics.txbPercentualeOggettiTitle.Text = (eventArgs.NewValue != null) ? eventArgs.NewValue.ToString() : string.Empty;
                }
            }
        }
    }
}
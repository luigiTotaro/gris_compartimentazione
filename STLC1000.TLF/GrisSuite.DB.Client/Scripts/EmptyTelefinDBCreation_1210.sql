USE [master]
GO

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'Telefin')
BEGIN
	DECLARE @ActiveConnections AS INT

	SELECT
	@ActiveConnections = (select count(*) from master.dbo.sysprocesses p where dtb.database_id=p.dbid)
	FROM
	master.sys.databases AS dtb
	WHERE
	(dtb.name=N'Telefin')

	IF (@ActiveConnections > 0)
	BEGIN
		ALTER DATABASE [Telefin] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
	END

	DROP DATABASE [Telefin]
END

CREATE DATABASE [Telefin] ON  PRIMARY 
( NAME = N'Telefin', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\Telefin.mdf' , SIZE = 2240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Telefin_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\Telefin_log.LDF' , SIZE = 560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'Telefin', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Telefin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Telefin] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [Telefin] SET ANSI_NULLS ON 
GO
ALTER DATABASE [Telefin] SET ANSI_PADDING ON 
GO
ALTER DATABASE [Telefin] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [Telefin] SET ARITHABORT ON 
GO
ALTER DATABASE [Telefin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Telefin] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Telefin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Telefin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Telefin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Telefin] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [Telefin] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [Telefin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Telefin] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [Telefin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Telefin] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Telefin] SET AUTO_UPDATE_STATISTICS_ASYNC ON 
GO
ALTER DATABASE [Telefin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Telefin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Telefin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Telefin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Telefin] SET  READ_WRITE 
GO
ALTER DATABASE [Telefin] SET RECOVERY FULL 
GO
ALTER DATABASE [Telefin] SET  MULTI_USER 
GO
ALTER DATABASE [Telefin] SET PAGE_VERIFY NONE  
GO
ALTER DATABASE [Telefin] SET DB_CHAINING OFF 
GO
USE [Telefin]
GO
/****** Object:  User [STLC1000AppServices]    Script Date: 01/09/2008 17:04:11 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'STLC1000AppServices')
CREATE USER [STLC1000AppServices] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SWMAdmins]    Script Date: 01/09/2008 17:04:11 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'SWMAdmins')
CREATE USER [SWMAdmins] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SWMUsers]    Script Date: 01/09/2008 17:04:11 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'SWMUsers')
CREATE USER [SWMUsers] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[severity]    Script Date: 01/09/2008 17:03:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[severity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[severity](
	[SevLevel] [int] NOT NULL,
	[Description] [varchar](128) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_severity] PRIMARY KEY CLUSTERED 
(
	[SevLevel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[systems]    Script Date: 01/09/2008 17:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[systems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[systems](
	[SystemID] [int] NOT NULL,
	[SystemDescription] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
 CONSTRAINT [PK_systems] PRIMARY KEY CLUSTERED 
(
	[SystemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[regions]    Script Date: 01/09/2008 17:03:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[regions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[regions](
	[RegID] [bigint] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_regions] PRIMARY KEY CLUSTERED 
(
	[RegID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[vendors]    Script Date: 01/09/2008 17:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vendors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vendors](
	[VendorID] [int] NOT NULL,
	[VendorName] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
	[VendorDescription] [varchar](512) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_vendors] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[reference]    Script Date: 01/09/2008 17:03:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reference]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[reference](
	[ReferenceID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_field_reference_ReferenceID]  DEFAULT (newid()),
	[Value] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[DateTime] [datetime] NOT NULL CONSTRAINT [DF_reference_DateTime]  DEFAULT (getdate()),
	[Visible] [tinyint] NULL,
	[DeltaDevID] [bigint] NULL,
	[DeltaStrID] [int] NULL,
	[DeltaFieldID] [int] NULL,
	[DeltaArrayID] [int] NULL,
 CONSTRAINT [PK_field_reference] PRIMARY KEY CLUSTERED 
(
	[ReferenceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[station]    Script Date: 01/09/2008 17:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[station]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[station](
	[StationID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_station_StationID]  DEFAULT (newid()),
	[StationXMLID] [int] NULL,
	[StationName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_station] PRIMARY KEY CLUSTERED 
(
	[StationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[parameters]    Script Date: 01/09/2008 17:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[parameters](
	[ParameterName] [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
	[ParameterValue] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
	[ParameterDescription] [varchar](1024) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_parameters] PRIMARY KEY CLUSTERED 
(
	[ParameterName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  StoredProcedure [dbo].[util_ShrinkTransactionLog]    Script Date: 01/09/2008 17:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[util_ShrinkTransactionLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[util_ShrinkTransactionLog]
(
	@TriggerTranLogSize INT = 50
)
AS
	
	-- @TriggerTranLogSize è la grandezza massima in MB oltre la quale viene eseguito lo shrink del file
	
	DECLARE @RETURN BIT;
	DECLARE @LogFileName VARCHAR(500)
	DECLARE @PagesNumb INT

	SELECT @LogFileName = [name], @PagesNumb = [size] FROM sys.database_files WHERE type_desc = ''LOG''

	IF (''SIMPLE'' = (SELECT recovery_model_desc FROM sys.databases WHERE [name] = db_name()))
	BEGIN 
		BEGIN TRY
			IF ((@PagesNumb*8/1024) > @TriggerTranLogSize)
			BEGIN
				DBCC SHRINKFILE (@LogFileName) WITH NO_INFOMSGS;
				SET @RETURN = 1;
			END
			ELSE
			BEGIN
				SET @RETURN = 0;
			END
		END TRY
		BEGIN CATCH
			SET @RETURN = 0;
		END CATCH
	END
	ELSE 
	BEGIN
		--ALTER DATABASE Telefin
		--SET RECOVERY SIMPLE;
		--GO

		--DBCC SHRINKFILE (Telefin);
		--GO

		--ALTER DATABASE Telefin
		--SET RECOVERY FULL;
		SET @RETURN = 0;
	END;
	SELECT @RETURN;
RETURN 0;
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_Report_GetConfigurationInUsePorts]    Script Date: 01/09/2008 17:03:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_Report_GetConfigurationInUsePorts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_Report_GetConfigurationInUsePorts]
	@SrvID int
AS
	SELECT 
		ResultExt.SupervisorSystemXML.query(''.'') AS PortNode,
		ResultExt.SupervisorSystemXML.value(''@name[1]'', ''varchar(64)'') AS [Name],
		ResultExt.SupervisorSystemXML.value(''@type[1]'', ''varchar(64)'') AS [Type],
		ResultExt.SupervisorSystemXML.value(''@timeout[1]'', ''varchar(64)'') AS [Timeout]
	FROM servers EXT CROSS APPLY SupervisorSystemXML.nodes(''//port/item'') AS ResultExt(SupervisorSystemXML)
	WHERE (SrvID = @SrvID);
' 
END
GO
/****** Object:  Table [dbo].[servers]    Script Date: 01/09/2008 17:03:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[servers](
	[SrvID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Host] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[FullHostName] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[IP] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[LastUpdate] [datetime] NULL,
	[LastMessageType] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[SupervisorSystemXML] [xml] NULL,
	[ClientSupervisorSystemXMLValidated] [xml] NULL,
	[ClientValidationSign] [varbinary](128) NULL,
	[ClientDateValidationRequested] [datetime] NULL,
	[ClientDateValidationObtained] [datetime] NULL,
	[ClientKey] [varbinary](148) NULL,
 CONSTRAINT [PK_servers] PRIMARY KEY CLUSTERED 
(
	[SrvID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[devices]    Script Date: 01/09/2008 17:03:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[devices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[devices](
	[DevID] [bigint] NOT NULL,
	[NodID] [bigint] NULL,
	[SrvID] [int] NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Type] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[SN] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[Addr] [varchar](32) COLLATE Latin1_General_CI_AS NULL,
	[PortId] [uniqueidentifier] NULL,
	[ProfileID] [int] NULL,
	[Active] [tinyint] NULL,
	[Scheduled] [tinyint] NULL,
	[Removed] [tinyint] NULL,
	[RackID] [uniqueidentifier] NULL,
	[RackPositionRow] [int] NULL,
	[RackPositionCol] [int] NULL,
	[DefinitionVersion] [varchar](8) COLLATE Latin1_General_CI_AS NULL,
	[ProtocolDefinitionVersion] [varchar](8) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_devices] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[streams]    Script Date: 01/09/2008 17:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[streams]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[streams](
	[DevID] [bigint] NOT NULL,
	[StrID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Visible] [tinyint] NULL,
	[Data] [image] NULL,
	[DateTime] [datetime] NOT NULL CONSTRAINT [DF_streams_DateTime]  DEFAULT (getdate()),
	[SevLevel] [int] NULL,
	[Description] [text] COLLATE Latin1_General_CI_AS NULL,
	[Processed] [tinyint] NULL,
 CONSTRAINT [PK_streams] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC,
	[StrID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[device_status]    Script Date: 01/09/2008 17:03:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[device_status]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[device_status](
	[DevID] [bigint] NOT NULL,
	[SevLevel] [int] NULL,
	[Description] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[Offline] [tinyint] NULL,
	[AckFlag] [bit] NOT NULL CONSTRAINT [DF_device_status_AckFlag]  DEFAULT ((0)),
	[AckDate] [datetime] NULL,
 CONSTRAINT [PK_device_status] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[stream_fields]    Script Date: 01/09/2008 17:04:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stream_fields]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[stream_fields](
	[DevID] [bigint] NOT NULL,
	[StrID] [int] NOT NULL,
	[FieldID] [int] NOT NULL,
	[ArrayID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[SevLevel] [int] NULL,
	[Value] [varchar](1024) COLLATE Latin1_General_CI_AS NULL,
	[Description] [text] COLLATE Latin1_General_CI_AS NULL,
	[Visible] [tinyint] NULL,
	[ReferenceID] [uniqueidentifier] NULL,
	[AckFlag] [bit] NOT NULL CONSTRAINT [DF_stream_fields_AckFlag]  DEFAULT ((0)),
	[AckDate] [datetime] NULL,
 CONSTRAINT [PK_stream_fields_1] PRIMARY KEY CLUSTERED 
(
	[FieldID] ASC,
	[ArrayID] ASC,
	[StrID] ASC,
	[DevID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[device_type]    Script Date: 01/09/2008 17:03:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[device_type]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[device_type](
	[DeviceTypeID] [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
	[SystemID] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
	[DeviceTypeDescription] [varchar](512) COLLATE Latin1_General_CI_AS NULL,
	[WSUrlPattern] [varchar](512) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_device_type] PRIMARY KEY CLUSTERED 
(
	[DeviceTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[nodes]    Script Date: 01/09/2008 17:03:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[nodes](
	[NodID] [bigint] NOT NULL,
	[ZonID] [bigint] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
	[Meters] [int] NULL,
 CONSTRAINT [PK_nodes] PRIMARY KEY CLUSTERED 
(
	[NodID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[zones]    Script Date: 01/09/2008 17:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zones](
	[ZonID] [bigint] NOT NULL,
	[RegID] [bigint] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_zones] PRIMARY KEY CLUSTERED 
(
	[ZonID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[rack]    Script Date: 01/09/2008 17:03:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rack]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rack](
	[RackID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_rack_RackID]  DEFAULT (newid()),
	[RackXMLID] [int] NULL,
	[BuildingID] [uniqueidentifier] NULL,
	[RackName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[RackType] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[RackDescription] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_rack] PRIMARY KEY CLUSTERED 
(
	[RackID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[building]    Script Date: 01/09/2008 17:03:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[building]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[building](
	[BuildingID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_building_BuildingID]  DEFAULT (newid()),
	[BuildingXMLID] [int] NULL,
	[StationID] [uniqueidentifier] NULL,
	[BuildingName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[BuildingDescription] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_building] PRIMARY KEY CLUSTERED 
(
	[BuildingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[port]    Script Date: 01/09/2008 17:03:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[port]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[port](
	[PortID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_port_PortID]  DEFAULT (newid()),
	[PortXMLID] [int] NULL,
	[PortName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[PortType] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Parameters] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Status] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
	[SrvID] [int] NOT NULL,
 CONSTRAINT [PK_port] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[procedures]    Script Date: 01/09/2008 17:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procedures]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[procedures](
	[DevID] [bigint] NOT NULL,
	[ProID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[ExeCount] [tinyint] NULL,
	[LastExecution] [datetime] NULL,
	[InProgress] [tinyint] NULL,
 CONSTRAINT [PK_procedures] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC,
	[ProID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetDevPathbyDevID]    Script Date: 01/09/2008 17:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetDevPathbyDevID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetDevPathbyDevID]	@DevID bigint
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	regions.regid, zones.zonid, nodes.nodid, devices.devid
	FROM	regions, zones, nodes, devices
	WHERE	devices.nodid=nodes.nodid AND
			nodes.zonid=zones.zonid AND
			zones.regid=regions.regid AND
			devices.devid=@DevID

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetDevices_List]    Script Date: 01/09/2008 17:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDevices_List]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDevices_List]
(
	@ServerName varchar(64),
	@ServerHost varchar(64),
	@NodeName varchar(64),	   -- Stazioni
	@ZoneName varchar(64),	   -- Linea
	@RegionName varchar(64),   -- Compartimenti
	@DeviceName varchar(64),
	@DevID bigint
)
AS
SET NOCOUNT ON;

SELECT     servers.Name AS ServerName, servers.Host AS ServerHost, nodes.Name AS NodeName, zones.Name AS ZoneName, regions.Name AS RegionName, 
                      devices.Name, devices.Type, devices.SN, devices.PortID, devices.Addr, devices.ProfileID, devices.Active, devices.Scheduled, devices.DevID, 
                      nodes.NodID, zones.ZonID, regions.RegID, servers.SrvID
FROM         devices INNER JOIN
                      nodes ON devices.NodID = nodes.NodID INNER JOIN
                      servers ON devices.SrvID = servers.SrvID INNER JOIN
                      zones ON nodes.ZonID = zones.ZonID INNER JOIN
                      regions ON zones.RegID = regions.RegID
WHERE (ISNULL(@ServerName,'''') = '''' OR  servers.Name LIKE @ServerName)
AND (ISNULL(@ServerHost,'''') = '''' OR servers.Host LIKE @ServerHost) 
AND (ISNULL(@NodeName,'''') = '''' OR nodes.Name LIKE @NodeName) 
AND (ISNULL(@ZoneName,'''')= '''' OR zones.Name LIKE @ZoneName) 
AND (ISNULL(@RegionName,'''') = '''' OR regions.Name LIKE @RegionName) 
AND (ISNULL(@DeviceName,'''') = '''' OR devices.Name LIKE @DeviceName) 
AND (ISNULL(@DevID,0) =0 OR devices.DevID = @DevID)
AND	ISNULL(devices.Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetDevDetailsbyDevID]    Script Date: 01/09/2008 17:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetDevDetailsbyDevID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetDevDetailsbyDevID] @DevID bigint
AS
BEGIN
	SET NOCOUNT ON;
	SELECT     d.Name, d.Type, d_s.Offline, nodes.Name AS Nodo, zones.Name AS Zona, regions.Name AS Regione, servers.Host, d_s.Description AS Stato, d.SN, 
						  d.Addr, port.PortName, rack.RackName, building.BuildingName, d_s.SevLevel, port.PortType
	FROM         device_status AS d_s INNER JOIN
						  devices AS d ON d_s.DevID = d.DevID INNER JOIN
						  nodes ON d.NodID = nodes.NodID INNER JOIN
						  zones ON nodes.ZonID = zones.ZonID INNER JOIN
						  regions ON zones.RegID = regions.RegID INNER JOIN
						  servers ON d.SrvID = servers.SrvID INNER JOIN
						  port ON d.PortId = port.PortID AND servers.SrvID = port.SrvID INNER JOIN
						  rack ON d.RackID = rack.RackID INNER JOIN
						  building ON rack.BuildingID = building.BuildingID
	WHERE     (d.Removed = 0) AND (port.Removed = 0) AND (rack.Removed = 0) AND (building.Removed = 0) AND (d.DevID = @DevID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelNodes]    Script Date: 01/09/2008 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelNodes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelNodes]
(
	@Original_NodID bigint
)
AS
	SET NOCOUNT ON;
DELETE FROM [nodes] WHERE (([NodID] = @Original_NodID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdNodes]    Script Date: 01/09/2008 17:03:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdNodes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdNodes]    Script Date: 11/20/2006 18:19:08 ******/
CREATE PROCEDURE [dbo].[tf_UpdNodes]
(
	@NodID bigint,
	@ZonID bigint,
	@Name varchar(64),
	@Removed bit,
	@Meters int
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT NodID FROM nodes WHERE (NodID = @NodID))
		UPDATE [nodes] SET [NodID] = @NodID, [ZonID] = @ZonID, [Name] = @Name, [Removed] = @Removed, Meters = @Meters WHERE (([NodID] = @NodID));
	ELSE
		INSERT INTO [nodes] ([NodID], [ZonID], [Name], [Removed], Meters) VALUES (@NodID, @ZonID, @Name, @Removed, @Meters);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetNodes]    Script Date: 01/09/2008 17:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetNodes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetNodes]    Script Date: 11/20/2006 18:19:00 ******/
CREATE PROCEDURE [dbo].[tf_GetNodes]
AS
	SET NOCOUNT ON;
	SELECT NodID, ZonID, Name, Removed, Meters
	FROM nodes
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_FindDev]    Script Date: 01/09/2008 17:03:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_FindDev]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_FindDev] @Chiave varchar(45)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  devices.DevID,
			devices.[Name],
			d_s.Description,
			nodes.[Name] AS Nodo,
			severity.Description AS Status,
			d_s.SevLevel
	FROM    devices, device_status d_s, severity, nodes
	WHERE   devices.DevID = d_s.DevID AND
			devices.NodID = nodes.NodID AND
			d_s.SevLevel = severity.SevLevel AND
			devices.Removed = 0	AND
			(devices.[Name] like @Chiave)
	ORDER BY d_s.SevLevel DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetBadDevList]    Script Date: 01/09/2008 17:03:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetBadDevList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetBadDevList] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  devices.DevID, devices.[Name], d_s.Description, severity.Description AS [Status], d_s.SevLevel
	FROM	devices, device_status d_s, severity
	WHERE	devices.DevID = d_s.DevID
			AND	d_s.SevLevel = severity.SevLevel
			AND	devices.Removed = 0
			AND d_s.Offline = 0
			AND d_s.SevLevel > 0
	ORDER BY d_s.SevLevel DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_Report_GetDeviceType]    Script Date: 01/09/2008 17:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_Report_GetDeviceType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_Report_GetDeviceType]
AS
	SELECT DISTINCT device_type.DeviceTypeID, device_type.DeviceTypeDescription, devices.DefinitionVersion, devices.ProtocolDefinitionVersion
	FROM devices 
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	ORDER BY device_type.DeviceTypeDescription;
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_Report_GetDevices]    Script Date: 01/09/2008 17:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_Report_GetDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_Report_GetDevices]
AS
	SELECT devices.DevID, devices.Name, devices.SN, devices.Type, device_type.DeviceTypeDescription, devices.DefinitionVersion, devices.ProtocolDefinitionVersion
	FROM devices 
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	ORDER BY devices.Name;
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetZones]    Script Date: 01/09/2008 17:03:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetZones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetZones]    Script Date: 11/20/2006 18:19:06 ******/
CREATE PROCEDURE [dbo].[tf_GetZones]
AS
	SET NOCOUNT ON;
	SELECT ZonID, RegID, Name, Removed
	FROM zones
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelZones]    Script Date: 01/09/2008 17:03:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelZones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelZones]
(
	@Original_ZonID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [zones] WHERE (([ZonID] = @Original_ZonID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdZones]    Script Date: 01/09/2008 17:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdZones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdZones]    Script Date: 11/20/2006 18:19:11 ******/

CREATE PROCEDURE [dbo].[tf_UpdZones]
(
	@ZonID bigint,
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT ZonID FROM zones WHERE (ZonID = @ZonID))
		UPDATE [zones] SET [ZonID] = @ZonID, [RegID] = @RegID, [Name] = @Name, [Removed] = @Removed WHERE (([ZonID] = @ZonID));
	ELSE
		INSERT INTO [zones] ([ZonID], [RegID], [Name],[Removed]) VALUES (@ZonID, @RegID, @Name, @Removed);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelRegions]    Script Date: 01/09/2008 17:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelRegions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelRegions]
(
	@Original_RegID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [regions] WHERE (([RegID] = @Original_RegID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetRegions]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetRegions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetRegions]    Script Date: 11/20/2006 18:19:04 ******/
CREATE PROCEDURE [dbo].[tf_GetRegions]
AS
	SET NOCOUNT ON;
	SELECT RegID, Name, Removed
	FROM regions
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdRegions]    Script Date: 01/09/2008 17:03:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdRegions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdRegions]    Script Date: 11/20/2006 18:19:09 ******/
CREATE PROCEDURE [dbo].[tf_UpdRegions]
(
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT RegID FROM regions WHERE (RegID = @RegID))
		UPDATE [regions] SET [RegID] = @RegID, [Name] = @Name, [Removed] = @Removed WHERE (([RegID] = @RegID));
	ELSE
		INSERT INTO [regions] ([RegID], [Name], [Removed]) VALUES (@RegID, @Name, @Removed);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdStream_Fields]    Script Date: 01/09/2008 17:03:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdStream_Fields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdStream_Fields]    Script Date: 11/20/2006 18:19:10 ******/
CREATE PROCEDURE [dbo].[tf_UpdStream_Fields]
(
	@FieldID int,
	@ArrayID int,
	@StrID int,
	@DevID bigint,
	@Name varchar(64),
	@SevLevel int,
	@Value varchar(1024),
	@Description text,
	@Visible tinyint,
	@IsDeleted bit,
	@ReferenceID uniqueidentifier
)
AS
	SET NOCOUNT OFF;

IF EXISTS (SELECT FieldID FROM stream_fields WITH(UPDLOCK) WHERE (ArrayID = @ArrayID) AND (DevID = @DevID) AND (FieldID = @FieldID) AND (StrID = @StrID))
	IF @IsDeleted = 0
		UPDATE [stream_fields] SET [FieldID] = @FieldID, [ArrayID] = @ArrayID, [StrID] = @StrID, [DevID] = @DevID, [Name] = @Name, [SevLevel] = @SevLevel, [Value] = @Value, [Description] = @Description, [Visible] = @Visible, ReferenceID = @ReferenceID WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
	ELSE
		DELETE FROM [stream_fields] WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
ELSE
	INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], ReferenceID) VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, @Visible, @ReferenceID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelStream_Fields]    Script Date: 01/09/2008 17:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelStream_Fields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelStream_Fields]
(
	@Original_FieldID int,
	@Original_ArrayID int,
	@Original_StrID int,
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	
	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE (([DeltaFieldID] = @Original_FieldID) 
			AND ([DeltaArrayID] = @Original_ArrayID) 
			AND ([DeltaStrID] = @Original_StrID) 
			AND ([DeltaDevID] = @Original_DevID))

	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE [ReferenceId] IN  (SELECT [reference].[ReferenceId]	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE (([FieldID] = @Original_FieldID) 
									AND ([ArrayID] = @Original_ArrayID) 
									AND ([StrID] = @Original_StrID) 
									AND ([DevID] = @Original_DevID))
							 )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE (([FieldID] = @Original_FieldID) 
		AND ([ArrayID] = @Original_ArrayID) 
		AND ([StrID] = @Original_StrID) 
		AND ([DevID] = @Original_DevID))
	
	DELETE FROM [stream_fields] 
	WHERE (([FieldID] = @Original_FieldID) 
		AND ([ArrayID] = @Original_ArrayID) 
		AND ([StrID] = @Original_StrID) 
		AND ([DevID] = @Original_DevID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelDeviceStatus]    Script Date: 01/09/2008 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelDeviceStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_DelDeviceStatus]    Script Date: 11/20/2006 18:18:33 ******/
CREATE PROCEDURE [dbo].[tf_DelDeviceStatus]
(
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (	SELECT reference.ReferenceID FROM [reference] 
							INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
							WHERE ([DevID] = @Original_DevID) )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] = @Original_DevID)

	DELETE FROM [stream_fields] WHERE ([DevID] = @Original_DevID)
	DELETE FROM [streams] WHERE ([DevID] = @Original_DevID)
	DELETE FROM [device_status] WHERE ([DevID] = @Original_DevID)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetReference]    Script Date: 01/09/2008 17:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetReference]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetReference]
AS
	SET NOCOUNT ON;

	SELECT ReferenceID, Value, DateTime, Visible, DeltaDevID, DeltaStrID, DeltaFieldID, DeltaArrayID
	FROM reference
	WHERE ReferenceID IN 
		(SELECT DISTINCT ReferenceID 
			FROM Stream_Fields INNER JOIN Devices ON Stream_Fields.DevID = Devices.DevID
			WHERE ISNULL(Removed,0) = 0)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetFieldsbyDevID_StrID]    Script Date: 01/09/2008 17:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetFieldsbyDevID_StrID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetFieldsbyDevID_StrID]
			@DevID bigint,
			@StrID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	* 
	FROM	[stream_fields]
	WHERE	([DevID] = @DevID) AND (Visible = 1) AND
			([StrID] = @StrID)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelReference]    Script Date: 01/09/2008 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelReference]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelReference]
(
	@Original_ReferenceID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	
	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE ReferenceId =@Original_ReferenceID;
	
	DELETE FROM [reference] WHERE (([ReferenceID] = @Original_ReferenceID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelServers]    Script Date: 01/09/2008 17:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelServers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelServers]
(
	@Original_SrvID int
)
AS
	SET NOCOUNT ON; -- se off e non c''è la riga ADO.NET da errore

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))
	
	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN ( 
		SELECT Reference.ReferenceID
		FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
		WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	)

	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))

	DELETE FROM [stream_fields] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [streams]		WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [device_status] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [devices]		WHERE [SrvID] = @Original_SrvID
	UPDATE [devices] SET PortId = null WHERE PortID in (SELECT PortId FROM [port] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [port]			WHERE [SrvID] = @Original_SrvID
	DELETE FROM [servers]		WHERE [SrvID] = @Original_SrvID
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetStream_Fields]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStream_Fields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetStream_Fields]    Script Date: 11/20/2006 18:19:04 ******/
CREATE PROCEDURE [dbo].[tf_GetStream_Fields]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelStreams]    Script Date: 01/09/2008 17:03:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelStreams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_DelStreams]    Script Date: 11/20/2006 18:18:55 ******/
CREATE PROCEDURE [dbo].[tf_DelStreams]
(
	@Original_DevID bigint,
	@Original_StrID int
)
AS
	SET NOCOUNT ON;

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaStrID] = @Original_StrID) 
			AND ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (SELECT reference.ReferenceID FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
							WHERE ([StrID] = @Original_StrID) 
							  AND ([DevID] = @Original_DevID)
	)
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([StrID] = @Original_StrID) 
	  AND ([DevID] = @Original_DevID)

	DELETE FROM [stream_fields] WHERE (([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID))
	DELETE FROM [streams] WHERE (([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelDevices]    Script Date: 01/09/2008 17:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelDevices]
(
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	UPDATE [devices] WITH (ROWLOCK, XLOCK) SET Removed = 1 WHERE [DevID] = @Original_DevID

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (	SELECT reference.ReferenceID FROM [reference] 
								INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE ([DevID] = @Original_DevID) )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] = @Original_DevID)
	
	DELETE FROM [stream_fields] WHERE [DevID] = @Original_DevID
	DELETE FROM [streams]		WHERE [DevID] = @Original_DevID
	DELETE FROM [device_status] WHERE [DevID] = @Original_DevID
	DELETE FROM [devices]		WHERE [DevID] = @Original_DevID
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelBuilding]    Script Date: 01/09/2008 17:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelBuilding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelBuilding]
(
	@Original_BuildingID uniqueidentifier
)
AS
	SET NOCOUNT ON;

	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE [RackID] IN (SELECT RackID FROM [rack] WHERE ([BuildingID] = @Original_BuildingID))
	DELETE FROM [rack] WHERE (([BuildingID] = @Original_BuildingID))
	DELETE FROM [building] WHERE (([BuildingID] = @Original_BuildingID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelStation]    Script Date: 01/09/2008 17:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelStation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelStation]
(
	@Original_StationID uniqueidentifier
)
AS
	SET NOCOUNT ON; -- se off e non c''è la riga ADO.NET da errore

	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE [RackID] IN (SELECT RackID FROM [rack] INNER JOIN [building] ON [rack].[BuildingID] = [building].[BuildingID]
					   WHERE ([StationID] = @Original_StationID))
	DELETE [rack] 
		FROM [rack] INNER JOIN [building] ON [rack].[BuildingID] = [building].[BuildingID] 
		WHERE ([StationID] = @Original_StationID)
	DELETE FROM [building] WHERE ([StationID] = @Original_StationID)
	DELETE FROM [station] WHERE ([StationID] = @Original_StationID)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetBuilding]    Script Date: 01/09/2008 17:03:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetBuilding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetBuilding]
AS
	SET NOCOUNT ON;
	SELECT BuildingID, BuildingXMLID, StationID, BuildingName, BuildingDescription, Removed
	FROM building
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdBuilding]    Script Date: 01/09/2008 17:03:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdBuilding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdBuilding]
(
	@BuildingID uniqueidentifier,
	@BuildingXMLID int,
	@StationID uniqueidentifier,
	@BuildingName varchar(64),
	@BuildingDescription varchar(256),
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT BuildingID FROM [building] WHERE ([BuildingID] = @BuildingID))
		UPDATE [building] SET [BuildingXMLID] = @BuildingXMLID, [StationID] = @StationID, [BuildingName] = @BuildingName, [BuildingDescription] = @BuildingDescription, Removed = @Removed WHERE [BuildingID] = @BuildingID;
	ELSE
		INSERT INTO [building] ([BuildingID], [BuildingXMLID], [StationID], [BuildingName], [BuildingDescription], [Removed]) VALUES (@BuildingID, @BuildingXMLID, @StationID, @BuildingName, @BuildingDescription, @Removed);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdReference]    Script Date: 01/09/2008 17:03:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdReference]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdReference]
(
	@ReferenceID uniqueidentifier,
	@Value varchar(256),
	@DateTime datetime,
	@Visible tinyint,
	@DeltaDevID bigint,
	@DeltaStrID int,
	@DeltaFieldID int,
	@DeltaArrayID int
)
AS
	SET NOCOUNT OFF;
	IF EXISTS (SELECT ReferenceID FROM reference WHERE (ReferenceID = @ReferenceID))
		UPDATE [reference] SET [Value] = @Value, [DateTime] = @DateTime, [Visible] = @Visible, [DeltaDevID] = @DeltaDevID, [DeltaStrID] = @DeltaStrID, [DeltaFieldID] = @DeltaFieldID, [DeltaArrayID] = @DeltaArrayID WHERE (([ReferenceID] = @ReferenceID));
	ELSE
		INSERT INTO [reference] ([ReferenceID], [Value], [DateTime], [Visible], [DeltaDevID], [DeltaStrID], [DeltaFieldID], [DeltaArrayID]) VALUES (@ReferenceID, @Value, @DateTime, @Visible, @DeltaDevID, @DeltaStrID, @DeltaFieldID, @DeltaArrayID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetDeviceStatus]    Script Date: 01/09/2008 17:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDeviceStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetDeviceStatus]    Script Date: 11/20/2006 18:18:59 ******/
CREATE PROCEDURE [dbo].[tf_GetDeviceStatus]
AS
	SET NOCOUNT ON;
	SELECT DevID, SevLevel, Description, Offline, 0 as IsDeleted
	FROM device_status
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetOffDevList]    Script Date: 01/09/2008 17:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetOffDevList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetOffDevList] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT     devices.DevID, devices.Name, device_status.Description, ''Offline'' AS [Status], device_status.SevLevel
	FROM         devices INNER JOIN
						  device_status ON devices.DevID = device_status.DevID
	WHERE     (devices.Removed = 0) AND (device_status.Offline = 1)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdPort]    Script Date: 01/09/2008 17:03:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdPort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdPort]
(
	@PortID uniqueidentifier,
	@PortXMLID int,
	@PortName varchar(64),
	@PortType varchar(64),
	@Parameters varchar(64),
	@Status varchar(64),
	@Removed tinyint,
	@SrvId int
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS (SELECT PortID FROM port WHERE (PortID = @PortID))
		UPDATE [port] SET [PortXMLID] = @PortXMLID, [PortName] = @PortName, [PortType] = @PortType, [Parameters] = @Parameters, [Status] = @Status, [Removed] = @Removed, [SrvId] = @SrvId WHERE ([PortID] = @PortID);
	ELSE
		INSERT INTO [port] ([PortID],[PortXMLID], [PortName], [PortType], [Parameters], [Status], [Removed], [SrvId]) VALUES (@PortID, @PortXMLID, @PortName, @PortType, @Parameters, @Status, @Removed, @SrvId);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetPort]    Script Date: 01/09/2008 17:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetPort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetPort]
AS
	SET NOCOUNT ON;
	SELECT PortID, PortXMLID, PortName, PortType, Parameters, Status, Removed, SrvID
	FROM port
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelPort]    Script Date: 01/09/2008 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelPort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelPort]
(
	@Original_PortID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	
	UPDATE Devices SET PortID = Null WHERE (([PortID] = @Original_PortID))
	DELETE FROM [port] WHERE (([PortID] = @Original_PortID))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetStreamsbyDevID]    Script Date: 01/09/2008 17:03:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetStreamsbyDevID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetStreamsbyDevID] @DevID bigint
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [Name],
		convert(char,[DateTime],105) As Data,
		convert(char,[DateTime],108) As Ora,
		SevLevel,
		DevID,
		StrID 
	FROM	streams
	WHERE	((Visible = 1) AND
		(DevID = @DevID))
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetStreams]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStreams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetStreams]    Script Date: 11/20/2006 18:19:05 ******/
CREATE PROCEDURE [dbo].[tf_GetStreams]
AS
	SET NOCOUNT ON;
	SELECT DevID, StrID, Name, Data, DateTime, SevLevel, Description, Visible, 0 as IsDeleted	
	FROM streams
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetStation]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetStation]
AS
	SET NOCOUNT ON;
	SELECT StationID, StationXMLID, StationName, Removed
	FROM station
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdStation]    Script Date: 01/09/2008 17:03:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdStation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdStation]
(
	@StationID uniqueidentifier,
	@StationXMLID int,
	@StationName varchar(64), 
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT StationID FROM station WHERE (StationID = @StationID))
		UPDATE [station] SET [StationID] = @StationID, [StationXMLID] = @StationXMLID, [StationName] = @StationName, [Removed] = @Removed WHERE (([StationID] = @StationID));
	ELSE
		INSERT INTO [station] ([StationID], [StationXMLID], [StationName], [Removed]) VALUES (@StationID, @StationXMLID, @StationName, @Removed);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdServers]    Script Date: 01/09/2008 17:03:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdServers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdServers]
(
	@SrvID int,
	@Name varchar(64),
	@Host varchar(64),
	@FullHostName varchar(256),
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64),
	@SupervisorSystemXML xml,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ClientDateValidationRequested datetime,
	@ClientDateValidationObtained datetime,
	@ClientKey varbinary(148)
)
AS
	SET NOCOUNT OFF;

	IF EXISTS (SELECT SrvID FROM servers WHERE (SrvID = @SrvID))
		UPDATE [servers] 
			SET [SrvID] = @SrvID, [Name] = @Name, [Host] = @Host, [FullHostName] = @FullHostName, 
			[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
			[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
			[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
			[ClientKey] = @ClientKey
		WHERE (([SrvID] = @SrvID));
	ELSE
		INSERT INTO [servers] ([SrvID], [Name], [Host], [FullHostName], [IP], [LastUpdate], [LastMessageType], [SupervisorSystemXML], [ClientSupervisorSystemXMLValidated], [ClientValidationSign], [ClientDateValidationRequested], [ClientDateValidationObtained], [ClientKey]) 
		VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetServerByIP]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServerByIP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServerByIP]
( @IP varchar(16))
AS
	SET NOCOUNT ON;
	SELECT 
		SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML, 
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey
	FROM servers
	WHERE IP = @IP
		AND (SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0))
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetServers]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServers]
AS
	SET NOCOUNT ON;
	SELECT 
		SrvID, [Name], Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML, 
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey
	FROM servers 
	WHERE SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetServerById]    Script Date: 01/09/2008 17:03:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServerById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServerById]
( @SrvID int)
AS
	SET NOCOUNT ON;
	SELECT SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML, 
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey
	FROM servers
	WHERE SrvID = @SrvID
		AND SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetServer]    Script Date: 01/09/2008 17:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetServer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetServer]
AS
	SELECT 
		OutServers.SrvID, OutServers.Name, OutServers.Host, OutServers.FullHostName, OutServers.IP, OutServers.LastUpdate, 
		OutServers.LastMessageType, OutServers.SupervisorSystemXML, OutServers.ClientSupervisorSystemXMLValidated, 
		OutServers.ClientValidationSign, OutServers.ClientDateValidationRequested, OutServers.ClientDateValidationObtained, OutServers.ClientKey
	FROM servers OutServers
	WHERE EXISTS
	(
		SELECT *
		FROM servers InnServers
		INNER JOIN devices ON InnServers.SrvID = devices.SrvID
		WHERE InnServers.SrvID = OutServers.SrvID
	);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_Report_GetConfigurationInUse]    Script Date: 01/09/2008 17:03:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_Report_GetConfigurationInUse]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_Report_GetConfigurationInUse]
	@SrvID int
AS
	SELECT 
		ResultExt.SupervisorSystemXML.query(''.'') AS DeviceNode, 
		ResultExt.SupervisorSystemXML.value(''../../../../../@code[1]'', ''varchar(64)'') AS SystemCode,
		ResultExt.SupervisorSystemXML.value(''../../../../../@name[1]'', ''varchar(64)'') AS SystemName,
		ResultExt.SupervisorSystemXML.value(''../../../../@SrvID[1]'', ''int'') AS ServerID,
		ResultExt.SupervisorSystemXML.value(''../../../../@host[1]'', ''varchar(64)'') AS ServerHost,
		ResultExt.SupervisorSystemXML.value(''../../../../@name[1]'', ''varchar(64)'') AS ServerName,
		ResultExt.SupervisorSystemXML.value(''../../../@RegID[1]'', ''bigint'') AS RegionID,
		ResultExt.SupervisorSystemXML.value(''../../../@name[1]'', ''varchar(64)'') AS RegionName,
		ResultExt.SupervisorSystemXML.value(''../../@ZonID[1]'', ''bigint'') AS ZoneID,
		ResultExt.SupervisorSystemXML.value(''../../@name[1]'', ''varchar(64)'') AS ZoneName,
		ResultExt.SupervisorSystemXML.value(''../@NodID[1]'', ''bigint'') AS NodeID,
		ResultExt.SupervisorSystemXML.value(''../@name[1]'', ''varchar(64)'') AS NodeName,
		ResultExt.SupervisorSystemXML.value(''../@modem_size[1]'', ''varchar(64)'') AS NodeModemSize,
		ResultExt.SupervisorSystemXML.value(''../@modem_addr[1]'', ''varchar(64)'') AS NodeModemAddr,
		ResultExt.SupervisorSystemXML.value(''@name[1]'', ''varchar(64)'') AS [Name],
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(1000)'')
			FROM servers CROSS APPLY SupervisorSystemXML.nodes(''//station'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@station[1]'', ''int''))
		) AS Station,
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(1000)'')
			FROM servers CROSS APPLY SupervisorSystemXML.nodes(''//building'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@building[1]'', ''int''))
		) AS Building,
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(1000)'')
			FROM servers CROSS APPLY SupervisorSystemXML.nodes(''//location'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@location[1]'', ''int''))
		) AS Location, 
		ResultExt.SupervisorSystemXML.value(''@position[1]'', ''varchar(64)'') AS [Position], 
		ResultExt.SupervisorSystemXML.value(''@addr[1]'', ''varchar(32)'') AS [Address], 
		ResultExt.SupervisorSystemXML.value(''@SN[1]'', ''varchar(16)'') AS SN, 
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(64)'')
			FROM servers CROSS APPLY SupervisorSystemXML.nodes(''//port/item'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@port[1]'', ''int''))
		) AS Port,
		ResultExt.SupervisorSystemXML.value(''@type[1]'', ''varchar(16)'') AS [Type]
	FROM servers EXT CROSS APPLY SupervisorSystemXML.nodes(''//device'') AS ResultExt(SupervisorSystemXML)
	WHERE (SrvID = @SrvID)
	AND (LOWER(ResultExt.SupervisorSystemXML.value(''@active[1]'', ''varchar(5)'')) = ''true'')
	AND (LOWER(ResultExt.SupervisorSystemXML.value(''@scheduled[1]'', ''varchar(5)'')) = ''true'')
	;
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetServerBySrvID]    Script Date: 01/09/2008 17:03:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetServerBySrvID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetServerBySrvID]
	@SrvID int = 0
AS
	SELECT SrvID, Name, Host, FullHostName, IP, LastUpdate, LastMessageType, SupervisorSystemXML, ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey
	FROM servers
	WHERE (SrvID = @SrvID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_UpdValidateServer]    Script Date: 01/09/2008 17:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_UpdValidateServer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_UpdValidateServer]
	@SrvID int,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign int,
	@ClientDateValidationObtained int,
	@ClientKey int
AS
	UPDATE servers
	SET ClientSupervisorSystemXMLValidated = @ClientSupervisorSystemXMLValidated, ClientValidationSign = @ClientValidationSign, ClientDateValidationObtained = @ClientDateValidationObtained, ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_GetSystemXMLInUseChanged]    Script Date: 01/09/2008 17:03:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetSystemXMLInUseChanged]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetSystemXMLInUseChanged]
	@SrvID INT
AS
	SELECT 
		(
			CASE WHEN (
						SupervisorSystemXML IS NOT NULL 
						AND 
						ClientSupervisorSystemXMLValidated IS NOT NULL 
						AND 
						(CAST(SupervisorSystemXML AS NVARCHAR(MAX)) = CAST(ClientSupervisorSystemXMLValidated AS NVARCHAR(MAX)))
						) 
			THEN 1 
			ELSE 0 
			END
		) as SystemXMLInUseChanged
	FROM servers
	WHERE (SrvID = @SrvID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_UpdServerAsValidated]    Script Date: 01/09/2008 17:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_UpdServerAsValidated]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_UpdServerAsValidated]
(
	@SrvID int,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ClientDateValidationObtained datetime,
	@ClientKey varbinary(148)
	)
AS
	UPDATE servers
	SET ClientSupervisorSystemXMLValidated = @ClientSupervisorSystemXMLValidated, ClientValidationSign = @ClientValidationSign, ClientDateValidationObtained = @ClientDateValidationObtained, ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_Report_GetConfigurationValidated]    Script Date: 01/09/2008 17:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_Report_GetConfigurationValidated]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_Report_GetConfigurationValidated]
	@SrvID int
AS
	SELECT 
		ResultExt.SupervisorSystemXML.query(''.'') AS DeviceNode, 
		ResultExt.SupervisorSystemXML.value(''../../../../../@code[1]'', ''varchar(64)'') AS SystemCode,
		ResultExt.SupervisorSystemXML.value(''../../../../../@name[1]'', ''varchar(64)'') AS SystemName,
		ResultExt.SupervisorSystemXML.value(''../../../../@SrvID[1]'', ''int'') AS ServerID,
		ResultExt.SupervisorSystemXML.value(''../../../../@host[1]'', ''varchar(64)'') AS ServerHost,
		ResultExt.SupervisorSystemXML.value(''../../../../@name[1]'', ''varchar(64)'') AS ServerName,
		ResultExt.SupervisorSystemXML.value(''../../../@RegID[1]'', ''bigint'') AS RegionID,
		ResultExt.SupervisorSystemXML.value(''../../../@name[1]'', ''varchar(64)'') AS RegionName,
		ResultExt.SupervisorSystemXML.value(''../../@ZonID[1]'', ''bigint'') AS ZoneID,
		ResultExt.SupervisorSystemXML.value(''../../@name[1]'', ''varchar(64)'') AS ZoneName,
		ResultExt.SupervisorSystemXML.value(''../@NodID[1]'', ''bigint'') AS NodeID,
		ResultExt.SupervisorSystemXML.value(''../@name[1]'', ''varchar(64)'') AS NodeName,
		ResultExt.SupervisorSystemXML.value(''../@modem_size[1]'', ''varchar(64)'') AS NodeModemSize,
		ResultExt.SupervisorSystemXML.value(''../@modem_addr[1]'', ''varchar(64)'') AS NodeModemAddr,
		ResultExt.SupervisorSystemXML.value(''@name[1]'', ''varchar(64)'') AS [Name],
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(1000)'')
			FROM servers CROSS APPLY ClientSupervisorSystemXMLValidated.nodes(''//station'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@station[1]'', ''int''))
		) AS Station,
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(1000)'')
			FROM servers CROSS APPLY ClientSupervisorSystemXMLValidated.nodes(''//building'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@building[1]'', ''int''))
		) AS Building,
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(1000)'')
			FROM servers CROSS APPLY ClientSupervisorSystemXMLValidated.nodes(''//location'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@location[1]'', ''int''))
		) AS Location, 
		ResultExt.SupervisorSystemXML.value(''@position[1]'', ''varchar(64)'') AS [Position], 
		ResultExt.SupervisorSystemXML.value(''@addr[1]'', ''varchar(32)'') AS [Address], 
		ResultExt.SupervisorSystemXML.value(''@SN[1]'', ''varchar(16)'') AS SN, 
		(
			SELECT 
				Result.SupervisorSystemXML.value(''@name[1]'', ''varchar(64)'')
			FROM servers CROSS APPLY ClientSupervisorSystemXMLValidated.nodes(''//port/item'') AS Result(SupervisorSystemXML)
			WHERE (SrvID = @SrvID) AND (Result.SupervisorSystemXML.value(''@id[1]'', ''int'') = ResultExt.SupervisorSystemXML.value(''@port[1]'', ''int''))
		) AS Port,
		ResultExt.SupervisorSystemXML.value(''@type[1]'', ''varchar(16)'') AS [Type]
	FROM servers EXT CROSS APPLY ClientSupervisorSystemXMLValidated.nodes(''//device'') AS ResultExt(SupervisorSystemXML)
	WHERE (SrvID = @SrvID)
	AND (LOWER(ResultExt.SupervisorSystemXML.value(''@active[1]'', ''varchar(5)'')) = ''true'')
	AND (LOWER(ResultExt.SupervisorSystemXML.value(''@scheduled[1]'', ''varchar(5)'')) = ''true'');
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sWEB_Report_GetConfigurationValidatedPorts]    Script Date: 01/09/2008 17:03:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_Report_GetConfigurationValidatedPorts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_Report_GetConfigurationValidatedPorts]
	@SrvID int
AS
	SELECT 
		ResultExt.SupervisorSystemXML.query(''.'') AS PortNode,
		ResultExt.SupervisorSystemXML.value(''@name[1]'', ''varchar(64)'') AS [Name],
		ResultExt.SupervisorSystemXML.value(''@type[1]'', ''varchar(64)'') AS [Type],
		ResultExt.SupervisorSystemXML.value(''@timeout[1]'', ''varchar(64)'') AS [Timeout]
	FROM servers EXT CROSS APPLY ClientSupervisorSystemXMLValidated.nodes(''//port/item'') AS ResultExt(SupervisorSystemXML)
	WHERE (SrvID = @SrvID);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdDevices]    Script Date: 01/09/2008 17:03:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdDevices]
(
	@DevID bigint,
	@NodID bigint,
	@SrvID int,
	@Name varchar(64),
	@Type varchar(16),
	@SN varchar(16),
	@PortID uniqueidentifier,
	@Addr varchar(32),
	@ProfileID int,
	@Active tinyint,
	@Scheduled tinyint,
	@Removed bit,
	@RackID uniqueidentifier,
	@RackPositionRow int,
	@RackPositionCol int
	)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT DevID FROM devices WITH(UPDLOCK) WHERE (DevID = @DevID))
		UPDATE [devices] 
		SET [DevID] = @DevID, 
			[NodID] = @NodID, 
			[SrvID] = @SrvID, 
			[Name] = @Name, 
			[Type] = @Type, 
			[SN] = @SN, 
			[PortID] = @PortID, 
			[Addr] = @Addr, 
			[ProfileID] = @ProfileID, 
			[Active] = @Active, 
			[Scheduled] = @Scheduled, 
			Removed = @Removed,
			RackID= @RackID,
			RackPositionRow= @RackPositionRow,
			RackPositionCol= @RackPositionCol
		WHERE (([DevID] = @DevID));
	ELSE
		INSERT INTO [devices] ([DevID], 
								[NodID], 
								[SrvID], 
								[Name], 
								[Type], 
								[SN], 
								[PortID], 
								[Addr], 
								[ProfileID], 
								[Active], 
								[Scheduled], 
								Removed,
								RackID,
								RackPositionRow,
								RackPositionCol) 
		VALUES (@DevID, 
				@NodID, 
				@SrvID, 
				@Name, 
				@Type, 
				@SN, 
				@PortID, 
				@Addr, 
				@ProfileID, 
				@Active, 
				@Scheduled, 
				@Removed,
				@RackID,
				@RackPositionRow,
				@RackPositionCol);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_DelRack]    Script Date: 01/09/2008 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelRack]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelRack]
(
	@Original_RackID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE ([RackID] = @Original_RackID)
	DELETE FROM [rack] WHERE ([RackID] = @Original_RackID)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[util_ChangeTestNodiD]    Script Date: 01/09/2008 17:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[util_ChangeTestNodiD]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[util_ChangeTestNodiD]
AS
	-- San Bonifacio 10625834352643
	UPDATE devices
	SET NodID = 10625834352643
	WHERE (NodID = 99999999)

	UPDATE devices
	SET Addr = ''123456789''
	WHERE Addr = ''address''
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetDevices]    Script Date: 01/09/2008 17:03:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDevices]
AS
	SET NOCOUNT ON;
	SELECT DevID, NodID, SrvID, Name, Type, SN, PortID, Addr, ProfileID, Active, Scheduled, Removed, RackID, RackPositionRow, RackPositionCol
	FROM devices
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_GetRack]    Script Date: 01/09/2008 17:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetRack]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetRack]
AS
	SET NOCOUNT ON;
	SELECT RackID, RackXMLID, BuildingID, RackName, RackType, RackDescription, Removed
	FROM rack
	WHERE ISNULL(Removed,0) = 0
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdRack]    Script Date: 01/09/2008 17:03:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdRack]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdRack]
(
	@RackID uniqueidentifier,
	@RackXMLID int,
	@BuildingID uniqueidentifier,
	@RackName varchar(64),
	@RackType varchar(64),
	@RackDescription varchar(256),
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT RackID FROM rack WHERE (RackID = @RackID))
		UPDATE [rack] SET [RackXMLID] = @RackXMLID, [BuildingID] = @BuildingID, [RackName] = @RackName, [RackType] = @RackType, [RackDescription] = @RackDescription, [Removed] = @Removed WHERE (([RackID] = @RackID));
	ELSE
		INSERT INTO [rack] ([RackID], [RackXMLID], [BuildingID], [RackName], [RackType], [RackDescription], [Removed]) VALUES (@RackID, @RackXMLID, @BuildingID, @RackName, @RackType, @RackDescription, @Removed);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdDeviceStatus]    Script Date: 01/09/2008 17:03:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdDeviceStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdDeviceStatus]    Script Date: 11/20/2006 18:19:08 ******/
CREATE PROCEDURE [dbo].[tf_UpdDeviceStatus]
(
	@DevID bigint,
	@SevLevel int,
	@Description varchar(256),
	@Offline tinyint,
	@IsDeleted bit
)
AS
	SET NOCOUNT OFF;

	IF EXISTS (SELECT [DevID] FROM [device_status] WITH(UPDLOCK) WHERE [DevID] = @DevID)
		IF @IsDeleted = 0
			UPDATE [device_status] 
			SET [DevID] = @DevID, [SevLevel] = @SevLevel, [Description] = @Description, [Offline] = @Offline 
			WHERE [DevID] = @DevID;
		ELSE
			EXEC [tf_DelDeviceStatus] @DevID;
	ELSE
		INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline]) 
		VALUES (@DevID, @SevLevel, @Description, @Offline);
' 
END
GO
/****** Object:  StoredProcedure [dbo].[tf_UpdStreams]    Script Date: 01/09/2008 17:03:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdStreams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdStreams]    Script Date: 11/20/2006 18:19:10 ******/
CREATE PROCEDURE [dbo].[tf_UpdStreams]
(
	@DevID bigint,
	@StrID int,
	@Name varchar(64),
	@Data image,
	@DateTime datetime,
	@SevLevel int,
	@Description text,
	@Visible tinyint,
	@IsDeleted bit
)
AS
	SET NOCOUNT OFF;
	
IF EXISTS( SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID))
	IF @IsDeleted = 0
		UPDATE [streams] SET [DevID] = @DevID, [StrID] = @StrID, [Name] = @Name, [Data] = @Data, [DateTime] = @DateTime, [SevLevel] = @SevLevel, [Description] = @Description, [Visible] = @Visible WHERE (([DevID] = @DevID) AND ([StrID] = @StrID));
	ELSE
		EXEC [tf_DelStreams] @DevID, @StrID 
ELSE
	INSERT INTO [streams] ([DevID], [StrID], [Name], [Data], [DateTime], [SevLevel], [Description], [Visible]) VALUES (@DevID, @StrID, @Name, @Data, @DateTime, @SevLevel, @Description, @Visible)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[util_EraseTestServer]    Script Date: 01/09/2008 17:03:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[util_EraseTestServer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[util_EraseTestServer]
AS	
-- Erase stress test Servers
	DECLARE @SrvID int
	DECLARE servers_cur CURSOR FOR 
		SELECT SrvId
		FROM servers
		where [Name] Like ''STRESSTEST:%''
	
	OPEN servers_cur
	
	FETCH NEXT FROM servers_cur INTO @SrvID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [dbo].[tf_DelServers] @SrvID 
	   -- Get the next server
	   FETCH NEXT FROM servers_cur INTO @SrvID
	END
	
	CLOSE servers_cur
	DEALLOCATE servers_cur


-- Erase stress test Stations
	DECLARE @StationID uniqueidentifier
	DECLARE stations_cur CURSOR FOR 
		SELECT StationId
		FROM station
		where StationName Like ''STRESSTEST:%''
	
	OPEN stations_cur
	
	FETCH NEXT FROM stations_cur INTO @StationID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [dbo].[tf_DelStation] @StationID 
	   -- Get the next station
	   FETCH NEXT FROM stations_cur INTO @StationID
	END
	
	CLOSE stations_cur
	DEALLOCATE stations_cur
' 
END
GO
/****** Object:  ForeignKey [FK_building_station]    Script Date: 01/09/2008 17:03:31 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_building_station]') AND parent_object_id = OBJECT_ID(N'[dbo].[building]'))
ALTER TABLE [dbo].[building]  WITH CHECK ADD  CONSTRAINT [FK_building_station] FOREIGN KEY([StationID])
REFERENCES [dbo].[station] ([StationID])
GO
ALTER TABLE [dbo].[building] CHECK CONSTRAINT [FK_building_station]
GO
/****** Object:  ForeignKey [FK_device_status_devices]    Script Date: 01/09/2008 17:03:33 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_status_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
ALTER TABLE [dbo].[device_status]  WITH NOCHECK ADD  CONSTRAINT [FK_device_status_devices] FOREIGN KEY([DevID])
REFERENCES [dbo].[devices] ([DevID])
GO
ALTER TABLE [dbo].[device_status] CHECK CONSTRAINT [FK_device_status_devices]
GO
/****** Object:  ForeignKey [FK_device_status_severity]    Script Date: 01/09/2008 17:03:34 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_status_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
ALTER TABLE [dbo].[device_status]  WITH NOCHECK ADD  CONSTRAINT [FK_device_status_severity] FOREIGN KEY([SevLevel])
REFERENCES [dbo].[severity] ([SevLevel])
GO
ALTER TABLE [dbo].[device_status] CHECK CONSTRAINT [FK_device_status_severity]
GO
/****** Object:  ForeignKey [FK_device_type_systems]    Script Date: 01/09/2008 17:03:35 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_type_systems]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_type]'))
ALTER TABLE [dbo].[device_type]  WITH CHECK ADD  CONSTRAINT [FK_device_type_systems] FOREIGN KEY([SystemID])
REFERENCES [dbo].[systems] ([SystemID])
GO
ALTER TABLE [dbo].[device_type] CHECK CONSTRAINT [FK_device_type_systems]
GO
/****** Object:  ForeignKey [FK_device_type_vendors]    Script Date: 01/09/2008 17:03:36 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_type_vendors]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_type]'))
ALTER TABLE [dbo].[device_type]  WITH CHECK ADD  CONSTRAINT [FK_device_type_vendors] FOREIGN KEY([VendorID])
REFERENCES [dbo].[vendors] ([VendorID])
GO
ALTER TABLE [dbo].[device_type] CHECK CONSTRAINT [FK_device_type_vendors]
GO
/****** Object:  ForeignKey [FK_devices_nodes]    Script Date: 01/09/2008 17:03:40 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_nodes]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_nodes] FOREIGN KEY([NodID])
REFERENCES [dbo].[nodes] ([NodID])
GO
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_nodes]
GO
/****** Object:  ForeignKey [FK_devices_port]    Script Date: 01/09/2008 17:03:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_port]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_port] FOREIGN KEY([PortId])
REFERENCES [dbo].[port] ([PortID])
GO
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_port]
GO
/****** Object:  ForeignKey [FK_devices_rack]    Script Date: 01/09/2008 17:03:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_rack]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_rack] FOREIGN KEY([RackID])
REFERENCES [dbo].[rack] ([RackID])
GO
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_rack]
GO
/****** Object:  ForeignKey [FK_devices_servers]    Script Date: 01/09/2008 17:03:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_servers]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_servers] FOREIGN KEY([SrvID])
REFERENCES [dbo].[servers] ([SrvID])
GO
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_servers]
GO
/****** Object:  ForeignKey [FK_nodes_zones]    Script Date: 01/09/2008 17:03:43 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_nodes_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[nodes]'))
ALTER TABLE [dbo].[nodes]  WITH CHECK ADD  CONSTRAINT [FK_nodes_zones] FOREIGN KEY([ZonID])
REFERENCES [dbo].[zones] ([ZonID])
GO
ALTER TABLE [dbo].[nodes] CHECK CONSTRAINT [FK_nodes_zones]
GO
/****** Object:  ForeignKey [FK_port_servers]    Script Date: 01/09/2008 17:03:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_port_servers]') AND parent_object_id = OBJECT_ID(N'[dbo].[port]'))
ALTER TABLE [dbo].[port]  WITH CHECK ADD  CONSTRAINT [FK_port_servers] FOREIGN KEY([SrvID])
REFERENCES [dbo].[servers] ([SrvID])
GO
ALTER TABLE [dbo].[port] CHECK CONSTRAINT [FK_port_servers]
GO
/****** Object:  ForeignKey [FK_procedures_devices]    Script Date: 01/09/2008 17:03:48 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_procedures_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[procedures]'))
ALTER TABLE [dbo].[procedures]  WITH CHECK ADD  CONSTRAINT [FK_procedures_devices] FOREIGN KEY([DevID])
REFERENCES [dbo].[devices] ([DevID])
GO
ALTER TABLE [dbo].[procedures] CHECK CONSTRAINT [FK_procedures_devices]
GO
/****** Object:  ForeignKey [FK_rack_building]    Script Date: 01/09/2008 17:03:51 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_rack_building]') AND parent_object_id = OBJECT_ID(N'[dbo].[rack]'))
ALTER TABLE [dbo].[rack]  WITH CHECK ADD  CONSTRAINT [FK_rack_building] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[building] ([BuildingID])
GO
ALTER TABLE [dbo].[rack] CHECK CONSTRAINT [FK_rack_building]
GO
/****** Object:  ForeignKey [FK_stream_fields_reference]    Script Date: 01/09/2008 17:04:04 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_reference]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields]  WITH CHECK ADD  CONSTRAINT [FK_stream_fields_reference] FOREIGN KEY([ReferenceID])
REFERENCES [dbo].[reference] ([ReferenceID])
GO
ALTER TABLE [dbo].[stream_fields] CHECK CONSTRAINT [FK_stream_fields_reference]
GO
/****** Object:  ForeignKey [FK_stream_fields_severity]    Script Date: 01/09/2008 17:04:04 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields]  WITH NOCHECK ADD  CONSTRAINT [FK_stream_fields_severity] FOREIGN KEY([SevLevel])
REFERENCES [dbo].[severity] ([SevLevel])
GO
ALTER TABLE [dbo].[stream_fields] CHECK CONSTRAINT [FK_stream_fields_severity]
GO
/****** Object:  ForeignKey [FK_stream_fields_streams]    Script Date: 01/09/2008 17:04:05 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_streams]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields]  WITH CHECK ADD  CONSTRAINT [FK_stream_fields_streams] FOREIGN KEY([DevID], [StrID])
REFERENCES [dbo].[streams] ([DevID], [StrID])
GO
ALTER TABLE [dbo].[stream_fields] CHECK CONSTRAINT [FK_stream_fields_streams]
GO
/****** Object:  ForeignKey [FK_streams_device_status]    Script Date: 01/09/2008 17:04:08 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_streams_device_status]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
ALTER TABLE [dbo].[streams]  WITH CHECK ADD  CONSTRAINT [FK_streams_device_status] FOREIGN KEY([DevID])
REFERENCES [dbo].[device_status] ([DevID])
GO
ALTER TABLE [dbo].[streams] CHECK CONSTRAINT [FK_streams_device_status]
GO
/****** Object:  ForeignKey [FK_streams_severity]    Script Date: 01/09/2008 17:04:08 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_streams_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
ALTER TABLE [dbo].[streams]  WITH NOCHECK ADD  CONSTRAINT [FK_streams_severity] FOREIGN KEY([SevLevel])
REFERENCES [dbo].[severity] ([SevLevel])
GO
ALTER TABLE [dbo].[streams] CHECK CONSTRAINT [FK_streams_severity]
GO
/****** Object:  ForeignKey [FK_zones_regions]    Script Date: 01/09/2008 17:04:11 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_zones_regions]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones]  WITH CHECK ADD  CONSTRAINT [FK_zones_regions] FOREIGN KEY([RegID])
REFERENCES [dbo].[regions] ([RegID])
GO
ALTER TABLE [dbo].[zones] CHECK CONSTRAINT [FK_zones_regions]
GO

INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription]) VALUES ('DBVersion', '1.2.1.0', 'La versione del DataBase')
PRINT 'Database created'
GO
﻿
EXEC sp_addrolemember N'db_datareader', N'SWMAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'SWMUsers'
GO
EXEC sp_addrolemember N'db_datareader', N'STLC1000AppServices'
GO
EXEC sp_addrolemember N'db_datawriter', N'SWMAdmins'
GO
EXEC sp_addrolemember N'db_datawriter', N'STLC1000AppServices'

GO

﻿-- =============================================
-- Static Data
-- =============================================

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[stlc_parameters] WHERE [ParameterName] = 'DBVersion')
     INSERT INTO [dbo].[stlc_parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('DBVersion', '1.4.0.0', 'DataBase version')
ELSE
     UPDATE [dbo].[stlc_parameters] SET [ParameterValue] = '1.4.0.0', [ParameterDescription] = 'DataBase version' WHERE [ParameterName] = 'DBVersion'

IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 0)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (0, 'GLOBALDB_Severity0')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'GLOBALDB_Severity0' WHERE [SevLevel] = 0


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 1)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (1, 'GLOBALDB_Severity1')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'GLOBALDB_Severity1' WHERE [SevLevel] = 1


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 2)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (2, 'GLOBALDB_Severity2')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'GLOBALDB_Severity2' WHERE [SevLevel] = 2


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 9)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (9, 'GLOBALDB_Severity9')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'GLOBALDB_Severity9' WHERE [SevLevel] = 9


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 255)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (255, 'GLOBALDB_Severity255')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'GLOBALDB_Severity255' WHERE [SevLevel] = 255

IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 1)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (1, 'Default vendor', NULL)
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Default vendor', [VendorDescription] = NULL WHERE [VendorID] = 1

IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 1)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (1, 'Default system')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Default system' WHERE [SystemID] = 1


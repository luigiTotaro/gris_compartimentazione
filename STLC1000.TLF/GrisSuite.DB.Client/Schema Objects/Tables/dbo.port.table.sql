﻿CREATE TABLE [dbo].[port]
(
[PortID] [uniqueidentifier] NOT NULL,
[PortXMLID] [int] NULL,
[PortName] [varchar] (64) NULL,
[PortType] [varchar] (64) NULL,
[Parameters] [varchar] (64) NULL,
[Status] [varchar] (64) NULL,
[Removed] [tinyint] NULL,
[SrvID] [int] NOT NULL
) ON [PRIMARY]



﻿CREATE TABLE [dbo].[zones]
(
[ZonID] [bigint] NOT NULL,
[RegID] [bigint] NOT NULL,
[Name] [varchar] (64) NULL,
[Removed] [tinyint] NULL
) ON [PRIMARY]



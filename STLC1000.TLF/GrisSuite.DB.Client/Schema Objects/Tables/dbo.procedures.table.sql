﻿CREATE TABLE [dbo].[procedures]
(
[DevID] [bigint] NOT NULL,
[ProID] [int] NOT NULL,
[Name] [varchar] (64) NULL,
[ExeCount] [tinyint] NULL,
[LastExecution] [datetime] NULL,
[InProgress] [tinyint] NULL
) ON [PRIMARY]



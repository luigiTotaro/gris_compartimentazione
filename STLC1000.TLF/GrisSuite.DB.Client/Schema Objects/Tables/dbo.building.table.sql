﻿CREATE TABLE [dbo].[building]
(
[BuildingID] [uniqueidentifier] NOT NULL,
[BuildingXMLID] [int] NULL,
[StationID] [uniqueidentifier] NULL,
[BuildingName] [varchar] (64) NULL,
[BuildingDescription] [varchar] (256) NULL,
[Removed] [tinyint] NULL
) ON [PRIMARY]



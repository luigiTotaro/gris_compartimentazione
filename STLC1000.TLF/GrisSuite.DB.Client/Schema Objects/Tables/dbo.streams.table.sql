﻿CREATE TABLE [dbo].[streams]
(
[DevID] [bigint] NOT NULL,
[StrID] [int] NOT NULL,
[Name] [varchar] (64) NULL,
[Visible] [tinyint] NULL,
[Data] [image] NULL,
[DateTime] [datetime] NOT NULL,
[SevLevel] [int] NULL,
[Description] [text] NULL,
[Processed] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



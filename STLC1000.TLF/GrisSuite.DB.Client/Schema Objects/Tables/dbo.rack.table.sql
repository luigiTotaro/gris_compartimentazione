﻿CREATE TABLE [dbo].[rack]
(
[RackID] [uniqueidentifier] NOT NULL,
[RackXMLID] [int] NULL,
[BuildingID] [uniqueidentifier] NULL,
[RackName] [varchar] (64) NULL,
[RackType] [varchar] (64) NULL,
[RackDescription] [varchar] (256) NULL,
[Removed] [tinyint] NULL
) ON [PRIMARY]



﻿CREATE TABLE [dbo].[events]
(
[EventID] [uniqueidentifier] NOT NULL,
[DevID] [bigint] NOT NULL,
[EventData] [varbinary] (max) NOT NULL,
[Created] [datetime] NULL,
[Requested] [datetime] NOT NULL,
[ToBeDeleted] [bit] NOT NULL,
[EventCategory] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



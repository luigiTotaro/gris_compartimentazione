﻿CREATE TABLE [dbo].[stream_fields]
(
[DevID] [bigint] NOT NULL,
[StrID] [int] NOT NULL,
[FieldID] [int] NOT NULL,
[ArrayID] [int] NOT NULL,
[Name] [varchar] (64) NULL,
[SevLevel] [int] NULL,
[Value] [varchar] (1024) NULL,
[Description] [text] NULL,
[Visible] [tinyint] NULL,
[ReferenceID] [uniqueidentifier] NULL,
[AckFlag] [bit] NOT NULL,
[AckDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



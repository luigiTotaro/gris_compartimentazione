﻿CREATE TABLE [dbo].[devices]
(
[DevID] [bigint] NOT NULL,
[NodID] [bigint] NULL,
[SrvID] [int] NULL,
[Name] [varchar] (64) NULL,
[Type] [varchar] (16) NULL,
[SN] [varchar] (16) NULL,
[Addr] [varchar] (32) NULL,
[PortId] [uniqueidentifier] NULL,
[ProfileID] [int] NULL,
[Active] [tinyint] NULL,
[Scheduled] [tinyint] NULL,
[Removed] [tinyint] NULL,
[RackID] [uniqueidentifier] NULL,
[RackPositionRow] [int] NULL,
[RackPositionCol] [int] NULL,
[DefinitionVersion] [varchar] (8) NULL,
[ProtocolDefinitionVersion] [varchar] (8) NULL
) ON [PRIMARY]



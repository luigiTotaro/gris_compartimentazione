﻿CREATE TABLE [dbo].[device_type]
(
[DeviceTypeID] [varchar] (16) NOT NULL,
[SystemID] [int] NOT NULL,
[VendorID] [int] NOT NULL,
[DeviceTypeDescription] [varchar] (512) NULL,
[WSUrlPattern] [varchar] (512) NULL
) ON [PRIMARY]



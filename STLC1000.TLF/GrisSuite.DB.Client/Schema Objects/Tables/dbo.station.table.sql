﻿CREATE TABLE [dbo].[station]
(
[StationID] [uniqueidentifier] NOT NULL,
[StationXMLID] [int] NULL,
[StationName] [varchar] (64) NULL,
[Removed] [tinyint] NULL
) ON [PRIMARY]



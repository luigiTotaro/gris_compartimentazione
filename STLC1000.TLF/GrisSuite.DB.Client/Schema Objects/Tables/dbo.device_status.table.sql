﻿CREATE TABLE [dbo].[device_status]
(
[DevID] [bigint] NOT NULL,
[SevLevel] [int] NULL,
[Description] [varchar] (256) NULL,
[Offline] [tinyint] NULL,
[AckFlag] [bit] NOT NULL,
[AckDate] [datetime] NULL
) ON [PRIMARY]



﻿CREATE PROCEDURE [dbo].[tf_DelPort]
(
	@Original_PortID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	
	UPDATE Devices SET PortID = Null WHERE (([PortID] = @Original_PortID))
	DELETE FROM [port] WHERE (([PortID] = @Original_PortID))



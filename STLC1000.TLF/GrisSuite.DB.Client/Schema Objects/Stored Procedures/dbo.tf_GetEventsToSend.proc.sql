﻿-- Stored procedure
CREATE PROCEDURE [dbo].[tf_GetEventsToSend]
AS
SET NOCOUNT ON;
SELECT TOP (1000) EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory
FROM events
ORDER BY Created

GO


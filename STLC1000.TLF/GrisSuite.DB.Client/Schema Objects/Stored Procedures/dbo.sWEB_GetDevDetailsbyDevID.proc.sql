﻿CREATE PROCEDURE [dbo].[sWEB_GetDevDetailsbyDevID] @DevID bigint
AS
BEGIN
	SET NOCOUNT ON;
	SELECT     d.Name, d.Type, d_s.Offline, nodes.Name AS Nodo, zones.Name AS Zona, regions.Name AS Regione, servers.Host, d_s.Description AS Stato, d.SN, 
						  d.Addr, port.PortName, rack.RackName, building.BuildingName, d_s.SevLevel, port.PortType
	FROM         device_status AS d_s INNER JOIN
						  devices AS d ON d_s.DevID = d.DevID INNER JOIN
						  nodes ON d.NodID = nodes.NodID INNER JOIN
						  zones ON nodes.ZonID = zones.ZonID INNER JOIN
						  regions ON zones.RegID = regions.RegID INNER JOIN
						  servers ON d.SrvID = servers.SrvID INNER JOIN
						  port ON d.PortId = port.PortID AND servers.SrvID = port.SrvID INNER JOIN
						  rack ON d.RackID = rack.RackID INNER JOIN
						  building ON rack.BuildingID = building.BuildingID
	WHERE     (d.Removed = 0) AND (port.Removed = 0) AND (rack.Removed = 0) AND (building.Removed = 0) AND (d.DevID = @DevID)
END



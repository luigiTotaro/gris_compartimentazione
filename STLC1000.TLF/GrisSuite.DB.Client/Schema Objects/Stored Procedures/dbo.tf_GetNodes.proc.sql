﻿/****** Object:  StoredProcedure [dbo].[tf_GetNodes]    Script Date: 11/20/2006 18:19:00 ******/
CREATE PROCEDURE [dbo].[tf_GetNodes]
AS
	SET NOCOUNT ON;
	SELECT NodID, ZonID, Name, Removed, Meters
	FROM nodes
	WHERE ISNULL(Removed,0) = 0


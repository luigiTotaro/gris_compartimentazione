﻿CREATE PROCEDURE [dbo].[sWEB_UpdServerAsValidated]
(
	@SrvID int,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ClientDateValidationObtained datetime,
	@ClientKey varbinary(148)
	)
AS
	UPDATE servers
	SET ClientSupervisorSystemXMLValidated = @ClientSupervisorSystemXMLValidated, ClientValidationSign = @ClientValidationSign, ClientDateValidationObtained = @ClientDateValidationObtained, ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);
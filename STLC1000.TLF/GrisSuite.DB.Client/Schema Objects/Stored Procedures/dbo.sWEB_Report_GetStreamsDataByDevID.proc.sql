﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 22/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sWEB_Report_GetStreamsDataByDevID] 
	@DevID BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ReturnT TABLE
	(
		DevID BIGINT NOT NULL
		, StrID INT NOT NULL
		, FieldID INT NOT NULL
		, ArrayID INT NOT NULL
		, StreamName VARCHAR(64) NOT NULL
		, SteamDate DATETIME NOT NULL
		, StreamSevLevel INT NOT NULL
		, FieldName VARCHAR(64) NOT NULL
		, FieldValue VARCHAR(1024) NULL
		, FieldSevLevel INT NOT NULL
		, CaseDescription VARCHAR(2048) NULL
		, CaseSevLevel INT NOT NULL
	);

	CREATE TABLE ##Cases ( Seq INT NOT NULL, Term VARCHAR(2048) NULL );

	DECLARE 
		@StrID INT
		, @FieldID INT
		, @ArrayID INT
		, @StreamName VARCHAR(64)
		, @SteamDate DATETIME
		, @StreamSevLevel INT
		, @FieldName VARCHAR(64)
		, @FieldDesc VARCHAR(MAX)
		, @FieldValue VARCHAR(1024)
		, @FieldSevLevel INT
		, @ArraySize INT
		, @MultiCase INT;

	DECLARE device_cursor CURSOR FOR 
	SELECT 
		s.StrID, sf.FieldID, sf.ArrayID
		, s.Name as StreamName, s.[DateTime] as SteamDate, s.SevLevel as StreamSevLevel
		, sf.Name as FieldName, sf.[Description] as FieldDesc, sf.[Value] as FieldValue, sf.SevLevel as FieldSevLevel
		, COUNT(*) OVER(PARTITION BY d.DevID, s.StrID, sf.FieldID) as ArraySize
		, CHARINDEX(';', sf.Description) as MultiCase
	FROM devices d
	INNER JOIN streams s ON d.DevID = s.DevID
	INNER JOIN stream_fields sf ON s.DevID = sf.DevID and s.StrID = sf.StrID
	WHERE d.DevID = @DevID
	ORDER BY d.DevID, s.StrID, FieldID, ArrayID;

	OPEN device_cursor;

	FETCH NEXT FROM device_cursor 
	INTO 
		@StrID
		, @FieldID
		, @ArrayID
		, @StreamName
		, @SteamDate
		, @StreamSevLevel
		, @FieldName
		, @FieldDesc
		, @FieldValue
		, @FieldSevLevel
		, @ArraySize
		, @MultiCase;

	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF ( @MultiCase > 0 )
			BEGIN
				INSERT INTO ##Cases
				SELECT * FROM dbo.Split(';', @FieldDesc) OPTION (MAXRECURSION 400);
			END
		ELSE
			BEGIN
				INSERT INTO ##Cases
				VALUES (1, @FieldDesc);
			END
		
		INSERT INTO @ReturnT 
		(
		DevID
		, StrID
		, FieldID
		, ArrayID	
		, StreamName
		, SteamDate
		, StreamSevLevel
		, FieldName
		, FieldValue
		, FieldSevLevel
		, CaseDescription
		, CaseSevLevel
		)
		SELECT 
			@DevID
			, @StrID
			, @FieldID
			, @ArrayID
			, @StreamName
			, @SteamDate
			, @StreamSevLevel
			, CASE WHEN ( @ArraySize > 1 ) THEN @FieldName + ' ' + CAST((@ArrayID + 1) AS VARCHAR(3)) ELSE @FieldName END
			, @FieldValue
			, @FieldSevLevel
			, CASE WHEN CHARINDEX('=', Term) > 1 THEN SUBSTRING(Term, 1, CHARINDEX('=', Term) - 1) ELSE Term END 
			, CASE WHEN CHARINDEX('=', Term) > 1 THEN CAST(SUBSTRING(Term, CHARINDEX('=', Term) + 1, LEN(Term) - 1) as INT) ELSE -2 END -- -1 è un valore valido su GRIS
		FROM ##Cases
		--ORDER BY Term;
		
		TRUNCATE TABLE ##Cases;

		FETCH NEXT FROM device_cursor 
		INTO 
			@StrID
			, @FieldID
			, @ArrayID
			, @StreamName
			, @SteamDate
			, @StreamSevLevel
			, @FieldName
			, @FieldDesc
			, @FieldValue
			, @FieldSevLevel
			, @ArraySize
			, @MultiCase;
	END
	CLOSE device_cursor;
	DEALLOCATE device_cursor;
	DROP TABLE ##Cases;

	SELECT 
		DevID
		, StrID
		, FieldID
		, ArrayID	
		, StreamName
		, SteamDate
		, (SELECT [Description] FROM severity WHERE SevLevel = rt.StreamSevLevel) as StreamSevLevel
		, FieldName
		, FieldValue
		, (SELECT [Description] FROM severity WHERE SevLevel = rt.FieldSevLevel) as FieldSevLevel
		, CaseDescription
		, (SELECT [Description] FROM severity WHERE SevLevel = rt.CaseSevLevel) as CaseSevLevel
	FROM @ReturnT rt
	ORDER BY DevID, StrID, FieldID, ArrayID;

END
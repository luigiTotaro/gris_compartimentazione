﻿CREATE PROCEDURE [dbo].[tf_DelStream_Fields]
(
	@Original_FieldID int,
	@Original_ArrayID int,
	@Original_StrID int,
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	
	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE (([DeltaFieldID] = @Original_FieldID) 
			AND ([DeltaArrayID] = @Original_ArrayID) 
			AND ([DeltaStrID] = @Original_StrID) 
			AND ([DeltaDevID] = @Original_DevID))

	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE [ReferenceId] IN  (SELECT [reference].[ReferenceId]	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE (([FieldID] = @Original_FieldID) 
									AND ([ArrayID] = @Original_ArrayID) 
									AND ([StrID] = @Original_StrID) 
									AND ([DevID] = @Original_DevID))
							 )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE (([FieldID] = @Original_FieldID) 
		AND ([ArrayID] = @Original_ArrayID) 
		AND ([StrID] = @Original_StrID) 
		AND ([DevID] = @Original_DevID))
	
	DELETE FROM [stream_fields] 
	WHERE (([FieldID] = @Original_FieldID) 
		AND ([ArrayID] = @Original_ArrayID) 
		AND ([StrID] = @Original_StrID) 
		AND ([DevID] = @Original_DevID))



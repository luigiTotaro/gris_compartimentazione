﻿CREATE PROCEDURE [dbo].[tf_UpdStation]
(
	@StationID uniqueidentifier,
	@StationXMLID int,
	@StationName varchar(64), 
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT StationID FROM station WHERE (StationID = @StationID))
		UPDATE [station] SET [StationID] = @StationID, [StationXMLID] = @StationXMLID, [StationName] = @StationName, [Removed] = @Removed WHERE (([StationID] = @StationID));
	ELSE
		INSERT INTO [station] ([StationID], [StationXMLID], [StationName], [Removed]) VALUES (@StationID, @StationXMLID, @StationName, @Removed);



﻿/****** Object:  StoredProcedure [dbo].[tf_UpdStreams]    Script Date: 11/20/2006 18:19:10 ******/
CREATE PROCEDURE [dbo].[tf_UpdStreams]
(
	@DevID bigint,
	@StrID int,
	@Name varchar(64),
	@DateTime datetime,
	@SevLevel int,
	@Visible tinyint,
	@IsDeleted bit,
	@Data image = null
)
AS
	SET NOCOUNT OFF;
	
IF EXISTS( SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID))
	IF @IsDeleted = 0
		UPDATE [streams] SET [DevID] = @DevID, [StrID] = @StrID, [Name] = @Name, [DateTime] = @DateTime, [SevLevel] = @SevLevel, [Visible] = @Visible, [Data] = @Data  WHERE (([DevID] = @DevID) AND ([StrID] = @StrID));
	ELSE
		EXEC [tf_DelStreams] @DevID, @StrID 
ELSE
	INSERT INTO [streams] ([DevID], [StrID], [Name], [DateTime], [SevLevel], [Visible], [Data]) VALUES (@DevID, @StrID, @Name, @DateTime, @SevLevel, @Visible, @Data)


﻿/****** Object:  StoredProcedure [dbo].[tf_GetStream_Fields]    Script Date: 11/20/2006 18:19:04 ******/
CREATE PROCEDURE [dbo].[tf_GetStream_Fields]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0) 




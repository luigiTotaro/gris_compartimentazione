﻿CREATE PROCEDURE [dbo].[tf_GetStation]
AS
	SET NOCOUNT ON;
	SELECT StationID, StationXMLID, StationName, Removed
	FROM station
	WHERE ISNULL(Removed,0) = 0



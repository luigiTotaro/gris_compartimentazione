﻿/****** Object:  StoredProcedure [dbo].[tf_GetStreams]    Script Date: 11/20/2006 18:19:05 ******/
CREATE PROCEDURE [dbo].[tf_GetStreams]
AS
	SET NOCOUNT ON;
	SELECT DevID, StrID, [Name], [DateTime], SevLevel, Visible, 0 as IsDeleted, Data	
	FROM streams
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0) 



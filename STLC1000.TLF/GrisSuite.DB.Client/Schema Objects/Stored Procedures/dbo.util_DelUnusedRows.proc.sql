CREATE PROCEDURE [dbo].[util_DelUnusedRows]
AS
	
	DELETE	FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE	FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);
	DELETE	FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE	FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE  FROM procedures WHERE DevID  IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE	FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE	FROM devices WHERE (devices.Removed = 1);

	DELETE FROM nodes WHERE Removed = 1;
	DELETE FROM zones WHERE Removed = 1;
	DELETE FROM regions WHERE Removed = 1;
	

	DELETE FROM rack WHERE Removed = 1;
	DELETE FROM building WHERE Removed = 1;
	DELETE FROM station WHERE Removed = 1;
	DELETE FROM port WHERE Removed = 1;

	DELETE FROM port WHERE SrvID NOT IN (SELECT DISTINCT SrvID FROM devices);
	DELETE FROM servers WHERE SrvID NOT IN (SELECT DISTINCT SrvID FROM devices);
GO
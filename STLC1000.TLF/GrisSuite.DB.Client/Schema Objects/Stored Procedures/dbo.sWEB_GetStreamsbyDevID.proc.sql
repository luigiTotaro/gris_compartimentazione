﻿CREATE PROCEDURE [dbo].[sWEB_GetStreamsbyDevID] @DevID bigint
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [Name],
		[DateTime] As Data,
		[DateTime] As Ora,
		SevLevel,
		DevID,
		StrID 
	FROM	streams
	WHERE	((Visible = 1) AND
		(DevID = @DevID))
END



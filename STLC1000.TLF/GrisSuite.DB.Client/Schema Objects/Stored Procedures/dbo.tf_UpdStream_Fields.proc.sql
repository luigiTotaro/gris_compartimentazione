﻿/****** Object:  StoredProcedure [dbo].[tf_UpdStream_Fields]    Script Date: 11/20/2006 18:19:10 ******/
CREATE PROCEDURE [dbo].[tf_UpdStream_Fields]
(
	@FieldID int,
	@ArrayID int,
	@StrID int,
	@DevID bigint,
	@Name varchar(64),
	@SevLevel int,
	@Value varchar(1024),
	@Description text,
	@Visible tinyint,
	@IsDeleted bit,
	@ReferenceID uniqueidentifier
)
AS
	SET NOCOUNT OFF;

IF EXISTS (SELECT FieldID FROM stream_fields WITH(UPDLOCK) WHERE (ArrayID = @ArrayID) AND (DevID = @DevID) AND (FieldID = @FieldID) AND (StrID = @StrID))
	IF @IsDeleted = 0
		UPDATE [stream_fields] SET [FieldID] = @FieldID, [ArrayID] = @ArrayID, [StrID] = @StrID, [DevID] = @DevID, [Name] = @Name, [SevLevel] = @SevLevel, [Value] = @Value, [Description] = @Description, [Visible] = @Visible, ReferenceID = @ReferenceID WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
	ELSE
		DELETE FROM [stream_fields] WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
ELSE
	INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], ReferenceID) VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, @Visible, @ReferenceID);



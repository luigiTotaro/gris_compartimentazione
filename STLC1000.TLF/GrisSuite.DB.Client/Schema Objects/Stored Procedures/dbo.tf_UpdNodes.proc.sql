﻿/****** Object:  StoredProcedure [dbo].[tf_UpdNodes]    Script Date: 11/20/2006 18:19:08 ******/
CREATE PROCEDURE [dbo].[tf_UpdNodes]
(
	@NodID bigint,
	@ZonID bigint,
	@Name varchar(64),
	@Removed bit,
	@Meters int
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT NodID FROM nodes WHERE (NodID = @NodID))
		UPDATE [nodes] SET [NodID] = @NodID, [ZonID] = @ZonID, [Name] = @Name, [Removed] = @Removed, Meters = @Meters WHERE (([NodID] = @NodID));
	ELSE
		INSERT INTO [nodes] ([NodID], [ZonID], [Name], [Removed], Meters) VALUES (@NodID, @ZonID, @Name, @Removed, @Meters);



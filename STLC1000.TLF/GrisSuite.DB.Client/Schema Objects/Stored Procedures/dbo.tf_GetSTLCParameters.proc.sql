﻿CREATE PROCEDURE [dbo].[tf_GetSTLCParameters]
AS
	DECLARE @SrvID BIGINT
	SET @SrvID = (SELECT SrvID FROM Servers WHERE SrvID IN (SELECT DISTINCT SrvID FROM devices))
	
	IF NOT @SrvID IS NULL
		SELECT @SrvID AS SrvID, ParameterName, ParameterValue, ParameterDescription
		FROM stlc_parameters
	ELSE
		RAISERROR ('SrvID not found.',16 /*Severity*/,1 /*State*/);
		
RETURN 0;
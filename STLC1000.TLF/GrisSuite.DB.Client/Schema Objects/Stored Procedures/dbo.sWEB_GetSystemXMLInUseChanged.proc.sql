﻿CREATE PROCEDURE [dbo].[sWEB_GetSystemXMLInUseChanged]
	@SrvID INT
AS
	SELECT 
		(
			CASE WHEN (
						SupervisorSystemXML IS NOT NULL 
						AND 
						ClientSupervisorSystemXMLValidated IS NOT NULL 
						AND 
						(CAST(SupervisorSystemXML AS NVARCHAR(MAX)) = CAST(ClientSupervisorSystemXMLValidated AS NVARCHAR(MAX)))
						) 
			THEN 1 
			ELSE 0 
			END
		) as SystemXMLInUseChanged
	FROM servers
	WHERE (SrvID = @SrvID);
﻿/****** Object:  StoredProcedure [dbo].[tf_UpdZones]    Script Date: 11/20/2006 18:19:11 ******/

CREATE PROCEDURE [dbo].[tf_UpdZones]
(
	@ZonID bigint,
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT ZonID FROM zones WHERE (ZonID = @ZonID))
		UPDATE [zones] SET [ZonID] = @ZonID, [RegID] = @RegID, [Name] = @Name, [Removed] = @Removed WHERE (([ZonID] = @ZonID));
	ELSE
		INSERT INTO [zones] ([ZonID], [RegID], [Name],[Removed]) VALUES (@ZonID, @RegID, @Name, @Removed);



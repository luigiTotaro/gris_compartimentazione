﻿CREATE PROCEDURE [dbo].[sWEB_GetDevPathbyDevID]	@DevID bigint
AS
BEGIN
	SET NOCOUNT ON;
    
	SELECT	regions.regid, zones.zonid, nodes.nodid, devices.devid
	FROM	regions, zones, nodes, devices
	WHERE	devices.nodid=nodes.nodid AND
			nodes.zonid=zones.zonid AND
			zones.regid=regions.regid AND
			devices.devid=@DevID

END



﻿CREATE PROCEDURE [dbo].[tf_UpdServers]
(
	@SrvID int,
	@Name varchar(64),
	@Host varchar(64),
	@FullHostName varchar(256),
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64),
	@SupervisorSystemXML xml,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ClientDateValidationRequested datetime,
	@ClientDateValidationObtained datetime,
	@ClientKey varbinary(148),
	@NodID bigint = NULL,
	@ServerVersion varchar(32) = NULL,
	@MAC varchar(16) = NULL
)
AS
	SET NOCOUNT OFF;
	DECLARE @OriginalSrvID int

	SET @MAC = RTRIM(ISNULL(@MAC,''))

	IF @MAC <> '' 
	BEGIN -- AGGIORNAMENTO VIA MAC
		SELECT @OriginalSrvID = SrvID FROM servers WHERE (MAC = @MAC)
		IF NOT @OriginalSrvID IS NULL
			BEGIN
				IF @OriginalSrvID <> @SrvID	
					RAISERROR ('SrvID change not supported ',16 /*Severity*/,1 /*State*/);
				ELSE
					UPDATE [servers] 
						SET [SrvID] = @SrvID, [Name] = @Name, [Host] = @Host, [FullHostName] = @FullHostName, 
						[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
						[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
						[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
						[ClientKey] = @ClientKey, [NodID] = @NodID, [ServerVersion] = @ServerVersion, [MAC] = @MAC
					WHERE (([SrvID] = @SrvID));
			END
		ELSE
			BEGIN
				IF EXISTS (SELECT SrvID FROM servers WHERE ([SrvID] = @SrvID and RTRIM(ISNULL(MAC,'')) = '')) 
					-- Prima centralizzazione post upgrade.
					UPDATE [servers] 
						SET [SrvID] = @SrvID, [Name] = @Name, [Host] = @Host, [FullHostName] = @FullHostName, 
						[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
						[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
						[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
						[ClientKey] = @ClientKey, [NodID] = @NodID, [ServerVersion] = @ServerVersion, [MAC] = @MAC
					WHERE (([SrvID] = @SrvID));
				ELSE
					INSERT INTO [servers] ([SrvID], [Name], [Host], [FullHostName], [IP], [LastUpdate], [LastMessageType], [SupervisorSystemXML], [ClientSupervisorSystemXMLValidated], [ClientValidationSign], [ClientDateValidationRequested], [ClientDateValidationObtained], [ClientKey], [NodID], [ServerVersion], [MAC]) 
					VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC);
			END
	END
	ELSE 
	BEGIN -- AGGIORNAMENTO VIA SRVID 
		IF EXISTS (SELECT SrvID FROM servers WHERE ([SrvID] = @SrvID))
			UPDATE [servers] 
				SET [SrvID] = @SrvID, [Name] = @Name, [Host] = @Host, [FullHostName] = @FullHostName, 
				[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
				[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
				[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
				[ClientKey] = @ClientKey, [NodID] = @NodID, [ServerVersion] = @ServerVersion, [MAC] = @MAC
			WHERE (([SrvID] = @SrvID));
		ELSE
			INSERT INTO [servers] ([SrvID], [Name], [Host], [FullHostName], [IP], [LastUpdate], [LastMessageType], [SupervisorSystemXML], [ClientSupervisorSystemXMLValidated], [ClientValidationSign], [ClientDateValidationRequested], [ClientDateValidationObtained], [ClientKey], [NodID], [ServerVersion], [MAC]) 
			VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC);
	END



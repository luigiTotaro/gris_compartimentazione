﻿CREATE PROCEDURE [dbo].[tf_DelBuilding]
(
	@Original_BuildingID uniqueidentifier
)
AS
	SET NOCOUNT ON;

	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE [RackID] IN (SELECT RackID FROM [rack] WHERE ([BuildingID] = @Original_BuildingID))
	DELETE FROM [rack] WHERE (([BuildingID] = @Original_BuildingID))
	DELETE FROM [building] WHERE (([BuildingID] = @Original_BuildingID))



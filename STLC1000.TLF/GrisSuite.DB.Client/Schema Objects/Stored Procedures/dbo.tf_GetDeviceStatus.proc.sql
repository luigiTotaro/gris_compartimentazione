﻿/****** Object:  StoredProcedure [dbo].[tf_GetDeviceStatus]    Script Date: 11/20/2006 18:18:59 ******/
CREATE PROCEDURE [dbo].[tf_GetDeviceStatus]
AS
	SET NOCOUNT ON;
	SELECT DevID, SevLevel, Description, Offline, 0 as IsDeleted
	FROM device_status
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0)



﻿CREATE PROCEDURE [dbo].[sWEB_GetServer]
AS
	SELECT 
		OutServers.SrvID, OutServers.Name, OutServers.Host, OutServers.FullHostName, OutServers.IP, OutServers.LastUpdate, 
		OutServers.LastMessageType, OutServers.SupervisorSystemXML, OutServers.ClientSupervisorSystemXMLValidated, 
		OutServers.ClientValidationSign, OutServers.ClientDateValidationRequested, OutServers.ClientDateValidationObtained, OutServers.ClientKey
	FROM servers OutServers
	WHERE EXISTS
	(
		SELECT *
		FROM servers InnServers
		INNER JOIN devices ON InnServers.SrvID = devices.SrvID
		WHERE InnServers.SrvID = OutServers.SrvID
	);
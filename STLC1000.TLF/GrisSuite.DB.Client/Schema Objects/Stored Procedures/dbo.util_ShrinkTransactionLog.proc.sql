﻿CREATE PROCEDURE [dbo].[util_ShrinkTransactionLog]
(
	@TriggerTranLogSize INT = 50
)
AS
	
	-- @TriggerTranLogSize è la grandezza massima in MB oltre la quale viene eseguito lo shrink del file
	
	DECLARE @RETURN BIT;
	DECLARE @LogFileName VARCHAR(500)
	DECLARE @PagesNumb INT

	SELECT @LogFileName = [name], @PagesNumb = [size] FROM sys.database_files WHERE type_desc = 'LOG'

	IF ('SIMPLE' = (SELECT recovery_model_desc FROM sys.databases WHERE [name] = db_name()))
	BEGIN 
		BEGIN TRY
			IF ((@PagesNumb*8/1024) > @TriggerTranLogSize)
			BEGIN
				DBCC SHRINKFILE (@LogFileName) WITH NO_INFOMSGS;
				SET @RETURN = 1;
			END
			ELSE
			BEGIN
				SET @RETURN = 0;
			END
		END TRY
		BEGIN CATCH
			SET @RETURN = 0;
		END CATCH
	END
	ELSE 
	BEGIN
		--ALTER DATABASE Telefin
		--SET RECOVERY SIMPLE;
		--GO

		--DBCC SHRINKFILE (Telefin);
		--GO

		--ALTER DATABASE Telefin
		--SET RECOVERY FULL;
		SET @RETURN = 0;
	END;
	SELECT @RETURN;
RETURN 0;



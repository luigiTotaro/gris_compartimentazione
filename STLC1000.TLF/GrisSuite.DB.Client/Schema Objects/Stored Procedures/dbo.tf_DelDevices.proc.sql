﻿CREATE PROCEDURE [dbo].[tf_DelDevices]
(
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	UPDATE [devices] WITH (ROWLOCK, XLOCK) SET Removed = 1 WHERE [DevID] = @Original_DevID

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (	SELECT reference.ReferenceID FROM [reference] 
								INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE ([DevID] = @Original_DevID) )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] = @Original_DevID)
	
	DELETE FROM [stream_fields] WHERE [DevID] = @Original_DevID
	DELETE FROM [streams]		WHERE [DevID] = @Original_DevID
	DELETE FROM [device_status] WHERE [DevID] = @Original_DevID
	DELETE FROM [devices]		WHERE [DevID] = @Original_DevID



﻿/****** Object:  StoredProcedure [dbo].[tf_UpdDeviceStatus]    Script Date: 11/20/2006 18:19:08 ******/
CREATE PROCEDURE [dbo].[tf_UpdDeviceStatus]
(
	@DevID bigint,
	@SevLevel int,
	@Description varchar(256),
	@Offline tinyint,
	@IsDeleted bit
)
AS
	SET NOCOUNT OFF;

	IF EXISTS (SELECT [DevID] FROM [device_status] WITH(UPDLOCK) WHERE [DevID] = @DevID)
		IF @IsDeleted = 0
			UPDATE [device_status] 
			SET [DevID] = @DevID, [SevLevel] = @SevLevel, [Description] = @Description, [Offline] = @Offline 
			WHERE [DevID] = @DevID;
		ELSE
			EXEC [tf_DelDeviceStatus] @DevID;
	ELSE
		INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline]) 
		VALUES (@DevID, @SevLevel, @Description, @Offline);



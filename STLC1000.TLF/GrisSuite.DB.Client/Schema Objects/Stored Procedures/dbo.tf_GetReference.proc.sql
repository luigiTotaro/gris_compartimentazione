﻿CREATE PROCEDURE [dbo].[tf_GetReference]
AS
	SET NOCOUNT ON;

	SELECT ReferenceID, Value, DateTime, Visible, DeltaDevID, DeltaStrID, DeltaFieldID, DeltaArrayID
	FROM reference
	WHERE ReferenceID IN 
		(SELECT DISTINCT ReferenceID 
			FROM Stream_Fields INNER JOIN Devices ON Stream_Fields.DevID = Devices.DevID
			WHERE ISNULL(Removed,0) = 0)
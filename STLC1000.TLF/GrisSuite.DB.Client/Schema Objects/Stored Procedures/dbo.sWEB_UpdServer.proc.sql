﻿CREATE PROCEDURE [dbo].[sWEB_UpdValidateServer]
	@SrvID int,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign int,
	@ClientDateValidationObtained int,
	@ClientKey int
AS
	UPDATE servers
	SET ClientSupervisorSystemXMLValidated = @ClientSupervisorSystemXMLValidated, ClientValidationSign = @ClientValidationSign, ClientDateValidationObtained = @ClientDateValidationObtained, ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);
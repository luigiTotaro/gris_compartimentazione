﻿CREATE PROCEDURE [dbo].[sWEB_Report_GetConfigurationValidated]
	@SrvID int
AS
	WITH Topography
	AS
	(
		SELECT 
			Topography.SupervisorSystemXML.value('../../@id[1]', 'int') AS [StationID],
			Topography.SupervisorSystemXML.value('../../@name[1]', 'varchar(1000)') AS [Station],
			Topography.SupervisorSystemXML.value('../@id[1]', 'int') AS [BuildingID],
			Topography.SupervisorSystemXML.value('../@name[1]', 'varchar(1000)') AS [Building],
			Topography.SupervisorSystemXML.value('@id[1]', 'int') AS [LocationID],
			Topography.SupervisorSystemXML.value('@name[1]', 'varchar(1000)') AS [Location]
		FROM servers CROSS APPLY ClientSupervisorSystemXMLValidated.nodes('//location') AS Topography(SupervisorSystemXML)
		WHERE (SrvID = @SrvID)
	),
	Ports
	AS
	(
		SELECT 
			Ports.SupervisorSystemXML.value('@id[1]', 'int') AS [PortID],
			Ports.SupervisorSystemXML.value('@name[1]', 'varchar(64)') AS [Port]
		FROM servers CROSS APPLY ClientSupervisorSystemXMLValidated.nodes('//port/item') AS Ports(SupervisorSystemXML)
		WHERE (SrvID = @SrvID)
	),	
	Devices
	AS
	(
		SELECT 
			Devices.SupervisorSystemXML.value('../../../../../@code[1]', 'varchar(64)') AS SystemCode,
			Devices.SupervisorSystemXML.value('../../../../../@name[1]', 'varchar(64)') AS SystemName,
			Devices.SupervisorSystemXML.value('../../../../@SrvID[1]', 'int') AS ServerID,
			Devices.SupervisorSystemXML.value('../../../../@host[1]', 'varchar(64)') AS ServerHost,
			Devices.SupervisorSystemXML.value('../../../../@name[1]', 'varchar(64)') AS ServerName,
			Devices.SupervisorSystemXML.value('../../../@RegID[1]', 'bigint') AS RegionID,
			Devices.SupervisorSystemXML.value('../../../@name[1]', 'varchar(64)') AS RegionName,
			Devices.SupervisorSystemXML.value('../../@ZonID[1]', 'bigint') AS ZoneID,
			Devices.SupervisorSystemXML.value('../../@name[1]', 'varchar(64)') AS ZoneName,
			Devices.SupervisorSystemXML.value('../@NodID[1]', 'bigint') AS NodeID,
			Devices.SupervisorSystemXML.value('../@name[1]', 'varchar(64)') AS NodeName,
			Devices.SupervisorSystemXML.value('../@modem_size[1]', 'varchar(64)') AS NodeModemSize,
			Devices.SupervisorSystemXML.value('../@modem_addr[1]', 'varchar(64)') AS NodeModemAddr,
			Devices.SupervisorSystemXML.value('@DevID[1]', 'int') AS [DevID],
			Devices.SupervisorSystemXML.value('@name[1]', 'varchar(64)') AS [Name],
			Devices.SupervisorSystemXML.value('@station[1]', 'int') AS [StationID],
			Devices.SupervisorSystemXML.value('@building[1]', 'int') AS [BuildingID],
			Devices.SupervisorSystemXML.value('@location[1]', 'int') AS [LocationID],
			Devices.SupervisorSystemXML.value('@position[1]', 'varchar(64)') AS [Position], 
			Devices.SupervisorSystemXML.value('@addr[1]', 'varchar(32)') AS [Address], 
			Devices.SupervisorSystemXML.value('@SN[1]', 'varchar(16)') AS SN, 
			Devices.SupervisorSystemXML.value('@type[1]', 'varchar(16)') AS [Type],
			Devices.SupervisorSystemXML.value('@port[1]', 'int') AS [PortID]
		FROM servers EXT CROSS APPLY ClientSupervisorSystemXMLValidated.nodes('//device') AS Devices(SupervisorSystemXML)
		WHERE (SrvID = @SrvID)
		AND (LOWER(Devices.SupervisorSystemXML.value('@active[1]', 'varchar(5)')) = 'true')
		AND (LOWER(Devices.SupervisorSystemXML.value('@scheduled[1]', 'varchar(5)')) = 'true')
	)
	SELECT 
		SystemCode, SystemName, ServerID, ServerHost, ServerName, RegionName, 
		ZoneName, NodeName, NodeModemSize, NodeModemAddr, [DevID], [Name], [Type], 
		[Position], [Address], SN, [Station], [Building], [Location], [Port]
	FROM Devices
	INNER JOIN Topography ON Topography.StationID = Devices.StationID AND Topography.BuildingID = Devices.BuildingID AND Topography.LocationID = Devices.LocationID
	INNER JOIN Ports ON Ports.PortID = Devices.PortID
	ORDER BY RegionID, ZoneID, [DevID];
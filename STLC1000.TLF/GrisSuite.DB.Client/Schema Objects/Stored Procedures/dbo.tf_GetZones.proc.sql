﻿/****** Object:  StoredProcedure [dbo].[tf_GetZones]    Script Date: 11/20/2006 18:19:06 ******/
CREATE PROCEDURE [dbo].[tf_GetZones]
AS
	SET NOCOUNT ON;
	SELECT ZonID, RegID, Name, Removed
	FROM zones
	WHERE ISNULL(Removed,0) = 0



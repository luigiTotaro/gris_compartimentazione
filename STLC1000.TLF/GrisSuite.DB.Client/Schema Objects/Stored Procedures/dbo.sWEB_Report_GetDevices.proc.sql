﻿CREATE PROCEDURE [dbo].[sWEB_Report_GetDevices]
AS
	SELECT devices.DevID, devices.Name, devices.SN, devices.Type, device_type.DeviceTypeDescription, devices.DefinitionVersion, devices.ProtocolDefinitionVersion
	FROM devices 
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	ORDER BY devices.Name;
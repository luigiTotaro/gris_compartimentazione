﻿CREATE PROCEDURE [dbo].[sWEB_GetOffDevList] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT     devices.DevID, devices.Name, device_status.Description, 'Offline' AS [Status], device_status.SevLevel, devices.Type as DeviceType
	FROM         devices INNER JOIN
						  device_status ON devices.DevID = device_status.DevID
	WHERE     (devices.Removed = 0) AND (device_status.Offline = 1)
	ORDER BY devices.Name
END



﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 21/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sWEB_Report_GetStationStatus] 
AS
BEGIN
	SET NOCOUNT ON;

	select 
		r.Name as RegionName, z.Name as ZoneName, n.Name as NodeName, b.BuildingName, k.RackName, /* topography */
		s.Host, p.PortName, p.PortType, /* server and ports */
		d.DevID, d.Name, d.[Type], d.SN, ds.[Offline], ds.[Description] as [Status], ds.SevLevel, /* device */
		case 
			when p.PortType = 'TCP Client' then dbo.GetIPStringFromInt(d.Addr)
			else d.Addr
		end as DeviceAddress
	from device_status ds 
		inner join devices d on ds.DevID = d.DevID 
		inner join nodes n on d.NodID = n.NodID 
		inner join zones z on n.ZonID = z.ZonID 
		inner join regions r on z.RegID = r.RegID 
		inner join [servers] s on d.SrvID = s.SrvID 
		inner join port p on d.PortId = p.PortID and s.SrvID = p.SrvID 
		inner join rack k on d.RackID = k.RackID 
		inner join building b on k.BuildingID = b.BuildingID
	where
		(d.Removed = 0) and (p.Removed = 0) and (k.Removed = 0) and (b.Removed = 0)
	order by 
		r.Name, z.Name, n.Name, d.Name;
END
﻿CREATE PROCEDURE [dbo].[tf_UpdRack]
(
	@RackID uniqueidentifier,
	@RackXMLID int,
	@BuildingID uniqueidentifier,
	@RackName varchar(64),
	@RackType varchar(64),
	@RackDescription varchar(256),
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT RackID FROM rack WHERE (RackID = @RackID))
		UPDATE [rack] SET [RackXMLID] = @RackXMLID, [BuildingID] = @BuildingID, [RackName] = @RackName, [RackType] = @RackType, [RackDescription] = @RackDescription, [Removed] = @Removed WHERE (([RackID] = @RackID));
	ELSE
		INSERT INTO [rack] ([RackID], [RackXMLID], [BuildingID], [RackName], [RackType], [RackDescription], [Removed]) VALUES (@RackID, @RackXMLID, @BuildingID, @RackName, @RackType, @RackDescription, @Removed);



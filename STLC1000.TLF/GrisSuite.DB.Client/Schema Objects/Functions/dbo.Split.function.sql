﻿CREATE FUNCTION dbo.Split (@sep char(1), @str varchar(2048))
RETURNS table
AS
RETURN (
	WITH Pieces(seq, start, stop) AS 
	(
	  SELECT 1, 1, CHARINDEX(@sep, @str)
	  UNION ALL
	  SELECT seq + 1, stop + 1, CHARINDEX(@sep, @str, stop + 1)
	  FROM Pieces
	  WHERE stop > 0
	)
	SELECT seq,
	  SUBSTRING(@str, start, CASE WHEN stop > 0 THEN stop-start ELSE 2048 END) AS SubStr
	FROM Pieces
  )

﻿CREATE FUNCTION [dbo].[GetIPStringFromInt]
(
	@IpIntRepresentation  INT
)
RETURNS VARCHAR(15)
AS
BEGIN
	DECLARE @Result AS VARCHAR(15);
	DECLARE @IpHexRepresentation AS CHAR(10);

	SET @Result = '';
	EXECUTE @IpHexRepresentation = sys.fn_varbintohexstr @IpIntRepresentation;

	SET @Result = @Result + CONVERT(VARCHAR(3), CONVERT(INT, dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 3, 2)))) + '.';
	SET @Result = @Result + CONVERT(VARCHAR(3), CONVERT(INT, dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 5, 2)))) + '.';
	SET @Result = @Result + CONVERT(VARCHAR(3), CONVERT(INT, dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 7, 2)))) + '.';
	SET @Result = @Result + CONVERT(VARCHAR(3), CONVERT(INT, dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 9, 2))));

	IF (@@ERROR <> 0)
	BEGIN
		SET @Result = '0.0.0.0';
	END

	RETURN @Result;
END



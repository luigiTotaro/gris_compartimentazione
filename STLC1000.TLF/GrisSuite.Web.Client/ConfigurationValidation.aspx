<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true"
	codefile="ConfigurationValidation.aspx.cs" inherits="ConfigurationValidation"
	title="sWEB - Gestione Validazione STLC1000 -" meta:resourcekey="ConfigurationValidationPage" %>

<asp:content id="cntMainArea" contentplaceholderid="cphMainArea" runat="Server">
	<table cellpadding="0" cellspacing="0" style="width: 800px;">
		<tr style="height: 20px"><td class="nav_shadow_bg"></td></tr>
		<tr>
			<td>
				<br />
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="vertical-align: bottom;">
							<img alt="" src="Images/boxDX_top.gif" style="vertical-align: bottom;" />
						</td>
					</tr>
					<tr>
						<td class="box_bg">
							<table cellpadding="4" cellspacing="0" style="width: 100%; text-align: left;">
								<tr>
									<td class="CellBottomLine">
										<asp:label id="lblName" runat="server" text="Name:" cssclass="colonnaEtichetta" 
											meta:resourcekey="lblName"></asp:label>
									</td>
									<td class="CellBottomLine">
										<asp:label id="lblNameValue" runat="server" cssclass="colonnaDato"></asp:label>
									</td>
								</tr>
								<tr>
									<td class="CellBottomLine">
										<asp:label id="lblHost" runat="server" text="Host:" cssclass="colonnaEtichetta" 
											meta:resourcekey="lblHost"></asp:label>
									</td>
									<td class="CellBottomLine">
										<asp:label id="lblHostValue" runat="server" cssclass="colonnaDato"></asp:label>
									</td>
								</tr>
								<tr>
									<td class="CellBottomLine">
										<asp:label id="lblFullHostName" runat="server" text="Full Host Name:" 
											cssclass="colonnaEtichetta" meta:resourcekey="lblFullHostName"></asp:label>
									</td>
									<td class="CellBottomLine">
										<asp:label id="lblFullHostNameValue" runat="server" cssclass="colonnaDato"></asp:label>
									</td>
								</tr>
								<tr>
									<td class="CellBottomLine">
										<asp:label id="lblIP" runat="server" text="IP:" cssclass="colonnaEtichetta" 
											meta:resourcekey="lblIP"></asp:label>
									</td>
									<td class="CellBottomLine">
										<asp:label id="lblIPValue" runat="server" cssclass="colonnaDato"></asp:label>
									</td>
								</tr>
								<tr>
									<td class="CellBottomLine">
										<asp:label id="lblLastRequest" runat="server" text="Data Ultima Richiesta:" 
											cssclass="colonnaEtichetta" meta:resourcekey="lblLastRequest"></asp:label>
									</td>
									<td class="CellBottomLine">
										<asp:label id="lblLastRequestValue" runat="server" cssclass="colonnaDato"></asp:label>
									</td>
								</tr>
								<tr>
									<td class="CellBottomLine">
										<asp:label id="lblValidation" runat="server" text="Data Ultima Validazione:" 
											cssclass="colonnaEtichetta" meta:resourcekey="lblValidation"></asp:label>
									</td>
									<td class="CellBottomLine">
										<asp:label id="lblValidationValue" runat="server" cssclass="colonnaDato"></asp:label>
									</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: right; vertical-align: top;">
										<asp:label id="lblMessage" runat="server" text="Richiedi Validazione" 
											meta:resourcekey="lblMessage"></asp:label>&nbsp;&nbsp;
										<asp:imagebutton id="imgbValidate" runat="server" 
											alternatetext="Richiedi Validazione" imageurl="~/Images/valida_on.gif" 
											onclick="imgbValidate_Click" meta:resourcekey="imgbValidate" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<img alt="" src="Images/boxDX_bot.gif" style="vertical-align: top;" />
						</td>
					</tr>
				</table>		
				<br />	
			</td>
		</tr>
	</table>
</asp:content>

<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
    }
    
    void Application_End(object sender, EventArgs e) 
    {
    }
        
    void Application_Error(object sender, EventArgs e) 
    {
		string sMessage = "";
		string sStackTrace = "";

		Exception ex = this.Server.GetLastError();

		if ( ex.InnerException != null )
		{
			sMessage = ex.InnerException.Message != null ? ex.InnerException.Message : string.Empty;
			sStackTrace = ex.InnerException.Message != null ? ex.InnerException.StackTrace : string.Empty;
		}
		else
		{
			sMessage = ex.Message != null ? ex.Message : string.Empty;
			sStackTrace = ex.Message != null ? ex.StackTrace : string.Empty;
		}

		System.Diagnostics.Trace.TraceError("Error -> " + sMessage + ", Stack Trace -> " + sStackTrace);
	}

    void Session_Start(object sender, EventArgs e) 
    {
    }

    void Session_End(object sender, EventArgs e) 
    {
    }
       
</script>

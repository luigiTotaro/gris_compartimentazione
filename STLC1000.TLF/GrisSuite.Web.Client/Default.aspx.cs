using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Resources;
using GrisSuite.Common;

public partial class _Default : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//if ( this.Session != null && this.Session.IsNewSession )
		//{
		//    // Il resize avviene solo quando parte la sessione (la prima volta che l'utente avvia l'applicazione)
		//    ScriptManager.RegisterStartupScript(this, this.GetType(), "ResizeWindow", "window.resizeTo(900, 500);", true);
		//}
		this.imgUploadDownload.Style.Add("float", "left");
		if ( !Page.IsPostBack )
		{
			popolaTreeview(tvwDevices);
		}

		if ( Page.Request.QueryString["DevID"] != null )
		{
			Session["DevID"] = Page.Request.QueryString["DevID"];
			MultiView1.SetActiveView(View2);
		}

		if ( Session["EspandiFields"] == null )
			Session.Add("EspandiFields", false);

		this.imgbRefreshPage.Attributes["OnMouseOver"] = "this.src = 'Images/btnAggiorna_over.gif';";
		this.imgbRefreshPage.Attributes["OnMouseOut"] = "this.src = 'Images/btnAggiorna_default.gif';";
    }

	protected void Page_PreRender ( object sender, EventArgs e )
	{
		//forzo il caricamento dei dati!
		forceDatabind();

		//verifico se attivare o meno l'espansione automatica dei fields
		if ( Session["EspandiFields"].Equals(true) && GridViewStreams.SelectedIndex != -1 )
		{
			foreach ( DataListItem cpe in DataListFields.Items )
			{
				( (AjaxControlToolkit.CollapsiblePanelExtender)cpe.FindControl("cpe") ).Collapsed = false;
			}
		}

		if ( ( GridViewDev.Rows.Count == 0 ) & ( GridViewDevOff.Rows.Count == 0 ) & ( MultiView1.ActiveViewIndex == 0 ) )
		{
			lblNoData.Visible = true;
		}
		else
		{
			lblNoData.Visible = false;
		}
	}

	private void popolaTreeview ( TreeView tv )
	{
		//resetto il treeview
		tv.Nodes.Clear();

		//creo il dataset
		DataSet ds = new DataSet();

		string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
		using ( SqlConnection conn = new SqlConnection(connectionString) )
		{
			//preparo le query per le tabelle nel datset
			string queryString1 = SQLQueries.GetRegions; //"select regid, name from regions where removed=0";
			string queryString2 = SQLQueries.GetZones; //"select zonid, name, regid from zones where removed=0";
			string queryString3 = SQLQueries.GetNodes; //"select nodid, name, zonid from nodes where removed=0";
			string queryString4 = SQLQueries.GetDevices;

			try
			{
				//popolo il dataset con le tabelle            
				SqlDataAdapter regionAdapter = new SqlDataAdapter(queryString1, conn);
				regionAdapter.Fill(ds, "regions");

				SqlDataAdapter zoneAdapter = new SqlDataAdapter(queryString2, conn);
				zoneAdapter.Fill(ds, "zones");

				SqlDataAdapter nodeAdapter = new SqlDataAdapter(queryString3, conn);
				nodeAdapter.Fill(ds, "nodes");

				SqlDataAdapter deviceAdapter = new SqlDataAdapter(queryString4, conn);
				deviceAdapter.Fill(ds, "devices");
			}
			catch ( Exception ex )
			{
				System.Diagnostics.Trace.TraceError("Function: popolaTreeview(Popolo DATASET) - " + ex.Message);
			}
		}

		try
		{
			foreach ( DataRow regionRow in ds.Tables["regions"].Rows )
			{
				TreeNode root = new TreeNode();
				root.Value = regionRow.ItemArray[0].ToString();
				root.Text = troncateText(regionRow.ItemArray[1].ToString());
				root.ToolTip = regionRow.ItemArray[1].ToString();
				root.SelectAction = TreeNodeSelectAction.Expand;
				tv.Nodes.Add(root);

				foreach ( DataRow zoneRow in ds.Tables["zones"].Select("regid=" + root.Value) )
				{
					TreeNode zoneNode = new TreeNode();
					zoneNode.Value = zoneRow.ItemArray[0].ToString();
					zoneNode.Text = troncateText(zoneRow.ItemArray[1].ToString());
					zoneNode.ToolTip = zoneRow.ItemArray[1].ToString();
					zoneNode.SelectAction = TreeNodeSelectAction.Expand;
					root.ChildNodes.Add(zoneNode);

					foreach ( DataRow nodeRow in ds.Tables["nodes"].Select("zonid=" + zoneNode.Value) )
					{
						TreeNode nodeNode = new TreeNode();
						nodeNode.Value = nodeRow.ItemArray[0].ToString();
						nodeNode.Text = troncateText(nodeRow.ItemArray[1].ToString());
						nodeNode.ToolTip = nodeRow.ItemArray[1].ToString();
						nodeNode.SelectAction = TreeNodeSelectAction.Expand;
						zoneNode.ChildNodes.Add(nodeNode);

						foreach ( DataRow devRow in ds.Tables["devices"].Select("nodid=" + nodeNode.Value) )
						{
							TreeNode devNode = new TreeNode();
							devNode.Value = devRow.ItemArray[0].ToString();
							devNode.Text = troncateText(devRow.ItemArray[1].ToString());
							devNode.ToolTip = devRow.ItemArray[1].ToString();
							devNode.SelectAction = TreeNodeSelectAction.Select;

							if ( !Convert.IsDBNull(devRow["Status"]) && !Convert.IsDBNull(devRow["Active"]) )
							{
								int status = Convert.ToInt32(devRow["Status"]);
								byte active = Convert.ToByte(devRow["Active"]);

								if ( active == (byte)0 ) status = 9;
								devNode.ImageUrl = string.Format("Images/stato_{0}.gif", status);
							}

							nodeNode.ChildNodes.Add(devNode);
						}
					}
				}
			}
		}
		catch ( Exception ex )
		{
			System.Diagnostics.Trace.TraceError("Function: popolaTreeview(Expand TreeView) - " + ex.Message);
		}

		if ( this.tvwDevices.Nodes.Count > 0 ) this.tvwDevices.ExpandAll();
	}

	private void esplodoNodoPeriferica ( TreeView tv )
	{
		if ( Session["DevID"] != null )
		{
			string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
			try
			{
				using ( SqlConnection conn = new SqlConnection(connectionString) )
				{
					SqlCommand cmd = new SqlCommand("sWEB_GetDevPathbyDevID", conn);
					cmd.CommandType = CommandType.StoredProcedure;
					SqlParameter parameter = cmd.Parameters.Add("@DevID", SqlDbType.BigInt);
					parameter.Value = Session["DevID"].ToString();

					conn.Open();

					SqlDataReader reader = cmd.ExecuteReader();

					if ( reader.Read() )
					{
						tv.FindNode(reader[0].ToString()).Expand();
						tv.FindNode(reader[0].ToString() + "/"
									+ reader[1].ToString()).Expand();
						tv.FindNode(reader[0].ToString() + "/"
									+ reader[1].ToString() + "/"
									+ reader[2].ToString()).Expand();
						tv.FindNode(reader[0].ToString() + "/"
									+ reader[1].ToString() + "/"
									+ reader[2].ToString() + "/"
									+ reader[3].ToString()).Selected = true;
					}

					reader.Close();
					conn.Close();
				}
			}
			catch ( Exception ex )
			{
				System.Diagnostics.Trace.TraceError("Function: esplodoNodoPeriferica - " + ex.Message);
			}
		}

	}

	protected string testoAcapo ( object testo )
	{
		string tabellaStr = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Resources.GrismResources.NoMessage);

		//if (testo.ToString() != "" & testo != null )
		if ( testo.GetType().ToString() == "System.String" )
		{
			//verifico se � una stringa composta
			if ( testo.ToString().IndexOf(";") == -1 && testo.ToString().IndexOf("=") == -1 ) return testo.ToString();

			//estraggo il testo in una array
			string[] messages = ( (string)testo ).Split(new Char[] { ';' });

			//inserisco i messaggi ed il valore della severity su di una ArraList
			ArrayList msgList = new ArrayList();
			foreach ( string msg in messages )
			{
				Array temp = msg.Split(new char[] { '=' });
				msgList.Add(temp);
			}

			//intestazione tabella
			string str = "<table>";

			foreach ( string[] msg in msgList )
			{
				if ( msg.Length > 1 )
				{
					str += "<tr valign=\"middle\"><td><img alt=\"\" src=\"" + chkStatus(int.Parse(msg[1])).Substring(2) + "\" />" + msg[0] + "</td></tr>";
				}
			}

			//chiudo tabella
			str += "</table>";

			tabellaStr = str;
		}

		return tabellaStr;
	}

	private string getClassCSS ( string severity )
	{
		string cssClass = "";
		switch ( severity )
		{
			case "1":
				cssClass = "avviso";
				break;
			case "2":
				cssClass = "errore";
				break;
		}
		return cssClass;
	}

	protected string chkOffline ( object offline )
	{
		return ( offline.ToString() == "1" ) ? string.Format(" {0} ", Resources.GrismResources.Offline.ToUpper()) : "";
	}

	protected string chkStatus ( Int32 sevLevel )
	{
		string ok, warning, error, unknown;

		unknown = "~/Images/stato_255.gif";
		ok = "~/Images/stato_0.gif";
		warning = "~/Images/stato_1.gif";
		error = "~/Images/stato_2.gif";

		switch ( sevLevel )
		{
			case 1:
				return warning;
			case 2:
				return error;
			case 255:
				return unknown;
			default:
				return ok;
		}
	}

	protected string GetInterfaceDescription ( string portName, string address, string portType )
	{
		return (portName ?? "") + string.Format(" - {0}: ", Utility.CapitalizeFirstLetter(Resources.GrismResources.Address)) + formatAddress((address ?? ""), (portType ?? ""));
	}

	protected System.Drawing.Color getColorCode ( int level )
	{
		switch ( level )
		{
			case 0:
				return System.Drawing.Color.FromName("#00CC00");
			case 1:
				return System.Drawing.Color.FromName("#FFCC00");
			case 2:
				return System.Drawing.Color.FromName("#CC3333");
			case 255:
				return System.Drawing.Color.FromName("#CCCCCC");
			default:
				return System.Drawing.Color.Empty;
		}

	}

	protected void compilaLabel ()
	{
		if ( txtboxricerca.Text == "Telefin.Settings.Version" )
		{
			lblHideSearch.Text = GetAppVersion();
			lblHideSearch.Visible = true;
		}
		else
		{
			if ( txtboxricerca.Text == "" )
			{
				lblHideSearch.Text = Utility.CapitalizeFirstLetter(Resources.GrismResources.SupplySearchInput);
				lblHideSearch.Visible = true;
			}
			else
			{
				lblHideSearch.Visible = false;
				lblHideSearch.Text = "%" + txtboxricerca.Text + "%";
				odsGridViewSearch.DataBind();
			}
		}
	}

	protected string GetAppVersion ()
	{
		string version = System.Configuration.ConfigurationManager.AppSettings["Version"].ToString();
		if ( version != "" )
		{
			return version;
		}
		else
		{
			return Utility.CapitalizeFirstLetter(Resources.GrismResources.NoVersion);
		}

	}

	protected bool IsArray ( string DevID, string StrID, string FieldID )
	{
		string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
		bool result = false;
		using ( SqlConnection conn = new SqlConnection(connectionString) )
		{
			string query = SQLQueries.GetIsArray;
			query = query.Replace("DEVIDFIELD", DevID);
			query = query.Replace("STRIDFIELD", StrID);
			query = query.Replace("FIELDIDFIELD", FieldID);

			SqlCommand cmd = new SqlCommand(query, conn);
			conn.Open();
			SqlDataReader reader = cmd.ExecuteReader();

			if ( reader.Read() )
			{
				if ( reader[0].ToString().Equals("1") )
					result = true;
			}
		}

		return result;
	}

	protected string formatName ( object Name, object DevID, object StrID, object FieldID, object ArrayID )
	{
		string result = Name.ToString();
		if ( IsArray(DevID.ToString(), StrID.ToString(), FieldID.ToString()) )
		{
			result += " " + ( (int)( ArrayID ) + 1 ).ToString();
		}

		return result;
	}

	protected string formatValue ( object value, int chunk )
	{
		string result;
		int count = 0;
		int newchunk = chunk;
		if ((((string)value).Length > chunk) && (chunk > 0))
		{
			result = "<div class=\"DivValue\">";
			while ( count < ( (string)value ).Length )
			{
				result += ( (string)value ).Substring(count, newchunk) + "<br />";
                count += chunk;
                if ((((string)value).Length - count) < chunk)
					newchunk = ( ( (string)value ).Length - count );
			}
			result += "</div>";
		}
		else
		{
			result = value.ToString();
		}
		return result;
	}

	protected void forceDatabind ()
	{
		DataListFields.DataBind();
		FormDettaglio.DataBind();
		GridViewDev.DataBind();
		GridViewDevOff.DataBind();
		GridViewStreams.DataBind();
		GridViewSearch.DataBind();

		popolaTreeview(tvwDevices);
		esplodoNodoPeriferica(tvwDevices);

		//meccanismo per nascondere il bottone (nell'evento DataListFields_PreRender veniva abilitato troppo tardi)
		if ( DataListFields.Items.Count == 0 )
		{
			imgAutomaticExpansion.Visible = false;
		}
		else
		{
			imgAutomaticExpansion.Visible = true;
		}
	}

	protected void resetSession ()
	{
		Session.Remove("DevID");
		Session.Remove("StrID");

		GridViewStreams.SelectedIndex = -1;
	}

	protected void tvwDevices_SelectedNodeChanged ( object sender, EventArgs e )
	{
		if ( tvwDevices.SelectedNode.Depth == 3 )
		{
			resetSession();
			Session["DevID"] = tvwDevices.SelectedNode.Value.ToString();
			MultiView1.SetActiveView(View2);
		}
	}

	protected void TextBox1_TextChanged ( object sender, EventArgs e )
	{
		compilaLabel();
		MultiView1.SetActiveView(View3);
	}

	protected void Button1_Click ( object sender, EventArgs e )
	{
		compilaLabel();
		MultiView1.SetActiveView(View3);
	}

	protected void lnkbProblematicDevices_Click ( object sender, EventArgs e )
	{
		MultiView1.SetActiveView(View1);
	}

	protected void lnkbDevices_Click ( object sender, EventArgs e )
	{
		MultiView1.SetActiveView(View2);
		esplodoNodoPeriferica(tvwDevices);
	}

	protected void View1_Activate ( object sender, EventArgs e )
	{
		tdProblematicDevices.Attributes["class"] = "MenuItemSelectedBackground";
		lnkbProblematicDevices.CssClass = "MenuItemSelected";
		lblBoxDx.Text = Resources.GrismResources.DeviceListTitle;
	}

	protected void View1_Deactivate ( object sender, EventArgs e )
	{
		tdProblematicDevices.Attributes["class"] = "MenuItemBackground";
		lnkbProblematicDevices.CssClass = "MenuItem";
	}

	protected void View2_Activate ( object sender, EventArgs e )
	{
		tdDevices.Attributes["class"] = "MenuItemSelectedBackground";
		lnkbDevices.CssClass = "MenuItemSelected";
		lblBoxDx.Text = Utility.CapitalizeFirstLetter(Resources.GrismResources.NoSelectedDevice);
	}

	protected void View2_Deactivate ( object sender, EventArgs e )
	{
		tdDevices.Attributes["class"] = "MenuItemBackground";
		lnkbDevices.CssClass = "MenuItem";
		resetSession();
	}

	protected void View3_Activate ( object sender, EventArgs e )
	{
		lblBoxDx.Text = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Resources.GrismResources.SearchResult);
	}

	protected void GridViewStreams_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row.RowType == DataControlRowType.DataRow )
		{
			object dataItem = e.Row.DataItem;
			LinkButton imgbtn = (LinkButton)e.Row.Cells[3].Controls[0];
			imgbtn.CommandArgument = DataBinder.Eval(dataItem, "DevID").ToString() + "|"
				+ DataBinder.Eval(dataItem, "StrID").ToString() + "|"
				+ e.Row.RowIndex.ToString();
		}

	}

	protected void GridViewStreams_RowCommand ( object sender, GridViewCommandEventArgs e )
	{
		string[] temp = e.CommandArgument.ToString().Split('|');
		Session["DevID"] = temp[0];
		Session["StrID"] = temp[1];

		( (GridView)sender ).SelectedIndex = int.Parse(temp[2]);
	}

	protected void GridViewDevOff_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row.RowType == DataControlRowType.DataRow )
		{
			object dataItem = e.Row.DataItem;
			LinkButton imgbtn = (LinkButton)e.Row.Cells[3].Controls[0];
			imgbtn.CommandArgument = DataBinder.Eval(dataItem, "DevID").ToString();
		}
	}

	protected void GridViewDev_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row.RowType == DataControlRowType.DataRow )
		{
			object dataItem = e.Row.DataItem;
			int sevLevel = (int)DataBinder.Eval(dataItem, "SevLevel");
			LinkButton imgbtn = (LinkButton)e.Row.Cells[3].Controls[0];

			Image imgStatus = (Image)e.Row.FindControl("imgStatus");
			imgStatus.ImageUrl = chkStatus(sevLevel);

			imgbtn.CommandArgument = DataBinder.Eval(dataItem, "DevID").ToString();
		}
	}

	protected void GridViewSearch_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row.RowType == DataControlRowType.DataRow )
		{
			object dataItem = e.Row.DataItem;
			LinkButton imgbtn = (LinkButton)e.Row.Cells[3].Controls[0];
			imgbtn.CommandArgument = DataBinder.Eval(dataItem, "DevID").ToString();
		}
	}

	protected void GridViewDevOff_RowCommand ( object sender, GridViewCommandEventArgs e )
	{
		Session["DevID"] = e.CommandArgument;
		MultiView1.SetActiveView(View2);
		esplodoNodoPeriferica(tvwDevices);
	}

	protected void GridViewDev_RowCommand ( object sender, GridViewCommandEventArgs e )
	{
		Session["DevID"] = e.CommandArgument;
		MultiView1.SetActiveView(View2);
		esplodoNodoPeriferica(tvwDevices);
	}

	protected void GridViewSearch_RowCommand ( object sender, GridViewCommandEventArgs e )
	{
		Session["DevID"] = e.CommandArgument;
		MultiView1.SetActiveView(View2);
		esplodoNodoPeriferica(tvwDevices);
	}

	protected void imgAutomaticExpansion_Click ( object sender, ImageClickEventArgs e )
	{
		if ( Session["EspandiFields"] != null )
		{
			if ( Session["EspandiFields"].Equals(false) )
			{
				Session["EspandiFields"] = true;
				( (ImageButton)sender ).AlternateText = Utility.CapitalizeFirstLetter(Resources.GrismResources.DisableAutomaticExpansion);
				( (ImageButton)sender ).ImageUrl = "~/Images/box2_btn_contrai.gif";
			}
			else
			{
				Session["EspandiFields"] = false;
				( (ImageButton) sender ).AlternateText = Utility.CapitalizeFirstLetter(Resources.GrismResources.EnableAutomaticExpansion);
				( (ImageButton)sender ).ImageUrl = "~/Images/box2_btn_espandi.gif";
			}
		}
	}

	protected void btnRicercaCancel_Click ( object sender, EventArgs e )
	{
		txtboxricerca.Text = "";
		lblHideSearch.Text = "";
		MultiView1.SetActiveView(View1);
	}

	protected void View3_Deactivate ( object sender, EventArgs e )
	{
		txtboxricerca.Text = string.Empty;
	}

	protected void GridViewStreams_PreRender ( object sender, EventArgs e )
	{
		if ( ( (GridView)sender ).Rows.Count > 0 )
			lblStrList.Visible = true;
		else
			lblStrList.Visible = false;
	}

	protected void FormDettaglio_PreRender ( object sender, EventArgs e )
	{
		if ( ( (FormView)sender ).Row != null )
			lblBoxDx.Text = Utility.CapitalizeFirstLetter(Resources.GrismResources.DeviceDetail);
	}

	protected void imgbRefreshPage_Click ( object sender, ImageClickEventArgs e )
	{
		this.imgbRefreshPage.ImageUrl = "~/Images/btnAggiorna_ok.gif";

		popolaTreeview(tvwDevices);
		this.forceDatabind();
	}

	protected string formatAddress ( string addr, string portType )
	{
		string result = addr;
		if ( portType.Equals("TCP Client") )
		{
			//estraggo la stringa IP dalla codifica del DB

			UInt32 ipNum = UInt32.Parse(addr);
			string IP = "";
			IP = ( (UInt32)( ipNum >> 24 ) ).ToString() + ".";
			UInt32 mask = 0x00FFFFFF;
			IP += ( (UInt32)( ( ipNum & mask ) >> 16 ) ).ToString() + ".";
			mask = 0x0000FFFF;
			IP += ( (UInt32)( ( ipNum & mask ) >> 8 ) ).ToString() + ".";
			mask = 0x000000FF;
			IP += ( (UInt32)( ipNum & mask ) ).ToString();

			result = IP;
		}

		return result;
	}

	protected string troncateText ( string text )
	{
		string result = text;
		if ( text.Length > 25 )
			result = text.Substring(0, 25) + " ...";

		return result;
	}

	protected void imgUploadDownload_Click ( object sender, ImageClickEventArgs e )
	{
		this.Response.Redirect("~/FileUpload.aspx");
	}
}

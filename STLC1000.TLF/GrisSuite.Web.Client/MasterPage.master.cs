using System;
using System.Configuration;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using Resources;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public string NodeName
    {
        get { return (this.ViewState["NodeName"] ?? "").ToString(); }
        set { this.ViewState["NodeName"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlRfi.Style["background-repeat"] = "no-repeat";

        //onmouseover immagini menu di navigazione
        string imgsExcludedFilters = "this.src.indexOf('_selez') == -1";
        //string imgsExcludedAlertsFilters = "this.src.indexOf('_selez') == -1 && this.src.indexOf('_active') == -1";
        string scriptStatusOver = "if ( {0} ) this.src = this.src.replace('.gif', '_over.gif');";
        string scriptStatusOut = "if ( {0} ) this.src = this.src.replace('_over.gif', '.gif');";

        this.imgDevices.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
        this.imgDevices.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);
        this.imgReports.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
        this.imgReports.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);
        this.imgValidation.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
        this.imgValidation.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);

        if (this.Session["SelectedInterface"] != null)
        {
            this.SelectInterface(this.Session["SelectedInterface"].ToString(), false);
        }

        if (this.WindowsSecurityEnable())
        {
            if ((!this.Page.User.IsInRole("STLC1000SWMUsers")) && (!this.Page.User.IsInRole("STLC1000SWMAdmins")) &&
                (!this.Page.User.IsInRole("BUILTIN\\Users")))
            {
                this.Page.Response.Redirect("UserMessage.aspx");
            }

            if (this.Page.User.IsInRole("STLC1000SWMAdmins") || this.Page.User.IsInRole("BUILTIN\\Users"))
            {
                this.LoginName1.ForeColor = System.Drawing.Color.FromArgb(255, 180, 105);
                this.lblVersion.Visible = true;
            }
            else
            {
                this.lblVersion.Visible = false;
            }
        }

        if (!this.IsPostBack)
        {
            if (this.NodeName.Length == 0)
            {
                dsSTLCServersTableAdapters.serversTableAdapter server = new dsSTLCServersTableAdapters.serversTableAdapter();
                this.NodeName = server.GetServerPhysicalLocation();
            }

            this.lblVersion.Text =
                Utility.CapitalizeFirstLetter(string.Format(GrismResources.VersionTemplate,
                                                            ConfigurationManager.AppSettings["Version"] ?? GrismResources.NotAvailable));
            this.lblHostName.Text = Utility.GetFullHostName();
        }

        this.Page.Title = string.Format(GrismResources.TitleTemplate,
                                        this.NodeName.Length == 0
                                            ? string.Format("{0} {1}", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(GrismResources.Station),
                                                            GrismResources.NotAvailable)
                                            : this.NodeName);
    }

    protected bool WindowsSecurityEnable()
    {
        string WindowsSecurity = System.Configuration.ConfigurationManager.AppSettings["WindowsSecurity"];
        bool chk = false;
        if (WindowsSecurity.ToLower() == "true")
        {
            chk = true;
        }
        return chk;
    }

    protected void imgDevices_Click(object sender, ImageClickEventArgs e)
    {
        this.SelectInterface("devices", true);
    }

    protected void imgReports_Click(object sender, ImageClickEventArgs e)
    {
        this.SelectInterface("reports", true);
    }

    protected void imgValidation_Click(object sender, ImageClickEventArgs e)
    {
        this.SelectInterface("validate", true);
    }

    private string DefaultCulture
    {
        get
        {
            return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultUICulture"])
                        ? PageBase.DEFAULT_LANGUAGE
                        : ConfigurationManager.AppSettings["DefaultUICulture"].Trim());
        }
    }

    private void SelectInterface(string interfaceName, bool performRedirect)
    {
        this.imgDevices.ImageUrl = this.imgDevices.ImageUrl.Replace("_selez", "");
        this.imgReports.ImageUrl = this.imgReports.ImageUrl.Replace("_selez", "");
        this.imgValidation.ImageUrl = this.imgValidation.ImageUrl.Replace("_selez", "");

        this.Session["SelectedInterface"] = interfaceName;

        switch (interfaceName)
        {
            case "devices":
                {
                    this.imgDevices.ImageUrl = this.imgDevices.ImageUrl.Replace(".gif", "_selez.gif");
                    if (performRedirect)
                    {
                        string currentLanguage = PageBase.GetParamValue(this.Request.Url.AbsoluteUri, PageBase.PARAM_NAME_LANGUAGE);
                        if (!string.IsNullOrEmpty(currentLanguage))
                        {
                            this.Response.Redirect(string.Format("~/Default.aspx?{0}={1}", PageBase.PARAM_NAME_LANGUAGE, currentLanguage));
                        }
                        else
                        {
                            this.Response.Redirect(string.Format("~/Default.aspx?{0}={1}", PageBase.PARAM_NAME_LANGUAGE, this.DefaultCulture));
                        }
                    }
                    break;
                }
            case "reports":
                {
                    this.imgReports.ImageUrl = this.imgReports.ImageUrl.Replace(".gif", "_selez.gif");
                    if (performRedirect)
                    {
                        string currentLanguage = PageBase.GetParamValue(this.Request.Url.AbsoluteUri, PageBase.PARAM_NAME_LANGUAGE);
                        if (!string.IsNullOrEmpty(currentLanguage))
                        {
                            this.Response.Redirect(string.Format("~/Reports.aspx?{0}={1}", PageBase.PARAM_NAME_LANGUAGE, currentLanguage));
                        }
                        else
                        {
                            this.Response.Redirect(string.Format("~/Reports.aspx?{0}={1}", PageBase.PARAM_NAME_LANGUAGE, this.DefaultCulture));
                        }
                    }
                    break;
                }
            case "validate":
                {
                    this.imgValidation.ImageUrl = this.imgValidation.ImageUrl.Replace(".gif", "_selez.gif");
                    if (performRedirect)
                    {
                        string currentLanguage = PageBase.GetParamValue(this.Request.Url.AbsoluteUri, PageBase.PARAM_NAME_LANGUAGE);
                        if (!string.IsNullOrEmpty(currentLanguage))
                        {
                            this.Response.Redirect(string.Format("~/ConfigurationValidation.aspx?{0}={1}", PageBase.PARAM_NAME_LANGUAGE,
                                                                 currentLanguage));
                        }
                        else
                        {
                            this.Response.Redirect(string.Format("~/ConfigurationValidation.aspx?{0}={1}", PageBase.PARAM_NAME_LANGUAGE,
                                                                 this.DefaultCulture));
                        }
                    }
                    break;
                }
        }
    }

    protected void cboCurrentCulture_SelectedIndexChanged(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Redirect(PageBase.ComposeUrlByParam(this.Request.Url.AbsoluteUri, PageBase.PARAM_NAME_LANGUAGE,
                                                                         this.cboCurrentCulture.SelectedValue));
    }

    protected void cboCurrentCulture_PreRender(object sender, EventArgs e)
    {
        this.cboCurrentCulture.SelectedIndexChanged -= this.cboCurrentCulture_SelectedIndexChanged;

        string currentLanguage = PageBase.GetParamValue(this.Request.Url.AbsoluteUri, PageBase.PARAM_NAME_LANGUAGE);
        if (!string.IsNullOrEmpty(currentLanguage))
        {
            ListItem li = this.cboCurrentCulture.Items.FindByValue(currentLanguage);
            if (li != null)
            {
                li.Selected = true;
            }
        }
        else
        {
            ListItem li = this.cboCurrentCulture.Items.FindByValue(this.DefaultCulture);
            if (li != null)
            {
                li.Selected = true;
            }
        }

        this.cboCurrentCulture.SelectedIndexChanged += this.cboCurrentCulture_SelectedIndexChanged;
    }
}
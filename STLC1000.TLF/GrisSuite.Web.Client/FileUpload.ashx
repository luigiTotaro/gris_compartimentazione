﻿<%@ WebHandler Language="C#" Class="FileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Configuration;

public class FileUpload : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
		string configPath = ConfigurationManager.AppSettings["UploadedFiles"] ?? "UploadedFiles";
		string fullFolderPath = ( Path.IsPathRooted(configPath) ) ? configPath : context.Server.MapPath(configPath);

		if ( !Directory.Exists(fullFolderPath) ) Directory.CreateDirectory(fullFolderPath);

		if ( context.Request.Files != null && context.Request.Files.Count > 0 )
		{
			string fileName = Path.GetFileName(context.Request.Files[0].FileName);

			try
			{
				context.Request.Files[0].SaveAs(Path.Combine(fullFolderPath, fileName));
				context.Response.ContentType = "text/plain"; 
				context.Response.Write("success");	
			}
			catch ( Exception exc )
			{
				System.Diagnostics.Trace.TraceError("Upload file error -> " + exc.Message + ", Stack Trace -> " + exc.StackTrace);
			}
		}
	}
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
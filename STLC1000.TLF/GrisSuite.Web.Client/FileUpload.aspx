﻿<%@ page title="" language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true"
	codefile="FileUpload.aspx.cs" inherits="FileUpload" %>

<asp:content id="cntMainArea" contentplaceholderid="cphMainArea" runat="Server">
	<table cellpadding="0" cellspacing="0" style="width: 800px; text-align: left;">
		<tr style="height: 20px">
			<td colspan="3" class="nav_shadow_bg">
			</td>
		</tr>
	</table>
	<!-- We will use version hosted by Google-->
	<script src="Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="Scripts/ajaxupload.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var button = $('#btnUpload'), interval;

			new AjaxUpload(button, {
				action: 'FileUpload.aspx',
				name: 'userFile',
				onSubmit: function (file, ext) {
					button.text('Uploading');

					this.disable();

					// Uploding -> Uploading. -> Uploading...
					interval = window.setInterval(function () {
						var text = button.text();
						if (text.length < 13) {
							button.text(text + '.');
						} else {
							button.text('Uploading');
						}
					}, 200);
				},
				onComplete: function (file, response) {
					button.text('Upload');

					window.clearInterval(interval);

					this.enable();

					__doPostBack('updFiles', '');
				}
			});
		});   /*]]>*/
	</script>
	<div id="divMainArea" style="margin-top: 20px; margin-bottom: 20px;">
		<asp:updatepanel id="updFiles" runat="server" updatemode="Conditional">
			<contenttemplate>
				<asp:datalist id="dlstFiles" runat="server" repeatdirection="Vertical" itemstyle-horizontalalign="Left" width="90%" onitemdatabound="dlstFiles_ItemDataBound">
					<itemstyle />
					<itemtemplate>
						<img alt='<%# Eval("Name") %>' src="Images/u_doc.png" /><asp:hyperlink id="lnkFile" runat="server" text='<%# Eval("Name") %>' cssclass="UploadedFileName"></asp:hyperlink>
					</itemtemplate>
				</asp:datalist>
			</contenttemplate>
		</asp:updatepanel>
		<div id="divUploader">
			<div class="wrapper">
				<div id="btnUpload" class="updButton">
					Upload</div>
			</div>
		</div>
		<div id="divError" style="margin-top: 10px;">
			<asp:label id="lblError" runat="server" forecolor="Red"></asp:label>
		</div>
	</div>
</asp:content>

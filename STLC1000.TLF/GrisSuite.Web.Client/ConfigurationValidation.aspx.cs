using System;
using System.Web.UI;

using GrisSuite.Common;
using System.Resources;
using System.Reflection;

public partial class ConfigurationValidation : PageBase
{
	private dsSTLCServers.serversRow _server;

    protected void Page_Load(object sender, EventArgs e)
    {
		if ( this.GetServer() )
		{
			this.InitializeFields();
		}
    }

	protected void imgbValidate_Click ( object sender, EventArgs e )
	{
		DateTime dateRequested = DateTime.Now;

		if ( this.GetServer() )
		{
			CentralWS.Central centralWS = new CentralWS.Central();
			centralWS.Credentials = System.Net.CredentialCache.DefaultCredentials;

			try
			{
				SystemValidationEnum reqReceived = (SystemValidationEnum)centralWS.SendValidationRequest(this._server.SrvID, dateRequested);

				switch ( reqReceived )
				{
					case SystemValidationEnum.RequestCorrectlyReceived:
						{
							new dsSTLCServersTableAdapters.serversTableAdapter().UpdateDateValidationRequested(dateRequested, this._server.SrvID);
							this._server.ClientDateValidationRequested = dateRequested;
							this._server.AcceptChanges();
							this.InitializeFields();
							break;
						}
					case SystemValidationEnum.ServerNotFound:
						{
							this.lblMessage.Text = this.GetLocalResourceObject("ValidationRequestError_LocalServerNotFound").ToString();
							break;
						}
					case SystemValidationEnum.SqlError:
						{
							this.lblMessage.Text = this.GetLocalResourceObject("ValidationRequestError_SqlError").ToString();
							break;
						}
				}
			}
			catch ( System.Net.WebException exc )
			{
				System.Diagnostics.Trace.TraceError("Error -> " + exc.Message + ", Stack Trace -> " + exc.StackTrace);
				this.lblMessage.Text = this.GetLocalResourceObject("ValidationRequestError_ServerNotFound").ToString();
			}
		}
	}

	private bool GetServer ()
	{
		dsSTLCServers.serversDataTable servers = new dsSTLCServersTableAdapters.serversTableAdapter().GetData();

		if ( servers.Count > 0 )
		{
			this._server = servers[0];
			return true;
		}
		else
		{
			this.lblMessage.Text = this.GetLocalResourceObject("NotConfiguredDevices").ToString();
		}

		return false;
	}

	private void InitializeFields ()
	{
		if ( this._server != null )
		{
			DateTime requested;
			DateTime obtained;

			this.imgbValidate.Style[HtmlTextWriterStyle.PaddingRight] = "5px";

			this.lblNameValue.Text = string.IsNullOrEmpty(this._server.Name) ? "&nbsp;" : this._server.Name;
			this.lblHostValue.Text = string.IsNullOrEmpty(this._server.Host) ? "&nbsp;" : this._server.Host;
			this.lblFullHostNameValue.Text = string.IsNullOrEmpty(this._server.FullHostName) ? "&nbsp;" : this._server.FullHostName;
			this.lblIPValue.Text = string.IsNullOrEmpty(this._server.IP) ? "&nbsp;" : this._server.IP;

			if ( this._server.IsClientDateValidationRequestedNull() )
			{
				requested = new DateTime(1900, 1, 1);
				this.lblLastRequestValue.Text = "&nbsp;";
			}
			else
			{
				requested = this._server.ClientDateValidationRequested;
				this.lblLastRequestValue.Text = requested.ToString("g");
			}

			if ( this._server.IsClientDateValidationObtainedNull() )
			{
				obtained = new DateTime(1900, 1, 2);
				this.lblValidationValue.Text = "&nbsp;";
			}
			else
			{
				obtained = this._server.ClientDateValidationObtained;
				this.lblValidationValue.Text = obtained.ToString("g");
			}

			if ( obtained > requested )
			{
				if ( !this._server.IsSupervisorSystemXMLNull() && 
					!this._server.IsClientSupervisorSystemXMLValidatedNull() && 
					this._server.SupervisorSystemXML == this._server.ClientSupervisorSystemXMLValidated )
				{
					// il system.xml in uso � uguale al system.xml validato
					this.lblMessage.ForeColor = System.Drawing.Color.Green;
					this.lblMessage.Text = this.GetLocalResourceObject("ConfigurationValidated").ToString();
					this.imgbValidate.ImageUrl = "~/Images/valida_ok.gif";
					this.imgbValidate.Enabled = false;
					this.imgbValidate.Attributes["OnMouseOver"] = "";
					this.imgbValidate.Attributes["OnMouseOut"] = "";
					this.imgbValidate.Style[HtmlTextWriterStyle.Cursor] = "default";
				}
				else
				{
					// il system.xml in uso non � uguale al system.xml validato o non � mai stata inoltrata richiesta di validazione
					this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(255, 255, 255);
					this.lblMessage.Text = this.GetLocalResourceObject("RequestValidation").ToString();
					this.imgbValidate.ImageUrl = "~/Images/valida_on.gif";
					this.imgbValidate.Enabled = true;
					this.imgbValidate.Attributes["OnMouseOver"] = string.Format("this.src = 'Images/valida_over.gif'; $get('{0}').style.color = '#0885cc';", this.lblMessage.ClientID);
					this.imgbValidate.Attributes["OnMouseOut"] = string.Format("this.src = 'Images/valida_on.gif'; $get('{0}').style.color = '#ffffff';", this.lblMessage.ClientID);
				}
			}
			else
			{
				// una richiesta � gi� stata inoltrata
				this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(8, 133, 204); //#0885cc
				this.lblMessage.Text = this.GetLocalResourceObject("RequestPresented").ToString();
				this.imgbValidate.ImageUrl = "~/Images/valida_off.gif";
				this.imgbValidate.Enabled = true;
				this.imgbValidate.Attributes["OnMouseOver"] = "this.src = 'Images/valida_over.gif';";
				this.imgbValidate.Attributes["OnMouseOut"] = "this.src = 'Images/valida_off.gif';";
			}
		}
		else 
		{
			this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(76, 76, 76); //#4c4c4c
			this.lblMessage.Text = this.GetLocalResourceObject("LocalServerNotConfigured").ToString();
			this.imgbValidate.ImageUrl = "~/Images/valida_off.gif";
			this.imgbValidate.Enabled = false;
			this.imgbValidate.Attributes["OnMouseOver"] = "";
			this.imgbValidate.Attributes["OnMouseOut"] = "";
		}
	}
}

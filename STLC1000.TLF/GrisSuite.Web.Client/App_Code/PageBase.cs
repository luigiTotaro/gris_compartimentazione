﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;

public class PageBase : System.Web.UI.Page
{
    public const string DEFAULT_LANGUAGE = "en-US";
    public const string PARAM_NAME_LANGUAGE = "L";
    private string uiCulture = DEFAULT_LANGUAGE;

    protected override void InitializeCulture()
    {
        base.InitializeCulture();

        string languageParam = this.Request.QueryString[PARAM_NAME_LANGUAGE];

        if (string.IsNullOrEmpty(languageParam))
        {
            languageParam = this.DefaultCulture;
        }

        this.SetLanguage(languageParam);

        this.UICulture = this.uiCulture;
        System.Globalization.CultureInfo customCulture = new System.Globalization.CultureInfo(this.uiCulture, false);
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
    }

    public string DefaultCulture
    {
        get
        {
            return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultUICulture"])
                        ? DEFAULT_LANGUAGE
                        : ConfigurationManager.AppSettings["DefaultUICulture"].Trim());
        }
    }

    public string LanguageCode
    {
        get { return this.uiCulture; }
        set { this.uiCulture = value; }
    }

    private void SetLanguage(string code)
    {
        if (string.IsNullOrEmpty(code))
        {
            this.uiCulture = this.DefaultCulture;
        }
        else
        {
            if (code.Equals("it", StringComparison.OrdinalIgnoreCase) || code.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
            {
                this.uiCulture = "it-IT";
            }
            else if (code.Equals("en", StringComparison.OrdinalIgnoreCase) || code.Equals("en-US", StringComparison.OrdinalIgnoreCase))
            {
                this.uiCulture = "en-US";
            }
            else if (code.Equals("hr", StringComparison.OrdinalIgnoreCase) || code.Equals("hr-HR", StringComparison.OrdinalIgnoreCase))
            {
                this.uiCulture = "hr-HR";
            }
            else
            {
                this.uiCulture = this.DefaultCulture;
            }
        }
    }

    public static string RemoveParam(string inputString, string paramName)
    {
        return Regex.Replace(inputString, @"(?:&|\?)" + paramName + @"=.*?(&|\?|$)", "${1}", RegexOptions.IgnoreCase);
    }

    public static string GetParamValue(string inputString, string paramName)
    {
        Match paramMatch = Regex.Match(inputString, @"(?:&|\?)" + paramName + @"=(?<value>.*?)(&|\?|$)");
        if (paramMatch.Groups["value"] != null)
        {
            return paramMatch.Groups["value"].Value;
        }

        return string.Empty;
    }

    public static string GetUrlParamSeparator(string url)
    {
        if ((url.IndexOf(".aspx?", StringComparison.OrdinalIgnoreCase) >= 0) || (url.IndexOf(".ascx?", StringComparison.OrdinalIgnoreCase) >= 0))
        {
            return "&";
        }

        return "?";
    }

    public static string ComposeUrlByParam(string inputString, string paramName, string paramValue)
    {
        return string.Format("{0}{1}{2}={3}", RemoveParam(inputString, paramName), GetUrlParamSeparator(RemoveParam(inputString, paramName)),
                             paramName, HttpUtility.UrlEncode(paramValue));
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web.UI;
using BadDevListDSTableAdapters;
using DevDetailsbyDevIdDSTableAdapters;
using FieldsbyDevIdStrIdDSTableAdapters;
using FindDevDSTableAdapters;
using OfflineDevListDSTableAdapters;
using StationStatusTableAdapters;
using StreamsByDevIdDSTableAdapters;
using StreamsDataTableAdapters;

public class DataSetLocalizer : TemplateControl
{
    private readonly Regex regexResourceData = new Regex("(?<resourceKey>(?<className>.+?)_(?:[a-z]|[A-Z]|[0-9]|_)+)",
                                                         RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.Compiled);

    public BadDevListDS.sWEB_GetBadDevListDataTable GetBadDevListLocalizedData()
    {
        sWEB_GetBadDevListTableAdapter ta = new sWEB_GetBadDevListTableAdapter();
        BadDevListDS.sWEB_GetBadDevListDataTable dt = ta.GetData();

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    public StreamsByDevIdDS.sWEB_GetStreamsbyDevIDDataTable GetStreamsByDevIdLocalizedData(long? devId)
    {
        sWEB_GetStreamsbyDevIDTableAdapter ta = new sWEB_GetStreamsbyDevIDTableAdapter();
        StreamsByDevIdDS.sWEB_GetStreamsbyDevIDDataTable dt = ta.GetData(devId);

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    public DevDetailsbyDevIdDS.sWEB_GetDevDetailsbyDevIDDataTable GetDevDetailsbyDevIdLocalizedData(long? devId)
    {
        sWEB_GetDevDetailsbyDevIDTableAdapter ta = new sWEB_GetDevDetailsbyDevIDTableAdapter();
        DevDetailsbyDevIdDS.sWEB_GetDevDetailsbyDevIDDataTable dt = ta.GetData(devId);

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    public FieldsbyDevIdStrIdDS.sWEB_GetFieldsbyDevID_StrIDDataTable GetFieldsbyDevIdStrIdLocalizedData(long? devId, int? strId)
    {
        sWEB_GetFieldsbyDevID_StrIDTableAdapter ta = new sWEB_GetFieldsbyDevID_StrIDTableAdapter();
        FieldsbyDevIdStrIdDS.sWEB_GetFieldsbyDevID_StrIDDataTable dt = ta.GetData(devId, strId);

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    public FindDevDS.sWEB_FindDevDataTable FindDevLocalizedData(string chiave)
    {
        sWEB_FindDevTableAdapter ta = new sWEB_FindDevTableAdapter();
        FindDevDS.sWEB_FindDevDataTable dt = ta.GetData(chiave);

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    public OfflineDevListDS.sWEB_GetOffDevListDataTable GetOffDevListLocalizedData()
    {
        sWEB_GetOffDevListTableAdapter ta = new sWEB_GetOffDevListTableAdapter();
        OfflineDevListDS.sWEB_GetOffDevListDataTable dt = ta.GetData();

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    public StreamsData GetStreamsDataByDevIdLocalizedData(long devId)
    {
        StreamsData ds = new StreamsData();
        sWEB_Report_GetStreamsDataByDevIDTableAdapter ta = new sWEB_Report_GetStreamsDataByDevIDTableAdapter();
        ta.Fill(ds.sWEB_Report_GetStreamsDataByDevID, devId);

        this.LocalizeDatasetByData(ds.sWEB_Report_GetStreamsDataByDevID);

        return ds;
    }

    public StationStatus.sWEB_Report_GetStationStatusDataTable GetStationStatusLocalizedData()
    {
        sWEB_Report_GetStationStatusTableAdapter ta = new sWEB_Report_GetStationStatusTableAdapter();
        StationStatus.sWEB_Report_GetStationStatusDataTable dt = ta.GetData();

        this.LocalizeDatasetByData(dt);

        return dt;
    }

    private void LocalizeDatasetByData(DataTable dt)
    {
        if ((dt != null) && (dt.Rows.Count > 0))
        {
            foreach (DataRow dr in dt.Rows)
            {
                for (int columnCounter = 0; columnCounter < dt.Columns.Count; columnCounter++)
                {
                    string data = dr[columnCounter] as string;

                    if (data != null)
                    {
                        string[] dataArray = data.Split(';');

                        foreach (string dataElement in dataArray)
                        {
                            Match m = this.regexResourceData.Match(dataElement);

                            if (m.Groups.Count == 3)
                            {
                                string className = m.Groups["className"].Value;
                                string resourceKey = m.Groups["resourceKey"].Value;

                                if ((!string.IsNullOrEmpty(className)) && (!string.IsNullOrEmpty(resourceKey)))
                                {
                                    string localizedData = null;
                                    try
                                    {
                                        localizedData = this.GetGlobalResourceObject(className, resourceKey) as string;
                                    }
                                    catch (MissingManifestResourceException)
                                    {}

                                    if (localizedData != null)
                                    {
                                        try
                                        {
                                            dr[columnCounter] = ((string)dr[columnCounter]).Replace(dataElement,
                                                                                                    this.FormatData(dataElement.Replace(resourceKey,
                                                                                                                                        localizedData)));

                                            Debug.WriteLine(string.Format("Translated '{0}' into '{1}', inside '{2}'", resourceKey, localizedData,
                                                                          dataElement));
                                        }
                                        catch (ArgumentException)
                                        {}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private string FormatData(string dataElement)
    {
        string isolatedData = dataElement;
        string severityValue = string.Empty;

        if (dataElement.IndexOf("=") > 0)
        {
            isolatedData = dataElement.Substring(0, dataElement.LastIndexOf('='));
            severityValue = dataElement.Substring(dataElement.LastIndexOf('='));
        }

        List<object> parameters = new List<object>();
        string[] values = isolatedData.Split('|');

        if (values.Length < 2)
        {
            return dataElement;
        }

        for (int paramCounter = 1; paramCounter < values.Length; paramCounter++)
        {
            parameters.Add(values[paramCounter]);
        }

        string returnValue = string.Format("UNABLE TO FORMAT DATA FROM: {0}", dataElement);

        try
        {
            returnValue = string.Concat(string.Format(values[0], parameters.ToArray()), severityValue);
        }
        catch (FormatException)
        {}

        return returnValue;
    }
}
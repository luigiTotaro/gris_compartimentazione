﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true"
    CodeFile="StationStatus.aspx.cs" Inherits="Reports_StationStatus" %>

<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" runat="Server">
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" runat="Server">
    <rsweb:ReportViewer ID="rptvSelectedReport" runat="server" Height="500px" Width="98%"
        Font-Names="Verdana" Font-Size="8pt">
        <LocalReport ReportPath="Reports\StationStatus.rdlc" EnableExternalImages="true"
            OnSubreportProcessing="rptvSelectedReport_OnSubreportProcessing">
            <DataSources>
                <rsweb:ReportDataSource Name="ReportDS" DataSourceId="odsStationStatus" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="odsStationStatus" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetStationStatusLocalizedData" TypeName="DataSetLocalizer"></asp:ObjectDataSource>
</asp:Content>

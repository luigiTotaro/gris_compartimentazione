using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Microsoft.Reporting.WebForms;

public partial class Reports_Devices : ReportPublisher
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		this.odsDevices.Select();
		this.ReportsMaster.ReportListPanelVisible = false;
		this.ReportsMaster.ReportPanelVisible = true;
	}

	public override Microsoft.Reporting.WebForms.ReportViewer ReportViewer
	{
		get { return this.rptvSelectedReport; }
	}
}

﻿using System;
using Microsoft.Reporting.WebForms;

public partial class Reports_StationStatus : ReportPublisher
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void rptvSelectedReport_OnSubreportProcessing(object sender, SubreportProcessingEventArgs e)
    {
        if (e.Parameters != null && e.Parameters["DevID"] != null)
        {
            long devId = long.Parse(e.Parameters["DevID"].Values[0]);

            DataSetLocalizer loc = new DataSetLocalizer();
            e.DataSources.Add(new ReportDataSource("ReportDS", loc.GetStreamsDataByDevIdLocalizedData(devId).sWEB_Report_GetStreamsDataByDevID));
        }
    }

    #region Overrides of ReportPublisher

    public override ReportViewer ReportViewer
    {
        get { return this.rptvSelectedReport; }
    }

    #endregion
}
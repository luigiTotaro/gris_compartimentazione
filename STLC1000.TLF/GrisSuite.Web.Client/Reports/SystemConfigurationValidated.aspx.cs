using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Reporting.WebForms;

public partial class Reports_SystemConfigurationValidated : ReportPublisher
{
	public int SrvID
	{
		get
		{
			return Convert.ToInt32(this.ViewState["SrvID"] ?? -1);
		}

		set
		{
			this.ViewState["SrvID"] = value;
		}
	}

	protected void Page_Load ( object sender, EventArgs e )
	{
		this.odsServers.Select();
		this.ReportsMaster.ReportListPanelVisible = false;
		this.ReportsMaster.ReportPanelVisible = true;
	}

	protected void odsServers_Selected ( object sender, ObjectDataSourceStatusEventArgs e )
	{
		object o = e.ReturnValue;

		if ( o is ServerDS.serversDataTable )
		{
			ServerDS.serversDataTable servers = (ServerDS.serversDataTable)o;

			if ( servers.Rows.Count > 0 )
			{
				this.SrvID = servers[0].SrvID;
				this.CheckConfigurationValidation(servers[0]);
			}
		}
	}

	protected void odsSystemConfigurationValidated_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
	{
		e.InputParameters["SrvID"] = this.SrvID;
	}

	public override Microsoft.Reporting.WebForms.ReportViewer ReportViewer
	{
		get { return this.rptvSelectedReport; }
	}

	private void CheckConfigurationValidation ( ServerDS.serversRow row )
	{
		if ( !row.IsSupervisorSystemXMLNull() && !row.IsClientSupervisorSystemXMLValidatedNull() )
		{
			if ( row.IsClientSupervisorSystemXMLValidatedNull() ) row.ClientSupervisorSystemXMLValidated = "";
			if ( row.IsClientValidationSignNull() ) row.ClientValidationSign = new byte[0];
			if ( row.IsClientKeyNull() ) row.ClientKey = new byte[0];

			row.ConfigurationValid = GrisSuite.Common.SignCryptUtility.VerifySignedHash(row.ClientSupervisorSystemXMLValidated, row.ClientValidationSign, row.ClientKey);

			if ( row.ConfigurationValid )
			{
				row.ConfigurationInUseValid = ( row.SupervisorSystemXML == row.ClientSupervisorSystemXMLValidated );
			}
			else
			{
				row.ConfigurationInUseValid = false;
			}

			ReportParameter[] parameters = new ReportParameter[3];
			parameters[0] = new ReportParameter("ConfigurationValid", row.ConfigurationValid.ToString());
			parameters[1] = new ReportParameter("ConfigurationValidationDate", row.ClientDateValidationObtained.ToString("dd/MM/yyyy"));
			parameters[2] = new ReportParameter("EverValidated", ( !row.IsClientSupervisorSystemXMLValidatedNull() ).ToString());

			try
			{
				this.ReportViewer.LocalReport.SetParameters(parameters);
			}
			catch ( Exception ) { }

			row.AcceptChanges();
		}
	}
}

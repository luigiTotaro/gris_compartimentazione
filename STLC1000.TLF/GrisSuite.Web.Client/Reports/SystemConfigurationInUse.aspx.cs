using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Reporting.WebForms;
using System.Collections.Generic;

public partial class Reports_SystemConfigurationInUse : ReportPublisher
{
	private IList<ReportParameter> _paramsList = new List<ReportParameter>();

	public int SrvID
	{
		get
		{
			return Convert.ToInt32(this.ViewState["SrvID"] ?? -1);
		}

		set
		{
			this.ViewState["SrvID"] = value;
		}
	}

	protected void Page_Load ( object sender, EventArgs e )
    {
		this.odsServers.Select();
		this.ReportsMaster.ReportListPanelVisible = false;
		this.ReportsMaster.ReportPanelVisible = true;
	}

	protected void odsServers_Selected ( object sender, ObjectDataSourceStatusEventArgs e )
	{
		object o = e.ReturnValue;

		if ( o is ServerDS.serversDataTable )
		{
			ServerDS.serversDataTable servers = (ServerDS.serversDataTable)o;

			if ( servers.Rows.Count > 0 )
			{
				this.SrvID = servers[0].SrvID;
				this.CheckConfigurationValidation(servers[0]);
			}
		}
	}

	protected void odsSystemConfigurationInUse_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
	{
		e.InputParameters["SrvID"] = this.SrvID;
	}

	public override Microsoft.Reporting.WebForms.ReportViewer ReportViewer
	{
		get { return this.rptvSelectedReport; }
	}

	private void CheckConfigurationValidation ( ServerDS.serversRow row )
	{
		if ( !row.IsSupervisorSystemXMLNull() && !row.IsClientSupervisorSystemXMLValidatedNull() )
		{
			if ( row.IsClientSupervisorSystemXMLValidatedNull() ) row.ClientSupervisorSystemXMLValidated = "";
			if ( row.IsClientValidationSignNull() ) row.ClientValidationSign = new byte[0];
			if ( row.IsClientKeyNull() ) row.ClientKey = new byte[0];

			GrisSuite.Common.SignCryptUtility.VerifySignedHash(row.ClientSupervisorSystemXMLValidated, row.ClientValidationSign, row.ClientKey);

			if ( row.ConfigurationValid )
			{
				row.ConfigurationInUseValid = ( row.SupervisorSystemXML == row.ClientSupervisorSystemXMLValidated );

				_paramsList.Add(new ReportParameter("ConfigurationInUseValid",  ( row.SupervisorSystemXML == row.ClientSupervisorSystemXMLValidated ).ToString()));
				_paramsList.Add(new ReportParameter("ConfigurationValidationDate", row.ClientDateValidationObtained.ToString()));
			}
			else
			{
				row.ConfigurationInUseValid = false;
			}

			row.AcceptChanges();
		}
		else
		{
			_paramsList.Add(new ReportParameter("ConfigurationInUseValid", "False"));
			_paramsList.Add(new ReportParameter("ConfigurationValidationDate", DateTime.MinValue.ToString()));

		}
	}

	public override void SetupReportParameters ()
	{
		_paramsList.Clear();
		_paramsList.Add(new ReportParameter("ConfigurationInUseValid", "False"));
		_paramsList.Add(new ReportParameter("ConfigurationValidationDate", DateTime.MinValue.ToString()));

		_paramsList.Add(this.CurrentCultureParameter);

		try
		{
			this.ReportViewer.LocalReport.SetParameters(_paramsList);
		}
		catch ( Exception exc ) { string s = exc.Message; } // I parametri possono essere settati solo in alcuni momenti nel life-cycle della pagina. Se il report � gi� read-only genera un'eccezione. Visto che non c'� modo di capire se il controllo � in uno stato immodificabile per ora si trappa l'eccezione.
	}
}

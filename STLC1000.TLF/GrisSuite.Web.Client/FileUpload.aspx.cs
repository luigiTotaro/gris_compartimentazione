﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

public partial class FileUpload : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string configPath = ConfigurationManager.AppSettings["UploadedFiles"] ?? "UploadedFiles";
		string fullFolderPath = ( Path.IsPathRooted(configPath) ) ? configPath : this.Server.MapPath(configPath);

		if ( !Directory.Exists(fullFolderPath) ) Directory.CreateDirectory(fullFolderPath);

		if ( this.Session["UploadError"] != null )
		{
			this.lblError.Text = this.Session["UploadError"].ToString();
			this.Session.Remove("UploadError");
		}

		FileInfo[] files = new DirectoryInfo(fullFolderPath).GetFiles();
		if ( files != null && files.Length > 0 )
		{
			this.dlstFiles.DataSource = files;
			this.dlstFiles.DataBind();

			this.updFiles.Update();
		}

		if ( this.Request.Files != null && this.Request.Files.Count > 0 )
		{
			string fileName = Path.GetFileName(this.Request.Files[0].FileName);

			try
			{
				this.Request.Files[0].SaveAs(Path.Combine(fullFolderPath, fileName));
			}
			catch ( Exception exc )
			{
				this.Session["UploadError"] = string.Format("Upload error: {0}", exc.Message);
				System.Diagnostics.Trace.TraceError("Upload file error -> " + exc.Message + ", Stack Trace -> " + exc.StackTrace);
			}
		}
    }

	protected void dlstFiles_ItemDataBound ( object sender, DataListItemEventArgs e )
	{
		if ( e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item )
		{
			FileInfo file = (FileInfo)e.Item.DataItem;
			HyperLink fileLink = (HyperLink)e.Item.FindControl("lnkFile");
			fileLink.NavigateUrl = string.Format("~/{0}/{1}", ( new DirectoryInfo(Path.GetDirectoryName(file.FullName)) ).Name, file.Name);
		}
	}
}
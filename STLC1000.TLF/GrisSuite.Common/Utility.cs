using System;
using System.Globalization;

namespace GrisSuite.Common
{
	public static class Utility
	{
		public static string GetFullHostName ()
		{
			string domainName = "";
			string fullhostName = "";
			string hostname = Environment.MachineName;

			using ( System.Management.ManagementObject cs = new System.Management.ManagementObject("Win32_ComputerSystem.Name='" + hostname + "'") )
			{
				try
				{
					cs.Get();

					if ( cs["partOfDomain"] != null && (bool)cs["partOfDomain"] )
					{
						domainName = cs["domain"].ToString();
					}
				}
				catch ( System.Management.ManagementException )
				{
					domainName = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters", "Domain", "").ToString();
				}
			}

			if ( domainName == "" )
			{
				fullhostName = hostname;
			}
			else
			{
				fullhostName = string.Format("{0}.{1}", hostname, domainName);
			}

			return fullhostName;
		}

		public static string CapitalizeFirstLetter ( string text )
		{
			if ( !string.IsNullOrEmpty(text) )
			{
				string[] aText = text.Split(' ');
				if ( aText != null && aText.Length > 0 )
				{
					aText[0] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(aText[0]);
					text = string.Join(" ", aText);
				}
			}

			return text;
		}

		# region Gris Encoding Methods
		
		public static string GetCompCode ( Int64 RegID )
		{
			const long mask = 0x000000000000FFFF;
			return ( RegID & mask ).ToString();
		}

		public static string GetLineCode ( Int64 ZonID )
		{
			const long mask = 0x000000000000FFFF;
			return ( ( ZonID >> 16 ) & mask ).ToString();
		}

		public static string GetNodeCode ( Int64 NodID )
		{
			const long mask = 0x000000000000FFFF;
			return ( ( NodID >> 32 ) & mask ).ToString();
		}
		
		/// <summary>
		/// Decodifica un Device ID da database nei rispettivi sotto ID originali da System.XML
		/// </summary>
		/// <param name="devID">Device ID da database</param>
		/// <param name="originalDevID">Device ID originale da System.XML</param>
		/// <param name="originalNodeID">Node ID originale da System.XML</param>
		/// <param name="originalZoneID">Zone ID originale da System.XML</param>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		public static void DecodeDeviceID ( ulong devID, out ushort originalDevID, out ushort originalNodeID, out ushort originalZoneID,
									   out ushort originalRegionID )
		{
			originalDevID = (ushort)( ( devID & 0xFFFF000000000000 ) >> 48 );
			originalNodeID = (ushort)( ( devID & 0xFFFF00000000 ) >> 32 );
			originalZoneID = (ushort)( ( devID & 0xFFFF0000 ) >> 16 );
			originalRegionID = (ushort)( ( devID & 0xFFFF ) );
		}

		/// <summary>
		/// Decodifica un Node ID da database nei rispettivi sotto ID originali da System.XML
		/// </summary>
		/// <param name="nodID">Node ID da database</param>
		/// <param name="originalNodeID">Node ID originale da System.XML</param>
		/// <param name="originalZoneID">Zone ID originale da System.XML</param>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		public static void DecodeNodeID ( ulong nodID, out ushort originalNodeID, out ushort originalZoneID, out ushort originalRegionID )
		{
			originalNodeID = (ushort)( ( nodID & 0xFFFF00000000 ) >> 32 );
			originalZoneID = (ushort)( ( nodID & 0xFFFF0000 ) >> 16 );
			originalRegionID = (ushort)( ( nodID & 0xFFFF ) );
		}

		/// <summary>
		/// Decodifica uno Zone ID da database nei rispettivi sotto ID originali da System.XML
		/// </summary>
		/// <param name="zonID">Zone ID da database</param>
		/// <param name="originalZoneID">Zone ID originale da System.XML</param>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		public static void DecodeZoneID ( ulong zonID, out ushort originalZoneID, out ushort originalRegionID )
		{
			originalZoneID = (ushort)( ( zonID & 0xFFFF0000 ) >> 16 );
			originalRegionID = (ushort)( ( zonID & 0xFFFF ) );
		}

		/// <summary>
		/// Decodifica una Region ID da database nei rispettivi sotto ID originali da System.XML
		/// </summary>
		/// <param name="regID">Region ID da database</param>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		public static void DecodeRegionID ( ulong regID, out ushort originalRegionID )
		{
			originalRegionID = (ushort)( ( regID & 0xFFFF ) );
		}

		/// <summary>
		/// Codifica un Device ID costituito dai valori da System.XML nella versione per database
		/// </summary>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		/// <param name="originalZoneID">Zone ID originale da System.XML</param>
		/// <param name="originalNodeID">Node ID originale da System.XML</param>
		/// <param name="originalDevID">Device ID originale da System.XML</param>
		/// <returns>Intero del Device ID per il database</returns>
		public static ulong EncodeDeviceID ( ushort originalRegionID, ushort originalZoneID, ushort originalNodeID, ushort originalDevID )
		{
			return ( originalRegionID | ( (ulong)originalZoneID << 16 ) | ( (ulong)originalNodeID << 32 ) | ( (ulong)originalDevID << 48 ) );
		}

		/// <summary>
		/// Codifica un Node ID costituito dai valori da System.XML nella versione per database
		/// </summary>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		/// <param name="originalZoneID">Zone ID originale da System.XML</param>
		/// <param name="originalNodeID">Node ID originale da System.XML</param>
		/// <returns>Intero del Node ID per il database</returns>
		public static ulong EncodeNodeID ( ushort originalRegionID, ushort originalZoneID, ushort originalNodeID )
		{
			return ( originalRegionID | ( (ulong)originalZoneID << 16 ) | ( (ulong)originalNodeID << 32 ) );
		}

		/// <summary>
		/// Codifica uno Zone ID costituito dai valori da System.XML nella versione per database
		/// </summary>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		/// <param name="originalZoneID">Zone ID originale da System.XML</param>
		/// <returns>Intero dello Zone ID per il database</returns>
		public static ulong EncodeZoneID ( ushort originalRegionID, ushort originalZoneID )
		{
			return ( originalRegionID | ( (ulong)originalZoneID << 16 ) );
		}

		/// <summary>
		/// Codifica un Region ID costituito dai valori da System.XML nella versione per database
		/// </summary>
		/// <param name="originalRegionID">Region ID originale da System.XML</param>
		/// <returns>Intero del Region ID per il database</returns>
		public static ulong EncodeRegionID ( ushort originalRegionID )
		{
			return ( originalRegionID | ( (ulong)0xFFFFFFFF0000 ) );
		}

        /// <summary>
        /// Ritorna la stringa con il numero di serie decodificato dal Server ID
        /// </summary>
        /// <param name="servId">Server ID</param>
        /// <returns>Stringa con il numero di serie dell'STLC, nel formato 00.00.000 (anno, mese, progressivo)</returns>
        public static string GetSNFromSrvID(ulong servId)
        {
            return string.Format("{0:00}.{1:00}.{2:000}", ((servId & 0xFF000000) >> 24), ((servId & 0xFF0000) >> 16), (servId & 0xFFFF));
        }
		
		# endregion
	}
}

﻿using System;
using System.Resources;
using System.Reflection;
using System.Globalization;

namespace GrisSuite.Common
{
	public class ReportLocalizer
	{
		private ResourceManager _resManager;
		public string ResourceName { get; private set; }

		public ReportLocalizer ( string resourceName )
		{
			this.ResourceName = resourceName;
			Initialize();
		}

		private void Initialize ()
		{
			try
			{
				_resManager = new ResourceManager(string.Format("resources.{0}", this.ResourceName.ToLower()), Assembly.Load("App_GlobalResources"));
			}
			catch ( ArgumentNullException )
			{
				_resManager = null;
			}
			catch ( ArgumentException )
			{
				_resManager = null;
			}
		}

		public string GetLocalText ( string key )
		{
			if ( _resManager != null )
			{
				return _resManager.GetString(key);				
			}

			return "";
		}

		public string GetLocalText ( string key, string culture )
		{
			if ( _resManager != null )
			{
				return _resManager.GetString(key, new CultureInfo(culture));
			}

			return "";
		}
	}
}

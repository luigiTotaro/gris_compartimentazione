﻿using System.Collections.Generic;
using System.Configuration;
using System.Web;

namespace GrisSuite.Common {
    public static class GrisPermissions {
        # region Permessi
        // Restituisce l'elenco dei compartimenti per cui l'utente ha visibilità almeno di livello 1
        public static long[] VisibleRegions() {
            return VisibleRegions(1);
        }

        public static long[] VisibleRegions(int level) {
            long regid = 281474976645120;
            List<long> regs = new List<long>(15);
            for (int reg = 1; reg <= 15; reg++) {
                if (!NotInRole(reg.ToString(), level)) {
                    regs.Add(regid + reg);
                }
            }

            return regs.ToArray();
        }

        public static bool NotInRole(long regId) {
            return NotInRole(Utility.GetCompCode(regId));
        }

        public static bool NotInRole(string compartimento) {
            return NotInRole(compartimento, 1);
        }

        public static bool NotInRole(long regId, int level) {
            return NotInRole(Utility.GetCompCode(regId), level);
        }

        public static bool NotInRole(string compartimento, int level) {
            System.Security.Principal.IPrincipal principal = HttpContext.Current.User;

            if (principal != null) {
                switch (level) {
                    case 1: {
                            return (!principal.IsInRole(ConfigurationManager.AppSettings["groupComp" + compartimento])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupComp" + compartimento + "_2"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupComp" + compartimento + "_3"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAll"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAll_2"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAll_3"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupGrisOperators"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAdmin"]));
                        }
                    case 2: {
                            //return true;
                            return (!principal.IsInRole(ConfigurationManager.AppSettings["groupComp" + compartimento + "_2"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupComp" + compartimento + "_3"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAll_2"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAll_3"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupGrisOperators"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAdmin"]));
                        }
                    case 3: {
                            //return true;
                            return (!principal.IsInRole(ConfigurationManager.AppSettings["groupComp" + compartimento + "_3"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAll_3"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupGrisOperators"])
                                && !principal.IsInRole(ConfigurationManager.AppSettings["groupAdmin"]));
                        }
                }
            }

            return true;
        }

        public static bool IsUserInLevel(int level) {
            bool isInLevel = false;
            for (int comp = 1; comp <= 15; comp++) {
                if (!NotInRole(comp.ToString(), level)) {
                    isInLevel = true;
                    break;
                }
            }

            return isInLevel;
        }

        public static bool IsUserInConfigValidationGroup() {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupConfigValidation"])) {
                return false;
            }

            return (HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupConfigValidation"]));
        }

        public static bool IsUserInGrisOperatorsGroup() {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupGrisOperators"])) {
                return false;
            }

            return HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupGrisOperators"]);
        }

        public static bool IsUserInAlertOperatorsGroup() {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupAlertOperators"])) {
                return false;
            }

            return HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupAlertOperators"]);
        }

        public static bool IsUserInAlertUsersGroup() {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupAlertUsers"])) {
                return false;
            }

            return HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupAlertUsers"]);
        }

        public static bool IsUserInAdminGroup() {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupAdmin"])) {
                return false;
            }

            return HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupAdmin"]);
        }

        public static bool CanUserAccessGris() {
            return (IsUserInLevel(1) || IsUserInConfigValidationGroup() || IsUserInAlertOperatorsGroup() || IsUserInAlertUsersGroup());
        }

        public static bool CanUserViewAlerts() {
            return (CanUserEditAlerts() || IsUserInAlertUsersGroup());
        }

        public static bool CanUserEditAlerts() {
            return (IsUserInAdminGroup() || IsUserInAlertOperatorsGroup());
        }

        # endregion
    }
}
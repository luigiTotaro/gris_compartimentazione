﻿namespace GrisSuite.Common
{
    public enum Status
    {
        None = -1,
        Ok = 0,
        Warning = 1,
        Error = 2,
        Offline = 3,
        Maintenance = 9,
        Unknown = 255
    }

	public enum AlertsPresence
	{
		Present,
		NotPresent,
		Error
	}

	public enum ScagentActionEnum
	{
		SendConfigToCentralComplete = 128,
		SendConfigToCentralOnlyChanges,
		SendDeviceStatusToCentralComplete,
		SendDeviceStatusToCentralOnlyChanges,
		SendDeviceStatusAndConfigToCentral
	}

	public enum SystemValidationEnum : short
	{
		RequestCorrectlyReceived,
		ServerNotFound,
		SqlError
	}

    public enum GrisEventCategory : short
    {
        DeficeLevel1Events = 1,             //Eventi di periferica livello 1
        DeficeLevel2Events = 2,             //Eventi di periferica livello 2
        DeficeLevel3Events = 3,             //Eventi di periferica livello 3
        DeficeLevel4Events = 4,             //Eventi di periferica livello 4    
        SystemEvents = 5,                   //Eventi di Sistema
        ComunicationEvents = 6,             //Eventi relativi alle porte di comunicazione
        DatabaseEvents = 7                  //Eventi relativi al database
    }

    // Next Free Count  4027

    //    Categoria 5 = Eventi di Sistema
    public enum GrisSystemEvents : int
    {
        // Generici 
        OkInfo = 4010,                      //Sistema di diagnostica in stato di funzionamento corretto 0
        Warning = 4011,                     //Sistema di diagnostica in stato di attenzione 1
        Error = 4012,                       //Sistema di diagnostica in stato di errore 2
        InitializationStateWarning = 4013,  //Sistema di diagnostica in fase di inizializzazione 1
        ClosingStateWarning = 4014,         //Sistema di diagnostica in fase di chiusura 1
        SchedulingStartedInfo = 4015,       //Schedulazione del sistema di diagnostica avviata 0
        SchedulingStoppedWarning = 4016,    //Schedulazione del sistema di diagnostica arrestata 1
        ErrorOnCostructor = 4027,           //Errore nel costruttore 
        DBSemaphoreLockWarning = 2028,      //Attenzione problema in fase di lock di un semaforo
        DBSemaphoreUnlockWarning = 2029,    //Attenzione problema in fase di unlock di un semaforoù
        DBSemaphoreUnlockForcedWarning = 2031,  //Attenzione unlock forzato per "timeout" del blocco
        ReenterOnTimerWarning = 2030        //Rientro nell'evento di un timer prima che l'iterazione precedente sia conclusa
    }

    //Categoria 6 = Eventi relativi alle porte di comunicazione
    public enum GrisComunicationEvents : int
    {
        PortOkInfo = 4020,              //Porta di comunicazione in stato di funzionamento corretto 0
        PortWarning = 4021,             //Porta di comunicazione in stato di funzionamento anomalo 1
        PortError = 4022,               //Porta di comunicazione in stato di errore 2
        PortOpenedOkInfo = 4023,        //Porta aperta correttamente 0
        PortClosedOkInfo = 4024,        //Porta chiusa correttamente 0
        PortOpeningError = 4025,        //Errore nell'apertura della porta 2
        PortClosingWarning = 4026       //Errore nella chiusura della porta 1
    }

    //Categoria 7 = Eventi relativi al database
    public enum GrisDatabaseEvents : int
    {
        OpenConnctionInfo = 4000,       //Connessione al database riuscita 0
        OpenConnctionError = 4001,      //Connessione al database fallita 2
        CloseConnectionInfo = 4002,     //Disconnessione dal database riuscita  0
        CloseConnectionError = 4003,    //Disconnessione dal database fallita  2
        QueryInfo = 4004,               //Query sul database riuscita 0
        QueryError = 4005               //Query sul database fallita 2
    }
    
}

namespace GrisSuite.Data
{

    public enum LogDBLevel
    {
        Off,
        MessageEnvelope,
        MessageComplete,
        MessageEnvelopeAndStream,
        MessageCompleteAndStream
    }
}
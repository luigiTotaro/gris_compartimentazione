
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace GrisSuite.Common
{
	public class SignCryptUtility
	{
		static SignCryptUtility ()
		{
			RSACryptoServiceProvider.UseMachineKeyStore = true;
		}

		public static byte[] HashAndSign ( string stringDataToSign, byte[] privKey )
		{
			ASCIIEncoding ByteConverter = new ASCIIEncoding();

			byte[] originalData = ByteConverter.GetBytes(stringDataToSign);


			////////////////
			//System.Security.Cryptography.CspParameters parameters = new System.Security.Cryptography.CspParameters();
			//System.Security.AccessControl.CryptoKeySecurity sec = new System.Security.AccessControl.CryptoKeySecurity();

			//sec.SetOwner(System.Security.Principal.WindowsIdentity.GetCurrent().User);
			//sec.SetAccessRule(new System.Security.AccessControl.CryptoKeyAccessRule(@"rfidev\!Gris", System.Security.AccessControl.CryptoKeyRights.FullControl, System.Security.AccessControl.AccessControlType.Allow));
			//parameters.CryptoKeySecurity = sec;
			//parameters.Flags = System.Security.Cryptography.CspProviderFlags.UseDefaultKeyContainer | System.Security.Cryptography.CspProviderFlags.UseMachineKeyStore;

			//parameters.KeyContainerName = "GrisContainer";

			//System.Security.Cryptography.RSACryptoServiceProvider RSAalg = new System.Security.Cryptography.RSACryptoServiceProvider(parameters);
			/////////

			RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();
			RSAalg.ImportCspBlob(privKey);

			return RSAalg.SignData(originalData, new SHA1CryptoServiceProvider());
		}

		public static bool VerifySignedHash ( string dataToVerify, byte[] sign, byte[] pubKey )
		{
			RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();

			RSAalg.ImportCspBlob(pubKey);
			return RSAalg.VerifyData(ASCIIEncoding.ASCII.GetBytes(dataToVerify), new SHA1CryptoServiceProvider(), sign);
		}

		public static void GenerateRSAKeys ( out byte[] privateKeyBlog, out byte[] publicKeyBlog )
		{
			//CspParameters RSAParams = new CspParameters(); 
			//RSAParams.Flags = CspProviderFlags.UseMachineKeyStore;
			RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();

			privateKeyBlog = RSAalg.ExportCspBlob(true);
			publicKeyBlog = RSAalg.ExportCspBlob(false);
		}

		public static byte[] AESEncrypt ( string plainText )
		{
			byte[] key = null;
			byte[] iv = null;

			GetSymmetricParams(out key, out iv);

			RijndaelManaged aes = new RijndaelManaged();
			aes.Padding = PaddingMode.PKCS7;
			aes.Key = key;
			aes.IV = iv;
			ICryptoTransform encryptor = aes.CreateEncryptor();

			MemoryStream msEncrypt = new MemoryStream();
			CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
			StreamWriter swEncrypt = new StreamWriter(csEncrypt);

			try
			{
				swEncrypt.Write(plainText);
			}
			finally
			{
				if ( swEncrypt != null )
					swEncrypt.Close();
				if ( csEncrypt != null )
					csEncrypt.Close();
				if ( msEncrypt != null )
					msEncrypt.Close();

				if ( aes != null )
					aes.Clear();
			}

			return msEncrypt.ToArray();
		}

		public static string AESDecrypt ( byte[] cypher )
		{
			string plaintext;

			byte[] key = null;
			byte[] iv = null;

			GetSymmetricParams(out key, out iv);

			RijndaelManaged aes = new RijndaelManaged();
			aes.Padding = PaddingMode.PKCS7;
			aes.Key = key;
			aes.IV = iv;

			ICryptoTransform decryptor = aes.CreateDecryptor(key, iv);

			MemoryStream msDecrypt = new MemoryStream(cypher);
			CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
			StreamReader srDecrypt = new StreamReader(csDecrypt);

			try
			{
				plaintext = srDecrypt.ReadToEnd();
			}
			finally
			{
				if ( srDecrypt != null )
					srDecrypt.Close();
				if ( csDecrypt != null )
					csDecrypt.Close();
				if ( msDecrypt != null )
					msDecrypt.Close();

				if ( aes != null )
					aes.Clear();
			}

			return plaintext;
		}

		private static void GetSymmetricParams ( out byte[] key, out byte[] iv )
		{

			key = ASCIIEncoding.ASCII.GetBytes(@"dklajfd jdklafjdka�jf ds�jaf dijaf dklaj fkdlajfdhgkljirot 
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												iiieieurjrfkjekjfwjf ifj kwjfeowjfoewjfiowjeiwieofehwfoewjfjeowfjeiowjfkeowjfeioqf fio
												4892u4jhfihfe f9h ffe02urre02udiejfi2hfe02ui0e2uujfie0j2fie02rur010uijdpqjjhhadsodpuif
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												iiieieurjrfkjekjfwjf ifj kwjfeowjfoewjfiowjeiwieofehwfoewjfjeowfjeiowjfkeowjfeioqf fio
												4892u4jhfihfe f9h ffe02urre02udiejfi2hfe02ui0e2uujfie0j2fie02rur010uijdpqjjhhadsodpuif
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												iiieieurjrfkjekjfwjf ifj kwjfeowjfoewjfiowjeiwieofehwfoewjfjeowfjeiowjfkeowjfeioqf fio
												4892u4jhfihfe f9h ffe02urre02udiejfi2hfe02ui0e2uujfie0j2fie02rur010uijdpqjjhhadsodpuif
												kdfajfoj402jfii34f9f0 j 02 jf20 jfi2jief20j ie e2fj ief0fji2e j20iji20jei02hi4j2 i j02
												iiieieurjrfkjekjfwjf ifj kwjfeowjfoewjfiowjeiwieofehwfoewjfjeowfjeiowjfkeowjfeioqf fio
												4892u4jhfihfe f9h ffe02urre02udiejfi2hfe02ui0e2uujfie0j2fie02rur010uijdpqjjhhadsodpuif
												fji 48ffir93ff39ffj d3ghirfjdhjgfuwhfhiogfrie3gj 3ijgri3ogj3".Substring(150, 32));

			iv = ASCIIEncoding.ASCII.GetBytes(@"
											fjdkf ioaj fidsoa jfdsajf ioaj doaj fiojfdi afj ihqui huh o hof 
											fkdowj fd jiw fjfw jfwd hdow fh ffko jf fjwoj kw j jof jfw fojwk
											fkdowj fd jiw fjfw jfwd hdow fh ffko jf fjwoj kw j jof jfw fojwk
											irirjfj584 j 84 4u ju4848ffhfjfi84e8uf jjdeufjh fjdddididjehru
											 fjdowjfiodwdejfriow jfie jwfepwj fpewj fpwj fep wjfipe j
											fkdowj fd jiw fjfw jfwd hdow fh ffko jf fjwoj kw j jof jfw fojwk
											irirjfj584 j 84 4u ju4848ffhfjfi84e8uf jjdeufjh fjdddididjehru
											 fjdowjfiodwdejfriow jfie jwfepwj fpewj fpwj fep wjfipe j
											fkdowj fd jiw fjfw jfwd hdow fh ffko jf fjwoj kw j jof jfw fojwk
											irirjfj584 j 84 4u ju4848ffhfjfi84e8uf jjdeufjh fjdddididjehru
											 fjdowjfiodwdejfriow jfie jwfepwj fpewj fpwj fep wjfipe j
											".Substring(33, 16));
		}
	}
}

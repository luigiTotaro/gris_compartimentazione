﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace GrisSuite.Common
{
    public enum StateEnum
    {
        Ok = 0,
        Warning = 1,
        Error = 2,
        Unknown = 255
    }

    public interface IDeviceMapB2O
    {
        IObjectWithState GetStream(byte[][] data);
    }

    public interface IDeviceStreamMapB2O
    {
        IObjectWithState GetStream(byte[] data);
    }

    public interface IObjectWithState
    {
        string Label { get; }
        StateEnum State { get; }
        List<IStateMessage> StateMessages { get; }
        IObjectWithState Root { get; }
    }

    public interface IValue
    {
        object Value { get; }
        string Text { get; }
        string Description { get; }
        bool VisibleValue { get;  }
    }

    public interface IStateMessage
    {
        string Message { get; }
        StateEnum State { get; }
    }

    public delegate bool MessageRuleFunction<T>(ValueWithState<T> obj);

    public delegate StateEnum StateRuleFunction(ObjectWithState obj);

    public delegate string TextFormatFunction<T>(ValueWithState<T> obj);

    public class StateMessage : IStateMessage
    {

        public string Message { get; set; }
        public StateEnum State { get; set; }

        #region IStateMessage Members

        string IStateMessage.Message
        {
            get { return Message; }
        }

        StateEnum IStateMessage.State
        {
            get { return this.State; }
        }

        #endregion
    }

    public class StateMessageRule<T> 
    {
        public MessageRuleFunction<T> Rule { get; set; }
        public StateMessage Message { get; set; }
    }

    public class StateMessageCollection : List<IStateMessage>
    {
        public override string ToString()
        {
            string s = string.Empty;
            foreach (var message in this)
                s += "," + message.Message ;

            if (s != string.Empty)
                return s.Substring(1);

            return string.Empty;
        }
    }

    public class ObjectWithState : IObjectWithState
    {
        public StateRuleFunction StateRule {get; set;}
        private string label;
        private StateEnum state;
        private readonly StateMessageCollection stateMessages;
        private ObjectWithState root;

        public ObjectWithState() : this(null) { }

        public ObjectWithState(ObjectWithState root)
        {
            this.state = StateEnum.Ok;
            this.label = string.Empty;
            this.StateRule = null;
            this.stateMessages = new StateMessageCollection();
            this.root = root;
        }

        public void RefreshState()
        {
            if (StateRule == null)
                OnRefreshState();
            else
                this.state = StateRule(this);
        }

        protected virtual void OnRefreshState()
        {
            this.state = GetMaxState(this.Childs.ToArray(), true);
        }

        public List<IStateMessage> StateMessages
        {
            get
            {
                return this.stateMessages;
            }
        }

        public ObjectWithState Root
        {
            get
            {
                return this.root;
            }
            set
            {
                this.root = value;
            }
        }

        public List<ObjectWithState> Childs
        {
            get { return GetChilds(this); }
        }

        public StateEnum State
        {
            get
            {
                OnGettingState(this.state);
                return this.state;
            }
            protected set
            {
                this.state = value;
            }

        }

        public virtual string Label
        {
            get
            {
                return this.label;
            }
            set
            {
                this.label = value;
            }
        }

        StateEnum IObjectWithState.State
        {
            get
            {
                OnGettingState(this.state);
                return this.state;
            }
        }

        List<IStateMessage> IObjectWithState.StateMessages
        {
            get { return this.stateMessages; }
        }

        IObjectWithState IObjectWithState.Root
        {
            get { return this.root; }
        }

        protected virtual void OnGettingState(StateEnum stateParam)
        {
            //
        }

        public static StateEnum GetMaxState(ObjectWithState[] objects, bool refreshState)
        {
           return GetMaxState(objects, refreshState, StateEnum.Ok, null);
        }

        public static StateEnum GetMaxState(ObjectWithState[] objects, bool refreshState, StateEnum startState, List<ObjectWithState> excludedObjects)
        {
            var s = startState;
            
            if (s == StateEnum.Error)
                return s;

            foreach (var o in objects)
            {
                if (refreshState)
                    o.RefreshState();

                if (excludedObjects != null && excludedObjects.Contains(o))
                    continue;

                if (o.State > s)
                    s = o.State;
            }
            return s;
        }

        public static List<ObjectWithState> GetChilds(object obj)
        {

            List<ObjectWithState> childs = new List<ObjectWithState>();

            PropertyInfo[] props = obj.GetType().GetProperties();

            if (props != null && props.Length > 0)
            {
                foreach (PropertyInfo p in props)
                {
                    if (p.Name == "Root") continue;
                    if (p.Name == "Childs") continue;

                    if (typeof(IEnumerable).IsAssignableFrom(p.PropertyType))
                    {
                        IEnumerable enumerableElement = (IEnumerable)p.GetValue(obj, null);

                        if (enumerableElement != null)
                        {
                            foreach (object ows in enumerableElement)
                            {
                                if (ows != null && ows is ObjectWithState)
                                    childs.Add(ows as ObjectWithState);
                            }
                        }
                    }
                    else if (typeof(ObjectWithState).IsAssignableFrom(p.PropertyType))
                    {
                        var ows = p.GetValue(obj, null) as ObjectWithState;
                        if (ows != null)
                            childs.Add(ows);
                    }
                }
            }

            return childs;
        }


    }

    public class ValueWithState<T> : ObjectWithState, IValue
    {
        public delegate void ValueChangedHandler(ValueWithState<T> sender, EventArgs args);

        public event ValueChangedHandler ValueChanged;

        public TextFormatFunction<T> FormatFunction { get; set; }
        private StateMessageRule<T>[] stateMessageRules;

        public string FormatString { get; set; }

        internal T[] WarningValues { get; set; }
        public T WarningValue { set{this.WarningValues = new[] {value}; }}

        internal T[] ErrorValues { get; set; }
        public T ErrorValue { set { this.ErrorValues = new[] { value }; } }

        public ValueWithState(): this(null)
        {}

        public ValueWithState(ObjectWithState root) : base(root)
        {
            this.FormatFunction = null;
            this.FormatString = string.Empty;
            this.mDescription = string.Empty;
            this.mVisible = true;
        }

        protected T mValue;
        protected string mText;
        protected string mDescription;
        protected bool mVisible;

        public StateMessageRule<T>[] StateMessageRules 
        {
            get
            {
                return this.stateMessageRules;
            }
            set
            {
                this.stateMessageRules = value;
                base.RefreshState();
            }
        }

        protected override void OnRefreshState()
        {
             base.OnRefreshState();
             StateEnum max = base.State;
            
            if (StateMessageRules != null && StateMessageRules.Length > 0)
            {
                StateMessages.Clear();
                foreach (StateMessageRule<T> rule in this.StateMessageRules)
                {
                    if (rule.Rule(this))
                    {
                        StateMessages.Add(rule.Message);
                        if (rule.Message.State > max)
                            max = rule.Message.State;
                    }
                }
            }

            if (max < StateEnum.Warning && this.WarningValues != null  && this.WarningValues.Length > 0)
            {
                foreach (var warningValue in this.WarningValues)
                {
                    if (IsEqual(this.mValue, warningValue))
                    {
                        max = StateEnum.Warning;
                        break;
                    }
                }
            }

            if (max < StateEnum.Error && this.ErrorValues != null && this.ErrorValues.Length > 0)
            {
                foreach (var errorValue in this.ErrorValues)
                {
                    if (IsEqual(this.mValue, errorValue))
                    {
                        max = StateEnum.Error;
                        break;
                    }
                }
            }

            this.State = max;
        }

        #region IValue Members

        static bool IsEqual(T v1, T v2)
        {
            Comparer<T> comparer = Comparer<T>.Default;
            return (comparer.Compare(v1, v2) == 0 ? true : false);
        }

        object IValue.Value
        {
            get
            {
                return this.mValue;
            }
        }

        public string Text
        {
            get
            {
                OnGettingText();
                return mText;
            }

        }

        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        public bool VisibleValue
        {
            get { return mVisible; }
            set { mVisible = value; }
        }

        #endregion

        public T Value
        {
            get
            {
                OnGettingValue();
                return this.mValue;
            }
            set
            {
                this.mValue = value;
                OnValueChanged();
            }
        }

        protected virtual void OnGettingValue()
        { 
        }
        
        
        protected virtual void OnValueChanged()
        {
            if (ValueChanged != null)
                ValueChanged(this, null);

            base.RefreshState();
        }

        protected virtual void OnGettingText()
        {
            if(this.FormatFunction != null)
            {
                this.mText = this.FormatFunction(this); 
            }
            else if (!string.IsNullOrEmpty(this.FormatString))
            {
                this.mText = string.Format(this.FormatString, this.Value);    
            }
            else
            {
                this.mText = this.Value.ToString();
            }
        }


    }
    
    //public static class ObjectWithStateHelper
    //{
    //    public static StateEnum GetMaxState(object o)
    //    {
    //        StateEnum max = StateEnum.Ok;
    //        PropertyInfo[] props = o.GetType().GetProperties();

    //        if (props != null && props.Length > 0)
    //        {
    //            foreach (PropertyInfo p in props)
    //            {
    //                if (p.Name == "Root") continue;

    //                if (typeof(IEnumerable).IsAssignableFrom(p.PropertyType))
    //                {
    //                    IEnumerable enumerableElement = (IEnumerable)p.GetValue(o, null);

    //                    if (enumerableElement != null)
    //                    {
    //                        foreach (object value in enumerableElement)
    //                        {
    //                            StateEnum childMax = GetMaxState(value);
    //                            if (childMax > max)
    //                                max = childMax;
    //                        }
    //                    }
    //                }
    //                else if (typeof (IObjectWithState).IsAssignableFrom(p.PropertyType))
    //                {
    //                    var ows = p.GetValue(o, null) as IObjectWithState;
    //                    if (ows != null)
    //                        if(ows.State > max)
    //                            max = ows.State;
    //                }
    //            }
    //        }

    //        return max;
    //    }
    //}
    
}

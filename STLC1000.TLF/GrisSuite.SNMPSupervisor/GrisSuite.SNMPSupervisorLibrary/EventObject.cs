﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GrisSuite.SnmpSupervisorLibrary
{
    /// <summary>
    ///   Rappresenta l'oggetto di stato per il passaggio di informazioni relative all'evento
    /// </summary>
    public class EventObject
    {
        #region Costruttore

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "deviceId">ID della periferica</param>
        /// <param name = "eventId">GUID dell'evento</param>
        /// <param name = "eventData">Dati grezzi dell'evento</param>
        /// <param name = "created">Data e ora di creazione dell'evento</param>
        public EventObject(long? deviceId, Guid? eventId, string eventData, DateTime created)
        {
            this.DeviceId = deviceId;
            this.EventId = eventId;
            this.EventData = eventData;
            this.Created = created;
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///   ID della periferica
        /// </summary>
        public long? DeviceId { get; private set; }

        /// <summary>
        ///   GUID dell'evento
        /// </summary>
        public Guid? EventId { get; private set; }

        /// <summary>
        ///   Dati grezzi dell'evento
        /// </summary>
        public string EventData { get; private set; }

        /// <summary>
        ///   Data e ora di creazione dell'evento
        /// </summary>
        public DateTime Created { get; private set; }

        /// <summary>
        ///   Ritorna i dati grezzi dell'evento, in formato bytes
        /// </summary>
        /// <returns>Bytes dei dati dell'Evento</returns>
        public byte[] GetEventDataBytes()
        {
            if (this.EventData != null)
            {
                return Encoding.GetEncoding(1252).GetBytes(this.EventData);
            }

            return null;
        }

        /// <summary>
        ///   Ritorna l'hash SHA-256 dell'evento
        /// </summary>
        /// <returns>Array di byte contenente l'hash. Nullo in caso di mancanza di dati nell'evento.</returns>
        public byte[] GetHash()
        {
            if (String.IsNullOrEmpty(this.EventData))
            {
                return null;
            }

            using (SHA256 sha = new SHA256Managed())
            {
                return sha.ComputeHash(Encoding.GetEncoding(1252).GetBytes(this.EventData));
            }
        }

        #endregion
    }
}
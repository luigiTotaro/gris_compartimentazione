﻿namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Rappresenta la severità dei valori ricevuti
    /// </summary>
    public enum Severity : byte
    {
        /// <summary>
        ///   Stato normale
        /// </summary>
        Ok = 0,
        /// <summary>
        ///   Allarme lieve
        /// </summary>
        Warning = 1,
        /// <summary>
        ///   Allarme grave
        /// </summary>
        Error = 2,
        /// <summary>
        ///   Sconosciuto o nessuna risposta
        /// </summary>
        Unknown = 255
    }
}
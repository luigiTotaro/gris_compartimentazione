﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Gestisce la formattazione di un valore, in base al tipo dati e alla stringa di formattazione
    /// </summary>
    public class FormatExpression
    {
        /// <summary>
        ///   Stringa di formattazione
        /// </summary>
        public string Format { get; private set; }

        /// <summary>
        ///   Tipo di dati elaborato dalla stringa di formattazione
        /// </summary>
        public FormatExpressionDataType FormatDataType { get; private set; }

        /// <summary>
        ///   Indica se la stringa di formattazione è stata correttamente riconosciuta ed è quindi applicabile
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        ///   Indica il fattore di moltiplicazione da applicare al dato, nel caso di tipo dati numerico. Se non presente, riporta 1, neutro rispetto alla moltiplicazione
        /// </summary>
        public float Factor { get; private set; }

        /// <summary>
        ///   Indica il valore da sommare al dato, nel caso di tipo dati numerico. Se non presente, riporta 0, neutro rispetto all'addizione
        /// </summary>
        public float Addend { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "expression">Espressione che esprime la formattazione</param>
        /// <example>
        ///   {(long)0:P}
        /// </example>
        /// <example>
        ///   {(long)0:###,0}
        /// </example>
        /// <example>
        ///   {(string)0:0 (A)}
        /// </example>
        /// <example>
        ///   {(string)0:0 s}
        /// </example>
        /// <example>
        ///   {(long)0:(*0.0166666666666667)0 min} divide il valore corrente per 60, per rappresentare i secondi in minuti
        /// </example>
        /// <example>
        ///   {(long)0:(*0.5555555555555556+32)0 °C} converte un valore in gradi Fahrenheit in Celsius
        /// </example>
        /// <example>
        ///   {(long)0:(-15.5)0 A} aggiusta un valore di misura con una relativa taratura
        /// </example>
        /// <example>
        ///   {(long)0:(*0.1)0 Hz} converte una lettura in decimi di Hertz in Hertz
        /// </example>
        /// <example>
        ///   {(long)0:(*0.0000000009313225746154785)0.00 GB} converte il numero di bytes in gigabytes
        /// </example>
        /// <example>
        ///   {(long)0:(*0.00000095367431640625)0.00 MB} converte il numero di bytes in megabytes
        /// </example>
        /// <example>
        ///   {(long)0:(*0.0009765625)0.00 KB} converte il numero di bytes in kilobytes
        /// </example>
        public FormatExpression(string expression)
        {
            this.IsValid = false;
            this.FormatDataType = FormatExpressionDataType.Unknown;
            this.Format = string.Empty;
            this.Factor = 1;
            this.Addend = 0;

            if (!string.IsNullOrEmpty(expression))
            {
                Regex mainRegex = new Regex("\\{\\((?<datatype>.*?)\\)0:(?<formatstring>.*?)\\}", RegexOptions.Singleline | RegexOptions.CultureInvariant);

                Match data = mainRegex.Match(expression);
                if ((data != null) && (data.Groups.Count == 3) && (data.Groups["datatype"] != null) && (data.Groups["formatstring"] != null) &&
                    (!string.IsNullOrEmpty(data.Groups["datatype"].Value)) && (!string.IsNullOrEmpty(data.Groups["formatstring"].Value)))
                {
                    switch (data.Groups["datatype"].Value.ToUpperInvariant().Trim())
                    {
                        case "LONG":
                            this.FormatDataType = FormatExpressionDataType.Long;
                            this.IsValid = true;
                            break;
                        case "STRING":
                            this.FormatDataType = FormatExpressionDataType.String;
                            this.IsValid = true;
                            break;
                        default:
                            this.FormatDataType = FormatExpressionDataType.Unknown;
                            break;
                    }

                    if (this.FormatDataType == FormatExpressionDataType.Long)
                    {
                        string formatstring = data.Groups["formatstring"].Value;

                        Regex longRegex = new Regex("\\((?:\\*(?<factor>\\d+\\.*\\d*))?(?:\\+(?<positiveaddend>\\d+\\.*\\d*))?(?:\\-(?<negativeaddend>\\d+\\.*\\d*))?\\)",
                                                    RegexOptions.Singleline | RegexOptions.CultureInvariant);

                        Match longData = longRegex.Match(formatstring);
                        if (longData != null)
                        {
                            if ((longData.Groups["factor"] != null) && (!string.IsNullOrEmpty(longData.Groups["factor"].Value)))
                            {
                                float factor;
                                if (float.TryParse(longData.Groups["factor"].Value, NumberStyles.Float, CultureInfo.InvariantCulture, out factor))
                                {
                                    this.Factor = factor;
                                }
                            }

                            if ((longData.Groups["positiveaddend"] != null) && (!string.IsNullOrEmpty(longData.Groups["positiveaddend"].Value)))
                            {
                                float positiveaddend;
                                if (float.TryParse(longData.Groups["positiveaddend"].Value, NumberStyles.Float, CultureInfo.InvariantCulture, out positiveaddend))
                                {
                                    this.Addend += positiveaddend;
                                }
                            }

                            if ((longData.Groups["negativeaddend"] != null) && (!string.IsNullOrEmpty(longData.Groups["negativeaddend"].Value)))
                            {
                                float negativeaddend;
                                if (float.TryParse("-" + longData.Groups["negativeaddend"].Value, NumberStyles.Float, CultureInfo.InvariantCulture, out negativeaddend))
                                {
                                    this.Addend += negativeaddend;
                                }
                            }

                            this.Format = string.IsNullOrEmpty(longData.Value) ? formatstring : formatstring.Replace(longData.Value, string.Empty);
                        }
                        else
                        {
                            this.Format = formatstring;
                        }
                    }
                    else
                    {
                        this.Format = data.Groups["formatstring"].Value;
                    }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security;
using System.Xml;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Classe di supporto per il parsing del file di configurazione (System.xml)
    /// </summary>
    public static class SystemXmlHelper
    {
        /// <summary>
        ///   File di configurazione (System.xml) da cui caricare le periferiche da monitorare
        /// </summary>
        public static string ConfigurationSystemFile { get; private set; }

        /// <summary>
        ///   Recupera la lista dei dispositivi da monitorare da file di configurazione System.xml
        /// </summary>
        /// <param name = "configurationSystemFile">File di configurazione (System.xml) da cui caricare le periferiche da monitorare</param>
        /// <returns>Lista di DeviceObject da monitorare</returns>
        public static ReadOnlyCollection<DeviceObject> GetMonitorDeviceList(string configurationSystemFile)
        {
            ConfigurationSystemFile = configurationSystemFile;

            ReadOnlyCollection<DeviceObject> devices;

            try
            {
                devices = LoadDevicesFromSystemXml();
            }
            catch (ConfigurationSystemException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "An error has occurred during devices list loading. " + ex.Message);
                return null;
            }

            return devices;
        }

        /// <summary>
        ///   Carica il file di configurazione System.xml con la relativa topologia e dispositivi
        /// </summary>
        /// <returns>La lista dei dispositivi caricati</returns>
        private static ReadOnlyCollection<DeviceObject> LoadDevicesFromSystemXml()
        {
            XmlNode root = GetDefinitionXmlRootElement();

            List<TopographyLocation> locations = new List<TopographyLocation>();

            bool existsAtLeastOneLocation = false;

            // Normalmente il nodo della topografia è presente prima di quello con i sistemi
            // Per sicurezza lo cerchiamo singolarmente, in modo che la topografia sia popolata quando si elaborano i sistemi
            if (root != null)
            {
                foreach (XmlNode rootElement in root.ChildNodes)
                {
                    #region Elaborazione nodo Topography

                    if (rootElement.Name.Equals("topography", StringComparison.OrdinalIgnoreCase))
                    {
                        foreach (XmlNode station in rootElement.ChildNodes)
                        {
                            #region Elaborazione nodo Station

                            if (station.Name.Equals("station", StringComparison.OrdinalIgnoreCase))
                            {
                                if ((station.Attributes["id"] == null) || (station.Attributes["id"].Value.Trim().Length == 0) || (station.Attributes["name"] == null) ||
                                    (station.Attributes["name"].Value.Trim().Length == 0))
                                {
                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                         "Unable to find attributes 'id' or 'name' inside 'station' node, or station id or name are null in file {0}. Node contents are:\r\n{1}",
                                                                                         ConfigurationSystemFile, station.OuterXml));
                                }

                                #region StationID

                                int stationID;

                                if (!int.TryParse(station.Attributes["id"].Value, out stationID))
                                {
                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                         "Attribute 'id' inside 'station' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                         ConfigurationSystemFile, station.Attributes["id"].Value));
                                }

                                #endregion

                                string stationName = station.Attributes["name"].Value;

                                foreach (XmlNode building in station.ChildNodes)
                                {
                                    #region Elaborazione nodo Building

                                    if (building.Name.Equals("building", StringComparison.OrdinalIgnoreCase))
                                    {
                                        if ((building.Attributes["id"] == null) || (building.Attributes["id"].Value.Trim().Length == 0) || (building.Attributes["name"] == null) ||
                                            (building.Attributes["name"].Value.Trim().Length == 0))
                                        {
                                            throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                 "Unable to find attributes 'id' or 'name' inside 'building' node, or building id or name are null in file {0}. Node contents are:\r\n{1}",
                                                                                                 ConfigurationSystemFile, building.OuterXml));
                                        }

                                        #region BuildingID

                                        int buildingID;

                                        if (!int.TryParse(building.Attributes["id"].Value, out buildingID))
                                        {
                                            throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                 "Attribute 'id' inside 'building' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                 ConfigurationSystemFile, building.Attributes["id"].Value));
                                        }

                                        #endregion

                                        string buildingName = building.Attributes["name"].Value;

                                        #region BuildingNotes

                                        string buildingNotes = string.Empty;
                                        if (building.Attributes["note"] != null)
                                        {
                                            buildingNotes = building.Attributes["note"].Value;
                                        }

                                        #endregion

                                        foreach (XmlNode location in building.ChildNodes)
                                        {
                                            #region Elaborazione nodo location

                                            if (location.Name.Equals("location", StringComparison.OrdinalIgnoreCase))
                                            {
                                                if ((location.Attributes["id"] == null) || (location.Attributes["id"].Value.Trim().Length == 0) ||
                                                    (location.Attributes["name"] == null) || (location.Attributes["name"].Value.Trim().Length == 0) ||
                                                    (location.Attributes["type"] == null) || (location.Attributes["type"].Value.Trim().Length == 0))
                                                {
                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                         "Unable to find attributes 'id', 'name' or 'type' inside 'location' node, or location name, id or type are null in file {0}. Node contents are:\r\n{1}",
                                                                                                         ConfigurationSystemFile, location.OuterXml));
                                                }

                                                #region LocationID

                                                int locationID;

                                                if (!int.TryParse(location.Attributes["id"].Value, out locationID))
                                                {
                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                         "Attribute 'id' inside 'location' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                         ConfigurationSystemFile, location.Attributes["id"].Value));
                                                }

                                                #endregion

                                                string locationName = location.Attributes["name"].Value;
                                                string locationType = location.Attributes["type"].Value;

                                                #region LocationNotes

                                                string locationNotes = string.Empty;
                                                if (location.Attributes["note"] != null)
                                                {
                                                    locationNotes = location.Attributes["note"].Value;
                                                }

                                                #endregion

                                                locations.Add(new TopographyLocation(stationID, stationName, buildingID, buildingName, buildingNotes, locationID, locationName,
                                                                                     locationType, locationNotes));

                                                existsAtLeastOneLocation = true;
                                            }

                                            #endregion
                                        }
                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                        break;
                    }

                    #endregion
                }
            }
            else
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Invalid device XML configuration in file {0}",
                                                                     ConfigurationSystemFile));
            }

            if (!existsAtLeastOneLocation)
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Unable to find at least one station/building/rack in file {0}.",
                                                                     ConfigurationSystemFile));
            }

            bool existsAtLeastOneDevice = false;

            List<DeviceObject> devices = new List<DeviceObject>();

            foreach (XmlNode rootElement in root.ChildNodes)
            {
                if (rootElement.Name.Equals("system", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (XmlNode server in rootElement.ChildNodes)
                    {
                        if (server.Name.Equals("server", StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (XmlNode region in server.ChildNodes)
                            {
                                if (region.Name.Equals("region", StringComparison.OrdinalIgnoreCase))
                                {
                                    foreach (XmlNode zone in region.ChildNodes)
                                    {
                                        if (zone.Name.Equals("zone", StringComparison.OrdinalIgnoreCase))
                                        {
                                            foreach (XmlNode node in zone.ChildNodes)
                                            {
                                                if (node.Name.Equals("node", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    foreach (XmlNode device in node.ChildNodes)
                                                    {
                                                        if (device.Name.Equals("device", StringComparison.OrdinalIgnoreCase))
                                                        {
                                                            #region supervisorId

                                                            // Il default è 0, ossia il Supervisor originale
                                                            int supervisorId;

                                                            if (device.Attributes["supervisor_id"] == null)
                                                            {
                                                                // In mancanza dell'attributo, il default è 0, ossia il Supervisor originale
                                                                supervisorId = 0;
                                                            }
                                                            else
                                                            {
                                                                if (!int.TryParse(device.Attributes["supervisor_id"].Value, out supervisorId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'supervisor_id' inside 'device' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile,
                                                                                                                         device.Attributes["supervisor_id"].Value));
                                                                }
                                                            }

                                                            #endregion

                                                            // Processiamo solo i device che riguardano la gestione di questo Supervisor Snmp
                                                            if (supervisorId == Settings.Default.SnmpSupervisorID)
                                                            {
                                                                #region Verifiche obbligatorietà su nodo Device

                                                                if ((device.Attributes["DevID"] == null) || (device.Attributes["DevID"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["name"] == null) || (device.Attributes["name"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["station"] == null) || (device.Attributes["station"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["building"] == null) || (device.Attributes["building"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["location"] == null) || (device.Attributes["location"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["type"] == null) || (device.Attributes["type"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["addr"] == null) || (device.Attributes["addr"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["SN"] == null) || (device.Attributes["SN"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["profile"] == null) || (device.Attributes["profile"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["active"] == null) || (device.Attributes["active"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["scheduled"] == null) || (device.Attributes["scheduled"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["position"] == null) || (device.Attributes["position"].Value.Trim().Length == 0) ||
                                                                    (device.Attributes["port"] == null) || (device.Attributes["port"].Value.Trim().Length == 0))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Unable to find attributes 'DevID', 'name', 'station', 'building', 'location', 'type', 'addr', 'SN', 'port', 'profile', 'active', 'scheduled' or 'position' inside 'device' node, or at least one attribute is null in file {0}. Node contents are:\r\n{1}",
                                                                                                                         ConfigurationSystemFile, device.OuterXml));
                                                                }

                                                                #endregion

                                                                #region Verifiche obbligatorietà su nodo Node

                                                                if ((node.Attributes["NodID"] == null) || (node.Attributes["NodID"].Value.Trim().Length == 0) ||
                                                                    (node.Attributes["name"] == null) || (node.Attributes["name"].Value.Trim().Length == 0))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Unable to find attributes 'NodID' or 'name' inside 'node' node, or at least one attribute is null in file {0}. Node contents are:\r\n{1}",
                                                                                                                         ConfigurationSystemFile, node.OuterXml));
                                                                }

                                                                #endregion

                                                                #region Verifiche obbligatorietà su nodo Zone

                                                                if ((zone.Attributes["ZonID"] == null) || (zone.Attributes["ZonID"].Value.Trim().Length == 0) ||
                                                                    (zone.Attributes["name"] == null) || (zone.Attributes["name"].Value.Trim().Length == 0))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Unable to find attributes 'ZonID' or 'name' inside 'zone' node, or at least one attribute is null in file {0}. Node contents are:\r\n{1}",
                                                                                                                         ConfigurationSystemFile, zone.OuterXml));
                                                                }

                                                                #endregion

                                                                #region Verifiche obbligatorietà su nodo Region

                                                                if ((region.Attributes["RegID"] == null) || (region.Attributes["RegID"].Value.Trim().Length == 0) ||
                                                                    (region.Attributes["name"] == null) || (region.Attributes["name"].Value.Trim().Length == 0))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Unable to find attributes 'RegID' or 'name' inside 'region' node, or at least one attribute is null in file {0}. Node contents are:\r\n{1}",
                                                                                                                         ConfigurationSystemFile, region.OuterXml));
                                                                }

                                                                #endregion

                                                                #region Verifiche obbligatorietà su nodo Server

                                                                if ((server.Attributes["SrvID"] == null) || (server.Attributes["SrvID"].Value.Trim().Length == 0) ||
                                                                    (server.Attributes["host"] == null) || (server.Attributes["host"].Value.Trim().Length == 0) ||
                                                                    (server.Attributes["name"] == null) || (server.Attributes["name"].Value.Trim().Length == 0))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Unable to find attributes 'SrvID', 'host' or 'name' inside 'server' node, or at least one attribute is null in file {0}. Node contents are:\r\n{1}",
                                                                                                                         ConfigurationSystemFile, server.OuterXml));
                                                                }

                                                                #endregion

                                                                #region originalDeviceId

                                                                ushort originalDeviceId;

                                                                if (!ushort.TryParse(device.Attributes["DevID"].Value, out originalDeviceId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'DevID' inside 'device' node in file {0}, does not contain a positive integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, device.Attributes["DevID"].Value));
                                                                }

                                                                #endregion

                                                                string deviceName = device.Attributes["name"].Value;

                                                                #region stationId

                                                                int stationId;

                                                                if (!int.TryParse(device.Attributes["station"].Value, out stationId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'station' inside 'device' in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, device.Attributes["station"].Value));
                                                                }

                                                                #endregion

                                                                #region buildingId

                                                                int buildingId;

                                                                if (!int.TryParse(device.Attributes["building"].Value, out buildingId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'building' inside 'device' in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile,
                                                                                                                         device.Attributes["building"].Value));
                                                                }

                                                                #endregion

                                                                #region locationId

                                                                int locationId;

                                                                if (!int.TryParse(device.Attributes["location"].Value, out locationId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'location' inside 'device' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile,
                                                                                                                         device.Attributes["location"].Value));
                                                                }

                                                                #endregion

                                                                string deviceType = device.Attributes["type"].Value;

                                                                #region deviceAddr

                                                                IPAddress deviceAddr;

                                                                if (!IPAddress.TryParse(device.Attributes["addr"].Value, out deviceAddr))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'addr' inside 'device' node in file {0}, does not contain a valid IP address. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, device.Attributes["addr"].Value));
                                                                }

                                                                #endregion

                                                                string serialNumber = device.Attributes["SN"].Value;

                                                                #region portId

                                                                int portId;

                                                                if (!int.TryParse(device.Attributes["port"].Value, out portId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'port' inside 'device' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, device.Attributes["port"].Value));
                                                                }

                                                                #endregion

                                                                #region profileId

                                                                int profileId;

                                                                if (!int.TryParse(device.Attributes["profile"].Value, out profileId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'profile' inside 'device' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, device.Attributes["profile"].Value));
                                                                }

                                                                #endregion

                                                                #region active

                                                                bool activeValue;

                                                                if (!bool.TryParse(device.Attributes["active"].Value.ToLowerInvariant(), out activeValue))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'active' inside 'device' node in file {0}, does not contain a valid boolean value (true/false). Current value: {1}",
                                                                                                                         ConfigurationSystemFile, device.Attributes["active"].Value));
                                                                }

                                                                byte active = (byte) (activeValue ? 1 : 0);

                                                                #endregion

                                                                #region scheduled

                                                                bool scheduledValue;

                                                                if (!bool.TryParse(device.Attributes["scheduled"].Value.ToLowerInvariant(), out scheduledValue))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'scheduled' inside 'device' node in file {0}, does not contain a valid boolean value (true/false). Current value: {1}",
                                                                                                                         ConfigurationSystemFile,
                                                                                                                         device.Attributes["scheduled"].Value));
                                                                }

                                                                byte scheduled = (byte) (scheduledValue ? 1 : 0);

                                                                #endregion

                                                                #region snmp_community

                                                                string snmpCommunity;

                                                                if (device.Attributes["snmp_community"] == null)
                                                                {
                                                                    // Nel caso non sia presente l'attributo nel file System.xml, ripieghiamo sul
                                                                    // default da configurazione
                                                                    snmpCommunity = Settings.Default.SnmpDefaultCommunity;
                                                                }
                                                                else
                                                                {
                                                                    snmpCommunity = device.Attributes["snmp_community"].Value.Trim();
                                                                }

                                                                #endregion

                                                                #region snmp_port

                                                                int snmpPort;

                                                                if (device.Attributes["snmp_port"] == null)
                                                                {
                                                                    // Nel caso non sia presente l'attributo nel file System.xml, ripieghiamo sul
                                                                    // default da configurazione
                                                                    snmpPort = Settings.Default.SnmpDefaultPort;
                                                                }
                                                                else
                                                                {
                                                                    if (!int.TryParse(device.Attributes["snmp_port"].Value, out snmpPort))
                                                                    {
                                                                        throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                             "Attribute 'snmp_port' inside 'device' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                             ConfigurationSystemFile,
                                                                                                                             device.Attributes["snmp_port"].Value));
                                                                    }
                                                                }

                                                                #endregion

                                                                #region rackPosition (column / row)

                                                                int rackPositionColumn;
                                                                int rackPositionRow;

                                                                string rackPositionString = device.Attributes["position"].Value.Trim();
                                                                string[] rackPositions = rackPositionString.Split(',');

                                                                if (rackPositions.Length == 2)
                                                                {
                                                                    if (!int.TryParse(rackPositions[0], out rackPositionColumn))
                                                                    {
                                                                        throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                             "Attribute 'position' inside 'device' node in file {0}, does not contain two numeric values comma separated. Current value: {1}",
                                                                                                                             ConfigurationSystemFile,
                                                                                                                             device.Attributes["position"].Value));
                                                                    }

                                                                    if (!int.TryParse(rackPositions[1], out rackPositionRow))
                                                                    {
                                                                        throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                             "Attribute 'position' inside 'device' node in file {0}, does not contain two numeric values comma separated. Current value: {1}",
                                                                                                                             ConfigurationSystemFile,
                                                                                                                             device.Attributes["position"].Value));
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'position' inside 'device' node in file {0}, does not contain two numeric values comma separated. Current value: {1}",
                                                                                                                         ConfigurationSystemFile,
                                                                                                                         device.Attributes["position"].Value));
                                                                }

                                                                #endregion

                                                                #region originalRegionId

                                                                ushort originalRegionId;

                                                                if (!ushort.TryParse(region.Attributes["RegID"].Value, out originalRegionId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'RegID' inside 'region' node in file {0}, does not contain a positive integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, region.Attributes["RegID"].Value));
                                                                }

                                                                #endregion

                                                                string regionName = region.Attributes["name"].Value;

                                                                #region originalZoneId

                                                                ushort originalZoneId;

                                                                if (!ushort.TryParse(zone.Attributes["ZonID"].Value, out originalZoneId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'ZonID' inside 'region' node in file {0}, does not contain a positive integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, zone.Attributes["ZonID"].Value));
                                                                }

                                                                #endregion

                                                                string zoneName = zone.Attributes["name"].Value;

                                                                #region originalNodeId

                                                                ushort originalNodeId;

                                                                if (!ushort.TryParse(node.Attributes["NodID"].Value, out originalNodeId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'NodID' inside 'node' node in file {0}, does not contain a positive integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, node.Attributes["NodID"].Value));
                                                                }

                                                                #endregion

                                                                string nodeName = node.Attributes["name"].Value;

                                                                #region serverId

                                                                int serverId;

                                                                if (!int.TryParse(server.Attributes["SrvID"].Value, out serverId))
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Attribute 'SrvID' inside 'server' node in file {0}, does not contain an integer value. Current value: {1}",
                                                                                                                         ConfigurationSystemFile, server.Attributes["SrvID"].Value));
                                                                }

                                                                #endregion

                                                                string serverHost = server.Attributes["host"].Value;
                                                                string serverName = server.Attributes["name"].Value;

                                                                #region FakeLocalData

                                                                bool fakeLocalData;

                                                                if (device.Attributes["fake_local_data"] != null)
                                                                {
                                                                    if (!bool.TryParse(device.Attributes["fake_local_data"].Value, out fakeLocalData))
                                                                    {
                                                                        throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                             "Attribute 'fake_local_data' inside 'device' node in file {0}, does not contain a valid boolean value (true/false). Current value: {1}",
                                                                                                                             ConfigurationSystemFile,
                                                                                                                             device.Attributes["fake_local_data"].Value));
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    fakeLocalData = false;
                                                                }

                                                                #endregion

                                                                TopographyLocation location = null;

                                                                foreach (TopographyLocation topography in locations)
                                                                {
                                                                    location = topography.GetTopography(stationId, buildingId, locationId);
                                                                    if (location != null)
                                                                    {
                                                                        break;
                                                                    }
                                                                }

                                                                if (location != null)
                                                                {
                                                                    devices.Add(new DeviceObject(fakeLocalData,
                                                                                                 new DeviceIdentifier(originalDeviceId, originalNodeId, originalZoneId,
                                                                                                                      originalRegionId).DeviceId, deviceName, deviceType, deviceAddr,
                                                                                                 new RegionIdentifier(originalRegionId).RegionId, regionName,
                                                                                                 new ZoneIdentifier(originalZoneId, originalRegionId).ZoneId, zoneName,
                                                                                                 new NodeIdentifier(originalNodeId, originalZoneId, originalRegionId).NodeId,
                                                                                                 nodeName, serverId, serverHost, serverName, serialNumber, portId, profileId, active,
                                                                                                 scheduled, rackPositionColumn, rackPositionRow, location, snmpCommunity, snmpPort));

                                                                    existsAtLeastOneDevice = true;
                                                                }
                                                                else
                                                                {
                                                                    throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                         "Unable to find a station/building/rack bound to current device in file {0}. Current device: {1}",
                                                                                                                         ConfigurationSystemFile, device.OuterXml));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }

            if (!existsAtLeastOneDevice)
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Unable to find at least one configured device in file {0}",
                                                                     ConfigurationSystemFile));
            }

            return devices.AsReadOnly();
        }

        #region Metodi privati

        private static XmlNode GetDefinitionXmlRootElement()
        {
            if (String.IsNullOrEmpty(ConfigurationSystemFile))
            {
                throw new ConfigurationSystemException("Undefined configuration file name containing devices list to monitor");
            }

            if (!FileUtility.CheckFileCanRead(ConfigurationSystemFile))
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Unable to load configuration file containing devices list: {0}",
                                                                     ConfigurationSystemFile));
            }

            XmlDocument doc = new XmlDocument();
            XmlNode root;

            try
            {
                doc.Load(ConfigurationSystemFile);
                root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition", ConfigurationSystemFile), ex);
            }
            catch (ArgumentException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition", ConfigurationSystemFile), ex);
            }
            catch (IOException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition", ConfigurationSystemFile), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition", ConfigurationSystemFile), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition", ConfigurationSystemFile), ex);
            }
            catch (SecurityException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition", ConfigurationSystemFile), ex);
            }

            if (root == null)
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                                                     ConfigurationSystemFile));
            }

            if (!root.Name.Equals("telefin", StringComparison.OrdinalIgnoreCase))
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                     "Unable to find element 'telefin' as root node in configuration file {0}.",
                                                                     ConfigurationSystemFile));
            }

            return root;
        }

        #endregion
    }
}
﻿namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Rappresenta il tipo di valori ad intervallo Minimo-Massimo
    /// </summary>
    public class DBStreamFieldValueRange
    {
        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "minValue">Valore minimo</param>
        /// <param name = "maxValue">Valore massimo</param>
        public DBStreamFieldValueRange(long? minValue, long? maxValue)
        {
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "value">Valore singolo (inteso come minimo e massimo uguali)</param>
        public DBStreamFieldValueRange(long value)
        {
            this.MinValue = value;
            this.MaxValue = value;
        }

        /// <summary>
        ///   Valore minimo
        /// </summary>
        public long? MinValue { get; private set; }

        /// <summary>
        ///   Valore massimo
        /// </summary>
        public long? MaxValue { get; private set; }
    }
}
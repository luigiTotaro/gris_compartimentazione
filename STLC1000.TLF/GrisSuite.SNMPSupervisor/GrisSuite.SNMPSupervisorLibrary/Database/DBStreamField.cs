﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text.RegularExpressions;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Rappresenta il singolo Field da persistere su database
    /// </summary>
    public class DBStreamField
    {
        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "fieldId">Id del Field</param>
        /// <param name = "arrayId">Posizione del valore nel vettore, nel caso la query SNMP restituisca più valori</param>
        /// <param name = "fieldName">Nome del Field</param>
        /// <param name = "fieldOid">OID della query SNMP</param>
        /// <param name = "fieldTableOid">OID della query SNMP tabellare</param>
        /// <param name = "fieldCorrelationTableOid">OID della query SNMP tabellare correlata</param>
        /// <param name = "tableRowIndex">Indice della riga della tabella restituita dalla query SNMP</param>
        /// <param name = "evaluationFormula">Codice C# per l'elaborazione della riga della tabella restituita dalla query SNMP</param>
        /// <param name = "correlationEvaluationFormula">Codice C# per l'elaborazione della riga della tabella correlata restituita dalla query SNMP</param>
        /// <param name = "filterColumnIndex">Indice della colonna della tabella SNMP sui cui eseguire le ricerche</param>
        /// <param name = "correlationFilterColumnIndex">Indice della colonna della tabella SNMP correlata sui cui eseguire le ricerche</param>
        /// <param name = "filterColumnRegex">Espressione regolare da applicare alla colonna della tabella SNMP, per ottenere la riga cercata</param>
        /// <param name = "formatExpression">Espressione di formattazione da applicare al valore</param>
        /// <param name = "tableIndex">Indice della riga di tabella dinamica a cui questo stream field si riferisce</param>
        /// <param name = "scalarTableCounterOid">OID della query SNMP scalare che indica il numero di righe per una tabella dinamica</param>
        /// <param name = "excludedFromSeverityComputation">Indica se lo stream field e la sua severity devono essere tenuti in considerazione nel calcolo della severità dello stream padre</param>
        /// <param name = "noDataAvailableForcedState">Informazioni per la valorizzazione dello Stream nel caso che non siano restituiti dati nelle query SNMP</param>
        /// <param name="hideIfNoDataAvailable">Indica se lo stream sarà persistito su database quando la query SNMP non ritorna un valore</param>
        public DBStreamField(int fieldId, int arrayId, string fieldName, ObjectIdentifier fieldOid, ObjectIdentifier fieldTableOid, ObjectIdentifier fieldCorrelationTableOid,
                             int? tableRowIndex, string evaluationFormula, string correlationEvaluationFormula, int? filterColumnIndex, int? correlationFilterColumnIndex,
                             Regex filterColumnRegex, FormatExpression formatExpression, int? tableIndex, ObjectIdentifier scalarTableCounterOid,
                             bool excludedFromSeverityComputation, NoDataAvailableForcedState noDataAvailableForcedState,
                             bool hideIfNoDataAvailable)
        {
            this.FieldId = fieldId;
            this.ArrayId = arrayId;
            this.Name = fieldName;
            this.FieldOid = fieldOid;
            this.FieldTableOid = fieldTableOid;
            this.FieldCorrelationTableOid = fieldCorrelationTableOid;
            this.TableRowIndex = tableRowIndex;
            this.TableEvaluationFormula = evaluationFormula;
            this.TableCorrelationEvaluationFormula = correlationEvaluationFormula;
            this.TableFilterColumnIndex = filterColumnIndex;
            this.TableCorrelationFilterColumnIndex = correlationFilterColumnIndex;
            this.TableFilterColumnRegex = filterColumnRegex;
            this.DbStreamFieldValueDescriptions = new Collection<DBStreamFieldValueDescription>();
            this.ValueFormatExpression = formatExpression;
            this.PersistOnDatabase = true;
            this.TableIndex = tableIndex;
            this.ScalarTableCounterOid = scalarTableCounterOid;
            this.ExcludedFromSeverityComputation = excludedFromSeverityComputation;
            this.NoSnmpDataAvailableForcedState = noDataAvailableForcedState;
            this.HideIfNoDataAvailable = hideIfNoDataAvailable;
        }

        #region Proprietà pubbliche

        /// <summary>
        ///   Severità relativa al Field
        /// </summary>
        public Severity SeverityLevel { get; set; }

        /// <summary>
        ///   Decodifica descrittiva del valore del Field
        /// </summary>
        public string ValueDescription { private get; set; }

        /// <summary>
        ///   Decodifica descrittiva del valore del Field, concatenata alla severità (solo nel caso non sia sconosciuta)
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                if (this.SeverityLevel == Severity.Unknown)
                {
                    return this.ValueDescription;
                }

                return this.ValueDescription + "=" + (byte) this.SeverityLevel;
            }
        }

        /// <summary>
        ///   Id del Field
        /// </summary>
        public int FieldId { get; private set; }

        /// <summary>
        ///   OID della query SNMP
        /// </summary>
        public ObjectIdentifier FieldOid { get; private set; }

        /// <summary>
        ///   OID della query SNMP tabellare
        /// </summary>
        public ObjectIdentifier FieldTableOid { get; private set; }

        /// <summary>
        ///   OID della query SNMP tabellare correlata
        /// </summary>
        public ObjectIdentifier FieldCorrelationTableOid { get; private set; }

        /// <summary>
        ///   Valore del Field, come letto dalla query SNMP
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        ///   Lista di valori
        /// </summary>
        public IList<Variable> RawData { private get; set; }

        /// <summary>
        ///   Posizione del valore nel vettore
        /// </summary>
        public int ArrayId { get; private set; }

        /// <summary>
        ///   Nome del Field
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///   Lista di descrizioni per la decodifica del valore e della severità
        /// </summary>
        public Collection<DBStreamFieldValueDescription> DbStreamFieldValueDescriptions { get; private set; }

        /// <summary>
        ///   Indice della riga della tabella restituita dalla query SNMP
        /// </summary>
        public int? TableRowIndex { get; private set; }

        /// <summary>
        ///   Codice C# per l'elaborazione della riga della tabella restituita dalla query SNMP
        /// </summary>
        public string TableEvaluationFormula { get; private set; }

        /// <summary>
        ///   Codice C# per l'elaborazione della riga della tabella correlata restituita dalla query SNMP
        /// </summary>
        public string TableCorrelationEvaluationFormula { get; private set; }

        /// <summary>
        ///   Indice della colonna della tabella SNMP sui cui eseguire le ricerche
        /// </summary>
        public int? TableFilterColumnIndex { get; private set; }

        /// <summary>
        ///   Indice della colonna della tabella SNMP correlata sui cui eseguire le ricerche
        /// </summary>
        public int? TableCorrelationFilterColumnIndex { get; private set; }

        /// <summary>
        ///   Espressione regolare da applicare alla colonna della tabella SNMP, per ottenere la riga cercata
        /// </summary>
        public Regex TableFilterColumnRegex { get; private set; }

        /// <summary>
        ///   Espressione di formattazione da applicare al valore
        /// </summary>
        public FormatExpression ValueFormatExpression { get; private set; }

        /// <summary>
        ///   Indica se lo stream field deve essere salvato sul database
        /// </summary>
        /// <remarks>
        ///   E' possibile che uno stream field di una tabella dinamica non abbia senso,
        ///   a fronte di una valutazione del contatore che riguarda le righe della tabella
        /// </remarks>
        public bool PersistOnDatabase { get; set; }

        /// <summary>
        ///   Indice della riga di tabella dinamica a cui questo stream field si riferisce
        /// </summary>
        public int? TableIndex { get; private set; }

        /// <summary>
        ///   OID della query SNMP scalare che indica il numero di righe per una tabella dinamica
        /// </summary>
        public ObjectIdentifier ScalarTableCounterOid { get; private set; }

        /// <summary>
        ///   Indica se lo stream field e la sua severity devono essere tenuti in considerazione nel calcolo della severità dello stream padre
        /// </summary>
        public bool ExcludedFromSeverityComputation { get; private set; }

        /// <summary>
        ///   Informazioni per la valorizzazione dello Stream nel caso che non siano restituiti dati nelle query SNMP
        /// </summary>
        public NoDataAvailableForcedState NoSnmpDataAvailableForcedState { get; private set; }

        /// <summary>
        ///   Contiene l'eventuale eccezione generata durante il popolamento con dati del field
        /// </summary>
        public Exception LastError { get; set; }

        /// <summary>
        ///   Indica se lo stream sarà persistito su database quando la query SNMP non ritorna un valore
        /// </summary>
        public bool HideIfNoDataAvailable { get; private set; }

        #endregion

        private enum EvaluationNeeded
        {
            Unknown,
            StringComparison,
            IntComparison,
            RangeComparison
        }

        private EvaluationNeeded ChooseEvaluationMethod()
        {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
            {
                if (this.DbStreamFieldValueDescriptions.Count == 1)
                {
                    evaluationNeeded = GetEvaluationNeeded(this.DbStreamFieldValueDescriptions[0]);
                }
                else
                {
                    foreach (DBStreamFieldValueDescription dbStreamFieldValueDescription in
                        this.DbStreamFieldValueDescriptions)
                    {
                        if (!dbStreamFieldValueDescription.IsDefault)
                        {
                            // Consideriamo solo il primo tipo - casi di tipi di valori etereogenei non sono supportati
                            evaluationNeeded = GetEvaluationNeeded(dbStreamFieldValueDescription);
                            break;
                        }
                    }
                }
            }

            return evaluationNeeded;
        }

        private static EvaluationNeeded GetEvaluationNeeded(DBStreamFieldValueDescription dbStreamFieldValueDescription)
        {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((dbStreamFieldValueDescription.RangeValueToCompare != null) &&
                ((dbStreamFieldValueDescription.RangeValueToCompare.MinValue.HasValue) || (dbStreamFieldValueDescription.RangeValueToCompare.MaxValue.HasValue)))
            {
                evaluationNeeded = EvaluationNeeded.RangeComparison;
            }
            else if (dbStreamFieldValueDescription.ExactIntValueToCompare.HasValue)
            {
                evaluationNeeded = EvaluationNeeded.IntComparison;
            }
            else if (!String.IsNullOrEmpty(dbStreamFieldValueDescription.ExactStringValueToCompare))
            {
                evaluationNeeded = EvaluationNeeded.StringComparison;
            }
            return evaluationNeeded;
        }

        /// <summary>
        ///   Esegue la valutazione del valore contenuto nel campo, in base alle regole configurate
        /// </summary>
        public void Evaluate()
        {
            if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
            {
                if (this.DbStreamFieldValueDescriptions.Count == 1)
                {
                    this.ParseDefaultSingleValue(this.DbStreamFieldValueDescriptions[0].DescriptionIfMatch);
                }
                else
                {
                    switch (this.ChooseEvaluationMethod())
                    {
                        case EvaluationNeeded.Unknown:
                            this.ParseStringValue();
                            break;
                        case EvaluationNeeded.StringComparison:
                            this.ParseStringValue();
                            break;
                        case EvaluationNeeded.IntComparison:
                            this.ParseLongValue();
                            break;
                        case EvaluationNeeded.RangeComparison:
                            this.ParseRangeValue();
                            break;
                    }
                }
            }
        }

        #region Metodi privati per l'elaborazione dei dati relativi ai vari Fields degli Stream, con decodifica di descrizione e severità

        /// <summary>
        ///   Elabora i dati di tipo stringa relativi ai vari Fields degli Streams
        /// </summary>
        private void ParseStringValue()
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown();
                    }
                    else
                    {
                        DBStreamFieldValueDescription defaultDescription = null;
                        bool fieldValueSet = false;

                        foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                        {
                            if (String.Equals(valueString, description.ExactStringValueToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                this.SetFieldDataString(description.DescriptionIfMatch, valueString, description.SeverityLevelIfMatch);
                                fieldValueSet = true;
                                break;
                            }

                            if (description.IsDefault)
                            {
                                // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                defaultDescription = description;
                            }
                        }

                        if (!fieldValueSet)
                        {
                            if (defaultDescription != null)
                            {
                                this.SetFieldDataString(defaultDescription.DescriptionIfMatch, valueString, defaultDescription.SeverityLevelIfMatch);
                            }
                            else
                            {
                                this.SetFieldStateToUnknown();
                            }
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown();
            }
        }

        /// <summary>
        ///   Elabora i dati di tipo intero relativi ai vari Fields degli Streams
        /// </summary>
        private void ParseLongValue()
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.GetRawStringData(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown();
                    }
                    else
                    {
                        bool isTimeTicksData = false;

                        if (this.RawData[this.ArrayId].Data.TypeCode == SnmpType.TimeTicks)
                        {
                            isTimeTicksData = true;
                            valueString = ((TimeTicks) this.RawData[this.ArrayId].Data).ToUInt32().ToString(CultureInfo.InvariantCulture);
                        }

                        long value;
                        if (long.TryParse(valueString, out value))
                        {
                            DBStreamFieldValueDescription defaultDescription = null;
                            bool fieldValueSet = false;

                            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                            {
                                if (value == description.ExactIntValueToCompare)
                                {
                                    if (isTimeTicksData)
                                    {
                                        this.SetFieldDataString(description.DescriptionIfMatch, SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]),
                                                                description.SeverityLevelIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataLong(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                    }
                                    fieldValueSet = true;
                                    break;
                                }

                                if (description.IsDefault)
                                {
                                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                    defaultDescription = description;
                                }
                            }

                            if (!fieldValueSet)
                            {
                                if (defaultDescription != null)
                                {
                                    if (isTimeTicksData)
                                    {
                                        this.SetFieldDataString(defaultDescription.DescriptionIfMatch, SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]),
                                                                defaultDescription.SeverityLevelIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataLong(defaultDescription.DescriptionIfMatch, value, defaultDescription.SeverityLevelIfMatch);
                                    }
                                }
                                else
                                {
                                    this.SetFieldStateToUnknown();
                                }
                            }
                        }
                        else
                        {
                            this.SetFieldStateToUnknown();
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown();
            }
        }

        /// <summary>
        ///   Elabora i dati di tipo intervallo relativi ai vari Fields degli Streams
        /// </summary>
        private void ParseRangeValue()
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.GetRawStringData(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown();
                    }
                    else
                    {
                        bool isTimeTicksData = false;

                        if (this.RawData[this.ArrayId].Data.TypeCode == SnmpType.TimeTicks)
                        {
                            isTimeTicksData = true;
                            valueString = ((TimeTicks) this.RawData[this.ArrayId].Data).ToUInt32().ToString(CultureInfo.InvariantCulture);
                        }

                        long value;
                        if (long.TryParse(valueString, out value))
                        {
                            DBStreamFieldValueDescription defaultDescription = null;
                            bool fieldValueSet = false;

                            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                            {
                                if ((description.RangeValueToCompare.MinValue.HasValue) && (description.RangeValueToCompare.MaxValue.HasValue))
                                {
                                    if ((value >= description.RangeValueToCompare.MinValue.Value) && (value <= description.RangeValueToCompare.MaxValue.Value))
                                    {
                                        if (isTimeTicksData)
                                        {
                                            this.SetFieldDataString(description.DescriptionIfMatch, SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]),
                                                                    description.SeverityLevelIfMatch);
                                        }
                                        else
                                        {
                                            this.SetFieldDataLong(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                        }
                                        fieldValueSet = true;
                                        break;
                                    }
                                }
                                else if ((description.RangeValueToCompare.MinValue.HasValue) && (!description.RangeValueToCompare.MaxValue.HasValue))
                                {
                                    if (value >= description.RangeValueToCompare.MinValue.Value)
                                    {
                                        if (isTimeTicksData)
                                        {
                                            this.SetFieldDataString(description.DescriptionIfMatch, SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]),
                                                                    description.SeverityLevelIfMatch);
                                        }
                                        else
                                        {
                                            this.SetFieldDataLong(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                        }
                                        fieldValueSet = true;
                                        break;
                                    }
                                }
                                else if ((!description.RangeValueToCompare.MinValue.HasValue) && (description.RangeValueToCompare.MaxValue.HasValue))
                                {
                                    if (value <= description.RangeValueToCompare.MaxValue.Value)
                                    {
                                        if (isTimeTicksData)
                                        {
                                            this.SetFieldDataString(description.DescriptionIfMatch, SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]),
                                                                    description.SeverityLevelIfMatch);
                                        }
                                        else
                                        {
                                            this.SetFieldDataLong(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                        }
                                        fieldValueSet = true;
                                        break;
                                    }
                                }

                                if (description.IsDefault)
                                {
                                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                    defaultDescription = description;
                                }
                            }

                            if (!fieldValueSet)
                            {
                                if (defaultDescription != null)
                                {
                                    if (isTimeTicksData)
                                    {
                                        this.SetFieldDataString(defaultDescription.DescriptionIfMatch, SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]),
                                                                defaultDescription.SeverityLevelIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataLong(defaultDescription.DescriptionIfMatch, value, defaultDescription.SeverityLevelIfMatch);
                                    }
                                }
                                else
                                {
                                    this.SetFieldStateToUnknown();
                                }
                            }
                        }
                        else
                        {
                            this.SetFieldStateToUnknown();
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown();
            }
        }

        /// <summary>
        ///   Elabora un dato unico relativo al Field degli Streams
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore passato</param>
        private void ParseDefaultSingleValue(string valueDescription)
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string value = SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]);

                    if ((value != null) && (value.Trim().Length > 0))
                    {
                        if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
                        {
                            if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.Long)
                            {
                                string valueString = SnmpUtility.GetRawStringData(this.RawData[this.ArrayId]);

                                if (valueString == null)
                                {
                                    this.SetFieldStateToUnknown();
                                }
                                else
                                {
                                    long valueLong;
                                    if (long.TryParse(valueString, out valueLong))
                                    {
                                        this.SetFieldDataInternal(valueDescription,
                                                                  (valueLong*this.ValueFormatExpression.Factor + this.ValueFormatExpression.Addend).ToString(
                                                                      this.ValueFormatExpression.Format), Severity.Ok);
                                    }
                                    else
                                    {
                                        this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, Severity.Ok);
                                    }
                                }
                            }
                            else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                            {
                                this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, Severity.Ok);
                            }
                        }
                        else
                        {
                            this.SetFieldDataString(valueDescription, value, Severity.Ok);
                        }
                    }
                    else
                    {
                        this.SetFieldStateToUnknown();
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown();
            }
        }

        /// <summary>
        ///   Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore da impostare</param>
        /// <param name = "value">Valore da impostare</param>
        /// <param name = "severity">Severità da impostare</param>
        private void SetFieldDataLong(string valueDescription, long value, Severity severity)
        {
            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.Long)
                {
                    this.SetFieldDataInternal(valueDescription,
                                              (value*this.ValueFormatExpression.Factor + this.ValueFormatExpression.Addend).ToString(this.ValueFormatExpression.Format), severity);
                }
                else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity);
                }
            }
            else
            {
                this.SetFieldDataInternal(valueDescription, value.ToString(), severity);
            }
        }

        /// <summary>
        ///   Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore da impostare</param>
        /// <param name = "value">Valore da impostare</param>
        /// <param name = "severity">Severità da impostare</param>
        private void SetFieldDataString(string valueDescription, string value, Severity severity)
        {
            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity);
                }
            }
            else
            {
                this.SetFieldDataInternal(valueDescription, value, severity);
            }
        }

        /// <summary>
        ///   Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore da impostare</param>
        /// <param name = "value">Valore da impostare</param>
        /// <param name = "severity">Severità da impostare</param>
        private void SetFieldDataInternal(string valueDescription, string value, Severity severity)
        {
            this.SeverityLevel = severity;
            this.ValueDescription = valueDescription;
            this.Value = value;
        }

        /// <summary>
        ///   Imposta il valore del Field nel caso il valore sia nullo, usando i dati di forzatura se presenti
        /// </summary>
        private void SetFieldStateToUnknown()
        {
            if (this.LastError != null)
            {
                this.SetFieldDataInternal(Settings.Default.ErrorOnDataRetrievalMessage, Settings.Default.ErrorOnDataRetrievalValue, Severity.Warning);
            }
            else if (this.NoSnmpDataAvailableForcedState != null)
            {
                this.SetFieldDataInternal(this.NoSnmpDataAvailableForcedState.ForcedValueDescription, this.NoSnmpDataAvailableForcedState.ForcedValue,
                                          this.NoSnmpDataAvailableForcedState.ForcedSeverity);
            }
            else
            {
                this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue, Severity.Unknown);
            }
        }

        /// <summary>
        ///   Imposta il valore del Field nel caso si sia richiesto un valore con indice non disponibile
        /// </summary>
        private void SetFieldStateToInvalidArrayIndex()
        {
            this.SetFieldDataInternal(
                string.Format(CultureInfo.InvariantCulture, "Array index {0} is not valid, based on available data size {1}", this.ArrayId, this.RawData.Count),
                Settings.Default.NoDataAvailableValue, Severity.Unknown);
        }

        #endregion
    }
}
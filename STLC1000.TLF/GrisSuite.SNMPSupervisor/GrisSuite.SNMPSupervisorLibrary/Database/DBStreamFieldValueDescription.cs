﻿namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Rappresenta gli oggetti indicati per la decodifica dei valori SNMP
    /// </summary>
    public class DBStreamFieldValueDescription
    {
        /// <summary>
        ///   Costruttore privato
        /// </summary>
        /// <param name = "field">DBStreamField da valutare</param>
        /// <param name = "isDefault">Indica se è il valore predefinito</param>
        /// <param name = "descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name = "severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name = "exactStringValueToCompare">Valore di tipo stringa da confrontare con quello recuperato dalla query SNMP</param>
        /// <param name = "exactIntValueToCompare">Valore di tipo intero da confrontare con quello recuperato dalla query SNMP</param>
        /// <param name = "rangeValueToCompare">Valore di tipo intervallo da confrontare con quello recuperato dalla query SNMP</param>
        public DBStreamFieldValueDescription(DBStreamField field, bool isDefault, string descriptionIfMatch, Severity severityLevelIfMatch, string exactStringValueToCompare,
                                             long? exactIntValueToCompare, DBStreamFieldValueRange rangeValueToCompare)
        {
            this.Field = field;
            this.IsDefault = isDefault;
            this.DescriptionIfMatch = descriptionIfMatch;
            this.SeverityLevelIfMatch = severityLevelIfMatch;
            this.ExactStringValueToCompare = exactStringValueToCompare;
            this.ExactIntValueToCompare = exactIntValueToCompare;

            this.RangeValueToCompare = rangeValueToCompare ?? new DBStreamFieldValueRange(null, null);
        }

        /// <summary>
        ///   Field relativo alla decodifica dei valori
        /// </summary>
        public DBStreamField Field { get; private set; }

        /// <summary>
        ///   Indica se il valore è quello predefinito
        /// </summary>
        public bool IsDefault { get; private set; }

        /// <summary>
        ///   Descrizione assegnata al valore del Field nel caso di corrispondenza con le regole indicate
        /// </summary>
        public string DescriptionIfMatch { get; private set; }

        /// <summary>
        ///   Severità assegnata al valore del Field nel caso di corrispondenza con le regole indicate
        /// </summary>
        public Severity SeverityLevelIfMatch { get; private set; }

        /// <summary>
        ///   Valore statico di tipo stringa da confrontare con il valore recuperato
        /// </summary>
        public string ExactStringValueToCompare { get; private set; }

        /// <summary>
        ///   Valore statico di tipo intero da confrontare con il valore recuperato
        /// </summary>
        public long? ExactIntValueToCompare { get; private set; }

        /// <summary>
        ///   Valore statico di tipo intervallo da confrontare con il valore recuperato
        /// </summary>
        public DBStreamFieldValueRange RangeValueToCompare { get; private set; }
    }
}
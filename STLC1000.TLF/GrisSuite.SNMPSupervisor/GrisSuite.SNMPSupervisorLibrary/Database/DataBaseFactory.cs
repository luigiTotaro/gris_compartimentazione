using System;
using System.Net;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Factory che si occupa della persistenza e recupero dei dati
    /// </summary>
    public class DatabaseFactory
    {
        private readonly bool persistOnSqlDatabase;

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "persistOnDatabase">Indica se persistere i dati su database o meno (dump su file). Se non indicato, usa l'impostazione da configurazione (parametro 'PersistOnDatabase')</param>
        public DatabaseFactory(bool? persistOnDatabase)
        {
            if (persistOnDatabase.HasValue)
            {
                this.persistOnSqlDatabase = persistOnDatabase.Value;
            }
            else
            {
                this.persistOnSqlDatabase = Settings.Default.PersistOnDatabase;
            }
        }

        /// <summary>
        ///   Aggiorna lo stato della periferica
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "deviceId">Id del Device</param>
        /// <param name = "severityLevel">Livello di severit� del Device</param>
        /// <param name = "descriptionStatus">Stato del Device</param>
        /// <param name = "offline">Indica se il Device � fuori linea o non raggiungibile</param>
        public void UpdateDeviceStatus(string dbConnectionString, long deviceId, int severityLevel, string descriptionStatus, byte offline)
        {
            if (this.persistOnSqlDatabase)
            {
                SqlDatabaseUtility.UpdateDeviceStatus(dbConnectionString, deviceId, severityLevel, descriptionStatus, offline);
            }
            else
            {
                FakeDatabaseUtility.UpdateDeviceStatus(deviceId, severityLevel, descriptionStatus, offline);
            }
        }

        /// <summary>
        ///   Aggiorna lo Stream
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "deviceId">Id del Device</param>
        /// <param name = "streamId">Id dello Stream</param>
        /// <param name = "name">Nome dello Stream</param>
        /// <param name = "severityLevel">Livello di severit� dello Stream</param>
        /// <param name = "description">Descrizione del valore dello Stream</param>
        public void UpdateStream(string dbConnectionString, long deviceId, int streamId, string name, int severityLevel, string description)
        {
            if (this.persistOnSqlDatabase)
            {
                SqlDatabaseUtility.UpdateStream(dbConnectionString, deviceId, streamId, name, severityLevel, description);
            }
            else
            {
                FakeDatabaseUtility.UpdateStream(deviceId, streamId, name, severityLevel, description);
            }
        }

        /// <summary>
        ///   Aggiorna il Field
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "deviceId">Id del Device</param>
        /// <param name = "streamId">Id dello Stream</param>
        /// <param name = "fieldId">Id del Field</param>
        /// <param name = "arrayId">Id del vettore dei valori del Field</param>
        /// <param name = "name">Nome del Field</param>
        /// <param name = "severityLevel">Livello di severit� del Field</param>
        /// <param name = "value">Valore del Field</param>
        /// <param name = "description">Descrizione del valore del Field</param>
        public void UpdateStreamField(string dbConnectionString, long deviceId, int streamId, int fieldId, int arrayId, string name, int severityLevel, string value,
                                      string description)
        {
            if (this.persistOnSqlDatabase)
            {
                SqlDatabaseUtility.UpdateStreamField(dbConnectionString, deviceId, streamId, fieldId, arrayId, name, severityLevel, value, description);
            }
            else
            {
                FakeDatabaseUtility.UpdateStreamField(deviceId, fieldId, arrayId, name, severityLevel, value, description);
            }
        }

        /// <summary>
        ///   Verifica l'esistenza di una periferica in base dati
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "deviceId">Id del Device</param>
        public bool? CheckDeviceExistence(string dbConnectionString, long deviceId)
        {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.CheckDeviceExistence(dbConnectionString, deviceId);
            }

            return FakeDatabaseUtility.CheckDeviceExistence();
        }

        /// <summary>
        ///   Recupera i dati di lookup per poter creare una periferica su database
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "location">Dati della topografia della periferica</param>
        /// <param name = "serverId">ID del Server da System.xml</param>
        /// <param name = "portId">ID dell'interfaccia da System.xml</param>
        /// <returns>Dati di lookup con l'Id di Stazione, Fabbricato, Armadio e Interfaccia</returns>
        public DeviceDatabaseSupport GetStationBuildingRackPortData(string dbConnectionString, TopographyLocation location, int serverId, int portId)
        {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.GetStationBuildingRackPortData(dbConnectionString, location, serverId, portId);
            }

            return FakeDatabaseUtility.GetStationBuildingRackPortData();
        }

        /// <summary>
        ///   Aggiorna o inserisce i dati della periferica su database
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "deviceId">Id della periferica</param>
        /// <param name = "nodeId">Id del Nodo</param>
        /// <param name = "serverId">Id del Server</param>
        /// <param name = "deviceName">Nome della periferica</param>
        /// <param name = "deviceType">Tipo della periferica</param>
        /// <param name = "serialNumber">Numero di serie della periferica</param>
        /// <param name = "deviceAddress">Indirizzo IP della periferica</param>
        /// <param name = "portId">Id dell'interfaccia della periferica</param>
        /// <param name = "profileId">Id del profilo della periferica</param>
        /// <param name = "active">Stato della periferica</param>
        /// <param name = "scheduled">Stato di schedulazione della periferica</param>
        /// <param name = "removed">Stato di rimozione della periferica</param>
        /// <param name = "rackId">Id dell'Armadio</param>
        /// <param name = "rackPositionRow">Indice della posizione di riga nell'Armadio</param>
        /// <param name = "rackPositionCol">Indice della posizione di colonna nell'Armadio</param>
        /// <returns>Booleano che indica se l'operazione � avvenuta correttamente</returns>
        public bool UpdateDevice(string dbConnectionString, long deviceId, long nodeId, int serverId, string deviceName, string deviceType, string serialNumber,
                                 IPAddress deviceAddress, Guid portId, int profileId, Byte active, Byte scheduled, Byte removed, Guid rackId, int rackPositionRow,
                                 int rackPositionCol)
        {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.UpdateDevice(dbConnectionString, deviceId, nodeId, serverId, deviceName, deviceType, serialNumber, deviceAddress, portId, profileId,
                                                       active, scheduled, removed, rackId, rackPositionRow, rackPositionCol);
            }

            return FakeDatabaseUtility.UpdateDevice(deviceId, nodeId, serverId, deviceName, deviceType, serialNumber, deviceAddress, portId, profileId, active, scheduled, removed,
                                                    rackId, rackPositionRow, rackPositionCol);
        }

        /// <summary>
        ///   Elimina i dati della periferica su database
        /// </summary>
        /// <param name = "dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name = "deviceId">Id della periferica</param>
        /// <returns>Booleano che indica se l'operazione � avvenuta correttamente</returns>
        public bool DeleteDevice(string dbConnectionString, long deviceId)
        {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.DeleteDevice(dbConnectionString, deviceId);
            }

            return FakeDatabaseUtility.DeleteDevice();
        }
    }
}
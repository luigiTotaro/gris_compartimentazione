﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security;
using System.Text;
using System.Web;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary
{
    public static class FileUtility
    {
        #region Variabili private

        private static string logFilePath;
        private static readonly object locker = new object();

        #endregion

        #region Prorietà pubbliche

        public static LoggingLevel LogLevel { get; private set; }

        #endregion

        #region Costruttore

        static FileUtility()
        {
            if (Assembly.GetEntryAssembly() != null)
            {
                logFilePath = Assembly.GetEntryAssembly().Location + ".log.htm";
            }
            else
            {
                logFilePath = Assembly.GetExecutingAssembly().Location + ".log.htm";
            }

            if (Enum.IsDefined(typeof (LoggingLevel), Settings.Default.LogLevel))
            {
                LogLevel = (LoggingLevel) Settings.Default.LogLevel;
            }
            else
            {
                LogLevel = LoggingLevel.Warning;
                AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                                                   "LogLevel value declared in configuration file is not inside expected range (0-6). Log Level was set to 3 - Warnings only");
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        ///   Aggiunge una messaggio di log ad un file di testo su disco. Il messaggio è preceduto dalla data/ora completa. In caso di errore nell'accesso al file, non fa nulla.
        /// </summary>
        /// <param name = "level">Livello di logging</param>
        /// <param name = "text">Testo del messaggio</param>
        public static void AppendStringToFileWithLoggingLevel(LoggingLevel level, string text)
        {
            if ((LogLevel > LoggingLevel.None) && (level <= LogLevel) && (!String.IsNullOrEmpty(text)) && (!String.IsNullOrEmpty(logFilePath)))
            {
                lock (locker)
                {
                    try
                    {
                        using (TextWriter fileWriter = new StreamWriter(logFilePath, true, Encoding.GetEncoding(1252)))
                        {
                            string fontWeight = string.Empty;
                            string fontColor = string.Empty;

                            switch (level)
                            {
                                case LoggingLevel.Critical:
                                    fontWeight = "font-weight:bold;";
                                    fontColor = "color:#FF0000;";
                                    break;
                                case LoggingLevel.Error:
                                    fontColor = "color:#FF0000;";
                                    break;
                                case LoggingLevel.Warning:
                                    fontColor = "color:#FFB700;";
                                    break;
                            }

                            fileWriter.Write(string.Format(CultureInfo.InvariantCulture, "<div style=\"font-family:Consolas,Courier;font-size:10pt;{2}{3}\">[{0}] {1}</div><hr/>",
                                                           DateTime.Now.ToString("o", CultureInfo.InvariantCulture), HtmlNewLine(text), fontColor,
                                                           fontWeight));

                            fileWriter.Flush();
                        }
                    }
                    catch (UnauthorizedAccessException)
                    {
                    }
                    catch (IOException)
                    {
                    }
                    catch (ObjectDisposedException)
                    {
                    }
                    catch (ArgumentException)
                    {
                    }
                    catch (SecurityException)
                    {
                    }
                }
            }
        }

        /// <summary>
        ///   Aggiunge una messaggio ad un file di testo su disco. In caso di errore nell'accesso al file, non fa nulla.
        /// </summary>
        /// <param name = "filePath">Path del file su cui salvare il log</param>
        /// <param name = "text">Testo del messaggio</param>
        public static void AppendStringToFile(string filePath, string text)
        {
            if (!String.IsNullOrEmpty(filePath))
            {
                try
                {
                    using (TextWriter fileWriter = new StreamWriter(filePath, true, Encoding.GetEncoding(1252)))
                    {
                        fileWriter.Write(text);
                        fileWriter.Flush();
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (IOException)
                {
                }
                catch (ObjectDisposedException)
                {
                }
                catch (ArgumentException)
                {
                }
                catch (SecurityException)
                {
                }
            }
        }

        /// <summary>
        ///   Controlla la dimensione del file di log e, se eccede la dimensione massima configurata, ne crea uno nuovo, rinominando il corrente
        /// </summary>
        /// <returns></returns>
        public static void RecycleLogFile()
        {
            if (File.Exists(logFilePath))
            {
                try
                {
                    FileInfo logFileInfo = new FileInfo(logFilePath);
                    long debugLogMaxSize = Settings.Default.DebugLogMaxByteSize;

                    if ((logFileInfo.Length > debugLogMaxSize) && (debugLogMaxSize > 0))
                    {
                        string logFileInfoOld = logFilePath.Replace(".log.htm", "_old.log.htm");

                        if (File.Exists(logFileInfoOld))
                        {
                            if ((File.GetAttributes(logFileInfoOld) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            {
                                File.SetAttributes(logFileInfoOld, FileAttributes.Normal);
                            }

                            File.Delete(logFileInfoOld);
                        }

                        logFileInfo.MoveTo(logFileInfoOld);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    logFilePath = null;
                }
                catch (IOException)
                {
                    logFilePath = null;
                }
                catch (ObjectDisposedException)
                {
                    logFilePath = null;
                }
                catch (ArgumentException)
                {
                    logFilePath = null;
                }
                catch (SecurityException)
                {
                    logFilePath = null;
                }
                catch (NotSupportedException)
                {
                    logFilePath = null;
                }
            }
        }

        public static string HtmlNewLine(string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                return HttpUtility.HtmlEncode(input).Replace("\n", "<br/>");
            }
            return string.Empty;
        }

        /// <summary>
        ///   Verifica se un file esiste su disco e se può essere aperto e letto
        /// </summary>
        /// <param name = "fileName">Nome del file completo</param>
        /// <returns>True se il file esiste e può essere correttamente letto. False altrimenti.</returns>
        public static bool CheckFileCanRead(string fileName)
        {
            bool returnValue;

            FileStream fs = null;
            try
            {
                if (!File.Exists(fileName))
                {
                    returnValue = false;
                }
                else
                {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    returnValue = fs.CanRead;
                }
            }
            catch (UnauthorizedAccessException)
            {
                returnValue = false;
            }
            catch (IOException)
            {
                returnValue = false;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }

            return returnValue;
        }

        /// <summary>
        ///   Legge un file da disco
        /// </summary>
        /// <param name = "fileName">Nome del file completo</param>
        /// <param name = "fileEncoding">Encoding del file da caricare</param>
        /// <returns>Stringa che contiene il contenuto del file. Null nel caso non possa essere letto</returns>
        public static string ReadFile(string fileName, Encoding fileEncoding)
        {
            string file = null;

            if (fileEncoding != null)
            {
                FileStream fs = null;
                try
                {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    byte[] fileData = new byte[fs.Length];

                    if (fs.Read(fileData, 0, (int) fs.Length) > 0)
                    {
                        file = fileEncoding.GetString(fileData);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (IOException)
                {
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }

            return file;
        }

        #endregion
    }
}
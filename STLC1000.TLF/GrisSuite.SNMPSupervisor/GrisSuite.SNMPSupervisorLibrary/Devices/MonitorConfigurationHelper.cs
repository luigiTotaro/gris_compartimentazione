﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security;
using System.Text.RegularExpressions;
using System.Xml;
using GrisSuite.SnmpSupervisorLibrary.Database;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Classe helper per il caricamento delle definizioni
    /// </summary>
    public class MonitorConfigurationHelper
    {
        #region Costanti

        /// <summary>
        ///   Indica una periferica che non ha categoria valorizzata o per cui la categoria non è applicabile
        /// </summary>
        public const int DEVICE_CATEGORY_UNDEFINED = -1;

        /// <summary>
        ///   Indica una periferica che non è in grado di recuperare la categoria, in base alla MIB caricata, dai dati SNMP letti
        /// </summary>
        public const int DEVICE_CATEGORY_NOT_AVAILABLE = -2;

        #endregion

        #region Proprietà

        /// <summary>
        ///   Definizione della periferica
        /// </summary>
        public string DefinitionFileName { get; private set; }

        /// <summary>
        ///   Versione del protocollo Snmp per le richieste
        /// </summary>
        public VersionCode SnmpVersionCode { get; private set; }

        /// <summary>
        ///   Indica se la definizione è configurata per effettuare il caricamento dinamico di altre definizioni
        /// </summary>
        public bool HasDynamicLoadingRules { get; private set; }

        /// <summary>
        ///   Intero che indica la categoria della periferica (dipende da un enumerato indicato nella MIB, se presente). Il valore -1 indica nessuna category specificata
        /// </summary>
        public int DeviceCategory { get; private set; }

        /// <summary>
        ///   Indica se caricare i dati da file di risposte locali per simulazione
        /// </summary>
        public bool FakeLocalData { get; private set; }

        /// <summary>
        ///   Community string per l'accesso Snmp in lettura
        /// </summary>
        public string SnmpCommunity { get; private set; }

        /// <summary>
        ///   Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        #endregion

        #region Variabili private

        private readonly IPEndPoint ipEndPoint;
        private readonly MessengerFactory messengerFactory;
        private readonly DeviceKind type;

        #endregion

        #region Costruttore

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "definitionFileName">Nome del file di definizione XML da caricare</param>
        /// <param name = "messengerFactory">Classe messenger per l'esecuzione delle query SNMP</param>
        /// <param name = "ipEndPoint">Indirizzo IP e porta della periferica</param>
        /// <param name = "snmpCommunity">Community string per l'accesso Snmp in lettura</param>
        /// <param name = "type">Genere della periferica</param>
        public MonitorConfigurationHelper(bool fakeLocalData, string definitionFileName, MessengerFactory messengerFactory, IPEndPoint ipEndPoint,
                                          string snmpCommunity, DeviceKind type)
        {
            if (string.IsNullOrEmpty(definitionFileName))
            {
                throw new ArgumentException("XML definition file name is mandatory.", "definitionFileName");
            }

            if (messengerFactory == null)
            {
                throw new ArgumentException("Messenger class factory is mandatory.", "messengerFactory");
            }

            if ((ipEndPoint == null) || (ipEndPoint.Address == null))
            {
                throw new ArgumentException("IP address and port are mandatory.", "ipEndPoint");
            }

            this.DefinitionFileName = definitionFileName;
            this.messengerFactory = messengerFactory;
            this.ipEndPoint = ipEndPoint;
            this.DeviceCategory = DEVICE_CATEGORY_UNDEFINED; // non inizializzato
            this.FakeLocalData = fakeLocalData;
            this.SnmpCommunity = snmpCommunity;
            this.NoMatchDeviceStatus = null;
            this.type = type;
        }

        #endregion

        #region Precaricamento definizione, per poter configurare il caricamento dinamico delle sotto-definizioni, se presente

        /// <summary>
        ///   Esegue un parsing preliminare della definizione, per poter configurare il caricamento dinamico delle sotto-definizioni, se presente
        /// </summary>
        public void PreloadConfiguration()
        {
            XmlNode root = this.GetDefinitionXmlRootElement();

            if (root != null)
            {
                #region VersionCode

                object versionCode = null;

                if (root.Attributes["VersionCode"] != null)
                {
                    try
                    {
                        string versionCodeString = root.Attributes["VersionCode"].Value.Trim();
                        versionCode = Enum.Parse(typeof(VersionCode), versionCodeString, true);
                    }
                    catch (ArgumentException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Attribute 'VersionCode' inside 'Device' node in file {0}, does not contain an allowed value. Current value: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   root.Attributes["VersionCode"].Value.Trim()));
                    }
                }

                if (versionCode == null)
                {
                    // Per default le query Snmp usano la versione 2
                    this.SnmpVersionCode = VersionCode.V2;
                }
                else
                {
                    this.SnmpVersionCode = (VersionCode)versionCode;
                }

                #endregion

                #region HasDynamicLoadingRules

                bool hasDynamicLoadingRules = false;

                if (root.Attributes["HasDynamicLoadingRules"] != null)
                {
                    string hasDynamicLoadingRulesString = root.Attributes["HasDynamicLoadingRules"].Value.Trim().ToLowerInvariant();
                    if (!Boolean.TryParse(hasDynamicLoadingRulesString, out hasDynamicLoadingRules))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Attribute 'HasDynamicLoadingRules' inside 'Device' node in file {0}, does not contain a valid boolean value (true/false). Current value: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   root.Attributes["HasDynamicLoadingRules"].Value.Trim()));
                    }
                }

                this.HasDynamicLoadingRules = hasDynamicLoadingRules;

                #endregion

                if (this.HasDynamicLoadingRules)
                {
                    bool rulesFound = false;
                    List<LoadingRule> loadingRules = new List<LoadingRule>();
                    LoadingRuleExactMatch loadingRuleExactMatch = null;

                    #region Caricamento del nodo base LoadingRules

                    foreach (XmlNode rootElement in root.ChildNodes)
                    {
                        if (rootElement.Name.Equals("loadingrules", StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (XmlNode rule in rootElement.ChildNodes)
                            {
                                if (rule.Name.Equals("rule", StringComparison.OrdinalIgnoreCase))
                                {
                                    #region Regola di caricamento di tipo Rule

                                    if ((rule.Attributes["ProbingCategoryOid"] == null) ||
                                        (rule.Attributes["ProbingCategoryOid"].Value.Trim().Length == 0) || (rule.Attributes["Manufacturer"] == null) ||
                                        (rule.Attributes["Manufacturer"].Value.Trim().Length == 0) || (rule.Attributes["DefinitionFile"] == null) ||
                                        (rule.Attributes["DefinitionFile"].Value.Trim().Length == 0))
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Unable to find attributes 'ProbingCategoryOid', 'Manufacturer' or 'DefinitionFile' inside 'Rule' node, or category Oid, vendor name or definition file name are null {0}. Node contents are:\r\n{1}",
                                                                                                   this.DefinitionFileName, rule.OuterXml));
                                    }

                                    #region ProbingCategoryOid

                                    ObjectIdentifier probingCategoryOid;

                                    if (rule.Attributes["ProbingCategoryOid"] != null)
                                    {
                                        string probingCategoryOidString = rule.Attributes["ProbingCategoryOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingCategoryOidString))
                                        {
                                            probingCategoryOid = SnmpUtility.TryParseOid(probingCategoryOidString);

                                            if (probingCategoryOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'ProbingCategoryOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           rule.Attributes["ProbingCategoryOid"].Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                       "Attribute 'ProbingCategoryOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                       this.DefinitionFileName,
                                                                                                       rule.Attributes["ProbingCategoryOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Attribute 'ProbingCategoryOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                   this.DefinitionFileName,
                                                                                                   rule.Attributes["ProbingCategoryOid"].Value));
                                    }

                                    #endregion

                                    loadingRules.Add(new LoadingRule(probingCategoryOid, rule.Attributes["Manufacturer"].Value.Trim(),
                                                                     rule.Attributes["DefinitionFile"].Value.Trim()));

                                    rulesFound = true;

                                    #endregion
                                }
                                else if (rule.Name.Equals("exactmatchrule", StringComparison.OrdinalIgnoreCase))
                                {
                                    #region Regola di caricamento di tipo ExactMatchRule

                                    if ((rule.Attributes["ProbingCategoryOid"] == null) ||
                                        (rule.Attributes["ProbingCategoryOid"].Value.Trim().Length == 0) ||
                                        (rule.Attributes["ProbingManufacturerOid"] == null) ||
                                        (rule.Attributes["ProbingManufacturerOid"].Value.Trim().Length == 0))
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Unable to find attributes 'ProbingCategoryOid' or 'ProbingManufacturerOid' inside 'ExactMatchRule' node, or category Oid or vendor Oid are null in file {0}. Node contents are:\r\n{1}",
                                                                                                   this.DefinitionFileName, rule.OuterXml));
                                    }

                                    bool manufacturerDefinitionFound = false;

                                    List<ManufacturerDefinition> manufacturerDefinitionList = new List<ManufacturerDefinition>();

                                    #region Caricamento sotto nodi ManufacturerDefinition

                                    foreach (XmlNode manufacturerDefinition in rule.ChildNodes)
                                    {
                                        if (manufacturerDefinition.Name.Equals("manufacturerdefinition", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if ((manufacturerDefinition.Attributes["Description"] == null) ||
                                                (manufacturerDefinition.Attributes["Description"].Value.Trim().Length == 0) ||
                                                (manufacturerDefinition.Attributes["DefinitionFile"] == null) ||
                                                (manufacturerDefinition.Attributes["DefinitionFile"].Value.Trim().Length == 0))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Unable to find attributes 'Description' or 'DefinitionFile' inside 'ManufacturerDefinition' node, or vendor description or definition file name are null in file {0}. Node contents are:\r\n{1}",
                                                                                                           this.DefinitionFileName,
                                                                                                           manufacturerDefinition.OuterXml));
                                            }

                                            #region DescriptionRegex

                                            Regex descriptionRegex = null;

                                            if (manufacturerDefinition.Attributes["Description"] != null)
                                            {
                                                try
                                                {
                                                    descriptionRegex = new Regex(manufacturerDefinition.Attributes["Description"].Value,
                                                                                 RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                                                }
                                                catch (ArgumentNullException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'Description' inside 'ManufacturerDefinition' node in file {0}, does not contain a valid regular expression. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               manufacturerDefinition.Attributes[
                                                                                                                   "Description"].Value));
                                                }
                                                catch (ArgumentException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'Description' inside 'ManufacturerDefinition' node in file {0}, does not contain a valid regular expression. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               manufacturerDefinition.Attributes[
                                                                                                                   "Description"].Value));
                                                }
                                            }

                                            #endregion

                                            manufacturerDefinitionList.Add(new ManufacturerDefinition(descriptionRegex,
                                                                                                      manufacturerDefinition.Attributes[
                                                                                                          "DefinitionFile"].Value.Trim()));

                                            manufacturerDefinitionFound = true;
                                        }
                                    }

                                    #endregion

                                    if (!manufacturerDefinitionFound)
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Unable to load XML definition file {0}. Unable to find at least one 'ManufacturerDefinition' element inside 'ExactMatchRule' node, needed to dynamically load definition files, based on vendor data",
                                                                                                   this.DefinitionFileName));
                                    }

                                    #region ProbingCategoryOid

                                    ObjectIdentifier probingCategoryOid;

                                    if (rule.Attributes["ProbingCategoryOid"] != null)
                                    {
                                        string probingCategoryOidString = rule.Attributes["ProbingCategoryOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingCategoryOidString))
                                        {
                                            probingCategoryOid = SnmpUtility.TryParseOid(probingCategoryOidString);

                                            if (probingCategoryOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'ProbingCategoryOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           rule.Attributes["ProbingCategoryOid"].Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                       "Attribute 'ProbingCategoryOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                       this.DefinitionFileName,
                                                                                                       rule.Attributes["ProbingCategoryOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Attribute 'ProbingCategoryOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                   this.DefinitionFileName,
                                                                                                   rule.Attributes["ProbingCategoryOid"].Value));
                                    }

                                    #endregion

                                    #region ProbingManufacturerOid

                                    ObjectIdentifier probingManufacturerOid;

                                    if (rule.Attributes["ProbingManufacturerOid"] != null)
                                    {
                                        string probingManufacturerOidString = rule.Attributes["ProbingManufacturerOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingManufacturerOidString))
                                        {
                                            probingManufacturerOid = SnmpUtility.TryParseOid(probingManufacturerOidString);

                                            if (probingManufacturerOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'ProbingManufacturerOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           rule.Attributes["ProbingManufacturerOid"].
                                                                                                               Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                       "Attribute 'ProbingManufacturerOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                       this.DefinitionFileName,
                                                                                                       rule.Attributes["ProbingManufacturerOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Attribute 'ProbingManufacturerOid' inside 'Rule' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                   this.DefinitionFileName,
                                                                                                   rule.Attributes["ProbingManufacturerOid"].Value));
                                    }

                                    #endregion

                                    #region NoMatchDeviceStatusSeverityAndDescription

                                    NoDynamicRuleMatchDeviceStatus noRuleMatchDeviceStatus = null;

                                    if (rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim()))
                                        {
                                            string noMatchDeviceStatusSeverityAndDescription =
                                                rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim();

                                            string[] values = noMatchDeviceStatusSeverityAndDescription.Split(';');

                                            if (values.Length == 2)
                                            {
                                                object severity;

                                                try
                                                {
                                                    severity = Enum.Parse(typeof(Severity), values[0].Trim(), true);
                                                }
                                                catch (ArgumentException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Unable to load XML definition file {0}. Attribute content 'NoMatchDeviceStatusSeverityAndDescription' inside node 'ExactMatchRule' does not contain a valid Severity value (allowed values: Ok, Warning, Error or Unknown). Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               values[0].Trim()));
                                                }

                                                string severityDescription = values[1].Trim();

                                                if (string.IsNullOrEmpty(severityDescription))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Unable to load XML definition file {0}. Attribute content 'NoMatchDeviceStatusSeverityAndDescription' inside node 'ExactMatchRule' does not contain a valid Description for device state. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               severityDescription));
                                                }

                                                if ((severity != null) && (severity is Severity))
                                                {
                                                    noRuleMatchDeviceStatus = new NoDynamicRuleMatchDeviceStatus((Severity)severity,
                                                                                                                 severityDescription);
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Unable to load XML definition file {0}. Attribute content 'NoMatchDeviceStatusSeverityAndDescription' inside node 'ExactMatchRule' does not contain a valid Severity value (allowed values: Ok, Warning, Error or Unknown)",
                                                                                                               this.DefinitionFileName));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Unable to load XML definition file {0}. Attribute content 'NoMatchDeviceStatusSeverityAndDescription' inside node 'ExactMatchRule' is not declared as 'DeviceSeverity;Device status description' (where 'DeviceSeverity' can be a value between: Ok, Warning, Error or Unknown and 'Device status description' must not contain semicolon ';')",
                                                                                                           this.DefinitionFileName));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                       "Unable to load XML definition file {0}. Attribute content 'NoMatchDeviceStatusSeverityAndDescription' inside node 'ExactMatchRule' is null and not valid",
                                                                                                       this.DefinitionFileName));
                                        }
                                    }

                                    #endregion

                                    loadingRuleExactMatch = new LoadingRuleExactMatch(probingCategoryOid, probingManufacturerOid,
                                                                                      manufacturerDefinitionList.AsReadOnly(), noRuleMatchDeviceStatus);

                                    rulesFound = true;

                                    #endregion
                                }
                            }

                            break;
                        }
                    }

                    #endregion

                    if (!rulesFound)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Unable to load XML definition file {0}. Unable to find 'LoadingRules' node at first level, inside a definition declared as dynamic",
                                                                                   this.DefinitionFileName));
                    }

                    int deviceCategory;

                    if (loadingRules.Count > 0)
                    {
                        // Nel caso siano indicate sia regole per produttore che generica con Enterprise unica, prevale quella per produttore
                        foreach (LoadingRule rule in loadingRules)
                        {
                            // Nel caso siano indicate le regole di caricamento per produttore, si esegue ogni query per recuperare la categoria
                            // Alla prima query che ottiene una risposta valida (categoria con valore maggiore o uguale a zero),
                            // sappiamo di avere trovato una periferica che appartiene a quel produttore, perché siamo riusciti ad entrare in una
                            // Enterprise privata, quindi possiamo usare la definizione dinamica configurata, impostare la categoria e uscire

                            deviceCategory = this.GetCategoryFromDevice(rule.ProbingCategoryOid, false);

                            if (deviceCategory >= 0)
                            {
                                this.DeviceCategory = deviceCategory;
                                this.DefinitionFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Definitions\\") + rule.DefinitionFile;
                                break;
                            }
                        }
                    }
                    else if ((loadingRuleExactMatch != null) && (loadingRuleExactMatch.ManufacturerDefinitions.Count > 0))
                    {
                        // Nel caso sia indicata la regola unica, che si basa su una Enterprise comune a tutti i produttori
                        // recupera dalla periferica la categoria ed il nome del produttore
                        // Dato il nome del produttore cerca tra quelli configurati, per poter estrarre la
                        // relativa definizione. Nel caso che trovi il produttore e la relativa definizione, reimposta
                        // la definizione corrente e la categoria della periferica ed esce

                        deviceCategory = this.GetCategoryFromDevice(loadingRuleExactMatch.ProbingCategoryOid, true);
                        this.DeviceCategory = deviceCategory;

                        string deviceManufaturer = this.GetManufacturerFromDevice(loadingRuleExactMatch.ProbingManufacturerOid);

                        if (string.IsNullOrEmpty(deviceManufaturer))
                        {
                            // Nel caso che il produttore non sia recuperabile, resettiamo anche la categoria
                            // I due valori sono strettamente legati per la parte dinamica
                            deviceCategory = DEVICE_CATEGORY_NOT_AVAILABLE;
                            this.DeviceCategory = DEVICE_CATEGORY_NOT_AVAILABLE;
                        }

                        if (deviceCategory >= 0)
                        {
                            if (!string.IsNullOrEmpty(deviceManufaturer))
                            {
                                foreach (ManufacturerDefinition manufacturerDefinition in
                                    loadingRuleExactMatch.ManufacturerDefinitions)
                                {
                                    if (manufacturerDefinition.Description.IsMatch(deviceManufaturer))
                                    {
                                        this.DefinitionFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Definitions\\") +
                                                                  manufacturerDefinition.DefinitionFile;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Unable to load XML definition file {0}. Unable to find valid loading rules inside a definition declared as dynamic",
                                                                                   this.DefinitionFileName));
                    }

                    // Se arriviamo qui senza avere cambiato definizione corrente e categoria della periferica, rimarranno impostate
                    // la definizione originale e la categoria a "-2". La definizione originale indicherà una MIB-II standard
                    // e saremo in una condizione di query SNMP non specifiche per produttore/periferica

                    if ((this.DeviceCategory == DEVICE_CATEGORY_NOT_AVAILABLE) && (loadingRuleExactMatch != null) &&
                        (loadingRuleExactMatch.NoMatchDeviceStatus != null))
                    {
                        this.NoMatchDeviceStatus = loadingRuleExactMatch.NoMatchDeviceStatus;
                    }
                }
            }
        }

        #endregion

        #region LoadConfiguration - Carica la definizione, la valida e popola la struttura di Stream e StreamField

        /// <summary>
        ///   Carica la definizione, la valida e popola la struttura di Stream e StreamField
        /// </summary>
        /// <param name = "streamData">Collezione di DbStream che sarà popolata nel parsing della definizione</param>
        public void LoadConfiguration(Collection<DBStream> streamData)
        {
            if (streamData == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Stream collection is empty. Unable to load XML definition file."));
            }

            XmlNode root = this.GetDefinitionXmlRootElement();

            if (root != null)
            {
                #region VersionCode

                object versionCode = null;

                if (root.Attributes["VersionCode"] != null)
                {
                    try
                    {
                        string versionCodeString = root.Attributes["VersionCode"].Value.Trim();
                        versionCode = Enum.Parse(typeof(VersionCode), versionCodeString, true);
                    }
                    catch (ArgumentException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Attribute 'VersionCode' inside 'Device' node in file {0}, does not contain an allowed value. Current value: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   root.Attributes["VersionCode"].Value.Trim()));
                    }
                }

                if (versionCode == null)
                {
                    // Per default le query Snmp usano la versione 2
                    this.SnmpVersionCode = VersionCode.V2;
                }
                else
                {
                    this.SnmpVersionCode = (VersionCode)versionCode;
                }

                #endregion

                bool streamsFound = false;

                foreach (XmlNode streams in root.ChildNodes)
                {
                    if (streams.Name.Equals("streams", StringComparison.OrdinalIgnoreCase))
                    {
                        #region Stream

                        bool streamFound = false;

                        foreach (XmlNode stream in streams.ChildNodes)
                        {
                            if (stream.Name.Equals("stream", StringComparison.OrdinalIgnoreCase))
                            {
                                if ((stream.Attributes["ID"] == null) || (stream.Attributes["ID"].Value.Trim().Length == 0) ||
                                    (stream.Attributes["Name"] == null) || (stream.Attributes["Name"].Value.Trim().Length == 0))
                                {
                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                               "Unable to find attributes 'ID' or 'Name' inside 'Stream' node, or Stream name is null in file {0}. Node contents are:\r\n{1}",
                                                                                               this.DefinitionFileName, stream.OuterXml));
                                }

                                #region ID

                                int streamId;

                                if (!int.TryParse(stream.Attributes["ID"].Value, out streamId))
                                {
                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                               "Attribute 'ID' inside 'Stream' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                               this.DefinitionFileName, stream.Attributes["ID"].Value));
                                }

                                #endregion

                                #region IsMib2

                                bool isMib2 = false;

                                if (stream.Attributes["IsMIB2"] != null)
                                {
                                    string isMib2String = stream.Attributes["IsMIB2"].Value.Trim().ToLowerInvariant();
                                    if (!Boolean.TryParse(isMib2String, out isMib2))
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Attribute 'IsMIB2' inside 'Stream' node in file {0}, does not contain a valid boolean value (true/false). Current value: '{1}'",
                                                                                                   this.DefinitionFileName,
                                                                                                   stream.Attributes["IsMIB2"].Value.Trim()));
                                    }
                                }

                                #endregion

                                #region ExcludedFromSeverityComputation

                                bool streamExcludedFromSeverityComputation = false;

                                if (stream.Attributes["ExcludedFromSeverityComputation"] != null)
                                {
                                    string excludedFromSeverityComputationString =
                                        stream.Attributes["ExcludedFromSeverityComputation"].Value.Trim().ToLowerInvariant();
                                    if (!bool.TryParse(excludedFromSeverityComputationString, out streamExcludedFromSeverityComputation))
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                   "Attribute 'ExcludedFromSeverityComputation' inside 'Stream' node in file {0}, does not contain a valid boolean value (true/false). Current value: '{1}'",
                                                                                                   this.DefinitionFileName,
                                                                                                   stream.Attributes["ExcludedFromSeverityComputation"
                                                                                                       ].Value.Trim()));
                                    }
                                }

                                #endregion

                                DBStream str = new DBStream(streamId, stream.Attributes["Name"].Value.Trim(), isMib2,
                                                            streamExcludedFromSeverityComputation);

                                #region StreamField

                                bool streamFieldFound = false;

                                foreach (XmlNode streamField in stream.ChildNodes)
                                {
                                    if (streamField.Name.Equals("StreamField", StringComparison.OrdinalIgnoreCase))
                                    {
                                        #region Verifica presenza attributi ID e Name

                                        if ((streamField.Attributes["ID"] == null) || (streamField.Attributes["ID"].Value.Trim().Length == 0) ||
                                            (streamField.Attributes["Name"] == null) || (streamField.Attributes["Name"].Value.Trim().Length == 0))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                       "Unable to find attributes 'ID' or 'Name' inside 'StreamField' node in file {0}, or StreamField name is null. Node contents are:\r\n{1}",
                                                                                                       this.DefinitionFileName, streamField.OuterXml));
                                        }

                                        #endregion

                                        #region Validazione attributi nel caso non sia indicato l'Oid principale (quindi se si usa la gestione tabellare) e la periferica è di tipo SNMP

                                        if ((this.type == DeviceKind.Snmp) &&
                                            ((streamField.Attributes["Oid"] == null) || (streamField.Attributes["Oid"].Value.Trim().Length == 0)))
                                        {
                                            #region Verifica presenza TableOid

                                            if ((streamField.Attributes["TableOid"] == null) ||
                                                (streamField.Attributes["TableOid"].Value.Trim().Length == 0))
                                            {
                                                // Nel caso che non sia presente l'Oid per la richiesta scalare, è obbligatorio quello per la richiesta tabellare
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "At least one attribute between 'Oid' and 'TableOid' is mandatory inside 'StreamField' node in file {0}. Node contents are:\r\n{1}",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.OuterXml));
                                            }

                                            #endregion

                                            // In questo punto, siamo nella gestione tabellare, quindi occorre verificare gli opportuni attributi

                                            #region Discrimina verifiche in base a presenza TableRowIndex

                                            if ((streamField.Attributes["TableRowIndex"] != null) &&
                                                (streamField.Attributes["TableRowIndex"].Value.Trim().Length > 0))
                                            {
                                                // E' presente la TableRowIndex, è obbligatorio il TableEvaluationFormula

                                                #region Verifica presenza TableEvaluationFormula

                                                if ((streamField.Attributes["TableEvaluationFormula"] == null) ||
                                                    (streamField.Attributes["TableEvaluationFormula"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Unable to find attributes 'TableEvaluationFormula' inside 'StreamField' node in file {0}. Node contents are:\r\n{1}",
                                                                                                               this.DefinitionFileName,
                                                                                                               streamField.OuterXml));
                                                }

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Verifica presenza TableFilterColumnIndex e TableFilterColumnRegex

                                                // Non è presente la TableRowIndex, quindi sono obbligatori TableFilterColumnIndex e TableFilterColumnRegex
                                                if ((streamField.Attributes["TableFilterColumnIndex"] == null) ||
                                                    (streamField.Attributes["TableFilterColumnIndex"].Value.Trim().Length == 0) ||
                                                    (streamField.Attributes["TableFilterColumnRegex"] == null) ||
                                                    (streamField.Attributes["TableFilterColumnRegex"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Unable to find attributes 'TableFilterColumnIndex', 'TableFilterColumnRegex' or 'TableCorrelationEvaluationFormula' inside 'StreamField' node in file {0}, or SNMP table column index, lookup regular expression or SNMP tabular related evaluation code are null. Node contents are:\r\n{1}",
                                                                                                               this.DefinitionFileName,
                                                                                                               streamField.OuterXml));
                                                }

                                                #endregion

                                                #region Presente CorrelationTableOid

                                                if ((streamField.Attributes["CorrelationTableOid"] != null) &&
                                                    (streamField.Attributes["CorrelationTableOid"].Value.Trim().Length > 0))
                                                {
                                                    // E' indicato l'Oid per il recupero di dati da tabella correlata, è obbligatorio TableCorrelationEvaluationFormula
                                                    if ((streamField.Attributes["TableCorrelationEvaluationFormula"] == null) ||
                                                        (streamField.Attributes["TableCorrelationEvaluationFormula"].Value.Trim().Length == 0))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                   "Unable to find attributes 'TableCorrelationEvaluationFormula' inside 'StreamField' node in file {0}, or SNMP tabular related evaluation code is null. Node contents are:\r\n{1}",
                                                                                                                   this.DefinitionFileName,
                                                                                                                   streamField.OuterXml));
                                                    }

                                                    #region Presente TableCorrelationFilterColumnIndex

                                                    if ((streamField.Attributes["TableCorrelationFilterColumnIndex"] != null) &&
                                                        (streamField.Attributes["TableCorrelationFilterColumnIndex"].Value.Trim().Length > 0))
                                                    {
                                                        // E' indicato l'attributo TableCorrelationFilterColumnIndex, è obbligatorio TableEvaluationFormula
                                                        if ((streamField.Attributes["TableEvaluationFormula"] == null) ||
                                                            (streamField.Attributes["TableEvaluationFormula"].Value.Trim().Length == 0))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Unable to find attributes 'TableEvaluationFormula' inside 'StreamField' node in file {0}, or SNMP tabular evaluation code is null. Node contents are:\r\n{1}",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       streamField.OuterXml));
                                                        }
                                                    }

                                                    #endregion
                                                }

                                                #endregion

                                                #region CorrelationTableOid nullo

                                                if ((streamField.Attributes["CorrelationTableOid"] == null) ||
                                                    (streamField.Attributes["CorrelationTableOid"].Value.Trim().Length == 0))
                                                {
                                                    // Non è indicato l'Oid per il recupero di dati da tabella correlata, quindi è obbligatoria la TableEvaluationFormula
                                                    if ((streamField.Attributes["TableEvaluationFormula"] == null) ||
                                                        (streamField.Attributes["TableEvaluationFormula"].Value.Trim().Length == 0))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                   "Unable to find attributes 'TableEvaluationFormula' inside 'StreamField' node in file {0}, or SNMP tabular evaluation code is null. Node contents are:\r\n{1}",
                                                                                                                   this.DefinitionFileName,
                                                                                                                   streamField.OuterXml));
                                                    }
                                                }

                                                #endregion
                                            }

                                            #endregion
                                        }

                                        #endregion

                                        #region ID

                                        int fieldId;

                                        if (!int.TryParse(streamField.Attributes["ID"].Value, out fieldId))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                       "Attribute 'ID' inside 'StreamField' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                       this.DefinitionFileName,
                                                                                                       streamField.Attributes["ID"].Value));
                                        }

                                        #endregion

                                        #region ArrayID

                                        int arrayId = 0;

                                        if (streamField.Attributes["ArrayID"] != null)
                                        {
                                            if (!int.TryParse(streamField.Attributes["ArrayID"].Value, out arrayId))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'ArrayID' inside 'StreamField' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes["ArrayID"].Value));
                                            }
                                        }

                                        #endregion

                                        #region Oid

                                        ObjectIdentifier oid = null;

                                        if (streamField.Attributes["Oid"] != null)
                                        {
                                            string oidString = streamField.Attributes["Oid"].Value.Trim();

                                            if (!string.IsNullOrEmpty(oidString))
                                            {
                                                oid = SnmpUtility.TryParseOid(oidString);

                                                if (oid == null)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'Oid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               streamField.Attributes["Oid"].Value));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'Oid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes["Oid"].Value));
                                            }
                                        }

                                        #endregion

                                        #region TableOid

                                        ObjectIdentifier tableOid = null;

                                        if (streamField.Attributes["TableOid"] != null)
                                        {
                                            string tableOidString = streamField.Attributes["TableOid"].Value.Trim();

                                            if (!string.IsNullOrEmpty(tableOidString))
                                            {
                                                tableOid = SnmpUtility.TryParseOid(tableOidString);

                                                if (tableOid == null)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'TableOid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               streamField.Attributes["TableOid"].
                                                                                                                   Value));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'TableOid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes["TableOid"].Value));
                                            }
                                        }

                                        #endregion

                                        #region CorrelationTableOid

                                        ObjectIdentifier correlationTableOid = null;

                                        if (streamField.Attributes["CorrelationTableOid"] != null)
                                        {
                                            string correlationTableOidString = streamField.Attributes["CorrelationTableOid"].Value.Trim();

                                            if (!string.IsNullOrEmpty(correlationTableOidString))
                                            {
                                                correlationTableOid = SnmpUtility.TryParseOid(correlationTableOidString);

                                                if (correlationTableOid == null)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'CorrelationTableOid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               streamField.Attributes[
                                                                                                                   "CorrelationTableOid"].Value));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'CorrelationTableOid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "CorrelationTableOid"].Value));
                                            }
                                        }

                                        #endregion

                                        #region TableRowIndex

                                        int? tableRowIndex = null;

                                        if (streamField.Attributes["TableRowIndex"] != null)
                                        {
                                            int tableRowIndexValue;

                                            if (!int.TryParse(streamField.Attributes["TableRowIndex"].Value, out tableRowIndexValue))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'TableRowIndex' inside 'StreamField' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes["TableRowIndex"].
                                                                                                               Value));
                                            }

                                            tableRowIndex = tableRowIndexValue;
                                        }

                                        #endregion

                                        #region TableEvaluationFormula

                                        string evaluationFormula = null;

                                        if (streamField.Attributes["TableEvaluationFormula"] != null)
                                        {
                                            evaluationFormula = streamField.Attributes["TableEvaluationFormula"].Value;
                                        }

                                        #endregion

                                        #region TableCorrelationEvaluationFormula

                                        string tableCorrelationEvaluationFormula = null;

                                        if (streamField.Attributes["TableCorrelationEvaluationFormula"] != null)
                                        {
                                            tableCorrelationEvaluationFormula = streamField.Attributes["TableCorrelationEvaluationFormula"].Value;
                                        }

                                        #endregion

                                        #region TableFilterColumnIndex

                                        int? tableFilterColumnIndex = null;

                                        if (streamField.Attributes["TableFilterColumnIndex"] != null)
                                        {
                                            int tableFilterColumnIndexValue;

                                            if (!int.TryParse(streamField.Attributes["TableFilterColumnIndex"].Value, out tableFilterColumnIndexValue))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'TableFilterColumnIndex' inside 'StreamField' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "TableFilterColumnIndex"].Value));
                                            }

                                            tableFilterColumnIndex = tableFilterColumnIndexValue;
                                        }

                                        #endregion

                                        #region TableCorrelationFilterColumnIndex

                                        int? tableCorrelationFilterColumnIndex = null;

                                        if (streamField.Attributes["TableCorrelationFilterColumnIndex"] != null)
                                        {
                                            int tableCorrelationFilterColumnIndexValue;

                                            if (
                                                !int.TryParse(streamField.Attributes["TableCorrelationFilterColumnIndex"].Value,
                                                              out tableCorrelationFilterColumnIndexValue))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'TableCorrelationFilterColumnIndex' inside 'StreamField' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "TableCorrelationFilterColumnIndex"].
                                                                                                               Value));
                                            }

                                            tableCorrelationFilterColumnIndex = tableCorrelationFilterColumnIndexValue;
                                        }

                                        #endregion

                                        #region TableFilterColumnRegex

                                        Regex tableFilterColumnRegex = null;

                                        if (streamField.Attributes["TableFilterColumnRegex"] != null)
                                        {
                                            try
                                            {
                                                tableFilterColumnRegex = new Regex(streamField.Attributes["TableFilterColumnRegex"].Value,
                                                                                   RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                                            }
                                            catch (ArgumentNullException)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'TableFilterColumnRegex' inside 'StreamField' node in file {0}, does not contain a valid regular expression. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "TableFilterColumnRegex"].Value));
                                            }
                                            catch (ArgumentException)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'TableFilterColumnRegex' inside 'StreamField' node in file {0}, does not contain a valid regular expression. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "TableFilterColumnRegex"].Value));
                                            }
                                        }

                                        #endregion

                                        #region RelevantToDeviceCategory

                                        List<int> relevantToDeviceCategoryList = null;

                                        if (streamField.Attributes["RelevantToDeviceCategory"] != null)
                                        {
                                            string relevantToDeviceCategoryString = streamField.Attributes["RelevantToDeviceCategory"].Value.Trim();

                                            if (!string.IsNullOrEmpty(relevantToDeviceCategoryString))
                                            {
                                                if (relevantToDeviceCategoryString.Equals("*", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    relevantToDeviceCategoryList = new List<int>();
                                                }
                                                else
                                                {
                                                    relevantToDeviceCategoryList = new List<int>();

                                                    foreach (string relevantToDeviceCategoryValueString in
                                                        relevantToDeviceCategoryString.Split(','))
                                                    {
                                                        int relevantToDeviceCategoryValue;

                                                        if (
                                                            !int.TryParse(relevantToDeviceCategoryValueString.Trim(),
                                                                          out relevantToDeviceCategoryValue))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'RelevantToDeviceCategory' inside 'StreamField' node in file {0}, does not contain a list of numerical integer values comma separated. Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       streamField.Attributes[
                                                                                                                           "RelevantToDeviceCategory"]
                                                                                                                           .Value));
                                                        }

                                                        relevantToDeviceCategoryList.Add(relevantToDeviceCategoryValue);
                                                    }
                                                }
                                            }
                                        }

                                        #endregion

                                        #region ValueFormatExpression

                                        string valueFormatExpression = null;

                                        if (streamField.Attributes["ValueFormatExpression"] != null)
                                        {
                                            valueFormatExpression = streamField.Attributes["ValueFormatExpression"].Value;
                                        }

                                        #endregion

                                        #region TableIndexAndScalarCounterOid

                                        int? tableIndex = -1;
                                        ObjectIdentifier scalarCounterOid = null;

                                        if (streamField.Attributes["TableIndexAndScalarCounterOid"] != null)
                                        {
                                            string tableIndexAndScalarCounterOidString =
                                                streamField.Attributes["TableIndexAndScalarCounterOid"].Value.Trim();

                                            if (!string.IsNullOrEmpty(tableIndexAndScalarCounterOidString))
                                            {
                                                string[] values = tableIndexAndScalarCounterOidString.Split(',');

                                                if (values.Length == 2)
                                                {
                                                    #region Parsing indice di riga per tabella dinamica

                                                    string tableIndexValueString = values[0].Trim();

                                                    int tableIndexValue;

                                                    if (!int.TryParse(tableIndexValueString, out tableIndexValue))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                   "Attribute 'TableIndexAndScalarCounterOid' inside 'StreamField' node in file {0}, does not contain a numerical integer value preceding comma. Current value: '{1}'",
                                                                                                                   this.DefinitionFileName,
                                                                                                                   streamField.Attributes[
                                                                                                                       "TableIndexAndScalarCounterOid"
                                                                                                                       ].Value));
                                                    }

                                                    tableIndex = tableIndexValue;

                                                    #endregion

                                                    #region Parsing Oid a valore scalare con il contatore relativo alla tabella

                                                    string scalarCounterOidString = values[1].Trim();
                                                    if (!string.IsNullOrEmpty(scalarCounterOidString))
                                                    {
                                                        scalarCounterOid = SnmpUtility.TryParseOid(scalarCounterOidString);

                                                        if (scalarCounterOid == null)
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'TableIndexAndScalarCounterOid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated following comma. Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       streamField.Attributes[
                                                                                                                           "TableIndexAndScalarCounterOid"
                                                                                                                           ].Value));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                   "Attribute 'TableIndexAndScalarCounterOid' inside 'StreamField' node in file {0}, does not contain a list of numerical values dot separated following comma. Current value: '{1}'",
                                                                                                                   this.DefinitionFileName,
                                                                                                                   streamField.Attributes[
                                                                                                                       "TableIndexAndScalarCounterOid"
                                                                                                                       ].Value));
                                                    }

                                                    #endregion
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'TableIndexAndScalarCounterOid' inside 'StreamField' node in file {0}, does not contain a numerical integer value followed by a comma and a list of numerical integer values dot separated. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               streamField.Attributes[
                                                                                                                   "TableIndexAndScalarCounterOid"].
                                                                                                                   Value));
                                                }
                                            }
                                        }

                                        #endregion

                                        #region ExcludedFromSeverityComputation

                                        bool excludedFromSeverityComputation = false;

                                        if (streamField.Attributes["ExcludedFromSeverityComputation"] != null)
                                        {
                                            string excludedFromSeverityComputationString =
                                                streamField.Attributes["ExcludedFromSeverityComputation"].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(excludedFromSeverityComputationString, out excludedFromSeverityComputation))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'ExcludedFromSeverityComputation' inside 'StreamField' node in file {0}, does not contain a valid boolean value (true/false). Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "ExcludedFromSeverityComputation"].
                                                                                                               Value.Trim()));
                                            }
                                        }

                                        #endregion

                                        #region NoDataAvailableForcedState

                                        NoDataAvailableForcedState noDataAvailableForcedState = null;

                                        if (streamField.Attributes["NoDataAvailableForcedState"] != null)
                                        {
                                            string noDataAvailableForcedStateString =
                                                streamField.Attributes["NoDataAvailableForcedState"].Value.Trim();

                                            string[] values = noDataAvailableForcedStateString.Split(';');

                                            if (values.Length == 3)
                                            {
                                                object severity;

                                                try
                                                {
                                                    severity = Enum.Parse(typeof(Severity), values[0].Trim(), true);
                                                }
                                                catch (ArgumentException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'NoDataAvailableForcedState' inside 'StreamField' node in file {0}, does not contain a valid Severity value (allowed values: Ok, Warning, Error or Unknown). Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               values[0].Trim()));
                                                }

                                                string value = values[1].Trim();

                                                if (string.IsNullOrEmpty(value))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'NoDataAvailableForcedState' inside 'StreamField' node in file {0}, does not contain a valid string value, or string is empty. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               values[1].Trim()));
                                                }

                                                string valueDescription = values[2].Trim();

                                                if (string.IsNullOrEmpty(valueDescription))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'NoDataAvailableForcedState' inside 'StreamField' node in file {0}, does not contain a valid description value, or string is empty. Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               values[2].Trim()));
                                                }

                                                if ((severity != null) && (severity is Severity))
                                                {
                                                    noDataAvailableForcedState = new NoDataAvailableForcedState((Severity)severity, valueDescription,
                                                                                                                value);
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                               "Attribute 'NoDataAvailableForcedState' inside 'StreamField' node in file {0}, does not contain a valid Severity value (allowed values: Ok, Warning, Error or Unknown). Current value: '{1}'",
                                                                                                               this.DefinitionFileName,
                                                                                                               values[0].Trim()));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'NoDataAvailableForcedState' inside 'StreamField' node in file {0}, does not contain three semicolon separated values, e.g. 'Error;0;Not running'. Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "NoDataAvailableForcedState"].Value.
                                                                                                               Trim()));
                                            }
                                        }

                                        #endregion

                                        #region HideIfNoDataAvailable

                                        bool hideIfNoDataAvailable = false;

                                        if (streamField.Attributes["HideIfNoDataAvailable"] != null)
                                        {
                                            string hideIfNoDataAvailableString =
                                                streamField.Attributes["HideIfNoDataAvailable"].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(hideIfNoDataAvailableString, out hideIfNoDataAvailable))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Attribute 'HideIfNoDataAvailable' inside 'StreamField' node in file {0}, does not contain a boolean value (true/false). Current value: '{1}'",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes[
                                                                                                               "HideIfNoDataAvailable"].Value.Trim()));
                                            }
                                        }

                                        #endregion

                                        DBStreamField strField = null;

                                        if ((this.DeviceCategory == DEVICE_CATEGORY_UNDEFINED) ||
                                            ((this.DeviceCategory == DEVICE_CATEGORY_NOT_AVAILABLE) && (relevantToDeviceCategoryList.Count == 0)) ||
                                            ((this.DeviceCategory >= 0) && (relevantToDeviceCategoryList != null) &&
                                             ((relevantToDeviceCategoryList.Count == 0) ||
                                              ((relevantToDeviceCategoryList.Count > 0) &&
                                               (relevantToDeviceCategoryList.Contains(this.DeviceCategory))))))
                                        {
                                            // Lo Stream Field deve essere aggiunto se:
                                            // La periferica corrente non ha una categoria indicata (-1)
                                            // La periferica corrente è di tipo dinamico, ma non è stato possibile sapere la categoria (-2) e
                                            // quindi non si carica una definizione custom per produttore, di cui si usano solo gli stream per categorie "*"
                                            // La periferica corrente ha una categoria e:
                                            // o ha configurato la lista delle categorie relative come "*", quindi valido per qualsiasi categoria
                                            // oppure esiste una lista di categorie relative e quella della periferica corrente è contenuta nella lista
                                            // Nel caso che sia disponibile la categoria della periferica, l'attributo RelevantToDeviceCategory
                                            // deve essere presente e contenere o "*" oppure una lista di categorie separate da virgole

                                            strField = new DBStreamField(fieldId, arrayId, streamField.Attributes["Name"].Value.Trim(), oid, tableOid,
                                                                         correlationTableOid, tableRowIndex, evaluationFormula,
                                                                         tableCorrelationEvaluationFormula, tableFilterColumnIndex,
                                                                         tableCorrelationFilterColumnIndex, tableFilterColumnRegex,
                                                                         new FormatExpression(valueFormatExpression), tableIndex, scalarCounterOid,
                                                                         excludedFromSeverityComputation, noDataAvailableForcedState,
                                                                         hideIfNoDataAvailable);
                                        }

                                        if (strField != null)
                                        {
                                            #region FieldValueDescription

                                            bool streamFieldValueDescriptionFound = false;

                                            foreach (XmlNode fieldValueDescription in streamField.ChildNodes)
                                            {
                                                if (fieldValueDescription.Name.Equals("FieldValueDescription", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    if ((fieldValueDescription.Attributes["ValueDescription"] == null) ||
                                                        (fieldValueDescription.Attributes["ValueDescription"].Value.Trim().Length == 0))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                   "Unable to find attribute 'ValueDescription' inside 'FieldValueDescription' node, or value description is null in file {0}. Node contents are:\r\n{1}",
                                                                                                                   this.DefinitionFileName,
                                                                                                                   fieldValueDescription.OuterXml));
                                                    }

                                                    #region Severity

                                                    object severity = null;

                                                    if (fieldValueDescription.Attributes["Severity"] != null)
                                                    {
                                                        try
                                                        {
                                                            string severityString = fieldValueDescription.Attributes["Severity"].Value.Trim();
                                                            severity = Enum.Parse(typeof(Severity), severityString, true);
                                                            if (severity == null)
                                                            {
                                                                throw new DeviceConfigurationHelperException(
                                                                    string.Format(CultureInfo.InvariantCulture,
                                                                                  "Attribute 'Severity' inside 'FieldValueDescription' node in file {0}, does not contain an allowed value. Current value: '{1}'",
                                                                                  this.DefinitionFileName,
                                                                                  fieldValueDescription.Attributes["Severity"].Value.Trim()));
                                                            }
                                                        }
                                                        catch (ArgumentException)
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'Severity' inside 'FieldValueDescription' node in file {0}, does not contain an allowed value. Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       fieldValueDescription.
                                                                                                                           Attributes["Severity"].
                                                                                                                           Value.Trim()));
                                                        }
                                                    }

                                                    if (severity == null)
                                                    {
                                                        severity = Severity.Ok;
                                                    }

                                                    #endregion

                                                    #region ExactValueString

                                                    string exactValueString = null;

                                                    if (fieldValueDescription.Attributes["ExactValueString"] != null)
                                                    {
                                                        exactValueString = fieldValueDescription.Attributes["ExactValueString"].Value;
                                                    }

                                                    #endregion

                                                    #region ExactValueInt

                                                    int? exactValueIntNull = null;

                                                    if (fieldValueDescription.Attributes["ExactValueInt"] != null)
                                                    {
                                                        string exactValueIntString = fieldValueDescription.Attributes["ExactValueInt"].Value.Trim();
                                                        int exactValueInt;

                                                        if (!int.TryParse(exactValueIntString, out exactValueInt))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'ExactValueInt' inside 'FieldValueDescription' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       fieldValueDescription.
                                                                                                                           Attributes["ExactValueInt"]
                                                                                                                           .Value.Trim()));
                                                        }

                                                        exactValueIntNull = exactValueInt;
                                                    }

                                                    #endregion

                                                    #region DBStreamFieldValueRange

                                                    #region RangeValueMin

                                                    long? rangeValueMinNull = null;

                                                    if (fieldValueDescription.Attributes["RangeValueMin"] != null)
                                                    {
                                                        string rangeValueMinString = fieldValueDescription.Attributes["RangeValueMin"].Value.Trim();
                                                        long rangeValueMin;

                                                        if (!long.TryParse(rangeValueMinString, out rangeValueMin))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'RangeValueMin' inside 'FieldValueDescription' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       fieldValueDescription.
                                                                                                                           Attributes["RangeValueMin"]
                                                                                                                           .Value.Trim()));
                                                        }

                                                        rangeValueMinNull = rangeValueMin;
                                                    }

                                                    #endregion

                                                    #region RangeValueMax

                                                    long? rangeValueMaxNull = null;

                                                    if (fieldValueDescription.Attributes["RangeValueMax"] != null)
                                                    {
                                                        string rangeValueMaxString = fieldValueDescription.Attributes["RangeValueMax"].Value.Trim();
                                                        long rangeValueMax;

                                                        if (!long.TryParse(rangeValueMaxString, out rangeValueMax))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'RangeValueMax' inside 'FieldValueDescription' node in file {0}, does not contain an integer value. Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       fieldValueDescription.
                                                                                                                           Attributes["RangeValueMax"]
                                                                                                                           .Value.Trim()));
                                                        }

                                                        rangeValueMaxNull = rangeValueMax;
                                                    }

                                                    #endregion

                                                    DBStreamFieldValueRange dbStreamFieldValueRange = null;

                                                    if (rangeValueMinNull.HasValue && !rangeValueMaxNull.HasValue)
                                                    {
                                                        dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMinNull.Value);
                                                    }
                                                    else if (!rangeValueMinNull.HasValue && rangeValueMaxNull.HasValue)
                                                    {
                                                        dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMaxNull.Value);
                                                    }
                                                    else if (rangeValueMinNull.HasValue && rangeValueMaxNull.HasValue)
                                                    {
                                                        dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMinNull, rangeValueMaxNull);
                                                    }

                                                    #endregion

                                                    #region IsDefault

                                                    bool isDefault = false;

                                                    if (fieldValueDescription.Attributes["IsDefault"] != null)
                                                    {
                                                        string isDefaultString =
                                                            fieldValueDescription.Attributes["IsDefault"].Value.Trim().ToLowerInvariant();
                                                        if (!Boolean.TryParse(isDefaultString, out isDefault))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                                       "Attribute 'IsDefault' inside 'FieldValueDescription' node in file {0}, does not contain a valid boolean value (true/false). Current value: '{1}'",
                                                                                                                       this.DefinitionFileName,
                                                                                                                       fieldValueDescription.
                                                                                                                           Attributes["IsDefault"].
                                                                                                                           Value.Trim()));
                                                        }
                                                    }

                                                    #endregion

                                                    strField.DbStreamFieldValueDescriptions.Add(new DBStreamFieldValueDescription(strField, isDefault,
                                                                                                                                  fieldValueDescription
                                                                                                                                      .Attributes[
                                                                                                                                          "ValueDescription"
                                                                                                                                      ].Value.Trim(),
                                                                                                                                  (Severity)severity,
                                                                                                                                  exactValueString,
                                                                                                                                  exactValueIntNull,
                                                                                                                                  dbStreamFieldValueRange));

                                                    streamFieldValueDescriptionFound = true;
                                                }
                                            }

                                            if (!streamFieldValueDescriptionFound)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                                           "Unable to load XML definition file {0}. Unable to find at least one stream field value description in definition file. Current stream field '{1}'.",
                                                                                                           this.DefinitionFileName,
                                                                                                           streamField.Attributes["Name"].Value.Trim()));
                                            }

                                            #endregion

                                            str.FieldData.Add(strField);
                                        }

                                        streamFieldFound = true;
                                    }
                                }

                                if (!streamFieldFound)
                                {
                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                               "Unable to load XML definition file {0}. Unable to find at least one stream field defined for stream '{1}', related to current device type.",
                                                                                               this.DefinitionFileName,
                                                                                               stream.Attributes["Name"].Value.Trim()));
                                }

                                #endregion

                                if ((str != null) && (str.FieldData != null) && (str.FieldData.Count > 0))
                                {
                                    // Lo Stream è aggiunto solo se contiene almeno uno Stream Field
                                    // Può succedere che uno Stream sia vuoto, pur se configurato nella definizione
                                    // se stiamo filtrando per categoria della periferica e tutti gli Stream Field riguardano
                                    // una categoria diversa da quella della periferica corrente
                                    streamData.Add(str);
                                }
                            }

                            streamFound = true;
                        }

                        if (!streamFound)
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                       "Unable to load XML definition file {0}. Unable to find at least one Stream.",
                                                                                       this.DefinitionFileName));
                        }

                        #endregion

                        streamsFound = true;
                        break;
                    }
                }

                if (!streamsFound)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "Unable to load XML definition file {0}. Unable to find 'Streams' node at first level.",
                                                                               this.DefinitionFileName));
                }
            }
        }

        #endregion

        #region Metodi privati

        private XmlNode GetDefinitionXmlRootElement()
        {
            if (string.IsNullOrEmpty(this.DefinitionFileName))
            {
                throw new DeviceConfigurationHelperException("Definition file name is undefined");
            }
            if (!FileUtility.CheckFileCanRead(this.DefinitionFileName))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Unable to load definition file: {0}",
                                                                           this.DefinitionFileName));
            }

            XmlDocument doc = new XmlDocument();
            XmlNode root;

            try
            {
                doc.Load(this.DefinitionFileName);
                root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                  this.DefinitionFileName), ex);
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                  this.DefinitionFileName), ex);
            }
            catch (IOException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                  this.DefinitionFileName), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                  this.DefinitionFileName), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                  this.DefinitionFileName), ex);
            }
            catch (SecurityException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Invalid configuration file {0} containing device XML definition",
                                  this.DefinitionFileName), ex);
            }

            if (root == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Invalid configuration file {0} containing device XML definition",
                                                                           this.DefinitionFileName));
            }

            if (!root.Name.Equals("Device", StringComparison.OrdinalIgnoreCase))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Unable to find 'Device' element as root node in configuration file {0}.",
                                                                           this.DefinitionFileName));
            }

            return root;
        }

        /// <summary>
        ///   Ritorna il produttore di una periferica dato l'ObjectIdentifier per recuperarlo via SNMP
        /// </summary>
        /// <param name = "oid">ObjectIdentifier per recuperare il produttore della periferica</param>
        /// <returns>La stringa contenente la descrizione del produttore</returns>
        private string GetManufacturerFromDevice(ObjectIdentifier oid)
        {
            return this.GetSnmpScalarDataValue(oid);
        }

        /// <summary>
        ///   Ritorna la categoria di una periferica dato l'ObjectIdentifier per recuperarla via SNMP
        /// </summary>
        /// <param name = "oid">ObjectIdentifier per recuperare la categoria della periferica</param>
        /// <param name = "returnNotAvailable">Ritorna il valore -2 (non disponibile) se true, -1 (non definito) se false</param>
        /// <returns>Un intero che indica la categoria della periferica, dato un enumerato specificato nella MIB.</returns>
        private int GetCategoryFromDevice(ObjectIdentifier oid, bool returnNotAvailable)
        {
            string categoryString = this.GetSnmpScalarDataValue(oid);
            int category;

            if (string.IsNullOrEmpty(categoryString))
            {
                if (returnNotAvailable)
                {
                    return DEVICE_CATEGORY_NOT_AVAILABLE;
                }

                return DEVICE_CATEGORY_UNDEFINED;
            }

            if (int.TryParse(categoryString, out category))
            {
                return category;
            }

            if (returnNotAvailable)
            {
                return DEVICE_CATEGORY_NOT_AVAILABLE;
            }

            return DEVICE_CATEGORY_UNDEFINED;
        }

        /// <summary>
        ///   Esegue una query SNMP verso la periferica corrente, ritornando il valore di ritorno nel formato stringa
        /// </summary>
        /// <param name = "oid">ObjectIdentifier per recuperare il valore</param>
        /// <returns>La stringa contenente il valore scalare recuperato. null in caso non ci siano dati o la periferica non abbia risposto</returns>
        private string GetSnmpScalarDataValue(ObjectIdentifier oid)
        {
            string returnValue = null;

            IList<Variable> values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.ipEndPoint,
                                                                       new OctetString(this.SnmpCommunity), new List<Variable> {new Variable(oid)});

            if ((values != null) && (values.Count > 0))
            {
                returnValue = SnmpUtility.DecodeDataByType(values[0]);
            }

            return returnValue;
        }

        #endregion
    }
}
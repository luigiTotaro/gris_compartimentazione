﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using GrisSuite.SnmpSupervisorLibrary.Database;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    ///<summary>
    ///  Interfaccia per una periferica
    ///</summary>
    public interface IDevice
    {
        /// <summary>
        ///   Nome della periferica
        /// </summary>
        string DeviceName { get; }

        /// <summary>
        ///   Codice univoco della periferica
        /// </summary>
        string DeviceType { get; }

        /// <summary>
        ///   Nome completo della definizione XML della periferica
        /// </summary>
        string DefinitionFileName { get; }

        /// <summary>
        ///   EndPoint IP della periferica
        /// </summary>
        IPEndPoint DeviceIPEndPoint { get; }

        /// <summary>
        ///   Lista degli Stream relativi alla periferica
        /// </summary>
        Collection<DBStream> StreamData { get; }

        /// <summary>
        ///   Severità relativa al Device
        /// </summary>
        Severity SeverityLevel { get; }

        /// <summary>
        ///   Stato del Device
        /// </summary>
        byte Offline { get; }

        /// <summary>
        ///   <para>Decodifica descrittiva dello stato della periferica</para>
        ///   <para>0 = "In servizio" - Colore verde, significa che la periferica è in servizio e non presenta alcuna anomalia</para>
        ///   <para>1 = "Anomalia lieve" - Colore giallo, significa che ci sono delle anomalie nel funzionamento, ma non pregiudicano il servizio del sistema</para>
        ///   <para>2 = "Anomalia grave" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
        ///   <para>255 = "Sconosciuto" - Non è stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
        /// </summary>
        string ValueDescriptionComplete { get; }

        /// <summary>
        ///   Indica se la periferica è raggiungibile via ICMP (ping)
        /// </summary>
        bool IsAliveIcmp { get; }

        /// <summary>
        ///   Indica se c'è stata una eccezione durante il caricamento della periferica
        /// </summary>
        Exception LastError { get; }

        /// <summary>
        ///   Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        EventObject LastEvent { get; }

        /// <summary>
        ///   Lista degli Eventi relativi alla periferica
        /// </summary>
        Collection<EventObject> EventsData { get; }

        /// <summary>
        ///   Genere della periferica
        /// </summary>
        DeviceKind Type { get; }

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        void Populate();

        /// <summary>
        ///   Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
        /// </summary>
        /// <returns></returns>
        string Dump();
    }
}
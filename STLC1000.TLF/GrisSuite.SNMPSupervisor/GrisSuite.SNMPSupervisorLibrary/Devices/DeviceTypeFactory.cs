﻿using System;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    public class DeviceTypeFactory
    {
        /// <summary>
        ///   Ritorna una istanza di periferica, in base al tipo indicato
        /// </summary>
        /// <param name = "deviceObject">Oggetto di stato con le informazioni relative al device</param>
        /// <returns></returns>
        public IDevice Create(DeviceObject deviceObject)
        {
            IDevice device;

            if (deviceObject.DeviceType.Equals("XXIP000", StringComparison.OrdinalIgnoreCase))
            {
                device = new IcmpDeviceBase(deviceObject.FakeLocalData, deviceObject.Name, deviceObject.DeviceType, deviceObject.EndPoint, deviceObject.LastEvent, DeviceKind.Icmp);
            }
            else
            {
                device = new SnmpDeviceBase(deviceObject.FakeLocalData, deviceObject.Name, deviceObject.DeviceType, deviceObject.EndPoint, deviceObject.LastEvent,
                                            deviceObject.SnmpCommunity, DeviceKind.Snmp);
            }

            return device;
        }
    }
}
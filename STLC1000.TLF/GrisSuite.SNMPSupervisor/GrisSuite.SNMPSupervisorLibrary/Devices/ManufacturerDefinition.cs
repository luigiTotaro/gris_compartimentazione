﻿using System.Text.RegularExpressions;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Indica l'associazione tra un produttore ed un file di definizione
    /// </summary>
    public class ManufacturerDefinition
    {
        /// <summary>
        ///   Espressione regolare per identificare il nome del produttore
        /// </summary>
        public Regex Description { get; private set; }

        /// <summary>
        ///   Nome del file con la definizione relativa
        /// </summary>
        public string DefinitionFile { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "description">Espressione regolare per identificare il nome del produttore</param>
        /// <param name = "definitionFile">Nome del file con la definizione relativa</param>
        public ManufacturerDefinition(Regex description, string definitionFile)
        {
            this.Description = description;
            this.DefinitionFile = definitionFile;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   <para>Rappresenta la classe base per i dispositivi SNMP.</para>
    ///   <para>Comprende la definizione parziale di MIB II standard, comune a tutti i dispositivi.</para>
    /// </summary>
    public class SnmpDeviceBase : ISnmpDevice
    {
        #region Variabili private

        private readonly int deviceCategory;
        private readonly bool hasDynamicLoadingRules;
        private bool isPopulated;
        private readonly Collection<DBStream> streamData;
        private readonly Collection<EventObject> eventsData;
        private readonly MessengerFactory messengerFactory;
        // Indica se la periferica ha risposto solo a stream marcati come MIB-II base
        private bool replyOnlyToMIBBase;
        // Indica se esiste almeno uno stream field che ha generato eccezioni durante il caricamento dei dati
        private bool deviceContainsStreamWithError;

        #endregion

        #region Variabili fisse usate per il test di funzionalità SNMP - query sysName

        /// <summary>
        ///   Indice posizionale dei valori retituiti in base al sysName
        /// </summary>
        /// <see cref = "oidSysName" />
        private const int OID_SYS_NAME_INDEX = 0;

        /// <summary>
        ///   Oid standard del nome (sysName) per la MIB II parziale - usata per testare il funzionamento SNMP
        /// </summary>
        private readonly uint[] oidSysName = new uint[] {1, 3, 6, 1, 2, 1, 1, 1, 0};

        #endregion

        #region Costruttore

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "deviceName">Nome della periferica</param>
        /// <param name = "deviceType">Codice univoco della periferica</param>
        /// <param name = "deviceIP">Indirizzo IP e porta della periferica</param>
        /// <param name = "lastEvent">Informazioni dell'ultimo evento in ordine temporale associato alla periferica</param>
        /// <param name = "snmpCommunity">Community string per l'accesso Snmp in lettura</param>
        /// <param name = "type">Genere della periferica</param>
        public SnmpDeviceBase(bool fakeLocalData, string deviceName, string deviceType, IPEndPoint deviceIP, EventObject lastEvent,
                              string snmpCommunity, DeviceKind type)
        {
            if (String.IsNullOrEmpty(deviceName))
            {
                throw new ArgumentException("Device name is mandatory.", "deviceName");
            }

            if (String.IsNullOrEmpty(deviceType))
            {
                throw new ArgumentException("Device type is mandatory.", "deviceType");
            }

            if ((deviceIP == null) || (deviceIP.Address == null))
            {
                throw new ArgumentException("Device IP address is mandatory.", "deviceIP");
            }

            this.DeviceName = deviceName;
            this.DeviceType = deviceType;
            this.streamData = new Collection<DBStream>();
            this.eventsData = new Collection<EventObject>();
            this.DeviceIPEndPoint = deviceIP;
            this.LastEvent = lastEvent;
            this.LastError = null;
            this.messengerFactory = new MessengerFactory();
            this.DefinitionFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Definitions\\") + this.DeviceType + ".xml";
            this.deviceCategory = MonitorConfigurationHelper.DEVICE_CATEGORY_UNDEFINED;
            this.FakeLocalData = fakeLocalData;
            this.SnmpCommunity = snmpCommunity;
            this.NoMatchDeviceStatus = null;
            this.Type = type;

            try
            {
                MonitorConfigurationHelper monitorConfigurationHelper = new MonitorConfigurationHelper(this.FakeLocalData, this.DefinitionFileName,
                                                                                                       this.messengerFactory, this.DeviceIPEndPoint,
                                                                                                       this.SnmpCommunity, this.Type);

                // Elaboriamo la definizione per sapere se contiene una configurazione di caricamento dinamico
                // Nel caso positivo, si riconfigurerà su un file di definizione corretto e troveremo la categoria della periferica impostata
                // Il precaricamento imposta anche la versione Snmp per come è indicata nella definizione originale,
                // in modo da poter caricare i dati della categoria e del produttore via SNMP
                monitorConfigurationHelper.PreloadConfiguration();

                this.SnmpVersionCode = monitorConfigurationHelper.SnmpVersionCode;
                this.deviceCategory = monitorConfigurationHelper.DeviceCategory;
                this.hasDynamicLoadingRules = monitorConfigurationHelper.HasDynamicLoadingRules;
                this.DefinitionFileName = monitorConfigurationHelper.DefinitionFileName;
                this.NoMatchDeviceStatus = monitorConfigurationHelper.NoMatchDeviceStatus;

                monitorConfigurationHelper.LoadConfiguration(this.streamData);

                // rileggiamo la versione Snmp nel caso sia cambiata a seguito di caricamento successivo di definizione dinamica
                this.SnmpVersionCode = monitorConfigurationHelper.SnmpVersionCode;
            }
            catch (DeviceConfigurationHelperException ex)
            {
                this.LastError = ex;
            }
            catch (ArgumentException ex)
            {
                this.LastError = ex;
            }

            if (this.LastError == null)
            {
                this.CheckStatus();
            }
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///   Nome della periferica
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        ///   Codice univoco della periferica
        /// </summary>
        public string DeviceType { get; private set; }

        /// <summary>
        ///   Community string per l'accesso Snmp in lettura
        /// </summary>
        public string SnmpCommunity { get; private set; }

        /// <summary>
        ///   Versione del protocollo Snmp per le richieste
        /// </summary>
        public VersionCode SnmpVersionCode { get; private set; }

        /// <summary>
        ///   Nome completo della definizione XML della periferica
        /// </summary>
        public string DefinitionFileName { get; private set; }

        /// <summary>
        ///   EndPoint IP della periferica
        /// </summary>
        public IPEndPoint DeviceIPEndPoint { get; private set; }

        /// <summary>
        ///   Lista degli Stream relativi alla periferica
        /// </summary>
        public Collection<DBStream> StreamData
        {
            get
            {
                if (this.LastError != null)
                {
                    return new Collection<DBStream>();
                }

                return this.streamData;
            }
        }

        /// <summary>
        ///   Genere della periferica
        /// </summary>
        public DeviceKind Type { get; private set; }

        /// <summary>
        ///   Indica se caricare i dati da file di risposte locali per simulazione
        /// </summary>
        public bool FakeLocalData { get; private set; }

        /// <summary>
        ///   <para>Severità relativa al Device.</para>
        ///   <para>Nel caso il Device sia visibile come ICMP, ma non come SNMP, la severità è sconosciuta.</para>
        /// </summary>
        public Severity SeverityLevel
        {
            get
            {
                if (this.LastError != null)
                {
                    return Severity.Unknown;
                }

                if (this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return Severity.Warning;
                }

                // Se sono impostate la severità e la descrizione forzate per la periferica, restituisce quelle
                // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
                // con regole dinamiche che è ricaduta nella MIB-II base
                if (this.NoMatchDeviceStatus != null)
                {
                    return this.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity;
                }

                Severity streamSeverity = Severity.Unknown;

                if (this.StreamData.Count > 0)
                {
                    int streamDataMib2BaseIndex = -1;

                    for (int streamCounter = 0; streamCounter < this.StreamData.Count; streamCounter++)
                    {
                        // in caso siano definiti più stream come base MIB-II, sarà considerato il primo
                        if (this.StreamData[streamCounter].IsMib2)
                        {
                            streamDataMib2BaseIndex = streamCounter;
                            break;
                        }
                    }

                    if (this.StreamData.Count == 1)
                    {
                        if (streamDataMib2BaseIndex >= 0)
                        {
                            // esiste solo la MIB-II base
                            if ((this.streamData[streamDataMib2BaseIndex].SeverityLevel != Severity.Unknown) &&
                                ((streamSeverity == Severity.Unknown) ||
                                 ((byte)this.StreamData[streamDataMib2BaseIndex].SeverityLevel > (byte)streamSeverity)))
                            {
                                streamSeverity = this.StreamData[streamDataMib2BaseIndex].SeverityLevel;
                            }
                        }
                        else
                        {
                            // restituiamo lo stato dell'unico stream esistente
                            streamDataMib2BaseIndex = 0;

                            // Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
                            if ((this.StreamData[streamDataMib2BaseIndex].SeverityLevel != Severity.Unknown) &&
                                ((streamSeverity == Severity.Unknown) ||
                                 ((byte)this.StreamData[streamDataMib2BaseIndex].SeverityLevel > (byte)streamSeverity)) &&
                                (!this.StreamData[streamDataMib2BaseIndex].ExcludedFromSeverityComputation))
                            {
                                streamSeverity = this.StreamData[streamDataMib2BaseIndex].SeverityLevel;
                            }
                        }
                    }
                    else
                    {
                        // Livello di severità della MIB-II parziale
                        Severity mib2BaseStreamSeverity = Severity.Unknown;

                        if (streamDataMib2BaseIndex >= 0)
                        {
                            if ((this.StreamData[streamDataMib2BaseIndex].SeverityLevel != Severity.Unknown) &&
                                ((mib2BaseStreamSeverity == Severity.Unknown) ||
                                 ((byte)this.StreamData[streamDataMib2BaseIndex].SeverityLevel > (byte)mib2BaseStreamSeverity)))
                            {
                                mib2BaseStreamSeverity = this.StreamData[streamDataMib2BaseIndex].SeverityLevel;
                            }
                        }
                        else
                        {
                            // nel caso che non sia definita nessuna MIB-II base, lo stato rimane a sconosciuto
                            mib2BaseStreamSeverity = Severity.Unknown;
                        }

                        // Livello di severità delle MIB proprietarie
                        Severity customMibStreamSeverity = Severity.Unknown;

                        for (int streamCounter = 0; streamCounter < this.StreamData.Count; streamCounter++)
                        {
                            if (this.StreamData[streamCounter].IsMib2)
                            {
                                // saltiamo tutti gli stream marcati come di tipo MIB-II base
                                continue;
                            }

                            // Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
                            if ((this.StreamData[streamCounter].SeverityLevel != Severity.Unknown) &&
                                ((customMibStreamSeverity == Severity.Unknown) ||
                                 ((byte)this.StreamData[streamCounter].SeverityLevel > (byte)customMibStreamSeverity)) &&
                                (!this.StreamData[streamCounter].ExcludedFromSeverityComputation))
                            {
                                customMibStreamSeverity = this.StreamData[streamCounter].SeverityLevel;
                            }

                            if ((!this.deviceContainsStreamWithError) && (this.StreamData[streamCounter].ContainsStreamFieldWithError))
                            {
                                this.deviceContainsStreamWithError = true;
                            }
                        }

                        if ((customMibStreamSeverity == Severity.Unknown) && (mib2BaseStreamSeverity == Severity.Ok))
                        {
                            // Nel caso che non risponda la MIB proprietaria, ma solo quella base, siamo in stato di allarme lieve
                            // (nel caso ordinario, saremmo in OK) - la periferica è probabilmente del tipo sbagliato nel System.xml
                            streamSeverity = Severity.Warning;
                            this.replyOnlyToMIBBase = true;
                        }
                        else
                        {
                            foreach (DBStream stream in this.StreamData)
                            {
                                // Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
                                if ((stream.SeverityLevel != Severity.Unknown) &&
                                    ((streamSeverity == Severity.Unknown) || ((byte)stream.SeverityLevel > (byte)streamSeverity)) &&
                                    (!stream.ExcludedFromSeverityComputation))
                                {
                                    streamSeverity = stream.SeverityLevel;
                                }
                            }
                        }

                        // Valutiamo, come ultima cosa, la condizione di eccezione negli stream field.
                        // Essendo la finale, prevale su qualsiasi altra, ma solo se la periferica non è già in errore critico
                        // In caso di modifiche alla severità della condizione di eccezione, occorre adattare il codice
                        // in modo che la severità forzata non nasconda severità più gravi calcolate
                        if ((streamSeverity != Severity.Error) && (this.deviceContainsStreamWithError))
                        {
                            streamSeverity = Severity.Warning;
                        }
                    }
                }

                return streamSeverity;
            }
        }

        /// <summary>
        ///   Stato del Device, inteso come Online (raggiungibile almeno via SNMP) oppure Offline (non raggiungibile e non monitorabile)
        /// </summary>
        public byte Offline
        {
            get
            {
                if (this.IsAliveIcmp && this.IsAliveSnmp)
                {
                    return 0;
                }

                if (!this.IsAliveIcmp && this.IsAliveSnmp)
                {
                    return 0;
                }

                if (this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return 0;
                }

                return 1;
            }
        }

        /// <summary>
        ///   <para>Decodifica descrittiva dello stato della periferica</para>
        ///   <para>0 = "In servizio" - Colore verde, significa che la periferica è in servizio e non presenta alcuna anomalia</para>
        ///   <para>1 = "Anomalia lieve" - Colore giallo, significa che ci sono delle anomalie al funzionamento ma non pregiudicano il servizio del sistema</para>
        ///   <para>2 = "Anomalia grave" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
        ///   <para>255 = "Sconosciuto" - Non è stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                if (this.LastError != null)
                {
                    return Settings.Default.DeviceStatusMessageUnknown;
                }

                if (this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return Settings.Default.DeviceStatusMessageMaintenanceMode;
                }

                if ((this.IsAliveIcmp && this.IsAliveSnmp) || (!this.IsAliveIcmp && this.IsAliveSnmp))
                {
                    // Se sono impostate la severità e la descrizione forzate per la periferica, restiuisce quelle
                    // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
                    // con regole dinamiche che è ricaduta nella MIB-II base
                    if (this.NoMatchDeviceStatus != null)
                    {
                        return this.NoMatchDeviceStatus.NoMatchDeviceStatusDescription;
                    }

                    switch (this.SeverityLevel)
                    {
                        case Severity.Ok:
                            return Settings.Default.DeviceStatusMessageOK;
                        case Severity.Warning:
                            if (this.deviceContainsStreamWithError)
                            {
                                return Settings.Default.DeviceStatusMessageIncompleteData;
                            }

                            if (this.replyOnlyToMIBBase)
                            {
                                return Settings.Default.DeviceStatusMessageMaintenanceModeMIB2Only;
                            }

                            return Settings.Default.DeviceStatusMessageWarning;
                        case Severity.Error:
                            return Settings.Default.DeviceStatusMessageError;
                        case Severity.Unknown:
                            return Settings.Default.DeviceStatusMessageUnknown;
                    }
                }

                if (!this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return Settings.Default.DeviceStatusMessageUnreachable;
                }

                return Settings.Default.DeviceStatusMessageUnknown;
            }
        }

        /// <summary>
        ///   Indica se la periferica è raggiungibile via ICMP (ping)
        /// </summary>
        public bool IsAliveIcmp { get; private set; }

        /// <summary>
        ///   Indica se la periferica è raggiungibile via query SNMP
        /// </summary>
        public bool IsAliveSnmp { get; private set; }

        /// <summary>
        ///   Indica se c'è stata una eccezione durante il caricamento della periferica
        /// </summary>
        public Exception LastError { get; private set; }

        /// <summary>
        ///   Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        public EventObject LastEvent { get; private set; }

        /// <summary>
        ///   Lista degli Eventi relativi alla periferica
        /// </summary>
        public Collection<EventObject> EventsData
        {
            get { return this.eventsData; }
        }

        /// <summary>
        ///   Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        #endregion

        #region Metodi privati

        private void RetrieveData()
        {
            Dictionary<ObjectIdentifier, Variable[,]> snmpTableCache = new Dictionary<ObjectIdentifier, Variable[,]>();

            Dictionary<ObjectIdentifier, IList<Variable>> snmpScalarCache = new Dictionary<ObjectIdentifier, IList<Variable>>();

            foreach (DBStream stream in this.StreamData)
            {
                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Thread: {0} - Data collection Stream ID: {1} - Stream Name: {2}",
                                                                                 Thread.CurrentThread.GetHashCode(), stream.StreamId, stream.Name));
                }

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                     "Thread: {0} - Data collection Stream Field ID: {1} - Stream Field Name: {2}",
                                                                                     Thread.CurrentThread.GetHashCode(), field.FieldId, field.Name));
                    }

                    if (field.FieldTableOid != null)
                    {
                        #region Recupero dati SNMP tabellari

                        // La presenza dell'Oid per la query tabellare forza l'uso della gestione tabellare

                        Variable[,] table;

                        if (snmpTableCache.ContainsKey(field.FieldTableOid))
                        {
                            if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Data collection tabular SNMP from cache, Oid: {1}",
                                                                                             Thread.CurrentThread.GetHashCode(), field.FieldTableOid));
                            }

                            table = snmpTableCache[field.FieldTableOid];
                        }
                        else
                        {
                            table = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                                                       new OctetString(this.SnmpCommunity), field.FieldTableOid);
                            snmpTableCache.Add(field.FieldTableOid, table);
                        }

                        Variable[,] correlationTable = null;

                        if (field.FieldCorrelationTableOid != null)
                        {
                            // Estraiamo la tabella correlata prima possibile rispetto a quella principale, perché potrebbe essere necessario usare un indice di riga
                            // della tabella principale per accedere a quella correlata, che nel frattempo potrebbe essere cambiata (ad esempio le liste processi)

                            if (snmpTableCache.ContainsKey(field.FieldCorrelationTableOid))
                            {
                                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                {
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                                 "Thread: {0} - Data collection tabular SNMP from cache, Oid: {1}",
                                                                                                 Thread.CurrentThread.GetHashCode(),
                                                                                                 field.FieldCorrelationTableOid));
                                }

                                correlationTable = snmpTableCache[field.FieldCorrelationTableOid];
                            }
                            else
                            {
                                correlationTable = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                                                                      new OctetString(this.SnmpCommunity),
                                                                                      field.FieldCorrelationTableOid);
                                snmpTableCache.Add(field.FieldCorrelationTableOid, correlationTable);
                            }
                        }

                        #region Valutazione di contatore di righe di tabella dinamica e relativo indice di riga da definizione

                        if ((field.TableIndex.HasValue) && (field.TableIndex.Value >= 0) && (field.ScalarTableCounterOid != null))
                        {
                            // E' indicato un indice di riga dinamica per la tabella e il relativo Oid da cui recuperare il valore
                            // Nel caso che il contatore delle righe della tabella dinamica sia maggiore o uguale all'indice corrente di riga,
                            // riportato nella definizione, lo stream field dovrà essere salvato su database, altrimenti sarà saltato
                            IList<Variable> tableCounters;

                            if (snmpScalarCache.ContainsKey(field.ScalarTableCounterOid))
                            {
                                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                {
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                                 "Thread: {0} - Data collection scalar SNMP from cache, Oid: {1}",
                                                                                                 Thread.CurrentThread.GetHashCode(),
                                                                                                 field.ScalarTableCounterOid));
                                }

                                tableCounters = snmpScalarCache[field.ScalarTableCounterOid];
                            }
                            else
                            {
                                tableCounters = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                                                                  new OctetString(this.SnmpCommunity),
                                                                                  new List<Variable> {new Variable(field.ScalarTableCounterOid)});

                                snmpScalarCache.Add(field.ScalarTableCounterOid, tableCounters);
                            }

                            // Inizializziamo lo stream field in modo che sia saltato durante la persistenza su base dati
                            field.PersistOnDatabase = false;

                            if ((tableCounters != null) && (tableCounters.Count > 0) && (tableCounters[0].Data != null))
                            {
                                string dynamicTableRowCounterString = SnmpUtility.DecodeDataByType(tableCounters[0]).Trim();

                                if (!string.IsNullOrEmpty(dynamicTableRowCounterString))
                                {
                                    int dynamicTableRowCounter;

                                    if (int.TryParse(dynamicTableRowCounterString, out dynamicTableRowCounter))
                                    {
                                        // Il contatore di righe per la tabella dinamica ha un valore valido
                                        if (field.TableIndex.Value <= dynamicTableRowCounter)
                                        {
                                            // Lo stream field appartiene ad una riga dinamica di tabella con
                                            // indice inferiore o uguale al contatore totale, quindi può essere
                                            // salvato
                                            field.PersistOnDatabase = true;
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        string snmpValue = null;

                        if (field.TableRowIndex.HasValue)
                        {
                            // E' stata indicata una riga per l'accesso alla tabella
                            try
                            {
                                snmpValue = SnmpUtility.GetValueFromTableAtRow(table, field.TableRowIndex.Value, field.TableEvaluationFormula);
                            }
                            catch (DeviceConfigurationHelperException ex)
                            {
                                field.LastError =
                                    new DeviceConfigurationHelperException(
                                        string.Format(CultureInfo.InvariantCulture,
                                                      "Error collecting SNMP data from device, StreamField: '{0}', ID: {1}", field.Name, field.FieldId),
                                        ex);
                            }
                        }
                        else
                        {
                            if ((field.TableFilterColumnIndex.HasValue) && (field.TableFilterColumnRegex != null))
                            {
                                if (field.FieldCorrelationTableOid == null)
                                {
                                    // Recupero di dati da una tabella, usando un filtro specifico su colonna
                                    // per individuare la prima riga possibile che corrisponde alla regex
                                    try
                                    {
                                        snmpValue = SnmpUtility.GetValueFromTableWithColumnFilter(table, field.TableFilterColumnIndex.Value,
                                                                                                  field.TableFilterColumnRegex,
                                                                                                  field.TableEvaluationFormula);
                                    }
                                    catch (DeviceConfigurationHelperException ex)
                                    {
                                        field.LastError =
                                            new DeviceConfigurationHelperException(
                                                string.Format(CultureInfo.InvariantCulture,
                                                              "Error collecting SNMP data from device, StreamField: '{0}', ID: {1}", field.Name,
                                                              field.FieldId), ex);
                                    }
                                }
                                else
                                {
                                    if (field.TableCorrelationFilterColumnIndex.HasValue)
                                    {
                                        // Recupero di dati da una tabella correlata, usando un filtro su colonna indicata ed un valore estratto
                                        // dalla tabella principale, filtrando su una colonna specifica ed applicando una regex
                                        try
                                        {
                                            snmpValue = SnmpUtility.GetValueFromCorrelationTableWithCorrelationValue(table, correlationTable,
                                                                                                                     field.TableFilterColumnIndex.
                                                                                                                         Value,
                                                                                                                     field.
                                                                                                                         TableCorrelationFilterColumnIndex
                                                                                                                         .Value,
                                                                                                                     field.TableFilterColumnRegex,
                                                                                                                     field.TableEvaluationFormula,
                                                                                                                     field.
                                                                                                                         TableCorrelationEvaluationFormula);
                                        }
                                        catch (DeviceConfigurationHelperException ex)
                                        {
                                            field.LastError =
                                                new DeviceConfigurationHelperException(
                                                    string.Format(CultureInfo.InvariantCulture,
                                                                  "Error collecting SNMP data from device, StreamField: '{0}', ID: {1}", field.Name,
                                                                  field.FieldId), ex);
                                        }
                                    }
                                    else
                                    {
                                        // Recupero di dati da una tabella correlata, usando un indice di riga trovato con una ricerca
                                        // sulla tabella principale, su colonna specifica, usando una regex
                                        try
                                        {
                                            snmpValue = SnmpUtility.GetValueFromCorrelationTableWithColumnFilter(table, correlationTable,
                                                                                                                 field.TableFilterColumnIndex.Value,
                                                                                                                 field.TableFilterColumnRegex,
                                                                                                                 field.
                                                                                                                     TableCorrelationEvaluationFormula);
                                        }
                                        catch (DeviceConfigurationHelperException ex)
                                        {
                                            field.LastError =
                                                new DeviceConfigurationHelperException(
                                                    string.Format(CultureInfo.InvariantCulture,
                                                                  "Error collecting SNMP data from device, StreamField: '{0}', ID: {1}", field.Name,
                                                                  field.FieldId), ex);
                                        }
                                    }
                                }
                            }
                        }

                        if (snmpValue != null)
                        {
                            IList<Variable> valuesString = new List<Variable> {new Variable(field.FieldTableOid, new OctetString(snmpValue))};
                            field.RawData = valuesString;
                        }
                        else
                        {
                            if (field.LastError != null)
                            {
                                // La severità sarà calcolata internamente allo StreamField,
                                // basandosi sul fatto che il valore è nullo e l'errore è impostato
                                field.SeverityLevel = Severity.Warning;
                                field.ValueDescription = Settings.Default.ErrorOnDataRetrievalMessage;
                                field.Value = Settings.Default.ErrorOnDataRetrievalValue;
                            }
                            else if (field.NoSnmpDataAvailableForcedState != null)
                            {
                                field.SeverityLevel = field.NoSnmpDataAvailableForcedState.ForcedSeverity;
                                field.ValueDescription = field.NoSnmpDataAvailableForcedState.ForcedValueDescription;
                                field.Value = field.NoSnmpDataAvailableForcedState.ForcedValue;
                            }
                            else
                            {
                                field.SeverityLevel = Severity.Unknown;
                                field.ValueDescription = Settings.Default.NoDataAvailableMessage;
                                field.Value = Settings.Default.NoDataAvailableValue;

                                if (field.HideIfNoDataAvailable)
                                {
                                    // Se non ci sono dati validi restituiti dalla query e il campo non deve essere persistito in questo caso, lo forziamo
                                    field.PersistOnDatabase = false;
                                }
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region Recupero dati SNMP a valore scalare

                        IList<Variable> values;

                        if (snmpScalarCache.ContainsKey(field.FieldOid))
                        {
                            if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Data collection scalar SNMP from cache, Oid: {1}",
                                                                                             Thread.CurrentThread.GetHashCode(), field.FieldOid));
                            }

                            values = snmpScalarCache[field.FieldOid];
                        }
                        else
                        {
                            values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                                                       new OctetString(this.SnmpCommunity),
                                                                       new List<Variable> {new Variable(field.FieldOid)});

                            snmpScalarCache.Add(field.FieldOid, values);
                        }

                        if ((values == null) || (values.Count == 0))
                        {
                            if (field.NoSnmpDataAvailableForcedState != null)
                            {
                                field.SeverityLevel = field.NoSnmpDataAvailableForcedState.ForcedSeverity;
                                field.ValueDescription = field.NoSnmpDataAvailableForcedState.ForcedValueDescription;
                                field.Value = field.NoSnmpDataAvailableForcedState.ForcedValue;
                            }
                            else
                            {
                                field.SeverityLevel = Severity.Unknown;
                                field.ValueDescription = Settings.Default.NoDataAvailableMessage;
                                field.Value = Settings.Default.NoDataAvailableValue;

                                if (field.HideIfNoDataAvailable)
                                {
                                    // Se non ci sono dati validi restituiti dalla query e il campo non deve essere persistito in questo caso, lo forziamo
                                    field.PersistOnDatabase = false;
                                }
                            }
                        }
                        else
                        {
                            IList<Variable> valuesString = new List<Variable>();

                            foreach (Variable snmpValue in values)
                            {
                                valuesString.Add(snmpValue);
                            }

                            field.RawData = valuesString;
                        }

                        #endregion
                    }
                }
            }
        }

        private void CheckStatus()
        {
            this.GetIsAliveIcmp();
            this.GetIsAliveSnmp();
        }

        private void GetIsAliveIcmp()
        {
            this.IsAliveIcmp = this.messengerFactory.GetPingResponse(this.FakeLocalData, this.DeviceIPEndPoint);
        }

        private void GetIsAliveSnmp()
        {
            bool returnValue = false;

            IList<Variable> snmpData = this.messengerFactory.GetAliveSnmp(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                                                          new OctetString(this.SnmpCommunity),
                                                                          new List<Variable> {new Variable(this.oidSysName)});

            if ((snmpData != null) && (snmpData.Count > 0) && (snmpData[OID_SYS_NAME_INDEX].Data != null))
            {
                string value = SnmpUtility.DecodeDataByType(snmpData[OID_SYS_NAME_INDEX]);

                if (value.Trim().Length > 0)
                {
                    returnValue = true;
                }
            }

            this.IsAliveSnmp = returnValue;
        }

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        private void PopulateInternal()
        {
            this.LastError = null;

            if (this.IsAliveSnmp)
            {
                this.RetrieveData();
            }

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate();
                    }
                }
            }
            else
            {
                this.isPopulated = false;
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        public void Populate()
        {
            this.LastError = null;

            if (this.IsAliveSnmp)
            {
                this.RetrieveData();
            }

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate();
                    }
                }

                if (Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem > 0)
                {
                    // E' attivo il tentativo di recupero in caso di severità non normale della periferica
                    // Tenta di recuperare una periferica in condizione di errore, ma non permanente (fluttuazione di stato)

                    // C'è già stato un tentativo, quello principale, quindi il prossimo sarà il secondo
                    ushort retryCounter = 2;

                    while (retryCounter <= Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem)
                    {
                        if (this.SeverityLevel != Severity.Ok)
                        {
                            Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenRetriesToRecoverFromDeviceSeverityProblem);
                            this.PopulateInternal();
                            retryCounter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                this.isPopulated = false;
            }
        }

        /// <summary>
        ///   Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
        /// </summary>
        /// <returns>Rappresentazione completa e descrittiva della periferica a fini diagnostici</returns>
        public string Dump()
        {
            if ((!this.isPopulated) && (this.LastError == null))
            {
                // Se ci sono errori impostati, è già stato tentato un popolamento, quindi è inutile ritentare
                this.Populate();
            }

            StringBuilder dump = new StringBuilder();

            if (this.hasDynamicLoadingRules)
            {
                dump.AppendFormat(CultureInfo.InvariantCulture,
                                  "Device Name: {0}, Type: {1}, Community: {2}, SNMP Version: {3}, Offline: {4}, Severity: {5}, ReplyICMP: {6}, ReplySNMP: {7}, Device Category: {8}, Device with dynamic loading rules enabled{9}" +
                                  Environment.NewLine, this.DeviceName, this.DeviceType, this.SnmpCommunity, this.SnmpVersionCode, this.Offline,
                                  (byte)this.SeverityLevel, this.IsAliveIcmp, this.IsAliveSnmp, this.deviceCategory,
                                  ((this.NoMatchDeviceStatus != null)
                                       ? string.Format(CultureInfo.InvariantCulture,
                                                       ", NoMatchDeviceStatusSeverity: {0}, NoMatchDeviceStatusDescription: {1}",
                                                       this.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity,
                                                       this.NoMatchDeviceStatus.NoMatchDeviceStatusDescription) : string.Empty));
                dump.AppendFormat("Dynamic Definition File: {0}" + Environment.NewLine, Path.GetFileName(this.DefinitionFileName));
            }
            else
            {
                dump.AppendFormat(CultureInfo.InvariantCulture,
                                  "Device Name: {0}, Type: {1}, Community: {2}, SNMP Version: {3}, Offline: {4}, Severity: {5}, ReplyICMP: {6}, ReplySNMP: {7}" +
                                  Environment.NewLine, this.DeviceName, this.DeviceType, this.SnmpCommunity, this.SnmpVersionCode, this.Offline,
                                  (byte)this.SeverityLevel, this.IsAliveIcmp, this.IsAliveSnmp);
            }

            foreach (DBStream stream in this.StreamData)
            {
                dump.AppendFormat(CultureInfo.InvariantCulture, "Stream Id: {0}, Name: {1}, Severity: {2}, Description: {3}" + Environment.NewLine,
                                  stream.StreamId, stream.Name, (byte)stream.SeverityLevel, stream.ValueDescriptionComplete);

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (field.PersistOnDatabase)
                    {
                        dump.AppendFormat(CultureInfo.InvariantCulture,
                                          "Field Id: {0}, Array Id: {1}, Name: {2}, Severity: {3}, Value: {4}, Description: {5}{6}" +
                                          Environment.NewLine, field.FieldId, field.ArrayId, field.Name, (byte)field.SeverityLevel, field.Value,
                                          field.ValueDescriptionComplete,
                                          (field.LastError != null
                                               ? string.Format(CultureInfo.InvariantCulture, ", Exception: {0}, Inner Exception: {1}",
                                                               field.LastError.Message,
                                                               (field.LastError.InnerException != null)
                                                                   ? field.LastError.InnerException.Message : "N/A") : string.Empty));
                    }
                }
            }

            return dump.ToString();
        }

        #endregion
    }
}
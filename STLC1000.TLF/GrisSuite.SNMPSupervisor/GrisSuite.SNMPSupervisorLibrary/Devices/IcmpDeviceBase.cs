﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Rappresenta la classe base per i dispositivi ICMP
    /// </summary>
    public class IcmpDeviceBase : IDevice
    {
        #region Variabili private

        private bool isPopulated;
        private readonly Collection<DBStream> streamData;
        private readonly Collection<EventObject> eventsData;
        private readonly MessengerFactory messengerFactory;

        #endregion

        #region Costruttore

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "deviceName">Nome della periferica</param>
        /// <param name = "deviceType">Codice univoco della periferica</param>
        /// <param name = "deviceIP">Indirizzo IP e porta della periferica</param>
        /// <param name = "lastEvent">Informazioni dell'ultimo evento in ordine temporale associato alla periferica</param>
        /// <param name = "type">Genere della periferica</param>
        public IcmpDeviceBase(bool fakeLocalData, string deviceName, string deviceType, IPEndPoint deviceIP, EventObject lastEvent, DeviceKind type)
        {
            if (String.IsNullOrEmpty(deviceName))
            {
                throw new ArgumentException("Device name is mandatory.", "deviceName");
            }

            if (String.IsNullOrEmpty(deviceType))
            {
                throw new ArgumentException("Device type is mandatory.", "deviceType");
            }

            if ((deviceIP == null) || (deviceIP.Address == null))
            {
                throw new ArgumentException("Device IP address is mandatory.", "deviceIP");
            }

            this.DeviceName = deviceName;
            this.DeviceType = deviceType;
            this.streamData = new Collection<DBStream>();
            this.eventsData = new Collection<EventObject>();
            this.DeviceIPEndPoint = deviceIP;
            this.LastEvent = lastEvent;
            this.LastError = null;
            this.messengerFactory = new MessengerFactory();
            this.DefinitionFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Definitions\\") + this.DeviceType + ".xml";
            this.FakeLocalData = fakeLocalData;
            this.Type = type;

            try
            {
                MonitorConfigurationHelper monitorConfigurationHelper = new MonitorConfigurationHelper(this.FakeLocalData, this.DefinitionFileName, this.messengerFactory,
                                                                                                       this.DeviceIPEndPoint, string.Empty, this.Type);

                monitorConfigurationHelper.PreloadConfiguration();

                monitorConfigurationHelper.LoadConfiguration(this.streamData);
            }
            catch (DeviceConfigurationHelperException ex)
            {
                this.LastError = ex;
            }
            catch (ArgumentException ex)
            {
                this.LastError = ex;
            }

            if (this.LastError == null)
            {
                this.CheckStatus();
            }
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///   Nome della periferica
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        ///   Codice univoco della periferica
        /// </summary>
        public string DeviceType { get; private set; }

        /// <summary>
        ///   Nome completo della definizione XML della periferica
        /// </summary>
        public string DefinitionFileName { get; private set; }

        /// <summary>
        ///   EndPoint IP della periferica
        /// </summary>
        public IPEndPoint DeviceIPEndPoint { get; private set; }

        /// <summary>
        ///   Lista degli Stream relativi alla periferica
        /// </summary>
        public Collection<DBStream> StreamData
        {
            get
            {
                if (this.LastError != null)
                {
                    return new Collection<DBStream>();
                }

                return this.streamData;
            }
        }

        /// <summary>
        ///   Indica se caricare i dati da file di risposte locali per simulazione
        /// </summary>
        public bool FakeLocalData { get; private set; }

        /// <summary>
        ///   Severità relativa al Device
        /// </summary>
        public Severity SeverityLevel
        {
            get
            {
                if (this.LastError != null)
                {
                    return Severity.Unknown;
                }

                Severity streamSeverity = Severity.Unknown;

                if (this.StreamData.Count > 0)
                {
                    // restituiamo lo stato dell'unico stream esistente
                    if ((this.StreamData[0].SeverityLevel != Severity.Unknown) &&
                        ((streamSeverity == Severity.Unknown) || ((byte) this.StreamData[0].SeverityLevel > (byte) streamSeverity)))
                    {
                        streamSeverity = this.StreamData[0].SeverityLevel;
                    }
                }

                return streamSeverity;
            }
        }

        /// <summary>
        ///   Stato del Device, inteso come Online (raggiungibile via ICMP) oppure Offline (non raggiungibile)
        /// </summary>
        public byte Offline
        {
            get
            {
                if (this.IsAliveIcmp)
                {
                    return 0;
                }

                return 1;
            }
        }

        /// <summary>
        ///   <para>Decodifica descrittiva dello stato della periferica</para>
        ///   <para>0 = "In servizio" - Colore verde, significa che la periferica è in servizio e non presenta alcuna anomalia</para>
        ///   <para>1 = "Anomalia lieve" - Colore giallo, significa che ci sono delle anomalie al funzionamento ma non pregiudicano il servizio del sistema</para>
        ///   <para>2 = "Anomalia grave" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
        ///   <para>255 = "Sconosciuto" - Non è stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                if (this.LastError != null)
                {
                    return Settings.Default.DeviceStatusMessageUnknown;
                }

                switch (this.SeverityLevel)
                {
                    case Severity.Ok:
                        return Settings.Default.DeviceStatusMessageOK;
                    case Severity.Warning:
                        return Settings.Default.DeviceStatusMessageWarning;
                    case Severity.Error:
                        return Settings.Default.DeviceStatusMessageError;
                    case Severity.Unknown:
                        return Settings.Default.DeviceStatusMessageUnknown;
                }

                return Settings.Default.DeviceStatusMessageUnknown;
            }
        }

        /// <summary>
        ///   Indica se la periferica è raggiungibile via ICMP (ping)
        /// </summary>
        public bool IsAliveIcmp { get; private set; }

        /// <summary>
        ///   Indica se c'è stata una eccezione durante il caricamento della periferica
        /// </summary>
        public Exception LastError { get; private set; }

        /// <summary>
        ///   Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        public EventObject LastEvent { get; private set; }

        /// <summary>
        ///   Lista degli Eventi relativi alla periferica
        /// </summary>
        public Collection<EventObject> EventsData
        {
            get { return this.eventsData; }
        }

        /// <summary>
        ///   Genere della periferica
        /// </summary>
        public DeviceKind Type { get; private set; }

        #endregion

        #region Metodi privati

        private void RetrieveData()
        {
            foreach (DBStream stream in this.StreamData)
            {
                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                   string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Data collection Stream ID: {1} - Stream Name: {2}",
                                                                                 Thread.CurrentThread.GetHashCode(), stream.StreamId, stream.Name));
                }

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                     "Thread: {0} - Data collection Stream Field ID: {1} - Stream Field Name: {2}",
                                                                                     Thread.CurrentThread.GetHashCode(), field.FieldId, field.Name));
                    }

                    // Generiamo un Oid fittizio
                    IList<Variable> valuesString = new List<Variable> {new Variable(new ObjectIdentifier(new uint[] {1, 1}), new OctetString(this.IsAliveIcmp ? "1" : "0"))};

                    field.RawData = valuesString;

                    break;
                }

                break;
            }
        }

        private void CheckStatus()
        {
            this.GetIsAliveIcmp();
        }

        private void GetIsAliveIcmp()
        {
            this.IsAliveIcmp = this.messengerFactory.GetPingResponse(this.FakeLocalData, this.DeviceIPEndPoint);
        }

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        private void PopulateInternal()
        {
            this.LastError = null;

            this.RetrieveData();

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate();
                    }
                }
            }
            else
            {
                this.isPopulated = false;
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        public void Populate()
        {
            this.LastError = null;

            this.RetrieveData();

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate();
                    }
                }

                if (Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem > 0)
                {
                    // E' attivo il tentativo di recupero in caso di severità non normale della periferica
                    // Tenta di recuperare una periferica in condizione di errore, ma non permanente (fluttuazione di stato)

                    // C'è già stato un tentativo, quello principale, quindi il prossimo sarà il secondo
                    ushort retryCounter = 2;

                    while (retryCounter <= Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem)
                    {
                        if (this.SeverityLevel != Severity.Ok)
                        {
                            Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenRetriesToRecoverFromDeviceSeverityProblem);
                            this.PopulateInternal();
                            retryCounter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                this.isPopulated = false;
            }
        }

        /// <summary>
        ///   Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
        /// </summary>
        /// <returns>Rappresentazione completa e descrittiva della periferica a fini diagnostici</returns>
        public string Dump()
        {
            if ((!this.isPopulated) && (this.LastError == null))
            {
                // Se ci sono errori impostati, è già stato tentato un popolamento, quindi è inutile ritentare
                this.Populate();
            }

            StringBuilder dump = new StringBuilder();

            dump.AppendFormat(CultureInfo.InvariantCulture, "Device Name: {0}, Type: {1}, Offline: {2}, Severity: {3}, ReplyICMP: {4}" + Environment.NewLine, this.DeviceName,
                              this.DeviceType, this.Offline, (byte) this.SeverityLevel, this.IsAliveIcmp);

            foreach (DBStream stream in this.StreamData)
            {
                dump.AppendFormat(CultureInfo.InvariantCulture, "Stream Id: {0}, Name: {1}, Severity: {2}, Description: {3}" + Environment.NewLine, stream.StreamId, stream.Name,
                                  (byte) stream.SeverityLevel, stream.ValueDescriptionComplete);

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (field.PersistOnDatabase)
                    {
                        dump.AppendFormat(CultureInfo.InvariantCulture, "Field Id: {0}, Array Id: {1}, Name: {2}, Severity: {3}, Value: {4}, Description: {5}" + Environment.NewLine,
                                          field.FieldId, field.ArrayId, field.Name, (byte) field.SeverityLevel, field.Value, field.ValueDescriptionComplete);
                    }
                }
            }

            return dump.ToString();
        }

        #endregion
    }
}
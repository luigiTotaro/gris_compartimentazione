﻿using System;
using System.CodeDom.Compiler;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;
using Microsoft.CSharp;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    internal static class SnmpUtility
    {
        private const string DYNAMIC_ASSEMBLY_FILENAME_PREFIX = "DynamicSnmpTableEvaluation";
        private const string DYNAMIC_ASSEMBLY_FOLDER = "DynamicAssemblies";
        private static readonly object locker = new object();

        #region DecodeDataByType - Produce una rappresentazione stringa di un tipo dati SNMP

        /// <summary>
        ///   Produce una rappresentazione stringa di un tipo dati SNMP
        /// </summary>
        /// <param name = "inputData">Variabile SNMP</param>
        /// <returns>Rappresentazione interpretata della variabile</returns>
        internal static string DecodeDataByType(Variable inputData)
        {
            string data;
            switch (inputData.Data.TypeCode)
            {
                case SnmpType.TimeTicks:
                    data = DecodeTimeTicksData(inputData);
                    break;
                default:
                    data = inputData.Data.ToString();
                    break;
            }

            return data;
        }

        #endregion

        #region GetRawStringData - Produce una rappresentazione stringa di un tipo dati SNMP, senza nessuna decodifica in base al tipo dati

        /// <summary>
        ///   Produce una rappresentazione stringa di un tipo dati SNMP, senza nessuna decodifica in base al tipo dati
        /// </summary>
        /// <param name = "inputData">Variabile SNMP</param>
        /// <returns>Rappresentazione della variabile</returns>
        internal static string GetRawStringData(Variable inputData)
        {
            return inputData.Data.ToString();
        }

        #endregion

        #region DecodeTimeTicksData - Decodifica formato TimeTicks in giorni, ore, minuti, secondi

        private static string DecodeTimeTicksData(Variable inputData)
        {
            return DecodeTimeTicksData(inputData, TimeTicksFormat.Simple);
        }

        private static string DecodeTimeTicksData(Variable inputData, TimeTicksFormat format)
        {
            string data = string.Empty;

            TimeSpan ts = ((TimeTicks)inputData.Data).ToTimeSpan();

            switch (format)
            {
                case TimeTicksFormat.Undefined:
                case TimeTicksFormat.Simple:
                    if (ts.Days != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} days, ", ts.Days);
                    }
                    else if (ts.Days == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} day, ", ts.Days);
                    }
                    data = data + string.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00}:{2:00}.{3:000}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
                    break;
                case TimeTicksFormat.Extended:
                    if (ts.Days != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} days ", ts.Days);
                    }
                    else if (ts.Days == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} day ", ts.Days);
                    }
                    if (ts.Hours != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} hours ", ts.Hours);
                    }
                    else if (ts.Hours == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} hour ", ts.Hours);
                    }
                    if (ts.Minutes != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} minutes ", ts.Minutes);
                    }
                    else if (ts.Minutes == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} minute ", ts.Minutes);
                    }
                    if (ts.Seconds != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} seconds ", ts.Seconds);
                    }
                    else if (ts.Seconds == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} second ", ts.Seconds);
                    }
                    if (ts.Milliseconds != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} milliseconds", ts.Milliseconds);
                    }
                    else if (ts.Milliseconds == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} millisecond", ts.Milliseconds);
                    }
                    break;
            }

            return data;
        }

        #endregion

        #region TimeTicksFormat - Specifica del formato di formattazione date

        /// <summary>
        ///   Specifica del formato di formattazione date
        /// </summary>
        private enum TimeTicksFormat
        {
            Undefined,
            Extended,
            Simple
        }

        #endregion

        #region Metodi pubblici per l'elaborazione delle tabelle SNMP

        /// <summary>
        ///   Elabora un valore di ritorno, cercando su una specifica colonna della tabella correlata il valore estratto dalla riga della tabella principale, trovata con una espressione regolare di ricerca applicata ad una colonna specifica
        /// </summary>
        /// <param name = "table">Tabella SNMP</param>
        /// <param name = "correlationTable">Tabella SNMP correlata</param>
        /// <param name = "filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name = "correlationFilterColumnIndex">Indice della colonna della tabella correlata sui cui eseguire le ricerche</param>
        /// <param name = "filterColumnRegex">Espressione regolare da applicare alla colonna della tabella, per ottenere la riga cercata</param>
        /// <param name = "evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <param name = "correlationEvaluationFormula">Codice C# per l'elaborazione della riga correlata</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromCorrelationTableWithCorrelationValue(Variable[,] table, Variable[,] correlationTable, int filterColumnIndex,
                                                                                int correlationFilterColumnIndex, Regex filterColumnRegex, string evaluationFormula,
                                                                                string correlationEvaluationFormula)
        {
            string seekValue = null;
            string returnValue = null;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        seekValue = EvaluateValueExpressionFromTable(table, row, evaluationFormula, Settings.Default.ReferencedAssemblies, Settings.Default.EvaluationCode);
                        break;
                    }
                }
            }

            if (!string.IsNullOrEmpty(seekValue))
            {
                if (correlationTable != null)
                {
                    for (int row = 0; row <= correlationTable.GetUpperBound(0); row++)
                    {
                        if ((correlationFilterColumnIndex <= correlationTable.GetUpperBound(1)) &&
                            (Regex.IsMatch(correlationTable[row, correlationFilterColumnIndex].Data.ToString(), seekValue, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)))
                        {
                            returnValue = EvaluateValueExpressionFromTable(correlationTable, row, correlationEvaluationFormula, Settings.Default.ReferencedAssemblies,
                                                                           Settings.Default.EvaluationCode);
                            break;
                        }
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        ///   Elabora un valore di ritorno, data una riga della tabella correlata, trovata con una espressione regolare di ricerca applicata ad una colonna specifica
        /// </summary>
        /// <param name = "table">Tabella SNMP</param>
        /// <param name = "correlationTable">Tabella SNMP correlata</param>
        /// <param name = "filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name = "filterColumnRegex">Espressione regolare da applicare alla colonna della tabella, per ottenere la riga cercata</param>
        /// <param name = "correlationEvaluationFormula">Codice C# per l'elaborazione della riga correlata</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromCorrelationTableWithColumnFilter(Variable[,] table, Variable[,] correlationTable, int filterColumnIndex, Regex filterColumnRegex,
                                                                            string correlationEvaluationFormula)
        {
            int seekValueRow = -1;
            string returnValue = null;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        seekValueRow = row;
                        break;
                    }
                }
            }

            if (seekValueRow >= 0)
            {
                if ((correlationTable != null) && (seekValueRow <= correlationTable.GetUpperBound(0)))
                {
                    returnValue = EvaluateValueExpressionFromTable(correlationTable, seekValueRow, correlationEvaluationFormula, Settings.Default.ReferencedAssemblies,
                                                                   Settings.Default.EvaluationCode);
                }
            }

            return returnValue;
        }

        /// <summary>
        ///   Elabora un valore di ritorno, data una riga della tabella, trovata con una espressione regolare di ricerca applicata ad una colonna specifica
        /// </summary>
        /// <param name = "table">Tabella SNMP</param>
        /// <param name = "filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name = "filterColumnRegex">Espressione regolare da applicare alla colonna della tabella, per ottenere la riga cercata</param>
        /// <param name = "evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromTableWithColumnFilter(Variable[,] table, int filterColumnIndex, Regex filterColumnRegex, string evaluationFormula)
        {
            string returnValue = null;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        returnValue = EvaluateValueExpressionFromTable(table, row, evaluationFormula, Settings.Default.ReferencedAssemblies, Settings.Default.EvaluationCode);
                        break;
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        ///   Elabora un valore di ritorno, basato su una specifica riga di una tabella SNMP
        /// </summary>
        /// <param name = "table">Tabella SNMP</param>
        /// <param name = "rowIndex">Indice della riga della tabella</param>
        /// <param name = "evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromTableAtRow(Variable[,] table, int rowIndex, string evaluationFormula)
        {
            string returnValue = null;

            if (table != null)
            {
                if ((rowIndex <= table.GetUpperBound(0)))
                {
                    returnValue = EvaluateValueExpressionFromTable(table, rowIndex, evaluationFormula, Settings.Default.ReferencedAssemblies, Settings.Default.EvaluationCode);
                }
            }

            return returnValue;
        }

        /// <summary>
        ///   Converte un Oid in formato stringa nel relativo oggetto ObjectIdentifier
        /// </summary>
        /// <param name = "oidString">Stringa che contiene l'Oid, nel formato "1.3.6.1.2.1.1.5.0"</param>
        /// <returns>Oggetto ObjectIdentifier. Null nel caso non sia una stringa valida.</returns>
        internal static ObjectIdentifier TryParseOid(string oidString)
        {
            ObjectIdentifier oid = null;

            try
            {
                oid = new ObjectIdentifier(oidString);
            }
            catch (ArgumentException)
            {
            }
            catch (FormatException)
            {
            }
            catch (OverflowException)
            {
            }

            return oid;
        }

        #endregion

        #region EvaluateValueExpressionFromTable - Elabora i dati di una riga di tabella, eseguendo codice C# dinamico

        /// <summary>
        ///   Elabora i dati di una riga di tabella, eseguendo codice C# dinamico
        /// </summary>
        /// <param name = "table">Tabella SNMP</param>
        /// <param name = "tableRow">Indice della riga della tabella</param>
        /// <param name = "evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <param name = "referencesAssemblies">Assembly referenziati per la compilazione del codice C#</param>
        /// <param name = "evaluationCode">Codice C# che contiene lo scheletro della classe da eseguire</param>
        /// <returns>Valore stringa elaborato</returns>
        private static string EvaluateValueExpressionFromTable(Variable[,] table, int tableRow, string evaluationFormula, string referencesAssemblies, string evaluationCode)
        {
            try
            {
                evaluationFormula = Regex.Replace(evaluationFormula, "#(?<col>\\d{1,10})#", "table[row, ${col}]", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException("Error during evaluation formula expansion processing SNMP table", ex);
            }

            const string className = "Evaluation";
            const string methodName = "GetValue";

            if (string.IsNullOrEmpty(evaluationCode))
            {
                evaluationCode =
                    "internal class {0} {{public string {1}(Lextm.SharpSnmpLib.Variable[,] table, int row) {{if (table == null) {{return null;}} return ({2}).ToString();}}}}";
            }

            string code;
            try
            {
                code = string.Format(CultureInfo.InvariantCulture, evaluationCode, className, methodName, evaluationFormula);
            }
            catch (ArgumentNullException ex)
            {
                throw new DeviceConfigurationHelperException("Error in code formatting inside evaluation formula, processing SNMP table", ex);
            }
            catch (FormatException ex)
            {
                throw new DeviceConfigurationHelperException("Error in code formatting inside evaluation formula, processing SNMP table", ex);
            }

            if (string.IsNullOrEmpty(code))
            {
                throw new DeviceConfigurationHelperException("Error in code formatting inside evaluation formula, processing SNMP table, C# code is null or empty");
            }

            string codeHash;

            using (SHA256 sha = new SHA256Managed())
            {
                try
                {
                    codeHash = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding(1252).GetBytes(code))).Replace("-", string.Empty);
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException("Error on file name generation during dynamic code processing for SNMP table");
                }
                catch (ObjectDisposedException)
                {
                    throw new DeviceConfigurationHelperException("Error on file name generation during dynamic code processing for SNMP table");
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException("Error on file name generation during dynamic code processing for SNMP table");
                }
            }

            string dynamicAssemblyFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DYNAMIC_ASSEMBLY_FOLDER);

            if (!Directory.Exists(dynamicAssemblyFolder))
            {
                try
                {
                    Directory.CreateDirectory(dynamicAssemblyFolder);
                }
                catch (IOException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "Error creating folder to store dynamically generated files for SNMP tables evaluation ({0})",
                                                                               dynamicAssemblyFolder));
                }
                catch (UnauthorizedAccessException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "Error creating folder to store dynamically generated files for SNMP tables evaluation ({0})",
                                                                               dynamicAssemblyFolder));
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "Error creating folder to store dynamically generated files for SNMP tables evaluation ({0})",
                                                                               dynamicAssemblyFolder));
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "Error creating folder to store dynamically generated files for SNMP tables evaluation ({0})",
                                                                               dynamicAssemblyFolder));
                }
            }

            if (string.IsNullOrEmpty(codeHash))
            {
                throw new DeviceConfigurationHelperException("Error creating name for dynamic assembly for SNMP table evaluation");
            }

            string dynamicAssembly = Path.Combine(dynamicAssemblyFolder, string.Format(CultureInfo.InvariantCulture, "{0}-{1}.dll", DYNAMIC_ASSEMBLY_FILENAME_PREFIX, codeHash));

            object loObject = null;
            Assembly loAssembly;

            lock (locker)
            {
                if (FileUtility.CheckFileCanRead(dynamicAssembly))
                {
                    #region Caricamento dinamico Assembly già esistente

                    try
                    {
                        loAssembly = Assembly.LoadFile(dynamicAssembly);
                        loObject = loAssembly.CreateInstance(className);
                    }
                    catch (ArgumentException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture, "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                    }
                    catch (MissingMethodException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture, "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture, "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                    }
                    catch (FileLoadException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture, "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                    }
                    catch (BadImageFormatException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture, "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                    }

                    #endregion
                }
                else
                {
                    #region L'Assembly dinamico non esiste, deve essere generato e compilato

                    #region Preparazione parametri per il compilatore dinamico

                    CompilerParameters parameters = new CompilerParameters();
                    parameters.GenerateExecutable = false;
                    parameters.OutputAssembly = dynamicAssembly;
                    parameters.GenerateInMemory = true;
                    parameters.TreatWarningsAsErrors = true;
                    parameters.TempFiles.KeepFiles = false;
                    parameters.CompilerOptions = "/target:library /optimize+";
                    parameters.IncludeDebugInformation = false;
                    parameters.WarningLevel = 3;

                    #endregion

                    #region Caricamento ed assegnazione References

                    if (string.IsNullOrEmpty(referencesAssemblies))
                    {
                        // ad una DLL da referenziare che sia preceduta da "(local)", sarà aggiunto il prefisso con il path completo dell'applicazione
                        // per effettuare la ricerca della DLL nella cartella corrente (in caso opposto, si presuppone che le DLL siano in GAC)
                        referencesAssemblies = "System.dll,(local)SharpSnmpLib.dll";
                    }

                    foreach (string refAssembly in referencesAssemblies.Split(','))
                    {
                        if (!string.IsNullOrEmpty(refAssembly))
                        {
                            if (refAssembly.Trim().StartsWith("(local)", StringComparison.OrdinalIgnoreCase))
                            {
                                parameters.ReferencedAssemblies.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, refAssembly.Trim().Replace("(local)", string.Empty)));
                            }
                            else
                            {
                                parameters.ReferencedAssemblies.Add(refAssembly.Trim());
                            }
                        }
                    }

                    #endregion

                    using (CSharpCodeProvider provider = new CSharpCodeProvider())
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            #region Compilazione dinamica codice

                            CompilerResults compilerResults;

                            try
                            {
                                compilerResults = provider.CompileAssemblyFromSource(parameters, code);
                            }
                            catch (NotImplementedException ex)
                            {
                                throw new DeviceConfigurationHelperException("Error compiling code for SNMP table evaluation", ex);
                            }
                            catch (IOException ex)
                            {
                                throw new DeviceConfigurationHelperException("Error compiling code for SNMP table evaluation", ex);
                            }

                            if ((compilerResults != null) && (compilerResults.Errors != null) && (compilerResults.Errors.Count > 0))
                            {
                                StringBuilder compileErrors = new StringBuilder();

                                foreach (CompilerError compileError in compilerResults.Errors)
                                {
                                    compileErrors.AppendLine(compileError.ErrorText);
                                }

                                throw new DeviceConfigurationHelperException("Error compiling code for SNMP table evaluation\r\n" + compileErrors);
                            }

                            #endregion

                            #region Caricamento dinamico Assembly appena generato

                            if (compilerResults != null)
                            {
                                try
                                {
                                    loAssembly = compilerResults.CompiledAssembly;
                                    loObject = loAssembly.CreateInstance(className);
                                }
                                catch (ArgumentException ex)
                                {
                                    throw new DeviceConfigurationHelperException(
                                        string.Format(CultureInfo.InvariantCulture,
                                                      "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                                }
                                catch (MissingMethodException ex)
                                {
                                    throw new DeviceConfigurationHelperException(
                                        string.Format(CultureInfo.InvariantCulture,
                                                      "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                                }
                                catch (FileNotFoundException ex)
                                {
                                    throw new DeviceConfigurationHelperException(
                                        string.Format(CultureInfo.InvariantCulture,
                                                      "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                                }
                                catch (FileLoadException ex)
                                {
                                    throw new DeviceConfigurationHelperException(
                                        string.Format(CultureInfo.InvariantCulture,
                                                      "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                                }
                                catch (BadImageFormatException ex)
                                {
                                    throw new DeviceConfigurationHelperException(
                                        string.Format(CultureInfo.InvariantCulture,
                                                      "Error instancing code for SNMP table evaluation\r\n{0}", code), ex);
                                }
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
            }

            if (loObject == null)
            {
                throw new DeviceConfigurationHelperException("Error instancing code for evaluation formula for SNMP table evaluation");
            }

            object[] loCodeParms = new object[2];
            loCodeParms[0] = table;
            loCodeParms[1] = tableRow;

            try
            {
                return loObject.GetType().InvokeMember(methodName, BindingFlags.InvokeMethod, null, loObject, loCodeParms, CultureInfo.InvariantCulture) as String;
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (MethodAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (MissingFieldException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (MissingMethodException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (TargetException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (TargetInvocationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (AmbiguousMatchException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Error executing code inside evaluation formula for SNMP table\r\n{0}", code), ex);
            }
        }

        #endregion
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    [Serializable]
    public class DeviceConfigurationHelperException : Exception
    {
        public DeviceConfigurationHelperException()
        {
        }

        public DeviceConfigurationHelperException(string message) : base(message)
        {
        }

        public DeviceConfigurationHelperException(string message, Exception inner) : base(message, inner)
        {
        }

        protected DeviceConfigurationHelperException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿using System.Collections.ObjectModel;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Indica le regole di caricamento dinamiche per produttore
    /// </summary>
    public class LoadingRuleExactMatch
    {
        /// <summary>
        ///   Oid da testare per valutare se la periferica è di una categoria
        /// </summary>
        public ObjectIdentifier ProbingCategoryOid { get; private set; }

        /// <summary>
        ///   Oid da testare per valutare se la periferica è di un produttore
        /// </summary>
        public ObjectIdentifier ProbingManufacturerOid { get; private set; }

        /// <summary>
        ///   Lista di associazioni tra un produttore ed un file di definizione
        /// </summary>
        public ReadOnlyCollection<ManufacturerDefinition> ManufacturerDefinitions { get; private set; }

        /// <summary>
        ///   Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "probingCategoryOid">Oid da testare per valutare se la periferica è di una categoria</param>
        /// <param name = "probingManufacturerOid">Oid da testare per valutare se la periferica è di un produttore</param>
        /// <param name = "manufacturerDefinitions">Lista di associazioni tra un produttore ed un file di definizione</param>
        /// <param name = "noDynamicRuleMatchDeviceStatus">Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico</param>
        public LoadingRuleExactMatch(ObjectIdentifier probingCategoryOid, ObjectIdentifier probingManufacturerOid,
                                     ReadOnlyCollection<ManufacturerDefinition> manufacturerDefinitions, NoDynamicRuleMatchDeviceStatus noDynamicRuleMatchDeviceStatus)
        {
            this.ProbingCategoryOid = probingCategoryOid;
            this.ProbingManufacturerOid = probingManufacturerOid;
            this.ManufacturerDefinitions = manufacturerDefinitions;
            this.NoMatchDeviceStatus = noDynamicRuleMatchDeviceStatus;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;
using Lextm.SharpSnmpLib.Messaging;
using TimeoutException = Lextm.SharpSnmpLib.Messaging.TimeoutException;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    public static class SnmpDataRetriever
    {
        /// <summary>
        ///   Ritorna una serie di risultati data una query SNMP
        /// </summary>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>Lista di variabili SNMP contenenti i valori di ritorno</returns>
        public static IList<Variable> Get(VersionCode version, IPEndPoint endpoint, OctetString community, IList<Variable> variables)
        {
            if ((endpoint != null) && (variables != null))
            {
                DateTime startRequest = DateTime.Now;

                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    foreach (Variable variable in variables)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                     "Thread: {0} - Start request scalar SNMP, IP: {1}, community: {2}, Oid: {3}",
                                                                                     Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id));
                    }
                }

                IList<Variable> data = null;

                ushort retryCounter = 1;

                while (retryCounter <= Settings.Default.SnmpMaxRetriesCount)
                {
                    try
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            foreach (Variable variable in variables)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Request scalar SNMP, IP: {1}, community: {2}, Oid: {3}, Attempt: {4}, Timeout: {5}",
                                                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id,
                                                                                             retryCounter, retryCounter*Settings.Default.SnmpTimeoutMilliseconds));
                            }
                        }

                        data = Messenger.Get(version, endpoint, community, variables, retryCounter*Settings.Default.SnmpTimeoutMilliseconds);
                        break;
                    }
                    catch (TimeoutException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            foreach (Variable variable in variables)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Exception timeout request scalar SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Timeout: {5}, Details: {6}",
                                                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id,
                                                                                             ex.Message, ex.Timeout, ex.Details));
                            }
                        }

                        Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenSnmpRetries);
                        retryCounter++;
                    }
                    catch (SnmpException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            foreach (Variable variable in variables)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Exception request scalar SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Details: {5}",
                                                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id,
                                                                                             ex.Message, ex.Details));
                            }
                        }

                        break;
                    }
                    catch (SocketException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            foreach (Variable variable in variables)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Exception socket request scalar SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Error code: {5}",
                                                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id,
                                                                                             ex.Message, ex.ErrorCode));
                            }
                        }

                        break;
                    }
                    catch (ArgumentException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            foreach (Variable variable in variables)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Exception socket request scalar SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Parameter name {5}",
                                                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id,
                                                                                             ex.Message, ex.ParamName));
                            }
                        }

                        break;
                    }
                }

                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
                    foreach (Variable variable in variables)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                     "Thread: {0} - End request scalar SNMP, IP: {1}, community: {2}, Oid: {3}, Request duration: {4} (ms)",
                                                                                     Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id,
                                                                                     requestDuration.TotalMilliseconds));
                    }
                }

                return data;
            }

            return null;
        }

        /// <summary>
        ///   Ritorna un risultato tabellare data una query SNMP
        /// </summary>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "oid">ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella, non ci sono risultati)</param>
        /// <returns>Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno</returns>
        public static Variable[,] GetTable(VersionCode version, IPEndPoint endpoint, OctetString community, ObjectIdentifier oid)
        {
            if (endpoint != null)
            {
                DateTime startRequest = DateTime.Now;

                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Thread: {0} - Start request tabular SNMP, IP: {1}, community: {2}, Oid: {3}",
                                                                                 Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid));
                }

                Variable[,] data = null;

                ushort retryCounter = 1;

                while (retryCounter <= Settings.Default.SnmpMaxRetriesCount)
                {
                    try
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Request tabular SNMP, IP: {1}, community: {2}, Oid: {3}, Attempt: {4}, Timeout: {5}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, retryCounter,
                                                                                         retryCounter*Settings.Default.SnmpTableTimeoutMilliseconds));
                        }

                        data = Messenger.GetTable(version, endpoint, community, oid, retryCounter*Settings.Default.SnmpTableTimeoutMilliseconds,
                                                  Settings.Default.SnmpTableMaxRepetitions, null);
                        break;
                    }
                    catch (TimeoutException ex)
                    {
                        Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenSnmpRetries);
                        retryCounter++;

                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception timeout request tabular SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Timeout: {5}, Details: {6}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message,
                                                                                         ex.Timeout, ex.Details));
                        }
                    }
                    catch (SnmpException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception request tabular SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Details: {5}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message,
                                                                                         ex.Details));
                        }

                        break;
                    }
                    catch (SocketException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception socket request tabular SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Error code: {5}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message,
                                                                                         ex.ErrorCode));
                        }

                        break;
                    }
                    catch (ArgumentException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception socket request tabular SNMP, IP: {1}, community: {2}, Oid: {3}, Message: {4}, Parameter name {5}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message,
                                                                                         ex.ParamName));
                        }

                        break;
                    }
                }

                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Thread: {0} - End request tabular SNMP, IP: {1}, community: {2}, Oid: {3}, Request duration: {4} (ms)",
                                                                                 Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid,
                                                                                 requestDuration.TotalMilliseconds));
                }

                return data;
            }

            return null;
        }
    }
}
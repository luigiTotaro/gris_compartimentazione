﻿using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    public class LoadingRule
    {
        /// <summary>
        ///   Oid da testare per valutare se la periferica è di una categoria
        /// </summary>
        public ObjectIdentifier ProbingCategoryOid { get; private set; }

        /// <summary>
        ///   Produttore della periferica
        /// </summary>
        public string Manufacturer { get; private set; }

        /// <summary>
        ///   Nome del file con la definizione relativa
        /// </summary>
        public string DefinitionFile { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "probingCategoryOid">Oid da testare per valutare se la periferica è di una categoria</param>
        /// <param name = "manufacturer">Produttore della periferica</param>
        /// <param name = "definitionFile">Nome del file con la definizione relativa</param>
        public LoadingRule(ObjectIdentifier probingCategoryOid, string manufacturer, string definitionFile)
        {
            this.ProbingCategoryOid = probingCategoryOid;
            this.Manufacturer = manufacturer;
            this.DefinitionFile = definitionFile;
        }
    }
}
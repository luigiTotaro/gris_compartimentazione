﻿using System.Collections.Generic;
using System.Net;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    public class MessengerFactory
    {
        /// <summary>
        ///   Ritorna una serie di risultati data una query SNMP
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>Lista di variabili SNMP contenenti i valori di ritorno</returns>
        public IList<Variable> GetSnmpData(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community, IList<Variable> variables)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.Get(version, endpoint, community, variables);
                    }

                    return FakeDataRetriever.Get(version, endpoint, community, variables);
                }

                return SnmpDataRetriever.Get(version, endpoint, community, variables);
            }

            return null;
        }

        /// <summary>
        ///   Ritorna un risultato tabellare data una query SNMP
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "oid">ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella, non ci sono risultati)</param>
        /// <returns>Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno</returns>
        public Variable[,] GetSnmpTable(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community, ObjectIdentifier oid)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.GetTable(version, endpoint, community, oid);
                    }

                    return FakeDataRetriever.GetTable(version, endpoint, community, oid);
                }

                return SnmpDataRetriever.GetTable(version, endpoint, community, oid);
            }

            return null;
        }

        /// <summary>
        ///   Ottiene una risposta alla richiesta Ping ICMP
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "endpoint">Endpoint IP</param>
        /// <returns>True se l'endpoint risponde a ICMP, false altrimenti</returns>
        public bool GetPingResponse(bool fakeLocalData, IPEndPoint endpoint)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return IcmpDataRetriever.GetPingResponse(endpoint);
                    }

                    return FakeDataRetriever.GetPingResponse(endpoint);
                }

                return IcmpDataRetriever.GetPingResponse(endpoint);
            }

            return false;
        }

        /// <summary>
        ///   Ottiene lo stato SNMP della periferica
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>True se l'endpoint risponde a SNMP, false altrimenti</returns>
        public IList<Variable> GetAliveSnmp(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community, IList<Variable> variables)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.Get(version, endpoint, community, variables);
                    }

                    return FakeDataRetriever.GetAliveSnmp(version, endpoint, community, variables);
                }

                return SnmpDataRetriever.Get(version, endpoint, community, variables);
            }

            return null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using GrisSuite.SnmpSupervisorLibrary.Database;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    internal static class FakeDataRetriever
    {
        /// <summary>
        ///   Ritorna una serie di risultati data una query SNMP
        /// </summary>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>Lista di variabili SNMP contenenti i valori di ritorno</returns>
        public static IList<Variable> Get(VersionCode version, IPEndPoint endpoint, OctetString community, IList<Variable> variables)
        {
            List<Variable> returnValue = null;

            string response = LoadFakeResponseFile(endpoint);

            if (LoadSnmpVersionCode(response, version) && LoadCommunityString(response, community) && (variables.Count == 1))
            {
                Variable v = variables[0];

                if (!String.IsNullOrEmpty(response))
                {
                    string[] lines = Regex.Split(response, "\r\n");
                    Regex regexOid = new Regex(v.Id.ToString().Replace(".", "\\.") + "ç", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

                    foreach (string line in lines)
                    {
                        if (!String.IsNullOrEmpty(line))
                        {
                            if (regexOid.IsMatch(line))
                            {
                                string[] columns = Regex.Split(line, "ç");

                                if (columns.Length > 1)
                                {
                                    // Presuppone un file di testo nel seguente formato
                                    // oid,valore_di_ritorno
                                    // oppure
                                    // oid,(Tipo)valore_di_ritorno

                                    string stringValue = columns[1];

                                    if ((stringValue.StartsWith("(", StringComparison.OrdinalIgnoreCase)) && (stringValue.IndexOf(")", StringComparison.OrdinalIgnoreCase) > 0) &&
                                        (!stringValue.EndsWith(")", StringComparison.OrdinalIgnoreCase)))
                                    {
                                        string valueString = null;
                                        string typeString = null;

                                        try
                                        {
                                            typeString = stringValue.Substring(stringValue.IndexOf("(", StringComparison.OrdinalIgnoreCase) + 1,
                                                                               stringValue.IndexOf(")", StringComparison.OrdinalIgnoreCase) - 1);
                                            valueString = stringValue.Substring(stringValue.IndexOf(")", StringComparison.OrdinalIgnoreCase) + 1).Trim();
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        catch (ArgumentException)
                                        {
                                        }
                                        catch (IndexOutOfRangeException)
                                        {
                                        }

                                        if ((!string.IsNullOrEmpty(valueString)) && (!string.IsNullOrEmpty(typeString)))
                                        {
                                            switch (typeString)
                                            {
                                                case "Integer32":
                                                    returnValue = new List<Variable> {new Variable(v.Id, new Integer32(int.Parse(valueString, CultureInfo.InvariantCulture)))};
                                                    break;
                                                case "OctetString":
                                                    // la codifica 00-11-12-13-14-15 è considerata un MAC Address codificato nel file fake,
                                                    // quindi è ricostruita per come viaggia su filo, in 6 byte separati
                                                    // alcuni produttori hanno inviato il MAC come stringa ASCII, quindi 17 bytes
                                                    // ovviamente questo genera problemi nella costruzione di un file di simulazione, perché in un caso
                                                    // occorre inviare i byte corretti, nell'altro una stringa con la rappresentazione ASCII.
                                                    // Non c'è modo di discriminare via file fake l'una o l'altra, quindi considerare i test con attenzione
                                                    if (Regex.IsMatch(valueString,
                                                                      "(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])"))
                                                    {
                                                        // E' un MAC address
                                                        string[] bytes = valueString.Split('-');
                                                        byte[] mac = new byte[bytes.Length];

                                                        byte[] addressBytes = PhysicalAddress.Parse(valueString).GetAddressBytes();

                                                        for (int byteCounter = 0; byteCounter < addressBytes.Length; byteCounter++)
                                                        {
                                                            mac[byteCounter] = addressBytes[byteCounter];
                                                        }
                                                        returnValue = new List<Variable>
                                                                      {new Variable(v.Id, new OctetString(Encoding.GetEncoding(1252).GetString(mac), Encoding.GetEncoding(1252)))};
                                                    }
                                                    else
                                                    {
                                                        returnValue = new List<Variable> {new Variable(v.Id, new OctetString(valueString))};
                                                    }
                                                    break;
                                                case "Counter32":
                                                    returnValue = new List<Variable> {new Variable(v.Id, new Counter32(uint.Parse(valueString, CultureInfo.InvariantCulture)))};
                                                    break;
                                                case "Gauge32":
                                                    returnValue = new List<Variable> {new Variable(v.Id, new Gauge32(uint.Parse(valueString, CultureInfo.InvariantCulture)))};
                                                    break;
                                                case "IPAddress":
                                                    returnValue = new List<Variable> {new Variable(v.Id, new IP(IPAddress.Parse(valueString)))};
                                                    break;
                                                case "TimeTicks":
                                                    returnValue = new List<Variable> {new Variable(v.Id, new TimeTicks(uint.Parse(valueString, CultureInfo.InvariantCulture)))};
                                                    break;
                                                case "ObjectIdentifier":
                                                    returnValue = new List<Variable> { new Variable(v.Id, new ObjectIdentifier(valueString)) };
                                                    break;
                                                default:
                                                    returnValue = new List<Variable> {new Variable(v.Id, new OctetString(stringValue))};
                                                    break;
                                            }
                                        }
                                        break;
                                    }

                                    if (!string.IsNullOrEmpty(stringValue))
                                    {
                                        returnValue = new List<Variable>
                                                      {
                                                          new Variable(v.Id,
                                                                       new OctetString(
                                                                           stringValue.Replace("(Integer32)", string.Empty).Replace("(OctetString)", string.Empty).Replace(
                                                                               "(Counter32)", string.Empty).Replace("(Gauge32)", string.Empty).Replace("(IPAddress)", string.Empty).
                                                                               Replace("(TimeTicks)", string.Empty)))
                                                      };
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        ///   Verifica la correttezza della community string
        /// </summary>
        /// <param name = "response">Configurazione completa fittizia</param>
        /// <param name = "community">Community string richiesta</param>
        /// <returns>True nel caso che le community coincidano, false altrimenti</returns>
        private static bool LoadCommunityString(string response, OctetString community)
        {
            if (String.IsNullOrEmpty(response))
            {
                return true;
            }

            Regex communityRegex = new Regex("çreadCommunity=\\\"(?<community>\\w+?)\\\"", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            Match communityMatch = communityRegex.Match(response);

            string configuredCommunity = "public";

            if ((communityMatch.Groups["community"] != null) && (!String.IsNullOrEmpty(communityMatch.Groups["community"].Value)))
            {
                configuredCommunity = communityMatch.Groups["community"].Value;
            }

            if (community.ToString().Equals(configuredCommunity))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///   Verifica la correttezza della versione Snmp
        /// </summary>
        /// <param name = "response">Configurazione completa fittizia</param>
        /// <param name = "versionCode">Versione Snmp richiesta</param>
        /// <returns>True nel caso che le versioni coincidano, false altrimenti</returns>
        private static bool LoadSnmpVersionCode(string response, VersionCode versionCode)
        {
            if (String.IsNullOrEmpty(response))
            {
                return true;
            }

            Regex versionCodeRegex = new Regex("çversionCode=\\\"(?<versioncode>\\w+?)\\\"", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            Match versionCodeMatch = versionCodeRegex.Match(response);

            // Se non indicata la versione di default per le periferiche è 2
            string configuredVersionCode = "V2";

            if ((versionCodeMatch.Groups["versioncode"] != null) && (!String.IsNullOrEmpty(versionCodeMatch.Groups["versioncode"].Value)))
            {
                configuredVersionCode = versionCodeMatch.Groups["versioncode"].Value;
            }

            if (versionCode.ToString().Equals(configuredVersionCode, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///   Ritorna un risultato tabellare data una query SNMP
        /// </summary>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "oid">ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella, non ci sono risultati)</param>
        /// <returns>Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno</returns>
        public static Variable[,] GetTable(VersionCode version, IPEndPoint endpoint, OctetString community, ObjectIdentifier oid)
        {
            List<List<Variable>> dataRows = new List<List<Variable>>();
            int columnCount = -1;

            string response = LoadFakeResponseFile(endpoint);

            if (LoadSnmpVersionCode(response, version) && LoadCommunityString(response, community) && (!String.IsNullOrEmpty(response)))
            {
                string[] lines = Regex.Split(response, "\r\n");
                // cerchiamo solo le righe che iniziano per quell'OID
                // altrimenti, una tabella che contenga un OID tra i valori, potrebbe risultare come match
                Regex regexOid = new Regex(oid.ToString().Replace(".", "\\.") + "ç", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

                foreach (string line in lines)
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        if (regexOid.IsMatch(line))
                        {
                            string[] columns = Regex.Split(line, "ç");

                            if (columns.Length == 2)
                            {
                                // Presuppone un file di testo nel seguente formato
                                // oid,valori_di_ritorno

                                string values = columns[1];

                                if (!String.IsNullOrEmpty(values))
                                {
                                    string[] valuesData = values.Split('§');

                                    List<Variable> dataCols = new List<Variable>();
                                    foreach (string stringValue in valuesData)
                                    {
                                        // stringValue = (.1.3.6.1.2.1.25.2.3.1.1.1@Integer32)1

                                        string oidAndType = null;
                                        string oidString = null;
                                        string typeString = null;
                                        string value = null;

                                        try
                                        {
                                            oidAndType = stringValue.Substring(stringValue.IndexOf("(", StringComparison.OrdinalIgnoreCase) + 1,
                                                                               stringValue.IndexOf(")", StringComparison.OrdinalIgnoreCase) - 1);
                                            oidString = oidAndType.Trim().Split('@')[0].Trim();
                                            typeString = oidAndType.Trim().Split('@')[1].Trim();
                                            value = stringValue.Substring(stringValue.IndexOf(")", StringComparison.OrdinalIgnoreCase) + 1);
                                        }
                                        catch (ArgumentOutOfRangeException)
                                        {
                                        }
                                        catch (ArgumentException)
                                        {
                                        }
                                        catch (IndexOutOfRangeException)
                                        {
                                        }

                                        if ((oidAndType != null) && (oidString != null) && (typeString != null) && (value != null))
                                        {
                                            ObjectIdentifier newOid = SnmpUtility.TryParseOid(oidString);

                                            if (newOid != null)
                                            {
                                                ISnmpData newData;

                                                try
                                                {
                                                    switch (typeString)
                                                    {
                                                        // simulazione incompleta
                                                        case "ObjectIdentifier":
                                                            newData = new ObjectIdentifier(value);
                                                            break;
                                                        case "Integer32":
                                                            newData = new Integer32(int.Parse(value, CultureInfo.InvariantCulture));
                                                            break;
                                                        case "OctetString":
                                                            // la codifica 00-11-12-13-14-15 è considerata un MAC Address codificato nel file fake,
                                                            // quindi è ricostruita per come viaggia su filo, in 6 byte separati
                                                            // alcuni produttori hanno inviato il MAC come stringa ASCII, quindi 17 bytes
                                                            // ovviamente questo genera problemi nella costruzione di un file di simulazione, perché in un caso
                                                            // occorre inviare i byte corretti, nell'altro una stringa con la rappresentazione ASCII.
                                                            // Non c'è modo di discriminare via file fake l'una o l'altra, quindi considerare i test con attenzione
                                                            if (Regex.IsMatch(value,
                                                                              "(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])-(?:\\d|[A-Z])(?:\\d|[A-Z])"))
                                                            {
                                                                // E' un MAC address
                                                                string[] bytes = value.Split('-');
                                                                byte[] mac = new byte[bytes.Length];

                                                                byte[] addressBytes = PhysicalAddress.Parse(value).GetAddressBytes();

                                                                for (int byteCounter = 0; byteCounter < addressBytes.Length; byteCounter++)
                                                                {
                                                                    mac[byteCounter] = addressBytes[byteCounter];
                                                                }
                                                                newData = new OctetString(Encoding.GetEncoding(1252).GetString(mac), Encoding.GetEncoding(1252));
                                                            }
                                                            else
                                                            {
                                                                newData = new OctetString(value);
                                                            }
                                                            break;
                                                        case "Counter32":
                                                            newData = new Counter32(uint.Parse(value, CultureInfo.InvariantCulture));
                                                            break;
                                                        case "Gauge32":
                                                            newData = new Gauge32(uint.Parse(value, CultureInfo.InvariantCulture));
                                                            break;
                                                        case "IPAddress":
                                                            newData = new IP(IPAddress.Parse(value));
                                                            break;
                                                        case "TimeTicks":
                                                            newData = new TimeTicks(uint.Parse(value, CultureInfo.InvariantCulture));
                                                            break;
                                                        default:
                                                            // Tipo dati non gestito
                                                            newData = null;
                                                            break;
                                                    }
                                                }
                                                catch (Exception)
                                                {
                                                    newData = null;
                                                }

                                                if (newData != null)
                                                {
                                                    dataCols.Add(new Variable(newOid, newData));
                                                }
                                            }
                                        }
                                    }

                                    if (columnCount == -1)
                                    {
                                        columnCount = dataCols.Count;
                                    }

                                    if ((columnCount >= 0) && (columnCount == dataCols.Count))
                                    {
                                        dataRows.Add(dataCols);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Variable[,] data = null;

            if ((dataRows.Count > 0) && (columnCount > 0))
            {
                data = new Variable[dataRows.Count,columnCount];

                for (int rowCounter = 0; rowCounter < dataRows.Count; rowCounter++)
                {
                    for (int colCounter = 0; colCounter < columnCount; colCounter++)
                    {
                        data[rowCounter, colCounter] = dataRows[rowCounter][colCounter];
                    }
                }
            }

            return data;
        }

        /// <summary>
        ///   Ritorna una serie di risultati data una query SNMP
        /// </summary>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>Lista di variabili SNMP contenenti i valori di ritorno</returns>
        public static IList<Variable> GetAliveSnmp(VersionCode version, IPEndPoint endpoint, OctetString community, IList<Variable> variables)
        {
            List<Variable> returnValue = null;

            string response = LoadFakeResponseFile(endpoint);
            bool responseLineFound = false;

            if (LoadSnmpVersionCode(response, version) && LoadCommunityString(response, community) && (variables.Count == 1))
            {
                Variable v = variables[0];

                if (!String.IsNullOrEmpty(response))
                {
                    string[] lines = Regex.Split(response, "\r\n");
                    Regex regexSnmp = new Regex("snmp", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

                    foreach (string line in lines)
                    {
                        if (!String.IsNullOrEmpty(line))
                        {
                            if (regexSnmp.IsMatch(line))
                            {
                                string[] columns = Regex.Split(line, "ç");

                                if (columns.Length > 1)
                                {
                                    // Presuppone un file di testo nel seguente formato
                                    // snmpçvalore_di_ritorno

                                    string value = columns[1];

                                    if (!String.IsNullOrEmpty(value))
                                    {
                                        if (value.Equals("true", StringComparison.OrdinalIgnoreCase))
                                        {
                                            returnValue = new List<Variable> {new Variable(v.Id, new OctetString("FakeName"))};
                                        }

                                        responseLineFound = true;

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!responseLineFound)
                {
                    // se non è presente la riga del snmp nel file di risposta simulata, il default è una risposta valida
                    returnValue = new List<Variable> {new Variable(v.Id, new OctetString("FakeName"))};
                }
            }

            return returnValue;
        }

        private static string LoadFakeResponseFile(IPEndPoint ipEndPoint)
        {
            string fakeResponseFileData = string.Empty;
            string fileName = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FakeResponses\\"),
                                           string.Format(CultureInfo.InvariantCulture, "{0}.txt", DataUtility.EncodeIPAddress(ipEndPoint.Address)));

            if (FileUtility.CheckFileCanRead(fileName))
            {
                fakeResponseFileData = FileUtility.ReadFile(fileName, Encoding.GetEncoding(1252));
            }

            return (fakeResponseFileData ?? string.Empty);
        }

        /// <summary>
        ///   Ottiene una risposta alla richiesta Ping ICMP
        /// </summary>
        /// <param name = "ipEndPoint">Endpoint IP</param>
        /// <returns>True se l'endpoint risponde a ICMP, false altrimenti</returns>
        public static bool GetPingResponse(IPEndPoint ipEndPoint)
        {
            // se non è presente la riga del ping nel file di risposta simulata, il default è false
            bool returnValue = false;
            string response = LoadFakeResponseFile(ipEndPoint);

            if (!String.IsNullOrEmpty(response))
            {
                string[] lines = Regex.Split(response, "\r\n");
                Regex regexPing = new Regex("ping", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

                foreach (string line in lines)
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        if (regexPing.IsMatch(line))
                        {
                            string[] columns = Regex.Split(line, "ç");

                            if (columns.Length > 1)
                            {
                                // Presuppone un file di testo nel seguente formato
                                // pingçvalore_di_ritorno

                                string value = columns[1];

                                if (!String.IsNullOrEmpty(value))
                                {
                                    returnValue = (value.Equals("true", StringComparison.OrdinalIgnoreCase) ? true : false);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return returnValue;
        }
    }
}
﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    public static class IcmpDataRetriever
    {
        /// <summary>
        ///   Ottiene una risposta alla richiesta Ping ICMP
        /// </summary>
        /// <param name = "endpoint">Endpoint IP</param>
        /// <returns>True se l'endpoint risponde a ICMP, false altrimenti</returns>
        public static bool GetPingResponse(IPEndPoint endpoint)
        {
            bool returnValue = false;

            if (endpoint != null)
            {
                DateTime startRequest = DateTime.Now;

                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                   string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Start request ICMP, IP: {1}",
                                                                                 Thread.CurrentThread.GetHashCode(), endpoint.Address));
                }

                PingReply reply = null;

                ushort retryCounter = 1;

                while (retryCounter <= Settings.Default.IcmpMaxRetriesCount)
                {
                    try
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Request ICMP, IP: {1}, Attempt: {2}, Timeout: {3}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, retryCounter,
                                                                                         retryCounter*Settings.Default.IcmpTimeoutMilliseconds));
                        }

                        reply = Ping.Send(endpoint.Address, retryCounter*Settings.Default.IcmpTimeoutMilliseconds);

                        if ((reply != null) && (reply.Status == IPStatus.TimedOut))
                        {
                            if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Thread: {0} - Request timeout ICMP, IP: {1}, RoundtripTime: {2}",
                                                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address, reply.RoundtripTime));
                            }

                            Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenIcmpRetries);
                            retryCounter++;

                            continue;
                        }

                        break;
                    }
                    catch (ArgumentException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Socket exception request ICMP, IP: {1}, Message: {2}, Parameter name: {3}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, ex.Message, ex.ParamName));
                        }

                        break;
                    }
                    catch (PingException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception ping request ICMP, IP: {1}, Message: {2}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, ex.Message));
                        }

                        break;
                    }
                    catch (ObjectDisposedException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception object not available request ICMP, IP: {1}, Message: {2}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, ex.Message));
                        }

                        break;
                    }
                    catch (InvalidOperationException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception invalid operation request ICMP, IP: {1}, Message: {2}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, ex.Message));
                        }

                        break;
                    }
                    catch (NotSupportedException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception not supported operation request ICMP, IP: {1}, Message: {2}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, ex.Message));
                        }

                        break;
                    }
                    catch (SocketException ex)
                    {
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Thread: {0} - Exception socket request ICMP, IP: {1}, Message: {2}, Error code: {3}",
                                                                                         Thread.CurrentThread.GetHashCode(), endpoint.Address, ex.Message, ex.ErrorCode));
                        }

                        break;
                    }
                }

                if ((reply != null) && (reply.Status == IPStatus.Success))
                {
                    returnValue = true;
                }

                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Thread: {0} - End request ICMP, IP: {1}, Request duration: {2} (ms)",
                                                                                 Thread.CurrentThread.GetHashCode(), endpoint.Address, requestDuration.TotalMilliseconds));
                }
            }

            return returnValue;
        }
    }
}
﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("GrisSuite.SNMPSupervisorLibrary")]
[assembly: AssemblyDescription("Collect data from devices")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Telefin S.p.A.")]
[assembly: AssemblyProduct("GrisSuite.SNMPSupervisorLibrary")]
[assembly: AssemblyCopyright("Copyright © Telefin S.p.A. 2008-2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(true)]
[assembly:
    InternalsVisibleTo(
        "GrisSuite.SnmpSupervisor.msTest.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100dbd9dfaff24bc28236d974698825915eabab80d07e85f77b74c2dbe0f47f6ed364b9b74b47fa10dc957022bf2c4bf3da72fc711df80a9f4239d8ad7063b6ed2f09e0cbdbdbe8b8e732779b6b7afa4310148040f7a80b4afe5b7b7e0e78dee26be6f92b65bffa7ef38eedd74443c8e872af7529dd2020b8f8e9771624c811b9bf"
        )]
[assembly:
    InternalsVisibleTo(
        "GrisSuite.SnmpSupervisor.nUnit.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100dbd9dfaff24bc28236d974698825915eabab80d07e85f77b74c2dbe0f47f6ed364b9b74b47fa10dc957022bf2c4bf3da72fc711df80a9f4239d8ad7063b6ed2f09e0cbdbdbe8b8e732779b6b7afa4310148040f7a80b4afe5b7b7e0e78dee26be6f92b65bffa7ef38eedd74443c8e872af7529dd2020b8f8e9771624c811b9bf"
        )]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("6da1f534-1566-4f31-b5aa-bcbfd99f934c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("2.0.8.0")]
[assembly: AssemblyFileVersion("2.0.8.0")]
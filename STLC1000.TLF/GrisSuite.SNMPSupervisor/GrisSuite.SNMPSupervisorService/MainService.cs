﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using GrisSuite.SnmpSupervisorService.Properties;
using Timer = System.Timers.Timer;

namespace GrisSuite.SnmpSupervisorService
{
    public partial class MainService : ServiceBase
    {
        private const string SYSTEM_XML_FILE_NAME = "System.xml";

        #region Campi Privati

        private static SqlConnection dbConnection;
        private static SqlConnection dbEventsConnection;
        private static ReadOnlyCollection<DeviceObject> devices;
        private static Timer pollingTimer;
        private static Timer startupTimer;
        private static bool processingInProgress;
        private static DatabaseFactory persistenceFactory;
        private static string configurationSystemFile;

        private enum DeviceWriteMode
        {
            Insert,
            Update
        } ;

        #endregion

        #region Costruttore

        public MainService()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Start del servizio

        protected override void OnStart(string[] args)
        {
            configurationSystemFile = string.Empty;

            if (string.IsNullOrEmpty(Settings.Default.SystemXmlFolder) || (Settings.Default.SystemXmlFolder.Trim().Length == 0) || (Settings.Default.SystemXmlFolder.Equals(".")))
            {
                configurationSystemFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_XML_FILE_NAME);
            }
            else
            {
                try
                {
                    configurationSystemFile = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Settings.Default.SystemXmlFolder.Trim()), SYSTEM_XML_FILE_NAME);
                }
                catch (ArgumentException)
                {
                }
            }

            // non inzializzando persistOnDatabase, lasciamo che si usi la configurazione da file
            InitializeStart(configurationSystemFile, null);
        }

        #endregion

        #region Inizializzazione, verifica configurazione ed avvio monitoraggio

        public static Exception InitializeStart(string configurationSystemFilename, bool? persistOnDatabase)
        {
            configurationSystemFile = configurationSystemFilename;

            ConnectionStringSettings localDatabaseConnectionString = null;
            ConnectionStringSettings localDatabaseEventsConnectionString = null;
            Exception lastException = null;
            persistenceFactory = new DatabaseFactory(persistOnDatabase);

            if (string.IsNullOrEmpty(configurationSystemFile))
            {
                lastException = new ConfigurationSystemException("File name with device list is mandatory");
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, lastException.Message);
                return lastException;
            }

            if (!FileUtility.CheckFileCanRead(configurationSystemFile))
            {
                lastException =
                    new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Unable to load file with device list: {0}",
                                                                   configurationSystemFile));
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, lastException.Message);
                return lastException;
            }

            try
            {
                localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
                localDatabaseEventsConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabaseEvents"];
            }
            catch (ConfigurationErrorsException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, "Invalid configuration. " + ex.Message);
                lastException = ex;
            }

            if (localDatabaseConnectionString == null)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Database connection string is not valid.");
            }
            else
            {
                try
                {
                    dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Database connection string is not valid. " + ex.Message);
                    lastException = ex;
                }
            }

            if (localDatabaseEventsConnectionString == null)
            {
                if (dbConnection != null)
                {
                    dbEventsConnection = dbConnection;
                }
            }
            else
            {
                try
                {
                    dbEventsConnection = new SqlConnection(localDatabaseEventsConnectionString.ConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Database connection string for events data is not valid. " + ex.Message);
                    lastException = ex;
                }
            }

            if ((lastException == null) && (dbConnection != null) && (dbEventsConnection != null))
            {
                if (Settings.Default.StartupDelayMinutes > 0)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                                   string.Format(CultureInfo.InvariantCulture, "Service started with {0} minutes delay in first data polling.",
                                                                                 Settings.Default.StartupDelayMinutes));

                    startupTimer = new Timer {Interval = (Settings.Default.StartupDelayMinutes*60*1000)};
                    startupTimer.Elapsed += OnStartupTimerElapsed;

                    startupTimer.Start();
                }
                else
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format(CultureInfo.InvariantCulture, "Service started."));

                    CleanupAndCreateDevices();
                }

#if (DEBUG)
                // In fase di debug lasciamo vari secondi per il completamento dei thread, da aumentare all'occorrenza
                Thread.Sleep(Settings.Default.StartupDelayMinutes*60*1000);
                Thread.Sleep(((int) Settings.Default.PollingIntervalSeconds - 1)*1000);
#endif
            }
            else
            {
                lastException = new ConfigurationErrorsException("Connection strings to database (data or events) are not available.");
            }

            return lastException;
        }

        #endregion

        #region Gestione eventi tick del timer di avvio

        private static void OnStartupTimerElapsed(object source, ElapsedEventArgs e)
        {
            if (startupTimer != null)
            {
                startupTimer.Stop();
                startupTimer.Dispose();
            }

            CleanupAndCreateDevices();
        }

        #endregion

        #region CleanupAndCreateDevices - Carica le periferiche da configurazione, ripulisce la base dati e ricrea le periferiche

        private static void CleanupAndCreateDevices()
        {
            devices = SystemXmlHelper.GetMonitorDeviceList(configurationSystemFile);

            bool isMonitoring = false;

            if ((devices != null) && (devices.Count > 0))
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                               string.Format(CultureInfo.InvariantCulture,
                                                                             "Loaded {0} devices to monitor from configuration file. Polling is set to {1} seconds.",
                                                                             devices.Count, Settings.Default.PollingIntervalSeconds));

                foreach (DeviceObject deviceObject in devices)
                {
                    if (deviceObject.Active == 1)
                    {
                        deviceObject.MonitoringEnabled = false;

                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose, deviceObject.ToString());

                        bool? deviceExistence = persistenceFactory.CheckDeviceExistence(dbConnection.ConnectionString, deviceObject.DeviceId);

                        if (deviceExistence.HasValue)
                        {
                            if (deviceExistence.Value)
                            {
                                // la periferica esiste in base dati
                                if (Settings.Default.ShouldDeleteExistingDevicesAtStartup)
                                {
                                    // la periferica esiste e deve essere cancellata
                                    if (DeleteDevice(deviceObject))
                                    {
                                        // la periferica è stata correttamente cancellata, possiamo reinserirla
                                        deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert);

                                        if (deviceObject.MonitoringEnabled)
                                        {
                                            isMonitoring = true;
                                        }
                                    }
                                    else
                                    {
                                        // non è stato possibile cancellare la periferica esistente
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                                     "Device is already stored on database and it's requested to be removed at service start, but delete operation failed. Any attempt to further interact with this device will be ignored. {0}",
                                                                                                     deviceObject));

                                        deviceObject.MonitoringEnabled = false;
                                    }
                                }
                                else
                                {
                                    // aggiorna la periferica esistente
                                    deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Update);

                                    if (deviceObject.MonitoringEnabled)
                                    {
                                        isMonitoring = true;
                                    }
                                }
                            }
                            else
                            {
                                // la periferica non esiste in base dati e dovrebbe essere creata
                                if (Settings.Default.ShouldCreateNewDevices)
                                {
                                    deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert);

                                    if (deviceObject.MonitoringEnabled)
                                    {
                                        isMonitoring = true;
                                    }
                                }
                                else
                                {
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                                 "Device not stored on database, but devices creation is disabled by configuration (to enable it, change configuration flag 'ShouldCreateNewDevices'). Streams and Stream Fields will not be stored on database. {0}",
                                                                                                 deviceObject));
                                }
                            }
                        }
                    }
                    else
                    {
                        // la periferica è indicata come non attiva
                        deviceObject.MonitoringEnabled = false;

                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                     "Device loaded from configuration file, but declared as not active. Any further interaction with this device will be ignored. {0}",
                                                                                     deviceObject));
                    }
                }

                pollingTimer = new Timer {Interval = (Settings.Default.PollingIntervalSeconds*1000)};
                pollingTimer.Elapsed += OnPollingTimerElapsed;

                if (isMonitoring)
                {
#if (!DEBUG)
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Polling started.");

                    pollingTimer.Start();
#else
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Polling started.");

                    if ((devices != null) && (devices.Count > 0)) {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                                                                       "Polling data just once (debug mode).");
                        PollDevices();
                    }
#endif
                }
                else
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                                                                   "All configured devices were in error during updating / inserting. No polling is going on. Please check device problems and restart service.");
                }
            }
            else
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning, "There are no available devices in configuration file.");
            }
        }

        #endregion

        #region Gestione eventi tick del timer

        private static void OnPollingTimerElapsed(object source, ElapsedEventArgs e)
        {
            try
            {
                PollDevices();
            }
            catch (Exception ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                               string.Format(CultureInfo.InvariantCulture, "{0}\r\n{1}\r\n{2}", ex.Message, ex.StackTrace, ex.InnerException));
            }
        }

        #endregion

        #region Elaborazione eventi tick del timer e accodamento dei thread

        private static void PollDevices()
        {
            // Evita le chiamate nidificate
            if (processingInProgress)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                                                               string.Format(CultureInfo.InvariantCulture,
                                                                             "Device polling and evaluation loop skipped. Previous operation is still in progress. Consider raising value in configuration parameter 'PollingIntervalSeconds'."));
                return;
            }

            processingInProgress = true;

            if (FileUtility.LogLevel >= LoggingLevel.Verbose)
            {
                FileUtility.RecycleLogFile();
            }

            if ((dbConnection != null) && (dbEventsConnection != null))
            {
                if ((devices != null) && (devices.Count > 0))
                {
                    if (FileUtility.LogLevel >= LoggingLevel.Verbose)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
                                                                       string.Format(CultureInfo.InvariantCulture, "Started polling {0} devices.", devices.Count));
                    }

                    using (ThreadPoolWait tpw = new ThreadPoolWait())
                    {
                        foreach (DeviceObject device in devices)
                        {
                            if (device.MonitoringEnabled)
                            {
                                device.LastEvent = null;

                                tpw.QueueUserWorkItem(ProcessMonitorDevice, device);
                            }
                        }

                        // aspettiamo che tutti i thread abbiano completato la loro esecuzione
                        tpw.WaitOne();
                    }

                    if (FileUtility.LogLevel >= LoggingLevel.Verbose)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
                                                                       string.Format(CultureInfo.InvariantCulture, "Finished polling {0} devices.", devices.Count));
                    }
                }
            }

            processingInProgress = false;
        }

        #endregion

        #region Creazione istanze periferiche, recupero dati ed inserimento in base dati - Metodo lanciato dai thread

        private static void ProcessMonitorDevice(object state)
        {
            DeviceObject deviceObject = state as DeviceObject;

            if (deviceObject != null)
            {
                if ((String.IsNullOrEmpty(deviceObject.Name)) || (String.IsNullOrEmpty(deviceObject.DeviceType)) || (deviceObject.EndPoint == null) ||
                    (deviceObject.Address == null))
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Some mandatory information is missing from device. Device name: {0}, Device Id: {1}, IP Address: {2}, Type: {3}",
                                                                                 deviceObject.Name, deviceObject.DeviceId, deviceObject.Address, deviceObject.DeviceType));
                    return;
                }

                IDevice device;
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();

                try
                {
                    device = deviceTypeFactory.Create(deviceObject);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                                   string.Format(CultureInfo.InvariantCulture, "{0}, Exception: {1}", deviceObject, ex.Message));
                    return;
                }

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        // Errori a questo livello significano errori nella definizione XML come correttezza sintattica
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                                       string.Format(CultureInfo.InvariantCulture, "{0}, Definition: {1}\r\nException: {2}\r\n{3}", deviceObject,
                                                                                     device.DefinitionFileName, device.LastError.Message,
                                                                                     ((device.LastError.InnerException != null) &&
                                                                                      (device.LastError.InnerException.InnerException != null)
                                                                                          ? string.Format(CultureInfo.InvariantCulture, ", Internal Exception: {0}",
                                                                                                          device.LastError.InnerException.InnerException.Message)
                                                                                          : string.Empty)));
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            // Errori a questo livello indicano problemi di caricamento dati SNMP,
                            // come tabelle SNMP non corrette o incomplete
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                                           string.Format(CultureInfo.InvariantCulture, "{0}, Definition: {1}\r\nException: {2}{3}{4}", deviceObject,
                                                                                         device.DefinitionFileName, device.LastError.Message,
                                                                                         ((device.LastError.InnerException != null)
                                                                                              ? string.Format(CultureInfo.InvariantCulture, "\r\nInternal Exception: {0}",
                                                                                                              device.LastError.InnerException.Message)
                                                                                              : string.Empty),
                                                                                         ((device.LastError.InnerException != null) &&
                                                                                          (device.LastError.InnerException.InnerException != null)
                                                                                              ? string.Format(CultureInfo.InvariantCulture, "\r\nSecond Internal Exception: {0}",
                                                                                                              device.LastError.InnerException.InnerException.Message)
                                                                                              : string.Empty)));
                        }
                    }

                    // La periferica è persistita comunque in base dati, anche se sono avvenuti errori in fase
                    // di caricamento dati.
                    bool? deviceExistence = persistenceFactory.CheckDeviceExistence(dbConnection.ConnectionString, deviceObject.DeviceId);

                    if (deviceExistence.HasValue)
                    {
                        persistenceFactory.UpdateDeviceStatus(dbConnection.ConnectionString, deviceObject.DeviceId, (int) device.SeverityLevel, device.ValueDescriptionComplete,
                                                              device.Offline);
                        foreach (DBStream stream in device.StreamData)
                        {
                            persistenceFactory.UpdateStream(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, stream.Name, (int) stream.SeverityLevel,
                                                            stream.ValueDescriptionComplete);

                            foreach (DBStreamField field in stream.FieldData)
                            {
                                if (field.PersistOnDatabase)
                                {
                                    persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, field.FieldId, field.ArrayId,
                                                                         field.Name, (int) field.SeverityLevel, field.Value, field.ValueDescriptionComplete);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Aggiornamento / inserimento di una periferica in base dati

        private static bool UpdateInsertDevice(DeviceObject deviceObject, DeviceWriteMode deviceWriteMode)
        {
            bool returnValue = false;

            DeviceDatabaseSupport deviceDatabaseSupport = persistenceFactory.GetStationBuildingRackPortData(dbConnection.ConnectionString, deviceObject.Topography,
                                                                                                            deviceObject.ServerId, deviceObject.PortId);

            if (deviceDatabaseSupport != null)
            {
                if (persistenceFactory.UpdateDevice(dbConnection.ConnectionString, deviceObject.DeviceId, deviceObject.NodeId, deviceObject.ServerId, deviceObject.Name,
                                                    deviceObject.DeviceType, deviceObject.SerialNumber, deviceObject.Address, deviceDatabaseSupport.PortId, deviceObject.ProfileId,
                                                    deviceObject.Active, deviceObject.Scheduled, 0, deviceDatabaseSupport.RackId, deviceObject.RackPositionRow,
                                                    deviceObject.RackPositionColumn))
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                                   string.Format(CultureInfo.InvariantCulture, "Device successfully {0}. {1}, {2}",
                                                                                 (deviceWriteMode == DeviceWriteMode.Insert ? "created" : "updated"), deviceObject,
                                                                                 deviceDatabaseSupport));

                    returnValue = true;
                }
                else
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                                   string.Format(CultureInfo.InvariantCulture, "Error during {0} device on database. {1}, {2}",
                                                                                 (deviceWriteMode == DeviceWriteMode.Insert ? "inserting" : "updating"), deviceObject,
                                                                                 deviceDatabaseSupport));
                }
            }
            else
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                               string.Format(CultureInfo.InvariantCulture,
                                                                             "Unable to find Station/Building/Rack/Interface/Server data in Database, to {0} device. {1}",
                                                                             (deviceWriteMode == DeviceWriteMode.Insert ? "create" : "update"), deviceObject));
            }

            return returnValue;
        }

        #endregion

        #region Eliminazione di una periferica da base dati

        private static bool DeleteDevice(DeviceObject deviceObject)
        {
            bool returnValue = false;

            if (persistenceFactory.DeleteDevice(dbConnection.ConnectionString, deviceObject.DeviceId))
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                               string.Format(CultureInfo.InvariantCulture, "Device removed successfully. {0}", deviceObject));

                returnValue = true;
            }
            else
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                               string.Format(CultureInfo.InvariantCulture, "Error during device removal. {0}",
                                                                             deviceObject));
            }

            return returnValue;
        }

        #endregion

        #region Stop del servizio

        protected override void OnStop()
        {
            if (pollingTimer != null)
            {
                pollingTimer.Stop();
                pollingTimer.Dispose();
            }

            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Polling stopped.");
        }

        #endregion

        #region Metodo di avvio forzato del servizio da usare per debug

#if (DEBUG)
        public void Run() {
            OnStart(null);
            OnStop();
        }
#endif

        #endregion
    }
}
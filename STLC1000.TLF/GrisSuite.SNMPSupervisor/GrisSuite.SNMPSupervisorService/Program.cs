using System.Globalization;
using System.ServiceProcess;
using System.Threading;

namespace GrisSuite.SnmpSupervisorService
{
    /// <summary>
    ///   Classe pubblic avvio SNMP Supervisor
    /// </summary>
    public static class Program
    {
        private static void Main()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

#if (!DEBUG)
            // Se compilato in modalit� Release � un servizio windows
            ServiceBase[] ServicesToRun = new ServiceBase[] {new MainService()};
            ServiceBase.Run(ServicesToRun);
#else
            // Se compilato in modalit� Debug � una applicazione console (lo stop corrisponde con la chiusura dell'applicazione)
            using (MainService service = new MainService())
            {
                service.Run();
            }
#endif
        }

#if (DEBUG)
        /// <summary>
        /// Esecuzione del programma in fase di strumentazione
        /// </summary>
        public static bool Run()
        {
            using (MainService service = new MainService())
            {
                service.Run();
            }

            return true;
        }
#endif
    }
}
﻿using System;
using System.IO;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorService;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    /// <summary>
    ///   Test del servizio, servono principalmente per la Code Coverage
    /// </summary>
    [TestFixture]
    public class ServiceTests
    {
        [Test]
        public void ServiceTest_InitializeStart_ErrorsOnConfiguration_NullConfig()
        {
            Exception lastException = MainService.InitializeStart(null, false);

            Assert.IsInstanceOf(typeof (ConfigurationSystemException), lastException);
            Assert.AreEqual(lastException.Message, "File name with device list is mandatory");
        }

        [Test]
        public void ServiceTest_InitializeStart_ErrorsOnConfiguration_ConfigFileNotValid()
        {
            Exception lastException = MainService.InitializeStart("dummy", false);

            Assert.IsInstanceOf(typeof (ConfigurationSystemException), lastException);
            Assert.AreEqual(lastException.Message, "Unable to load file with device list: dummy");
        }

        [Test]
        [Ignore("Da attivare per la Code Coverage")]
        public void ServiceTest_InitializeStart_CompleteWithNoErrors_OnDatabase()
        {
            Exception lastException = MainService.InitializeStart(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "System.xml"), true);

            Assert.IsNull(lastException);
        }

        [Test]
        [Ignore("Da attivare per la Code Coverage")]
        public void ServiceTest_InitializeStart_CompleteWithNoErrors_OnFileDump()
        {
            Exception lastException = MainService.InitializeStart(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "System.xml"), false);

            Assert.IsNull(lastException);
        }
    }
}
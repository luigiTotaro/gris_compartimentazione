﻿using System;
using System.Collections;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryUnitTests
    {
        [Test]
        public void DeviceObject_ToStringTest()
        {
            TopographyLocation topography = new TopographyLocation(135004171, "Nome stazione", 17676, "Fabbricato", "Note fabbricato", 127671276, "Nome rack", "SUPPTELEIND1",
                                                                   "Note rack");
            DeviceObject target = new DeviceObject(true, 1110290192220, "Nome Monitor test 1", "INFOSTAZIONI1", IPAddress.Parse("192.168.0.1"), 281474976645125, "Nome regione",
                                                   73072645, "Nome zona", 4097471873029, "Nome nodo", 188222, "Nome host server", "Nome server", "Numero di serie", 2, 1, 1, 1, 5, 3,
                                                   topography, "public", 161);

            Assert.AreEqual(
                "Device Id: 1110290192220, Device name: Nome Monitor test 1, IP Address: 192.168.0.1, Type: INFOSTAZIONI1, Station Id: 135004171, Station Name: Nome stazione, Building Id: 17676, Building Name: Fabbricato, Building Notes: Note fabbricato, Location Id: 127671276, Location Name: Nome rack, Location Type: SUPPTELEIND1, Location Notes: Note rack, Region Id: 281474976645125, Region: Nome regione, Zone Id: 73072645, Zone: Nome zona, Node Id: 4097471873029, Node: Nome nodo, Server Id: 188222, Server Host: Nome host server, Server Name: Nome server, Serial Number: Numero di serie, Port Id: 2, Profile Id: 1, Active: 1, Scheduled: 1, RackPositionColumn: 5, RackPositionRow: 3",
                target.ToString());
        }

        [Test]
        public void EventObject_EventData_EventDataAndHashTest()
        {
            EventObject eventObject = new EventObject(871287, new Guid(), "Dati evento", new DateTime(2009, 09, 08, 17, 08, 25, 199));
            string actual = Encoding.GetEncoding(1252).GetString(eventObject.GetEventDataBytes());

            Assert.AreEqual("Dati evento", actual);

            SHA256 sha = new SHA256Managed();

            byte[] expectedHash = sha.ComputeHash(Encoding.GetEncoding(1252).GetBytes("Dati evento"));
            byte[] actualHash = eventObject.GetHash();

            Assert.IsTrue(ArraysEqual(expectedHash, actualHash));
        }

        [Test]
        public void EventObject_NullEventData_EventDataAndHashTest()
        {
            EventObject eventObject = new EventObject(871287, new Guid(), null, new DateTime(2009, 09, 08, 17, 08, 25, 199));
            byte[] actual = eventObject.GetEventDataBytes();

            Assert.IsNull(actual);

            byte[] actualHash = eventObject.GetHash();

            Assert.IsNull(actualHash);
        }

        private static bool ArraysEqual(IList a1, IList a2)
        {
            if (a1 == a2)
            {
                return true;
            }

            if (a1 == null || a2 == null)
            {
                return false;
            }

            if (a1.Count != a2.Count)
            {
                return false;
            }

            IList list1 = a1, list2 = a2;

            for (int i = 0; i < a1.Count; i++)
            {
                if (!Equals(list1[i], list2[i]))
                {
                    return false;
                }
            }
            return true;
        }

        [Test]
        public void DeviceIdentifier_DecodeData_ValidTest()
        {
            DeviceIdentifier deviceIdentifier1 = new DeviceIdentifier(13035896845);
            Assert.AreEqual(0, deviceIdentifier1.OriginalDeviceId);
            Assert.AreEqual(3, deviceIdentifier1.OriginalNodeId);
            Assert.AreEqual(2304, deviceIdentifier1.OriginalZoneId);
            Assert.AreEqual(13, deviceIdentifier1.OriginalRegionId);

            DeviceIdentifier deviceIdentifier2 = new DeviceIdentifier(281488012607501);
            Assert.AreEqual(1, deviceIdentifier2.OriginalDeviceId);
            Assert.AreEqual(3, deviceIdentifier2.OriginalNodeId);
            Assert.AreEqual(2304, deviceIdentifier2.OriginalZoneId);
            Assert.AreEqual(13, deviceIdentifier2.OriginalRegionId);

            DeviceIdentifier deviceIdentifier3 = new DeviceIdentifier(562962989318157);
            Assert.AreEqual(2, deviceIdentifier3.OriginalDeviceId);
            Assert.AreEqual(3, deviceIdentifier3.OriginalNodeId);
            Assert.AreEqual(2304, deviceIdentifier3.OriginalZoneId);
            Assert.AreEqual(13, deviceIdentifier3.OriginalRegionId);

            DeviceIdentifier deviceIdentifier4 = new DeviceIdentifier(281474989746552845);
            Assert.AreEqual(1000, deviceIdentifier4.OriginalDeviceId);
            Assert.AreEqual(3, deviceIdentifier4.OriginalNodeId);
            Assert.AreEqual(2304, deviceIdentifier4.OriginalZoneId);
            Assert.AreEqual(13, deviceIdentifier4.OriginalRegionId);
        }

        [Test]
        public void NodeIdentifier_DecodeData_ValidTest()
        {
            NodeIdentifier nodeIdentifier1 = new NodeIdentifier(511252299789);
            Assert.AreEqual(119, nodeIdentifier1.OriginalNodeId);
            Assert.AreEqual(2307, nodeIdentifier1.OriginalZoneId);
            Assert.AreEqual(13, nodeIdentifier1.OriginalRegionId);

            NodeIdentifier nodeIdentifier2 = new NodeIdentifier(1962879352834);
            Assert.AreEqual(457, nodeIdentifier2.OriginalNodeId);
            Assert.AreEqual(1210, nodeIdentifier2.OriginalZoneId);
            Assert.AreEqual(2, nodeIdentifier2.OriginalRegionId);
        }

        [Test]
        public void ZoneIdentifier_DecodeData_ValidTest()
        {
            ZoneIdentifier zoneIdentifier1 = new ZoneIdentifier(131530762);
            Assert.AreEqual(2007, zoneIdentifier1.OriginalZoneId);
            Assert.AreEqual(10, zoneIdentifier1.OriginalRegionId);

            ZoneIdentifier zoneIdentifier2 = new ZoneIdentifier(131465226);
            Assert.AreEqual(2006, zoneIdentifier2.OriginalZoneId);
            Assert.AreEqual(10, zoneIdentifier2.OriginalRegionId);
        }

        [Test]
        public void RegionIdentifier_DecodeData_ValidTest()
        {
            RegionIdentifier regionIdentifier1 = new RegionIdentifier(281474976645130);
            Assert.AreEqual(10, regionIdentifier1.OriginalRegionId);

            RegionIdentifier regionIdentifier2 = new RegionIdentifier(281474976645123);
            Assert.AreEqual(3, regionIdentifier2.OriginalRegionId);
        }

        #region FormatExpression

        [Test]
        public void FormatExpression_Constructor_Parsing_Invalid()
        {
            FormatExpression fe = new FormatExpression(string.Empty);
            Assert.AreEqual(false, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Unknown, fe.FormatDataType);
            Assert.AreEqual(string.Empty, fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_SimpleFormat1()
        {
            FormatExpression fe = new FormatExpression("{(long)0:P}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("P", fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatDecimal()
        {
            FormatExpression fe = new FormatExpression("{(long)0:###,0}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("###,0", fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_String_FormatInteger()
        {
            FormatExpression fe = new FormatExpression("{(string)0:0 (A)}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.String, fe.FormatDataType);
            Assert.AreEqual("0 (A)", fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_String_FormatSeconds()
        {
            FormatExpression fe = new FormatExpression("{(string)0:0 s}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.String, fe.FormatDataType);
            Assert.AreEqual("0 s", fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatMinutes()
        {
            FormatExpression fe = new FormatExpression("{(long)0:(*0.0166666666666667)0 min}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0 min", fe.Format);
            Assert.AreEqual(0.0166666666666667f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatFahrenheit()
        {
            FormatExpression fe = new FormatExpression("{(long)0:(*0.5555555555555556+32)0 °C}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0 °C", fe.Format);
            Assert.AreEqual(0.5555555555555556f, fe.Factor);
            Assert.AreEqual(32.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatNegativeOffset()
        {
            FormatExpression fe = new FormatExpression("{(long)0:(-15.5)0 A}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0 A", fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(-15.5f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatFactorDeciHertz()
        {
            FormatExpression fe = new FormatExpression("{(long)0:(*0.1)0 Hz}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0 Hz", fe.Format);
            Assert.AreEqual(0.1f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatFactorAddendComplete()
        {
            FormatExpression fe = new FormatExpression("{(long)0:(*0.001+12.3-0.4) 0.00 Mhz (nominali)}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual(" 0.00 Mhz (nominali)", fe.Format);
            Assert.AreEqual(0.001f, fe.Factor);
            Assert.AreEqual(11.9000006f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Invalid_Unknown_FormatInvalidDataType()
        {
            FormatExpression fe = new FormatExpression("{0:0}");
            Assert.AreEqual(false, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Unknown, fe.FormatDataType);
            Assert.AreEqual(string.Empty, fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatPercentage()
        {
            FormatExpression fe = new FormatExpression(@"{(long)0:0 \%}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual(@"0 \%", fe.Format);
            Assert.AreEqual(1.0f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatGigaByte()
        {
            FormatExpression fe = new FormatExpression(@"{(long)0:(*0.0000000009313225746154785)0.00 GB}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0.00 GB", fe.Format);
            Assert.AreEqual(9.313225746154785e-10f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatMegaByte()
        {
            FormatExpression fe = new FormatExpression(@"{(long)0:(*0.00000095367431640625)0.00 MB}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0.00 MB", fe.Format);
            Assert.AreEqual(9.5367431640625e-7f, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        [Test]
        public void FormatExpression_Constructor_Parsing_Valid_Long_FormatKiloByte()
        {
            FormatExpression fe = new FormatExpression(@"{(long)0:(*0.0009765625)0.00 KB}");
            Assert.AreEqual(true, fe.IsValid);
            Assert.AreEqual(FormatExpressionDataType.Long, fe.FormatDataType);
            Assert.AreEqual("0.00 KB", fe.Format);
            Assert.AreEqual(0.0009765625, fe.Factor);
            Assert.AreEqual(0.0f, fe.Addend);
        }

        #endregion
    }
}
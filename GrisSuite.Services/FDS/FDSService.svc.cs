﻿using System;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.StreamDSTableAdapters;
using GrisSuite.FDS.B2O;
using GrisSuite.Common;

namespace GrisSuite.Services.FDS
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FDSService : IFDSService
    {
        #region IFDSService Members

        public FDSData Get(long devId, TipoFDS fdsType)
        {
            DateTime lastUpdate;

            byte[][] bDevice = GetStreamBytes(devId, out lastUpdate);

            var fds = new FdsDeviceMapB2O().GetDevice(bDevice, fdsType);

            return GetFDSData(fds, lastUpdate, fdsType);
        }

        #endregion

        private static byte[][] GetStreamBytes(long devId, out DateTime lastUpdate)
        {
            byte[][] bDevice = new byte[6][];
            lastUpdate = DateTime.MinValue;
            StreamDS ds = new StreamDS();
            StreamsTableAdapter ta = new StreamsTableAdapter();

            if (ta.FillByDevID(ds.Streams, devId) > 0)
            {
                foreach (var st in ds.Streams)
                {
                    if (!st.IsDataNull() && st.Data != null)
                    {
                        if (lastUpdate < st.DateTime)
                        {
                            lastUpdate = st.DateTime;
                        }

                        switch (st.StrID)
                        {
                            case 1:
                                bDevice[0] = st.Data;
                                break;
                            case 2:
                                bDevice[1] = st.Data;
                                break;
                            case 3:
                                bDevice[2] = st.Data;
                                break;
                            case 4:
                                bDevice[3] = st.Data;
                                break;
                            case 5:
                                bDevice[4] = st.Data;
                                break;
                            case 6:
                                bDevice[5] = st.Data;
                                break;
                        }
                    }
                }
            }

            return bDevice;
        }

        private static FDSData GetFDSData(FdsDevice fdsDevice, DateTime lastUpdate, TipoFDS fdsType)
        {
            FDSData fdsData = new FDSData(fdsType);

            if (fdsDevice.PowerStatus != null)
            {
                FillPowerCards(fdsData.SchedeLocali, fdsDevice.PowerStatus.Local, false);
                FillPowerCards(fdsData.SchedeRemote, fdsDevice.PowerStatus.Remote, true);
            }

            if (fdsDevice.InterfacesStatus != null)
            {
                FillInterfaceCards(fdsData.SchedeLocali, fdsDevice.InterfacesStatus.LocalCards, false);
                FillInterfaceCards(fdsData.SchedeRemote, fdsDevice.InterfacesStatus.RemoteCards, true);
            }

            fdsData.SchedeLocali[FdsConfigHelper.GetFD80000Position(fdsType)] = new SchedaData
                                                                                {
                                                                                    Nome = "FD80000",
                                                                                    Descrizione = "Gestione Ridondanza",
                                                                                    Stato = StateEnum.Ok,
                                                                                    Tipo = TipoScheda.FD80000,
                                                                                    IsRemote = false,
                                                                                    IsSelectable = false
                                                                                };
            fdsData.SchedeRemote[FdsConfigHelper.GetFD80000Position(fdsType)] = new SchedaData
                                                                                {
                                                                                    Nome = "FD80000",
                                                                                    Descrizione = "Gestione Ridondanza",
                                                                                    Stato = StateEnum.Ok,
                                                                                    Tipo = TipoScheda.FD80000,
                                                                                    IsRemote = true,
                                                                                    IsSelectable = false
                                                                                };

            if ((fdsDevice.GeneralStatus != null) && (fdsDevice.E1Counters != null))
            {
                FillCPUCard(fdsData.SchedeLocali, fdsDevice.GeneralStatus.LocalSystemStatus, fdsDevice.E1Counters.Local, false, fdsType);
                FillCPUCard(fdsData.SchedeRemote, fdsDevice.GeneralStatus.RemoteSystemStatus, fdsDevice.E1Counters.Remote, true, fdsType);
            }

            if (fdsDevice.GeneralStatus != null)
            {
                FillFiberInterfaceCard(fdsData.SchedeLocali, fdsDevice.GeneralStatus.LocalSystemStatus, fdsDevice.CPUsMeasures.Local, false, fdsType);
                FillFiberInterfaceCard(fdsData.SchedeRemote, fdsDevice.GeneralStatus.RemoteSystemStatus, fdsDevice.CPUsMeasures.Remote, true, fdsType);
            }

            fdsData.LastUpdate = lastUpdate;

            return fdsData;
        }

        private static void FillInterfaceCards(SchedaData[] schede, IEnumerable<FdsInterfaceCardStatusStream> cards, bool isRemote)
        {
            foreach (var card in cards)
            {
                SchedaData scheda = new SchedaData();
                scheda.IsRemote = isRemote;
                scheda.Stato = card.State;
                scheda.Descrizione = card.CardCode.Description;
                scheda.Tipo = DecodeInterfaceCardType(card.CardCode.Value);
                scheda.Nome = scheda.Tipo.ToString();

                List<SchedaAttributoData> attributi = scheda.Attributi[GruppiAttributi.Principale];

                attributi.Add(new SchedaAttributoData {Nome = "Stato scheda", Stato = scheda.Stato, StatoDescrizione = card.StateMessages.ToString()});

                if (card.CardStatus == null)
                {
                    continue;
                }

                attributi.Add(GetSchedaAttributoFromOWS(card.CardStatus.BusError));
                attributi.Add(GetSchedaAttributoFromOWS(card.CardStatus.V5Error));
                attributi.Add(GetSchedaAttributoFromOWS(card.CardStatus.V24Error));

                if (card.CardStatus is FdsInterfaceCardFD20000StatusBits)
                {
                    attributi.Add(GetSchedaAttributoFromOWS(card.CardStatus.LinkPCMNonAttivo));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD20000StatusBits) card.CardStatus).LocalSyncError));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD20000StatusBits) card.CardStatus).RemoteSyncError));
                }
                else if (card.CardStatus is FdsInterfaceCardFD39200StatusBits)
                {
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD39200StatusBits) card.CardStatus).V12Error));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD39200StatusBits) card.CardStatus).MV12Error));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD39200StatusBits) card.CardStatus).FrameError));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD39200StatusBits) card.CardStatus).RelazioneASDE));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD39200StatusBits) card.CardStatus).CorrenteASDE));
                }
                else if (card.CardStatus is FdsInterfaceCardFD40100StatusBits)
                {
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD40100StatusBits) card.CardStatus).V12Error));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD40100StatusBits) card.CardStatus).MV12Error));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD40100StatusBits) card.CardStatus).FrameError));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD40100StatusBits) card.CardStatus).AICError));
                }
                else if (card.CardStatus is FdsInterfaceCardFD60X00StatusBits)
                {
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD60X00StatusBits) card.CardStatus).V12Error));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD60X00StatusBits) card.CardStatus).MV12Error));
                    attributi.Add(GetSchedaAttributoFromOWS(((FdsInterfaceCardFD60X00StatusBits) card.CardStatus).FrameError));
                }

                schede[card.Index - 1] = scheda;
            }
        }

        private static TipoScheda DecodeInterfaceCardType(FdsInterfaceCardTypes types)
        {
            switch (types)
            {
                case FdsInterfaceCardTypes.FD20000_ConsoleDTS:
                    return TipoScheda.FD20000;
                case FdsInterfaceCardTypes.FD39200_ADSE:
                    return TipoScheda.FD39200;
                case FdsInterfaceCardTypes.FD40100_Telecomando:
                    return TipoScheda.FD40100;
                case FdsInterfaceCardTypes.FD60X00_BCA:
                    return TipoScheda.FD60X00;
                case FdsInterfaceCardTypes.Unknown:
                    return TipoScheda.Unknown;
                default:
                    return TipoScheda.Empty;
            }
        }

        private static void FillPowerCards(SchedaData[] schede, IEnumerable<FdsPowerStatusPartStream> powerCards, bool isRemote)
        {
            foreach (var powerCard in powerCards)
            {
                SchedaData scheda = new SchedaData();
                scheda.IsRemote = isRemote;
                scheda.StatoDescrizione = powerCard.StateMessages.ToString();
                scheda.Stato = powerCard.State;
                scheda.Descrizione = powerCard.CardType.Description;
                scheda.Tipo = DecodPowerCardType(powerCard.CardType.Value);
                scheda.Nome = scheda.Tipo.ToString();

                if (powerCard is FdsPowerStatusInputStream)
                {
                    var inputCard = powerCard as FdsPowerStatusInputStream;
                    if (inputCard.CardType.Value == FdsPowerCardTypes.FD13000)
                    {
                        SetAttributiSchedeAlimIn(scheda, inputCard);
                    }
                }

                if (powerCard is FdsPowerStatusOutputStream)
                {
                    var outputCard = powerCard as FdsPowerStatusOutputStream;
                    SetAttributiSchedeAlimOut(scheda, outputCard);
                }

                schede[powerCard.Index - 1] = scheda;
            }
        }

        private static void SetAttributiSchedeAlimIn(SchedaData scheda, FdsPowerStatusInputStream card)
        {
            SchedaAttributoData attributo = new SchedaAttributoData {Nome = "Configurazione Scheda", Stato = card.Status.Configurazione.State};

            List<SchedaAttributoData> attributi = scheda.Attributi[GruppiAttributi.Principale];

            attributi.Add(attributo);

            attributi.Add(GetSchedaAttributoFromOWS(card.BusTension));
            attributi.Add(GetSchedaAttributoFromOWS(card.In1Tension));
            attributi.Add(GetSchedaAttributoFromOWS(card.In2Tension));
            attributi.Add(GetSchedaAttributoFromOWS(card.Current));
        }

        private static void SetAttributiSchedeAlimOut(SchedaData scheda, FdsPowerStatusOutputStream card)
        {
            SchedaAttributoData attributo = new SchedaAttributoData {Nome = "Stato Scheda", Stato = card.Status.AlimentatoreInFunzione.State};

            List<SchedaAttributoData> attributi = scheda.Attributi[GruppiAttributi.Principale];

            attributi.Add(attributo);

            attributi.Add(GetSchedaAttributoFromOWS(card.BusTension));
            if (card.CardType.Value == FdsPowerCardTypes.FD11000_V5 || card.CardType.Value == FdsPowerCardTypes.FD11100_V12)
            {
                attributi.Add(GetSchedaAttributoFromOWS(card.IntTension));
            }
            attributi.Add(GetSchedaAttributoFromOWS(card.Current));
            attributi.Add(GetSchedaAttributoFromOWS(card.Temperature));
        }

        private static SchedaAttributoData GetSchedaAttributoFromOWS(IObjectWithState ows)
        {
            return GetSchedaAttributoFromOWS(ows, string.Empty);
        }

        private static SchedaAttributoData GetSchedaAttributoFromOWS(IObjectWithState ows, string gruppo)
        {
            SchedaAttributoData a = new SchedaAttributoData
                                    {
                                        Nome = ows.Label,
                                        StatoDescrizione = ows.StateMessages.ToString(),
                                        Stato = ows.State,
                                        Gruppo = gruppo
                                    };

            if (ows is IValue && ((IValue) ows).VisibleValue)
            {
                a.Valore = ((IValue) ows).Text;
            }
            return a;
        }

        private static SchedaAttributoData GetSchedaAttributoFromOWS(IObjectWithState ows, string gruppo, string statoDescrizione)
        {
            SchedaAttributoData a = new SchedaAttributoData
                                    {
                                        Nome = ows.Label,
                                        StatoDescrizione = statoDescrizione,
                                        Stato = ows.State,
                                        Gruppo = gruppo
                                    };

            if (ows is IValue && ((IValue) ows).VisibleValue)
            {
                a.Valore = ((IValue) ows).Text;
            }
            return a;
        }

        private static TipoScheda DecodPowerCardType(FdsPowerCardTypes types)
        {
            switch (types)
            {
                case FdsPowerCardTypes.FD11000_V5:
                    return TipoScheda.FD11000;
                case FdsPowerCardTypes.FD11100_V12:
                    return TipoScheda.FD11100;
                case FdsPowerCardTypes.FD11200_M12:
                    return TipoScheda.FD11200;
                case FdsPowerCardTypes.FD13000:
                    return TipoScheda.FD13000;
                case FdsPowerCardTypes.SlotEmpty:
                    return TipoScheda.Empty;
                default:
                    return TipoScheda.Unknown;
            }
        }

        private static void FillCPUCard(SchedaData[] schede, FdsSystemStatusStream fdsSystemStatus, FdsE1StatusStream fdsE1CountersStream,
                                        bool isRemote, TipoFDS fdsType)
        {
            SchedaData scheda = new SchedaData();
            scheda.IsRemote = isRemote;
            scheda.Stato = fdsSystemStatus.State;
            scheda.Descrizione = "CPU";
            scheda.Tipo = TipoScheda.FD90000;
            scheda.Nome = scheda.Tipo.ToString();

            List<SchedaAttributoData> attributi = scheda.Attributi[GruppiAttributi.Principale];

            attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.StatusFlags.StatoInterfacce));
            attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.StatusFlags.FileEventiSistema));
            attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.StatusFlags.CpuAttiva));
            attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.StatusFlags.TipoClock));
            attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.Warnings.LinkPCME1));

            List<SchedaAttributoData> attributiE1 = new List<SchedaAttributoData>();

            // Allarmi E1
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.StatoE1, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.SyncFramePresente, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.LinkE1Stabile, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.SignalStatusIndicator, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.AlarmIndicatorStatusSignal, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.RemoteAlarmIndicatorStatusSignal, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Alarms.RemoteAlarmIndicatorCrcError, "E1"));
            // Sincronismi E1
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Sync.ReceiveBasicFrameAligment, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Sync.ReceiveMultiframeAligment, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Sync.ReceiveCrc4Synchronization, "E1"));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Sync.BitE1, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Sync.BitE2, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.Sync.Crc4Internetworking, "E1"));
            // Contatori E1
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.BitErrorRateCounter, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.RAIContCRCErrorCounter, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.ErroredFrameAlignSignalCounter, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.BipolarViolationErrorCounter, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.EBitErrorCounter, "E1", " "));
            attributiE1.Add(GetSchedaAttributoFromOWS(fdsE1CountersStream.CRC4ErrorCounter, "E1", " "));

            scheda.Attributi.Add(GruppiAttributi.E1, attributiE1);

            int cpuIndex = (fdsSystemStatus.StatusFlags.GerarchiaCPU.Value == PrincipaleSecondariaEnum.Principale
                                ? FdsConfigHelper.GetFD90000Position(fdsType, false)
                                : FdsConfigHelper.GetFD90000Position(fdsType, true));
            schede[cpuIndex] = scheda;

            cpuIndex = (cpuIndex == FdsConfigHelper.GetFD90000Position(fdsType, false)
                            ? FdsConfigHelper.GetFD90000Position(fdsType, true)
                            : FdsConfigHelper.GetFD90000Position(fdsType, false));

            SchedaData altraScheda = new SchedaData();
            altraScheda.IsRemote = scheda.IsRemote;
            altraScheda.Stato = StateEnum.Ok;
            altraScheda.IsSelectable = false;
            altraScheda.Descrizione = scheda.Descrizione;
            altraScheda.Tipo = scheda.Tipo;
            altraScheda.Nome = scheda.Nome;

            schede[cpuIndex] = altraScheda;
        }

        private static void FillFiberInterfaceCard(SchedaData[] schede, FdsSystemStatusStream fdsSystemStatus, FdsMeasureStream fdsCPUsMeasure,
                                                   bool isRemote, TipoFDS fdsType)
        {
            SchedaData scheda = new SchedaData();
            scheda.IsRemote = isRemote;
            scheda.Stato = fdsSystemStatus.Warnings.ConnessionFibre.State;
            if (fdsSystemStatus.StatusFlags.TipoInterfacciaCollegamento.Value == OtticaG703Enum.Ottica)
            {
                scheda.Tipo = TipoScheda.FD91000;
                scheda.Descrizione = "Interfaccia Ottica";
            }
            else
            {
                scheda.Tipo = TipoScheda.FD92000;
                scheda.Descrizione = "Interfaccia G.703";
            }

            scheda.Nome = scheda.Tipo.ToString();

            List<SchedaAttributoData> attributi = scheda.Attributi[GruppiAttributi.Principale];

            if (fdsSystemStatus.StatusFlags.TipoInterfacciaCollegamento.Value == OtticaG703Enum.Ottica)
            {
                attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.Warnings.LivelloSegnaleFibra));
            }

            if (scheda.Tipo == TipoScheda.FD91000)
            {
                attributi.Add(GetSchedaAttributoFromOWS(fdsCPUsMeasure.FiberVolt));
            }

            attributi.Add(GetSchedaAttributoFromOWS(fdsSystemStatus.Warnings.ConnessionFibre));

            int cpuInterface = (fdsSystemStatus.StatusFlags.GerarchiaCPU.Value == PrincipaleSecondariaEnum.Principale
                                    ? FdsConfigHelper.GetSchedeFibraPosition(fdsType, false)
                                    : FdsConfigHelper.GetSchedeFibraPosition(fdsType, true));

            schede[cpuInterface] = scheda;

            cpuInterface = (cpuInterface == FdsConfigHelper.GetSchedeFibraPosition(fdsType, false)
                                ? FdsConfigHelper.GetSchedeFibraPosition(fdsType, true)
                                : FdsConfigHelper.GetSchedeFibraPosition(fdsType, false));

            SchedaData altraScheda = new SchedaData();
            altraScheda.IsRemote = scheda.IsRemote;
            altraScheda.Stato = StateEnum.Ok;
            altraScheda.IsSelectable = false;
            altraScheda.Descrizione = scheda.Descrizione;
            altraScheda.Nome = scheda.Nome;
            altraScheda.Tipo = scheda.Tipo;

            schede[cpuInterface] = altraScheda;
        }
    }
}
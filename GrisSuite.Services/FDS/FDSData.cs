﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using GrisSuite.Common;

namespace GrisSuite.Services.FDS
{
    public enum GruppiAttributi
    {
        Principale,
        E1
    }

    public enum TipoScheda
    {
        Empty,
        Unknown,
        FD11000,
        FD11100,
        FD11200,
        FD13000,
        FD20000,
        FD39200,
        FD40100,
        FD80000,
        FD90000,
        FD91000,
        FD92000,
        FD60X00
    }

    [DataContract]
    public class FDSData
    {
        [DataMember]
        public SchedaData[] SchedeLocali
        {
            get;
            set;
        }

        [DataMember]
        public SchedaData[] SchedeRemote
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastUpdate
        {
            get;
            set;
        }

        [DataMember]
        public TipoFDS Tipo
        {
            get;
            set;
        }

        public FDSData(TipoFDS fdsType)
        {
            this.Tipo = fdsType;

            this.SchedeLocali = new SchedaData[FdsConfigHelper.GetSlotCount(fdsType)];
            for (int i = 0; i < FdsConfigHelper.GetSlotCount(fdsType); i++)
            {
                this.SchedeLocali[i] = new SchedaData {Tipo = TipoScheda.Empty};
            }

            this.SchedeRemote = new SchedaData[FdsConfigHelper.GetSlotCount(fdsType)];
            for (int i = 0; i < FdsConfigHelper.GetSlotCount(fdsType); i++)
            {
                this.SchedeRemote[i] = new SchedaData {Tipo = TipoScheda.Empty};
            }

            this.LastUpdate = DateTime.MinValue;
        }
    }

    public class SchedaData
    {
        public string Nome
        {
            get;
            set;
        }

        private string _statoDescrizione;

        public string StatoDescrizione
        {
            get
            {
                if (string.IsNullOrEmpty(_statoDescrizione))
                {
                    switch (_stato)
                    {
                        case StateEnum.Ok:
                            return "ok";
                        case StateEnum.Error:
                            return "errore";
                        case StateEnum.Unknown:
                            return "sconosciuto";
                        case StateEnum.Warning:
                            return "attenzione";
                        default:
                            return _stato.ToString();
                    }
                }
                return _statoDescrizione;
            }
            set
            {
                _statoDescrizione = value;
            }
        }

        private StateEnum _stato;

        public StateEnum Stato
        {
            get
            {
                return _stato;
            }
            set
            {
                _stato = value;
            }
        }

        public string Descrizione
        {
            get;
            set;
        }

        public bool IsRemote
        {
            get;
            set;
        }

        public bool IsSelectable
        {
            get;
            set;
        }

        public TipoScheda Tipo
        {
            get;
            set;
        }

        //public List<SchedaAttributoData> Attributi { get; set; }
        public Dictionary<GruppiAttributi, List<SchedaAttributoData>> Attributi
        {
            get;
            set;
        }

        public SchedaData()
        {
            //this.Attributi = new List<SchedaAttributoData>();
            this.Attributi = new Dictionary<GruppiAttributi, List<SchedaAttributoData>>();
            this.Attributi.Add(GruppiAttributi.Principale, new List<SchedaAttributoData>());
            this.IsSelectable = true;
        }
    }

    public class SchedaAttributoData
    {
        public string Nome
        {
            get;
            set;
        }

        public string Valore
        {
            get;
            set;
        }

        public string Gruppo
        {
            get;
            set;
        }

        private StateEnum _stato;
        private string _statoDescrizione;

        public string StatoDescrizione
        {
            get
            {
                if (string.IsNullOrEmpty(_statoDescrizione))
                {
                    switch (_stato)
                    {
                        case StateEnum.Ok:
                            return "ok";
                        case StateEnum.Error:
                            return "errore";
                        case StateEnum.Unknown:
                            return "sconosciuto";
                        case StateEnum.Warning:
                            return "attenzione";
                        default:
                            return _stato.ToString();
                    }
                }
                return _statoDescrizione;
            }
            set
            {
                _statoDescrizione = value;
            }
        }

        public SchedaAttributoData()
        {
            this.Nome = string.Empty;
            this.Valore = string.Empty;
            this.Gruppo = string.Empty;
            this.Stato = StateEnum.Ok;
        }

        public StateEnum Stato
        {
            get
            {
                return _stato;
            }
            set
            {
                _stato = value;
            }
        }
    }
}
﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using GrisSuite.Common;

namespace GrisSuite.Services.FDS
{
    // NOTE: If you change the interface name "IFDSService" here, you must also update the reference to "IFDSService" in Web.config.
    [ServiceContract(Namespace = "GrisSuite.Services.FDS")]
    public interface IFDSService
    {
        [OperationContract]
        FDSData Get(long devId, TipoFDS fdsType);
    }
}

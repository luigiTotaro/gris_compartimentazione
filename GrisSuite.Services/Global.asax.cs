﻿using System;
using System.Web;

namespace GrisSuite.Services
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
#if (!DEBUG)
            Exception ex = this.Server.GetLastError();

            if (ex != null)
            {
                // Persiste l'eccezione serializzata in base dati
                Tracer.TraceMessage(this.Context, ex, true);
            }
#endif
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}
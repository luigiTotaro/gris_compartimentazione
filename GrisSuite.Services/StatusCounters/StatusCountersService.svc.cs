﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.AreaStatisticsDSTableAdapters;
using GrisSuite.Data.Gris.RegionDSTableAdapters;
using GrisSuite.Data.Gris.StatusDSTableAdapters;
using GrisSuite.Data.Gris.SystemsInfoDSTableAdapters;
using GrisSuite.Data.Gris.ZoneDSTableAdapters;

namespace GrisSuite.Services.StatusCounters
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class StatusCountersService : IStatusCountersService
	{
		private readonly Dictionary<Status, string> _dictStatus = GetStatusDescriptions();

		#region IStatusCountersService Members

		public ZoneCounters GetZoneCounters(Guid? objectStatusId)
		{
			ZoneCounters rc = new ZoneCounters {Name = string.Empty, Counters = new List<StatusCounter>()};

			try
			{
				ZoneTableAdapter ta = new ZoneTableAdapter();
				ZoneDS.ZoneDataTable t = ta.GetDataByNode(objectStatusId);
				ZoneDS.ZoneRow r;
				if (t.Rows.Count == 1)
				{
					r = (ZoneDS.ZoneRow) t.Rows[0];
				}
				else
				{
					return rc;
				}

				rc.ObjectStatusIdFilter = objectStatusId;
				rc.Name = r.ZonName;
				rc.ZoneId = r.ZonID;
				rc.Counters.Add(new StatusCounter {Status = Status.Ok, Label = this._dictStatus[Status.Ok], Counter = r.Ok});
				rc.Counters.Add(new StatusCounter {Status = Status.Warning, Label = this._dictStatus[Status.Warning], Counter = r.Warning});
				rc.Counters.Add(new StatusCounter {Status = Status.Error, Label = this._dictStatus[Status.Error], Counter = r.Error});
				rc.Counters.Add(new StatusCounter {Status = Status.Maintenance, Label = this._dictStatus[Status.Maintenance], Counter = r.InMaintenance});
				rc.Counters.Add(new StatusCounter {Status = Status.Unknown, Label = this._dictStatus[Status.Unknown], Counter = r.Unknown});
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return rc;
		}

		public RegionCounters GetRegionCounters(Guid? objectStatusId)
		{
            //System.Diagnostics.Debug.WriteLine("GetRegionCounters");

			RegionCounters rc = new RegionCounters {Name = string.Empty, Counters = new List<StatusCounter>()};

			try
			{
				RegionTableAdapter ta = new RegionTableAdapter();
				RegionDS.RegionDataTable t = ta.GetDataByNode(objectStatusId);
				RegionDS.RegionRow r;
				if (t.Rows.Count == 1)
				{
					r = (RegionDS.RegionRow) t.Rows[0];
				}
				else
				{
					r = t.NewRegionRow();
					r.RegID = 0;
					r.RegName = "Intera rete";
					r.Ok = 0;
					r.Warning = 0;
					r.Error = 0;
					r.Unknown = 0;
					r.InMaintenance = 0;
					foreach (RegionDS.RegionRow r1 in t.Rows)
					{
						r.Ok += r1.Ok;
						r.Warning += r1.Warning;
						r.Error += r1.Error;
						r.Unknown += r1.Unknown;
						r.InMaintenance += r1.InMaintenance;
					}
				}

				rc.ObjectStatusIdFilter = objectStatusId;
				rc.Name = r.RegName;
				rc.RegionId = r.RegID;
				rc.Counters.Add(new StatusCounter {Status = Status.Ok, Label = this._dictStatus[Status.Ok], Counter = r.Ok});
				rc.Counters.Add(new StatusCounter {Status = Status.Warning, Label = this._dictStatus[Status.Warning], Counter = r.Warning});
				rc.Counters.Add(new StatusCounter {Status = Status.Error, Label = this._dictStatus[Status.Error], Counter = r.Error});
				rc.Counters.Add(new StatusCounter {Status = Status.Maintenance, Label = this._dictStatus[Status.Maintenance], Counter = r.InMaintenance});
				rc.Counters.Add(new StatusCounter {Status = Status.Unknown, Label = this._dictStatus[Status.Unknown], Counter = r.Unknown});
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return rc;
		}

		public NodeSystemsCounters GetNodeSystemsCounters(long nodId, int filterSystemId)
		{
			NodeSystemsCounters nsc = new NodeSystemsCounters {SystemsCounters = new List<SystemCounters>(), Name = string.Empty};

			ushort regID;
			ushort zonID;
			ushort nodID;
			Utility.DecodeNodeID((ulong)nodId, out nodID, out zonID, out regID);

			try
			{
				SystemTableAdapter ta = new SystemTableAdapter();
				SystemsInfoDS.SystemDataTable t = ta.GetData(nodId, filterSystemId);

				foreach (SystemsInfoDS.SystemRow systemRow in t.Rows)
				{
					if (
						((Status) systemRow.Status != Status.Maintenance)
						// Il sistema nuvola è visibile solo agli admin
						&& ((systemRow.SystemID != Systems.AltrePeriferiche) || GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
						// Il sistema VPN Verde è visibile solamente agli admin, ai rispettivi visualizzatori di sistema e ai gruppi compartimentali visualizzatori di sistema
						&& ((systemRow.SystemID != Systems.MonitoraggioVPNVerde) || ((systemRow.SystemID == Systems.MonitoraggioVPNVerde) && (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInVPNVerdeItaliaGroup() || GrisPermissions.IsUserInLocalVPNVerdeCompGroup((RegCodes)regID))))
						// Il sistema Web Radio è visibile solamente agli admin, ai rispettivi visualizzatori di sistema e ai gruppi compartimentali visualizzatori di sistema
						&& ((systemRow.SystemID != Systems.WebRadio) || ((systemRow.SystemID == Systems.WebRadio) && (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInWebRadioItaliaGroup() || GrisPermissions.IsUserInLocalWebRadioCompGroup((RegCodes)regID))))
						)
					{
						nsc.Name = systemRow.NodName;
						nsc.SystemsCounters.Add(new SystemCounters
						{
							Name = systemRow.SystemDescription,
							SystemId = systemRow.SystemID,
							Status = (Status) systemRow.Status,
							Counters = this.GetSystemCounters(systemRow)
						});
					}
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return nsc;
		}

		public NodeSystemsCounters GetNodeSystemsCountersByObjectStatusId(Guid objectStatusId, int filterSystemId)
		{
			NodeSystemsCounters returnValue = new NodeSystemsCounters {SystemsCounters = new List<SystemCounters>(), Name = string.Empty};

			try
			{
				SystemTableAdapter ta = new SystemTableAdapter();
				long? nodid = ta.GetNodIdFromObjectStatusId(objectStatusId);
				if (nodid != null)
				{
					return this.GetNodeSystemsCounters((long) nodid, filterSystemId);
				}

				return new NodeSystemsCounters {SystemsCounters = new List<SystemCounters>(), Name = string.Empty};
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return returnValue;
		}

		public AreaCountersByEntityState GetAreaStatisticsByObjectStatusId(Guid objectStatusId)
		{
			try
			{
				AreaStatisticsTableAdapter ta = new AreaStatisticsTableAdapter();
				AreaStatisticsDS.AreaStatisticsDataTable dt = ta.GetData(objectStatusId);
				if ((dt != null) && (dt.Rows.Count > 0))
				{
					return new AreaCountersByEntityState
					{
						TotOk = (int) dt[0]["TotOk"],
						TotWarning = (int) dt[0]["TotWarning"],
						TotError = (int) dt[0]["TotError"],
						TotOkPercentage = (Decimal) dt[0]["TotOkPercentage"],
						TotWarningPercentage = (Decimal) dt[0]["TotWarningPercentage"],
						TotErrorPercentage = (Decimal) dt[0]["TotErrorPercentage"],
						EntityStateSeverityOk = (Decimal) dt[0]["EntityStateSeverityOk"],
						EntityStateSeverityWarning = (Decimal) dt[0]["EntityStateSeverityWarning"],
						EntityStateSeverityError = (Decimal) dt[0]["EntityStateSeverityError"],
						EntityStateNameOk = (string) dt[0]["EntityStateNameOk"],
						EntityStateNameWarning = (string) dt[0]["EntityStateNameWarning"],
						EntityStateNameError = (string) dt[0]["EntityStateNameError"],
						TotOkWeighed = (Decimal) dt[0]["TotOkWeighed"],
						TotWarningWeighed = (Decimal) dt[0]["TotWarningWeighed"],
						TotErrorWeighed = (Decimal) dt[0]["TotErrorWeighed"],
						AvgValue = (Decimal) dt[0]["AvgValue"],
						SevLevel = (int) dt[0]["SevLevel"],
						ForcedSevLevel = (byte) dt[0]["ForcedSevLevel"],
						EntityStateMinValueOk = (short) dt[0]["EntityStateMinValueOk"],
						EntityStateMaxValueOk = (short) dt[0]["EntityStateMaxValueOk"],
						EntityStateMinValueWarning = (short) dt[0]["EntityStateMinValueWarning"],
						EntityStateMaxValueWarning = (short) dt[0]["EntityStateMaxValueWarning"],
						EntityStateMinValueError = (short) dt[0]["EntityStateMinValueError"],
						EntityStateMaxValueError = (short) dt[0]["EntityStateMaxValueError"],
						IsWithServerObject = (bool) dt[0]["IsWithServerObject"],
						ObjectTypeId = (int) dt[0]["ObjectTypeId"]
					};
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return null;
		}

		public List<STLCInfoEntity> GetSTLCInfoEntities(Guid objectStatusId)
		{
			List<STLCInfoEntity> STLCInfoEntities = new List<STLCInfoEntity>();

			try
			{
				SystemTableAdapter nta = new SystemTableAdapter();
				long? nodid = nta.GetNodIdFromObjectStatusId(objectStatusId);

				if (nodid != null)
				{
					STLCInfobyNodIDAndDevIDTableAdapter ta = new STLCInfobyNodIDAndDevIDTableAdapter();
					// Lista STLC per nodo, quindi non specifiche per device
					AreaStatisticsDS.STLCInfobyNodIDAndDevIDDataTable dt = ta.GetData(nodid.Value, -1);

					if ((dt != null) && (dt.Rows.Count > 0))
					{
						STLCInfoEntities.AddRange(from AreaStatisticsDS.STLCInfobyNodIDAndDevIDRow row in dt.Rows
							select
								new STLCInfoUtility(row.FullHostName, row.LastMessageType, row.IP, row.RegID, (row.IsLastUpdateNull() ? (DateTime?) null : row.LastUpdate),
									row.SrvID, row.InMaintenance, row.SevLevel, row.SevLevelDetailId, row.SevLevelReal, row.SevLevelDesc, row.SevLevelDetailDesc)
							into stlcInfoUtility
							select
								new STLCInfoEntity
								{
									StatusImageUrl = stlcInfoUtility.StatusImageUrl,
									StatusToolTip = stlcInfoUtility.StatusToolTip,
									HostNameText = stlcInfoUtility.HostNameText,
									HostNameToolTip = stlcInfoUtility.HostNameToolTip,
									OfflineMessageText = stlcInfoUtility.OfflineMessageText,
									OfflineMessageToolTip = stlcInfoUtility.OfflineMessageToolTip,
									LastUpdateVisible = stlcInfoUtility.LastUpdateVisible,
									LastUpdateText = stlcInfoUtility.LastUpdateText,
									LastUpdateDate = stlcInfoUtility.LastUpdateDate
								});
					}
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return STLCInfoEntities;
		}

		private List<StatusCounter> GetSystemCounters(SystemsInfoDS.SystemRow r)
		{
			List<StatusCounter> sc = new List<StatusCounter>
			{
				new StatusCounter {Status = Status.Ok, Label = this._dictStatus[Status.Ok], Counter = r.Ok},
				new StatusCounter {Status = Status.Warning, Label = this._dictStatus[Status.Warning], Counter = r.Warning},
				new StatusCounter {Status = Status.Error, Label = this._dictStatus[Status.Error], Counter = r.Error},
				new StatusCounter
				{
					Status = Status.Maintenance,
					Label = this._dictStatus[Status.Maintenance],
					Counter = 0 //r.InMaintenance
				},
				new StatusCounter {Status = Status.Unknown, Label = this._dictStatus[Status.Unknown], Counter = r.Unknown}
			};
			return sc;
		}

		#endregion

		private static Dictionary<Status, string> GetStatusDescriptions()
		{
			StatusDS.SeverityDataTable t;

			if (HttpContext.Current != null)
			{
				if (HttpContext.Current.Application["StatusDS.SeverityDataTable"] != null)
				{
					t = (StatusDS.SeverityDataTable) HttpContext.Current.Application["StatusDS.SeverityDataTable"];
				}
				else
				{
					SeverityTableAdapter ta = new SeverityTableAdapter();
					t = ta.GetData();

					HttpContext.Current.Application["StatusDS.SeverityDataTable"] = t;
				}
			}
			else
			{
				SeverityTableAdapter ta = new SeverityTableAdapter();
				t = ta.GetData();
			}

			return t.Rows.Cast<StatusDS.SeverityRow>().ToDictionary(r => (Status) r.SevLevel, r => r.Description);
		}
	}
}
﻿using System;
using System.Globalization;
using System.Security.Principal;
using System.Web;
using GrisSuite.Common;

namespace GrisSuite.Services.StatusCounters
{
    // Mappa il più possibile con il controllo GrisSuite.GRIS\Controls\STLCInfo.ascx, controparte di Gris
    public class STLCInfoUtility
    {
        public string StatusImageUrl { get; private set; }
        public string StatusToolTip { get; private set; }
        public string HostNameText { get; private set; }
        public string HostNameToolTip { get; private set; }
        public string OfflineMessageText { get; private set; }
        public string OfflineMessageToolTip { get; private set; }
        public bool LastUpdateVisible { get; private set; }
        public string LastUpdateDate { get; private set; }
        public string LastUpdateText { get; private set; }

        private string FullHostName { get; set; }
        private string LastMessageType { get; set; }
        private string IP { get; set; }
        private long RegID { get; set; }
        private DateTime? LastUpdate { get; set; }
        private long SrvID { get; set; }
        private bool InMaintenance { get; set; }
        private int SevLevel { get; set; }
        private int? SevLevelDetailId { get; set; }
        private int? SevLevelReal { get; set; }
        private string SevLevelDesc { get; set; }
        private string SevLevelDetailDesc { get; set; }

        public STLCInfoUtility(string fullHostName, string lastMessageType, string IP, long regID, DateTime? lastUpdate, long srvID,
                               bool inMaintenance, int sevLevel, int? sevLevelDetailId, int? sevLevelReal, string sevLevelDesc,
                               string sevLevelDetailDesc)
        {
            this.FullHostName = fullHostName;
            this.LastMessageType = lastMessageType;
            this.IP = IP;
            this.RegID = regID;
            this.LastUpdate = lastUpdate;
            this.SrvID = srvID;
            this.InMaintenance = inMaintenance;
            this.SevLevel = sevLevel;
            this.SevLevelDetailId = sevLevelDetailId;
            this.SevLevelReal = sevLevelReal;
            this.SevLevelDesc = sevLevelDesc;
            this.SevLevelDetailDesc = sevLevelDetailDesc;

            if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
            {
                this.LastUpdateVisible = true;
            }
            else
            {
                this.LastUpdateVisible = false;
            }

            this.SetupControl();
        }

        private void SetupControl()
        {
            if (GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2))
            {
                if (this.IP != "")
                {
                    this.HostNameText = this.IP;
                }
                else
                {
                    this.HostNameText = this.FullHostName != "" ? this.FormatHostName(this.FullHostName) : "STLC1000";
                }
            }
            else
            {
                this.HostNameText = this.FullHostName != "" ? this.FormatHostName(this.FullHostName) : "STLC1000";
            }

            if (this.LastMessageType != "" && this.LastUpdate.HasValue)
            {
                this.OfflineMessageToolTip = string.Format("Ultimo messaggio \"{0}\", ricevuto il {1}", this.LastMessageType,
                                                           this.LastUpdate.Value.ToString("dd/MM/yyyy HH.mm", CultureInfo.InvariantCulture));
                this.LastUpdateDate = this.LastUpdate.Value.ToString("dd/MM/yyyy - HH.mm", CultureInfo.InvariantCulture);
                this.LastUpdateText = this.LastMessageType;
            }

            this.HostNameToolTip = string.Format("{0} ({1}) - {2} SN: {3}", this.FormatHostName(this.FullHostName), this.IP,
                                                 this.GetLocalServerTypeName(), Utility.GetSNFromSrvID((ulong)this.SrvID));

            string localServerTypeName = this.GetLocalServerTypeName();

            # region Stato STLC1000

            const string IMG_PATH_TEMPLATE = "server_status_{0}{1}.png";
            if (this.InMaintenance || (this.SevLevel == 9) || (this.SevLevel == 2))
            {
                // Le stazioni prive periferiche associate con stato e che non siano storico vanno in stato 2 di anomalia grave forzata (problemi del collettore)
                this.StatusImageUrl = string.Format(IMG_PATH_TEMPLATE, this.SevLevelReal, "_inmaint");
                this.StatusToolTip = this.SevLevelDesc;
                this.OfflineMessageText = this.SevLevelDesc;

                if (this.SevLevelDetailId != 7)
                {
                    this.OfflineMessageText = this.SevLevelDetailDesc;
                }
            }
            else // In servizio
            {
                this.StatusImageUrl = string.Format(IMG_PATH_TEMPLATE, this.SevLevelReal, string.Empty);
                if (this.SevLevel == 0)
                {
                    this.OfflineMessageText = string.Format("{0} in linea", localServerTypeName);
                    this.StatusToolTip = this.OfflineMessageText;
                }
                else
                {
                    this.OfflineMessageText = string.Format("ATTENZIONE: {0} non in linea", localServerTypeName);
                    this.StatusToolTip = this.OfflineMessageText;
                }
            }

            # endregion
        }

        private string FormatHostName(string fullHostName)
        {
            if (fullHostName.IndexOf('.') > -1)
            {
                string hostName = fullHostName.Substring(0, fullHostName.IndexOf('.'));
                fullHostName = fullHostName.Replace(hostName, hostName.ToUpper());
            }
            else
            {
                fullHostName = fullHostName.ToUpper();
            }

            return fullHostName;
        }

        public string GetLocalServerTypeName()
        {
            return "STLC1000";
        }
    }
}
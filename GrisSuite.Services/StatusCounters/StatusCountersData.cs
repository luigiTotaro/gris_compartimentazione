﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GrisSuite.Common;

namespace GrisSuite.Services.StatusCounters
{
    [DataContract]
    public class StatusCounter
    {
        [DataMember]
        public Status Status { get; set; }

        [DataMember]
        public int Counter { get; set; }

        [DataMember]
        public string Label { get; set; }
    }

    [DataContract]
    public class NodeSystemsCounters
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<SystemCounters> SystemsCounters { get; set; }
    }

    [DataContract]
    public class SystemCounters
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int SystemId { get; set; }

        [DataMember]
        public Status Status { get; set; }

        [DataMember]
        public List<StatusCounter> Counters { get; set; }
    }

    [DataContract]
    public class RegionCounters
    {
        [DataMember]
        public Guid? ObjectStatusIdFilter { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long RegionId { get; set; }

        [DataMember]
        public List<StatusCounter> Counters { get; set; }
    }

    [DataContract]
    public class ZoneCounters
    {
        [DataMember]
        public Guid? ObjectStatusIdFilter { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long ZoneId { get; set; }

        [DataMember]
        public List<StatusCounter> Counters { get; set; }
    }

    [DataContract]
    public class AreaCountersByEntityState
    {
        [DataMember]
        public int TotOk { get; set; }

        [DataMember]
        public int TotWarning { get; set; }

        [DataMember]
        public int TotError { get; set; }

        [DataMember]
        public Decimal TotOkPercentage { get; set; }

        [DataMember]
        public Decimal TotWarningPercentage { get; set; }

        [DataMember]
        public Decimal TotErrorPercentage { get; set; }

        [DataMember]
        public Decimal EntityStateSeverityOk { get; set; }

        [DataMember]
        public Decimal EntityStateSeverityWarning { get; set; }

        [DataMember]
        public Decimal EntityStateSeverityError { get; set; }

        [DataMember]
        public string EntityStateNameOk { get; set; }

        [DataMember]
        public string EntityStateNameWarning { get; set; }

        [DataMember]
        public string EntityStateNameError { get; set; }

        [DataMember]
        public Decimal TotOkWeighed { get; set; }

        [DataMember]
        public Decimal TotWarningWeighed { get; set; }

        [DataMember]
        public Decimal TotErrorWeighed { get; set; }

        [DataMember]
        public Decimal AvgValue { get; set; }

        [DataMember]
        public int SevLevel { get; set; }

        [DataMember]
        public byte ForcedSevLevel { get; set; }

        [DataMember]
        public short EntityStateMinValueOk { get; set; }

        [DataMember]
        public short EntityStateMaxValueOk { get; set; }

        [DataMember]
        public short EntityStateMinValueWarning { get; set; }

        [DataMember]
        public short EntityStateMaxValueWarning { get; set; }

        [DataMember]
        public short EntityStateMinValueError { get; set; }

        [DataMember]
        public short EntityStateMaxValueError { get; set; }

        [DataMember]
        public bool IsWithServerObject { get; set; }

        [DataMember]
        public int ObjectTypeId { get; set; }
    }

    [DataContract]
    public class STLCInfoEntity
    {
        [DataMember]
        public string StatusImageUrl { get; set; }

        [DataMember]
        public string StatusToolTip { get; set; }

        [DataMember]
        public string HostNameText { get; set; }

        [DataMember]
        public string HostNameToolTip { get; set; }

        [DataMember]
        public string OfflineMessageText { get; set; }

        [DataMember]
        public string OfflineMessageToolTip { get; set; }

        [DataMember]
        public bool LastUpdateVisible { get; set; }

        [DataMember]
        public string LastUpdateText { get; set; }

        [DataMember]
        public string LastUpdateDate { get; set; }
    }
}
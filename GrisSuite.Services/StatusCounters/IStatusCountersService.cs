﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace GrisSuite.Services.StatusCounters
{
    [ServiceContract(Namespace = "GrisSuite.Services.StatusCounters")]
    public interface IStatusCountersService
    {
        [OperationContract]
        RegionCounters GetRegionCounters(Guid? objectStatusId);

        [OperationContract]
        ZoneCounters GetZoneCounters(Guid? objectStatusId);

        [OperationContract]
		NodeSystemsCounters GetNodeSystemsCounters(long nodId, int filterSystemId);

        [OperationContract]
		NodeSystemsCounters GetNodeSystemsCountersByObjectStatusId(Guid objectStatusId, int filterSystemId);

        [OperationContract]
        AreaCountersByEntityState GetAreaStatisticsByObjectStatusId(Guid objectStatusId);

        [OperationContract]
        List<STLCInfoEntity> GetSTLCInfoEntities(Guid objectStatusId);
    }
}
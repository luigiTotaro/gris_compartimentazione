﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Web;
using System.Web.SessionState;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.TraceDSTableAdapters;
using GrisSuite.Data.Gris.UsersDSTableAdapters;

namespace GrisSuite.Services
{
    public static class Tracer
    {
        public static bool TraceRequest(HttpContext ctx)
        {
            if (ctx != null && ctx.Session != null)
            {
                try
                {
                    if (ctx.Session.IsNewSession)
                    {
                        new sessionsTableAdapter().InsertSession(ctx.Session.SessionID, ctx.Request.UserHostAddress, ctx.Request.UserAgent,
                                                                 GUtility.GetUserInfos(false), GUtility.GetUserGroups(), DateTime.Now);

                        new usersTableAdapter().LogUser(GUtility.GetUserInfos(false));
                    }

                    TraceDS.requestsDataTable requests = new TraceDS.requestsDataTable();
                    TraceDS.requestsRow request = requests.NewrequestsRow();

                    request.SessionKey = ctx.Session.SessionID;
                    request.Page = ctx.Request.Url.AbsolutePath;
                    request.QueryString = ctx.Request.Url.Query;
                    request.Created = DateTime.Now;

                    requests.AddrequestsRow(request);
                    new requestsTableAdapter().Update(requests);

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        public static bool TraceMessage(HttpContext ctx, string message)
        {
            if (ctx != null && ctx.Session != null)
            {
                if (message.Length > 2048)
                {
                    message = message.Substring(0, 2048);
                }

                try
                {
                    TraceDS.trace_messagesDataTable messages = new TraceDS.trace_messagesDataTable();
                    TraceDS.trace_messagesRow msg = messages.Newtrace_messagesRow();

                    msg.SessionKey = ctx.Session.SessionID;
                    msg.Message = message;
                    msg.Created = DateTime.Now;

                    messages.Addtrace_messagesRow(msg);
                    new trace_messagesTableAdapter().Update(messages);

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        public static bool TraceMessage(HttpContext ctx, Exception exc)
        {
            return TraceMessage(ctx, exc, false);
        }

        public static bool TraceMessage(HttpContext ctx, Exception exc, bool logExtraInfos)
        {
            if (ctx != null && exc != null && ctx.Session != null)
            {
                try
                {
                    string excXml;

                    string msg = (exc.Message.Length > 2048) ? exc.Message.Substring(0, 2048) : exc.Message;

                    MemoryStream memStream;
                    using (memStream = new MemoryStream())
                    {
                        SoapFormatter serializer = new SoapFormatter();
                        serializer.Serialize(memStream, exc);

                        char[] buffer = new char[memStream.Length];
                        memStream.Position = 0;
                        StreamReader reader = new StreamReader(memStream, Encoding.UTF8);
                        reader.Read(buffer, 0, (int)memStream.Length);

                        excXml = new string(buffer);
                    }

                    if (excXml.Length > 0)
                    {
                        string sessionString = "";
                        string formString = "";
                        if (logExtraInfos)
                        {
                            sessionString = GetCollectionString(ctx.Session);
                            formString = GetCollectionString(ctx.Request.Form);
                        }

                        TraceDS.trace_messagesDataTable messages = new TraceDS.trace_messagesDataTable();
                        TraceDS.trace_messagesRow message = messages.Newtrace_messagesRow();

                        message.SessionKey = ctx.Session.SessionID;
                        message.Message = msg;
                        message.Exception = excXml;
                        message.FormValues = formString;
                        message.SessionValues = sessionString;
                        message.Created = DateTime.Now;

                        messages.Addtrace_messagesRow(message);
                        new trace_messagesTableAdapter().Update(messages);

                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        public static string GetCollectionString(IEnumerable collection)
        {
            List<string> tmp = new List<string>();

            if (collection is HttpSessionState)
            {
                HttpSessionState hss = (HttpSessionState)collection;
                for (int counter = 0; counter < hss.Count; counter++)
                {
                    tmp.Add(string.Format("{0}={1}", hss.Keys[counter], hss[counter]));
                }
            }
            else if (collection is NameValueCollection)
            {
                NameValueCollection nvc = (NameValueCollection)collection;
                for (int counter = 0; counter < nvc.Count; counter++)
                {
                    tmp.Add(string.Format("{0}={1}", nvc.AllKeys[counter], nvc[counter]));
                }
            }

            if (tmp.Count > 0)
            {
                return string.Join("|", tmp.ToArray());
            }

            return "";
        }
    }
}
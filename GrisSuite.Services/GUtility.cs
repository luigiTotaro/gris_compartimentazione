﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;

namespace GrisSuite.Services
{
    public class GUtility
    {
        // restituisce una string descrittiva dell'utente loggato e dei gruppi in cui è incluso
        public static string GetUserInfos(bool concatGroups)
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                string userGroups = "";
                WindowsIdentity wIdentity = (WindowsIdentity)HttpContext.Current.User.Identity;

                if (concatGroups)
                {
                    userGroups = string.Format(" ({0})", GetUserGroups());
                }

                return wIdentity.Name + userGroups;
            }

            return "";
        }

        // restituisce la stringa descrittiva dei gruppi in cui è incluso l'utente loggato
        public static string GetUserGroups()
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                string userGroups = "";
                WindowsIdentity wIdentity = (WindowsIdentity)HttpContext.Current.User.Identity;

                string[] arrGroups = null;
                List<string> groups = new List<string>();

                IdentityReferenceCollection irc = wIdentity.Groups;

                if (irc != null)
                {
                    foreach (IdentityReference ir in irc)
                    {
                        NTAccount acc = (NTAccount)ir.Translate(typeof(NTAccount));
                        groups.Add(acc.Value);
                    }
                }

                arrGroups = new string[groups.Count];
                groups.CopyTo(arrGroups);

                userGroups = string.Join(", ", arrGroups);

                return userGroups;
            }

            return "";
        }

        // Persiste l'eccezione serializzata in base dati
        public static void PersistException(Exception ex)
        {
            if (ex != null)
            {
                Tracer.TraceMessage(HttpContext.Current, ex, true);
            }
        }

        // Persiste una stringa di log in base dati
        public static void PersistMessage(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                Tracer.TraceMessage(HttpContext.Current, message);
            }
        }
    }
}
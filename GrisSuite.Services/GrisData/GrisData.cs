﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.Services.Data
{
	[DataContract]
	public class Topografia
	{
		[DataMember]
		public short IdCompartimento { get; set; }

		[DataMember]
		public string NomeCompartimento { get; set; }

		[DataMember]
		public short IdLinea { get; set; }

		[DataMember]
		public string NomeLinea { get; set; }

		[DataMember]
		public short IdLocalita { get; set; }

		[DataMember]
		public string NomeLocalita { get; set; }
	}

	[DataContract]
	public class Sistema
	{
		[DataMember]
		public int IdSistema { get; set; }

		[DataMember]
		public string NomeSistema { get; set; }

		[DataMember] public Periferica[] Periferiche;
	}

	[DataContract]
	public class Periferica
	{
		[DataMember]
		public long IdPeriferica { get; set; }

		[DataMember]
		public string NomePeriferica { get; set; }

		[DataMember]
		public string Tipo { get; set; }

		[DataMember]
		public string NumeroDiSerie { get; set; }

		[DataMember]
		public string Indirizzo { get; set; }

		[DataMember]
		public int Severita { get; set; }

		[DataMember]
		public string SeveritaDescrizione { get; set; }

		[DataMember]
		public int SeveritaDettaglio { get; set; }

		[DataMember]
		public bool UltimaSeveritaConosciuta { get; set; }

		[DataMember]
		public bool InManutenzione { get; set; }

		[DataMember]
		public int IdSistema { get; set; }

		[DataMember]
		public string NomeSistema { get; set; }

		[DataMember]
		public string Produttore { get; set; }

		[DataMember]
		public string Porta { get; set; }

		[DataMember]
		public string PortaTipo { get; set; }

		[DataMember]
		public string PortaStato { get; set; }

		[DataMember]
		public string Stazione { get; set; }

		[DataMember]
		public string Fabbricato { get; set; }

		[DataMember]
		public string FabbricatoDescrizione { get; set; }

		[DataMember]
		public string Supporto { get; set; }

		[DataMember]
		public string SupportoDescrizione { get; set; }

		[DataMember]
		public string SupportoTipo { get; set; }

		[DataMember]
		public string Server { get; set; }

		[DataMember]
		public string ServerNomeHostCompleto { get; set; }

		[DataMember]
		public string ServerIP { get; set; }

		[DataMember]
		public int SupportoPosizioneRiga { get; set; }

		[DataMember]
		public int SupportoPosizioneColonna { get; set; }

		[DataMember]
		public string SeveritaDescrizioneCompleta { get; set; }

		[DataMember]
		public string PosizioneCompleta { get; set; }
	}


    [DataContract]
    public class Localita
    {
        [DataMember] public int Severita;
        
        [DataMember] public Topografia Locazione;
    }

	[DataContract]
	public class LocalitaSistemi
	{
        [DataMember] public int         Severita; 

		[DataMember] public Topografia  Locazione;

		[DataMember] public Sistema[]   Sistemi;
	}

	[DataContract]
	public class DettaglioPeriferica
	{
		[DataMember] public StatoPeriferica[] Stati;

		[DataMember] public Periferica Periferica;
	}

	[DataContract]
	public class StatoPeriferica
	{
		[DataMember]
		public long IdPeriferica { get; set; }

		[DataMember]
		public int IdStato { get; set; }

		[DataMember]
		public string StatoNome { get; set; }

		[DataMember]
		public int StatoSeverita { get; set; }

		[DataMember]
		public DateTime DataAggiornamento { get; set; }

		[DataMember]
		public int IdStatoDettaglio { get; set; }

		[DataMember]
		public int IdStatoSottoDettaglio { get; set; }

		[DataMember]
		public string StatoDettaglioNome { get; set; }

		[DataMember]
		public int StatoDettaglioSeverita { get; set; }

		[DataMember]
		public string StatoDettaglioValore { get; set; }

		[DataMember]
		public string StatoDettaglioDescrizione { get; set; }
	}

    [DataContract]
    public class CompartimentoSeverita
    {
        [DataMember]
        public short IdLocalita;

        [DataMember]
        public int Severita;
    }

}
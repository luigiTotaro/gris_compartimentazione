﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using GrisSuite.Data.Services;
using GrisSuite.Data.Services.ServicesDSTableAdapters;
using GrisSuite.Services.Data;

namespace GrisSuite.Services.GrisData
{
    public enum ObjectTypeId
    {
        Regions = 1,
        Zones = 2,
        Nodes = 3,
        Node_Systems = 4,
        Servers = 5,
        Devices = 6,
        Virtual_Object = 7
    }

	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class GrisService : IGrisService
	{
        public Localita OttieniDatiLocalita(string gettoneSicurezza, short localitaId)
        {
            if (gettoneSicurezza.Equals("CE900392-6931-4173-AD8D-EA9A120635C9"))
            {
                Localita    localita    = new Localita();

                Topografia  topografia  = null;

                NodeDataTableAdapter                                nodeDataTA       = new NodeDataTableAdapter                      ();
                ServicesDS.NodeDataDataTable                        nodeDataDT       = nodeDataTA.GetData                            (localitaId);

                ObjectSeverityByObjectStatusIdTableAdapter          objectSeverityTA = new ObjectSeverityByObjectStatusIdTableAdapter();
                ServicesDS.ObjectSeverityByObjectStatusIdDataTable  objectSeverityDT = objectSeverityTA.GetData                      (nodeDataDT[0].NodeObjectID, (int)ObjectTypeId.Nodes);

                if ((nodeDataDT != null) && (nodeDataDT.Rows.Count > 0))
                {
                    topografia                      = new Topografia();
                    topografia.IdCompartimento      = nodeDataDT[0].RegionID;
                    topografia.NomeCompartimento    = nodeDataDT[0].RegionName;
                    topografia.IdLinea              = nodeDataDT[0].ZoneID;
                    topografia.NomeLinea            = nodeDataDT[0].ZoneName;
                    topografia.IdLocalita           = nodeDataDT[0].NodeID;
                    topografia.NomeLocalita         = nodeDataDT[0].NodeName;

                    if (topografia != null)
                    {
                        if ((objectSeverityDT != null) && (objectSeverityDT.Rows.Count > 0))
                        {
                            localita.Severita = objectSeverityDT[0].SevLevel;
                        }
                        else
                        {
                            localita.Severita = 255;
                        }

                        localita.Locazione = topografia;
                    }
                    else
                        return null;
                }

                return localita;
            }

            return null;
        }

		public LocalitaSistemi OttieniDatiLocalitaSistemi(string gettoneSicurezza, short localitaId, int sistemaId = 0)
		{
			if (gettoneSicurezza.Equals("CE900392-6931-4173-AD8D-EA9A120635C9"))
			{
                LocalitaSistemi localita = new LocalitaSistemi();

				Topografia topografia = null;
				Dictionary<int, Sistema> sistemi = new Dictionary<int, Sistema>();
				List<Periferica> periferiche = new List<Periferica>();

				NodeDataTableAdapter nodeDataTA = new NodeDataTableAdapter();
				ServicesDS.NodeDataDataTable nodeDataDT = nodeDataTA.GetData(localitaId);

                ObjectSeverityByObjectStatusIdTableAdapter objectSeverityTA = new ObjectSeverityByObjectStatusIdTableAdapter();
                ServicesDS.ObjectSeverityByObjectStatusIdDataTable objectSeverityDT = objectSeverityTA.GetData(nodeDataDT[0].NodeObjectID, (int)ObjectTypeId.Nodes); 


				if ((nodeDataDT != null) && (nodeDataDT.Rows.Count > 0))
				{
					topografia = new Topografia();
					topografia.IdCompartimento = nodeDataDT[0].RegionID;
					topografia.NomeCompartimento = nodeDataDT[0].RegionName;
					topografia.IdLinea = nodeDataDT[0].ZoneID;
					topografia.NomeLinea = nodeDataDT[0].ZoneName;
					topografia.IdLocalita = nodeDataDT[0].NodeID;
					topografia.NomeLocalita = nodeDataDT[0].NodeName;
				}

				if (topografia != null)
				{
					DevicesFromNodeTableAdapter devicesFromNodeTA = new DevicesFromNodeTableAdapter();
					ServicesDS.DevicesFromNodeDataTable devicesFromNodeDT = devicesFromNodeTA.GetData(localitaId, sistemaId, 0 /* tutte le device */);

					if (devicesFromNodeDT != null)
					{
						foreach (ServicesDS.DevicesFromNodeRow device in devicesFromNodeDT.Rows)
						{
							Periferica periferica = new Periferica();

							if (!sistemi.ContainsKey(device.SystemID))
							{
								Sistema sistema = new Sistema();
								sistema.IdSistema = device.SystemID;
								sistema.NomeSistema = device.SystemDescription;

								sistemi.Add(device.SystemID, sistema);
							}

							periferica.IdPeriferica                 = device.DevID;
							periferica.NomePeriferica               = device.Name;
							periferica.Tipo                         = device.Type;
							periferica.NumeroDiSerie                = device.IsDeviceSerialNumberNull() ? null : device.DeviceSerialNumber;
							periferica.Indirizzo                    = device.Address;
							periferica.Severita                     = device.SevLevel;
							periferica.SeveritaDescrizione          = device.SevLevelDescription;
							periferica.SeveritaDettaglio            = device.SevLevelDetail;
							periferica.UltimaSeveritaConosciuta     = device.IsLastSevLevelReturnedNull() ? false : device.LastSevLevelReturned;
							periferica.InManutenzione               = device.OperatorMaintenance;
							periferica.IdSistema                    = device.SystemID;
							periferica.NomeSistema                  = device.SystemDescription;
							periferica.Produttore                   = device.IsVendorNameNull() ? null : device.VendorName;
							periferica.Porta                        = device.IsPortNameNull() ? null : device.PortName;
							periferica.PortaTipo                    = device.IsPortTypeNull() ? null : device.PortType;
							periferica.PortaStato                   = device.IsPortNameNull() ? null : device.PortStatus;
							periferica.Stazione                     = device.IsPortStatusNull() ? null : device.StationName;
							periferica.Fabbricato = device.IsBuildingNameNull() ? null : device.BuildingName;
							periferica.FabbricatoDescrizione = device.IsBuildingDescriptionNull() ? null : device.BuildingDescription;
							periferica.Supporto = device.IsRackNameNull() ? null : device.RackName;
							periferica.SupportoTipo = device.IsRackTypeNull() ? null : device.RackType;
							periferica.SupportoDescrizione = device.IsRackDescriptionNull() ? null : device.RackDescription;
							periferica.Server = device.ServerName;
							periferica.ServerNomeHostCompleto = device.ServerFullHostName;
							periferica.ServerIP = device.ServerIP;
							periferica.SupportoPosizioneRiga = device.IsRackPositionRowNull() ? 0 : device.RackPositionRow;
							periferica.SupportoPosizioneColonna = device.IsRackPositionColNull() ? 0 : device.RackPositionCol;
							periferica.SeveritaDescrizioneCompleta = device.IsSevLevelDescriptionCompleteNull() ? null : device.SevLevelDescriptionComplete;
							periferica.PosizioneCompleta = device.IsPositionNull() ? null : device.Position;

							periferiche.Add(periferica);
						}

						foreach (KeyValuePair<int, Sistema> sistema in sistemi)
						{
							sistema.Value.Periferiche = (from p in periferiche where p.IdSistema == sistema.Key select p).ToArray();
						}

                        if((objectSeverityDT != null) && (objectSeverityDT.Rows.Count > 0))
                        {
                            localita.Severita = objectSeverityDT[0].SevLevel;
                        }
                        else
                        { 
                            localita.Severita = 255;
                        }

						localita.Locazione  = topografia;
						localita.Sistemi    = sistemi.Values.ToArray();

						return localita;
					}
				}
			}

			return null;
		}

		public DettaglioPeriferica OttieniDettaglioPeriferica(string gettoneSicurezza, short localitaId, long perifericaId)
		{
			if (gettoneSicurezza.Equals("CE900392-6931-4173-AD8D-EA9A120635C9"))
			{
				DettaglioPeriferica dettaglioPeriferica = new DettaglioPeriferica();

				Periferica periferica = new Periferica();
				List<StatoPeriferica> statiPeriferica = new List<StatoPeriferica>();

				DevicesFromNodeTableAdapter devicesFromNodeTA = new DevicesFromNodeTableAdapter();
				ServicesDS.DevicesFromNodeDataTable devicesFromNodeDT = devicesFromNodeTA.GetData(localitaId, 0 /* Tutti i sistemi */, perifericaId);

				if ((devicesFromNodeDT != null) && (devicesFromNodeDT.Rows.Count > 0))
				{
					ServicesDS.DevicesFromNodeRow device = (ServicesDS.DevicesFromNodeRow)devicesFromNodeDT.Rows[0];

					periferica.IdPeriferica = device.DevID;
					periferica.NomePeriferica = device.Name;
					periferica.Tipo = device.Type;
					periferica.NumeroDiSerie = device.IsDeviceSerialNumberNull() ? null : device.DeviceSerialNumber;
					periferica.Indirizzo = device.Address;
					periferica.Severita = device.SevLevel;
					periferica.SeveritaDescrizione = device.SevLevelDescription;
					periferica.SeveritaDettaglio = device.SevLevelDetail;
					periferica.UltimaSeveritaConosciuta = device.IsLastSevLevelReturnedNull() ? false : device.LastSevLevelReturned;
					periferica.InManutenzione = device.OperatorMaintenance;
					periferica.IdSistema = device.SystemID;
					periferica.NomeSistema = device.SystemDescription;
					periferica.Produttore = device.IsVendorNameNull() ? null : device.VendorName;
					periferica.Porta = device.IsPortNameNull() ? null : device.PortName;
					periferica.PortaTipo = device.IsPortTypeNull() ? null : device.PortType;
					periferica.PortaStato = device.IsPortNameNull() ? null : device.PortStatus;
					periferica.Stazione = device.IsPortStatusNull() ? null : device.StationName;
					periferica.Fabbricato = device.IsBuildingNameNull() ? null : device.BuildingName;
					periferica.FabbricatoDescrizione = device.IsBuildingDescriptionNull() ? null : device.BuildingDescription;
					periferica.Supporto = device.IsRackNameNull() ? null : device.RackName;
					periferica.SupportoTipo = device.IsRackTypeNull() ? null : device.RackType;
					periferica.SupportoDescrizione = device.IsRackDescriptionNull() ? null : device.RackDescription;
					periferica.Server = device.ServerName;
					periferica.ServerNomeHostCompleto = device.ServerFullHostName;
					periferica.ServerIP = device.ServerIP;
					periferica.SupportoPosizioneRiga = device.IsRackPositionRowNull() ? 0 : device.RackPositionRow;
					periferica.SupportoPosizioneColonna = device.IsRackPositionColNull() ? 0 : device.RackPositionCol;
					periferica.SeveritaDescrizioneCompleta = device.IsSevLevelDescriptionCompleteNull() ? null : device.SevLevelDescriptionComplete;
					periferica.PosizioneCompleta = device.IsPositionNull() ? null : device.Position;

					dettaglioPeriferica.Periferica = periferica;
				}

				if (dettaglioPeriferica.Periferica != null)
				{
					StreamsFieldsDataByDeviceTableAdapter streamsFieldsDataByDeviceTA = new StreamsFieldsDataByDeviceTableAdapter();
					ServicesDS.StreamsFieldsDataByDeviceDataTable streamsFieldsDataByDeviceDT = streamsFieldsDataByDeviceTA.GetData(perifericaId);

					if (streamsFieldsDataByDeviceDT != null)
					{
						foreach (ServicesDS.StreamsFieldsDataByDeviceRow streamField in streamsFieldsDataByDeviceDT.Rows)
						{
							StatoPeriferica statoPeriferica = new StatoPeriferica();
							statoPeriferica.IdPeriferica = streamField.DevID;
							statoPeriferica.IdStato = streamField.StrID;
							statoPeriferica.StatoNome = streamField.StreamName;
							statoPeriferica.StatoSeverita = streamField.StreamSevLevel;
							statoPeriferica.DataAggiornamento = streamField.Date;
							statoPeriferica.IdStatoDettaglio = streamField.FieldID;
							statoPeriferica.IdStatoSottoDettaglio = streamField.ArrayID;
							statoPeriferica.StatoDettaglioNome = streamField.StreamFieldName;
							statoPeriferica.StatoDettaglioSeverita = streamField.StreamFieldSevLevel;
							statoPeriferica.StatoDettaglioValore = streamField.StreamFieldValue;
							statoPeriferica.StatoDettaglioDescrizione = streamField.StreamFieldDescription;

							statiPeriferica.Add(statoPeriferica);
						}

						dettaglioPeriferica.Stati = statiPeriferica.ToArray();

						return dettaglioPeriferica;
					}
				}
			}

			return null;
		}

		public Sistema[] OttieniListaSistemi(string gettoneSicurezza, short localitaId = 0)
		{
			if (gettoneSicurezza.Equals("CE900392-6931-4173-AD8D-EA9A120635C9"))
			{
				List<Sistema> sistemi = new List<Sistema>();

				SystemsListFromNodeTableAdapter systemsListFromNodeTA = new SystemsListFromNodeTableAdapter();
				ServicesDS.SystemsListFromNodeDataTable systemsListFromNodeDT = systemsListFromNodeTA.GetData(localitaId);

				if ((systemsListFromNodeDT != null) && (systemsListFromNodeDT.Rows.Count > 0))
				{
					foreach (ServicesDS.SystemsListFromNodeRow system in systemsListFromNodeDT.Rows)
					{
						Sistema sistema = new Sistema();
						sistema.IdSistema = system.SystemID;
						sistema.NomeSistema = system.SystemDescription;
						sistema.Periferiche = null;

						sistemi.Add(sistema);
					}

					return sistemi.ToArray();
				}
			}

			return null;
		}

        public CompartimentoSeverita[] OttieniSeverita(string gettoneSicurezza, int compartimento)
        {
            if (gettoneSicurezza.Equals("CE900392-6931-4173-AD8D-EA9A120635C9"))
            {
                List<CompartimentoSeverita> severita = new List<CompartimentoSeverita>();

                SeverityByNodeListFromRegionTableAdapter severityByNodeFromRegionTA = new SeverityByNodeListFromRegionTableAdapter();
                ServicesDS.SeverityByNodeListFromRegionDataTable severityByNodeFromRegionDT = severityByNodeFromRegionTA.GetData(compartimento);

                if ((severityByNodeFromRegionDT != null) && (severityByNodeFromRegionDT.Rows.Count > 0))
                {
                    foreach (ServicesDS.SeverityByNodeListFromRegionRow system in severityByNodeFromRegionDT.Rows)
                    {
                        CompartimentoSeverita compartimentoSeverita = new CompartimentoSeverita();
                        compartimentoSeverita.IdLocalita = system.NodeID;
                        compartimentoSeverita.Severita = system.SevLevel;

                        severita.Add(compartimentoSeverita);
                    }

                    return severita.ToArray();
                }
            }

            return null;
        }

	}
}
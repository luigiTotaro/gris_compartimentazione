﻿using System.ServiceModel;
using GrisSuite.Services.Data;

namespace GrisSuite.Services.GrisData
{
	[ServiceContract(Namespace = "GrisSuite.Services.GrisData")]
	public interface IGrisService
	{
        [OperationContract]
        Localita            OttieniDatiLocalita         (string gettoneSicurezza, short localitaId);

		[OperationContract]
		LocalitaSistemi     OttieniDatiLocalitaSistemi  (string gettoneSicurezza, short localitaId, int sistemaId);

		[OperationContract]
		DettaglioPeriferica OttieniDettaglioPeriferica  (string gettoneSicurezza, short localitaId, long perifericaId);

		[OperationContract]
		Sistema[]           OttieniListaSistemi         (string gettoneSicurezza, short localitaId);

        [OperationContract]
        CompartimentoSeverita[]  OttieniSeverita        (string gettoneSicurezza, int compartimento);
	}
}
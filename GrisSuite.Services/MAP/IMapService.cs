﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System;

namespace GrisSuite.Services.MAP
{
	[ServiceContract(Namespace = "GrisSuite.Services.MAP")]
	public interface IMapService
	{
		[OperationContract]
		List<Classification> GetClassifications();

		[OperationContract]
		List<GrisMapLayer> GetMapLayersByClassification(byte classificationId);

		[OperationContract]
		List<GrisMapElement> GetMapElementsByLayer(int mapLayerId, byte classificationId, DateTime? lastPositionUpdate, int filterSystemId, byte hideNotActiveNodes);

		[OperationContract]
		List<GrisMapElement> GetMapElementsByStatus(int sevLevel, DateTime? lastPositionUpdate, int filterSystemId, byte hideNotActiveNodes);

		[OperationContract]
		GrisMapElement GetMapElementByObjectId(long id, GrisMapElementType objectType);

		[OperationContract]
		List<GrisSystem> GetSystems();
	}
}
﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using GrisSuite.Common;
using System;

public enum GrisMapElementType
{
	None = 0,
	Region = 1,
	Zone = 2,
	Node = 3,
	Italia = 4
}

public enum GrisMapLayerType
{
	StandardLayer,
	RegionLayer,
	StatusFilterLayer
}

[DataContract]
public class Classification
{
	[DataMember]
	public byte Id { get; set; }
	[DataMember]
	public string Name { get; set; }
	[DataMember]
	public bool CanFilterBySystemId { get; set; }
}

[DataContract]
public class GrisMapLayer
{
	[DataMember]
	public int Id { get; set; }
	[DataMember]
	public string LayerName { get; set; }
	[DataMember]
	public string GroupName { get; set; }
	[DataMember]
	public string ShapefileUri { get; set; }
	[DataMember]
	public double VisibleFromScale { get; set; }
	[DataMember]
	public double VisibleToScale { get; set; }
	[DataMember]
	public bool IsSensitive { get; set; }
	[DataMember]
	public bool ShowElementsSymbols { get; set; }
	[DataMember]
	public bool ShowElementsTooltips { get; set; }
	[DataMember]
	public bool ShowElementsStatus { get; set; }
	[DataMember]
	public double SymbolSize { get; set; }
	[DataMember]
	public GrisMapLayerType MapLayerType { get; set; }
}

[DataContract]
public class GrisMapElement
{
	[DataMember]
	public Guid ObjectStatusId { get; set; }
	[DataMember]
	public long ObjectId { get; set; }
	[DataMember]
	public string Name { get; set; }
	[DataMember]
	public GrisMapElementType MapElementType { get; set; }
	[DataMember]
	public Status Status { get; set; }
	[DataMember]
	public string MapPoints { get; set; }
	[DataMember]
	public string StatusDescription { get; set; }
	[DataMember]
	public double MapOffsetX { get; set; }
	[DataMember]
	public double MapOffsetY { get; set; }
	[DataMember]
	public double CaptionFromScale { get; set; }
	[DataMember]
	public bool WithServer { get; set; }
	[DataMember]
	public string GroupName { get; set; }
	[DataMember]
	public DateTime PositionDate { get; set; }
	[DataMember]
	public IEnumerable<GrisMapLayer> GrisMapLayers { get; set; }
	[DataMember]
	public bool IsNodeAccessibleToUser { get; set; }
	[DataMember]
	public string CustomColor { get; set; }
}

[DataContract]
public class GrisSystem
{
	[DataMember]
	public int SystemId { get; set; }
	[DataMember]
	public string SystemDescription { get; set; }
	[DataMember]
	public string SystemImageUrl { get; set; }
}

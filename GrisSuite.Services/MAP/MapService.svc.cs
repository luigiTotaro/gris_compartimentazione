﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel.Activation;
using System.Threading;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.MapDSTableAdapters;

namespace GrisSuite.Services.MAP
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class MapService : IMapService
	{
		#region IMapService Members

		public List<Classification> GetClassifications()
		{
			List<Classification> classifications = new List<Classification>();

			try
			{
				ClassificationsTableAdapter ta = new ClassificationsTableAdapter();
				MapDS.ClassificationsDataTable t = ta.GetData();

				foreach (MapDS.ClassificationsRow r in t.Rows)
				{
					Classification classification = new Classification { Id = r.ClassificationId, Name = r.ClassificationName, CanFilterBySystemId = r.CanFilterBySystemId };
					classifications.Add(classification);
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return classifications;
		}

		public List<GrisMapLayer> GetMapLayersByClassification(byte classificationId)
		{
			List<GrisMapLayer> layers = new List<GrisMapLayer>();

			try
			{
				MapLayersTableAdapter ta = new MapLayersTableAdapter();
				MapDS.MapLayersDataTable t = ta.GetDataByClassification(classificationId);
				const string layerNameMask = "Layer{0:00}";

				int i = 0;
				foreach (MapDS.MapLayersRow r in t.Rows)
				{
					GrisMapLayer layer = new GrisMapLayer
										 {
											 Id = r.MapLayerId,
											 GroupName = r.GroupName,
											 ShapefileUri = r.IsShapefileUriNull() ? string.Empty : r.ShapefileUri,
											 IsSensitive = r.IsSensitive,
											 LayerName = string.Format(layerNameMask, i++),
											 ShowElementsSymbols = r.ShowElementsSymbols,
											 VisibleFromScale = r.VisibleFromScale,
											 VisibleToScale = r.VisibleToScale,
											 ShowElementsTooltips = r.ShowElementsTooltips,
											 ShowElementsStatus = r.ShowElementsStatus,
											 SymbolSize = r.SymbolSize,
											 MapLayerType = (GrisMapLayerType)Enum.Parse(typeof(GrisMapLayerType), r.MapLayerTypeId.ToString(), true)
										 };
					layers.Add(layer);
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return layers;
		}

		public List<GrisMapElement> GetMapElementsByLayer(int mapLayerId, byte classificationId, DateTime? lastPositionUpdate, int filterSystemId, byte hideNotActiveNodes)
		{
			List<GrisMapElement> elements = new List<GrisMapElement>();

			try
			{
				ObjectStatusTableAdapter ta = new ObjectStatusTableAdapter();

				MapDS.ObjectStatusDataTable t = null;
				try
				{
					t = ta.GetDataByMapLayer(mapLayerId, classificationId, lastPositionUpdate, filterSystemId, hideNotActiveNodes);
				}
				catch (SqlException exc)
				{
					if (exc.Number == 1205 /* deadlock */)
					{
						Thread.Sleep(1000);
						t = ta.GetDataByMapLayer(mapLayerId, classificationId, lastPositionUpdate, filterSystemId, hideNotActiveNodes);
					}
				}

				if (t != null)
				{
					foreach (MapDS.ObjectStatusRow r in t.Rows)
					{
						GrisMapElement element = new GrisMapElement
												 {
													 ObjectStatusId = r.ObjectStatusId,
													 ObjectId = r.ObjectId,
													 MapElementType =
														 (GrisMapElementType)Enum.Parse(typeof(GrisMapElementType), r.ObjectTypeId.ToString()),
													 Name = r.Name,
													 Status = (Status)Enum.Parse(typeof(Status), r.SevLevel.ToString()),
													 MapPoints = SimplifyMapPoints(r.MapPoints, 1),
													 StatusDescription = r.SevLevelDescription,
													 MapOffsetX = r.MapOffsetX,
													 MapOffsetY = r.MapOffsetY,
													 CaptionFromScale = r.CaptionFromScale,
													 WithServer = r.WithServer,
													 GroupName = r.GroupName,
													 PositionDate = r.PositionUpdateDate,
													 IsNodeAccessibleToUser =
														 (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInAlertOperatorsGroup()),
													 CustomColor = r.IsCustomColorNull() ? null : r.CustomColor
												 };
						elements.Add(element);
					}
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return elements;
		}

		public List<GrisMapElement> GetMapElementsByStatus(int sevLevel, DateTime? lastPositionUpdate, int filterSystemId, byte hideNotActiveNodes)
		{
			List<GrisMapElement> elements = new List<GrisMapElement>();

			try
			{
				ObjectStatusTableAdapter ta = new ObjectStatusTableAdapter();

				MapDS.ObjectStatusDataTable t = null;
				try
				{
					t = ta.GetDataByStatus(sevLevel, lastPositionUpdate, filterSystemId, hideNotActiveNodes);
				}
				catch (SqlException exc)
				{
					if (exc.Number == 1205 /* deadlock */)
					{
						Thread.Sleep(1000);
						t = ta.GetDataByStatus(sevLevel, lastPositionUpdate, filterSystemId, hideNotActiveNodes);
					}
				}

				if (t != null)
				{
					foreach (MapDS.ObjectStatusRow r in t.Rows)
					{
						GrisMapElement element = new GrisMapElement
												 {
													 ObjectStatusId = r.ObjectStatusId,
													 ObjectId = r.ObjectId,
													 MapElementType =
														 (GrisMapElementType)Enum.Parse(typeof(GrisMapElementType), r.ObjectTypeId.ToString()),
													 Name = r.Name,
													 Status = (Status)Enum.Parse(typeof(Status), r.SevLevel.ToString()),
													 MapPoints = r.MapPoints,
													 CaptionFromScale = r.CaptionFromScale,
													 WithServer = r.WithServer,
													 GroupName = r.GroupName,
													 PositionDate = r.PositionUpdateDate
												 };
						elements.Add(element);
					}
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return elements;
		}

		public GrisMapElement GetMapElementByObjectId(long id, GrisMapElementType objectType)
		{
			ObjectStatusTableAdapter taOs = new ObjectStatusTableAdapter();

			try
			{
				MapDS.ObjectStatusDataTable tblOs = taOs.GetDataByObjectId(id, (int)objectType);

				if (tblOs.Rows.Count > 0)
				{
					MapDS.ObjectStatusRow row = tblOs[0];

					GrisMapElement element = new GrisMapElement
											 {
												 ObjectStatusId = row.ObjectStatusId,
												 ObjectId = row.ObjectId,
												 MapElementType =
													 (GrisMapElementType)Enum.Parse(typeof(GrisMapElementType), row.ObjectTypeId.ToString()),
												 Name = row.Name,
												 Status = (Status)Enum.Parse(typeof(Status), row.SevLevel.ToString()),
												 MapPoints = row.MapPoints,
												 CaptionFromScale = row.CaptionFromScale,
												 WithServer = row.WithServer,
												 GroupName = row.GroupName
											 };

					int index = 0;
					MapLayersTableAdapter taMl = new MapLayersTableAdapter();
					MapDS.MapLayersDataTable tblMl = taMl.GetDataByObjectStatusId(row.ObjectStatusId);
					const string layerNameMask = "Layer{0:00}";

					foreach (var layerRow in tblMl)
					{
						element.GrisMapLayers = new List<GrisMapLayer>
                                                {
                                                    new GrisMapLayer
                                                    {
                                                        Id = layerRow.MapLayerId,
                                                        GroupName = layerRow.GroupName,
                                                        ShapefileUri = layerRow.IsShapefileUriNull() ? string.Empty : layerRow.ShapefileUri,
                                                        IsSensitive = layerRow.IsSensitive,
                                                        LayerName = string.Format(layerNameMask, index++),
                                                        ShowElementsSymbols = layerRow.ShowElementsSymbols,
                                                        VisibleFromScale = layerRow.VisibleFromScale,
                                                        VisibleToScale = layerRow.VisibleToScale,
                                                        ShowElementsTooltips = layerRow.ShowElementsTooltips,
                                                        ShowElementsStatus = layerRow.ShowElementsStatus,
                                                        SymbolSize = layerRow.SymbolSize,
                                                        MapLayerType =
                                                            (GrisMapLayerType)
                                                            Enum.Parse(typeof(GrisMapLayerType), layerRow.MapLayerTypeId.ToString(), true)
                                                    }
                                                };
						index++;
					}

					return element;
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return null;
		}

		public List<GrisSystem> GetSystems()
		{
			const string IMG_PATH_TEMPLATE = "system_{0}g.png";

			List<GrisSystem> systems = new List<GrisSystem>();

			try
			{
				SystemsTableAdapter ta = new SystemsTableAdapter();
				MapDS.SystemsDataTable t = ta.GetData();

				foreach (MapDS.SystemsRow systemRow in t.Rows)
				{
					if (
                        (systemRow.FilterVisible)
                        &&
						// Il sistema nuvola è visibile solo agli admin
						((systemRow.SystemID != Systems.AltrePeriferiche) || GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
						// Il sistema VPN Verde è visibile solamente agli admin, ai rispettivi visualizzatori di sistema e ai gruppi compartimentali visualizzatori di sistema
						&& ((systemRow.SystemID != Systems.MonitoraggioVPNVerde) || ((systemRow.SystemID == Systems.MonitoraggioVPNVerde) && (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInVPNVerdeItaliaGroup() || GrisPermissions.IsUserInLocalVPNVerdeCompGroup())))
						// Il sistema Web Radio è visibile solamente agli admin, ai rispettivi visualizzatori di sistema e ai gruppi compartimentali visualizzatori di sistema
						&& ((systemRow.SystemID != Systems.WebRadio) || ((systemRow.SystemID == Systems.WebRadio) && (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInWebRadioItaliaGroup() || GrisPermissions.IsUserInLocalWebRadioCompGroup())))
						)
					{
						GrisSystem system = new GrisSystem
						{
							SystemId = systemRow.SystemID,
							SystemDescription = systemRow.SystemDescription,
							SystemImageUrl = String.Format(IMG_PATH_TEMPLATE, systemRow.SystemID)
						};
						systems.Add(system);
					}
				}
			}
			catch (Exception ex)
			{
				GUtility.PersistException(ex);
			}

			return systems;
		}

		private static string SimplifyMapPoints(string mapPoints, int keepPoints)
		{
			if (mapPoints.StartsWith("polyline") && keepPoints > 1)
			{
				string pts = mapPoints.Substring("polyline".Length + 1);
				string[] parts = pts.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
				List<string> newParts = new List<string>(parts.Length);

				foreach (string part in parts)
				{
					string[] aPts = part.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
					List<string> newPoints = new List<string>((aPts.Length / keepPoints) + 2) { aPts[0] };
					for (int i = 1; i < aPts.Length - 1; i++)
					{
						if (i % keepPoints == 0)
						{
							newPoints.Add(aPts[i]);
						}
					}
					newPoints.Add(aPts[aPts.Length - 1]);
					string[] aNewPts = newPoints.ToArray();
					newParts.Add(string.Join(",", aNewPts));
				}

				string[] aNewParts = newParts.ToArray();
				return "polyline " + string.Join("|", aNewParts);
				//return "polyline " + string.Join(",", aNewParts);
			}

			return mapPoints;
		}

		#endregion
	}
}
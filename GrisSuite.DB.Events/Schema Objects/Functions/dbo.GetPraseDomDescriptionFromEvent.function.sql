﻿CREATE FUNCTION [GetPraseDomDescriptionFromEvent]
(
	@EventData VARBINARY(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = CONVERT(VARCHAR(MAX), @EventData);
	
	DECLARE @SYS_Marker VARCHAR(10);
	SET @SYS_Marker = 'SYS-';

	DECLARE @SYS_Marker_Position BIGINT;
	SET @SYS_Marker_Position = CHARINDEX(@SYS_Marker, @Result)
	
	IF (@SYS_Marker_Position > 0)
	BEGIN
		SET @Result = LTRIM(RTRIM(SUBSTRING(@Result, @SYS_Marker_Position, LEN(@Result) - @SYS_Marker_Position + 1)));
	END

	RETURN ISNULL(@Result, '');
END
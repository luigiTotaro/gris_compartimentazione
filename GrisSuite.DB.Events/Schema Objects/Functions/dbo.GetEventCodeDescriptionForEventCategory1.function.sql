﻿CREATE FUNCTION [dbo].[GetEventCodeDescriptionForEventCategory1] (@EventCode TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @CodeDescriptionForEventCategory1 VARCHAR(MAX);
	SET @CodeDescriptionForEventCategory1 = NULL;

	IF (@EventCode = 0)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'NESSUN ERRORE';
	END
	ELSE IF (@EventCode = 1)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 2)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'RESET';
	END
	ELSE IF (@EventCode = 3)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'SISTEMA OK';
	END
	ELSE IF (@EventCode = 4)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE OROLOGIO';
	END
	ELSE IF (@EventCode = 5)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'CONFIGURAZIONE AUTOMATICA';
	END
	ELSE IF (@EventCode = 12)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 13)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 14)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 20)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE PARAMETRI EEPROM';
	END
	ELSE IF (@EventCode = 29)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT SPI NON GESTITO';
	END
	ELSE IF (@EventCode = 30)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT PAIE NON GESTITO';
	END
	ELSE IF (@EventCode = 31)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT PAO NON GESTITO';
	END
	ELSE IF (@EventCode = 32)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC5 NON GESTITO';
	END
	ELSE IF (@EventCode = 33)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC4 NON GESTITO';
	END
	ELSE IF (@EventCode = 34)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC3 NON GESTITO';
	END
	ELSE IF (@EventCode = 35)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC2 NON GESTITO';
	END
	ELSE IF (@EventCode = 36)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TIC3 NON GESTITO';
	END
	ELSE IF (@EventCode = 37)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TIC2 NON GESTITO';
	END
	ELSE IF (@EventCode = 38)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TIC1 NON GESTITO';
	END
	ELSE IF (@EventCode = 39)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT XIRQ NON GESTITO';
	END
	ELSE IF (@EventCode = 40)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT SWI NON GESTITO';
	END
	ELSE IF (@EventCode = 41)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT IOT NON GESTITO';
	END
	ELSE IF (@EventCode = 42)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT NOCOP NON GESTITO';
	END
	ELSE IF (@EventCode = 43)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT CME NON GESTITO';
	END
	ELSE IF (@EventCode = 44)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT RTI NON GESTITO';
	END
	ELSE IF (@EventCode = 45)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'NESSUN ERRORE';
	END
	ELSE IF (@EventCode = 47)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'CONFIGURAZIONE AGGIORNATA';
	END
	ELSE IF (@EventCode = 57)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'MODIFICATO CONFIGURAZIONE RAM';
	END
	ELSE IF (@EventCode = 112)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT SCI NON GESTITO';
	END
	ELSE IF (@EventCode = 113)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TO NON GESTITO';
	END
	ELSE IF (@EventCode = 114)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC1 NON GESTITO';
	END
	ELSE IF (@EventCode = 115)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT IRQ NON GESTITO';
	END
	ELSE IF (@EventCode = 116)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'AGGIORNATO DATA-ORA';
	END
	ELSE IF (@EventCode = 117)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE ERRORI NON VALIDO';
	END
	ELSE IF (@EventCode = 118)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE EVENTI NON VALIDO';
	END
	ELSE IF (@EventCode = 138)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'TEST TDS CONCLUSO CON SUCCESSO';
	END
	ELSE IF (@EventCode = 139)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'TEST TDS FALLITO';
	END
	ELSE IF (@EventCode = 143)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE ERRORI CANCELLATO';
	END
	ELSE IF (@EventCode = 144)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE EVENTI CANCELLATO';
	END
	ELSE
	BEGIN
		SET @CodeDescriptionForEventCategory1 = '';
	END

	RETURN @CodeDescriptionForEventCategory1
END



﻿CREATE FUNCTION [dbo].[GetFDSEventDescription] (@EventCode INT, @DescriptionType TINYINT)
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @EventDescription VARCHAR(MAX);
	SET @EventDescription = NULL;
	
	IF (@DescriptionType = 0) -- Modalità Gris
	BEGIN
		IF (@EventCode = 0)
		BEGIN
			SET @EventDescription = 'Sistema in funzione';
		END
		ELSE IF (@EventCode = 1)
		BEGIN
			SET @EventDescription = 'Power on reset';
		END
		ELSE IF (@EventCode = 2)
		BEGIN
		  SET @EventDescription = 'File eventi non valido';
		END
		ELSE IF (@EventCode = 3)
		BEGIN
		  SET @EventDescription = 'File eventi azzerato';
		END
		ELSE IF (@EventCode = 4)
		BEGIN
		  SET @EventDescription = 'Configurazione non valida';
		END
		ELSE IF (@EventCode = 5)
		BEGIN
		  SET @EventDescription = 'Errore orologio (RTC)';
		END
		ELSE IF (@EventCode = 6)
		BEGIN
		  SET @EventDescription = 'VCC non valida';
		END
		ELSE IF (@EventCode = 7)
		BEGIN
		  SET @EventDescription = 'VCC normale';
		END
		ELSE IF (@EventCode = 8)
		BEGIN
		  SET @EventDescription = '24V non validi';
		END
		ELSE IF (@EventCode = 9)
		BEGIN
		  SET @EventDescription = '24V normali';
		END
		ELSE IF (@EventCode = 10)
		BEGIN
		  SET @EventDescription = '+12V non validi';
		END
		ELSE IF (@EventCode = 11)
		BEGIN
		  SET @EventDescription = '12V normali';
		END
		ELSE IF (@EventCode = 12)
		BEGIN
		  SET @EventDescription = '12V non validi';
		END
		ELSE IF (@EventCode = 13)
		BEGIN
		  SET @EventDescription = '12V normali';
		END
		ELSE IF (@EventCode = 14)
		BEGIN
		  SET @EventDescription = 'Temperatura elevata';
		END
		ELSE IF (@EventCode = 15)
		BEGIN
		  SET @EventDescription = 'Temperatura normale';
		END
		ELSE IF (@EventCode = 16)
		BEGIN
		  SET @EventDescription = 'Sovracarico di corrente';
		END
		ELSE IF (@EventCode = 17)
		BEGIN
		  SET @EventDescription = 'Corrente normale';
		END
		ELSE IF (@EventCode = 18)
		BEGIN
		  SET @EventDescription = 'Periferica sconosciuta';
		END
		ELSE IF (@EventCode = 19)
		BEGIN
		  SET @EventDescription = 'HDLC: Ricevuta sequenza di ABORT';
		END
		ELSE IF (@EventCode = 20)
		BEGIN
		  SET @EventDescription = 'HDLC: Ricevuto pacchetto con FCS errato';
		END
		ELSE IF (@EventCode = 21)
		BEGIN
		  SET @EventDescription = 'HDLC: Overflow di ricezione';
		  END
		ELSE IF (@EventCode = 22)
		BEGIN
		  SET @EventDescription = 'HDLC: Underrun in trasmissione';
		END
		ELSE IF (@EventCode = 23)
		BEGIN
		  SET @EventDescription = 'Segnale di sincronismo frame assente';
		END
		ELSE IF (@EventCode = 24)
		BEGIN
		  SET @EventDescription = 'E1: Stato instabile';
		END
		ELSE IF (@EventCode = 25)
		BEGIN
		  SET @EventDescription = 'E1: Loss Of Signal Status Indicator';
		END
		ELSE IF (@EventCode = 26)
		BEGIN
		  SET @EventDescription = 'E1: Alarm Indicator Status Signal';
		END
		ELSE IF (@EventCode = 27)
		BEGIN
		  SET @EventDescription = 'E1: Remote Alarm Indication Status';
		END
		ELSE IF (@EventCode = 28)
		BEGIN
		  SET @EventDescription = 'E1: RAI and Continuous CRC Error Status';
		END
		ELSE IF (@EventCode = 29)
		BEGIN
		  SET @EventDescription = 'Collegamento su fibra non disponibile';
		END
		ELSE IF (@EventCode = 30)
		BEGIN
		  SET @EventDescription = 'Collegamento incrociato';
		END
		ELSE IF (@EventCode = 31)
		BEGIN
		  SET @EventDescription = 'Collegamento su fibra disponibile';
		END
		ELSE IF (@EventCode = 32)
		BEGIN
		  SET @EventDescription = 'Periferica inserita nel rack';
		END
		ELSE IF (@EventCode = 33)
		BEGIN
		  SET @EventDescription = 'Periferica rimossa dal rack';
		END
		ELSE IF (@EventCode = 34)
		BEGIN
		  SET @EventDescription = 'Periferica cambiata';
		END
		ELSE IF (@EventCode = 35)
		BEGIN
		  SET @EventDescription = 'Periferica operativa';
		END
		ELSE IF (@EventCode = 36)
		BEGIN
		  SET @EventDescription = 'Periferica in anomalia (collegamento disponibile)';
		END
		ELSE IF (@EventCode = 37)
		BEGIN
		  SET @EventDescription = 'Periferica in errore (collegamento non disponibile)';
		END
		ELSE IF (@EventCode = 38)
		BEGIN
		  SET @EventDescription = 'Periferica disabilitata, periferica remota non presente';
		END
		ELSE IF (@EventCode = 39)
		BEGIN
		  SET @EventDescription = 'Periferica disabilitata, periferica remota diversa';
		END
		ELSE IF (@EventCode = 40)
		BEGIN
		  SET @EventDescription = 'Periferica abilitata, collegamento disponibile';
		END
		ELSE IF (@EventCode = 41)
		BEGIN
		  SET @EventDescription = 'Nessun slot PCM30 assegnato alla periferica (slot insufficienti)';
		END
		ELSE IF (@EventCode = 42)
		BEGIN
		  SET @EventDescription = '0 slot PCM30 assegnati alla periferica (non richiesti / diverse)';
		END
		ELSE IF (@EventCode = 43)
		BEGIN
		  SET @EventDescription = '1 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 44)
		BEGIN
		  SET @EventDescription = '2 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 45)
		BEGIN
		  SET @EventDescription = '3 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 46)
		BEGIN
		  SET @EventDescription = '4 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 47)
		BEGIN
		  SET @EventDescription = '5 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 48)
		BEGIN
		  SET @EventDescription = '6 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 49)
		BEGIN
		  SET @EventDescription = '7 slot PCM30 assegnati alla periferica';
		END
		ELSE IF (@EventCode = 50)
		BEGIN
		  SET @EventDescription = 'Sottosistema attivo (controllo diretto delle periferiche)';
		END
		ELSE IF (@EventCode = 51)
		BEGIN
		  SET @EventDescription = 'Sottosistema in riserva';
		END
		ELSE IF (@EventCode = 52)
		BEGIN
		  SET @EventDescription = 'Aggiornato orologio di sistema';
		END
		ELSE IF (@EventCode = 53)
		BEGIN
		  SET @EventDescription = 'Aggiornamento orologio di sistema fallito - ERRORE RTC';
		END
		ELSE IF (@EventCode = 54)
		BEGIN
		  SET @EventDescription = 'Contatori dell´E1 Controller azzerati';
		END
		ELSE IF (@EventCode = 55)
		BEGIN
		  SET @EventDescription = 'Configurato Clock di sistema come MASTER';
		END
		ELSE IF (@EventCode = 56)
		BEGIN
		  SET @EventDescription = 'Configurato Clock di sistema come SLAVE';
		END
		ELSE IF (@EventCode = 57)
		BEGIN
		  SET @EventDescription = 'Indirizzo per collegamento seriale aggiornato';
		END
		ELSE IF (@EventCode = 58)
		BEGIN
		  SET @EventDescription = 'Password di attivazione configurazione aggiornata';
		END
		ELSE IF (@EventCode = 59)
		BEGIN
		  SET @EventDescription = 'Scrittura su EEPROM fallita';
		END
		ELSE IF (@EventCode = 60)
		BEGIN
		  SET @EventDescription = 'Segnale proveniente dalla fibra non valido';
		END
		ELSE IF (@EventCode = 61)
		BEGIN
		  SET @EventDescription = 'Segnale proveniente dalla fibra critico';
		END
		ELSE IF (@EventCode = 62)
		BEGIN
		  SET @EventDescription = 'Segnale proveniente dalla fibra valido';
		END
		ELSE IF (@EventCode = 63)
		BEGIN
		  SET @EventDescription = 'Configurazione scheda ingresso alimentazione errata';
		END
		ELSE IF (@EventCode = 64)
		BEGIN
		  SET @EventDescription = 'Configurazione scheda ingresso alimentazione valida';
		END
		ELSE IF (@EventCode = 65)
		BEGIN
		  SET @EventDescription = 'Tensione su bus scheda ingresso alimentazione bassa';
		END
		ELSE IF (@EventCode = 66)
		BEGIN
		  SET @EventDescription = 'Tensione su bus scheda ingresso alimentazione alta';
		END
		ELSE IF (@EventCode = 67)
		BEGIN
		  SET @EventDescription = 'Tensione su bus scheda ingresso alimentazione normale';
		END
		ELSE IF (@EventCode = 68)
		BEGIN
		  SET @EventDescription = 'Tensione IN1 scheda ingresso alimentazione bassa';
		END
		ELSE IF (@EventCode = 69)
		BEGIN
		  SET @EventDescription = 'Tensione IN1 scheda ingresso alimentazione normale';
		END
		ELSE IF (@EventCode = 70)
		BEGIN
		  SET @EventDescription = 'Tensione IN2 scheda ingresso alimentazione bassa';
		END
		ELSE IF (@EventCode = 71)
		BEGIN
		  SET @EventDescription = 'Tensione IN2 scheda ingresso alimentazione normale';
		END
		ELSE IF (@EventCode = 72)
		BEGIN
		  SET @EventDescription = 'Corrente scheda ingresso alimentazione bassa';
		END
		ELSE IF (@EventCode = 73)
		BEGIN
		  SET @EventDescription = 'Corrente scheda ingresso alimentazione alta';
		END
		ELSE IF (@EventCode = 74)
		BEGIN
		  SET @EventDescription = 'Corrente scheda ingresso alimentazione normale';
		END
		ELSE IF (@EventCode = 75)
		BEGIN
		  SET @EventDescription = 'Aggiornata configurazione periferiche installate';
		END
		ELSE IF (@EventCode = 76)
		BEGIN
		  SET @EventDescription = 'Scheda disabilitata, scheda non rilevata nel sistema locale';
		END
		ELSE IF (@EventCode = 77)
		BEGIN
		  SET @EventDescription = 'Scheda disabilitata, scheda non installata mediante procedura';
		END
		ELSE IF (@EventCode = 78)
		BEGIN
		  SET @EventDescription = 'Scheda disabilitata, scheda diversa da quella installata';
		END
		ELSE IF (@EventCode = 79)
		BEGIN
		  SET @EventDescription = 'Collegamento con CPU remota non operativo';
		END
		ELSE IF (@EventCode = 80)
		BEGIN
		  SET @EventDescription = 'Reset CPU comandato da software';
		END
		ELSE IF ((@EventCode >= 1000) AND (@EventCode <= 1255))
		BEGIN
		  SET @EventDescription = 'ERRORE CPU: Eccezione ' + CONVERT(NVARCHAR(4), @EventCode - 1000);
		END
		ELSE IF (@EventCode = 1256)
		BEGIN
		  SET @EventDescription = 'Programma ritornato da main';
		END
		ELSE IF (@EventCode = 2001)
		BEGIN
		  SET @EventDescription = 'Segnalazione anomalia periferica (Status[1] bit 7)';
		END
		ELSE IF (@EventCode = 2002)
		BEGIN
		  SET @EventDescription = 'Segnalazione anomalia periferica (Status[1] bit 6)';
		END
		ELSE IF (@EventCode = 2003)
		BEGIN
		  SET @EventDescription = 'Segnalazione anomalia periferica (Status[1] bit 5)';
		END
		ELSE IF (@EventCode = 2004)
		BEGIN
		  SET @EventDescription = 'Segnalazione anomalia periferica (Status[1] bit 4)';
		END
		ELSE IF (@EventCode = 2005)
		BEGIN
		  SET @EventDescription = 'Segnalazione anomalia periferica (Status[1] bit 3)';
		END
		ELSE IF (@EventCode = 2006)
		BEGIN
		  SET @EventDescription = 'Segnalazione anomalia periferica (Status[1] bit 2)';
		END
		ELSE IF (@EventCode = 2529)
		BEGIN
		  SET @EventDescription = 'Bus Error';
		END
		ELSE IF (@EventCode = 2530)
		BEGIN
		  SET @EventDescription = 'Vcc Error';
		END
		ELSE IF (@EventCode = 2531)
		BEGIN
		  SET @EventDescription = '24V Error';
		END
		ELSE IF (@EventCode = 2532)
		BEGIN
		  SET @EventDescription = '12V Error';
		END
		ELSE IF (@EventCode = 2533)
		BEGIN
		  SET @EventDescription = '12V Error';
		END
		ELSE IF (@EventCode = 2534)
		BEGIN
		  SET @EventDescription = 'Circuito di relazione aperto';
		END
		ELSE IF (@EventCode = 3057)
		BEGIN
		  SET @EventDescription = 'Bus Error';
		END
		ELSE IF (@EventCode = 3058)
		BEGIN
		  SET @EventDescription = 'Vcc Error';
		END
		ELSE IF (@EventCode = 3059)
		BEGIN
		  SET @EventDescription = '24V Error';
		END
		ELSE IF (@EventCode = 3060)
		BEGIN
		  SET @EventDescription = '12V Error';
		END
		ELSE IF (@EventCode = 3061)
		BEGIN
		  SET @EventDescription = '12V Error';
		END
		ELSE IF (@EventCode = 3062)
		BEGIN
		  SET @EventDescription = 'AIC Error';
		END
		ELSE IF (@EventCode = 3537)
		BEGIN
		  SET @EventDescription = 'Bus Error';
		END
		ELSE IF (@EventCode = 3538)
		BEGIN
		  SET @EventDescription = 'Vcc Error';
		END
		ELSE IF (@EventCode = 3539)
		BEGIN
		  SET @EventDescription = '24V Error';
		END
		ELSE IF (@EventCode = 3540)
		BEGIN
		  SET @EventDescription = 'Local Sync Error';
		END
		ELSE IF (@EventCode = 3541)
		BEGIN
		  SET @EventDescription = 'Remote Sync Error';
		END
		ELSE IF (@EventCode = 3585)
		BEGIN
		  SET @EventDescription = 'Bus Error';
		END
		ELSE IF (@EventCode = 3586)
		BEGIN
		  SET @EventDescription = 'Vcc Error';
		END
		ELSE IF (@EventCode = 3587)
		BEGIN
		  SET @EventDescription = '24V Error';
		END
		ELSE IF (@EventCode = 3588)
		BEGIN
		  SET @EventDescription = '12V Error';
		END
		ELSE IF (@EventCode = 3589)
		BEGIN
		  SET @EventDescription = '12V Error';
		END
		ELSE IF (@EventCode = 3590)
		BEGIN
		  SET @EventDescription = 'Frame Error';
		END
	END

	RETURN ISNULL(@EventDescription, ''); 
END
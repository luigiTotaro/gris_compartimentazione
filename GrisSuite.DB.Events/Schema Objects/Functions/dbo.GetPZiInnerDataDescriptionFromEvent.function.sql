﻿CREATE FUNCTION [dbo].[GetPZiInnerDataDescriptionFromEvent] (@AudioOut1 TINYINT, @AudioOut2 TINYINT, @AudioOut3 TINYINT, @AudioOut4 TINYINT, @ZoneOut1 TINYINT, @ZoneOut2 TINYINT, @ZoneOut3 TINYINT, @ZoneOut4 TINYINT, @ZoneOut5 TINYINT, @ZoneOut6 TINYINT, @ZoneOut7 TINYINT, @ZoneOut8 TINYINT, @ZoneOut9 TINYINT, @ZoneOut10 TINYINT, @ZoneOut11 TINYINT, @ZoneOut12 TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = NULL;

	DECLARE @PartialValue CHAR;
	DECLARE @Separator VARCHAR(2);
	SET @Separator = ', ';

	IF (@AudioOut1 = 1)
	BEGIN
		SET @Result = 'Amplificatore 1';
	END
	
	IF (@AudioOut2 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 2';
	END
	
	IF (@AudioOut3 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 3';
	END
	
	IF (@AudioOut4 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 4';
	END						

	IF (@ZoneOut1 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 1';
	END

	IF (@ZoneOut2 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 2';
	END

	IF (@ZoneOut3 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 3';
	END

	IF (@ZoneOut4 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 4';
	END

	IF (@ZoneOut5 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 5';
	END

	IF (@ZoneOut6 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 6';
	END

	IF (@ZoneOut7 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 7';
	END

	IF (@ZoneOut8 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 8';
	END

	IF (@ZoneOut9 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 9';
	END

	IF (@ZoneOut10 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 10';
	END

	IF (@ZoneOut11 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 11';
	END

	IF (@ZoneOut12 = 1)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 12';
	END

	RETURN ISNULL(@Result, '');
END
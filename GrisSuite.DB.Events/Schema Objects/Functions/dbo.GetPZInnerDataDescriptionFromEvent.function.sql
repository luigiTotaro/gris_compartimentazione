﻿CREATE FUNCTION [dbo].[GetPZInnerDataDescriptionFromEvent]
(
	@EventData VARBINARY(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = NULL;

	IF (DATALENGTH(@EventData) = 24)
	BEGIN
		DECLARE @digit CHAR;
		DECLARE @i INT;

		-- Estrae il campo Data dallo stream dell'evento di categoria 2: array di 8 bytes, codificati come byte doppi in base dati, in ordine invertito
		DECLARE @DataEventCategory2 VARBINARY(16);
		SET @DataEventCategory2 = SUBSTRING(@EventData, 7, 16);

		DECLARE @UnwrappedData VARCHAR(16);
		SET @UnwrappedData = '';

		SET @i = 0;

		WHILE (@i < 8)
		BEGIN
			SET @digit = SUBSTRING(@DataEventCategory2, 2 * @i + 2, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END

			SET @digit  = SUBSTRING(@DataEventCategory2, 2 * @i + 1, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END
			SET @i = @i + 1;
		END

		DECLARE @PartialValue CHAR;
		DECLARE @Separator VARCHAR(2);
		SET @Separator = ', ';

		-- Amplificatore 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 1, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = 'Amplificatore 1';
		END

		-- Amplificatore 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 2, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 2';
		END

		-- Zona 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 5, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 1';
		END

		-- Zona 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 6, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 2';
		END

		-- Zona 3
		SET @PartialValue = SUBSTRING(@UnwrappedData, 7, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 3';
		END

		-- Zona 4
		SET @PartialValue = SUBSTRING(@UnwrappedData, 8, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 4';
		END

		-- Zona 5
		SET @PartialValue = SUBSTRING(@UnwrappedData, 9, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 5';
		END

		-- Zona 6
		SET @PartialValue = SUBSTRING(@UnwrappedData, 10, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 6';
		END

		-- Zona 7
		SET @PartialValue = SUBSTRING(@UnwrappedData, 11, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 7';
		END

		-- Zona 8
		SET @PartialValue = SUBSTRING(@UnwrappedData, 12, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 8';
		END

		-- Zona 9
		SET @PartialValue = SUBSTRING(@UnwrappedData, 13, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 9';
		END

		-- Zona 10
		SET @PartialValue = SUBSTRING(@UnwrappedData, 14, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 10';
		END

		-- Zona 11
		SET @PartialValue = SUBSTRING(@UnwrappedData, 15, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 11';
		END

		-- Zona 12
		SET @PartialValue = SUBSTRING(@UnwrappedData, 16, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 12';
		END
	END

	RETURN ISNULL(@Result, '');
END



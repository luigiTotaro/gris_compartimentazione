﻿CREATE FUNCTION [dbo].[GetPZiInnerDataTestDescriptionFromEvent] (@EventCode TINYINT, @AudioOut1 TINYINT, @AudioOut2 TINYINT, @AudioOut3 TINYINT, @AudioOut4 TINYINT, @ZoneOut1 TINYINT, @ZoneOut2 TINYINT, @ZoneOut3 TINYINT, @ZoneOut4 TINYINT, @ZoneOut5 TINYINT, @ZoneOut6 TINYINT, @ZoneOut7 TINYINT, @ZoneOut8 TINYINT, @ZoneOut9 TINYINT, @ZoneOut10 TINYINT, @ZoneOut11 TINYINT, @ZoneOut12 TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = NULL;
	DECLARE @Separator VARCHAR(2);
	SET @Separator = ', ';	

	DECLARE @TestType VARCHAR(50);
	IF (@EventCode = 152)
	BEGIN
		SET @TestType = 'ESITO TEST PERIODICO - ';
	END
	ELSE IF (@EventCode = 153)
	BEGIN
		SET @TestType = 'ESITO TEST SU ATTIVAZIONE - ';
	END
	ELSE
	BEGIN
		SET @TestType = '';
	END

	IF (@AudioOut1 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Amplificatore 1:' + dbo.GetPZiStatusCodeDescription(@AudioOut1);
	END
	
	IF (@AudioOut2 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Amplificatore 2:' + dbo.GetPZiStatusCodeDescription(@AudioOut2);
	END
	
	IF (@AudioOut3 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Amplificatore 3:' + dbo.GetPZiStatusCodeDescription(@AudioOut3);
	END
	
	IF (@AudioOut4 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Amplificatore 4:' + dbo.GetPZiStatusCodeDescription(@AudioOut4);
	END	

	IF (@ZoneOut1 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 1:' + dbo.GetPZiStatusCodeDescription(@ZoneOut1);
	END
	
	IF (@ZoneOut2 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 2:' + dbo.GetPZiStatusCodeDescription(@ZoneOut2);
	END

	IF (@ZoneOut3 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 3:' + dbo.GetPZiStatusCodeDescription(@ZoneOut3);
	END

	IF (@ZoneOut4 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 4:' + dbo.GetPZiStatusCodeDescription(@ZoneOut4);
	END

	IF (@ZoneOut5 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 5:' + dbo.GetPZiStatusCodeDescription(@ZoneOut5);
	END

	IF (@ZoneOut6 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 6:' + dbo.GetPZiStatusCodeDescription(@ZoneOut6);
	END

	IF (@ZoneOut7 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 7:' + dbo.GetPZiStatusCodeDescription(@ZoneOut7);
	END

	IF (@ZoneOut8 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 8:' + dbo.GetPZiStatusCodeDescription(@ZoneOut8);
	END

	IF (@ZoneOut9 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 9:' + dbo.GetPZiStatusCodeDescription(@ZoneOut9);
	END

	IF (@ZoneOut10 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 10:' + dbo.GetPZiStatusCodeDescription(@ZoneOut10);
	END

	IF (@ZoneOut11 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 11:' + dbo.GetPZiStatusCodeDescription(@ZoneOut11);
	END

	IF (@ZoneOut12 > 0)
	BEGIN
		SET @Result = ISNULL(@Result + @Separator, '') + ISNULL(@TestType, '') + 'Zona 12:' + dbo.GetPZiStatusCodeDescription(@ZoneOut12);
	END

	RETURN ISNULL(@Result, '');
END
﻿CREATE FUNCTION [dbo].[GetFunctionCodeDescriptionForEventCategory1] (@FunctionCode TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @FunctionCodeDescriptionForEventCategory1 VARCHAR(MAX);
	SET @FunctionCodeDescriptionForEventCategory1 = NULL;

	IF (@FunctionCode = 0)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'NESSUNA FUNZIONE';
	END
	ELSE IF (@FunctionCode = 12)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE GESTIONE OROLOGIO';
	END
	ELSE IF (@FunctionCode = 13)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE DI INIZIALIZZAZIONE';
	END
	ELSE IF (@FunctionCode = 14)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE GESTIONE RS485';
	END
	ELSE IF (@FunctionCode = 36)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE TEST TDS';
	END
	ELSE
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE SCONOSCIUTA';
	END	

	RETURN @FunctionCodeDescriptionForEventCategory1
END



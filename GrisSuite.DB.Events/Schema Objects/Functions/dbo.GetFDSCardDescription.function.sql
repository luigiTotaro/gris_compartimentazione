﻿CREATE FUNCTION [dbo].[GetFDSCardDescription] (@CardType INT, @DescriptionType TINYINT)
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @CardDescription VARCHAR(MAX);
	SET @CardDescription = NULL;
	
	IF (@DescriptionType = 0) -- Modalità Gris
	BEGIN
		IF (@CardType = 1)
		BEGIN
			SET @CardDescription = 'FDxxxxx';
		END
		ELSE IF (@CardType = 2)
		BEGIN
			SET @CardDescription = 'FD13000';
		END
		ELSE IF (@CardType = 3)
		BEGIN
			SET @CardDescription = 'FD11000';
		END
		ELSE IF (@CardType = 4)
		BEGIN
			SET @CardDescription = 'FD11100';
		END
		ELSE IF (@CardType = 5)
		BEGIN
			SET @CardDescription = 'FD11200';
		END
		ELSE IF (@CardType = 33)
		BEGIN
			SET @CardDescription = 'FD39000';
		END
		ELSE IF (@CardType = 66)
		BEGIN
			SET @CardDescription = 'FD40000';
		END
		ELSE IF (@CardType = 96)
		BEGIN
			SET @CardDescription = 'FD20000';
		END
		ELSE IF (@CardType = 99)
		BEGIN
			SET @CardDescription = 'FD60000';
		END
		ELSE IF (@CardType = 255)
		BEGIN
			SET @CardDescription = 'FD90000';
		END	
	END
	ELSE IF (@DescriptionType = 1) -- Modalità Supervisore FDS
	BEGIN
		IF (@CardType = 1)
		BEGIN
			SET @CardDescription = 'FDxxxxx (sch.sconosciuta)';
		END
		ELSE IF (@CardType = 2)
		BEGIN
			SET @CardDescription = 'FD13000 (Ing.alimentazione)';
		END
		ELSE IF (@CardType = 3)
		BEGIN
			SET @CardDescription = 'FD11000 (VCC)';
		END
		ELSE IF (@CardType = 4)
		BEGIN
			SET @CardDescription = 'FD11100 (+12V)';
		END
		ELSE IF (@CardType = 5)
		BEGIN
			SET @CardDescription = 'FD11200 (-12V)';
		END
		ELSE IF (@CardType = 33)
		BEGIN
			SET @CardDescription = 'FD39000 (ASDE)';
		END
		ELSE IF (@CardType = 66)
		BEGIN
			SET @CardDescription = 'FD40000 (Telecomando)';
		END
		ELSE IF (@CardType = 96)
		BEGIN
			SET @CardDescription = 'FD20000 (Int.Telefono)';
		END
		ELSE IF (@CardType = 99)
		BEGIN
			SET @CardDescription = 'FD60000 (Int. BCA)';
		END
		ELSE IF (@CardType = 255)
		BEGIN
			SET @CardDescription = 'FD90000 (CPU)';
		END	
	END

	RETURN ISNULL(@CardDescription, ''); 
END
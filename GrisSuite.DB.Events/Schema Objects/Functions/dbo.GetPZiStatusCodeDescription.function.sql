﻿CREATE FUNCTION [dbo].[GetPZiStatusCodeDescription] (@Status TINYINT)
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @StatusCodeDescription VARCHAR(MAX);
	SET @StatusCodeDescription = NULL;
	
	IF (@Status = 2)
	BEGIN
		SET @StatusCodeDescription = 'OK'
	END
	ELSE IF (@Status = 3)
	BEGIN
		SET @StatusCodeDescription = 'Attenzione'
	END
	ELSE IF (@Status = 4)
	BEGIN
		SET @StatusCodeDescription = 'Errore'
	END
	ELSE IF (@Status = 5)
	BEGIN
		SET @StatusCodeDescription = 'Aperta'
	END
	ELSE IF (@Status = 6)
	BEGIN
		SET @StatusCodeDescription = 'Corto Circuito'
	END
	ELSE IF (@Status = 7)
	BEGIN
		SET @StatusCodeDescription = 'Non determinabile'
	END
					
	RETURN ISNULL(@StatusCodeDescription, ''); 
END
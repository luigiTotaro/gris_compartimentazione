﻿-- =============================================
-- Author:		Luigi Ferrarini
-- Create date: 10 Dec 2010
-- Last update: 10 Dec 2010
-- Description:	Ritorna un varchar
-- =============================================

CREATE FUNCTION [dbo].[fn_CardTypeDescription] (@CardType TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @CardDescription VARCHAR(MAX);
	SET @CardDescription = NULL;

	SET @CardDescription =  CASE
		WHEN @CardType = 0     THEN 'CHL'               
		WHEN @CardType = 1     THEN 'ULU'               
		WHEN @CardType = 2     THEN 'SS5'               
		WHEN @CardType = 3     THEN 'STS'               
		WHEN @CardType = 4     THEN 'PH2'               
		WHEN @CardType = 5     THEN 'MFZ'               
		WHEN @CardType = 6     THEN 'UBC'               
		WHEN @CardType = 7     THEN 'UPP'               
		WHEN @CardType = 8     THEN 'UPS'               
		WHEN @CardType = 9     THEN 'CHL8'              
		WHEN @CardType = 10    THEN 'ULU8'              
		WHEN @CardType = 11    THEN 'STS8'              
		WHEN @CardType = 12    THEN 'FDM8'              
		WHEN @CardType = 13    THEN 'BIB_STS'           
		WHEN @CardType = 14    THEN 'BIB_PH'            
		WHEN @CardType = 15    THEN 'TRC'               
		WHEN @CardType = 15    THEN 'STFSOS'            
		WHEN @CardType = 15    THEN 'STF64'             
		WHEN @CardType = 14    THEN 'PH64'              
		WHEN @CardType = 0x20  THEN 'BIB_AS'            
		WHEN @CardType = 0x21  THEN 'U68000'            
		WHEN @CardType = 0x22  THEN 'REM_TENS'          
		WHEN @CardType = 0x23  THEN 'BIB_TLF'           
		WHEN @CardType = 0x23  THEN 'TLF64'             
		WHEN @CardType = 0x24  THEN 'BIB_SYS'           
		WHEN @CardType = 0x25  THEN 'SIM_PHONE'         
		WHEN @CardType = 0x26  THEN 'UBC2'              
		WHEN @CardType = 0x27  THEN 'CHL64'             
		WHEN @CardType = 0x28  THEN 'ULU64'             
		WHEN @CardType = 0x28  THEN 'SWCTC'             
		WHEN @CardType = 0x29  THEN 'AS64'              
		WHEN @CardType = 0x2a  THEN 'STS64'             
		WHEN @CardType = 0x2b  THEN 'SLIC'              
		WHEN @CardType = 0x2c  THEN 'UKLB'              
		WHEN @CardType = 0x2d  THEN 'AET64'             
		WHEN @CardType = 0x2e  THEN 'GI64'              
		WHEN @CardType = 0x2f  THEN 'STSI'              
		WHEN @CardType = 0x2f  THEN 'LAST_DEFINED'      
		WHEN @CardType = 0x80  THEN 'REC_PLAY'          
		WHEN @CardType = 0x81  THEN 'PLAY'              
		WHEN @CardType = 0x82  THEN 'SOS_PHONE'         
		WHEN @CardType = 0x83  THEN 'SOS_LINE'          
		WHEN @CardType = 0x84  THEN 'SDETECT'           
		WHEN @CardType = 0x85  THEN 'ISDNem64'          
		WHEN @CardType = 0x86  THEN 'BIB_GSM'
		ELSE 'CPU'
	END
	RETURN @CardDescription
END
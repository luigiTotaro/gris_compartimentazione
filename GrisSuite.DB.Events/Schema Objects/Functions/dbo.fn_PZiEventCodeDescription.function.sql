﻿-- =============================================
-- Author:		Luigi Ferrarini
-- Create date: 19 Jan 2011
-- Last update: 19 Jan 2011
-- Description:	Ritorna un varchar
-- =============================================

CREATE FUNCTION [dbo].[fn_PZiEventCodeDescription] (@EventCode TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @CodeDescription VARCHAR(MAX);
	SET @CodeDescription = NULL;

	SET @CodeDescription =  CASE
		WHEN @EventCode =   0	THEN 'SISTEMA OK'             	                    
		WHEN @EventCode =   1	THEN 'RESET'        		    	                
		WHEN @EventCode =   2	THEN 'ERRORE FILE EVENTI'     	                    
		WHEN @EventCode =   3	THEN 'ERRORE OROLOGIO'        	                    
		WHEN @EventCode =   4	THEN 'AGGIORNATO DATA-ORA'    	                    
		WHEN @EventCode =   5	THEN 'ATTIVATA CONF.TEST'     	                    
		WHEN @EventCode =   6	THEN 'ERRORE CONFIGURAZIONE'  	                    
		WHEN @EventCode =   7	THEN 'FILE EVENTI CANCELLATO' 	                    
		WHEN @EventCode =   8	THEN 'MODIFICATO CONFIGURAZIONE RAM'                
		WHEN @EventCode =   9	THEN 'CONFIGURAZIONE AGGIORNATA'                    
		WHEN @EventCode =  10	THEN 'ESEGUITO AUTOSETUP'                           
		WHEN @EventCode =  11	THEN 'VARIATO VOLUME MASTER'                        
		WHEN @EventCode = 100	THEN 'ATTIVATO INGRESSO VIRTUALE'                   
		WHEN @EventCode = 101	THEN 'DISATTIVATO INGRESSO VIRTUALE'                
		WHEN @EventCode = 102	THEN 'ATTIVATA INGRESSO'                            
		WHEN @EventCode = 103	THEN 'DISATTIVATA INGRESSO'                         
		WHEN @EventCode = 104	THEN 'ATTIVATA USCITA'                              
		WHEN @EventCode = 105	THEN 'DISATTIVATA USCITA'                           
		WHEN @EventCode = 150	THEN 'ATTIVAZIONE SONORA'                           
		WHEN @EventCode = 151	THEN 'FINE ATTIVAZIONE SONORA'                      
		WHEN @EventCode = 152	THEN 'ESITO TEST PERIODICO'                         
		WHEN @EventCode = 153	THEN 'ESITO TEST SU ATTIVAZIONE'                    
		ELSE 'NON CODIFICATO'
	END
	RETURN @CodeDescription
END
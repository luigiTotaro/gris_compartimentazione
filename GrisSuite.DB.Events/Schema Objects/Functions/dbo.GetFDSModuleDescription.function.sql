﻿CREATE FUNCTION [dbo].[GetFDSModuleDescription] (@ModuleWord INT, @Position TINYINT, @DescriptionType TINYINT)
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @ModuleDescription VARCHAR(MAX);
	SET @ModuleDescription = NULL;
	
	DECLARE @ModuleRemote TINYINT;
	SET @ModuleRemote = 0;
	
	IF ((@ModuleWord & 0x8000) = 0x8000)
	BEGIN
		SET @ModuleRemote = 1;
	END
	ELSE
	BEGIN
		SET @ModuleRemote = 0;
	END
	
	DECLARE @ModuleSource INT;
	SET @ModuleSource = @ModuleWord & 0x0FFF;

	IF (@DescriptionType = 0) -- Modalità Gris
	BEGIN
		SET @ModuleDescription = CONVERT(NVARCHAR(2), ISNULL(@Position, '--'));
		
		IF (@ModuleRemote = 1)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' Remoto';
		END
		ELSE
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' Locale';
		END		
	END
	ELSE IF (@DescriptionType = 1) -- Modalità Supervisore FDS
	BEGIN
		IF (@ModuleRemote = 1)
		BEGIN
			SET @ModuleDescription = 'Sist. REM.';
		END
		ELSE
		BEGIN
			SET @ModuleDescription = 'Sist. LOC.';
		END		
	
		IF (@ModuleSource = 1)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (Main)';
		END
		ELSE IF (@ModuleSource = 2)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (Power)';
		END
		ELSE IF (@ModuleSource = 3)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (LapLink)';
		END
		ELSE IF (@ModuleSource = 4)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (FiberLink)';
		END
		ELSE IF (@ModuleSource = 5)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (Comms)';
		END
		ELSE IF (@ModuleSource = 6)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (Card)';
		END
		ELSE IF (@ModuleSource = 7)
		BEGIN
			SET @ModuleDescription = @ModuleDescription + ' (User)';
		END
	END	

	RETURN ISNULL(@ModuleDescription, ''); 
END
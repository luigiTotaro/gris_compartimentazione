﻿-- =============================================
-- Author:		Luigi Ferrarini
-- Create date: 30 Oct 2008
-- Description:	Ritorna un tinyint
-- =============================================

CREATE FUNCTION [dbo].[fn_FieldFromEventsRawData] 
(
	-- Add the parameters for the function here
	@data varchar(8000),
	@fieldId int
)
RETURNS tinyint
AS
BEGIN
	-- Declare the return variable here
	DECLARE @code tinyint, @offset int

	SET @offset = 2*@fieldId-1;
	
	-- Add the T-SQL statements to compute the return value here
	DECLARE @hex char(1)
	SET @hex = substring(@data,@offset,1)
	SET @code = convert(
					tinyint, 
					CASE WHEN @hex LIKE '[0-9]' 
         				THEN CAST(@hex as tinyint) 
         				ELSE CAST(ASCII(UPPER(@hex))-55 as tinyint) 
		 			END )
	SET @hex = substring(@data,@offset+1,1)
				SET @code = 16* @code + convert(
								tinyint, 
								CASE WHEN @hex LIKE '[0-9]' 
			         				THEN CAST(@hex as tinyint) 
			         				ELSE CAST(ASCII(UPPER(@hex))-55 as tinyint) 
					 			END )
    
	
-- Return the result of the function
	RETURN @code
END
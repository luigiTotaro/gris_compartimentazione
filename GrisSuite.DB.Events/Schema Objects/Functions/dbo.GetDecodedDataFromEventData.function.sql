﻿CREATE FUNCTION [dbo].[GetDecodedDataFromEventData] 
(
	@EventData VARBINARY(24),
	@FieldId INT
)
-- Decodifica 2 bytes dallo stream dell'evento, in base alla posizione.
RETURNS TINYINT
AS
BEGIN
	DECLARE @code TINYINT;
	DECLARE @hex CHAR(1);
	
	SET @hex = SUBSTRING(@EventData, 2 * @FieldId - 1, 1)

	SET @code = CONVERT(
					TINYINT, 
					CASE WHEN @hex LIKE '[0-9]' THEN CAST(@hex AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex)) - 55 AS TINYINT) 
		 			END)

	SET @hex = SUBSTRING(@EventData, 2 * @FieldId, 1);

	SET @code = 16 * @code + CONVERT(
								TINYINT, 
								CASE WHEN @hex LIKE '[0-9]' THEN CAST(@hex AS TINYINT) 
         							ELSE CAST(ASCII(UPPER(@hex)) - 55 AS TINYINT) 
		 						END)
    
	RETURN @code
END



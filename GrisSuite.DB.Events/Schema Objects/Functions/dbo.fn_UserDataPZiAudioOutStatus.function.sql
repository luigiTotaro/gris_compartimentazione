﻿-- =============================================
-- Author:		Luigi Ferrarini
-- Create date: 19 Jan 2011
-- Description:	Ritorna un VARCHAR
-- =============================================
CREATE FUNCTION [dbo].[fn_UserDataPZiAudioOutStatus]
(
	-- Add the parameters for the function here
	@data varchar(8000)
)
RETURNS varchar(64)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @digit_odd char,
	 		@digit_even char,
			@i int,
			@tmp varchar(64), 
			@result varchar(64) 

	SET @tmp = '';
	SET @result = '';
	SET @i = 0;
	WHILE @i < 2 -- Lunghezza del buffer dati supplementari usato
		BEGIN
			SET @digit_odd  = substring(@data,2*@i+1,1)
			SET @digit_even = substring(@data,2*@i+2,1)

			SET @tmp = @tmp + CASE 
								WHEN @digit_even = '0' THEN '  ---, ' 
								WHEN @digit_even = '1' THEN '   ON, ' 
								WHEN @digit_even = '2' THEN '   OK, ' 
								WHEN @digit_even = '3' THEN '  WAR, ' 
								WHEN @digit_even = '4' THEN '  ERR, ' 
								WHEN @digit_even = '5' THEN ' OPEN, ' 
								WHEN @digit_even = '6' THEN 'SHORT, ' 
								WHEN @digit_even = '7' THEN 'UNDEF, ' 
								WHEN @digit_even = 'A' THEN '0'
								WHEN @digit_even = 'B' THEN '*'
								WHEN @digit_even = 'C' THEN '#'
								WHEN @digit_even = '0' THEN ''
								ELSE '+'
							END

			SET @tmp = @tmp + CASE 
								WHEN @digit_odd = '0' THEN '  ---, ' 
								WHEN @digit_odd = '1' THEN '   ON, ' 
								WHEN @digit_odd = '2' THEN '   OK, ' 
								WHEN @digit_odd = '3' THEN '  WAR, ' 
								WHEN @digit_odd = '4' THEN '  ERR, ' 
								WHEN @digit_odd = '5' THEN ' OPEN, ' 
								WHEN @digit_odd = '6' THEN 'SHORT, ' 
								WHEN @digit_odd = '7' THEN 'UNDEF, ' 
								WHEN @digit_odd = 'A' THEN '0'
								WHEN @digit_odd = 'B' THEN '*'
								WHEN @digit_odd = 'C' THEN '#'
								WHEN @digit_odd = '0' THEN ''
								ELSE '+'
							END
			SET @i = @i+1
		END

	SET @result = @tmp 
		
-- Return the result of the function
	RETURN @result
END
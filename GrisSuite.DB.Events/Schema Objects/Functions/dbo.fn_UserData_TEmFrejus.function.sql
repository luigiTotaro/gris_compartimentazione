﻿-- =============================================
-- Author:		Luigi Ferrarini
-- Create date: 24 Jan 2011
-- Description:	Ritorna un VARCHAR
-- =============================================
CREATE FUNCTION [dbo].[fn_UserData_TEmFrejus]
(
	-- Add the parameters for the function here
	@data varchar(8000)
)
RETURNS varchar(128)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @digit_odd char,
	 		@digit_even char,
			@i int,
			@tmp varchar(16), 
			@result varchar(128) 

	SET @tmp = '';
	SET @result = '';
	SET @i = 0;
	WHILE @i < 8 -- Lunghezza del buffer dati supplementari usato
		BEGIN
			SET @digit_odd  = substring(@data,2*@i+1,1)
			SET @digit_even = substring(@data,2*@i+2,1)

			SET @tmp = @tmp + CASE 
								WHEN @digit_even LIKE '[1-9]' THEN @digit_even 
								WHEN @digit_even = 'A' THEN '0'
								WHEN @digit_even = 'B' THEN '*'
								WHEN @digit_even = 'C' THEN '#'
								WHEN @digit_even = '0' THEN ''
								ELSE '+'
							END

			SET @tmp = @tmp + CASE 
								WHEN @digit_odd LIKE '[1-9]' THEN @digit_odd 
								WHEN @digit_odd = 'A' THEN '0'
								WHEN @digit_odd = 'B' THEN '*'
								WHEN @digit_odd = 'C' THEN '#'
								WHEN @digit_odd = '0' THEN ''
								ELSE '+'
							END
			SET @i = @i+1
		END

	SET @result = CASE
					WHEN substring(@tmp,3,1)='0' THEN ' OK (ONLINE with PC)'
					WHEN substring(@tmp,3,1)='1' THEN ' WARNING (Link ISDN ON - OFFLINE with PC)'
					WHEN substring(@tmp,3,1)='2' THEN ' FAIL (link ISDN OFF)'
				END
 

	SET @result = @result + CASE 
								WHEN substring(@tmp,1,1) = '1' THEN ' - Side MST' 
								WHEN substring(@tmp,1,1) = '2' THEN ' - Side SLV'  
								ELSE '/' 
							END
		
-- Return the result of the function
	RETURN @result
END
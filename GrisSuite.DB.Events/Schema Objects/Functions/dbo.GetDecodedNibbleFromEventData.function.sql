﻿CREATE FUNCTION [dbo].[GetDecodedNibbleFromEventData] 
(
	@EventData VARBINARY(24),
	@FieldId INT
)
-- Decodifica 1 byte dallo stream dell'evento, in base alla posizione.
RETURNS TINYINT
AS
BEGIN
	DECLARE @code TINYINT;
	DECLARE @hex CHAR(1);
	
	SET @hex = SUBSTRING(@EventData, @FieldId, 1)

	SET @code = CONVERT(
					TINYINT, 
					CASE WHEN @hex LIKE '[0-9]' THEN CAST(@hex AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex)) - 55 AS TINYINT) 
		 			END)
    
	RETURN @code
END
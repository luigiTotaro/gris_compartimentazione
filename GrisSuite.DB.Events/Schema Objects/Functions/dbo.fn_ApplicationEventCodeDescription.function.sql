﻿-- =============================================
-- Author:		Luigi Ferrarini
-- Create date: 10 Dec 2010
-- Last update: 10 Dec 2010
-- Description:	Ritorna un varchar
-- =============================================

CREATE FUNCTION [dbo].[fn_ApplicationEventCodeDescription] (@EventCode TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @CodeDescription VARCHAR(MAX);
	SET @CodeDescription = NULL;

	SET @CodeDescription =  CASE
		WHEN @EventCode = 1     THEN 'Microtelefono abbassato'              
		WHEN @EventCode = 2     THEN 'Microtelefono sollevato'              
		WHEN @EventCode = 3     THEN 'Linea inclusa'                        
		WHEN @EventCode = 4     THEN 'Chiamata arrivata'                    
		WHEN @EventCode = 5     THEN 'Messa in attesa'                      
		WHEN @EventCode = 6     THEN 'Inviata chiamata '                    
		WHEN @EventCode = 7     THEN 'Tolta attesa'                         
		WHEN @EventCode = 8     THEN 'Inclusa linea in parallelo'           
		WHEN @EventCode = 9     THEN 'Transizione ON'                       
		WHEN @EventCode = 10    THEN 'Transizione OFF'                      
		WHEN @EventCode = 11    THEN 'Linea disinserita'                    
		WHEN @EventCode = 12    THEN 'Link ISDN presente'                   
		WHEN @EventCode = 13    THEN 'Link ISDN assente'                    
		WHEN @EventCode = 14    THEN 'Impianto occupato'                    
		WHEN @EventCode = 15    THEN 'Impianto pronto'                      
		WHEN @EventCode = 16    THEN 'Sistema attivato'                     
		WHEN @EventCode = 17    THEN 'Sistema disattivato'                  
		WHEN @EventCode = 18    THEN 'Modo notte attivo'                    
		WHEN @EventCode = 19    THEN 'Modo notte disattivato'               
		WHEN @EventCode = 20    THEN 'Diffusione sonora inclusa'            
		WHEN @EventCode = 21    THEN 'Linea ISDN alzata'                    
		WHEN @EventCode = 22    THEN 'Linea ISDN abbassata'                 
		WHEN @EventCode = 30    THEN 'Eccezione processore'                 
		WHEN @EventCode = 31    THEN 'Attivato ACK'                         
		WHEN @EventCode = 32    THEN 'Disattivato ACK'                      
		WHEN @EventCode = 33    THEN 'Uscita attiva'                        
		WHEN @EventCode = 34    THEN 'Uscita disattiva'                     
		WHEN @EventCode = 35    THEN 'Ingresso attivo'                      
		WHEN @EventCode = 36    THEN 'Ingresso disattivo'                   
		WHEN @EventCode = 37    THEN 'Chiamata emergenza'                   
		WHEN @EventCode = 38    THEN 'Posizione chiave OFF'                 
		WHEN @EventCode = 39    THEN 'Posizione chiave 1'                   
		WHEN @EventCode = 40    THEN 'Posizione chiave 2'                   
		WHEN @EventCode = 41    THEN 'Telefono SOS avaria'                  
		WHEN @EventCode = 42    THEN 'Telefono SOS OK'                      
		WHEN @EventCode = 43    THEN 'Comando attivazione ON'               
		WHEN @EventCode = 44    THEN 'Comando attivazione OFF'              
		WHEN @EventCode = 45    THEN 'Segnalazione stato sistema'
		ELSE 'NON CODIFICATO'
	END
	RETURN @CodeDescription
END
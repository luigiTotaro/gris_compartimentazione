﻿CREATE FUNCTION [dbo].[GetDecodedWordFromEventData] 
(
	@EventData VARBINARY(24),
	@FieldId INT
)
-- Decodifica 4 bytes dallo stream dell'evento, in base alla posizione.
RETURNS INT
AS
BEGIN
	DECLARE @code1 TINYINT;
	DECLARE @code2 TINYINT;
	DECLARE @code3 TINYINT;
	DECLARE @code4 TINYINT;
	DECLARE @hex1 CHAR(1);
	DECLARE @hex2 CHAR(1);
	DECLARE @hex3 CHAR(1);
	DECLARE @hex4 CHAR(1);
	
	SET @hex1 = SUBSTRING(@EventData, 4 * @FieldId - 3, 1);

	SET @code1 = CONVERT(
					TINYINT, 
					CASE WHEN @hex1 LIKE '[0-9]' THEN CAST(@hex1 AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex1)) - 55 AS TINYINT) 
		 			END);

	SET @hex2 = SUBSTRING(@EventData, 4 * @FieldId - 2, 1);

	SET @code2 = CONVERT(
					TINYINT, 
					CASE WHEN @hex2 LIKE '[0-9]' THEN CAST(@hex2 AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex2)) - 55 AS TINYINT) 
		 			END);
		 						
	SET @hex3 = SUBSTRING(@EventData, 4 * @FieldId - 1, 1);

	SET @code3 = CONVERT(
					TINYINT, 
					CASE WHEN @hex3 LIKE '[0-9]' THEN CAST(@hex3 AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex3)) - 55 AS TINYINT) 
		 			END);
		 			
	SET @hex4 = SUBSTRING(@EventData, 4 * @FieldId, 1);

	SET @code4 = CONVERT(
					TINYINT, 
					CASE WHEN @hex4 LIKE '[0-9]' THEN CAST(@hex4 AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex4)) - 55 AS TINYINT) 
		 			END);		 				 								 						
    
	RETURN @code1 * 4096 + @code2 * 256 + @code3 * 16 + @code4
END
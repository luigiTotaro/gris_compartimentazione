﻿CREATE FUNCTION [dbo].[GetPZInnerDataFromEvent]
(
	@EventData VARBINARY(MAX),
	@PZFieldName VARCHAR(3)
)
RETURNS CHAR
AS
BEGIN
	DECLARE @Result CHAR;
	SET @Result = '';

	IF (DATALENGTH(@EventData) = 24)
	BEGIN
		DECLARE @digit CHAR;
		DECLARE @i INT;

		-- Estrae il campo Data dallo stream dell'evento di categoria 2: array di 8 bytes, codificati come byte doppi in base dati, in ordine invertito
		DECLARE @DataEventCategory2 VARBINARY(16);
		SET @DataEventCategory2 = SUBSTRING(@EventData, 7, 16);

		DECLARE @UnwrappedData VARCHAR(16);
		SET @UnwrappedData = '';

		SET @i = 0;

		WHILE (@i < 8)
		BEGIN
			SET @digit = SUBSTRING(@DataEventCategory2, 2 * @i + 2, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END

			SET @digit  = SUBSTRING(@DataEventCategory2, 2 * @i + 1, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END
			SET @i = @i + 1;
		END

		IF (@PZFieldName LIKE 'A1')
		BEGIN
			-- Amplificatore 1
			SET @Result = SUBSTRING(@UnwrappedData, 1, 1);
		END
		ELSE IF (@PZFieldName LIKE 'A2')
		BEGIN
			-- Amplificatore 2
			SET @Result = SUBSTRING(@UnwrappedData, 2, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z1')
		BEGIN
			-- Zona 1
			SET @Result = SUBSTRING(@UnwrappedData, 5, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z2')
		BEGIN
			-- Zona 2
			SET @Result = SUBSTRING(@UnwrappedData, 6, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z3')
		BEGIN
			-- Zona 3
			SET @Result = SUBSTRING(@UnwrappedData, 7, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z4')
		BEGIN
			-- Zona 4
			SET @Result = SUBSTRING(@UnwrappedData, 8, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z5')
		BEGIN
			-- Zona 5
			SET @Result = SUBSTRING(@UnwrappedData, 9, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z6')
		BEGIN
			-- Zona 6
			SET @Result = SUBSTRING(@UnwrappedData, 10, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z7')
		BEGIN
			-- Zona 7
			SET @Result = SUBSTRING(@UnwrappedData, 11, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z8')
		BEGIN
			-- Zona 8
			SET @Result = SUBSTRING(@UnwrappedData, 12, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z9')
		BEGIN
			-- Zona 9
			SET @Result = SUBSTRING(@UnwrappedData, 13, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z10')
		BEGIN
			-- Zona 10
			SET @Result = SUBSTRING(@UnwrappedData, 14, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z11')
		BEGIN
			-- Zona 11
			SET @Result = SUBSTRING(@UnwrappedData, 15, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z12')
		BEGIN
			-- Zona 12
			SET @Result = SUBSTRING(@UnwrappedData, 16, 1);
		END
	END

	RETURN @Result;
END



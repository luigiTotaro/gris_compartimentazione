﻿CREATE FUNCTION dbo.ExistsEvent153RelatedTo150InPZI (@DevID BIGINT, @CreatedOn DATETIME)
RETURNS TINYINT
AS
BEGIN
	DECLARE @ExistsEvent153RelatedTo150InPZI TINYINT;
	SET @ExistsEvent153RelatedTo150InPZI = 0;
	
	IF EXISTS (
		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), dbo.GetDecodedNibbleFromEventData(events.EventData, 6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 6) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		150 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 6) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 5) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		151 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 5) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 8), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 8) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		152 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 8) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 7), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 7) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		153 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 7) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 10), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 10) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		154 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 10) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 9), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 9) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		155 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 9) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 12), 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 12) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		156 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 12) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 11), 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 11) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		157 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 11) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 14), 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 14) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		158 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 14) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 13), 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 13) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		159 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 13) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 16), 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 16) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		160 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 16) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 15), 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 15) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		161 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 15) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 18), 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 18) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		162 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 18) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 17), 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 17) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		163 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 17) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 20), 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 20) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		164 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 20) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 19) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		165 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 19) IN (2, 3, 4, 5, 6, 7))
	)
	BEGIN
		SET @ExistsEvent153RelatedTo150InPZI = 1;
	END
	
	RETURN ISNULL(@ExistsEvent153RelatedTo150InPZI, 0);
END
﻿
CREATE VIEW [dbo].[current_events_devices]
AS

WITH lastEventDevices AS
(
	SELECT DevID, MAX(EventDevId) AS EventDevId
	FROM dbo.events_devices
	GROUP BY DevID
)
SELECT ED.EventDevId, CreationDate, ED.DevID, NodID, SrvID, Name, Type, SN, Addr, ServerName, ServerHost, ServerFullHostName, ServerIP, ServerVersion, ServerMAC,  NodeName
FROM dbo.events_devices ED
	INNER JOIN lastEventDevices LED ON ED.EventDevId = LED.EventDevId
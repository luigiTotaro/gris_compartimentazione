CREATE PROCEDURE [dbo].[tf_InsEvents]
@EventID UNIQUEIDENTIFIER, @DevID BIGINT, @EventData VARBINARY (MAX), @Created DATETIME, @Requested DATETIME, @ToBeDeleted BIT, @EventCategory TINYINT
AS
IF EXISTS(SELECT [EventID] FROM  [events] WHERE ([EventID] = @EventID) )
	BEGIN
		UPDATE [events] 
		SET [EventID] = @EventID, [DevID] = @DevID, [EventData] = @EventData, [Created] = @Created, [Requested] = @Requested, [ToBeDeleted] = @ToBeDeleted, [EventCategory] = @EventCategory 
		WHERE ([EventID] = @EventID);
	END
	ELSE
	BEGIN
		INSERT INTO [events] ([EventID], [DevID], [EventData], [Created], [Requested], [ToBeDeleted], [EventCategory]) VALUES (@EventID, @DevID, @EventData, @Created, @Requested, @ToBeDeleted, @EventCategory);
	END

RETURN @@ROWCOUNT;
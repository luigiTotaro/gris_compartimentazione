﻿CREATE PROCEDURE [dbo].[gris_GetDecodedEventDataByCreationTime]
@EventID UNIQUEIDENTIFIER,
@DeviceTypeID VARCHAR(16) = 'TT10210V3'
AS
SET NOCOUNT ON;

DECLARE @DevID BIGINT 
DECLARE @CreatedOn DATETIME

SELECT @DevID = DevID, @CreatedOn = Created
FROM events
WHERE (EventID = @EventID)

IF (@DeviceTypeID LIKE 'TT10210V3')
BEGIN
	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ATTIVATA USCITA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	40 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 33)
	AND (@CreatedOn = Created)

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'DISATTIVATA USCITA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	41 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 34)
	AND (@CreatedOn = Created)

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ATTIVATO INGRESSO' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	20 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 35)
	AND (@CreatedOn = Created)

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'DISATTIVATO INGRESSO' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	21 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 36)
	AND (@CreatedOn = Created)

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ATTIVAZIONE SONORA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
	dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	30 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
	AND (@CreatedOn = Created)

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'FINE ATTIVAZIONE SONORA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	31 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
	AND (@CreatedOn = Created)

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '1') THEN 'ESITO TEST - Amplificatore 1:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '2') THEN 'ESITO TEST - Amplificatore 1:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '1') THEN 'ESITO TEST - Amplificatore 2:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '2') THEN 'ESITO TEST - Amplificatore 2:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '1') THEN 'ESITO TEST - Zona 1:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '2') THEN 'ESITO TEST - Zona 1:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '1') THEN 'ESITO TEST - Zona 2:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '2') THEN 'ESITO TEST - Zona 2:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '1') THEN 'ESITO TEST - Zona 3:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '2') THEN 'ESITO TEST - Zona 3:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '1') THEN 'ESITO TEST - Zona 4:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '2') THEN 'ESITO TEST - Zona 4:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '1') THEN 'ESITO TEST - Zona 5:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '2') THEN 'ESITO TEST - Zona 5:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '1') THEN 'ESITO TEST - Zona 6:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '2') THEN 'ESITO TEST - Zona 6:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '1') THEN 'ESITO TEST - Zona 7:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '2') THEN 'ESITO TEST - Zona 7:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '1') THEN 'ESITO TEST - Zona 8:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '2') THEN 'ESITO TEST - Zona 8:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '1') THEN 'ESITO TEST - Zona 9:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '2') THEN 'ESITO TEST - Zona 9:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '1') THEN 'ESITO TEST - Zona 10:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '2') THEN 'ESITO TEST - Zona 10:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '1') THEN 'ESITO TEST - Zona 11:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '2') THEN 'ESITO TEST - Zona 11:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') IN ('1' /* OK */, '2' /* KO */))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	CASE
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '1') THEN 'ESITO TEST - Zona 12:OK'
		WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '2') THEN 'ESITO TEST - Zona 12:KO'
	END AS EventDescription,
	dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
	10 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND (@CreatedOn = Created)
	AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') IN ('1' /* OK */, '2' /* KO */))

	ORDER BY events.Created DESC, CustomOrder
END
ELSE IF (@DeviceTypeID LIKE 'PEAVDOM')
BEGIN
	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	0 AS EventCode,
	dbo.GetPraseDomDescriptionFromEvent(EventData) AS EventDescription,
	CONVERT(VARCHAR(MAX), EventData) AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	0 AS DeviceCode,
	0 AS CustomOrder
	FROM events
	WHERE (events.DevID = @DevID)
	AND (events.EventCategory = 100)
	AND (@CreatedOn = Created)

	ORDER BY events.Created DESC, CustomOrder
END
IF (@DeviceTypeID LIKE 'TT10220')
BEGIN
	-- Tipo dell'evento passato, può essere ATTIVAZIONE SONORA 150, FINE ATTIVAZIONE SONORA 151, ESITO TEST PERIODICO 152
	DECLARE @CurrentEventData TINYINT;
	SET @CurrentEventData = ISNULL((SELECT dbo.GetDecodedDataFromEventData(events.EventData, 1) FROM events WHERE (EventID = @EventID)), 0);
	
	-- Raggruppiamo forzatamente per tipo master, perché nel caso del PZi, ci sono eventi discordanti nello stesso secondo (inizio e fine attivazione sonora, test e attivazione...)
	IF (@CurrentEventData = 150) -- ATTIVAZIONE SONORA
	BEGIN
		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'ATTIVATA USCITA' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		400 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 104)
		AND (@CreatedOn = Created)

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'ATTIVATO INGRESSO' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		200 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 102)
		AND (@CreatedOn = Created)
		
		UNION ALL
		
		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'ATTIVATO INGRESSO VIRTUALE' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		210 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 100)
		AND (@CreatedOn = Created)
		
		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'ATTIVAZIONE SONORA' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		dbo.GetPZiInnerDataDescriptionFromEvent(
			dbo.GetDecodedNibbleFromEventData(events.EventData, 6)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 5)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 8)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 7)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 10)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 9)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 12)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 11)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 14)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 13)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 16)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 15)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 18)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 17)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 20)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)	
		) AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		300 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 150)
		AND (@CreatedOn = Created)	

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), dbo.GetDecodedNibbleFromEventData(events.EventData, 6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 6) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		150 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 6) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 5) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		151 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 5) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 8), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 8) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		152 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 8) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 7), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 7) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		153 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 7) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 10), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 10) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		154 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 10) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 9), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 9) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		155 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 9) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 12), 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 12) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		156 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 12) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 11), 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 11) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		157 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 11) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 14), 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 14) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		158 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 14) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 13), 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 13) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		159 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 13) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 16), 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 16) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		160 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 16) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 15), 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 15) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		161 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 15) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 18), 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 18) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		162 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 18) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 17), 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 17) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		163 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 17) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 20), 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 20) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		164 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 20) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 19) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		165 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 153)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 19) IN (2, 3, 4, 5, 6, 7))
		
		ORDER BY events.Created DESC, CustomOrder, InOutID
	END
	ELSE IF (@CurrentEventData = 151) -- FINE ATTIVAZIONE SONORA
	BEGIN
		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'DISATTIVATA USCITA' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		410 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 105)
		AND (@CreatedOn = Created)

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'DISATTIVATO INGRESSO' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		220 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 103)
		AND (@CreatedOn = Created)

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'DISATTIVATO INGRESSO VIRTUALE' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		230 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 101)
		AND (@CreatedOn = Created)	

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		'FINE ATTIVAZIONE SONORA' AS EventDescription,
		NULL AS EventDescriptionValue,
		dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
		NULL AS MicroID,
		dbo.GetPZiInnerDataDescriptionFromEvent(
			dbo.GetDecodedNibbleFromEventData(events.EventData, 6)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 5)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 8)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 7)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 10)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 9)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 12)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 11)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 14)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 13)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 16)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 15)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 18)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 17)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 20)
			, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)	
		) AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		310 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 151)
		AND (@CreatedOn = Created)
		
		ORDER BY events.Created DESC, CustomOrder, InOutID
	END
	ELSE IF (@CurrentEventData = 152) -- ESITO TEST PERIODICO
	BEGIN
		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), dbo.GetDecodedNibbleFromEventData(events.EventData, 6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 6) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		100 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 6) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 5) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		101 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 5) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 8), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 8) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		102 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 8) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 7), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 7) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		103 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 7) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 10), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 10) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		104 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 10) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 9), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 9) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		105 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 9) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 12), 0, 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 12) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		106 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 12) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 11), 0, 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 11) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		107 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 11) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 14), 0, 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 14) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		108 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 14) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 13), 0, 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 13) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		109 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 13) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 16), 0, 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 16) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		110 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 16) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 15), 0, 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 15) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		111 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 15) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 18), 0, 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 18) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		112 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 18) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 17), 0, 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 17) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		113 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 17) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 20), 0) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 20) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		114 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 20) IN (2, 3, 4, 5, 6, 7))

		UNION ALL

		SELECT
		events.EventID,
		events.Created,
		events.EventCategory,
		dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
		dbo.GetPZiInnerDataTestDescriptionFromEvent(dbo.GetDecodedDataFromEventData(events.EventData, 1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)) AS EventDescription,
		dbo.GetDecodedNibbleFromEventData(events.EventData, 19) AS EventDescriptionValue,
		NULL AS InOutID,
		NULL AS MicroID,
		NULL AS SubEventDescription,
		NULL AS FunctionCode,
		NULL AS DeviceAddress,
		NULL AS FunctionDescription,
		NULL AS DeviceCode,
		115 AS CustomOrder
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
		AND (@CreatedOn = Created)
		AND (dbo.GetDecodedNibbleFromEventData(events.EventData, 19) IN (2, 3, 4, 5, 6, 7))

		ORDER BY events.Created DESC, CustomOrder, InOutID
	END
END



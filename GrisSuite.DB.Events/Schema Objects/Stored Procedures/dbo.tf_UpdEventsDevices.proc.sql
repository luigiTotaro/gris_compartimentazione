﻿
CREATE PROCEDURE [dbo].[tf_UpdEventsDevices]
AS
BEGIN

	BEGIN TRANSACTION;

	BEGIN TRY
		PRINT 'Start';

		-- Copia dal DB TelefinCentralPRO le informazioni sulle device non presenti o con valori cambiati rispetto a Events
		WITH devices AS
		(
		SELECT DISTINCT D.DevID
			, D.NodID
			, D.SrvID
			, D.Name
			, D.Type
			, D.SN
			, D.Addr
			, S.Name AS ServerName
			, S.Host as ServerHost
			, S.FullHostName AS ServerFullHostName
			, S.IP as ServerIP
			, S.ServerVersion
			, S.MAC ServerMAC
			, N.Name AS NodeName
		FROM [events] E
		 INNER JOIN	[TelefinCentralPRO].dbo.devices D ON E.DevID = D.DevID
		 LEFT JOIN [TelefinCentralPRO].dbo.nodes N ON N.NodID = D.NodID
		 LEFT JOIN [TelefinCentralPRO].dbo.servers S ON D.SrvID = S.SrvID
		WHERE E.EventDevId IS NULL 
		)
		INSERT INTO events_devices 
		(	  DevID
			, NodID
			, SrvID
			, Name
			, Type
			, SN
			, Addr
			, ServerName
			, ServerHost
			, ServerFullHostName
			, ServerIP
			, ServerVersion
			, ServerMAC
			, NodeName
		)
		SELECT D.DevID
			, D.NodID
			, D.SrvID
			, D.Name
			, D.Type
			, D.SN
			, D.Addr
			, D.ServerName
			, D.ServerHost
			, D.ServerFullHostName
			, D.ServerIP
			, D.ServerVersion
			, D.ServerMAC
			, D.NodeName
		FROM devices D 
		LEFT JOIN current_events_devices CED ON  D.DevID = CED.DevID
		WHERE CED.DevID IS NULL 
			OR D.NodID <> CED.NodID 
			OR D.SrvID <> CED.SrvID
			OR D.Name <> CED.Name
			OR D.Type <> CED.Type
			OR D.SN <> CED.SN
			OR D.Addr <> CED.Addr
			OR D.ServerName <> CED.ServerName
			OR D.ServerHost <> CED.ServerHost
			OR D.ServerFullHostName <> CED.ServerFullHostName
			OR D.ServerIP <> CED.ServerIP
			OR D.ServerVersion <> CED.ServerVersion
			OR D.ServerMAC <> CED.ServerMAC
			OR D.NodeName <> CED.NodeName;

		-- aggiorna il legame con la tabella events_devices
		UPDATE events 
		SET EventDevId = CED.EventDevId
		FROM events E INNER JOIN current_events_devices CED ON  E.DevID = CED.DevID
		WHERE E.EventDevId IS NULL

		-- se dopo l'operazione precedente esistono eventi senza DevID non è possibile stabilire il legame 
		-- vengono quindi messi a 0 = NoDevice.
		UPDATE events 
		SET EventDevId = 0
		WHERE EventDevId IS NULL
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
		
		DECLARE @msg VARCHAR(MAX)
		DECLARE @sev INT
		DECLARE @state INT
		SET @msg = ERROR_MESSAGE()
		SET @sev = ERROR_SEVERITY()
		SET @state = ERROR_STATE()
		RAISERROR(@msg,@sev,@state)
	END CATCH;

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;
			
END
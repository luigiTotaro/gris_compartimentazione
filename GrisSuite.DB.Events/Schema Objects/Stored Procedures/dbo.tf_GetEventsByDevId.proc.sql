﻿CREATE PROCEDURE [dbo].[tf_GetEventsByDevId]
	@DevID as bigint
AS
	SELECT EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory
	FROM events
	WHERE DevID = @DevID
RETURN @@ROWCOUNT;
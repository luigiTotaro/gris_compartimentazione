﻿CREATE PROCEDURE [dbo].[tf_UpdEvents]
	@Original_EventID uniqueidentifier,
	@DevID bigint,
	@EventData varbinary(max), 
	@Created datetime, 
	@Requested datetime,
	@ToBeDeleted bit, 
	@EventCategory tinyint
AS
	UPDATE [events] SET [DevID] = @DevID, [EventData] = @EventData, [Created] = @Created, [Requested] = @Requested, [ToBeDeleted] = @ToBeDeleted, [EventCategory] = @EventCategory 
	WHERE ([EventID] = @Original_EventID);

RETURN @@ROWCOUNT;
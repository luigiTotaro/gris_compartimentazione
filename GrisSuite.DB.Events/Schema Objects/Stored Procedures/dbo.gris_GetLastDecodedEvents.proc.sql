﻿CREATE PROCEDURE [dbo].[gris_GetLastDecodedEvents]
@DevID BIGINT=NULL, @EventCodeFilter43 BIT, @EventCodeFilter44 BIT, @EventCodeFilter45 BIT, @DeviceTypeID VARCHAR(16) = 'TT10210V3'
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (@DeviceTypeID LIKE 'TT10210V3')
	BEGIN
		SELECT MAX(Created) AS LastCreated
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
		AND ((@EventCodeFilter43 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43) OR (@EventCodeFilter44 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44) OR (@EventCodeFilter45 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45))
	END
	ELSE IF (@DeviceTypeID LIKE 'PEAVDOM')
	BEGIN
		SELECT MAX(Created) AS LastCreated
		FROM events
		WHERE (events.DevID = @DevID)
		AND (events.EventCategory = 100)
	END
	ELSE IF (@DeviceTypeID LIKE 'TT10220')
	BEGIN
		SELECT MAX(Created) AS LastCreated
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
		AND ((@EventCodeFilter43 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 150) OR (@EventCodeFilter44 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 151) OR (@EventCodeFilter45 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152))
	END
	ELSE IF (@DeviceTypeID LIKE 'FD90000%')
	BEGIN
		SELECT MAX(Created) AS LastCreated
		FROM events
		WHERE (events.DevID = @DevID)
		AND ((events.EventCategory = 3) AND (DATALENGTH(events.EventData) = 12))			
	END
END



﻿CREATE PROCEDURE [dbo].[gris_GetDecodedEventData]
@DevID BIGINT=NULL, @CreatedFrom DATETIME=NULL, @CreatedTo DATETIME=NULL, @EventCodeFilter43 BIT, @EventCodeFilter44 BIT, @EventCodeFilter45 BIT, @DeviceTypeID VARCHAR(16) = 'TT10210V3'
AS
SET NOCOUNT ON;

IF (@DeviceTypeID LIKE 'TT10210V3')
BEGIN
	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ATTIVAZIONE SONORA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
	dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
	FROM events
	WHERE (@EventCodeFilter43 = 1)
	AND (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'FINE ATTIVAZIONE SONORA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
	FROM events
	WHERE (@EventCodeFilter44 = 1)
	AND (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ESITO TEST' AS EventDescription,
	dbo.GetPZInnerDataTestDescriptionFromEvent(events.EventData) AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
	FROM events
	WHERE (@EventCodeFilter45 = 1)
	AND (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	ORDER BY events.Created DESC, EventDescription
END
ELSE IF (@DeviceTypeID LIKE 'PEAVDOM')
BEGIN
	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	0 AS EventCode,
	dbo.GetPraseDomDescriptionFromEvent(EventData) AS EventDescription,
	CONVERT(VARCHAR(MAX), EventData) AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	0 AS DeviceCode
	FROM events
	WHERE (events.DevID = @DevID)
	AND (events.EventCategory = 100)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	ORDER BY events.Created DESC, EventDescription
END
ELSE IF (@DeviceTypeID LIKE 'TT10220')
BEGIN
	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ATTIVAZIONE SONORA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	NULL AS MicroID,
		dbo.GetPZiInnerDataDescriptionFromEvent(
		dbo.GetDecodedNibbleFromEventData(events.EventData, 6)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 5)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 8)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 7)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 10)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 9)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 12)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 11)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 14)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 13)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 16)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 15)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 18)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 17)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 20)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)
	) AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	dbo.ExistsEvent153RelatedTo150InPZI (events.DevID, events.Created) AS FunctionDescription,
	NULL AS DeviceCode
	FROM events
	WHERE (@EventCodeFilter43 = 1)
	AND (events.DevID = @DevID)
	AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 150)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'FINE ATTIVAZIONE SONORA' AS EventDescription,
	NULL AS EventDescriptionValue,
	dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
	NULL AS MicroID,
	dbo.GetPZiInnerDataDescriptionFromEvent(
		dbo.GetDecodedNibbleFromEventData(events.EventData, 6)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 5)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 8)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 7)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 10)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 9)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 12)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 11)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 14)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 13)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 16)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 15)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 18)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 17)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 20)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)
	) AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	NULL AS DeviceCode
	FROM events
	WHERE (@EventCodeFilter44 = 1)
	AND (events.DevID = @DevID)
	AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 151)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	UNION ALL

	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
	'ESITO TEST PERIODICO' AS EventDescription,
	dbo.GetPZiInnerDataTestDescriptionFromEvent(
		0
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 6)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 5)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 8)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 7)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 10)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 9)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 12)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 11)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 14)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 13)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 16)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 15)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 18)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 17)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 20)
		, dbo.GetDecodedNibbleFromEventData(events.EventData, 19)
	) AS EventDescriptionValue,
	NULL AS InOutID,
	NULL AS MicroID,
	NULL AS SubEventDescription,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	NULL AS DeviceCode
	FROM events
	WHERE (@EventCodeFilter45 = 1)
	AND (events.DevID = @DevID)
	AND ((events.EventCategory = 4 /* Eventi */) AND (DATALENGTH(events.EventData) = 20))
	AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 152)
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

	ORDER BY events.Created DESC, EventDescription
END
ELSE IF (@DeviceTypeID LIKE 'FD90000%')
BEGIN
	SELECT
	events.EventID,
	events.Created,
	events.EventCategory,
	0 AS EventCode /* dbo.GetDecodedWordFromEventData(events.EventData, 1) */,
	dbo.GetFDSEventDescription(dbo.GetDecodedWordFromEventData(events.EventData, 1), 0 /* @DescriptionType */) AS EventDescription /* Evento */,
	dbo.GetFDSModuleDescription(dbo.GetDecodedWordFromEventData(events.EventData, 2), dbo.GetDecodedDataFromEventData(events.EventData, 5), 0 /* @DescriptionType */) AS EventDescriptionValue /* Posizione */,
	dbo.GetDecodedDataFromEventData(events.EventData, 5) AS InOutID /* Position */,
	NULL AS MicroID,
	dbo.GetFDSCardDescription(dbo.GetDecodedDataFromEventData(events.EventData, 6), 0 /* @DescriptionType */) AS SubEventDescription /* TipoScheda */,
	NULL AS FunctionCode,
	NULL AS DeviceAddress,
	NULL AS FunctionDescription,
	0 AS DeviceCode
	--dbo.GetDecodedWordFromEventData(events.EventData, 2) AS Module,
	--dbo.GetDecodedNibbleFromEventData(events.EventData, 5) AS RemoteDevice,
	--dbo.GetDecodedDataFromEventData(events.EventData, 6) AS CardType
	FROM events
	WHERE
	(events.DevID = @DevID)
	AND ((events.EventCategory = 3) AND (DATALENGTH(events.EventData) = 12))
	AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
	AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
	
	ORDER BY events.Created
END
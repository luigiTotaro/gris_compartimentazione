﻿CREATE PROCEDURE [dbo].[tf_UpdArchiveEvents]
@EventsKeepDays INT = 10, @EventsArchiveL1KeepDays INT = 365
AS
DECLARE @limitEvents DATETIME
DECLARE @limitEventsArchiveL1 DATETIME

SET @limitEvents = DATEADD(d,-@EventsKeepDays,GETDATE())
SET @limitEventsArchiveL1 = DATEADD(d,-@EventsArchiveL1KeepDays,GETDATE())
BEGIN TRANSACTION;

BEGIN TRY
	-- sposta Events -> events_archiveL1
	INSERT INTO events_archiveL1
						  (EventID, DevID, EventData, EventCategory, Created, Requested, ToBeDeleted, Centralized, EventDevId)
	SELECT EventID, DevID, EventData, EventCategory, Created, Requested, ToBeDeleted, Centralized, EventDevId
	FROM [events]
	WHERE (Centralized < @limitEvents)

	DELETE FROM [events]
	WHERE (Centralized < @limitEvents)

	-- sposta events_archiveL1 -> events_archiveL2
	INSERT INTO TelefinEventsArchive.dbo.events_archiveL2
						  (EventID, DevID, EventData, EventCategory, Created, Requested, ToBeDeleted, Centralized, EventDevId)
	SELECT EventID, DevID, EventData, EventCategory, Created, Requested, ToBeDeleted, Centralized, EventDevId
	FROM [events_archiveL1]
	WHERE (Centralized < @limitEventsArchiveL1)
	
	-- copia in TelefinEventsArchive i device necessari
	INSERT INTO TelefinEventsArchive.dbo.events_devices
						  (EventDevId, NodeName, ServerMAC, ServerVersion, ServerIP, ServerFullHostName, ServerHost, ServerName, Addr, SN, Type, Name, SrvID, NodID, DevID, 
						  CreationDate)
	SELECT DISTINCT ED.EventDevId, ED.NodeName, ED.ServerMAC, ED.ServerVersion, ED.ServerIP, ED.ServerFullHostName, ED.ServerHost, ED.ServerName, ED.Addr, ED.SN, ED.Type, 
						  ED.Name, ED.SrvID, ED.NodID, ED.DevID, ED.CreationDate
	FROM events_archiveL1 AS AL1 
			INNER JOIN events_devices AS ED ON AL1.EventDevId = ED.EventDevId 
			LEFT OUTER JOIN  TelefinEventsArchive.dbo.events_devices AS EDA ON ED.EventDevId = EDA.EventDevId
	WHERE     (AL1.Centralized < @limitEventsArchiveL1) AND (EDA.EventDevId IS NULL)

	DELETE FROM [events_archiveL1]
	WHERE (Centralized < @limitEventsArchiveL1)
	
	-- Elimina i devices non più utilizzati
	DELETE FROM events_devices
	FROM events_devices AS ED 
		LEFT OUTER JOIN events_archiveL1 AS EA1 ON ED.EventDevId = EA1.EventDevId 
		LEFT OUTER JOIN events AS E ON ED.EventDevId = E.EventDevId
	WHERE (E.EventDevId IS NULL) AND (EA1.EventDevId IS NULL)
	
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
	
	DECLARE @msg VARCHAR(MAX)
	DECLARE @sev INT
	DECLARE @state INT
	SET @msg = ERROR_MESSAGE()
	SET @sev = ERROR_SEVERITY()
	SET @state = ERROR_STATE()
	RAISERROR(@msg,@sev,@state)
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
    
RETURN

-- Script di update del database TelefinEvents alla versione 1.0.0.1 a partire dalla versione 1.0.0.0

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[GetPZInnerDataDescriptionFromEvent]'
GO
ALTER FUNCTION [dbo].[GetPZInnerDataDescriptionFromEvent]
(
	@EventData VARBINARY(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = NULL;

	IF (DATALENGTH(@EventData) = 24)
	BEGIN
		DECLARE @digit CHAR;
		DECLARE @i INT;

		-- Estrae il campo Data dallo stream dell'evento di categoria 2: array di 8 bytes, codificati come byte doppi in base dati, in ordine invertito
		DECLARE @DataEventCategory2 VARBINARY(16);
		SET @DataEventCategory2 = SUBSTRING(@EventData, 7, 16);

		DECLARE @UnwrappedData VARCHAR(16);
		SET @UnwrappedData = '';

		SET @i = 0;

		WHILE (@i < 8)
		BEGIN
			SET @digit = SUBSTRING(@DataEventCategory2, 2 * @i + 2, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END

			SET @digit  = SUBSTRING(@DataEventCategory2, 2 * @i + 1, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END
			SET @i = @i + 1;
		END

		DECLARE @PartialValue CHAR;
		DECLARE @Separator VARCHAR(2);
		SET @Separator = ', ';

		-- Amplificatore 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 1, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = 'Amplificatore 1';
		END

		-- Amplificatore 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 2, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 2';
		END

		-- Zona 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 5, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 1';
		END

		-- Zona 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 6, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 2';
		END

		-- Zona 3
		SET @PartialValue = SUBSTRING(@UnwrappedData, 7, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 3';
		END

		-- Zona 4
		SET @PartialValue = SUBSTRING(@UnwrappedData, 8, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 4';
		END

		-- Zona 5
		SET @PartialValue = SUBSTRING(@UnwrappedData, 9, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 5';
		END

		-- Zona 6
		SET @PartialValue = SUBSTRING(@UnwrappedData, 10, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 6';
		END

		-- Zona 7
		SET @PartialValue = SUBSTRING(@UnwrappedData, 11, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 7';
		END

		-- Zona 8
		SET @PartialValue = SUBSTRING(@UnwrappedData, 12, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 8';
		END

		-- Zona 9
		SET @PartialValue = SUBSTRING(@UnwrappedData, 13, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 9';
		END

		-- Zona 10
		SET @PartialValue = SUBSTRING(@UnwrappedData, 14, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 10';
		END

		-- Zona 11
		SET @PartialValue = SUBSTRING(@UnwrappedData, 15, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 11';
		END

		-- Zona 12
		SET @PartialValue = SUBSTRING(@UnwrappedData, 16, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 12';
		END
	END

	RETURN ISNULL(@Result, '');
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetPZInnerDataTestDescriptionFromEvent]'
GO
CREATE FUNCTION [dbo].[GetPZInnerDataTestDescriptionFromEvent]
(
	@EventData VARBINARY(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = NULL;

	IF (DATALENGTH(@EventData) = 24)
	BEGIN
		DECLARE @digit CHAR;
		DECLARE @i INT;

		-- Estrae il campo Data dallo stream dell'evento di categoria 2: array di 8 bytes, codificati come byte doppi in base dati, in ordine invertito
		DECLARE @DataEventCategory2 VARBINARY(16);
		SET @DataEventCategory2 = SUBSTRING(@EventData, 7, 16);

		DECLARE @UnwrappedData VARCHAR(16);
		SET @UnwrappedData = '';

		SET @i = 0;

		WHILE (@i < 8)
		BEGIN
			SET @digit = SUBSTRING(@DataEventCategory2, 2 * @i + 2, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE
														WHEN @digit LIKE '[0-9]' THEN @digit
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END

			SET @digit  = SUBSTRING(@DataEventCategory2, 2 * @i + 1, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE
														WHEN @digit LIKE '[0-9]' THEN @digit
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END
			SET @i = @i + 1;
		END

		DECLARE @PartialValue CHAR;
		DECLARE @Separator VARCHAR(2);
		SET @Separator = ', ';

		-- Amplificatore 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 1, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = 'Amplificatore 1:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = 'Amplificatore 1:KO';
		END

		-- Amplificatore 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 2, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 2:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 2:KO';
		END

		-- Zona 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 5, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 1:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 1:KO';
		END

		-- Zona 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 6, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 2:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 2:KO';
		END

		-- Zona 3
		SET @PartialValue = SUBSTRING(@UnwrappedData, 7, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 3:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 3:KO';
		END

		-- Zona 4
		SET @PartialValue = SUBSTRING(@UnwrappedData, 8, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 4:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 4:KO';
		END

		-- Zona 5
		SET @PartialValue = SUBSTRING(@UnwrappedData, 9, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 5:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 5:KO';
		END

		-- Zona 6
		SET @PartialValue = SUBSTRING(@UnwrappedData, 10, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 6:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 6:KO';
		END

		-- Zona 7
		SET @PartialValue = SUBSTRING(@UnwrappedData, 11, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 7:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 7:KO';
		END

		-- Zona 8
		SET @PartialValue = SUBSTRING(@UnwrappedData, 12, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 8:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 8:KO';
		END

		-- Zona 9
		SET @PartialValue = SUBSTRING(@UnwrappedData, 13, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 9:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 9:KO';
		END

		-- Zona 10
		SET @PartialValue = SUBSTRING(@UnwrappedData, 14, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 10:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 10:KO';
		END

		-- Zona 11
		SET @PartialValue = SUBSTRING(@UnwrappedData, 15, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 11:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 11:KO';
		END

		-- Zona 12
		SET @PartialValue = SUBSTRING(@UnwrappedData, 16, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 12:OK';
		END
		ELSE IF (@PartialValue = '2')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 12:KO';
		END
	END

	RETURN 'ESITO TEST (' + ISNULL(@Result, '') + ')';
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[gris_GetDecodedEventDataByCreationTime]'
GO
CREATE PROCEDURE [dbo].[gris_GetDecodedEventDataByCreationTime] (@EventID UNIQUEIDENTIFIER) AS
SET NOCOUNT ON;

DECLARE @DevID BIGINT 
DECLARE @CreatedOn DATETIME

SELECT @DevID = DevID, @CreatedOn = Created
FROM events
WHERE (EventID = @EventID)


SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVATA USCITA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
40 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 33)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'DISATTIVATA USCITA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
41 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 34)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVATO INGRESSO' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
20 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 35)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'DISATTIVATO INGRESSO' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
21 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 36)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
30 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'FINE ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
31 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '1') THEN 'ESITO TEST - Amplificatore 1:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '2') THEN 'ESITO TEST - Amplificatore 1:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '1') THEN 'ESITO TEST - Amplificatore 2:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '2') THEN 'ESITO TEST - Amplificatore 2:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '1') THEN 'ESITO TEST - Zona 1:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '2') THEN 'ESITO TEST - Zona 1:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '1') THEN 'ESITO TEST - Zona 2:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '2') THEN 'ESITO TEST - Zona 2:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '1') THEN 'ESITO TEST - Zona 3:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '2') THEN 'ESITO TEST - Zona 3:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '1') THEN 'ESITO TEST - Zona 4:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '2') THEN 'ESITO TEST - Zona 4:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '1') THEN 'ESITO TEST - Zona 5:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '2') THEN 'ESITO TEST - Zona 5:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '1') THEN 'ESITO TEST - Zona 6:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '2') THEN 'ESITO TEST - Zona 6:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '1') THEN 'ESITO TEST - Zona 7:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '2') THEN 'ESITO TEST - Zona 7:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '1') THEN 'ESITO TEST - Zona 8:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '2') THEN 'ESITO TEST - Zona 8:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '1') THEN 'ESITO TEST - Zona 9:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '2') THEN 'ESITO TEST - Zona 9:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '1') THEN 'ESITO TEST - Zona 10:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '2') THEN 'ESITO TEST - Zona 10:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '1') THEN 'ESITO TEST - Zona 11:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '2') THEN 'ESITO TEST - Zona 11:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '1') THEN 'ESITO TEST - Zona 12:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '2') THEN 'ESITO TEST - Zona 12:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') IN ('1' /* OK */, '2' /* KO */))

ORDER BY events.Created DESC, CustomOrder
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[gris_GetDecodedEventData]'
GO
ALTER PROCEDURE [dbo].[gris_GetDecodedEventData] (@DevID BIGINT = NULL, @CreatedFrom DATETIME = NULL, @CreatedTo DATETIME = NULL, @EventCodeFilter43 BIT /* ATTIVAZIONE SONORA */, @EventCodeFilter44 BIT /* FINE ATTIVAZIONE SONORA */, @EventCodeFilter45 BIT /* ESITO TEST */) AS
SET NOCOUNT ON;

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@EventCodeFilter43 = 1)
AND ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'FINE ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@EventCodeFilter44 = 1)
AND ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ESITO TEST' AS EventDescription,
dbo.GetPZInnerDataTestDescriptionFromEvent(events.EventData) AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@EventCodeFilter45 = 1)
AND ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

ORDER BY events.Created DESC, EventDescription
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_InsEvents]'
GO
CREATE PROCEDURE [dbo].[tf_InsEvents]
	@EventID uniqueidentifier,
	@DevID bigint,
	@EventData varbinary(max), 
	@Created datetime, 
	@Requested datetime,
	@ToBeDeleted bit, 
	@EventCategory tinyint
AS
	
	IF EXISTS(SELECT [EventID] FROM  [events] WHERE ([EventID] = @EventID) )
		UPDATE [events] 
		SET [EventID] = @EventID, [DevID] = @DevID, [EventData] = @EventData, [Created] = @Created, [Requested] = @Requested, [ToBeDeleted] = @ToBeDeleted, [EventCategory] = @EventCategory 
		WHERE ([EventID] = @EventID);
	ELSE
		INSERT INTO [events] ([EventID], [DevID], [EventData], [Created], [Requested], [ToBeDeleted], [EventCategory]) VALUES (@EventID, @DevID, @EventData, @Created, @Requested, @ToBeDeleted, @EventCategory);

RETURN @@ROWCOUNT;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_DelEvents]'
GO
CREATE PROCEDURE [dbo].[tf_DelEvents]
		@Original_EventID uniqueidentifier
AS
	DELETE FROM [events] WHERE ([EventID] = @Original_EventID);
RETURN @@ROWCOUNT;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[gris_GetLastDecodedEvents]'
GO
-- =============================================
-- Author:		Cristian Storti
-- Create date: 16-12-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[gris_GetLastDecodedEvents] (@DevID BIGINT = NULL, @EventCodeFilter43 BIT /* ATTIVAZIONE SONORA */, @EventCodeFilter44 BIT /* FINE ATTIVAZIONE SONORA */, @EventCodeFilter45 BIT /* ESITO TEST */)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT MAX(Created) as LastCreated
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND ((dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43) OR (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44) OR (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_UpdEvents]'
GO
CREATE PROCEDURE [dbo].[tf_UpdEvents]
	@Original_EventID uniqueidentifier,
	@DevID bigint,
	@EventData varbinary(max), 
	@Created datetime, 
	@Requested datetime,
	@ToBeDeleted bit, 
	@EventCategory tinyint
AS
	UPDATE [events] SET [DevID] = @DevID, [EventData] = @EventData, [Created] = @Created, [Requested] = @Requested, [ToBeDeleted] = @ToBeDeleted, [EventCategory] = @EventCategory 
	WHERE ([EventID] = @Original_EventID);

RETURN @@ROWCOUNT;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_GetEventsByDevId]'
GO
CREATE PROCEDURE [dbo].[tf_GetEventsByDevId]
	@DevID as bigint
AS
	SELECT EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory
	FROM events
	WHERE DevID = @DevID
RETURN @@ROWCOUNT;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [idx_nonclust_devid_eventcategory_created_desc] on [dbo].[events]'
GO
CREATE NONCLUSTERED INDEX [idx_nonclust_devid_eventcategory_created_desc] ON [dbo].[events] ([DevID], [EventCategory], [Created] DESC) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded.'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed.'
GO
DROP TABLE #tmpErrors
GO

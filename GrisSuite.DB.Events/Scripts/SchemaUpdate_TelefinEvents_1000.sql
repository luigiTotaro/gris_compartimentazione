
-- Script di update del database TelefinEvents alla versione 1.0.0.0 a partire da un database vuoto

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[GetFunctionCodeDescriptionForEventCategory1]'
GO
CREATE FUNCTION [dbo].[GetFunctionCodeDescriptionForEventCategory1] (@FunctionCode TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @FunctionCodeDescriptionForEventCategory1 VARCHAR(MAX);
	SET @FunctionCodeDescriptionForEventCategory1 = NULL;

	IF (@FunctionCode = 0)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'NESSUNA FUNZIONE';
	END
	ELSE IF (@FunctionCode = 12)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE GESTIONE OROLOGIO';
	END
	ELSE IF (@FunctionCode = 13)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE DI INIZIALIZZAZIONE';
	END
	ELSE IF (@FunctionCode = 14)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE GESTIONE RS485';
	END
	ELSE IF (@FunctionCode = 36)
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE TEST TDS';
	END
	ELSE
	BEGIN
		SET @FunctionCodeDescriptionForEventCategory1 = 'FUNZIONE SCONOSCIUTA';
	END	

	RETURN @FunctionCodeDescriptionForEventCategory1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetPZInnerDataFromEvent]'
GO
CREATE FUNCTION [dbo].[GetPZInnerDataFromEvent]
(
	@EventData VARBINARY(MAX),
	@PZFieldName VARCHAR(3)
)
RETURNS CHAR
AS
BEGIN
	DECLARE @Result CHAR;
	SET @Result = '';

	IF (DATALENGTH(@EventData) = 24)
	BEGIN
		DECLARE @digit CHAR;
		DECLARE @i INT;

		-- Estrae il campo Data dallo stream dell'evento di categoria 2: array di 8 bytes, codificati come byte doppi in base dati, in ordine invertito
		DECLARE @DataEventCategory2 VARBINARY(16);
		SET @DataEventCategory2 = SUBSTRING(@EventData, 7, 16);

		DECLARE @UnwrappedData VARCHAR(16);
		SET @UnwrappedData = '';

		SET @i = 0;

		WHILE (@i < 8)
		BEGIN
			SET @digit = SUBSTRING(@DataEventCategory2, 2 * @i + 2, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END

			SET @digit  = SUBSTRING(@DataEventCategory2, 2 * @i + 1, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END
			SET @i = @i + 1;
		END

		IF (@PZFieldName LIKE 'A1')
		BEGIN
			-- Amplificatore 1
			SET @Result = SUBSTRING(@UnwrappedData, 1, 1);
		END
		ELSE IF (@PZFieldName LIKE 'A2')
		BEGIN
			-- Amplificatore 2
			SET @Result = SUBSTRING(@UnwrappedData, 2, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z1')
		BEGIN
			-- Zona 1
			SET @Result = SUBSTRING(@UnwrappedData, 5, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z2')
		BEGIN
			-- Zona 2
			SET @Result = SUBSTRING(@UnwrappedData, 6, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z3')
		BEGIN
			-- Zona 3
			SET @Result = SUBSTRING(@UnwrappedData, 7, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z4')
		BEGIN
			-- Zona 4
			SET @Result = SUBSTRING(@UnwrappedData, 8, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z5')
		BEGIN
			-- Zona 5
			SET @Result = SUBSTRING(@UnwrappedData, 9, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z6')
		BEGIN
			-- Zona 6
			SET @Result = SUBSTRING(@UnwrappedData, 10, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z7')
		BEGIN
			-- Zona 7
			SET @Result = SUBSTRING(@UnwrappedData, 11, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z8')
		BEGIN
			-- Zona 8
			SET @Result = SUBSTRING(@UnwrappedData, 12, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z9')
		BEGIN
			-- Zona 9
			SET @Result = SUBSTRING(@UnwrappedData, 13, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z10')
		BEGIN
			-- Zona 10
			SET @Result = SUBSTRING(@UnwrappedData, 14, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z11')
		BEGIN
			-- Zona 11
			SET @Result = SUBSTRING(@UnwrappedData, 15, 1);
		END
		ELSE IF (@PZFieldName LIKE 'Z12')
		BEGIN
			-- Zona 12
			SET @Result = SUBSTRING(@UnwrappedData, 16, 1);
		END
	END

	RETURN @Result;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetPZInnerDataDescriptionFromEvent]'
GO
CREATE FUNCTION [dbo].[GetPZInnerDataDescriptionFromEvent]
(
	@EventData VARBINARY(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX);
	SET @Result = NULL;

	IF (DATALENGTH(@EventData) = 24)
	BEGIN
		DECLARE @digit CHAR;
		DECLARE @i INT;

		-- Estrae il campo Data dallo stream dell'evento di categoria 2: array di 8 bytes, codificati come byte doppi in base dati, in ordine invertito
		DECLARE @DataEventCategory2 VARBINARY(16);
		SET @DataEventCategory2 = SUBSTRING(@EventData, 7, 16);

		DECLARE @UnwrappedData VARCHAR(16);
		SET @UnwrappedData = '';

		SET @i = 0;

		WHILE (@i < 8)
		BEGIN
			SET @digit = SUBSTRING(@DataEventCategory2, 2 * @i + 2, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END

			SET @digit  = SUBSTRING(@DataEventCategory2, 2 * @i + 1, 1);

			SET @UnwrappedData = @UnwrappedData +	CASE 
														WHEN @digit LIKE '[0-9]' THEN @digit 
														WHEN @digit = 'A' THEN '0'
														WHEN @digit = 'B' THEN '*'
														WHEN @digit = 'C' THEN '#'
														ELSE '+'
													END
			SET @i = @i + 1;
		END

		DECLARE @PartialValue CHAR;
		DECLARE @Separator VARCHAR;
		SET @Separator = '-';

		-- Amplificatore 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 1, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = 'Amplificatore 1';
		END

		-- Amplificatore 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 2, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Amplificatore 2';
		END

		-- Zona 1
		SET @PartialValue = SUBSTRING(@UnwrappedData, 5, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 1';
		END

		-- Zona 2
		SET @PartialValue = SUBSTRING(@UnwrappedData, 6, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 2';
		END

		-- Zona 3
		SET @PartialValue = SUBSTRING(@UnwrappedData, 7, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 3';
		END

		-- Zona 4
		SET @PartialValue = SUBSTRING(@UnwrappedData, 8, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 4';
		END

		-- Zona 5
		SET @PartialValue = SUBSTRING(@UnwrappedData, 9, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 5';
		END

		-- Zona 6
		SET @PartialValue = SUBSTRING(@UnwrappedData, 10, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 6';
		END

		-- Zona 7
		SET @PartialValue = SUBSTRING(@UnwrappedData, 11, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 7';
		END

		-- Zona 8
		SET @PartialValue = SUBSTRING(@UnwrappedData, 12, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 8';
		END

		-- Zona 9
		SET @PartialValue = SUBSTRING(@UnwrappedData, 13, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 9';
		END

		-- Zona 10
		SET @PartialValue = SUBSTRING(@UnwrappedData, 14, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 10';
		END

		-- Zona 11
		SET @PartialValue = SUBSTRING(@UnwrappedData, 15, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 11';
		END

		-- Zona 12
		SET @PartialValue = SUBSTRING(@UnwrappedData, 16, 1);
		IF (@PartialValue = '1')
		BEGIN
			SET @Result = ISNULL(@Result + @Separator, '') + 'Zona 12';
		END
	END

	RETURN ISNULL(@Result, '');
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetDecodedDataFromEventData]'
GO
CREATE FUNCTION [dbo].[GetDecodedDataFromEventData] 
(
	@EventData VARBINARY(24),
	@FieldId INT
)
-- Decodifica 2 bytes dallo stream dell'evento, in base alla posizione.
RETURNS TINYINT
AS
BEGIN
	DECLARE @code TINYINT;
	DECLARE @hex CHAR(1);
	
	SET @hex = SUBSTRING(@EventData, 2 * @FieldId - 1, 1)

	SET @code = CONVERT(
					TINYINT, 
					CASE WHEN @hex LIKE '[0-9]' THEN CAST(@hex AS TINYINT) 
         				ELSE CAST(ASCII(UPPER(@hex)) - 55 AS TINYINT) 
		 			END)

	SET @hex = SUBSTRING(@EventData, 2 * @FieldId, 1);

	SET @code = 16 * @code + CONVERT(
								TINYINT, 
								CASE WHEN @hex LIKE '[0-9]' THEN CAST(@hex AS TINYINT) 
         							ELSE CAST(ASCII(UPPER(@hex)) - 55 AS TINYINT) 
		 						END)
    
	RETURN @code
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetEventCodeDescriptionForEventCategory1]'
GO
CREATE FUNCTION [dbo].[GetEventCodeDescriptionForEventCategory1] (@EventCode TINYINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @CodeDescriptionForEventCategory1 VARCHAR(MAX);
	SET @CodeDescriptionForEventCategory1 = NULL;

	IF (@EventCode = 0)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'NESSUN ERRORE';
	END
	ELSE IF (@EventCode = 1)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 2)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'RESET';
	END
	ELSE IF (@EventCode = 3)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'SISTEMA OK';
	END
	ELSE IF (@EventCode = 4)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE OROLOGIO';
	END
	ELSE IF (@EventCode = 5)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'CONFIGURAZIONE AUTOMATICA';
	END
	ELSE IF (@EventCode = 12)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 13)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 14)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE SCONOSCIUTO';
	END
	ELSE IF (@EventCode = 20)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'ERRORE PARAMETRI EEPROM';
	END
	ELSE IF (@EventCode = 29)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT SPI NON GESTITO';
	END
	ELSE IF (@EventCode = 30)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT PAIE NON GESTITO';
	END
	ELSE IF (@EventCode = 31)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT PAO NON GESTITO';
	END
	ELSE IF (@EventCode = 32)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC5 NON GESTITO';
	END
	ELSE IF (@EventCode = 33)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC4 NON GESTITO';
	END
	ELSE IF (@EventCode = 34)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC3 NON GESTITO';
	END
	ELSE IF (@EventCode = 35)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC2 NON GESTITO';
	END
	ELSE IF (@EventCode = 36)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TIC3 NON GESTITO';
	END
	ELSE IF (@EventCode = 37)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TIC2 NON GESTITO';
	END
	ELSE IF (@EventCode = 38)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TIC1 NON GESTITO';
	END
	ELSE IF (@EventCode = 39)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT XIRQ NON GESTITO';
	END
	ELSE IF (@EventCode = 40)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT SWI NON GESTITO';
	END
	ELSE IF (@EventCode = 41)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT IOT NON GESTITO';
	END
	ELSE IF (@EventCode = 42)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT NOCOP NON GESTITO';
	END
	ELSE IF (@EventCode = 43)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT CME NON GESTITO';
	END
	ELSE IF (@EventCode = 44)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT RTI NON GESTITO';
	END
	ELSE IF (@EventCode = 45)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'NESSUN ERRORE';
	END
	ELSE IF (@EventCode = 47)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'CONFIGURAZIONE AGGIORNATA';
	END
	ELSE IF (@EventCode = 57)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'MODIFICATO CONFIGURAZIONE RAM';
	END
	ELSE IF (@EventCode = 112)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT SCI NON GESTITO';
	END
	ELSE IF (@EventCode = 113)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TO NON GESTITO';
	END
	ELSE IF (@EventCode = 114)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT TOC1 NON GESTITO';
	END
	ELSE IF (@EventCode = 115)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'INTERRUPT IRQ NON GESTITO';
	END
	ELSE IF (@EventCode = 116)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'AGGIORNATO DATA-ORA';
	END
	ELSE IF (@EventCode = 117)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE ERRORI NON VALIDO';
	END
	ELSE IF (@EventCode = 118)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE EVENTI NON VALIDO';
	END
	ELSE IF (@EventCode = 138)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'TEST TDS CONCLUSO CON SUCCESSO';
	END
	ELSE IF (@EventCode = 139)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'TEST TDS FALLITO';
	END
	ELSE IF (@EventCode = 143)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE ERRORI CANCELLATO';
	END
	ELSE IF (@EventCode = 144)
	BEGIN
		SET @CodeDescriptionForEventCategory1 = 'FILE EVENTI CANCELLATO';
	END
	ELSE
	BEGIN
		SET @CodeDescriptionForEventCategory1 = '';
	END

	RETURN @CodeDescriptionForEventCategory1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[events]'
GO
CREATE TABLE [dbo].[events]
(
[EventID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_events_EventID] DEFAULT (newid()),
[DevID] [bigint] NOT NULL,
[EventData] [varbinary] (max) NOT NULL,
[EventCategory] [tinyint] NOT NULL,
[Created] [datetime] NULL,
[Requested] [datetime] NOT NULL CONSTRAINT [DF_events_Requested] DEFAULT (getdate()),
[ToBeDeleted] [bit] NOT NULL CONSTRAINT [DF_events_ToBeDeleted] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_events] on [dbo].[events]'
GO
ALTER TABLE [dbo].[events] ADD CONSTRAINT [PK_events] PRIMARY KEY CLUSTERED  ([EventID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_events_EventCategory] on [dbo].[events]'
GO
CREATE NONCLUSTERED INDEX [IX_events_EventCategory] ON [dbo].[events] ([EventCategory]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_events_Created] on [dbo].[events]'
GO
CREATE NONCLUSTERED INDEX [IX_events_Created] ON [dbo].[events] ([Created]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[gris_GetDecodedEventData]'
GO
CREATE PROCEDURE [dbo].[gris_GetDecodedEventData] (@DevID BIGINT = NULL, @CreatedFrom DATETIME = NULL, @CreatedTo DATETIME = NULL, @IncludeErrorCategory BIT = 0, @IncludeUnrecognizedEvents BIT = 0) AS
SET NOCOUNT ON;

SELECT
events.EventID,
events.Created,
events.EventCategory,
NULL AS EventCode,
'Impossibile decodificare il messaggio di categoria 1, lunghezza dati imprevista' AS EventDescription,
NULL AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
NULL AS DeviceCode
FROM events
WHERE (@IncludeErrorCategory = 1)
AND ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 1 /* Errore */) AND (DATALENGTH(events.EventData) <> 8))
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
NULL AS EventCode,
'Impossibile decodificare il messaggio di categoria 2, lunghezza dati imprevista' AS EventDescription,
NULL AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
NULL AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) <> 24))
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
dbo.GetEventCodeDescriptionForEventCategory1(dbo.GetDecodedDataFromEventData(events.EventData, 1)) AS EventDescription,
NULL AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS FunctionCode,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS DeviceAddress,
DBO.GetFunctionCodeDescriptionForEventCategory1(dbo.GetDecodedDataFromEventData(events.EventData, 2)) AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 4) AS DeviceCode
FROM events
WHERE (@IncludeErrorCategory = 1)
AND ((@DevID IS NULL) OR (events.DevID = @DevID)) 
AND ((events.EventCategory = 1 /* Errore */) AND (DATALENGTH(events.EventData) = 8))
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVATA USCITA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 33)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'DISATTIVATA USCITA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 34)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVATO INGRESSO' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 35)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'DISATTIVATO INGRESSO' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 36)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'FINE ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '1') THEN 'ESITO TEST - Amplificatore 1:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '2') THEN 'ESITO TEST - Amplificatore 1:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '1') THEN 'ESITO TEST - Amplificatore 2:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '2') THEN 'ESITO TEST - Amplificatore 2:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '1') THEN 'ESITO TEST - Zona 1:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '2') THEN 'ESITO TEST - Zona 1:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '1') THEN 'ESITO TEST - Zona 2:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '2') THEN 'ESITO TEST - Zona 2:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '1') THEN 'ESITO TEST - Zona 3:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '2') THEN 'ESITO TEST - Zona 3:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '1') THEN 'ESITO TEST - Zona 4:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '2') THEN 'ESITO TEST - Zona 4:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '1') THEN 'ESITO TEST - Zona 5:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '2') THEN 'ESITO TEST - Zona 5:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '1') THEN 'ESITO TEST - Zona 6:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '2') THEN 'ESITO TEST - Zona 6:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '1') THEN 'ESITO TEST - Zona 7:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '2') THEN 'ESITO TEST - Zona 7:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '1') THEN 'ESITO TEST - Zona 8:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '2') THEN 'ESITO TEST - Zona 8:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '1') THEN 'ESITO TEST - Zona 9:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '2') THEN 'ESITO TEST - Zona 9:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '1') THEN 'ESITO TEST - Zona 10:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '2') THEN 'ESITO TEST - Zona 10:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '1') THEN 'ESITO TEST - Zona 11:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '2') THEN 'ESITO TEST - Zona 11:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '1') THEN 'ESITO TEST - Zona 12:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '2') THEN 'ESITO TEST - Zona 12:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'NESSUN EVENTO' AS EventDescription,
NULL AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@IncludeUnrecognizedEvents <> 0)
AND ((@DevID IS NULL) OR (events.DevID = @DevID))
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) NOT IN (33, 34, 35, 36, 43, 44, 45))
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

ORDER BY events.Created DESC, EventDescription
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_GetEventsToSend]'
GO
CREATE PROCEDURE dbo.tf_GetEventsToSend
AS
SET NOCOUNT ON;
SELECT TOP (100) EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory
FROM events
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded.'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed.'
GO
DROP TABLE #tmpErrors
GO

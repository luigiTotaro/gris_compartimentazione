﻿
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;
SET NUMERIC_ROUNDABORT OFF;
GO

USE [master]

GO
:on error exit
GO

USE TelefinEvents
GO

PRINT N'Altering dbo.gris_GetDecodedEventData...';


GO
ALTER PROCEDURE [dbo].[gris_GetDecodedEventData]
@DevID BIGINT=NULL, @CreatedFrom DATETIME=NULL, @CreatedTo DATETIME=NULL, @EventCodeFilter43 BIT, @EventCodeFilter44 BIT, @EventCodeFilter45 BIT
AS
SET NOCOUNT ON;

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@EventCodeFilter43 = 1)
AND (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'FINE ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@EventCodeFilter44 = 1)
AND (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ESITO TEST' AS EventDescription,
dbo.GetPZInnerDataTestDescriptionFromEvent(events.EventData) AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode
FROM events
WHERE (@EventCodeFilter45 = 1)
AND (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND ((@CreatedFrom IS NULL) OR (Created >= @CreatedFrom))
AND ((@CreatedTo IS NULL) OR (Created <= @CreatedTo))

ORDER BY events.Created DESC, EventDescription


GO
PRINT N'Altering dbo.gris_GetDecodedEventDataByCreationTime...';


GO
ALTER PROCEDURE [dbo].[gris_GetDecodedEventDataByCreationTime]
@EventID UNIQUEIDENTIFIER
AS
SET NOCOUNT ON;

DECLARE @DevID BIGINT 
DECLARE @CreatedOn DATETIME

SELECT @DevID = DevID, @CreatedOn = Created
FROM events
WHERE (EventID = @EventID)

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVATA USCITA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
40 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 33)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'DISATTIVATA USCITA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
41 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 34)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVATO INGRESSO' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
20 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 35)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'DISATTIVATO INGRESSO' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
21 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 36)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
dbo.GetPZInnerDataDescriptionFromEvent(events.EventData) AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
30 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
'FINE ATTIVAZIONE SONORA' AS EventDescription,
NULL AS EventDescriptionValue,
dbo.GetDecodedDataFromEventData(events.EventData, 2) AS InOutID,
dbo.GetDecodedDataFromEventData(events.EventData, 3) AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
31 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44)
AND (@CreatedOn = Created)

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '1') THEN 'ESITO TEST - Amplificatore 1:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') = '2') THEN 'ESITO TEST - Amplificatore 1:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A1') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '1') THEN 'ESITO TEST - Amplificatore 2:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') = '2') THEN 'ESITO TEST - Amplificatore 2:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'A2') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '1') THEN 'ESITO TEST - Zona 1:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') = '2') THEN 'ESITO TEST - Zona 1:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z1') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '1') THEN 'ESITO TEST - Zona 2:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') = '2') THEN 'ESITO TEST - Zona 2:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z2') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '1') THEN 'ESITO TEST - Zona 3:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') = '2') THEN 'ESITO TEST - Zona 3:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z3') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '1') THEN 'ESITO TEST - Zona 4:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') = '2') THEN 'ESITO TEST - Zona 4:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z4') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '1') THEN 'ESITO TEST - Zona 5:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') = '2') THEN 'ESITO TEST - Zona 5:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z5') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '1') THEN 'ESITO TEST - Zona 6:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') = '2') THEN 'ESITO TEST - Zona 6:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z6') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '1') THEN 'ESITO TEST - Zona 7:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') = '2') THEN 'ESITO TEST - Zona 7:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z7') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '1') THEN 'ESITO TEST - Zona 8:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') = '2') THEN 'ESITO TEST - Zona 8:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z8') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '1') THEN 'ESITO TEST - Zona 9:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') = '2') THEN 'ESITO TEST - Zona 9:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z9') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '1') THEN 'ESITO TEST - Zona 10:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') = '2') THEN 'ESITO TEST - Zona 10:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z10') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '1') THEN 'ESITO TEST - Zona 11:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') = '2') THEN 'ESITO TEST - Zona 11:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z11') IN ('1' /* OK */, '2' /* KO */))

UNION ALL

SELECT
events.EventID,
events.Created,
events.EventCategory,
dbo.GetDecodedDataFromEventData(events.EventData, 1) AS EventCode,
CASE
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '1') THEN 'ESITO TEST - Zona 12:OK'
	WHEN (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') = '2') THEN 'ESITO TEST - Zona 12:KO'
END AS EventDescription,
dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') AS EventDescriptionValue,
NULL AS InOutID,
NULL AS MicroID,
NULL AS SubEventDescription,
NULL AS FunctionCode,
NULL AS DeviceAddress,
NULL AS FunctionDescription,
dbo.GetDecodedDataFromEventData(events.EventData, 12) AS DeviceCode,
10 AS CustomOrder
FROM events
WHERE (events.DevID = @DevID)
AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
AND (dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45)
AND (@CreatedOn = Created)
AND (dbo.GetPZInnerDataFromEvent(events.EventData, 'Z12') IN ('1' /* OK */, '2' /* KO */))

ORDER BY events.Created DESC, CustomOrder


GO
PRINT N'Altering dbo.gris_GetLastDecodedEvents...';


GO
ALTER PROCEDURE [dbo].[gris_GetLastDecodedEvents]
@DevID BIGINT=NULL, @EventCodeFilter43 BIT, @EventCodeFilter44 BIT, @EventCodeFilter45 BIT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT MAX(Created) as LastCreated
	FROM events
	WHERE (events.DevID = @DevID)
	AND ((events.EventCategory = 2 /* Eventi */) AND (DATALENGTH(events.EventData) = 24))
	AND ((@EventCodeFilter43 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 43) OR (@EventCodeFilter44 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 44) OR (@EventCodeFilter45 = 1 AND dbo.GetDecodedDataFromEventData(events.EventData, 1) = 45))
END


GO

GO

GO

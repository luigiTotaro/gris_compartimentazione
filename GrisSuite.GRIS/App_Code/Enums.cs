public enum GrisLayoutMode
{
    None,
    Map,
	Filters,
    Reports,
    Alerts,
    Validation,
    Administration
}

public enum MapLayoutMode
{
    None,
    Graph,
    Grid
}

public enum PageDestination
{
    None,
    Italia,
    Region,
    Zone,
    Node,
    Device
}

public enum GrisSessionGroup
{
    None,
    Region,
    Zone,
    Node
}

public enum InformationBarIcon
{
    None,
    Info,
    Exclamation
}
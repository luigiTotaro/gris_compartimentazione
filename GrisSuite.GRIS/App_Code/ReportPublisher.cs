﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.Globalization;

public abstract class ReportPublisher : FiltersSavingPage
{
	protected ReportParameter CurrentCultureParameter = new ReportParameter("currentCulture", CultureInfo.CurrentCulture.Name);

	# region Properties
	
	public ReportsMaster ReportsMaster
	{
		get
		{
			if ( this.Master != null )
			{
				return (ReportsMaster)this.Master;
			}

			return null;
		}
	}

	public abstract ReportViewer ReportViewer
	{
		get;
	}
	
	/// <summary>
	/// Abilita l'interpretazione della stringa di bookmark per ottenere le informaizoni di navigazione verso altre pagine
	/// </summary>
	public bool UseBookmarksToNavigate
	{
		get
		{
			return Convert.ToBoolean(this.ViewState["UseBookmarksToNavigate"] ?? true);
		}
		set
		{
			this.ViewState["UseBookmarksToNavigate"] = value;
		}
	}
	
	# endregion

	# region Event Methods and Handlers
	
	protected override void OnLoad ( EventArgs e )
	{
		base.OnLoad(e);

		if ( this.ReportViewer != null )
		{
			this.ReportViewer.Style[HtmlTextWriterStyle.TextAlign] = "right";

			if ( this.Page.Request.Browser.Type == "IE7" )
			{
				this.ReportViewer.Style[HtmlTextWriterStyle.MarginBottom] = "30px";
			}

			// quando si usano i bookmarks per la navigazione viene registrato il gestore di eventi predefinito solo se la proprità UseBookmarksToNavigate è vera
			if ( this.UseBookmarksToNavigate ) this.ReportViewer.BookmarkNavigation += report_OnBookmarkNavigation;
		}

		if ( this.ReportsMaster != null )
		{
			// visibilità dei pannelli
			if ( !this.IsPostBack )
			{
				this.ReportsMaster.ReportListPanelVisible = false;
				this.ReportsMaster.FiltersPanelVisible = true;
				this.ReportsMaster.ReportPanelVisible = false;

				this.ReportsMaster.ReportListEnabled = true;
				this.ReportsMaster.FiltersButtonEnabled = true;

				// impostazione report selezionato
				if ( HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.QueryString["selRep"] != null )
				{
					this.ReportsMaster.SelectedReport = HttpContext.Current.Request.QueryString["selRep"];
				}
			}
		}
	}

	protected override void OnPreRender ( EventArgs e )
	{
		base.OnPreRender(e);

		if ( !this.IsPostBack && this.IsFiltersStateRestored )
		{
			this.ReportsMaster.InvokeReportGeneration();
		}
	}

	/// <summary>
	/// Metodo che interpreta la striga di bookmark per ottenere le informazioni di navigazione
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="eventArgs"></param>
	protected void report_OnBookmarkNavigation ( object sender, BookmarkNavigationEventArgs eventArgs )
	{
		long id;
		var paramz = eventArgs.BookmarkId.Split('|');

		if ( paramz.Length >= 2 && long.TryParse(paramz[1], out id) && this.GrisMaster != null )
		{
			this.GrisMaster.SelectInterface(GrisLayoutMode.Map, false);

			if ( id != -1 )
			{
				Navigation.GetAreaNavigationObj(paramz[0], true).ResetState().NavigateToArea(id);
			}
			else
			{
				new ItaliaNavigation(null).ResetState().NavigateToArea(id);
			}
		}
	}
	
	# endregion

	public virtual void SetupReportParameters ()
	{
		if ( this.ReportViewer != null )
		{
			// passo la cultura assegnata al thread corrente, i reports risultano sempre en-US
			IList<ReportParameter> paramsList = new List<ReportParameter> { this.CurrentCultureParameter };
			this.ReportViewer.LocalReport.SetParameters(paramsList);
		}
	}

    public  virtual void OnInvokeReportGeneration()
    {

    }
}
﻿using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using AjaxControlToolkit;
using Controllers;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.NodeDSTableAdapters;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NavigationBarGoToNode : WebService
{
    [WebMethod]
    [ScriptMethod]
    public string[] GetCompletionList(string prefixText, int count)
    {
        if (prefixText.Length > 0)
        {
            NodeDS.VisibleNodesDataTable nodes;
            VisibleNodesTableAdapter ta = new VisibleNodesTableAdapter();

            int i;
            if (int.TryParse(prefixText[0].ToString(), out i))
            {
                // IP stlc
                if (prefixText.Split('.').Length < 4)
                {
                    return null;
                }
                nodes = ta.GetNodesByInitialsOrIP(false, prefixText);
            }
            else
            {
                // Nome Stazione
                nodes = ta.GetNodesByInitialsOrIP(true, prefixText);
            }

            var selectedResults = (from NodeDS.VisibleNodesRow node in nodes
                                   from permReg in GrisPermissions.VisibleRegions()
                                   where
                                       ((permReg == node.RegID) &&
                                        Node.CanUserViewDevices(node.RegID, node.STLCOK, node.STLCInMaintenance, node.STLCTotalCount)) &&
                                       (node.IsDeleted == false)
                                   select
                                       AutoCompleteExtender.CreateAutoCompleteItem(
                                           node.NodeName.Replace("Compartimento di ", "").Replace("Linea ", ""), node.NodID.ToString())).Take(count);

            return selectedResults.ToArray();
        }

        return null;
    }
}
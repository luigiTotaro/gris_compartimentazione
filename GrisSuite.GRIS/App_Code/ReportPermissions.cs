﻿
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ReportsDSTableAdapters;
using GrisSuite.Common;

public class ReportPermissionGroups
{
	public int ReportId { get; set; }
	public string Category { get; set; }
	public string ReportCode { get; set; }
	public string ReportName { get; set; }
    /*
public string Groups { get; set; }
public IList<string > WindowsGroups { get; set; }
public static IEnumerable<ReportPermissionGroups> GetReportsAndGroups ( string reportName, string groupName )
{
    var reportsTa = new ReportsGroupsTableAdapter();
    var reportTable = reportsTa.GetData(reportName, groupName);

    var reports = from ReportsDS.ReportsGroupsRow reportRow in reportTable.Rows
                  group reportRow.GroupName by new { Cat = reportRow.ReportCategory, Id = reportRow.ReportID, Name = reportRow.ReportName }
                      into groups
                      select new ReportPermissionGroups
                      {
                          ReportId = groups.Key.Id,
                          Category = groups.Key.Cat,
                          ReportName = groups.Key.Name,
                          Groups = groups.Aggregate(( running, next ) => string.Format("{0}, {1}", running, next))
                      };

    return reports;
}
 */

	public static IEnumerable<ReportPermissionGroups> GetReportsByCategory ( int categoryId )
	{
		var reportsTa = new ReportsByCategoryTableAdapter();
		var reportTable = reportsTa.GetData(categoryId);

		var reports = from ReportsDS.ReportsByCategoryRow reportRow in reportTable.Rows
					  group reportRow.GroupName by new { Id = reportRow.ReportID, Code = reportRow.ReportCode, Name = reportRow.ReportName }
						  into groups
						  select new ReportPermissionGroups
						  {
							  ReportId = groups.Key.Id,
							  ReportCode = groups.Key.Code,
							  ReportName = groups.Key.Name
						  };

		return reports;
	}
}


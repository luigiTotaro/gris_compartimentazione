﻿using System;

[Serializable]
public class FiltersPageSelections
{
    public bool HasFilters { get; set; }
    public string RegionIDs { get; set; }
    public string RegionNames { get; set; }
    public string ZoneIDs { get; set; }
    public string ZoneNames { get; set; }
    public string NodeIDs { get; set; }
    public string NodeNames { get; set; }
    public string SystemIDs { get; set; }
    public string SystemNames { get; set; }
    public string DeviceTypeIDs { get; set; }
    public string DeviceTypeNames { get; set; }
    public string SevLevelIDs { get; set; }
    public string DeviceFilterName { get; set; }
    public string DeviceData { get; set; }
    public bool IsShared { get; set; }
    public int ResourceId { get; set; }
}
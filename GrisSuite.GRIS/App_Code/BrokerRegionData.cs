﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class BrokerRegionData
{
    public BrokerRegionData()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string ID { get; set; }
    public string Name { get; set; }
    public string ip { get; set; }
    public string username { get; set; }
    public string password { get; set; }
    public string port { get; set; }
    public string urlCommandToAwg { get; set; }
    public string urlListenFromAwg { get; set; }

}     
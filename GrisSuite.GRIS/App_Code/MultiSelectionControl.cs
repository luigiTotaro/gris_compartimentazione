﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

[DefaultProperty("SelectedValues")]
[ControlValueProperty("SelectedValues", "Selected Values")]
public class MultiSelectionControl : FiltersSavingControl
{	
	# region Properties

	private ListControl _innerControl;
	protected ListControl InnerControl 
	{ 
		get
		{
			return this._innerControl;
		} 
		
		set
		{
			this._innerControl = value;

			if ( value != null )
			{
				value.SelectedIndexChanged += MultiSelectionControl_SelectedIndexChanged;
			}
		} 
	}

	void MultiSelectionControl_SelectedIndexChanged ( object sender, EventArgs e )
	{
		this.UpdateControlSelectedValues();
	}

	public SelectionType SelectionType
	{
		get { return (SelectionType)(this.ViewState["SelectionType"] ?? SelectionType.Single); }
		set { this.ViewState["SelectionType"] = value; }
	}

	public RepeatDirection RepeatDirection
	{
		get { return (RepeatDirection)(this.ViewState["RepeatDirection"] ?? RepeatDirection.Vertical); }
		set { this.ViewState["RepeatDirection"] = value; }
	}

	public int RepeatColumns
	{
		get { return Convert.ToInt32(this.ViewState["RepeatColumns"] ?? 0); }
		set { this.ViewState["RepeatColumns"] = value; }
	}

	// proprietà mantenuta perchè presente anche per la checkboxlist
	public string SelectedValue
	{
		get { return ( this.SelectedValues != null && this.SelectedValues.Length > 0 ) ? this.SelectedValues[0] : ""; }
		set { this.SelectedValues = new[] { value }; }
	}
	
	public string[] SelectedValues
	{
		get { return (string[])(this.ViewState["SelectedValues"] ?? new string[0]); }
		set { this.ViewState["SelectedValues"] = value; }
	}

    public string[] SelectedTexts
    {
        get
        {
            return (from item in this._innerControl.Items.Cast<ListItem>() where item.Selected select item.Text).ToArray();   
        }
    }


    # endregion

	protected override void OnInit ( EventArgs e )
	{
		base.OnInit(e);
		this.Page.LoadComplete += Page_LoadComplete;
	}
	
	void Page_LoadComplete ( object sender, EventArgs e )
	{
		if ( !this.IsPostBack ) this.InitializeControl();
	}
	
	public void InitializeControl ()
	{
		this.SetInnerControlRepeatColumns();
		this.SetInnerControlRepeatDirection();
		this.SetInnerControlSelectedValues();
	}

	private void SetInnerControlRepeatDirection ()
	{
		if ( this._innerControl is CheckBoxList )
			( (CheckBoxList)this._innerControl ).RepeatDirection = this.RepeatDirection;
	}

	private void SetInnerControlRepeatColumns ()
	{
		if ( this._innerControl is CheckBoxList )
			( (CheckBoxList)this._innerControl ).RepeatColumns = this.RepeatColumns;
	}

	private void SetInnerControlSelectedValues ()
	{
		switch ( SelectionType )
		{
			case SelectionType.Single:
				{
					if ( this._innerControl != null && this._innerControl is DropDownList )
					{
						try
						{
							this._innerControl.DataBind();
						}
						catch (ArgumentOutOfRangeException) { }

						this._innerControl.SelectedValue = this.SelectedValue;
					}
					
					break;
				}
			case SelectionType.Multiple:
				{
					if ( this._innerControl != null && this._innerControl is CheckBoxList )
					{
						if ( this.SelectedValues != null && this.SelectedValues.Length > 0 )
						{
							string[] values = this.SelectedValues;

							try
							{
								this._innerControl.DataBind();
							}
							catch (ArgumentException) {}

							switch ( this.SelectionType )
							{
								case SelectionType.Single:
									{
										this._innerControl.SelectedValue = values[0];
										break;
									}
								case SelectionType.Multiple:
									{
										List<string> selectedValues = new List<string>(values);

										foreach ( ListItem item in this._innerControl.Items ) item.Selected = selectedValues.Contains(item.Value);

										break;
									}
							}
						}
					}
					break;
				}
		}
	}
	
	private void UpdateControlSelectedValues ()
	{
		switch ( SelectionType )
		{
			case SelectionType.Single:
				{
					this.SelectedValue = this._innerControl.SelectedValue;					
					break;
				}
			case SelectionType.Multiple:
				{
					if ( this._innerControl != null && this._innerControl is CheckBoxList )
					{
						this.SelectedValues = ( from item in this._innerControl.Items.Cast<ListItem>()
												where item.Selected
												select item.Value ).ToArray();
					}
					
					break;
				}
		}
	}
}

public enum SelectionType
{
	Single, Multiple
}

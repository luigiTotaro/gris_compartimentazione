﻿
using GrisControls;

/// <summary>
/// Summary description for ZeusGaugeDataProvider
/// </summary>
public class ZeusGaugeDataProvider : GaugeDataProvider
{
	public ZeusGaugeDataProvider ( 
							string referenceValue, string actualValue, 
							string inferiorLimitPerc, string inferiorYellowRedOffsetPerc, string inferiorYellowGreenOffsetPerc, 
							int pixelHeight 
						)
						: base (
							referenceValue, actualValue,
							"0%", "0%", "0%",
							inferiorLimitPerc, inferiorYellowRedOffsetPerc, inferiorYellowGreenOffsetPerc,
							pixelHeight
						){}

	protected override void InitializeProvider ( double referenceValue, double actualValue, double superiorLimitPerc, double superiorYellowRedOffsetPerc, double superiorYellowGreenOffsetPerc, double inferiorLimitPerc, double inferiorYellowRedOffsetPerc, double inferiorYellowGreenOffsetPerc, int pixelHeight )
	{
        base.InitializeProvider(referenceValue, actualValue, superiorLimitPerc, superiorYellowRedOffsetPerc, superiorYellowGreenOffsetPerc, 100, 100 - ((100 * inferiorYellowRedOffsetPerc) / referenceValue), 100 - ((100 * inferiorYellowRedOffsetPerc) / referenceValue), pixelHeight);
	}
}
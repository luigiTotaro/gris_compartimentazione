﻿
namespace GrisControls
{
	public interface IGaugeDataProvider
	{
		double SuperiorLimit { get; }
		double InferiorLimit { get; }
		double SuperiorYellowRed { get; }
		double InferiorYellowRed { get; }
		double SuperiorYellowGreen { get; }
		double InferiorYellowGreen { get; }

		double ReferenceValue { get; }
		double ActualValue { get; }
		double SuperiorLimitValue { get; }
		double InferiorLimitValue { get; }
		double SuperiorYellowRedValue { get; }
		double InferiorYellowRedValue { get; }
		double SuperiorYellowGreenValue { get; }
		double InferiorYellowGreenValue { get; }

		double InferiorRedZoneHeight { get; }
		double InferiorYellowZoneHeight { get; }
		double InferiorGreenZoneHeight { get; }
		double SuperiorGreenZoneHeight { get; }
		double SuperiorYellowZoneHeight { get; }
		double SuperiorRedZoneHeight { get; }
		double IndicatorHeight { get; }

		void Initialize();
	}
}
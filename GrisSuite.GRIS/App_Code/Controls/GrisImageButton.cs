﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrisControls {
    public class GrisImageButton : ImageButton {

        public bool AutoComposeImageDisabledUrl {
            get {
                return (this.ViewState["AutoComposeImageDisabledUrl"] == null) ? true : (bool)this.ViewState["AutoComposeImageDisabledUrl"];
            }
            set {
                this.ViewState["AutoComposeImageDisabledUrl"] = value;
            }
        }

        public string ImageDisabledUrl {
            get {
                return (this.ViewState["ImageDisabled"] == null) ? string.Empty : this.ViewState["ImageDisabled"].ToString();
            }
            set {
                this.ViewState["ImageDisabled"] = value;
            }
        }

        protected override void Render(HtmlTextWriter writer) {
            if (!this.Enabled) {
                this.ImageUrl = this.AutoComposeImageDisabledUrl ? ComposeDisabledURL(this.ImageUrl) : this.ImageDisabledUrl;

                this.Style.Add("cursor", "default");
                this.Attributes["onClick"] = "return false;";
                this.ToolTip = String.Empty;
            }

            base.Render(writer);
        }

        private static string ComposeDisabledURL(string url) {
            int iPos = url.LastIndexOf(".");

            if (iPos >= 0) {
                return url.Substring(0, iPos) + "_disabled" + url.Substring(iPos);
            }

            return String.Empty;
        }
    }
}
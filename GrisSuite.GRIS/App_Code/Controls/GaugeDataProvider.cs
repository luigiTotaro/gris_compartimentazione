﻿
using System;
using System.Text.RegularExpressions;
using System.Globalization;

namespace GrisControls
{
	public class GaugeDataProvider : IGaugeDataProvider
	{
		private int _pixelHeight;

		public GaugeDataProvider ( string referenceValue, string actualValue, string superiorLimitPerc, string superiorYellowRedOffsetPerc, string superiorYellowGreenOffsetPerc, string inferiorLimitPerc, string inferiorYellowRedOffsetPerc, string inferiorYellowGreenOffsetPerc, int pixelHeight )
		{
			# region Validazione input

			if ( !this.ExtractValue(referenceValue, out this._referenceValue) )
				throw new GaugeDataProviderInitializationException("Valore di riferimento non formattato correttamente.");

			if ( !this.ExtractValue(actualValue, out this._actualValue) )
				throw new GaugeDataProviderInitializationException("Valore da rappresentare non formattato correttamente.");

			if ( !this.ExtractAbsValue(superiorLimitPerc, out this._superiorLimit) )
				throw new GaugeDataProviderInitializationException("Percentuale di fondo scala superiore non formattata correttamente.");

			if ( !this.ExtractAbsValue(superiorYellowRedOffsetPerc, out this._superiorYellowRed) )
				throw new GaugeDataProviderInitializationException("Percentuale soglia del giallo/rosso positivo non formattata correttamente.");

			if ( !this.ExtractAbsValue(superiorYellowGreenOffsetPerc, out this._superiorYellowGreen) )
				throw new GaugeDataProviderInitializationException("Percentuale soglia del verde/giallo positivo non formattata correttamente.");

			if ( !this.ExtractAbsValue(inferiorLimitPerc, out this._inferiorLimit) )
				throw new GaugeDataProviderInitializationException("Percentuale di fondo scala inferiore non formattata correttamente.");

			if ( !this.ExtractAbsValue(inferiorYellowRedOffsetPerc, out this._inferiorYellowRed) )
				throw new GaugeDataProviderInitializationException("Percentuale soglia del giallo/rosso negativo non formattata correttamente.");

			if ( !this.ExtractAbsValue(inferiorYellowGreenOffsetPerc, out this._inferiorYellowGreen) )
				throw new GaugeDataProviderInitializationException("Percentuale soglia del verde/giallo negativo non formattata correttamente.");

			this._pixelHeight = pixelHeight;

			# endregion
		}

		private bool ExtractAbsValue ( string formattedValue, out double value )
		{
			return this.ExtractValue(formattedValue, @"[\d.,]+", out value);
		}

		private bool ExtractValue ( string formattedValue, out double value )
		{
			return this.ExtractValue(formattedValue, @"-*[\d.,]+", out value);
		}

		private bool ExtractValue ( string formattedValue, string pattern, out double value )
		{
			value = .0;

			Match match = Regex.Match(formattedValue, pattern);
			if ( match.Success )
			{
				if ( double.TryParse(match.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out value) )
				{
					return true;
				}
			}

			return false;
		}

		protected virtual void InitializeProvider ( double referenceValue, double actualValue, double superiorLimitPerc, double superiorYellowRedOffsetPerc, double superiorYellowGreenOffsetPerc, double inferiorLimitPerc, double inferiorYellowRedOffsetPerc, double inferiorYellowGreenOffsetPerc, int pixelHeight )
		{
			if ( superiorLimitPerc < 0 ) throw new GaugeDataProviderInitializationException("Il limite superiore deve avere un valore positivo.");
			if ( inferiorLimitPerc < 0 ) throw new GaugeDataProviderInitializationException("Il limite inferiore deve avere un valore positivo.");
			if ( superiorYellowGreenOffsetPerc > 100 ) throw new GaugeDataProviderInitializationException("La percentuale di soglia superiore del verde/giallo non può essere maggiore di 100.");
			if ( inferiorYellowGreenOffsetPerc > 100 ) throw new GaugeDataProviderInitializationException("La percentuale di soglia inferiore del verde/giallo non può essere maggiore di 100.");
			if ( superiorYellowRedOffsetPerc > 100 ) throw new GaugeDataProviderInitializationException("La percentuale di soglia superiore del verde/giallo non può essere maggiore di 100.");
			if ( inferiorYellowRedOffsetPerc > 100 ) throw new GaugeDataProviderInitializationException("La percentuale di soglia inferiore del verde/giallo non può essere maggiore di 100.");
			if ( superiorYellowGreenOffsetPerc != 0 && superiorYellowRedOffsetPerc != 0 && superiorYellowGreenOffsetPerc > superiorYellowRedOffsetPerc ) throw new GaugeDataProviderInitializationException("La percentuale di soglia superiore del verde/giallo non può essere maggiore della percentuale di soglia superiore del giallo/rosso.");
			if ( inferiorYellowGreenOffsetPerc != 0 && inferiorYellowRedOffsetPerc != 0 && inferiorYellowGreenOffsetPerc > inferiorYellowRedOffsetPerc ) throw new GaugeDataProviderInitializationException("La percentuale di soglia inferiore del verde/giallo non può essere maggiore della percentuale di soglia inferiore del giallo/rosso.");

			double totalRange = .0;
			double superiorRange = .0;
			double inferiorRange = .0;
			this._referenceValue = referenceValue;
			this._actualValue = actualValue;
			this._superiorLimitValue = referenceValue * ( 1 + superiorLimitPerc / 100 );
			this._inferiorLimitValue = referenceValue * ( 1 - inferiorLimitPerc / 100 );

			if ( this._actualValue < this._inferiorLimitValue ) this._actualValue = this._inferiorLimitValue;
			if ( this._actualValue > this._superiorLimitValue ) this._actualValue = this._superiorLimitValue;

			totalRange = this._superiorLimitValue - this._inferiorLimitValue;
			superiorRange = this._superiorLimitValue - this._referenceValue;
			inferiorRange = this._referenceValue - this._inferiorLimitValue;

			this._superiorYellowGreenValue = this._referenceValue + superiorRange * superiorYellowGreenOffsetPerc / 100;
			this._superiorYellowRedValue = this._referenceValue + superiorRange * superiorYellowRedOffsetPerc / 100;
			this._inferiorYellowGreenValue = this._referenceValue - inferiorRange * inferiorYellowGreenOffsetPerc / 100;
			this._inferiorYellowRedValue = this._referenceValue - inferiorRange * inferiorYellowRedOffsetPerc / 100;

			// determinazione delle altezze in pixels
			double inferiorYellowRedPixelValue = this.GetPixelValue(this._inferiorYellowRedValue, totalRange, pixelHeight);
			double inferiorYellowGreenPixelValue = this.GetPixelValue(this._inferiorYellowGreenValue, totalRange, pixelHeight);
			double referencePixelValue = this.GetPixelValue(this._referenceValue, totalRange, pixelHeight);
			double superiorYellowGreenPixelValue = this.GetPixelValue(this._superiorYellowGreenValue, totalRange, pixelHeight);
			double superiorYellowRedPixelValue = this.GetPixelValue(this._superiorYellowRedValue, totalRange, pixelHeight);
			double indicatorPixelValue = this.GetPixelValue(this._actualValue, totalRange, pixelHeight);

			// altezza zone inferiori
			this._inferiorRedZoneHeight = ( inferiorYellowRedPixelValue != referencePixelValue ) ? inferiorYellowRedPixelValue : 0;
			this._inferiorYellowZoneHeight = ( inferiorYellowGreenPixelValue != referencePixelValue ) ? inferiorYellowGreenPixelValue - this._inferiorRedZoneHeight : 0;
			this._inferiorGreenZoneHeight = referencePixelValue - this._inferiorYellowZoneHeight - this._inferiorRedZoneHeight;

			// altezza zone superiori
			this._superiorRedZoneHeight = ( superiorYellowRedPixelValue != referencePixelValue ) ? pixelHeight - superiorYellowRedPixelValue : 0;
			this._superiorYellowZoneHeight = ( superiorYellowGreenPixelValue != referencePixelValue ) ? superiorYellowRedPixelValue - superiorYellowGreenPixelValue : 0;
			this._superiorGreenZoneHeight = pixelHeight - this._superiorRedZoneHeight - this._superiorYellowZoneHeight - referencePixelValue;

			// indicatore
			this._indicatorHeight = indicatorPixelValue;
		}

		private double GetPixelValue ( double value, double totalRange, int pixelHeight )
		{
			if ( totalRange > 0 )
			{
				// calcolato con base scala a 0, come i pixel
				value -= this._inferiorLimitValue;
				return Math.Round(value * pixelHeight / totalRange);
			}

			return 0D;
		}

		#region IGaugeDataProvider Members

		private double _superiorLimit;
		public double SuperiorLimit
		{
			get { return this._superiorLimit; }
		}

		private double _inferiorLimit;
		public double InferiorLimit
		{
			get { return this._inferiorLimit; }
		}

		private double _superiorYellowRed;
		public double SuperiorYellowRed
		{
			get { return this._superiorYellowRed; }
		}

		private double _inferiorYellowRed;
		public double InferiorYellowRed
		{
			get { return this._inferiorYellowRed; }
		}

		private double _superiorYellowGreen;
		public double SuperiorYellowGreen
		{
			get { return this._superiorYellowGreen; }
		}

		private double _inferiorYellowGreen;
		public double InferiorYellowGreen
		{
			get { return this._inferiorYellowGreen; }
		}

		private double _referenceValue;
		public double ReferenceValue
		{
			get { return this._referenceValue; }
		}

		private double _actualValue;
		public double ActualValue
		{
			get { return this._actualValue; }
		}

		private double _superiorLimitValue;
		public double SuperiorLimitValue
		{
			get { return this._superiorLimitValue; }
		}

		private double _inferiorLimitValue;
		public double InferiorLimitValue
		{
			get { return this._inferiorLimitValue; }
		}

		private double _superiorYellowRedValue;
		public double SuperiorYellowRedValue
		{
			get { return this._superiorYellowRedValue; }
		}

		private double _inferiorYellowRedValue;
		public double InferiorYellowRedValue
		{
			get { return this._inferiorYellowRedValue; }
		}

		private double _superiorYellowGreenValue;
		public double SuperiorYellowGreenValue
		{
			get { return this._superiorYellowGreenValue; }
		}

		private double _inferiorYellowGreenValue;
		public double InferiorYellowGreenValue
		{
			get { return this._inferiorYellowGreenValue; }
		}

		private double _inferiorRedZoneHeight;
		public double InferiorRedZoneHeight
		{
			get { return this._inferiorRedZoneHeight; }
		}

		private double _inferiorYellowZoneHeight;
		public double InferiorYellowZoneHeight
		{
			get { return this._inferiorYellowZoneHeight; }
		}

		private double _inferiorGreenZoneHeight;
		public double InferiorGreenZoneHeight
		{
			get { return this._inferiorGreenZoneHeight; }
		}

		private double _superiorGreenZoneHeight;
		public double SuperiorGreenZoneHeight
		{
			get { return this._superiorGreenZoneHeight; }
		}

		private double _superiorYellowZoneHeight;
		public double SuperiorYellowZoneHeight
		{
			get { return this._superiorYellowZoneHeight; }
		}

		private double _superiorRedZoneHeight;
		public double SuperiorRedZoneHeight
		{
			get { return this._superiorRedZoneHeight; }
		}

		private double _indicatorHeight;
		public double IndicatorHeight
		{
			get { return this._indicatorHeight; }
		}

		public void Initialize()
		{
			this.InitializeProvider(this._referenceValue, this._actualValue, this._superiorLimit, this._superiorYellowRed, this._superiorYellowGreen, this._inferiorLimit, this._inferiorYellowRed, this._inferiorYellowGreen, this._pixelHeight);
		}

		#endregion
	}

	public class GaugeDataProviderInitializationException : Exception
	{
		public GaugeDataProviderInitializationException ( string message ) : base(message) { }
	}
}

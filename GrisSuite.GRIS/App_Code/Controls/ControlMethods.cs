﻿
using System;
using System.Web.UI.WebControls;

public static class ListItemCollectionExtender
{
	public static bool ContainsText ( this ListItemCollection items, string text )
	{
		foreach (ListItem item in items)
		{
			if ( item.Text.Equals(text, StringComparison.InvariantCultureIgnoreCase) ) return true;
		}

		return false;
	}

	public static bool ContainsValue ( this ListItemCollection items, string value )
	{
		foreach ( ListItem item in items )
		{
			if ( item.Value.Equals(value, StringComparison.InvariantCultureIgnoreCase) ) return true;
		}

		return false;
	}
}
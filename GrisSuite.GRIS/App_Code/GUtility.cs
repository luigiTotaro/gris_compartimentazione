using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using Resources;

public class GUtility
{
    public static string FormatMessaggi(string nomeStream, string messaggi)
    {
        string result = string.Empty;
        if (messaggi.Length > 0)
        {
            string[] temp1 = messaggi.Split(';');
            foreach (string s in temp1)
            {
                if (s.Length > 0)
                {
                    string[] temp2 = s.Split('=');
                    if (temp2.Length > 1)
                    {
                        if (!temp2[1].Equals("0"))
                        {
                            result += temp2[0] + "<br />";
                        }
                    }
                }
            }
        }
        return result;
    }

    public static string GetStringIP(string ip)
    {
        //estraggo la stringa IP dalla codifica del DB

        UInt32 ipNum = UInt32.Parse(ip);
        string IP = string.Format("{0}.", (ipNum >> 24));
        UInt32 mask = 0x00FFFFFF;
        IP += string.Format("{0}.", ((ipNum & mask) >> 16));
        mask = 0x0000FFFF;
        IP += ((ipNum & mask) >> 8) + ".";
        mask = 0x000000FF;
        IP += (ipNum & mask).ToString();

        return IP;
    }

    public static bool IsArray(long DevID, int StrID, int FieldID)
    {
        bool result = false;
        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.Query_getIsArray;
            query = query.Replace("DEVIDFIELD", DevID.ToString());
            query = query.Replace("STRIDFIELD", StrID.ToString());
            query = query.Replace("FIELDIDFIELD", FieldID.ToString());

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.Read())
            {
                if (reader[0].ToString().Equals("1"))
                {
                    result = true;
                }
            }
        }

        return result;
    }

    public static string FormatFieldName(string name, long devID, int strID, int fieldID, int arrayID)
    {
        string result = name;
        if (IsArray(devID, strID, fieldID))
        {
            int id = arrayID + 1;
            result = string.Format("{0} {1}", result, id);
        }

        return result;
    }

    public static void EnableImageButton(Page page, ImageButton img, bool enable)
    {
        if (page != null)
        {
            img.Enabled = enable;
            if (page.Request.Browser.Type != "IE7")
            {
                img.Style.Add(HtmlTextWriterStyle.Cursor, enable ? "pointer" : "default");
            }
        }
    }

    public static void EnableLinkButton(Page page, LinkButton lnk, bool enable)
    {
        if (enable)
        {
            lnk.Style[HtmlTextWriterStyle.Color] = "White";
            lnk.Style[HtmlTextWriterStyle.TextDecoration] = "none";
            lnk.Attributes["OnMouseOver"] = "this.style.color = '#68bfef'";
            lnk.Attributes["OnMouseOut"] = "this.style.color = 'White'";
        }
        else
        {
            lnk.Attributes["OnClick"] = "return false;";
            lnk.Style[HtmlTextWriterStyle.Color] = "#aaaaaa";
            lnk.Style[HtmlTextWriterStyle.TextDecoration] = "none";
            lnk.Style.Add(HtmlTextWriterStyle.Cursor, "default");
        }
    }

    public static string GetLocalServerTypeName(params string[] ServerID)
    {
        
        if (ServerID.Length == 0) return ConfigurationManager.AppSettings["LocalServerTypeName"] ?? "STLC1000";
        
        if ((string.IsNullOrEmpty(ServerID[0])) || !(Regex.IsMatch(ServerID[0], @"^\d+$"))) return ConfigurationManager.AppSettings["LocalServerTypeName"] ?? "STLC1000";
        

        string ret = "";

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = "SELECT ParameterValue FROM stlc_parameters WHERE (SrvID = " + ServerID[0] + ") AND (ParameterName = '" + GetLocalServerTypeParameterName() + "')";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    ret = reader[reader.GetOrdinal("ParameterValue")].ToString();
                }
            } else {

                ret = ConfigurationManager.AppSettings["LocalServerTypeName"] ?? "STLC1000";
            }

            conn.Close();

        }


        return ret;
               
    }

    public static string GetLocalServerTypeParameterName()
    {
        return ConfigurationManager.AppSettings["LocalServerTypeParameterName"] ?? "";
    }

    public static string GetLocalServerVersionParameterName()
    {
        return ConfigurationManager.AppSettings["LocalServerVersionParameterName"] ?? "STLCVersion";
    }

    public static string GetApplicationParameter(string parameterName)
    {
        return GetApplicationParameter(parameterName, true);
    }

    public static string GetApplicationParameter(string parameterName, bool fromCache)
    {
        string result = null;

        if (fromCache && HttpContext.Current != null && HttpContext.Current.Cache != null && HttpContext.Current.Cache[parameterName] != null)
        {
            // memorizzati in cache perch� parametri con scope applicativo
            result = HttpContext.Current.Cache[parameterName].ToString();
        }
        else
        {
            using (
                SqlConnection conn =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
            {
                string query = Resource.GetParameter;
                query = query.Replace("PARAMNAME", parameterName);

                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                object tmp = cmd.ExecuteScalar();

                if (tmp != null && !Convert.IsDBNull(tmp))
                {
                    result = tmp.ToString();
                }
                conn.Close();

                if (HttpContext.Current != null && HttpContext.Current.Cache != null && result != null)
                {
                    HttpContext.Current.Cache[parameterName] = result;
                }
            }
        }

        return result;
    }


    public static string GetUrlListen(long NodID)
    {
        //il risultato finale � composto da 2 parti
        //1) l'indirizzo della pagina php
        //2) l'indirizzo dell'Head ARM

        string result="";

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            //1) indirizzo pagina PHP
            string query = Resource.GetAwgUrlListen;
            query = query.Replace("NODID", NodID.ToString());

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            string result1 = "";
            string result2 = "";

            if (reader != null && reader.Read())
            {
                if (reader.FieldCount > 0) result1 = reader[0].ToString();
                else result1 = "";
            }

            reader.Close();

            if (result1 != "")
            {

                //2) indirizzo Head ARM
                query = Resource.GetListenArmAddrPort;
                query = query.Replace("NODID", NodID.ToString());

                cmd = new SqlCommand(query, conn);
                reader = cmd.ExecuteReader();

                if (reader != null && reader.Read())
                {
                    if (reader.FieldCount > 0)
                    {
                        var port = reader[1].ToString();
                        if (port != "") port = "&port=" + port;
                        result2 = "?ip=" + reader[0].ToString() + port;
                    }
                    else result2 = "";
                }

                reader.Close();

            }
            if ((result1 == "") || (result2 == "")) result = "";
            else result = result1 + result2;

            conn.Close();


        }


        return result;
    }


    public static string GetUrlCommand(long NodID)
    {
        //il risultato finale � composto da 2 parti
        //1) l'indirizzo della pagina php
        //2) l'indirizzo dell'Head ARM
        string result="";

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            //1) indirizzo pagina PHP
            string query = Resource.GetAwgUrlCommand;
            query = query.Replace("NODID", NodID.ToString());

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            string result1 = "";
            string result2 = "";

            if (reader != null && reader.Read())
            {
                if (reader.FieldCount > 0) result1 = reader[0].ToString();
                else result1 = "";
            }

            reader.Close();

            if (result1 != "")
            {

                //2) indirizzo Head ARM
                query = Resource.GetCommandArmAddrPort;
                query = query.Replace("NODID", NodID.ToString());

                cmd = new SqlCommand(query, conn);
                reader = cmd.ExecuteReader();

                if (reader != null && reader.Read())
                {
                    if (reader.FieldCount > 0)
                    {
                        var port = reader[1].ToString();
                        if (port != "") port = "&port=" + port;
                        result2 = "?ip=" + reader[0].ToString() + port;
                    }
                    else result2 = "";
                }

                reader.Close();

            }
            if ((result1 == "") || (result2 == "")) result = "";
            else result = result1 + result2;
            
            conn.Close();


        }


        return result;
    }

    public static string[] GetRegionExtData(string regId)
    {
        string[] result = new string[4];

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetRegionExtData;
            //tento di convertire l'id in bigint
            Int64 regInt = 0;
            if (regId == null) regId = "0";
            try
            {
                regInt = Int64.Parse(regId);
            }
            catch (FormatException e)
            {
                regInt = 0;
            }

            query = query.Replace("REGID", regInt.ToString());

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            
            if (reader != null && reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    result[i] = reader[i].ToString();
                }
            }            
            

            conn.Close();

        }


        return result;
    }



    public static List<BrokerRegionData> GetAllRegionExtData()
    {
        //string[] result = new string[3];

        List<BrokerRegionData> RegionData = new List<BrokerRegionData>();

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetAllRegionExtData;

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    RegionData.Add(new BrokerRegionData()
                    {
                        ID = (reader.GetInt64(reader.GetOrdinal("RegId"))).ToString(),
                        Name = reader[reader.GetOrdinal("name")].ToString(),
                        ip = reader[reader.GetOrdinal("BrokerIP")].ToString(),
                        username = reader[reader.GetOrdinal("Username")].ToString(),
                        password = reader[reader.GetOrdinal("Password")].ToString(),
                        port = reader[reader.GetOrdinal("Port")].ToString(),
                        urlCommandToAwg = reader[reader.GetOrdinal("UrlCommandToAwg")].ToString(),
                        urlListenFromAwg = reader[reader.GetOrdinal("UrlListenFromAwg")].ToString()
                    });

                }
            }           
            
            conn.Close();

        }


        return RegionData;
    }



    public static List<NotificationObjectsGroups> GetNotificationObjectDataSms(string searchText)
    {
        //string[] result = new string[3];

        List<NotificationObjectsGroups> GroupsData = new List<NotificationObjectsGroups>();

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = "select * from (";
                    query += "select os.ObjectStatusId, r.Name, 'Compartimento' as Tipologia, ngc.SmsGroupADName Gruppo from dbo.regions r JOIN dbo.object_status os on r.RegID=os.ObjectId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=1 AND r.Name LIKE '%" + searchText + "%' ";
                    query += "UNION ";
                    query += "select os.ObjectStatusId, z.Name, 'Linea' as Tipologia, ngc.SmsGroupADName Gruppo from dbo.zones z JOIN dbo.object_status os on z.ZonID=os.ObjectId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=2 AND z.Name LIKE '%" + searchText + "%' ";
                    query += "UNION ";
                    query += "select os.ObjectStatusId, n.Name, 'Stazione' as Tipologia, ngc.SmsGroupADName Gruppo from dbo.nodes n JOIN dbo.object_status os on n.NodID=os.ObjectId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=3 AND n.Name LIKE '%" + searchText + "%' ";
                    query += ") as tabella order by Tipologia, Name";

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    GroupsData.Add(new NotificationObjectsGroups()
                    {
                        ObjectStatusID = reader[reader.GetOrdinal("ObjectStatusId")].ToString(),
                        ObjectStatusName = reader[reader.GetOrdinal("Name")].ToString(),
                        ObjectStatusTipologia = reader[reader.GetOrdinal("Tipologia")].ToString(),
                        ObjectStatusWindowsGroups = reader[reader.GetOrdinal("Gruppo")].ToString(),
                        
 
                    });

                }
            }           
            
            conn.Close();

        }


        return GroupsData;
    }


    public static List<NotificationObjectsGroups> GetNotificationObjectDataMail(string searchText)
    {
        //string[] result = new string[3];

        List<NotificationObjectsGroups> GroupsData = new List<NotificationObjectsGroups>();

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
       
            string query = "select * from (";
                    query += "select os.ObjectStatusId, r.Name, '' as Sistema, 'Compartimento' as Tipologia, ngc.MailGroupADName Gruppo, 1 as ordine from dbo.regions r JOIN dbo.object_status os on r.RegID=os.ObjectId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=1 AND r.Name LIKE '%" + searchText + "%' ";
                    query += "UNION ";
                    query += "select os.ObjectStatusId, z.Name, '' as Sistema, 'Linea' as Tipologia, ngc.MailGroupADName Gruppo, 2 as ordine from dbo.zones z JOIN dbo.object_status os on z.ZonID=os.ObjectId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=2 AND z.Name LIKE '%" + searchText + "%' ";
                    query += "UNION ";
                    query += "select os.ObjectStatusId, n.Name, '' as Sistema, 'Stazione' as Tipologia, ngc.MailGroupADName Gruppo, 3 as ordine from dbo.nodes n JOIN dbo.object_status os on n.NodID=os.ObjectId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=3 AND n.Name LIKE '%" + searchText + "%' ";
                    query += "UNION ";
                    query += "select os.ObjectStatusId, n.Name, s.SystemDescription as Sistema, 'Sistema' as Tipologia, ngc.MailGroupADName Gruppo, 4 as ordine from dbo.object_status os JOIN dbo.node_systems ns on os.ObjectId = ns.NodeSystemsId JOIN dbo.systems s on s.SystemId=ns.SystemId JOIN dbo.Nodes n on n.NodId=ns.NodId LEFT JOIN dbo.notification_group_contacts ngc on os.ObjectStatusId = ngc.ObjectStatusId where os.ObjectTypeId=4 AND n.Name LIKE '%" + searchText + "%' ";
                    query += ") as tabella order by ordine, Name";

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    GroupsData.Add(new NotificationObjectsGroups()
                    {
                        ObjectStatusID = reader[reader.GetOrdinal("ObjectStatusId")].ToString(),
                        ObjectStatusName = reader[reader.GetOrdinal("Name")].ToString(),
                        ObjectStatusSistema = reader[reader.GetOrdinal("Sistema")].ToString(),
                        ObjectStatusTipologia = reader[reader.GetOrdinal("Tipologia")].ToString(),
                        ObjectStatusWindowsGroups = reader[reader.GetOrdinal("Gruppo")].ToString(),
 
                    });

                }
            }           
            
            conn.Close();

        }


        return GroupsData;
    }




    public static List<GroupsCCData> GetGroupsCCData()
    {
        //string[] result = new string[3];

        List<GroupsCCData> GroupsCCData = new List<GroupsCCData>();

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetGroupsCCData;

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    GroupsCCData.Add(new GroupsCCData()
                    {
                        GroupID = (reader.GetInt32(reader.GetOrdinal("GroupID"))).ToString(),
                        GroupName = reader[reader.GetOrdinal("GroupName")].ToString(),
                        GroupDescription = reader[reader.GetOrdinal("GroupDescription")].ToString(),
                        WindowsGroups = reader[reader.GetOrdinal("WindowsGroups")].ToString()
                    });

                }
            }           
            
            conn.Close();

        }


        return GroupsCCData;
    }


    public static int DeleteHistoryCommandRow(string id)
    {
        
        int result = 0;

        
        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.DelHistoryCommandRow;
            query = query.Replace("HISTORYID", id);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            result = cmd.ExecuteNonQuery();

            conn.Close();

        }

        
        return result;
    }


    public static int UpdateNotificationGroupDataSms(string objectStatusID, string windowsGroup)
    {
        
        int result = 0;

        
        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
           
            string query = "UPDATE [notification_group_contacts] SET SmsGroupADName = 'WINDOWSGROUPS' WHERE ObjectStatusId = 'OSID'; IF @@ROWCOUNT=0 insert into [notification_group_contacts] (ObjectStatusId,SmsGroupADName) values('OSID','WINDOWSGROUPS')";
            query = query.Replace("OSID", objectStatusID);
            query = query.Replace("WINDOWSGROUPS", windowsGroup);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            result = cmd.ExecuteNonQuery();

            conn.Close();

        }

        
        return result;
    }

    public static int UpdateNotificationGroupDataMail(string objectStatusID, string windowsGroup)
    {

        int result = 0;


        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {

            string query = "UPDATE [notification_group_contacts] SET MailGroupADName = 'WINDOWSGROUPS' WHERE ObjectStatusId = 'OSID'; IF @@ROWCOUNT=0 insert into [notification_group_contacts] (ObjectStatusId,MailGroupADName) values('OSID','WINDOWSGROUPS')";
            query = query.Replace("OSID", objectStatusID);
            query = query.Replace("WINDOWSGROUPS", windowsGroup);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            result = cmd.ExecuteNonQuery();

            conn.Close();

        }


        return result;
    }

    public static int UpdateGroupData(string groupID, string windowsGroup)
    {
        
        int result = 0;

        
        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.UpdGroupCCData;
            query = query.Replace("GROUPID", groupID);
            query = query.Replace("WINDOWSGROUPS", windowsGroup);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            result = cmd.ExecuteNonQuery();

            conn.Close();

        }

        
        return result;
    }
    

    public static int UpdateRegionExtData(string regId, string ip, string port, string username, string password)
    {
        int result = 0;


        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.UpdRegionExtData;
            query = query.Replace("REGID", regId);
            query = query.Replace("VALOREIP", ip);
            query = query.Replace("VALOREPORT", port);
            query = query.Replace("VALOREUSERNAME", username);
            query = query.Replace("VALOREPASSWORD", password);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            result = cmd.ExecuteNonQuery();

            conn.Close();

        }


        return result;
    }

    public static int UpdateAudioExtData(string regId, string command, string listen)
    {
        int result = 0;


        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.UpdAudioExtData;
            query = query.Replace("REGID", regId);
            query = query.Replace("VALORECOMMAND", command);
            query = query.Replace("VALORELISTEN", listen);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            result = cmd.ExecuteNonQuery();

            conn.Close();

        }


        return result;
    }

    public static List<DeviceCommandData> GetCommandsByDeviceType(string type)
    {

        List<DeviceCommandData> CommandData = new List<DeviceCommandData>();

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetDeviceCommandData;
            query = query.Replace("DEVTYPEID", type);
            
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    CommandData.Add(new DeviceCommandData()
                    {
                        CommandID = (reader.GetInt64(reader.GetOrdinal("CommandID"))).ToString(),
                        Class = reader[reader.GetOrdinal("Class")].ToString(),
                        Comando = reader[reader.GetOrdinal("Comando")].ToString(),
                        Descrizione = reader[reader.GetOrdinal("Descrizione")].ToString(),
                        Parametri = (bool)reader[reader.GetOrdinal("Parametri")],
                        Versione = reader[reader.GetOrdinal("Versione")].ToString(),
                        DeviceTypeID = reader[reader.GetOrdinal("DeviceTypeID")].ToString(),
                        DefaultParameter = reader[reader.GetOrdinal("DefaultParameter")].ToString()
                    });

                }
            }           
            
            conn.Close();


        }


        return CommandData;
    }

    public static string[] GetinfoDevice(long devId)
    {

        string[] ret = new string[6];

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetinfoDevice;
            query = query.Replace("DEVID", devId.ToString());
            
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    ret[0] = reader[reader.GetOrdinal("nodeId")].ToString();
                    ret[1] = reader[reader.GetOrdinal("regionId")].ToString();
                    ret[2] = reader[reader.GetOrdinal("type")].ToString();
                    ret[3] = reader[reader.GetOrdinal("nomeStazione")].ToString();
                    ret[4] = reader[reader.GetOrdinal("nomeDevice")].ToString();
                    ret[5] = reader[reader.GetOrdinal("systemID")].ToString();

                }
            }           
            
            conn.Close();


        }


        return ret;
    }

    public static string GetRealInfoDevice(string devType, long devId)
    {

        string ret = "";

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = "select dbo.GetDeviceInfoFromStreamsByDeviceType('"+devType+"', 1,"+devId+") as type";
            
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    ret = reader[reader.GetOrdinal("type")].ToString(); 
                }
            }           
            
            conn.Close();


        }


        return ret;
    }


    // restituisce una string descrittiva dell'utente loggato e dei gruppi in cui � incluso
    public static string GetUserInfos(bool concatGroups)
    {
        if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
        {
            string userGroups = "";
            WindowsIdentity wIdentity = (WindowsIdentity)HttpContext.Current.User.Identity;

            if (concatGroups)
            {
                userGroups = string.Format(" ({0})", GetUserGroups());
            }

            return wIdentity.Name + userGroups;
        }

        return "";
    }

    // restituisce la stringa descrittiva dei gruppi in cui � incluso l'utente loggato
    public static string GetUserGroups()
    {
        if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
        {
            string userGroups;
            WindowsIdentity wIdentity = (WindowsIdentity)HttpContext.Current.User.Identity;

            if ((userGroups = GrisSessionManager.Session.Get("UserGroups", "")) == "")
            {
                List<string> groups = new List<string>();

                IdentityReferenceCollection irc = wIdentity.Groups;

                if (irc != null)
                {
                    foreach (IdentityReference ir in irc)
                    {
                        NTAccount acc = (NTAccount)ir.Translate(typeof(NTAccount));
                        groups.Add(acc.Value);
                    }
                }

                string[] arrGroups = new string[groups.Count];
                groups.CopyTo(arrGroups);

                userGroups = string.Join(", ", arrGroups);

                GrisSessionManager.Session.Add("UserGroups", userGroups);
            }

            return userGroups;
        }

        return "";
    }

    public static Control FindControl(Control ctrl, string id)
    {
        Control innerCtrl = ctrl.FindControl(id);

        if (innerCtrl != null)
        {
            return innerCtrl;
        }

        foreach (Control childCtrl in ctrl.Controls)
        {
            innerCtrl = FindControl(childCtrl, id);

            if (innerCtrl != null)
            {
                return innerCtrl;
            }
        }

        return innerCtrl;
    }

    public static string GetAbsoluteUrl(string tildeUrl)
    {
        if (HttpContext.Current != null && HttpContext.Current.Request != null)
        {
            HttpRequest request = HttpContext.Current.Request;

            string appUrl = (request.Url.Scheme + Uri.SchemeDelimiter + request.Url.Authority +
                             (request.ApplicationPath.EndsWith("/") ? request.ApplicationPath : request.ApplicationPath + "/"));

            return Path.Combine(appUrl, new Control().ResolveClientUrl(tildeUrl));
        }

        return "";
    }

    public static string GetGetaUrl(long nodID, bool suppressPostback)
    {
        return
            string.Format(
                "try {{ var w = window.open('{0}?nod={1}'); w.moveTo(0,0); w.resizeTo(screen.width,screen.height); }} catch(err){{}}{2}",
                ConfigurationManager.AppSettings["GETAUrl"], nodID, (suppressPostback ? " return false;" : string.Empty));
    }

    public static string GetGetaUrl(bool suppressPostback)
    {
        if (HttpContext.Current != null && HttpContext.Current.CurrentHandler != null && HttpContext.Current.CurrentHandler is NavigationPage)
        {
            return GetGetaUrl(((NavigationPage)HttpContext.Current.CurrentHandler).NodID, suppressPostback);
        }

        return GetGetaUrl(-1, suppressPostback);
    }

    public static string GetGetaUrl()
    {
        return GetGetaUrl(true);
    }

    public static string GetSTLCConfiguratorWebUrl(string stlcIP, string username, string password, bool STLCConfiguratorIsGuestUserEnabled)
    {
        // http://%IP_STLC%/?ga=AES( "installatore" + ":" + MD5(IaP#Telefin) + ":" + IP + ":" + username )

        string credentials = EncryptString(string.Format("{0}:{1}:{2}:{3}", username,
                                                         password,
                                                         stlcIP, GetUserName()),
                                           Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["STLCConfiguratorAESKey"]),
                                           Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["STLCConfiguratorAESIV"]));

        return
            string.Format(
                "try {{ var w = window.open('{0}'); w.moveTo(0,0); w.resizeTo(screen.width,screen.height); }} catch(err){{}} return false;",
                ConfigurationManager.AppSettings["STLCConfiguratorWebUrl"].Replace("{STLC1000URL}", stlcIP).Replace("{CREDENTIALS}", credentials));
    }

    public static string GetUserName()
    {
        string userName = null;

        IPrincipal user = HttpContext.Current.User;
        if (user != null)
        {
            IIdentity identity = user.Identity;
            userName = identity.Name;
        }

        return userName;
    }

    private static string EncryptString(string plainText, byte[] Key, byte[] IV)
    {
        if (plainText == null || plainText.Length <= 0)
        {
            throw new ArgumentNullException("plainText");
        }
        if (Key == null || Key.Length <= 0)
        {
            throw new ArgumentNullException("Key");
        }
        if (IV == null || IV.Length <= 0)
        {
            throw new ArgumentNullException("Key");
        }

        byte[] encrypted;
        using (RijndaelManaged rijAlg = new RijndaelManaged())
        {
            rijAlg.Key = Key;
            rijAlg.IV = IV;
            rijAlg.Padding = PaddingMode.Zeros;

            ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
        }

        return GetStringFromBytes(encrypted);
    }

    private static string GetStringFromBytes(byte[] data)
    {
        if ((data == null) || (data.Length == 0))
        {
            return string.Empty;
        }

        StringBuilder returnString = new StringBuilder(data.Length * 2);
        foreach (byte b in data)
        {
            returnString.AppendFormat("{0:x2}", b);
        }

        return returnString.ToString();
    }

    private static byte[] GetBytesFromHexStringRepresentation(string data)
    {
        if (string.IsNullOrEmpty(data))
        {
            return null;
        }

        byte[] returnArray = new byte[data.Length / 2];

        if ((data.Length >= 2) && (data.Length % 2 == 0))
        {
            for (int dataIndex = 0; dataIndex < data.Length / 2; dataIndex++)
            {
                returnArray[dataIndex] = byte.Parse(data.Substring(dataIndex * 2, 2), NumberStyles.AllowHexSpecifier);
            }
        }
        else
        {
            return null;
        }

        return returnArray;
    }

    private static string DecryptString(byte[] cipherArray, byte[] Key, byte[] IV)
    {
        if (cipherArray == null || cipherArray.Length <= 0)
        {
            throw new ArgumentNullException("cipherArray");
        }
        if (Key == null || Key.Length <= 0)
        {
            throw new ArgumentNullException("Key");
        }
        if (IV == null || IV.Length <= 0)
        {
            throw new ArgumentNullException("Key");
        }

        string plaintext = null;

        using (RijndaelManaged rijAlg = new RijndaelManaged())
        {
            rijAlg.Key = Key;
            rijAlg.IV = IV;
            rijAlg.Padding = PaddingMode.Zeros;

            ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

            using (MemoryStream msDecrypt = new MemoryStream(cipherArray))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
        }

        return plaintext;
    }

    public static string GetListSelectedValues(ListItemCollection items)
    {
        string s = "|";
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].Selected)
            {
                s += items[i].Value + "|";
            }
        }
        return s;
    }

    public static void SetListSelectedValues(ListItemCollection items, string idString)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (idString.IndexOf("|" + items[i].Value + "|") >= 0)
            {
                items[i].Selected = true;
            }
        }
    }

    /// <summary>
    ///   Nome della funzione Javascript che visualizza il DIV per la waiting animation
    /// </summary>
    private const string SHOW_UPDATE_PROGRESS_DIV_JS_FUNCTION = "ToggleUpdateProgress";

    /// <summary>
    ///   Nome della funzione Javascript che nasconde il DIV per la waiting animation
    /// </summary>
    private const string HIDE_UPDATE_PROGRESS_DIV_JS_FUNCTION = "HideUpdateProgress";

    /// <summary>
    ///   Nomde del div HTML che contiene il controllo Silverlight con la mappa
    /// </summary>
    private const string SL_MAP_HTML_DIV = "silverlightControlHost";

    /// <summary>
    ///   Ritorna nome funzione Javascript per visualizzare la waiting animation
    /// </summary>
    /// <returns></returns>
    public static string GetUpdateProgressDivJsFunctionNameOnly()
    {
        return SHOW_UPDATE_PROGRESS_DIV_JS_FUNCTION;
    }

    /// <summary>
    ///   Ritorna nome funzione Javascript per nascondere la waiting animation
    /// </summary>
    /// <returns></returns>
    public static string GetHideProgressDivJsFunctionNameOnly()
    {
        return HIDE_UPDATE_PROGRESS_DIV_JS_FUNCTION;
    }

    /// <summary>
    ///   Ritorna nome elemento HTML che contiene la mappa Silverlight
    /// </summary>
    /// <returns></returns>
    public static string GetSilverligtMapHtmlDiv()
    {
        return SL_MAP_HTML_DIV;
    }

    /// <summary>
    ///   Ritorna il nome della funzione Javascript per visualizzare la waiting animation
    /// </summary>
    /// <param name = "contextScript">Script in cui � contenuta la funzione</param>
    /// <param name = "includeScriptTags">Include o meno i tags Script></param>
    /// <returns></returns>
    public static string GetHideProgressDivJsFunction(string contextScript, bool includeScriptTags)
    {
        return GetProgressDivJsFunction(HIDE_UPDATE_PROGRESS_DIV_JS_FUNCTION, contextScript, includeScriptTags);
    }

    /// <summary>
    ///   Ritorna script Javascript per visualizzare la waiting animation, da usare lato client
    /// </summary>
    /// <returns></returns>
    public static string GetUpdateProgressDivJsFunctionName()
    {
        return string.Format("{0}()", SHOW_UPDATE_PROGRESS_DIV_JS_FUNCTION);
    }

    /// <summary>
    ///   Ritorna il nome della funzione Javascript per visualizzare la waiting animation
    /// </summary>
    /// <returns></returns>
    public static string GetUpdateProgressDivJsFunction()
    {
        return GetUpdateProgressDivJsFunction(false);
    }

    public static bool CheckGETAUrl()
    {
        string GETAUrltest = ConfigurationManager.AppSettings.Get("GETAUrl");
        if (string.IsNullOrEmpty(GETAUrltest)) return false; else return true;
    }

    /// <summary>
    ///   Ritorna il nome della funzione Javascript per visualizzare la waiting animation
    /// </summary>
    /// <param name = "includeScriptTags">Include o meno i tags Script</param>
    /// <returns></returns>
    public static string GetUpdateProgressDivJsFunction(bool includeScriptTags)
    {
        return GetUpdateProgressDivJsFunction(null, includeScriptTags);
    }

    /// <summary>
    ///   Ritorna il nome della funzione Javascript per visualizzare la waiting animation
    /// </summary>
    /// <param name = "contextScript">Script in cui � contenuta la funzione</param>
    /// <param name = "includeScriptTags">Include o meno i tags Script></param>
    /// <returns></returns>
    public static string GetUpdateProgressDivJsFunction(string contextScript, bool includeScriptTags)
    {
        return GetProgressDivJsFunction(SHOW_UPDATE_PROGRESS_DIV_JS_FUNCTION, contextScript, includeScriptTags);
    }

    /// <summary>
    ///   Ritorna il nome di una delle funzioni Javascript per la waiting animation
    /// </summary>
    /// <param name = "functionName">nome della funzione</param>
    /// <param name = "contextScript">Script in cui � contenuta la funzione</param>
    /// <param name = "includeScriptTags">Include o meno i tags Script></param>
    /// <returns></returns>
    private static string GetProgressDivJsFunction(string functionName, string contextScript, bool includeScriptTags)
    {
        string script = (string.IsNullOrEmpty(contextScript)) ? string.Format("{0}();", functionName) : string.Format(contextScript, functionName);
        return (includeScriptTags) ? EmbedInScriptTag(script) : script;
    }

    /// <summary>
    ///   Aggiunge gli tag Script da una stringa di script
    /// </summary>
    /// <param name = "script">Stringa contenente il client script</param>
    /// <returns></returns>
    public static string EmbedInScriptTag(string script)
    {
        if (!string.IsNullOrEmpty(script))
        {
            return string.Format("<script type=\"text/javascript\">{0}</script>", script);
        }
        return "";
    }



    public static string GetRestConnectionString()
    {
        string result = "";

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetRestConnectionString;

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    result = reader[reader.GetOrdinal("parameterValue")].ToString();
                }
            }           
            
            conn.Close();

        }


        return result;
    }


    public static long InsertCommandRequest(string commandId, string versione, string parametri, string devId, string user)
    {
        long result = 0;

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.InsertCommandRequest;
            query = query.Replace("COMMANDID", commandId);
            query = query.Replace("VERSIONE", versione);
            query = query.Replace("PARAMETRI", parametri);
            query = query.Replace("USERNAME", user);
            query = query.Replace("DEVID", devId);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            object result_temp = cmd.ExecuteScalar();
            result = Convert.ToInt64(result_temp.ToString());

            conn.Close();

        }


        return result;
    }

    public static Boolean UpdateCommandRequest(long commandHistoryId, string result)
    {
       
        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.UpdateCommandRequest;

            String withDoubleQuotes = result.Replace("'", "''");
            query = query.Replace("COMMANDHISTORYID", commandHistoryId.ToString());
            query = query.Replace("RESPONSE", withDoubleQuotes);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            object result_temp = cmd.ExecuteScalar();
           
            conn.Close();

        }

        return true;
    }
    

    public static List<CommandSentData> GetSentCommandByID(string DevId, bool purgeScreenDump)
    {
        //string[] result = new string[3];

        List<CommandSentData> CommandSentData = new List<CommandSentData>();

        using (
            SqlConnection conn =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"].ConnectionString))
        {
            string query = Resource.GetSentCommandByID;
            query = query.Replace("DEVID", DevId);

            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    //controllo se � uno screen dump
                    String response = reader[reader.GetOrdinal("Response")].ToString();
                    if (purgeScreenDump)
                    {
                        if (response.StartsWith("{\"tipo\":\"screenDump\"")) response = "{\"tipo\":\"screenDump\"}";
                    }

                    CommandSentData.Add(new CommandSentData()
                    {
                        Command_historyID = reader[reader.GetOrdinal("Command_historyID")].ToString(),
                        CommandName = reader[reader.GetOrdinal("comando")].ToString(),
                        Versione = reader[reader.GetOrdinal("Versione")].ToString(),
                        Parametri = reader[reader.GetOrdinal("Parametri")].ToString(),
                        ExecutionUser = reader[reader.GetOrdinal("ExecutionUser")].ToString(),
                        DateSent = Convert.ToDateTime(reader[reader.GetOrdinal("DateSent")]).ToString("dd/MM/yyyy HH:mm:ss"),
                        DateReceived = reader.IsDBNull(reader.GetOrdinal("DateReceived")) ? null : Convert.ToDateTime(reader[reader.GetOrdinal("DateReceived")]).ToString("dd/MM/yyyy HH:mm:ss"),
                        Response = response
                    });

                }
            }           
            
            conn.Close();

        }


        return CommandSentData;
    }


}
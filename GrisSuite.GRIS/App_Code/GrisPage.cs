﻿
using System.Web.UI;

/// <summary>
/// Pagina base per le funzionalità comuni a tutte le pagine di Gris
/// </summary>
public abstract class GrisPage : Page, IGrisPage
{
	#region IGrisPage Members
	public Control FindControl ( Control ctrl, string id )
	{
		return GUtility.FindControl(ctrl, id);
	}

	public IGrisMasterPage GrisMaster
	{
		get
		{
			return this.Master as IGrisMasterPage;
		}
	}
	#endregion
}

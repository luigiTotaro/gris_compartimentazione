﻿
using FormulaEngine;
using GrisSuite.Data.Gris.ObjectStatusDSTableAdapters;

public class FormulaEngineService
{
	public void EvaluateDeviceAndParents ( long deviceId )
	{
		object_devicesTableAdapter odTa = new object_devicesTableAdapter();
		FormulaServiceClient formulaEngine = new FormulaServiceClient();

		var objectDevices = odTa.GetDataByObjectId(deviceId);

		if ( objectDevices != null && objectDevices.Count > 0 )
		{
			formulaEngine.EvaluateAndSaveObjectAndAncestors(objectDevices[0].ObjectStatusId);
		}
	}

	public void EvaluateRegion ( long regId )
	{
		object_regionsTableAdapter orTa = new object_regionsTableAdapter();
		FormulaServiceClient formulaEngine = new FormulaServiceClient();

		var objectRegions = orTa.GetDataByObjectId(regId);

		if ( objectRegions != null && objectRegions.Count > 0 )
		{
			formulaEngine.EvaluateAndSaveRegion(objectRegions[0].ObjectStatusId);
		}
	}
}
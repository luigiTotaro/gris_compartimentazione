﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[Serializable]
public class DeviceCommandData
{
    public DeviceCommandData()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string CommandID { get; set; }
    public string Class { get; set; }
    public string Comando { get; set; }
    public string Descrizione { get; set; }
    public bool Parametri { get; set; }
    public string Versione { get; set; }
    public string DeviceTypeID { get; set; }
    public string DefaultParameter { get; set; }

}    
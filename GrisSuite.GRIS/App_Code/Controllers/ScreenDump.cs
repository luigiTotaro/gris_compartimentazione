﻿using GrisSuite.Common;

public class ScreenDump
{
    public static bool CanUserViewScreenDump(long regId, int severityLevel)
    {
        return (GrisPermissions.IsInRole(regId, PermissionLevel.Level3) || ((severityLevel == 0) && GrisPermissions.IsInRole(regId, PermissionLevel.Level2)));
    }
}
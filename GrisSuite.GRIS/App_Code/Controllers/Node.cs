﻿
using GrisSuite.Common;

namespace Controllers
{
	public static class Node
	{
		public static bool CanUserViewDevices ( long regId, int numStlcOk, int numStlcInMaint, int numStlc )
		{
			// se la stazione non ha STLC1000 online o in maint mode e l'utente non appartiene almeno ad un gruppo di livello 3 non si può accedere alla pagina delle periferiche
			bool can = ( numStlcOk > 0 || numStlcInMaint > 0 || GrisPermissions.IsInRole(regId, PermissionLevel.Level3) )
			// se la stazione ha tutti gli stlc in maint mode e l'utente non appartiene almeno al gruppo GrisAdmin o GrisOperators non si può accedere alla pagina delle periferiche
						&& !( numStlcInMaint > 0 && numStlcInMaint == numStlc && !GrisPermissions.IsUserInAdminGroup() && !GrisPermissions.IsUserInAlertOperatorsGroup() );
			return can;
		}
	}
}

﻿using GrisSuite.Common;

public class StationListen
{
    public static bool CanUserViewStationListen(long regId, int severityLevel)
    {
        return (GrisPermissions.IsInRole(regId, PermissionLevel.Level3) || ((severityLevel == 0) && GrisPermissions.IsInRole(regId, PermissionLevel.Level2)));
    }
}
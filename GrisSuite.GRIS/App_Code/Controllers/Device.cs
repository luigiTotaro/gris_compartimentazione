﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using GrisSuite.Common;

/// <summary>
/// Questa classe implementa membri di business logic legati alla presentazione delle Periferiche
/// </summary>
namespace Controllers
{
	public class Device
	{
        public static string GetDeviceImageRelativePath(string imageName, bool hasTickets)
		{
            const string imgHasTicketsRelativePath = "~/IMG/Periferiche/hastickets.png";
            string imgRelativePath = (imageName.EndsWith(".png") ||
                imageName.EndsWith(".jpg") ||
                imageName.EndsWith(".jpeg") ||
                imageName.EndsWith(".gif"))
                ? string.Format("~/IMG/Periferiche/{0}", imageName) : string.Format("~/IMG/Periferiche/{0}.png", imageName); 
            string imgRelativePathHT = string.Format("~/IMG/Periferiche/{0}_hastickets.png", imageName);
			string imgHasTicketsPhysicalPath = HttpContext.Current.Server.MapPath(imgHasTicketsRelativePath);
			string imgPhysicalPath = HttpContext.Current.Server.MapPath(imgRelativePath);
			string imgPhysicalPathHT = HttpContext.Current.Server.MapPath(imgRelativePathHT);

			if ( File.Exists(imgPhysicalPath) )
			{
				if ( hasTickets && GrisPermissions.CanUserViewAlerts() )
				{
					if ( !File.Exists(imgPhysicalPathHT) )
					{
						try
						{
							using (Image deviceImage = Image.FromFile(imgPhysicalPath))
							{
								using (Image ticketImage = Image.FromFile(imgHasTicketsPhysicalPath))
								{
									Graphics graph = Graphics.FromImage(deviceImage);
									graph.DrawImage(ticketImage, new Point(0, 0));
									deviceImage.Save(imgPhysicalPathHT, ImageFormat.Png);						        						            
								}
							}							
						}
						catch ( Exception exc )
						{
							if ( HttpContext.Current != null ) Tracer.TraceMessage(HttpContext.Current, exc, true);
							return imgHasTicketsRelativePath;
						}
					}

					return imgRelativePathHT;
				}

				return imgRelativePath;
			}

			return "~/IMG/Periferiche/255.png";
		}

		public static string GetDeviceStatusImageRelativePath ( bool darkBackGround, int severityLevel)
		{
			return string.Format(( darkBackGround ) ? "~/IMG/interfaccia/tab_stato_{0}.gif" : "~/IMG/Interfaccia/GraphicalPanel/pan_status_{0}.gif", severityLevel);
		}

		public static string BuildScreenDumpUrl ( long devId )
		{
			StringBuilder handlerUrl = new StringBuilder(256);
			handlerUrl.Append("javascript:window.open('ScreenDump/ScreenDump.aspx?dev=");
			handlerUrl.Append(devId);
			handlerUrl.Append("', '_blank', 'height=500,width=700,status=no,toolbar=no,menubar=no,location=no,resizable=yes'); return false;");
			return handlerUrl.ToString();
		}

        public static string BuildListenDumpUrl(long devId)
        {
            StringBuilder handlerUrl = new StringBuilder(256);

            handlerUrl.Append("javascript:window.open('StationListen/StationListen.aspx?dev=");
            handlerUrl.Append(devId);
            handlerUrl.Append("', '_blank', 'height=290,width=400,status=no,toolbar=no,menubar=no,location=no,resizable=yes'); return false;");
            
            return handlerUrl.ToString();
        }

        public static string BuildCommandDumpUrl(long devId, bool autoScreenDump)
        {
            StringBuilder handlerUrl = new StringBuilder(256);

            var altezza = 800;
            var larghezza = 1000;
            if (autoScreenDump == true)
            {
                altezza = 200;
                larghezza = 600;
            }

            handlerUrl.Append("javascript:window.open('CommandControl/CommandControl.aspx?dev=");
            handlerUrl.Append(devId);
            handlerUrl.Append("&autoSD=");
            handlerUrl.Append(autoScreenDump);
            handlerUrl.Append("', '_blank', 'height="+altezza+",width="+larghezza+",status=no,scrollbars=yes,toolbar=no,menubar=no,location=no,resizable=yes'); return false;");
            //handlerUrl.Append("', '_blank'); return false;");
            
            return handlerUrl.ToString();
        }

		public static bool CanUserViewDeviceDetails(NavigationPage page, int severityLevel, int severityLevelDetail, string devType)
		{
			return CanUserViewDeviceDetails(page.RegID, severityLevel, severityLevelDetail, devType);
		}

		public static bool CanUserViewDeviceDetails ( long regId, int severityLevel, int severityLevelDetail, string devType )
		{
			return (((severityLevelDetail != 7 /* Diagnostica disattivata dall'operatore */) || GrisPermissions.IsUserInAdminGroup()) &&
					GrisPermissions.IsInRole(regId, PermissionLevel.Level2) &&
					((severityLevel != 255 && severityLevel != 3) || GrisPermissions.IsInRole(regId, PermissionLevel.Level3)));
		}

		public static bool CanUserChangeMaintenanceMode()
		{
			return (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup());
		}
	}
}

﻿using System;
using System.Collections.Generic;

public class DBSettings
{
	private DBSettings(){}
	
	# region Application Settings in DB
	// indica il numero di secondi di intervallo di refresh dei valori delle pagine
	public static int GetPageRefreshTime ()
	{
		string refreshSecsStr = "";
		int refreshSecs;

		refreshSecsStr = GUtility.GetApplicationParameter("PageRefreshTime");
		if ( string.IsNullOrEmpty(refreshSecsStr) || ( !int.TryParse(refreshSecsStr, out refreshSecs) ) )
		{
			refreshSecs = 60; // valore di default se il parametro non viene definito
		}

		return refreshSecs;
	}

    public static Boolean GetFDSVistaArmadioVisible()
	{
        string fdsVistaArmadioVisible = "";
        bool value = false;

        fdsVistaArmadioVisible = GUtility.GetApplicationParameter("FDSVistaArmadioVisible");
        if (bool.TryParse(fdsVistaArmadioVisible, out value))
        {
            return value;
        }
        else
        {
            return false;
        }
	}

	// indica un secondo intervallo di refresh (di regola maggiore del primo) dei valori di alcune pagine
	public static int GetLargerPageRefreshTime ()
	{
		string refreshSecsStr = "";
		int refreshSecs;

		refreshSecsStr = GUtility.GetApplicationParameter("LargerPageRefreshTime");
		if ( string.IsNullOrEmpty(refreshSecsStr) || ( !int.TryParse(refreshSecsStr, out refreshSecs) ) )
		{
			refreshSecs = 1800; // valore di default se il parametro non viene definito
		}

		return refreshSecs;
	}

	// indica se va segnalata la presenza anche di un solo STLC1000 offline nel compartimento
	public static bool GetRegionSTLCOfflineEnabled ()
	{
		string regSTLCoffStr = GUtility.GetApplicationParameter("RegionSTLCOfflineEnabled");
		bool regSTLCoff = Convert.ToBoolean(regSTLCoffStr ?? "false");

		return regSTLCoff;
	}

	// indica se va segnalata la presenza anche di un solo STLC1000 offline nella linea
	public static bool GetZoneSTLCOfflineEnabled ()
	{
		string zonSTLCoffStr = GUtility.GetApplicationParameter("ZoneSTLCOfflineEnabled");
		bool zonSTLCoff = Convert.ToBoolean(zonSTLCoffStr ?? "false");

		return zonSTLCoff;
	}



	// Tempo di attesa in millisecondi per dar modo all'STLC1000 di reinviare i dati delle periferiche
	public static int GetDevicesRefreshSleepTime ()
	{
		string devRefreshSleepTimeStr = "";
		int devRefreshSleepTime;

		devRefreshSleepTimeStr = GUtility.GetApplicationParameter("DevicesRefreshSleepTime");
		if ( string.IsNullOrEmpty(devRefreshSleepTimeStr) || ( !int.TryParse(devRefreshSleepTimeStr, out devRefreshSleepTime) ) )
		{
			devRefreshSleepTime = 3000; // valore di default se il parametro non viene definito
		}

		return devRefreshSleepTime;
	}

    /// <summary>
    /// Numero di righe oltre cui subentra la visualizzazione tabulare dei dispositivi
    /// </summary>
    /// <returns>Numero intero di righe oltre cui passare alla visualizzazione in tabella</returns>
    public static int GetTabularViewClippingRows() {
        int tabularViewClippingRows;

        string tabularViewClippingRowsStr = GUtility.GetApplicationParameter("TabularViewClippingRows");

        if (String.IsNullOrEmpty(tabularViewClippingRowsStr) || (!Int32.TryParse(tabularViewClippingRowsStr, out tabularViewClippingRows))) {
            tabularViewClippingRows = 12; // Numero di righe oltre cui subentra la visualizzazione tabulare dei dispositivi
        }

        return tabularViewClippingRows;
    }

    /// <summary>
    /// Ritorna il numero di minuti oltre cui è visualizzato un avviso relativo a problemi con il motore di scodamento righe di log
    /// </summary>
    /// <returns>Numero di minuti oltre cui è visualizzato un avviso relativo a problemi con il motore di scodamento righe di log</returns>
    public static int GetCacheLogMessagesObsoleteTimespanMinutes()
    {
        int cacheLogMessagesObsoleteTimespanMinutes;

        string cacheLogMessagesObsoleteTimespanMinutesStr = GUtility.GetApplicationParameter("CacheLogMessagesObsoleteTimespanMinutes");

        if (String.IsNullOrEmpty(cacheLogMessagesObsoleteTimespanMinutesStr) || (!Int32.TryParse(cacheLogMessagesObsoleteTimespanMinutesStr, out cacheLogMessagesObsoleteTimespanMinutes)))
        {
            cacheLogMessagesObsoleteTimespanMinutes = -1;
        }

        return cacheLogMessagesObsoleteTimespanMinutes;
    }

	/// <summary>
	/// Ritorna un booleano che indica se il bottone di accesso alla pagina dei filtri è attivo o meno
	/// </summary>
	/// <returns>Valore di ritorno del parametro FiltersButtonVisible</returns>
	public static Boolean GetFiltersButtonVisible()
	{
		bool value;

		string isFiltersButtonVisible = GUtility.GetApplicationParameter("FiltersButtonVisible");

		if (String.IsNullOrEmpty(isFiltersButtonVisible))
		{
			return false;
		}

		if (bool.TryParse(isFiltersButtonVisible, out value))
		{
			return value;
		}

		return false;
	}

    /// <summary>
    /// Ritorna un booleano che indica se il bottone di accesso alla pagina della mappa è attivo o meno
    /// </summary>
    /// <returns>Valore di ritorno del parametro MappaButtonVisible</returns>
    public static Boolean GetMappaButtonVisible()
    {
        bool value;

        string isMappaButtonVisible = GUtility.GetApplicationParameter("MappaButtonVisible");

        if (String.IsNullOrEmpty(isMappaButtonVisible))
        {
            return false;
        }

        if (bool.TryParse(isMappaButtonVisible, out value))
        {
            return value;
        }

        return false;
    }


    /// <summary>
    /// Ritorna un booleano che indica se il bottone di accesso alla pagina dei reports è attivo o meno
    /// </summary>
    /// <returns>Valore di ritorno del parametro ReportsButtonVisible</returns>
    public static Boolean GetReportsButtonVisible()
    {
        bool value;

        string isReportsButtonVisible = GUtility.GetApplicationParameter("ReportsButtonVisible");

        if (String.IsNullOrEmpty(isReportsButtonVisible))
        {
            return false;
        }

        if (bool.TryParse(isReportsButtonVisible, out value))
        {
            return value;
        }

        return false;
    }



    /// <summary>
    /// Ritorna un booleano che indica se il bottone degli allarmi è attivo o meno
    /// </summary>
    /// <returns>Valore di ritorno del parametro FiltersButtonVisible</returns>
    public static Boolean GetAlertsButtonVisible()
    {
        bool value;

        string isAlertsButtonVisible = GUtility.GetApplicationParameter("AlertsButtonVisible");

        if (String.IsNullOrEmpty(isAlertsButtonVisible))
        {
            return false;
        }

        if (bool.TryParse(isAlertsButtonVisible, out value))
        {
            return value;
        }

        return false;
    }


    /// <summary>
    /// Ritorna l'url da assegnare all'attributo src del tag audio utilizzato per l'ascolto da tromba o da sonda in stazione
    /// </summary>
    /// <returns>Url recuperato dalla tabella parameters del DB</returns>
    public static string GetUrlListenFromAwg()
    {
        string sUrl = "";
 
        string sTmp = GUtility.GetApplicationParameter("UrlListenFromAwg", false);

        if (!String.IsNullOrEmpty(sTmp))
        {
            sUrl = sTmp;
        }

        return sUrl;
    }

    /// <summary>
    /// Ritorna l'url da assegnare all'attributo src del tag audio utilizzato per l'ascolto da tromba o da sonda in stazione
    /// </summary>
    /// <returns>Url recuperato dalla tabella parameters del DB</returns>
    public static string GetUrlListenFromAwgNew(long NodID)
    {
        string sUrl = "";

        string sTmp = GUtility.GetUrlListen(NodID);

        if (!String.IsNullOrEmpty(sTmp))
        {
            sUrl = sTmp;
        }

        return sUrl;
    }

    /// <summary>
    /// Ritorna la porta da associare all'url dell'attributo src del tag audio
    /// utilizzato per l'ascolto da tromba da sonda in stazione
    /// </summary>
    /// <returns></returns>
    public static long GetPortListenFromAwg()
    {
        long lPort = -1;

        string sTmp = GUtility.GetApplicationParameter("UrlListenFromAwg", false);

        if (!String.IsNullOrEmpty(sTmp))
        {
            Uri objUri = new Uri(sTmp);

            string sPort = System.Web.HttpUtility.ParseQueryString(objUri.Query).Get("port");

            if (!String.IsNullOrEmpty(sPort))
            {
                long lTmp;
                if (long.TryParse(sPort, out lTmp))
                {
                    lPort = lTmp;
                }
            }
        }

        return lPort;
    }

    /// <summary>
    /// Ritorna la porta da associare all'url dell'attributo src del tag audio
    /// utilizzato per l'ascolto da tromba da sonda in stazione
    /// </summary>
    /// <returns></returns>
    public static long GetPortListenFromAwgNew(long NodID)
    {
        long lPort = -1;

        string sTmp = GUtility.GetUrlListen(NodID);

        if (!String.IsNullOrEmpty(sTmp))
        {
            Uri objUri = new Uri(sTmp);

            string sPort = System.Web.HttpUtility.ParseQueryString(objUri.Query).Get("port");

            if (!String.IsNullOrEmpty(sPort))
            {
                long lTmp;
                if (long.TryParse(sPort, out lTmp))
                {
                    lPort = lTmp;
                }
            }
        }

        return lPort;
    }


    /// <summary>
    /// Ritorna l'url da utilizzare per attivare/disattivare l'ascolto dal server awg
    /// </summary>
    /// <returns>Url recuperato dalla tabella parameters del DB</returns>
    public static string GetUrlCommandToAwg()
    {
        string sUrl = "";

        string sTmp = GUtility.GetApplicationParameter("UrlCommandToAwg", false);

        if (!String.IsNullOrEmpty(sTmp))
        {
            sUrl = sTmp;
        }

        return sUrl;
    }

    /// <summary>
    /// Ritorna l'url da utilizzare per attivare/disattivare l'ascolto dal server awg
    /// </summary>
    /// <returns>Url recuperato dalla tabella parameters del DB</returns>
    public static string GetUrlCommandToAwgNew(long NodID)
    {
        string sUrl = "";

        string sTmp = GUtility.GetUrlCommand(NodID);

        if (!String.IsNullOrEmpty(sTmp))
        {
            sUrl = sTmp;
        }

        return sUrl;
    }

    /// <summary>
    /// Ritorna la tipologia di periferica utilizzata per l'ascolto
    /// </summary>
    /// <returns>Tipo di periferica</returns>
    public static string GetAwgClientDeviceType()
    {
        string sType = "";

        string sTmp = GUtility.GetApplicationParameter("AwgClientDeviceType", false);

        if (!String.IsNullOrEmpty(sTmp))
        {
            sType = sTmp;
        }

        return sType;
    }

    /// <summary>
    /// Ritorna true o false se quel device type ha dei comandi da poter essere eseguiti
    /// </summary>
    /// <returns>true/false</returns>
    public static bool HasDeviceCommand(string type)
    {
        bool ret = false;

        List<DeviceCommandData> CommandData = GUtility.GetCommandsByDeviceType(type);

        if (CommandData.Count > 0) ret = true;

        /*
        if (!String.IsNullOrEmpty(sTmp))
        {
            sType = sTmp;
        }
        */
        return ret;
    }

    /// <summary>
    /// Ritorna la lista dei comandi da poter essere eseguiti su quel device type
    /// </summary>
    /// <returns>true/false</returns>
    public static List<DeviceCommandData> GetDeviceCommandList(string type)
    {
        List<DeviceCommandData> CommandData = GUtility.GetCommandsByDeviceType(type);

        return CommandData;
    }


    /// <summary>
    /// Ritorna regionId e NodeID per quel deviceID
    /// </summary>
    /// <returns>Tipo di periferica</returns>
    public static string[] GetinfoDevice(long devId)
    {
        string[] ret = GUtility.GetinfoDevice(devId);

        return ret;
    }

    /// <summary>
    /// Ritorna il device type reale per quel deviceID
    /// </summary>
    /// <returns>Tipo di periferica</returns>
    public static string GetRealInfoDevice(string devType, long devId)
    {
        string ret = GUtility.GetRealInfoDevice(devType, devId);

        return ret;
    }

    


	# endregion
}

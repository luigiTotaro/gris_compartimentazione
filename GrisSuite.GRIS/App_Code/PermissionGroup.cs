﻿
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ResourcesDSTableAdapters;

public class PermissionGroup
{
    public int GroupId { get; set; }
    public string GroupName { get; set; }
    public bool Associated { get; set; }

    public static IEnumerable<PermissionGroup> GetAssociatedGroups(int selectedReportId)
    {
        var ta = new ResourceGroupAssociationTableAdapter();
        var groupTbl = ta.GetData(selectedReportId);

        var groups = from ResourcesDS.ResourceGroupAssociationRow groupRow in groupTbl
                     select new PermissionGroup { GroupId = groupRow.GroupID, GroupName = groupRow.GroupName, Associated = groupRow.Associated };
        return groups;
    }
}
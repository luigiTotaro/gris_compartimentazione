using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;

/// <summary>
/// Summary description for SessionManager
/// </summary>
/// 
public delegate T SessionManagerCallbackHandler<T>();

# region SessionManager

public class SessionManager
{
	# region Session CRUD Members

	public bool Add<T>(string key, T value)
	{
		if (HttpContext.Current != null && HttpContext.Current.Session != null)
		{
			HttpContext.Current.Session[key] = value;
			return true;
		}

		return false;
	}

	public void Remove(string key)
	{
		if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[key] != null)
		{
			HttpContext.Current.Session.Remove(key);
		}
	}

	public void RemoveAll()
	{
		if (HttpContext.Current != null && HttpContext.Current.Session != null)
		{
			HttpContext.Current.Session.RemoveAll();
		}
	}

	public T Get<T>(string key)
	{
		return this.Get(key, null, default(T));
	}

	public T Get<T>(string key, T defaultValue)
	{
		return this.Get(key, null, defaultValue);
	}

	public T Get<T>(string key, SessionManagerCallbackHandler<T> callback)
	{
		return this.Get(key, callback, default(T));
	}

	public T Get<T>(string key, SessionManagerCallbackHandler<T> callback, T defaultValue)
	{
		if (HttpContext.Current != null)
		{
			if (HttpContext.Current.Session != null && HttpContext.Current.Session[key] != null &&
				!string.IsNullOrEmpty(HttpContext.Current.Session[key].ToString())) // se il tipo in sessione � una stringa non deve essere empty
			{
				try
				{
					return (T)HttpContext.Current.Session[key];
				}
				catch (InvalidCastException)
				{
					throw new ArgumentException("Impossibile convertire il tipo passato al metodo generico Get<T>");
				}
			}

			if (callback != null)
			{
				T t = callback.Invoke();
				if (!Equals(t, null))
				{
					if (HttpContext.Current.Session != null)
					{
						HttpContext.Current.Session[key] = t;
					}
					return t;
				}
			}

			if (!Equals(defaultValue, null))
			{
				return defaultValue;
			}

			Tracer.TraceMessage(HttpContext.Current,
								string.Format(
									"La chiave '{0}' non � pi� presente in sessione, l'utente � stato redirezionato alla pagina 'SessionEnd.aspx'.",
									key));
			HttpContext.Current.Response.Redirect("~/SessionEnd.aspx");
		}

#if (!DEBUG)
		Trace.WriteLine(
			string.Format("{0:dd/MM/yyyy HH:mm:ss.fffffffzzz} La chiave '{1}' non � pi� presente in sessione, contesto http non disponibile.",
						  DateTime.Now, key));
#endif

		return default(T);
	}

	public bool Exists(string key)
	{
		return (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[key] != null);
	}

	# endregion

	# region Session Group Members

	private bool AddInSessionGroup(string group, string key)
	{
		var groupValue = this.Get(group, "");

		List<string> keys = groupValue.Length > 0 ? new List<string>(groupValue.Split(',')) : new List<string>();

		if (!keys.Contains(key))
		{
			keys.Add(key);
		}

		return this.Add(group, string.Join(",", keys.ToArray()));
	}

	private bool RemoveFromSessionGroup(string group, string key)
	{
		var groupValue = this.Get(group, "");

		List<string> keys = groupValue.Length > 0 ? new List<string>(groupValue.Split(',')) : new List<string>();

		keys.Remove(key);

		return this.Add(group, string.Join(",", keys.ToArray()));
	}

	public bool AddWithGroup<T>(string key, T value, string group)
	{
		return this.Add(key, value) && this.AddInSessionGroup(group, key);
	}

	public bool RemoveWithGroup(string key, string group)
	{
		this.Remove(key);
		return this.RemoveFromSessionGroup(group, key);
	}

	public void RemoveAllGroup(string group)
	{
		var groupValue = this.Get(group, "");
		string[] keys = groupValue.Split(',');

		foreach (string key in keys)
		{
			this.Remove(key);
		}

		this.Remove(group);
	}

	# endregion
}

# endregion

# region GrisSessionManager

public class GrisSessionManager
{
	public static SessionManager Session
	{
		get { return new SessionManager(); }
	}

	public static void RemoveNavigationGroup(GrisSessionGroup group)
	{
		switch (group)
		{
			case GrisSessionGroup.Region:
				{
					Session.RemoveAllGroup(group.ToString());
					Session.RemoveAllGroup(GrisSessionGroup.Zone.ToString());
					Session.RemoveAllGroup(GrisSessionGroup.Node.ToString());
					break;
				}
			case GrisSessionGroup.Zone:
				{
					Session.RemoveAllGroup(group.ToString());
					Session.RemoveAllGroup(GrisSessionGroup.Node.ToString());
					break;
				}
			case GrisSessionGroup.Node:
				{
					Session.RemoveAllGroup(group.ToString());
					break;
				}
		}
	}

	public static void RemoveAllNavigationData()
	{
		RemoveNavigationGroup(GrisSessionGroup.Region);
	}

	# region Specific Session Properties

	/************************** SelectedSystem *****************************/
	public const string SelectedSystemKey = "SelectedSystem";

	public static int SelectedSystem
	{
		get { return Session.Get(SelectedSystemKey, -1); }

		set { Session.AddWithGroup(SelectedSystemKey, value, GrisSessionGroup.Node.ToString()); }
	}

	/************************** End SelectedSystem *****************************/

	/************************** SelectedNodeSystem *****************************/
	public const string SelectedNodeSystemKey = "SelectedNodeSystem";

	public static long SelectedNodeSystem
	{
		get { return Session.Get(SelectedNodeSystemKey, (long)-1); }

		set { Session.AddWithGroup(SelectedNodeSystemKey, value, GrisSessionGroup.Node.ToString()); }
	}

	/************************** End SelectedNodeSystem *****************************/

	/************************** SystemDevicesCount *****************************/
	public const string SystemDevicesCountKey = "SystemDevicesCount";

	public static int SystemDevicesCount
	{
		get { return Session.Get(SystemDevicesCountKey, -1); }

		set { Session.AddWithGroup(SystemDevicesCountKey, value, GrisSessionGroup.Node.ToString()); }
	}

	/************************** End SystemDevicesCount *****************************/

	/************************** SelectedSystemDescription *****************************/
	public const string SelectedSystemDescriptionKey = "SelectedSystemDescription";

	public static string SelectedSystemDescription
	{
		get { return Session.Get(SelectedSystemDescriptionKey, ""); }

		set { Session.AddWithGroup(SelectedSystemDescriptionKey, value, GrisSessionGroup.Node.ToString()); }
	}

	public static bool SelectedSystemDescriptionExists
	{
		get { return Session.Exists(SelectedSystemDescriptionKey); }
	}

	/************************** End SelectedSystemDescription *****************************/

	/************************** GrisLayoutMode *****************************/
	public const string GrisLayoutModeKey = "GrisLayoutMode";

	public static GrisLayoutMode GrisLayoutMode
	{
		get { return (GrisLayoutMode)Enum.Parse(typeof(GrisLayoutMode), Session.Get(GrisLayoutModeKey, "Map")); }

		set { Session.Add(GrisLayoutModeKey, value.ToString()); }
	}

	public static bool GrisLayoutModeExists
	{
		get { return Session.Exists(GrisLayoutModeKey); }
	}

	public static void GrisLayoutModeReset()
	{
		Session.Remove(GrisLayoutModeKey);
		Session.RemoveAllGroup(GrisSessionGroup.Node.ToString());
	}

	/************************** End GrisLayoutMode *****************************/

	/************************** CheckTableItaOpen *****************************/
	public const string CheckTableItaOpenKey = "CheckTableItaOpen";

	public static bool? CheckTableItaOpen
	{
		get { return Session.Get(CheckTableItaOpenKey, false); }

		set { Session.Add(CheckTableItaOpenKey, value); }
	}

	public static bool CheckTableItaOpenExists
	{
		get { return Session.Exists(CheckTableItaOpenKey); }
	}

	/************************** End CheckTableItaOpen *****************************/

	/************************** OriginalMapLayoutMode *****************************/
	public const string OriginalMapLayoutModeKey = "OriginalMapLayoutMode";

	public static MapLayoutMode OriginalMapLayoutMode
	{
		get { return (MapLayoutMode)Enum.Parse(typeof(MapLayoutMode), Session.Get(OriginalMapLayoutModeKey, "None")); }

		set { Session.Add(OriginalMapLayoutModeKey, value.ToString()); }
	}

	public static bool OriginalMapLayoutModeExists
	{
		get { return Session.Exists(OriginalMapLayoutModeKey); }
	}

	/************************** End OriginalMapLayoutMode *****************************/

	/************************** LinkCompartimentoText *****************************/
	public const string LinkCompartimentoTextKey = "LinkCompartimentoText";

	public static string LinkCompartimentoText
	{
		get { return Session.Get(LinkCompartimentoTextKey, ""); }

		set { Session.AddWithGroup(LinkCompartimentoTextKey, value, GrisSessionGroup.Region.ToString()); }
	}

	public static bool LinkCompartimentoTextExists
	{
		get { return Session.Exists(LinkCompartimentoTextKey); }
	}

	/************************** End LinkCompartimentoText *****************************/

	/************************** LinkCompartimentoTooltip *****************************/
	public const string LinkCompartimentoTooltipKey = "LinkCompartimentoTooltip";

	public static string LinkCompartimentoTooltip
	{
		get { return Session.Get(LinkCompartimentoTooltipKey, ""); }

		set { Session.AddWithGroup(LinkCompartimentoTooltipKey, value, GrisSessionGroup.Region.ToString()); }
	}

	public static bool LinkCompartimentoTooltipExists
	{
		get { return Session.Exists(LinkCompartimentoTooltipKey); }
	}

	/************************** End LinkCompartimentoTooltip *****************************/

	/************************** LinkLineaText *****************************/
	public const string LinkLineaTextKey = "LinkLineaText";

	public static string LinkLineaText
	{
		get { return Session.Get(LinkLineaTextKey, ""); }

		set { Session.AddWithGroup(LinkLineaTextKey, value, GrisSessionGroup.Zone.ToString()); }
	}

	public static bool LinkLineaTextExists
	{
		get { return Session.Exists(LinkLineaTextKey); }
	}

	/************************** End LinkLineaText *****************************/

	/************************** LinkLineaTooltip *****************************/
	public const string LinkLineaTooltipKey = "LinkLineaTooltip";

	public static string LinkLineaTooltip
	{
		get { return Session.Get(LinkLineaTooltipKey, ""); }

		set { Session.AddWithGroup(LinkLineaTooltipKey, value, GrisSessionGroup.Zone.ToString()); }
	}

	public static bool LinkLineaTooltipExists
	{
		get { return Session.Exists(LinkLineaTooltipKey); }
	}

	/************************** End LinkLineaTooltip *****************************/

	/************************** LinkStazioneText *****************************/
	public const string LinkStazioneTextKey = "LinkStazioneText";

	public static string LinkStazioneText
	{
		get { return Session.Get(LinkStazioneTextKey, ""); }

		set { Session.AddWithGroup(LinkStazioneTextKey, value, GrisSessionGroup.Node.ToString()); }
	}

	public static bool LinkStazioneTextExists
	{
		get { return Session.Exists(LinkStazioneTextKey); }
	}

	/************************** End LinkStazioneText *****************************/

	/************************** LinkStazioneTooltip *****************************/
	public const string LinkStazioneTooltipKey = "LinkStazioneTooltip";

	public static string LinkStazioneTooltip
	{
		get { return Session.Get(LinkStazioneTooltipKey, ""); }

		set { Session.AddWithGroup(LinkStazioneTooltipKey, value, GrisSessionGroup.Node.ToString()); }
	}

	public static bool LinkStazioneTooltipExists
	{
		get { return Session.Exists(LinkStazioneTooltipKey); }
	}

	/************************** End LinkStazioneTooltip *****************************/

	/************************** LastUrl *****************************/
	public const string LastUrlKey = "LastUrl";

	public static string LastUrl
	{
		get { return Session.Get(LastUrlKey, "~/Italia.aspx"); }

		set { Session.Add(LastUrlKey, value); }
	}

	public static bool LastUrlExists
	{
		get { return Session.Exists(LastUrlKey); }
	}

	/************************** End LastUrl *****************************/

	/************************** LastMapUrl *****************************/
	public const string LastMapUrlKey = "LastMapUrl";

	public static string LastMapUrl
	{
		get { return Session.Get(LastMapUrlKey, "~/Italia.aspx"); }

		set { Session.Add(LastMapUrlKey, value); }
	}

	public static bool LastMapUrlExists
	{
		get { return Session.Exists(LastMapUrlKey); }
	}

	/************************** End LastMapUrl *****************************/

	/************************** LastFiltersUrl *****************************/
	public const string LastFiltersUrlKey = "LastFiltersUrl";

	public static string LastFiltersUrl
	{
		get { return Session.Get(LastFiltersUrlKey, "~/Filters.aspx"); }

		set { Session.Add(LastFiltersUrlKey, value); }
	}

	public static bool LastFiltersUrlExists
	{
		get { return Session.Exists(LastFiltersUrlKey); }
	}

	/************************** End LastFiltersUrl *************************/

	/************************** LastReportsUrl *****************************/
	public const string LastReportsUrlKey = "LastReportsUrl";

	public static string LastReportsUrl
	{
		get { return Session.Get(LastReportsUrlKey, "~/Reports.aspx"); }

		set { Session.Add(LastReportsUrlKey, value); }
	}

	public static bool LastReportsUrlExists
	{
		get { return Session.Exists(LastReportsUrlKey); }
	}

	/************************** End LastReportsUrl *****************************/

	/************************** SelectedDeviceType *****************************/
	public const string SelectedDeviceTypeKey = "SelectedDeviceType";

	public static string SelectedDeviceType
	{
		get { return Session.Get(SelectedDeviceTypeKey, ""); }

		set { Session.AddWithGroup(SelectedDeviceTypeKey, value, GrisSessionGroup.Node.ToString()); }
	}

	public static bool SelectedDeviceTypeExists
	{
		get { return Session.Exists(SelectedDeviceTypeKey); }
	}

	/************************** End SelectedDeviceType *****************************/

	/************************** SelectedAlert *****************************/
	public const string SelectedAlertKey = "SelectedAlert";

	public static Guid SelectedAlert
	{
		get { return Session.Get(SelectedAlertKey, Guid.Empty); }

		set { Session.Add(SelectedAlertKey, value); }
	}

	public static bool SelectedAlertExists
	{
		get { return Session.Exists(SelectedAlertKey); }
	}

	/************************** End SelectedAlert *****************************/

	/************************** SelectedDeviceFilterName *****************************/
	public const string SelectedDeviceFilterNameKey = "SelectedDeviceFilterName";

	public static string SelectedDeviceFilterName
	{
		get { return Session.Get(SelectedDeviceFilterNameKey, String.Empty); }

		set { Session.Add(SelectedDeviceFilterNameKey, value); }
	}

	public static bool SelectedDeviceFilterNameExists
	{
		get { return Session.Exists(SelectedDeviceFilterNameKey); }
	}

	/************************** End SelectedDeviceFilterName *****************************/

	/************************** SelectedDeviceFilterId *****************************/
	public const string SelectedDeviceFilterIdKey = "SelectedDeviceFilterId";

	public static long SelectedDeviceFilterId
	{
		get { return Session.Get(SelectedDeviceFilterIdKey, 0L); }

		set { Session.Add(SelectedDeviceFilterIdKey, value); }
	}

	public static bool SelectedDeviceFilterIdExists
	{
		get { return Session.Exists(SelectedDeviceFilterIdKey); }
	}

	/************************** End SelectedDeviceFilterId *****************************/

	/************************** CompartimentoSeverita *****************************/
	public const string CompartimentoSeveritaKey = "CompartimentoSeverita";

	public static int CompartimentoSeverita
	{
		get { return Session.Get(CompartimentoSeveritaKey, -1); }

		set { Session.AddWithGroup(CompartimentoSeveritaKey, value, GrisSessionGroup.Region.ToString()); }
	}

	public static bool CompartimentoSeveritaExists
	{
		get { return Session.Exists(CompartimentoSeveritaKey); }
	}

	/************************** End CompartimentoSeverita *****************************/

	/************************** LineaSeverita *****************************/
	public const string LineaSeveritaKey = "LineaSeverita";

	public static int LineaSeverita
	{
		get { return Session.Get(LineaSeveritaKey, -1); }

		set { Session.AddWithGroup(LineaSeveritaKey, value, GrisSessionGroup.Zone.ToString()); }
	}

	public static bool LineaSeveritaExists
	{
		get { return Session.Exists(LineaSeveritaKey); }
	}

	/************************** End LineaSeverita *****************************/

	/************************** StazioneSeverita *****************************/
	public const string StazioneSeveritaKey = "StazioneSeverita";

	public static int StazioneSeverita
	{
		get { return Session.Get(StazioneSeveritaKey, -1); }

		set { Session.AddWithGroup(StazioneSeveritaKey, value, GrisSessionGroup.Node.ToString()); }
	}

	public static bool StazioneSeveritaExists
	{
		get { return Session.Exists(StazioneSeveritaKey); }
	}

	/************************** End LineaSeverita *****************************/

	/************************** InformationBarFirstLineMessage *****************************/
	public const string InformationBarFirstLineMessageKey = "InformationBarFirstLineMessage";

	public static string InformationBarFirstLineMessage
	{
		get { return Session.Get(InformationBarFirstLineMessageKey, string.Empty); }

		set { Session.Add(InformationBarFirstLineMessageKey, value); }
	}

	public static bool InformationBarFirstLineMessageExists
	{
		get { return Session.Exists(InformationBarFirstLineMessageKey); }
	}

	/************************** End InformationBarFirstLineMessage *****************************/

	/************************** InformationBarSecondLineMessage *****************************/
	public const string InformationBarSecondLineMessageKey = "InformationBarSecondLineMessage";

	public static string InformationBarSecondLineMessage
	{
		get { return Session.Get(InformationBarSecondLineMessageKey, string.Empty); }

		set { Session.Add(InformationBarSecondLineMessageKey, value); }
	}

	public static bool InformationBarSecondLineMessageExists
	{
		get { return Session.Exists(InformationBarSecondLineMessageKey); }
	}

	/************************** End InformationBarSecondLineMessage *****************************/

	/************************** InformationBarFirstLineIcon *****************************/
	public const string InformationBarFirstLineIconKey = "InformationBarFirstLineIcon";

	public static InformationBarIcon InformationBarFirstLineIcon
	{
		get { return (InformationBarIcon)Enum.Parse(typeof(InformationBarIcon), Session.Get(InformationBarFirstLineIconKey, "None")); }

		set { Session.Add(InformationBarFirstLineIconKey, value.ToString()); }
	}

	public static bool InformationBarFirstLineIconExists
	{
		get { return Session.Exists(InformationBarFirstLineIconKey); }
	}

	/************************** End InformationBarFirstLineIcon *****************************/

    /************************** InformationBarSecondLineIcon *****************************/
    public const string InformationBarSecondLineIconKey = "InformationBarSecondLineIcon";

    public static InformationBarIcon InformationBarSecondLineIcon
    {
        get { return (InformationBarIcon)Enum.Parse(typeof(InformationBarIcon), Session.Get(InformationBarSecondLineIconKey, "None")); }

        set { Session.Add(InformationBarSecondLineIconKey, value.ToString()); }
    }

    public static bool InformationBarSecondLineIconExists
    {
        get { return Session.Exists(InformationBarSecondLineIconKey); }
    }

    /************************** End InformationBarSecondLineIcon *****************************/

    /************************** FiltersPageSelections *****************************/
    public const string FiltersPageSelectionsKey = "FiltersPageSelections";

    public static FiltersPageSelections FiltersPageSelections
    {
        get { return Session.Get(FiltersPageSelectionsKey, new FiltersPageSelections()); }

        set { Session.Add(FiltersPageSelectionsKey, value); }
    }

    public static bool FiltersPageSelectionsExists
    {
        get { return Session.Exists(FiltersPageSelectionsKey); }
    }

    /************************** End FiltersPageSelections *****************************/

	# endregion
}

# endregion
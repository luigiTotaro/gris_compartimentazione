﻿
using System;
using System.Web.UI.WebControls;

public abstract class ReportsMaster : GrisMasterPageBase
{
    public ReportPublisher ReportPublisher
    {
        get
        {
            return this.Page as ReportPublisher;
        }
    }

	public string SelectedReport
	{
		get { return ( this.ViewState["SelReport"] ?? "" ).ToString(); }
		set { this.ViewState["SelReport"] = value; }
	}

	public abstract bool ReportListEnabled { get; set; }

	public abstract bool FiltersButtonEnabled { get; set; }

	public abstract bool ReportListPanelVisible { get; set; }

	public abstract bool FiltersPanelVisible { get; set; }

	public abstract bool ReportPanelVisible { get; set; }

	public abstract string GetSelectedReportTitle ();

	public abstract void InvokeReportGeneration ();

	public virtual void ObjectDataSource_Selected ( object sender, ObjectDataSourceStatusEventArgs e ) { }

	protected override void OnLoad ( EventArgs e )
	{
		base.OnLoad(e);

	}
}
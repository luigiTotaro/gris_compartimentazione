﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class CommandSentData
{
    public CommandSentData()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string Command_historyID { get; set; }
    public string CommandID { get; set; }
    public string CommandName { get; set; }
    public string Versione { get; set; }
    public string Parametri { get; set; }
    public string ExecutionUser { get; set; }
    public string DateSent { get; set; }
    public string DateReceived { get; set; }
    public string Response { get; set; }

}     

﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;

public class ScreenDumpPDFPrinter {
    private const string DEFAULT_FONT_NAME = "Arial";
    private const int LABEL_LARGE_WIDTH_CONTENT = 100;
    private const int LABEL_LARGE_WIDTH = 20;
    private const int LABEL_LARGE_HEIGHT = 10;
    private const int LABEL_SPACING = 5;
    private const int LABEL_SMALL_SPACING_VERTICAL = 1;
    private const int COLUMN_SPACING = 25;
    private const int LABEL_SMALL_HEIGHT = 4;
    private const int LABEL_SMALL_WIDTH = 30;
    private const int LABEL_SMALL_WIDTH_CONTENT = 100;
    private const int PAGE_BORDER = 10;
    private const int PAGE_WIDTH = 297;
    private const int PAGE_HEIGHT = 210;
    private const int IMAGE_ORIGIN_HORIZONTAL = 10;
    private const int IMAGE_ORIGIN_VERTICAL = 40;
    private const int SCREEN_RESOLUTION = 72;

    public void Get(string downloadFileName, string documentTitle, string documentAuthor, string documentSubject,
                    Image image, string stazione, string device, string produttore, string ip, string ubicazione,
                    string note, string dataVisualizzazione, string labelVendor, string labelIP, string labelWhere,
                    string labelDate, string labelNote, string labelNode, string labelDevice) {
        if ((HttpContext.Current != null) && (HttpContext.Current.Response != null)) {
            try {
                PdfDocument document = new PdfDocument();
                document.Info.Title = documentTitle;
                document.Info.Author = documentAuthor;
                document.Info.Subject = documentSubject;

                PdfPage page = document.AddPage();
                page.Width = XUnit.FromMillimeter(PAGE_WIDTH);
                page.Height = XUnit.FromMillimeter(PAGE_HEIGHT);

                XGraphics gfx = XGraphics.FromPdfPage(page);

                this.RenderImage(gfx, image);

                XFont largeBoldLabel = new XFont(DEFAULT_FONT_NAME, 10, XFontStyle.Bold);
                XFont largeRegularLabel = new XFont(DEFAULT_FONT_NAME, 10, XFontStyle.Regular);
                XFont smallRegularLabel = new XFont(DEFAULT_FONT_NAME, 9, XFontStyle.Regular);

                this.RenderHeader(gfx, labelNode, largeBoldLabel,
                                  new XRect(XUnit.FromMillimeter(PAGE_BORDER), XUnit.FromMillimeter(PAGE_BORDER),
                                            XUnit.FromMillimeter(LABEL_LARGE_WIDTH),
                                            XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Right);
                this.RenderHeader(gfx, stazione, largeRegularLabel,
                                  new XRect(XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING),
                                            XUnit.FromMillimeter(PAGE_BORDER),
                                            XUnit.FromMillimeter(LABEL_LARGE_WIDTH_CONTENT),
                                            XUnit.FromMillimeter(LABEL_LARGE_HEIGHT)), XParagraphAlignment.Left);

                this.RenderHeader(gfx, labelDevice, largeBoldLabel,
                                  new XRect(XUnit.FromMillimeter(PAGE_BORDER),
                                            XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_HEIGHT + LABEL_SPACING),
                                            XUnit.FromMillimeter(LABEL_LARGE_WIDTH),
                                            XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Right);
                this.RenderHeader(gfx, device, largeRegularLabel,
                                  new XRect(XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING),
                                            XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_HEIGHT + LABEL_SPACING),
                                            XUnit.FromMillimeter(LABEL_LARGE_WIDTH_CONTENT),
                                            XUnit.FromMillimeter(LABEL_LARGE_HEIGHT)), XParagraphAlignment.Left);

                this.RenderHeader(gfx, labelVendor, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER), XUnit.FromMillimeter(LABEL_SMALL_WIDTH),
                                      XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Right);
                this.RenderHeader(gfx, produttore, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING +
                                                           LABEL_SMALL_WIDTH + LABEL_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER), XUnit.FromMillimeter(LABEL_SMALL_WIDTH_CONTENT),
                                      XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Left);

                this.RenderHeader(gfx, labelIP, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_SMALL_HEIGHT +
                                                           LABEL_SMALL_SPACING_VERTICAL),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH), XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)),
                                  XParagraphAlignment.Right);
                this.RenderHeader(gfx, ip, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING +
                                                           LABEL_SMALL_WIDTH + LABEL_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_SMALL_HEIGHT +
                                                           LABEL_SMALL_SPACING_VERTICAL),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH_CONTENT),
                                      XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Left);

                this.RenderHeader(gfx, labelWhere, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER +
                                                           (LABEL_SMALL_HEIGHT + LABEL_SMALL_SPACING_VERTICAL)*2),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH), XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)),
                                  XParagraphAlignment.Right);
                this.RenderHeader(gfx, ubicazione, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING +
                                                           LABEL_SMALL_WIDTH + LABEL_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER +
                                                           (LABEL_SMALL_HEIGHT + LABEL_SMALL_SPACING_VERTICAL)*2),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH_CONTENT),
                                      XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Left);

                this.RenderHeader(gfx, labelDate, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER +
                                                           (LABEL_SMALL_HEIGHT + LABEL_SMALL_SPACING_VERTICAL)*3),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH), XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)),
                                  XParagraphAlignment.Right);
                this.RenderHeader(gfx, dataVisualizzazione, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING +
                                                           LABEL_SMALL_WIDTH + LABEL_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER +
                                                           (LABEL_SMALL_HEIGHT + LABEL_SMALL_SPACING_VERTICAL)*3),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH_CONTENT),
                                      XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Left);

                this.RenderHeader(gfx, labelNote, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER +
                                                           (LABEL_SMALL_HEIGHT + LABEL_SMALL_SPACING_VERTICAL)*4),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH), XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)),
                                  XParagraphAlignment.Right);
                this.RenderHeader(gfx, note, smallRegularLabel,
                                  new XRect(
                                      XUnit.FromMillimeter(PAGE_BORDER + LABEL_LARGE_WIDTH + LABEL_SPACING +
                                                           LABEL_LARGE_WIDTH_CONTENT + COLUMN_SPACING +
                                                           LABEL_SMALL_WIDTH + LABEL_SPACING),
                                      XUnit.FromMillimeter(PAGE_BORDER +
                                                           (LABEL_SMALL_HEIGHT + LABEL_SMALL_SPACING_VERTICAL)*4),
                                      XUnit.FromMillimeter(LABEL_SMALL_WIDTH_CONTENT),
                                      XUnit.FromMillimeter(LABEL_SMALL_HEIGHT)), XParagraphAlignment.Left);

                MemoryStream stream = new MemoryStream();
                document.Save(stream, false);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("content-length", stream.Length.ToString());
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + downloadFileName);
                HttpContext.Current.Response.BinaryWrite(stream.ToArray());
                HttpContext.Current.Response.Flush();
                stream.Close();
                HttpContext.Current.Response.End();
            }
            catch (Exception) {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.End();
            }
        }
    }

    private void RenderHeader(XGraphics gfx, string text, XFont font, XRect rect, XParagraphAlignment alignment) {
        XTextFormatter tf = new XTextFormatter(gfx);
        tf.Alignment = alignment;
        tf.DrawString(text, font, XBrushes.Black, rect, XStringFormats.TopLeft);
    }

    private void RenderImage(XGraphics gfx, Image imageStream) {
        XImage image = XImage.FromGdiPlusImage(imageStream);

        double width = image.PixelWidth*SCREEN_RESOLUTION/image.HorizontalResolution;
        double height = image.PixelHeight*SCREEN_RESOLUTION/image.HorizontalResolution;

        double maxWidth = XUnit.FromMillimeter(277);
        double maxHeight = XUnit.FromMillimeter(160);

        if ((width > maxWidth) || (height > maxHeight)) {
            if (width > height) {
                double newWidth = maxWidth;
                double newHeight = height*newWidth/width;
                gfx.DrawImage(image, XUnit.FromMillimeter(IMAGE_ORIGIN_HORIZONTAL),
                              XUnit.FromMillimeter(IMAGE_ORIGIN_VERTICAL), newWidth, newHeight);
            }
            else {
                double newHeight = maxHeight;
                double newWidth = width*newHeight/height;
                gfx.DrawImage(image, XUnit.FromMillimeter(IMAGE_ORIGIN_HORIZONTAL),
                              XUnit.FromMillimeter(IMAGE_ORIGIN_VERTICAL), newWidth, newHeight);
            }
        }
        else {
            gfx.DrawImage(image, XUnit.FromMillimeter(IMAGE_ORIGIN_HORIZONTAL),
                          XUnit.FromMillimeter(IMAGE_ORIGIN_VERTICAL), width, height);
        }
    }
}
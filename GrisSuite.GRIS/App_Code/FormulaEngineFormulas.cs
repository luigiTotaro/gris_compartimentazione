﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

public class FormulaEngineFormulas
{
    private readonly XDocument _formulaEngineFormulasConfig;
    private readonly HttpContext _context;

    public FormulaEngineFormulas(HttpContext context, string configurationFilename)
    {
        this._context = context;

        if (File.Exists(configurationFilename))
        {
            this._formulaEngineFormulasConfig = XDocument.Load(configurationFilename);
            try
            {
                if (!this._formulaEngineFormulasConfig.Descendants("Formulas").Any())
                {
                    this._formulaEngineFormulasConfig = null;
                }
            }
            catch (Exception ex)
            {
#if (!DEBUG)
                // Persiste l'eccezione serializzata in base dati
                if ((this._context != null) && (!Tracer.TraceMessage(this._context, ex, true)))
                {
                    // Logga l'eccezione via trace listener (su disco)
                    Trace.TraceError(string.Format("{0}, User -> {1}", ex.Message,
                                                   GUtility.GetUserInfos(true)));
                }
#endif
            }
        }
    }

    public Dictionary<string, string> GetFormulas(string type)
    {
        if ((this._context != null) && (this._context.Session != null))
        {
            if ((this._context.Session[type] != null) && (this._context.Session[type] is Dictionary<string, string>))
            {
                return (Dictionary<string, string>)this._context.Session[type];
            }

            Dictionary<string, string> formulas = new Dictionary<string, string>();

            if (this._formulaEngineFormulasConfig != null)
            {
                try
                {
                    var formulaParent = this._formulaEngineFormulasConfig.Descendants("Formulas").SingleOrDefault();
                    if (formulaParent != null)
                    {
                        foreach (
                            var formula in
                                formulaParent.Descendants(type).Descendants("Formula").Where(
                                    formula =>
                                    formula.HasAttributes))
                        {
                            var scriptPathAttribute = formula.Attribute("ScriptPath");
                            var descriptionAttribute = formula.Attribute("Description");
                            if ((scriptPathAttribute != null) && (descriptionAttribute != null))
                            {
                                formulas.Add(scriptPathAttribute.Value, descriptionAttribute.Value);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
#if (!DEBUG)
                    // Persiste l'eccezione serializzata in base dati
                    if ((this._context != null) && (!Tracer.TraceMessage(this._context, ex, true)))
                    {
                        // Logga l'eccezione via trace listener (su disco)
                        Trace.TraceError(string.Format("{0}, User -> {1}", ex.Message,
                                                       GUtility.GetUserInfos(true)));
                    }
#endif
                }
            }

            this._context.Session[type] = formulas;

            return formulas;
        }

        return new Dictionary<string, string>();
    }
}
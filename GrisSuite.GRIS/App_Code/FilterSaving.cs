﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Interfaccia che tutti i TemplateControl che vogliono essere abilitati alla funzionalità di salvataggio dei filtri devono implementare
/// </summary>
public interface IFilterSavingTemplateControl
{
	bool IsFilterSavingEnabled { get; set; }
	
	bool IsFiltersStateRestored { get; }

	List<string> FilterControlIDs { get; }

	List<Triplet> FilterValues { get; }

	void SavePageFilters();

	string GetFiltersKey();
}

/// <summary>
/// Implementa le logiche di individuazione dei controlli di filtro, di impostazione e salvataggio delle proprietà di filtro.
/// </summary>
internal class FilterSavingController
{
	internal List<Control> FilterControls { get; set; }

	/// <summary>
	/// Inizializza la lista dei controlli filtro.
	/// </summary>
	/// <param name="control">TemplateControl da cui estrapolare la lista dei controlli filtro.</param>
	internal void InitializeFilterControls ( IFilterSavingTemplateControl control )
	{
		if ( !( control is TemplateControl ) ) throw new ArgumentException("Il controllo passato al metodo InitializeFilterControls deve derivare da TemplateControl", "control");
		
		// per ora non applicabile a controlli non accessibili come campi della pagina
		this.FilterControls = new List<Control>(10);
		var fields = control.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

		foreach ( string filterControlID in control.FilterControlIDs )
		{
			var controlFields = from field in fields
								where field.Name == filterControlID
								select field;

			if ( controlFields.SingleOrDefault() != null )
			{
				Control filterControl = controlFields.SingleOrDefault().GetValue(control) as Control;
				if ( filterControl != null ) this.FilterControls.Add(filterControl);
			}
		}

		//TODO: vedere se possibile senza ricorso alla reflection
		//foreach ( string id in this.FilterControlIDs )
		//{
		//    var filtCtrl = this.FindControl(id, 2);
		//    if ( filtCtrl != null ) this.FilterControls.Add(filtCtrl);			
		//}
	}

	/// <summary>
	/// Imposta i controlli con il relativo valore di filtro.
	/// </summary>
	/// <returns>Booleano che indica se almeno un filtro è stato caricato</returns>
	/// <param name="filterValues">Lista di ID/Proprietà/Valore da carcare.</param>
	internal bool LoadFilterValues ( List<Triplet> filterValues )
	{
		bool valuesLoaded = false;
		
		foreach ( Control filterControl in this.FilterControls )
		{
			var reflectedValue = filterValues.SingleOrDefault(triplet => triplet.First.ToString() == filterControl.ID);
			if ( (reflectedValue != null) )
			{
				if ( filterControl is DataBoundControl ) filterControl.DataBind();
				( (PropertyInfo)reflectedValue.Second ).SetValue(filterControl, reflectedValue.Third, null);
				valuesLoaded = true;
			}
		}
		
		return valuesLoaded;
	}
	
	
	
	/// <summary>
	/// Salva i valore di filtro nella proprietà FilterValues del TemplateControl.
	/// </summary>
	/// <returns>Booleano che indica se almeno un filtro è stato impostato</returns>
	/// <param name="control">TemplateControl da cui estrapolare la lista dei controlli filtro.</param>
	internal bool SaveFilterValues ( IFilterSavingTemplateControl control )
	{
		bool valuesLoaded = false;

		if ( this.FilterControls != null && this.FilterControls.Count > 0 )
		{
			control.FilterValues.Clear();
			foreach ( Control filterControl in this.FilterControls )
			{
				object bindAttr = filterControl.GetType().GetCustomAttributes(typeof(ControlValuePropertyAttribute), true).SingleOrDefault();

				if ( bindAttr != null )
				{
					string propName = ( (ControlValuePropertyAttribute)bindAttr ).Name; //Ottengo il nome della proprietà di default usata per il binding
					var bindProp = CheckPropAccess(filterControl, propName);
					if ( bindProp != null )
					{
						control.FilterValues.Add(new Triplet(filterControl.ID, bindProp, bindProp.GetValue(filterControl, null)));
						valuesLoaded = true;
					}
				}
			}
		}

		return valuesLoaded;
	}

	/// <summary>
	/// Gestisce le anomalie nell'accesso alla proprità di default per il binding per controlli particolari. Restituisce la proprietà bindabile quando quella di default non è disponibile a tale scopo.
	/// </summary>
	/// <param name="control">Controllo di cui testare la proprietà.</param>
	/// <param name="bindPropertyName">Nome della proprietà di default per il binding</param>
	/// <remarks>Ad esempio la proprietà di default per il databinding della DataList è SelectedValue che però è accessibile solo in lettura.</remarks>
	/// <returns>Oggetto che rappresenta la Proprietà utilizzabile per il salvataggio/lettura del filtro.</returns>
	private PropertyInfo CheckPropAccess ( Control control, string bindPropertyName )
	{
		var bindProp = control.GetType().GetProperty(bindPropertyName);
		if ( !( bindProp.CanRead && bindProp.CanWrite ) )
		{
			switch ( bindProp.Name )
			{
				case "SelectedValue":
					{
						if ( control is DataList )
						{
							return control.GetType().GetProperty("SelectedIndex");
						}
						break;
					}
			}

			return null;
		}

		return bindProp;
	}
}

/// <summary>
/// Questa classe astratta permette alle pagine che la specializzano di ereditare le funzionalità di salvataggio dei filtri
/// </summary>
public abstract class FiltersSavingPage : GrisPage, IFilterSavingTemplateControl
{
	private bool _savePageFilters;
	private bool _isFiltersStateRestored;
	private FilterSavingController _controller;
	
	public EventHandler FiltersLoaded;
	public EventHandler FiltersSaved;

	#region IFilterSavingTemplateControl Members

	public bool IsFiltersStateRestored
	{
		get { return this._isFiltersStateRestored; }
	}

	public bool IsFilterSavingEnabled
	{
		get { return Convert.ToBoolean(this.ViewState["IsFilterSavingEnabled"] ?? false); }
		set { this.ViewState["IsFilterSavingEnabled"] = value; }
	}

	public List<string> FilterControlIDs
	{
		get
		{
			if ( this.ViewState["FilterControlIDs"] == null ) this.ViewState["FilterControlIDs"] = new List<string>(10);
			return (List<string>)this.ViewState["FilterControlIDs"];
		}
	}

	List<Triplet> IFilterSavingTemplateControl.FilterValues
	{
		get
		{
			if ( this.Session == null ) throw new Exception("Impossibile usare la proprietà 'FilterValues'. La sessione di asp.net non è disponibile.");

			string filtersKey = ( (IFilterSavingTemplateControl)this ).GetFiltersKey();
			if ( this.Session[filtersKey] == null ) this.Session[filtersKey] = new List<Triplet>(10);
			return (List<Triplet>)this.Session[filtersKey];
		}
	}

	public virtual void SavePageFilters ()
	{
		this._savePageFilters = true;
	}

	string IFilterSavingTemplateControl.GetFiltersKey ()
	{
		return string.Format("FLTRS_{0}", this.Page);
	}

	#endregion

	# region Page Methods

	protected override void OnInit ( EventArgs e )
	{
		base.OnInit(e);
		if ( this.IsFilterSavingEnabled ) this._controller = new FilterSavingController();
	}

	protected override void OnLoadComplete ( EventArgs e )
	{
		if ( this.IsFilterSavingEnabled && !this.IsPostBack )
		{
			this._controller.InitializeFilterControls(this);

			if ( ( (IFilterSavingTemplateControl)this ).FilterValues != null &&
				( (IFilterSavingTemplateControl)this ).FilterValues.Count > 0 &&
				this._controller.LoadFilterValues(( (IFilterSavingTemplateControl)this ).FilterValues) )
			{
				this._isFiltersStateRestored = true;
				if ( this.FiltersLoaded != null ) this.FiltersLoaded(this, null);
			}
		}

		base.OnLoadComplete(e);
	}

	protected override void LoadViewState ( object savedState )
	{
		base.LoadViewState(savedState);
		if ( this.IsFilterSavingEnabled ) this._controller.InitializeFilterControls(this);
	}

	protected override object SaveViewState ()
	{
		if ( this.IsFilterSavingEnabled && this._savePageFilters && this._controller != null && this._controller.SaveFilterValues(this) )
		{
			if ( this.FiltersSaved != null ) this.FiltersSaved(this, null);
		}

		return base.SaveViewState();
	}

	# endregion
	
	internal void SetFiltersStateRestored()
	{
		this._isFiltersStateRestored = true;
	}
}

/// <summary>
/// Questa classe astratta permette ai controlli che la specializzano di ereditare le funzionalità di salvataggio dei filtri
/// </summary>
public abstract class FiltersSavingControl : UserControl, IFilterSavingTemplateControl
{
	private bool _savePageFilters;
	private bool _isFiltersStateRestored;
	private FilterSavingController _controller;

	public EventHandler FiltersLoaded;
	public EventHandler FiltersSaved;

	#region IFilterSavingTemplateControl Members

	public bool IsFiltersStateRestored
	{
		get { return this._isFiltersStateRestored; }
	}

	public bool IsFilterSavingEnabled
	{
		get { return Convert.ToBoolean(this.ViewState["IsFilterSavingEnabled"] ?? false); }
		set { this.ViewState["IsFilterSavingEnabled"] = value; }
	}

	public List<string> FilterControlIDs
	{
		get
		{
			if ( this.ViewState["FilterControlIDs"] == null ) this.ViewState["FilterControlIDs"] = new List<string>(10);
			return (List<string>)this.ViewState["FilterControlIDs"];
		}
	}

	List<Triplet> IFilterSavingTemplateControl.FilterValues
	{
		get
		{
			if ( this.Session == null ) throw new Exception("Impossibile usare la proprietà 'FilterValues'. La sessione di asp.net non è disponibile.");

			string filtersKey = ( (IFilterSavingTemplateControl)this ).GetFiltersKey();
			if ( this.Session[filtersKey] == null ) this.Session[filtersKey] = new List<Triplet>(10);
			return (List<Triplet>)this.Session[filtersKey];
		}
	}

	public void SavePageFilters ()
	{
		this._savePageFilters = true;
	}

	string IFilterSavingTemplateControl.GetFiltersKey ()
	{
		return string.Format("FLTRS_{0}_{1}", this.Page, this.ID);
	}

	#endregion

	# region Page Methods

	protected override void OnInit ( EventArgs e )
	{
		base.OnInit(e);
		this.Page.LoadComplete += Page_LoadComplete;
		if ( this.IsFilterSavingEnabled ) this._controller = new FilterSavingController();
	}

	void Page_LoadComplete ( object sender, EventArgs e )
	{
		if ( this.IsFilterSavingEnabled && !this.IsPostBack )
		{
			this._controller.InitializeFilterControls(this);

			if ( ( (IFilterSavingTemplateControl)this ).FilterValues != null && 
				( (IFilterSavingTemplateControl)this ).FilterValues.Count > 0 && 
				this._controller.LoadFilterValues(( (IFilterSavingTemplateControl)this ).FilterValues) )
			{
				this._isFiltersStateRestored = true;
				if ( this.Page is FiltersSavingPage ) ( (FiltersSavingPage)this.Page ).SetFiltersStateRestored();
				if ( this.FiltersLoaded != null ) this.FiltersLoaded(this, null);
			}
		}
	}

	protected override void LoadViewState ( object savedState )
	{
		base.LoadViewState(savedState);
		if ( this.IsFilterSavingEnabled ) this._controller.InitializeFilterControls(this);
	}

	protected override object SaveViewState ()
	{
		if ( this.IsFilterSavingEnabled && this._savePageFilters && this._controller != null && this._controller.SaveFilterValues(this) )
		{
			if ( this.FiltersSaved != null ) this.FiltersSaved(this, null);
		}

		return base.SaveViewState();
	}

	# endregion
}


﻿using System;
using System.Web;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.NavigationIDsTableAdapters;
using System.Data.SqlClient;

public class Navigation
{
	private Navigation ()
	{
	}

	private const string MAP_LAYOUT_MODE_COOKIE_NAME = "MapLayoutMode";

	public static void GoToPage ( IAreaNavigation navigateToArea, long id )
	{
		if ( navigateToArea != null )
		{
			navigateToArea.ResetState().NavigateToArea(id);
		}
	}

	public static IAreaNavigation GetAreaNavigationObj ( PageDestination areaType )
	{
		return GetAreaNavigationObj(areaType.ToString(), new NavigationIdsProvider(), false);
	}

	public static IAreaNavigation GetAreaNavigationObj ( string areaType )
	{
		return GetAreaNavigationObj(areaType, new NavigationIdsProvider(), false);
	}

	public static IAreaNavigation GetAreaNavigationObj ( PageDestination areaType, bool invalidateAll )
	{
		return GetAreaNavigationObj(areaType.ToString(), new NavigationIdsProvider(), invalidateAll);
	}

	public static IAreaNavigation GetAreaNavigationObj ( string areaType, bool invalidateAll )
	{
		return GetAreaNavigationObj(areaType, new NavigationIdsProvider(), invalidateAll);
	}

	public static IAreaNavigation GetAreaNavigationObj ( PageDestination areaType, INavigationIdsProvider idsProvider,
													   bool invalidateAll )
	{
		return GetAreaNavigationObj(areaType.ToString(), idsProvider, invalidateAll);
	}

	public static IAreaNavigation GetAreaNavigationObj ( string areaType, INavigationIdsProvider idsProvider,
													   bool invalidateAll )
	{
		switch ( areaType.ToLowerInvariant() )
		{
			case "italia":
				{
					return new ItaliaNavigation(idsProvider, invalidateAll);
				}
			case "region":
				{
					return new RegionNavigation(idsProvider, invalidateAll);
				}
			case "zone":
				{
					return new ZoneNavigation(idsProvider, invalidateAll);
				}
			case "node":
				{
					return new NodeNavigation(idsProvider, invalidateAll);
				}
			case "device":
				{
					return new DeviceNavigation(idsProvider, invalidateAll);
				}
			default:
				{
					return new ItaliaNavigation(idsProvider, invalidateAll);
				}
		}
	}

	# region Old GoToPage

	private static void GoToPage ( string url )
	{
		if ( HttpContext.Current != null && !string.IsNullOrEmpty(url) )
		{
			HttpContext.Current.Response.Redirect(url);
		}
	}

	public static void GoToPage ( string destination, long id )
	{
		GoToPage(GetGoToPageString(destination, id));
	}

	public static void GoToPage ( PageDestination destination, params long[] ids )
	{
		GoToPage(GetGoToPageString(destination, ids));
	}

	public static string GetGoToPageString ( string destination, long id )
	{
		switch ( destination.ToLower() )
		{
			case "italia":
				{
					return GetGoToPageString(PageDestination.Italia, new long[] { -1 });
				}
			case "region":
				{
					return GetGoToPageString(PageDestination.Region, new[] { id });
				}
			case "zone":
				{
					NavIDsTableAdapter ta = new NavIDsTableAdapter();
					NavigationIDs.NavIDsDataTable navs = ta.GetParentIDsByZonID(id);

					if ( navs.Count > 0 )
					{
						NavigationIDs.NavIDsRow nav = navs[0];
						return GetGoToPageString(PageDestination.Zone, nav.RegID, id);
					}

					break;
				}
			case "node":
				{
					NavIDsTableAdapter ta = new NavIDsTableAdapter();
					NavigationIDs.NavIDsDataTable navs = ta.GetParentIDsByNodID(id);

					if ( navs.Count > 0 )
					{
						NavigationIDs.NavIDsRow nav = navs[0];
						return GetGoToPageString(PageDestination.Node, nav.RegID, nav.ZonID, id);
					}

					break;
				}
			case "device":
				{
					NavIDsTableAdapter ta = new NavIDsTableAdapter();
					NavigationIDs.NavIDsDataTable navs = ta.GetParentIDsByDevID(id);

					if ( navs.Count > 0 )
					{
						NavigationIDs.NavIDsRow nav = navs[0];
						return GetGoToPageString(PageDestination.Device, nav.RegID, nav.ZonID, nav.NodID, id);
					}

					break;
				}
		}

		return "";
	}

	public static string GetGoToPageString ( PageDestination destination, params long[] ids )
	{
		switch ( destination )
		{
			case PageDestination.Italia:
				{
					return "~/Italia.aspx";
				}
			case PageDestination.Region:
				{
					return string.Format("~/Region.aspx?RegID={0}", ids[0]);
				}
			case PageDestination.Zone:
				{
					return string.Format("~/Zone.aspx?RegID={0}&ZonID={1}", ids[0], ids[1]);
				}
			case PageDestination.Node:
				{
					return string.Format("~/Node.aspx?RegID={0}&ZonID={1}&NodID={2}", ids[0], ids[1], ids[2]);
				}
			case PageDestination.Device:
				{
					return string.Format("~/Device.aspx?RegID={0}&ZonID={1}&NodID={2}&DevID={3}", ids[0], ids[1], ids[2],
										 ids[3]);
				}
		}

		return "";
	}

	# endregion

	// Questa proprietà è scritta e letta da diverse pagine e controlli
	// Non è possibile persistere in un cookie e rileggerlo immediatamente, perché il cookie aggiunto o aggiornato
	// sarà disponibile solo dopo un roundtrip completo verso il client
	// Dato che un controllo scrive la proprietà e una pagina la rilegge, usiamo la session come buffer
	// lato server. Nel caso la session non sia disponibile, carichiamo dal cookie
	public static MapLayoutMode CurrentVisualizationMode
	{
		get
		{
			MapLayoutMode currentLayoutMode = MapLayoutMode.Graph;

			if ( ( HttpContext.Current.Session != null ) &&
				( HttpContext.Current.Session[MAP_LAYOUT_MODE_COOKIE_NAME] != null ) )
			{
				string visualizationModeSession =
					HttpContext.Current.Session[MAP_LAYOUT_MODE_COOKIE_NAME].ToString();

				try
				{
					currentLayoutMode =
						(MapLayoutMode) Enum.Parse(typeof(MapLayoutMode), visualizationModeSession);
				}
				catch ( ArgumentException )
				{
					currentLayoutMode = MapLayoutMode.Grid;
				}
			}
			else
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies[MAP_LAYOUT_MODE_COOKIE_NAME];
				if ( ( cookie != null ) && ( !string.IsNullOrEmpty(cookie.Value) ) )
				{
					try
					{
						currentLayoutMode =
							(MapLayoutMode) Enum.Parse(typeof(MapLayoutMode), cookie.Value);
					}
					catch ( ArgumentException )
					{
						currentLayoutMode = MapLayoutMode.Grid;
					}
				}
			}

			return currentLayoutMode;
		}
		set
		{
			if ( HttpContext.Current.Session != null )
			{
                HttpCookie cookie = HttpContext.Current.Response.Cookies[MAP_LAYOUT_MODE_COOKIE_NAME];
                if ( cookie != null )
				{
				    cookie.Value = value.ToString();
				    cookie.Expires = DateTime.Now.AddMonths(6);
					//cookie.HttpOnly = true;
					//cookie.Secure = true;
				    HttpContext.Current.Response.Cookies.Set(cookie);
				}
				else
				{
					cookie = new HttpCookie(MAP_LAYOUT_MODE_COOKIE_NAME) { Value = value.ToString(), Expires = DateTime.Now.AddMonths(6) };
					HttpContext.Current.Response.Cookies.Add(cookie);
				}

				HttpContext.Current.Session[MAP_LAYOUT_MODE_COOKIE_NAME] = value.ToString();
			}
		}
	}
}

/// <summary>
/// Classe che sa come reperire gli ids delle aree padre rispetto a quella specificata, in questo caso è un Dataset tipizzato.
/// </summary>
public class NavigationIdsProvider : INavigationIdsProvider
{
	private NavigationIDs.NavIDsRow _dataContainer;

	#region INavigationIdsProvider Members

	public long RegID
	{
		get { return this._dataContainer.RegID; }
	}

	public long ZonID
	{
		get { return this._dataContainer.ZonID; }
	}

	public long NodID
	{
		get { return this._dataContainer.NodID; }
	}

	private void InitializeData ( long id, string area )
	{
		NavIDsTableAdapter ta = new NavIDsTableAdapter();
		NavigationIDs.NavIDsDataTable navs;

		switch ( area )
		{
			case "z":
				{
					navs = ta.GetParentIDsByZonID(id);
					break;
				}
			case "n":
				{
					navs = ta.GetParentIDsByNodID(id);
					break;
				}
			case "d":
				{
					navs = ta.GetParentIDsByDevID(id);
					break;
				}
			default:
				{
					navs = ta.GetParentIDsByZonID(id);
					break;
				}
		}

		if ( navs.Count == 0 )
		{
			throw new ArgumentException("L' id passato per la navigazione non è valido.", "id");
		}

		this._dataContainer = navs[0];
	}

	public void InitializeDataForZone ( long id )
	{
		this.InitializeData(id, "z");
	}

	public void InitializeDataForNode ( long id )
	{
		this.InitializeData(id, "n");
	}

	public void InitializeDataForDevice ( long id )
	{
		this.InitializeData(id, "d");
	}

	#endregion
}

/// <summary>
/// E' la classe base di tutte le classi che gestiscolo la navigazione verso le pagine specifiche.
/// </summary>
public abstract class AreaNavigation : IAreaNavigation
{
	protected string BaseUrl = "~/Italia.aspx";
	/// <summary>
	/// Oggetto che sa come reperire gli ids delle aree padre rispetto a quella specificata.
	/// </summary>
	protected readonly INavigationIdsProvider IdsProvider;

	/// <summary>
	/// Campo flag che indica se invalidare le informazioni relative alla navigazione contenute in sessione. 
	/// E' utile per richieste di spostamento randomico (non sequenziali rispetto alla gerarchia delle aree).
	/// Indica anche la volonta di non considerare gli ids contenuti nella QueryString e di reperire tali informazioni dalla base dati.
	/// </summary>
	protected readonly bool InvalidateAll;

	protected AreaNavigation ( INavigationIdsProvider idsProvider )
	{
		this.IdsProvider = idsProvider;
	}

	protected AreaNavigation ( INavigationIdsProvider idsProvider, bool invalidateAll )
	{
		this.IdsProvider = idsProvider;
		this.InvalidateAll = invalidateAll;
	}

	protected virtual void NavigateTo ( string url )
	{
		if ( HttpContext.Current != null && !string.IsNullOrEmpty(url) )
		{
			HttpContext.Current.Response.Redirect(url);
		}
	}

	protected long GetQSId ( string queryStringParameter )
	{
		long id;

		if ( HttpContext.Current == null ||
			HttpContext.Current.Request.QueryString[queryStringParameter] == null ||
			long.TryParse(HttpContext.Current.Request.QueryString[queryStringParameter], out id) )
		{
			id = -1;
		}

		return id;
	}

	#region IAreaNavigation Members

	public abstract IAreaNavigation ResetState ();

	public virtual void NavigateToArea ( long id )
	{
		this.NavigateTo(this.GetNavigateToAreaUrl(id));
	}

	public abstract string GetNavigateToAreaUrl ( long id );

	#endregion
}

public class ItaliaNavigation : AreaNavigation
{
	# region Ctors

	public ItaliaNavigation ( INavigationIdsProvider idsProvider )
		: base(idsProvider)
	{
	}

	public ItaliaNavigation ( INavigationIdsProvider idsProvider, bool invalidateAll )
		: base(idsProvider, invalidateAll)
	{
	}

	# endregion

	#region IAreaNavigation Members

	public override IAreaNavigation ResetState ()
	{
		GrisSessionManager.RemoveAllNavigationData();
		return this;
	}

	public override string GetNavigateToAreaUrl ( long id )
	{
        if (Navigation.CurrentVisualizationMode == MapLayoutMode.Grid)
        {
            return "~/Italia.aspx";
        }

        return string.Format("{0}?Aid={1}&Atype={2}", "~/Italia.aspx", 0, PageDestination.Italia);
	}

	#endregion
}

public class RegionNavigation : AreaNavigation
{
	# region Ctors

	public RegionNavigation ( INavigationIdsProvider idsProvider )
		: base(idsProvider)
	{
	}

	public RegionNavigation ( INavigationIdsProvider idsProvider, bool invalidateAll )
		: base(idsProvider, invalidateAll)
	{
	}

	# endregion

	#region IAreaNavigation Members

	public override IAreaNavigation ResetState ()
	{
		if ( this.InvalidateAll )
		{
			GrisSessionManager.RemoveAllNavigationData();
		}
		else
		{
			// rimuove i dati di zone e figli -> mi tengo i dati di region
			GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Zone);
		}

		return this;
	}

	public override string GetNavigateToAreaUrl ( long id )
	{
		if ( Navigation.CurrentVisualizationMode == MapLayoutMode.Grid )
		{
			return string.Format("~/Region.aspx?RegID={0}", id);			
		}

		return string.Format("{0}?Aid={1}&Atype={2}", this.BaseUrl, id, PageDestination.Region);
	}

	#endregion
}

public class ZoneNavigation : AreaNavigation
{
	# region Ctors

	public ZoneNavigation ( INavigationIdsProvider idsProvider )
		: base(idsProvider)
	{
	}

	public ZoneNavigation ( INavigationIdsProvider idsProvider, bool invalidateAll )
		: base(idsProvider, invalidateAll)
	{
	}

	# endregion

	#region IAreaNavigation Members

	public override IAreaNavigation ResetState ()
	{
		if ( this.InvalidateAll )
		{
			GrisSessionManager.RemoveAllNavigationData();
		}
		else
		{
			// rimuove i dati di node e figli -> mi tengo i dati di region e zone
			GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Node);
		}

		return this;
	}

	public override string GetNavigateToAreaUrl ( long id )
	{
		long regId = this.GetQSId("RegID");

		if ( Navigation.CurrentVisualizationMode == MapLayoutMode.Grid )
		{
			if ( this.IdsProvider != null && ( this.InvalidateAll || -1 == regId ) )
			{
				try
				{
					this.IdsProvider.InitializeDataForZone(id);
					regId = this.IdsProvider.RegID;
				}
				catch ( SqlException )
				{
				}
			}
			
			return string.Format("~/Zone.aspx?RegID={0}&ZonID={1}", regId, id);
		}

		return string.Format("{0}?Aid={1}&Atype={2}", this.BaseUrl, id, PageDestination.Zone);
	}

	#endregion
}

public class NodeNavigation : AreaNavigation
{
	# region Ctors

	public NodeNavigation ( INavigationIdsProvider idsProvider )
		: base(idsProvider)
	{
	}

	public NodeNavigation ( INavigationIdsProvider idsProvider, bool invalidateAll )
		: base(idsProvider, invalidateAll)
	{
	}

	# endregion

	#region IAreaNavigation Members

	public override IAreaNavigation ResetState ()
	{
		if ( this.InvalidateAll )
		{
			// la pagina di dettaglio periferica non ha informazioni di navigazione in sessione
			GrisSessionManager.RemoveAllNavigationData();
		}

		return this;
	}

	public override string GetNavigateToAreaUrl ( long id )
	{
		long regId = this.GetQSId("RegID");
		long zonId = this.GetQSId("ZonID");

		if ( this.IdsProvider != null && ( this.InvalidateAll || -1 == regId || -1 == zonId ) )
		{
			try
			{
				this.IdsProvider.InitializeDataForNode(id);
				regId = this.IdsProvider.RegID;
				zonId = this.IdsProvider.ZonID;
			}
			catch ( SqlException )
			{
			}
		}

		return string.Format("~/Node.aspx?RegID={0}&ZonID={1}&NodID={2}", regId, zonId, id);
	}

	#endregion
}

public class DeviceNavigation : AreaNavigation
{
	# region Ctors

	public DeviceNavigation ( INavigationIdsProvider idsProvider )
		: base(idsProvider)
	{
	}

	public DeviceNavigation ( INavigationIdsProvider idsProvider, bool invalidateAll )
		: base(idsProvider, invalidateAll)
	{
	}

	# endregion

	#region IAreaNavigation Members

	public override IAreaNavigation ResetState ()
	{
		if ( this.InvalidateAll )
		{
			GrisSessionManager.RemoveAllNavigationData();
		}

		return this;
	}

	public override string GetNavigateToAreaUrl ( long id )
	{
		long regId = this.GetQSId("RegID");
		long zonId = this.GetQSId("ZonID");
		long nodId = this.GetQSId("NodID");

		if ( this.IdsProvider != null && ( this.InvalidateAll || -1 == regId || -1 == zonId || -1 == nodId ) )
		{
			try
			{
				this.IdsProvider.InitializeDataForDevice(id);
				regId = this.IdsProvider.RegID;
				zonId = this.IdsProvider.ZonID;
				nodId = this.IdsProvider.NodID;
			}
			catch ( SqlException )
			{
			}
		}

		return string.Format("~/Device.aspx?RegID={0}&ZonID={1}&NodID={2}&DevID={3}", regId, zonId, nodId, id);
	}

	#endregion
}
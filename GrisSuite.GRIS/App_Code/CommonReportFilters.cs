
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for CommonReportFilters
/// </summary>
public abstract class CommonReportFilters : System.Web.UI.UserControl
{
	public System.Web.UI.WebControls.ObjectDataSource DataSource { get; set; }

	public ReportViewer ReportViewer { get; set; }

	public virtual void GenerateReport ()
	{
		if ( this.ReportViewer != null )
		{
			this.ReportViewer.LocalReport.Refresh();
		}
	}
}

using System;
using System.IO;
using System.Web.Services;
using System.Web.UI.WebControls;

using GrisSuite.Common;

public abstract class NavigationPage : FiltersSavingPage
{
	protected override void OnLoad ( EventArgs e )
	{
		base.OnLoad(e);

		// verifico i permessi
		if ( (this.RegID == -1 /* per entrare nella schermata Italia */) )
		{
		    if (GrisPermissions.CanUserAccessGris()) return;

		    String currentPage = Path.GetFileNameWithoutExtension(this.Request.PhysicalPath);
            if (currentPage == "Filters" && GrisPermissions.CanAccessResource((int) DatabaseResource.DeviceFilters))
                 return;

		    this.Response.Redirect("~/Default.aspx");
		}
		else
		{
		    if (GrisPermissions.IsInRole(this.RegID)) return;

		    // Stiamo facendo il ritorno alla pagina di Default, da cui, probabilmente, torneremo a Italia,
		    // ma questo avviene durante i click sulla mappa SL, su stazione non accessibile. In questo caso forziamo la modalit�
		    // a griglia, in modo che si resti sulla tabellare con la lista dei compartimenti accessibili
		    // In caso contrario, si rimarrebbe sulla tabellare nelle stazioni grandi (perch� il numero di periferiche forzerebbe la griglia)
		    // oppure si tornerebbe alla mappa, nel caso di stazioni piccole
		    if (Navigation.CurrentVisualizationMode == MapLayoutMode.Graph)
		    {
		        Navigation.CurrentVisualizationMode = MapLayoutMode.Grid;
		    }
		    this.Response.Redirect("~/Default.aspx");
		}
	}

	# region Navigation Members
	public long GetID ( string key, string redirect )
	{
		long id;
		if ( !string.IsNullOrEmpty(Request.QueryString[key]) && long.TryParse(Request.QueryString[key], out id) )
		{
			GrisSessionManager.Session.Add(key, id);
			return id;
		}
		
		if ( GrisSessionManager.Session.Exists(key) )
		{
			return GrisSessionManager.Session.Get<long>(key, -1);
		}
		
		this.Response.Redirect(redirect);

		return -1;
	}

	public long RegID
	{
		get
		{
			return Convert.ToInt64(this.ViewState["RegID"] ?? -1);
		}

		set
		{
			this.ViewState["RegID"] = value;
		}
	}

	public long ZonID
	{
		get
		{
			return Convert.ToInt64(this.ViewState["ZonID"] ?? -1);
		}
		set
		{
			this.ViewState["ZonID"] = value;
		}
	}

	public long NodID
	{
		get
		{
			return Convert.ToInt64(this.ViewState["NodID"] ?? -1);
		}
		set
		{
			this.ViewState["NodID"] = value;
		}
	}

	public long DevID
	{
		get
		{
			return Convert.ToInt64(this.ViewState["DevID"] ?? -1);
		}
		set
		{
			this.ViewState["DevID"] = value;
		}
	}

	public abstract MultiView LayoutMultiView
	{
		get;
	}

	public abstract View GraphView
	{
		get;
	}

	public abstract View GridView
	{
		get;
	}
	# endregion

	# region GoToPage Methods
	public void GoToPage ( PageDestination destination )
	{
		switch ( destination )
		{
			case PageDestination.Italia:
				{
					this.GoToPage(destination, -1);
					break;
				}
			case PageDestination.Region:
				{
					this.GoToPage(destination, this.RegID);
					break;
				}
			case PageDestination.Zone:
				{
					this.GoToPage(destination, this.ZonID);
					break;
				}
			case PageDestination.Node:
				{
					this.GoToPage(destination, this.NodID);
					break;
				}
			case PageDestination.Device:
				{
					this.GoToPage(destination, this.DevID);
					break;
				}
		}
	}

	public void GoToPage ( PageDestination destination, long id )
	{
		Navigation.GetAreaNavigationObj(destination).NavigateToArea(id);
	}

	[WebMethod]
	public static string ClientGoToPage ( string destination, string id )
	{
		long nodeId;
		if ( destination.Length > 0 && long.TryParse(id, out nodeId) )
		{
			GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Node);
			return GUtility.GetAbsoluteUrl(Navigation.GetAreaNavigationObj(destination, true).GetNavigateToAreaUrl(nodeId));
		}
		
		return "";
	}
	# endregion
}

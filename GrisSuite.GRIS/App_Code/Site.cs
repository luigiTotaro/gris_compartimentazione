using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.SiteDSTableAdapters;

public class ThisSite
{
	private static SiteDS.siteRow Site = null;

	private ThisSite ()
	{
	}

	static ThisSite ()
	{
		GetDataFromDB();
	}

	public static SiteDS.siteRow GetSiteData ()
	{
		return Site;
	}

	public static void UpdateSiteKeys (byte[] serverKey, byte[] clientKey, byte siteID)
	{
		new siteTableAdapter().UpdateSiteKeys(serverKey, clientKey, siteID);
		GetDataFromDB();
	}

	private static void GetDataFromDB ()
	{
		SiteDS.siteDataTable sites = new siteTableAdapter().GetData();
		if ( sites.Count == 1 )
		{
			Site = sites[0];
		}
		else
		{
			throw new ApplicationException("Impossibile trovare i dati di configurazione del sito nel database.");
		}		
	}
}

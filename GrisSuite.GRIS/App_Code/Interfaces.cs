
using System.Web.UI;

public interface IGrisPage
{
	Control FindControl ( Control ctrl, string id );
	IGrisMasterPage GrisMaster { get; }
}

public interface IGrisMasterPage
{
	void SelectInterface ( GrisLayoutMode layoutMode, bool performRedirect );
}

public interface IAreaNavigation
{
    IAreaNavigation ResetState();

	void NavigateToArea ( long id );

	string GetNavigateToAreaUrl ( long id );
}

public interface INavigationIdsProvider
{
	long RegID { get; }

	long ZonID { get; }

	long NodID { get; }

	void InitializeDataForZone ( long id );

	void InitializeDataForNode ( long id );

	void InitializeDataForDevice ( long id );

}

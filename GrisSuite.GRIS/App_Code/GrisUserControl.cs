
public class GrisUserControl : System.Web.UI.UserControl
{
	public NavigationPage NavigationPage
	{
		get 
		{
			if ( this.IsHostedInNavigationPage ) return (NavigationPage)this.Page;
			return null;
		}
	}

	public bool IsHostedInNavigationPage
	{
		get 
		{
			return ( this.Page is NavigationPage );
		}
	}
}

﻿
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ResourcesDSTableAdapters;
using GrisSuite.Common;

public class ResourcePermissionGroups
{
	public int ResourceId { get; set; }
	public string Category { get; set; }
	public string ResourceCode { get; set; }
	public string ResourceName { get; set; }
	public string Groups { get; set; }
	public IList<string > WindowsGroups { get; set; }

    public static IEnumerable<ResourcePermissionGroups> GetResourcesAndGroups(ResourceType resourceType, string resourceName, string groupName)
	{
	    var ta = new ResourceAndGroupsTableAdapter();
        var table = ta.GetData((short)resourceType, resourceName, groupName, -1);

		var resources = from ResourcesDS.ResourceAndGroupsRow resource in table.Rows
					  group resource.GroupName by new { Cat = resource.Category, Id = resource.ResourceID, Name = resource.ResourceDescription }
						  into groups
						  select new ResourcePermissionGroups
						  {
							  ResourceId = groups.Key.Id,
							  Category = groups.Key.Cat,
							  ResourceName = groups.Key.Name,
							  Groups = groups.Aggregate(( running, next ) => string.Format("{0}, {1}", running, next))
						  };

		return resources;
	}

	public static IEnumerable<ResourcePermissionGroups> GetResourcesByCategory (ResourceType resourceType, int categoryId )
	{
	    var ta = new ResourceByCategoryTableAdapter();
		var table = ta.GetData((short)resourceType, categoryId,null);

		var resources = from ResourcesDS.ResourceByCategoryRow resource in table.Rows
					  group resource.GroupName by new { Id = resource.ResourceID, Code = resource.ResourceCode, Name = resource.ResourceDescription }
						  into groups
						  select new ResourcePermissionGroups
						  {
							  ResourceId = groups.Key.Id,
							  ResourceCode = groups.Key.Code,
							  ResourceName = groups.Key.Name,
							  WindowsGroups =  GrisPermissions.GetWindowsGroups(groups)
						  };

		return resources;
	}
}

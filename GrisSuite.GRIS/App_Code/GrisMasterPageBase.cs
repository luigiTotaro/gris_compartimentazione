﻿
using System.Web.UI;

public class GrisMasterPageBase : MasterPage, IGrisMasterPage
{
    public GrisMasterPageBase()
    {
    }

    #region IGrisMasterPage Members
    public virtual void SelectInterface(GrisLayoutMode layoutMode, bool performRedirect)
    {
        if (this.Master is IGrisMasterPage)
        {
            ((IGrisMasterPage)this.Master).SelectInterface(layoutMode, performRedirect);
        }
    }
    #endregion
}

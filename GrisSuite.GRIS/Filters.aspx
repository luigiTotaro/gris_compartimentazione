﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
	CodeFile="Filters.aspx.cs" CodeFileBaseClass="FiltersSavingPage" Inherits="Filters"
	Title="GRIS - Stato Diagnostico Periferiche" IsFilterSavingEnabled="True" %>

<%@ Register TagPrefix="gris" TagName="DeviceTypeIconAndStatus" Src="~/Controls/DeviceTypeIconAndStatus.ascx" %>
<%@ Register TagPrefix="gris" TagName="DeviceStreamFieldsPill" Src="~/Controls/DeviceStreamFieldsPill.ascx" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.dimensions.js" type="text/javascript"></script>
    <script src="../JS/jquery.tooltip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function hideModalPopupViaClient(id) {
            var modalPopupBehavior = $find(id);
            modalPopupBehavior.hide();
        }
    </script>

	<table cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr>
			<td>
				<asp:UpdatePanel ID="updToolBar" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
							<tr>
								<td style="background-image: url(IMG/interfaccia/nav_bg.gif); height: 30px;">
									&nbsp;
								</td>
								<td style="width: 42px;">
									<asp:Image runat="server" ID="imgNavSep2" ImageUrl="~/IMG/Interfaccia/nav_sep2.gif"
										CssClass="imgnb" />
									<asp:ImageButton ID="imgbUpdate" runat="server" ImageUrl="~/IMG/Interfaccia/btnEmptyMenu.gif"
										CssClass="imgnb" Enabled="false" />
								</td>
							</tr>
							<tr>
								<td colspan="2" style="background-image: url(IMG/interfaccia/nav_shadow_bg.gif);
									height: 21px; background-repeat: repeat-x;">
								</td>
							</tr>
						</table>
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
		<tr>
			<td>
				<script type="text/javascript">
					var xPosStazioni, yPosStazioni, xPosLinee, yPosLinee, xPosCompartimenti, yPosCompartimenti, xPosFiltri, yPosFiltri, xPosSistemi, yPosSistemi, xPosTipiApparato, yPosTipiApparato;
					var prm = Sys.WebForms.PageRequestManager.getInstance();

					function BeginRequestHandler(sender, args) {
						SCPFPF($get('<%=pnlFiltri.ClientID%>'));
						SCPFPC($get('<%=pnlCompartimentiFilters.ClientID%>'));
						SCPFPL($get('<%=pnlLineeFilters.ClientID%>'));
						SCPFPN($get('<%=pnlStazioniFilters.ClientID%>'));
						SCPFPS($get('<%=pnlSistemiFilters.ClientID%>'));
						SCPFPTA($get('<%=pnlTipiApparatoFilters.ClientID%>'));

					}

					function SCPFPF(pnl) { if (pnl != null) { xPosFiltri = pnl.scrollLeft; yPosFiltri = pnl.scrollTop; } }
					function SCPFPC(pnl) { if (pnl != null) { xPosCompartimenti = pnl.scrollLeft; yPosCompartimenti = pnl.scrollTop; } }
					function SCPFPL(pnl) { if (pnl != null) { xPosLinee = pnl.scrollLeft; yPosLinee = pnl.scrollTop; } }
					function SCPFPN(pnl) { if (pnl != null) { xPosStazioni = pnl.scrollLeft; yPosStazioni = pnl.scrollTop; } }
					function SCPFPS(pnl) { if (pnl != null) { xPosSistemi = pnl.scrollLeft; yPosSistemi = pnl.scrollTop; } }
					function SCPFPTA(pnl) { if (pnl != null) { xPosTipiApparato = pnl.scrollLeft; yPosTipiApparato = pnl.scrollTop; } }

					function EndRequestHandler(sender, args) {
						RCPFPF($get('<%=pnlFiltri.ClientID%>'));
						RCPFPC($get('<%=pnlCompartimentiFilters.ClientID%>'));
						RCPFPL($get('<%=pnlLineeFilters.ClientID%>'));
						RCPFPN($get('<%=pnlStazioniFilters.ClientID%>'));
						RCPFPS($get('<%=pnlSistemiFilters.ClientID%>'));
						RCPFPTA($get('<%=pnlTipiApparatoFilters.ClientID%>'));
					}

					function RCPFPF(pnl) { if (pnl != null) { pnl.scrollLeft = xPosFiltri; pnl.scrollTop = yPosFiltri; } }
					function RCPFPC(pnl) { if (pnl != null) { pnl.scrollLeft = xPosCompartimenti; pnl.scrollTop = yPosCompartimenti; } }
					function RCPFPL(pnl) { if (pnl != null) { pnl.scrollLeft = xPosLinee; pnl.scrollTop = yPosLinee; } }
					function RCPFPN(pnl) { if (pnl != null) { pnl.scrollLeft = xPosStazioni; pnl.scrollTop = yPosStazioni; } }
					function RCPFPS(pnl) { if (pnl != null) { pnl.scrollLeft = xPosSistemi; pnl.scrollTop = yPosSistemi; } }
					function RCPFPTA(pnl) { if (pnl != null) { pnl.scrollLeft = xPosTipiApparato; pnl.scrollTop = yPosTipiApparato; } }

					prm.add_beginRequest(BeginRequestHandler);
					prm.add_endRequest(EndRequestHandler);

					function controlEnter(obj, event) { var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode; if (keyCode == 13) { document.getElementById(obj).click(); return false; } else { return true; } }
					function updateBhvKey(bn, value) { var behavior = $find(bn); if (behavior) { behavior.populate(value); } }


				</script>
				<asp:UpdatePanel runat="server" ID="updpnlManageFilters" UpdateMode="Conditional">
					<ContentTemplate>
						<table width="850">
							<tr>
								<td width="110px" class="filtersLBL">
                                    <asp:Label ID="lblFilterName" EnableViewState="false" runat="server" CssClass="filtersMainLBL">
                                        Filtro
                                    </asp:Label>
								</td>
								<td class="filtersLBL">
									<asp:TextBox runat="server" ID="txtNomeFiltro" Style="width: 706px; height: 15px;
										font-size: 12px; vertical-align: top; padding-top: 1px;" CssClass="filtersTA"
										MaxLength="110" /><asp:Image runat="server" ID="imgFiltriSelectors" ImageUrl="~/IMG/Interfaccia/Arrow3 Down.png"
											CssClass="filtersTAi" />
									<act:PopupControlExtender ID="popexFiltriSelector" runat="server" TargetControlID="imgFiltriSelectors"
										PopupControlID="pnlFiltri" Position="Bottom" OffsetX="-660" OffsetY="2" CommitProperty="ToolTip" />
								</td>
							</tr>
						</table>
					</ContentTemplate>
				</asp:UpdatePanel>
				<asp:UpdatePanel runat="server" ID="updpnlFilters" UpdateMode="Conditional">
					<ContentTemplate>
						
						<table width="760px" runat="server" id="tblFilters" style="margin-left: 114px">
							<tr>
								<td width="33%" class="filtersLBL" style="padding-left: 60px;">
									Compartimento
								</td>
								<td width="33%" class="filtersLBL" style="padding-left: 90px;">
									Linea
								</td>
								<td width="33%" class="filtersLBL" style="padding-left: 80px;">
									Stazione
									<br />
									<span style="font-size: 10px; margin-left: -60px;">(Selezionare almeno una stazione)</span>
								</td>
							</tr>
							<tr>
								<td>
									<asp:TextBox runat="server" ID="txtCompartimenti" TextMode="MultiLine" CssClass="filtersTA"
										ReadOnly="true" /><asp:Image runat="server" ID="imgCompartimentiSelectors" ImageUrl="~/IMG/Interfaccia/Arrow3 Down.png"
											CssClass="filtersTAi" />
									<act:PopupControlExtender ID="popexCompartimenti" runat="server" TargetControlID="imgCompartimentiSelectors"
										PopupControlID="pnlCompartimentiFilters" Position="Bottom" OffsetX="-203" OffsetY="39" />
								</td>
								<td>
									<asp:TextBox runat="server" ID="txtLinee" TextMode="MultiLine" CssClass="filtersTA"
										ReadOnly="true" /><asp:Image runat="server" ID="imgLineeSelectors" ImageUrl="~/IMG/Interfaccia/Arrow3 Down.png"
											CssClass="filtersTAi" />
									<act:PopupControlExtender ID="popexLinee" runat="server" TargetControlID="imgLineeSelectors"
										PopupControlID="pnlLineeFilters" Position="Bottom" OffsetX="-203" OffsetY="39" />
								</td>
								<td>
									<asp:TextBox runat="server" ID="txtStazioni" TextMode="MultiLine" CssClass="filtersTA"
										ReadOnly="true" /><asp:Image runat="server" ID="imgStazioniSelectors" ImageUrl="~/IMG/Interfaccia/Arrow3 Down.png"
											CssClass="filtersTAi" />
									<act:PopupControlExtender ID="popexStazioni" runat="server" TargetControlID="imgStazioniSelectors"
										PopupControlID="pnlStazioniFilters" Position="Bottom" OffsetX="-203" OffsetY="39" />
								</td>
							</tr>
                            <tr>
                                <td width="33%" class="filtersLBL" style="padding-left: 80px;">
									Sistema
								</td>
								<td width="33%" class="filtersLBL" style="padding-left: 60px;">
									Tipo Apparato
								</td>
								<td width="33%" class="filtersLBL" style="padding-left: 85px;">
									Stati
								</td>

                            </tr>
                            <tr>
								<td>
									<asp:TextBox runat="server" ID="txtSistemi" TextMode="MultiLine" CssClass="filtersTA"
										ReadOnly="true" /><asp:Image runat="server" ID="imgSistemiSelectors" ImageUrl="~/IMG/Interfaccia/Arrow3 Down.png"
											CssClass="filtersTAi" />
									<act:PopupControlExtender ID="popexSistemi" runat="server" TargetControlID="imgSistemiSelectors"
										PopupControlID="pnlSistemiFilters" Position="Bottom" OffsetX="-203" OffsetY="39" />
								</td>
								<td>
									<asp:TextBox runat="server" ID="txtTipiApparato" TextMode="MultiLine" CssClass="filtersTA"
										ReadOnly="true" /><asp:Image runat="server" ID="imgTipiApparatoSelectors" ImageUrl="~/IMG/Interfaccia/Arrow3 Down.png"
											CssClass="filtersTAi" />
									<act:PopupControlExtender ID="popexTipiApparato" runat="server" TargetControlID="imgTipiApparatoSelectors"
										PopupControlID="pnlTipiApparatoFilters" Position="Bottom" OffsetX="-501" OffsetY="39" />
								</td>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkStato0" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStato1" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStato2" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStato3" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStato9" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStato255" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStatom1" CssClass="filtersCHKS" />
									<asp:CheckBox runat="server" ID="chkStatom255" CssClass="filtersCHKS" />
                                    <br>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStato0" ImageUrl="~/IMG/Interfaccia/tab_stato_0.gif" 
                                               CssClass="filtersCHKSI" ToolTip="In servizio"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStato1" ImageUrl="~/IMG/Interfaccia/tab_stato_1.gif" 
                                               CssClass="filtersCHKSI" ToolTip="Anomalia lieve"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStato2" ImageUrl="~/IMG/Interfaccia/tab_stato_2.gif" 
                                               CssClass="filtersCHKSI" ToolTip="Anomalia grave"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStato3" ImageUrl="~/IMG/Interfaccia/tab_stato_3.gif" 
                                               CssClass="filtersCHKSI" ToolTip="Offline"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStato9" ImageUrl="~/IMG/Interfaccia/tab_stato_9.gif" 
                                               CssClass="filtersCHKSI" ToolTip="Non attivo"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStato255" ImageUrl="~/IMG/Interfaccia/tab_stato_255.gif"
                                               CssClass="filtersCHKSI" ToolTip="Sconosciuto"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStatom1" ImageUrl="~/IMG/Interfaccia/tab_stato_m1.gif"
                                               CssClass="filtersCHKSI" ToolTip="Non classificato"/>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgStatom255" ImageUrl="~/IMG/Interfaccia/tab_stato_-255.gif"
                                               CssClass="filtersCHKSI" ToolTip="Informazione"/>
								</td>

                            </tr>
							<tr>
							</tr>
                            <tr><td colspan="5">
                                <asp:CheckBox runat="server" id="chkShared"  CssClass="filtersCHKS filtersLBL"
                                  Text="Filtro condiviso" />
                            </td></tr>

						    <tr><td colspan="5">&nbsp;</td></tr>
						</table>
						<table width="900" >
							<tr>
								<td width="110px"  class="filtersLBL" style="vertical-align: top">
									<asp:Label ID="lblRicerca" EnableViewState="false" runat="server" CssClass="filtersMainLBL">
									    Ricerca
									</asp:Label>
								</td>
								<td class="filtersCNT">
									<asp:TextBox runat="server" ID="txtRicerca" Style="width: 726px; height: 15px;
										vertical-align: top; padding-top: 1px;" CssClass="filtersTA" MaxLength="110" />
                                    <act:TextBoxWatermarkExtender ID="tbw" runat="server"
                                        TargetControlID="txtRicerca"
                                        WatermarkText="Nome della periferica, Indirizzo, IP, Numero di serie"
                                        WatermarkCssClass="filtersWatermarked" />
								</td>
							</tr>
                            <tr>
                                <td colspan="2" class="filtersCNT" style="text-align: center">
                                    <asp:Button runat="server" ID="btnCerca" Style="vertical-align: top; margin: 5px;" OnClick="btnCerca_OnClick" Width="150px" Text="Cerca" />
                                    <asp:Button runat="server" ID="btnCercaSalva"  Style="vertical-align: top; margin: 5px;" Width="150px" Text="Cerca e Salva" OnClick="btnCercaSalva_OnClick"/> 
                                    <asp:Button runat="server" ID="btnExportXls"  Style="vertical-align: top; margin: 5px;" Width="150px" Text="Esporta (Excel)" OnClick="btnExportXls_OnClick" /> 
                                </td>
							</tr>

						</table>
					</ContentTemplate>
				</asp:UpdatePanel>
				<asp:UpdatePanel runat="server" ID="updpnlDevices" UpdateMode="Conditional">
					<ContentTemplate>
                    	<asp:Panel runat="server" ID="pnlSearchMessage" Style="text-align: center; width: 100%; margin: 10px; color: #551111; background-color: #AAAA11; "
							Visible="false">
							<asp:Label runat="server" ID="lblSearchMessage" Style=" font-size: 12px; font-style: italic;" />
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlDevices" Style="text-align: center; width: 100%;"
							Visible="false">
							<asp:Repeater ID="rptDev" runat="server" 
                                OnDataBinding="rptDev_DataBinding" 
                                OnItemCommand="rptDev_ItemCommand"
								OnItemDataBound="rptDev_ItemDataBound">
								<HeaderTemplate>
									<table style="width: 95%" class="filtersDev">
										<tr>
											<th class="filtersDevTh" style="width: 60px; border-right: 3px solid #f4f4f4; text-align: left;">
												<asp:LinkButton ID="lnkbDeviceStatus" runat="server" Text="Stato" CommandName="SortDeviceStatus"
													Style="width: 35px; height: 16px; padding-left: 2px; display: inline-block;"></asp:LinkButton>
												<asp:ImageButton ID="imgbSortDeviceStatus" ImageUrl="~/IMG/Interfaccia/Arrow2 Up.png"
													Visible="False" runat="server" CommandName="SortDeviceStatus" Style="margin-bottom: -2px;
													display: inline-block; height: 16px;" />
											</th>
											<th class="filtersDevTh" style="width: 242px;">
												Stato Diagnostico
											</th>
											<th class="filtersDevTh" style="width: 298px; text-align: left;">
												<asp:LinkButton ID="lnkbDeviceName" runat="server" Text="Periferica" CommandName="SortDeviceName"
													Style="width: 80px; height: 16px; padding-left: 85px; display: inline-block;"></asp:LinkButton>
												<asp:ImageButton ID="imgbSortDeviceName" ImageUrl="~/IMG/Interfaccia/Arrow2 Up.png"
													Visible="False" runat="server" CommandName="SortDeviceName" Style="margin-bottom: -2px;
													display: inline-block; height: 16px;" />
											</th>
											<th class="filtersDevTh" style="width: 95px;">
												Indirizzo
											</th>
											<th class="filtersDevTh" style="width: 148px; text-align: left;">
												<asp:LinkButton ID="lnkbDeviceType" runat="server" Text="Tipo" CommandName="SortDeviceType"
													Style="width: 28px; height: 16px; padding-left: 50px; display: inline-block;"></asp:LinkButton>
												<asp:ImageButton ID="imgbSortDeviceType" ImageUrl="~/IMG/Interfaccia/Arrow2 Up.png"
													Visible="False" runat="server" CommandName="SortDeviceType" Style="margin-bottom: -2px;
													display: inline-block; height: 16px;" />
											</th>
											<th class="filtersDevTh" style="width: 80px">
												Sistema
											</th>
											<th class="filtersDevTh" style="width: 228px; text-align: left;">
												<asp:LinkButton ID="lnkbNodeName" runat="server" Text="Stazione" CommandName="SortNodeName"
													Style="width: 52px; height: 16px; padding-left: 73px; display: inline-block;"></asp:LinkButton>
												<asp:ImageButton ID="imgbSortNodeName" ImageUrl="~/IMG/Interfaccia/Arrow2 Up.png"
													Visible="False" runat="server" CommandName="SortNodeName" Style="margin-bottom: -2px;
													display: inline-block; height: 16px;" />
											</th>
										</tr>
								</HeaderTemplate>
								<FooterTemplate>
									</table>
								</FooterTemplate>
								<ItemTemplate>
									<tr>
										<td class="filtersDevTd" style="border-right: 3px solid #f4f4f4;">
											<gris:DeviceTypeIconAndStatus ID="deviceTypeIconAndStatus" runat="server" DevID='<%# Eval("DevID") %>'
												DeviceType='<%# Eval("Type") %>' SevLevel='<%# Eval("SevLevel") %>' StatusDescription='<%# Eval("SevLevelDescriptionComplete") %>'
												OnDeviceTypeIconAndStatusClick="DeviceTypeIconAndStatus_Click" />
										</td>
										<td class="filtersDevTd">
											<gris:DeviceStreamFieldsPill ID="deviceStreamFieldsPill" runat="server" DevID='<%# Eval("DevID") %>'
												DeviceType='<%# Eval("Type") %>' SevLevel='<%# Eval("SevLevel") %>' CountStatusm255='<%# Eval("Cm255") %>'
												CountStatus255='<%# Eval("C255") %>' CountStatus0='<%# Eval("C0") %>' CountStatus1='<%# Eval("C1") %>'
												CountStatus2='<%# Eval("C2") %>' CountStatus9='<%# Eval("C9") %>' />
											<asp:ImageButton runat="server" ID="imgbDevDetail" ImageUrl="~/IMG/Interfaccia/Info2.png"
												CommandName="SDD" CommandArgument='<%# Eval("DevID") %>' CssClass="filterImg"
												Visible='<%# ((int)Eval("Cm255") + (int)Eval("C0") + (int)Eval("C1") + (int)Eval("C2") + (int)Eval("C255")) > 0 %>' />
										</td>
										<td class="filtersDevTd" style="text-align: left !important; padding: 5px !important;">
											<asp:Label runat="server" ID="lblDevName" Text='<%#Eval("Name")%>' />
										</td>
										<td class="filtersDevTd">
											<asp:Label runat="server" ID="lblAddress" Text='<%#Eval("Address")%>' ToolTip='<%#Eval("Position")%>' />
										</td>
										<td class="filtersDevTd">
											<asp:Label runat="server" ID="lblType" Text='<%#Eval("Type")%>' />
										</td>
										<td class="filtersDevTd">
											<asp:Image runat="server" ID="imgSystem" ImageUrl='<%# String.Format ("~/IMG/Interfaccia/alerts_system_{0}.gif", Eval("SystemID")) %>'
												ToolTip='<%#Eval("SystemDescription")%>' />
										</td>
										<td class="filtersDevTd">
											<asp:Label runat="server" ID="lblStazione" Text='<%#Eval("Node")%>' ToolTip='<%#Eval("Topography")%>' />
										</td>
									</tr>
									<tr runat="server" id="trDD" visible="false" enableviewstate="false">
										<td colspan="7" style="background-color: #808080; background-image: url('IMG/interfaccia/sd.png');
											background-repeat: no-repeat;">
											<asp:Panel runat="server" ID="pnlDeviceDetail" Style="text-align: center; width: 100%;">
												<asp:Repeater ID="rptDevDet" runat="server" OnItemDataBound="rptDevDet_ItemDataBound">
													<HeaderTemplate>
														<table style="width: 1050px" class="filtersDevDet">
															<thead>
																<tr>
																	<th class="filtersDevDetTh" style="border-right: 0px none;">
																		&nbsp;
																	</th>
																	<th colspan="3" class="filtersDevDetTh" style="color: #fff; border-left: 0px none;
																		border-right: 0px none;">
																		<asp:Label runat="server" ID="lblDeviceData" />
																	</th>
                                                                    <!--
																	<th class="filtersDevDetTh" style="border-left: 0px none;">
																		<asp:Button runat="server" ID="imgbDevHistory" CssClass="filtersSB" UseSubmitBehavior="false" />
																		<asp:Panel ID="pnlDH" runat="server" CssClass="filtersSFHn" />
																		<act:DynamicPopulateExtender ID="dpDH" runat="server" TargetControlID="pnlDH" ClearContentsDuringUpdate="true"
																			ServiceMethod="GetDeviceHistoryHtml" UpdatingCssClass="filtersSFHu" />
																		<act:ModalPopupExtender ID="mdlpopexDevHistory" runat="server" TargetControlID="imgbDevHistory"
																			PopupControlID="pnlDH" BackgroundCssClass="ModalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
																	</th>
                                                                    -->
																</tr>
																<tr>
																	<th class="filtersDevDetTh" style="width: 55px;">
																		Stato
																	</th>
																	<th class="filtersDevDetTh" style="width: 350px;">
																		Messaggio
																	</th>
																	<th class="filtersDevDetTh" style="width: 380px;">
																		Valore
																	</th>
																	<th class="filtersDevDetTh" style="width: 130px;">
																		Data ricezione
																	</th>
                                                                    <!--
																	<th class="filtersDevDetTh" style="width: 80px">
																		Email
																	</th>
																	<th class="filtersDevDetTh" style="width: 55px">
																		Storico
																	</th>
                                                                    -->
																</tr>
															</thead>
													</HeaderTemplate>
													<FooterTemplate>
														</table>
													</FooterTemplate>
													<ItemTemplate>
														<tr>
															<td class="filtersDevDetTd">
																<asp:Image runat="server" ID="imgDDSevLevel" ImageUrl='<%# String.Format ("~/IMG/Interfaccia/tab_stato_{0}.gif", Eval("StreamFieldSevLevel")) %>' />
															</td>
															<td class="filtersDevDetTd" style="text-align: left !important; padding: 5px !important;">
																<asp:Label runat="server" ID="lblMessage" Text='<%#Eval("Message")%>' />
															</td>
															<td class="filtersDevDetTd">
																<asp:Label runat="server" ID="lblStreamFieldValue" Text='<%#Eval("StreamFieldValue")%>' />
															</td>
															<td class="filtersDevDetTd">
																<asp:Label runat="server" ID="lblDate" Text='<%#String.Format("{0:dd/MM/yyyy HH:mm}", (DateTime)Eval("Date"))%>' />
															</td>
                                                            <!--
															<td class="filtersDevDetTd">
																<asp:Label runat="server" ID="lblEmail" Text='<%#((Eval("ShouldSendNotificationByEmail")).ToString() == "1") ? "email" : "-"%>' />
															</td>
															<td class="filtersDevDetTd">
																<asp:Button runat="server" ID="imgbDevDetailHistory" CssClass="filtersSB" UseSubmitBehavior="false"
																	Visible='<%#(bool)Eval("IsVisibleOnHistoryReportFilter")%>' />
																<asp:Panel ID="pnlSFH" runat="server" CssClass="filtersSFHn" Visible='<%#(bool)Eval("IsVisibleOnHistoryReportFilter")%>' />
																<asp:Label runat="server" ID="lblDevDetailHistoryNA" Text="N/D" Visible='<%#!(bool)Eval("IsVisibleOnHistoryReportFilter")%>' />
																<act:DynamicPopulateExtender ID="dpSFH" runat="server" TargetControlID="pnlSFH" ClearContentsDuringUpdate="true"
																	ServiceMethod="GetStreamFieldHistoryHtml" UpdatingCssClass="filtersSFHu" />
																<act:ModalPopupExtender ID="mdlpopexDevDetailHistory" runat="server" TargetControlID="imgbDevDetailHistory"
																	PopupControlID="pnlSFH" BackgroundCssClass="ModalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
															</td>
                                                            -->
														</tr>
													</ItemTemplate>
												</asp:Repeater>
											</asp:Panel>
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</asp:Panel>
					</ContentTemplate>
				</asp:UpdatePanel>
				<asp:Panel ID="pnlFiltri" runat="server" CssClass="filtersPopupMenu" Style="width: 677px;">
					<asp:UpdatePanel runat="server" ID="updpnlFiltri" UpdateMode="Conditional">
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="dlstFiltri" EventName="ItemCommand" />
						</Triggers>
						<ContentTemplate>
							<asp:DataList ID="dlstFiltri" runat="server" CssClass="filtersCHKL" OnItemCommand="dlstFiltri_ItemCommand"
								OnItemDataBound="dlstFiltri_ItemDataBound">
								<ItemTemplate>
									<asp:Button runat="server" ID="imgbSelectFilter" CssClass="filtersCHKLS" UseSubmitBehavior="false"
										ToolTip="Seleziona il filtro" CommandName="FS" CommandArgument='<%# String.Format("{0}‡{1}", Eval("DeviceFilterId"), Eval("DeviceFilterName"))%>' />
									<asp:Label ID="lblFilter" runat="server" Text='<%#Eval("DeviceFilterName")%>' />
									<asp:Button runat="server" ID="imgbDeleteFilter" CssClass="filtersCHKLD" ToolTip="Elimina il filtro" 
										OnClientClick="return false;" UseSubmitBehavior="false" CommandName="FD" CommandArgument='<%# String.Format("{0}‡{1}", Eval("DeviceFilterId"), Eval("DeviceFilterName"))%>' 
                                        />
									<act:ModalPopupExtender ID="mdlpopexDelete" runat="server" TargetControlID="imgbDeleteFilter"
										PopupControlID="divConfirmDelete" OkControlID="btnOkDelete" OnOkScript="okClick2();"
										CancelControlID="btnCancelDelete" OnCancelScript="cancelClick();" BackgroundCssClass="ModalBackground"
										RepositionMode="RepositionOnWindowScroll" />
									<asp:Panel id="divConfirmDelete" runat="server" class="deleteConfirm" style="display: none">
										<img src="IMG/Interfaccia/warning.gif" alt="warning" style="padding-right: 10px;
											vertical-align: middle" />Vuoi eliminare il filtro '<%# Eval("DeviceFilterName")%>'?<br />
										<br />
										<asp:Button ID="btnOkDelete" runat="server" Text="Sì" CausesValidation="true" CssClass="confirmButton" />
										<asp:Button ID="btnCancelDelete" runat="server" Text="No" CausesValidation="false"
											CssClass="confirmButton" />
									</asp:Panel>
								</ItemTemplate>
							</asp:DataList>
						</ContentTemplate>
					</asp:UpdatePanel>

				</asp:Panel>
				<asp:Panel ID="pnlCompartimentiFilters" runat="server" CssClass="filtersPopupMenu">
					<asp:UpdatePanel runat="server" ID="updpnlCompartimentiFilters" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:CheckBoxList runat="server" ID="chklCompartimenti" AutoPostBack="True" OnSelectedIndexChanged="chklCompartimenti_SelectedIndexChanged"
								DataSourceID="SqlDataSourceCompartimenti" DataTextField="Name" DataValueField="RegID"
								CssClass="filtersCHKL">
							</asp:CheckBoxList>
						</ContentTemplate>
					</asp:UpdatePanel>
				</asp:Panel>
				<asp:Panel ID="pnlLineeFilters" runat="server" CssClass="filtersPopupMenu">
					<asp:UpdatePanel runat="server" ID="updpnlLineeFilters" UpdateMode="Conditional">
						<ContentTemplate>
						    <asp:CheckBox runat="server" id="chkAllLinee" Text="Seleziona tutto" OnCheckedChanged="chkAllLinee_OnCheckedChanged" AutoPostBack="True" />
							<asp:CheckBoxList runat="server" ID="chklLinee" AutoPostBack="True" OnSelectedIndexChanged="chklLinee_SelectedIndexChanged"
								DataSourceID="SqlDataSourceLinee" DataTextField="Name" DataValueField="ZonID"
								CssClass="filtersCHKL">
							</asp:CheckBoxList>
						</ContentTemplate>
					</asp:UpdatePanel>
				</asp:Panel>
				<asp:Panel ID="pnlStazioniFilters" runat="server" CssClass="filtersPopupMenu">
					<asp:UpdatePanel runat="server" ID="updpnlStazioniFilters" UpdateMode="Conditional">
						<ContentTemplate>
						    <asp:CheckBox runat="server" id="chkAllStazioni" Text="Seleziona tutto" OnCheckedChanged="chkAllStazioni_OnCheckedChanged" AutoPostBack="True" />
							<asp:CheckBoxList runat="server" ID="chklStazioni" AutoPostBack="True" OnSelectedIndexChanged="chklStazioni_SelectedIndexChanged"
								DataTextField="Name" DataValueField="NodID" CssClass="filtersCHKL" OnDataBinding="chklStazioni_DataBinding">
							</asp:CheckBoxList>
						</ContentTemplate>
					</asp:UpdatePanel>
				</asp:Panel>
				<asp:Panel ID="pnlSistemiFilters" runat="server" CssClass="filtersPopupMenu">
					<asp:UpdatePanel runat="server" ID="updpnlSistemiFilters" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:CheckBoxList runat="server" ID="chklSistemi" AutoPostBack="True" OnSelectedIndexChanged="chklSistemi_SelectedIndexChanged"
								DataSourceID="SqlDataSourceSistemi" DataTextField="SystemDescription" DataValueField="SystemID"
								CssClass="filtersCHKL">
							</asp:CheckBoxList>
						</ContentTemplate>
					</asp:UpdatePanel>
				</asp:Panel>
				<asp:Panel ID="pnlTipiApparatoFilters" runat="server" CssClass="filtersPopupMenu"
					Style="width: 500px;">
					<asp:UpdatePanel runat="server" ID="updpnlTipiApparatoFilters" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:CheckBoxList runat="server" ID="chklTipiApparato" AutoPostBack="True" OnSelectedIndexChanged="chklTipiApparato_SelectedIndexChanged"
								DataSourceID="SqlDataSourceTipiApparato" DataTextField="DeviceTypeDescription"
								DataValueField="DeviceTypeID" CssClass="filtersCHKL">
							</asp:CheckBoxList>
						</ContentTemplate>
					</asp:UpdatePanel>
				</asp:Panel>
				<asp:SqlDataSource ID="SqlDataSourceCompartimenti" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					OnSelecting="SqlDataSourceCompartimenti_Selecting" SelectCommand="gris_GetRegionList"
					SelectCommandType="StoredProcedure">
					<SelectParameters>
						<asp:Parameter Name="VisibleRegions" Type="String" />
					</SelectParameters>
				</asp:SqlDataSource>
				<asp:SqlDataSource ID="SqlDataSourceLinee" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					SelectCommand="gris_GetZoneList" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceLinee_Selecting">
					<SelectParameters>
						<asp:Parameter Name="VisibleRegions" Type="String" />
						<asp:Parameter Name="RegIDs" Type="String" />
					</SelectParameters>
				</asp:SqlDataSource>
				<asp:SqlDataSource ID="SqlDataSourceStazioni" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					SelectCommand="gris_GetNodeList" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceStazioni_Selecting">
					<SelectParameters>
						<asp:Parameter Name="VisibleRegions" Type="String" />
						<asp:Parameter Name="RegIDs" Type="String" />
						<asp:Parameter Name="ZonIDs" Type="String" />
					</SelectParameters>
				</asp:SqlDataSource>
				<asp:SqlDataSource ID="SqlDataSourceSistemi" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					OnSelecting="SqlDataSourceSistemi_Selecting" SelectCommand="gris_GetSystemList"
					SelectCommandType="StoredProcedure">
					<SelectParameters>
						<asp:Parameter Name="NodIDs" Type="String" />
					</SelectParameters>
				</asp:SqlDataSource>
				<asp:SqlDataSource ID="SqlDataSourceTipiApparato" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					OnSelecting="SqlDataSourceTipiApparato_Selecting" SelectCommand="gris_GetDeviceTypeList"
					SelectCommandType="StoredProcedure">
					<SelectParameters>
						<asp:Parameter Name="NodIDs" Type="String" />
						<asp:Parameter Name="SystemIDs" Type="String" />
					</SelectParameters>
				</asp:SqlDataSource>
				<asp:SqlDataSource ID="SqlDataSourceFindDevices" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					OnSelecting="SqlDataSourceFindDevices_Selecting" SelectCommand="gris_GetFindDevices"
					SelectCommandType="StoredProcedure">
					<SelectParameters>
						<asp:Parameter Name="NodIDs" Type="String" />
						<asp:Parameter Name="SystemIDs" Type="String" />
						<asp:Parameter Name="DeviceTypeIDs" Type="String" />
						<asp:Parameter Name="SevLevelIDs" Type="String" />
						<asp:Parameter Name="DeviceData" Type="String" />
					</SelectParameters>
				</asp:SqlDataSource>
				<asp:SqlDataSource ID="SqlDataSourceDeviceDetail" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
					OnSelecting="SqlDataSourceDeviceDetail_Selecting" SelectCommand="gris_GetStreamsFieldsDataByDevice"
					SelectCommandType="StoredProcedure">
					<SelectParameters>
						<asp:Parameter Name="DevID" Type="Int64" />
					</SelectParameters>
				</asp:SqlDataSource>
			</td>
		</tr>
	</table>

</asp:Content>


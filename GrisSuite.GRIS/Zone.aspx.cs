using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.NodeDSTableAdapters;

public partial class Zone : NavigationPage
{

    #region Properties
    public override MultiView LayoutMultiView
    {
        get { return this.LineaMultiView; }
    }

    public override View GraphView
    {
        // Forzato a vista tabellare, quando era GraphicalView, per evitare che la linearizzata sia visibile
        get { return this.TabularView; }
    }

    public override View GridView
    {
        get { return this.TabularView; }
    }
    #endregion

    # region Event Handlers
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.tmrZone.Interval = DBSettings.GetPageRefreshTime() * 1000;

            this.RegID = this.GetID("RegID", "~/Italia.aspx");
            this.ZonID = this.GetID("ZonID", "~/Region.aspx");

            # region Visualizzazione informazione in base ai permessi
            // Colonna Ack disabilitata nella visualizzazione tabellare dalla versione 1.4.0.0
            //if ( !GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3) )
            //{
            this.gvwStazioni.Columns[10].Visible = false; // la colonna 10 � quella degli ack
            this.gvwStazioni.Columns[11].Visible = false; // la colonna 11 � il separatore
            //}

            // Nascondo la colonna Warning nella visualizzazione tabellare se l'utente non appartiene ad un gruppo che sia almeno di livello 2
            if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2))
            {
                this.gvwStazioni.Columns[8].Visible = false; // la colonna 8 � quella dei warning
                this.gvwStazioni.Columns[9].Visible = false; // la colonna 9 � il separatore
            }
            # endregion

            # region Binding Linearizzata
            this.DataRefresh();
            # endregion
        }

    }

    protected void tmrZone_Tick(object sender, EventArgs e)
    {
        this.DataRefresh();
    }

    protected void rptTratta_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        IGrouping<int, NodeDS.NodeRow> stazioni = e.Item.DataItem as IGrouping<int, NodeDS.NodeRow>;
        DataList dlstStazione = e.Item.FindControl("dlstStazione") as DataList;

        if (stazioni != null && dlstStazione != null)
        {
            dlstStazione.DataSource = stazioni;
            dlstStazione.DataBind();
        }
    }

    # region dlstStazione
    protected void dlstStazione_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        // scrittura etichette sulla barra di navigazione
        NodeDS.NodeRow row = e.Item.DataItem as NodeDS.NodeRow;

        if (row != null)
        {
            this.SetNavigationLabels(row);

            StazioneIcon stazIcon = (StazioneIcon)e.Item.FindControl("StazioneIcon1");

            if (stazIcon != null)
            {
                stazIcon.Index = e.Item.ItemIndex;
                stazIcon.Count = ((IGrouping<int, NodeDS.NodeRow>)((DataList)sender).DataSource).Count();
                stazIcon.RepeatColumns = ((DataList)sender).RepeatColumns;
            }
        }
    }

    protected void dlstStazione_ItemCreated(object sender, DataListItemEventArgs e)
    {
        StazioneIcon stazIcon = (StazioneIcon)e.Item.FindControl("StazioneIcon1");

        if (stazIcon != null) stazIcon.StazioneClick += new ImageClickEventHandler(StazioneIcon_StazioneClick);
    }
    # endregion

    # region gvwStazioni

    protected void gvwStazioni_SelectedIndexChanged(object sender, EventArgs e)
    {
        string nodID = ((GridView)sender).SelectedDataKey["NodID"].ToString();
        if (nodID != null)
        {
            this.GoToPage(PageDestination.Node, long.Parse(nodID));
        }
    }

    protected void gvwStazioni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        NodeDS.NodeRow row;
        if (e.Row.RowType == DataControlRowType.DataRow && (row = ((DataRowView)e.Row.DataItem).Row as NodeDS.NodeRow) != null)
        {
            // scrittura etichette sulla barra di navigazione
            this.SetNavigationLabels(row);

            // gli appartenenti ad un gruppo di livello 1 non possono vedere le periferiche in attenzione
            if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2) && row.Status == 1/* attenzione */ ) row.Status = 0;
            if (row.Status == -1) row.Status = 255;

            # region immagine stato stazione
            Controls_LayeredIcon imgStatus = (Controls_LayeredIcon)e.Row.FindControl("ImageStatus");

            if (row.STLCTotalCount == row.STLCOffline)
            {
                imgStatus.Status = 2; // solo se tutti gli stlc offline
                imgStatus.LayeredIconTypeAdditionalLayer = Controls_LayeredIcon.IconTypeAdditionalLayer.Offline;
                imgStatus.ImageToolTip = string.Format("{0} offline", GUtility.GetLocalServerTypeName());
            }
            else
            {
                imgStatus.Status = row.Status;
                if (!row.IsNodeColorNull())
                {
                    imgStatus.CssBackgroundColor = row.NodeColor;
                }
                imgStatus.ImageToolTip = row.StatusDescription;
            }

            # endregion

            # region gestione permessi di visualizzazione delle periferiche della stazione
            LinkButton lnk = (LinkButton)e.Row.FindControl("lnkbStazione");
            WebControl imgInfo = (WebControl)e.Row.Cells[14].Controls[0];		// immagine in fondo alla griglia
            //( (WebControl)lnk.Parent ).Style.Add(HtmlTextWriterStyle.Position, "relative");

            // se la stazione non ha STLC1000 online o in maint mode e l'utente non appartiene almeno ad un gruppo di livello 3 non si pu� accedere alla pagina delle periferiche
            Controls_InfoFloatingBalloon infoBalloon = e.Row.FindControl("InfoBalloon") as Controls_InfoFloatingBalloon;
            if (Controllers.Node.CanUserViewDevices(row.RegID, row.STLCOK, row.STLCInMaintenance, row.STLCTotalCount))
            {
                GUtility.EnableLinkButton(this, lnk, true);
                imgInfo.Enabled = true;
            }
            else
            {
                GUtility.EnableLinkButton(this, lnk, false);
                imgInfo.Enabled = false;

                if (infoBalloon != null)
                {
                    lnk.Attributes["OnMouseOver"] = infoBalloon.OverScript;
                    lnk.Attributes["OnMouseOut"] = infoBalloon.OutScript;
                }
            }
            # endregion
        }
    }
    # endregion

    void StazioneIcon_StazioneClick(object sender, ImageClickEventArgs e)
    {
        StazioneIcon stazIcon = (StazioneIcon)sender;
        this.GoToPage(PageDestination.Node, long.Parse(stazIcon.NodID));
    }

    protected void odsNodes_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["ZonId"] = this.ZonID;
    }

    # region navBar
    protected void navBar_OnLayoutSwitched(object sender, EventArgs e)
    {
        this.GoToPage(PageDestination.Zone, this.ZonID);
    }

    protected void navBar_OnRefreshPageButtonClick(object sender, ImageClickEventArgs e)
    {
        this.DataRefresh();
    }
    # endregion
    # endregion

    # region Methods
    private void SetNavigationLabels(NodeDS.NodeRow row)
    {
        if (row != null)
        {
            this.navBar.LinkLineaText = row.ZonName.Replace("Linea ", "");
            this.navBar.LinkLineaTooltip = row.ZonName;
            this.navBar.SetCompartimentoSeverita(row.SevLevelRegion, (row.IsRegionColorNull() ? null : row.RegionColor));
            this.navBar.SetLineaSeverita(row.SevLevelZone, (row.IsZoneColorNull() ? null : row.ZoneColor));
        }
    }

    private void DataRefresh()
    {
        if (this.navBar.LayoutMode == MapLayoutMode.Graph)
        {
            // l'introduzione della visualizzazione della linearizzata per tratte ha reso necessario il binding programmatico del repeater contenente i datalist
            NodeTableAdapter ta = new NodeTableAdapter();
            var stazioni = ta.GetData(this.ZonID);

            var tratte = from stz in stazioni.AsEnumerable()
                         group stz by (stz.Meters / 1000000);

            this.rptTratta.DataSource = tratte;
            this.rptTratta.DataBind();
        }
        else
        {
            this.gvwStazioni.DataBind();
        }

        this.updPageLayout.Update();
        this.updNavBar.Update();
    }
    # endregion
}

﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.AlertsDSTableAdapters;

public partial class Alerts : FiltersSavingPage
{
	private LinkButton _sortHeader;
	
	# region Properties

	# region Nested Controls
	protected TextBox txtCurrentPageTop
	{
		get
		{
			return this.dpgAlertsTop.Controls[0].FindControl("txtCurrentPageTop") as TextBox;
		}
	}

	protected TextBox txtCurrentPageBot
	{
		get
		{
			return this.dpgAlertsBot.Controls[0].FindControl("txtCurrentPageBot") as TextBox;
		}
	}

	protected Label lblPageNumberTop
	{
		get
		{
			return this.dpgAlertsTop.Controls[0].FindControl("lblPageNumberTop") as Label;
		}
	}

	protected Label lblPageNumberBot
	{
		get
		{
			return this.dpgAlertsBot.Controls[0].FindControl("lblPageNumberBot") as Label;
		}
	}
	# endregion
	
	public long DevID
	{
		get { return Convert.ToInt64(this.Session["DevID"] ?? -1); }
		set { this.Session["DevID"] = value; }
	}
	// proprità memorizzate in sessione 
	public int CurrentAlertPage
	{
		get { return Convert.ToInt32(this.Session["CurrentAlertPage"] ?? -1); }
		set { this.Session["CurrentAlertPage"] = value; }
	}
	
	public int TotalAlertPages
	{
		get { return Convert.ToInt32(this.Session["TotalAlertPages"] ?? -1); }
		set { this.Session["TotalAlertPages"] = value; }
	}
	# endregion
	
	# region Event Handlers
	protected void Page_Load ( object sender, EventArgs e )
	{
		if ( !GrisPermissions.CanUserViewAlerts() )
		{
			this.Response.Redirect("~/Italia.aspx");
		}
		
		if ( !this.IsPostBack )
		{
			this.tmrAlerts.Interval = DBSettings.GetPageRefreshTime() * 1000;

            this.imgbUpdate.Attributes["OnMouseOver"] = @"this.src = this.src.replace(/btnRefresh_\w*\.gif/, 'btnRefresh_over.gif');";
			this.imgbUpdate.Attributes["OnMouseOut"] = "this.src = this.src.replace('_over', '_default');";

			// solo gli operatori degli allarmi e gli amministratori possono assegnare tickets
			this.txtSaveTicketName.Enabled = GrisPermissions.CanUserEditAlerts();
			this.txtSaveTicketNotes.Enabled = GrisPermissions.CanUserEditAlerts();
			this.imgbSaveTicket.Visible = GrisPermissions.CanUserEditAlerts();
			
			// inizializzazione filtri di pagina
			this.FilterControlIDs.Add(this.txtTicketName.ID);
			this.FilterControlIDs.Add(this.FilterNew.ID);
			this.FilterControlIDs.Add(this.FilterProcessed.ID);
			this.FilterControlIDs.Add(this.FilterDiscarded.ID);
			this.FilterControlIDs.Add(this.dlstRules.ID);
			this.FilterControlIDs.Add(this.dlstDevType.ID);
			this.FilterControlIDs.Add(this.lblSelectedRule.ID);
			this.FilterControlIDs.Add(this.lblDevType.ID);
			this.FilterControlIDs.Add(this.txtLocation.ID);
		}
	}

	protected void imgbUpdate_Click ( object sender, ImageClickEventArgs e )
	{
        this.imgbUpdate.ImageUrl = "~/IMG/interfaccia/btnRefresh_ok.gif";

		this.lvwAlerts.DataBind();
	}

	protected void tmrAlerts_Tick ( object sender, EventArgs e )
	{
		this.lvwAlerts.DataBind();
	}

	protected void FlagFilter_Click ( object sender, EventArgs e )
	{
		Controls_FilterButton filter = (Controls_FilterButton)sender;
		if ( filter.ID == "FilterProcessed" && !filter.Active ) this.txtTicketName.Text = "";
	
		this.lvwAlerts.DataBind();
		this.updFilters.Update();
	}
	
	protected void imgbDevice_Click ( object sender, ImageClickEventArgs e )
	{
		if ( this.DevID != -1 )
		{
			GrisSessionManager.GrisLayoutModeReset();
			Navigation.GoToPage(PageDestination.Device.ToString(), this.DevID);
		}
	}

	protected void imgbSaveTicket_Click ( object sender, ImageClickEventArgs e )
	{
		// solo gli operatori degli allarmi e gli amministratori possono assegnare tickets
		if ( this.IsValid && GrisSessionManager.SelectedAlert != Guid.Empty && ( GrisPermissions.CanUserEditAlerts() ) )
		{
			AlertTicketsTableAdapter ta = new AlertTicketsTableAdapter();
			if ( 1 == ta.UpdateTicket(this.txtSaveTicketName.Text, this.txtSaveTicketNotes.Text, DateTime.Now, GrisSessionManager.SelectedAlert) )
			{
				this.lvwAlerts.DataBind();
			}
			else
			{
				// TODO: feedback utente
				this.txtSaveTicketName.Text = "";
				this.txtSaveTicketNotes.Text = "";
			}
		}
		else
		{
			// TODO: feedback utente
			this.txtSaveTicketName.Text = "";
			this.txtSaveTicketNotes.Text = "";
		}
	}

	protected void btnTkSearch_Command ( object sender, CommandEventArgs e )
	{
		if ( e.CommandName.ToLower() == "clear" ) this.txtTicketName.Text = "";

		if ( this.txtTicketName.Text.Length > 0 )
		{
			this.dlstRules.SelectedIndex = -1;
			this.lblSelectedRule.Text = "Nessun Filtro";
			this.FilterNew.Active = true;
			this.FilterProcessed.Active = true;
			this.FilterDiscarded.Active = true;
		}
		this.lvwAlerts.DataBind();
	}

	protected void btnLcSearch_Command ( object sender, CommandEventArgs e )
	{
		if ( e.CommandName.ToLower() == "clear" ) this.txtLocation.Text = "";
		this.lvwAlerts.DataBind();
	}

	protected void dlstRules_ItemCommand ( object source, DataListCommandEventArgs e )
	{
		if ( e.CommandName.ToLower() == "select" )
		{
			LinkButton lnkbRule = e.Item.FindControl("lnkbRule") as LinkButton;

			if ( lnkbRule != null )
			{
				this.lblSelectedRule.Text = lnkbRule.Text;
				this.lblSelectedRule.ToolTip = lnkbRule.ToolTip;
			}
			this.updAlerts.Update();
		}
	}

	protected void dlstDevType_ItemCommand ( object source, DataListCommandEventArgs e )
	{
		if ( e.CommandName.ToLower() == "select" )
		{
			LinkButton lnkbDevType = e.Item.FindControl("lnkbDevType") as LinkButton;

			if ( lnkbDevType != null )
			{
				this.lblDevType.Text = lnkbDevType.Text;
				this.lblDevType.ToolTip = lnkbDevType.ToolTip;
			}
			this.updAlerts.Update();
		}
	}
	
	# region Object Data Sources
	protected void odsViolatedRules_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
	{
		if ( this.lvwAlerts.SelectedIndex == -1 ) e.Cancel = true;
	}
	
	protected void odsAlertTickets_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
	{
		// se trovo la pagina corrente inizializzata in sessione devo ripristinarla
		if ( !this.IsPostBack && this.CurrentAlertPage != -1 )
		{
			this.dpgAlertsTop.SetPageProperties(this.GetPageFirstRowIndex(this.CurrentAlertPage, this.dpgAlertsTop.PageSize, -1), this.dpgAlertsTop.PageSize, false);
		}
		
		this.SavePageFilters();
	}
	# endregion

	protected void txtCreatedOn_TextChanged ( object sender, EventArgs e )
	{
		this.lvwAlerts.SelectedIndex = -1;
		this.lvwAlerts.DataBind();
		this.updAlerts.Update();
	}
	
	# region lvwAlerts
	protected void lvwAlerts_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			ImageButton imgbAlertType = e.Item.FindControl("imgbAlertType") as ImageButton;
			Image imgSystem = e.Item.FindControl("imgSystem") as Image;
			Label lblTicketName = e.Item.FindControl("lblTicketName") as Label;

			ListViewDataItem currentDataItem = (ListViewDataItem)e.Item;
			AlertsDS.AlertTicketsRow alert = ( (DataRowView)currentDataItem.DataItem ).Row as AlertsDS.AlertTicketsRow;
			
			string imagePathTemplate = currentDataItem.DisplayIndex == this.lvwAlerts.SelectedIndex ? "~/IMG/Interfaccia/{0}{1}_selected.gif" : "~/IMG/Interfaccia/{0}{1}.gif";
			
			if ( alert != null )
			{
				if (imgSystem != null && alert.SystemID != -1)
					imgSystem.ImageUrl = string.Format(imagePathTemplate, "alerts_system_", alert.SystemID);

				if (imgbAlertType != null)
				{
					if (!alert.IsAlertTypeNull())
					{
						string alertType = alert.AlertType.ToLower();
						string warningToken = (alert.RuleSevLevel == 1 && alertType == "new") ? "_war" : "";
						string alertPath = string.Format("{0}{1}", alertType, warningToken);
						imgbAlertType.ImageUrl = string.Format(imagePathTemplate, "alerts_", alertPath);
						imgbAlertType.Visible = true;
					}
					else
					{
						imgbAlertType.Visible = false;
					}
				}

				if (lblTicketName != null)
				{
					lblTicketName.Text = "ticket";
					lblTicketName.ForeColor = System.Drawing.Color.Black;

					if (!alert.IsTicketNameNull() && !string.IsNullOrEmpty(alert.TicketName))
					{
						lblTicketName.Text = alert.TicketName;
						lblTicketName.ForeColor = System.Drawing.Color.White;
					}
				}
			}
		}
	}

	protected void lvwAlerts_DataBound ( object sender, EventArgs e )
	{
		bool gotAlerts = false;

		if ( this.lvwAlerts.Items.Count > 0 )
		{
			gotAlerts = true;

			int totRows = this.dpgAlertsTop.TotalRowCount;
			this.CurrentAlertPage = ( this.dpgAlertsTop.StartRowIndex / this.dpgAlertsTop.PageSize ) + 1;
			this.TotalAlertPages = (int)Math.Ceiling(totRows / (decimal)this.dpgAlertsTop.PageSize);
			
			this.lblAlertCountBot.Text = this.lblAlertCountTop.Text = string.Format("Totale Allarmi: {0}", totRows);
			
			// impostazioni relative alla paginazione
			this.txtCurrentPageBot.Text = this.txtCurrentPageTop.Text = this.CurrentAlertPage.ToString();
			this.lblPageNumberBot.Text = this.lblPageNumberTop.Text = string.Format(" di {0}", this.TotalAlertPages);

			// Ripristino della riga selezionata
			this.SelectAlert();
			if ( this.lvwAlerts.SelectedDataKey != null ) GrisSessionManager.SelectedAlert = (Guid)this.lvwAlerts.SelectedDataKey.Value;
		}

		// La paginazione è visibile solo con più di una pagina
		this.dpgAlertsBot.Visible = this.dpgAlertsTop.Visible = ( this.TotalAlertPages > 1 );

		this.tblDeviceDetail.Visible = this.lblAlertCountBot.Visible = gotAlerts;
		
		this.dpgAlertsTop.DataBind();
		this.dpgAlertsBot.DataBind();
		
		this.updAlerts.Update();
		this.UpdateDeviceDetailPanel(gotAlerts);

		if ( !this.IsPostBack )
		{
			this.FilterControlIDs.Add(this.txtCurrentPageTop.ID);
		}
	}

	protected void lvwAlerts_SelectedIndexChanged ( object sender, EventArgs e )
	{
		if ( this.lvwAlerts.SelectedDataKey != null ) GrisSessionManager.SelectedAlert = (Guid)this.lvwAlerts.SelectedDataKey.Value;
		this.UpdateDeviceDetailPanel(true);
	}

	protected void lvwAlerts_ItemCommand ( object sender, ListViewCommandEventArgs e )
	{
		if ( e.CommandName.Equals("select", StringComparison.InvariantCultureIgnoreCase) && (e.Item is ListViewDataItem) && (((ListViewDataItem)e.Item).DisplayIndex != this.lvwAlerts.SelectedIndex) )
		{
			this.DevID = Convert.ToInt64(e.CommandArgument);
		}
		else if ( e.CommandName.Equals("sort", StringComparison.InvariantCultureIgnoreCase) )
		{
			this._sortHeader = e.CommandSource as LinkButton;
		}
	}
	
	protected void lvwAlerts_Sorting ( object sender, ListViewSortEventArgs e )
	{
		if ( this._sortHeader != null )
		{
			this.ClearAlertHeaderSortSymbols();
			this._sortHeader.Text += e.SortDirection == SortDirection.Ascending ? "&nbsp;&#x2193;" : "&nbsp;&#x2191;";
		}
	}

	# endregion

	# region gvwViolatedRules
	protected void gvwViolatedRules_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row != null && e.Row.RowType == DataControlRowType.DataRow )
		{
			Image imgIsOpen = e.Row.FindControl("imgIsOpen") as Image;
			AlertsDS.ViolatedRulesRow violatedRule = ((DataRowView)e.Row.DataItem).Row as AlertsDS.ViolatedRulesRow;

			if ( violatedRule != null )
			{
				if ( imgIsOpen != null )
				{
					if ( violatedRule.IsAlertLogOpen )
					{
						imgIsOpen.ImageUrl = violatedRule.RuleSevLevel == 1 ? "~/IMG/Interfaccia/alerts_new_war_rule.gif" : "~/IMG/Interfaccia/alerts_new_rule.gif";
					}
					else
					{
						imgIsOpen.ImageUrl = "~/IMG/Interfaccia/alerts_discarded_rule.gif";
					}
				}
				
				if ( e.Row.DataItemIndex == 0 ) this.BindDeviceInAlert(violatedRule);
			}
		}
	}

	protected void gvwViolatedRules_DataBound(object sender, EventArgs e)
	{
		// Nel caso siano visualizzati più di 5 elementi, impostiamo l'altezza del DIV, in modo che appaiano le barre di scorrimento
		// Nel caso normale (fino a 5 elementi) l'altezza si adatta al contenuto
		// Su IE 7, è possibile supportare l'altezza massima via max-height, ma non funziona su IE 6 e precedenti
		if (this.gvwViolatedRules.Rows.Count > 5)
		{
			this.divViolatedRules.Style.Add("height", "175px");
		}
	}
	# endregion

	protected void dpgAlerts_PageChanged ( object sender, DataPagerCommandEventArgs e )
	{
		e.NewMaximumRows = e.Item.Pager.MaximumRows;

		switch ( e.CommandName.ToLower() )
		{
			case "first":
				{
					e.NewStartRowIndex = 0;
					
					break;
				}
			case "previous":
				{
					e.NewStartRowIndex = e.Item.Pager.StartRowIndex - e.Item.Pager.PageSize;

					break;
				}
			case "goto":
				{
					int currentAlertPage;
					string tmpCurrPage = ( e.CommandArgument.ToString() == "top" ) ? this.txtCurrentPageTop.Text : this.txtCurrentPageBot.Text;

					if ( !int.TryParse(tmpCurrPage, out currentAlertPage) ) currentAlertPage = 1;

					int newIndex = this.GetPageFirstRowIndex(currentAlertPage, e.Item.Pager.PageSize, e.Item.Pager.TotalRowCount);
					if ( newIndex <= e.TotalRowCount )
					{
						e.NewStartRowIndex = newIndex;
					}

					break;
				}
			case "next":
				{
					int newIndex = e.Item.Pager.StartRowIndex + e.Item.Pager.PageSize;
					if ( newIndex <= e.TotalRowCount )
					{
						e.NewStartRowIndex = newIndex;
					}

					break;
				}
			case "last":
				{
					e.NewStartRowIndex = this.GetLastPageFirstRowIndex(e.Item.Pager.TotalRowCount, e.Item.Pager.PageSize);
					
					break;
				}
		}
	}
	# endregion
	
	# region Methods
	private void ClearAlertHeaderSortSymbols()
	{
		Func<LinkButton, bool> cleanHeader = l =>
											  {
												if ( l != null )
												{
													l.Text = l.Text.Replace("&nbsp;&#x2191;", "").Replace("&nbsp;&#x2193;", "");
													return true;
												}

												return false;
											  };
											  
		cleanHeader(this.lvwAlerts.FindControl("lnkNodeHeader") as LinkButton);
		cleanHeader(this.lvwAlerts.FindControl("lnkDeviceHeader") as LinkButton);
		cleanHeader(this.lvwAlerts.FindControl("lnkSystemHeader") as LinkButton);
		cleanHeader(this.lvwAlerts.FindControl("lnkCreationDateHeader") as LinkButton);
		cleanHeader(this.lvwAlerts.FindControl("lnkCloseDateHeader") as LinkButton);
		cleanHeader(this.lvwAlerts.FindControl("lnkTicketHeader") as LinkButton);
	}
	
	private void UpdateDeviceDetailPanel ( bool bindToData )
	{
		if ( bindToData )
		{
			this.gvwViolatedRules.DataBind();		
		}
		else
		{
			this.tblDeviceDetail.Visible = false;
			this.dpgAlertsBot.Visible = this.dpgAlertsTop.Visible = false;

			this.lblAlertCountTop.Text = this.lblAlertCountBot.Text = "Totale Allarmi: 0";
		}

		this.updDeviceDetail.Update();
	}
	
	private void BindDeviceInAlert ( AlertsDS.ViolatedRulesRow deviceInAlert )
	{
		if ( deviceInAlert != null )
		{
			this.lblDevice.Text = deviceInAlert.DeviceName;
			this.imgStatus.ImageUrl = Controllers.Device.GetDeviceStatusImageRelativePath(false, deviceInAlert.Severity);
			this.imgbDevice.ImageUrl = Controllers.Device.GetDeviceImageRelativePath(deviceInAlert.ImageName, !(deviceInAlert.IsTicketNameNull() || deviceInAlert.TicketName == ""));
			this.imgbDevice.AlternateText = this.imgbDevice.ToolTip = deviceInAlert.DeviceType;
			GUtility.EnableImageButton(this.Page, this.imgbDevice, Controllers.Device.CanUserViewDeviceDetails(deviceInAlert.RegID, deviceInAlert.Severity, deviceInAlert.SeverityDetail, deviceInAlert.DeviceType));
			this.lblVendor.Text = deviceInAlert.VendorName;
			this.txtSaveTicketName.Text = ( deviceInAlert.IsTicketNameNull() ) ? "" : deviceInAlert.TicketName;
			this.txtSaveTicketNotes.Text = ( deviceInAlert.IsTicketNoteNull() ) ? "" : deviceInAlert.TicketNote;
			this.imgSTLCOffline.Visible = deviceInAlert.STLCOffline;
			this.lblNodeName.Text = deviceInAlert.Location;
		}
	}
	
	private void SelectAlert ()
	{
		int selectedAlertIndex = this.lvwAlerts.DataKeys.Cast<DataKey>().Select(key => (Guid)key.Value).ToList().IndexOf(GrisSessionManager.SelectedAlert);
		
		if ( this.lvwAlerts.Items.Count > 0 )
		{
			this.lvwAlerts.SelectedIndex = selectedAlertIndex != -1 ? selectedAlertIndex : 0;
			
			// Assegno la proprietà DevID relativo all' allarme selezionato
			ImageButton imgbAlertType = this.lvwAlerts.Items[this.lvwAlerts.SelectedIndex].FindControl("imgbAlertType") as ImageButton;
			if ( imgbAlertType != null && imgbAlertType.CommandArgument.Length > 0 )
			{
				this.DevID = Convert.ToInt64(imgbAlertType.CommandArgument);
			}
		}
	}

	protected int GetPageFirstRowIndex ( int pageNumber, int pageSize, int totalRowCount )
	{
		if ( pageNumber > 0 && pageSize > 0 )
		{
			// indice di prima riga a prescindere dal check di correttezza rispetto all'intervallo dei valori di indice
			int ipoteticalPageFirstRowIndex = ( pageNumber - 1 ) * pageSize;
			
			// non mi interessa sapere se cade all'interno del range, lo prendo per buono
			if ( totalRowCount < 0 ) return ipoteticalPageFirstRowIndex;
			
			// è nel range?
			int lastPageFirstRowIndex = this.GetLastPageFirstRowIndex(totalRowCount, pageSize);
			return ( ipoteticalPageFirstRowIndex < lastPageFirstRowIndex ) ? ipoteticalPageFirstRowIndex : lastPageFirstRowIndex;
		}

		return 0;
	}

	protected int GetLastPageFirstRowIndex ( int totalRowCount, int pageSize )
	{
		if ( totalRowCount > 0 && pageSize > 0 )
		{
			return pageSize * (int)Math.Floor(( totalRowCount - 1 ) / (decimal)pageSize);
		}

		return 0;
	}

	# endregion	
}

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Region.aspx.cs" Inherits="Region" Theme="Gris" %>

<%@ Register Src="Controls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="gris" %>
<%@ Register TagPrefix="gris" TagName="LayeredIcon" Src="~/Controls/LayeredIcon.ascx" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <asp:Timer ID="tmrRegion" runat="server" OnTick="tmrRegion_Tick" Enabled="true">
    </asp:Timer>
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; height: 100%">
        <tr align="left">
            <td>
                <asp:UpdatePanel ID="updNavBar" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <gris:NavigationBar ID="navBar" runat="server" OnLayoutSwitched="navBar_OnLayoutSwitched"
                            OnRefreshPageButtonClick="navBar_OnRefreshPageButtonClick" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrRegion" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="updPageLayout" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="text-align: left;">
                            <asp:UpdatePanel ID="updGridView" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvwLinee" runat="server" SkinID="SWMC4" DataSourceID="odsZones"
                                        AutoGenerateColumns="False" DataKeyNames="RegID,ZonID" Width="100%" OnSelectedIndexChanged="gvwLinee_SelectedIndexChanged"
                                        OnRowDataBound="gvwLinee_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="ZonID" HeaderText="ZonID" ReadOnly="True" Visible="False" />
                                            <asp:BoundField DataField="RegID" HeaderText="RegID" ReadOnly="True" Visible="False" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <gris:LayeredIcon ID="ImageStatus" runat="server" LayeredIconType="VistaTabellare" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep0" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Linea" SortExpression="ZonName">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbLinea" runat="server" Text='<%# Bind("ZonName") %>' CommandName="Select"></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="GridViewLeftPadding" Width="420px" />
                                                <ItemStyle CssClass="GridViewLeftPadding" HorizontalAlign="Left" Width="420px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep1" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Unknown" HeaderText="Sconosciuto" SortExpression="Unknown">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep2" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Error" HeaderText="Anomalia grave" SortExpression="Error">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep3" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Warning" HeaderText="Anomalia lieve" SortExpression="Warning">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep4" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="STLCOffline" HeaderText="STLC Offline" SortExpression="STLCOffline">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep6" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" BackColor="#4d4d4d" />
                                            </asp:TemplateField>
                                            <asp:CommandField SelectText="..." ShowSelectButton="True" ButtonType="Image" SelectImageUrl="~/IMG/interfaccia/tab_select.gif">
                                                <ItemStyle Width="30px" BackColor="#4d4d4d" />
                                            </asp:CommandField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <span style="font-size: 16pt; color: #ffffff; font-family: Arial;"><strong>Informazioni
                                                    non Disponibili</strong> </span>
                                            </div>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="tmrRegion" EventName="Tick" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="odsZones" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.ZoneDSTableAdapters.ZoneTableAdapter"
                                OnSelecting="odsZones_Selecting">
                                <SelectParameters>
                                    <asp:Parameter Name="RegId" Type="Int64" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <br />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="navBar" EventName="LayoutSwitched" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

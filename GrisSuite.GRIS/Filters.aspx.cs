﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Controllers;
using Edulife.Presentation.Web.Components;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.DeviceFiltersDSTableAdapters;
using GrisSuite.Data.Gris.DevicesDSTableAdapters;
using GrisSuite.Data.Gris.ResourcesDSTableAdapters;

public partial class Filters : NavigationPage
{
	// Lista dei Compartimenti su cui l'utente ha visibilità
	private string visibleRegions = String.Empty;

    // Device correntemente selezionata
	public long SelectedDevID
	{
		get { return Convert.ToInt64(this.ViewState["SelectedDevID"] ?? 0); }
		set { this.ViewState["SelectedDevID"] = value; }
	}

    public long SelectedResourceID
    {
        get { return Convert.ToInt64(this.ViewState["SelectedResourceID"] ?? 0); }
        set { this.ViewState["SelectedResourceID"] = value; }
    }


	public SortType DeviceNameSortOrder
	{
		get { return (SortType) (this.ViewState["DeviceNameSortOrder"] ?? SortType.Default); }
		set { this.ViewState["DeviceNameSortOrder"] = value; }
	}

	public SortType DeviceStatusSortOrder
	{
		get { return (SortType) (this.ViewState["DeviceStatusSortOrder"] ?? SortType.Default); }
		set { this.ViewState["DeviceStatusSortOrder"] = value; }
	}

	public SortType DeviceTypeSortOrder
	{
		get { return (SortType) (this.ViewState["DeviceTypeSortOrder"] ?? SortType.Default); }
		set { this.ViewState["DeviceTypeSortOrder"] = value; }
	}

	public SortType NodeNameSortOrder
	{
		get { return (SortType) (this.ViewState["NodeNameSortOrder"] ?? SortType.Default); }
		set { this.ViewState["NodeNameSortOrder"] = value; }
	}


    public int CurrentResourceId
    {
        get
        {
            FiltersPageSelections filters = GrisSessionManager.FiltersPageSelectionsExists
                                && GrisSessionManager.FiltersPageSelections != null
                              ? GrisSessionManager.FiltersPageSelections
                              : null;

            int resourceId = 0;
            if (filters != null && filters.ResourceId > 0)
            {
                resourceId = filters.ResourceId;
            }
            return resourceId;

        }
    }
	public enum SortType
	{
		Default,
		Asc,
		Desc
	}

	#region NavigationPage Properties

	public override MultiView LayoutMultiView
	{
		get { return null; }
	}

	public override View GraphView
	{
		get { return null; }
	}

	public override View GridView
	{
		get { return null; }
	}

	#endregion

	protected void Page_Load(object sender, EventArgs e)
	{
		# region Verifica Permessi

	    if (!GrisPermissions.CanAccessResource((int) DatabaseResource.DeviceFilters))
	    {
            this.GoToPage(PageDestination.Italia);
	    }

		# endregion

		this.visibleRegions = String.Join(" ", GrisPermissions.VisibleRegions().Select(regid => regid.ToString()).ToArray());

        
	}

	protected void Page_PreRender(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
		    
		    this.chkShared.Enabled = GrisPermissions.IsUserInAdminGroup();
			// La lista delle stazioni è bindata manualmente, quindi occorre chiamare il DataBind all'apertura della pagina
            this.chklStazioni.DataBind();
			this.updpnlStazioniFilters.Update();

			this.SetSeverityLevelChecksDefaults();

			this.LoadFilters();

			// Se esistenti, carichiamo i dati degli ultimi filtri usati dalla sessione
			this.RestoreSelectedDeviceFilterName();
		}

		// Il click dell'invio sulle due textbox provoca lo scatenarsi della ricerca
		this.txtRicerca.Attributes.Add("onkeypress", "return controlEnter('" + this.btnCerca.ClientID + "', event)");
		this.txtNomeFiltro.Attributes.Add("onkeypress", "return controlEnter('" + this.btnCerca.ClientID + "', event)");
        this.btnExportXls.Enabled = GrisSessionManager.FiltersPageSelectionsExists && GrisSessionManager.FiltersPageSelections != null && GrisSessionManager.FiltersPageSelections.HasFilters;
	}

	#region Eventi di cambio selezione oggetti su checkboxlist

	protected void chklCompartimenti_SelectedIndexChanged(object sender, EventArgs e)
	{
		this.txtCompartimenti.Text = string.Join(", ",
			this.chklCompartimenti.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text).ToArray());
		this.txtLinee.Text = String.Empty;
		this.txtStazioni.Text = String.Empty;
		this.txtSistemi.Text = String.Empty;
		this.txtTipiApparato.Text = String.Empty;
		this.updpnlFilters.Update();

	    this.chkAllLinee.Checked = false;
		this.chklLinee.DataBind();
		this.updpnlLineeFilters.Update();

        this.chkAllStazioni.Checked = false;
        this.chklStazioni.DataBind();
		this.updpnlStazioniFilters.Update();

		this.chklSistemi.DataBind();
		this.updpnlSistemiFilters.Update();

		this.chklTipiApparato.DataBind();
		this.updpnlTipiApparatoFilters.Update();
	}

	protected void chklLinee_SelectedIndexChanged(object sender, EventArgs e)
	{
		this.txtLinee.Text = string.Join(", ", this.chklLinee.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text).ToArray());
		this.txtStazioni.Text = String.Empty;
		this.txtSistemi.Text = String.Empty;
		this.txtTipiApparato.Text = String.Empty;
		this.updpnlFilters.Update();

        this.chkAllStazioni.Checked = false;
        this.chklStazioni.DataBind();
		this.updpnlStazioniFilters.Update();

		this.chklSistemi.DataBind();
		this.updpnlSistemiFilters.Update();

		this.chklTipiApparato.DataBind();
		this.updpnlTipiApparatoFilters.Update();
	}

	protected void chklStazioni_SelectedIndexChanged(object sender, EventArgs e)
	{
		this.txtStazioni.Text = string.Join(", ", this.chklStazioni.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text).ToArray());
		this.txtSistemi.Text = String.Empty;
		this.txtTipiApparato.Text = String.Empty;
		this.updpnlFilters.Update();

		this.chklSistemi.DataBind();
		this.updpnlSistemiFilters.Update();

		this.chklTipiApparato.DataBind();
		this.updpnlTipiApparatoFilters.Update();
	}

	protected void chklSistemi_SelectedIndexChanged(object sender, EventArgs e)
	{
		this.txtSistemi.Text = string.Join(", ", this.chklSistemi.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text).ToArray());
		this.txtTipiApparato.Text = String.Empty;
		this.updpnlFilters.Update();

		this.chklTipiApparato.DataBind();
		this.updpnlTipiApparatoFilters.Update();
	}

	protected void chklTipiApparato_SelectedIndexChanged(object sender, EventArgs e)
	{
		this.txtTipiApparato.Text = string.Join(", ",
			this.chklTipiApparato.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text).ToArray());
		this.updpnlFilters.Update();
	}

	#endregion

	#region Eventi di caricamento dati su Data Sources

	protected void SqlDataSourceCompartimenti_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@VisibleRegions"].Value = this.visibleRegions;
	}

	protected void SqlDataSourceLinee_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@VisibleRegions"].Value = this.visibleRegions;

		if (this.chklCompartimenti.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@RegIDs"].Value = string.Join(" ",
				this.chklCompartimenti.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@RegIDs"].Value = "";
		}
	}

	protected void SqlDataSourceStazioni_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@VisibleRegions"].Value = this.visibleRegions;

		if (this.chklCompartimenti.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@RegIDs"].Value = string.Join(" ",
				this.chklCompartimenti.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@RegIDs"].Value = "";
		}

		if (this.chklLinee.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@ZonIDs"].Value = string.Join(" ",
				this.chklLinee.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@ZonIDs"].Value = "";
		}
	}

	protected void SqlDataSourceSistemi_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		if (this.chklStazioni.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@NodIDs"].Value = string.Join(" ",
				this.chklStazioni.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@NodIDs"].Value = String.Empty;
		}
	}

	protected void SqlDataSourceTipiApparato_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		if (this.chklStazioni.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@NodIDs"].Value = string.Join(" ",
				this.chklStazioni.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@NodIDs"].Value = String.Empty;
		}

		if (this.chklSistemi.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@SystemIDs"].Value = string.Join(" ",
				this.chklSistemi.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@SystemIDs"].Value = string.Join(" ", this.chklSistemi.Items.Cast<ListItem>().Select(item => item.Value).ToArray());
		}
	}

	private string GetSortOrderString()
	{
		if (this.DeviceNameSortOrder == SortType.Asc)
		{
			return "Name ASC";
		}

		if (this.DeviceNameSortOrder == SortType.Desc)
		{
			return "Name DESC";
		}

		if (this.DeviceStatusSortOrder == SortType.Asc)
		{
			return "SevLevel ASC";
		}

		if (this.DeviceStatusSortOrder == SortType.Desc)
		{
			return "SevLevel DESC";
		}

		if (this.DeviceTypeSortOrder == SortType.Asc)
		{
			return "Type ASC";
		}

		if (this.DeviceTypeSortOrder == SortType.Desc)
		{
			return "Type DESC";
		}

		if (this.NodeNameSortOrder == SortType.Asc)
		{
			return "Node ASC";
		}

		if (this.NodeNameSortOrder == SortType.Desc)
		{
			return "Node DESC";
		}

		return "";
	}

	protected void SqlDataSourceFindDevices_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Arguments.SortExpression = this.GetSortOrderString();

		if (this.chklStazioni.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@NodIDs"].Value = string.Join(" ",
				this.chklStazioni.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@NodIDs"].Value = String.Empty;
		}

		if (this.chklSistemi.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@SystemIDs"].Value = string.Join(" ",
				this.chklSistemi.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@SystemIDs"].Value = string.Join(" ", this.chklSistemi.Items.Cast<ListItem>().Select(item => item.Value).ToArray());
		}

		if (this.chklTipiApparato.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			e.Command.Parameters["@DeviceTypeIDs"].Value = string.Join(" ",
				this.chklTipiApparato.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
		}
		else
		{
			e.Command.Parameters["@DeviceTypeIDs"].Value = string.Join(" ", this.chklTipiApparato.Items.Cast<ListItem>().Select(item => item.Value).ToArray());
		}

		e.Command.Parameters["@SevLevelIDs"].Value = this.GetSeverityLevelsIDs();

		e.Command.Parameters["@DeviceData"].Value = this.txtRicerca.Text;
       
	}

	protected void SqlDataSourceDeviceDetail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@DevID"].Value = this.SelectedDevID;
	}

	#endregion

	#region Utilità settaggio e lettura valori su checkbox severità

	private void SetSeverityLevelChecksDefaults()
	{
		this.chkStatom255.Checked = false;
		this.chkStatom1.Checked = false;
		this.chkStato0.Checked = true;
		this.chkStato1.Checked = true;
		this.chkStato2.Checked = true;
		this.chkStato3.Checked = true;
		this.chkStato9.Checked = true;
		this.chkStato255.Checked = true;
	}

	private string GetSeverityLevelsIDs()
	{
		List<String> severityLevelsIDs = new List<string>();

		#region Lista checkbox severità

		if (this.chkStatom255.Checked)
		{
			severityLevelsIDs.Add("-255");
		}

		if (this.chkStatom1.Checked)
		{
			severityLevelsIDs.Add("-1");
		}

		if (this.chkStato0.Checked)
		{
			severityLevelsIDs.Add("0");
		}

		if (this.chkStato1.Checked)
		{
			severityLevelsIDs.Add("1");
		}

		if (this.chkStato2.Checked)
		{
			severityLevelsIDs.Add("2");
		}

		if (this.chkStato3.Checked)
		{
			severityLevelsIDs.Add("3");
		}

		if (this.chkStato9.Checked)
		{
			severityLevelsIDs.Add("9");
		}

		if (this.chkStato255.Checked)
		{
			severityLevelsIDs.Add("255");
		}

		#endregion

		return string.Join(" ", severityLevelsIDs.ToArray());
	}

	#endregion

	#region Eventi vari controlli pagina

	protected void chklStazioni_DataBinding(object sender, EventArgs e)
	{
		// Una parte dei filtri e delle logiche di accesso alla singola stazione possono essere controllati solo via codice,
		// quindi agiamo sulla lista di righe restituite dalla query di filtro
		DataView dv = this.SqlDataSourceStazioni.Select(DataSourceSelectArguments.Empty) as DataView;

		if (dv != null)
		{
			foreach (DataRow row in dv.Table.Rows)
			{
                if (!Controllers.Node.CanUserViewDevices((long)row["RegID"], (int)row["STLCOK"], (int)row["STLCInMaintenance"], (int)row["STLCTotalCount"]))
				{
					// Eliminiamo le stazioni non accessibili
					row.Delete();
				}
			}
		}

		this.chklStazioni.DataSource = dv;
	}


	protected void rptDev_DataBinding(object sender, EventArgs e)
	{
		// Una parte dei filtri e delle logiche di accesso alla singola device possono essere controllati solo via codice,
		// quindi agiamo sulla lista di righe restituite dalla query di filtro
		DataView dv = this.SqlDataSourceFindDevices.Select(DataSourceSelectArguments.Empty) as DataView;

		if (dv != null)
		{
			foreach (DataRow row in dv.Table.Rows)
			{
				if (!Controllers.Device.CanUserViewDeviceDetails((long) row["RegID"], (int) row["SevLevel"], (int) row["SevLevelDetail"], (string) row["Type"]))
				{
					// Eliminiamo le device non accessibili
					row.Delete();
				}
			}
		}

		this.rptDev.DataSource = dv;
	}

	protected void DeviceTypeIconAndStatus_Click(object sender, EventArgs e)
	{
		if (sender is Controls_DeviceTypeIconAndStatus)
		{
			Controls_DeviceTypeIconAndStatus deviceTypeIconAndStatus = (Controls_DeviceTypeIconAndStatus) sender;

			if (deviceTypeIconAndStatus.DevID != -1)
			{
				GrisSessionManager.GrisLayoutModeReset();
				Navigation.GoToPage(PageDestination.Device.ToString(), deviceTypeIconAndStatus.DevID);
			}
		}
	}

	protected void rptDev_ItemCommand(object source, RepeaterCommandEventArgs e)
	{
		if (e.CommandName == "SDD") // Show Device Detail
		{
			long devID = long.Parse(e.CommandArgument.ToString());

			if (devID != -1)
			{
				HtmlControl trDD = e.Item.FindControl("trDD") as HtmlControl;
				Repeater rptDevDet = e.Item.FindControl("rptDevDet") as Repeater;

				if ((trDD != null) && (rptDevDet != null))
				{
					if (devID == this.SelectedDevID)
					{
						trDD.Visible = false;

						this.SelectedDevID = 0;
					}
					else
					{
						trDD.Visible = true;

						this.SelectedDevID = devID;

						this.SqlDataSourceDeviceDetail.DataBind();
						rptDevDet.DataSource = this.SqlDataSourceDeviceDetail;
						rptDevDet.DataBind();
					}
				}

				this.updpnlDevices.Update();
			}
		}
		else if (e.CommandName == "SortDeviceName")
		{
			var sortOrder = this.DeviceNameSortOrder;
			sortOrder = sortOrder == SortType.Desc ? SortType.Default : sortOrder + 1;

			this.DeviceNameSortOrder = sortOrder;
			this.DeviceStatusSortOrder = SortType.Default;
			this.DeviceTypeSortOrder = SortType.Default;
			this.NodeNameSortOrder = SortType.Default;
			this.rptDev.DataBind();
			this.updpnlDevices.Update();
		}
		else if (e.CommandName == "SortDeviceStatus")
		{
			var sortOrder = this.DeviceStatusSortOrder;
			sortOrder = sortOrder == SortType.Desc ? SortType.Default : sortOrder + 1;

			this.DeviceNameSortOrder = SortType.Default;
			this.DeviceStatusSortOrder = sortOrder;
			this.DeviceTypeSortOrder = SortType.Default;
			this.NodeNameSortOrder = SortType.Default;
			this.rptDev.DataBind();
			this.updpnlDevices.Update();
		}
		else if (e.CommandName == "SortDeviceType")
		{
			var sortOrder = this.DeviceTypeSortOrder;
			sortOrder = sortOrder == SortType.Desc ? SortType.Default : sortOrder + 1;

			this.DeviceNameSortOrder = SortType.Default;
			this.DeviceStatusSortOrder = SortType.Default;
			this.DeviceTypeSortOrder = sortOrder;
			this.NodeNameSortOrder = SortType.Default;
			this.rptDev.DataBind();
			this.updpnlDevices.Update();
		}
		else if (e.CommandName == "SortNodeName")
		{
			var sortOrder = this.NodeNameSortOrder;
			sortOrder = sortOrder == SortType.Desc ? SortType.Default : sortOrder + 1;

			this.DeviceNameSortOrder = SortType.Default;
			this.DeviceStatusSortOrder = SortType.Default;
			this.DeviceTypeSortOrder = SortType.Default;
			this.NodeNameSortOrder = sortOrder;
			this.rptDev.DataBind();
			this.updpnlDevices.Update();
		}
	}

	protected void rptDevDet_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
        /*
		if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
		{
			// Bottoni per visualizzare la history dei valori degli stream fields
			Button imgbDevDetailHistory = e.Item.FindControl("imgbDevDetailHistory") as Button;
			ModalPopupExtender mdlpopexDevDetailHistory = e.Item.FindControl("mdlpopexDevDetailHistory") as ModalPopupExtender
            DynamicPopulateExtender dpSFH = e.Item.FindControl("dpSFH") as DynamicPopulateExtender;
			DataRowView dataRowView = e.Item.DataItem as DataRowView;

			if ((imgbDevDetailHistory != null) && (mdlpopexDevDetailHistory != null) && (dpSFH != null) && (dataRowView != null))
			{
				string behaviorID = String.Format("dpSFHb{0}", ((IDataItemContainer) e.Item).DataItemIndex);
				imgbDevDetailHistory.OnClientClick =
					String.Format("updateBhvKey('{0}','{1},{2},{3}');showConfirm(null, 'mdlpopexDevDetailHistory{4}');return false;", behaviorID,
						dataRowView["DevID"], dataRowView["StrID"], dataRowView["FieldID"], ((IDataItemContainer) e.Item).DataItemIndex);
				mdlpopexDevDetailHistory.BehaviorID = String.Format("mdlpopexDevDetailHistory{0}", ((IDataItemContainer) e.Item).DataItemIndex);
				dpSFH.BehaviorID = behaviorID;
			}
		}

		if (e.Item.ItemType == ListItemType.Header)
		{
			// Bottone per visualizzare la history dei sevlevel per device
			Button imgbDevHistory = e.Item.FindControl("imgbDevHistory") as Button;
			ModalPopupExtender mdlpopexDevHistory = e.Item.FindControl("mdlpopexDevHistory") as ModalPopupExtender;
			DynamicPopulateExtender dpDH = e.Item.FindControl("dpDH") as DynamicPopulateExtender;
			Label lblDeviceData = e.Item.FindControl("lblDeviceData") as Label;

			if ((imgbDevHistory != null) && (mdlpopexDevHistory != null) && (dpDH != null) && (lblDeviceData != null) && (this.SelectedDevID > 0))
			{
				devicesTableAdapter tad = new devicesTableAdapter();
				DevicesDS.devicesDataTable dtd = tad.GetDataById(this.SelectedDevID, null);

				if ((dtd != null) && (dtd.Rows.Count > 0))
				{
					lblDeviceData.Text = String.Format("{0} - {1} ({2}) {3}", dtd[0].Type, dtd[0].Name, dtd[0].VendorName, dtd[0].Address);
				}

				string behaviorID = String.Format("dpDHb{0}", this.SelectedDevID);
				imgbDevHistory.OnClientClick = String.Format("updateBhvKey('{0}','{1}');showConfirm(null, 'mdlpopexDevHistory{2}');return false;", behaviorID,
					this.SelectedDevID, this.SelectedDevID);
				mdlpopexDevHistory.BehaviorID = String.Format("mdlpopexDevHistory{0}", this.SelectedDevID);
				dpDH.BehaviorID = behaviorID;
			}
		}
         */
	}

	protected void rptDev_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Header)
		{
			var imgbSortDeviceName = e.Item.FindControl("imgbSortDeviceName") as ImageButton;
			if (imgbSortDeviceName != null)
			{
				imgbSortDeviceName.Visible = this.DeviceNameSortOrder != SortType.Default;
				imgbSortDeviceName.ImageUrl = this.DeviceNameSortOrder == SortType.Asc ? "~/IMG/Interfaccia/Arrow2 Up.png" : "~/IMG/Interfaccia/Arrow2 Down.png";
			}

			var imgbSortDeviceStatus = e.Item.FindControl("imgbSortDeviceStatus") as ImageButton;
			if (imgbSortDeviceStatus != null)
			{
				imgbSortDeviceStatus.Visible = this.DeviceStatusSortOrder != SortType.Default;
				imgbSortDeviceStatus.ImageUrl = this.DeviceStatusSortOrder == SortType.Asc
					? "~/IMG/Interfaccia/Arrow2 Up.png"
					: "~/IMG/Interfaccia/Arrow2 Down.png";
			}

			var imgbSortDeviceType = e.Item.FindControl("imgbSortDeviceType") as ImageButton;
			if (imgbSortDeviceType != null)
			{
				imgbSortDeviceType.Visible = this.DeviceTypeSortOrder != SortType.Default;
				imgbSortDeviceType.ImageUrl = this.DeviceTypeSortOrder == SortType.Asc ? "~/IMG/Interfaccia/Arrow2 Up.png" : "~/IMG/Interfaccia/Arrow2 Down.png";
			}

			var imgbSortNodeName = e.Item.FindControl("imgbSortNodeName") as ImageButton;
			if (imgbSortNodeName != null)
			{
				imgbSortNodeName.Visible = this.NodeNameSortOrder != SortType.Default;
				imgbSortNodeName.ImageUrl = this.NodeNameSortOrder == SortType.Asc ? "~/IMG/Interfaccia/Arrow2 Up.png" : "~/IMG/Interfaccia/Arrow2 Down.png";
			}
		}
	}

	#endregion

	#region Utilità persistenza e lettura dati filtri

	private void SaveFilterSession(Boolean saveToDb)
	{
		string userInfo = GUtility.GetUserInfos(false);
		var filters = new FiltersPageSelections();

		string regionIDs;
		string regionNames;
		this.SetFilterIdsAndNames(this.chklCompartimenti, out regionIDs, out regionNames);
		filters.RegionIDs = regionIDs;
		filters.RegionNames = regionNames;

		string zoneIDs;
		string zoneNames;
		this.SetFilterIdsAndNames(this.chklLinee, out zoneIDs, out zoneNames);
		filters.ZoneIDs = zoneIDs;
		filters.ZoneNames = zoneNames;

		string nodeIDs;
		string nodeNames;
		this.SetFilterIdsAndNames(this.chklStazioni, out nodeIDs, out nodeNames);
		filters.NodeIDs = nodeIDs;
		filters.NodeNames = nodeNames;

		string systemIDs;
		string systemNames;
		this.SetFilterIdsAndNames(this.chklSistemi, out systemIDs, out systemNames);
		filters.SystemIDs = systemIDs;
		filters.SystemNames = systemNames;

		string deviceTypeIDs;
		string deviceTypeNames;
		this.SetFilterIdsAndNames(this.chklTipiApparato, out deviceTypeIDs, out deviceTypeNames);
		filters.DeviceTypeIDs = deviceTypeIDs;
		filters.DeviceTypeNames = deviceTypeNames;

		filters.SevLevelIDs = this.GetSeverityLevelsIDs();

		filters.DeviceData = this.txtRicerca.Text;

	    filters.IsShared = chkShared.Checked;



		filters.DeviceFilterName = this.txtNomeFiltro.Text.Trim();

		if (String.IsNullOrEmpty(filters.DeviceFilterName))
		{
			if (GrisSessionManager.SelectedDeviceFilterNameExists)
			{
				GrisSessionManager.Session.Remove(GrisSessionManager.SelectedDeviceFilterIdKey);
			}
		}
        else if (saveToDb)
		{
			DeviceFiltersListByUserTableAdapter ta = new DeviceFiltersListByUserTableAdapter();
			DeviceFiltersDS.DeviceFiltersListByUserDataTable dt = new DeviceFiltersDS.DeviceFiltersListByUserDataTable();
			ta.InsDeviceFilters(dt, userInfo, filters.DeviceFilterName, regionIDs, regionNames, zoneIDs, zoneNames, nodeIDs, nodeNames, systemIDs, systemNames,
				deviceTypeIDs, deviceTypeNames, filters.SevLevelIDs, filters.DeviceData, filters.IsShared);

			if (dt.Rows.Count > 0)
			{
			    filters.ResourceId = dt[0].ResourceID;
				this.SaveSelectedDeviceFilter(dt[0].DeviceFilterId, dt[0].DeviceFilterName);
			}
		}

		filters.HasFilters = true;
		GrisSessionManager.FiltersPageSelections = filters;

		this.LoadFilters();
	}

	private void SetFilterIdsAndNames(CheckBoxList chk, out string ids, out string names)
	{
		ids = "";
		names = "";
		if (chk != null && chk.Items.Cast<ListItem>().Any(item => item.Selected))
		{
			ids = string.Join(" ", chk.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToArray());
			names = string.Join(", ", chk.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text).ToArray());
		}
	}

	private void DeleteFilter(long filterId)
	{
		if (filterId > 0 && (!chkShared.Checked || GrisPermissions.IsUserInAdminGroup()))
		{
			DeviceFiltersListByUserTableAdapter ta = new DeviceFiltersListByUserTableAdapter();
			ta.DelDeviceFilters(filterId);

			this.LoadFilters();
		}
	}

	private void LoadFilters()
	{
		string userInfo = GUtility.GetUserInfos(false);

		DeviceFiltersListByUserTableAdapter ta = new DeviceFiltersListByUserTableAdapter();
		DeviceFiltersDS.DeviceFiltersListByUserDataTable dt = ta.GetData(userInfo);

        // escludi i filtri su quali l'untente non ha il permesso
        DeviceFiltersDS.DeviceFiltersListByUserDataTable dtWithPermission = new DeviceFiltersDS.DeviceFiltersListByUserDataTable();

	    DeviceFiltersDS.DeviceFiltersListByUserRow emptyRow = dtWithPermission.NewDeviceFiltersListByUserRow();
	    emptyRow.ResourceID = 0;
	    emptyRow.DeviceFilterId = -1;
	    emptyRow.DeviceFilterName = "Nessun filtro";
        dtWithPermission.AddDeviceFiltersListByUserRow(emptyRow);
	    foreach (DeviceFiltersDS.DeviceFiltersListByUserRow rowfilter in dt.Rows)
	    {
	        if (rowfilter.IsResourceIDNull() 
                || GrisPermissions.CanAccessResource(rowfilter.ResourceID)
                || GrisPermissions.IsUserInAdminGroup())
	        {
	            dtWithPermission.ImportRow(rowfilter);
	        }   
	    }

        this.dlstFiltri.DataSource = dtWithPermission;
		this.dlstFiltri.DataBind();
		this.updpnlFiltri.Update();
	}

	private void RestoreFilter(FiltersPageSelections filters)
	{

	    bool canEdit = !filters.IsShared || GrisPermissions.IsUserInAdminGroup();

        popexLinee.Enabled = canEdit;
        popexCompartimenti.Enabled = canEdit;
        popexStazioni.Enabled = canEdit;
        popexSistemi.Enabled = canEdit;
        popexTipiApparato.Enabled = canEdit;
	    chkStato0.Enabled = canEdit;
        chkStato1.Enabled = canEdit;
        chkStato2.Enabled = canEdit;
        chkStato3.Enabled = canEdit;
        chkStato9.Enabled = canEdit;
        chkStato255.Enabled = canEdit;
        chkStatom1.Enabled = canEdit;
        chkStatom255.Enabled = canEdit;
            
		if (filters.HasFilters)
		{
			this.txtCompartimenti.Text = filters.RegionNames;
			this.txtLinee.Text = filters.ZoneNames;
			this.txtStazioni.Text = filters.NodeNames;
			this.txtSistemi.Text = filters.SystemNames;
			this.txtTipiApparato.Text = filters.DeviceTypeNames;


			this.chklCompartimenti.DataBind();
			this.RestoreCheckboxListStatusFromIDList(this.chklCompartimenti, filters.RegionIDs.Split(' '));
			this.updpnlCompartimentiFilters.Update();

            this.chkAllLinee.Checked = false;
            this.chklLinee.DataBind();
			this.RestoreCheckboxListStatusFromIDList(this.chklLinee, filters.ZoneIDs.Split(' '));
			this.updpnlLineeFilters.Update();

		    this.chkAllStazioni.Checked = false;
			this.chklStazioni.DataBind();
			this.RestoreCheckboxListStatusFromIDList(this.chklStazioni, filters.NodeIDs.Split(' '));
			this.updpnlStazioniFilters.Update();

			this.chklSistemi.DataBind();
			this.RestoreCheckboxListStatusFromIDList(this.chklSistemi, filters.SystemIDs.Split(' '));
			this.updpnlSistemiFilters.Update();

			this.chklTipiApparato.DataBind();
			this.RestoreCheckboxListStatusFromIDList(this.chklTipiApparato, filters.DeviceTypeIDs.Split(' '));
			this.updpnlTipiApparatoFilters.Update();

			this.RestoreSeverityLevelsIDsFromIDList(filters.SevLevelIDs.Split(' '));

			this.txtRicerca.Text = filters.DeviceData;
            this.chkShared.Checked = filters.IsShared;
		}
		else
		{
			this.txtCompartimenti.Text = String.Empty;
			this.txtLinee.Text = String.Empty;
			this.txtStazioni.Text = String.Empty;
			this.txtSistemi.Text = String.Empty;
			this.txtTipiApparato.Text = String.Empty;


			this.chklCompartimenti.DataBind();
			this.updpnlCompartimentiFilters.Update();

		    this.chkAllLinee.Checked = false;
			this.chklLinee.DataBind();
			this.updpnlLineeFilters.Update();

		    this.chkAllStazioni.Checked = false;
			this.chklStazioni.DataBind();
			this.updpnlStazioniFilters.Update();

			this.chklSistemi.DataBind();
			this.updpnlSistemiFilters.Update();

			this.chklTipiApparato.DataBind();
			this.updpnlTipiApparatoFilters.Update();

			this.SetSeverityLevelChecksDefaults();

			this.txtRicerca.Text = String.Empty;
            this.chkShared.Checked = false;
		}
        this.btnCercaSalva.Enabled = !chkShared.Checked || GrisPermissions.IsUserInAdminGroup();
    }

	private void RestoreCheckboxListStatusFromIDList(ListControl chkl, string[] values)
	{
		foreach (string id in values)
		{
			ListItem item = chkl.Items.FindByValue(id);
			if (item != null)
			{
				item.Selected = true;
			}
		}
	}

	private void RestoreSeverityLevelsIDsFromIDList(string[] values)
	{
		this.chkStatom255.Checked = values.Contains("-255");
		this.chkStatom1.Checked = values.Contains("-1");
		this.chkStato0.Checked = values.Contains("0");
		this.chkStato1.Checked = values.Contains("1");
		this.chkStato2.Checked = values.Contains("2");
		this.chkStato3.Checked = values.Contains("3");
		this.chkStato9.Checked = values.Contains("9");
		this.chkStato255.Checked = values.Contains("255");
	}

	protected void dlstFiltri_ItemCommand(object source, DataListCommandEventArgs e)
	{
		string[] commandArguments = (e.CommandArgument.ToString().Split('‡'));

		long deviceFilterId = 0;
		string deviceFilterName = String.Empty;

		if (commandArguments.Length >= 2)
		{
			deviceFilterId = long.Parse(commandArguments[0]);
			deviceFilterName = commandArguments[1];
		}

		switch (e.CommandName)
		{
			case "FS":
				this.FilterSelect(deviceFilterId, deviceFilterName);
				break;
			case "FD":
				this.DeleteFilter(deviceFilterId);
				this.FilterSelect(0, String.Empty);
				break;
		}

		this.pnlDevices.Visible = false;
		this.pnlSearchMessage.Visible = false;

		this.rptDev.DataSource = null;
		this.rptDev.DataBind();
		this.updpnlDevices.Update();
	}

	protected void dlstFiltri_ItemDataBound(object sender, DataListItemEventArgs e)
	{
		Button imgbDeleteFilter = e.Item.FindControl("imgbDeleteFilter") as Button;
		ModalPopupExtender mdlpopexDelete = e.Item.FindControl("mdlpopexDelete") as ModalPopupExtender;

		if ((imgbDeleteFilter != null) && (mdlpopexDelete != null))
		{
		    DeviceFiltersDS.DeviceFiltersListByUserRow row =
		        (DeviceFiltersDS.DeviceFiltersListByUserRow) ((DataRowView) e.Item.DataItem).Row;
            imgbDeleteFilter.Visible = (row.IsResourceIDNull() || GrisPermissions.IsUserInAdminGroup()) && row.DeviceFilterId != -1;
			imgbDeleteFilter.OnClientClick = "showConfirm(this, 'mdlpopexDelete" + ((IDataItemContainer) e.Item).DataItemIndex + "');return false;";
			mdlpopexDelete.BehaviorID = "mdlpopexDelete" + ((IDataItemContainer) e.Item).DataItemIndex;
		}
	}

	private void FilterSelect(long deviceFilterId, string deviceFilterName)
	{
		this.SaveSelectedDeviceFilter(deviceFilterId, deviceFilterName);

		FiltersPageSelections filters;

		if (deviceFilterId > 0)
		{
			filters = this.QueryForFilters(deviceFilterId);
			this.txtNomeFiltro.Text = deviceFilterName;
		}
		else
		{
            filters = (GrisSessionManager.FiltersPageSelectionsExists && GrisSessionManager.FiltersPageSelections != null && deviceFilterId != -1)
				? GrisSessionManager.FiltersPageSelections
				: new FiltersPageSelections();

			this.txtNomeFiltro.Text = String.Empty;
		}
        GrisSessionManager.FiltersPageSelections = filters;
		this.RestoreFilter(filters);

		this.updpnlManageFilters.Update();

		// Nel caso arrivassimo da un postback non asincrono, quindi da altra pagina di Gris, con ripristino dell'ultimo filtro in corso,
		// la popup non sarà aperta e quindi, non c'è possibilità di chiuderla
		ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
		if (scriptManager != null && scriptManager.IsInAsyncPostBack)
		{
			this.popexFiltriSelector.Commit(null);
		}

		this.updpnlFilters.Update();
	}

	private FiltersPageSelections QueryForFilters(long deviceFilterId)
	{
	    if (deviceFilterId <= 0) return null;
	    
        var ta = new DeviceFilterDataByIDTableAdapter();
	    var dt = ta.GetData(deviceFilterId);

	    if (dt == null || dt.Rows.Count <= 0) return null;
	    
        return new FiltersPageSelections
	    {
	        RegionIDs = dt[0].RegIDs,
	        RegionNames = dt[0].RegNames,
	        ZoneIDs = dt[0].ZonIDs,
	        ZoneNames = dt[0].ZonNames,
	        NodeIDs = dt[0].NodIDs,
	        NodeNames = dt[0].NodNames,
	        SystemIDs = dt[0].SystemIDs,
	        SystemNames = dt[0].SystemNames,
	        DeviceTypeIDs = dt[0].DeviceTypeIDs,
	        DeviceTypeNames = dt[0].DeviceTypeNames,
	        SevLevelIDs = dt[0].SevLevelIDs,
	        DeviceFilterName = dt[0].DeviceFilterName,
	        DeviceData = dt[0].DeviceData,
	        IsShared = dt[0].IsShared,
	        ResourceId = dt[0].ResourceID,
	        HasFilters = true
	    };
	}

	#endregion

	#region Utilità persistenza e ripristino ultimo filtro usato da sessione

	private void SaveSelectedDeviceFilter(long deviceFilterId, string deviceFilterName)
	{
		GrisSessionManager.SelectedDeviceFilterId = deviceFilterId;
		GrisSessionManager.SelectedDeviceFilterName = deviceFilterName;
	}

	private void RestoreSelectedDeviceFilterName()
	{
		if ((GrisSessionManager.SelectedDeviceFilterNameExists) && (!String.IsNullOrEmpty(GrisSessionManager.SelectedDeviceFilterName)) &&
		    (GrisSessionManager.SelectedDeviceFilterIdExists) && (GrisSessionManager.SelectedDeviceFilterId > 0))
		{
			this.FilterSelect(GrisSessionManager.SelectedDeviceFilterId, GrisSessionManager.SelectedDeviceFilterName);
		}
		else
		{
			this.FilterSelect(0, String.Empty);
		}
	}

	#endregion

	#region Web service che restituiscono l'HTML per le popup con i dati di storico per device e stream fields

	[WebMethod()]
	[ScriptMethod]
	public static string GetStreamFieldHistoryHtml(string contextKey)
	{
		string[] data = contextKey.Split(',');

		long devID = 0;
		int strID = -1;
		int fieldID = -1;
		if (data.Length >= 3)
		{
			long.TryParse(data[0], out devID);
			int.TryParse(data[1], out strID);
			int.TryParse(data[2], out fieldID);
		}

		StringBuilder streamFieldHistoryHtml = new StringBuilder();
		streamFieldHistoryHtml.AppendFormat("<div style=\"text-align:right\">");
		streamFieldHistoryHtml.AppendFormat("<img class=\"filtersSFHc\" onclick=\"cancelClick();\" src=\"IMG/interfaccia/Cancel.png\" alt=\"Chiudi\" />");
		streamFieldHistoryHtml.AppendFormat("</div>");

		streamFieldHistoryHtml.AppendFormat("<div style=\"padding-top:5px;\">");

		if ((devID > 0) && (strID >= 0) && (fieldID >= 0))
		{
			// Storia dalla mezzanotte degli ultimi sette giorni
			// In caso di modifica del numero di giorni, allineare anche il messaggio sotto, nel caso non siano presenti dati
			DateTime startDate = DateTime.Now.AddDays(-7);
			DateTime historyDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);

			HistoryStreamFieldsValueTableAdapter ta = new HistoryStreamFieldsValueTableAdapter();
			DeviceFiltersDS.HistoryStreamFieldsValueDataTable dt = ta.GetData(devID, strID, fieldID, historyDate, historyDate);

			if ((dt != null) && (dt.Rows.Count > 0))
			{
				streamFieldHistoryHtml.AppendFormat("<div class=\"filtersSFHd1\">");
				streamFieldHistoryHtml.AppendFormat("<table class=\"filtersSFHt1\">");
				streamFieldHistoryHtml.AppendFormat("<tr><td colspan=\"4\" class=\"filtersSFHt1h\">{0} - {1}</td></tr>", dt[0].StreamName, dt[0].StreamFieldName);
				streamFieldHistoryHtml.AppendFormat(
					"<tr><td class=\"filtersSFHt1c1\">{0}</td><td class=\"filtersSFHt1c2\">{1}</td><td class=\"filtersSFHt1c3\">{2}</td><td class=\"filtersSFHt1c4\">{3}</td></tr>",
					"Inizio Stato", "Stato", "Valore", "Durata");
				streamFieldHistoryHtml.AppendFormat("</table>");
				streamFieldHistoryHtml.AppendFormat("</div>");
				streamFieldHistoryHtml.AppendFormat("<div class=\"filtersSFHd2\">");
				streamFieldHistoryHtml.AppendFormat("<table class=\"filtersSFHt2\">");

				foreach (DeviceFiltersDS.HistoryStreamFieldsValueRow row in dt.Rows)
				{
					streamFieldHistoryHtml.AppendFormat(
						"<tr><td class=\"filtersSFHt2c1\">{0:dd/MM/yyyy HH:mm}</td><td class=\"filtersSFHt2c2\">{1}</td><td class=\"filtersSFHt2c3\">{2}</td><td class=\"filtersSFHt2c4\">{3}</td></tr>",
						row.Created, row.SeverityDescription, HttpUtility.HtmlEncode(row.Value), HttpUtility.HtmlEncode(row.DurationDescription));
				}

				streamFieldHistoryHtml.AppendFormat("</table>");
				streamFieldHistoryHtml.AppendFormat("</div>");
			}
			else
			{
				streamFieldHistoryHtml.AppendFormat("<div class=\"filtersSFHd1\">");
				streamFieldHistoryHtml.AppendFormat("<table class=\"filtersSFHt1nd\">");
				streamFieldHistoryHtml.AppendFormat(
					"<tr><td class=\"filtersSFHt1h\" style=\"text-align:center;color:#fff;font-size:12px;height:450px;\">{0}</td></tr>",
					"Non sono presenti dati di storico relativi all'ultima settimana");
				streamFieldHistoryHtml.AppendFormat("</table>");
				streamFieldHistoryHtml.AppendFormat("</div>");
			}
		}
		else
		{
			streamFieldHistoryHtml.AppendFormat("&nbsp;");
		}

		streamFieldHistoryHtml.AppendFormat("</div>");

		return streamFieldHistoryHtml.ToString();
	}

	[WebMethod()]
	[ScriptMethod]
	public static string GetDeviceHistoryHtml(string contextKey)
	{
		long devID = 0;
		if (!String.IsNullOrEmpty(contextKey))
		{
			long.TryParse(contextKey, out devID);
		}

		StringBuilder streamFieldHistoryHtml = new StringBuilder();
		streamFieldHistoryHtml.AppendFormat("<div style=\"text-align:right\">");
		streamFieldHistoryHtml.AppendFormat("<img class=\"filtersSFHc\" onclick=\"cancelClick();\" src=\"IMG/interfaccia/Cancel.png\" alt=\"Chiudi\" />");
		streamFieldHistoryHtml.AppendFormat("</div>");

		streamFieldHistoryHtml.AppendFormat("<div style=\"padding-top:5px;\">");

		if (devID > 0)
		{
			// Storia dalla mezzanotte degli ultimi sette giorni
			// In caso di modifica del numero di giorni, allineare anche il messaggio sotto, nel caso non siano presenti dati
			DateTime startDate = DateTime.Now.AddDays(-7);
			DateTime historyDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);

			HistoryDeviceSevLevelTableAdapter ta = new HistoryDeviceSevLevelTableAdapter();
			DeviceFiltersDS.HistoryDeviceSevLevelDataTable dt = ta.GetData(devID, historyDate, historyDate);

			if ((dt != null) && (dt.Rows.Count > 0))
			{
				streamFieldHistoryHtml.AppendFormat("<div class=\"filtersSFHd1\">");
				streamFieldHistoryHtml.AppendFormat("<table class=\"filtersSFHt1\">");
				streamFieldHistoryHtml.AppendFormat("<tr><td colspan=\"3\" class=\"filtersSFHt1h\">{0} - {1}</td></tr>", dt[0].Type, dt[0].DeviceName);
				streamFieldHistoryHtml.AppendFormat(
					"<tr><td class=\"filtersDHt1c1\">{0}</td><td class=\"filtersDHt1c2\">{1}</td><td class=\"filtersDHt1c3\">{2}</td></tr>", "Inizio Stato", "Stato",
					"Durata");
				streamFieldHistoryHtml.AppendFormat("</table>");
				streamFieldHistoryHtml.AppendFormat("</div>");
				streamFieldHistoryHtml.AppendFormat("<div class=\"filtersSFHd2\">");
				streamFieldHistoryHtml.AppendFormat("<table class=\"filtersSFHt2\">");

				foreach (DeviceFiltersDS.HistoryDeviceSevLevelRow row in dt.Rows)
				{
					streamFieldHistoryHtml.AppendFormat(
						"<tr><td class=\"filtersDHt2c1\">{0:dd/MM/yyyy HH:mm}</td><td class=\"filtersDHt2c2\">{1}</td><td class=\"filtersDHt2c3\">{2}</td></tr>",
						row.Created, row.SeverityDescription, HttpUtility.HtmlEncode(row.DurationDescription));
				}

				streamFieldHistoryHtml.AppendFormat("</table>");
				streamFieldHistoryHtml.AppendFormat("</div>");
			}
			else
			{
				streamFieldHistoryHtml.AppendFormat("<div class=\"filtersSFHd1\">");
				streamFieldHistoryHtml.AppendFormat("<table class=\"filtersSFHt1nd\">");
				streamFieldHistoryHtml.AppendFormat(
					"<tr><td class=\"filtersSFHt1h\" style=\"text-align:center;color:#fff;font-size:12px;height:450px;\">{0}</td></tr>",
					"Non sono presenti dati di storico relativi all'ultima settimana");
				streamFieldHistoryHtml.AppendFormat("</table>");
				streamFieldHistoryHtml.AppendFormat("</div>");
			}
		}
		else
		{
			streamFieldHistoryHtml.AppendFormat("&nbsp;");
		}

		streamFieldHistoryHtml.AppendFormat("</div>");

		return streamFieldHistoryHtml.ToString();
	}

	#endregion

    protected void btnCercaSalva_OnClick(object sender, EventArgs e)
    {
        //this.txtNomeFiltro.Text = txtNomeFiltroSave.Text;
        Cerca(true);
    }

    protected void btnCerca_OnClick(object sender, EventArgs e)
    {
        Cerca(false);
    }

    private void Cerca(bool save)
    {
        this.pnlDevices.Visible = false;
        this.pnlSearchMessage.Visible = false;

        if (!this.chklStazioni.Items.Cast<ListItem>().Any(item => item.Selected))
        {
            this.pnlSearchMessage.Visible = true;
            this.lblSearchMessage.Text = "Selezionare almeno una stazione";
        }
        else
        {
            this.pnlDevices.Visible = true;

            // Si salva il filtro corrente su db solamente se 
            //   non è un filtro condiviso
            //   parte effettivamente la ricerca
            //   è un filtro condiviso ma sono amministratore
            if (save)
            {
                if (chkShared.Checked && !GrisPermissions.IsUserInAdminGroup())
                {
                    this.pnlSearchMessage.Visible = true;
                    this.lblSearchMessage.Text = "Non si dispone dei permessi per salvare il filtro";
                }
                else if (String.IsNullOrEmpty(this.txtNomeFiltro.Text.Trim()))
                {
                    this.pnlSearchMessage.Visible = true;
                    this.lblSearchMessage.Text = "Filtro non salvato, per salvarlo scrivere un nome nel campo filtro.";
                }
                else
                {
                    this.SaveFilterSession(true);    
                }
            }
            else
            {
                // salvo i dati del filtro in sessione per poter andare su altre pagine e tornare con lo 
                // stesso filtro impostato
                this.SaveFilterSession(false);    
            }

        }

        this.rptDev.DataBind();
        this.updpnlFilters.Update();
        this.updpnlDevices.Update();
        this.updpnlManageFilters.Update();

    }
    /*
    protected void chkAllCompartimenti_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in this.chklCompartimenti.Items)
        {
            item.Selected = chkAllCompartimenti.Checked;
        }
        chklCompartimenti_SelectedIndexChanged(null, null);
    }
    */

    protected void chkAllLinee_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in this.chklLinee.Items)
        {
            item.Selected = chkAllLinee.Checked;
        }
        chklLinee_SelectedIndexChanged(null, null);
    }

    protected void chkAllStazioni_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in this.chklStazioni.Items)
        {
            item.Selected = chkAllStazioni.Checked;
        }
        chklStazioni_SelectedIndexChanged(null, null);
    }
    /*
    protected void chkAllSistemi_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in this.chklSistemi.Items)
        {
            item.Selected = chkAllSistemi.Checked;
        }
        chklSistemi_SelectedIndexChanged(null, null);
    }

    protected void chkAllTipiApparato_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (ListItem item in this.chklTipiApparato.Items)
        {
            item.Selected = chkAllTipiApparato.Checked;
        }
        chklTipiApparato_SelectedIndexChanged(null, null);
    }
    */


    protected void btnExportXls_OnClick(object sender, EventArgs e)
    {
        FiltersPageSelections filters = GrisSessionManager.FiltersPageSelectionsExists
                && GrisSessionManager.FiltersPageSelections != null
            ? GrisSessionManager.FiltersPageSelections
            : null;
        if (filters != null && filters.HasFilters)
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW",
                "window.open( 'FiltersExport.aspx', null );",
                true);
        }

    }
}
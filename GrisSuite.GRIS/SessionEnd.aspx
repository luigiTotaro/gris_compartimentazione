<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SessionEnd.aspx.cs" Inherits="SessionEnd" Title="Untitled Page" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" Runat="Server">
	<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
		<tr align="left">
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
					<tr style="background-image: url(IMG/interfaccia/nav_bg.gif); background-repeat: repeat-x ">
						<td style="height:30px">
							<table border="0" cellpadding="0" cellspacing="0" style="height:100%;">
								<tr align="left">
									<td><asp:LinkButton ID="LinkBtnItalia" runat="server" CssClass="menuLink" Text="Italia" PostBackUrl="~/Italia.aspx"></asp:LinkButton></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" style="background-image: url(IMG/interfaccia/nav_shadow_bg.gif); height:21px; background-repeat:repeat-x;"></td></tr>        
				</table>     
			</td>
		</tr>
	</table>   
	<table>
		<tr><td style="height:100px;">&nbsp;</td></tr>
		<tr><td><asp:label ID="lblError" runat="server" EnableViewState="false" ForeColor="White" Font-Bold="true" Font-Size="Large"></asp:label></td></tr>
		<tr><td style="height:100px;">&nbsp;</td></tr>
	</table>
</asp:Content>


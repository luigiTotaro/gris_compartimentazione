﻿<%@ Page Language="C#"  CodeFile="FiltersExport.aspx.cs" Inherits="FiltersExport" %>

<asp:SqlDataSource ID="SqlDataSourceFindDevices" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
	OnSelecting="SqlDataSourceFindDevices_Selecting" SelectCommand="gris_GetFindDevices"
	SelectCommandType="StoredProcedure">
	<SelectParameters>
		<asp:Parameter Name="NodIDs" Type="String" />
		<asp:Parameter Name="SystemIDs" Type="String" />
		<asp:Parameter Name="DeviceTypeIDs" Type="String" />
		<asp:Parameter Name="SevLevelIDs" Type="String" />
		<asp:Parameter Name="DeviceData" Type="String" />
	</SelectParameters>
</asp:SqlDataSource>
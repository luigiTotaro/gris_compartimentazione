﻿<%@ WebHandler Language="C#" Class="agentService" %>

using System;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using System.Text;

public class agentService : IHttpHandler {

    private static readonly int iterations = 1000;    
        
    public void ProcessRequest (HttpContext context) {


        HttpResponse r = context.Response;

        string regId = context.Request.QueryString["regId"];
        
        string json="";
        
        if (regId == null)
        {
            json = "{\"Error\":\"ID not defined\"}";
        }
        else
        {
            //string regId = "281474976645132";
            string[] sTmp = GUtility.GetRegionExtData(regId);

            //devo criptare la password
            string password = sTmp[2];
            if (password == null) password = "---";
            ClsCrypto cry = new ClsCrypto("chiavediprova");
            string passwordCriptata = cry.Encrypt(password);
            //string passwordCriptata = EncryptText(password, "chiavediprova");

            string requestIp = "";
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    requestIp = addresses[0];
                }
            }
            else requestIp = context.Request.ServerVariables["REMOTE_ADDR"];


            json = "{\"brokerIp\":\"" + sTmp[0] + "\",\"username\":\"" + sTmp[1] + "\",\"password\":\"" + passwordCriptata + "\",\"port\":\"" + sTmp[3] + "\",\"requestIp\":\"" + requestIp + "\"}";
        }
               
        
        r.Clear();
        r.ContentType = "application/json; charset=utf-8";
        r.Write(json);
        r.End();        
        

    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}


using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.Gris;

public partial class ConfigurationValidation : GrisPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if ( !GrisPermissions.IsUserInConfigValidationGroup() && !GrisPermissions.IsUserInAdminGroup() )
		{
			this.Response.Redirect("~/Italia.aspx");
		}

		if ( !this.IsPostBack )
		{
            this.imgbUpdate.Attributes["OnMouseOver"] = @"this.src = this.src.replace(/btnRefresh_\w*\.gif/, 'btnRefresh_over.gif');";
            this.imgbUpdate.Attributes["OnMouseOut"] = "this.src = this.src.replace('_over', '_default');";

			// genero le chiavi pubblica e privata per la validazione se non sono mai state generate
			SiteDS.siteRow site = ThisSite.GetSiteData();

			if ( site.IsClientKeyNull() || site.IsServerKeyNull() )
			{
				byte[] privateKey;
				byte[] publicKey;

				SignCryptUtility.GenerateRSAKeys(out privateKey, out publicKey);

				//System.Security.Cryptography.RSACryptoServiceProvider.UseMachineKeyStore = true;
				//this.lblMessage.Text = "User: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
				//try
				//{
				//    System.Security.Cryptography.RSACryptoServiceProvider RSAalg = new System.Security.Cryptography.RSACryptoServiceProvider();
				//    this.lblMessage.Text += ", container: " + RSAalg.CspKeyContainerInfo.KeyContainerName + ", machine key?: " + RSAalg.CspKeyContainerInfo.MachineKeyStore.ToString();

				//    byte[] privateKeyBlog = RSAalg.ExportCspBlob(true);
				//    byte[] publicKeyBlog = RSAalg.ExportCspBlob(false);
				//}
				//catch ( Exception exc )
				//{
				//    this.lblMessage.Text += ", first exc mess: " + exc.Message;
				//}

				//try
				//{
				//    System.Security.Cryptography.CspParameters parameters = new System.Security.Cryptography.CspParameters();
				//    System.Security.AccessControl.CryptoKeySecurity sec = new System.Security.AccessControl.CryptoKeySecurity();

				//    sec.SetOwner(System.Security.Principal.WindowsIdentity.GetCurrent().User);
				//    parameters.CryptoKeySecurity = sec;
				//    parameters.Flags = System.Security.Cryptography.CspProviderFlags.UseDefaultKeyContainer | System.Security.Cryptography.CspProviderFlags.UseMachineKeyStore;

				//    //parameters.KeyContainerName = "GrisContainer";

				//    System.Security.Cryptography.RSACryptoServiceProvider RSAalg = new System.Security.Cryptography.RSACryptoServiceProvider(parameters);
				//    this.lblMessage.Text += ", container: " + RSAalg.CspKeyContainerInfo.KeyContainerName + ", machine key?: " + RSAalg.CspKeyContainerInfo.MachineKeyStore.ToString();

				//    byte[] privateKeyBlog = RSAalg.ExportCspBlob(true);
				//    byte[] publicKeyBlog = RSAalg.ExportCspBlob(false);
				//}
				//catch ( Exception exc )
				//{
				//    this.lblMessage.Text += ", first exc mess: " + exc.Message + ", stack trace: " + (exc.StackTrace ?? "");
				//}

				ThisSite.UpdateSiteKeys(privateKey, publicKey, site.SiteID);
			}
		}
	}

	protected void imgbUpdate_Click ( object sender, ImageClickEventArgs e )
	{
        this.imgbUpdate.ImageUrl = "~/IMG/interfaccia/btnRefresh_ok.gif";

		this.gvwConfigValidation.DataBind();
		this.gvwConfigValidated.DataBind();
	}

	# region gvwConfigValidation
	protected void gvwConfigValidation_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row.RowType == DataControlRowType.DataRow )
		{
			ImageButton img = (ImageButton)e.Row.FindControl("imgbValidate");
			if ( img != null )
			{
				img.Attributes["OnMouseOver"] = "this.src = this.src.replace('_default', '_over')";
				img.Attributes["OnMouseOut"] = "this.src = this.src.replace('_over', '_default')";
			}
		}
	}

	protected void gvwConfigValidation_DataBound ( object sender, EventArgs e )
	{
		if ( this.gvwConfigValidation.Rows.Count > 0 )
		{
			TableRow lastRow = this.gvwConfigValidation.Rows[this.gvwConfigValidation.Rows.Count - 1];

			foreach ( TableCell cell in lastRow.Cells )
			{
				cell.CssClass = "";
			}
		}
	}

	protected void gvwConfigValidation_SelectedIndexChanged ( object sender, EventArgs e )
	{
		int srvID = Convert.ToInt32(this.gvwConfigValidation.SelectedDataKey.Value);
		this.SetServerAsValidated(srvID);
	}
	# endregion

	# region gvwConfigValidated
	protected void gvwConfigValidated_DataBound ( object sender, EventArgs e )
	{
		if ( this.gvwConfigValidated.Rows.Count > 0 )
		{
			TableRow lastRow = this.gvwConfigValidated.Rows[this.gvwConfigValidated.Rows.Count - 1];

			foreach ( TableCell cell in lastRow.Cells )
			{
				cell.CssClass = "";
			}
		}
	}
	# endregion

	# region Methods
	private void SetServerAsValidated ( int srvID )
	{
		string stlcUrl = "";
		DateTime validationObtained;
		byte[] sign;
		SiteDS.siteRow site = ThisSite.GetSiteData();
		GrisSuite.Data.dsCentralServersTableAdapters.serversTableAdapter ta = new GrisSuite.Data.dsCentralServersTableAdapters.serversTableAdapter();

		dsCentralServers.serversDataTable servers = ta.GetDataByID(srvID);

		if ( servers.Count == 1 )
		{
			dsCentralServers.serversRow server = servers[0];

			if ( server.IsSupervisorSystemXMLNull() )
			{
				this.lblMessage.Text = "Non � stato possibile completare l'operazione di validazione: nessun dato di configurazione presente sul server.";
			}
			else 
			{
				if ( ConfigurationManager.AppSettings["STLCWSTemplateUrl"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["STLCWSTemplateUrl"]) )
				{
					sign = SignCryptUtility.HashAndSign(server.SupervisorSystemXML, site.ServerKey);

					stlcUrl = string.Format(ConfigurationManager.AppSettings["STLCWSTemplateUrl"], server.IP);
					STLCWS.STLCService stlcWS = new STLCWS.STLCService();
					stlcWS.Url = stlcUrl;
					//stlcWS.Url = "http://localhost:54324/Telefin.STLCWS/STLCService.asmx";
					stlcWS.Credentials = System.Net.CredentialCache.DefaultCredentials;

					try
					{
						SystemValidationEnum response = (SystemValidationEnum)stlcWS.SetConfigurationAsValidated(srvID, server.SupervisorSystemXML, sign, site.ClientKey);

						switch ( response )
						{
							case SystemValidationEnum.RequestCorrectlyReceived:
								{
									ta.UpdateServerAsValidated(srvID, server.SupervisorSystemXML, sign, sign, DateTime.Now, site.ClientKey);
									this.gvwConfigValidation.DataBind();
									break;
								}
							case SystemValidationEnum.ServerNotFound:
								{
									//this.lblMessage.Text = "Non � stato possibile completare l'operazione di validazione: server STLC1000 non trovato.";
									break;
								}
							case SystemValidationEnum.SqlError:
								{
									this.lblMessage.Text = "Non � stato possibile completare l'operazione di validazione: errore sql.";
									break;
								}
						}
					}
					catch ( System.Net.WebException wex )
					{
						this.lblMessage.Text = "Non � stato possibile completare l'operazione di validazione: il dispositivo non � raggiungibile.";
						Tracer.TraceMessage(HttpContext.Current, wex.Message);
					}
				}
			}
		}
		else
		{
			this.lblMessage.Text = "Non � stato possibile completare l'operazione di validazione: sono presenti pi� dispositivi con lo stesso identificativo univoco.";
		}
	}
	# endregion
}


using System;
using System.Data;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.DevicesDSTableAdapters;

public partial class Node : NavigationPage
{
	private int _stlcNum;
	private int _stlcOfflineNum;
	private string _sysCompleteIp = "";

	#region Properties

	public override MultiView LayoutMultiView
	{
		get { return this.StazioneMultiView; }
	}

	public override View GraphView
	{
		get { return this.GraphicalView; }
	}

	public override View GridView
	{
		get { return this.TabularView; }
	}

	#endregion

	# region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
		# region WaitingAnimation

		this.litWaiting.Text = GUtility.GetUpdateProgressDivJsFunction(true);
		this.litWaiting.Text += GUtility.GetHideProgressDivJsFunction("window.onload = {0}", true);

		# endregion

        this.navBar.ClearInformationBarData();
        
        if (!this.IsPostBack)
		{
			this.tmrNode.Interval = DBSettings.GetPageRefreshTime()*1000;

			this.RegID = this.GetID("RegID", "~/Italia.aspx");
			this.ZonID = this.GetID("ZonID", "~/Region.aspx");
			this.NodID = this.GetID("NodID", "~/Zone.aspx");

			this.SystemSelector.NodID = this.NodID;

			this.AllDataRefresh();

            this.EvaluateCurrentLayoutMode(this.SystemSelector.SystemDevicesCount);
		}
	}

	protected void Page_PreRender(object sender, EventArgs e)
	{
		this.SystemSelector.NodID = this.NodID;
		this.SystemSelector.DataBind();

		this.lblSystemDesc.Text = this.SystemSelector.SelectedSystemDescription;

		# region Pulsante Raggruppamenti e Formule

        if (GrisPermissions.IsUserInAdminGroup() ||
            GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup((RegCodes)int.Parse(Utility.GetCompCode(this.NodID))) ||
            GrisPermissions.IsUserInLocalAdminGroup((RegCodes)int.Parse(Utility.GetCompCode(this.NodID))))
		{
			this.pnlGroupsAndFormulas.Visible = true;
		}
		else
		{
			this.pnlGroupsAndFormulas.Visible = false;
		}

		# endregion
	}

	protected void tmrNode_Tick(object sender, EventArgs e)
	{
		this.AllDataRefresh();
	}

	protected void SystemSelector_SelectorSystemClicked(object sender, SelectedSystemEventArgs e)
	{
        this.EvaluateCurrentLayoutMode(e.SystemDevicesCount);

        this.navBar.ResetRefreshButtonImage();
    }

    private void EvaluateCurrentLayoutMode(int totalDevicesCount)
    {
        this.navBar.EvaluateCurrentLayoutModeByDevicesNumber(totalDevicesCount);

        this.AllDataRefresh();
    }

	protected void DevPanel_Click(object sender, EventArgs e)
	{
		if (sender != null && sender is DevPanel)
		{
			DevPanel devPnl = (DevPanel) sender;
			GrisSessionManager.SelectedDeviceType = devPnl.Type;
			this.GoToPage(PageDestination.Device, devPnl.DevID);
		}
	}

	protected void odsDevices_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
	{
        e.InputParameters["NodID"] = this.NodID;
        e.InputParameters["SystemID"] = this.SystemSelector.SelectedSystemId;
        // Non � pi� possibile vedere l'ultimo stato conosciuto delle periferiche a livello di stazione (issue 11427)
        e.InputParameters["LastStatusServers"] = string.Empty;
        e.InputParameters["VirtualObjectID"] = new Nullable<long>(0); //  this._currentVirtualObjectID;
	}

	protected void odsDevices_Selected(object sender, ObjectDataSourceStatusEventArgs e)
	{
	}

	protected void SqlDataSTLCInfo_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@NodID"].Value = this.NodID;
	}

	protected void SqlDataSTLCInfo_Selected(object sender, SqlDataSourceStatusEventArgs e)
	{
		if (e != null)
		{
			if (e.AffectedRows <= 0)
			{
				this.GoToPage(PageDestination.Zone);
			}
			else
			{
				this._stlcNum = e.AffectedRows;
			}
		}
		else
		{
			this.GoToPage(PageDestination.Zone);
		}
	}

	protected void STLCInfo1_OnSyncComplete(object sender, EventArgs e)
	{
		Thread.Sleep(DBSettings.GetDevicesRefreshSleepTime());

		this._sysCompleteIp = ((STLCInfo) sender).IP;

		this.dlstSTLCInfo.DataBind();
	}

	protected void OnChangeMaintenance(object sender, EventArgs e)
	{
		this.AllDataRefresh();
	}

	protected void STLCInfo1_OnServerDelete(object sender, EventArgs e)
	{
		this.STLCDataRefresh();
	}

	protected void dlstSTLCInfo_ItemDataBound(object sender, DataListItemEventArgs e)
	{
		STLCInfo stlcInfo = (STLCInfo) e.Item.FindControl("STLCInfo1");

		if (stlcInfo.IP == this._sysCompleteIp)
		{
			stlcInfo.SetSyncLayoutComplete();
		}

		if (stlcInfo.SevLevelReal == 3) // Offline;
		{
			this._stlcOfflineNum++;

			if (this._stlcNum == this._stlcOfflineNum && !GrisPermissions.IsUserAtLeastInLevel(PermissionLevel.Level3)) // sono tutti offline e sono in un gruppo minore di 3
			{
				this.GoToPage(PageDestination.Zone);
			}
		}
	}

	protected void navBar_OnRefreshPageButtonClick(object sender, ImageClickEventArgs e)
	{
		this.AllDataRefresh();
	}

	protected void navBar_LayoutSwitched(object sender, EventArgs e)
	{
		this.AllDataRefresh();
	}

	# region dlstDevPanel

	protected void dlstDevPanel_ItemDataBound(object sender, DataListItemEventArgs e)
	{
		DevicesDS.devicesRow row = ((DataRowView) e.Item.DataItem).Row as DevicesDS.devicesRow;

		if (row != null)
		{
			// scrittura etichette sulla barra di navigazione
			this.SetNavigationLabels(row);
		}
	}

	# endregion

	# region gvwDevice

	protected void gvwDevice_RowDataBound(object sender, GridViewRowEventArgs e)
	{
		DevicesDS.devicesRow row;

	    if (e.Row.RowType == DataControlRowType.DataRow && ((row = ((DataRowView) e.Row.DataItem).Row as DevicesDS.devicesRow) != null))
		{
			// gli appartenenti ad un gruppo di livello 1 non possono vedere le periferiche in attenzione
			if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2) && row.SevLevel == 1 /* attenzione */)
			{
				row.SevLevel = 0;
			}

			// scrittura etichette sulla barra di navigazione
			this.SetNavigationLabels(row);

			LinkButton  lnkbinfo                = (LinkButton)  e.Row.FindControl("lnkbPerif");
			ImageButton imgbOperatorMaintenance = (ImageButton) e.Row.FindControl("imgbOperatorMaintenance");
			
            ImageButton imgbScreenDump      = (ImageButton) e.Row.FindControl("imgbScreenDump");
            ImageButton imgbListenDump      = (ImageButton) e.Row.FindControl("imgbListenDump");
            ImageButton imgbCommandDump = (ImageButton)e.Row.FindControl("imgbCommandDump");

			Image imgTickets                = (Image) e.Row.FindControl("imgTickets");
			Image imgLastStatusEnabled      = (Image) e.Row.FindControl("imgLastStatusEnabled");
			Label lblSevLevelDescription    = (Label) e.Row.FindControl("lblSevLevelDescription");

			// il pulsante di visualizzazione dello screen dump viene visualizzato solo per le periferiche monitor.
			if (row.SystemID == 2)
			{
				// nel caso che la URL del web service non sia recuperabile per il monitor
				if ((!string.IsNullOrEmpty(row.WSUrlPattern)) && (!row.IsWSUrlPatternNull()))
				{
					imgbScreenDump.Visible = ScreenDump.CanUserViewScreenDump(this.RegID, row.SevLevel);
					imgbScreenDump.Attributes["OnMouseOver"] = "this.src = 'IMG/Interfaccia/screendump_over.gif'";
					imgbScreenDump.Attributes["OnMouseOut"] = "this.src = 'IMG/Interfaccia/screendump.gif'";
				}
				else
				{
					imgbScreenDump.Visible = false;
				}
			}
			else
			{
				imgbScreenDump.Visible = false;
			}
            //Screen dump sempre invisibile.... per adesso
            //imgbScreenDump.Visible = false;

			//imgbScreenDump.OnClientClick = Controllers.Device.BuildScreenDumpUrl(row.DevID);
            imgbScreenDump.OnClientClick = Controllers.Device.BuildCommandDumpUrl(row.DevID, true);

            //if (row.Type == DBSettings.GetAwgClientDeviceType()) 
            if (row.Type.StartsWith(DBSettings.GetAwgClientDeviceType())) 
            {
                imgbListenDump.Visible                      = StationListen.CanUserViewStationListen(this.RegID, row.SevLevel);
                imgbListenDump.Attributes["OnMouseOver"]    = "this.src = 'IMG/Interfaccia/arm_play_over_tab.gif'";
                imgbListenDump.Attributes["OnMouseOut"]     = "this.src = 'IMG/Interfaccia/arm_play_tab.gif'";
            }
            else
            {
                imgbListenDump.Visible = false;
            }

            if (imgbListenDump.Visible)
            {
                imgbListenDump.OnClientClick = Controllers.Device.BuildListenDumpUrl(row.DevID);
            }

            //Il pulsante di controllo solo se questo tipo di device ha comandi associati

            //if (GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3) || ((row.SevLevel == 0) && GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2)))
            if (GrisPermissions.IsUserInGroupingCommandControl(row.SystemID))
            {
                if (DBSettings.HasDeviceCommand(row.Type))
                {
                    imgbCommandDump.Visible = true;
                    imgbCommandDump.OnClientClick = Controllers.Device.BuildCommandDumpUrl(row.DevID, false);
                }
                else
                {
                    imgbCommandDump.Visible = false;
                }
            }
            else
            {
                imgbCommandDump.Visible = false;
            }
            
            
            // visualizzazione icona per le periferiche con tickets aperti
			if (string.IsNullOrEmpty(row.Ticket))
			{
				imgTickets.Visible = false;
			}
			else
			{
				imgTickets.AlternateText = imgTickets.ToolTip = row.Ticket;
				imgTickets.Visible = true;
			}

			lblSevLevelDescription.Text = row.SevLevelDescription + (row.OperatorMaintenance ? " (Non attivo)" : "");

			imgbOperatorMaintenance.ImageUrl = string.Format("~/IMG/Interfaccia/tab_inconf_{0}.gif", row.OperatorMaintenance ? "on" : "off");
			imgbOperatorMaintenance.CommandArgument = e.Row.RowIndex.ToString();

			// ultimo stato
			imgLastStatusEnabled.Visible = row.LastSevLevelReturned;

			if (lnkbinfo != null)
			{
				lnkbinfo.CommandArgument += "|" + row.DevID;
				lnkbinfo.CommandArgument += "|" + row.Name;
				lnkbinfo.CommandName = "DevInfo";
			}

			Label lblType = e.Row.FindControl("lblType") as Label;
			if (lblType != null)
			{
				if ((row.RawType.StartsWith("INFSTAZ", StringComparison.OrdinalIgnoreCase)) && (!row.Type.StartsWith("INFSTAZ", StringComparison.OrdinalIgnoreCase)))
				{
					lblType.ToolTip = "Infostazioni";
				}
				else
				{
					lblType.ToolTip = string.Empty;
				}
			}

			Label lblAddress = e.Row.FindControl("lblAddress") as Label;
			if (lblAddress != null)
			{
				if (!row.IsPortTypeNull())
				{
					lblAddress.Text = row.PortType.IndexOf("tcp", StringComparison.OrdinalIgnoreCase) < 0 ? string.Format("{0} - {1}", row.Address, row.PortName) : string.Format("{0}", row.Address);
				}
				else
				{
					lblAddress.Text = string.Format("{0}", row.Address);
				}
			}

			# region Gestione Stato

            Controls_LayeredIcon imgStatus = (Controls_LayeredIcon)e.Row.FindControl("imgStatus");

			imgStatus.Status = row.SevLevel;
			imgStatus.ImageToolTip = row.SevLevelDescription;

			# endregion

			# region Permessi di accesso alla pagina di dettaglio periferica

			// non � possibile visualizzare il dettaglio periferica se appartenenti al livello 1, o se appartenenti al livello 2 ma con periferica offline, o se la periferica ha la diagnostica disattivata
			GUtility.EnableLinkButton(this, lnkbinfo, Controllers.Device.CanUserViewDeviceDetails(this, row.SevLevel, row.SevLevelDetail, row.Type));
			GUtility.EnableImageButton(this, imgbOperatorMaintenance, Controllers.Device.CanUserChangeMaintenanceMode());

			# endregion
		}
	}

	protected void gvwDevice_RowCommand(object sender, GridViewCommandEventArgs e)
	{
		switch (e.CommandName)
		{
			case "DevInfo":
				string[] devInfos = (e.CommandArgument.ToString()).Split('|');
				GrisSessionManager.SelectedDeviceType = devInfos[0];
				this.GoToPage(PageDestination.Device, long.Parse(devInfos[1]));
				break;
			case "OperatorMaintenance":
				if ( Controllers.Device.CanUserChangeMaintenanceMode() )
				{
					int index = Convert.ToInt32(e.CommandArgument);
					GridViewRow row = ((GridView)sender).Rows[index];

					HiddenField hidDevId = (HiddenField) row.FindControl("hidDevId");
					HiddenField hidOperatorMaintenance = (HiddenField) row.FindControl("hidOperatorMaintenance");

					devicesTableAdapter ta = new devicesTableAdapter();
					bool operatorMaintenance = !Convert.ToBoolean(hidOperatorMaintenance.Value);
					ta.UpdateInMaintenance(operatorMaintenance, Convert.ToInt64(hidDevId.Value));
					FormulaEngineService formulaService = new FormulaEngineService();
					formulaService.EvaluateRegion(this.RegID);

					this.AllDataRefresh();
				}
				break;
		}
	}

	# endregion

	# endregion

	# region Methods

	private void SetNavigationLabels(DevicesDS.devicesRow row)
	{
		if (row != null)
		{
			this.navBar.LinkCompartimentoText = row.Region.Replace("Compartimento di ", "");
			this.navBar.LinkCompartimentoTooltip = row.Region;
			this.navBar.LinkLineaText = row.Zone.Replace("Linea ", "");
			this.navBar.LinkLineaTooltip = row.Zone;
            this.navBar.SetCompartimentoSeverita(row.SevLevelRegion, (row.IsRegionColorNull() ? null : row.RegionColor));
            this.navBar.SetLineaSeverita(row.SevLevelZone, (row.IsZoneColorNull() ? null : row.ZoneColor));
            this.navBar.SetStazioneSeverita(row.SevLevelNode, (row.IsNodeColorNull() ? null : row.NodeColor));
		}
	}

	private void SystemsDataRefresh()
	{
		this.SystemSelector.DataBind();
		this.updSystems.Update();
	    this.updNavBar.Update();
	}

	private void DevicesDataRefresh()
	{
		if (this.navBar.LayoutMode == MapLayoutMode.Graph)
		{
			this.dlstDevPanel.DataBind();
		}
		else
		{
            this.rptVO.DataBind();
		}

		this.updDevices.Update();
	}

	private void STLCDataRefresh()
	{
		this.dlstSTLCInfo.DataBind();
		this.updSTLCInfo.Update();
	}

	private void AllDataRefresh()
	{
		this.SystemsDataRefresh();
		this.DevicesDataRefresh();
		this.STLCDataRefresh();
	}

	# endregion

    protected void rptVO_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            GridView gvwDevice = e.Item.FindControl("gvwDevice") as GridView;
            HtmlControl voHeader = e.Item.FindControl("VOHeader") as HtmlControl;
            long virtualObjectID = ((long)((DataRowView)e.Item.DataItem)["VirtualObjectID"]);
            if ((voHeader != null) && (virtualObjectID >= 0) && (gvwDevice != null))
            {
                devicesTableAdapter ta = new devicesTableAdapter();
                // Non � pi� possibile vedere l'ultimo stato conosciuto delle periferiche a livello di stazione (issue 11427)
                DevicesDS.devicesDataTable dt = ta.GetData(this.NodID, this.SystemSelector.SelectedSystemId, string.Empty, virtualObjectID);
                gvwDevice.DataSource = dt;

                if (dt.Rows.Count > 0)
                {
                    voHeader.Visible = virtualObjectID > 0;
                    gvwDevice.Visible = true;
                    gvwDevice.DataBind();
                }
                else
                {
                    voHeader.Visible = false;
                    gvwDevice.Visible = false;
                }
            }
        }
    }

    protected void SqlDataVOByNode_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@NodID"].Value = this.NodID;
        e.Command.Parameters["@SystemID"].Value = this.SystemSelector.SelectedSystemId;
    }
}
using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.AlertsDSTableAdapters;
using System.Text;

public partial class MasterPage : System.Web.UI.MasterPage, IGrisMasterPage
{
	# region Properties

	public bool ToolBarOpened
	{
		get { return Convert.ToBoolean(this.ViewState["ToolBarOpened"] ?? false); }
		set
		{
			this.ViewState["ToolBarOpened"] = value;

			if (value)
			{
				this.tdGrip.Visible = true;
				this.tdToolBar.Visible = true;
				this.imgbClosedGrip.ImageUrl = "~/IMG/interfaccia/pan_off_dx.gif";
				this.imgbClosedGrip.Attributes["OnMouseOut"] = string.Format("this.src = '{0}';", this.ResolveClientUrl("~/IMG/interfaccia/pan_off_dx.gif"));
				this.imgbClosedGrip.Enabled = false;
				this.imgbClosedGrip.ToolTip = "";
			}
			else
			{
				this.tdGrip.Visible = false;
				this.tdToolBar.Visible = false;
				this.imgbClosedGrip.ImageUrl = "~/IMG/interfaccia/head_dx.gif";
				this.imgbClosedGrip.Attributes["OnMouseOver"] = string.Format("this.src = '{0}';", this.ResolveClientUrl("~/IMG/interfaccia/head_dx_over.gif"));
				this.imgbClosedGrip.Attributes["OnMouseOut"] = string.Format("this.src = '{0}';", this.ResolveClientUrl("~/IMG/interfaccia/head_dx.gif"));
				this.imgbClosedGrip.Enabled = true;
				this.imgbClosedGrip.ToolTip = "Apri il Menu";
			}
		}
	}

	public bool ShowProgressAnimation
	{
		get { return Convert.ToBoolean(this.ViewState["ShowProgressAnimation"] ?? true); }
		set { this.ViewState["ShowProgressAnimation"] = value; }
	}

	# endregion

	# region Event Handlers

	protected void Page_Load(object sender, EventArgs e)
	{
		this.pnlClosedToolBar.Style["background-repeat"] = "repeat-x";
		this.pnlHeadSx.Style["background-repeat"] = "no-repeat";
		this.pnlRfi.Style["background-repeat"] = "no-repeat";
		this.pnlPic.Style["background-repeat"] = "no-repeat";
		this.pnlPic.Style["background-position"] = "right top";
		this.pnlGris.Style["background-repeat"] = "no-repeat";
		this.pnlGris.Style["background-position"] = "left top";

		//onmouseover immagini menu di navigazione
		string imgsExcludedFilters = "this.src.indexOf('_selez') == -1";
		string imgsExcludedAlertsFilters = "this.src.indexOf('_selez') == -1 && this.src.indexOf('_active') == -1";
		string scriptStatusOver = "if ( {0} ) this.src = this.src.replace('.gif', '_over.gif');";
		string scriptStatusOut = "if ( {0} ) this.src = this.src.replace('_over.gif', '.gif');";

		this.imgbMappa.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
		this.imgbMappa.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);
		this.imgbFiltri.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
		this.imgbFiltri.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);
		this.imgbReports.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
		this.imgbReports.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);
		this.imgbAdmin.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedFilters);
		this.imgbAdmin.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedFilters);
		this.imgbAlerts.Attributes["OnMouseOver"] = string.Format(scriptStatusOver, imgsExcludedAlertsFilters);
		this.imgbAlerts.Attributes["OnMouseOut"] = string.Format(scriptStatusOut, imgsExcludedAlertsFilters);

		if (!this.IsPostBack)
		{
			// Imposta l'aspetto della menu toolbar
			this.SelectInterface((GrisLayoutMode)Enum.Parse(typeof(GrisLayoutMode),
				GrisSessionManager.Session.Get(GrisSessionManager.GrisLayoutModeKey,
											   new SessionManagerCallbackHandler<string>(this.ReadCurrentLayoutMode))), false);

			this.imgbAlerts.ToolTip = "Allarmi";

			int cacheLogMessagesObsoleteTimespanMinutes = DBSettings.GetCacheLogMessagesObsoleteTimespanMinutes();

			if (GrisSessionManager.GrisLayoutMode != GrisLayoutMode.Alerts)
			{
				switch (new AlertTicketsTableAdapter().AlertsPresent(cacheLogMessagesObsoleteTimespanMinutes, GrisPermissions.IsUserInAdminGroup()))
				{
					case AlertsPresence.Present:
						{
							this.imgbAlerts.ImageUrl = "~/IMG/interfaccia/pan_allarmi_active.gif";
							break;
						}
					case AlertsPresence.NotPresent:
						{
							this.imgbAlerts.ImageUrl = "~/IMG/interfaccia/pan_allarmi.gif";
							break;
						}
					case AlertsPresence.CacheEngineError:
						{
							this.imgbAlerts.ImageUrl = "~/IMG/interfaccia/pan_allarmi_cache_engine_error.gif";
							this.imgbAlerts.ToolTip = string.Format("Il motore di caching non ha scodato messaggi negli ultimi {0} minuti", cacheLogMessagesObsoleteTimespanMinutes);
							break;
						}
					default:
						{
							this.imgbAlerts.ImageUrl = "~/IMG/interfaccia/pan_allarmi_error.gif";
							break;
						}
				}
			}

			// La Menu Toolbox � chiusa per gli utenti non appartenenti ai gruppi Admins, GrisOperators e ConfigValidators
			GUtility.EnableImageButton(this.Page, this.imgbClosedGrip, false);

			if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup() || GrisPermissions.IsUserInLocalAdminGroup())
			{
				this.lblVersion.Visible = true;
				this.lblVersion.Text = "Versione " + (ConfigurationManager.AppSettings["Version"] ?? "non disponibile");

				this.lblHostName.Visible = true;
				this.lblHostName.Text = string.Format("&nbsp;|&nbsp;{0}", this.Server.MachineName);

				if (!this.ToolBarOpened)
				{
					GUtility.EnableImageButton(this.Page, this.imgbClosedGrip, true);
				}
			}
			else
			{
				if (GrisPermissions.IsUserInConfigValidationGroup() || GrisPermissions.IsUserInLocalAdminGroup() || GrisPermissions.IsUserInGrisClientLicenseAdminGroup() || GrisPermissions.IsUserInGrisClientLicenseUsersGroup())
				{
					GUtility.EnableImageButton(this.Page, this.imgbClosedGrip, true);
				}

				this.lblVersion.Visible = false;
				this.lblHostName.Visible = false;
			}

			# region Etichetta gruppi utente loggato

			if (GrisPermissions.IsUserInAdminGroup())
			{
				this.LoginName1.ToolTip = "Gruppo Amministratori";
			}
			else if (GrisPermissions.IsUserInGrisOperatorsGroup())
			{
				this.LoginName1.ToolTip = "Gruppo Operatori Gris";
			}
			else if (this.IsInLevel(this.Page, PermissionLevel.Level3))
			{
				this.LoginName1.ToolTip = "Gruppo Livello 3";
			}
			else if (this.IsInLevel(this.Page, PermissionLevel.Level2))
			{
				this.LoginName1.ToolTip = "Gruppo Livello 2";
			}
			else if (this.IsInLevel(this.Page, PermissionLevel.Level1))
			{
				this.LoginName1.ToolTip = "Gruppo Livello 1";
			}
			else
			{
				this.LoginName1.ToolTip = "";
			}

			if (GrisPermissions.IsUserInConfigValidationGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gruppo Validatori";
			}

			if (GrisPermissions.IsUserInAlertOperatorsGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gruppo Operatori Allarmi";
			}

			if (GrisPermissions.IsUserInAlertUsersGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gruppo Visualizzatori Allarmi";
			}

			if (GrisPermissions.IsUserInSTLCOperatorsGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gruppo Operatori STLC 1000";
			}

			#region Amministratori di compartimento

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Torino))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Torino";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Milano))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Milano";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Verona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Verona";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Venezia))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Venezia";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Trieste))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Trieste";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Genova))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Genova";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Bologna))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Bologna";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Firenze))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Firenze";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Ancona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Ancona";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Roma))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Roma";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Napoli))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Napoli";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Bari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Bari";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.ReggioCalabria))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Reggio Calabria";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Palermo))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Palermo";
			}

			if (GrisPermissions.IsUserInLocalAdminGroup(RegCodes.Cagliari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Amministratori di compartimento Cagliari";
			}

			#endregion

			#region Gestori Raggruppamenti di compartimento

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Torino))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Torino";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Milano))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Milano";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Verona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Verona";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Venezia))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Venezia";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Trieste))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Trieste";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Genova))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Genova";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Bologna))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Bologna";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Firenze))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Firenze";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Ancona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Ancona";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Roma))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Roma";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Napoli))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Napoli";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Bari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Bari";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.ReggioCalabria))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Reggio Calabria";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Palermo))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Palermo";
			}

			if (GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes.Cagliari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestori Raggruppamenti di compartimento Cagliari";
			}
			#endregion

			#region Monitoraggio VPN Verde

			if (GrisPermissions.IsUserInVPNVerdeItaliaGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde a livello Italia";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Torino))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Torino";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Milano))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Milano";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Verona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Verona";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Venezia))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Venezia";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Trieste))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Trieste";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Genova))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Genova";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Bologna))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Bologna";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Firenze))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Firenze";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Ancona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Ancona";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Roma))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Roma";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Napoli))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Napoli";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Bari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Bari";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.ReggioCalabria))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Reggio Calabria";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Palermo))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Palermo";
			}

			if (GrisPermissions.IsUserInLocalVPNVerdeCompGroup(RegCodes.Cagliari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio VPN Verde di compartimento Cagliari";
			}

			#endregion

			#region Monitoraggio Web Radio

			if (GrisPermissions.IsUserInWebRadioItaliaGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio a livello Italia";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Torino))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Torino";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Milano))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Milano";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Verona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Verona";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Venezia))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Venezia";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Trieste))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Trieste";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Genova))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Genova";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Bologna))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Bologna";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Firenze))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Firenze";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Ancona))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Ancona";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Roma))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Roma";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Napoli))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Napoli";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Bari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Bari";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.ReggioCalabria))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Reggio Calabria";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Palermo))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Palermo";
			}

			if (GrisPermissions.IsUserInLocalWebRadioCompGroup(RegCodes.Cagliari))
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Monitoraggio Web Radio di compartimento Cagliari";
			}

			if (GrisPermissions.IsUserInGrisClientLicenseAdminGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Gestione licenze server";
			}

			if (GrisPermissions.IsUserInGrisClientLicenseUsersGroup())
			{
				if (this.LoginName1.ToolTip.Length > 0)
				{
					this.LoginName1.ToolTip += ", ";
				}
				this.LoginName1.ToolTip += "Visualizzazione licenze server";
			}

			#endregion

			#endregion
		}

		this.SetToolButtonsVisibility();

		ClientScriptManager cs = this.Page.ClientScript;
		if (!cs.IsClientScriptBlockRegistered("SilverlightErrorManager"))
		{
			string silverlightErrorFunction;
#if (!DEBUG)
			silverlightErrorFunction =
				"function onSilverlightError(sender, args) {var appSource = \"\";if (sender != null && sender != 0) {appSource = sender.getHost().Source;}var errorType = args.ErrorType;var iErrorCode = args.ErrorCode;if (errorType == \"ImageError\" || errorType == \"MediaError\") {return;}ToggleAlertTable('block');window.scrollBy(0, 600);}";
#else
			silverlightErrorFunction =
				"function onSilverlightError(sender, args) {var appSource = \"\";if (sender != null && sender != 0) {appSource = sender.getHost().Source;}var errorType = args.ErrorType;var iErrorCode = args.ErrorCode;if (errorType == \"ImageError\" || errorType == \"MediaError\") {return;}var errMsg = \"Unhandled Error in Silverlight Application \" +  appSource + \"\\n\" ;errMsg += \"Code: \"+ iErrorCode + \"    \\n\";errMsg += \"Category: \" + errorType + \"       \\n\";errMsg += \"Message: \" + args.ErrorMessage + \"     \\n\";if (errorType == \"ParserError\") {errMsg += \"File: \" + args.xamlFile + \"     \\n\";errMsg += \"Line: \" + args.lineNumber + \"     \\n\";errMsg += \"Position: \" + args.charPosition + \"     \\n\";}else if (errorType == \"RuntimeError\") {           if (args.lineNumber != 0) {errMsg += \"Line: \" + args.lineNumber + \"     \\n\";errMsg += \"Position: \" +  args.charPosition + \"     \\n\";}errMsg += \"MethodName: \" + args.methodName + \"     \\n\";}throw new Error(errMsg);}";
#endif
			cs.RegisterClientScriptBlock(this.GetType(), "SilverlightErrorManager", silverlightErrorFunction, true);
		}

		this.CreateUpdateProgressPanel();
	}

	private void CreateUpdateProgressPanel()
	{
		Panel updateProgressDiv = new Panel { ID = "updateProgressDiv", CssClass = "updateProgress" };

		Image imgUpdateProgress = new Image
								  {
									  ID = "imgUpdateProgress",
									  CssClass = "updateProgressImage",
									  ImageUrl = "~/IMG/loadinfo.gif",
									  AlternateText = "Caricamento in corso"
								  };
		updateProgressDiv.Controls.Add(imgUpdateProgress);

		this.phoUpdateProgress.Controls.Add(updateProgressDiv);

		StringBuilder scripts = new StringBuilder();
		scripts.AppendFormat("var _updateProgressDiv=$get('{0}');var _imgUpdateProgress=$get('{1}');", updateProgressDiv.ClientID, imgUpdateProgress.ClientID);
		scripts.AppendFormat("function {0}{{if ((_updateProgressDiv != undefined) && (_updateProgressDiv != null)) {{ToggleDomElements('hidden');_updateProgressDiv.style.visibility = 'visible';if ((_imgUpdateProgress != undefined) && (_imgUpdateProgress != null)) {{setTimeout(\"_imgUpdateProgress.src = '{1}';\", 200);}}}}}}", GUtility.GetUpdateProgressDivJsFunctionName(), this.ResolveClientUrl(imgUpdateProgress.ImageUrl));

		if (this.ShowProgressAnimation)
		{
			scripts.Append("function EndRequestHandler(sender, args) {try {if (args.get_error() != undefined) {args.set_errorHandled(true);ToggleAlertTable('block');window.scrollBy(0, 600);}else {ToggleAlertTable('none');}if ((_updateProgressDiv != undefined) && (_updateProgressDiv != null)) {ToggleDomElements('visible');_updateProgressDiv.style.visibility = 'hidden';}}catch (err) {}}");
			scripts.AppendFormat("function {0}(){{try {{if ((_updateProgressDiv != undefined) && (_updateProgressDiv != null)) {{ToggleDomElements('visible');_updateProgressDiv.style.visibility = 'hidden';}}}}catch (err) {{}}}}", GUtility.GetHideProgressDivJsFunctionNameOnly());
			scripts.AppendFormat("Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);function BeginRequestHandler(sender, args){{{0}}}Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);function ToggleAlertTable(visString){{var atbl = $get('tblClientError');$get('tblClientError').style.display = visString;}}", GUtility.GetUpdateProgressDivJsFunction());
		}
		scripts.Append("function ToggleDomElements(status){if ((Sys.Browser.agent === Sys.Browser.InternetExplorer) && (Sys.Browser.version < 7)) {var elems=document.getElementsByTagName('select');for (var k = 0; k < elems.length; k++) {elems[k].style.visibility = status;}}}");

		this.litWaiting.Text = GUtility.EmbedInScriptTag(scripts.ToString());
	}

	protected void Page_Prerender(object sender, EventArgs e)
	{
		if (this.Page is IGrisPage)
		{
			this.RecordLayoutModeLastRequestedUrl();
		}
	    if (GrisPermissions.CanUserAccessGris() )
	    {
	        this.imgbMappa.Visible = DBSettings.GetMappaButtonVisible();
	        this.imgbAlerts.Visible = DBSettings.GetAlertsButtonVisible();
	        this.imgbReports.Visible = DBSettings.GetReportsButtonVisible();
	    }
        else if (GrisPermissions.CanAccessResource((int)DatabaseResource.DeviceFilters))
	    {
            this.imgbMappa.Visible = false;
            this.imgbAlerts.Visible = false;
            this.imgbReports.Visible = false;
        }
        this.imgbFiltri.Visible = DBSettings.GetFiltersButtonVisible();

    }

	protected void imgbGrip_Click(object sender, ImageClickEventArgs e)
	{
		this.ToolBarOpened = !this.ToolBarOpened;
	}

	protected void imgbMappa_Click(object sender, ImageClickEventArgs e)
	{
		this.SelectInterface(GrisLayoutMode.Map, true);
	}

	protected void imgbFiltri_Click(object sender, ImageClickEventArgs e)
	{
		this.SelectInterface(GrisLayoutMode.Filters, true);
	}

	protected void imgbReports_Click(object sender, ImageClickEventArgs e)
	{
		this.SelectInterface(GrisLayoutMode.Reports, true);
	}

	protected void imgbAlerts_Click(object sender, ImageClickEventArgs e)
	{
		this.SelectInterface(GrisLayoutMode.Alerts, true);
	}

	protected void imgbValidation_Click(object sender, ImageClickEventArgs e)
	{
		this.SelectInterface(GrisLayoutMode.Validation, true);
	}

	protected void imgbAdmin_Click(object sender, ImageClickEventArgs e)
	{
		this.SelectInterface(GrisLayoutMode.Administration, true);
	}

	protected void smMain_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
	{
		string message = "";
		string stackTrace = "";

		if (e.Exception != null)
		{
			if (e.Exception.InnerException != null)
			{
				message = e.Exception.InnerException.Message;
				stackTrace = e.Exception.InnerException.StackTrace ?? string.Empty;
			}
			else
			{
				message = e.Exception.Message;
				stackTrace = e.Exception.StackTrace ?? string.Empty;
			}
		}

		string errorMessage = string.Format("Error -> {0}, Stack Trace -> {1}", message, stackTrace);

#if (DEBUG)
		this.smMain.AsyncPostBackErrorMessage = errorMessage;
#else
		if (e.Exception != null)
		{
			// Persiste l'eccezione serializzata in base dati
			if (!Tracer.TraceMessage(this.Context, e.Exception, true))
			{
				// Logga l'eccezione via trace listener (su disco)
				System.Diagnostics.Trace.TraceError(string.Format("{0}, User -> {1}", errorMessage,
																  GUtility.GetUserInfos(true)));
			}
		}

		this.smMain.AsyncPostBackErrorMessage = "Errore nella comunicazione con il server.";
#endif
	}

	# endregion

	# region Methods

	public void SelectInterface(GrisLayoutMode layoutMode, bool performRedirect)
	{
		GrisSessionManager.GrisLayoutMode = layoutMode;

		this.imgbMappa.ImageUrl = this.imgbMappa.ImageUrl.Replace("_selez", "");
		this.imgbFiltri.ImageUrl = this.imgbFiltri.ImageUrl.Replace("_selez", "");
		this.imgbReports.ImageUrl = this.imgbReports.ImageUrl.Replace("_selez", "");
		this.imgbAlerts.ImageUrl = this.imgbAlerts.ImageUrl.Replace("_selez", "");
		this.imgbAdmin.ImageUrl = this.imgbAdmin.ImageUrl.Replace("_selez", "");

		this.ToolBarOpened = false;

		switch (layoutMode)
		{
			case GrisLayoutMode.Map:
				{
					this.imgbMappa.ImageUrl = this.imgbMappa.ImageUrl.Replace(".gif", "_selez.gif");
					if (performRedirect)
					{
						this.Response.Redirect(GrisSessionManager.LastMapUrl);
					}
					GUtility.EnableImageButton(this.Page, this.imgbMappa, false);
					break;
				}
			case GrisLayoutMode.Filters:
				{
					this.imgbFiltri.ImageUrl = this.imgbFiltri.ImageUrl.Replace(".gif", "_selez.gif");
					if (performRedirect)
					{
						this.Response.Redirect(GrisSessionManager.LastFiltersUrl);
					}
					GUtility.EnableImageButton(this.Page, this.imgbFiltri, false);
					break;
				}
			case GrisLayoutMode.Reports:
				{
					this.imgbReports.ImageUrl = this.imgbReports.ImageUrl.Replace(".gif", "_selez.gif");
					if (performRedirect)
					{
						this.Response.Redirect(GrisSessionManager.LastReportsUrl);
					}
					GUtility.EnableImageButton(this.Page, this.imgbReports, false);
					break;
				}
			case GrisLayoutMode.Alerts:
				{
					this.imgbAlerts.ImageUrl = this.imgbAlerts.ImageUrl.Replace(".gif", "_selez.gif");
					if (performRedirect)
					{
						this.Response.Redirect("~/Alerts.aspx");
					}
					GUtility.EnableImageButton(this.Page, this.imgbAlerts, false);
					break;
				}
			case GrisLayoutMode.Administration:
				{
					this.imgbAdmin.ImageUrl = this.imgbAdmin.ImageUrl.Replace(".gif", "_selez.gif");
					if (performRedirect)
					{
						this.Response.Redirect("~/Admin/AdminMain.aspx");
					}
					GUtility.EnableImageButton(this.Page, this.imgbAdmin, false);
					this.ToolBarOpened = true;
					break;
				}
		}
	}

	private void SetToolButtonsVisibility()
	{
		this.imgbAlerts.Visible = GrisPermissions.CanUserViewAlerts();
		this.imgbAdmin.Visible = GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInLocalAdminGroup() || GrisPermissions.IsUserInGrisClientLicenseAdminGroup() ||
											GrisPermissions.IsUserInGrisClientLicenseUsersGroup();
	}

	private void RecordLayoutModeLastRequestedUrl()
	{
		GrisSessionManager.LastUrl = string.Format("{0}?{1}", this.Request.AppRelativeCurrentExecutionFilePath, this.Request.QueryString);

		switch ((GrisLayoutMode)Enum.Parse(typeof(GrisLayoutMode),
			GrisSessionManager.Session.Get(GrisSessionManager.GrisLayoutModeKey,
										 new SessionManagerCallbackHandler<string>(this.ReadCurrentLayoutMode))))
		{
			case GrisLayoutMode.Map:
				{
					GrisSessionManager.LastMapUrl = GrisSessionManager.LastUrl;
					break;
				}
			case GrisLayoutMode.Filters:
				{
					GrisSessionManager.LastFiltersUrl = GrisSessionManager.LastUrl;
					break;
				}
			case GrisLayoutMode.Reports:
				{
					GrisSessionManager.LastReportsUrl = GrisSessionManager.LastUrl;
					break;
				}
			case GrisLayoutMode.Alerts:
				{
					break;
				}
			case GrisLayoutMode.Validation:
				{
					break;
				}
			case GrisLayoutMode.Administration:
				{
					break;
				}
		}
	}

	private string ReadCurrentLayoutMode()
	{
		if (this.Request.RawUrl.IndexOf("Italia.aspx", StringComparison.InvariantCultureIgnoreCase) != -1)
		{
			return GrisLayoutMode.Map.ToString();
		}

		if (this.Request.RawUrl.IndexOf("/Reports", StringComparison.InvariantCultureIgnoreCase) != -1)
		{
			return GrisLayoutMode.Reports.ToString();
		}

		if (this.Request.RawUrl.IndexOf("Alerts.aspx", StringComparison.InvariantCultureIgnoreCase) != -1)
		{
			return GrisLayoutMode.Alerts.ToString();
		}

		if (this.Request.RawUrl.IndexOf("ConfigurationValidation.aspx", StringComparison.InvariantCultureIgnoreCase) != -1)
		{
			return GrisLayoutMode.Validation.ToString();
		}

		if (this.Request.RawUrl.IndexOf("/Admin/", StringComparison.InvariantCultureIgnoreCase) != -1)
		{
			return GrisLayoutMode.Administration.ToString();
		}

		return GrisLayoutMode.Map.ToString();
	}

	// restituisce il massimo livello di appartenenza sulla pagina dell' Italia, altrimenti il  massimo livello per il compartimento selezionato
	private bool IsInLevel(Page page, PermissionLevel level)
	{
		NavigationPage gPage;

		if ((page is NavigationPage) && (gPage = page as NavigationPage).RegID != -1)
		{
			return GrisPermissions.IsInRole(gPage.RegID, level);
		}

		return GrisPermissions.IsUserAtLeastInLevel(level);
	}

	# endregion
}
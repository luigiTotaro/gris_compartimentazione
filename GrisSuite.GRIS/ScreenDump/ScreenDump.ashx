<%@ WebHandler Language="C#" Class="ScreenDumpHandler" %>

using System;
using System.Configuration;
using System.Web;
using System.Xml;
using System.Web.Services.Protocols;
using System.Reflection;
using System.IO;

public class ScreenDumpHandler : IHttpHandler {
    HttpContext _context;

    public void ProcessRequest(HttpContext context) {
        this._context = context;

        string vendor = context.Request.QueryString["ven"];
        string uri = context.Request.QueryString["uri"];
        string ip = context.Request.QueryString["ip"];

        string fakeMonWS = ConfigurationManager.AppSettings["FakeMonitorWS.Service"];

        if (!string.IsNullOrEmpty(fakeMonWS)) {
            // Siamo in sviluppo o preproduzione, il FakeMon URL � indicato
            vendor = "FakeMon";
        }

        if (!context.User.Identity.IsAuthenticated) {
            this.HandleException("Errore: un utente non autenticato non pu� visualizzare lo screen dump del monitor.");
        }

        if (!string.IsNullOrEmpty(vendor) && !string.IsNullOrEmpty(uri) && !string.IsNullOrEmpty(ip)) {
            object result = this.MonitorWSMethodCall(vendor, uri, ip, "GetScreenDump", null);

            if (result != null) {
                string xmlScreenDump = result.ToString();

                XmlDocument xmlDoc = new XmlDocument();

                try {
                    xmlDoc.LoadXml(xmlScreenDump);
                }
                catch (XmlException) {
                    this.HandleException("Errore: il documento xml trasmesso non � valido.");
                    return;
                }

                XmlNode rootNode = xmlDoc.GetElementsByTagName("ScreenDump")[0];
                if (rootNode == null) {
                    rootNode = xmlDoc.GetElementsByTagName("mstns:ScreenDump")[0];
                    if (rootNode == null) {
                        this.HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'ScreenDump', la specifiche non sono state rispettate.");
                        return;
                    }
                }

                XmlNode formatoNode = xmlDoc.GetElementsByTagName("Formato")[0];
                if (formatoNode == null) {
                    this.HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'Formato' che specifica il formato di codifica dell'immagine di screen dump.");
                    return;
                }

                XmlNode datiNode = xmlDoc.GetElementsByTagName("Dati")[0];
                if (datiNode == null) {
                    this.HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'Dati' che contiene l'immagine di screen dump.");
                    return;
                }

                if (datiNode.InnerXml.Trim().Length == 0) {
                    this.HandleException("Errore: l'xml restituito dal web service non contiene informazioni nel nodo 'Dati' che contiene l'immagine di screen dump.");
                    return;
                }

                string b64ImgString = datiNode.InnerText.Replace("\n", "");

                _context.Response.ContentType = GetContentMimeType(formatoNode.InnerText);
                _context.Response.BinaryWrite(Convert.FromBase64String(b64ImgString));
                _context.Response.Flush();
                _context.Response.End();
            }
            else {
                this.HandleException("Errore: nessuna immagine restituita dal web service.");
            }
        }
        else {
            this.HandleException("Errore: lo screen dump handler non ha ricevuto i parametri corretti per il rendering dell' immagine.");
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

    private object MonitorWSMethodCall(string monitorType, string wsUri, string ip, string methodName, object[] parameters) {
        SoapHttpClientProtocol wsProxy;
        switch (monitorType) {
            case "Sysco": {
                    wsProxy = new SyscoMonitorWS.Ws();
                    break;
                }
            case "Aesys": {
                    wsProxy = new AesysMonitorWS.ICMonitor();
                    break;
                }
            case "Fida":
            case "Solari":
                {
                    wsProxy = new SolariMonitorWS.ProxyDispositivoInfostazioni();
                    break;
                }
            default: {
                    wsProxy = new FakeMonitorWS.Service();
                    break;
                }
        }

        if (monitorType != "FakeMon") {
            wsProxy.Url = string.Format(wsUri, ip);
        }

        MethodInfo wsMethod = null;
        try {
            wsMethod = wsProxy.GetType().GetMethod(methodName);
        }
        catch (AmbiguousMatchException) {
            this.HandleException("Errore di reflection durante la risoluzione del metodo del proxy al web service. Il nome potrebbe essere ambiguo.");
        }

        if (wsMethod != null) {
            try {
                return wsMethod.Invoke(wsProxy, parameters);
            }
            catch (TargetInvocationException) {
                this.HandleException("Errore: eccezione sollevata nel metodo del web service.");
            }
            catch (Exception) {
                this.HandleException("Errore generico durante l'invocazione del metodo del web service.");
            }
        }

        return null;
    }

    private static string GetContentMimeType(string format) {
        switch (format.ToUpper()) {
            case "JPG": {
                    return "image/jpg";
                }
            case "JPEG": {
                    return "image/jpeg";
                }
            case "GIF": {
                    return "image/gif";
                }
            case "PNG": {
                    return "image/png";
                }
            case "BMP": {
                    return "image/bmp";
                }
        }

        return null;
    }

    private void HandleException(string errorMessage) {
        FileStream fs;

        string errorIconPath = this._context.Server.MapPath("~/IMG/interfaccia/risposta-inaspettata.gif");

        if (File.Exists(errorIconPath)) {
            byte[] buffer;
            using (fs = File.Open(errorIconPath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                Tracer.TraceMessage(this._context, errorMessage);
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
                fs.Close();
            }

            this._context.Response.ContentType = "image/gif";
            this._context.Response.BinaryWrite(buffer);
            this._context.Response.Flush();
            this._context.Response.End();
        }
        else {
            Tracer.TraceMessage(this._context, "Errore: Impossibile trovare l'immagine d'errore.");
        }
    }
}
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ScreenDumpDevicesDSTableAdapters;

public partial class ScreenDump : Page {
    private const string LABEL_VENDOR = "Produttore:";
    private const string LABEL_IP = "Indirizzo IP:";
    private const string LABEL_WHERE = "Ubicazione:";
    private const string LABEL_DATE = "Data visualizzazione:";
    private const string LABEL_NOTE = "Note:";
    private const string LABEL_NODE = "Stazione:";
    private const string LABEL_DEVICE = "Nome:";

    #region Properties

    public long DevID {
        get { return Convert.ToInt64(this.ViewState["DevID"]); }

        set { this.ViewState["DevID"] = value; }
    }

    public long RegID {
        get { return Convert.ToInt64(this.ViewState["RegID"]); }

        set { this.ViewState["RegID"] = value; }
    }

    public bool IsScreenDumpLayout {
        get { return Convert.ToBoolean(this.ViewState["IsScreenDumpLayout"] ?? true); }

        set {
            this.ViewState["IsScreenDumpLayout"] = value;
            this.SetPageLayout(value);

            if (value) {
                this.UpdateScreenDump();
            }
            else {
                this.UpdateShownData();
            }
        }
    }

    public bool AutoSize {
        get { return Convert.ToBoolean(this.ViewState["AutoSize"] ?? false); }

        set { this.ViewState["AutoSize"] = value; }
    }

    public bool IsRefreshing {
        get { return Convert.ToBoolean(this.ViewState["IsRefreshing"] ?? false); }

        set { this.ViewState["IsRefreshing"] = value; }
    }

    public string Vendor {
        get { return (this.ViewState["Vendor"] ?? "").ToString(); }

        set { this.ViewState["Vendor"] = value; }
    }

    public string Uri {
        get { return (this.ViewState["Uri"] ?? "").ToString(); }

        set { this.ViewState["Uri"] = value; }
    }

    public string Device {
        get { return (this.ViewState["Device"] ?? "").ToString(); }

        set { this.ViewState["Device"] = value; }
    }

    public string Building {
        get { return (this.ViewState["Building"] ?? "").ToString(); }

        set { this.ViewState["Building"] = value; }
    }

    public string Rack {
        get { return (this.ViewState["Rack"] ?? "").ToString(); }

        set { this.ViewState["Rack"] = value; }
    }

    public string RackDescription {
        get { return (this.ViewState["RackDescription"] ?? "").ToString(); }

        set { this.ViewState["RackDescription"] = value; }
    }

    public string IP {
        get { return (this.ViewState["IP"] ?? "").ToString(); }

        set { this.ViewState["IP"] = value; }
    }

    public string Type {
        get { return (this.ViewState["Type"] ?? "").ToString(); }

        set { this.ViewState["Type"] = value; }
    }

    public string NodeName {
        get { return (this.ViewState["NodeName"] ?? "").ToString(); }

        set { this.ViewState["NodeName"] = value; }
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e) {
#if (DEBUG)
        this.manScreenDump.Scripts.Add(new ScriptReference("~/ScreenDump/ScreenDump.debug.js"));
#else
        this.manScreenDump.Scripts.Add(new ScriptReference("~/ScreenDump/ScreenDump.js"));
#endif

        //TODO: controllo sui diritti legati allo stato

        this.lblVendorLabel.Text = LABEL_VENDOR;
        this.lblIPLabel.Text = LABEL_IP;
        this.lblWhereLabel.Text = LABEL_WHERE;
        this.lblDataOraLabel.Text = LABEL_DATE;
        this.lblNotesLabel.Text = LABEL_NOTE;
        this.lblNodeLabel.Text = LABEL_NODE;
        this.lblMonLabel.Text = LABEL_DEVICE;

        if (!this.IsPostBack) {
            long devId;
            if (this.Request.QueryString["dev"] != null && long.TryParse(this.Request.QueryString["dev"], out devId)) {
                this.DevID = devId;
                // ottengo gli altri dati relativi alla periferica in DB
                if (this.InitializeFieldsFromData(this.DevID)) {
                    #region Recupero dati da base dati del device
                    this.lblNodeName.Text = this.NodeName;
                    this.lblMonName.Text = this.Device;

                    // topografia
                    this.lblWhere.Text = this.Rack;
                    if (!string.IsNullOrEmpty(this.RackDescription) && this.RackDescription != "Nessuna") {
                        this.lblNotesLabel.Visible = true;
                        this.lblNotes.Visible = true;

                        this.lblNotes.Text = this.RackDescription;
                    }
                    else {
                        this.lblNotesLabel.Visible = false;
                        this.lblNotes.Visible = false;
                    }

                    // altre info
                    this.lblIP.Text = this.IP;
                    this.lblVendor.Text = this.Vendor;

                    if (this.IP.Length > 0) {
                        Ping ping = new Ping();
                        PingReply reply = ping.Send(this.IP, 2000);

                        string fakeMonWS = ConfigurationManager.AppSettings["FakeMonitorWS.Service"];

                        if (string.IsNullOrEmpty(fakeMonWS)) {
                            #region Produzione o senza URL del web service FakeMon
                            if ((reply != null) && (reply.Status == IPStatus.Success)) {
                                #region Il dispositivo risponde al ping
                                this.Form.Attributes["OnLoad"] = "SetupWindow()";

                                this.IsScreenDumpLayout = true;

                                this.SetButtonsVisibility();
                                #endregion
                            }
                            else {
                                #region Dispositivo non raggiungibile
                                this.ClientScript.RegisterStartupScript(this.GetType(), "GrisError",
                                                                        "SetupWindow(); SetLayout(LAYOUT_TYPE_ERROR);",
                                                                        true);
                                this.lblError.Text =
                                    string.IsNullOrEmpty(ConfigurationManager.AppSettings["SDPingErrorMessage"])
                                        ? "Dispositivo Video Non Raggiungibile"
                                        : ConfigurationManager.AppSettings["SDPingErrorMessage"];

                                this.HideAllButtons();
                                #endregion
                            }
                            #endregion
                        }
                        else {
                            #region Sviluppo o preproduzione, il FakeMon URL � indicato
                            this.Form.Attributes["OnLoad"] = "SetupWindow()";

                            this.IsScreenDumpLayout = true;

                            this.SetButtonsVisibility();
                            #endregion
                        }
                    }
                    #endregion
                }
                else {
                    #region Errore recupero dati da base dati per il device - si nascondono tutte le informazioni e controlli
                    this.ClientScript.RegisterStartupScript(this.GetType(), "GrisError",
                                                            "SetupWindow(); SetLayout(LAYOUT_TYPE_ERROR);", true);
                    this.lblError.Text = string.IsNullOrEmpty(ConfigurationManager.AppSettings["SDDataErrorMessage"])
                                             ? "Impossibile ottenere le informazioni della periferica dalla base dati."
                                             : ConfigurationManager.AppSettings["SDDataErrorMessage"];

                    this.HideAllButtons();
                    #endregion
                }
            }
        }

        this.Title = string.Format("{0} - {1}", this.lblNodeName.Text, this.lblMonName.Text);
    }

    private void SetButtonsVisibility() {
        if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3)) {
            // la visualizzazione HTML � accessibile solo agli utenti di livello 3
            this.lnkShownData.Visible = false;
            this.lblSep2.Visible = false;
            // la stampa � sempre attiva per tutti
            this.lnkPrintPDF.Visible = true;
            this.lblSep5.Visible = true;
            this.lnkPrint.Visible = true;
            this.lblSep4.Visible = true;
            // la visualizzazione dell'immagine, anche adattata, � accessibile a tutti
            this.lnkScreenDump.Visible = true;
            this.lblSep3.Visible = true;
            this.lnkScreenDumpAuto.Visible = true;
            this.lblSep.Visible = true;
        }
        else {
            // la visualizzazione HTML � accessibile solo agli utenti di livello 3
            this.lnkShownData.Visible = true;
            this.lblSep2.Visible = true;
            // la stampa � sempre attiva per tutti
            this.lnkPrintPDF.Visible = true;
            this.lblSep5.Visible = true;
            this.lnkPrint.Visible = true;
            this.lblSep4.Visible = true;
            // la visualizzazione dell'immagine, anche adattata, � accessibile a tutti
            this.lnkScreenDump.Visible = true;
            this.lblSep3.Visible = true;
            this.lnkScreenDumpAuto.Visible = true;
            this.lblSep.Visible = true;
        }
    }

    private void HideAllButtons() {
        this.lnkPrintPDF.Visible = false;
        this.lblSep5.Visible = false;
        this.lnkPrint.Visible = false;
        this.lblSep4.Visible = false;
        this.lnkScreenDump.Visible = false;
        this.lblSep3.Visible = false;
        this.lnkScreenDumpAuto.Visible = false;
        this.lblSep.Visible = false;
        this.lnkShownData.Visible = false;
        this.lblSep2.Visible = false;
        this.lnkUpdateSD.Visible = false;
        this.lnkUpdateHTML.Visible = false;
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        this.ClientScript.RegisterHiddenField("AutoSize", (this.AutoSize ? "1" : "0"));
        this.ClientScript.RegisterHiddenField("IsRefreshing", (this.IsRefreshing ? "1" : "0"));
        this.SetLinkLayout(this.IsScreenDumpLayout);
        this.lblDataOra.Text = DateTime.Now.ToString();
    }

    # region Gestione switching tra i tabs

    protected void lnkScreenDump_Click(object sender, EventArgs e) {
        this.IsScreenDumpLayout = true;
        this.AutoSize = false;
        this.IsRefreshing = false;
    }

    protected void lnkScreenDumpAuto_Click(object sender, EventArgs e) {
        this.IsScreenDumpLayout = true;
        this.AutoSize = true;
        this.IsRefreshing = false;
    }

    protected void lnkShownData_Click(object sender, EventArgs e) {
        this.IsScreenDumpLayout = false;
        this.IsRefreshing = false;
    }

    protected void lnkUpdate_Click(object sender, EventArgs e) {
        this.Refresh();
    }

    private void Refresh() {
        this.IsRefreshing = true;
        if (this.IsScreenDumpLayout) {
            this.UpdateScreenDump();
        }
        else {
            this.UpdateShownData();
        }
    }

    protected void lnkPrint_Click(object sender, EventArgs e) {
        this.Refresh();
    }

    protected void lnkPrintPDF_Click(object sender, EventArgs e) {
        WebClient wc = new WebClient();
        wc.Credentials = CredentialCache.DefaultNetworkCredentials;
        byte[] imgScreenDumpData = null;

        try {
            imgScreenDumpData =
                wc.DownloadData(string.Format("{0}?ven={1}&uri={2}&ip={3}",
                                              ConfigurationManager.AppSettings["ScreenDumpHTTPHandlerUrl"], this.Vendor,
                                              this.Uri, this.IP));
        }
        catch (Exception ex) {
            this.HandleError(ex);
        }

        if (imgScreenDumpData != null) {
            MemoryStream ms = new MemoryStream(imgScreenDumpData);
            Image imageScreenDump = null;

            try {
                imageScreenDump = Image.FromStream(ms);
            }
            catch (ArgumentException ex) {
                this.HandleError(ex);
            }

            if (imageScreenDump != null) {
                try {
                    ScreenDumpPDFPrinter screenDumpPDFPrinter = new ScreenDumpPDFPrinter();
                    screenDumpPDFPrinter.Get(string.Format("ScreenDumpTeleindicatore-{0}.pdf", this.IP),
                                             string.Format("{0}", this.Title), HttpContext.Current.User.Identity.Name,
                                             string.Format("{0}, {5} {1}, {6} {2}, {7} {3}, {8} {4}", this.Title,
                                                           this.Vendor, this.IP, this.Rack, this.RackDescription,
                                                           LABEL_VENDOR, LABEL_IP, LABEL_WHERE, LABEL_NOTE),
                                             imageScreenDump, this.NodeName, this.Device, this.Vendor, this.IP,
                                             this.Rack, this.RackDescription, DateTime.Now.ToString(), LABEL_VENDOR,
                                             LABEL_IP, LABEL_WHERE, LABEL_DATE, LABEL_NOTE, LABEL_NODE, LABEL_DEVICE);
                }
                catch (Exception ex) {
                    this.HandleError(ex);
                }
            }
        }
    }

    # endregion

    protected void manScreenDump_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e) {
        this.HandleError(e.Exception);
    }

    private void HandleError(Exception e) {
        string message = "";
        string stackTrace = "";

        if (e != null) {
            if (e.InnerException != null) {
                message = e.InnerException.Message ?? string.Empty;
                stackTrace = e.InnerException.StackTrace ?? string.Empty;
            }
            else {
                message = e.Message ?? string.Empty;
                stackTrace = e.StackTrace ?? string.Empty;
            }
        }

        string errorMessage = string.Format("Error -> {0}, Stack Trace -> {1}", message, stackTrace);

#if (DEBUG)
        this.manScreenDump.AsyncPostBackErrorMessage = errorMessage;
#else
        if (e != null) {
            // Persiste l'eccezione serializzata in base dati
            if (!Tracer.TraceMessage(this.Context, e, true)) {
                // Logga l'eccezione via trace listener (su disco)
                System.Diagnostics.Trace.TraceError(string.Format("{0}, User -> {1}", errorMessage,
                                                                  GUtility.GetUserInfos(true)));
            }
        }

        this.manScreenDump.AsyncPostBackErrorMessage = "Errore nella comunicazione con il server.";
#endif
    }

    #endregion

    #region Methods

    private bool InitializeFieldsFromData(long devId) {
        DevicesTableAdapter ta = new DevicesTableAdapter();

        ScreenDumpDevicesDS.DevicesDataTable devices;
        try {
            devices = ta.GetDataByID(devId);
        }
        catch (Exception exc) {
            Tracer.TraceMessage(this.Context, exc);
            return false;
        }

        if (devices.Count > 0) {
            ScreenDumpDevicesDS.DevicesRow device = devices[0];

            this.Device = device.Name;
            this.IP = GUtility.GetStringIP(device.Address);
            this.Uri = this.Request.QueryString["uri"];
            this.RackDescription = device.RackDescription;
            this.Uri = string.Format(device.WSUrlPattern, this.IP);
            this.RegID = device.RegID;

            if (!device.IsTypeNull()) {
                this.Type = device.Type;
            }
            if (!device.IsVendorNameNull()) {
                this.Vendor = device.VendorName;
            }
            if (!device.IsBuildingNameNull()) {
                this.Building = device.BuildingName;
            }
            if (!device.IsRackNameNull()) {
                this.Rack = device.RackName;
            }
            if (!device.IsNodeNameNull()) {
                this.NodeName = device.NodeName;
            }

            return true;
        }

        return false;
    }

    private void SetPageLayout(bool isScreenDump) {
        this.updScreenDump.Visible = isScreenDump;
        this.updShownData.Visible = !isScreenDump;

        this.SetLinkLayout(isScreenDump);
    }

    private void SetLinkLayout(bool isScreenDump) {
        if (isScreenDump) {
            if (this.AutoSize) {
                this.lnkScreenDump.ForeColor = Color.White;
                this.lnkScreenDumpAuto.ForeColor = Color.Black;
            }
            else {
                this.lnkScreenDump.ForeColor = Color.Black;
                this.lnkScreenDumpAuto.ForeColor = Color.White;
            }
            this.lnkShownData.ForeColor = Color.White;
            this.lnkUpdateSD.Visible = true;
            this.lnkUpdateHTML.Visible = false;
        }
        else {
            this.lnkScreenDump.ForeColor = Color.White;
            this.lnkScreenDumpAuto.ForeColor = Color.White;
            this.lnkShownData.ForeColor = Color.Black;
            this.lnkUpdateSD.Visible = false;
            this.lnkUpdateHTML.Visible = true;
        }
    }

    private void UpdateScreenDump() {
        this.imgScreenDump.ImageUrl = string.Format("ScreenDump.ashx?ven={0}&uri={1}&ip={2}", this.Vendor, this.Uri,
                                                    this.IP);
        this.imgScreenDump.Attributes["OnLoad"] = "GotOutput()";
        this.imgScreenDump.Attributes["OnError"] = "ImageError()";
    }

    private void UpdateShownData() {
        this.ifrShownData.Attributes["Src"] = string.Format("ShownData.ashx?ven={0}&uri={1}&ip={2}", this.Vendor,
                                                            this.Uri, this.IP);
        this.ifrShownData.Attributes["OnLoad"] = "GotOutput()";
        this.ifrShownData.Attributes["OnError"] = "HtmlError()";
    }

    #endregion
}
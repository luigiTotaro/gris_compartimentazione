<%@ WebHandler Language="C#" Class="ShownData" %>

using System;
using System.Configuration;
using System.Web;
using System.Web.Services.Protocols;
using System.Reflection;

public class ShownData : IHttpHandler {
    HttpContext _context;

    public void ProcessRequest(HttpContext context) {
        this._context = context;

        try {
            string vendor = context.Request.QueryString["ven"];
            string uri = context.Request.QueryString["uri"];
            string ip = context.Request.QueryString["ip"];

            string fakeMonWS = ConfigurationManager.AppSettings["FakeMonitorWS.Service"];

            if (!string.IsNullOrEmpty(fakeMonWS)) {
                // Siamo in sviluppo o preproduzione, il FakeMon URL � indicato
                vendor = "FakeMon";
            }

#if (!DEBUG)
            if (!context.User.Identity.IsAuthenticated) {
                this.HandleException("Errore: un utente non autenticato non pu� visualizzare lo screen dump del monitor.");
            }
#endif

            if (!string.IsNullOrEmpty(vendor) && !string.IsNullOrEmpty(uri) && !string.IsNullOrEmpty(ip)) {
                object result = this.MonitorWSMethodCall(vendor, uri, ip, "GetShownData", null);

                if (result != null) {
                    string xmlShownData = result.ToString();

                    System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(
                        xmlShownData,
                        @"\<Dati(.*?)\>(?<html>.*?)\</Dati\>",
                        System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);

                    if (match.Value.Length == 0) {
                        this.HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'Dati' che contiene l'html da renderizzare.");
                        return;
                    }
                    if (!(match.Groups.Count == 3 && match.Groups["html"].Value.Length > 0)) {
                        this.HandleException("Errore: l'xml restituito dal web service non contiene informazioni nel nodo 'Dati' che contiene l'html da renderizzare.");
                        return;
                    }

                    string content = match.Groups["html"].Value.Replace("\n", "");

                    _context.Response.Write(HttpContext.Current.Server.HtmlDecode(content));
                    _context.Response.Write("<script type=\"text/javascript\">if (parent.ResizeIframe){parent.ResizeIframe(document.body.scrollWidth, document.body.scrollHeight);}</script>");
                    _context.Response.Flush();
                    _context.Response.End();
                }
                else {
                    this.HandleException("Errore: nessun testo html restituito dal web service.");
                }
            }
            else {
                this.HandleException("Errore: lo shown data handler non ha ricevuto i parametri corretti per il rendering dell' html.");
            }
        }
        catch (System.Threading.ThreadAbortException) {
        }
        catch (Exception exc) {
            this.HandleException("Errore di esecuzione durante la richiesta.");
            Tracer.TraceMessage(this._context, exc.Message + " Stack Trace: " + (exc.StackTrace ?? ""));
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

    private object MonitorWSMethodCall(string monitorType, string wsUri, string ip, string methodName, object[] parameters) {
        SoapHttpClientProtocol wsProxy;
        switch (monitorType) {
            case "Sysco": {
                    wsProxy = new SyscoMonitorWS.Ws();
                    break;
                }
            case "Aesys": {
                    wsProxy = new AesysMonitorWS.ICMonitor();
                    break;
                }
            case "Fida":                
            case "Solari": {
                    wsProxy = new SolariMonitorWS.ProxyDispositivoInfostazioni();
                    break;
                }
            default: {
                    wsProxy = new FakeMonitorWS.Service();
                    break;
                }
        }

        if (monitorType != "FakeMon") {
            wsProxy.Url = string.Format(wsUri, ip);
        }

        MethodInfo wsMethod = null;
        try {
            wsMethod = wsProxy.GetType().GetMethod(methodName);
        }
        catch (AmbiguousMatchException) {
            this.HandleException(
                "Errore di reflection durante la risoluzione del metodo del proxy al web service. Il nome potrebbe essere ambiguo.");
        }

        if (wsMethod != null) {
            try {
                return wsMethod.Invoke(wsProxy, parameters);
            }
            catch (TargetInvocationException) {
                this.HandleException("Errore: eccezione sollevata nel metodo del web service.");
            }
            catch (Exception) {
                this.HandleException("Errore generico durante l'invocazione del metodo del web service.");
            }
        }

        return null;
    }

    private void HandleException(string errorMessage) {
        this._context.Response.Write(errorMessage);
        this._context.Response.Flush();
        this._context.Response.End();
    }
}
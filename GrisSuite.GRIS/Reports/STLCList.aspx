<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true"
    CodeFile="STLCList.aspx.cs" Inherits="Reports_STLCList" Title="Elenco" CodeFileBaseClass="ReportPublisher"
    IsFilterSavingEnabled="True" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="Filters/RegionZoneNodeFilter.ascx" TagName="RegionZoneNodeFilter"
    TagPrefix="gc" %>
<%@ Register Src="Filters/OnlineFilter.ascx" TagName="OnlineFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/MaintenanceFilter.ascx" TagName="MaintenanceFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/ForExcelAndOptimizedFilter.ascx" TagName="ForExcelAndOptimizedFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/ServerVersionFilter.ascx" TagName="ServerVersionFilter"
    TagPrefix="gc" %>
<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" runat="Server">
    <table style="width: 100%; text-align: left;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <gc:RegionZoneNodeFilter ID="filterRegionZoneNode" runat="server" EnableSelectAllNode="true"
                    EnableSelectAllZone="true" EnableSelectAllRegion="true" LastTopograpyLevelFilter="Node" />
            </td>
        </tr>
        <tr>
            <td>
                <gc:OnlineFilter ID="filterOnline" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <gc:MaintenanceFilter ID="filterMaintenance" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <gc:ServerVersionFilter ID="filterServerVersion" runat="server" SelectionType="Multiple"
                    Height="100px" />
            </td>
        </tr>
        <tr>
            <td>
                <gc:ForExcelAndOptimizedFilter ID="filterForExcelAndOptimized" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" runat="Server">
    <asp:Panel ID="pnlResult" runat="server" Style="width: 95%; background: #ffffff;">
        <rsweb:ReportViewer ID="rptvSelectedReport" runat="server" Height="500px" Width="100%"
            AsyncRendering="false" ShowWaitControlCancelLink="false" PageCountMode="Actual" KeepSessionAlive="False">
            <LocalReport ReportPath="Reports\STLCList_flat.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource Name="ReportDS" DataSourceId="odsSTLCList" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </asp:Panel>
    <asp:Panel ID="pnlNoResult" runat="server" Visible="false">
        <asp:Label ID="lblNoResult" runat="server" Text="Nessun risultato soddisfa i filtri utilizzati."></asp:Label>
    </asp:Panel>
    <br />
    <asp:ObjectDataSource ID="odsSTLCList" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="GrisSuite.Data.Reports.STLCListTableAdapters.STLCListTableAdapter"
        OnSelecting="odsSTLCList_Selecting">
        <SelectParameters>
            <asp:Parameter Name="RegID" Type="Int64" />
            <asp:Parameter Name="ZonID" Type="Int64" />
            <asp:Parameter Name="NodID" Type="Int64" />
            <asp:Parameter Name="Online" Type="Int32" />
            <asp:Parameter Name="InMaintenance" Type="Int32" />
            <asp:Parameter Name="ServerVersionList" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

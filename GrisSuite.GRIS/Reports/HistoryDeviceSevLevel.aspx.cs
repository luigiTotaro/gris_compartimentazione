using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_HistoryDeviceSevLevel : ReportPublisher
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // inizializzazione filtri di pagina
            this.FilterControlIDs.Add(this.txtDeviceType.ID);
            this.FilterControlIDs.Add(this.chkOk.ID);
            this.FilterControlIDs.Add(this.chkWarning.ID);
            this.FilterControlIDs.Add(this.chkError.ID);
            this.FilterControlIDs.Add(this.chkNoActive.ID);
            this.FilterControlIDs.Add(this.chkUnknown.ID);
            this.FilterControlIDs.Add(this.chkLun.ID);
            this.FilterControlIDs.Add(this.chkMar.ID);
            this.FilterControlIDs.Add(this.chkMer.ID);
            this.FilterControlIDs.Add(this.chkGio.ID);
            this.FilterControlIDs.Add(this.chkVen.ID);
            this.FilterControlIDs.Add(this.chkSab.ID);
            this.FilterControlIDs.Add(this.chkDom.ID);
            this.FilterControlIDs.Add(this.txtDataInizio.ID);
            this.FilterControlIDs.Add(this.txtDataFine.ID);
            this.FilterControlIDs.Add(this.txtOraInizio.ID);
            this.FilterControlIDs.Add(this.txtOraFine.ID);
            this.FilterControlIDs.Add(this.txtDurataMin.ID);
            this.FilterControlIDs.Add(this.txtDurataMax.ID);
            this.FilterControlIDs.Add(this.cboDurataMin_udm.ID);
            this.FilterControlIDs.Add(this.cboDurataMax_udm.ID);
        }

        # region waiting animation

        this.rptvSelectedReport.FindControl("AsyncWait").Visible = false;

        if (this.rptvSelectedReport.FindControl("ctl10") is WebControl)
        {
            // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010
            ((WebControl) this.rptvSelectedReport.FindControl("ctl10")).Style["text-align"] = "left";
        }
        else
        {
            if (this.rptvSelectedReport.FindControl("ctl05") is WebControl)
            {
                // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010 SP 1 (modificato albero controlli)
                ((WebControl) this.rptvSelectedReport.FindControl("ctl05")).Style["text-align"] = "left";
            }
        }

        # endregion
    }

    protected void odsSelectedReport_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["Stazione"] = this.filterRegionZoneNode.SelectedValueNode > 0 ? this.filterRegionZoneNode.TextNode : string.Empty;
        e.InputParameters["Linea"] = this.filterRegionZoneNode.SelectedValueZone > 0 ? this.filterRegionZoneNode.TextZone : string.Empty; 
        e.InputParameters["Compartimento"] = this.filterRegionZoneNode.SelectedValueRegion > 0 ? this.filterRegionZoneNode.TextRegion : string.Empty; 
        e.InputParameters["ListaSevLevel"] = this.EncodeSevLevelList();
        e.InputParameters["TipoPeriferica"] = this.txtDeviceType.Text;
        e.InputParameters["DataInizio"] = ParseDateTime(this.txtDataInizio.Text);
        e.InputParameters["DataFine"] = ParseDateTime(this.txtDataFine.Text);
        e.InputParameters["OraInizio"] = ParseDateTime(txtOraInizio.Text);
        e.InputParameters["OraFine"] = ParseDateTime(txtOraFine.Text);
        e.InputParameters["DurataMin"] = GetDurataSeconds(this.txtDurataMin.Text, this.cboDurataMin_udm.Text);
        e.InputParameters["DurataMax"] = GetDurataSeconds(this.txtDurataMax.Text, this.cboDurataMax_udm.Text);
        e.InputParameters["GiorniDellaSettimana"] = this.EncodeWeekDayList();
        e.InputParameters["ListaSistemi"] = string.Format("|{0}|", string.Join("|", (from val in this.filterSystemList.SelectedValues select val).ToArray())).Replace("||", string.Empty);

    }

    protected void odsSelectedReportHeader_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["Stazione"] = this.filterRegionZoneNode.SelectedValueNode > 0 ? this.filterRegionZoneNode.TextNode : string.Empty;
        e.InputParameters["Linea"] = this.filterRegionZoneNode.SelectedValueZone > 0 ? this.filterRegionZoneNode.TextZone : string.Empty;
        e.InputParameters["Compartimento"] = this.filterRegionZoneNode.SelectedValueRegion > 0 ? this.filterRegionZoneNode.TextRegion : string.Empty;
        e.InputParameters["ListaSevLevel"] = this.EncodeSevLevelListText();
        e.InputParameters["TipoPeriferica"] = this.txtDeviceType.Text;
        e.InputParameters["DataInizio"] = ParseDateTime(this.txtDataInizio.Text);
        e.InputParameters["DataFine"] = ParseDateTime(this.txtDataFine.Text);
        e.InputParameters["OraInizio"] = ParseDateTime(txtOraInizio.Text);
        e.InputParameters["OraFine"] = ParseDateTime(txtOraFine.Text);
        e.InputParameters["DurataMin"] = ParseInt(this.txtDurataMin.Text);
        e.InputParameters["DurataMinUdm"] = this.cboDurataMin_udm.Text;
        e.InputParameters["DurataMax"] = ParseInt(this.txtDurataMax.Text);
        e.InputParameters["DurataMaxUdm"] = this.cboDurataMax_udm.Text;
        e.InputParameters["GiorniDellaSettimana"] = this.EncodeWeekDayListText();
        e.InputParameters["ListaSistemi"] = string.Join(",", filterSystemList.SelectedTexts);
    }

    private int ParseInt(string num)
    {
        int d;
        if (int.TryParse(num, out d))
            return d;
        return 0;
    }

    private int? GetDurataSeconds(string duration, string udm)
    {
        int d;
        if (int.TryParse(duration, out d))
        {
            int m = udm == "minuti" ? 60 : udm == "ore" ? 3600 : udm == "giorni" ? 86400 : 60;

            if (d * m == 0)
                return null;

            return d*m;
        }
        
        return null;
    }



    private DateTime? ParseDateTime(string date)
    {
        DateTime d;
        if (DateTime.TryParse(date, new CultureInfo("it-IT", false), DateTimeStyles.None, out d))
            return d;

        return null;
    }


    private string EncodeSevLevelList()
    {
        string s = "|";
        s += this.chkOk.Checked ? "0|" : "";
        s += this.chkWarning.Checked ? "1|" : "";
        s += this.chkError.Checked ? "2|" : "";
        s += this.chkNoActive.Checked ? "9|" : "";
        s += this.chkUnknown.Checked ? "255|" : "";
        if (s == "|")
        {
            s = null;
        }

        return s;
    }

    private string EncodeSevLevelListText()
    {
        string s = this.chkOk.Checked ? chkOk.Text + "," : "";
        s += this.chkWarning.Checked ? chkWarning.Text + "," : "";
        s += this.chkError.Checked ? chkError.Text + "," : "";
        s += this.chkNoActive.Checked ? chkNoActive.Text + "," : "";
        s += this.chkUnknown.Checked ? this.chkUnknown.Text + "," : "";
        if (s.EndsWith(","))
        {
            s = s.Substring(0, s.Length - 1);
        }

        return s;
    }

    private string EncodeWeekDayList()
    {
        string s = "|";
        s += this.chkLun.Checked ? "1|" : "";
        s += this.chkMar.Checked ? "2|" : "";
        s += this.chkMer.Checked ? "3|" : "";
        s += this.chkGio.Checked ? "4|" : "";
        s += this.chkVen.Checked ? "5|" : "";
        s += this.chkSab.Checked ? "6|" : "";
        s += this.chkDom.Checked ? "7|" : "";
        if (s == "|")
        {
            s = null;
        }

        return s;
    }

    private string EncodeWeekDayListText()
    {
        string s = this.chkLun.Checked ? this.chkLun.Text + "," : "";
        s += this.chkMar.Checked ? this.chkMar.Text + "," : "";
        s += this.chkMer.Checked ? this.chkMer.Text + "," : "";
        s += this.chkGio.Checked ? this.chkGio.Text + "," : "";
        s += this.chkVen.Checked ? this.chkVen.Text + "," : "";
        s += this.chkSab.Checked ? this.chkSab.Text + "," : "";
        s += this.chkDom.Checked ? this.chkDom.Text + "," : "";
        if (s.EndsWith(","))
        {
            s = s.Substring(0, s.Length - 1);
        }

        return s;
    }


    #region ReportPublisher Members

    public override ReportViewer ReportViewer
    {
        get { return this.rptvSelectedReport; }
    }

    #endregion

    public override void SavePageFilters()
    {
        base.SavePageFilters();
        this.filterRegionZoneNode.SavePageFilters();
    }

    public override void OnInvokeReportGeneration()
    {
        // assegnati da codice per impedire che la prima visualizzazione della pagina senza filtri impostati generi una chiamata alla base dati
        if (rptvSelectedReport.LocalReport.DataSources.Count == 0)
        {
            rptvSelectedReport.LocalReport.DataSources.Add(new ReportDataSource() { DataSourceId = "odsSelectedReport", Name = "ReportDS" });
            rptvSelectedReport.LocalReport.DataSources.Add(new ReportDataSource()
            {
                DataSourceId = "odsSelectedReportHeader",
                Name = "ReportHeaderDS"
            });
        }
    }

}
using System;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_FieldsByZoneSevLevelType : ReportPublisher
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // inizializzazione filtri di pagina
            this.FilterControlIDs.Add(this.txtDeviceType.ID);
            this.FilterControlIDs.Add(this.txtMessage.ID);
            this.FilterControlIDs.Add(this.chkOk.ID);
            this.FilterControlIDs.Add(this.chkWarning.ID);
            this.FilterControlIDs.Add(this.chkError.ID);
            this.FilterControlIDs.Add(this.chkIncludeInMaintenance.ID);
            this.FilterControlIDs.Add(this.chkInfo.ID);
        }

        # region waiting animation

        this.rptvSelectedReport.FindControl("AsyncWait").Visible = false;

        if (this.rptvSelectedReport.FindControl("ctl10") is WebControl)
        {
            // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010
            ((WebControl) this.rptvSelectedReport.FindControl("ctl10")).Style["text-align"] = "left";
        }
        else
        {
            if (this.rptvSelectedReport.FindControl("ctl05") is WebControl)
            {
                // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010 SP 1 (modificato albero controlli)
                ((WebControl) this.rptvSelectedReport.FindControl("ctl05")).Style["text-align"] = "left";
            }
        }

        # endregion
    }

    protected void odsSelectedReport_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["NodID"] = this.filterRegionZoneNode.SelectedValueNode;
        e.InputParameters["ZonID"] = this.filterRegionZoneNode.SelectedValueZone;
        e.InputParameters["RegID"] = this.filterRegionZoneNode.SelectedValueRegion;
        e.InputParameters["SevLevelList"] = this.EncodeSevLevelList();
        e.InputParameters["DeviceType"] = this.txtDeviceType.Text;
        e.InputParameters["Message"] = this.txtMessage.Text;
        e.InputParameters["IncludeInMaintenance"] = this.chkIncludeInMaintenance.Checked;
    }

    private string EncodeSevLevelList()
    {
        string s = "|";

        if (this.chkError.Checked)
        {
            s += "2|";
        }
        if (this.chkWarning.Checked)
        {
            s += "1|";
        }
        if (this.chkOk.Checked)
        {
            s += "0|";
        }
        if (this.chkInfo.Checked)
        {
            s += "-255|";
        }

        if (s == "|")
        {
            s = string.Empty;
        }

        return s;
    }

    #region ReportPublisher Members

    public override ReportViewer ReportViewer
    {
        get { return this.rptvSelectedReport; }
    }

    #endregion

    public override void SavePageFilters()
    {
        base.SavePageFilters();
        this.filterRegionZoneNode.SavePageFilters();
    }
}
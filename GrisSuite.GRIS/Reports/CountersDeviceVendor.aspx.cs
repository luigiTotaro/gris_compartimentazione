
using System;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Reports_CountersDeviceVendor : ReportPublisher
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		# region waiting animation
		this.rptvSelectedReport.FindControl("AsyncWait").Visible = false;

        if (this.rptvSelectedReport.FindControl("ctl10") is WebControl)
        {
            // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010
            ((WebControl)this.rptvSelectedReport.FindControl("ctl10")).Style["text-align"] = "left";
        }
        else
        {
            if (this.rptvSelectedReport.FindControl("ctl05") is WebControl)
            {
                // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010 SP 1 (modificato albero controlli)
                ((WebControl)this.rptvSelectedReport.FindControl("ctl05")).Style["text-align"] = "left";
            }
        }

		# endregion

		if ( !this.IsPostBack )
		{
			// questa gestione della memorizzzione dei filtri fa eccezione rispetto alle altre
			// la riassegnazione dei valori alla proprietÓ di default viene delegata allo user control contenitore.
			this.FilterControlIDs.Add(this.filterSystemList.ID);
		}
	}

    protected void odsSelectedReport_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
    {
		e.InputParameters["Systems"] = string.Format("|{0}|", string.Join("|", ( from val in this.filterSystemList.SelectedValues select val.ToString() ).ToArray()));
    }

	#region ReportPublisher Members

	public override Microsoft.Reporting.WebForms.ReportViewer ReportViewer
	{
		get { return this.rptvSelectedReport; }
	}

	#endregion
}

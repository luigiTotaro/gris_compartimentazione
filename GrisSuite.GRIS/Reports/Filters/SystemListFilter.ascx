﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SystemListFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_SystemListFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Sistema</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%;">
            <asp:Panel ID="pnlSingleSelect" runat="server">
                <asp:DropDownList ID="ddlSystems" runat="server" DataSourceID="sqlSistemi" DataTextField="SystemName"
                    DataValueField="SystemID" AutoPostBack="false" Width="100%" OnDataBound="ddlSistema_DataBound">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="pnlMultiSelect" runat="server">
                <div style="float: left; overflow: auto; width: 80%; height: 100px; background-color: White;
                    color: Black;">
                    <asp:CheckBoxList ID="chklSystems" runat="server" DataSourceID="sqlSistemi" DataTextField="SystemName"
                        DataValueField="SystemID" AutoPostBack="False">
                    </asp:CheckBoxList>
                </div>
            </asp:Panel>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlSistemi" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT SystemID, SystemDescription as SystemName&#13;&#10;FROM systems&#13;&#10;ORDER BY SystemName">
</asp:SqlDataSource>

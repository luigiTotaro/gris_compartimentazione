﻿using System;
using System.Web.UI.WebControls;
using GrisSuite.Common;

public partial class Reports_Filters_RegionZoneNodeFilter : FiltersSavingControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if ( !this.IsPostBack )
		{
			switch ( this.LastTopograpyLevelFilter )
			{
				case LastTopograpyLevelFilter.Region:
				{
					this.trZone.Visible = false;
					this.trNode.Visible = false;
					
					// inizializzazione filtri di pagina
					this.FilterControlIDs.Add(this.ddlRegion.ID);
					
					break;
				}
				case LastTopograpyLevelFilter.Zone:
				{
					this.trNode.Visible = false;
					
					// inizializzazione filtri di pagina
					this.FilterControlIDs.Add(this.ddlRegion.ID);
					this.FilterControlIDs.Add(this.ddlZone.ID);
					
					break;
				}
				default:
				{
					// inizializzazione filtri di pagina
					this.FilterControlIDs.Add(this.ddlRegion.ID);
					this.FilterControlIDs.Add(this.ddlZone.ID);
					this.FilterControlIDs.Add(this.ddlNode.ID);
					
					break;    				
				}
			}
		}
	}

	public bool EnableSelectAllZone { get; set; }
	public bool EnableSelectAllNode { get; set; }
	public bool EnableSelectAllRegion { get; set; }
	public LastTopograpyLevelFilter LastTopograpyLevelFilter { get; set; }

	public long SelectedValueNode
	{
		get { return string.IsNullOrEmpty(ddlNode.SelectedValue) ? -1 : Convert.ToInt64(ddlNode.SelectedValue); }
		set { ddlNode.SelectedValue = value.ToString(); }
	}

	public long SelectedValueZone
	{
		get { return string.IsNullOrEmpty(ddlZone.SelectedValue) ? -1 : Convert.ToInt64(ddlZone.SelectedValue); }
		set { ddlZone.SelectedValue = value.ToString(); }
	}

	public long SelectedValueRegion
	{
		get { return string.IsNullOrEmpty(ddlRegion.SelectedValue) ? -1 : Convert.ToInt64(ddlRegion.SelectedValue); }
		set { ddlRegion.SelectedValue = value.ToString(); }
	}

    public string TextNode
    {
        get { return ddlNode.SelectedItem == null ? string.Empty : ddlNode.SelectedItem.Text; }
    }

    public string TextZone
    {
        get { return ddlZone.SelectedItem == null ? string.Empty : ddlZone.SelectedItem.Text; }
    }

    public string TextRegion
    {
        get { return ddlRegion.SelectedItem == null ? string.Empty : ddlRegion.SelectedItem.Text; }
    }
	
	protected void ddlNode_DataBound(object sender, EventArgs e)
	{
		if (EnableSelectAllNode)
			this.ddlNode.Items.Insert(0, new ListItem(" - Tutte le stazioni - ", "-1"));
	}

	protected void ddlZone_DataBound(object sender, EventArgs e)
	{
		if (EnableSelectAllZone)
			this.ddlZone.Items.Insert(0, new ListItem(" - Tutte le linee - ", "-1"));

		if ( this.IsPostBack ) this.ddlNode.DataBind();
	}

	protected void ddlRegion_DataBound(object sender, EventArgs e)
	{
		if ( GrisPermissions.IsUserAtLeastInLevelForAllRegions(PermissionLevel.Level3) )
		{
			if (EnableSelectAllRegion)
				this.ddlRegion.Items.Insert(0, new ListItem(" - Tutti i compartimenti - ", "-1"));

			if (!this.IsPostBack) this.ddlRegion.SelectedValue = "-1";
		}
		else
		{
			for ( byte index = 1; index < 16; index++ )
			{
				if ( !GrisPermissions.IsInRole((RegCodes)index) )
				{
					long regId = (long) Utility.EncodeRegionID(index);
					this.ddlRegion.Items.FindByValue(regId.ToString()).Enabled = false;
				}
			}

			//il framework tiene come elemento selezionato il primo della lista anche se non è abilitato, devo forzare a mano la selezione corretta
			foreach (ListItem item in ddlRegion.Items)
			{
				if (item.Enabled)
				{
					item.Selected = true;
					break;
				}
			}
		}

	}
}

public enum LastTopograpyLevelFilter
{
	Region, Zone, Node
}

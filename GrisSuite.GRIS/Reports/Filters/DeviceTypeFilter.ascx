﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceTypeFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_DeviceTypeFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Tipo</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%; color: Black; font-size: 10pt;">
            <asp:Panel ID="pnlSingleSelect" runat="server">
                <asp:DropDownList ID="ddlTipo" runat="server" DataSourceID="sqlTipi" DataTextField="DeviceType"
                    DataValueField="DeviceTypeID" AutoPostBack="False" Width="100%" OnDataBound="Tipo_DataBound">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="pnlMultiSelect" runat="server" BackColor="White" Width="100%">
                <asp:CheckBoxList ID="chklTipo" runat="server" DataSourceID="sqlTipi" DataTextField="DeviceType"
                    DataValueField="DeviceTypeID" AutoPostBack="False" Width="100%" OnDataBound="Tipo_DataBound">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlTipi" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT DeviceTypeID, DeviceTypeID + ISNULL(' (' + DeviceTypeDescription + ')', '') AS DeviceType FROM device_type ORDER BY DeviceTypeID">
</asp:SqlDataSource>

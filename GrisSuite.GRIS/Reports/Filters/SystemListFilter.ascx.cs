﻿
using System;
using System.Web.UI.WebControls;

public partial class Reports_Filters_SystemListFilter : MultiSelectionControl
{
	public bool EnableSelectAll { get; set; }

	protected override void OnInit ( EventArgs e )
	{
		base.OnInit(e);
		this.InnerControl = ( this.SelectionType == SelectionType.Single ) ? this.ddlSystems : (ListControl)this.chklSystems;
	}

	protected void Page_Load ( object sender, EventArgs e )
	{
		// inizializzazione filtri di pagina
		if ( !this.IsPostBack )
		{
			switch ( SelectionType )
			{
				case SelectionType.Single:
					{
						this.FilterControlIDs.Add(this.ddlSystems.ID);

						this.pnlSingleSelect.Visible = true;
						this.pnlMultiSelect.Visible = false;

						break;
					}
				case SelectionType.Multiple:
					{
						this.FilterControlIDs.Add(this.chklSystems.ID);

						this.pnlSingleSelect.Visible = false;
						this.pnlMultiSelect.Visible = true;

						break;
					}
			}
		}
	}

	protected void ddlSistema_DataBound ( object sender, EventArgs e )
	{
		if ( this.SelectionType == SelectionType.Single && this.EnableSelectAll && ( this.ddlSystems.Items != null ) && ( this.ddlSystems.Items.FindByValue("-2") == null ) )
		{
			this.ddlSystems.Items.Insert(0, new ListItem(" - Tutti i sistemi - ", "-1"));
			this.ddlSystems.SelectedIndex = 0;
		}
	}
}

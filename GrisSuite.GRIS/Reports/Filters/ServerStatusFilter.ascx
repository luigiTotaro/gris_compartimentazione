﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServerStatusFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_ServerStatusFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Stato STLC1000</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%; color: Black; font-size:10pt;">
            <asp:Panel ID="pnlSingleSelect" runat="server">
                <asp:DropDownList ID="ddlServerStato" runat="server" DataSourceID="sqlStatiServer"
                    DataTextField="Description" DataValueField="SevLevel" AutoPostBack="false" Width="100%"
                    OnDataBound="StatiServer_DataBound">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="pnlMultiSelect" Width="100%" runat="server" BackColor="White">
                <asp:CheckBoxList ID="chklServerStatus" runat="server" DataSourceID="sqlStatiServer"
                    DataTextField="Description" DataValueField="SevLevel" AutoPostBack="false" Width="100%"
                    OnDataBound="StatiServer_DataBound">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlStatiServer" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT SevLevel, Description FROM severity WHERE (IsRelevantToServer = 1) ORDER BY SevLevel">
</asp:SqlDataSource>

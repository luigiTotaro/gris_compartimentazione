<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForExcelAndOptimizedFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_ForExcelAndOptimizedFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            &nbsp;
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%;">
            <asp:CheckBox runat="server" ID="chkFlat" Text="Report in formato Excel" Checked="true" Font-Size="10pt" /><br />
            <asp:CheckBox runat="server" ID="chkOptimized" Text="Ottimizza per esportazione"
                Checked="false" Font-Size="10pt" Style="padding-left: 18px;" />
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>

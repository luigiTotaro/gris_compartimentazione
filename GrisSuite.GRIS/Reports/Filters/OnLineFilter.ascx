﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OnLineFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_OnlineFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Connessione</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%;">
            <asp:DropDownList ID="ddlOnLine" runat="server" Width="100%">
                <asp:ListItem Value="-1" Text="In linea e non in linea" Selected="True"></asp:ListItem>
                <asp:ListItem Value="1" Text="In linea"></asp:ListItem>
                <asp:ListItem Value="0" Text="Non in linea"></asp:ListItem>
            </asp:DropDownList>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>

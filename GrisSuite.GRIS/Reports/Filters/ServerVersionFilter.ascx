﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServerVersionFilter.ascx.cs" 
	CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_ServerVersionFilter" IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Versione STLC1000</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%; color: Black; font-size:10pt;">
            <asp:Panel ID="pnlSingleSelect" runat="server">
                <asp:DropDownList ID="ddlServerVersion" runat="server" DataSourceID="sqlServerVersions" DataTextField="ParameterValue"
                    DataValueField="ParameterValue" AutoPostBack="false" Width="100%" OnDataBound="ServerVersion_DataBound">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="pnlMultiSelect" width="100%" runat="server" backcolor="White">
                <asp:CheckBoxList ID="chklServerVersion" runat="server" DataSourceID="sqlServerVersions" DataTextField="ParameterValue"
                    DataValueField="ParameterValue" AutoPostBack="false" OnDataBound="ServerVersion_DataBound">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlServerVersions" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="select distinct ParameterValue from stlc_parameters where ParameterName = 'STLCVersion' order by ParameterValue">
</asp:SqlDataSource>
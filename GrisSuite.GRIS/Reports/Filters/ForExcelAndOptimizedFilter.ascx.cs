using System;

public partial class Reports_Filters_ForExcelAndOptimizedFilter : FiltersSavingControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // inizializzazione filtri di pagina
        if (!this.IsPostBack)
        {
            this.FilterControlIDs.Add(this.chkFlat.ID);
            this.FilterControlIDs.Add(this.chkOptimized.ID);

            this.chkFlat.Attributes["onclick"] =
                "if(!this.checked){document.getElementById('" + this.chkOptimized.ClientID + "').checked=false;}";

            this.chkOptimized.Attributes["onclick"] =
                "if(this.checked){document.getElementById('" + this.chkFlat.ClientID + "').checked=true;}";
        }
    }

    public bool CheckedFlat
    {
        get
        {
            return this.chkFlat.Checked;
        }
        set
        {
            this.chkFlat.Checked = value;
        }
    }

    public bool SelectedValueFlat
    {
        get
        {
            return this.chkFlat.Checked;
        }
        set
        {
            this.chkFlat.Checked = value;
        }
    }

    public bool CheckedOptimized
    {
        get
        {
            return this.chkOptimized.Checked;
        }
        set
        {
            this.chkOptimized.Checked = value;
        }
    }

    public bool SelectedValueOptimized
    {
        get
        {
            return this.chkOptimized.Checked;
        }
        set
        {
            this.chkOptimized.Checked = value;
        }
    }
}
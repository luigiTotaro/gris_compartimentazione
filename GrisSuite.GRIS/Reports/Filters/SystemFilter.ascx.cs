﻿using System;
using System.Web.UI.WebControls;

public partial class Reports_Filters_SystemFilter : FiltersSavingControl
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		// inizializzazione filtri di pagina
		if ( !this.IsPostBack ) this.FilterControlIDs.Add(this.ddlSistema.ID);
	}

	public bool EnableSelectAll { get; set; }

	public int SelectedValue
	{
		get { return Convert.ToInt32(this.ddlSistema.SelectedValue); }
		set { this.ddlSistema.SelectedValue = value.ToString(); }
	}

	protected void ddlSistema_DataBound ( object sender, EventArgs e )
	{
		if ( EnableSelectAll )
			this.ddlSistema.Items.Insert(0, new ListItem(" - Tutti i sistemi - ", "-1"));
	}
}

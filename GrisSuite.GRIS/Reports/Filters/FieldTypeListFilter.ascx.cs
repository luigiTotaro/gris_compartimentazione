﻿
using System;
using System.Web.UI.WebControls;

public partial class Reports_Filters_FieldTypeListFilter : MultiSelectionControl
{
	public bool EnableSelectAll { get; set; }

	protected override void OnInit ( EventArgs e )
	{
		base.OnInit(e);
		this.InnerControl = ( this.SelectionType == SelectionType.Single ) ? this.ddl : (ListControl)this.chkList;
	}

	protected void Page_Load ( object sender, EventArgs e )
	{
		// inizializzazione filtri di pagina
		if ( !this.IsPostBack )
		{
			switch ( SelectionType )
			{
				case SelectionType.Single:
					{
						this.FilterControlIDs.Add(this.ddl.ID);

						this.pnlSingleSelect.Visible = true;
						this.pnlMultiSelect.Visible = false;

						break;
					}
				case SelectionType.Multiple:
					{
						this.FilterControlIDs.Add(this.chkList.ID);

						this.pnlSingleSelect.Visible = false;
						this.pnlMultiSelect.Visible = true;

						break;
					}
			}
		}
	}

	protected void ddl_DataBound ( object sender, EventArgs e )
	{
		if ( this.SelectionType == SelectionType.Single && this.EnableSelectAll && ( this.ddl.Items.FindByValue("-1") == null ) )
		{
			this.ddl.Items.Insert(0, new ListItem(" - Tutti i valori - ", "-1"));
			this.ddl.SelectedIndex = 0;
		}
	}
}

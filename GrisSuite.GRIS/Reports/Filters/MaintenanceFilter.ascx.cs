﻿using System;

public partial class Reports_Filters_MaintenanceFilter : FiltersSavingControl
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		// inizializzazione filtri di pagina
        if (!this.IsPostBack) this.FilterControlIDs.Add(this.ddlInMaintenance.ID);
	}

	public bool EnableSelectAll { get; set; }

	public int SelectedValue
	{
		get { return Convert.ToInt32(this.ddlInMaintenance.SelectedValue); }
		set { this.ddlInMaintenance.SelectedValue = value.ToString(); }
	}

	public string Label
	{
		get { return this.lbl.Text; }
		set { if ( !string.IsNullOrEmpty(value) ) this.lbl.Text = value; }
	}
}

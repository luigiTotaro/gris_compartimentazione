﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegionZoneNodeFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_RegionZoneNodeFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr id="trRegion" runat="server">
        <td style="width: 3%;">&nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lblRegion" runat="server">Compartimento</asp:Label>
        </td>
        <td style="width: 4%;">
        </td>
        <td style="width: 70%;">
            <asp:DropDownList ID="ddlRegion" runat="server" DataSourceID="sqlRegion" DataTextField="RegionName"
                DataValueField="RegID" AutoPostBack="True" Width="100%" OnDataBound="ddlRegion_DataBound">
            </asp:DropDownList>
        </td>
        <td style="width: 3%;">&nbsp;
        </td>
    </tr>
    <tr id="trZone" runat="server">
        <td>&nbsp;
        </td>
        <td>
            <asp:Label ID="Zone" runat="server">Linea</asp:Label>
        </td>
        <td>&nbsp;
        </td>
        <td>
            <asp:DropDownList ID="ddlZone" runat="server" DataSourceID="sqlZone" DataTextField="LineaName"
                DataValueField="ZonID" AutoPostBack="True" OnDataBound="ddlZone_DataBound" Width="100%">
            </asp:DropDownList>
        </td>
        <td>&nbsp;
        </td>
    </tr>
    <tr id="trNode" runat="server">
        <td>&nbsp;
        </td>
        <td>
            <asp:Label ID="lblNode" runat="server">Stazione</asp:Label>
        </td>
        <td>&nbsp;
        </td>
        <td>
            <asp:DropDownList ID="ddlNode" runat="server" DataSourceID="sqlNode" DataTextField="NodeName"
                DataValueField="NodID" Width="100%" OnDataBound="ddlNode_DataBound">
            </asp:DropDownList>
        </td>
        <td>&nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlRegion" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT RegID, Name as RegionName&#13;&#10;FROM regions&#13;&#10;ORDER BY Name">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqlZone" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT ZonID, Name as LineaName&#13;&#10;FROM zones&#13;&#10;WHERE (RegID = @RegID) OR (@RegID = -1)&#13;&#10;ORDER BY Name">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlRegion" Name="RegID" PropertyName="SelectedValue"
            Type="Int64" DefaultValue="-1" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqlNode" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT NodID, nodes.Name as NodeName&#13;&#10;FROM nodes INNER JOIN zones ON zones.ZonID = nodes.ZonID&#13;&#10;WHERE (nodes.ZonID = @ZonID) OR ((@ZonID = -1) AND ((zones.RegID = @RegID) OR (@RegID = -1)))&#13;&#10;ORDER BY NodeName">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlRegion" Name="RegID" PropertyName="SelectedValue"
            Type="Int64" DefaultValue="-1" />
        <asp:ControlParameter ControlID="ddlZone" Name="ZonID" PropertyName="SelectedValue"
            Type="Int64" DefaultValue="-1" />
    </SelectParameters>
</asp:SqlDataSource>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForExcelFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_ForExcelFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            &nbsp;
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%;">
            <asp:CheckBox runat="server" ID="chkFlat" Text="Report ottimizzato per esportazione Excel."
                Checked="true" />
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>

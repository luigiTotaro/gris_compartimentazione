﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Filters_ServerStatusFilter : MultiSelectionControl
{
    public Unit Width
    {
        get { return this.pnlMultiSelect.Width; }
        set { this.pnlMultiSelect.Width = value; }
    }

    public Unit Height
    {
        get { return this.pnlMultiSelect.Height; }
        set { this.pnlMultiSelect.Height = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.InnerControl = (this.SelectionType == SelectionType.Single) ? this.ddlServerStato : (ListControl) this.chklServerStatus;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.pnlMultiSelect.Style.Add(HtmlTextWriterStyle.OverflowY, "auto");
            // inizializzazione filtri di pagina
            switch (this.SelectionType)
            {
                case SelectionType.Single:
                    {
                        this.FilterControlIDs.Add(this.ddlServerStato.ID);

                        this.pnlSingleSelect.Visible = true;
                        this.pnlMultiSelect.Visible = false;

                        break;
                    }
                case SelectionType.Multiple:
                    {
                        this.FilterControlIDs.Add(this.chklServerStatus.ID);

                        this.pnlSingleSelect.Visible = false;
                        this.pnlMultiSelect.Visible = true;

                        break;
                    }
            }
        }
    }

    protected void StatiServer_DataBound(object sender, EventArgs e)
    {
        if (this.InnerControl.Items.FindByValue("-2") == null)
        {
            ListControl listControl = this.InnerControl;

            listControl.Items.Insert(0, new ListItem(" - Tutti gli stati - ", "-2"));
            listControl.SelectedValue = "-2";
        }
    }
}
﻿using System;

public partial class Reports_Filters_ForExcelFilter : FiltersSavingControl
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		// inizializzazione filtri di pagina
        if (!this.IsPostBack) this.FilterControlIDs.Add(this.chkFlat.ID);
	}

    public bool Checked
    {
        get { return this.chkFlat.Checked; }
        set { this.chkFlat.Checked = value; }
    }

	public bool SelectedValue
	{
        get { return this.chkFlat.Checked; }
        set { this.chkFlat.Checked = value; }
	}
}

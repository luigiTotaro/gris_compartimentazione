﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Filters_DeviceTypeFilter : MultiSelectionControl
{
    public Unit Width
    {
        get { return this.pnlMultiSelect.Width; }
        set { this.pnlMultiSelect.Width = value; }
    }

    public Unit Height
    {
        get { return this.pnlMultiSelect.Height; }
        set { this.pnlMultiSelect.Height = value; }
    }

    public bool EnableSelectAll { get; set; }
    public bool InfoStazTypeVisible { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.InnerControl = (this.SelectionType == SelectionType.Single) ? this.ddlTipo : (ListControl)this.chklTipo;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // inizializzazione filtri di pagina
        if (!this.IsPostBack)
        {
            this.pnlMultiSelect.Style.Add(HtmlTextWriterStyle.OverflowY, "auto");

            switch (this.SelectionType)
            {
                case SelectionType.Single:
                {
                    this.FilterControlIDs.Add(this.ddlTipo.ID);

                    this.pnlSingleSelect.Visible = true;
                    this.pnlMultiSelect.Visible = false;

                    break;
                }
                case SelectionType.Multiple:
                {
                    this.FilterControlIDs.Add(this.chklTipo.ID);

                    this.pnlSingleSelect.Visible = false;
                    this.pnlMultiSelect.Visible = true;

                    break;
                }
            }
        }
    }

    protected void Tipo_DataBound(object sender, EventArgs e)
    {
        ListControl listControl = this.InnerControl;

        if (this.EnableSelectAll)
        {
            if (this.InnerControl.Items.FindByValue("-2") == null)
            {
                listControl.Items.Insert(0, new ListItem(" - Tutti i tipi - ", "-2"));
                listControl.SelectedValue = "-2";
            }
        }

        if (!this.InfoStazTypeVisible)
        {
            var infostazListItem = (from ListItem infoLI in listControl.Items.Cast<ListItem>()
                                    where infoLI.Value == "INFSTAZ1"
                                    select infoLI).FirstOrDefault();

            if (infostazListItem != null)
            {
                this.ddlTipo.Items.Remove(infostazListItem);
            }
        }
    }
}
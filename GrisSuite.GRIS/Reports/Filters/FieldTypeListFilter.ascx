﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FieldTypeListFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_FieldTypeListFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Tipo valore</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%;">
            <asp:Panel ID="pnlSingleSelect" runat="server">
                <asp:DropDownList ID="ddl" runat="server" DataSourceID="sqlSource" DataTextField="Description"
                    DataValueField="FieldTypeId" AutoPostBack="false" Width="100%" OnDataBound="ddl_DataBound">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="pnlMultiSelect" runat="server">
                <div style="float: left; overflow: auto; width: 80%; height: 100px; background-color: White;
                    color: Black;">
                    <asp:CheckBoxList ID="chkList" runat="server" DataSourceID="sqlSource" DataTextField="Description"
                        DataValueField="FieldTypeId" AutoPostBack="False">
                    </asp:CheckBoxList>
                </div>
            </asp:Panel>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlSource" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    SelectCommand="gris_GetStreamFieldsTypeForHistoryReport" 
    SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

﻿using System;
using System.Web.UI.WebControls;

public partial class Reports_Filters_OnlineFilter : FiltersSavingControl
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		// inizializzazione filtri di pagina
        if (!this.IsPostBack) this.FilterControlIDs.Add(this.ddlOnLine.ID);
	}

	public bool EnableSelectAll { get; set; }

	public int SelectedValue
	{
        get { return Convert.ToInt32(this.ddlOnLine.SelectedValue); }
        set { this.ddlOnLine.SelectedValue = value.ToString(); }
	}
}

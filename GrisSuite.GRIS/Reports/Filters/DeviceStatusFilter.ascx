﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceStatusFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_DeviceStatusFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Stato</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%; color: Black; font-size: 10pt;">
            <asp:Panel ID="pnlSingleSelect" runat="server">
                <asp:DropDownList ID="ddlStato" runat="server" DataSourceID="sqlStati" DataTextField="Description"
                    DataValueField="SevLevel" AutoPostBack="false" Width="100%" OnDataBound="Stato_DataBound">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="pnlMultiSelect" runat="server" BackColor="White" Width="100%">
                <asp:CheckBoxList ID="chklStato" runat="server" DataSourceID="sqlStati" DataTextField="Description"
                    DataValueField="SevLevel" AutoPostBack="false" Width="100%" OnDataBound="Stato_DataBound">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqlStati" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    ProviderName="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString.ProviderName %>"
    SelectCommand="SELECT SevLevel, Description FROM severity WHERE (IsRelevantToDevice = 1) ORDER BY SevLevel">
</asp:SqlDataSource>

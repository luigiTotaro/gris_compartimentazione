﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MaintenanceFilter.ascx.cs"
    CodeFileBaseClass="FiltersSavingControl" Inherits="Reports_Filters_MaintenanceFilter"
    IsFilterSavingEnabled="True" %>
<table style="width: 100%; text-align: left;">
    <tr>
        <td style="width: 3%;">
            &nbsp;
        </td>
        <td style="width: 20%;">
            <asp:Label ID="lbl" runat="server">Stato attivazione</asp:Label>
        </td>
        <td style="width: 4%;">
            &nbsp;
        </td>
        <td style="width: 70%;">
            <asp:DropDownList ID="ddlInMaintenance" runat="server" Width="100%">
                <asp:ListItem Value="-1" Text="Attivo e non attivo" Selected="True"></asp:ListItem>
                <asp:ListItem Value="0" Text="Attivo"></asp:ListItem>
                <asp:ListItem Value="1" Text="Non attivo"></asp:ListItem>
            </asp:DropDownList>
        </td>
        <td style="width: 3%;">
            &nbsp;
        </td>
    </tr>
</table>

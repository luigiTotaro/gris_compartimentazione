<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true"
    CodeFile="HistoryStreamFieldsValue.aspx.cs" Inherits="Reports_HistoryStreamFieldsValue"
    Title="Storico valori" CodeFileBaseClass="ReportPublisher" IsFilterSavingEnabled="True" %>

<%@ Register Src="Filters/RegionZoneNodeFilter.ascx" TagName="RegionZoneNodeFilter"
    TagPrefix="gc" %>
<%@ Register Src="Filters/SystemListFilter.ascx" TagName="SystemListFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/FieldTypeListFilter.ascx" TagName="FieldTypeListFilter"
    TagPrefix="gc" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" runat="Server">
    <table style="width: 100%; text-align: left;">
        <tr>
            <td colspan="5">
                <gc:RegionZoneNodeFilter ID="filterRegionZoneNode" runat="server" EnableSelectAllNode="true"
                    EnableSelectAllRegion="true" EnableSelectAllZone="true" LastTopograpyLevelFilter="Node" />
            </td>
        </tr>
        <!--
        <tr>     
            <td colspan="5">
                <gc:SystemListFilter ID="filterSystemList" runat="server" SelectionType="Multiple" />
            </td>
        </tr>
        -->
        <tr>
            <td colspan="5">
                <gc:FieldTypeListFilter ID="filterFieldType" runat="server" SelectionType="Multiple" />
            </td>
        </tr>
        <tr runat="server">
            <td style="width: 3%;">
                &nbsp;
            </td>
            <td style="width: 20%;">
                <asp:Label runat="server">Giorni</asp:Label>
            </td>
            <td style="width: 4%;">
                &nbsp;
            </td>
            <td style="width: 70%;">
                <asp:CheckBox ID="chkLun" runat="server" Text="Luned�" />
                <asp:CheckBox ID="chkMar" runat="server" Text="Marted�" />
                <asp:CheckBox ID="chkMer" runat="server" Text="Mercoled�" />
                <asp:CheckBox ID="chkGio" runat="server" Text="Gioved�" />
                <asp:CheckBox ID="chkVen" runat="server" Text="Venerd�" />
                <asp:CheckBox ID="chkSab" runat="server" Text="Sabato" />
                <asp:CheckBox ID="chkDom" runat="server" Text="Domenica" />
            </td>
            <td style="width: 3%;">
                &nbsp;
            </td>
        </tr>
        <tr id="Tr1" runat="server">
            <td style="width: 3%;">
                &nbsp;
            </td>
            <td style="width: 20%;">
                <asp:Label ID="Label1" runat="server">Data</asp:Label>
            </td>
            <td style="width: 4%;">
                &nbsp;
            </td>
            <td style="width: 70%;">
                <div style="float: left; width: 210px;">
                    da
                    <asp:TextBox runat="server" ID="txtDataInizio"> </asp:TextBox>
                    <act:CalendarExtender ID="txtDataInizio_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtDataInizio">
                    </act:CalendarExtender>
                    <act:MaskedEditExtender ID="txtDataInizio_MaskedEditExtender" runat="server" MaskType="Date"
                        Mask="99/99/9999" MessageValidatorTip="true" ErrorTooltipEnabled="True" Enabled="True"
                        TargetControlID="txtDataInizio" />
                    <act:MaskedEditValidator ID="txtDataInizio_MaskedEditValidator" runat="server" ControlExtender="txtDataInizio_MaskedEditExtender"
                        ControlToValidate="txtDataInizio" IsValidEmpty="True" InvalidValueMessage="Data non valida"
                        Display="Static" InvalidValueBlurredMessage="(!)" />
                </div>
                <div style="float: left; width: 210px;">
                    a
                    <asp:TextBox runat="server" ID="txtDataFine"> </asp:TextBox>
                    <act:CalendarExtender ID="txtDataFine_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="txtDataFine">
                    </act:CalendarExtender>
                    <act:MaskedEditExtender ID="txtDataFine_MaskedEditExtender" runat="server" MaskType="Date"
                        Mask="99/99/9999" MessageValidatorTip="true" ErrorTooltipEnabled="True" Enabled="True"
                        TargetControlID="txtDataFine" />
                    <act:MaskedEditValidator ID="txtDataFine_MaskedEditValidator" runat="server" ControlExtender="txtDataFine_MaskedEditExtender"
                        ControlToValidate="txtDataFine" IsValidEmpty="True" InvalidValueMessage="Data non valida"
                        Display="Static" InvalidValueBlurredMessage="(!)" />
                    <asp:CompareValidator runat="server" Operator="GreaterThanEqual" ControlToCompare="txtDataInizio"
                        ToolTip='La data finale deve essere maggiore della data iniziale' ControlToValidate="txtDataFine"
                        Type="Date" Display="Dynamic" ErrorMessage="(!)"></asp:CompareValidator>
                </div>
            </td>
            <td style="width: 3%;">
                &nbsp;
            </td>
        </tr>
        <tr id="Tr2" runat="server">
            <td style="width: 3%;">
                &nbsp;
            </td>
            <td style="width: 20%;">
                <asp:Label ID="Label2" runat="server">Fascia oraria</asp:Label>
            </td>
            <td style="width: 4%;">
                &nbsp;
            </td>
            <td style="width: 70%;">
                <div style="float: left; width: 210px;">
                    da
                    <asp:TextBox runat="server" ID="txtOraInizio"> </asp:TextBox>
                    <act:MaskedEditExtender ID="txtOraInizio_MaskedEditExtender" runat="server" MaskType="Time"
                        Mask="99:99" MessageValidatorTip="true" ErrorTooltipEnabled="True" CultureTimePlaceholder=":"
                        Enabled="True" TargetControlID="txtOraInizio">
                    </act:MaskedEditExtender>
                    <act:MaskedEditValidator runat="server" ControlExtender="txtOraInizio_MaskedEditExtender"
                        ControlToValidate="txtOraInizio" IsValidEmpty="True" InvalidValueMessage="L'orario non � valido"
                        Display="Static" InvalidValueBlurredMessage="(!)" />
                </div>
                <div style="float: left; width: 210px;">
                    a
                    <asp:TextBox runat="server" ID="txtOraFine"> </asp:TextBox>
                    <act:MaskedEditExtender ID="txtOraFine_MaskedEditExtender" runat="server" MaskType="Time"
                        Mask="99:99" MessageValidatorTip="true" ErrorTooltipEnabled="True" CultureTimePlaceholder=":"
                        Enabled="True" TargetControlID="txtOraFine">
                    </act:MaskedEditExtender>
                    <act:MaskedEditValidator runat="server" ControlExtender="txtOraFine_MaskedEditExtender"
                        ControlToValidate="txtOraFine" IsValidEmpty="True" InvalidValueMessage="L'orario non � valido"
                        Display="Static" InvalidValueBlurredMessage="(!)" />
                </div>
            </td>
            <td style="width: 3%;">
                &nbsp;
            </td>
        </tr>
        <tr id="Tr3" runat="server">
            <td style="width: 3%;">
                &nbsp;
            </td>
            <td style="width: 20%;">
                <asp:Label ID="Label3" runat="server">Durata</asp:Label>
            </td>
            <td style="width: 4%;">
                &nbsp;
            </td>
            <td style="width: 70%;">
                <div style="float: left; width: 210px;">
                    da
                    <asp:TextBox runat="server" ID="txtDurataMin" Width="83px"> </asp:TextBox>
                    <act:MaskedEditExtender ID="MaskedEditExtender_txtDurataMin" runat="server" MaskType="Number"
                        Mask="999" InputDirection="RightToLeft" AcceptNegative="None" MessageValidatorTip="true"
                        ErrorTooltipEnabled="True" Enabled="True" TargetControlID="txtDurataMin" />
                    <asp:DropDownList ID="cboDurataMin_udm" runat="server">
                        <asp:ListItem Text="minuti" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="ore"></asp:ListItem>
                        <asp:ListItem Text="giorni"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float: left; width: 210px;">
                    a
                    <asp:TextBox runat="server" ID="txtDurataMax" Width="83px"> </asp:TextBox>
                    <act:MaskedEditExtender ID="MaskedEditExtender_txtDurataMax" runat="server" MaskType="Number"
                        Mask="999" InputDirection="RightToLeft" AcceptNegative="None" MessageValidatorTip="true"
                        ErrorTooltipEnabled="True" Enabled="True" TargetControlID="txtDurataMax" />
                    <asp:DropDownList ID="cboDurataMax_udm" runat="server">
                        <asp:ListItem Text="minuti" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="ore"></asp:ListItem>
                        <asp:ListItem Text="giorni"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </td>
            <td style="width: 3%;">
                &nbsp;
            </td>
        </tr>
        <tr runat="server">
            <td style="width: 3%;">
                &nbsp;
            </td>
            <td style="width: 20%;">
                <asp:Label runat="server">Stato</asp:Label>
            </td>
            <td style="width: 4%;">
                &nbsp;
            </td>
            <td style="width: 70%;">
                <asp:CheckBox ID="chkOk" runat="server" Checked="true" Text="Ok" />
                <asp:CheckBox ID="chkWarning" runat="server" Checked="true" Text="Avviso" />
                <asp:CheckBox ID="chkError" runat="server" Checked="true" Text="Errore" Enabled="false" />
                <asp:CheckBox ID="chkNoActive" runat="server" Checked="true" Text="Non Attivo" />
                <asp:CheckBox ID="chkUnknown" runat="server" Checked="true" Text="Sconosciuto" />
            </td>
            <td style="width: 3%;">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" runat="Server">
    <asp:Panel ID="pnlResult" runat="server" Style="width: 95%; background: #ffffff;">
        <rsweb:ReportViewer AsyncRendering="false" Height="500px" ID="rptvSelectedReport"
            PageCountMode="Actual" runat="server" ShowWaitControlCancelLink="false" Width="100%" KeepSessionAlive="False">
            <LocalReport ReportPath="Reports\HistoryStreamFieldsValue.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </asp:Panel>
    <asp:Panel ID="pnlNoResult" runat="server" Visible="false">
        <asp:Label ID="lblNoResult" runat="server" Text="Nessun risultato soddisfa i filtri utilizzati."></asp:Label>
    </asp:Panel>
    <br />
    <asp:ObjectDataSource ID="odsSelectedReport" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetBigData" TypeName="GrisSuite.Data.Reports.HistoryStreamFieldsValueTableAdapters.HistoryValueTableAdapter"
        OnSelecting="odsSelectedReport_Selecting" >
        <SelectParameters>
            <asp:Parameter Name="DataInizio" Type="DateTime" />
            <asp:Parameter Name="DataFine" Type="DateTime" />
            <asp:Parameter Name="GiorniDellaSettimana" Type="String" />
            <asp:Parameter Name="OraInizio" Type="DateTime" />
            <asp:Parameter Name="OraFine" Type="DateTime" />
            <asp:Parameter Name="ListaSistemi" Type="string" />
            <asp:Parameter Name="Stazione" Type="String" />
            <asp:Parameter Name="Linea" Type="String" />
            <asp:Parameter Name="Compartimento" Type="String" />
            <asp:Parameter Name="DurataMin" Type="Int32" />
            <asp:Parameter Name="DurataMax" Type="Int32" />
            <asp:Parameter Name="ListaSevLevel" Type="String" />
            <asp:Parameter Name="ListaTipiCampi" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="odsSelectedReportHeader" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetDataFilter" TypeName="GrisSuite.Data.Reports.HistoryStreamFieldsValueTableAdapters.HistoryValueHeaderTableAdapter"
        OnSelecting="odsSelectedReportHeader_Selecting">
        <SelectParameters>
            <asp:Parameter Name="DataInizio" Type="DateTime" />
            <asp:Parameter Name="DataFine" Type="DateTime" />
            <asp:Parameter Name="GiorniDellaSettimana" Type="String" />
            <asp:Parameter Name="OraInizio" Type="DateTime" />
            <asp:Parameter Name="OraFine" Type="DateTime" />
            <asp:Parameter Name="ListaSistemi" Type="string" />
            <asp:Parameter Name="Stazione" Type="String" />
            <asp:Parameter Name="Linea" Type="String" />
            <asp:Parameter Name="Compartimento" Type="String" />
            <asp:Parameter Name="DurataMin" Type="Int32" />
            <asp:Parameter Name="DurataMinUdm" Type="String" />
            <asp:Parameter Name="DurataMax" Type="Int32" />
            <asp:Parameter Name="DurataMaxUdm" Type="String" />
            <asp:Parameter Name="ListaSevLevel" Type="String" />
            <asp:Parameter Name="ListaTipiCampi" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="Counters.aspx.cs" Inherits="Reports_Counters" Title="Messaggi per linea" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" Runat="Server">
    Nessun filtro disponibile
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" Runat="Server">
	<asp:panel id="pnlResult" runat="server" style="width:95%;background:#ffffff;">
		<rsweb:reportviewer id="rptvSelectedReport" runat="server" height="500px" width="100%" AsyncRendering="false" ShowWaitControlCancelLink="false" PageCountMode="Actual" KeepSessionAlive="False">
			<localreport reportpath="Reports\Counters.rdlc">
				<datasources>
					<rsweb:reportdatasource name="ReportDS" datasourceid="odsSelectedReport" />
				</datasources>
			</localreport>
		</rsweb:reportviewer>
	</asp:panel>
	<asp:panel id="pnlNoResult" runat="server" visible="false">
		<asp:label id="lblNoResult" runat="server" text="Nessun dato disponibile."></asp:label>
	</asp:panel>
	<br />
	<asp:objectdatasource id="odsSelectedReport" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="GrisSuite.Data.Reports.CountersTableAdapters.CountersTableAdapter" onselecting="odsSelectedReport_Selecting">
	</asp:objectdatasource>
</asp:Content>


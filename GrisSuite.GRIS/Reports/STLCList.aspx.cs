using System;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_STLCList : ReportPublisher
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.rptvSelectedReport.Load += this.rptvSelectedReport_Load;

        # region waiting animation

        this.rptvSelectedReport.FindControl("AsyncWait").Visible = false;

        if (this.rptvSelectedReport.FindControl("ctl10") is WebControl)
        {
            // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010
            ((WebControl) this.rptvSelectedReport.FindControl("ctl10")).Style["text-align"] = "left";
        }
        else
        {
            if (this.rptvSelectedReport.FindControl("ctl05") is WebControl)
            {
                // Permette di allineare la toolbar di reporting services a sinistra, con report viewer 2010 SP 1 (modificato albero controlli)
                ((WebControl) this.rptvSelectedReport.FindControl("ctl05")).Style["text-align"] = "left";
            }
        }

        # endregion

        if (!this.IsPostBack)
        {
            // questa gestione della memorizzazione dei filtri fa eccezione rispetto alle altre
            // la riassegnazione dei valori alla proprietÓ di default viene delegata allo user control contenitore.
            this.FilterControlIDs.Add(this.filterServerVersion.ID);
        }
    }

    private void rptvSelectedReport_Load(object sender, EventArgs e)
    {
        string reportToLoad = "Reports\\STLCList.rdlc";

        if (this.filterForExcelAndOptimized.CheckedFlat)
        {
            reportToLoad = "Reports\\STLCList_flat.rdlc";
        }

        if ((this.filterForExcelAndOptimized.CheckedOptimized) && (this.filterForExcelAndOptimized.CheckedFlat))
        {
            reportToLoad = "Reports\\STLCList_Flat_NoBookmarks.rdlc";
        }

        if (!reportToLoad.Equals(this.rptvSelectedReport.LocalReport.ReportPath, StringComparison.OrdinalIgnoreCase))
        {
            this.rptvSelectedReport.Reset();

            this.rptvSelectedReport.LocalReport.ReportPath = reportToLoad;

            this.rptvSelectedReport.LocalReport.DataSources.Clear();
            this.rptvSelectedReport.LocalReport.DataSources.Add(new ReportDataSource("ReportDS", "odsSTLCList"));
        }
    }

    protected void odsSTLCList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["RegID"] = this.filterRegionZoneNode.SelectedValueRegion;
        e.InputParameters["ZonID"] = this.filterRegionZoneNode.SelectedValueZone;
        e.InputParameters["NodID"] = this.filterRegionZoneNode.SelectedValueNode;
        e.InputParameters["Online"] = this.filterOnline.SelectedValue;
        e.InputParameters["InMaintenance"] = this.filterMaintenance.SelectedValue;
        e.InputParameters["ServerVersionList"] = (this.filterServerVersion.SelectedValues.Length > 0 || this.filterServerVersion.SelectedValue == "||")
                                                     ? string.Format("|{0}|",
                                                                     string.Join("|",
                                                                                 (from val in this.filterServerVersion.SelectedValues select val).
                                                                                     ToArray()))
                                                     : "";
    }

    #region ReportPublisher Members

    public override ReportViewer ReportViewer
    {
        get { return this.rptvSelectedReport; }
    }

    #endregion

    public override void SavePageFilters()
    {
        base.SavePageFilters();
        this.filterRegionZoneNode.SavePageFilters();
        this.filterOnline.SavePageFilters();
        this.filterMaintenance.SavePageFilters();
        this.filterServerVersion.SavePageFilters();
        this.filterForExcelAndOptimized.SavePageFilters();
    }
}
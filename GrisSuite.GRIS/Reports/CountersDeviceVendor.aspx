<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="CountersDeviceVendor.aspx.cs" Inherits="Reports_CountersDeviceVendor" Title="Contatori per produttore" CodeFileBaseClass="ReportPublisher" IsFilterSavingEnabled="True" %>

<%@ register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
	namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@ Register src="Filters/SystemListFilter.ascx" tagname="SystemListFilter" tagprefix="gc" %>

<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" Runat="Server">
    <gc:SystemListFilter ID="filterSystemList" runat="server" enableselectall="false" selectiontype="Multiple"/>
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" Runat="Server">
	<asp:panel id="pnlResult" runat="server" style="width:95%;background:#ffffff;">
		<rsweb:reportviewer id="rptvSelectedReport" runat="server" height="500px" width="100%" AsyncRendering="false" ShowWaitControlCancelLink="false" PageCountMode="Actual" KeepSessionAlive="False">
			<localreport reportpath="Reports\CountersDeviceVendor.rdlc">
				<datasources>
					<rsweb:reportdatasource name="ReportDS" datasourceid="odsSelectedReport" />
				</datasources>
			</localreport>
		</rsweb:reportviewer>
	</asp:panel>
	<asp:panel id="pnlNoResult" runat="server" visible="false">
		<asp:label id="lblNoResult" runat="server" text="Nessun risultato soddisfa i filtri utilizzati."></asp:label>
	</asp:panel>
	<br />
	<asp:objectdatasource id="odsSelectedReport" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="GrisSuite.Data.Reports.CountersDeviceVendorTableAdapters.CountersDeviceVendorTableAdapter" onselecting="odsSelectedReport_Selecting">
		<selectparameters>
			<asp:parameter name="Systems" type="String" />
		</selectparameters>		
	</asp:objectdatasource>
</asp:Content>


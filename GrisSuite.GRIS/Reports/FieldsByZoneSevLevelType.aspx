<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true"
    CodeFile="FieldsByZoneSevLevelType.aspx.cs" Inherits="Reports_FieldsByZoneSevLevelType"
    Title="Messaggi per linea" CodeFileBaseClass="ReportPublisher" IsFilterSavingEnabled="True" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="Filters/RegionZoneNodeFilter.ascx" TagName="RegionZoneNodeFilter"
    TagPrefix="gc" %>
<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" runat="Server">
    <table style="width: 100%; text-align: left;">
        <tr>
            <td colspan="5">
                <gc:RegionZoneNodeFilter ID="filterRegionZoneNode" runat="server" EnableSelectAllNode="true"
                    EnableSelectAllRegion="true" EnableSelectAllZone="true" LastTopograpyLevelFilter="Node" />
            </td>
        </tr>
        <tr id="trRegion" runat="server">
            <td style="width: 3%;">
                &nbsp;
            </td>
            <td style="width: 20%;">
                <asp:Label ID="lblStato" runat="server">Stato</asp:Label>
            </td>
            <td style="width: 4%;">
                &nbsp;
            </td>
            <td style="width: 70%;">
                <asp:CheckBox ID="chkOk" runat="server" Checked="false" Text="Ok" />
                <asp:CheckBox ID="chkWarning" runat="server" Checked="false" Text="Avviso" />
                <asp:CheckBox ID="chkError" runat="server" Checked="true" Text="Errore" Enabled="false" />
                <asp:CheckBox ID="chkInfo" runat="server" Checked="false" Text="Informazione" />
            </td>
            <td style="width: 3%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Label ID="lblDeviceType" runat="server">Tipo Dispositivo</asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtDeviceType" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Label ID="lblMessage" runat="server">Messaggio</asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Label ID="lblIncludeInMaintenance" runat="server">Includi Periferiche Non Attive</asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:CheckBox ID="chkIncludeInMaintenance" runat="server" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" runat="Server">
    <asp:Panel ID="pnlResult" runat="server" Style="width: 95%; background: #ffffff;">
        <rsweb:ReportViewer ID="rptvSelectedReport" runat="server" Height="500px" Width="100%"
            AsyncRendering="false" ShowWaitControlCancelLink="false" PageCountMode="Actual" KeepSessionAlive="False">
            <LocalReport ReportPath="Reports\FieldsByZoneSevLevelType.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource Name="ReportDS" DataSourceId="odsSelectedReport" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </asp:Panel>
    <asp:Panel ID="pnlNoResult" runat="server" Visible="false">
        <asp:Label ID="lblNoResult" runat="server" Text="Nessun risultato soddisfa i filtri utilizzati."></asp:Label>
    </asp:Panel>
    <br />
    <asp:ObjectDataSource ID="odsSelectedReport" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="GrisSuite.Data.Reports.FieldsByZoneSevLevelTypeTableAdapters.FieldsByZoneSevLevelTypeTableTableAdapter"
        OnSelecting="odsSelectedReport_Selecting">
        <SelectParameters>
            <asp:Parameter Name="RegID" Type="Int64" />
            <asp:Parameter Name="ZonID" Type="Int64" />
            <asp:Parameter Name="NodID" Type="Int64" />
            <asp:Parameter Name="SevLevelList" Type="string" />
            <asp:Parameter Name="DeviceType" Type="string" />
            <asp:Parameter Name="Message" Type="string" />
            <asp:Parameter Name="IncludeInMaintenance" Type="Boolean" DefaultValue="False" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

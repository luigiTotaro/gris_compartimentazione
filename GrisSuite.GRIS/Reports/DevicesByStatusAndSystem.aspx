<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true"
    CodeFile="DevicesByStatusAndSystem.aspx.cs" CodeFileBaseClass="ReportPublisher"
    Inherits="Reports_DevicesByStatusAndSystem" IsFilterSavingEnabled="True" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="Filters/DeviceStatusFilter.ascx" TagName="DeviceStatusFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/DeviceTypeFilter.ascx" TagName="DeviceTypeFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/RegionZoneNodeFilter.ascx" TagName="RegionZoneNodeFilter"
    TagPrefix="gc" %>
<%@ Register Src="Filters/SystemFilter.ascx" TagName="SystemFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/ServerStatusFilter.ascx" TagName="ServerStatusFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/MaintenanceFilter.ascx" TagName="MaintenanceFilter" TagPrefix="gc" %>
<%@ Register Src="Filters/ForExcelAndOptimizedFilter.ascx" TagName="ForExcelAndOptimizedFilter" TagPrefix="gc" %>
<asp:Content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" runat="Server">
    <gc:RegionZoneNodeFilter ID="filterRegionZoneNode" runat="server" EnableSelectAllNode="true"
        EnableSelectAllRegion="true" EnableSelectAllZone="true" LastTopograpyLevelFilter="Node" />
    <gc:SystemFilter ID="filterSystem" runat="server" EnableSelectAll="true" />
    <gc:DeviceStatusFilter ID="filterDeviceStatus" runat="server" EnableSelectAll="true" SelectionType="Multiple" />
    <gc:DeviceTypeFilter ID="filterDeviceType" runat="server" EnableSelectAll="true" SelectionType="Multiple" Height="215px" />
    <gc:ServerStatusFilter ID="filterServerStatus" runat="server" SelectionType="Multiple" EnableSelectAll="true" />
    <gc:MaintenanceFilter ID="filterMaintenance" Label="Stato attivazione STLC1000" runat="server" />
    <gc:ForExcelAndOptimizedFilter ID="filterForExcelAndOptimized" runat="server" />
</asp:Content>
<asp:Content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" runat="Server">
    <asp:Panel ID="pnlResult" runat="server" Style="width: 95%; background: #ffffff;">
        <rsweb:ReportViewer ID="rptvSelectedReport" runat="server" Height="500px" Width="100%"
            AsyncRendering="false" ShowWaitControlCancelLink="false" PageCountMode="Actual" KeepSessionAlive="False">
            <LocalReport ReportPath="Reports\DevicesByStatusAndSystem_Flat.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource Name="ReportDS" DataSourceId="odsDevicesByStatusAndSystem" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </asp:Panel>
    <asp:Panel ID="pnlNoResult" runat="server" Visible="false">
        <asp:Label ID="lblNoResult" runat="server" Text="Nessun risultato soddisfa i filtri utilizzati."></asp:Label>
    </asp:Panel>
    <asp:ObjectDataSource ID="odsDevicesByStatusAndSystem" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="GrisSuite.Data.Reports.DevicesByStatusAndSystemTableAdapters.gris_Report_GetDevicesByNodeAndSystem_DevStatusTableAdapter"
        OnSelecting="odsDevicesByStatusAndSystem_Selecting">
        <SelectParameters>
            <asp:Parameter Name="RegID" Type="Int64" />
            <asp:Parameter Name="ZonID" Type="Int64" />
            <asp:Parameter Name="NodID" Type="Int64" />
            <asp:Parameter Name="SystemID" Type="Int32" />
            <asp:Parameter Name="DeviceStatus" Type="String" />
            <asp:Parameter Name="DeviceType" Type="String" />
            <asp:Parameter Name="ServerStatus" Type="String" />
            <asp:Parameter Name="ServerInMaintenance" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris;

public partial class Region : NavigationPage
{
    #region Properties

    public override MultiView LayoutMultiView
    {
        get { return null; }
    }

    public override View GraphView
    {
        get { return null; }
    }

    public override View GridView
    {
        get { return null; }
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.RegID = this.GetID("RegID", "~/Italia.aspx");

            this.tmrRegion.Interval = DBSettings.GetPageRefreshTime() * 1000;

            # region Visualizzazione informazione in base ai permessi

            // Nascondo la colonna Warning nella visualizzazione tabellare se l'utente non appartiene ad un gruppo che sia almeno di livello 2
            if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2))
            {
                this.gvwLinee.Columns[10].Visible = false; // la colonna 10 � quella dei warning
                this.gvwLinee.Columns[11].Visible = false; // la colonna 11 � il separatore
            }

            # endregion
        }
    }

    protected void tmrRegion_Tick(object sender, EventArgs e)
    {
        this.gvwLinee.DataBind();
    }

    # region gvwLinee

    protected void gvwLinee_SelectedIndexChanged(object sender, EventArgs e)
    {
        string zonID = (sender != null) ? ((GridView)sender).SelectedDataKey["ZonID"].ToString() : "";
        if (!string.IsNullOrEmpty(zonID))
        {
            this.GoToPage(PageDestination.Zone, long.Parse(zonID));
        }
    }

    protected void gvwLinee_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ZoneDS.ZoneRow row = null;
        if (e.Row.RowType == DataControlRowType.DataRow && (row = ((DataRowView)e.Row.DataItem).Row as ZoneDS.ZoneRow) != null)
        {
            // scrittura etichette sulla barra di navigazione
            this.SetNavigationLabels(row);

            // gli appartenenti ad un gruppo di livello 1 non possono vedere le periferiche in attenzione
            if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2) && row.Status == 1 /* attenzione */)
            {
                row.Status = 0;
            }
            if (row.Status == -1)
            {
                row.Status = 255;
            }

            LinkButton lnk = (LinkButton)e.Row.FindControl("lnkbLinea");
            GUtility.EnableLinkButton(this, lnk, true);

            Controls_LayeredIcon imgStatus = (Controls_LayeredIcon)e.Row.FindControl("ImageStatus");

            if (row.STLCTotalCount == row.STLCOffline)
            {
                imgStatus.Status = 2; // solo se tutti gli stlc offline
                imgStatus.LayeredIconTypeAdditionalLayer = Controls_LayeredIcon.IconTypeAdditionalLayer.Offline;
                imgStatus.ImageToolTip = string.Format("{0} offline", GUtility.GetLocalServerTypeName());
            }
            else
            {
                imgStatus.Status = row.Status;
                if (!row.IsZoneColorNull())
                {
                    imgStatus.CssBackgroundColor = row.ZoneColor;
                }
                imgStatus.ImageToolTip = row.StatusDescription;
            }
        }
    }

    # endregion

    protected void odsZones_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["RegId"] = this.RegID;
    }

    # region navBar

    protected void navBar_OnLayoutSwitched(object sender, ImageClickEventArgs e)
    {
        this.GoToPage(PageDestination.Region, this.RegID);
    }

    protected void navBar_OnRefreshPageButtonClick(object sender, ImageClickEventArgs e)
    {
        this.DataRefresh();
    }

    # endregion

    #endregion

    # region Methods

    private void DataRefresh()
    {
        if (this.navBar.LayoutMode == MapLayoutMode.Grid)
        {
            this.gvwLinee.DataBind();
            this.updGridView.Update();
            this.updNavBar.Update();
        }
    }

    private void SetNavigationLabels(ZoneDS.ZoneRow row)
    {
        if (row != null)
        {
            this.navBar.SetCompartimentoSeverita(row.SevLevelRegion, (row.IsRegionColorNull() ? null : row.RegionColor));
        }
    }

    # endregion
}
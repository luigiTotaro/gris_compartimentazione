﻿using System;
using System.Web.UI;

public partial class GroupsAndFormulas : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        long nodId;
        if (!long.TryParse(this.Request.QueryString["nodid"], out nodId))
        {
            nodId = -1;
        }

        long regId;
        if (!long.TryParse(this.Request.QueryString["regid"], out regId))
        {
            regId = -1;
        }

        if (!this.IsPostBack)
        {
            this.GroupsAndFormulasControl.NodID = nodId;
            this.GroupsAndFormulasControl.PageSelectedNodeSystemID = GrisSessionManager.SelectedNodeSystem;
            this.GroupsAndFormulasControl.RegID = regId;
        }
    }
}
using System;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.RegionDSTableAdapters;
using System.Collections.Generic;

public partial class _Italia : NavigationPage
{
    private const int COLUMN_WARNING = 6;

    #region Properties

    public bool? CheckTableVisible
    {
        get { return this.tblCheckTable.Visible; }

        set
        {
            this.tblCheckTable.Visible = value ?? false;
            this.SetCheckTableVisibility(value ?? false);
        }
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitCheckTable();

        if (!this.IsPostBack)
        {
            this.DataRefresh();

            this.tmrItalia.Interval = DBSettings.GetPageRefreshTime() * 1000;

            // visualizzazione della tabellina riassuntiva chiusa/aperta
            this.CheckTableVisible = GrisSessionManager.CheckTableItaOpen;

            // rimuovo i dati del compartimento dalla sessione
            GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Region);

            this.lblMM.Text = string.Format(this.lblMM.Text, GUtility.GetLocalServerTypeName());
            this.lblSTLC.Text = string.Format(this.lblSTLC.Text, GUtility.GetLocalServerTypeName());

            if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2))
            {
                this.gvwWholeNet.Columns[COLUMN_WARNING].Visible = false;
                this.gvwWholeNet.Columns[COLUMN_WARNING + 1].Visible = false; //separatore
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (GrisSessionManager.InformationBarFirstLineMessage.Equals(ConfigurationManager.AppSettings["NoSilverlightPluginMessage"]))
        {
            this.navBar.ClearInformationBarPostVisualization = true;
        }

        if (GrisSessionManager.InformationBarFirstLineMessage.Equals(ConfigurationManager.AppSettings["TabularViewClippingRowsMessage"]))
        {
            this.navBar.ClearInformationBarData();
        }

        if ((!GrisSessionManager.Session.Exists("AckPopupHidden")) ||
            (GrisSessionManager.Session.Exists("AckPopupHidden") && !(GrisSessionManager.Session.Get("AckPopupHidden", false))))
        {
            if (this.ackList.HasDataToShow)
            {
                this.programmaticModalPopup.Show();
            }
        }
    }

    #region gvwWholeNet

    protected void gvwWholeNet_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView grid;

        if (sender != null && ((grid = sender as GridView) != null) && grid.SelectedDataKey != null)
        {
            string regId = (grid.SelectedDataKey["RegID"] ?? "").ToString();
            if (!string.IsNullOrEmpty(regId))
            {
                this.GoToPage(PageDestination.Region, long.Parse(regId));
            }
        }
    }

    protected void gvwWholeNet_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RegionDS.RegionRow row;
        if (e.Row.RowType == DataControlRowType.DataRow && (row = e.Row.DataItem as RegionDS.RegionRow) != null)
        {
            // scrittura etichette sulla barra di navigazione
            this.SetNavigationLabels(row);

            // gli appartenenti ad un gruppo di livello 1 non possono vedere le periferiche in attenzione
            if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2) && row.Status == 1 /* attenzione */)
            {
                row.Status = 0;
            }
            if (row.Status == -1)
            {
                row.Status = 255;
            }

            # region immagine stato stazione

            var lnk = e.Row.FindControl("lnkbRegion") as LinkButton;

            if (lnk != null)
            {
                GUtility.EnableLinkButton(this, lnk, true);
            }

            Controls_LayeredIcon imgStatus = (Controls_LayeredIcon)e.Row.FindControl("ImageStatus");

            if (row.STLCTotalCount == row.STLCOffline)
            {
                imgStatus.Status = 2; // solo se tutti gli stlc offline
                imgStatus.LayeredIconTypeAdditionalLayer = Controls_LayeredIcon.IconTypeAdditionalLayer.Offline;
                imgStatus.ImageToolTip = string.Format("{0} offline", GUtility.GetLocalServerTypeName());
            }
            else
            {
                imgStatus.Status = row.Status;
                if (!row.IsRegionColorNull())
                {
                    imgStatus.CssBackgroundColor = row.RegionColor;
                }
                imgStatus.ImageToolTip = row.StatusDescription;
            }

            # endregion
        }
    }

    #endregion

    protected void imgOpenClose_Click(object sender, ImageClickEventArgs e)
    {
        this.CheckTableVisible = !this.CheckTableVisible;
        GrisSessionManager.CheckTableItaOpen = this.CheckTableVisible;
    }

    protected void lnkTabularMode_Click(object sender, EventArgs e)
    {
        this.navBar.SetInformationBarData(InformationBarIcon.Info, ConfigurationManager.AppSettings["NoSilverlightPluginMessage"]);

        this.navBar.LayoutMode = MapLayoutMode.Grid;
        this.GoToPage(PageDestination.Italia);
    }

    # region navBar

    protected void navBar_OnLayoutSwitched(object sender, EventArgs e)
    {
        this.DataRefresh();
        this.updPage.Update();
    }

    protected void navBar_OnRefreshPageButtonClick(object sender, ImageClickEventArgs e)
    {
        this.DataRefresh();
        this.updPage.Update();
    }

    # endregion

    protected void odsRegions_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["RegId"] = this.RegID;
    }

    #endregion

    #region First Time Initialization

    private void InitCheckTable()
    {
        this.trC1.Visible = GrisPermissions.IsInRole(RegCodes.Torino);
        this.trC2.Visible = GrisPermissions.IsInRole(RegCodes.Milano);
        this.trC3.Visible = GrisPermissions.IsInRole(RegCodes.Verona);
        this.trC4.Visible = GrisPermissions.IsInRole(RegCodes.Venezia);
        this.trC5.Visible = GrisPermissions.IsInRole(RegCodes.Trieste);
        this.trC6.Visible = GrisPermissions.IsInRole(RegCodes.Genova);
        this.trC7.Visible = GrisPermissions.IsInRole(RegCodes.Bologna);
        this.trC8.Visible = GrisPermissions.IsInRole(RegCodes.Firenze);
        this.trC9.Visible = GrisPermissions.IsInRole(RegCodes.Ancona);
        this.trC10.Visible = GrisPermissions.IsInRole(RegCodes.Roma);
        this.trC11.Visible = GrisPermissions.IsInRole(RegCodes.Napoli);
        this.trC12.Visible = GrisPermissions.IsInRole(RegCodes.Bari);
        this.trC13.Visible = GrisPermissions.IsInRole(RegCodes.ReggioCalabria);
        this.trC14.Visible = GrisPermissions.IsInRole(RegCodes.Palermo);
        this.trC15.Visible = GrisPermissions.IsInRole(RegCodes.Cagliari);

        this.tblCheckTableHeaders.Rows[0].Cells[0].Attributes["class"] = "LblT";
        //sono costretto a forzarlo per un bug del framework

        this.tblCheckTableHeaders.Rows[0].Cells[0].Style[HtmlTextWriterStyle.Width] = "30%;";
        this.tblCheckTableHeaders.Rows[0].Cells[5].Visible = false; // 5 � la colonna ack
        this.tblCheckTableHeaders.Rows[0].Cells[2].Visible = false; // 2 � la colonna sconosciuto
        this.tblCheckTableHeaders.Rows[0].Cells[6].Visible = false; // 6 � la colonna offline
        this.tblCheckTableHeaders.Rows[0].Cells[7].Visible = false; // 7 � la colonna STLC1000 non attivo

        foreach (HtmlTableRow row in this.tblCheckTable.Rows)
        {
            row.Cells[0].Style[HtmlTextWriterStyle.Width] = "30%";
            row.Cells[5].Visible = false; // 5 � la colonna ack
            row.Cells[2].Visible = false; // 2 � la colonna sconosciuto
            row.Cells[6].Visible = false; // 6 � la colonna offline
            row.Cells[7].Visible = false; // 7 � la colonna STLC1000 non attivo
        }

        if (!GrisPermissions.IsUserAtLeastInLevel(PermissionLevel.Level2))
        {
            // nascondo l'intestazione della colonna di allarme ed allargo la prima colonna
            if (this.tblCheckTableHeaders.Rows[0].Cells[0].Style[HtmlTextWriterStyle.Width] == "30%;")
            {
                this.tblCheckTableHeaders.Rows[0].Cells[0].Style[HtmlTextWriterStyle.Width] = "50%;";
            }

            this.tblCheckTableHeaders.Rows[0].Cells[3].Visible = false; // 3 � la colonna warning

            foreach (HtmlTableRow row in this.tblCheckTable.Rows)
            {
                if (row.Cells[0].Style[HtmlTextWriterStyle.Width] == "30%")
                {
                    row.Cells[0].Style[HtmlTextWriterStyle.Width] = "50%";
                }

                row.Cells[3].Visible = false; // 3 � la colonna warning
            }
        }
    }

    #endregion

    # region Methods

    private void SetNavigationLabels(RegionDS.RegionRow row)
    {
        if (row != null)
        {
            this.navBar.LinkCompartimentoText = row.RegName.Replace("Compartimento ", "");
            this.navBar.LinkCompartimentoTooltip = row.RegName;
            this.navBar.SetCompartimentoSeverita(-1, (row.IsRegionColorNull() ? null : row.RegionColor));
        }
    }

    public IEnumerable<RegionDS.RegionRow> GetRegions()
    {
        var regionsTable = new RegionDS.RegionDataTable();
        var ta = new RegionTableAdapter();
        ta.Fill(regionsTable, 0);

        return from RegionDS.RegionRow regionRow in regionsTable.Rows
               where GrisPermissions.IsInRole(regionRow.RegID)
               select regionRow;
    }

    private void InitTabularView(IEnumerable<RegionDS.RegionRow> regList)
    {
        this.gvwWholeNet.DataSource = regList;
        this.gvwWholeNet.DataBind();
    }

    private void InitCheckTableValues(IEnumerable<RegionDS.RegionRow> regList)
    {
        for (byte index = 1; index < 16; index++)
        {
            if (GrisPermissions.IsInRole((RegCodes)index))
            {
                byte tmp = index;
                RegionDS.RegionRow row = regList.SingleOrDefault(reg => reg.RegID == (long)Utility.EncodeRegionID(tmp));

                if (row == null)
                {
                    this.tblCheckTable.FindControl("trC" + index).Visible = false;
                    continue;
                }

                Label lblOff = (Label)this.tblCheckTable.FindControl("trC" + index).FindControl(string.Format("Lblc{0}_off", index));
                Label lblErr = (Label)this.tblCheckTable.FindControl("trC" + index).FindControl(string.Format("Lblc{0}_2", index));
                Label lblWrn = (Label)this.tblCheckTable.FindControl(string.Format("Lblc{0}_1", index));
                Label lblStlc = (Label)this.tblCheckTable.FindControl("trC" + index).FindControl(string.Format("Lblc{0}_stlc", index));
                Label lblMm = (Label)this.tblCheckTable.FindControl("trC" + index).FindControl(string.Format("Lblc{0}_mm", index));
                Label lblOk = (Label)this.tblCheckTable.FindControl("trC" + index).FindControl(string.Format("Lblc{0}_ok", index));

                lblOff.Text = row.Unknown.ToString(); // L'offline viene assimilato allo stato sconosciuto
                lblErr.Text = (row.Error + row.Unknown).ToString();
                lblStlc.Text = row.STLCOffline.ToString();
                lblMm.Text = row.STLCInMaintenance.ToString();
                lblOk.Text = row.Ok.ToString();

                lblWrn.Text = GrisPermissions.IsInRole((RegCodes)index, PermissionLevel.Level2) ? row.Warning.ToString() : "&nbsp;";
            }
        }
    }

    public string getClientID(object comp)
    {
        return ((Control)comp).ClientID;
    }

    private void SetCheckTableVisibility(bool visible)
    {
        string checkTableVisible = (visible ? "1" : "0");
        this.hidCompCheckTable.Value = checkTableVisible;

        if (visible)
        {
            this.lblLinea.Text = "Conteggio periferiche compartimento";
            this.imgOpenClose.ImageUrl = "IMG/interfaccia/riepilogo_contrai.gif";
            this.imgOpenClose.ToolTip = "Chiudi Tabella Riassuntiva";
        }
        else
        {
            this.lblLinea.Text = "Tabella Riassuntiva";
            this.imgOpenClose.ImageUrl = "IMG/interfaccia/riepilogo_espandi.gif";
            this.imgOpenClose.ToolTip = "Apri Tabella Riassuntiva";
        }

        this.lblWarning.Visible =
            this.lblError.Visible = this.lblOffline.Visible = this.lblSTLC.Visible = this.lblMM.Visible = this.lblOK.Visible = visible;
    }

    private void DataRefresh()
    {
        var compList = this.GetRegions();

        if (this.navBar.LayoutMode == MapLayoutMode.Grid)
        {
            if (string.IsNullOrEmpty(this.hidSelectedObject.Value))
            {
                this.InitTabularView(compList);
                this.updTabular.Update();
                this.updNavBar.Update();
            }
            else
            {
                var parms = this.hidSelectedObject.Value.Split('|');

                try
                {
                    long objectId;
                    PageDestination destination;
                    if (parms.Length == 2 && long.TryParse(parms[0], out objectId) &&
                        (destination = (PageDestination)Enum.Parse(typeof(PageDestination), parms[1], true)) != PageDestination.None)
                    {
                        this.GoToPage(destination, objectId);
                    }
                }
                catch (ArgumentException)
                {
                    this.InitTabularView(compList);
                    this.updTabular.Update();
                    this.updNavBar.Update();
                }
            }
        }
        else
        {
            this.InitCheckTableValues(compList);
            this.updGraphical.Update();
            this.updCheckTable.Update();
            this.updNavBar.Update();
        }
    }

    # endregion

    # region GrisPage Members implementation

    public override MultiView LayoutMultiView
    {
        get { return this.WholeNetMultiView; }
    }

    public override View GraphView
    {
        get { return this.GraphicalView; }
    }

    public override View GridView
    {
        get { return this.TabularView; }
    }

    # endregion

    protected void hideModalPopupViaServer_Click(object sender, EventArgs e)
    {
        GrisSessionManager.Session.Add("AckPopupHidden", true);
    }
}
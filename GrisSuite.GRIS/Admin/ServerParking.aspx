﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
	CodeFile="ServerParking.aspx.cs" Inherits="Admin_ServerParking" %>

<%@ Import Namespace="GrisSuite.Common" %>
<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
	<asp:UpdatePanel ID="updParking" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<table style="color: White; font-weight: normal; text-align: left; font-size: 10pt;
				margin-top: 5px; margin-bottom: 30px; width: 100%;">
				<tr>
					<td style="width: 60px;">
						<asp:Label runat="server" ID="lblSearchSrvID" AssociatedControlID="txtSearchSrvID"
							Text="Server ID:" EnableViewState="false" />
					</td>
					<td>
						<asp:TextBox runat="server" ID="txtSearchSrvID" Width="450px" /><asp:CompareValidator
							ID="cmpSearchSrvID" runat="server" Operator="DataTypeCheck" ToolTip='Inserire un numero intero'
							ControlToValidate="txtSearchSrvID" Type="Integer" Display="Static" ErrorMessage="(!)"></asp:CompareValidator>
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label runat="server" ID="lblSearchIP" AssociatedControlID="txtSearchIP" Text="IP:"
							EnableViewState="false" />
					</td>
					<td>
						<asp:TextBox runat="server" ID="txtSearchIP" Width="450px" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label runat="server" ID="lblSearchHost" AssociatedControlID="txtSearchHost"
							Text="Nome host:" EnableViewState="false" />
					</td>
					<td>
						<asp:TextBox runat="server" ID="txtSearchHost" Width="450px" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label runat="server" ID="lblSearchHardwareProfile" AssociatedControlID="txtSearchHardwareProfile"
							Text="Profilo hardware:" EnableViewState="false" />
					</td>
					<td>
						<asp:TextBox runat="server" ID="txtSearchHardwareProfile" Width="450px" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label runat="server" ID="lblSearchParkingType" AssociatedControlID="cboSearchParkingType"
							Text="Tipo parking:" EnableViewState="false" />
					</td>
					<td>
						<asp:DropDownList runat="server" ID="cboSearchParkingType" Width="450px" AppendDataBoundItems="true"
							DataSourceID="sdsGetServersParkingTypes" DataValueField="ParkingTypeID" DataTextField="ParkingTypeDescription">
							<asp:ListItem Value="-1" Text="Tutti" />
						</asp:DropDownList>
						<asp:ImageButton runat="server" ID="imgbSearch" ImageUrl="~/IMG/Interfaccia/Search.png"
							Style="padding-left: 20px; margin-bottom: -5px;" OnClick="imgbSearch_Click" />
					</td>
				</tr>
			</table>
			<asp:GridView ID="grdServersParking" runat="server" AllowSorting="True" AutoGenerateColumns="False"
				Width="100%" SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ServerParkingID"
				DataSourceID="sdsGetServersParking" ShowFooter="False" PageSize="10" EmptyDataText="Nessun server parcheggiato presente e corrispondente alla ricerca"
				AllowPaging="true" CssClass="modalGrid" OnRowCommand="grdServersParking_RowCommand"
				OnSorting="grdServersParking_Sorting">
				<PagerSettings Mode="NumericFirstLast" Position="Bottom" />
				<HeaderStyle CssClass="modalGridTh" />
				<RowStyle CssClass="modalGridRow" />
				<AlternatingRowStyle CssClass="modalGridRowA" />
				<PagerStyle CssClass="modalGridPager" />
				<Columns>
					<asp:BoundField Visible="false" DataField="ServerParkingID" />
					<asp:BoundField DataField="IP" HeaderText="IP" ReadOnly="True" SortExpression="IP">
						<ItemStyle Width="10%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="SrvID" HeaderText="Server ID" ReadOnly="True" SortExpression="SrvID">
						<ItemStyle Width="10%" />
						<HeaderStyle ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="Host" HeaderText="Nome host" ReadOnly="True" SortExpression="Host">
						<ItemStyle Width="8%" />
						<HeaderStyle ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="Name" HeaderText="Nome server" ReadOnly="True" SortExpression="Name">
						<ItemStyle Width="7%" />
						<HeaderStyle ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="Type" HeaderText="Tipo server" ReadOnly="True" SortExpression="Type">
						<ItemStyle Width="7%" />
						<HeaderStyle ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="HardwareProfile" HeaderText="Profilo hardware" ReadOnly="True"
						SortExpression="HardwareProfile">
						<ItemStyle Width="8%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="ClientRegID" HeaderText="Region ID" ReadOnly="True" SortExpression="ClientRegID">
						<ItemStyle Width="6%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="ClientZonID" HeaderText="Zone ID" ReadOnly="True" SortExpression="ClientZonID">
						<ItemStyle Width="6%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="ClientNodID" HeaderText="Node ID" ReadOnly="True" SortExpression="ClientNodID">
						<ItemStyle Width="6%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="ParkingTypeDescription" HeaderText="Tipo parking" ReadOnly="True"
						SortExpression="ParkingTypeDescription">
						<ItemStyle Width="20%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:BoundField DataField="LastUpdate" HeaderText="Data aggiornamento" ReadOnly="True"
						SortExpression="LastUpdate" DataFormatString="{0:dd/MM/yyyy HH.mm}">
						<ItemStyle Width="12%" HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:ImageButton ID="btnDeleteServeParking" runat="server" CommandName="DeleteServerParking"
								CommandArgument='<%# Eval("ServerParkingID") %>' ImageUrl="~/IMG/Interfaccia/Trash2.png"
								ToolTip="Elimina il server parcheggiato" Visible='<%# GrisPermissions.IsUserInAdminGroup() %>'
								OnClientClick='<%# string.Format("return confirm(\"Eliminare il server parcheggiato con IP: {0}?\");", Eval("IP"))%>'>
							</asp:ImageButton>
						</ItemTemplate>
						<ItemStyle Width="8%" HorizontalAlign="Center" Wrap="false" />
						<HeaderStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:SqlDataSource ID="sdsGetServersParkingTypes" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
		SelectCommand="gris_GetServersParkingTypes" SelectCommandType="StoredProcedure">
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="sdsGetServersParking" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
		SelectCommand="gris_GetServersParking" SelectCommandType="StoredProcedure" OnSelecting="sdsGetServersParking_Selecting">
		<SelectParameters>
			<asp:Parameter Name="IP" Type="String" />
			<asp:Parameter Name="SrvID" Type="Int64" />
			<asp:Parameter Name="Host" Type="String" />
			<asp:Parameter Name="HardwareProfile" Type="String" />
			<asp:Parameter Name="ParkingType" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="Contacts.aspx.cs" Inherits="Admin_Contacts" %>

<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="color: White; font-weight: bold; text-align: left;">
        Rubrica destinatari</div>
    <asp:UpdatePanel ID="updGrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
                <div style="text-align: left;  margin: 10px 0 10px">
                    <asp:TextBox ID="txtSearch" runat="server" MaxLength="128" Width="300px"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="Cerca" onclick="btnSearch_Click"/>
                    <br/><asp:Label runat="server" id="lblSearchResult" style="color: White; font-size: x-small " ></asp:Label>
                </div>
            <asp:ListView ID="lvwContacts" runat="server" DataSourceID="odsContacts"
                OnItemDataBound="lvwContacts_ItemDataBound" InsertItemPosition="FirstItem"
                DataKeyNames="ContactId" OnItemInserting="lvwContacts_ItemInserting"
                OnItemCreated="lvwContacts_ItemCreated" OnItemUpdating="lvwContacts_ItemUpdating"
                OnItemDeleting="lvwContacts_ItemDeleting">
                <LayoutTemplate>
                    <table id="tblGrid" runat="server" cellpadding="0" cellspacing="0" width="100%" class="ListViewHeader"
                        style="font-size: small; ">
                        <tr id="Tr1" runat="server" style="background-color: #afafaf; font-weight: bold;">
                            <th id="Th1" runat="server" width="19%">
                                <div style="padding: 2px;">
                                    Compartimento</div>
                            </th>
                            <th id="Th2" runat="server" width="19%">
                                <div style="padding: 2px;">
                                    Nome</div>
                            </th>
                            <th id="Th3" runat="server" width="19%">
                                <div style="padding: 2px;">
                                    Numero telefonico</div>
                            </th>
                            <th id="Th4" runat="server" width="19%">
                                <div style="padding: 2px;">
                                    E-mail</div>
                            </th>
                            <th id="Th6" runat="server" width="19%">
                                <div style="padding: 2px;">
                                    Windows User</div>
                            </th>
                            <th id="Th5" runat="server" width="5%">
                            </th>
                        </tr>
                        <tr runat="server" id="itemPlaceholder" />
                    </table>
                    <asp:DataPager ID="dpagGrid" runat="server" PageSize="20" style="width: 100%; display: block;
                            font-size: small; font-weight: bold; background-color: #afafaf;  text-align: center; padding: 2px;">
                        <Fields>
                            <asp:NumericPagerField NumericButtonCssClass="ListViewHeader" CurrentPageLabelCssClass="ListViewHeader"
                                NextPreviousButtonCssClass="ListViewHeader" />
                        </Fields>
                    </asp:DataPager>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr id="Tr2" runat="server" style="background-color: #4d4d4d;">
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblRegionName" runat="server" Text='<%# Eval("RegionName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblContactName" runat="server" Text='<%# Eval("ContactName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblPhoneNumber" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblWindowsUser" runat="server" Text='<%# Eval("WindowsUser") %>' />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" AlternateText="Modifica"
                                ImageUrl="~/IMG/Interfaccia/Grids/edit.gif" CausesValidation="false" />
                            <asp:ImageButton ID="imgDelete" runat="server" CommandName="Delete" Enabled='<%# !Convert.ToBoolean(Eval("IsContactIdAssociatedToNotificationContacts")) %>'
                                AlternateText='<%# Convert.ToBoolean(Eval("IsContactIdAssociatedToNotificationContacts")) ? "Il destinatario è associato a uno o più gruppi / notifiche, disassociarlo prima di cancellarlo" : "Elimina il destinatario" %>'
                                ImageUrl="~/IMG/Interfaccia/Grids/delete.gif" CausesValidation="false" OnClientClick='<%# string.Format("return confirm(\"Eliminare il destinatario “{0}”?\");", Eval("ContactName"))%>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr id="Tr2" runat="server" style="background-color: #333333;">
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblRegionName" runat="server" Text='<%# Eval("RegionName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblContactName" runat="server" Text='<%# Eval("ContactName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblPhoneNumber" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblWindowsUser" runat="server" Text='<%# Eval("WindowsUser") %>' />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" AlternateText="Modifica"
                                ImageUrl="~/IMG/Interfaccia/Grids/edit.gif" CausesValidation="false" />
                            <asp:ImageButton ID="imgDelete" runat="server" CommandName="Delete" Enabled='<%# !Convert.ToBoolean(Eval("IsContactIdAssociatedToNotificationContacts")) %>'
                                AlternateText='<%# Convert.ToBoolean(Eval("IsContactIdAssociatedToNotificationContacts")) ? "Impossibile eliminare il destinatario, perché associato ad almeno una notifica" : "Elimina il destinatario" %>'
                                ImageUrl="~/IMG/Interfaccia/Grids/delete.gif" CausesValidation="false" OnClientClick='<%# string.Format("return confirm(\"Eliminare il destinatario “{0}”?\");", Eval("ContactName"))%>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <InsertItemTemplate>
                    <tr id="Tr2" runat="server">
                        <td>
                            <div style="padding: 2px;">
                                <asp:DropDownList ID="cboRegionName" runat="server" Width="85%" ValidationGroup="insert"
                                    EnableViewState="True" DataTextField="value" DataValueField="key" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtContactName" runat="server" MaxLength="2000" Width="90%" />
                                <asp:RequiredFieldValidator ID="rfvalContactName" runat="server" ControlToValidate="txtContactName"
                                    ErrorMessage="(!)" Display="Dynamic" ToolTip="Campo richiesto" ValidationGroup="insert" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtPhoneNumber" runat="server" Width="90%" MaxLength="500" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtEmail" runat="server" Width="90%" MaxLength="500" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtWindowsUser" runat="server" Width="90%" MaxLength="500" />
                            </div>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgAccept" runat="server" CommandName="Insert" AlternateText="Conferma"
                                ValidationGroup="insert" ImageUrl="~/IMG/Interfaccia/Grids/accept.gif" />
                        </td>
                    </tr>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <tr id="Tr2" runat="server">
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblRegionName" runat="server" Text='<%# Eval("RegionName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtContactName" runat="server" Text='<%# Bind("ContactName") %>' MaxLength="2000"
                                    Width="90%" />
                                <asp:RequiredFieldValidator ID="rfvalContactName" runat="server" ControlToValidate="txtContactName"
                                    ErrorMessage="(!)" Display="Dynamic" ToolTip="Campo richiesto" ValidationGroup="edit" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtPhoneNumber" runat="server" Text='<%# Bind("PhoneNumber") %>'
                                    Width="90%" MaxLength="500" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("Email") %>'
                                    Width="90%" MaxLength="500" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtWindowsUser" runat="server" Text='<%# Bind("WindowsUser") %>'
                                    Width="90%" MaxLength="500" />
                            </div>
                        </td>
                        <td nowrap="nowrap">
                            <asp:ImageButton ID="imgAccept" runat="server" CommandName="Update" AlternateText="Conferma"
                                ValidationGroup="edit" ImageUrl="~/IMG/Interfaccia/Grids/accept.gif" />
                            <asp:ImageButton ID="imgCancel" runat="server" CommandName="Cancel" AlternateText="Annulla"
                                ImageUrl="~/IMG/Interfaccia/Grids/cancel.png" CausesValidation="false" />
                        </td>
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="odsContacts" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.ContactsTableAdapters.ContactsTableAdapter"
        UpdateMethod="Update" OnSelecting="odsContacts_Selecting" OnSelected="odsContacts_Selected">
        <SelectParameters>
            <asp:Parameter Name="RegIds" Type="String" />
            <asp:Parameter Name="Search" Type="String" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ContactId" Type="Int64" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="RegId" Type="Int64" />
            <asp:Parameter Name="ContactName" Type="String" />
            <asp:Parameter Name="PhoneNumber" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="WindowsUser" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ContactId" Type="Int64" />
            <asp:Parameter Name="ContactName" Type="String" />
            <asp:Parameter Name="PhoneNumber" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="WindowsUser" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</asp:Content>


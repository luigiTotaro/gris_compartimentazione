﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.DeviceFiltersDSTableAdapters;
using GrisSuite.Data.Gris.ResourcesDSTableAdapters;

public partial class Admin_ResourcesPermissions : System.Web.UI.Page
{
	private IEnumerable<ResourcePermissionGroups> _data;
    private ResourceType _resourceType;

	protected int SelectedResourceId
	{
		get { return Convert.ToInt32(this.ViewState["SelectedResourceId"] ?? -1); }
		set { this.ViewState["SelectedResourceId"] = value; }
	}

    #region Notifications Properties
    protected bool ContactsSelectedInit
    {
        get
        {
            if (ViewState["ContactsSelectedInit"] == null)
            {
                ViewState["ContactsSelectedInit"] = false;
            }
            return (bool)ViewState["ContactsSelectedInit"];
        }
        set { ViewState["ContactsSelectedInit"] = value; }
    }

    protected bool SelectAllContactsEmail
    {
        get
        {
            if (ViewState["SelectAllContactsEmail"] == null)
            {
                ViewState["SelectAllContactsEmail"] = false;
            }
            return (bool)ViewState["SelectAllContactsEmail"];
        }
        set { ViewState["SelectAllContactsEmail"] = value; }
    }

    protected HashSet<long> ContactsEmailSelected
    {
        get
        {
            if (ViewState["ContactsEmailSelected"] == null)
            {
                ViewState["ContactsEmailSelected"] = new HashSet<long>();
            }
            return (HashSet<long>)ViewState["ContactsEmailSelected"];

        }
    }

    protected bool SelectAllContactsSms
    {
        get
        {
            if (ViewState["SelectAllContactsSms"] == null)
            {
                ViewState["SelectAllContactsSms"] = false;
            }
            return (bool)ViewState["SelectAllContactsSms"];
        }
        set { ViewState["SelectAllContactsSms"] = value; }
    }

    protected HashSet<long> ContactsSmsSelected
    {
        get
        {
            if (ViewState["ContactsSmsSelected"] == null)
            {
                ViewState["ContactsSmsSelected"] = new HashSet<long>();
            }
            return (HashSet<long>)ViewState["ContactsSmsSelected"];
        }
    }

    #endregion

	protected void Page_Load ( object sender, EventArgs e )
	{
        _resourceType = (ResourceType)Enum.Parse(typeof(ResourceType), Request["type"]);
        this._data = ResourcePermissionGroups.GetResourcesAndGroups(_resourceType, "", "");

		if ( !this.IsPostBack )
		{
			this.BindCategories();
		}
	}

	protected void Page_Prerender ( object sender, EventArgs e )
	{
	    lblTitle.Text = Request["type"];
		this.lblNoDataAvailable.Visible = (this.rptCategories.Items.Count == 0);
        this.txtContactName.Attributes.Add("onkeypress", "return controlEnter('" + this.btnSearchContact.ClientID + "', event)");

	}

	protected void btnSearch_Click ( object sender, EventArgs e )
	{
        this._data = ResourcePermissionGroups.GetResourcesAndGroups(_resourceType, this.FormatSearchString(this.txtResourceName.Text), this.FormatSearchString(this.txtGroupName.Text));
		this.BindCategories();
	}

	protected void rptCategories_ItemDataBound ( object sender, RepeaterItemEventArgs e )
	{
		if ( ( e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem ) && e.Item.DataItem != null )
		{
			var listView = e.Item.FindControl("lvwResources") as ListView;
			var groups = e.Item.DataItem as IEnumerable<ResourcePermissionGroups>;

			if ( listView != null && groups != null )
			{
				listView.DataSource = groups;
				listView.DataBind();
			}
		}
	}

	protected void ListView_SelectedIndexChanging ( object sender, ListViewSelectEventArgs e )
	{
		var listView = sender as ListView;

		if ( listView != null && e.NewSelectedIndex >= 0 )
		{
			int resourceId = (int) listView.DataKeys[e.NewSelectedIndex].Value;
			string cat = this._data.SingleOrDefault(rep => rep.ResourceId == resourceId).Category;
			listView.SelectedIndex = e.NewSelectedIndex;
			this.SelectedResourceId = Convert.ToInt32(listView.SelectedValue);

			listView.DataSource = from resource in this._data where resource.Category == cat select resource;
			listView.DataBind();
		}
	}

	protected void ListView_OnItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			var listView = sender as ListView;
			var currentItem = e.Item as ListViewDataItem;
			if ( listView != null && currentItem != null && listView.SelectedIndex == currentItem.DisplayIndex )
			{
				var chklGroups = currentItem.FindControl("chklGroups") as CheckBoxList;

				if ( chklGroups != null )
				{
					var assGroups = PermissionGroup.GetAssociatedGroups(this.SelectedResourceId);
					chklGroups.DataSource = assGroups;
					chklGroups.DataBind();

					var itemsToCheck = from ListItem chkItem in chklGroups.Items
									   join assGroup in assGroups
										  on int.Parse(chkItem.Value) equals assGroup.GroupId
									   where assGroup.Associated
									   select chkItem;

					foreach ( var itemToCheck in itemsToCheck )
					{
						chklGroups.Items.FindByValue(itemToCheck.Value).Selected = true;
					}
				}

                var btnAssociations = currentItem.FindControl("btnNotifications") as Button;
			    if (btnAssociations != null) {
                    btnAssociations.Visible = (_resourceType == ResourceType.Filtri);    
			    }
			}
		}
	}

	protected void ListView_OnItemCommand ( object sender, ListViewCommandEventArgs e )
	{
	    switch (e.CommandName)
	    {
	        case "Associate":
	            var listView = sender as ListView;
	            var chklGroups = e.Item.FindControl("chklGroups") as CheckBoxList;
	            if (chklGroups != null && listView != null)
	            {
	                var groupIds =
	                (from ListItem checkedGroup in chklGroups.Items
	                    where checkedGroup.Selected
	                    select int.Parse(checkedGroup.Value)).ToList();
	                var ta = new ResourceGroupAssociationTableAdapter();
	                ta.UpdateResourcePermissions(
	                    string.Format("|{0}|", string.Join("|", groupIds.Select(id => id.ToString()).ToArray())),
	                    SelectedResourceId);

	                _data = ResourcePermissionGroups.GetResourcesAndGroups(_resourceType,
	                    FormatSearchString(txtResourceName.Text), FormatSearchString(txtGroupName.Text));
	                BindCategories();

	                GrisPermissions.GroupsCache.Reset();
	            }
	            break;
            case "Notifications":
	            openNotifications();
	            break;
	    }
	}

	private string FormatSearchString ( string searchString )
	{
		if ( string.IsNullOrEmpty(searchString) ) return "";

		return string.Format("%{0}%", searchString);
	}

	private void BindCategories ()
	{
		this.rptCategories.DataSource = from resource in this._data group resource by resource.Category into category select category;
		this.rptCategories.DataBind();
	}


    #region PopupContacts

    protected void sdsContacts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        // gris_GetResourceNotifications
        e.Command.Parameters["@ResourceId"].Value = SelectedResourceId;
        e.Command.Parameters["@ConcactFilter"].Value = txtContactName.Text;
        e.Command.Parameters["@OnlyEmail"].Value = false;
        e.Command.Parameters["@OnlySms"].Value = false;
    }

    protected void btnSaveContactAssociate_Click(object sender, EventArgs e)
    {
        UpdateContactsSelected();
        DataKey keys = this.grdContacts.DataKeys[0];
        int resourceId = (int)(keys["ResourceId"]);
        var ta = new ResourceNotificationsTableAdapter();

        ta.DelResourceNotifications(resourceId);

        foreach (long contactId in ContactsEmailSelected)
        {
            ta.UpdResourceNotifications(resourceId, contactId, ContactsSmsSelected.Contains(contactId), true);
            ContactsSmsSelected.Remove(contactId);
        }
        foreach (long contactId in ContactsSmsSelected)
        {
            ta.UpdResourceNotifications(resourceId, contactId, true, false);
        }

        // rebind
    }

    protected void btnCancelContactAssociate_Click(object sender, EventArgs e)
    {

    }

    protected void openNotifications    ()
    {
        ContactsSelectedInit = true;
        ContactsEmailSelected.Clear();
        ContactsSmsSelected.Clear();
        this.grdContacts.PageIndex = 0;
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
        this.mpeContactAssociate.Show();
    }

    protected void btnSearchContact_Click(object sender, EventArgs e)
    {
        ContactsSelectedInit = true;
        this.grdContacts.PageIndex = 0;
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
    }

    protected void btnOkDeleteNotification_OnClick(object sender, EventArgs e)
    {
        var ta = new ResourceNotificationsTableAdapter();
        ta.DelResourceNotifications(SelectedResourceId);
        mpeContactAssociate.Hide();
    }

    protected void chkSelectAllContactsSms_OnCheckedChanged(object sender, EventArgs e)
    {
        ContactsSmsSelected.Clear();
        CheckBox chkAll = ((CheckBox)sender);
        SelectAllContactsSms = chkAll.Checked;
        if (SelectAllContactsSms)
        {
            DataView dv = sdsContacts.Select(DataSourceSelectArguments.Empty) as DataView;
            if (dv != null)
            {
                foreach (DataRow row in dv.Table.Rows)
                {
                    ContactsSmsSelected.Add((long)row["ContactId"]);
                }
            }
        }
        grdContacts.DataBind();

    }

    protected void chkSelectAllContactsEmail_OnCheckedChanged(object sender, EventArgs e)
    {
        ContactsEmailSelected.Clear();
        CheckBox chkAll = ((CheckBox)sender);
        SelectAllContactsEmail = chkAll.Checked;
        if (SelectAllContactsEmail)
        {
            DataView dv = sdsContacts.Select(DataSourceSelectArguments.Empty) as DataView;
            if (dv != null)
            {
                foreach (DataRow row in dv.Table.Rows)
                {
                    ContactsEmailSelected.Add((long)row["ContactId"]);
                }
            }
        }
        grdContacts.DataBind();
    }

    protected void grdContacts_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox selectAllEmails = e.Row.FindControl("chkSelectAllContactsEmail") as CheckBox;
            if (selectAllEmails != null)
            {
                selectAllEmails.Checked = SelectAllContactsEmail;
                selectAllEmails.ToolTip = SelectAllContactsEmail ? "Deseleziona tutto" : "Seleziona tutto";
            }
            CheckBox selectAllSms = e.Row.FindControl("chkSelectAllContactsSms") as CheckBox;
            if (selectAllSms != null)
            {
                selectAllSms.Checked = SelectAllContactsSms;
                selectAllSms.ToolTip = SelectAllContactsSms ? "Deseleziona tutto" : "Seleziona tutto";
            }
        }

        if (e.Row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[e.Row.RowIndex] == null) return;

        CheckBox chkSendEMail = e.Row.FindControl("chkSendEMail") as CheckBox;
        CheckBox chkSendSms = e.Row.FindControl("chkSendSms") as CheckBox;

        if (ContactsSelectedInit)
        {
            ContactsSelectedInit = false;
            DataTable table = ((DataRowView)e.Row.DataItem).Row.Table;

            foreach (DataRow row in table.Rows)
            {
                Boolean sendEMail = (Boolean)row["SendEMail"];
                Boolean sendSms = (Boolean)row["SendSms"];

                if (!sendSms && !sendEMail) break;

                if (sendEMail) ContactsEmailSelected.Add((long)row["ContactId"]);
                if (sendSms) ContactsSmsSelected.Add((long)row["ContactId"]);
            }
        }

        long contactId = (long)this.grdContacts.DataKeys[e.Row.RowIndex]["ContactId"];
        if (chkSendEMail != null) chkSendEMail.Checked = ContactsEmailSelected.Contains(contactId);
        if (chkSendSms != null) chkSendSms.Checked = ContactsSmsSelected.Contains(contactId);
    }

    protected void grdContacts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        UpdateContactsSelected();
    }

    private void UpdateContactsSelected()
    {
        foreach (GridViewRow row in this.grdContacts.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[row.RowIndex] == null) continue;

            long contactId = (long)(this.grdContacts.DataKeys[row.RowIndex]["ContactId"]);

            bool sendeMail = ((CheckBox)row.FindControl("chkSendEMail")).Checked;
            if (sendeMail)
                ContactsEmailSelected.Add(contactId);
            else
                ContactsEmailSelected.Remove(contactId);

            bool sendSms = ((CheckBox)row.FindControl("chkSendSms")).Checked;
            if (sendSms)
                ContactsSmsSelected.Add(contactId);
            else
                ContactsSmsSelected.Remove(contactId);

        }
    }

    #endregion




}

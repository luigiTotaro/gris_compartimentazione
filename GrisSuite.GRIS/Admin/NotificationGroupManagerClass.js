﻿function NotificationGroupsManagerClass(container) {
    this.container = container;
}


NotificationGroupsManagerClass.prototype.loadData = function () {

    var self = this;

    var inputText = $(".txtMyInput").val();
    //console.log("input: " + inputText);
    if (inputText == "") {
        self.container.html("");
        return false;
    }




    var obj = {
        'action': 'GetDataSms',
        'searchText': inputText
    }

    

    $.ajax({
        cache: false,
        type: "GET",
        url: "NotificationGroupsParameters.ashx",
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = eval(response);
            //console.log(data);

            var html = "";

            for (var i = 0; i < data.length; i++) {

                var color = "#4D4D4D";
                if (i % 2 == 0) color = "#333333";


                html += '<tr style="background-color:' + color + '"><td align="left"><p>' + data[i].ObjectStatusName + '</p></td>';
                html += '   <td align="left"><p>' + data[i].ObjectStatusTipologia + '</p></td>';
                html += '   <td align="left"><p id="WindowsGroups_p_' + data[i].ObjectStatusID + '">' + data[i].ObjectStatusWindowsGroups + '</p><input id="WindowsGroups_input_' + data[i].ObjectStatusID + '" style="display:none;width:95%;"></td>';

                html += '   <td><img id="mod_image_' + data[i].ObjectStatusID + '" src="../IMG/Interfaccia/Grids/edit.gif" alt="Modifica" data-role-modifica-riga="' + data[i].ObjectStatusID + '" style="border-width:0px; cursor:pointer;"></img>';
                html += '       <img id="accept_image_' + data[i].ObjectStatusID + '" src="../IMG/Interfaccia/Grids/accept.gif" alt="Conferma"  data-role-accetta-riga="' + data[i].ObjectStatusID + '" style="border-width:0px;display:none;">&nbsp;';
                html += '       <img id="annulla_image_' + data[i].ObjectStatusID + '" src="../IMG/Interfaccia/Grids/cancel.png" alt="Annulla" data-role-annulla-riga="' + data[i].ObjectStatusID + '" style="border-width:0px;display:none;">';
                html += '   </td>';
                html += '</tr>';

            }

            var ctr = '<table cellspacing="0" cellpadding="4" border="0" id="tableBroker" style="color:White;font-size:Small;width:100%;border-collapse:collapse;">';
            ctr += '     <tbody>';
            ctr += '         <tr style="color:White;background-color:#AFAFAF;">';
            ctr += '         <th align="left" scope="col" style="">Nome</th>';
            ctr += '         <th align="left" scope="col" style="">Tipologia</th>';
            ctr += '         <th align="left" scope="col" style="">Gruppi Active Directory</th>';
            ctr += '         <th scope="col" style="">&nbsp;</th>';
            ctr += '         </tr>' + html + '</tbody></table>';

            var ctrObj = $(ctr);

            self.container.html(ctrObj);

            /*
            ctrObj.find('[data-role-modifica-riga]').click((e) => {
        
            var id = $(e.currentTarget).attr('data-role-modifica-riga');
            self.modificaRiga(id);
            return;
            });
            
            ctrObj.find('[data-role-accetta-riga]').click((e) => {
        
            var id = $(e.currentTarget).attr('data-role-accetta-riga');
            self.accettaRiga(id);
            return;
            });

            ctrObj.find('[data-role-annulla-riga]').click((e) => {
       
            self.annullaRiga(true); //ricarica la tabella
            return;
            });            
            
            */

            ctrObj.find('[data-role-modifica-riga]').click(function (e) {
                var id = $(e.currentTarget).attr('data-role-modifica-riga');
                self.modificaRiga(id);
                return;
            });

            ctrObj.find('[data-role-accetta-riga]').click(function (e) {

                var id = $(e.currentTarget).attr('data-role-accetta-riga');
                self.accettaRiga(id);
                return;
            });

            ctrObj.find('[data-role-annulla-riga]').click(function (e) {

                self.annullaRiga(true); //ricarica la tabella
                return;
            });


            return false;

        },
        failure: function (response) {
            alert(response);

            return false;
        }
    });

};

NotificationGroupsManagerClass.prototype.modificaRiga = function (id) {

    //console.log("modifico la riga " + id);

    //ma prima devo annullare tutte le eventuali modifiche in corso
    this.annullaRiga(false); //non devo ricaricare i dati

    $('#WindowsGroups_input_' + id).val($('#WindowsGroups_p_' + id)[0].innerText);

    $('#WindowsGroups_p_' + id).hide();
    $('#WindowsGroups_input_' + id).show();


    $('#mod_image_' + id).hide();
    $('#annulla_image_' + id).show();
    $('#accept_image_' + id).show();

    return false;
}

NotificationGroupsManagerClass.prototype.annullaRiga = function (reloadData) {
    //ritorno ai <p> senza modificare niente
    //alla fine è un "annulla tutto"

    $('[id^=WindowsGroups_input_]').val("");
    $('[id^=WindowsGroups_p_]').show();
    $('[id^=WindowsGroups_input_]').hide();


    $('[id^=mod_image_]').show();
    $('[id^=annulla_image_], [id^=accept_image_]').hide();

    if (reloadData) this.loadData();
    return false;
}

NotificationGroupsManagerClass.prototype.accettaRiga = function (id) {
    //ritorno ai <p> modificando i campi

    var self = this;

    var obj = {
        'action': 'SetGroupDataSms',
        'id': id,
        'WindowsGroups': $('#WindowsGroups_input_' + id).val()
    }

    $.ajax({
        type: "GET",
        url: "NotificationGroupsParameters.ashx",
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //var data = eval(response);

            self.loadData();
            return false;

        },
        failure: function (response) {
            //alert(response);

            self.loadData();
            return false;
        }
    });

    return false;

}



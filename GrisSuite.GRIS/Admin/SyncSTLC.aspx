<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="SyncSTLC.aspx.cs" Inherits="Admin_SyncSTLC" Theme="Gris" %>
<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" Runat="Server">
	<table cellpadding="8" cellspacing="8" style="width: 100%;">
		<tr>
			<td>
				<asp:updatepanel id="updServers" runat="server">
					<contenttemplate>
						<asp:GridView ID="gvwServers" SkinID="gvwCheckTable" runat="server" Width="100%" AutoGenerateColumns="False" datakeynames="SrvID" datasourceid="odsServers" onselectedindexchanged="gvwServers_SelectedIndexChanged">
							<RowStyle Font-Italic="True" Font-Size="14px" />
							<AlternatingRowStyle Font-Italic="True" Font-Size="14px" />
							<columns>
								<asp:boundfield datafield="SrvID" headertext="SrvID" readonly="True" sortexpression="SrvID"
									visible="False" />
								<asp:boundfield datafield="FullHostName" headertext="Nome Host" sortexpression="FullHostName">
									<headerstyle width="50%" />
								</asp:boundfield>
								<asp:boundfield datafield="IP" headertext="Indirizzo IP" sortexpression="IP">
									<headerstyle width="40%" />
								</asp:boundfield>
								<asp:templatefield showheader="False">
									<itemstyle width="10%" />
									<itemtemplate>
										<asp:button id="btnSync" runat="server" causesvalidation="false" commandname="Select" text="Forza Sincronizzazione" />
									</itemtemplate>
								</asp:templatefield>
							</columns>
						</asp:GridView>
						<asp:label id="lblMessage" runat="server" enableviewstate="false"></asp:label>
					</contenttemplate>
				</asp:updatepanel>
			</td>
		</tr>
	</table>
	<asp:objectdatasource id="odsServers" runat="server"
		oldvaluesparameterformatstring="original_{0}" selectmethod="GetServers"
		typename="GrisSuite.Data.dsCentralServersTableAdapters.serversTableAdapter">
	</asp:objectdatasource>
</asp:Content>


﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.NotificationMailContactsDSTableAdapters;

public partial class Admin_NotificationMailContacts : Page
{
    protected bool ContactsSelectedInit
    {
        get
        {
            if (ViewState["ContactsSelectedInit"] == null)
            {
                ViewState["ContactsSelectedInit"] = false;
            }
            return (bool)ViewState["ContactsSelectedInit"];
        }
        set { ViewState["ContactsSelectedInit"] = value; }
    }

    protected bool SelectAll
    {
        get
        {
            if (ViewState["SelectAll"] == null)
            {
                ViewState["SelectAll"] = false;
            }
            return (bool)ViewState["SelectAll"];
        }
        set { ViewState["SelectAll"] = value; }
    }

    protected HashSet<Guid> NotificationsSelected
    {
        get
        {
            if (ViewState["NotificationsSelected"] == null)
            {
                ViewState["NotificationsSelected"] = new HashSet<Guid>();
            }
            return (HashSet<Guid>)ViewState["NotificationsSelected"];

        }
    }

    protected bool SelectAllContacts
    {
        get
        {
            if (ViewState["SelectAllContacts"] == null)
            {
                ViewState["SelectAllContacts"] = false;
            }
            return (bool)ViewState["SelectAllContacts"];
        }
        set { ViewState["SelectAllContacts"] = value; }
    }

    protected HashSet<long> ContactsSelected
    {
        get
        {
            if (ViewState["ContactsSelected"] == null)
            {
                ViewState["ContactsSelected"] = new HashSet<long>();
            }
            return (HashSet<long>)ViewState["ContactsSelected"];

        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void odsNotificationMailContacts_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["RegIds"] = string.Format("|{0}|",
                                                    string.Join("|",
                                                                GrisPermissions.VisibleRegions(PermissionLevel.AllLevels)
                                                                    .Select(
                                                                        regionId => regionId.ToString()).ToArray()));

        e.InputParameters["Search"] = this.txtSearch.Text;
    }


    protected void lvwNotificationMailContacts_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (e.Item.ItemType != ListViewItemType.DataItem) return;

        DataRowView dataRowView = (DataRowView)((ListViewDataItem)e.Item).DataItem;

        if (dataRowView == null) return;

        CheckBox checkBox = (CheckBox)e.Item.FindControl("chkSelect");
        if (checkBox != null)
        {
            Guid objectStatusId = (Guid)dataRowView["ObjectStatusID"];
            checkBox.Checked = NotificationsSelected.Contains(objectStatusId);
        }
        
        
        string NotificationMailContacts = dataRowView["NotificationContacts"].ToString();

        Image imgNotificationMailContacts = (Image) e.Item.FindControl("imgNotificationMailContacts");
        imgNotificationMailContacts.Visible = !(string.IsNullOrEmpty(NotificationMailContacts));
    }

    protected void sdsContacts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        
        e.Command.Parameters["@ObjectStatusId"].Value = (this.ViewState["LastContactAssociate"] != null
                                                             ? (Guid) this.ViewState["LastContactAssociate"]
                                                             : Guid.Empty);
        e.Command.Parameters["@RegIds"].Value = string.Format("|{0}|", string.Join("|",
                                                                                   GrisPermissions.VisibleRegions(
                                                                                       PermissionLevel.AllLevels).Select
                                                                                       (
                                                                                           regionId =>
                                                                                           regionId.ToString()).ToArray()));
        e.Command.Parameters["@Search"].Value = this.txtSearchContact.Text;

    }

    protected void grdContacts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        UpdateContactsSelected();
    }

    protected void btnSaveContactAssociate_Click(object sender, EventArgs e)
    {
        UpdateContactsSelected();

        var ta = new NotificationMailContactsTableAdapter();

        foreach (Guid objectStatusId in NotificationsSelected)
        {
            ta.DeleteNotificationContacts(objectStatusId, null);
            foreach (long contactId in ContactsSelected)
            {
                ta.UpdateNotificationContacts(true, objectStatusId, contactId);
            }
        }

        SelectAllContacts = false;
        ContactsSelected.Clear();

        this.BindNotificationMailContacts();
    }

    private void UpdateContactsSelected()
    {
        foreach (GridViewRow row in this.grdContacts.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[row.RowIndex] == null) continue;

            bool selected = ((CheckBox)row.FindControl("chkSelectedContact")).Checked;
            long contactId = (long)(this.grdContacts.DataKeys[row.RowIndex]["ContactId"]);
            if (selected)
                ContactsSelected.Add(contactId);
            else
                ContactsSelected.Remove(contactId);
        }
    }

    protected void btnCancelContactAssociate_Click(object sender, EventArgs e)
    {
        SelectAllContacts = false;
        ContactsSelected.Clear();

        this.BindNotificationMailContacts();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SelectAll = false;
        NotificationsSelected.Clear();

        this.BindNotificationMailContacts();
    }

    private void BindNotificationMailContacts()
    {
        this.lvwNotificationMailContacts.DataBind();
        this.updGrid.Update();

        this.grdContacts.PageIndex = 0;
    }

    protected void odsNotificationMailContacts_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        lblSearchResult.Text = "";

        if (e == null || e.ReturnValue == null || !(e.ReturnValue is DataTable)) return;

        DataTable nodes = (DataTable)e.ReturnValue;

        int count = nodes.Rows.Count;

        if (SelectAll)
        {
            NotificationsSelected.Clear();
            foreach (DataRow row in nodes.Rows)
            {
                NotificationsSelected.Add((Guid)row["ObjectStatusId"]);
            }
        }

        lblSearchResult.Text = String.Format("#{0} risultat{1}", count, (count == 1) ? "o" : "i");
    }

    protected void btnAddContacts_OnClick(object sender, EventArgs e)
    {
        this.ContactsSelectedInit = true;
        this.ViewState["LastContactAssociate"] = new Guid();
        if (NotificationsSelected.Count == 1)
        {
            this.ViewState["LastContactAssociate"] = NotificationsSelected.ToArray()[0];
        }
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
        this.mpeContactAssociate.Show();
    }
    protected void chkSelectAll_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkSelectAll = (CheckBox)sender;
        SelectAll = chkSelectAll.Checked;
        if (!SelectAll) NotificationsSelected.Clear();
        lvwNotificationMailContacts.DataBind();
    }


    protected void lvwNotificationSmsContacts_OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        UpdateNotificationsSelected();
    }


    private void UpdateNotificationsSelected()
    {
        foreach (ListViewDataItem item in lvwNotificationMailContacts.Items)
        {
            if (item.ItemType != ListViewItemType.DataItem || lvwNotificationMailContacts.DataKeys[item.DisplayIndex] == null) continue;

            bool selected = ((CheckBox)item.FindControl("chkSelect")).Checked;
            Guid objectStatusId = (Guid)lvwNotificationMailContacts.DataKeys[item.DisplayIndex].Value;
            if (selected)
                NotificationsSelected.Add(objectStatusId);
            else
                NotificationsSelected.Remove(objectStatusId);
        }
    }

    protected void chkSelect_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkSelect = sender as CheckBox;
        if (chkSelect == null) return;
        ListViewDataItem item = chkSelect.NamingContainer as ListViewDataItem;
        if (item == null) return;
        Guid objectStatusId = (Guid)lvwNotificationMailContacts.DataKeys[item.DisplayIndex].Value;

        if (chkSelect.Checked)
            NotificationsSelected.Add(objectStatusId);
        else
            NotificationsSelected.Remove(objectStatusId);
    }


    protected void updGrid_OnPreRender(object sender, EventArgs e)
    {
        btnAddContacts.Enabled = (NotificationsSelected.Count > 0);
        String textStazioni = "sistem" + (NotificationsSelected.Count == 1 ? "a" : "i");
        btnSaveContactAssociate.Text = string.Format("Associa destinatari" + (NotificationsSelected.Count == 1 ? "" : " a {0} {1}"), NotificationsSelected.Count, textStazioni);
    }

    protected void btnSearchContact_OnClick(object sender, EventArgs e)
    {
        this.ContactsSelectedInit = true;
        grdContacts.PageIndex = 0;
        SelectAllContacts = false;
        ContactsSelected.Clear();
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
    }

    protected void chkSelectAllContacts_OnCheckedChanged(object sender, EventArgs e)
    {
        ContactsSelected.Clear();
        CheckBox chkAll = ((CheckBox)sender);
        SelectAllContacts = chkAll.Checked;
        if (SelectAllContacts)
        {
            DataView dv = sdsContacts.Select(DataSourceSelectArguments.Empty) as DataView;
            if (dv != null)
            {
                foreach (DataRow row in dv.Table.Rows)
                {
                    ContactsSelected.Add((long)row["ContactId"]);
                }
            }
        }
        grdContacts.DataBind();
    }


    protected void grdContacts_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox selectAll = e.Row.FindControl("chkSelectAllContacts") as CheckBox;
            if (selectAll != null)
            {
                selectAll.Checked = SelectAllContacts;
                selectAll.ToolTip = SelectAllContacts ? "Deseleziona tutto" : "Seleziona tutto";
            }
        }

        if (e.Row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[e.Row.RowIndex] == null) return;

        CheckBox checkBox = e.Row.FindControl("chkSelectedContact") as CheckBox;

        if (ContactsSelectedInit)
        {
            ContactsSelectedInit = false;
            DataTable table = ((DataRowView)e.Row.DataItem).Row.Table;
            foreach (DataRow row in table.Rows)
            {
                if (!(Boolean)row["Associated"]) break;
                ContactsSelected.Add((long)row["ContactId"]);
            }

        }

        long contactId = (long)this.grdContacts.DataKeys[e.Row.RowIndex]["ContactId"];
        if (checkBox != null) checkBox.Checked = ContactsSelected.Contains(contactId);

    }
}
﻿function BrokerDataManagerClass(container) {
    this.container = container;
}


BrokerDataManagerClass.prototype.loadData = function () {
    var obj = {
        'action': 'GetBrokerData'
    }

    var self = this;

    $.ajax({
        cache: false,
        type: "GET",
        url: "BrokerParameters.ashx",
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var data = eval(response);
            //console.log(data);

            var html = "";

            for (var i = 0; i < data.length; i++) {

                var color = "#4D4D4D";
                if (i % 2 == 0) color = "#333333";


                html += '<tr style="background-color:' + color + '"><td align="left"><p>' + data[i].Name + '</p></td>';
                html += '   <td align="left"><p id="ip_p_' + data[i].ID + '">' + data[i].ip + '</p><input id="ip_input_' + data[i].ID + '" style="display:none;"></td>';
                html += '   <td align="left"><p id="port_p_' + data[i].ID + '">' + data[i].port + '</p><input id="port_input_' + data[i].ID + '" style="display:none;"></td>';
                html += '   <td align="left"><p id="username_p_' + data[i].ID + '">' + data[i].username + '</p><input id="username_input_' + data[i].ID + '" style="display:none;"></td>';
                html += '   <td align="left"><p id="password_p_' + data[i].ID + '">********</p><input type="password" id="password_input_' + data[i].ID + '" style="display:none;" value="' + data[i].password + '"></td>';
                html += '   <td><img id="mod_image_' + data[i].ID + '" src="../IMG/Interfaccia/Grids/edit.gif" alt="Modifica" data-role-modifica-riga="' + data[i].ID + '" style="border-width:0px; cursor:pointer;"></img>';
                html += '       <img id="accept_image_' + data[i].ID + '" src="../IMG/Interfaccia/Grids/accept.gif" alt="Conferma"  data-role-accetta-riga="' + data[i].ID + '" style="border-width:0px;display:none;">&nbsp;';
                html += '       <img id="annulla_image_' + data[i].ID + '" src="../IMG/Interfaccia/Grids/cancel.png" alt="Annulla" data-role-annulla-riga="' + data[i].ID + '" style="border-width:0px;display:none;">';
                html += '   </td>';
                html += '</tr>';

            }

            var ctr = '<table cellspacing="0" cellpadding="4" border="0" id="tableBroker" style="color:White;font-size:Small;width:100%;border-collapse:collapse;">';
            ctr += '     <tbody>';
            ctr += '         <tr style="color:White;background-color:#AFAFAF;">';
            ctr += '         <th align="left" scope="col">Compartimento</th>';
            ctr += '         <th align="left" scope="col">Broker IP</th>';
            ctr += '         <th align="left" scope="col">Port</th>';
            ctr += '         <th align="left" scope="col">Username</th>';
            ctr += '         <th align="left" scope="col">Password</th>';
            ctr += '         <th scope="col" style="width:45px;">&nbsp;</th>';
            ctr += '         </tr>' + html + '</tbody></table>';

            var ctrObj = $(ctr);

            self.container.html(ctrObj);

            /*
            ctrObj.find('[data-role-modifica-riga]').click((e) => {
        
            var id = $(e.currentTarget).attr('data-role-modifica-riga');
            self.modificaRiga(id);
            return;
            });
            
            ctrObj.find('[data-role-accetta-riga]').click((e) => {
        
            var id = $(e.currentTarget).attr('data-role-accetta-riga');
            self.accettaRiga(id);
            return;
            });

            ctrObj.find('[data-role-annulla-riga]').click((e) => {
       
            self.annullaRiga(true); //ricarica la tabella
            return;
            });            
            
            */

            ctrObj.find('[data-role-modifica-riga]').click(function (e) {
                var id = $(e.currentTarget).attr('data-role-modifica-riga');
                self.modificaRiga(id);
                return;
            });

            ctrObj.find('[data-role-accetta-riga]').click(function (e) {

                var id = $(e.currentTarget).attr('data-role-accetta-riga');
                self.accettaRiga(id);
                return;
            });

            ctrObj.find('[data-role-annulla-riga]').click(function (e) {

                self.annullaRiga(true); //ricarica la tabella
                return;
            });


            return false;

        },
        failure: function (response) {
            alert(response);

            return false;
        }
    });

};

BrokerDataManagerClass.prototype.modificaRiga = function (id) {

    //console.log("modifico la riga " + id);

    //ma prima devo annullare tutte le eventuali modifiche in corso
    this.annullaRiga(false); //non devo ricaricare i dati

    $('#ip_input_' + id).val($('#ip_p_' + id)[0].innerText);
    $('#port_input_' + id).val($('#port_p_' + id)[0].innerText);
    $('#username_input_' + id).val($('#username_p_' + id)[0].innerText);

    $('#ip_p_' + id + ', #port_p_' + id + ', #username_p_' + id + ', #password_p_' + id).hide();
    $('#ip_input_' + id + ', #port_input_' + id + ', #username_input_' + id + ', #password_input_' + id).show();


    $('#mod_image_' + id).hide();
    $('#annulla_image_' + id).show();
    $('#accept_image_' + id).show();

    return false;
}

BrokerDataManagerClass.prototype.annullaRiga = function (reloadData) {
    //ritorno ai <p> senza modificare niente
    //alla fine è un "annulla tutto"

    $('[id^=ip_input], [id^=port_input_], [id^=username_input_]').val("");
    $('[id^=ip_p], [id^=port_p_], [id^=username_p_], [id^=password_p_]').show();
    $('[id^=ip_input], [id^=port_input_], [id^=username_input_], [id^=password_input_]').hide();


    $('[id^=mod_image_]').show();
    $('[id^=annulla_image_], [id^=accept_image_]').hide();

    if (reloadData) this.loadData();
    return false;
}

BrokerDataManagerClass.prototype.accettaRiga = function (id) {
    //ritorno ai <p> modificando i campi

    var self = this;

    var obj = {
        'action': 'SetBrokerData',
        'id': id,
        'ip': $('#ip_input_' + id).val(),
        'port': $('#port_input_' + id).val(),
        'username': $('#username_input_' + id).val(),
        'password': $('#password_input_' + id).val()
    }

    $.ajax({
        type: "GET",
        url: "BrokerParameters.ashx",
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //var data = eval(response);

            self.loadData();
            return false;

        },
        failure: function (response) {
            //alert(response);

            self.loadData();
            return false;
        }
    });

    return false;

}



﻿<%@ WebHandler Language="C#" Class="agentService" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;

public class agentService : IHttpHandler {


        
    public void ProcessRequest (HttpContext context) {


        HttpResponse r = context.Response;

        string action = context.Request.QueryString["action"];
        
        if (action.Equals("GetBrokerData"))
        {
            List<BrokerRegionData> RegionData = GUtility.GetAllRegionExtData();

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(RegionData);
            
            
            /*
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Console.WriteLine(reader[0].ToString());
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }            
            */

            r.Clear();
            r.ContentType = "application/json";
            r.Write(json);
            r.End();                 
        }
        else if (action.Equals("SetBrokerData")) 
        {
            var RegId = context.Request.QueryString["id"];
            var brokerIp = context.Request.QueryString["ip"];
            var port = context.Request.QueryString["port"];
            var username = context.Request.QueryString["username"];
            var password = context.Request.QueryString["password"];
            
            int res = GUtility.UpdateRegionExtData(RegId, brokerIp, port, username, password);        
        }  
        else if (action.Equals("SetAudioData")) 
        {
            var RegId = context.Request.QueryString["id"];
            var urlCommandAwg = context.Request.QueryString["urlCommandAwg"];
            var urlListenAwg = context.Request.QueryString["urlListenAwg"];

            
            int res = GUtility.UpdateAudioExtData(RegId, urlCommandAwg, urlListenAwg);        
        }  
        
        /*
        string regId = context.Request.QueryString["regId"];        
        
        //string regId = "281474976645132";
        string[] sTmp = GUtility.GetRegionExtData(regId);

        string json = "{\"brokerIp\":\"" + sTmp[0] + "\",\"username\":\"" + sTmp[1] + "\",\"password\":\"" + sTmp[2] + "\"}";
        r.Clear();
        r.ContentType = "application/json; charset=utf-8";
        r.Write(json);
        r.End();        
        
        */
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}


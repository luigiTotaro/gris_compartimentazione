﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using GrisSuite.Common;

public partial class Admin_OperationSchedulesSMS : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {}

    protected void sdsSMS_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OperationScheduleTypeId"].Value = OperationScheduleType.Sms;
    }

    protected void grdSMS_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblPhoneNumber = (Label)e.Row.FindControl("lblPhoneNumber");
            Label lblMessage = (Label)e.Row.FindControl("lblMessage");

            string operationParameters = string.Format("<?xml version=\"1.0\" encoding=\"utf-8\" ?>{0}",
                                                       ((DataRowView)e.Row.DataItem)["OperationParameters"]);

            lblPhoneNumber.Text = string.Empty;
            lblMessage.Text = string.Empty;

            XDocument operationParametersDoc = XDocument.Parse(operationParameters, LoadOptions.None);
            if (operationParametersDoc.Root != null)
            {
                var query =
                    from elem in
                        operationParametersDoc.Descendants("{http://schemas.microsoft.com/2003/10/Serialization/Arrays}KeyValueOfstringstring")
                    select new
                           {
                               Key = elem.Descendants("{http://schemas.microsoft.com/2003/10/Serialization/Arrays}Key").Single().Value,
                               elem.Descendants("{http://schemas.microsoft.com/2003/10/Serialization/Arrays}Value").Single().Value
                           };

                foreach (var operationParameter in query)
                {
                    if (operationParameter.Key == "phoneNum")
                    {
                        lblPhoneNumber.Text = operationParameter.Value;
                        continue;
                    }

                    if (operationParameter.Key == "message")
                    {
                        lblMessage.Text = operationParameter.Value;
                    }
                }
            }
        }
    }
}
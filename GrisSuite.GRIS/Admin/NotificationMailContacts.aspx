﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="NotificationMailContacts.aspx.cs" Inherits="Admin_NotificationMailContacts" %>

<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.dimensions.js" type="text/javascript"></script>
    <script src="../JS/jquery.tooltip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function hideModalPopupViaClient(id) {
            var modalPopupBehavior = $find(id);
            modalPopupBehavior.hide();
        }
        function pageLoad() {
            $("img[id$='imgNotificationMailContacts']").tooltip({
                showURL: false,
                positionLeft: true
            });
        }        
    </script>
    <div style="color: White; font-weight: bold; text-align: left;">
        Lista stazioni con notifiche e-mail attive / Assegnazione destinatari</div>
    <asp:UpdatePanel ID="updGrid" runat="server" UpdateMode="Conditional" OnPreRender="updGrid_OnPreRender">
        <ContentTemplate>
            <div style="text-align: left; color: white; padding: 5px;">
                <div style="text-align: left; margin: 10px 0 10px">
                    <asp:TextBox ID="txtSearch" runat="server" MaxLength="128" Width="300px" Style="float: left;
                    margin-right: 0px"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="Cerca" onclick="btnSearch_Click" Style="float: left; margin-right: 20px; height: 22px;" />
                    <asp:Label runat="server" id="lblSearchResult" style="color: White; font-size: x-small " ></asp:Label>
                </div>
                <div style="text-align: left; margin: 20px 0 10px">
                    <asp:Button runat="server" ID="btnAddContacts" Text="Associa destinatari" Style="margin-right: 5px"
                        ToolTip="Associa destinatari alle stazioni selezionate" OnClick="btnAddContacts_OnClick" />
                </div>
                <asp:ListView ID="lvwNotificationMailContacts" runat="server" DataSourceID="odsNotificationMailContacts"
                    DataKeyNames="ObjectStatusId" 
                    OnItemDataBound="lvwNotificationMailContacts_ItemDataBound">
                    <LayoutTemplate>
                        <table id="tblGrid" runat="server" cellpadding="0" cellspacing="0" width="100%" class="ListViewHeader"
                            style="font-size: small">
                            <tr id="Tr1" runat="server" style="background-color: #afafaf; font-weight: bold;">
                                <th id="Th0" runat="server" width="1%">
                                    <div style="padding: 2px;">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_OnCheckedChanged" ToolTip="Seleziona tutto" />
                                    </div>
                                </th>                            
                                <th id="Th1" runat="server" width="20%">
                                    <div style="padding: 2px;">
                                        Compartimento</div>
                                </th>
                                <th id="Th2" runat="server" width="25%">
                                    <div style="padding: 2px;">
                                        Linea</div>
                                </th>
                                <th id="Th3" runat="server" width="25%">
                                    <div style="padding: 2px;">
                                        Stazione</div>
                                </th>
                                <th id="Th4" runat="server" width="15%">
                                    <div style="padding: 2px;">
                                        Sistema</div>
                                </th>
                                <th id="Th5" runat="server" width="5%" style="text-align: center;">
                                    Notifiche e-mail
                                </th>
                            </tr>
                            <tr runat="server" id="itemPlaceholder" />
                        </table>
                        <asp:DataPager ID="dpagGrid" runat="server" PageSize="20" style="width: 100%; display: block;
                            font-size: small; font-weight: bold; background-color: #afafaf;  text-align: center; padding: 2px;">
                            <Fields>
                                <asp:NumericPagerField NumericButtonCssClass="ListViewHeader" CurrentPageLabelCssClass="ListViewHeader"
                                    NextPreviousButtonCssClass="ListViewHeader" />
                            </Fields>
                        </asp:DataPager>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr id="Tr2" runat="server" style="background-color: #4d4d4d;">
                            <td>
                                <div style="padding: 2px;">
                                    <asp:CheckBox ID="chkSelect" runat="server" 
                                    AutoPostBack="True" OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                </div>
                            </td>                            
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblRegionName" runat="server" Text='<%# Eval("RegionName") %>' />
                                </div>
                            </td>
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("ZoneName") %>' />
                                </div>
                            </td>
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblNodeName" runat="server" Text='<%# Eval("NodeName") %>' />
                                </div>
                            </td>
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblSystemName" runat="server" Text='<%# Eval("SystemName") %>' />
                                </div>
                            </td>
                            <td style="text-align: center;">
                                <div style="padding: 2px;">
                                    <asp:Image runat="server" ID="imgNotificationMailContacts" ImageUrl="~/IMG/Interfaccia/Grids/mail_replayall.png"
                                        title='<%# Eval("NotificationContacts")%>' />
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr id="Tr2" runat="server" style="background-color: #333333;">
                            <td>
                                <div style="padding: 2px;">
                                    <asp:CheckBox ID="chkSelect" runat="server" 
                                    AutoPostBack="True" OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                </div>
                            </td>                        
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblRegionName" runat="server" Text='<%# Eval("RegionName") %>' />
                                </div>
                            </td>
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("ZoneName") %>' />
                                </div>
                            </td>
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblNodeName" runat="server" Text='<%# Eval("NodeName") %>' />
                                </div>
                            </td>
                            <td>
                                <div style="padding: 2px;">
                                    <asp:Label ID="lblSystemName" runat="server" Text='<%# Eval("SystemName") %>' />
                                </div>
                            </td>
                            <td style="text-align: center;">
                                <div style="padding: 2px;">
                                    <asp:Image runat="server" ID="imgNotificationMailContacts" ImageUrl="~/IMG/Interfaccia/Grids/mail_replayall.png"
                                        title='<%# Eval("NotificationContacts")%>' />
                                </div>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnContactAssociate" Text="Associa destinatari"
        Style="float: left; margin-top: 10px; display: none;" />
    <act:ModalPopupExtender runat="server" ID="mpeContactAssociate" BehaviorID="mpeContactAssociateBehavior"
        TargetControlID="btnContactAssociate" PopupControlID="popupContactAssociate"
        BackgroundCssClass="ModalBackground" DropShadow="False" RepositionMode="RepositionOnWindowScroll">
    </act:ModalPopupExtender>
    <asp:Panel runat="server" CssClass="modalPopup" ID="popupContactAssociate"
        Style="display: none; background-color: #9A9A9A; border-width: 2px; border-style: solid;
        border-color: #9A9A9A; padding: 3px; width: 800px;">
        <asp:UpdatePanel ID="updContactAssociate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="text-align: left; margin: 10px 0 10px">
                    <asp:TextBox ID="txtSearchContact" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                    <asp:Button ID="btnSearchConcat" runat="server" Text="Cerca" OnClick="btnSearchContact_OnClick"/>
                    <br/><asp:label runat="server" ID="lblSearchContactResult" style="color: White; font-size: x-small" ></asp:label>
                </div>                
                <asp:GridView ID="grdContacts" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                    SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ContactId,ObjectStatusId" DataSourceID="sdsContacts"
                    Width="100%" ShowFooter="False" PageSize="20" EmptyDataText="Nessun destinatario presente"
                    AllowPaging="true" CssClass="modalGrid" OnRowDataBound="grdContacts_OnRowDataBound" OnPageIndexChanging="grdContacts_OnPageIndexChanging">
                    <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                    <HeaderStyle CssClass="modalGridTh" />
                    <RowStyle CssClass="modalGridRow" />
                    <AlternatingRowStyle CssClass="modalGridRowA" />
                    <PagerStyle CssClass="modalGridPager" />
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllContacts" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAllContacts_OnCheckedChanged" ToolTip="Seleziona tutto" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelectedContact" runat="server" Checked='<%# Eval("Associated")%>' />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="RegionName" HeaderText="Compartimento" ReadOnly="True"
                            SortExpression="RegionName">
                            <ItemStyle Width="30%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ContactName" HeaderText="Nome" ReadOnly="True">
                            <ItemStyle Width="35%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="True">
                            <ItemStyle Width="30%" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sdsContacts" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                    SelectCommand="gris_GetMailContactsAssociatedToObjectStatusIdByRegId" SelectCommandType="StoredProcedure"
                    OnSelecting="sdsContacts_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="ObjectStatusId" />
                        <asp:Parameter Name="RegIds" Type="String" />
                        <asp:Parameter Name="Search" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Button runat="server" ID="btnCancelContactAssociate" Text="Chiudi" Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-right: 10px; margin-top: 10px;"
                    OnClick="btnCancelContactAssociate_Click" OnClientClick="hideModalPopupViaClient('mpeContactAssociateBehavior');"
                    ToolTip="Chiudi" />
                <asp:Button runat="server" ID="btnSaveContactAssociate" Text="Associa Destinatari" Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-top: 10px;" OnClick="btnSaveContactAssociate_Click"
                    OnClientClick="hideModalPopupViaClient('mpeContactAssociateBehavior');"
                    ToolTip="Associa i soli contatti selezionati ai sistemi, se non ci sono contatti selezionari rimuove tutte le associazioni." />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:ObjectDataSource ID="odsNotificationMailContacts" runat="server" SelectMethod="GetData"
        TypeName="GrisSuite.Data.Gris.NotificationMailContactsDSTableAdapters.NotificationMailContactsTableAdapter"
        OnSelecting="odsNotificationMailContacts_Selecting" OnSelected="odsNotificationMailContacts_Selected">
        <SelectParameters>
            <asp:Parameter Name="RegIds" Type="String" />
            <asp:Parameter Name="Search" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br/><br/>
</asp:Content>
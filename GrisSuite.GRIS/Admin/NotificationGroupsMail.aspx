﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="NotificationGroupsMail.aspx.cs" Inherits="Admin_NotificationGroupsMail" Title="Gestione Gruppi Notifiche Mail"
    StylesheetTheme="Gris" %>



<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="text-align: left; color: White; font-weight: bold;">
        Gestione Gruppi Notifiche Mail</div>
    
    
    <div style="text-align: left; margin: 10px 0 10px">
        <asp:TextBox ID="txtSearch" runat="server" MaxLength="128" Width="300px" Style="float: left; margin-right: 0px" CssClass="txtMyInput"></asp:TextBox>
        <asp:Button ID="btnSearch"  runat="server" Text="Cerca" OnClientClick="Func();return false;" Style="float: left; margin-right: 20px; height: 22px;"  />
    </div>    



    <div id="tableContainer">

    </div>


    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
     <script src="NotificationGroupMailManagerClass.js" type="text/javascript"></script>
    
    
    <script type="text/javascript">
        var notificationGroupsDataManager = new NotificationGroupsManagerClass($('#tableContainer'));


        $(document).ready(function () {

            //console.log("ready");
            notificationGroupsDataManager.loadData();
        });


        function Func() {
            //console.log("chiamato");
            notificationGroupsDataManager.loadData();
        } 
    
    
    </script>

</asp:Content>



﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.ServerLicensesDSTableAdapters;

public partial class Admin_ServerLicenses : Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected void sdsGetServersLicensesByStatusNL_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@SrvID"].Value = (this.txtSearchSrvIDNL.Text.Trim().Length == 0 ? -1 : long.Parse(this.txtSearchSrvIDNL.Text.Trim()));
		e.Command.Parameters["@IP"].Value = (this.txtSearchIPNL.Text.Trim().Length == 0 ? "%%" : this.txtSearchIPNL.Text.Trim());
		e.Command.Parameters["@HardwareProfile"].Value = (this.txtSearchHardwareProfileNL.Text.Trim().Length == 0
			? "%%"
			: this.txtSearchHardwareProfileNL.Text.Trim());
		e.Command.Parameters["@IsLicensed"].Value = ServerLicenseStatus.NotLicensed;
	}

	protected void imgbSearchNL_Click(object sender, ImageClickEventArgs e)
	{
		this.RebindPanelServersNotLicensed();
	}

	protected void grdSrvNL_RowCommand(object sender, GridViewCommandEventArgs e)
	{
		switch (e.CommandName)
		{
			case "SetServerLicense":
				if (GrisPermissions.IsUserInGrisClientLicenseAdminGroup())
				{
					int serverLicenseID = int.Parse(e.CommandArgument.ToString());
					ServersLicensesByStatusTableAdapter ta = new ServersLicensesByStatusTableAdapter();
					ta.UpdateServerLicense(serverLicenseID, (byte) ServerLicenseStatus.Licensed, HttpContext.Current.User.Identity.Name);
				}
				break;
			case "DeleteServerNL":
				if (GrisPermissions.IsUserInGrisClientLicenseAdminGroup())
				{
					int serverLicenseID = int.Parse(e.CommandArgument.ToString());
					ServersLicensesByStatusTableAdapter ta = new ServersLicensesByStatusTableAdapter();
					ta.DeleteServerLicense(serverLicenseID, (byte) ServerLicenseStatus.NotLicensed, HttpContext.Current.User.Identity.Name);
				}
				break;
		}

		this.RebindAllPanels();
	}

	protected void sdsGetServersLicensesByStatusL_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@SrvID"].Value = (this.txtSearchSrvIDL.Text.Trim().Length == 0 ? -1 : long.Parse(this.txtSearchSrvIDL.Text.Trim()));
		e.Command.Parameters["@IP"].Value = (this.txtSearchIPL.Text.Trim().Length == 0 ? "%%" : this.txtSearchIPL.Text.Trim());
		e.Command.Parameters["@HardwareProfile"].Value = (this.txtSearchHardwareProfileL.Text.Trim().Length == 0
			? "%%"
			: this.txtSearchHardwareProfileL.Text.Trim());
		e.Command.Parameters["@IsLicensed"].Value = ServerLicenseStatus.Licensed;
	}

	protected void imgbSearchL_Click(object sender, ImageClickEventArgs e)
	{
		this.RebindPanelServersLicensed();
	}

	protected void grdSrvL_RowCommand(object sender, GridViewCommandEventArgs e)
	{
		switch (e.CommandName)
		{
			case "RemoveServerLicense":
				if (GrisPermissions.IsUserInGrisClientLicenseAdminGroup())
				{
					int serverLicenseID = int.Parse(e.CommandArgument.ToString());
					ServersLicensesByStatusTableAdapter ta = new ServersLicensesByStatusTableAdapter();
					ta.UpdateServerLicense(serverLicenseID, (byte) ServerLicenseStatus.NotLicensed, HttpContext.Current.User.Identity.Name);
				}
				break;
		}

		this.RebindAllPanels();
	}

	private void RebindPanelServersNotLicensed()
	{
		this.grdSrvNL.DataBind();
		this.updSrvNL.Update();
	}

	private void RebindPanelServersLicensed()
	{
		this.grdSrvL.DataBind();
		this.updSrvL.Update();
	}

	private void RebindPanelLicensing()
	{
		this.grdLicensing.DataBind();
		this.updLicensing.Update();
	}

	private void RebindAllPanels()
	{
		this.RebindPanelLicensing();
		this.RebindPanelServersNotLicensed();
		this.RebindPanelServersLicensed();
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Properties.PermissionsTableAdapters;

public partial class Admin_Contacts : Page
{
    private readonly Dictionary<long, string> _listaCompartimenti = new Dictionary<long, string>();

    protected void Page_Init(object sender, EventArgs e)
    {
        foreach (long regionId in GrisPermissions.VisibleRegions(PermissionLevel.AllLevels))
        {
            this._listaCompartimenti.Add(regionId, string.Format("Compartimento di {0}", Enum.GetName(typeof(DatabaseRegCodes), regionId).Replace("_", " ")));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {}

    protected void lvwContacts_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            DropDownList cboRegionName = e.Item.FindControl("cboRegionName") as DropDownList;

            if (cboRegionName != null)
            {
                cboRegionName.DataSource = this._listaCompartimenti;
                cboRegionName.DataBind();
            }

            ImageButton delButton = e.Item.FindControl("imgDelete") as ImageButton;
            if (delButton != null)
            {
                delButton.ImageUrl = (delButton.Enabled) ? delButton.ImageUrl : delButton.ImageUrl.Replace(".gif", "_disabled.gif");
            }
        }
    }

    protected void lvwContacts_ItemCreated(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.InsertItem)
        {
            DropDownList cboRegionName = e.Item.FindControl("cboRegionName") as DropDownList;

            if (cboRegionName != null)
            {
                cboRegionName.DataSource = this._listaCompartimenti;
                cboRegionName.DataBind();
            }
        }
    }

    protected void lvwContacts_ItemInserting(object sender, ListViewInsertEventArgs e)
    {
        if (this.IsValid)
        {
            DropDownList cboRegionName = e.Item.FindControl("cboRegionName") as DropDownList;
            TextBox txtContactName = e.Item.FindControl("txtContactName") as TextBox;
            TextBox txtPhoneNumber = e.Item.FindControl("txtPhoneNumber") as TextBox;
            TextBox txtEmail = e.Item.FindControl("txtEmail") as TextBox;
            TextBox txtWindowsUser = e.Item.FindControl("txtWindowsUser") as TextBox;

            if (cboRegionName == null || txtContactName == null || txtPhoneNumber == null || txtEmail == null || txtWindowsUser == null) return;
            
            e.Values["RegId"] = cboRegionName.SelectedValue;
            e.Values["ContactName"] = txtContactName.Text;
            e.Values["PhoneNumber"] = txtPhoneNumber.Text;
            e.Values["Email"] = txtEmail.Text;
            e.Values["WindowsUser"] = txtWindowsUser.Text;
        }
    }

    protected void lvwContacts_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        if (this.IsValid)
        {
            ListViewDataItem item = this.lvwContacts.Items[e.ItemIndex];

            if (item != null)
            {
                TextBox txtContactName = item.FindControl("txtContactName") as TextBox;
                TextBox txtPhoneNumber = item.FindControl("txtPhoneNumber") as TextBox;
                TextBox txtEmail = item.FindControl("txtEmail") as TextBox;
                TextBox txtWindowsUser = item.FindControl("txtWindowsUser") as TextBox;


                if (txtContactName == null || txtPhoneNumber == null || txtEmail == null || txtWindowsUser == null) return;
                e.NewValues["ContactId"] = Convert.ToInt32(e.Keys["ContactId"]);
                e.NewValues["ContactName"] = txtContactName.Text;
                e.NewValues["PhoneNumber"] = txtPhoneNumber.Text;
                e.NewValues["Email"] = txtEmail.Text;
                e.NewValues["WindowsUser"] = txtWindowsUser.Text;
            }
        }
    }

    protected void lvwContacts_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        e.Values["ContactId"] = e.Keys["ContactId"];
    }

    protected void odsContacts_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["RegIds"] = string.Format("|{0}|", string.Join("|", GrisPermissions.VisibleRegions(PermissionLevel.AllLevels).Select(regionId => regionId.ToString()).ToArray()));
        e.InputParameters["Search"] = txtSearch.Text;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lvwContacts.DataBind();
    }

    protected void odsContacts_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        lblSearchResult.Text = "";

        if (e == null || e.ReturnValue == null || !(e.ReturnValue is DataTable)) return;

        int count = ((DataTable) e.ReturnValue).Rows.Count;
        lblSearchResult.Text = String.Format("#{0} risultat{1}", count, (count == 1)?"o":"i" );
    }
}
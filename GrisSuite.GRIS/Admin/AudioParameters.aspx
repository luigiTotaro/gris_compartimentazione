﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="AudioParameters.aspx.cs" Inherits="Admin_AudioParameters" Title="Parametri Audio"
    StylesheetTheme="Gris" %>


<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="text-align: left; color: White; font-weight: bold;">
        Gestione parametri Audio</div>
    <div id="tableContainer">
    </div>


    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
     <script src="AudioDataManagerClass.js" type="text/javascript"></script>
    
    
    <script type="text/javascript">



        $(document).ready(function () {
            var audioDataManager = new AudioDataManagerClass($('#tableContainer'));
            audioDataManager.loadData();
        });  
    
    
    </script>

</asp:Content>



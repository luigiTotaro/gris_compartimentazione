﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
	CodeFile="ServerLicenses.aspx.cs" Inherits="Admin_ServerLicenses" %>

<%@ Import Namespace="GrisSuite.Common" %>
<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
	<asp:UpdatePanel ID="updLicensing" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel runat="server" ID="pnlLicensing">
				<div class="collapsePanelHeader" style="width: 100%; height: 30px; color: #FFF; font-weight: bold;
					background-color: #808080; text-align: left; font-size: 10pt; margin-top: 5px;
					margin-bottom: 0px;">
					<asp:Label runat="server" ID="lblTitleLicensing" Style="color: White; font-weight: bold;
						text-align: left; font-size:12pt; padding:5px; display:block;" Text="Conteggi licenze" />
				</div>
				<asp:GridView ID="grdLicensing" runat="server" AllowSorting="True" AutoGenerateColumns="False"
					Width="100%" SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ServerLicensingId"
					DataSourceID="sdsGetServersLicensing" ShowFooter="False" EmptyDataText="Nessun dato relativo alle licenze"
					AllowPaging="false" CssClass="modalGrid">
					<HeaderStyle CssClass="modalGridTh" />
					<RowStyle CssClass="modalGridRow" />
					<AlternatingRowStyle CssClass="modalGridRowA" />
					<Columns>
						<asp:BoundField Visible="false" DataField="ServerLicensingId" />
						<asp:BoundField DataField="HardwareProfile" HeaderText="Profilo hardware" ReadOnly="True">
							<ItemStyle Width="25%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="LicensingLastUpdate" HeaderText="Data aggiornamento" ReadOnly="True"
							DataFormatString="{0:dd/MM/yyyy HH.mm}">
							<ItemStyle Width="30%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="AcquiredLicenses" HeaderText="Licenze acquistate" ReadOnly="True">
							<ItemStyle Width="15%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="UsedLicenses" HeaderText="Licenze utilizzate" ReadOnly="True">
							<ItemStyle Width="15%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="AvailableLicenses" HeaderText="Licenze disponibili" ReadOnly="True">
							<ItemStyle Width="15%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>
	<hr style="border: 2px; margin-top: 5px; margin-bottom: 5px" />
	<asp:UpdatePanel ID="updSrvNL" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel ID="pnlSrvNLExp" runat="server" CssClass="collapsePanelHeader" Height="30px">
				<div style="padding: 5px; cursor: pointer; vertical-align: middle;">
					<div style="float: left;">
						<asp:Label runat="server" ID="lblTitleSrvNL" Style="color: White; font-weight: bold;
							text-align: left;" Text="Server privi di licenza" />
					</div>
					<div style="float: right; vertical-align: middle;">
						<asp:ImageButton ID="imgSrvNL" runat="server" ImageUrl="~/IMG/Interfaccia/Collapse.png"
							AlternateText="Mostra lista" />
					</div>
				</div>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlSrvNL" CssClass="collapsePanel" Height="0">
				<div style="color: White; font-weight: normal; text-align: left; font-size: 10pt;
					margin-top: 5px; margin-bottom: 10px;">
					<asp:Label runat="server" ID="lblSearchSrvIDNL" AssociatedControlID="txtSearchSrvIDNL"
						Text="Server ID:" EnableViewState="false" />
					<asp:TextBox runat="server" ID="txtSearchSrvIDNL" Width="150px" />
					<asp:CompareValidator ID="cmpSearchSrvIDNL" runat="server" Operator="DataTypeCheck"
						ToolTip='Inserire un numero intero' ControlToValidate="txtSearchSrvIDNL" Type="Integer"
						Display="Static" ErrorMessage="(!)"></asp:CompareValidator>
					<asp:Label runat="server" ID="lblSearchIPNL" AssociatedControlID="txtSearchIPNL"
						Text="IP:" Style="margin-left: 10px;" EnableViewState="false" />
					<asp:TextBox runat="server" ID="txtSearchIPNL" Width="150px" />
					<asp:Label runat="server" ID="lblSearchHardwareProfileNL" AssociatedControlID="txtSearchHardwareProfileNL"
						Text="Profilo hardware:" Style="margin-left: 10px;" EnableViewState="false" />
					<asp:TextBox runat="server" ID="txtSearchHardwareProfileNL" Width="150px" />
					<asp:ImageButton runat="server" ID="imgbSearchNL" ImageUrl="~/IMG/Interfaccia/Search.png"
						Style="padding-left: 20px; margin-bottom: -5px;" OnClick="imgbSearchNL_Click" />
				</div>
				<asp:GridView ID="grdSrvNL" runat="server" AllowSorting="True" AutoGenerateColumns="False"
					Width="100%" SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ServerLicenseID"
					DataSourceID="sdsGetServersLicensesByStatusNL" ShowFooter="False" PageSize="10"
					EmptyDataText="Nessun server privo di licenza presente e corrispondente alla ricerca"
					AllowPaging="true" CssClass="modalGrid" OnRowCommand="grdSrvNL_RowCommand">
					<PagerSettings Mode="NumericFirstLast" Position="Bottom" />
					<HeaderStyle CssClass="modalGridTh" />
					<RowStyle CssClass="modalGridRow" />
					<AlternatingRowStyle CssClass="modalGridRowA" />
					<PagerStyle CssClass="modalGridPager" />
					<Columns>
						<asp:BoundField Visible="false" DataField="ServerLicenseID" />
						<asp:BoundField DataField="SrvID" HeaderText="Server ID" ReadOnly="True">
							<ItemStyle Width="15%" />
						</asp:BoundField>
						<asp:BoundField DataField="SN" HeaderText="Numero di serie" ReadOnly="True">
							<ItemStyle Width="25%" />
						</asp:BoundField>
						<asp:BoundField DataField="IP" HeaderText="IP" ReadOnly="True">
							<ItemStyle Width="10%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="LicenseLastUpdate" HeaderText="Data aggiornamento" ReadOnly="True"
							DataFormatString="{0:dd/MM/yyyy HH.mm}">
							<ItemStyle Width="12%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="HardwareProfile" HeaderText="Profilo hardware" ReadOnly="True">
							<ItemStyle Width="25%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:TemplateField>
							<ItemTemplate>
								<asp:ImageButton ID="btnSetServerLicense" runat="server" CommandName="SetServerLicense"
									CommandArgument='<%# Eval("ServerLicenseID") %>' ImageUrl="~/IMG/Interfaccia/Lock.png"
									ToolTip="Assegna licenza" Visible='<%# GrisPermissions.IsUserInGrisClientLicenseAdminGroup() %>'>
								</asp:ImageButton>
								<asp:ImageButton ID="btnDeleteServerNL" runat="server" CommandName="DeleteServerNL"
									CommandArgument='<%# Eval("ServerLicenseID") %>' ImageUrl="~/IMG/Interfaccia/Trash2.png"
									ToolTip="Elimina server privo di licenza" Visible='<%# GrisPermissions.IsUserInGrisClientLicenseAdminGroup() %>'
									OnClientClick='<%# string.Format("return confirm(\"Eliminare il server privo di licenza con IP: {0}?\");", Eval("IP"))%>'>
								</asp:ImageButton>
							</ItemTemplate>
							<ItemStyle Width="8%" HorizontalAlign="Center" Wrap="false" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
			<act:CollapsiblePanelExtender ID="cpeSrvNL" runat="Server" TargetControlID="pnlSrvNL"
				ExpandControlID="pnlSrvNLExp" CollapseControlID="pnlSrvNLExp" CollapsedSize="0"
				ExpandedSize="440" Collapsed="False" AutoCollapse="False" AutoExpand="False"
				ScrollContents="False" TextLabelID="lblTitleSrvNL" ImageControlID="imgSrvNL"
				ExpandedImage="~/IMG/Interfaccia/Collapse.png" CollapsedImage="~/IMG/Interfaccia/Expand.png"
				ExpandDirection="Vertical" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<hr style="border: 2px; margin-top: 5px; margin-bottom: 5px" />
	<asp:UpdatePanel ID="updSrvL" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel ID="pnlSrvLExp" runat="server" CssClass="collapsePanelHeader" Height="30px">
				<div style="padding: 5px; cursor: pointer; vertical-align: middle;">
					<div style="float: left;">
						<asp:Label runat="server" ID="lblTitleSrvL" Style="color: White; font-weight: bold;
							text-align: left;" Text="Server forniti di licenza" />
					</div>
					<div style="float: right; vertical-align: middle;">
						<asp:ImageButton ID="imgSrvL" runat="server" ImageUrl="~/IMG/Interfaccia/Expand.png"
							AlternateText="Mostra lista" />
					</div>
				</div>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlSrvL" CssClass="collapsePanel" Height="0">
				<div style="color: White; font-weight: normal; text-align: left; font-size: 10pt;
					margin-top: 5px; margin-bottom: 10px;">
					<asp:Label runat="server" ID="lblSearchSrvIDL" AssociatedControlID="txtSearchSrvIDL"
						Text="Server ID:" EnableViewState="false" />
					<asp:TextBox runat="server" ID="txtSearchSrvIDL" Width="150px" />
					<asp:CompareValidator ID="cmpSearchSrvIDL" runat="server" Operator="DataTypeCheck"
						ToolTip='Inserire un numero intero' ControlToValidate="txtSearchSrvIDL" Type="Integer"
						Display="Static" ErrorMessage="(!)"></asp:CompareValidator>
					<asp:Label runat="server" ID="lblSearchIPL" AssociatedControlID="txtSearchIPL" Text="IP:"
						Style="margin-left: 10px;" EnableViewState="false" />
					<asp:TextBox runat="server" ID="txtSearchIPL" Width="150px" />
					<asp:Label runat="server" ID="lblSearchHardwareProfileL" AssociatedControlID="txtSearchHardwareProfileL"
						Text="Profilo hardware:" Style="margin-left: 10px;" EnableViewState="false" />
					<asp:TextBox runat="server" ID="txtSearchHardwareProfileL" Width="150px" />
					<asp:ImageButton runat="server" ID="imgbSearchL" ImageUrl="~/IMG/Interfaccia/Search.png"
						Style="padding-left: 20px; margin-bottom: -5px;" OnClick="imgbSearchL_Click" />
				</div>
				<asp:GridView ID="grdSrvL" runat="server" AllowSorting="True" AutoGenerateColumns="False"
					Width="100%" SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ServerLicenseID"
					DataSourceID="sdsGetServersLicensesByStatusL" ShowFooter="False" PageSize="10"
					EmptyDataText="Nessun server fornito di licenza presente e corrispondente alla ricerca"
					AllowPaging="true" CssClass="modalGrid" OnRowCommand="grdSrvL_RowCommand">
					<PagerSettings Mode="NumericFirstLast" Position="Bottom" />
					<HeaderStyle CssClass="modalGridTh" />
					<RowStyle CssClass="modalGridRow" />
					<AlternatingRowStyle CssClass="modalGridRowA" />
					<PagerStyle CssClass="modalGridPager" />
					<Columns>
						<asp:BoundField Visible="false" DataField="ServerLicenseID" />
						<asp:BoundField DataField="SrvID" HeaderText="Server ID" ReadOnly="True">
							<ItemStyle Width="15%" />
						</asp:BoundField>
						<asp:BoundField DataField="SN" HeaderText="Numero di serie" ReadOnly="True">
							<ItemStyle Width="25%" />
						</asp:BoundField>
						<asp:BoundField DataField="IP" HeaderText="IP" ReadOnly="True">
							<ItemStyle Width="10%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="LicenseLastUpdate" HeaderText="Data aggiornamento" ReadOnly="True"
							DataFormatString="{0:dd/MM/yyyy HH.mm}">
							<ItemStyle Width="20%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="HardwareProfile" HeaderText="Profilo hardware" ReadOnly="True">
							<ItemStyle Width="25%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:TemplateField>
							<ItemTemplate>
								<asp:ImageButton ID="btnRemoveServerLicense" runat="server" CommandName="RemoveServerLicense"
									CommandArgument='<%# Eval("ServerLicenseID") %>' ImageUrl="~/IMG/Interfaccia/Lock.png"
									ToolTip="Revoca licenza" Visible='<%# GrisPermissions.IsUserInGrisClientLicenseAdminGroup() %>'>
								</asp:ImageButton>
							</ItemTemplate>
							<ItemStyle Width="5%" HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
			<act:CollapsiblePanelExtender ID="cpeSrvL" runat="Server" TargetControlID="pnlSrvL"
				ExpandControlID="pnlSrvLExp" CollapseControlID="pnlSrvLExp" CollapsedSize="0"
				ExpandedSize="440" Collapsed="True" AutoCollapse="False" AutoExpand="False" ScrollContents="False"
				TextLabelID="lblTitleSrvL" ImageControlID="imgSrvL" ExpandedImage="~/IMG/Interfaccia/Collapse.png"
				CollapsedImage="~/IMG/Interfaccia/Expand.png" ExpandDirection="Vertical" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:SqlDataSource ID="sdsGetServersLicensing" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
		SelectCommand="gris_GetServerLicensing" SelectCommandType="StoredProcedure">
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="sdsGetServersLicensesByStatusNL" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
		SelectCommand="gris_GetServersLicensesByStatus" SelectCommandType="StoredProcedure"
		OnSelecting="sdsGetServersLicensesByStatusNL_Selecting">
		<SelectParameters>
			<asp:Parameter Name="SrvID" Type="Int64" />
			<asp:Parameter Name="IP" Type="String" />
			<asp:Parameter Name="HardwareProfile" Type="String" />
			<asp:Parameter Name="IsLicensed" Type="Byte" />
		</SelectParameters>
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="sdsGetServersLicensesByStatusL" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
		SelectCommand="gris_GetServersLicensesByStatus" SelectCommandType="StoredProcedure"
		OnSelecting="sdsGetServersLicensesByStatusL_Selecting">
		<SelectParameters>
			<asp:Parameter Name="SrvID" Type="Int64" />
			<asp:Parameter Name="IP" Type="String" />
			<asp:Parameter Name="HardwareProfile" Type="String" />
			<asp:Parameter Name="IsLicensed" Type="Byte" />
		</SelectParameters>
	</asp:SqlDataSource>
	<br />
	<br />
</asp:Content>

﻿<%@ WebHandler Language="C#" Class="groupsService" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;

public class groupsService : IHttpHandler {


        
    public void ProcessRequest (HttpContext context) {


        HttpResponse r = context.Response;

        string action = context.Request.QueryString["action"];
        string searchText = context.Request.QueryString["searchText"];
        

        if (action.Equals("GetDataSms"))
        {
            List<NotificationObjectsGroups> GroupsData = GUtility.GetNotificationObjectDataSms(searchText);

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(GroupsData);
            
            
            /*
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Console.WriteLine(reader[0].ToString());
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }            
            */

            r.Clear();
            r.ContentType = "application/json";
            r.Write(json);
            r.End();                 
        }
        else if (action.Equals("GetDataMail"))
        {
            List<NotificationObjectsGroups> GroupsData = GUtility.GetNotificationObjectDataMail(searchText);

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(GroupsData);


            /*
            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    Console.WriteLine(reader[0].ToString());
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }            
            */

            r.Clear();
            r.ContentType = "application/json";
            r.Write(json);
            r.End();
        }            
        else if (action.Equals("SetGroupDataSms")) 
        {
            
            var ObjectStatusId = context.Request.QueryString["id"];
            var WindowsGroups = context.Request.QueryString["WindowsGroups"];

            int res = GUtility.UpdateNotificationGroupDataSms(ObjectStatusId, WindowsGroups);
            GrisSuite.Common.GrisPermissions.GroupsCache.Reset();      
        
        }
        else if (action.Equals("SetGroupDataMail"))
        {

            var ObjectStatusId = context.Request.QueryString["id"];
            var WindowsGroups = context.Request.QueryString["WindowsGroups"];

            int res = GUtility.UpdateNotificationGroupDataMail(ObjectStatusId, WindowsGroups);
            GrisSuite.Common.GrisPermissions.GroupsCache.Reset();

        }          
        
        /*
        string regId = context.Request.QueryString["regId"];        
        
        //string regId = "281474976645132";
        string[] sTmp = GUtility.GetRegionExtData(regId);

        string json = "{\"brokerIp\":\"" + sTmp[0] + "\",\"username\":\"" + sTmp[1] + "\",\"password\":\"" + sTmp[2] + "\"}";
        r.Clear();
        r.ContentType = "application/json; charset=utf-8";
        r.Write(json);
        r.End();        
        
        */
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}


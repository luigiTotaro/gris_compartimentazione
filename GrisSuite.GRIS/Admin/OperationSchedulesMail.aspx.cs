﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using GrisSuite.Common;

public partial class Admin_OperationSchedulesMail : Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected void sdsMail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@OperationScheduleTypeId"].Value = OperationScheduleType.StreamFieldMail;
	}
}
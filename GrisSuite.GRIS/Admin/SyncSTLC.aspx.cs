using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using GrisSuite;
using GrisSuite.Common;

public partial class Admin_SyncSTLC : System.Web.UI.Page
{
	protected void Page_Load ( object sender, EventArgs e )
	{

	}

	protected void gvwServers_SelectedIndexChanged ( object sender, EventArgs e )
	{
		string ip = "";
		const int IP_COL = 2;

		ip = this.gvwServers.Rows[this.gvwServers.SelectedIndex].Cells[IP_COL].Text;
		this.SyncServer(ip);
	}

	private void SyncServer ( string srvIP )
	{
		string stlcUrl = string.Format(ConfigurationManager.AppSettings["STLCWSTemplateUrl"], srvIP);
		STLCWS.STLCService stlcWS = new STLCWS.STLCService();
		stlcWS.Url = stlcUrl;
		//stlcWS.Url = "http://localhost:54324/Telefin.STLCWS/STLCService.asmx";
		stlcWS.Credentials = System.Net.CredentialCache.DefaultCredentials;

		try
		{
			if ( stlcWS.SendDeviceStatusAndConfigToCentral() )
			{
				//
			}
			else
			{
				//
			}
		}
		catch ( System.Net.WebException exc )
		{
		//
		}
	}
}

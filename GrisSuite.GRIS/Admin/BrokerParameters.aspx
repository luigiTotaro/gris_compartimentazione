﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="BrokerParameters.aspx.cs" Inherits="Admin_BrokerParameters" Title="Parametri Broker"
    StylesheetTheme="Gris" %>


<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="text-align: left; color: White; font-weight: bold;">
        Gestione parametri Broker</div>
    <div id="tableContainer">
    </div>


    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
    <!-- <script src="../JS/bootstrap.min.js" type="text/javascript"></script> -->
    <!--<script src="BrokerDataManager.js" type="text/javascript"></script> -->
     <script src="BrokerDataManagerClass.js" type="text/javascript"></script>
    
    
    <script type="text/javascript">



        $(document).ready(function () {
            var brokerDataManager = new BrokerDataManagerClass($('#tableContainer'));
            brokerDataManager.loadData();
            //loadData();
        });  
    
    
    </script>

</asp:Content>



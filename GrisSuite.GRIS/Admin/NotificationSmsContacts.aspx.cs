﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.NotificationSmsContactsDSTableAdapters;

public partial class Admin_NotificationSmsContacts : Page
{

    protected bool ContactsSelectedInit
    {
        get
        {
            if (ViewState["ContactsSelectedInit"] == null)
            {
                ViewState["ContactsSelectedInit"] = false;
            }
            return (bool)ViewState["ContactsSelectedInit"];
        }
        set { ViewState["ContactsSelectedInit"] = value; }
    }

    protected bool SelectAll
    {
        get
        {
            if (ViewState["SelectAll"] == null)
            {
                ViewState["SelectAll"] = false;
            }
            return (bool)ViewState["SelectAll"];
        }
        set { ViewState["SelectAll"] = value; }
    }

    protected bool SelectAllContacts
    {
        get
        {
            if (ViewState["SelectAllContacts"] == null)
            {
                ViewState["SelectAllContacts"] = false;
            }
            return (bool)ViewState["SelectAllContacts"];
        }
        set { ViewState["SelectAllContacts"] = value; }
    }

    protected HashSet<long> ContactsSelected
    {
        get
        {
            if (ViewState["ContactsSelected"] == null)
            {
                ViewState["ContactsSelected"] = new HashSet<long>();
            }
            return (HashSet<long>)ViewState["ContactsSelected"];

        }
    }


    protected bool SelectAllNodes
    {
        get
        {
            if (ViewState["SelectAllNodes"] == null)
            {
                ViewState["SelectAllNodes"] = false;
            }
            return (bool)ViewState["SelectAllNodes"];
        }
        set { ViewState["SelectAllNodes"] = value; }
    }

    protected HashSet<Guid> NodesSelected
    {
        get
        {
            if (ViewState["NodesSelected"] == null)
            {
                ViewState["NodesSelected"] = new HashSet<Guid>();
            }
            return (HashSet<Guid>)ViewState["NodesSelected"];

        }
    }

    protected HashSet<Guid> NotificationsSelected
    {
        get
        {
            if (ViewState["NotificationsSelected"] == null)
            {
                ViewState["NotificationsSelected"] = new HashSet<Guid>();
            }
            return (HashSet<Guid>)ViewState["NotificationsSelected"];

        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    protected void odsNotificationSmsContacts_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["RegIds"] = string.Format("|{0}|",
                                                    string.Join("|",
                                                                GrisPermissions.VisibleRegions(PermissionLevel.AllLevels).Select(
                                                                    regionId => regionId.ToString()).ToArray()));
        e.InputParameters["Search"] = txtSearch.Text;
    }

    protected void lvwNotificationSmsContacts_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType != ListViewItemType.DataItem) return;
        
        DataRowView dataRowView = (DataRowView) ((ListViewDataItem) e.Item).DataItem;

        if (dataRowView == null) return;
        
        CheckBox checkBox = (CheckBox)e.Item.FindControl("chkSelect");
        if (checkBox != null)
        {
            Guid objectStatusId = (Guid)dataRowView["ObjectStatusID"];
            checkBox.Checked = NotificationsSelected.Contains(objectStatusId);
        }

        string NotificationSmsContacts = dataRowView["NotificationContacts"].ToString();
        Image imgNotificationSmsContacts = (Image)e.Item.FindControl("imgNotificationSmsContacts");
        imgNotificationSmsContacts.Visible = !(string.IsNullOrEmpty(NotificationSmsContacts));
    }

    protected void sdsNodes_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@RegIds"].Value = string.Format("|{0}|", string.Join("|",
                                                                                   GrisPermissions.VisibleRegions(PermissionLevel.AllLevels).Select(
                                                                                       regionId => regionId.ToString()).ToArray()));
        e.Command.Parameters["@Search"].Value = this.txtSearchNode.Text;

    }

    protected void sdsContacts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@ObjectStatusId"].Value = (this.ViewState["LastContactAssociate"] != null ? (Guid)this.ViewState["LastContactAssociate"] : Guid.Empty);
        e.Command.Parameters["@RegIds"].Value = string.Format("|{0}|", string.Join("|",
                                                                                   GrisPermissions.VisibleRegions(PermissionLevel.AllLevels).Select(
                                                                                       regionId => regionId.ToString()).ToArray()));
        e.Command.Parameters["@Search"].Value = this.txtSearchContact.Text;
    }

    protected void btnSaveAssociateNode_Click(object sender, EventArgs e)
    {
        UpdateNodesSelected();

        var ta = new NotificationSmsContactsTableAdapter();

        foreach (Guid objectStatusId in NodesSelected)
        {
            ta.UpdateSmsNotification(objectStatusId, true);
        }

        SelectAllNodes = false;
        NodesSelected.Clear();

        this.lvwNotificationSmsContacts.DataBind();
        this.updGrid.Update();

        this.grdNodes.PageIndex = 0;
        this.grdNodes.DataBind();
        this.updAssociateNode.Update();
    }

    protected void btnCancelAssociateNode_Click(object sender, EventArgs e)
    {
        SelectAllNodes = false;
        NodesSelected.Clear();
        this.grdNodes.PageIndex = 0;
        this.grdNodes.DataBind();
        this.updAssociateNode.Update();
    }

    protected void btnSaveContactAssociate_Click(object sender, EventArgs e)
    {
        UpdateContactsSelected();

        var ta = new NotificationSmsContactsTableAdapter();

        foreach (Guid objectStatusId in NotificationsSelected)
        {
            ta.DeleteNotificationContacts(objectStatusId,null);
            foreach (long contactId in ContactsSelected)
            {
                ta.UpdateNotificationContacts(true, objectStatusId, contactId);
            }
        }

        SelectAllContacts = false;
        ContactsSelected.Clear();

        this.lvwNotificationSmsContacts.DataBind();
        this.updGrid.Update();

        this.grdContacts.PageIndex = 0;

        this.grdNodes.PageIndex = 0;
        this.grdNodes.DataBind();
        this.updAssociateNode.Update();
    }

    protected void btnCancelContactAssociate_Click(object sender, EventArgs e)
    {
        SelectAllContacts = false;
        ContactsSelected.Clear();
        this.lvwNotificationSmsContacts.DataBind();
        this.updGrid.Update();

        this.grdContacts.PageIndex = 0;

        this.grdNodes.PageIndex = 0;
        this.grdNodes.DataBind();
        this.updAssociateNode.Update();
    }

    protected void btnSearchNode_Click(object sender, EventArgs e)
    {
        grdNodes.PageIndex = 0;
        SelectAllNodes = false;
        NodesSelected.Clear();
        this.grdNodes.DataBind();
        this.updContactAssociate.Update();
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        NotificationsSelected.Clear();
        lvwNotificationSmsContacts.DataBind();
    }

    protected void odsNotificationSmsContacts_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        lblSearchResult.Text = "";

        if (e == null || !(e.ReturnValue is DataTable)) return;

        DataTable nodes = (DataTable) e.ReturnValue;

        int count = nodes.Rows.Count;

        if (SelectAll)
        {
            NotificationsSelected.Clear();
            foreach (DataRow row in nodes.Rows)
            {
                NotificationsSelected.Add((Guid) row["ObjectStatusId"]);
            }
        }

        lblSearchResult.Text = String.Format("#{0} risultat{1}", count, (count == 1) ? "o" : "i");
    }

    protected void sdsNodes_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        lblSearchNodeResult.Text = "Nessun risultato";
        if (e == null || e.AffectedRows == 0) return;

        lblSearchNodeResult.Text = String.Format("#{0} risultat{1}", e.AffectedRows, (e.AffectedRows == 1) ? "o" : "i");
    }

    protected void grdNodes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        UpdateNodesSelected();
    }

    private void UpdateNodesSelected()
    {
        foreach (GridViewRow row in this.grdNodes.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow || this.grdNodes.DataKeys[row.RowIndex] == null) continue;

            bool selected = ((CheckBox) row.FindControl("chkSelectedNode")).Checked;
            Guid objectStatusId = (Guid) this.grdNodes.DataKeys[row.RowIndex].Value;
            if (selected)
                NodesSelected.Add(objectStatusId);
            else
                NodesSelected.Remove(objectStatusId);
        }
    }

    protected void grdNodes_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox selectAll = e.Row.FindControl("chkSelectAllNodes") as CheckBox;
            if (selectAll != null)
            {
                selectAll.Checked = SelectAllNodes;
                selectAll.ToolTip = SelectAllNodes ? "Deseleziona tutto" : "Seleziona tutto";
            }
        }

        if (e.Row.RowType != DataControlRowType.DataRow || this.grdNodes.DataKeys[e.Row.RowIndex] == null) return;

        CheckBox checkBox = e.Row.FindControl("chkSelectedNode") as CheckBox;
        Guid objectStatusId = (Guid)this.grdNodes.DataKeys[e.Row.RowIndex].Value;
        if (checkBox != null) checkBox.Checked = NodesSelected.Contains(objectStatusId);
    }

    protected void chkSelectAllNodes_OnCheckedChanged(object sender, EventArgs e)
    {
        this.NodesSelected.Clear();
        CheckBox chkAll = ((CheckBox) sender);
        SelectAllNodes = chkAll.Checked;
        if (SelectAllNodes)
        {
            DataView dv = sdsNodes.Select(DataSourceSelectArguments.Empty) as DataView;
            if (dv != null)
            {
                HashSet<Guid> nodesSelected = null;
                nodesSelected = this.NodesSelected;
                foreach (DataRow row in dv.Table.Rows)
                {
                    nodesSelected.Add((Guid) row["ObjectStatusId"]);
                }
            }
        }

        grdNodes.DataBind();
    }

    protected void chkSelectAll_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkSelectAll = (CheckBox) sender;
        SelectAll = chkSelectAll.Checked;
        if (!SelectAll) NotificationsSelected.Clear();
        lvwNotificationSmsContacts.DataBind();
    }


    protected void lvwNotificationSmsContacts_OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        UpdateNotificationsSelected();
    }


    private void UpdateNotificationsSelected()
    {
        foreach (ListViewDataItem item in lvwNotificationSmsContacts.Items)
        {
            if (item.ItemType != ListViewItemType.DataItem || lvwNotificationSmsContacts.DataKeys[item.DisplayIndex] == null) continue;
            
            bool selected = ((CheckBox)item.FindControl("chkSelect")).Checked;
            Guid objectStatusId = (Guid) lvwNotificationSmsContacts.DataKeys[item.DisplayIndex].Value;
            if (selected)
                NotificationsSelected.Add(objectStatusId);
            else
                NotificationsSelected.Remove(objectStatusId);
        }
    }

    protected void chkSelect_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkSelect = sender as CheckBox;
        if (chkSelect == null) return;
        ListViewDataItem item = chkSelect.NamingContainer as ListViewDataItem;
        if (item == null) return;
        Guid objectStatusId = (Guid)lvwNotificationSmsContacts.DataKeys[item.DisplayIndex].Value;
            
        if (chkSelect.Checked)
            NotificationsSelected.Add(objectStatusId);
        else
            NotificationsSelected.Remove(objectStatusId);
    }

    protected void btnDisassociateNodes_OnClick(object sender, EventArgs e)
    {
        Guid[] objectStatusIDs = NotificationsSelected.ToArray();

        for (int i = objectStatusIDs.Length - 1; i >= 0; i--)
        {
            Guid objectStatusID = objectStatusIDs[i];
            var ta = new NotificationSmsContactsTableAdapter();
            ta.UpdateSmsNotification(objectStatusID, false);
            NotificationsSelected.Remove(objectStatusID);
            
        }

        this.lvwNotificationSmsContacts.DataBind();
        this.grdNodes.DataBind();
        this.updAssociateNode.Update();
    }

    protected void btnAddContacts_OnClick(object sender, EventArgs e)
    {
        this.ContactsSelectedInit = true;
        this.ViewState["LastContactAssociate"] = new Guid();    
        if (NotificationsSelected.Count == 1)
        {
            this.ViewState["LastContactAssociate"] = NotificationsSelected.ToArray()[0];    
        }
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
        this.mpeContactAssociate.Show();
    }

    protected void btnSearchConcat_OnClick(object sender, EventArgs e)
    {
        this.ContactsSelectedInit = true;
        grdContacts.PageIndex = 0;
        SelectAllContacts = false;
        ContactsSelected.Clear();
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
    }

    protected void chkSelectAllContacts_OnCheckedChanged(object sender, EventArgs e)
    {
        ContactsSelected.Clear();
        CheckBox chkAll = ((CheckBox)sender);
        SelectAllContacts = chkAll.Checked;
        if (SelectAllContacts)
        {
            DataView dv = sdsContacts.Select(DataSourceSelectArguments.Empty) as DataView;
            if (dv != null)
            {
                foreach (DataRow row in dv.Table.Rows)
                {
                    ContactsSelected.Add((long)row["ContactId"]);
                }
            }
        }
        grdContacts.DataBind();
    }

    protected void grdContacts_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox selectAll = e.Row.FindControl("chkSelectAllContacts") as CheckBox;
            if (selectAll != null)
            {
                selectAll.Checked = SelectAllContacts;
                selectAll.ToolTip = SelectAllContacts ? "Deseleziona tutto" : "Seleziona tutto";
            }
        }

        if (e.Row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[e.Row.RowIndex] == null) return;

        CheckBox checkBox = e.Row.FindControl("chkSelectedContact") as CheckBox;

        if (this.ContactsSelectedInit)
        {
            this.ContactsSelectedInit = false;
            DataTable table = ((DataRowView) e.Row.DataItem).Row.Table;
            foreach (DataRow row in table.Rows)
            {
                if (!(Boolean)row["Associated"]) break;
                ContactsSelected.Add((long) row["ContactId"]);
            }

        }

        long contactId = (long)this.grdContacts.DataKeys[e.Row.RowIndex]["ContactId"];
        if (checkBox != null) checkBox.Checked = ContactsSelected.Contains(contactId);
    }

    protected void sdsContacts_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        lblSearchContactResult.Text = "Nessun risultato";
        if (e == null || e.AffectedRows == 0) return;

        lblSearchContactResult.Text = String.Format("#{0} risultat{1}", e.AffectedRows, (e.AffectedRows == 1) ? "o" : "i");
        
    }

    protected void updGrid_OnPreRender(object sender, EventArgs e)
    {
        btnDisassociateNodes.Enabled = (NotificationsSelected.Count > 0);
        String textStazioni = "stazion" + (NotificationsSelected.Count == 1 ? "e" : "i");
        btnDisassociateNodes.Text = string.Format("Rimuovi {0}", textStazioni);
        lblConfirmDisassociateNodes.Text = string.Format("Rimuovere le notifiche sms da {0} {1} ?",
            NotificationsSelected.Count, textStazioni);

        btnSaveContactAssociate.Text = string.Format("Associa destinatari" + (NotificationsSelected.Count == 1 ? "" : " a {0} {1}"), NotificationsSelected.Count, textStazioni);
        btnAddContacts.Enabled = (NotificationsSelected.Count > 0);
    }

    protected void btnAssociateNode_OnClick(object sender, EventArgs e)
    {
        mpeAssociateNode.Show();
    }


    protected void grdContacts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        UpdateContactsSelected();
    }

    private void UpdateContactsSelected()
    {
        foreach (GridViewRow row in this.grdContacts.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[row.RowIndex] == null) continue;

            bool selected = ((CheckBox)row.FindControl("chkSelectedContact")).Checked;
            long contactId = (long)(this.grdContacts.DataKeys[row.RowIndex]["ContactId"]);
            if (selected)
                ContactsSelected.Add(contactId);
            else
                ContactsSelected.Remove(contactId);
        }
    }

}
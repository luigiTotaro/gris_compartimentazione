﻿<%@ page title="" language="C#" masterpagefile="~/Admin/AdminMasterPage.master" autoeventwireup="true"
	codefile="ResourcesPermissions.aspx.cs" inherits="Admin_ResourcesPermissions" %>

<asp:content id="cntFunctionArea" contentplaceholderid="cphFunctionArea" runat="Server">
    <script type="text/javascript">
        function hideModalPopupViaClient(id) {
            var modalPopupBehavior = $find(id);
            modalPopupBehavior.hide();
        }

        function controlEnter(obj, event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                document.getElementById(obj).click();
                return false;
            } else {
                 return true;
            }
        }

    </script>
	<style type="text/css">
		.colWidth TD {
			width: 20%;
		}
	</style>
	<div style="color: White; font-weight: bold; text-align: left; margin-left:5px;">
		<asp:Label runat="server" id="lblTitle"></asp:Label></div>
	<div id="divResources" style="font-size:small;color:#ffffff;">
		Nome: <asp:textbox id="txtResourceName" runat="server"></asp:textbox>&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Gruppo: <asp:textbox id="txtGroupName" runat="server"></asp:textbox>
		<asp:button id="btnSearch" runat="server" text="Cerca" enableviewstate="false" 
			onclick="btnSearch_Click" />
	</div>
	<asp:updatepanel id="updGrid" runat="server" updatemode="Conditional">
		<contenttemplate>
			<div id="divGrid" style="margin-left: 5px; margin-right: 5px;">
				<asp:repeater id="rptCategories" runat="server" onitemdatabound="rptCategories_ItemDataBound">
					<headertemplate>
						<table cellpadding="0" cellspacing="0" style="width: 100%">
					</headertemplate>
					<itemtemplate>
						<tr>
							<td style="padding-top:15px;padding-bottom:10px;text-align:left;color:#ffffff;font-weight:bold;">
								<asp:label id="lblCategory" runat="server" text='<%# string.Format((string)Eval("Key"), new object[] { GUtility.GetLocalServerTypeName() } ) %>'></asp:label>
							</td>
						</tr>
						<tr>
							<td>
								<asp:listview id="lvwResources" runat="server" datakeynames="ResourceId" onselectedindexchanging="ListView_SelectedIndexChanging"
									onitemdatabound="ListView_OnItemDataBound" onitemcommand="ListView_OnItemCommand">
									<layouttemplate>
										<table id="tblGrid" runat="server" cellpadding="0" cellspacing="0" width="100%" class="ListViewHeader" style="font-size:small">
											<tr id="Tr1" runat="server" style="background-color: #afafaf; font-weight: bold;">
												<th id="Th1" runat="server" width="20%">
													<div style="padding: 2px;">
														Nome</div>
												</th>
												<th id="Th2" runat="server" width="30%">
													<div style="padding: 2px;">
														Gruppi Gris</div>
												</th>
											</tr>
											<tr runat="server" id="itemPlaceholder" />
										</table>
									</layouttemplate>
									<itemtemplate>
										<tr id="Tr2" runat="server" style="background-color: #4d4d4d;">
											<td>
												<div style="padding: 2px;">
													<asp:linkbutton id="lnkResourceName" runat="server" commandname="Select" text='<%# string.Format((string)Eval("ResourceName"), new object[] { GUtility.GetLocalServerTypeName() } ) %>' style="color:#ffffff;text-decoration:underline;" />
												</div>
											</td>
											<td>
												<div style="padding: 2px;">
													<asp:label id="lblGroups" runat="server" text='<%# Eval("Groups") %>' />
												</div>
											</td>
										</tr>
									</itemtemplate>
									<selecteditemtemplate>
										<tr id="Tr2" runat="server" style="background-color: #4d4d4d;">
											<td colspan="2" style="border:2px solid #000000;padding:10px;">
												<div style="padding: 2px;">
													<asp:label id="lblResourceName" runat="server" text='<%# string.Format((string)Eval("ResourceName"), new object[] { GUtility.GetLocalServerTypeName() } ) %>' Font-Bold="true" />
													<div style="padding: 10px">
                                                    <asp:panel id="pnlGroups" runat="server" height="300px" scrollbars="Vertical">
														<asp:checkboxlist id="chklGroups" runat="server" datatextfield="GroupName" datavaluefield="GroupID" cssclass="colWidth"
															repeatcolumns="5" />
													</asp:panel>
                                                    </div>
													<asp:button id="btnAssociate" runat="server" commandname="Associate" text="Associa" />
                                                    <asp:button id="btnNotifications" runat="server" commandname="Notifications" text="Notifiche" />
												</div>
											</td>
										</tr>
									</selecteditemtemplate>
								</asp:listview>
							</td>
						</tr>
					</itemtemplate>
					<footertemplate>
						</table>
					</footertemplate>
				</asp:repeater>
                <asp:Label runat="server" EnableViewState="false" ID="lblNoDataAvailable" Text="Non sono presenti dati relativi ai criteri di ricerca utilizzati" style="font-size:small;font-weight:bold;color:#ffffff;padding-top:20px;padding-bottom:150px;display:block;" Visible="false" />
			</div>
		</contenttemplate>
	</asp:updatepanel>
    <asp:Button runat="server" ID="btnContactAssociate" Text="Associa contatti" Style="float: left; margin-top: 10px; display: none;" />
    <act:ModalPopupExtender runat="server" ID="mpeContactAssociate" BehaviorID="mpeContactAssociateBehavior"
        TargetControlID="btnContactAssociate" PopupControlID="popupContactAssociate"
        BackgroundCssClass="ModalBackground" DropShadow="False" RepositionMode="RepositionOnWindowScroll" >
    </act:ModalPopupExtender>
    <asp:Panel runat="server" CssClass="modalPopup" ID="popupContactAssociate"
        Style="display: none; background-color: #9A9A9A; border-width: 2px; border-style: solid;
        border-color: #9A9A9A; padding: 3px; width: 800px;" >
        <asp:UpdatePanel ID="updContactAssociate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
	            <div id="div1" style="font-size:small;color:#ffffff; margin-bottom: 10px;">
	                <asp:textbox id="txtContactName" runat="server"/>
		            <asp:button id="btnSearchContact" runat="server" text="Cerca" enableviewstate="false" 
			            onclick="btnSearchContact_Click" />
	            </div>

                <asp:GridView ID="grdContacts" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                    SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ContactId,ResourceId" DataSourceID="sdsContacts"
                    Width="100%" ShowFooter="False" PageSize="20" EmptyDataText="Nessun contatto presente"
                    AllowPaging="true" CssClass="modalGrid" OnRowDataBound="grdContacts_OnRowDataBound" OnPageIndexChanging="grdContacts_OnPageIndexChanging">
                    <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                    <HeaderStyle CssClass="modalGridTh" />
                    <RowStyle CssClass="modalGridRow" />
                    <AlternatingRowStyle CssClass="modalGridRowA" />
                    <PagerStyle CssClass="modalGridPager" />
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Nome" ReadOnly="True">
                            <ItemStyle Width="40%" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllContactsEmail" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAllContactsEmail_OnCheckedChanged" ToolTip="Seleziona tutto" Text="eMail" />
                            </HeaderTemplate>                            
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSendEMail" runat="server" Text='<%# Eval("Email")%>' />
                            </ItemTemplate>
                            <ItemStyle Width="40%" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllContactsSms" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAllContactsSms_OnCheckedChanged" ToolTip="Seleziona tutto" Text="Sms" />
                            </HeaderTemplate>                            
                            
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSendSms" runat="server" Text='<%# Eval("PhoneNumber")%>'/>
                            </ItemTemplate>
                            <ItemStyle Width="20%"/>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sdsContacts" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                    SelectCommand="gris_GetResourceNotifications" SelectCommandType="StoredProcedure"
                    OnSelecting="sdsContacts_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="ResourceId" Type="Int32" />
                        <asp:Parameter Name="ConcactFilter" Type="String" />
                        <asp:Parameter Name="OnlyEmail" Type="Boolean" />
                        <asp:Parameter Name="OnlySms" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Button runat="server" ID="btnCancelContactAssociate"  Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-right: 10px; margin-top: 10px;"
                    OnClick="btnCancelContactAssociate_Click" 
                    OnClientClick="hideModalPopupViaClient('mpeContactAssociateBehavior');"
                    Text="Chiudi" />
                <asp:Button runat="server" ID="btnSaveContactAssociate"   Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-right: 20px; margin-top: 10px;" 
                    OnClick="btnSaveContactAssociate_Click"
                    OnClientClick="hideModalPopupViaClient('mpeContactAssociateBehavior');"
                    Text="Salva"
                    ToolTip="Salva e Chiudi" />
                <asp:Button runat="server" ID="btnRemoveContactAssociate"   Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-right: 10px; margin-top: 10px;" 
                    Text="Elimina notifiche"
                    ToolTip="Elimina tutte le notifiche e Chiudi" />
			    <act:ModalPopupExtender ID="mdlRemoveContactAssociate" runat="server" 
                    TargetControlID="btnRemoveContactAssociate"
				    PopupControlID="divConfirmRemoveContactAssociate" 
				    CancelControlID="btnCancelDelete" 
                    BackgroundCssClass="ModalBackground"
				    RepositionMode="RepositionOnWindowScroll" />
			    <asp:Panel  id="divConfirmRemoveContactAssociate" runat="server" class="deleteConfirm" style="display: none">
				    <img src="../IMG/Interfaccia/warning.gif" alt="warning" style="padding-right: 10px;
					    vertical-align: middle" />Vuoi eliminare tutte le notifiche '<%# Eval("DeviceFilterName")%>'?<br />
				    <br />
				    <asp:Button ID="btnOkDeleteNotification" 
                    runat="server" Text="Sì" 
                    CausesValidation="false"  
                    OnClick="btnOkDeleteNotification_OnClick"
                    CssClass="confirmButton" 
                    />
				    <asp:Button ID="btnCancelDelete" runat="server" 
                        Text="No" 
                        CausesValidation="false"
					    CssClass="confirmButton" />
			    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:content>

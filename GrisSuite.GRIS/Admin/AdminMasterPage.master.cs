using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using GrisSuite.Common;

public partial class AdminMasterPage : GrisMasterPageBase
{
    public bool FunctionsListPanelVisible
    {
        get { return Convert.ToBoolean(this.ViewState["FunctionsListPanelVisible"] ?? false); }
        set
        {
            this.ViewState["FunctionsListPanelVisible"] = value;
            if (value)
            {
                this.tdFunctionsMenu.Attributes["class"] = "ToolbarButtonToggleOn";
                this.tdFunctionsList.Style[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                this.tdFunctionsMenu.Attributes["class"] = "ToolbarButtonToggleOff";
                this.tdFunctionsList.Style[HtmlTextWriterStyle.Display] = "none";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.Title = "GRIS - Amministrazione -";
        if (!GrisPermissions.IsUserInAdminGroup() && !GrisPermissions.IsUserInLocalAdminGroup() && !GrisPermissions.IsUserInGrisClientLicenseAdminGroup() && !GrisPermissions.IsUserInGrisClientLicenseUsersGroup())
        {
            this.Response.Redirect("~/Italia.aspx");
        }

        if (!this.IsPostBack)
        {
            if (GUtility.CheckGETAUrl())
            {
                this.lnkbGeta.OnClientClick = GUtility.GetGetaUrl();
            } else {
            
                this.lnkbGeta.Visible=false;
            }
            
        }

        this.lnkbSTLCOffline.Text = string.Format(this.lnkbSTLCOffline.Text, GUtility.GetLocalServerTypeName());
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.OpenSelectedReportPanel(this.Request.AppRelativeCurrentExecutionFilePath + this.Request.Url.Query);
    }

    private void OpenSelectedReportPanel(string currentPage)
    {
        if (!string.IsNullOrEmpty(currentPage))
        {
            foreach (AccordionPane pane in this.accFunctionList.Panes)
            {
                if (pane.ContentContainer != null)
                {
                    foreach (Control ctrl in pane.ContentContainer.Controls)
                    {
                        LinkButton link = ctrl as LinkButton; 
                        if (link == null) continue;

                        link.Font.Bold = false;

                        String linkPage = BuildUri(link.CommandName, link.CommandArgument);
                        if (!linkPage.Equals(currentPage,StringComparison.OrdinalIgnoreCase)) continue;

                        accFunctionList.SelectedIndex = accFunctionList.Panes.IndexOf(pane);
                        link.Font.Bold = true;
                    }
                }
            }
        }
    }

    protected void FunctionLink_Command(Object sender, CommandEventArgs e)
    {
        this.Response.Redirect(BuildUri(e.CommandName, e.CommandArgument));
    }

    private string BuildUri(String CommandName, Object CommandArgument)
    {
        String parameters = "";
        if (CommandArgument != null && !String.IsNullOrEmpty(CommandArgument.ToString()))
        {
            parameters = "?" + CommandArgument;
        }
        return string.Format("~/Admin/{0}.aspx{1}", CommandName, parameters);
    }

    protected void btnFunctionsMenu_Click(object sender, EventArgs e)
    {
        this.FunctionsListPanelVisible = this.tdFunctionsMenu.Attributes["class"] != "ToolbarButtonToggleOn";
    }

    protected void accFunctionList_PreRender(object sender, EventArgs e)
    {
        this.acpanGeneral.Visible = GrisPermissions.IsUserInAdminGroup();
        this.acpanServer.Visible = GrisPermissions.IsUserInAdminGroup();
        this.acpanServerSMS.Visible = GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInLocalAdminGroup();
	    this.acpanServerLicensing.Visible = GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisClientLicenseAdminGroup() ||
	                                        GrisPermissions.IsUserInGrisClientLicenseUsersGroup();
    }

    protected void lnkFiltersVisible_DataBound(object sender, EventArgs e)
    {
        this.lnkFilters.Visible = DBSettings.GetFiltersButtonVisible();
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="GroupsCCParameters.aspx.cs" Inherits="Admin_GroupsCCParameters" Title="Parametri Comando e Controllo"
    StylesheetTheme="Gris" %>


<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="text-align: left; color: White; font-weight: bold;">
        Gestione parametri Comando e Controllo</div>
    <div id="tableContainer">
    </div>


    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
     <script src="GroupsCCDataManagerClass.js" type="text/javascript"></script>
    
    
    <script type="text/javascript">



        $(document).ready(function () {
            var groupsCCDataManager = new GroupsCCDataManagerClass($('#tableContainer'));
            groupsCCDataManager.loadData();
        });  
    
    
    </script>

</asp:Content>



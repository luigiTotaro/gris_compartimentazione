﻿
using System;
using System.Data;
using System.Web.UI.WebControls;
using GrisSuite.Data.Reports;

public partial class Admin_STLCOffline : NavigationPage
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		this.lblRefreshTime.Text = string.Format("Data ultimo aggiornamento: {0:dd/MM/yyyy HH:mm}", DateTime.Now);
	}
	
	protected void tmrRefresh_Tick ( object sender, EventArgs e )
	{
		this.lblRefreshTime.Text = string.Format("Data ultimo aggiornamento: {0:dd/MM/yyyy HH:mm}", DateTime.Now);
		this.gvwSTLCOffline.DataBind();
		this.updSTLCOffline.Update();
	}
	
	protected void gvwSTLCOffline_RowDataBound ( object sender, GridViewRowEventArgs e )
	{
		if ( e.Row.RowType == DataControlRowType.DataRow )
		{
			this.lblSTLCCount.Text = string.Format("{0} {1} offline", ((STLCOffline.STLCOfflineRow)((DataRowView)e.Row.DataItem).Row).STLCCount, GUtility.GetLocalServerTypeName());
		}
	}

	protected void CustomersGridView_RowCommand ( object sender, GridViewCommandEventArgs e )
	{
	    long nodId;

	    if ( e.CommandName == "NavigateToNode" && long.TryParse(e.CommandArgument.ToString(), out nodId) )
	    {
            this.GrisMaster.SelectInterface(GrisLayoutMode.Map, false);

            if (nodId != 0)
            {
                Navigation.GoToPage(Navigation.GetAreaNavigationObj("node", true), nodId);
            }
            else
            {
                ItaliaNavigation italia = new ItaliaNavigation(null);
                Navigation.GoToPage(italia, -1);
            }
        }
	}


    public override MultiView LayoutMultiView
    {
        get { throw new NotImplementedException(); }
    }

    public override View GraphView
    {
        get { throw new NotImplementedException(); }
    }

    public override View GridView
    {
        get { throw new NotImplementedException(); }
    }
}

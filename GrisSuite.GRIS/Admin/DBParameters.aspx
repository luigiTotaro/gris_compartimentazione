﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="DBParameters.aspx.cs" Inherits="Admin_DBParameters" Title="Parametri globali"
    StylesheetTheme="Gris" %>

<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="text-align: left; color: White; font-weight: bold;">
        Gestione parametri globali</div>
    <asp:GridView ID="grdParameters" runat="server" AllowSorting="True" AutoGenerateColumns="False"
        CellPadding="4" DataKeyNames="ParameterName" DataSourceID="odsParameters" Width="100%">
        <Columns>
            <asp:BoundField DataField="ParameterName" HeaderText="Nome" ReadOnly="True" ItemStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" SortExpression="ParameterName">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="Valore" SortExpression="ParameterValue">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ParameterValue") %>' Width="40px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="vreqTextBox1" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" Text="!" ForeColor="Red" ToolTip="Valore obbligatorio" EnableClientScript="true"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ParameterValue") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Descrizione" SortExpression="ParameterDescription">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ParameterDescription") %>'
                        Width="450px"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ParameterDescription") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:CommandField ButtonType="Image" HeaderStyle-Width="45px" CancelText="Annulla"
                CancelImageUrl="~/IMG/Interfaccia/Grids/cancel.png" EditImageUrl="~/IMG/Interfaccia/Grids/edit.gif"
                DeleteText="Cancella" EditText="Modifica" InsertText="Inserisci" NewText="Nuovo"
                SelectText="Seleziona" UpdateText="Conferma" UpdateImageUrl="~/IMG/Interfaccia/Grids/accept.gif"
                ShowEditButton="True" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsParameters" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.ParametersDSTableAdapters.ParametersTableAdapter"
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_ParameterName" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ParameterName" Type="String" />
            <asp:Parameter Name="ParameterValue" Type="String" />
            <asp:Parameter Name="ParameterDescription" Type="String" />
            <asp:Parameter Name="Original_ParameterName" Type="String" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="ParameterName" Type="String" />
            <asp:Parameter Name="ParameterValue" Type="String" />
            <asp:Parameter Name="ParameterDescription" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/AdminMasterPage.master"
    CodeFile="OperationSchedulesSMS.aspx.cs" Inherits="Admin_OperationSchedulesSMS" %>

<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <div style="color: White; font-weight: bold; text-align: left;">
        Visualizzazione coda invio SMS</div>
    <asp:UpdatePanel ID="updGrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="grdSMS" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                SkinID="gvwEmpty" CellPadding="4" DataKeyNames="OperationScheduleId" DataSourceID="sdsSMS"
                Width="100%" ShowFooter="False" PageSize="20" EmptyDataText="Nessun SMS presente nella coda"
                AllowPaging="true" CssClass="modalGrid" 
                onrowdatabound="grdSMS_RowDataBound">
                <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                <HeaderStyle CssClass="modalGridTh" />
                <RowStyle CssClass="modalGridRow" />
                <AlternatingRowStyle CssClass="modalGridRowA" />
                <PagerStyle CssClass="modalGridPager" />
                <Columns>
                    <asp:TemplateField HeaderText="Numero telefonico">
                        <ItemTemplate>
                            <asp:Label ID="lblPhoneNumber" runat="server" />
                        </ItemTemplate>
                        <ItemStyle Width="20%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Testo messaggio">
                        <ItemTemplate>
                            <asp:Label ID="lblMessage" runat="server" />
                        </ItemTemplate>
                        <ItemStyle Width="50%" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="DateCreated" HeaderText="Inserimento in coda" ReadOnly="True">
                        <ItemStyle Width="10%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DateExecuted" HeaderText="Elaborazione" ReadOnly="True">
                        <ItemStyle Width="10%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DateAborted" HeaderText="Errore elaborazione" ReadOnly="True">
                        <ItemStyle Width="10%" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="sdsSMS" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                SelectCommand="gris_GetOperationSchedules" SelectCommandType="StoredProcedure"
                OnSelecting="sdsSMS_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="OperationScheduleTypeId" Type="Int16" />
                </SelectParameters>
            </asp:SqlDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

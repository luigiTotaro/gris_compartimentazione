﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="STLCOffline.aspx.cs" Inherits="Admin_STLCOffline" Title="Offline" stylesheettheme="Gris" %>

<asp:content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" 
	Runat="Server">
	<table cellpadding="4" style="width: 100%;">
		<tr>
			<td style="text-align: left;">
				<asp:updatepanel id="updSTLCOffline" runat="server" updatemode="Conditional">
					<contenttemplate>
						<table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
							<tr>
								<td style="text-align: left; color: White; font-weight: bold;">
									&nbsp;<asp:label id="lblSTLCCount" runat="server"></asp:label>
								</td>
								<td style="text-align: right; color: White; font-weight: bold;">
									<asp:label id="lblRefreshTime" runat="server"></asp:label>
								</td>
							</tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr>
								<td colspan="2">
									<asp:gridview id="gvwSTLCOffline" runat="server" autogeneratecolumns="False" cellpadding="8"
										skinid="SWMC4" datasourceid="odsSTLCOffline" allowsorting="true" headerstyle-cssclass="blueTableHeaders" onrowdatabound="gvwSTLCOffline_RowDataBound" onrowcommand="CustomersGridView_RowCommand">
										<columns>
											<asp:boundfield datafield="RegionName" headertext="Compartimento" 
												sortexpression="RegionName" />
											<asp:boundfield datafield="ZoneName" headertext="Linea" 
												sortexpression="ZoneName" />
                                            <asp:TemplateField HeaderText="Stazione" SortExpression="NodeName">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkNode" runat="server" CommandName="NavigateToNode" CommandArgument='<%# Eval("NodID") %>' Text='<%# Eval("NodeName") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" CssClass="adminTableLink" />
                                            </asp:TemplateField>
											<asp:boundfield datafield="FullHostName" headertext="Server" 
												sortexpression="FullHostName" />
											<asp:boundfield datafield="IP" headertext="IP" sortexpression="IP" />
											<asp:boundfield datafield="LastUpdate" headertext="Data Ultimo Messaggio" 
												sortexpression="LastUpdate" dataformatstring="{0:dd/MM/yyyy}" />
											<asp:boundfield datafield="STLCStatusDescription" headertext="Stato" 
												sortexpression="STLCStatusDescription" />
										</columns>
									</asp:gridview>
								</td>
							</tr>
						</table>
					</contenttemplate>
				</asp:updatepanel>	
			</td>
		</tr>
	</table>
	<asp:objectdatasource id="odsSTLCOffline" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="GrisSuite.Data.Reports.STLCOfflineTableAdapters.STLCOfflineTableAdapter">
		<selectparameters>
			<asp:parameter name="RegID" type="Int64" defaultvalue="-1" />
			<asp:parameter name="ZonID" type="Int64" defaultvalue="-1" />
			<asp:parameter name="NodID" type="Int64" defaultvalue="-1" />
			<asp:parameter name="IncludeInMaintenance" type="Boolean" defaultvalue="True" />
		</selectparameters>
	</asp:objectdatasource>
	<asp:timer id="tmrRefresh" runat="server" ontick="tmrRefresh_Tick" interval="300000">
	</asp:timer>
</asp:content>




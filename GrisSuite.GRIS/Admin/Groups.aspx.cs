﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.GroupContactsDSTableAdapters;
using GrisSuite.Data.Properties.PermissionsTableAdapters;

public partial class Admin_Groups : System.Web.UI.Page
{
    protected bool ContactsSelectedInit
    {
        get
        {
            if (ViewState["ContactsSelectedInit"] == null)
            {
                ViewState["ContactsSelectedInit"] = false;
            }
            return (bool)ViewState["ContactsSelectedInit"];
        }
        set { ViewState["ContactsSelectedInit"] = value; }
    }

    protected bool SelectAllContacts
    {
        get
        {
            if (ViewState["SelectAllContacts"] == null)
            {
                ViewState["SelectAllContacts"] = false;
            }
            return (bool)ViewState["SelectAllContacts"];
        }
        set { ViewState["SelectAllContacts"] = value; }
    }

    protected HashSet<long> ContactsSelected
    {
        get
        {
            if (ViewState["ContactsSelected"] == null)
            {
                ViewState["ContactsSelected"] = new HashSet<long>();
            }
            return (HashSet<long>)ViewState["ContactsSelected"];

        }
    }


	protected void Page_Load ( object sender, EventArgs e )
	{

	}

	protected void lvwGroups_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
            ImageButton conctactsButton = e.Item.FindControl("imgContacts") as ImageButton;
		    if (conctactsButton != null)
		    {
		        String windowsUsers =
		        ((GrisSuite.Data.Properties.Permissions.PermissionGroupsRow)
		            ((System.Data.DataRowView) ((ListViewDataItem) e.Item).DataItem).Row).WindowsUsers;

                conctactsButton.ImageUrl = string.Format("~/IMG/Interfaccia/Grids/{0}", (windowsUsers.Length > 0 ? "users.png" : "no_users.png"));
                conctactsButton.AlternateText = string.Format("{0} contatti", (windowsUsers.Length > 0 ? "Gestisci" : "Associa"));
		    }

			ImageButton delButton = e.Item.FindControl("imgDelete") as ImageButton;
			if ( delButton != null ) delButton.ImageUrl = ( delButton.Enabled ) ? delButton.ImageUrl : delButton.ImageUrl.Replace(".gif", "_disabled.gif");
		}
	}

	protected void lvwGroups_ItemInserting ( object sender, ListViewInsertEventArgs e )
	{
		if ( this.IsValid )
		{
			TextBox txtName = e.Item.FindControl("txtName") as TextBox;
			TextBox txtDesc = e.Item.FindControl("txtDesc") as TextBox;
			TextBox txtWinGroups = e.Item.FindControl("txtWinGroups") as TextBox;

			if ( txtName != null && txtDesc != null && txtWinGroups != null )
			{
				e.Values["GroupName"] = txtName.Text;
				e.Values["GroupDescription"] = txtDesc.Text;
				e.Values["WindowsGroups"] = txtWinGroups.Text;
			}

			GrisPermissions.GroupsCache.Reset();
		}
	}

	protected void lvwGroups_ItemUpdating ( object sender, ListViewUpdateEventArgs e )
	{
		if ( this.IsValid )
		{
			ListViewDataItem item = this.lvwGroups.Items[e.ItemIndex];

			if ( item != null )
			{
				TextBox txtName = item.FindControl("txtName") as TextBox;
				TextBox txtDesc = item.FindControl("txtDesc") as TextBox;
				TextBox txtWinGroups = item.FindControl("txtWinGroups") as TextBox;
				CheckBox chkBuiltIn = item.FindControl("chkBuiltIn") as CheckBox;

				if ( txtName != null && txtDesc != null && txtWinGroups != null && chkBuiltIn != null )
				{
					e.NewValues["GroupID"] = Convert.ToInt32(e.Keys["GroupID"]);
					e.NewValues["GroupName"] = txtName.Text;
					e.NewValues["GroupDescription"] = txtDesc.Text;
					e.NewValues["WindowsGroups"] = txtWinGroups.Text;
					e.NewValues["IsBuiltIn"] = chkBuiltIn.Checked;
				}

				GrisPermissions.GroupsCache.Reset();
			}
		}
	}

	protected void lvwGroups_ItemDeleting ( object sender, ListViewDeleteEventArgs e )
	{
		e.Values["GroupID"] = e.Keys["GroupID"];
		GrisPermissions.GroupsCache.Reset();
	}

	protected void lvwGroups_ItemCommand ( object sender, ListViewCommandEventArgs e )
	{
		int groupId;
		ListViewDataItem dataItem = e.Item as ListViewDataItem;

		if ( dataItem != null && int.TryParse((this.lvwGroups.DataKeys[dataItem.DisplayIndex].Value ?? -1).ToString(), out groupId) )
		{
			switch ( e.CommandName )
			{
                case "Contacts":
                    this.ViewState["LastContactAssociate"] = Convert.ToInt32(e.CommandArgument.ToString());
                    this.ContactsSelectedInit = true;
                    this.ContactsSelected.Clear();
                    this.grdContacts.DataBind();
                    this.updContactAssociate.Update();
                    this.mpeContactAssociate.Show();

                    break;
				case "Move":
					{
						PermissionGroupsTableAdapter ta = new PermissionGroupsTableAdapter();
						switch ( e.CommandArgument.ToString() )
						{
							case "Up":
								{
									ta.MovePermissionGroup(groupId, true);
									this.lvwGroups.DataBind();
									break;
								}
							case "Down":
								{
									ta.MovePermissionGroup(groupId, false);
									this.lvwGroups.DataBind();
									break;
								}
						}

						GrisPermissions.GroupsCache.Reset();
						break;
					}
			}			
		}
	}

    protected void sdsContacts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@GroupId"].Value = (this.ViewState["LastContactAssociate"] != null
                                                             ? (int)this.ViewState["LastContactAssociate"]
                                                             : 0);
        e.Command.Parameters["@RegIds"].Value = string.Format("|{0}|", string.Join("|",
                                                                                   GrisPermissions.VisibleRegions(
                                                                                       PermissionLevel.AllLevels).Select
                                                                                       (
                                                                                           regionId =>
                                                                                           regionId.ToString()).ToArray()));

        e.Command.Parameters["@Search"].Value = this.txtSearchContact.Text;

    }

    protected void btnSearchConcat_OnClick(object sender, EventArgs e)
    {
        ContactsSelectedInit = true;
        grdContacts.PageIndex = 0;
        SelectAllContacts = false;
        ContactsSelected.Clear();
        this.grdContacts.DataBind();
        this.updContactAssociate.Update();
    }

    protected void btnSaveContactAssociate_Click(object sender, EventArgs e)
    {

        if (grdContacts.DataKeys.Count == 0) return;
        int groupId = (int)grdContacts.DataKeys[0]["GroupId"];
        
        UpdateContactsSelected();

        var ta = new GroupContactsTableAdapter();
        
        ta.DeleteGroupContacts(groupId);

        foreach (long contactId in ContactsSelected)
        {
            ta.UpdateContacts(true, groupId, contactId);    
        }
        
        ContactsSelected.Clear();
        SelectAllContacts = false;

        this.BindGroupContacts();
    }

    protected void btnCancelContactAssociate_Click(object sender, EventArgs e)
    {
        this.BindGroupContacts();
    }

    private void BindGroupContacts()
    {
        this.lvwGroups.DataBind();
        this.updGrid.Update();

        this.grdContacts.PageIndex = 0;
    }

    protected void chkSelectAllContacts_OnCheckedChanged(object sender, EventArgs e)
    {
        ContactsSelected.Clear();
        CheckBox chkAll = ((CheckBox)sender);
        SelectAllContacts = chkAll.Checked;
        if (SelectAllContacts)
        {
            DataView dv = sdsContacts.Select(DataSourceSelectArguments.Empty) as DataView;
            if (dv != null)
            {
                foreach (DataRow row in dv.Table.Rows)
                {
                    ContactsSelected.Add((long)row["ContactId"]);
                }
            }
        }
        grdContacts.DataBind();
    }

    protected void grdContacts_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox selectAll = e.Row.FindControl("chkSelectAllContacts") as CheckBox;
            if (selectAll != null)
            {
                selectAll.Checked = SelectAllContacts;
                selectAll.ToolTip = SelectAllContacts ? "Deseleziona tutto" : "Seleziona tutto";
            }
        }

        if (e.Row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[e.Row.RowIndex] == null) return;

        CheckBox checkBox = e.Row.FindControl("chkSelectedContact") as CheckBox;

        if (ContactsSelectedInit)
        {
            ContactsSelectedInit = false;
            DataTable table = ((DataRowView)e.Row.DataItem).Row.Table;
            foreach (DataRow row in table.Rows)
            {
                if (!(Boolean)row["Associated"]) break;
                ContactsSelected.Add((long)row["ContactId"]);
            }
        }

        long contactId = (long)this.grdContacts.DataKeys[e.Row.RowIndex]["ContactId"];
        if (checkBox != null) checkBox.Checked = ContactsSelected.Contains(contactId);

    }


    protected void grdContacts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        UpdateContactsSelected();
    }

    private void UpdateContactsSelected()
    {
        foreach (GridViewRow row in this.grdContacts.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow || this.grdContacts.DataKeys[row.RowIndex] == null) continue;

            bool selected = ((CheckBox)row.FindControl("chkSelectedContact")).Checked;
            long contactId = (long)(this.grdContacts.DataKeys[row.RowIndex]["ContactId"]);
            if (selected)
                ContactsSelected.Add(contactId);
            else
                ContactsSelected.Remove(contactId);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/AdminMasterPage.master"
	CodeFile="OperationSchedulesMail.aspx.cs" Inherits="Admin_OperationSchedulesMail" %>

<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
	<div style="color: White; font-weight: bold; text-align: left;">
		Visualizzazione coda invio Mail</div>
	<asp:UpdatePanel ID="updGrid" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:GridView ID="grdMail" runat="server" AllowSorting="True" AutoGenerateColumns="False"
				SkinID="gvwEmpty" CellPadding="4" DataKeyNames="OperationScheduleId" DataSourceID="sdsMail"
				Width="100%" ShowFooter="False" PageSize="20" EmptyDataText="Nessuna mail presente nella coda"
				AllowPaging="true" CssClass="modalGrid">
				<PagerSettings Mode="NumericFirstLast" Position="Bottom" />
				<HeaderStyle CssClass="modalGridTh" />
				<RowStyle CssClass="modalGridRow" />
				<AlternatingRowStyle CssClass="modalGridRowA" />
				<PagerStyle CssClass="modalGridPager" />
				<Columns>
					<asp:BoundField DataField="OperationParameters" HeaderText="Sistema e nodo" ReadOnly="True">
						<ItemStyle Width="55%" />
					</asp:BoundField>
					<asp:BoundField DataField="DateCreated" HeaderText="Inserimento in coda" ReadOnly="True">
						<ItemStyle Width="15%" />
					</asp:BoundField>
					<asp:BoundField DataField="DateExecuted" HeaderText="Elaborazione" ReadOnly="True">
						<ItemStyle Width="15%" />
					</asp:BoundField>
					<asp:BoundField DataField="DateAborted" HeaderText="Errore elaborazione" ReadOnly="True">
						<ItemStyle Width="15%" />
					</asp:BoundField>
				</Columns>
			</asp:GridView>
			<asp:SqlDataSource ID="sdsMail" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
				SelectCommand="gris_GetOperationSchedulesMail" SelectCommandType="StoredProcedure"
				OnSelecting="sdsMail_Selecting">
				<SelectParameters>
					<asp:Parameter Name="OperationScheduleTypeId" Type="Int16" />
				</SelectParameters>
			</asp:SqlDataSource>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

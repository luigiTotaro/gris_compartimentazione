﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.ServerParkingDSTableAdapters;

public partial class Admin_ServerParking : Page
{
	protected string ServersParkingSortExpression
	{
		get { return (string)(this.ViewState["ServersParkingSortExpression"] ?? String.Empty); }
		set { this.ViewState["ServersParkingSortExpression"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected void sdsGetServersParking_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Arguments.SortExpression = this.ServersParkingSortExpression;

		e.Command.Parameters["@IP"].Value = (this.txtSearchIP.Text.Trim().Length == 0 ? "%%" : string.Format("%{0}%", this.txtSearchIP.Text.Trim()));
		e.Command.Parameters["@SrvID"].Value = (this.txtSearchSrvID.Text.Trim().Length == 0 ? -1 : long.Parse(this.txtSearchSrvID.Text.Trim()));
		e.Command.Parameters["@Host"].Value = (this.txtSearchHost.Text.Trim().Length == 0 ? "%%" : string.Format("%{0}%", this.txtSearchHost.Text.Trim()));
		e.Command.Parameters["@HardwareProfile"].Value = (this.txtSearchHardwareProfile.Text.Trim().Length == 0 ? "%%" : string.Format("%{0}%", this.txtSearchHardwareProfile.Text.Trim()));
		e.Command.Parameters["@ParkingType"].Value = int.Parse(this.cboSearchParkingType.SelectedValue);
	}

	protected void imgbSearch_Click(object sender, ImageClickEventArgs e)
	{
		this.RebindPanelServersParking();
	}

	protected void grdServersParking_RowCommand(object sender, GridViewCommandEventArgs e)
	{
		switch (e.CommandName)
		{
			case "DeleteServerParking":
				if (GrisPermissions.IsUserInAdminGroup())
				{
					int serverParkingID = int.Parse(e.CommandArgument.ToString());
					ServersParkingTableAdapter ta = new ServersParkingTableAdapter();
					ta.DeleteServerParking(serverParkingID);
				}
				break;
		}

		this.RebindAllPanels();
	}

	private void RebindPanelServersParking()
	{
		this.grdServersParking.DataBind();
		this.updParking.Update();
	}

	private void RebindAllPanels()
	{
		this.RebindPanelServersParking();
	}

	protected void grdServersParking_Sorting(object sender, GridViewSortEventArgs e)
	{
		this.ServersParkingSortExpression = string.Format("{0} {1}", e.SortExpression, e.SortDirection == SortDirection.Ascending ? "ASC" : "DESC");
	}
}
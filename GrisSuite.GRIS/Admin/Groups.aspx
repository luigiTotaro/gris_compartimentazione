﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="Groups.aspx.cs" Inherits="Admin_Groups" %>

<asp:Content ID="cntFunctionArea" ContentPlaceHolderID="cphFunctionArea" runat="Server">
    <script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../JS/jquery.dimensions.js" type="text/javascript"></script>
    <script src="../JS/jquery.tooltip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function hideModalPopupViaClient(id) {
            var modalPopupBehavior = $find(id);
            modalPopupBehavior.hide();
        }
        function pageLoad() {
            $("img[id$='imgContacts']").tooltip({
                showURL: false,
                positionLeft: true
            });
        }        
    </script>

    <div style="color: White; font-weight: bold; text-align: left;">
        Gestione gruppi</div>
    <asp:UpdatePanel ID="updGrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:ListView ID="lvwGroups" runat="server" DataSourceID="odsGroups" OnItemDataBound="lvwGroups_ItemDataBound"
                InsertItemPosition="FirstItem" DataKeyNames="GroupID" OnItemInserting="lvwGroups_ItemInserting"
                OnItemUpdating="lvwGroups_ItemUpdating" OnItemDeleting="lvwGroups_ItemDeleting"
                OnItemCommand="lvwGroups_ItemCommand">
                <LayoutTemplate>
                    <table id="tblGrid" runat="server" cellpadding="0" cellspacing="0" width="100%" class="ListViewHeader"
                        style="font-size: small">
                        <tr id="Tr1" runat="server" style="background-color: #afafaf; font-weight: bold;">
                            <th id="Th1" runat="server" width="20%">
                                <div style="padding: 2px;">
                                    Nome</div>
                            </th>
                            <th id="Th2" runat="server" width="30%">
                                <div style="padding: 2px;">
                                    Descrizione</div>
                            </th>
                            <th id="Th3" runat="server" width="10%">
                                <div style="padding: 2px; text-align: center;">
                                    Di Sistema</div>
                            </th>
                            <th id="Th5" runat="server" width="25%">
                                <div style="padding: 2px;">
                                    Gruppi o Utenti Windows</div>
                            </th>
                            <th id="Th4" runat="server" width="15%">
                            </th>
                        </tr>
                        <tr runat="server" id="itemPlaceholder" />
                    </table>
                    <asp:DataPager ID="dpagGrid" runat="server" PageSize="20" style="font-size: small">
                        <Fields>
                            <asp:NumericPagerField NumericButtonCssClass="ListViewHeader" CurrentPageLabelCssClass="ListViewHeader"
                                NextPreviousButtonCssClass="ListViewHeader" />
                        </Fields>
                    </asp:DataPager>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr id="Tr2" runat="server" style="background-color: #4d4d4d;">
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("GroupName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("GroupDescription") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px; text-align: center">
                                <asp:CheckBox ID="chkBuiltIn" runat="server" Checked='<%# Eval("IsBuiltIn") %>' Enabled="false" />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblWinGroups" runat="server" Text='<%# Eval("WindowsGroups") %>' />
                            </div>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgContacts" runat="server" CommandName="Contacts" CommandArgument='<%# Eval("GroupID") %>'
                                AlternateText="Associa contatti" ImageUrl="~/IMG/Interfaccia/Grids/no_users.png" CausesValidation="false" />
                            <asp:ImageButton ID="imgUp" runat="server" CommandName="Move" CommandArgument="Up"
                                AlternateText="Su" ImageUrl="~/IMG/Interfaccia/Grids/arrow_up.png" CausesValidation="false" />
                            <asp:ImageButton ID="imgDown" runat="server" CommandName="Move" CommandArgument="Down"
                                AlternateText="Giù" ImageUrl="~/IMG/Interfaccia/Grids/arrow_down.png" CausesValidation="false" />
                            <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" AlternateText="Modifica"
                                ImageUrl="~/IMG/Interfaccia/Grids/edit.gif" CausesValidation="false" />
                            <asp:ImageButton ID="imgDelete" runat="server" CommandName="Delete" Enabled='<%# !Convert.ToBoolean(Eval("Associated")) %>'
                                AlternateText='<%# Convert.ToBoolean(Eval("Associated")) ? "Impossibile eliminare il gruppo perché di sistema oppure associato ad almeno un report" : "Elimina il gruppo" %>'
                                ImageUrl="~/IMG/Interfaccia/Grids/delete.gif" CausesValidation="false" OnClientClick='<%# string.Format("return confirm(\"Eliminare il gruppo {0}?\");", Eval("GroupName"))%>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr id="Tr2" runat="server" style="background-color: #333333;">
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("GroupName") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("GroupDescription") %>' />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px; text-align: center;">
                                <asp:CheckBox ID="chkBuiltIn" runat="server" Checked='<%# Eval("IsBuiltIn") %>' Enabled="false" />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:Label ID="lblWinGroups" runat="server" Text='<%# Eval("WindowsGroups") %>' />
                            </div>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgContacts" runat="server" CommandName="Contacts" CommandArgument='<%# Eval("GroupID") %>'
                                AlternateText  ="Associa contatti" ImageUrl="~/IMG/Interfaccia/Grids/no_users.png" CausesValidation="false" />
                            <asp:ImageButton ID="imgUp" runat="server" CommandName="Move" CommandArgument="Up"
                                AlternateText="Su" ImageUrl="~/IMG/Interfaccia/Grids/arrow_up.png" CausesValidation="false" />
                            <asp:ImageButton ID="imgDown" runat="server" CommandName="Move" CommandArgument="Down"
                                AlternateText="Giù" ImageUrl="~/IMG/Interfaccia/Grids/arrow_down.png" CausesValidation="false" />
                            <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" AlternateText="Modifica"
                                ImageUrl="~/IMG/Interfaccia/Grids/edit.gif" CausesValidation="false" />
                            <asp:ImageButton ID="imgDelete" runat="server" CommandName="Delete" AlternateText='<%# Convert.ToBoolean(Eval("Associated")) ? "Impossibile eliminare il gruppo perché di sistema oppure associato ad almeno un report" : "Elimina il gruppo" %>'
                                Enabled='<%# !Convert.ToBoolean(Eval("Associated")) %>' ImageUrl="~/IMG/Interfaccia/Grids/delete.gif"
                                CausesValidation="false" OnClientClick='<%# string.Format("return confirm(\"Eliminare il gruppo {0}?\");", Eval("GroupName"))%>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <InsertItemTemplate>
                    <tr id="Tr2" runat="server">
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtName" runat="server" MaxLength="500" Width="85%" ValidationGroup="insert" />
                                <asp:RequiredFieldValidator ID="rfvalName" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="(!)" Display="Dynamic" ToolTip="Campo richiesto" ValidationGroup="insert"></asp:RequiredFieldValidator>
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtDesc" runat="server" MaxLength="2000" Width="90%" />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px; text-align: center;">
                                <asp:CheckBox ID="chkBuiltIn" runat="server" Checked="false" Enabled="false" />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtWinGroups" runat="server" Width="90%" />
                            </div>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgAccept" runat="server" CommandName="Insert" AlternateText="Conferma"
                                ValidationGroup="insert" ImageUrl="~/IMG/Interfaccia/Grids/accept.gif" />
                        </td>
                    </tr>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <tr id="Tr2" runat="server">
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("GroupName") %>' MaxLength="500"
                                    Width="85%" ValidationGroup="edit" Enabled='<%# !Convert.ToBoolean(Eval("IsBuiltIn")) %>' />
                                <asp:RequiredFieldValidator ID="rfvalName" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="(!)" Display="Dynamic" ToolTip="Campo richiesto" ValidationGroup="edit"></asp:RequiredFieldValidator>
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtDesc" runat="server" Text='<%# Bind("GroupDescription") %>' MaxLength="2000"
                                    Width="90%" />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px; text-align: center;">
                                <asp:CheckBox ID="chkBuiltIn" runat="server" Checked='<%# Eval("IsBuiltIn") %>' Enabled="false" />
                            </div>
                        </td>
                        <td>
                            <div style="padding: 2px;">
                                <asp:TextBox ID="txtWinGroups" runat="server" Text='<%# Bind("WindowsGroups") %>'
                                    Width="90%" />
                            </div>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgAccept" runat="server" CommandName="Update" AlternateText="Conferma"
                                ValidationGroup="edit" ImageUrl="~/IMG/Interfaccia/Grids/accept.gif" />
                            <asp:ImageButton ID="imgCancel" runat="server" CommandName="Cancel" AlternateText="Annulla"
                                ImageUrl="~/IMG/Interfaccia/Grids/cancel.png" CausesValidation="false" />
                        </td>
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnContactAssociate" Text="Associa contatti"
        Style="float: left; margin-top: 10px; display: none;" />
    <act:ModalPopupExtender runat="server" ID="mpeContactAssociate" BehaviorID="mpeContactAssociateBehavior"
        TargetControlID="btnContactAssociate" PopupControlID="popupContactAssociate"
        BackgroundCssClass="ModalBackground" DropShadow="False" RepositionMode="RepositionOnWindowScroll">
    </act:ModalPopupExtender>
    <asp:Panel runat="server" CssClass="modalPopup" ID="popupContactAssociate"
        Style="display: none; background-color: #9A9A9A; border-width: 2px; border-style: solid;
        border-color: #9A9A9A; padding: 3px; width: 800px;">
        <asp:UpdatePanel ID="updContactAssociate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="text-align: left; margin: 10px 0 10px">
                    <asp:TextBox ID="txtSearchContact" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                    <asp:Button ID="btnSearchConcat" runat="server" Text="Cerca" OnClick="btnSearchConcat_OnClick"/>
                    <br/><asp:label runat="server" ID="lblSearchContactResult" style="color: White; font-size: x-small" ></asp:label>
                </div>                
                <asp:GridView ID="grdContacts" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                    SkinID="gvwEmpty" CellPadding="4" DataKeyNames="ContactId,GroupId" DataSourceID="sdsContacts"
                    Width="100%" ShowFooter="False" PageSize="20" EmptyDataText="Nessun contatto presente"
                    AllowPaging="true" CssClass="modalGrid" OnRowDataBound="grdContacts_OnRowDataBound" OnPageIndexChanging="grdContacts_OnPageIndexChanging"> 
                    <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                    <HeaderStyle CssClass="modalGridTh" />
                    <RowStyle CssClass="modalGridRow" />
                    <AlternatingRowStyle CssClass="modalGridRowA" />
                    <PagerStyle CssClass="modalGridPager" />
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllContacts" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAllContacts_OnCheckedChanged" ToolTip="Seleziona tutto" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelectedContact" runat="server" Checked='<%# Eval("Associated")%>' />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="ContactName" HeaderText="Nome" ReadOnly="True">
                            <ItemStyle Width="45%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="WindowsUser" HeaderText="Windows User" ReadOnly="True">
                            <ItemStyle Width="50%" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sdsContacts" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                    SelectCommand="gris_GetContactsAssociatedToGroupsByRegId" SelectCommandType="StoredProcedure" 
                    OnSelecting="sdsContacts_Selecting" >
                    <SelectParameters>
                        <asp:Parameter Name="GroupId" />
                        <asp:Parameter Name="RegIds" Type="String" />
                        <asp:Parameter Name="Search" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Button runat="server" ID="btnCancelContactAssociate" Text="Chiudi" Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-right: 10px; margin-top: 10px;"
                    OnClick="btnCancelContactAssociate_Click" OnClientClick="hideModalPopupViaClient('mpeContactAssociateBehavior');"
                    ToolTip="Chiudi" />
                <asp:Button runat="server" ID="btnSaveContactAssociate" Text="Salva" Style="font-size: 10pt;
                    text-decoration: none; color: #000000; float: left; margin-top: 10px;" OnClick="btnSaveContactAssociate_Click"
                    OnClientClick="hideModalPopupViaClient('mpeContactAssociateBehavior');"
                    ToolTip="Salva" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>


    <asp:ObjectDataSource ID="odsGroups" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        SelectMethod="GetData" TypeName="GrisSuite.Data.Properties.PermissionsTableAdapters.PermissionGroupsTableAdapter" filterexpression="GroupName not like 'groupCommandControlSystem%'"
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="GroupID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="GroupName" Type="String" />
            <asp:Parameter Name="GroupDescription" Type="String" />
            <asp:Parameter Name="WindowsGroups" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="GroupID" Type="Int32" />
            <asp:Parameter Name="GroupName" Type="String" />
            <asp:Parameter Name="GroupDescription" Type="String" />
            <asp:Parameter Name="WindowsGroups" Type="String" />
            <asp:Parameter Name="IsBuiltIn" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</asp:Content>

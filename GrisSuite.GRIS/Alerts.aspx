<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Alerts.aspx.cs" CodeFileBaseClass="FiltersSavingPage" Inherits="Alerts"
    Title="GRIS - Allarmi -" IsFilterSavingEnabled="True" %>

<%@ Register Src="Controls/FilterButton.ascx" TagName="FilterButton" TagPrefix="gris" %>
<%@ Register Namespace="GrisControls" TagPrefix="gris" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <asp:Timer ID="tmrAlerts" runat="server" Enabled="true" OnTick="tmrAlerts_Tick">
    </asp:Timer>
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td>
                <asp:UpdatePanel ID="updToolBar" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
                            <tr>
                                <td style="background-image: url(IMG/interfaccia/nav_bg.gif); height: 30px;">
                                    &nbsp;
                                </td>
                                <td style="width: 42px;">
                                    <asp:Image runat="server" ID="imgNavSep2" ImageUrl="~/IMG/Interfaccia/nav_sep2.gif"
                                        CssClass="imgnb" />
                                    <asp:ImageButton ID="imgbUpdate" runat="server" ImageUrl="~/IMG/Interfaccia/btnRefresh_default.gif"
                                                     CssClass="imgnb" OnClick="imgbUpdate_Click"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="background-image: url(IMG/interfaccia/nav_shadow_bg.gif);
                                    height: 21px; background-repeat: repeat-x;">
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="8" cellspacing="8">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="updDeviceDetail" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <!-- tabella cornice alla zona di dettaglio -->
                                    <table id="tblDeviceDetail" runat="server" cellpadding="0" cellspacing="0" style="width: 98%;
                                        background-color: #808080; color: White;">
                                        <tr>
                                            <td style="text-align: left; vertical-align: top; width: 10px">
                                                <asp:Image ID="imgTopSx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_top_sx.gif"
                                                    Height="30px" EnableViewState="false"></asp:Image>
                                            </td>
                                            <td style="vertical-align: top; width: 30px">
                                                <asp:Image ID="imgStatus" runat="server" AlternateText="" />
                                            </td>
                                            <td style="background-image: url(IMG/Interfaccia/GraphicalPanel/pan_top_bg.gif);
                                                background-repeat: repeat-x; height: 30px; width: 100%; font-weight: bold;">
                                                <div style="width: 84px; float: left;">
                                                    <asp:Image ID="imgSTLCOffline" runat="server" ImageUrl="~/IMG/Interfaccia/stlc_offline.gif"
                                                        ToolTip="STLC 1000 offline" AlternateText="STLC 1000 offline" Visible="false" /></div>
                                                <div style="width: 900px; float: left; padding-right: 110px; height: 30px; margin: 0px;
                                                    padding-top: 4px">
                                                    <asp:Label ID="lblNodeName" runat="server"></asp:Label></div>
                                            </td>
                                            <td style="text-align: right; vertical-align: top;">
                                                <asp:Image ID="imgTopDx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_top_dx.gif"
                                                    Height="30px" EnableViewState="false"></asp:Image>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="text-align: center;">
                                                <!-- tabella interna alla zona di dettaglio -->
                                                <table id="tblDetails" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 17%;">
                                                            <table id="tblDevice" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                <tr>
                                                                    <td style="text-align: center; white-space: normal;">
                                                                        <asp:Label ID="lblDevice" runat="server" Width="100%" Style="font-size: 14px"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: center;">
                                                                        <asp:ImageButton ID="imgbDevice" runat="server" AlternateText="" OnClick="imgbDevice_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: center;">
                                                                        <asp:Label ID="lblVendor" runat="server" Style="font-size: 14px"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 2%; border-right: #4c4c4c 1px solid;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 57%; vertical-align: top; text-align: left; padding-right: 10px;
                                                            padding-left: 10px;">
                                                            <table style="border: 0px; padding: 0px; width: 100%;">
                                                                <tr>
                                                                    <td width="5%">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td width="69%" style="font-size: 16px; color: Black; font-weight: bold; padding-left: 3px;">
                                                                        Messaggio
                                                                    </td>
                                                                    <td width="26%" class="GridViewHeader2" style="font-size: 16px; color: Black; font-weight: bold;
                                                                        padding-left: 6px;">
                                                                        Data
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div style="overflow-y: auto; overflow-x: hidden; scrollbar-face-color: #d3d3d3;
                                                                scrollbar-highlight-color: #d3d3d3; scrollbar-shadow-color: #d3d3d3; scrollbar-3dlight-color: #4c4c4c;
                                                                scrollbar-arrow-color: #000000; scrollbar-track-color: #d3d3d3; scrollbar-darkshadow-color: #4c4c4c;
                                                                scrollbar-base-color: #d3d3d3;" runat="server" id="divViolatedRules">
                                                                <asp:GridView ID="gvwViolatedRules" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                                                    CellPadding="1" DataSourceID="odsViolatedRules" OnRowDataBound="gvwViolatedRules_RowDataBound"
                                                                    Width="100%" OnDataBound="gvwViolatedRules_DataBound" ShowHeader="false">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemStyle Width="6%" />
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="imgIsOpen" runat="server" ImageUrl="~/IMG/Interfaccia/alerts_new_rule.gif"
                                                                                    Style="border: 0px; padding: 0px; margin: 0px" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="RuleDescription" HeaderText="Messaggio" ItemStyle-Width="69%"
                                                                            HeaderStyle-ForeColor="#000000" ItemStyle-Font-Size="14px" />
                                                                        <asp:BoundField DataField="AlertCreationDate" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy HH:mm}"
                                                                            ItemStyle-Width="25%" HeaderStyle-ForeColor="#000000" ItemStyle-Font-Size="14px" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <asp:ObjectDataSource ID="odsViolatedRules" runat="server" OldValuesParameterFormatString="original_{0}"
                                                                SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.AlertsDSTableAdapters.ViolatedRulesTableAdapter"
                                                                OnSelecting="odsViolatedRules_Selecting">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="lvwAlerts" DbType="Guid" Name="AlertTicketID" PropertyName="SelectedValue" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                        </td>
                                                        <td style="width: 2%; border-left: #4c4c4c 1px solid;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 22%;">
                                                            <table id="tblTicket" cellpadding="0" cellspacing="0" style="width: 100%; text-align: left;
                                                                color: #000000; font-family: Arial; font-size: 10pt;">
                                                                <tr>
                                                                    <td>
                                                                        Ticket
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSaveTicketName" runat="server" Width="90%" MaxLength="30" Style="border: 1px solid #D3D3D3;
                                                                            font-family: Arial; font-size: 11px;"></asp:TextBox>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <br />
                                                                        Note
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSaveTicketNotes" runat="server" TextMode="MultiLine" Width="90%"
                                                                            Rows="5" Style="border: 1px solid #D3D3D3; font-family: Arial; font-size: 11px;"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="imgbSaveTicket" runat="server" ImageUrl="~/IMG/Interfaccia/save_ticket.gif"
                                                                            AlternateText="Assegna Ticket" ToolTip="Assegna Ticket" OnClick="imgbSaveTicket_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgBotSx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_bot_sx.gif"
                                                    EnableViewState="false"></asp:Image>
                                            </td>
                                            <td colspan="2" style="background-image: url(IMG/Interfaccia/GraphicalPanel/pan_bot_bg.gif);
                                                background-repeat: repeat-x;">
                                            </td>
                                            <td>
                                                <asp:Image ID="imgBotDx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_bot_dx.gif"
                                                    EnableViewState="false"></asp:Image>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- tabella filtri -->
                            <asp:UpdatePanel ID="updFilters" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <!-- filtro delle regole -->
                                    <div id="divRules" class="SelectedRule">
                                        <div class="SelectedRuleDX">
                                        </div>
                                        <div id="divRulesPadding" runat="server" style="padding-top: 7px;">
                                            <asp:Label ID="lblSelectedRule" runat="server" Text="Nessuna Regola"></asp:Label>
                                            <asp:Panel ID="pnlRules" runat="server" CssClass="ContextMenuPanel" Style="display: none;
                                                visibility: hidden;">
                                                <asp:DataList ID="dlstRules" runat="server" DataSourceID="odsAlertRules" DataKeyField="RuleID"
                                                    OnItemCommand="dlstRules_ItemCommand" Style="text-align: left;">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbRule" runat="server" Text='<%# Eval("Rule") %>' ToolTip='<%# Eval("RuleDescription") %>'
                                                            CssClass="ContextMenuItem" CommandName="Select" CommandArgument='<%# Eval("RuleID") %>' />
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </asp:Panel>
                                            <act:DropDownExtender runat="server" ID="ddeRules" TargetControlID="divRulesPadding"
                                                DropDownControlID="pnlRules" DropArrowBackColor="Transparent" HighlightBackColor="Transparent"
                                                HighlightBorderColor="Transparent" DropArrowImageUrl="IMG/Interfaccia/arrow_combo.gif" />
                                        </div>
                                        <asp:ObjectDataSource ID="odsAlertRules" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetEnabledRules" TypeName="GrisSuite.Data.Gris.AlertsDSTableAdapters.AlertRulesTableAdapter">
                                        </asp:ObjectDataSource>
                                    </div>
                                    <!-- filtro di Compartimento/Linea/Stazione -->
                                    <asp:Panel ID="divLocation" runat="server" DefaultButton="btnLcSearch" Style="float: left;
                                        width: 250px; margin-left: 10px;">
                                        <table border="0" cellpadding="0" cellspacing="0" class="searchBox">
                                            <tr>
                                                <td class="searchBoxSx">
                                                    <asp:Button ID="btnLcClear" runat="server" CommandName="clear" EnableViewState="false"
                                                        CssClass="searchButton" OnCommand="btnLcSearch_Command" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="searchBoxText" MaxLength="30"></asp:TextBox>
                                                    <act:TextBoxWatermarkExtender ID="txtLocationWM" runat="server" TargetControlID="txtLocation"
                                                        WatermarkText=" -- Compartimento\Linea\Stazione" WatermarkCssClass="watermarked2" />
                                                </td>
                                                <td class="searchBoxDx">
                                                    <asp:Button ID="btnLcSearch" runat="server" CommandName="search" EnableViewState="false"
                                                        CssClass="searchButton" OnCommand="btnLcSearch_Command" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <!-- filtro di Tipo Periferica -->
                                    <div id="divDevType" class="SelectedRule_Short" style="margin-left: 10px;">
                                        <div class="SelectedRuleDX">
                                        </div>
                                        <div id="divTypePadding" runat="server" style="padding-top: 7px;">
                                            <asp:Label ID="lblDevType" runat="server" Text="Nessun Tipo Periferica"></asp:Label>
                                            <asp:Panel ID="pnlDevType" runat="server" CssClass="ContextMenuPanel" ScrollBars="Vertical"
                                                Style="display: none; visibility: hidden; height: 300px; width: 210px;">
                                                <asp:DataList ID="dlstDevType" runat="server" DataSourceID="sqlDevTypes" DataKeyField="DeviceTypeID"
                                                    OnItemCommand="dlstDevType_ItemCommand" Style="text-align: left;">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbDevType" runat="server" Text='<%# Eval("DeviceType") %>'
                                                            ToolTip='<%# Eval("DeviceTypeDescription") %>' CssClass="ContextMenuItem" CommandName="Select"
                                                            CommandArgument='<%# Eval("DeviceTypeID") %>' />
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </asp:Panel>
                                            <act:DropDownExtender runat="server" ID="ddeDevType" TargetControlID="divTypePadding"
                                                DropDownControlID="pnlDevType" DropArrowBackColor="Transparent" HighlightBackColor="Transparent"
                                                HighlightBorderColor="Transparent" DropArrowImageUrl="IMG/Interfaccia/arrow_combo.gif" />
                                        </div>
                                        <asp:SqlDataSource ID="sqlDevTypes" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                                            SelectCommand="SELECT '' as [DeviceTypeID], 'Nessun Filtro' as [DeviceType], 'Nessun Filtro' as [DeviceTypeDescription], -1 as [Order] UNION ALL SELECT [DeviceTypeID], (CASE WHEN LEN([DeviceTypeID] + ISNULL(' - ' + [DeviceTypeDescription], '')) > 25 THEN SUBSTRING([DeviceTypeID] + ISNULL(' - ' + [DeviceTypeDescription], ''), 1, 22) + '...' ELSE DeviceTypeDescription END) as [DeviceType], [DeviceTypeDescription], [Order] FROM [device_type] ORDER BY [Order]">
                                        </asp:SqlDataSource>
                                    </div>
                                    <!-- filtro di Ticket -->
                                    <asp:Panel ID="divTicketSearch" runat="server" DefaultButton="btnTkSearch" Style="float: left;
                                        width: 200px; margin-left: 10px;">
                                        <table border="0" cellpadding="0" cellspacing="0" class="searchBox">
                                            <tr>
                                                <td class="searchBoxSx">
                                                    <asp:Button ID="btnTkClear" runat="server" CommandName="clear" EnableViewState="false"
                                                        CssClass="searchButton" OnCommand="btnTkSearch_Command" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTicketName" runat="server" CssClass="searchBoxText" MaxLength="30"></asp:TextBox>
                                                    <act:TextBoxWatermarkExtender ID="txtTicketNameWM" runat="server" TargetControlID="txtTicketName"
                                                        WatermarkText=" -- Ricerca Ticket" WatermarkCssClass="watermarked2" />
                                                </td>
                                                <td class="searchBoxDx">
                                                    <asp:Button ID="btnTkSearch" runat="server" CommandName="search" EnableViewState="false"
                                                        CssClass="searchButton" OnCommand="btnTkSearch_Command" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div style="text-align: left;">
                                        <table cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <gris:FilterButton ID="FilterNew" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nuovi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                        Active="true" OnClick="FlagFilter_Click" />
                                                </td>
                                                <td>
                                                    <gris:FilterButton ID="FilterProcessed" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Processati&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                        OnClick="FlagFilter_Click" />
                                                </td>
                                                <td>
                                                    <gris:FilterButton ID="FilterDiscarded" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rientrati&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                        OnClick="FlagFilter_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- griglia allarmi -->
                            <asp:UpdatePanel ID="updAlerts" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table id="tblAlertsList" cellpadding="0" cellspacing="0" style="width: 100%;">
                                        <tr>
                                            <td style="width: 70%; text-align: left; padding-left: 10px;">
                                                <asp:Label ID="lblAlertCountTop" runat="server" ForeColor="#2CD3FB" />
                                            </td>
                                            <td style="width: 30%; text-align: center;">
                                                <asp:DataPager ID="dpgAlertsTop" runat="server" PageSize="20" PagedControlID="lvwAlerts">
                                                    <Fields>
                                                        <asp:TemplatePagerField OnPagerCommand="dpgAlerts_PageChanged">
                                                            <PagerTemplate>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnFirst" runat="server" CommandName="first" ToolTip="Allarmi pi� recenti"
                                                                        Enabled="<%# (Container.StartRowIndex > 0) %>" ImageUrl="~/IMG/Interfaccia/pag_first.gif"
                                                                        ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif" AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnPrivious" runat="server" CommandName="previous" ToolTip="Allarmi Precedenti"
                                                                        Enabled="<%# (Container.StartRowIndex > 0) %>" ImageUrl="~/IMG/Interfaccia/pag_prev.gif"
                                                                        ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif" AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 80px; border: 0px; padding: 0px; margin: 0px; text-align: center;
                                                                    padding-top: 2px; white-space: nowrap;">
                                                                    <asp:TextBox ID="txtCurrentPageTop" runat="server" Style="font-family: Arial; font-size: 12px;
                                                                        font-weight: bold; vertical-align: top; width: 50px;"></asp:TextBox></div>
                                                                <div style="float: left; width: 80px; border: 0px; padding: 0px; margin: 0px; text-align: center;
                                                                    padding-top: 2px; white-space: nowrap;">
                                                                    <asp:Label ID="lblPageNumberTop" runat="server" Style="font-family: Arial; color: #FFFFFF;
                                                                        font-size: 12px; font-weight: bold; vertical-align: top;"></asp:Label></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnGoTo" runat="server" CommandName="goto" CommandArgument="top"
                                                                        ToolTip="Pagina esatta" Enabled="<%# (Container.TotalRowCount > Container.PageSize) %>"
                                                                        ImageUrl="~/IMG/Interfaccia/pag_exact.gif" ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif"
                                                                        AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnNext" runat="server" CommandName="next" ToolTip="Allarmi Successivi"
                                                                        Enabled="<%# (Container.StartRowIndex < this.GetLastPageFirstRowIndex(Container.TotalRowCount, Container.PageSize)) %>"
                                                                        ImageUrl="~/IMG/Interfaccia/pag_next.gif" ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif"
                                                                        AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnLast" runat="server" CommandName="last" ToolTip="Fine"
                                                                        Enabled="<%# (Container.StartRowIndex < this.GetLastPageFirstRowIndex(Container.TotalRowCount, Container.PageSize)) %>"
                                                                        ImageUrl="~/IMG/Interfaccia/pag_last.gif" ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif"
                                                                        AutoComposeImageDisabledUrl="false" /></div>
                                                            </PagerTemplate>
                                                        </asp:TemplatePagerField>
                                                    </Fields>
                                                </asp:DataPager>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:ListView ID="lvwAlerts" runat="server" DataSourceID="odsAlertTickets" DataKeyNames="AlertTicketID"
                                                    OnItemDataBound="lvwAlerts_ItemDataBound" OnSelectedIndexChanged="lvwAlerts_SelectedIndexChanged"
                                                    OnDataBound="lvwAlerts_DataBound" OnItemCommand="lvwAlerts_ItemCommand" OnSorting="lvwAlerts_Sorting">
                                                    <LayoutTemplate>
                                                        <table id="Table2" runat="server" style="width: 100%;">
                                                            <tr id="Tr1" runat="server">
                                                                <td id="Td1" runat="server">
                                                                    <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;"
                                                                        cellpadding="0" cellspacing="0">
                                                                        <tr id="Tr2" runat="server" style="">
                                                                            <th id="Th1" runat="server" style="width: 4%;">
                                                                            </th>
                                                                            <th id="Th2" runat="server" style="width: 20%;" class="ListViewHeader">
                                                                                <asp:LinkButton ID="lnkNodeHeader" runat="server" Text="Stazione" CommandName="Sort"
                                                                                    CommandArgument="NodeName"></asp:LinkButton>
                                                                            </th>
                                                                            <th id="Th3" runat="server" style="width: 20%;" class="ListViewHeader">
                                                                                <asp:LinkButton ID="lnkDeviceHeader" runat="server" Text="Periferica" CommandName="Sort"
                                                                                    CommandArgument="DeviceName"></asp:LinkButton>
                                                                            </th>
                                                                            <th id="Th4" runat="server" style="width: 11%;" class="ListViewHeader">
                                                                                <asp:LinkButton ID="lnkSystemHeader" runat="server" Text="Sistema" CommandName="Sort"
                                                                                    CommandArgument="SystemID"></asp:LinkButton>
                                                                            </th>
                                                                            <th id="Th5" runat="server" style="width: 15%;" class="ListViewHeader">
                                                                                <asp:LinkButton ID="lnkCreationDateHeader" runat="server" Text="Data di creazione"
                                                                                    CommandName="Sort" CommandArgument="CreationDate"></asp:LinkButton>
                                                                            </th>
                                                                            <th id="Th7" runat="server" style="width: 15%;" class="ListViewHeader">
                                                                                <asp:LinkButton ID="lnkCloseDateHeader" runat="server" Text="Data di rientro" CommandName="Sort"
                                                                                    CommandArgument="CloseDate"></asp:LinkButton>
                                                                            </th>
                                                                            <th id="Th6" runat="server" style="width: 15%;" class="ListViewHeader">
                                                                                <asp:LinkButton ID="lnkTicketHeader" runat="server" Text="Ticket" CommandName="Sort"
                                                                                    CommandArgument="TicketName"></asp:LinkButton>
                                                                            </th>
                                                                        </tr>
                                                                        <tr id="itemPlaceholder" runat="server">
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </LayoutTemplate>
                                                    <ItemTemplate>
                                                        <tr style="">
                                                            <td>
                                                                <asp:ImageButton ID="imgbAlertType" runat="server" CommandName="select" CommandArgument='<%# Eval("DevID") %>'
                                                                    AlternateText="" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:LinkButton ID="lnkbNodeName" runat="server" CommandName="select" CommandArgument='<%# Eval("DevID") %>'
                                                                    Text='<%# Eval("NodeName") %>' ToolTip='<%# Eval("Location") %>' CssClass="GridViewRow" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:LinkButton ID="lnkbDeviceName" runat="server" CommandName="select" CommandArgument='<%# Eval("DevID") %>'
                                                                    Text='<%# Eval("DeviceName") %>' CssClass="GridViewRow" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Image ID="imgSystem" runat="server" AlternateText="" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblCreationDate" runat="server" Text='<%# Eval("CreationDate", "{0:dd/MM/yyyy HH:mm}") %>'
                                                                    CssClass="GridViewRow" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblCloseDate" runat="server" Text='<%# Eval("CloseDate", "{0:dd/MM/yyyy HH:mm}") %>'
                                                                    CssClass="GridViewRow" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblTicketName" runat="server" Text='<%# Eval("TicketName") %>' CssClass="GridViewRow" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <SelectedItemTemplate>
                                                        <tr style="background-color: #808080;">
                                                            <td>
                                                                <asp:ImageButton ID="imgbAlertType" CommandArgument='<%# Eval("DevID") %>' runat="server"
                                                                    AlternateText="" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblNodeName" runat="server" Text='<%# Eval("NodeName") %>' ToolTip='<%# Eval("Location") %>'
                                                                    CssClass="GridViewRowSelected" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblDeviceName" runat="server" Text='<%# Eval("DeviceName") %>' CssClass="GridViewRowSelected" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Image ID="imgSystem" runat="server" AlternateText="" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblCreationDate" runat="server" Text='<%# Eval("CreationDate", "{0:dd/MM/yyyy HH:mm}") %>'
                                                                    CssClass="GridViewRowSelected" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblCloseDate" runat="server" Text='<%# Eval("CloseDate", "{0:dd/MM/yyyy HH:mm}") %>'
                                                                    CssClass="GridViewRowSelected" />
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblTicketName" runat="server" Text='<%# Eval("TicketName") %>' CssClass="GridViewRowSelected" />
                                                            </td>
                                                        </tr>
                                                    </SelectedItemTemplate>
                                                    <ItemSeparatorTemplate>
                                                        <tr>
                                                            <td colspan="7">
                                                                <div style="border-top-style: solid; border-top-color: #808080; border-top-width: 1px;
                                                                    height: 2px;">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemSeparatorTemplate>
                                                </asp:ListView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70%; text-align: left; padding-left: 10px;">
                                                <asp:Label ID="lblAlertCountBot" runat="server" ForeColor="#2CD3FB" />
                                            </td>
                                            <td style="width: 30%; text-align: center;">
                                                <asp:DataPager ID="dpgAlertsBot" runat="server" PageSize="20" PagedControlID="lvwAlerts">
                                                    <Fields>
                                                        <asp:TemplatePagerField OnPagerCommand="dpgAlerts_PageChanged">
                                                            <PagerTemplate>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnFirst" runat="server" CommandName="first" ToolTip="Allarmi pi� recenti"
                                                                        Enabled="<%# (Container.StartRowIndex > 0) %>" ImageUrl="~/IMG/Interfaccia/pag_first.gif"
                                                                        ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif" AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnPrivious" runat="server" CommandName="previous" ToolTip="Allarmi Precedenti"
                                                                        Enabled="<%# (Container.StartRowIndex > 0) %>" ImageUrl="~/IMG/Interfaccia/pag_prev.gif"
                                                                        ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif" AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 80px; border: 0px; padding: 0px; margin: 0px; text-align: center;
                                                                    padding-top: 2px; white-space: nowrap;">
                                                                    <asp:TextBox ID="txtCurrentPageBot" runat="server" Style="font-family: Arial; font-size: 12px;
                                                                        font-weight: bold; vertical-align: top; width: 50px;"></asp:TextBox></div>
                                                                <div style="float: left; width: 80px; border: 0px; padding: 0px; margin: 0px; text-align: center;
                                                                    padding-top: 2px; white-space: nowrap;">
                                                                    <asp:Label ID="lblPageNumberBot" runat="server" Style="font-family: Arial; color: #FFFFFF;
                                                                        font-size: 12px; font-weight: bold; vertical-align: top;"></asp:Label></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnGoTo" runat="server" CommandName="goto" CommandArgument="bot"
                                                                        ToolTip="Pagina esatta" Enabled="<%# (Container.TotalRowCount > Container.PageSize) %>"
                                                                        ImageUrl="~/IMG/Interfaccia/pag_exact.gif" ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif"
                                                                        AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnNext" runat="server" CommandName="next" ToolTip="Allarmi Successivi"
                                                                        Enabled="<%# (Container.StartRowIndex < this.GetLastPageFirstRowIndex(Container.TotalRowCount, Container.PageSize)) %>"
                                                                        ImageUrl="~/IMG/Interfaccia/pag_next.gif" ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif"
                                                                        AutoComposeImageDisabledUrl="false" /></div>
                                                                <div style="float: left; width: 30px; border: 0px; padding: 0px; margin: 0px">
                                                                    <gris:GrisImageButton ID="btnLast" runat="server" CommandName="last" ToolTip="Fine"
                                                                        Enabled="<%# (Container.StartRowIndex < this.GetLastPageFirstRowIndex(Container.TotalRowCount, Container.PageSize)) %>"
                                                                        ImageUrl="~/IMG/Interfaccia/pag_last.gif" ImageDisabledUrl="~/IMG/Interfaccia/pag_disabled.gif"
                                                                        AutoComposeImageDisabledUrl="false" /></div>
                                                            </PagerTemplate>
                                                        </asp:TemplatePagerField>
                                                    </Fields>
                                                </asp:DataPager>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="tmrAlerts" EventName="Tick" />
                                    <asp:AsyncPostBackTrigger ControlID="imgbUpdate" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnTkSearch" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnLcSearch" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="odsAlertTickets" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetDataWithPermissions" TypeName="GrisSuite.Data.Gris.AlertsDSTableAdapters.AlertTicketsTableAdapter"
                                EnablePaging="True" MaximumRowsParameterName="MaximumRows" SelectCountMethod="GetAlertsCountWithPermissions"
                                StartRowIndexParameterName="StartRowIndex" OnSelecting="odsAlertTickets_Selecting"
                                SortParameterName="Order">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtTicketName" DefaultValue="" PropertyName="Text"
                                        Name="TicketName" Type="String"></asp:ControlParameter>
                                    <asp:ControlParameter ControlID="txtLocation" DefaultValue="" PropertyName="Text"
                                        Name="Location" Type="String"></asp:ControlParameter>
                                    <asp:ControlParameter ControlID="dlstDevType" DefaultValue="" PropertyName="SelectedValue"
                                        Name="Type" Type="String"></asp:ControlParameter>
                                    <asp:ControlParameter ControlID="FilterNew" DefaultValue="1" Name="New" PropertyName="Active"
                                        Type="Boolean" />
                                    <asp:ControlParameter ControlID="FilterProcessed" DefaultValue="0" Name="Processed"
                                        PropertyName="Active" Type="Boolean" />
                                    <asp:ControlParameter ControlID="FilterDiscarded" DefaultValue="0" Name="Discarded"
                                        PropertyName="Active" Type="Boolean" />
                                    <asp:ControlParameter ControlID="dlstRules" DefaultValue="-1" Name="AlertRuleID"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:Parameter DefaultValue="-1" Name="SrvID" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

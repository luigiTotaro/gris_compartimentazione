using System;
using GrisSuite.Common;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool okGris = GrisPermissions.CanUserAccessGris();
        bool okFilters = GrisPermissions.CanAccessResource((int) DatabaseResource.DeviceFilters);

        this.GrisStart.Visible = okGris;
        this.NoGrisStart.Visible = !okGris && okFilters;
        this.GrisNoPermissions.Visible = !okGris && !okFilters;
    }
}

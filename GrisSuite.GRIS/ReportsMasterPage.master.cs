using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using GrisSuite.Common;

public partial class ReportsMasterPage : ReportsMaster
{
    # region Properties
    public override bool FiltersButtonEnabled
    {
        get { return this.btnReportOptions.Enabled; }
        set
        {
            this.btnReportOptions.Style.Add(HtmlTextWriterStyle.Cursor, ( value ) ? "hand" : "none");
            this.btnReportOptions.Enabled = value;
        }
    }

    public override bool ReportListEnabled
    {
        get { return this.btnSelectReport.Enabled; }
        set
        {
            this.btnSelectReport.Style.Add(HtmlTextWriterStyle.Cursor, ( value ) ? "hand" : "none");
            this.btnSelectReport.Enabled = value;
        }
    }

    public override bool ReportListPanelVisible
    {
        get
        {
            return Convert.ToBoolean(this.ViewState["ReportListPanelVisible"] ?? false);
        }
        set
        {
            this.ViewState["ReportListPanelVisible"] = value;
            if ( value )
            {
                this.tdSelectReport.Attributes["class"] = "ToolbarButtonToggleOn";
                this.tdReportList.Style[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                this.tdSelectReport.Attributes["class"] = "ToolbarButtonToggleOff";
                this.tdReportList.Style[HtmlTextWriterStyle.Display] = "none";
            }
        }
    }

    public override bool FiltersPanelVisible
    {
        get
        {
            return Convert.ToBoolean(this.ViewState["FiltersPanelVisible"] ?? false);
        }
        set
        {
            this.ViewState["FiltersPanelVisible"] = value;
            if ( value )
            {
                this.tdReportOptions.Attributes["class"] = "ToolbarButtonToggleOn";
                this.tdReportFilters.Style[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                this.tdReportOptions.Attributes["class"] = "ToolbarButtonToggleOff";
                this.tdReportFilters.Style[HtmlTextWriterStyle.Display] = "none";
            }
        }
    }

    public override bool ReportPanelVisible
    {
        get
        {
            return Convert.ToBoolean(this.ViewState["ReportPanelVisible"] ?? false);
        }
        set
        {
            this.ViewState["ReportPanelVisible"] = value;
            if ( value )
            {
                this.tdReport.Style[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                this.tdReport.Style[HtmlTextWriterStyle.Display] = "none";
            }
        }
    }
    # endregion

    # region Reports Navigation Bar

    public override string GetSelectedReportTitle ()
    {
        if ( this.SelectedReport.Length > 0 )
        {
            foreach ( AccordionPane pane in this.accReportList.Panes )
            {
                if ( pane.ContentContainer != null )
                {
                    Repeater rptReportLinks = pane.ContentContainer.FindControl("rptReportLinks") as Repeater;

                    if (rptReportLinks != null)
                    {
                        foreach (RepeaterItem item in rptReportLinks.Items)
                        {
                            foreach (Control ctrl in item.Controls)
                            {
                                if (((ctrl as LinkButton) != null) && (((LinkButton) ctrl).CommandName == this.SelectedReport))
                                {
                                    return ((LinkButton) ctrl).Text;
                                }
                            }
                        }
                    }
                }
            }
        }

        return "";
    }

    private void OpenSelectedReportPanel ()
    {
        if ( this.SelectedReport.Length > 0 )
        {
            foreach ( AccordionPane pane in this.accReportList.Panes )
            {
                if (pane.ContentContainer != null)
                {
                    Repeater rptReportLinks = pane.ContentContainer.FindControl("rptReportLinks") as Repeater;

                    if (rptReportLinks != null)
                    {
                        foreach (RepeaterItem item in rptReportLinks.Items)
                        {
                            foreach (Control ctrl in item.Controls)
                            {

                                if ((ctrl as LinkButton) != null)
                                {
                                    LinkButton lnkb = (LinkButton) ctrl;
                                    lnkb.Font.Bold = false;

                                    if (((LinkButton) ctrl).CommandName == this.SelectedReport)
                                    {
                                        this.accReportList.SelectedIndex = this.accReportList.Panes.IndexOf(pane);
                                        ((LinkButton) ctrl).Font.Bold = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    # endregion

    # region Event Handlers

    protected void Page_Load ( object sender, EventArgs e )
    {
        this.Page.Title = "GRIS - Reports -";

        InsertLocalServerTypeName("lnkbSTLCOffline");
        InsertLocalServerTypeName("lnkbSTLCList");	}

    protected void Page_Prerender ( object sender, EventArgs e )
    {
        this.accReportList.DataBind();
        this.OpenSelectedReportPanel();

        this.lblReportTitle.Text = this.GetSelectedReportTitle().ToUpper();
    }

    protected void accReportList_ItemDataBound ( object sender, AccordionItemEventArgs e )
    {
        if ( e.ItemType == AccordionItemType.Content )
        {
            Repeater rpt = (Repeater) e.AccordionItem.FindControl("rptReportLinks");
            HiddenField hid = (HiddenField) e.AccordionItem.FindControl("hidResourceCategoryID");

            int reportCategoryId;
            if ( int.TryParse(hid.Value, out reportCategoryId) )
            {
                rpt.DataSource = ReportPermissionGroups.GetReportsByCategory(reportCategoryId);
                rpt.DataBind();
            }
        }
    }

    protected void ReportLink_Command ( Object sender, CommandEventArgs e )
    {
        // selezione della pagina di report
        this.Response.Redirect(string.Format("~/Reports/{0}.aspx?selRep={0}", e.CommandName));
    }

    protected void btnSelectReport_Click ( object sender, EventArgs e )
    {
        this.ReportListPanelVisible = !( this.tdSelectReport.Attributes["class"] == "ToolbarButtonToggleOn" );
    }

    protected void btnReportOptions_Click ( object sender, EventArgs e )
    {
        this.FiltersPanelVisible = !( this.tdReportOptions.Attributes["class"] == "ToolbarButtonToggleOn" );
    }


    protected void btnGenerate_Click ( object sender, EventArgs e )
    {
        //Console.WriteLine(this.Context.Timestamp);
        this.InvokeReportGeneration();

        // i filtri vanno salvati
        FiltersSavingPage filtersSavingPage = this.Page as FiltersSavingPage;
        if (filtersSavingPage != null)
        {
            ( filtersSavingPage ).SavePageFilters();
        }
    }

    protected void imgbAdvancedOptions_Click ( object sender, EventArgs e )
    {
        this.ShowAdvancedOptions(!this.ReportPublisher.ReportViewer.ShowToolBar);
    }

    public override void ObjectDataSource_Selected ( object sender, ObjectDataSourceStatusEventArgs e )
    {
        if ( this.IsPostBack && e.ReturnValue != null )
        {
            int rows = ( (DataTable) e.ReturnValue ).Rows.Count;

            //this.imgbAdvancedOptions.Visible = ( rows > 0 );
            this.pnlResult.Visible = ( rows > 0 );
            this.pnlNoResult.Visible = !( rows > 0 );
        }
    }

    # endregion

    # region Methods

    public override void InvokeReportGeneration ()
    {
        this.ReportPublisher.OnInvokeReportGeneration();
        this.ReportPanelVisible = true;
        this.ShowAdvancedOptions(true);
        this.pnlResult.Visible = true; // � necessario che il pannello sia visibile affinch� il reportviewer chieda i dati all' objectdatasource e venga scatenato l'evento di selected
        this.ReportPublisher.ReportViewer.LocalReport.Refresh();

        //this.imgbAdvancedOptions.Visible = true;
    }

    private void ShowAdvancedOptions ( bool show )
    {
        this.ReportPublisher.ReportViewer.ShowToolBar = show;

        if ( show )
        {
            this.imgbAdvancedOptions.ImageUrl = "~/IMG/interfaccia/opzioni_nascondi.gif";
            this.ReportPublisher.ReportViewer.Style[HtmlTextWriterStyle.PaddingBottom] = "30px";
        }
        else
        {
            this.imgbAdvancedOptions.ImageUrl = "~/IMG/interfaccia/opzioni_mostra.gif";
            this.ReportPublisher.ReportViewer.Style[HtmlTextWriterStyle.PaddingBottom] = "0px";
        }
    }

    private void InsertLocalServerTypeName ( string controlName )
    {
        LinkButton l = this.accReportList.FindControl(controlName) as LinkButton;
        if ( l != null ) l.Text = string.Format(l.Text, GUtility.GetLocalServerTypeName());
    }

    protected bool UserHasReportPermissions ( object reportId )
    {
        if (reportId is int)
        {
            return GrisPermissions.CanAccessResource((int)reportId);
        }
        return false;
    }

    # endregion
}

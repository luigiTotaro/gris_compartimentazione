<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Zone.aspx.cs" Inherits="Zone" StylesheetTheme="Gris" %>

<%@ Register Src="~/Controls/StazioneIcon.ascx" TagName="StazioneIcon" TagPrefix="ucStazioneIcon" %>
<%@ Register Src="Controls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="gris" %>
<%@ Register Src="Controls/InfoFloatingBalloon.ascx" TagName="InfoFloatingBalloon"
    TagPrefix="gris" %>
<%@ Register Src="~/Controls/LayeredIcon.ascx" TagName="LayeredIcon" TagPrefix="gris" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <asp:Timer ID="tmrZone" runat="server" Enabled="true" OnTick="tmrZone_Tick">
    </asp:Timer>
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; height: 100%">
        <tr align="left">
            <td>
                <asp:UpdatePanel ID="updNavBar" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <gris:NavigationBar ID="navBar" runat="server" OnLayoutSwitched="navBar_OnLayoutSwitched"
                            OnRefreshPageButtonClick="navBar_OnRefreshPageButtonClick" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrZone" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="updPageLayout" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:MultiView ID="LineaMultiView" runat="server" ActiveViewIndex="0">
                            <asp:View ID="GraphicalView" runat="server">
                                <table id="tblMambo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                    height: 100%">
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptTratta" runat="server" OnItemDataBound="rptTratta_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:DataList ID="dlstStazione" runat="server" DataKeyField="RegID" OnItemDataBound="dlstStazione_ItemDataBound"
                                                        RepeatDirection="Horizontal" BorderWidth="0px" CellPadding="0" RepeatColumns="6"
                                                        OnItemCreated="dlstStazione_ItemCreated">
                                                        <ItemTemplate>
                                                            <ucStazioneIcon:StazioneIcon ID="StazioneIcon1" runat="server" NodID='<%# Eval("NodID") %>'
                                                                Compartimento='<%# Eval("RegName") %>' Linea='<%# Eval("ZonName") %>' Stazione='<%# Eval("NodName") %>'
                                                                Ok='<%# Eval("Ok") %>' Offline='<%# Eval("Unknown") %>' Errors='<%# Eval("Error") %>'
                                                                Warning='<%# Eval("Warning") %>' STLCOK='<%# Eval("STLCOK") %>' STLCInMaintenance='<%# Eval("STLCInMaintenance") %>'
                                                                STLCOffline='<%# Eval("STLCOffline") %>' STLCNotClassified='<%# Eval("STLCNotClassified") %>'
                                                                STLCTotalCount='<%# Eval("STLCTotalCount") %>' Status='<%# Eval("Status") %>'>
                                                            </ucStazioneIcon:StazioneIcon>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="200px" />
                                                    </asp:DataList>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 50px; vertical-align: bottom;">
                                            <asp:Image ID="imgLegenda" runat="server" ImageUrl="~/IMG/Compartimenti/legenda_linearizzata.gif"
                                                EnableViewState="false" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="TabularView" runat="server">
                                <div style="text-align: left;">
                                    <asp:GridView ID="gvwStazioni" runat="server" SkinID="SWMC4" AutoGenerateColumns="False"
                                        DataKeyNames="RegID,ZonID,NodID" DataSourceID="odsNodes" Width="100%" OnRowDataBound="gvwStazioni_RowDataBound"
                                        OnSelectedIndexChanged="gvwStazioni_SelectedIndexChanged">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <gris:LayeredIcon ID="ImageStatus" runat="server" LayeredIconType="VistaTabellare" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep0" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Stazione" SortExpression="NodName">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbStazione" runat="server" Text='<%# Bind("NodName") %>' CommandName="Select"></asp:LinkButton>
                                                    <gris:InfoFloatingBalloon ID="InfoBalloon" runat="server" Message="Sistema di monitoraggio non raggiungibile"
                                                        ZIndex="10" Top="20" Left="0" />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="GridViewLeftPadding" Width="420px" />
                                                <ItemStyle CssClass="GridViewLeftPadding" Width="420px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep1" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Unknown" HeaderText="Sconosciuto" ReadOnly="True" SortExpression="Unknown">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep2" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Error" HeaderText="Anomalia grave" ReadOnly="True" SortExpression="Error">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep3" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Warning" HeaderText="Anomalia lieve" ReadOnly="True" SortExpression="Warning">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep4" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Tacitate" ReadOnly="True">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep5" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="STLCOffline" HeaderText="STLC Offline" SortExpression="STLCOffline">
                                                <ItemStyle Width="180px" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="ImageSep6" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                </ItemTemplate>
                                                <ControlStyle CssClass="GridViewTemplate" />
                                                <ItemStyle Width="2px" BackColor="#4d4d4d" />
                                            </asp:TemplateField>
                                            <asp:CommandField SelectText="..." ShowSelectButton="True" ButtonType="Image" SelectImageUrl="~/IMG/interfaccia/tab_select.gif">
                                                <ItemStyle Width="30px" BackColor="#4d4d4d" />
                                            </asp:CommandField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <span style="font-size: 16pt; color: #ffffff; font-family: Arial;"><strong>Informazioni
                                                    non Disponibili</strong> </span>
                                            </div>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <br />
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="navBar" EventName="LayoutSwitched" />
                        <asp:AsyncPostBackTrigger ControlID="tmrZone" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="odsNodes" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.NodeDSTableAdapters.NodeTableAdapter"
                    OnSelecting="odsNodes_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="ZonId" Type="Int64" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

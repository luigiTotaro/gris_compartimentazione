<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>GRIS</title>
    <link rel="icon" href="favicon.ico" />
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
</head>
<body style="background-color: Black; text-align:center">
    <form id="frmDefault" runat="server">
		<div style="position:relative; width: 800px; height:800px;">
			<asp:panel id="GrisStart" runat="server" style="width: 800px; height: 800px; display: block; position: absolute; top: 0px; left:0px" Visible="False">
				<img id="imgStart" runat="server" src="IMG/GrisStart.jpg" alt="Benvenuti in Gris" />
				<img id="imgWait" runat="server" src="IMG/GrisRefresh.gif" alt="Attendi" style="position: absolute; top: 430px; left: 375px" />
				<script id="jsRedirect" type="text/javascript" language="javascript">
					window.setTimeout(function() { window.location.href = 'Italia.aspx'; }, 500);
				</script>
			</asp:panel>
            <asp:panel id="NoGrisStart" runat="server" style="width: 800px; height: 800px; display: block; position: absolute; top: 0px; left:0px" Visible="False">  
				<img id="img1" runat="server" src="IMG/GrisStart.jpg" alt="Benvenuti in Gris" />
				<img id="img2" runat="server" src="IMG/GrisRefresh.gif" alt="Attendi" style="position: absolute; top: 430px; left: 375px" />
				<script id="Script1" type="text/javascript" language="javascript">
				    window.setTimeout(function () { window.location.href = 'Filters.aspx'; }, 500);
				</script>
			</asp:panel>
			<asp:panel id="GrisNoPermissions" runat="server" style="width: 800px; height: 800px; position:absolute; top: 0px; left:0px" Visible="False">
				<img id="imgNoPermissions" runat="server" src="IMG/GrisAutorizza.jpg" alt="Permessi necessari" />
			</asp:panel>
		</div>
    </form>
</body>
</html>

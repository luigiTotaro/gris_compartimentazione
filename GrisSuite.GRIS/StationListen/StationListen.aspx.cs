using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ListenDevicesDSTableAdapters; 

public partial class StationListen : Page {

    #region Properties

    public long DevID {
        get { return Convert.ToInt64(this.ViewState["DevID"]); }

        set { this.ViewState["DevID"] = value; }
    }

    public long NodID {
        get { return Convert.ToInt64(this.ViewState["NodID"]); }

        set { this.ViewState["NodID"] = value; }
    }

    public ushort original_NodID {
        get { return Convert.ToUInt16(this.ViewState["original_NodID"]); }

        set { this.ViewState["original_NodID"] = value; }
    }

    public ushort original_ZonID {
        get { return Convert.ToUInt16(this.ViewState["original_ZonID"]); }

        set { this.ViewState["original_ZonID"] = value; }
    }

    public ushort original_RegId {
        get { return Convert.ToUInt16(this.ViewState["original_RegId"]); }

        set { this.ViewState["original_RegId"] = value; }
    }

    public string stationCode {
        get { return (this.ViewState["stationCode"] ?? "").ToString(); }

        set { this.ViewState["stationCode"] = value; }
    }

    public string baseListenUrl {
        get { return (this.ViewState["baseListenUrl"] ?? "").ToString(); }

        set { this.ViewState["baseListenUrl"] = value; }
    }

    public long baseListenPort {
        get { return Convert.ToInt64(this.ViewState["baseListenPort"]); }

        set { this.ViewState["baseListenPort"] = value; }
    }

    public string baseCommandUrl {
        get { return (this.ViewState["baseCommandUrl"] ?? "").ToString(); }

        set { this.ViewState["baseCommandUrl"] = value; }
    }

    public long RegID {
        get { return Convert.ToInt64(this.ViewState["RegID"]); }

        set { this.ViewState["RegID"] = value; }
    }


    public bool AutoSize {
        get { return Convert.ToBoolean(this.ViewState["AutoSize"] ?? false); }

        set { this.ViewState["AutoSize"] = value; }
    }

    public bool IsRefreshing {
        get { return Convert.ToBoolean(this.ViewState["IsRefreshing"] ?? false); }

        set { this.ViewState["IsRefreshing"] = value; }
    }

    public string Vendor {
        get { return (this.ViewState["Vendor"] ?? "").ToString(); }

        set { this.ViewState["Vendor"] = value; }
    }

    public string Uri {
        get { return (this.ViewState["Uri"] ?? "").ToString(); }

        set { this.ViewState["Uri"] = value; }
    }

    public string Device {
        get { return (this.ViewState["Device"] ?? "").ToString(); }

        set { this.ViewState["Device"] = value; }
    }

    public string Building {
        get { return (this.ViewState["Building"] ?? "").ToString(); }

        set { this.ViewState["Building"] = value; }
    }

    public string Rack {
        get { return (this.ViewState["Rack"] ?? "").ToString(); }

        set { this.ViewState["Rack"] = value; }
    }

    public string RackDescription {
        get { return (this.ViewState["RackDescription"] ?? "").ToString(); }

        set { this.ViewState["RackDescription"] = value; }
    }

    public string IP {
        get { return (this.ViewState["IP"] ?? "").ToString(); }

        set { this.ViewState["IP"] = value; }
    }

    public string Type {
        get { return (this.ViewState["Type"] ?? "").ToString(); }

        set { this.ViewState["Type"] = value; }
    }

    public string NodeName {
        get { return (this.ViewState["NodeName"] ?? "").ToString(); }

        set { this.ViewState["NodeName"] = value; }
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e) {

#if (DEBUG)
        //this.manDeviceListen.Scripts.Add(new ScriptReference("~/StationListen/StationListen.debug.js"));
        this.manDeviceListen.Scripts.Add(new ScriptReference("~/StationListen/StationListen.js"));
#else
        this.manDeviceListen.Scripts.Add(new ScriptReference("~/StationListen/StationListen.js"));
#endif

        if (!this.IsPostBack) {
            long devId;
            if (this.Request.QueryString["dev"] != null && long.TryParse(this.Request.QueryString["dev"], out devId)) {
                this.DevID = devId;
                // ottengo gli altri dati relativi alla periferica in DB
                if (this.InitializeFieldsFromData(this.DevID)) {
                    
                    #region Recupero dati da base dati del device

                    this.stationCode                = Utility.GetNodeCode(this.NodID);

                    if (this.baseCommandUrl != "") hfUrlSonda.Value = string.Format("{0}&com=start&place=LO{1:D4}&device=0", this.baseCommandUrl, Convert.ToInt16(this.stationCode));
                    else hfUrlSonda.Value = "";
                    if (this.baseCommandUrl != "") hfUrlTromba.Value = string.Format("{0}&com=start&place=LO{1:D4}&device=1", this.baseCommandUrl, Convert.ToInt16(this.stationCode));
                    else hfUrlTromba.Value = "";
                    if (this.baseCommandUrl != "") hfUrlStop.Value = string.Format("{0}&com=stop&place=LO{1:D4}", this.baseCommandUrl, Convert.ToInt16(this.stationCode));
                    else hfUrlStop.Value = "";
                    hfAudioSource.Value             = string.Format("{0}",                                   this.baseListenUrl);

                    if (this.baseListenPort > 0)
                        hfAudioSourcePort.Value     = string.Format("{0}", this.baseListenPort);
                    else
                        hfAudioSourcePort.Value     = "";

                    lblNomeStazione.Text            = String.Format("{0}", this.NodeName);

                    this.Title                      = "Ascolto Remoto";
                    #endregion
                }
                else {
                    #region Errore recupero dati da base dati per il device - si nascondono tutte le informazioni e controlli

                    #endregion
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) 
    {
    }

    protected void manDeviceListen_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        this.HandleError(e.Exception);
    }

    private void HandleError(Exception e)
    {
        string message = "";
        string stackTrace = "";

        if (e != null)
        {
            if (e.InnerException != null)
            {
                message = e.InnerException.Message ?? string.Empty;
                stackTrace = e.InnerException.StackTrace ?? string.Empty;
            }
            else
            {
                message = e.Message ?? string.Empty;
                stackTrace = e.StackTrace ?? string.Empty;
            }
        }

        string errorMessage = string.Format("Error -> {0}, Stack Trace -> {1}", message, stackTrace);

#if (DEBUG)
        this.manDeviceListen.AsyncPostBackErrorMessage = errorMessage;
#else
        if (e != null)
        {
            // Persiste l'eccezione serializzata in base dati
            if (!Tracer.TraceMessage(this.Context, e, true))
            {
                // Logga l'eccezione via trace listener (su disco)
                System.Diagnostics.Trace.TraceError(string.Format("{0}, User -> {1}", errorMessage,
                                                                  GUtility.GetUserInfos(true)));
            }
        }

        this.manDeviceListen.AsyncPostBackErrorMessage = "Errore nella comunicazione con il server.";
#endif
    }

    #endregion

    #region Methods

    private bool InitializeFieldsFromData(long devId) {

        ListenDevicesTableAdapter               ta = new ListenDevicesTableAdapter();
        ListenDevicesDS.ListenDevicesDataTable  devices;

        try {
            devices = ta.GetData(DBSettings.GetAwgClientDeviceType() + "%", devId); 
        }
        catch (Exception exc) {
            Tracer.TraceMessage(this.Context, exc);
            return false;
        }

        if (devices.Count > 0) {

            ListenDevicesDS.ListenDevicesRow device = devices[0];
            
            this.Device                             = device.Name;
            this.NodID                              = device.NodID;
            //VECCHIA PROCEDURA
            //this.baseListenUrl = DBSettings.GetUrlListenFromAwg();
            //this.baseCommandUrl = DBSettings.GetUrlCommandToAwg();
            //this.baseListenPort = DBSettings.GetPortListenFromAwg();
            //NEW - con COMPARTIMENTAZIONE
            this.baseListenUrl = DBSettings.GetUrlListenFromAwgNew(this.NodID);
            this.baseCommandUrl = DBSettings.GetUrlCommandToAwgNew(this.NodID);
            this.baseListenPort = DBSettings.GetPortListenFromAwgNew(this.NodID);


            if (!device.IsNodeNameNull())
            {
                this.NodeName = device.NodeName;
            }

            return true;
        }

        return false;
    }

    #endregion
}
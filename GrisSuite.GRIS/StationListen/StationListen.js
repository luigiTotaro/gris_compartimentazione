﻿var UNKNOWN             = -1;
var IE                  = 0;
var OPERA               = 1;
var FIREFOX             = 2;
var SAFARI              = 3;
var CHROME              = 4;

var currentBrowser      = UNKNOWN;

var selectedElement     = 0;

document.onkeydown      = DisableCtrlN;
document.oncontextmenu = new Function("return false");

var test = false;

function GetBrowser() {

    if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1)      {
        return OPERA; 
    }
    else if (navigator.userAgent.indexOf("Chrome")  != -1)                                       {
        return CHROME; 
    }
    else if (navigator.userAgent.indexOf("Safari")  != -1)                                       {
        return SAFARI; 
    }
    else if (navigator.userAgent.indexOf("Firefox") != -1)                                       {
        return FIREFOX; 
    }
    else if ((navigator.userAgent.indexOf("MSIE")   != -1) || (!!document.documentMode == true)) {
        return IE; 
    }

    return UNKNOWN;
}

function DisplayMsg(sMsg) {

    if (document.getElementById('lblMsg')) {

        if (sMsg != undefined) {
            document.getElementById('lblMsg').innerHTML = sMsg;
        }
    }
}

function SelectionChange() {

    var x = document.getElementById("cmbSelectListen").selectedIndex;
    var y = document.getElementById("cmbSelectListen").options;

    switch (y[x].index) {
        case 0:
            document.getElementById('hfSelectedListen').value = 0;
            break;
        case 1:
            document.getElementById('hfSelectedListen').value = 1;
            break;
        default:
            break;
    }

    StopStreamAudio();
}

function StartStreamAudioSonda() {

    try {

        console.log('StartStreamAudioSonda');

        var xmlPlaySondaHttp = createCORSRequest('GET', document.getElementById('hfUrlSonda').value);

        xmlPlaySondaHttp.onreadystatechange = function () {

            if (xmlPlaySondaHttp.readyState == 4) {

                if (xmlPlaySondaHttp.status == 200) {

                    obj = JSON && JSON.parse(xmlPlaySondaHttp.responseText) || $.parseJSON(xmlPlaySondaHttp.responseText);

                    switch (obj.status) {
                        case 0:
                            DisplayMsg('Richiesta Ascolto da Sonda Accettata');
                            break;
                        case 1:
                            DisplayMsg('Errore: Stazione NON gestita !');
                            break;
                        case 2:
                            DisplayMsg('Errore: Ascolto da Sonda Non Disponibile !');
                            break;
                        default:
                            DisplayMsg('Errore: Il Server ha risposto in maniera inaspettata !');
                            break;
                    }

                    if (obj.status == 0) {

                        if ((document.getElementById('hfAudioSourcePort').value == '') && (obj.port != undefined)) {

                            AddAudioSondaTag('&port=' + obj.port, true);
                        }
                        else
                            AddAudioSondaTag('', true);
                    }
                    else
                        AddAudioSondaTag('', false);
                }
                else {
                    DisplayMsg              ('Errore: Richiesta Ascolto da Sonda Fallita !');
                    xmlPlaySondaHttp.abort  ();

                    AddAudioSondaTag        ('', false);
                }
            }
        }
        
        DisplayMsg('Invio Richiesta Ascolto da Sonda...');
    }
    catch (e) { }
}

function StartStreamAudioTromba() {

    try {

        console.log('StartStreamAudioTromba');

        var xmlPlayTrombaHttp = createCORSRequest('GET', document.getElementById('hfUrlTromba').value); 

        xmlPlayTrombaHttp.onreadystatechange = function () {

            if (xmlPlayTrombaHttp.readyState == 4) {

                if (xmlPlayTrombaHttp.status == 200) {

                    var obj = JSON && JSON.parse(xmlPlayTrombaHttp.responseText) || $.parseJSON(xmlPlayTrombaHttp.responseText);

                    switch (obj.status) {
                        case 0:
                            DisplayMsg('Richiesta Ascolto da Tromba Accettata');
                            break;
                        case 1:
                            DisplayMsg('Errore: Stazione NON gestita !');
                            break;
                        case 2:
                            DisplayMsg('Errore: Ascolto da Tromba Non Disponibile !');
                            break;
                        default:
                            DisplayMsg('Errore: Il Server ha risposto in maniera inaspettata !');
                            break;
                    }

                    if (obj.status == 0) {
                     
                        if ((document.getElementById('hfAudioSourcePort').value == '') && (obj.port != undefined)) {

                            AddAudioTrombaTag('&port=' + obj.port, true);
                        }
                        else
                            AddAudioTrombaTag('', true);
                    }
                    else
                        AddAudioTrombaTag('', false);
                }
                else {
                    DisplayMsg              ('Errore: Richiesta Ascolto da Tromba Fallita !');
                    xmlPlaySondaHttp.abort  ();

                    AddAudioTrombaTag       ('', false);
                }
            }
        }

        DisplayMsg('Invio Richiesta Ascolto da Tromba...');
    }
    catch (e) { }
}

function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        xhr.open(method, url + "&rand=" + Math.random(), true);
        xhr.send();

    } else if (typeof XDomainRequest != "undefined") {
        xhr = new XDomainRequest();
        xhr.open(method, url + "&rand=" + Math.random());
        xhr.send();
    } else {
        xhr = null;
    }

    return xhr;
}


function AddAudioSondaTag(portUrl, isStationOk) {

    try {

        if (document.getElementById('audioSondaTag') == null) {
            var tagAudio        = document.createElement    ('audio');
            var tagSegnaposto   = document.getElementById   ('divAudio');

            tagAudio.setAttribute       ('id',       'audioSondaTag');
            tagAudio.setAttribute       ('controls', 'controls');
            tagAudio.setAttribute       ('preload',  'none');
            tagAudio.setAttribute       ('src',       document.getElementById('hfAudioSource').value + portUrl);

            if(currentBrowser == IE)
                tagAudio.style.setAttribute('width', '370px');
            else
                tagAudio.setAttribute('width', '370px');

            if (isStationOk) {

                tagAudio.onloadstart = function () { DisplayMsg('Avvio Caricamento Audio'); }
                tagAudio.ondurationchange = function () { DisplayMsg('Rilevata Variazione Durata Audio'); }
                tagAudio.onloadedmetadata = function () { DisplayMsg('Meta dati Caricati'); }
                tagAudio.onloadeddata = function () { DisplayMsg('Frame Corrente Caricato'); }
                tagAudio.onprogress = function () { DisplayMsg('Download Audio Attivo'); }
                tagAudio.canplay = function () { DisplayMsg('Avvio Riproduzione Audio Abilitato'); }
                tagAudio.canplaythrough = function () { DisplayMsg('Buffering Non Necessario'); }
                tagAudio.onstalled = function () { DisplayMsg('Audio Non Disponibile'); }
                tagAudio.onwaiting = function () { DisplayMsg('Buffering In Corso...'); }
                tagAudio.onplay = function () { DisplayMsg('Riproduzione Avviata'); }
                tagAudio.onplaying = function () { DisplayMsg('Riproduzione Riavviata'); }
                tagAudio.onended = function () { DisplayMsg('Riproduzione Conclusa'); }
            }

            tagSegnaposto.appendChild(tagAudio);
        }
    }
    catch (e) { }
}

function AddAudioTrombaTag(portUrl, isStationOk) {

    try {

        if (document.getElementById('audioTrombaTag') == null) {
            var tagAudio        = document.createElement    ('audio');
            var tagSegnaposto   = document.getElementById   ('divAudio');

            tagAudio.setAttribute       ('id',       'audioTrombaTag');
            tagAudio.setAttribute       ('controls', 'controls');
            tagAudio.setAttribute       ('preload',  'none');
            tagAudio.setAttribute       ('src',       document.getElementById('hfAudioSource').value + portUrl);


            if (currentBrowser == IE)
                tagAudio.style.setAttribute('width', '370px');
            else
                tagAudio.setAttribute('width', '370px');

            if (isStationOk) {
            
                tagAudio.onloadstart = function () { DisplayMsg('Avvio Caricamento Audio'); }
                tagAudio.ondurationchange = function () { DisplayMsg('Rilevata Variazione Durata Audio'); }
                tagAudio.onloadedmetadata = function () { DisplayMsg('Meta dati Caricati'); }
                tagAudio.onloadeddata = function () { DisplayMsg('Frame Corrente Caricato'); }
                tagAudio.onprogress = function () { DisplayMsg('Download Audio Attivo'); }
                tagAudio.canplay = function () { DisplayMsg('Avvio Riproduzione Audio Abilitato'); }
                tagAudio.canplaythrough = function () { DisplayMsg('Buffering Non Necessario'); }
                tagAudio.onstalled = function () { DisplayMsg('Audio Non Disponibile'); }
                tagAudio.onwaiting = function () { DisplayMsg('Buffering In Corso...'); }
                tagAudio.onplay = function () { DisplayMsg('Riproduzione Avviata'); }
                tagAudio.onplaying = function () { DisplayMsg('Riproduzione Riavviata'); }
                tagAudio.onended = function () { DisplayMsg('Riproduzione Conclusa'); }
            }

            tagSegnaposto.appendChild(tagAudio);
        }
    }
    catch (e) { }
}

function RemoveAudioTag() {

    try {
        var tagAudioSonda   = document.getElementById('audioSondaTag');
        var tagAudioTromba  = document.getElementById('audioTrombaTag');
        var tagSegnaposto   = document.getElementById('divAudio');

        if (tagAudioSonda != null) {
            tagSegnaposto.removeChild(tagAudioSonda);
        }

        if (tagAudioTromba != null) {
            tagSegnaposto.removeChild(tagAudioTromba);
        }
    }
    catch (e) { }
}

function StopStreamAudio() {

    if ((document.getElementById('hfUrlSonda').value == "") || (document.getElementById('hfUrlSonda').value == "")) alert("Attenzione: Audio non disponibile, problemi di configurazione");

    if (test) {
        alert('AudioSonda\n' + "Url Start:" + document.getElementById('hfUrlSonda').value + "\n" + "Url Listen:" + document.getElementById('hfAudioSource').value);
        alert('AudioTromba\n' + "Url Start:" + document.getElementById('hfUrlTromba').value + "\n" + "Url Listen:" + document.getElementById('hfAudioSource').value);
    }


    try {

        var xmlStopHttp = createCORSRequest('GET', document.getElementById('hfUrlStop').value);

        xmlStopHttp.onreadystatechange = function () {

            if ((xmlStopHttp.readyState == 4) && (xmlStopHttp.status == 200)) {

                var obj = JSON && JSON.parse(xmlStopHttp.responseText) || $.parseJSON(xmlStopHttp.responseText);

                if (obj.status == 0) {

                    RemoveAudioTag();

                    switch (document.getElementById('hfSelectedListen').value) {
                        case '0':
                            StartStreamAudioSonda();
                            break;
                        case '1':
                            StartStreamAudioTromba();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
    catch (e) { console.log(e.message); }
}

function SelectElement(valueToSelect) {

    var element     = document.getElementById('cmbSelectListen');
    element.value   = valueToSelect;
}

function SetupWindow() {

    currentBrowser = GetBrowser ();

    DefaultWindowPositioning();
}

function DefaultWindowPositioning() {

    SelectElement           (document.getElementById('hfSelectedListen').value);
    CenterWindow            (document.body.offsetWidth, document.body.offsetHeight);

    window.resizeTo         (540, 290);

    document.getElementById ("cmbSelectListen").selectedIndex = 0;

    document.getElementById('hfSelectedListen').value = 0;

    StopStreamAudio();
}

function CenterWindow(windowWidth, windowHeight) {

    try {
        self.moveTo(Math.round((screen.availWidth - windowWidth) / 2), Math.round(((screen.availHeight - windowHeight) / 2)));
    }
    catch (e) { }
}

function DisableCtrlN() {
    try {
        if ((event.altKey) && (event.keyCode == 115/*f4*/)) {
            if (window.opener && !window.opener.closed) {
                window.opener.closeGris();
                window.close();
            }
        }
        else {
            return !((event.ctrlKey) && (event.keyCode == 78 || event.keyCode == 68))
        }
    }
    catch (e) { }
}

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
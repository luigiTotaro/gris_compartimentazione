<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StationListen.aspx.cs" Inherits="StationListen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GRIS</title>
    <link href="../CSS/Gris.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link rel="icon" href="../favicon.ico" />
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <style type="text/css">
        .style1
        {
            width: 100px;
        }
        .style2
        {
            width: 124px;
        }
    </style>
</head>
<body style="margin: 0px; padding: 0px; border: 0px; background-color: #4c4c4c; width: 100%;" onload="SetupWindow();" onunload="StopStreamAudio();" >
    <form id="frmSL" runat="server">

    <asp:HiddenField ID="hfUrlSonda"        runat = server />
    <asp:HiddenField ID="hfUrlTromba"       runat = server />
    <asp:HiddenField ID="hfUrlStop"         runat = server />
    <asp:HiddenField ID="hfSelectedListen"  runat = server />
    <asp:HiddenField ID="hfAudioSource"     runat = server />
    <asp:HiddenField ID="hfAudioSourcePort" runat = server />

    <act:ToolkitScriptManager ID="manDeviceListen" runat="server" AllowCustomErrorsRedirect="True"
        EnableScriptGlobalization="true" EnableScriptLocalization="true" AsyncPostBackTimeout="300"
        OnAsyncPostBackError="manDeviceListen_AsyncPostBackError" />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; text-align: center;
        background-color: #4c4c4c;">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="background-image: url(../IMG/interfaccia/nav_bg.gif); height: 30px;">
                            <div style="height: 30px; float: right;">
                                <table style="text-align: center; color: #ffffff; white-space: nowrap;">
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-image: url(../IMG/interfaccia/nav_shadow_bg.gif); height: 21px;
                            background-repeat: repeat-x; text-align: left;">
                            
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table cellpadding="2" cellspacing="2" style="width: 100%;">

                    <tr>
                        <td style="text-align: left;">
                            <asp:Label ID="Label1" runat=server>Stazione: </asp:Label>
                        </td>
                        <td style="text-align: left;">
                            <asp:Label ID="lblNomeStazione" runat=server></asp:Label>
                        </td>
                    </tr>
                    <tr>

                        <td style="text-align: center;" class="style2">
                            <div style="padding: 2px;">
                                <asp:Image runat="server" ID="imgSpeaker" ImageUrl="~/IMG/Interfaccia/speaker.png"  />
                            </div>
                        </td>
                        
                        <td style="vertical-align: bottom; text-align: left;" class="style1">
                            <table cellpadding="2" cellspacing="2" style="width: 100%;">
                                <tr>
                                    <td>
                                        <select name="cmbSelectListen" id="cmbSelectListen" onchange="SelectionChange();">
                                            <option value="0">Sonda Microfonica</option>
                                            <option value="1">Ascolto Tromba</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="divAudio">

                                        </div>
                                        <label style="color: white" id="lblMsg"></label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

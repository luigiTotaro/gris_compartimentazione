using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Threading;
using AjaxControlToolkit;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.DevicesDSTableAdapters;
using GrisSuite.Data.dsCentralEventsTableAdapters;

public partial class Device : NavigationPage
{
    private string _selectedDeviceType = "";
    private string _sysCompleteIP = "";
    private long _currentStrId = -1;

    private const string PANEL_MESSAGGI_HEADER_TEXT = "Messaggi";
    private const string PANEL_EVENTI_HEADER_TEXT = "Eventi";
    private const string PANEL_FDS_HEADER_TEXT = "Vista Armadio";

    private string lastBoundPosizione = string.Empty;
    private string lastBoundTipoScheda = string.Empty;
    private string lastFDSRowClass = "grdL";

    public override MultiView LayoutMultiView
    {
        get { return null; }
    }

    public override View GraphView
    {
        get { return null; }
    }

    public override View GridView
    {
        get { return null; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.tmrDevice.Interval = DBSettings.GetPageRefreshTime()*1000;

            this.RegID = this.GetID("RegID", "~/Italia.aspx");
            this.ZonID = this.GetID("ZonID", "~/Region.aspx");
            this.NodID = this.GetID("NodID", "~/Zone.aspx");
            this.DevID = this.GetID("DevID", "~/Node.aspx");

            Dictionary<int, bool> streamsCollapsedState = new Dictionary<int, bool>();
            this.ViewState["streamsCollapsedState"] = streamsCollapsedState;

            Dictionary<string, bool> streamFieldsValueCollapsedState = new Dictionary<string, bool>();
            this.ViewState["streamFieldsValueCollapsedState"] = streamFieldsValueCollapsedState;
        }

        this.SystemSelector.UsedInDevicePage = true;

        this.pnlMessages.HeaderText = PANEL_MESSAGGI_HEADER_TEXT;
        this.pnlEvents.HeaderText = PANEL_EVENTI_HEADER_TEXT;
        this.pnlFDS.HeaderText = PANEL_FDS_HEADER_TEXT;

        string deviceType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType,
                                                           "");

        this.silFDS.InitParameters = string.Format("DevID={0},HtmlControlHostId={1},AutoRefreshTimeoutSeconds={2},FDSType={3}", this.DevID, this.silFDS.ClientID, DBSettings.GetPageRefreshTime(), (int)this.MapFDSTypeToEnum(deviceType));

        this.RegisterClientTabScript();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SystemSelector.NodID = this.NodID;
        this.SystemSelector.DataBind();

        # region Verifica Permessi

        if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2))
        {
            this.GoToPage(PageDestination.Node);
        }

        if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3))
        {
            this.ImageCheckBoxUnKnown.Visible = this.ImageCheckBoxUnKnown.Pressed = false;
        }

        # endregion

        string deviceType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey,
                                                           this.GetDeviceType, "");

        if ((String.Equals(deviceType, "TT10210V3", StringComparison.OrdinalIgnoreCase)) ||
            (String.Equals(deviceType, "PEAVDOM", StringComparison.OrdinalIgnoreCase)) ||
            (String.Equals(deviceType, "TT10220", StringComparison.OrdinalIgnoreCase)) ||
            (deviceType.StartsWith("FD90000", StringComparison.OrdinalIgnoreCase)))
        {
            this.pnlEvents.Visible = true;
        }
        else
        {
            this.pnlEvents.Visible = false;
        }

        if (deviceType.StartsWith("FD90000", StringComparison.OrdinalIgnoreCase))
        {
            // Il tab con la visualizzazione Silverlight dell'FDS � presente solo per i tipi FDS
            this.pnlFDS.Visible = DBSettings.GetFDSVistaArmadioVisible();
        }
        else
        {
            this.pnlFDS.Visible = false;
        }

        if (deviceType == "SYSNETM384V4" || deviceType == "SYSNETM684V4")
        {
            var css = new HtmlLink();
            css.ID = "hlnkZeus";
            css.Href = "CSS/gauge_zeus.css";
            css.Attributes["rel"] = "stylesheet";
            css.Attributes["type"] = "text/css";
            css.Attributes["media"] = "all";
            this.Page.Header.Controls.Add(css);
        }
    }

    protected void tmrDevice_Tick(object sender, EventArgs e)
    {
        this.DataRefresh();
    }

    protected void navBar_OnRefreshPageButtonClick(object sender, ImageClickEventArgs e)
    {
        this.DataRefresh();
    }

    protected void lvwStreams_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            MessaggiDS.streamsRow streamRow =
                ((DataRowView) ((ListViewDataItem) e.Item).DataItem).Row as MessaggiDS.streamsRow;
            Image imgStatus = (Image) e.Item.FindControl("imgStatusStream");

            if (streamRow != null)
            {
                SetImageBySevLevel(streamRow.SevLevel, imgStatus);
                this._currentStrId = streamRow.StrID;
                ListView lvwMessaggi = (ListView) e.Item.FindControl("lvwMessaggi");
                if (lvwMessaggi != null)
                {
                    lvwMessaggi.DataBind();
                }

                #region Stream Collapsed State management

                Dictionary<int, bool> streamsCollapsedState = null;

                if (this.ViewState["streamsCollapsedState"] != null)
                {
                    streamsCollapsedState = (this.ViewState["streamsCollapsedState"] as Dictionary<int, bool>);
                }

                if (streamsCollapsedState != null)
                {
                    CollapsiblePanelExtender cpeStream = e.Item.FindControl("cpeStream") as CollapsiblePanelExtender;
                    if (cpeStream != null)
                    {
                        if (!this.IsPostBack)
                        {
                            cpeStream.Collapsed = ((streamRow.SevLevel == 0) || (streamRow.SevLevel == -255));
                        }
                        else
                        {
                            if (streamsCollapsedState.ContainsKey(streamRow.StrID))
                            {
                                cpeStream.Collapsed = streamsCollapsedState[streamRow.StrID];
                            }
                            else
                            {
                                cpeStream.Collapsed = ((streamRow.SevLevel == 0) || (streamRow.SevLevel == -255));
                            }
                        }

                        if (streamsCollapsedState.ContainsKey(streamRow.StrID))
                        {
                            streamsCollapsedState[streamRow.StrID] = cpeStream.Collapsed;
                        }
                        else
                        {
                            streamsCollapsedState.Add(streamRow.StrID, cpeStream.Collapsed);
                        }
                    }
                }

                #endregion
            }
            else
            {
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_0.gif";
            }
        }
    }

    protected void lvwMessaggi_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "ToggleDetails")
        {
            e.Item.FindControl("lblStreamFieldData").Visible = !(e.Item.FindControl("lblStreamFieldData").Visible);
            e.Item.FindControl("lblMessaggi").Visible = !e.Item.FindControl("lblMessaggi").Visible;

            #region Stream Field Values management

            Dictionary<string, bool> streamFieldsValueCollapsedState = null;

            if (this.ViewState["streamFieldsValueCollapsedState"] != null)
            {
                streamFieldsValueCollapsedState =
                    (this.ViewState["streamFieldsValueCollapsedState"] as Dictionary<string, bool>);
            }

            if (streamFieldsValueCollapsedState != null)
            {
                foreach (ListViewItem lvs in this.lvwStreams.Items)
                {
                    foreach (ListViewItem lvi in ((ListView) lvs.FindControl("lvwMessaggi")).Items)
                    {
                        if (lvi.FindControl("lblStreamFieldData") != null)
                        {
                            if (
                                streamFieldsValueCollapsedState.ContainsKey(
                                    ((LinkButton) lvi.FindControl("lblTMessaggi")).CommandArgument))
                            {
                                streamFieldsValueCollapsedState[
                                    ((LinkButton) lvi.FindControl("lblTMessaggi")).CommandArgument] =
                                    lvi.FindControl("lblStreamFieldData").Visible;
                            }
                            else
                            {
                                streamFieldsValueCollapsedState.Add(
                                    ((LinkButton) lvi.FindControl("lblTMessaggi")).CommandArgument,
                                    lvi.FindControl("lblStreamFieldData").Visible);
                            }
                        }
                    }
                }

                this.ViewState["streamFieldsValueCollapsedState"] = streamFieldsValueCollapsedState;
            }

            #endregion
        }
    }

    protected void lvwMessaggi_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            MessaggiDS.stream_fieldsRow messageRow =
                ((DataRowView) ((ListViewDataItem) e.Item).DataItem).Row as MessaggiDS.stream_fieldsRow;

            Image imgStatus = (Image) e.Item.FindControl("imgStatus");
            imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_0.gif";

            if (messageRow != null)
            {
                //setto l'immagine in base allo stato del messaggio
                SetImageBySevLevel(messageRow.SevLevel, imgStatus);
            }

            LinkButton lblTMessaggi = e.Item.FindControl("lblTMessaggi") as LinkButton;

            if (lblTMessaggi != null)
            {
                //verifica i permessi e lascia selezionabile gli elementi solo al livello 3
                if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3))
                {
                    lblTMessaggi.CommandName = "";
                    lblTMessaggi.Attributes["style"] = "cursor: default;";
                }
            }

            Label lblStreamValue = e.Item.FindControl("lblStreamValue") as Label;

            if (lblStreamValue != null)
            {
                //verifica i permessi e lascia selezionabile gli elementi solo al livello 3
                if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3))
                {
                    lblStreamValue.Visible = false;
                }
            }

            ImageButton imgToggleStreamFieldDetails = e.Item.FindControl("imgToggleStreamFieldDetails") as ImageButton;

            if (imgToggleStreamFieldDetails != null)
            {
                //verifica i permessi e lascia selezionabile gli elementi solo al livello 3
                if (!GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3))
                {
                    imgToggleStreamFieldDetails.Visible = false;
                }                
            }
        }
    }

    private static void SetImageBySevLevel(int sevLevel, Image imgStatus)
    {
        switch (sevLevel)
        {
            case 0:
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_0.gif";
                imgStatus.ToolTip = "OK";
                break;
            case 1:
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_1.gif";
                imgStatus.ToolTip = "Attenzione";
                break;
            case 2:
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_2.gif";
                imgStatus.ToolTip = "Errore";
                break;
            case 255:
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_255.gif";
                imgStatus.ToolTip = "Sconosciuto";
                break;
            case -255:
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_-255.gif";
                imgStatus.ToolTip = "Informazione";
                break;
            default:
                imgStatus.ImageUrl = "~/IMG/interfaccia/tab_stato_255.gif";
                imgStatus.ToolTip = "Sconosciuto";
                break;
        }
    }

    protected void STLCInfo1_OnSyncComplete(object sender, EventArgs e)
    {
        //soluzione temporanea al refresh delle periferiche dopo una sincronizzazione forzata 
        Thread.Sleep(DBSettings.GetDevicesRefreshSleepTime());

        this._sysCompleteIP = ((STLCInfo) sender).IP;

        this.dlstSTLCInfo.DataBind();
    }

    protected void STLCInfo1_OnServerDelete(object sender, EventArgs e)
    {
        this.DataRefresh();
    }

    protected void SqlDataSTLCInfo_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if ((e != null) && (e.AffectedRows <= 0))
        {
            this.GoToPage(PageDestination.Zone);
        }
    }

    protected void dlstDevPanel_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DevicesDS.devicesRow row;

        if (((row = ((DataRowView) e.Item.DataItem).Row as DevicesDS.devicesRow) != null))
        {
            if (!this.IsPostBack &&
                !Controllers.Device.CanUserViewDeviceDetails(this.RegID, row.SevLevel, row.SevLevelDetail, row.Type))
            {
                this.GoToPage(PageDestination.Node);
            }

            // scrittura etichette sulla barra di navigazione
            this.SetNavigationLabels(row);

            this.SystemSelector.SelectedSystemId = row.SystemID;
            this.SystemSelector.ChangeSelectedSystemVisualApparence();
        }
    }

    protected void dlstSTLCInfo_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        STLCInfo stlcInfo = (STLCInfo) e.Item.FindControl("STLCInfo1");

        if (stlcInfo.IP == this._sysCompleteIP)
        {
            stlcInfo.SetSyncLayoutComplete();
        }
    }

    protected void OnChangeMaintenance(object sender, EventArgs e)
    {
        this.DataRefresh();
    }

    protected void odsDevices_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["DevID"] = this.DevID;
        // A livello di periferica si vede sempre e solo l'ultimo stato conosciuto (issue 11427)
        e.InputParameters["LastStatusServers"] = string.Empty;
    }

    protected void odsMessaggi_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (this._currentStrId != -1)
        {
            e.InputParameters["DevID"] = this.DevID;
            e.InputParameters["StrID"] = this._currentStrId;
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void odsStreams_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["DevID"] = this.DevID;
    }

    protected void SqlDataSTLCInfo_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@NodID"].Value = this.NodID;
        e.Command.Parameters["@DevID"].Value = this.DevID;
    }

    protected void odsGaugeInfos_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        string deviceType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType,
                                                           "");

        if (!String.Equals(deviceType, "TT10210", StringComparison.OrdinalIgnoreCase) &&
            !String.Equals(deviceType, "TT10210V3", StringComparison.OrdinalIgnoreCase)
            && !String.Equals(deviceType, "DT04000", StringComparison.OrdinalIgnoreCase))
        {
            e.Cancel = true;
            return;
        }

        e.InputParameters["DevID"] = this.DevID;
        e.InputParameters["DeviceType"] = deviceType;
    }

    protected void odsGaugePZiInfos_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        string deviceType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType,
                                                           "");

        if (!String.Equals(deviceType, "TT10220", StringComparison.OrdinalIgnoreCase))
        {
            e.Cancel = true;
            return;
        }

        e.InputParameters["DevID"] = this.DevID;
    }

    protected void odsGaugeZeusInfos_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        string deviceType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType,
                                                           "");

        if (!String.Equals(deviceType, "SYSNETM384V4", StringComparison.OrdinalIgnoreCase) &&
            !String.Equals(deviceType, "SYSNETM684V4", StringComparison.OrdinalIgnoreCase))
        {
            e.Cancel = true;
            return;
        }

        e.InputParameters["DevID"] = this.DevID;
        e.InputParameters["DeviceType"] = deviceType;
    }

    protected void dlstGaugePzi_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item != null)
        {
            var gauge = e.Item.FindControl("GaugePZi") as Controls_GaugePanelPZi;

            if (gauge != null)
            {
                gauge.SuperiorLimitPerc = "100 %";
                gauge.InferiorLimitPerc = "100 %";
                gauge.DataBind();
            }
        }
    }

    protected void dlstGaugeZeus_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        string deviceType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType,
                                                           "");

        if (e.Item != null)
        {
            var gauge = e.Item.FindControl("GaugeZeus") as Controls_GaugePanelPZi;

            if (gauge != null)
            {
                gauge.DeviceType = deviceType;
                gauge.DataBind();
            }
        }
    }

    protected void odsEventiMaster_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        DateTime textCreatedOn;
        if (!DateTime.TryParse(this.txtCreatedOn.Text, out textCreatedOn))
        {
            // Se la data non � valida, reimpostiamo il default
            textCreatedOn = DateTime.Now;
            this.txtCreatedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        string[] fromRange;
        string[] toRange;

        if (this.cboCreatedHoursRange.SelectedValue.Length > 0)
        {
            fromRange = (this.cboCreatedHoursRange.SelectedValue.Split('-')[0]).Split('.');
            toRange = (this.cboCreatedHoursRange.SelectedValue.Split('-')[1]).Split('.');
        }
        else
        {
            fromRange = new[] {"00", "00", "00"};
            toRange = new[] {"23", "59", "59"};
        }

        DateTime createdFrom = new DateTime(textCreatedOn.Year, textCreatedOn.Month, textCreatedOn.Day,
                                            Int32.Parse(fromRange[0]), Int32.Parse(fromRange[1]),
                                            Int32.Parse(fromRange[2]));
        DateTime createdTo = new DateTime(textCreatedOn.Year, textCreatedOn.Month, textCreatedOn.Day,
                                          Int32.Parse(toRange[0]), Int32.Parse(toRange[1]), Int32.Parse(toRange[2]));

        e.InputParameters["DevID"] = this.DevID;
        e.InputParameters["CreatedFrom"] = createdFrom;
        e.InputParameters["CreatedTo"] = createdTo;
        e.InputParameters["DeviceTypeID"] = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey,
                                                                           this.GetDeviceType, "");
        this.updEventi.Update();
    }

    protected void odsEventiDetail_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["DeviceTypeID"] = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey,
                                                                           this.GetDeviceType, "");
    }

    protected void lvwEventiMaster_DataBound(object sender, EventArgs e)
    {
        this.lblEventsCountTop.Text =
            this.lblEventsCountBottom.Text = string.Format("Totale Eventi: {0}", this.lvwEventiMaster.Items.Count);
        this.lblEventsCountBottom.Visible = (this.lvwEventiMaster.Items.Count > 0);
    }

    protected void lvwEventiMaster_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if ((this.lvwEventiMaster.Visible) && (e.Item.ItemType == ListViewItemType.DataItem))
        {
            dsCentralEvents.DecodedEventDataRow eventRow =
                ((DataRowView) ((ListViewDataItem) e.Item).DataItem).Row as dsCentralEvents.DecodedEventDataRow;

            Image imgStatus = (Image) e.Item.FindControl("imgStatus");
            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";

            LinkButton lbtEventDescription = e.Item.FindControl("lbtEventDescription") as LinkButton;
            Label lblEventDescription = e.Item.FindControl("lblEventDescription") as Label;

            SetControlText(lbtEventDescription, String.Empty);
            SetControlText(lblEventDescription, String.Empty);

            if (eventRow != null)
            {
                if (eventRow.EventCategory == 2)
                {
                    // Eventi PZv3
                    switch (eventRow.EventCode)
                    {
                        case 33: // "attivata uscita"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_33.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 34: // "disattivata uscita"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_34.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 35: // "attivato ingresso"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_35.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 36: // "disattivato ingresso"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_36.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 43: // "attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_43.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 44: // "fine attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_44.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 45: // "esito test"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_45.gif";
                            SetControlText(lbtEventDescription, eventRow.EventDescription);
                            SetControlText(lblEventDescription, eventRow.EventDescription);
                            break;
                        default:
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                            SetControlText(lbtEventDescription, eventRow.EventDescription);
                            SetControlText(lblEventDescription, eventRow.EventDescription);
                            break;
                    }
                }
                else if (eventRow.EventCategory == 4)
                {
                    // Eventi PZi
                    switch (eventRow.EventCode)
                    {
                        case 150: // "attivazione sonora"
                            if (!eventRow.IsFunctionDescriptionNull() && eventRow.FunctionDescription == 1)
                            {
                                // Il raggruppamento "attivazione sonora" pu� contenere, per il PZi, gli eventi di esito test su attivazione sonora.
                                // Il campo FunctionDescription intero � usato per segnalare la cosa (era un campo inutilizzato per il PZi)

                                imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_150_group_with_test.gif";
                            }
                            else
                            {
                                imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_150_group.gif";
                            }

                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 151: // "fine attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_151.gif";
                            SetControlText(lbtEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            SetControlText(lblEventDescription,
                                           eventRow.EventDescription + " (" +
                                           (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) + ")");
                            break;
                        case 152: // "esito test periodico"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_152.gif";
                            SetControlText(lbtEventDescription, eventRow.EventDescription);
                            SetControlText(lblEventDescription, eventRow.EventDescription);
                            break;
                        default:
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                            SetControlText(lbtEventDescription, eventRow.EventDescription);
                            SetControlText(lblEventDescription, eventRow.EventDescription);
                            break;
                    }
                }
                else if (eventRow.EventCategory == 100)
                {
                    imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                    SetControlText(lbtEventDescription, eventRow.EventDescription);
                    SetControlText(lblEventDescription, eventRow.EventDescription);
                }
            }
        }
    }

    protected void lvwEventiDetail_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            dsCentralEvents.DecodedEventDataByCreationTimeRow eventRow =
                ((DataRowView) ((ListViewDataItem) e.Item).DataItem).Row as
                dsCentralEvents.DecodedEventDataByCreationTimeRow;

            Image imgStatus = (Image) e.Item.FindControl("imgStatus");
            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
            Label lblEventDescription = (Label) e.Item.FindControl("lblEventDescription");
            lblEventDescription.Text = String.Empty;

            if (eventRow != null)
            {
                if (eventRow.EventCategory == 2)
                {
                    // eventi PZv3
                    switch (eventRow.EventCode)
                    {
                        case 33: // "attivata uscita"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_33.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 34: // "disattivata uscita"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_34.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 35: // "attivato ingresso"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_35.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 36: // "disattivato ingresso"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_36.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 43: // "attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_43.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       "; Mic: " +
                                                       (eventRow.IsMicroIDNull() ? "-" : eventRow.MicroID.ToString()) +
                                                       "; " + eventRow.SubEventDescription + ")";
                            break;
                        case 44: // "fine attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_44.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 45: // "esito test"
                            if (eventRow.IsEventDescriptionValueNull())
                            {
                                // caso in cui gli esiti del test sono concatenati sulla stessa riga
                                imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                                lblEventDescription.Text = eventRow.EventDescription;
                            }
                            else
                            {
                                // ogni esito del test produce varie righe, una per ogni singolo risultato
                                if (eventRow.EventDescriptionValue.Equals("1", StringComparison.OrdinalIgnoreCase))
                                {
                                    imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_45_1.gif";
                                }
                                else if (eventRow.EventDescriptionValue.Equals("2", StringComparison.OrdinalIgnoreCase))
                                {
                                    imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_45_2.gif";
                                }
                                lblEventDescription.Text = eventRow.EventDescription;
                            }
                            break;
                        default:
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                            lblEventDescription.Text = eventRow.EventDescription;
                            break;
                    }
                }
                else if (eventRow.EventCategory == 4)
                {
                    // eventi PZi
                    switch (eventRow.EventCode)
                    {
                        case 104: // "attivata uscita"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_104.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 105: // "disattivata uscita"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_105.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 102: // "attivato ingresso"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_102.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 103: // "disattivato ingresso"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_103.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 100: // "attivato link"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_100.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 101: // "disattivato link"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_101.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 150: // "attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_150.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       "; " + eventRow.SubEventDescription + ")";
                            break;
                        case 151: // "fine attivazione sonora"
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_151.gif";
                            lblEventDescription.Text = eventRow.EventDescription + " (" +
                                                       (eventRow.IsInOutIDNull() ? "-" : eventRow.InOutID.ToString()) +
                                                       ")";
                            break;
                        case 152: // "esito test periodico"
                            if (eventRow.IsEventDescriptionValueNull())
                            {
                                // caso in cui gli esiti del test sono concatenati sulla stessa riga
                                imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                                lblEventDescription.Text = eventRow.EventDescription;
                            }
                            else
                            {
                                // ogni esito del test produce varie righe, una per ogni singolo risultato
                                imgStatus.ImageUrl = string.Format("~/IMG/interfaccia/event_code_152_{0}.gif",
                                                                   eventRow.EventDescriptionValue);
                                lblEventDescription.Text = eventRow.EventDescription;
                            }
                            break;
                        case 153: // "esito test su attivazione"
                            if (eventRow.IsEventDescriptionValueNull())
                            {
                                // caso in cui gli esiti del test sono concatenati sulla stessa riga
                                imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                                lblEventDescription.Text = eventRow.EventDescription;
                            }
                            else
                            {
                                // ogni esito del test produce varie righe, una per ogni singolo risultato
                                imgStatus.ImageUrl = string.Format("~/IMG/interfaccia/event_code_153_{0}.gif",
                                                                   eventRow.EventDescriptionValue);
                                lblEventDescription.Text = eventRow.EventDescription;
                            }
                            break;
                        default:
                            imgStatus.ImageUrl = "~/IMG/interfaccia/event_code_0.gif";
                            lblEventDescription.Text = eventRow.EventDescription;
                            break;
                    }
                }
            }
        }
    }

    private static void SetControlText(Control ctrl, string text)
    {
        if (ctrl != null)
        {
            if (ctrl is LinkButton)
            {
                ((LinkButton) ctrl).Text = text;
            }
            else if (ctrl is Label)
            {
                ((Label) ctrl).Text = text;
            }
        }
    }

    protected void EventsCodeFilter_Click(object sender, EventArgs e)
    {
        this.RebindEventi();
    }

    private void RebindEventi()
    {
        string devType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType, "");

        if ((String.Equals(devType, "TT10210V3", StringComparison.OrdinalIgnoreCase)) ||
            (String.Equals(devType, "TT10220", StringComparison.OrdinalIgnoreCase)))
        {
            this.lvwEventiMaster.SelectedIndex = -1;
            this.lvwEventiMaster.DataBind();
        }
        else if (String.Equals(devType, "PEAVDOM", StringComparison.OrdinalIgnoreCase))
        {
            this.lvwEventiMaster.SelectedIndex = -1;
            this.lvwEventiMaster.DataBind();
        }
        else if (devType.StartsWith("FD90000", StringComparison.OrdinalIgnoreCase))
        {
            this.lvwEventiFDS.SelectedIndex = -1;
            this.lvwEventiFDS.DataBind();
        }

        this.updEventi.Update();
    }

    protected void txtCreatedOn_TextChanged(object sender, EventArgs e)
    {
        this.RebindEventi();
    }

    protected void cboCreatedHoursRange_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.RebindEventi();
    }

    private void SetNavigationLabels(DevicesDS.devicesRow row)
    {
        if (row != null)
        {
            this.navBar.LinkCompartimentoText = row.Region.Replace("Compartimento di ", "");
            this.navBar.LinkCompartimentoTooltip = row.Region;
            this.navBar.LinkLineaText = row.Zone.Replace("Linea ", "");
            this.navBar.LinkLineaTooltip = row.Zone;
            this.navBar.SetCompartimentoSeverita(row.SevLevelRegion, (row.IsRegionColorNull() ? null : row.RegionColor));
            this.navBar.SetLineaSeverita(row.SevLevelZone, (row.IsZoneColorNull() ? null : row.ZoneColor));
            this.navBar.SetStazioneSeverita(row.SevLevelNode, (row.IsNodeColorNull() ? null : row.NodeColor));

            this.navBar.ClearInformationBarData();

            if (row.LastSevLevelReturned)
            {
                this.navBar.SetInformationBarData(InformationBarIcon.Exclamation,
                                                  ConfigurationManager.AppSettings["LastDeviceStatusReturnedMessage"]);
                this.navBar.UpdateInformationBarData();
            }
        }
    }

    private string GetDeviceType()
    {
        if (this._selectedDeviceType.Length == 0)
        {
            var ta = new devicesTableAdapter();
            this._selectedDeviceType = ta.GetTypeByID(this.DevID) ?? "";
        }

        return this._selectedDeviceType;
    }

    private void DataRefresh()
    {
        this.lvwStreams.DataBind();

        if (this.lvwEventiMaster.Visible)
        {
            this.lvwEventiMaster.DataBind();
        }

        if (this.lvwEventiFDS.Visible)
        {
            this.lvwEventiFDS.DataBind();
        }

        this.dlstDevPanel.DataBind();
        this.dlstGauge.DataBind();
        this.dlstGaugePzi.DataBind();
        this.dlstGaugeZeus.DataBind();
        this.dlstSTLCInfo.DataBind();

        this.updMessaggi.Update();
        this.updEventi.Update();
        this.updDevPanel.Update();
        this.updGauge.Update();
        this.updSTLCInfo.Update();
        this.updSystems.Update();
        this.updNavBar.Update();
    }

    protected string FormatStreamFieldData(string nomeStream, string messaggi)
    {
        StringBuilder result = new StringBuilder();
        if (messaggi.Length > 0)
        {
            string[] temp1 = messaggi.Split(';');
            foreach (string s in temp1)
            {
                if (s.Length > 0)
                {
                    string[] temp2 = s.Split('=');
                    if (temp2.Length > 1)
                    {
                        result.Append("<div style=\"width:100%\">");

                        if (temp2[1].Equals("0"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_0_small.gif") +
                                "\" Title=\"OK\" /></div>");
                        }
                        else if (temp2[1].Equals("1"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_1_small.gif") +
                                "\" Title=\"Attenzione\" /></div>");
                        }
                        else if (temp2[1].Equals("2"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_2_small.gif") +
                                "\" Title=\"Errore\" /></div>");
                        }
                        else if (temp2[1].Equals("255"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_3_small.gif") +
                                "\" Title=\"Sconosciuto\" /></div>");
                        }
                        else if (temp2[1].Equals("-255"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_-255_small.gif") +
                                "\" Title=\"Informazione\" /></div>");
                        }

                        result.Append("<div style=\"padding-top:4px;float:left;text-align:bottom\">" + temp2[0] +
                                      "</div>");

                        result.Append("</div>");
                    }
                }
            }
        }
        return result.ToString();
    }

    private void SetEventsDateFiltersDefault()
    {
        LastDecodedEventDatesTableAdapter ta = new LastDecodedEventDatesTableAdapter();
        var lastEventDates = ta.GetData(this.DevID, this.EventCodeFilter43.Active, this.EventCodeFilter44.Active,
                                        this.EventCodeFilter45.Active, this.GetDeviceType());

        if (lastEventDates != null && lastEventDates.Count > 0 && !lastEventDates[0].IsLastCreatedNull())
        {
            DateTime lastEvent = lastEventDates[0].LastCreated; // mi aspetto che siano gi� in ordine decrescente

            this.txtCreatedOn.Text = lastEvent.ToString("dd/MM/yyyy");
            this.cboCreatedHoursRange.SelectedValue = string.Format("{0}.00.00-{0}.59.59", lastEvent.Hour.ToString("00"));
        }
        else
        {
            this.txtCreatedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
            this.cboCreatedHoursRange.SelectedValue = string.Format("{0}.00.00-{0}.59.59",
                                                                    DateTime.Now.Hour.ToString("00"));
        }
    }

    public string FormatMessaggiNonOK(string nomeStream, string messaggi)
    {
        StringBuilder result = new StringBuilder();
        bool nonOKMessageFound = false;

        if (messaggi.Length > 0)
        {
            string[] temp1 = messaggi.Split(';');
            foreach (string s in temp1)
            {
                if (s.Length > 0)
                {
                    string[] temp2 = s.Split('=');
                    if (temp2.Length > 1)
                    {
                        result.Append("<div style=\"width:100%\">");

                        if (temp2[1].Equals("0"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_0_small.gif") +
                                "\" Title=\"OK\" /></div>");
                        }
                        else if (temp2[1].Equals("1"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_1_small.gif") +
                                "\" Title=\"Attenzione\" /></div>");
                            nonOKMessageFound = true;
                        }
                        else if (temp2[1].Equals("2"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_2_small.gif") +
                                "\" Title=\"Errore\" /></div>");
                            nonOKMessageFound = true;
                        }
                        else if (temp2[1].Equals("255"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_3_small.gif") +
                                "\" Title=\"Sconosciuto\" /></div>");
                            nonOKMessageFound = true;
                        }
                        else if (temp2[1].Equals("-255"))
                        {
                            result.Append(
                                "<div style=\"width:30px;height:22px;float:left;vertical-align:top\"><img src=\"" +
                                this.ResolveClientUrl("~/IMG/interfaccia/tab_stato_-255_small.gif") +
                                "\" Title=\"Informazione\" /></div>");
                        }

                        result.Append("<div style=\"padding-top:4px;float:left;text-align:bottom\">" + temp2[0] +
                                      "</div>");

                        result.Append("</div>");
                    }
                }
            }
        }

        if (nonOKMessageFound)
        {
            return result.ToString();
        }

        return String.Empty;
    }

    protected void lblTitle_Command(object sender, CommandEventArgs e)
    {
        int srvID = int.Parse(e.CommandArgument.ToString());

        this.PersistStreamsCollapsedState(srvID);
    }

    protected void imgToggleStreamDetails_Command(object sender, CommandEventArgs e)
    {
        int srvID = int.Parse(e.CommandArgument.ToString());

        this.PersistStreamsCollapsedState(srvID);
    }

    protected void lblCreationDateStream_Command(object sender, CommandEventArgs e)
    {
        int srvID = int.Parse(e.CommandArgument.ToString());

        this.PersistStreamsCollapsedState(srvID);
    }

    private void PersistStreamsCollapsedState(int srvID)
    {
        Dictionary<int, bool> streamsCollapsedState = null;

        if (this.ViewState["streamsCollapsedState"] != null)
        {
            streamsCollapsedState = (this.ViewState["streamsCollapsedState"] as Dictionary<int, bool>);
        }

        if (streamsCollapsedState != null)
        {
            foreach (ListViewItem lvi in this.lvwStreams.Items)
            {
                CollapsiblePanelExtender cpeStream = lvi.FindControl("cpeStream") as CollapsiblePanelExtender;
                LinkButton lblTitle = lvi.FindControl("lblTitle") as LinkButton;

                if ((cpeStream != null) && (lblTitle != null))
                {
                    if (srvID == int.Parse(lblTitle.CommandArgument))
                    {
                        cpeStream.Collapsed = !cpeStream.Collapsed;
                    }

                    if (streamsCollapsedState.ContainsKey(int.Parse(lblTitle.CommandArgument)))
                    {
                        streamsCollapsedState[int.Parse(lblTitle.CommandArgument)] = cpeStream.Collapsed;
                    }
                    else
                    {
                        streamsCollapsedState.Add(int.Parse(lblTitle.CommandArgument), cpeStream.Collapsed);
                    }
                }
            }

            this.ViewState["streamsCollapsedState"] = streamsCollapsedState;
        }
    }

    protected void lvwMessaggi_PreRender(object sender, EventArgs e)
    {
        Dictionary<string, bool> streamFieldsValueCollapsedState = null;

        if (this.ViewState["streamFieldsValueCollapsedState"] != null)
        {
            streamFieldsValueCollapsedState =
                (this.ViewState["streamFieldsValueCollapsedState"] as Dictionary<string, bool>);
        }

        if (streamFieldsValueCollapsedState != null)
        {
            foreach (ListViewItem lvs in this.lvwStreams.Items)
            {
                foreach (ListViewItem lvi in ((ListView) lvs.FindControl("lvwMessaggi")).Items)
                {
                    LinkButton lblTMessaggi = lvi.FindControl("lblTMessaggi") as LinkButton;
                    Label lblMessaggi = lvi.FindControl("lblMessaggi") as Label;
                    Label lblStreamFieldData = lvi.FindControl("lblStreamFieldData") as Label;
                    ImageButton imgToggleStreamFieldDetails =
                        lvi.FindControl("imgToggleStreamFieldDetails") as ImageButton;

                    if ((lblMessaggi != null) && (lblTMessaggi != null) && (lblStreamFieldData != null) &&
                        (imgToggleStreamFieldDetails != null))
                    {
                        if (!this.IsPostBack)
                        {
                            lblStreamFieldData.Visible = false;
                            lblMessaggi.Visible = !lblStreamFieldData.Visible;
                        }
                        else
                        {
                            if (streamFieldsValueCollapsedState.ContainsKey(lblTMessaggi.CommandArgument))
                            {
                                lblStreamFieldData.Visible =
                                    streamFieldsValueCollapsedState[lblTMessaggi.CommandArgument];
                                lblMessaggi.Visible = !lblStreamFieldData.Visible;
                            }
                            else
                            {
                                lblStreamFieldData.Visible = false;
                                lblMessaggi.Visible = !lblStreamFieldData.Visible;
                            }
                        }

                        if (lblMessaggi.Text.Length > 0)
                        {
                            // La lblMessaggi si occupa di presentare i dati dello stream field, se contiene messaggi con severity warning o error
                            // In quel caso, ha un testo associato (prodotto dalla FormatMessaggiNonOK) e rimane sempre espansa. Non c'� modo di collassarla.
                            // Per questo il bottone � solo il contrai, che, di fatto, non funziona.
                            // La seconda label lblStreamFieldData non sar� mai renderizzata
                            imgToggleStreamFieldDetails.ImageUrl = "IMG/interfaccia/contrai.gif";
                        }
                        else
                        {
                            // Il bottone di espansione / collassamento dati stream field dipende dalla visualizzazione o meno della lblStreamFieldData
                            imgToggleStreamFieldDetails.ImageUrl = lblStreamFieldData.Visible
                                                                       ? "IMG/interfaccia/contrai.gif"
                                                                       : "IMG/interfaccia/espandi.gif";
                        }

                        if (streamFieldsValueCollapsedState.ContainsKey(lblTMessaggi.CommandArgument))
                        {
                            streamFieldsValueCollapsedState[lblTMessaggi.CommandArgument] = lblStreamFieldData.Visible;
                        }
                        else
                        {
                            streamFieldsValueCollapsedState.Add(lblTMessaggi.CommandArgument, lblStreamFieldData.Visible);
                        }
                    }
                }
            }

            this.ViewState["streamFieldsValueCollapsedState"] = streamFieldsValueCollapsedState;
        }
    }

    protected void RegisterClientTabScript()
    {
        StringBuilder script = new StringBuilder();

        // per sapere se un tab � gi� caricato, verifichiamo la presenza nel DOM di un controllo al suo interno
        script.Append("<script type=\"text/javascript\">function clientActiveTabChanged(sender,args){");
        script.Append("var isTab0Loaded=$get('" + this.tblFDS.ClientID + "');");
        script.Append("var isTab1Loaded=$get('" + this.tlbMessages.ClientID + "');");
        script.Append("var isTab2Loaded=$get('" + this.tblEvents.ClientID + "');");
        script.Append("if(!isTab0Loaded&&(sender.get_activeTab().get_headerText().indexOf('" + PANEL_FDS_HEADER_TEXT +
                      "')>=0)){__doPostBack('" + this.btnFDSTrigger.UniqueID + "','');}");
        script.Append("else if(!isTab1Loaded&&(sender.get_activeTab().get_headerText().indexOf('" +
                      PANEL_MESSAGGI_HEADER_TEXT +
                      "')>=0)){__doPostBack('" + this.btnMessagesTrigger.UniqueID + "','');}");
        script.Append("else if(!isTab2Loaded&&(sender.get_activeTab().get_headerText().indexOf('" +
                      PANEL_EVENTI_HEADER_TEXT +
                      "')>=0)){__doPostBack('" + this.btnEventsTrigger.UniqueID + "','');}}");
        script.Append("</script>");

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientActiveTabChanged", script.ToString(), false);
    }

    protected void btnMessagesTrigger_Click(object sender, EventArgs args)
    {
        this.tlbMessages.Visible = true;
    }

    protected void btnEventsTrigger_Click(object sender, EventArgs args)
    {
        this.tblEvents.Visible = true;

        // imposta i filtri per data degli eventi ad un valore di default, tale valore � l'intervallo in cui si possono incontrare gli eventi pi� recenti
        string devType = GrisSessionManager.Session.Get(GrisSessionManager.SelectedDeviceTypeKey, this.GetDeviceType, "");

        this.lvwEventiMaster.Visible = false;
        this.lvwEventiFDS.Visible = false;

        if ((String.Equals(devType, "TT10210V3", StringComparison.OrdinalIgnoreCase)) ||
            (String.Equals(devType, "TT10220", StringComparison.OrdinalIgnoreCase)))
        {
            this.EventCodeFilter45.Visible = true;
            this.EventCodeFilter44.Visible = true;
            this.EventCodeFilter43.Visible = true;

            this.SetEventsDateFiltersDefault();
            this.lvwEventiMaster.Visible = true;
        }
        else if (String.Equals(devType, "PEAVDOM", StringComparison.OrdinalIgnoreCase))
        {
            this.EventCodeFilter45.Visible = false;
            this.EventCodeFilter44.Visible = false;
            this.EventCodeFilter43.Visible = false;

            this.SetEventsDateFiltersDefault();
            this.lvwEventiMaster.Visible = true;
        }
        else if (devType.StartsWith("FD90000", StringComparison.OrdinalIgnoreCase))
        {
            this.EventCodeFilter45.Visible = false;
            this.EventCodeFilter44.Visible = false;
            this.EventCodeFilter43.Visible = false;

            this.SetEventsDateFiltersDefault();
            this.lvwEventiFDS.Visible = true;
        }
    }

    protected void btnFDSTrigger_Click(object sender, EventArgs args)
    {
        this.tblFDS.Visible = true;
    }

    protected void SystemSelector_SelectorSystemClicked(object sender, SelectedSystemEventArgs e)
    {
        this.GoToPage(PageDestination.Node);
    }

    protected void lvwEventiFDS_DataBound(object sender, EventArgs e)
    {
        this.lblEventsCountTop.Text =
            this.lblEventsCountBottom.Text = string.Format("Totale Eventi: {0}", this.lvwEventiFDS.Items.Count);
        this.lblEventsCountBottom.Visible = (this.lvwEventiFDS.Items.Count > 0);
    }

    protected void lvwEventiFDS_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if ((this.lvwEventiFDS.Visible) && (e.Item.ItemType == ListViewItemType.DataItem))
        {
            dsCentralEvents.DecodedEventDataRow eventRow =
                ((DataRowView) ((ListViewDataItem) e.Item).DataItem).Row as dsCentralEvents.DecodedEventDataRow;

            if (eventRow != null)
            {
                if ((this.lastBoundPosizione.Equals(eventRow.EventDescriptionValue, StringComparison.OrdinalIgnoreCase)) &&
                    (this.lastBoundTipoScheda.Equals(eventRow.SubEventDescription, StringComparison.OrdinalIgnoreCase)))
                {
                    ((Label) e.Item.FindControl("lblPosizione")).Text = string.Empty;
                    ((Label) e.Item.FindControl("lblTipoScheda")).Text = string.Empty;
                }
                else
                {
                    ((Label) e.Item.FindControl("lblPosizione")).Text = eventRow.EventDescriptionValue;
                    ((Label) e.Item.FindControl("lblTipoScheda")).Text = eventRow.SubEventDescription;

                    this.lastFDSRowClass = this.lastFDSRowClass.Equals("grdL", StringComparison.OrdinalIgnoreCase)
                                               ? "grdD"
                                               : "grdL";
                }

                this.lastBoundPosizione = eventRow.EventDescriptionValue;
                this.lastBoundTipoScheda = eventRow.SubEventDescription;

                this.SetCurrentFDSRowClass(e);
            }
        }
    }

    private void SetCurrentFDSRowClass(ListViewItemEventArgs e)
    {
        string rowClass = this.lastFDSRowClass.Equals("grdL", StringComparison.OrdinalIgnoreCase) ? "grdD" : "grdL";

        ((HtmlControl) e.Item.FindControl("tdData")).Attributes["class"] = rowClass;
        ((HtmlControl) e.Item.FindControl("tdOra")).Attributes["class"] = rowClass;
        ((HtmlControl) e.Item.FindControl("tdPosizione")).Attributes["class"] = rowClass;
        ((HtmlControl) e.Item.FindControl("tdTipoScheda")).Attributes["class"] = rowClass;
        ((HtmlControl) e.Item.FindControl("tdEvento")).Attributes["class"] = rowClass;
    }

    private TipoFDS MapFDSTypeToEnum (string deviceType)
    {
        switch (deviceType)
        {
            case "FD90000-NL":
                return TipoFDS.NormaleLocale;
            case "FD90000-NR":
                return TipoFDS.NormaleRemoto;
            case "FD90000-RL":
                return TipoFDS.RiservaLocale;
            case "FD90000-RR":
                return TipoFDS.RiservaRemoto;
            case "FD90000L-NL":
                return TipoFDS.LightNormaleLocale;
            case "FD90000L-NR":
                return TipoFDS.LightNormaleRemoto;
            case "FD90000L-RL":
                return TipoFDS.LightRiservaLocale;
            case "FD90000L-RR":
                return TipoFDS.LightRiservaRemoto;
            default:
                return TipoFDS.Unknown;
        }
    }
}
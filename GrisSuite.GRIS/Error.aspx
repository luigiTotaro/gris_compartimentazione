<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="_Error" %>
<asp:content ID="cntError" ContentPlaceHolderID="cphMainArea" Runat="Server">
<asp:timer id="tmrError" runat="server" enabled="true" interval="4000" 
		ontick="tmrError_Tick"></asp:timer>
<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
    <tr align="left">
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tr style="background-image: url(IMG/interfaccia/nav_bg.gif); background-repeat: repeat-x ">
                    <td style="height:30px">
                        <table border="0" cellpadding="0" cellspacing="0" style="height:100%;">
                            <tr align="left">
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2" style="background-image: url(IMG/interfaccia/nav_shadow_bg.gif); height:21px; background-repeat:repeat-x;"></td></tr>
            </table>     
        </td>
    </tr>
</table>   
<table>
    <tr><td style="height:100px;">&nbsp;</td></tr>
    <tr style="text-align: center;"><td>
  <span style="color: White; font-weight: bold; font-size: larger; text-align: center;">Errore Applicazione</span>
 </td></tr>
 <tr style="text-align: center;"><td>
  <span style="color: White; font-weight: bold; font-size: larger;">Ripristino applicazione in corso!</span>
 </td></tr>
 <tr style="text-align: center;"><td>
  <span style="color: White; font-size: larger;">Se il ripristino dell'applicazione non avviene entro 4 secondi, fare click <a href="Italia.aspx" style="color: White; font-weight: bold; font-size: large;">qui</a></span>
 </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr style="text-align: center;">
  <td><img src="IMG/countdown_animation.gif" alt="Errore, sarai rediretto su un' altra pagina" /></td>
 </tr>
    <tr><td style="height:100px;">&nbsp;</td></tr>
</table></asp:content>


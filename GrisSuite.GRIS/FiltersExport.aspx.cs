﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Edulife.Presentation.Web.Components;
using GrisSuite.Common;

public partial class FiltersExport : Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        # region Verifica Permessi

        if (!GrisSessionManager.FiltersPageSelectionsExists)
        {
           Response.End();
        }

        # endregion

        DataView dv = this.SqlDataSourceFindDevices.Select(DataSourceSelectArguments.Empty) as DataView;
        if (dv == null) return;
        
        ExcelTransferUtility excelUtil = new ExcelTransferUtility(Server);
        excelUtil.Transfer(dv.Table);

        this.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}_{1}.xls", "Filter", DateTime.Today.ToShortDateString()));
        this.Response.ContentType = "application/ms-excel";
        this.Response.Write(excelUtil.Result);
        this.Response.End();
    }


    protected void SqlDataSourceFindDevices_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        FiltersPageSelections filters = GrisSessionManager.FiltersPageSelections;
        //e.Arguments.SortExpression = 
        e.Command.Parameters["@NodIDs"].Value = filters.NodeIDs;
        e.Command.Parameters["@SystemIDs"].Value = filters.SystemIDs;
        e.Command.Parameters["@DeviceTypeIDs"].Value = filters.DeviceTypeIDs;
        e.Command.Parameters["@SevLevelIDs"].Value = filters.SevLevelIDs;
        e.Command.Parameters["@DeviceData"].Value = filters.DeviceData;
    }
}
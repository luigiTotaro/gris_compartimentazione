<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Device.aspx.cs" Inherits="Device" StylesheetTheme="Gris" %>

<%@ Register Src="~/Controls/STLCInfo.ascx" TagName="STLCInfo" TagPrefix="gris" %>
<%@ Register Src="~/Controls/GaugePanel.ascx" TagName="GaugePanel" TagPrefix="gris" %>
<%@ Register Src="~/Controls/GaugePanelPZi.ascx" TagName="GaugePanelPZi" TagPrefix="gris" %>
<%@ Register Src="~/Controls/DevPanelHorizontal.ascx" TagName="DevPanelHorizontal"
    TagPrefix="gris" %>
<%@ Register Src="~/Controls/ImageCheckBox.ascx" TagName="ImageCheckBox" TagPrefix="gris" %>
<%@ Register Src="Controls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="gris" %>
<%@ Register Src="Controls/FilterButton.ascx" TagName="FilterButton" TagPrefix="gris" %>
<%@ Register TagPrefix="gris" TagName="selectorspanel" Src="~/Controls/SelectorsPanel.ascx" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <asp:Timer ID="tmrDevice" runat="server" Enabled="true" OnTick="tmrDevice_Tick" />
    <input id="btnMessagesTrigger" runat="server" type="button" style="display: none"
        onserverclick="btnMessagesTrigger_Click" causesvalidation="false" />
    <input id="btnEventsTrigger" runat="server" type="button" style="display: none" onserverclick="btnEventsTrigger_Click"
        causesvalidation="false" />
    <input id="btnFDSTrigger" runat="server" type="button" style="display: none" onserverclick="btnFDSTrigger_Click"
        causesvalidation="false" />
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; height: 100%">
        <tr>
            <td style="text-align: left; width: 100%">
                <asp:UpdatePanel ID="updNavBar" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <gris:NavigationBar ID="navBar" runat="server" OnRefreshPageButtonClick="navBar_OnRefreshPageButtonClick" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <asp:UpdatePanel ID="updSystems" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divSystems" style="width: 100%;">
                            <gris:selectorspanel ID="SystemSelector" runat="server" OnSelectorClick="SystemSelector_SelectorSystemClicked" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="navBar" EventName="RefreshPageButtonClick" />
                        <asp:AsyncPostBackTrigger ControlID="navBar" EventName="NavigationCommand" />
                        <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; vertical-align: top; height: 264px; width: 100%">
                <asp:UpdatePanel ID="updDevPanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlstDevPanel" runat="server" DataSourceID="odsDevices" CellPadding="15"
                            Height="35px" ShowFooter="False" ShowHeader="False" ItemStyle-VerticalAlign="Top"
                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100%" OnItemDataBound="dlstDevPanel_ItemDataBound"
                            RepeatDirection="Vertical">
                            <ItemTemplate>
                                <gris:DevPanelHorizontal ID="DevPanelDetail" runat="server" DevID='<%# Eval("DevID") %>'
                                    Host='<%# Eval("STLC1000") %>' Name='<%# Eval("Name") %>' Type='<%# Eval("Type") %>'
                                    RawType='<%# Eval("RawType") %>' Building='<%# Eval("BuildingName") %>' Rack='<%# Eval("RackName") %>'
                                    BuildingDescription='<%# Eval("BuildingDescription") %>' RackDescription='<%# Eval("RackDescription") %>'
                                    SeverityLevel='<%# Eval("SevLevel") %>' SeverityLevelDescription='<%# Eval("SevLevelDescription") %>'
                                    FullHostName='<%# Eval("STLC1000FullHostName") %>' Address='<%# Eval("Address") %>'
                                    SerialNumber='<%# Eval("DeviceSerialNumber") %>' WSUrlPattern='<%# Eval("WSUrlPattern") %>'
                                    Vendor='<%# Eval("VendorName") %>' PortName='<%# Eval("PortName") %>' PortType='<%# Eval("PortType") %>'
                                    SystemID='<%# Eval("SystemID") %>' OperatorMaintenance='<%# Eval("OperatorMaintenance") %>'
                                    OnChangeMaintenance="OnChangeMaintenance" Ticket='<%# Eval("Ticket") %>' RackPositionRow='<%# Eval("RackPositionRow") %>'
                                    RackPositionCol='<%# Eval("RackPositionCol") %>' SrvID='<%# Eval("SrvID") %>'
                                    DeviceImage='<%# Eval("DeviceImage") %>' />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" />
                        </asp:DataList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="odsDevices" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetDataByID" TypeName="GrisSuite.Data.Gris.DevicesDSTableAdapters.devicesTableAdapter"
                    OnSelecting="odsDevices_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="DevID" Type="Int64" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; vertical-align: top; width: 100%; padding-left: 11px;">
                <asp:UpdatePanel ID="updGauge" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlstGauge" runat="server" DataSourceID="odsGaugeInfos" CellPadding="2"
                            RepeatColumns="4" RepeatDirection="Horizontal" HorizontalAlign="Left">
                            <ItemTemplate>
                                <gris:GaugePanel ID="Gauge" runat="server" Delta='<%# Eval("DeltaValue") %>' Value='<%# Eval("Value") %>'
                                    ValueRif='<%# Eval("RefValue") %>' Header='<%# Eval("Name") %>' Desc='<%# Eval("Description") %>'
                                    MinValue='<%# Eval("MinValue") %>' MaxValue='<%# Eval("MaxValue") %>' />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlstGaugePzi" runat="server" DataSourceID="odsGaugePZiInfos" CellPadding="2"
                            RepeatColumns="4" RepeatDirection="Horizontal" HorizontalAlign="Left" OnItemDataBound="dlstGaugePzi_ItemDataBound">
                            <ItemTemplate>
                                <gris:GaugePanelPZi ID="GaugePZi" runat="server" Description='<%# Eval("Description") %>'
                                    ValueName='<%# Eval("Name") %>' ActualFormattedValue='<%# Eval("ActualValue") %>'
                                    ReferenceFormattedValue='<%# Eval("ReferenceValue") %>' SuperiorYellowRedOffsetPerc='<%# Eval("ErrorValue") %>'
                                    SuperiorYellowGreenOffsetPerc='<%# Eval("WarningValue") %>' InferiorYellowGreenOffsetPerc='<%# Eval("WarningValue") %>'
                                    InferiorYellowRedOffsetPerc='<%# Eval("ErrorValue") %>' />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList ID="dlstGaugeZeus" runat="server" DataSourceID="odsGaugeZeusInfos"
                            CellPadding="2" RepeatColumns="4" RepeatDirection="Horizontal" HorizontalAlign="Left"
                            OnItemDataBound="dlstGaugeZeus_ItemDataBound">
                            <ItemTemplate>
                                <gris:GaugePanelPZi ID="GaugeZeus" runat="server" Description='<%# Eval("Description") %>'
                                    ValueName='<%# Eval("Name") %>' ActualFormattedValue='<%# Eval("ActualValue") %>'
                                    ReferenceFormattedValue='<%# Eval("ReferenceValue") %>' InferiorLimitPerc='<%# Eval("InferiorLimitValue") %>'
                                    InferiorYellowGreenOffsetPerc='<%# Eval("WarningValue") %>' InferiorYellowRedOffsetPerc='<%# Eval("ErrorValue") %>'
                                    InferiorLimitFormattedValue='<%# Eval("InferiorLimitValue") %>' InferiorRedLimitFormattedValue='<%# Eval("ErrorValue") %>' />
                            </ItemTemplate>
                        </asp:DataList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="odsGaugeInfos" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.GaugeValuesDSTableAdapters.GaugeDataTableAdapter"
                    OnSelecting="odsGaugeInfos_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="DevID" Type="Int64" />
                        <asp:Parameter Name="DeviceType" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGaugePZiInfos" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.GaugeValuesDSTableAdapters.PZiGaugeDataTableAdapter"
                    OnSelecting="odsGaugePZiInfos_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="DevID" Type="Int64" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGaugeZeusInfos" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.GaugeValuesDSTableAdapters.ZeusGaugeDataTableAdapter"
                    OnSelecting="odsGaugeZeusInfos_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="DevID" Type="Int64" />
                        <asp:Parameter Name="DeviceType" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding-top: 15px; padding-left: 12px">
                <act:TabContainer runat="server" ID="TabsMessagesEvents" Width="100%" CssClass="gris gris-gray"
                    ActiveTabIndex="0" OnClientActiveTabChanged="clientActiveTabChanged">
                    <act:TabPanel runat="server" ID="pnlFDS" OnDemandMode="None">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updpnlFDS" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnFDSTrigger" />
                                </Triggers>
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="tblFDS" visible="true">
                                        <tr>
                                            <td style="text-align: right; padding-top: 10px">
                                                <asp:Silverlight ID="silFDS" runat="server" Source="~/ClientBin/GrisSuite.FDS.UI.xap"
                                                    MinimumVersion="3.0.40624.0" Width="100%" Height="508px" Windowless="true" BackColor="#4C4C4C">
                                                    <PluginNotInstalledTemplate>
                                                        <div style="width: 100%; text-align: center">
                                                            <a href="http://www.microsoft.com/silverlight/handlers/getsilverlight.ashx?v=3.0"
                                                                style="text-decoration: none;">
                                                                <img src="IMG/Interfaccia/FDS-NoSilverlight.png" alt="Scarica Microsoft Silverlight"
                                                                    style="border-style: none" />
                                                            </a>
                                                            <br />
                                                            <br />
                                                        </div>
                                                    </PluginNotInstalledTemplate>
                                                </asp:Silverlight>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </act:TabPanel>
                    <act:TabPanel runat="server" ID="pnlMessages" OnDemandMode="None">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updpnlMessages" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnMessagesTrigger" />
                                </Triggers>
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="tlbMessages"
                                        visible="true">
                                        <tr>
                                            <td style="padding-top: 10px; padding-right: 10px;">
                                                <asp:UpdatePanel ID="updDevices" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="height: 30px; float: right;">
                                                            <tr>
                                                                <td style="width: 117px; height: 30px;">
                                                                    <gris:ImageCheckBox ID="ImageCheckBoxErr" runat="server" FieldDescription="Errore"
                                                                        FieldImagePathOn="~/IMG/interfaccia/filter_stato2_pressed.gif" FieldImagePathOff="~/IMG/interfaccia/filter_stato2.gif" />
                                                                </td>
                                                                <td style="width: 117px; height: 30px;">
                                                                    <gris:ImageCheckBox ID="ImageCheckBoxWarn" runat="server" FieldDescription="Attenzione"
                                                                        FieldImagePathOn="~/IMG/interfaccia/filter_stato1_pressed.gif" FieldImagePathOff="~/IMG/interfaccia/filter_stato1.gif" />
                                                                </td>
                                                                <td style="width: 117px; height: 30px;">
                                                                    <gris:ImageCheckBox ID="ImageCheckBoxOK" runat="server" FieldDescription="OK" FieldImagePathOn="~/IMG/interfaccia/filter_stato0_pressed.gif"
                                                                        FieldImagePathOff="~/IMG/interfaccia/filter_stato0.gif" />
                                                                </td>
                                                                <td style="width: 117px; height: 30px;">
                                                                    <gris:ImageCheckBox ID="ImageCheckBoxUnKnown" runat="server" FieldDescription="Sconosciuto"
                                                                        FieldImagePathOn="~/IMG/interfaccia/filter_stato255_pressed.gif" FieldImagePathOff="~/IMG/interfaccia/filter_stato255.gif"
                                                                        Pressed="False" />
                                                                </td>
                                                                <td style="width: 117px; height: 30px;">
                                                                    <gris:ImageCheckBox ID="ImageCheckBoxInfo" runat="server" FieldDescription="Informazione"
                                                                        FieldImagePathOn="~/IMG/interfaccia/filter_stato-255_pressed.gif" FieldImagePathOff="~/IMG/interfaccia/filter_stato-255.gif" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; width: 100%; height: 40px">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                                                    <tr>
                                                        <td style="text-align: left; vertical-align: top;">
                                                            <asp:UpdatePanel ID="updMessaggi" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:ListView ID="lvwStreams" runat="server" DataSourceID="odsStreams" OnItemDataBound="lvwStreams_ItemDataBound"
                                                                        DataKeyNames="StrID">
                                                                        <LayoutTemplate>
                                                                            <div id="itemPlaceholderContainer" runat="server" style="width: 1170px;">
                                                                                <div id="trHead" runat="server" style="background-color: #647182; height: 25px;">
                                                                                    <div id="tdUltimaRicezioneStream" runat="server" style="width: 416px; text-align: left;
                                                                                        height: 25px; padding-top: 3px; float: left; font-weight: bold; padding-left: 10px;
                                                                                        color: #22344D; display: block;">
                                                                                        Ultima Ricezione
                                                                                    </div>
                                                                                    <div id="thImageStream" runat="server" style="width: 34px; float: left; height: 25px;
                                                                                        display: block;">
                                                                                    </div>
                                                                                    <div id="thDescrizioneStream" runat="server" style="width: 709px; float: left; font-weight: bold;
                                                                                        height: 25px; padding-top: 3px; padding-left: 1px; color: #22344D; display: block;">
                                                                                        Descrizione
                                                                                    </div>
                                                                                </div>
                                                                                <div id="itemPlaceholder" runat="server">
                                                                                </div>
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div style="width: 1170px;">
                                                                                <div>
                                                                                    <asp:Panel ID="PanelTitle" runat="server" Style="width: 1170px; height: 30px;">
                                                                                        <asp:LinkButton ID="lblCreationDateStream" runat="server" Text='<%# Eval("Date", "{0:dd/MM/yyy HH:mm}") %>'
                                                                                            Style="font-size: 11pt; font-weight: bold; text-align: center; float: left; width: 150px;
                                                                                            display: block; height: 30px; color: #FFFFFF; padding-top: 5px; text-decoration: none;
                                                                                            padding-right: 276px;" OnCommand="lblCreationDateStream_Command" CommandName="ToggleStream"
                                                                                            CommandArgument='<%# Eval("StrID")%>' />
                                                                                        <asp:ImageButton ID="imgToggleStreamDetails" runat="server" ImageUrl="IMG/interfaccia/espandi.gif"
                                                                                            AlternateText="Mostra dettagli" Style="float: left; width: 20px; padding-top: 2px;
                                                                                            display: block;" OnCommand="imgToggleStreamDetails_Command" CommandName="ToggleStream"
                                                                                            CommandArgument='<%# Eval("StrID")%>' />
                                                                                        <div style="vertical-align: text-top; width: 34px; float: left; display: block;">
                                                                                            <asp:Image ID="imgStatusStream" runat="server" />
                                                                                        </div>
                                                                                        <asp:LinkButton ID="lblTitle" runat="server" Text='<%# Eval("Name")%>' Style="color: #FFFFFF;
                                                                                            width: 690px; float: left; height: 30px; font-size: 11pt; font-weight: bold;
                                                                                            padding-top: 5px; text-decoration: none;" OnCommand="lblTitle_Command" CommandName="ToggleStream"
                                                                                            CommandArgument='<%# Eval("StrID")%>' />
                                                                                    </asp:Panel>
                                                                                </div>
                                                                                <act:CollapsiblePanelExtender ID="cpeStream" runat="server" TargetControlID="PanelContent"
                                                                                    ExpandControlID="PanelTitle" CollapseControlID="PanelTitle" ImageControlID="imgToggleStreamDetails"
                                                                                    ExpandedImage="IMG/interfaccia/contrai.gif" CollapsedImage="IMG/interfaccia/espandi.gif"
                                                                                    ExpandedText="Nascondi dettagli" CollapsedText="Mostra dettagli" SuppressPostBack="false" />
                                                                            </div>
                                                                            <div style="width: 1170px;">
                                                                                <asp:Panel ID="PanelContent" runat="server">
                                                                                    <asp:ListView ID="lvwMessaggi" runat="server" DataSourceID="odsMessaggi" OnItemDataBound="lvwMessaggi_ItemDataBound"
                                                                                        DataKeyNames="DevID,FieldID,StrID,ArrayID" OnItemCommand="lvwMessaggi_ItemCommand"
                                                                                        OnPreRender="lvwMessaggi_PreRender">
                                                                                        <LayoutTemplate>
                                                                                            <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 1170px;"
                                                                                                cellpadding="0" cellspacing="0">
                                                                                                <tr id="itemPlaceholder" runat="server">
                                                                                                </tr>
                                                                                            </table>
                                                                                        </LayoutTemplate>
                                                                                        <ItemTemplate>
                                                                                            <tr>
                                                                                                <td style="vertical-align: top; padding-top: 5px; width: 444px;">
                                                                                                    <asp:Label ID="lblCreationDate" runat="server" Text='<%# Eval("Date", "{0:dd/MM/yyy HH:mm}") %>'
                                                                                                        Style="font-size: 10pt; text-align: center; color: #FFFFFF; display: block; float: left;
                                                                                                        width: 150px;" />
                                                                                                    <div class="Tmessaggi" style="float: left; width: 280px; display: block; text-align: center;">
                                                                                                        <asp:Label ID="lblStreamValue" runat="server" Text='<%# Eval("Value") %>' Style="font-size: 10pt;" /></div>
                                                                                                </td>
                                                                                                <td style="vertical-align: text-top; width: 20px; padding-top: 2px;">
                                                                                                    <asp:ImageButton ID="imgToggleStreamFieldDetails" runat="server" ImageUrl="IMG/interfaccia/espandi.gif"
                                                                                                        AlternateText="Dettagli stream field" Style="width: 20px; display: block;" CommandName="ToggleDetails"
                                                                                                        CommandArgument='<%# Eval("StrID") + "," + Eval("FieldID") + "," + Eval("ArrayID") %>' />
                                                                                                </td>
                                                                                                <td style="vertical-align: text-top; width: 34px;">
                                                                                                    <asp:Image ID="imgStatus" runat="server" />
                                                                                                </td>
                                                                                                <td style="width: 654px;">
                                                                                                    <div class="Tmessaggi" style="width: 640px; padding-top: 4px; margin-right: 10px;
                                                                                                        height: 23px;">
                                                                                                        <asp:LinkButton ID="lblTMessaggi" runat="server" Text='<%# GUtility.FormatFieldName(Eval("Name").ToString(), (long)Eval("DevID"), (int)Eval("StrID"), (int)Eval("FieldID"), (int)Eval("ArrayID")) %>'
                                                                                                            CssClass="gvRow" CommandName="ToggleDetails" CommandArgument='<%# Eval("StrID") + "," + Eval("FieldID") + "," + Eval("ArrayID") %>' /></div>
                                                                                                    <div class="messaggi" style="color: #398DE5; font-size: 10pt; width: 640px;">
                                                                                                        <asp:Label ID="lblMessaggi" runat="server" Text='<%# FormatMessaggiNonOK(Eval("StreamName").ToString(), Eval("Description").ToString()) %>' /><asp:Label
                                                                                                            ID="lblStreamFieldData" runat="server" Text='<%# FormatStreamFieldData(Eval("StreamName").ToString(), Eval("Description").ToString()) %>' />
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                        <ItemSeparatorTemplate>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <div style="border-top-style: solid; border-top-color: #808080; border-top-width: 1px;
                                                                                                        height: 2px;">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemSeparatorTemplate>
                                                                                    </asp:ListView>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemSeparatorTemplate>
                                                                            <div>
                                                                                <div style="border-top-style: solid; border-top-color: #808080; border-top-width: 2px;
                                                                                    height: 2px;">
                                                                                </div>
                                                                            </div>
                                                                        </ItemSeparatorTemplate>
                                                                    </asp:ListView>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ImageCheckBoxErr" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ImageCheckBoxWarn" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ImageCheckBoxOK" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ImageCheckBoxUnKnown" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ImageCheckBoxInfo" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:ObjectDataSource ID="odsStreams" runat="server" SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.MessaggiDSTableAdapters.streamsTableAdapter"
                                                                OldValuesParameterFormatString="original_{0}" OnSelecting="odsStreams_Selecting">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="DevID" Type="Int64" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxUnKnown" DefaultValue="False" Name="ViewUnknowns"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxErr" DefaultValue="False" Name="ViewErrors"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxWarn" DefaultValue="False" Name="ViewWarnings"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxOK" DefaultValue="False" Name="ViewOKs"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxInfo" DefaultValue="False" Name="ViewInfos"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                            <asp:ObjectDataSource ID="odsMessaggi" runat="server" SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.MessaggiDSTableAdapters.stream_fieldsTableAdapter"
                                                                OldValuesParameterFormatString="original_{0}" OnSelecting="odsMessaggi_Selecting">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="DevID" Type="Int64" />
                                                                    <asp:Parameter Name="StrID" Type="Int64" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxUnKnown" DefaultValue="False" Name="ViewUnknowns"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxErr" DefaultValue="False" Name="ViewErrors"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxWarn" DefaultValue="False" Name="ViewWarnings"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxOK" DefaultValue="False" Name="ViewOKs"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="ImageCheckBoxInfo" DefaultValue="False" Name="ViewInfos"
                                                                        PropertyName="Pressed" Type="Boolean" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                            <br />
                                                        </td>
                                                        <td style="width: 10px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </act:TabPanel>
                    <act:TabPanel runat="server" ID="pnlEvents" OnDemandMode="None">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updpnlEvents" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnEventsTrigger" />
                                </Triggers>
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="tblEvents"
                                        visible="false">
                                        <tr>
                                            <td style="text-align: right; padding-top: 10px">
                                                <asp:UpdatePanel ID="updEvents" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="height: 30px;">
                                                            <tr>
                                                                <td style="width: 200px; height: 30px; vertical-align: top">
                                                                    <div style="font-family: Arial; font-size: 10pt; font-weight: bold; color: #fff;
                                                                        float: left; padding-top: 6px;">
                                                                        Giorno:&nbsp;</div>
                                                                    <div style="background-image: url(IMG/Interfaccia/sfondo_calendario.gif); border: 0px;
                                                                        background-repeat: no-repeat; width: 130px; margin: 0px; height: 30px; float: left;">
                                                                        <asp:TextBox ID="txtCreatedOn" runat="server" MaxLength="10" AutoPostBack="true"
                                                                            Style="font-family: Arial; font-size: 10pt; color: #0D0D0D; font-weight: bold;
                                                                            border: 0px; width: 100px; margin: 0px; height: 30px; background-color: Transparent;
                                                                            padding-top: 6px; padding-left: 25px;" OnTextChanged="txtCreatedOn_TextChanged" /><act:CalendarExtender
                                                                                ID="calCreatedOn" TargetControlID="txtCreatedOn" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                                                                runat="server" CssClass="gris-calendar" />
                                                                    </div>
                                                                </td>
                                                                <td style="width: 220px; height: 30px; vertical-align: top; border: 0px; padding: 0px;
                                                                    padding-right: 10px">
                                                                    <div style="font-family: Arial; font-size: 10pt; font-weight: bold; color: #fff;
                                                                        float: left; padding-top: 6px;">
                                                                        Dalle-alle:&nbsp;</div>
                                                                    <div style="border: 0px; width: 125px; margin: 0px; height: 30px; float: left;">
                                                                        <asp:DropDownList ID="cboCreatedHoursRange" runat="server" AutoPostBack="true" Style="font-family: Arial;
                                                                            font-size: 10pt; color: #0D0D0D; font-weight: bold; border: 0px; width: 120px;
                                                                            margin: 0px; height: 30px; margin-top: 3px; background-color: #EBEBEB; border-color: #4C4C4C"
                                                                            OnSelectedIndexChanged="cboCreatedHoursRange_SelectedIndexChanged">
                                                                            <asp:ListItem Value="00.00.00-00.59.59" Text="00.00-01.00" />
                                                                            <asp:ListItem Value="01.00.00-01.59.59" Text="01.00-02.00" />
                                                                            <asp:ListItem Value="02.00.00-02.59.59" Text="02.00-03.00" />
                                                                            <asp:ListItem Value="03.00.00-03.59.59" Text="03.00-04.00" />
                                                                            <asp:ListItem Value="04.00.00-04.59.59" Text="04.00-05.00" />
                                                                            <asp:ListItem Value="05.00.00-05.59.59" Text="05.00-06.00" />
                                                                            <asp:ListItem Value="06.00.00-06.59.59" Text="06.00-07.00" />
                                                                            <asp:ListItem Value="07.00.00-07.59.59" Text="07.00-08.00" />
                                                                            <asp:ListItem Value="08.00.00-08.59.59" Text="08.00-09.00" />
                                                                            <asp:ListItem Value="09.00.00-09.59.59" Text="09.00-10.00" />
                                                                            <asp:ListItem Value="10.00.00-10.59.59" Text="10.00-11.00" />
                                                                            <asp:ListItem Value="11.00.00-11.59.59" Text="11.00-12.00" />
                                                                            <asp:ListItem Value="12.00.00-12.59.59" Text="12.00-13.00" />
                                                                            <asp:ListItem Value="13.00.00-13.59.59" Text="13.00-14.00" />
                                                                            <asp:ListItem Value="14.00.00-14.59.59" Text="14.00-15.00" />
                                                                            <asp:ListItem Value="15.00.00-15.59.59" Text="15.00-16.00" />
                                                                            <asp:ListItem Value="16.00.00-16.59.59" Text="16.00-17.00" />
                                                                            <asp:ListItem Value="17.00.00-17.59.59" Text="17.00-18.00" />
                                                                            <asp:ListItem Value="18.00.00-18.59.59" Text="18.00-19.00" />
                                                                            <asp:ListItem Value="19.00.00-19.59.59" Text="19.00-20.00" />
                                                                            <asp:ListItem Value="20.00.00-20.59.59" Text="20.00-21.00" />
                                                                            <asp:ListItem Value="21.00.00-21.59.59" Text="21.00-22.00" />
                                                                            <asp:ListItem Value="22.00.00-22.59.59" Text="22.00-23.00" />
                                                                            <asp:ListItem Value="23.00.00-23.59.59" Text="23.00-00.00" />
                                                                            <asp:ListItem Value="" Text="Tutto il giorno" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 745px; height: 30px; vertical-align: top; border: 0px; padding: 0px;
                                                                    padding-right: 10px">
                                                                    <table cellpadding="1" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <gris:FilterButton ID="EventCodeFilter45" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Esito test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                                                    Active="true" OnClick="EventsCodeFilter_Click" />
                                                                            </td>
                                                                            <td>
                                                                                <gris:FilterButton ID="EventCodeFilter43" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;Attivazione sonora&nbsp;&nbsp;&nbsp;&nbsp;"
                                                                                    Active="true" OnClick="EventsCodeFilter_Click" />
                                                                            </td>
                                                                            <td>
                                                                                <gris:FilterButton ID="EventCodeFilter44" runat="server" Text="Fine attivazione sonora"
                                                                                    Active="true" OnClick="EventsCodeFilter_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; width: 100%; height: 40px">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                                                    <tr>
                                                        <td style="text-align: left; vertical-align: top;">
                                                            <asp:UpdatePanel ID="updEventi" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="lblEventsCountTop" runat="server" ForeColor="#2CD3FB" Style="padding-top: 20px;
                                                                        padding-bottom: 10px; display: block; padding-left: 10px;" />
                                                                    <asp:ListView ID="lvwEventiMaster" runat="server" DataSourceID="odsEventiMaster"
                                                                        OnItemDataBound="lvwEventiMaster_ItemDataBound" DataKeyNames="EventID" OnDataBound="lvwEventiMaster_DataBound">
                                                                        <LayoutTemplate>
                                                                            <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;"
                                                                                cellpadding="0" cellspacing="0">
                                                                                <tr id="trHead" runat="server">
                                                                                    <th id="thImage" runat="server" style="width: 34px">
                                                                                    </th>
                                                                                    <th id="thDescrizione" runat="server" style="width: 950px" class="GridViewLeftPadding">
                                                                                        Descrizione
                                                                                    </th>
                                                                                    <th id="tdCreazione" runat="server" style="width: 170px; text-align: center" class="GridViewLeftPadding">
                                                                                        Creazione
                                                                                    </th>
                                                                                </tr>
                                                                                <tr id="itemPlaceholder" runat="server">
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3" style="text-align: right; padding-top: 5px;">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="vertical-align: text-top">
                                                                                    <asp:Image ID="imgStatus" runat="server" />
                                                                                </td>
                                                                                <td style="height: 25px; vertical-align: top;">
                                                                                    <div class="Tmessaggi" style="float: left; width: 100%; height: 20px; padding-top: 6px;">
                                                                                        <asp:LinkButton ID="lbtEventDescription" runat="server" CssClass="gvRow" CommandName="Select" /></div>
                                                                                </td>
                                                                                <td style="text-align: center; vertical-align: top; padding-top: 6px">
                                                                                    <asp:Label ID="lblCreated" runat="server" Text='<%# Eval("Created", "{0:dd/MM/yyy HH:mm:ss}") %>'
                                                                                        CssClass="GridViewRow" />
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <SelectedItemTemplate>
                                                                            <tr>
                                                                                <td style="vertical-align: text-top">
                                                                                    <asp:Image ID="imgStatus" runat="server" />
                                                                                </td>
                                                                                <td style="height: 25px; vertical-align: top;">
                                                                                    <div class="Tmessaggi" style="float: left; width: 60%; height: 20px; padding-top: 6px;">
                                                                                        <asp:Label ID="lblEventDescription" runat="server" CssClass="GridViewRow"></asp:Label></div>
                                                                                    <div class="messaggi" style="clear: both">
                                                                                        <asp:ListView ID="lvwEventiDetail" runat="server" DataSourceID="odsEventiDetail"
                                                                                            OnItemDataBound="lvwEventiDetail_ItemDataBound" DataKeyNames="EventID">
                                                                                            <LayoutTemplate>
                                                                                                <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;"
                                                                                                    cellpadding="0" cellspacing="0">
                                                                                                    <tr id="itemPlaceholder" runat="server">
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </LayoutTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td style="vertical-align: text-top; width: 25px;">
                                                                                                        <asp:Image ID="imgStatus" runat="server" />
                                                                                                    </td>
                                                                                                    <td style="height: 25px">
                                                                                                        <div class="Tmessaggi" style="float: left; width: 100%; height: 20px; padding-top: 6px;">
                                                                                                            <asp:Label ID="lblEventDescription" runat="server" CssClass="messaggi" /></div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:ListView>
                                                                                    </div>
                                                                                </td>
                                                                                <td style="text-align: center; vertical-align: top; padding-top: 6px;">
                                                                                    <asp:Label ID="lblCreated" runat="server" Text='<%# Eval("Created", "{0:dd/MM/yyy HH:mm:ss}") %>'
                                                                                        CssClass="GridViewRow" />
                                                                                </td>
                                                                            </tr>
                                                                        </SelectedItemTemplate>
                                                                        <ItemSeparatorTemplate>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <div style="border-top-style: solid; border-top-color: #808080; border-top-width: 1px;
                                                                                        height: 2px;">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemSeparatorTemplate>
                                                                    </asp:ListView>
                                                                    <asp:ListView ID="lvwEventiFDS" runat="server" DataSourceID="odsEventiMaster" OnItemDataBound="lvwEventiFDS_ItemDataBound"
                                                                        DataKeyNames="EventID" OnDataBound="lvwEventiFDS_DataBound">
                                                                        <LayoutTemplate>
                                                                            <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;"
                                                                                cellpadding="0" cellspacing="0">
                                                                                <tr id="trHead" runat="server">
                                                                                    <th id="thData" runat="server" style="width: 100px; text-align: center">
                                                                                        Data
                                                                                    </th>
                                                                                    <th id="thOra" runat="server" style="width: 100px; text-align: center">
                                                                                        Ora
                                                                                    </th>
                                                                                    <th id="thPosizione" runat="server" style="width: 165px; text-align: left">
                                                                                        Posizione
                                                                                    </th>
                                                                                    <th id="thTipoScheda" runat="server" style="width: 185px; text-align: left">
                                                                                        Tipo scheda
                                                                                    </th>
                                                                                    <th id="thEvento" runat="server" style="width: 604px; text-align: left">
                                                                                        Evento
                                                                                    </th>
                                                                                </tr>
                                                                                <tr id="itemPlaceholder" runat="server">
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="5" style="text-align: right; padding-top: 5px;">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="text-align:center;" runat="server" id="tdData">
                                                                                    <asp:Label ID="lblData" runat="server" Text='<%# Eval("Created", "{0:dd/MM/yyy}") %>'
                                                                                        CssClass="grdr" />
                                                                                </td>
                                                                                <td style="text-align:center;" runat="server" id="tdOra">
                                                                                    <asp:Label ID="lblOra" runat="server" Text='<%# Eval("Created", "{0:HH:mm:ss}") %>'
                                                                                        CssClass="grdr" />
                                                                                </td>
                                                                                <td style="text-align:left;" runat="server" id="tdPosizione">
                                                                                    <asp:Label ID="lblPosizione" runat="server" CssClass="grdr" />
                                                                                </td>
                                                                                <td style="text-align:left;" runat="server" id="tdTipoScheda">
                                                                                    <asp:Label ID="lblTipoScheda" runat="server" CssClass="grdr" />
                                                                                </td>
                                                                                <td style="text-align:left;" runat="server" id="tdEvento">
                                                                                    <asp:Label ID="lblEvento" runat="server" Text='<%# Eval("EventDescription") %>' CssClass="grdr" />
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:ListView>
                                                                    <asp:Label ID="lblEventsCountBottom" runat="server" ForeColor="#2CD3FB" Style="padding-top: 5px;
                                                                        padding-bottom: 5px; display: block; padding-left: 10px;" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:ObjectDataSource ID="odsEventiMaster" runat="server" OldValuesParameterFormatString="original_{0}"
                                                                SelectMethod="GetData" TypeName="GrisSuite.Data.dsCentralEventsTableAdapters.DecodedEventDataTableAdapter"
                                                                OnSelecting="odsEventiMaster_Selecting">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="DevID" Type="Int64" />
                                                                    <asp:Parameter Name="CreatedFrom" Type="DateTime" />
                                                                    <asp:Parameter Name="CreatedTo" Type="DateTime" />
                                                                    <asp:ControlParameter ControlID="EventCodeFilter43" DefaultValue="1" Name="EventCodeFilter43"
                                                                        PropertyName="Active" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="EventCodeFilter44" DefaultValue="1" Name="EventCodeFilter44"
                                                                        PropertyName="Active" Type="Boolean" />
                                                                    <asp:ControlParameter ControlID="EventCodeFilter45" DefaultValue="1" Name="EventCodeFilter45"
                                                                        PropertyName="Active" Type="Boolean" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                            <asp:ObjectDataSource ID="odsEventiDetail" runat="server" OldValuesParameterFormatString="original_{0}"
                                                                SelectMethod="GetData" TypeName="GrisSuite.Data.dsCentralEventsTableAdapters.DecodedEventDataByCreationTimeTableAdapter"
                                                                OnSelecting="odsEventiDetail_Selecting">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="lvwEventiMaster" DbType="Guid" Name="EventID" PropertyName="SelectedValue" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                            <br />
                                                        </td>
                                                        <td style="width: 10px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </act:TabPanel>
                </act:TabContainer>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="updSTLCInfo" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlstSTLCInfo" runat="server" DataKeyField="SrvID" DataSourceID="SqlDataSTLCInfo"
                            Width="100%" OnItemDataBound="dlstSTLCInfo_ItemDataBound">
                            <ItemTemplate>
                                <gris:STLCInfo ID="STLCInfo1" runat="server" SrvID='<%# Eval("SrvID") %>' FullHostName='<%# Eval("FullHostName") %>'
                                    IP='<%# Eval("IP") %>' LastUpdate='<%# Eval("LastUpdate") %>' LastMessageType='<%# Eval("LastMessageType") %>'
                                    NodID='<%# Eval("NodID") %>' InMaintenance='<%# Eval("InMaintenance") %>' SevLevel='<%# Eval("SevLevel") %>'
                                    SevLevelDesc='<%# Eval("SevLevelDesc") %>' SevLevelReal='<%# Eval("SevLevelReal") %>'
                                    SevLevelDetailId='<%# Eval("SevLevelDetailId") %>' SevLevelDetailDesc='<%# Eval("SevLevelDetailDesc") %>'
                                    OnSyncComplete="STLCInfo1_OnSyncComplete" OnServerDelete="STLCInfo1_OnServerDelete"
                                    OnChangeMaintenance="OnChangeMaintenance" />
                            </ItemTemplate>
                        </asp:DataList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrDevice" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:SqlDataSource ID="SqlDataSTLCInfo" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                    SelectCommand="gris_GetSTLCInfobyNodIDAndDevID" SelectCommandType="StoredProcedure"
                    OnSelecting="SqlDataSTLCInfo_Selecting" OnSelected="SqlDataSTLCInfo_Selected">
                    <SelectParameters>
                        <asp:Parameter Name="NodID" Type="Int64" />
                        <asp:Parameter Name="DevID" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

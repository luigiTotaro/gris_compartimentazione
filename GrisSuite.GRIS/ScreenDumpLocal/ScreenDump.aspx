<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ScreenDump.aspx.cs" Inherits="ScreenDump" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GRIS</title>
    <link href="../CSS/Gris.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link rel="icon" href="../favicon.ico" />
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon"/>
</head>
<body style="margin: 0px; padding: 0px; border: 0px; background-color: #4c4c4c; width: 100%;">
    <form id="frmSD" runat="server">
    
    <asp:HiddenField ID="ImageBase64____"        runat = server />
    <asp:HiddenField ID="ImageBase64Raw____"        runat = server />
    
    
    <act:ToolkitScriptManager ID="manScreenDump" runat="server" AllowCustomErrorsRedirect="True"
        EnableScriptGlobalization="true" EnableScriptLocalization="true" AsyncPostBackTimeout="300"
        OnAsyncPostBackError="manScreenDump_AsyncPostBackError" />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; text-align: center;
        background-color: #4c4c4c;">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="background-image: url(../IMG/interfaccia/nav_bg.gif); height: 30px;">
                            <div style="height: 30px; float: right;">
                                <table style="text-align: center; color: #ffffff; white-space: nowrap;">
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPrintPDF" runat="server" Text="Esporta PDF" CssClass="DevPanelType"
                                                OnClick="lnkPrintPDF_Click"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSep5" runat="server" Text="|" EnableViewState="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Stampa" CssClass="DevPanelType"
                                                OnClientClick="window.print();" OnClick="lnkPrint_Click"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSep4" runat="server" Text="|" EnableViewState="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkScreenDump" runat="server" Text="IMG" CssClass="DevPanelType"
                                                OnClick="lnkScreenDump_Click"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSep3" runat="server" Text="|" EnableViewState="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkScreenDumpAuto" runat="server" Text="IMG (Adatta)" CssClass="DevPanelType"
                                                OnClick="lnkScreenDumpAuto_Click" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSep" runat="server" Text="|" EnableViewState="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-image: url(../IMG/interfaccia/nav_shadow_bg.gif); height: 21px;
                            background-repeat: repeat-x;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table cellpadding="2" cellspacing="2" style="width: 100%;">
                    <tr>
                        <td style="width: 80px; vertical-align: top; text-align: right;">
                            <asp:Label ID="lblNodeLabel" runat="server" EnableViewState="false" CssClass="labelBigBlack"
                                Font-Bold="true"></asp:Label>
                        </td>
                        <td style="vertical-align: top; text-align: left;">
                            <asp:Label ID="lblNodeName" runat="server" CssClass="labelBigWhite" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="width: 40%; vertical-align: top; white-space: nowrap;" rowspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Label ID="lblVendorLabel" runat="server" EnableViewState="false" Font-Size="10pt"
                                            CssClass="labelBigBlack"></asp:Label>
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblVendor" runat="server" Font-Size="10pt" CssClass="DevPanelDesc"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Label ID="lblIPLabel" runat="server" EnableViewState="false" Font-Size="10pt"
                                            CssClass="labelBigBlack"></asp:Label>
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblIP" runat="server" Font-Size="10pt" CssClass="DevPanelDesc"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Label ID="lblWhereLabel" runat="server" EnableViewState="false" Font-Size="10pt"
                                            CssClass="labelBigBlack"></asp:Label>
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblWhere" runat="server" Font-Size="10pt" CssClass="DevPanelDesc"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Label ID="lblDataOraLabel" runat="server" EnableViewState="false" Font-Size="10pt"
                                            CssClass="labelBigBlack"></asp:Label>
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblDataOra" runat="server" Font-Size="10pt" CssClass="DevPanelDesc"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Label ID="lblNotesLabel" runat="server" EnableViewState="false" Font-Size="10pt"
                                            CssClass="labelBigBlack"></asp:Label>
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblNotes" runat="server" Font-Size="10pt" CssClass="DevPanelDesc"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; text-align: right;">
                            <asp:Label ID="lblMonLabel" runat="server" EnableViewState="False" CssClass="labelBigBlack"
                                Font-Bold="true"></asp:Label>
                        </td>
                        <td style="vertical-align: top; text-align: left;">
                            <asp:Label ID="lblMonName" runat="server" CssClass="labelBigWhite" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="tblOutput" style="width: 100%;">
        <asp:UpdatePanel ID="updScreenDump" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table id="tblScreenDump" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                    text-align: center; background-color: #4c4c4c;">
                    <tr id="trProgress">
                        <td>
                            <table cellpadding="4" cellspacing="4" style="width: 100%;">
                                <tr>
                                    <td style="width: 20%; text-align: right;">
                                        <asp:Image ID="imgProgress" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/progressmonitor.gif" />&nbsp;
                                    </td>
                                    <td style="width: 80%; vertical-align: middle; text-align: left;">
                                        <asp:Label ID="lblProgress" runat="server" ForeColor="#bbbbbb" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trError">
                        <td>
                            <br />
                            <br />
                            <asp:Label ID="lblError" runat="server" ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div id="divSC" style="text-align: center;">
                    <asp:Image ID="imgScreenDump" runat="server" />
                </div>
            </ContentTemplate>

        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updShownData" runat="server" UpdateMode="Conditional" Visible="false">
            <ContentTemplate>
                <table id="tblShownData" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                    text-align: center; background-color: #000000;">
                    <tr>
                        <td>
                            <iframe id="ifrShownData" runat="server" style="width: 700px; height: 200px; background-color: #000000"
                                scrolling="auto" frameborder="0"></iframe>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Node.aspx.cs" Inherits="Node" Theme="Gris" %>

<%@ Register Src="~/Controls/STLCInfo.ascx" TagName="STLCInfo" TagPrefix="gris" %>
<%@ Register Src="~/Controls/SelectorsPanel.ascx" TagName="SelectorsPanel" TagPrefix="gris" %>
<%@ Register Src="~/Controls/DevPanel.ascx" TagName="DevPanel" TagPrefix="gris" %>
<%@ Register Src="Controls/NavigationBar.ascx" TagName="NavigationBar" TagPrefix="gris" %>
<%@ Register TagPrefix="gris" TagName="LayeredIcon" Src="~/Controls/LayeredIcon.ascx" %>
<%@ Register TagPrefix="gris" TagName="VirtualObjectHeader" Src="~/Controls/VirtualObjectHeader.ascx" %>

<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $('#divGroupsAndFormulas').dialog({
                autoOpen: false,
                width: 900,
                height: 600,
                resizable: false,
                modal: true,
                dialogClass: 'groupsAndFormulasBox'
            });
            $('#dialog_link').click(function () {
                $('#divGroupsAndFormulas').dialog('open');
                $('#ifrGroupsAndFormulas').attr("Src", "GroupsAndFormulas.aspx?nodid=<%=this.NodID %>&regid=<%=this.RegID %>");
                return false;
            });
        });
        function triggerRefresh() {
            __doPostBack($("input[id$='imgbRefreshPage']").attr('name'), '');
        }
        function pageLoad() {
            $("div[id$='vowc']").tooltip({
                effect: 'slide',
                position: "bottom center"
            });
            $("a[id$='lnkMenuItemGroupsAndFormulas']").click(function () {
                $('#divGroupsAndFormulas').dialog('open');
                $('#ifrGroupsAndFormulas').attr("Src", "GroupsAndFormulas.aspx?nodid=<%=this.NodID %>&regid=<%=this.RegID %>");
            });
        }

    </script>
    <asp:Literal ID="litWaiting" runat="server" EnableViewState="false"></asp:Literal>
    <asp:Timer ID="tmrNode" runat="server" Enabled="true" OnTick="tmrNode_Tick">
    </asp:Timer>
    
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; height: 100%">
        <tr align="left">
            <td>
                <asp:UpdatePanel ID="updNavBar" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <gris:NavigationBar ID="navBar" runat="server" OnRefreshPageButtonClick="navBar_OnRefreshPageButtonClick"
                            OnLayoutSwitched="navBar_LayoutSwitched" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="tmrNode" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100%;">
                            <asp:UpdatePanel ID="updSystems" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divSelectedSystem" style="float: right; margin-right: 10px;">
                                        <asp:Label ID="lblSystemDesc" runat="server" Text="" ToolTip="Nome del sistema visualizzato"
                                            ForeColor="White"></asp:Label>
                                    </div>
                                    <div id="divSystems" style="width: 100%;">
                                        <gris:SelectorsPanel ID="SystemSelector" runat="server" OnSelectorClick="SystemSelector_SelectorSystemClicked" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="navBar" EventName="LayoutSwitched" />
                                    <asp:AsyncPostBackTrigger ControlID="tmrNode" EventName="Tick" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <asp:Panel ID="pnlGroupsAndFormulas" runat="server" Visible="False">
                                <div id="divGroupsAndFormulas" title="Raggruppamenti e Regole" style="display: none;">
                                    <iframe id="ifrGroupsAndFormulas" width="870" height="543" frameborder="0"></iframe>
                                </div>
                            </asp:Panel>
                            <asp:UpdatePanel ID="updDevices" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:MultiView ID="StazioneMultiView" runat="server">
                                        <asp:View ID="GraphicalView" runat="server">
                                            <div>
                                                <asp:DataList ID="dlstDevPanel" runat="server" DataSourceID="odsDevices" CellPadding="0"
                                                    RepeatColumns="5" RepeatDirection="Horizontal" ShowFooter="False" ShowHeader="False"
                                                    ItemStyle-VerticalAlign="Top" OnItemDataBound="dlstDevPanel_ItemDataBound">
                                                    <ItemTemplate>
                                                        <gris:DevPanel ID="devPnl" runat="server" DevID='<%# Eval("DevID") %>' Host='<%# Eval("STLC1000") %>'
                                                            Name='<%# Eval("Name") %>' Type='<%# Eval("Type") %>' RawType='<%# Eval("RawType") %>'
                                                            Building='<%# Eval("BuildingName") %>' Rack='<%# Eval("RackName") %>' BuildingDescription='<%# Eval("BuildingDescription") %>'
                                                            RackDescription='<%# Eval("RackDescription") %>' SeverityLevel='<%# Eval("SevLevel") %>'
                                                            SeverityLevelDescription='<%# Eval("SevLevelDescription") %>' SeverityLevelDetail='<%# Eval("SevLevelDetail") %>'
                                                            FullHostName='<%# Eval("STLC1000FullHostName") %>' WSUrlPattern='<%# Eval("WSUrlPattern") %>'
                                                            Vendor='<%# Eval("VendorName") %>' SystemID='<%# Eval("SystemID") %>' Ticket='<%# Eval("Ticket") %>'
                                                            OperatorMaintenance='<%# Eval("OperatorMaintenance") %>' RackPositionRow='<%# Eval("RackPositionRow") %>'
                                                            RackPositionCol='<%# Eval("RackPositionCol") %>' DeviceImage='<%# Eval("DeviceImage") %>'
                                                            VirtualObjectName='<%# Eval("DeviceVirtualObjectName") %>' VirtualObjectDescription='<%# Eval("DeviceVirtualObjectDescription") %>'
                                                            OnChangeMaintenance="OnChangeMaintenance" OnDevPanelClick="DevPanel_Click" IsComputedByCustomFormula='<%# byte.Parse(Eval("DeviceVirtualObjectIsComputedByCustomFormula").ToString()) %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" />
                                                </asp:DataList>
                                            </div>
                                        </asp:View>
                                        <asp:View ID="TabularView" runat="server">
                                            <asp:Repeater ID="rptVO" runat="server" DataSourceID="SqlDataVOByNode" OnItemDataBound="rptVO_ItemDataBound">
                                                <HeaderTemplate>
                                                    <div style="text-align: left; padding-bottom: 8px;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div id="VOHeader" runat="server" visible="False">
                                                        <div>
                                                            <gris:VirtualObjectHeader runat="server" ID="voh" VirtualObjectName='<%#Eval("VirtualObjectName")%>'
                                                                VirtualObjectDescription='<%#Eval("VirtualObjectDescription")%>' SevLevel='<%#Eval("SevLevel")%>'
                                                                SevLevelDetailId='<%#Eval("SevLevelDetailId")%>' SevLevelDescription='<%#Eval("SevLevelDescription")%>'
                                                                IsComputedByCustomFormula='<%#Eval("IsObjectStatusIdComputedByCustomFormula")%>' />
                                                        </div>
                                                    </div>
                                                    <div id="VOGrid" runat="server">
                                                        <asp:GridView ID="gvwDevice" runat="server" Width="1180px" AutoGenerateColumns="False"
                                                            DataKeyNames="DevID" SkinID="SWMC4" OnRowDataBound="gvwDevice_RowDataBound" OnRowCommand="gvwDevice_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Wrap="false">
                                                                    <ItemTemplate>
                                                                        <gris:LayeredIcon ID="imgStatus" runat="server" LayeredIconType="VistaTabellare"
                                                                            AdditionalStyle="float:left" />
                                                                        <asp:ImageButton ID="imgbOperatorMaintenance" runat="server" ToolTip="Non attivo"
                                                                            AlternateText="Non attivo" CommandName="OperatorMaintenance" Style="float: left;" />
                                                                        <asp:HiddenField ID="hidDevId" runat="server" Value='<%# Bind("DevId") %>' />
                                                                        <asp:HiddenField ID="hidOperatorMaintenance" runat="server" Value='<%# Bind("OperatorMaintenance") %>' />
                                                                    </ItemTemplate>
                                                                    <ControlStyle CssClass="GridViewTemplate" />
                                                                    <ItemStyle Width="68px" BackColor="#4d4d4d" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="ImageSep2" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                                    </ItemTemplate>
                                                                    <ControlStyle CssClass="GridViewTemplate" />
                                                                    <ItemStyle Width="2px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Periferica" SortExpression="Periferica">
                                                                    <ItemStyle CssClass="GridViewLeftPadding" Width="338px" />
                                                                    <HeaderStyle CssClass="GridViewLeftPadding" />
                                                                    <ItemTemplate>
                                                                        <asp:Panel ID="pnlNebbiaPerif" runat="server" EnableViewState="false">
                                                                        </asp:Panel>
                                                                        <asp:LinkButton ID="lnkbPerif" runat="server" Text='<%# Bind("Name") %>' CommandName="DevInfo"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="ImageSep3" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                                    </ItemTemplate>
                                                                    <ControlStyle CssClass="GridViewTemplate" />
                                                                    <ItemStyle Width="2px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Descrizione" SortExpression="Descrizione">
                                                                    <ItemStyle CssClass="GridViewLeftPadding" Width="416px" />
                                                                    <HeaderStyle CssClass="GridViewLeftPadding" />
                                                                    <ItemTemplate>
                                                                        <asp:Panel ID="pnlNebbiaDesc" runat="server" EnableViewState="false">
                                                                        </asp:Panel>
                                                                        <asp:Label ID="lblSevLevelDescription" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="ImageSep4" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                                    </ItemTemplate>
                                                                    <ControlStyle CssClass="GridViewTemplate" />
                                                                    <ItemStyle Width="2px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Tipo" SortExpression="Tipo">
                                                                    <ItemStyle CssClass="GridViewLeftPadding" Width="120px" />
                                                                    <HeaderStyle CssClass="GridViewLeftPadding" />
                                                                    <ItemTemplate>
                                                                        <asp:Panel ID="pnlNebbiaTipo" runat="server" EnableViewState="false">
                                                                        </asp:Panel>
                                                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="ImageSep6" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                                    </ItemTemplate>
                                                                    <ControlStyle CssClass="GridViewTemplate" />
                                                                    <ItemStyle Width="2px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Indirizzo">
                                                                    <ItemStyle CssClass="GridViewLeftPadding" Width="150px" />
                                                                    <HeaderStyle CssClass="GridViewLeftPadding" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAddress" runat="server" Style="font-size: 9pt;"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="ImageSep5" runat="server" ImageUrl="IMG/Interfaccia/tab_sep.gif" />
                                                                    </ItemTemplate>
                                                                    <ControlStyle CssClass="GridViewTemplate" />
                                                                    <ItemStyle Width="2px" BackColor="#4d4d4d" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbScreenDump" runat="server" CausesValidation="false" ImageUrl="~/IMG/Interfaccia/screendump.gif" />
                                                                        <asp:ImageButton ID="imgbListenDump" runat="server" CausesValidation="false" ImageUrl="~/IMG/Interfaccia/arm_play_tab.gif" />
                                                                        <asp:ImageButton ID="imgbCommandDump" runat="server" CausesValidation="false" ImageUrl="~/IMG/Interfaccia/perif_command_tab.gif" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" BackColor="#4d4d4d" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="imgLastStatusEnabled" runat="server" causesvalidation="false" Visible="false"
                                                                            ImageUrl="~/IMG/Interfaccia/tab_laststatus.gif" />
                                                                        <asp:Image ID="imgTickets" runat="server" causesvalidation="false" Visible="false"
                                                                            ImageUrl="~/IMG/Interfaccia/hastickets.gif" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" BackColor="#4d4d4d" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">
                                                                    <span style="font-size: 16pt; color: #ffffff; font-family: Arial;"><strong>Informazioni
                                                                        non Disponibili</strong> </span>
                                                                </div>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </ItemTemplate>
                                                <SeparatorTemplate>
                                                    <div style="height: 7px; display: block;">
                                                    </div>
                                                </SeparatorTemplate>
                                                <FooterTemplate>
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </asp:View>
                                    </asp:MultiView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="navBar" EventName="LayoutSwitched" />
                                    <asp:AsyncPostBackTrigger ControlID="tmrNode" EventName="Tick" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="odsDevices" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.DevicesDSTableAdapters.devicesTableAdapter"
                    OnSelecting="odsDevices_Selecting" OnSelected="odsDevices_Selected">
                    <SelectParameters>
                        <asp:Parameter Name="NodID" Type="Int64" />
                        <asp:Parameter Name="SystemID" Type="Int32" />
                        <asp:Parameter Name="VirtualObjectID" Type="Int64" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div id="STLCInfoDiv">
                    <asp:UpdatePanel ID="updSTLCInfo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DataList ID="dlstSTLCInfo" runat="server" DataKeyField="SrvID" DataSourceID="SqlDataSTLCInfo"
                                Width="100%" OnItemDataBound="dlstSTLCInfo_ItemDataBound">
                                <ItemTemplate>
                                    <gris:STLCInfo ID="STLCInfo1" runat="server" SrvID='<%# Eval("SrvID") %>' FullHostName='<%# Eval("FullHostName") %>'
                                        IP='<%# Eval("IP") %>' LastUpdate='<%# Eval("LastUpdate") %>' LastMessageType='<%# Eval("LastMessageType") %>'
                                        NodID='<%# Eval("NodID") %>' InMaintenance='<%# Eval("InMaintenance") %>' SevLevel='<%# Eval("SevLevel") %>'
                                        SevLevelDesc='<%# Eval("SevLevelDesc") %>' SevLevelReal='<%# Eval("SevLevelReal") %>'
                                        SevLevelDetailId='<%# Eval("SevLevelDetailId") %>' SevLevelDetailDesc='<%# Eval("SevLevelDetailDesc") %>'
                                        OnSyncComplete="STLCInfo1_OnSyncComplete" OnServerDelete="STLCInfo1_OnServerDelete"
                                        OnChangeMaintenance="OnChangeMaintenance" />
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="tmrNode" EventName="Tick" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:SqlDataSource ID="SqlDataSTLCInfo" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                        SelectCommand="gris_GetSTLCInfobyNodIDAndDevID" SelectCommandType="StoredProcedure"
                        OnSelecting="SqlDataSTLCInfo_Selecting" OnSelected="SqlDataSTLCInfo_Selected">
                        <SelectParameters>
                            <asp:Parameter Name="NodID" Type="Int64" />
                            <asp:Parameter DefaultValue="-1" Name="DevID" Type="Int64" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataVOByNode" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
                        SelectCommand="gris_GetVirtualObjectsByNodIDAndSystemID" SelectCommandType="StoredProcedure"
                        OnSelecting="SqlDataVOByNode_Selecting">
                        <SelectParameters>
                            <asp:Parameter Name="NodID" Type="Int64" />
                            <asp:Parameter Name="SystemID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

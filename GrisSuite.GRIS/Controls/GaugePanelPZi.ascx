﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GaugePanelPZi.ascx.cs"
    Inherits="Controls_GaugePanelPZi" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 290px; height: 154px;
    background-color: #3f3f3f;">
    <tr>
        <td colspan="2" style="background-image: url(IMG/interfaccia/gauge_top_290.gif);
            background-repeat: no-repeat; height: 12px">
        </td>
    </tr>
    <tr>
        <td style="width: 245px; height: 130px;">
            <div class="GaugeText">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;
                    text-align: left;">
                    <tr>
                        <td>
                            <asp:Label ID="lblHeader" runat="server" CssClass="GaugeHeader"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblValueLabel" EnableViewState="false">Valore:</asp:Label>
                            <asp:Label ID="lblValue" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trRif" runat="server">
                        <td>
                            <asp:Label runat="server" ID="lblReferenceLabel" EnableViewState="false">Rif:</asp:Label>
                            <asp:Label ID="lblReference" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trWarn" runat="server">
                        <td>
                            <asp:Label runat="server" ID="lblWarnLabel" EnableViewState="false">Soglia Attenzione:</asp:Label>
                            <asp:Label ID="lblWarn" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trErr" runat="server">
                        <td>
                            Soglia Errore:
                            <asp:Label ID="lblErr" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDesc" runat="server" CssClass="GaugeDesc"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="width: 45px; height: 130px; padding-right: 5px">
            <div id="divGauge" class="gauge" runat="server" style="width: 40px; height: 130px;
                background-image: url(IMG/interfaccia/gauge_bg.gif);">
                <asp:UpdatePanel ID="updGauge" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="Update">
                            <div id="divUpRed" class="band upRed" runat="server">
                                <div style="height: 100%;">
                                </div>
                            </div>
                            <div id="divUpYellow" class="band upYellow" runat="server">
                                <div style="height: 100%;">
                                </div>
                            </div>
                            <div id="divUpGreen" class="band upGreen" runat="server">
                                <div style="height: 100%;">
                                </div>
                            </div>
                            <div id="divDownGreen" class="band downGreen" runat="server">
                                <div style="height: 100%;">
                                </div>
                            </div>
                            <div id="divDownYellow" class="band downYellow" runat="server">
                                <div style="height: 100%;">
                                </div>
                            </div>
                            <div id="divDownRed" class="band downRed" runat="server">
                                <div style="height: 100%;">
                                </div>
                            </div>
                            <img id="imgArrow" src="~/IMG/Interfaccia/gauge_arrow.gif" alt="actual value" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-image: url(IMG/interfaccia/gauge_bot_290.gif);
            background-repeat: no-repeat; height: 12px">
        </td>
    </tr>
</table>

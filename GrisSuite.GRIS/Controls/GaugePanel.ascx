<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GaugePanel.ascx.cs" Inherits="GaugePanel" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 290px; height: 154px;
    background-color: #3f3f3f;">
    <tr>
        <td colspan="2" style="background-image: url(IMG/interfaccia/gauge_top_290.gif); background-repeat: no-repeat;
            height: 12px">
        </td>
    </tr>
    <tr>
        <td style="width: 245px; height: 130px;">
            <div class="GaugeText">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;
                    text-align: left;">
                    <tr>
                        <td>
                            <asp:Label ID="lblHeader" runat="server" CssClass="GaugeHeader"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Valore:
                            <asp:Label ID="lblValore" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trRif" runat="server">
                        <td>
                            Rif:
                            <asp:Label ID="lblValoreRif" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trDelta" runat="server">
                        <td>
                            Delta:
                            <asp:Label ID="lblDelta" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trMax" runat="server" visible="false">
                        <td>
                            Max:
                            <asp:Label ID="lblValoreMax" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trMin" runat="server" visible="false">
                        <td>
                            Min:
                            <asp:Label ID="lblValoreMin" runat="server" CssClass="GaugeLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 4px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDesc" runat="server" Text="Label" CssClass="GaugeDesc"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="width: 45px; height: 130px; padding-right: 5px">
            <div id="divGaugeBg" runat="server" style="width: 40px; height: 130px; position: relative;
                background-image: url(IMG/interfaccia/gauge_c_bg.gif);">
                <img src="~/IMG/interfaccia/gauge_arrow.gif" alt="" style="position: absolute; left: 2px;
                    top: 0px;" id="freccia" runat="server" />
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-image: url(IMG/interfaccia/gauge_bot_290.gif); background-repeat: no-repeat;
            height: 12px">
        </td>
    </tr>
</table>

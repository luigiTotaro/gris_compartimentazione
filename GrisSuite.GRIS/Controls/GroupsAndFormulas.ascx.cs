﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using FormulaEngine;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ObjectFormulasCustomColorsDSTableAdapters;
using GrisSuite.Data.Gris.ObjectFormulasForObjectStatusDSTableAdapters;
using GrisSuite.Data.Gris.StatusDSTableAdapters;
using GrisSuite.Data.Gris.SystemsInfoDSTableAdapters;
using GrisSuite.Data.Gris.VirtualObjectDSTableAdapters;
using GrisSuite.Data.Gris.VirtualObjectRulesDSTableAdapters;

public partial class Controls_GroupsAndFormulas : UserControl
{
	private const byte FORMULA_INDEX_DEFAULT = 0;
	private const byte FORMULA_INDEX_CUSTOM = 1;
	private const byte FORMULA_INDEX_COLORS = 2;

	private List<string> _excludedSystems = new List<string>{};

	private ObjectFormulasCustomColorsDS.ObjectFormulaCustomColorsForObjectStatusIdDataTable _customColors =
		new ObjectFormulasCustomColorsDS.ObjectFormulaCustomColorsForObjectStatusIdDataTable();

	private readonly Dictionary<int, string> _systemSeverities = new Dictionary<int, string>();
	private FormulaEngineFormulas _formulasData;

	public long NodID
	{
		get { return Convert.ToInt64(this.ViewState["NodID"] ?? -1); }
		set { this.ViewState["NodID"] = value; }
	}

	public long RegID
	{
		get { return Convert.ToInt64(this.ViewState["RegID"] ?? -1); }
		set { this.ViewState["RegID"] = value; }
	}

	public long PageSelectedNodeSystemID
	{
		get { return Convert.ToInt32(this.ViewState["PageSelectedNodeSystemID"] ?? (long) -1); }
		set
		{
			this.ViewState["PageSelectedNodeSystemID"] = value;
			this.SetSystemId(value);
		}
	}

	private Dictionary<long, int> SystemIdMap
	{
		get { return (Dictionary<long, int>) ( this.ViewState["SystemIdMap"] ?? new Dictionary<long, int>(0) ); }
		set { this.ViewState["SystemIdMap"] = value; }
	}

	private List<string> SystemGroups
	{
		get { return (List<string>) ( this.ViewState["SystemGroups"] ?? new List<string>(0) ); }
		set { this.ViewState["SystemGroups"] = value; }
	}

	protected void Page_Load ( object sender, EventArgs e )
	{
		if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ExcludedSystemsOnFormulasManagement"]))
		{
			this._excludedSystems = new List<string>(ConfigurationManager.AppSettings["ExcludedSystemsOnFormulasManagement"].Split(','));
		}
		else
		{
			this._excludedSystems = new List<string> {"Altre Periferiche", "Sistema Illuminazione", "Sistema Telefonia", "Sistema FDS", "Monitoraggio VPN Verde", "Web Radio"};
		}

		this.ddlSystem.DataBind();

		#region Permessi di visualizzazione

		bool areGroupsVisible = ( GrisPermissions.IsUserInAdminGroup() ||
								GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup((RegCodes) this.RegID) ||
								GrisPermissions.IsUserInLocalAdminGroup((RegCodes) this.RegID) );

		bool areRulesVisible =	( GrisPermissions.IsUserInAdminGroup() ||
								GrisPermissions.IsUserInLocalAdminGroup((RegCodes) this.RegID) );

		this.tabGroups.Visible = this.pnlTabContent1.Visible = areGroupsVisible;
		this.tabRuels.Visible = this.pnlTabContent2.Visible = areRulesVisible;

		#endregion

		if ( !this.IsPostBack )
		{
			this.SystemGroups = new List<string>();
			this.LoadCreatableDefaultGroups();
		}

		this.LoadSeveritiesForSystems();

		this._formulasData = new FormulaEngineFormulas(this.Context, this.MapPath("~/FormulaEngineFormulas.config"));
	}

	private void LoadSeveritiesForSystems ()
	{
		SeverityTableAdapter ta = new SeverityTableAdapter();
		var dt = ta.GetData();

		foreach ( StatusDS.SeverityRow severityRow in dt.Where(s => s.IsRelevantToCustomColors) )
		{
			this._systemSeverities.Add(severityRow.SevLevel, severityRow.Description);
		}
	}

	protected void Page_PreRender ( object sender, EventArgs e )
	{
		this.BuildNodeSystemsVirtualObjectsTree(false);
	}

	private void BuildNodeSystemsVirtualObjectsTree ( bool forceRebind )
	{
		if ( ( this.tvwNode.Nodes.Count == 0 ) || ( forceRebind ) )
		{
			VirtualObjectsTreeByNodeTableAdapter ta = new VirtualObjectsTreeByNodeTableAdapter();
			var treeData = ta.GetData(this.NodID);

			this.tvwNode.Nodes.Clear();

			if ( ( treeData != null ) && ( treeData.Rows.Count > 0 ) )
			{
				VirtualObjectRulesDS.VirtualObjectsTreeByNodeRow firstRow = (VirtualObjectRulesDS.VirtualObjectsTreeByNodeRow) ( treeData.Rows[0] );
				TreeNode nodoStazione =
					new TreeNode(( ( firstRow.Name.Length > 33 ) ? string.Format("{0}...", firstRow.Name.Substring(0, 32)) : firstRow.Name ),
								 firstRow.ObjectStatusIdNode.ToString());
				this.tvwNode.Nodes.Add(nodoStazione);

				int currentSystemId = 0;
				Guid currentObjectStatusIdVirtualObject = Guid.Empty;
				TreeNode nodoSistema = null;
				foreach ( VirtualObjectRulesDS.VirtualObjectsTreeByNodeRow row in treeData.Rows )
				{
					if ( currentSystemId != row.SystemID )
					{
						nodoSistema = new TreeNode(row.SystemDescription, row.ObjectStatusIdNodeSystem.ToString());
						nodoStazione.ChildNodes.Add(nodoSistema);
					}

					if ( nodoSistema != null )
					{
						if ( ( !row.IsObjectStatusIdVirtualObjectNull() ) && ( currentObjectStatusIdVirtualObject != row.ObjectStatusIdVirtualObject ) )
						{
							// Il gruppo dei generici non è editabile
							if ( !string.Equals(row.VirtualObjectName, "generici", StringComparison.OrdinalIgnoreCase) )
							{
								TreeNode nodoVirtualObject = new TreeNode(row.VirtualObjectName, row.ObjectStatusIdVirtualObject.ToString());
								nodoSistema.ChildNodes.Add(nodoVirtualObject);
							}
						}
					}

					currentObjectStatusIdVirtualObject = row.IsObjectStatusIdVirtualObjectNull() ? Guid.Empty : row.ObjectStatusIdVirtualObject;

					currentSystemId = row.SystemID;
				}

				this.tvwNode.ExpandAll();
				nodoStazione.Select();
				this.BindDataOnCurrentTreeNode();
			}

			this.updRulesTree.Update();
			this.updRulesDetail.Update();
		}
	}

	protected void tvwNode_SelectedNodeChanged ( object sender, EventArgs e )
	{
		this.BindDataOnCurrentTreeNode();
	}

	private void BindDataOnCurrentTreeNode ()
	{
		if ( this.tvwNode.SelectedNode != null )
		{
			Guid objectStatusId = new Guid(this.tvwNode.SelectedValue);
			switch ( this.tvwNode.SelectedNode.Depth )
			{
				case 0:
					// Livello stazione
					this.pnlCustomColors.Visible = this.CanSetColors();
					this.btnAddColor.Visible = this.CanSetColors();
					this.BindFormulasCombo(this.ddlCustomFormula, this.tvwNode.SelectedNode.Depth);
					this.BindFormulasCombo(this.ddlDefaultFormula, this.tvwNode.SelectedNode.Depth);
					this.BindRulesDetailPanel(objectStatusId);
					this.BindCustomColorGridForNodeFromDB(objectStatusId);
					break;
				case 1:
					// Livello sistema
					this.pnlCustomColors.Visible = false;
					this.btnAddColor.Visible = false;
					this.BindFormulasCombo(this.ddlCustomFormula, this.tvwNode.SelectedNode.Depth);
					this.BindFormulasCombo(this.ddlDefaultFormula, this.tvwNode.SelectedNode.Depth);
					this.BindRulesDetailPanel(objectStatusId);
					break;
				case 2:
					// Livello gruppo
					this.pnlCustomColors.Visible = false;
					this.btnAddColor.Visible = false;
					this.BindFormulasCombo(this.ddlCustomFormula, this.tvwNode.SelectedNode.Depth);
					this.BindFormulasCombo(this.ddlDefaultFormula, this.tvwNode.SelectedNode.Depth);
					this.BindRulesDetailPanel(objectStatusId);
					break;
			}

			this.updRulesDetail.Update();
		}
	}

	private void BindRulesDetailPanel ( Guid objectStatusId )
	{
		ObjectFormulasForObjectStatusIdTableAdapter ta = new ObjectFormulasForObjectStatusIdTableAdapter();
		var formulasData = ta.GetData(objectStatusId);

		if ( ( formulasData != null ) && ( formulasData.Rows.Count > 0 ) )
		{
			if ( this.ddlDefaultFormula.Items.ContainsValue(formulasData[0].ScriptPath0) )
			{
				this.ddlDefaultFormula.SelectedValue = formulasData[0].ScriptPath0;
			}

			if ( this.ddlCustomFormula.Items.ContainsValue(formulasData[0].ScriptPath1) )
			{
				this.ddlCustomFormula.SelectedValue = formulasData[0].ScriptPath1;
			}
		}
	}

	private void BindFormulasCombo ( DropDownList combo, int type )
	{
		combo.Items.Clear();

		if ( combo.ID.IndexOf("default", StringComparison.OrdinalIgnoreCase) >= 0 )
		{
			switch ( type )
			{
				case 0:
					// Livello stazione
					foreach ( var item in this._formulasData.GetFormulas("FormulasListForNodeDefault") )
					{
						combo.Items.Add(new ListItem(item.Value, item.Key));
					}
					break;
				case 1:
					// Livello sistema
					foreach ( var item in this._formulasData.GetFormulas("FormulasListForNodeSystemDefault") )
					{
						combo.Items.Add(new ListItem(item.Value, item.Key));
					}
					break;
				case 2:
					// Livello gruppo
					foreach ( var item in this._formulasData.GetFormulas("FormulasListForNodeSystemVirtualObjectDefault") )
					{
						combo.Items.Add(new ListItem(item.Value, item.Key));
					}
					break;
			}
		}
		if ( combo.ID.IndexOf("custom", StringComparison.OrdinalIgnoreCase) >= 0 )
		{
			switch ( type )
			{
				case 0:
					// Livello stazione
					foreach ( var item in this._formulasData.GetFormulas("FormulasListForNodeCustom") )
					{
						combo.Items.Add(new ListItem(item.Value, item.Key));
					}
					break;
				case 1:
					// Livello sistema
					foreach ( var item in this._formulasData.GetFormulas("FormulasListForNodeSystemCustom") )
					{
						combo.Items.Add(new ListItem(item.Value, item.Key));
					}
					break;
				case 2:
					// Livello gruppo
					foreach ( var item in this._formulasData.GetFormulas("FormulasListForNodeSystemVirtualObjectCustom") )
					{
						combo.Items.Add(new ListItem(item.Value, item.Key));
					}
					break;
			}
		}
	}

	private void BindCustomColorGridForNodeFromDB ( Guid objectStatusId )
	{
		ObjectFormulaCustomColorsForObjectStatusIdTableAdapter ta = new ObjectFormulaCustomColorsForObjectStatusIdTableAdapter();
		this._customColors = ta.GetData(objectStatusId, FORMULA_INDEX_COLORS);

		this.ViewState["CustomColors"] = this._customColors;

		this.lvwVoCustomColors.DataSource = this._customColors;
		this.lvwVoCustomColors.DataBind();
	}

	private void BindCustomColorGridForNode ()
	{
		this.ReloadCustomColorsState();

		this.lvwVoCustomColors.DataSource = this._customColors;
		this.lvwVoCustomColors.DataBind();
	}

	protected void odsChildrenVirtualObjects_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
	{
		long nodeSysId;
		if ( !long.TryParse(this.ddlSystem.SelectedValue, out nodeSysId) )
		{
			nodeSysId = -1;
		}
		e.InputParameters["ObjectId"] = nodeSysId;
	}

	protected void ddlSystem_DataBound ( object sender, EventArgs e )
	{
		// vengono filtrati i systemi che non devono partecipare al calcolo degli stati
		// sono stati riutilizzati i comandi esistenti in Gris quindi il filtro non viene gestito in maniera centralizzata
		for ( int index = 0; index < this.ddlSystem.Items.Count; index++ )
		{
			ListItem item = this.ddlSystem.Items[index];
			if ( this._excludedSystems.IndexOf(item.Text) > -1 )
			{
				this.ddlSystem.Items.Remove(item);
			}
		}
	}

	private bool CanSetColors ()
	{
		return this.ddlSystem.Items.ContainsText("Diffusione sonora") &&
			   this.ddlSystem.Items.ContainsText("Informazione visiva");
	}

	protected void ddlSystem_SelectedIndexChanged ( object sender, EventArgs e )
	{
		this.BindTabs();
		this.updGroups.Update();
		this.BuildNodeSystemsVirtualObjectsTree(true);
		this.updRules.Update();
	}

	protected void rptTabBodies_DataBinding ( object sender, EventArgs e )
	{
		this.SystemGroups.Clear();
	}

	protected void rptTabBodies_ItemDataBound ( object sender, RepeaterItemEventArgs e )
	{
		var ddlMoveToGroup = e.Item.FindControl("ddlMoveToGroup") as DropDownList;
		var btnMoveToGroup = e.Item.FindControl("btnMoveToGroup") as Button;
		var lvwVoDevices = e.Item.FindControl("lvwVoDevices") as ListView;
		var group = e.Item.DataItem as DataRowView;

		if ( group != null )
		{
			if ( lvwVoDevices != null )
			{
				VirtualObjectDevicesTableAdapter ta = new VirtualObjectDevicesTableAdapter();
				var devices = ta.GetData(int.Parse(group["VirtualObjectID"].ToString()));
				lvwVoDevices.DataSource = devices;
				lvwVoDevices.DataBind();
			}

			if ( ddlMoveToGroup != null && btnMoveToGroup != null )
			{
				string groupName = group["VirtualObjectName"].ToString();
				this.RemoveDropDownListItemByText(groupName, ddlMoveToGroup);
				this.RemoveDropDownListItemByText(groupName, this.ddlCreateGroup); // rimosso il gruppo visualizzato da quelli creabili		

				// un gruppo solo, non posso spostare
				bool enableMove = ( ddlMoveToGroup.Items.Count > 0 && lvwVoDevices != null && lvwVoDevices.Items.Count > 0 );
				ddlMoveToGroup.Enabled = enableMove;
				btnMoveToGroup.Enabled = enableMove;
			}

			this.SystemGroups.Add(group["VirtualObjectName"].ToString());
		}

		this.ddlCreateGroup.Enabled = ( this.ddlCreateGroup.Items.Count > 0 );
		this.btnCreateGroup.Enabled = ( this.ddlCreateGroup.Items.Count > 0 );
	}

	protected void rptTabHeaders_ItemDataBound ( object sender, RepeaterItemEventArgs e )
	{
		if ( ( e.Item.ItemType == ListItemType.Item ) || ( e.Item.ItemType == ListItemType.AlternatingItem ) )
		{
			LinkButton lnkRemoveGroup = e.Item.FindControl("lnkRemoveGroup") as LinkButton;
			ModalPopupExtender mdlpopexDelete = e.Item.FindControl("mdlpopexDelete") as ModalPopupExtender;

			if ( ( lnkRemoveGroup != null ) && ( mdlpopexDelete != null ) )
			{
				lnkRemoveGroup.OnClientClick = "showConfirm(this, 'mdlDeletePopup" + e.Item.ItemIndex + "');return false;";
				mdlpopexDelete.BehaviorID = "mdlDeletePopup" + e.Item.ItemIndex;
			}
		}
	}

	private void SetSystemId ( long nodeSystemId )
	{
		if ( this.ddlSystem.Items.Count == 0 )
		{
			this.BindSystems();
		}

		if ( this.ddlSystem.Items.ContainsValue(nodeSystemId.ToString()) )
		{
			this.ddlSystem.SelectedValue = nodeSystemId.ToString();
		}
	}

	private int GetSystemId ()
	{
		int nodeSysId;
		if ( !int.TryParse(this.ddlSystem.SelectedValue, out nodeSysId) )
		{
			nodeSysId = -1;
		}

		if ( this.SystemIdMap.Count > 0 && this.SystemIdMap.ContainsKey(nodeSysId) )
		{
			return this.SystemIdMap[nodeSysId];
		}
		return -1;
	}

	private void RemoveDropDownListItemByText ( string text, DropDownList ddl )
	{
		for ( int index = 0; index < ddl.Items.Count; index++ )
		{
			if ( ddl.Items[index].Text == text )
			{
				ddl.Items.Remove(ddl.Items[index]);
			}
		}
	}

	private void BindSystems ()
	{
		SystemTableAdapter ta = new SystemTableAdapter();
		var systems = ta.GetData(this.NodID, null);
		this.ddlSystem.DataSource = systems;
		this.ddlSystem.DataBind();

		var sysMap = new Dictionary<long, int>(10);
		foreach ( var system in systems )
		{
			sysMap.Add(system.NodeSystemsId, system.SystemID);
		}
		this.SystemIdMap = sysMap;
	}

	private void LoadCreatableDefaultGroups ()
	{
		this.ddlCreateGroup.Enabled = true;
		this.ddlCreateGroup.Items.Clear();
		this.ddlCreateGroup.Items.Add(new ListItem("Fondamentali", "Fondamentali"));
		this.ddlCreateGroup.Items.Add(new ListItem("Importanti", "Importanti"));
		this.ddlCreateGroup.Items.Add(new ListItem("Complementari", "Complementari"));
	}

	private void BindTabs ()
	{
		this.LoadCreatableDefaultGroups();
		this.rptTabBodies.DataBind();
		this.rptTabHeaders.DataBind();
	}

	protected void btnCreateGroup_Click ( object sender, EventArgs e )
	{
		if ( this.ddlCreateGroup.Items.Count > 0 && !string.IsNullOrEmpty(this.ddlCreateGroup.SelectedValue) )
		{
			ChildrenVirtualObjectTableAdapter ta = new ChildrenVirtualObjectTableAdapter();

			if ( this.SystemGroups.Count == 0 )
			{
				// contestualmente alla creazione del primo Raggruppamento, devo creare anche il raggruppamento di default "Dispositivi Generici"
				ta.InsGenericiFormulaGroup(this.GetSystemId(), this.NodID);
			}

			switch ( this.ddlCreateGroup.SelectedValue )
			{
				case "Fondamentali":
					ta.InsFondamentaliFormulaGroup(this.GetSystemId(), this.NodID);
					break;
				case "Importanti":
					ta.InsImportantiFormulaGroup(this.GetSystemId(), this.NodID);
					break;
				case "Complementari":
					ta.InsComplementariFormulaGroup(this.GetSystemId(), this.NodID);
					break;
			}
		}

		this.BindTabs();
		this.updGroups.Update();
		this.BuildNodeSystemsVirtualObjectsTree(true);
		this.updRules.Update();
	}

	protected void rptTabHeaders_ItemCommand ( object source, RepeaterCommandEventArgs e )
	{
		if ( e.CommandName == "Rimuovi" )
		{
			ChildrenVirtualObjectTableAdapter ta = new ChildrenVirtualObjectTableAdapter();

			switch ( e.CommandArgument.ToString() )
			{
				case "Fondamentali":
					ta.DelFondamentaliFormulaGroup(this.GetSystemId(), this.NodID);
					break;
				case "Importanti":
					ta.DelImportantiFormulaGroup(this.GetSystemId(), this.NodID);
					break;
				case "Complementari":
					ta.DelComplementariFormulaGroup(this.GetSystemId(), this.NodID);
					break;
			}

			if ( this.SystemGroups.Count == 2 ) // ultimi 2 raggruppamenti rimasti (contatore aggiornato solo dopo il rebind)
			{
				// contestualmente alla cancellazione dell'ultimo Raggruppamento, devo cancellare anche il raggruppamento di default "Dispositivi Generici"
				ta.DelGenericiFormulaGroup(this.GetSystemId(), this.NodID);
			}

			this.BindTabs();
			this.updGroups.Update();
			this.BuildNodeSystemsVirtualObjectsTree(true);
			this.updRules.Update();
		}
	}

	protected void rptTabBodies_ItemCommand ( object source, RepeaterCommandEventArgs e )
	{
		if ( e.CommandName == "Sposta" )
		{
			long groupId;
			var ddlMoveToGroup = e.Item.FindControl("ddlMoveToGroup") as DropDownList;
			var lvwVoDevices = e.Item.FindControl("lvwVoDevices") as ListView;
			if ( lvwVoDevices != null && ddlMoveToGroup != null && long.TryParse(ddlMoveToGroup.SelectedValue, out groupId) )
			{
				VirtualObjectDevicesTableAdapter ta = new VirtualObjectDevicesTableAdapter();
				var devIds = this.GetCheckedDevicesIds(lvwVoDevices);
				ta.UpdAssociateToVirtualObject(groupId, string.Format("|{0}|", string.Join("|", devIds.ConvertAll(id => id.ToString()).ToArray())), 6
					/*periferiche*/);

				this.BindTabs();
				this.updGroups.Update();
				this.updRules.Update();
			}
		}
	}

	protected void btnAddColor_Click ( object sender, EventArgs e )
	{
		if ( this.tvwNode.SelectedNode != null )
		{
			Guid objectStatusId = new Guid(this.tvwNode.SelectedValue);
			this.ReloadCustomColorsState();

			this._customColors.AddObjectFormulaCustomColorsForObjectStatusIdRow(objectStatusId, FORMULA_INDEX_COLORS, (int) Status.Ok, (int) Status.Ok,
																				"#ffffff");

			this._customColors.AcceptChanges();

			this.ViewState["CustomColors"] = this._customColors;

			this.BindCustomColorGridForNode();
		}
	}

	protected void btnSave_Click ( object sender, EventArgs e )
	{
		if ( this.tvwNode.SelectedNode != null )
		{
			ObjectFormulasForObjectStatusIdTableAdapter ta = new ObjectFormulasForObjectStatusIdTableAdapter();

			Guid objectStatusId = new Guid(this.tvwNode.SelectedValue);
			this.ReloadCustomColorsState();

			string formulaScriptPathDefault = this.ddlDefaultFormula.SelectedValue;
			string formulaScriptPathCustom = this.ddlCustomFormula.SelectedValue;
			string formulaScriptPathColor = string.Empty;
			bool errorOnCustomColorServiceCall = false;

			if ( this.tvwNode.SelectedNode.Depth == 0 )
			{
				// La formula con il colore custom ha senso solo per la stazione
				List<SystemSeverityCombination> combinations = new List<SystemSeverityCombination>();
				foreach ( ObjectFormulasCustomColorsDS.ObjectFormulaCustomColorsForObjectStatusIdRow colorRow in this._customColors.Rows )
				{
					combinations.Add(new SystemSeverityCombination
									 {
										 Color = ColorTranslator.FromHtml(colorRow.HtmlColor),
										 SystemSeverities =
											 new[]
                                             {
                                                 new SystemSeverityTuple
                                                 {SystemId = Systems.DiffusioneSonora, Severity = colorRow.AudioSystemSevLevel},
                                                 new SystemSeverityTuple
                                                 {SystemId = Systems.InformazioneVisiva, Severity = colorRow.VideoSystemSevLevel}
                                             }
									 });
				}

				FormulaServiceClient formulaService = new FormulaServiceClient();
				try
				{
					string formulaName = formulaService.GenerateNodeColorFormula(objectStatusId, this.tvwNode.SelectedNode.Text,
																				 combinations.ToArray());
					formulaScriptPathColor = string.Format(@".\Lib\ObjectScripts\{0}", formulaName);
				}
				catch ( Exception ex )
				{
					errorOnCustomColorServiceCall = true;
					formulaScriptPathColor = string.Empty;

#if (!DEBUG)
					// Persiste l'eccezione serializzata in base dati
					if ( !Tracer.TraceMessage(this.Context, ex, true) )
					{
						// Logga l'eccezione via trace listener (su disco)
						System.Diagnostics.Trace.TraceError(string.Format("{0}, User -> {1}", ex.Message,
																		  GUtility.GetUserInfos(true)));
					}
#endif
				}
			}

			this.lblError.Visible = false;

			if ( errorOnCustomColorServiceCall )
			{
				this.lblError.Visible = true;
				this.lblError.Text =
					"Errore nel salvataggio dei dati relativi alla configurazione dei colori. Operazione annullata. Ritentare l'operazione.";
			}
			else
			{
				if ( !string.IsNullOrEmpty(formulaScriptPathDefault) )
				{
					if ( ( this.tvwNode.SelectedNode.Depth == 0 ) && ( !string.IsNullOrEmpty(formulaScriptPathColor) ) )
					{
						// Gestione tabella colori solo per la stazione
						ObjectFormulaCustomColorsForObjectStatusIdTableAdapter tac = new ObjectFormulaCustomColorsForObjectStatusIdTableAdapter();
						// Ripuliamo la tabella colori per la formula
						tac.DelAllObjectFormulaCustomColorsForObjectStatusId(objectStatusId, FORMULA_INDEX_COLORS);
						// Inseriamo / aggiorniamo le righe esistenti - nel caso ci siano definite su interfaccia più righe con gli stessi casi
						// ad esempio Sistema Audio Ok e Sistema Video Ok su due righe differenti, vincerà l'ultimo colore impostato, senza validazione o messaggi
						foreach ( ObjectFormulasCustomColorsDS.ObjectFormulaCustomColorsForObjectStatusIdRow colorRow in this._customColors.Rows )
						{
							tac.UpdObjectFormulaCustomColorsForObjectStatusId(objectStatusId, FORMULA_INDEX_COLORS, colorRow.AudioSystemSevLevel,
																			  colorRow.VideoSystemSevLevel, colorRow.HtmlColor);
						}
					}

					ta.UpdObjectFormulasForObjectStatusId(objectStatusId, FORMULA_INDEX_DEFAULT, formulaScriptPathDefault, FORMULA_INDEX_CUSTOM,
														  formulaScriptPathCustom, FORMULA_INDEX_COLORS,
														  ( this._customColors.Count == 0 ? string.Empty : formulaScriptPathColor ), 3, string.Empty, 4,
														  string.Empty);

					this.BindCustomColorGridForNodeFromDB(objectStatusId);
				}
			}
		}
	}

	protected void btnRecalculate_Click ( object sender, EventArgs e )
	{
		FormulaEngineService formulaService = new FormulaEngineService();
		formulaService.EvaluateRegion(this.RegID);
	}

	private List<long> GetCheckedDevicesIds ( ListView lvw )
	{
		if ( lvw != null )
		{
			var ids = new List<long>();
			foreach ( ListViewDataItem item in lvw.Items )
			{
				var chk = item.FindControl("chkIncluded") as CheckBox;
				var key = lvw.DataKeys[item.DataItemIndex];
				if ( chk != null && chk.Checked && key != null )
				{
					ids.Add((long) key.Value);
				}
			}
			return ids;
		}

		return new List<long>(0);
	}

	#region Gestione errori Script Manager Ajax

	protected void smMain_AsyncPostBackError ( object sender, AsyncPostBackErrorEventArgs e )
	{
		string message = "";
		string stackTrace = "";

		if ( e.Exception != null )
		{
			if ( e.Exception.InnerException != null )
			{
				message = e.Exception.InnerException.Message;
				stackTrace = e.Exception.InnerException.StackTrace ?? string.Empty;
			}
			else
			{
				message = e.Exception.Message;
				stackTrace = e.Exception.StackTrace ?? string.Empty;
			}
		}

		string errorMessage = string.Format("Error -> {0}, Stack Trace -> {1}", message, stackTrace);

#if (DEBUG)
		this.smMain.AsyncPostBackErrorMessage = errorMessage;
#else
		if ( e.Exception != null )
		{
			// Persiste l'eccezione serializzata in base dati
			if ( !Tracer.TraceMessage(this.Context, e.Exception, true) )
			{
				// Logga l'eccezione via trace listener (su disco)
				System.Diagnostics.Trace.TraceError(string.Format("{0}, User -> {1}", errorMessage,
																  GUtility.GetUserInfos(true)));
			}
		}

		this.smMain.AsyncPostBackErrorMessage = "Errore nella comunicazione con il server.";
#endif
	}

	#endregion

	protected void lvwVoDevices_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			var dataRow = (VirtualObjectDS.VirtualObjectDevicesRow) ( (DataRowView) ( (ListViewDataItem) e.Item ).DataItem ).Row;
			Label lblAddress = e.Item.FindControl("lblDevAddress") as Label;
			if ( lblAddress != null )
			{
				if ( string.IsNullOrEmpty(dataRow.PortType) )
				{
					lblAddress.Text = string.Format("{0}", dataRow.Addr);
				}
				else
				{
					lblAddress.Text = dataRow.PortType.IndexOf("tcp", StringComparison.OrdinalIgnoreCase) < 0 ? string.Format("{0} - {1}", dataRow.Addr, dataRow.PortName) : string.Format("{0}", GUtility.GetStringIP(dataRow.Addr));
				}
			}
		}
	}

	protected void lvwVoCustomColors_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			DropDownList ddlAudioSeverity = e.Item.FindControl("ddlAudioSeverity") as DropDownList;
			DropDownList ddlVideoSeverity = e.Item.FindControl("ddlVideoSeverity") as DropDownList;
			HtmlControl divColor = e.Item.FindControl("divColor") as HtmlControl;
			HiddenField hidColorSelector = e.Item.FindControl("hidColorSelector") as HiddenField;

			if ( ( ddlAudioSeverity != null ) && ( ddlVideoSeverity != null ) && ( divColor != null ) && ( hidColorSelector != null ) )
			{
				ddlAudioSeverity.DataSource = this._systemSeverities;
				ddlAudioSeverity.DataBind();

				ddlVideoSeverity.DataSource = this._systemSeverities;
				ddlVideoSeverity.DataBind();

				string audioSystemSevLevel = ( (DataRowView) ( (ListViewDataItem) e.Item ).DataItem )["AudioSystemSevLevel"].ToString();
				string videoSystemSevLevel = ( (DataRowView) ( (ListViewDataItem) e.Item ).DataItem )["VideoSystemSevLevel"].ToString();
				string systemColor = ( (DataRowView) ( (ListViewDataItem) e.Item ).DataItem )["HtmlColor"].ToString();

				if ( ddlAudioSeverity.Items.ContainsValue(audioSystemSevLevel) )
				{
					ddlAudioSeverity.SelectedValue = audioSystemSevLevel;
				}

				if ( ddlVideoSeverity.Items.ContainsValue(videoSystemSevLevel) )
				{
					ddlVideoSeverity.SelectedValue = videoSystemSevLevel;
				}

				divColor.Style["background-color"] = systemColor;
				hidColorSelector.Value = systemColor;
			}

			Button btnDeleteColorRow = e.Item.FindControl("btnDeleteColorRow") as Button;
			ModalPopupExtender mdlpopexDeleteColorRow = e.Item.FindControl("mdlpopexDeleteColorRow") as ModalPopupExtender;

			if ( ( btnDeleteColorRow != null ) && ( mdlpopexDeleteColorRow != null ) )
			{
				btnDeleteColorRow.OnClientClick = "showConfirm(this, 'mdlpopexDeleteColorRow" + ( (ListViewDataItem) e.Item ).DataItemIndex +
												  "');return false;";
				mdlpopexDeleteColorRow.BehaviorID = "mdlpopexDeleteColorRow" + ( (ListViewDataItem) e.Item ).DataItemIndex;
			}
		}
	}

	protected void lvwVoCustomColors_ItemCommand ( object sender, ListViewCommandEventArgs e )
	{
		if ( e.CommandName == "DeleteColorRow" )
		{
			if ( this.tvwNode.SelectedNode != null )
			{
				this.ReloadCustomColorsState();

				var dataKey = this.lvwVoCustomColors.DataKeys[( (ListViewDataItem) e.Item ).DataItemIndex];
				if ( dataKey != null )
				{
					int objectFormulasCustomColorId = int.Parse(dataKey.Value.ToString());

					var row = this._customColors.FindByObjectFormulasCustomColorId(objectFormulasCustomColorId);
					if ( row != null )
					{
						row.Delete();
					}

					this._customColors.AcceptChanges();
				}

				this.ViewState["CustomColors"] = this._customColors;

				this.BindCustomColorGridForNode();
			}
		}
	}

	protected void ddlSystemSeverity_SelectedIndexChanged ( object sender, EventArgs e )
	{
		this.SaveColorGridData();
	}

	protected void btnColorSelector_Click ( object sender, EventArgs e )
	{
		this.SaveColorGridData();
	}

	private void SaveColorGridData ()
	{
		if ( this.tvwNode.SelectedNode != null )
		{
			this.ReloadCustomColorsState();

			foreach ( var colorItem in this.lvwVoCustomColors.Items )
			{
				if ( colorItem.ItemType == ListViewItemType.DataItem )
				{
					var dataKey = this.lvwVoCustomColors.DataKeys[colorItem.DataItemIndex];
					if ( dataKey != null )
					{
						int objectFormulasCustomColorId = int.Parse(dataKey.Value.ToString());

						var row = this._customColors.FindByObjectFormulasCustomColorId(objectFormulasCustomColorId);

						DropDownList ddlAudioSeverity = (DropDownList) colorItem.FindControl("ddlAudioSeverity");
						DropDownList ddlVideoSeverity = (DropDownList) colorItem.FindControl("ddlVideoSeverity");
						HiddenField hidColorSelector = (HiddenField) colorItem.FindControl("hidColorSelector");

						row.AudioSystemSevLevel = int.Parse(ddlAudioSeverity.SelectedValue);
						row.VideoSystemSevLevel = int.Parse(ddlVideoSeverity.SelectedValue);
						row.HtmlColor = hidColorSelector.Value;
					}
				}
			}

			this._customColors.AcceptChanges();

			this.ViewState["CustomColors"] = this._customColors;
			this.BindCustomColorGridForNode();
		}
	}

	private void ReloadCustomColorsState ()
	{
		if ( this.tvwNode.SelectedNode != null )
		{
			Guid objectStatusId = new Guid(this.tvwNode.SelectedValue);

			if ( this._customColors.Rows.Count == 0 )
			{
				if ( this.ViewState["CustomColors"] != null )
				{
					// Gestione via viewstate, con persistenza temporanea
					this._customColors =
						( (ObjectFormulasCustomColorsDS.ObjectFormulaCustomColorsForObjectStatusIdDataTable) this.ViewState["CustomColors"] );
				}
				else
				{
					// Se inesistente, carichiamo da database
					ObjectFormulaCustomColorsForObjectStatusIdTableAdapter ta = new ObjectFormulaCustomColorsForObjectStatusIdTableAdapter();
					this._customColors = ta.GetData(objectStatusId, FORMULA_INDEX_COLORS);

					this.ViewState["CustomColors"] = this._customColors;
				}
			}
		}
	}
}
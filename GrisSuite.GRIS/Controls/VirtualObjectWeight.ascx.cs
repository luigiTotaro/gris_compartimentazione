﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_VirtualObjectWeight : UserControl
{
    public int SymbolsNumber { get; private set; }

    public enum IconType
    {
        Unknown,
        VistaTabellare,
        VistaGrafica
    }

    public IconType VirtualObjectWeightIconType { get; set; }

    public string Weight { get; set; }

    public string ImageToolTip { get; set; }

    public string CssBackgroundColor { get; set; }

    public string AdditionalStyle { get; set; }

    public byte IsComputedByCustomFormula { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {}

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SetupByIconType();
    }

    private void SetupByIconType()
    {
        string imageUrl = string.Empty;
        string floatSide = String.Empty;

        if (string.IsNullOrEmpty(this.Weight))
        {
            this.SymbolsNumber = 0;
        }
        else
        {
            switch (this.Weight.ToLowerInvariant())
            {
                case "fondamentali":
                    this.SymbolsNumber = 3;
                    break;
                case "importanti":
                    this.SymbolsNumber = 2;
                    break;
                case "complementari":
                    this.SymbolsNumber = 1;
                    break;
                case "generici":
                    this.SymbolsNumber = 0;
                    break;
            }
        }

        switch (this.VirtualObjectWeightIconType)
        {
            case IconType.Unknown:
                this.vowc.Visible = false;
                break;
            case IconType.VistaTabellare:
                this.vowc.Visible = true;
                imageUrl = this.ResolveClientUrl(string.Format("~/IMG/Interfaccia/VOW{0}{1}.png", "t", (this.IsComputedByCustomFormula == 1 ? "b" : "g")));
                floatSide = "float:right;";
                break;
            case IconType.VistaGrafica:
                this.vowc.Visible = true;
                // Grafica - Blu
                imageUrl = this.ResolveClientUrl(string.Format("~/IMG/Interfaccia/VOW{0}{1}.png", "g", (this.IsComputedByCustomFormula == 1 ? "b" : "g")));
                floatSide = "float:right;";
                break;
        }

        if (!string.IsNullOrEmpty(this.ImageToolTip))
        {
            if (this.vowc.Visible)
            {
                this.vowc.Attributes["title"] = this.ImageToolTip;
            }
        }

        StringBuilder innerContent = new StringBuilder();
        for (int symbolsCounter = 1; symbolsCounter <= this.SymbolsNumber; symbolsCounter++)
        {
            innerContent.AppendFormat("<div id=\"vowc{0}\" style=\"{1}{2}\"><img src=\"{3}\"/></div>", symbolsCounter,
                                      (string.IsNullOrEmpty(this.CssBackgroundColor)
                                           ? string.Empty : string.Format("background-color:#{0};", this.CssBackgroundColor)), floatSide, imageUrl);
        }

        if (!string.IsNullOrEmpty(this.AdditionalStyle))
        {
            this.vowc.Attributes["style"] = this.AdditionalStyle;
        }

        if (innerContent.Length > 0)
        {
            this.vowd.Text = innerContent.ToString();
            this.vowc.Visible = true;
        }
        else
        {
            this.vowc.Visible = false;
        }
    }
}
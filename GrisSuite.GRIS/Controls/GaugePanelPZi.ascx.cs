﻿
using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.UI;
using GrisControls;

public partial class Controls_GaugePanelPZi : UserControl
{
	private const int PIXEL_HEIGHT = 100;
	private const string NO_DATA_AVAILABLE_STRING = "N/D";	// Questo valore è forzato nella SP di estrazione, quando il valore non è aggiornato
	private IGaugeDataProvider _dataProvider;


	# region Public Properties
	public string ValueName
	{
		get { return this.ViewState["ValueName"].ToString(); }
		set { this.ViewState["ValueName"] = value; }
	}

	public string Description
	{
		get { return this.ViewState["Description"].ToString(); }
		set { this.ViewState["Description"] = value; }
	}

	public string ReferenceFormattedValue
	{
		get { return this.ViewState["ReferenceFormattedValue"].ToString(); }
		set { this.ViewState["ReferenceFormattedValue"] = value; }
	}

	public string ActualFormattedValue
	{
		get { return this.ViewState["ActualFormattedValue"].ToString(); }
		set { this.ViewState["ActualFormattedValue"] = value; }
	}

	public bool ActualValueND
	{
		get { return (this.ViewState["ActualValueND"] != null && Boolean.Parse(this.ViewState["ActualValueND"].ToString())); }
		set { this.ViewState["ActualValueND"] = value; }
	}

    public string InferiorLimitFormattedValue
    {
        get { return this.ViewState["InferiorLimitFormattedValue"].ToString(); }
        set { this.ViewState["InferiorLimitFormattedValue"] = value; }
    }

    public string InferiorYellowLimitFormattedValue
    {
        get { return this.ViewState["InferiorYellowLimitFormattedValue"].ToString(); }
        set { this.ViewState["InferiorYellowLimitFormattedValue"] = value; }
    }

    public string InferiorRedLimitFormattedValue
    {
        get { return this.ViewState["InferiorRedLimitFormattedValue"].ToString(); }
        set { this.ViewState["InferiorRedLimitFormattedValue"] = value; }
    }

	public string SuperiorLimitPerc
	{
		get { return this.ViewState["SuperiorLimitPerc"].ToString(); }
		set { this.ViewState["SuperiorLimitPerc"] = value; }
	}

	public string SuperiorYellowRedOffsetPerc
	{
		get { return this.ViewState["SuperiorYellowRedOffsetPerc"].ToString(); }
		set { this.ViewState["SuperiorYellowRedOffsetPerc"] = value; }
	}

	public string SuperiorYellowGreenOffsetPerc
	{
		get { return this.ViewState["SuperiorYellowGreenOffsetPerc"].ToString(); }
		set { this.ViewState["SuperiorYellowGreenOffsetPerc"] = value; }
	}

	public string InferiorYellowGreenOffsetPerc
	{
		get { return this.ViewState["InferiorYellowGreenOffsetPerc"].ToString(); }
		set { this.ViewState["InferiorYellowGreenOffsetPerc"] = value; }
	}

	public string InferiorYellowRedOffsetPerc
	{
		get { return this.ViewState["InferiorYellowRedOffsetPerc"].ToString(); }
		set { this.ViewState["InferiorYellowRedOffsetPerc"] = value; }
	}

	public string InferiorLimitPerc
	{
		get { return this.ViewState["InferiorLimitPerc"].ToString(); }
		set { this.ViewState["InferiorLimitPerc"] = value; }
	}

	public double ReferenceValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.ReferenceValue : 0D; }
	}

	public double ActualValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.ActualValue : 0D; }
	}

	public double SuperiorLimitValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.SuperiorLimitValue : 0D; }
	}

	public double InferiorLimitValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.InferiorLimitValue : 0D; }
	}

	public double SuperiorYellowRedValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.SuperiorYellowRedValue : 0D; }
	}

	public double InferiorYellowRedValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.InferiorYellowRedValue : 0D; }
	}

	public double SuperiorYellowGreenValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.SuperiorYellowGreenValue : 0D; }
	}

	public double InferiorYellowGreenValue
	{
		get { return ( this._dataProvider != null ) ? this._dataProvider.InferiorYellowGreenValue : 0D; }
	}

	public string InitializationExceptionMessage { get; private set; }

	public string DeviceType { get; set; }

	# endregion

	public new void DataBind ()
	{
		this.InitializeData();
		this.UpdateGauge();
	}

	private void InitializeData ()
	{
		try
		{
            this.trRif.Visible = false;
            this.trWarn.Visible = false;
            this.trErr.Visible = false;

			if (this.ActualFormattedValue.Equals(NO_DATA_AVAILABLE_STRING, StringComparison.OrdinalIgnoreCase))
			{
				// Il valore da database è corretto, ma c'è un errore nella device, che lo rende non valido da visualizzare
				// Per permettere l'inizializzazione del controllo, i valori da DB devono essere numerici, quindi ne impostiamo uno,
				// che poi sarà sovrascritto durante la visualizzazione
				this.ActualFormattedValue = "0.0";
				this.ActualValueND = true;
			}
			else
			{
				this.ActualValueND = false;
			}

			if ( this.DeviceType == "SYSNETM384V4" || this.DeviceType == "SYSNETM684V4" )
			{
				this._dataProvider = new ZeusGaugeDataProvider(this.ReferenceFormattedValue, this.ActualFormattedValue,
												this.InferiorLimitPerc, this.InferiorYellowRedOffsetPerc, this.InferiorYellowGreenOffsetPerc, PIXEL_HEIGHT);

                this.trRif.Visible = true;
                this.trWarn.Visible = true;
			    this.lblReferenceLabel.Text = "Vel. massima:";
			    this.lblWarnLabel.Text = "Vel. minima:";
			    this.lblValueLabel.Text = "Velocità:";
			}
			else
			{
				this._dataProvider = new GaugeDataProvider(this.ReferenceFormattedValue, this.ActualFormattedValue, this.SuperiorLimitPerc,
												this.SuperiorYellowRedOffsetPerc, this.SuperiorYellowGreenOffsetPerc,
												this.InferiorLimitPerc, this.InferiorYellowRedOffsetPerc, this.InferiorYellowGreenOffsetPerc, PIXEL_HEIGHT);
                this.trRif.Visible = true;
                this.trWarn.Visible = true;
                this.trErr.Visible = true;
            }

			this._dataProvider.Initialize();
		}
		catch ( GaugeDataProviderInitializationException exc )
		{
			this.InitializationExceptionMessage = exc.Message;
		}
	}

	private void UpdateGauge ()
	{
		if ( this._dataProvider != null && string.IsNullOrEmpty(this.InitializationExceptionMessage) )
		{
            // Serve per discrimare un valore indeterminato (normalmente di impedenza, che corrisponde a 100000 Ohm)
            string regexMaxImpedenceStringPZi = (ConfigurationManager.AppSettings["RegexMaxImpedenceStringPZi"] ?? "indeterminato");

			if (this.ActualValueND)
			{
				// Il valore da database è corretto, ma c'è un errore nella device, che lo rende non valido da visualizzare
				// Quindi si disattiva il gauge e si riporta una stringa corretta
				this.divGauge.Style[HtmlTextWriterStyle.BackgroundImage] = "url(IMG/interfaccia/gauge_d_bg.gif)";

				this.divUpRed.Visible = false;
				this.divUpYellow.Visible = false;
				this.divUpGreen.Visible = false;
				this.divDownGreen.Visible = false;
				this.divDownYellow.Visible = false;
				this.divDownRed.Visible = false;
				this.imgArrow.Visible = false;

				if (this.DeviceType == "SYSNETM384V4" || this.DeviceType == "SYSNETM684V4")
				{
					this.lblWarn.Text = this.InferiorRedLimitFormattedValue;
				}
				else
				{
					this.lblWarn.Text = (this._dataProvider.InferiorYellowGreen/100).ToString("p");
				}

				this.lblHeader.Text = this.ValueName;
				this.lblReference.Text = this.ReferenceFormattedValue;
				// Il valore qui non ha senso, indichiamo stringa statica che riporta la situazione
				this.lblValue.Text = NO_DATA_AVAILABLE_STRING;

				this.lblErr.Text = (this._dataProvider.InferiorYellowRed/100).ToString("p");
				this.lblDesc.Text = this.Description;
			}
			else
			{
				if (Regex.IsMatch(this.Description, regexMaxImpedenceStringPZi))
				{
					this.divGauge.Style[HtmlTextWriterStyle.BackgroundImage] = "url(IMG/interfaccia/gauge_d_bg.gif)";

					this.divUpRed.Visible = false;
					this.divUpYellow.Visible = false;
					this.divUpGreen.Visible = false;
					this.divDownGreen.Visible = false;
					this.divDownYellow.Visible = false;
					this.divDownRed.Visible = false;
					this.imgArrow.Visible = false;
				}
				else
				{
					this.divGauge.Style[HtmlTextWriterStyle.BackgroundImage] = "url(IMG/interfaccia/gauge_bg.gif)";

					this.divUpRed.Visible = true;
					this.divUpYellow.Visible = true;
					this.divUpGreen.Visible = true;
					this.divDownGreen.Visible = true;
					this.divDownYellow.Visible = true;
					this.divDownRed.Visible = true;
					this.imgArrow.Visible = true;

					if (this._dataProvider.SuperiorRedZoneHeight <= 0)
					{
						this.divUpRed.Visible = false;
					}
					else
					{
						this.divUpRed.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.SuperiorRedZoneHeight));
					}

					if (this._dataProvider.SuperiorYellowZoneHeight <= 0)
					{
						this.divUpYellow.Visible = false;
					}
					else
					{
						this.divUpYellow.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.SuperiorYellowZoneHeight));
					}

					if (this._dataProvider.SuperiorGreenZoneHeight <= 0)
					{
						this.divUpGreen.Visible = false;
					}
					else
					{
						this.divUpGreen.Style.Add(HtmlTextWriterStyle.Height,
							this._dataProvider.InferiorGreenZoneHeight <= 0
								? this.FormatPixels(this._dataProvider.SuperiorGreenZoneHeight)
								: this.FormatPixels(this._dataProvider.SuperiorGreenZoneHeight - 1));
					}

					if (this._dataProvider.InferiorGreenZoneHeight <= 0)
					{
						this.divDownGreen.Visible = false;
					}
					else
					{
						this.divDownGreen.Style.Add(HtmlTextWriterStyle.Height,
							this._dataProvider.SuperiorGreenZoneHeight <= 0
								? this.FormatPixels(this._dataProvider.InferiorGreenZoneHeight)
								: this.FormatPixels(this._dataProvider.InferiorGreenZoneHeight - 1));
					}

					if (this._dataProvider.InferiorYellowZoneHeight <= 0)
					{
						this.divDownYellow.Visible = false;
					}
					else
					{
						this.divDownYellow.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.InferiorYellowZoneHeight));
					}

					if (this._dataProvider.InferiorRedZoneHeight <= 0)
					{
						this.divDownRed.Visible = false;
					}
					else
					{
						this.divDownRed.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.InferiorRedZoneHeight));
					}

					this.imgArrow.Style.Add(HtmlTextWriterStyle.Top,
						this.FormatPixels(PIXEL_HEIGHT - this._dataProvider.IndicatorHeight - 7 /* metà altezza dell' immagine */));

					if (this.DeviceType == "SYSNETM384V4" || this.DeviceType == "SYSNETM684V4")
					{
						this.lblWarn.Text = this.InferiorRedLimitFormattedValue;
					}
					else
					{
						this.lblWarn.Text = (this._dataProvider.InferiorYellowGreen/100).ToString("p");
					}
				}

				this.lblHeader.Text = this.ValueName;
				this.lblReference.Text = this.ReferenceFormattedValue;
				this.lblValue.Text = this.ActualFormattedValue;

				this.lblErr.Text = (this._dataProvider.InferiorYellowRed/100).ToString("p");
				this.lblDesc.Text = this.Description;
			}
		}
		else
		{
			this.divGauge.Style[HtmlTextWriterStyle.BackgroundImage] = "url(IMG/interfaccia/gauge_d_bg.gif)";

			this.divUpRed.Visible = false;
			this.divUpYellow.Visible = false;
			this.divUpGreen.Visible = false;
			this.divDownGreen.Visible = false;
			this.divDownYellow.Visible = false;
			this.divDownRed.Visible = false;
			this.imgArrow.Visible = false;

			this.lblHeader.Text = "";
			this.lblReference.Text = "";
			this.lblValue.Text = "";
			this.lblWarn.Text = "";
			this.lblErr.Text = "";
			this.lblDesc.Text = "";
		}
	}

	private string FormatPixels ( double value )
	{
		return string.Format("{0}px", value);
	}
}

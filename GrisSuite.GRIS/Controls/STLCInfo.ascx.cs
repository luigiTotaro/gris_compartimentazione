using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ServersDSTableAdapters;
using GrisSuite.Data.Gris.STLCParametersDSTableAdapters;
using STLCWS;

public partial class STLCInfo : GrisUserControl
{
    public event EventHandler SyncComplete;
    public event EventHandler SyncError;
    public event EventHandler ServerDelete;
    public event EventHandler ChangeMaintenance;

    //private const string STLC_VERSION_PARAMETER_NAME = "STLCVersion";
    
    #region Properties

    public long NodID
    {
        get { return Convert.ToInt64(this.ViewState["NodID"] ?? -1); }
        set { this.ViewState["NodID"] = value; }
    }

    public long SrvID
    {
        get { return Convert.ToInt64(this.ViewState["SrvID"] ?? -1); }
        set { this.ViewState["SrvID"] = value; }
    }

    public string FullHostName
    {
        get { return (this.ViewState["FullHostName"] ?? "").ToString(); }
        set { this.ViewState["FullHostName"] = value; }
    }

    public string LastMessageType
    {
        get { return (this.ViewState["LastMessageType"] ?? "").ToString(); }
        set { this.ViewState["LastMessageType"] = value; }
    }

    public string IP
    {
        get { return (this.ViewState["IP"] ?? "").ToString(); }
        set { this.ViewState["IP"] = value; }
    }

    public string LastUpdate
    {
        get { return (this.ViewState["LastUpdate"] ?? "").ToString(); }
        set { this.ViewState["LastUpdate"] = value; }
    }

    public bool IsOffline
    {
        get { return Convert.ToBoolean(this.ViewState["IsOffline"] ?? false); }
        set { this.ViewState["IsOffline"] = value; }
    }

    public bool InMaintenance
    {
        get { return Convert.ToBoolean(this.ViewState["InMaintenance"] ?? false); }
        set { this.ViewState["InMaintenance"] = value; }
    }

    public int SevLevel
    {
        get { return Convert.ToInt32(this.ViewState["SevLevel"] ?? 3); }
        set
        {
            this.ViewState["SevLevel"] = value;
            this.IsOffline = (3 == value);
        }
    }

    public int? SevLevelDetailId
    {
        get { return Convert.ToInt32(this.ViewState["SevLevelDetailId"] ?? 3); }
        set
        {
            if (value.HasValue)
            {
                this.ViewState["SevLevelDetailId"] = value;
            }
        }
    }

    public int? SevLevelReal
    {
        get { return Convert.ToInt32(this.ViewState["SevLevelReal"] ?? 3); }
        set
        {
            if (value.HasValue)
            {
                this.ViewState["SevLevelReal"] = value;
            }
        }
    }

    public string SevLevelDesc
    {
        get { return (this.ViewState["SevLevelDesc"] ?? "").ToString(); }
        set { this.ViewState["SevLevelDesc"] = value; }
    }

    public string SevLevelDetailDesc
    {
        get { return (this.ViewState["SevLevelDetailDesc"] ?? "").ToString(); }
        set { this.ViewState["SevLevelDetailDesc"] = value; }
    }

    #endregion

    # region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
        {
            this.lblLastUpdate.Visible = true;
            GUtility.EnableImageButton(this.Page, this.imgbStatus, true);
        }
        else
        {
            this.lblLastUpdate.Visible = false;
            GUtility.EnableImageButton(this.Page, this.imgbStatus, false);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SetupControl();
        this.SetSyncVisibility();
        if (this.tdSync.Visible)
        {
            this.SetSyncOnMouseOver();
        }

        if (GrisPermissions.IsUserInAdminGroup())
        {
            this.imgbDelete.Visible = true;
            this.imgbDelete.Attributes["OnMouseOver"] = "this.src = 'IMG/Interfaccia/stlc_delete_over.gif';";
            this.imgbDelete.Attributes["OnMouseOut"] = "this.src = 'IMG/Interfaccia/stlc_delete.gif';";
            this.imgbDelete.OnClientClick = "showConfirm(this, 'mdlDeletePopup" + this.SrvID + "');return false;";
        }
        else
        {
            this.imgbDelete.Visible = false;
            this.imgbDelete.Attributes["OnMouseOver"] = String.Empty;
            this.imgbDelete.Attributes["OnMouseOut"] = String.Empty;
            this.imgbDelete.OnClientClick = String.Empty;
        }

        # region Pulsante di collegamento a STLC Configurator Web

        bool STLCConfiguratorIsGuestUserEnabled = bool.Parse(ConfigurationManager.AppSettings["STLCConfiguratorIsGuestUserEnabled"].ToLowerInvariant());

        if (GrisPermissions.IsUserInAdminGroup() ||
            (GrisPermissions.IsUserInSTLCOperatorsGroup() &&
             GrisPermissions.IsInRoleWithoutGlobalGroups(int.Parse(Utility.GetCompCode(this.NodID)), PermissionLevel.Level3)) ||
            STLCConfiguratorIsGuestUserEnabled)
        {
            string username = string.Empty;
            string password = string.Empty;

            if (GrisPermissions.IsUserInAdminGroup())
            {
                username = ConfigurationManager.AppSettings["STLCConfiguratorWebUsernameAdmin"];
                password = ConfigurationManager.AppSettings["STLCConfiguratorWebPasswordHashAdmin"];
            }
            else if (GrisPermissions.IsUserInSTLCOperatorsGroup() &&
                     GrisPermissions.IsInRoleWithoutGlobalGroups(int.Parse(Utility.GetCompCode(this.NodID)), PermissionLevel.Level3))
            {
                username = ConfigurationManager.AppSettings["STLCConfiguratorWebUsernameStandard"];
                password = ConfigurationManager.AppSettings["STLCConfiguratorWebPasswordHashStandard"];
            }
            else if (STLCConfiguratorIsGuestUserEnabled)
            {
                username = ConfigurationManager.AppSettings["STLCConfiguratorWebUsernameGuest"];
                password = ConfigurationManager.AppSettings["STLCConfiguratorWebPasswordHashGuest"];
            }

            if ((!string.IsNullOrEmpty(username)) && (!string.IsNullOrEmpty(password)))
            {
                this.lblHostName.Attributes.Add("OnClick",
                                                GUtility.GetSTLCConfiguratorWebUrl(this.IP, username, password, STLCConfiguratorIsGuestUserEnabled));
                this.lblHostName.CssClass = "hostnamec";
                this.lblHostName.ToolTip = ConfigurationManager.AppSettings["LinkConfiguratorWebTooltip"];
            }
            else
            {
                this.lblHostName.Attributes.Remove("OnClick");
                this.lblHostName.CssClass = "hostname";
                this.lblHostName.ToolTip = string.Empty;
            }
        }
        else
        {
            this.lblHostName.Attributes.Remove("OnClick");
            this.lblHostName.CssClass = "hostname";
            this.lblHostName.ToolTip = string.Empty;
        }

        if (this.IP != "" && GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2))
        {
            this.lblHostName.Attributes.Add("OnContextMenu",
                                            "window.clipboardData.setData('text', \"" + HttpUtility.HtmlEncode(this.IP) + "\");return false;");
        }
        else
        {
            this.lblHostName.Attributes.Remove("OnContextMenu");
        }

        # endregion

        this.mdlpopexDelete.BehaviorID = "mdlDeletePopup" + this.SrvID;
    }

    protected void imgbSync_Click(object sender, ImageClickEventArgs e)
    {
        if (!string.IsNullOrEmpty(this.IP))
        {
            this.SyncServer(this.IP.Trim());
        }
    }

    protected void imgbStatus_Click(object sender, ImageClickEventArgs e)
    {
        if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
        {
            try
            {
                serversTableAdapter ta = new serversTableAdapter();
                ta.UpdateInMaintenance(!this.InMaintenance, this.SrvID);
				FormulaEngineService formulaService = new FormulaEngineService();
				formulaService.EvaluateRegion(this.NavigationPage.RegID);
				this.OnChangeMaintenance();
            }
            catch (SqlException)
            {}
        }
    }

    protected void imgbDelete_Click(object sender, ImageClickEventArgs e)
    {
        if (GrisPermissions.IsUserInAdminGroup())
        {
            serversTableAdapter ta = new serversTableAdapter();
            ta.DelAllbySrvID(this.SrvID);

            this.OnServerDeleted();
        }
    }

    # endregion

    # region Methods

    protected void SetupControl()
    {
        if (this.FullHostName != "")
        {
            this.lblHostName.Text = this.FormatHostName(this.FullHostName);
        }

        if (this.IP != "" && GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2))
        {
            this.lblHostName.Text += string.Format(" ({0})", this.IP);
        }

        if (this.LastMessageType != "" && this.LastUpdate != "")
        {
            this.lblOfflineMessage.ToolTip =
                string.Format("Ultimo messaggio \"{0}\", ricevuto il {1}", this.LastMessageType,
                              DateTime.Parse(this.LastUpdate).ToString("dd/MM/yyyy HH.mm"));
            this.lblLastUpdate.Text = string.Format("\"{0}\" ricevuto il {1}", this.LastMessageType,
                                                    DateTime.Parse(this.LastUpdate).ToString("dd/MM/yyyy HH.mm"));
        }

        string localServerTypeName = GUtility.GetLocalServerTypeName(this.SrvID.ToString());

        if (localServerTypeName == "STLC1000")
        {
            this.lblSTLCVersion.ToolTip = string.Format("{0} SN: {1}", localServerTypeName, Utility.GetSNFromSrvID((ulong)this.SrvID));
        } else {
            this.lblSTLCVersion.ToolTip = string.Format("{0}", localServerTypeName);
        }
                

        # region Stato STLC1000

        const string IMG_PATH_TEMPLATE = "~/IMG/interfaccia/server_status_{0}{1}.gif";
        if (this.InMaintenance || (this.SevLevel == 9) || (this.SevLevel == 2))
        {
            // Le stazioni prive di periferiche associate con stato e che non siano storico vanno in stato 2 di anomalia grave forzata (problemi del collettore)
            this.imgbStatus.ImageUrl = string.Format(IMG_PATH_TEMPLATE, this.SevLevelReal, "_inmaint");
            this.imgbStatus.AlternateText = this.SevLevelDesc;
            this.imgbStatus.ToolTip = this.imgbStatus.AlternateText;
            this.lblOfflineMessage.Text = this.imgbStatus.AlternateText;

            if (this.SevLevelDetailId != 7)
            {
                this.lblOfflineMessage.Text = this.SevLevelDetailDesc;
            }
        }
        else // In servizio
        {
            this.imgbStatus.ImageUrl = string.Format(IMG_PATH_TEMPLATE, this.SevLevelReal, string.Empty);
            if (this.SevLevel == 0)
            {
                this.lblOfflineMessage.Text = string.Format("{0} in linea", localServerTypeName);
                this.imgbStatus.AlternateText = this.lblOfflineMessage.Text;
                this.imgbStatus.ToolTip = this.lblOfflineMessage.Text;
            }
            else
            {
                this.lblOfflineMessage.Text = string.Format("ATTENZIONE: {0} non in linea", localServerTypeName);
                this.imgbStatus.AlternateText = this.lblOfflineMessage.Text;
                this.imgbStatus.ToolTip = this.lblOfflineMessage.Text;
            }
        }

        # endregion

        this.lblSTLCVersion.Text = String.Empty;
        this.lblSTLCVersion.Visible = false;

        if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
        {
            string LocalServerVersionParameterName = GUtility.GetLocalServerVersionParameterName();
            
            stlc_parametersTableAdapter stlc_parametersTA = new stlc_parametersTableAdapter();
            STLCParametersDS.stlc_parametersDataTable stlc_parametersDT = stlc_parametersTA.GetData(this.SrvID, LocalServerVersionParameterName);

            if ((stlc_parametersDT != null) && (stlc_parametersDT.Rows.Count > 0))
            {
                this.lblSTLCVersion.Text = string.Format("Ver. {0}", stlc_parametersDT[0].ParameterValue);
                this.lblSTLCVersion.Visible = true;
            }
            else if (stlc_parametersDT.Rows.Count == 0)
            {
                stlc_parametersDT = stlc_parametersTA.GetData(this.SrvID, "STLCVersion");
                if ((stlc_parametersDT != null) && (stlc_parametersDT.Rows.Count > 0))
                {
                    this.lblSTLCVersion.Text = string.Format("Ver. {0}", stlc_parametersDT[0].ParameterValue);
                    this.lblSTLCVersion.Visible = true;
                }
            }
        }
    }

    protected string FormatHostName(string fullHostName)
    {
        if (fullHostName.IndexOf('.') > -1)
        {
            string hostName = fullHostName.Substring(0, fullHostName.IndexOf('.'));
            fullHostName = fullHostName.Replace(hostName, hostName.ToUpper());
        }
        else
        {
            fullHostName = fullHostName.ToUpper();
        }

        return fullHostName;
    }

    private void SyncServer(string srvIP)
    {
        string stlcUrl = string.Format(ConfigurationManager.AppSettings["STLCWSTemplateUrl"], srvIP);
        STLCService stlcWS = new STLCService();
        stlcWS.Url = stlcUrl;
        stlcWS.Credentials = CredentialCache.DefaultCredentials;

        try
        {
            if (stlcWS.SendDeviceStatusAndConfigToCentral())
            {
                this.SetSyncLayoutComplete();
                this.OnSyncComplete();
            }
            else
            {
                this.SetSyncLayoutError();
                this.OnSyncError();
            }
        }
        catch (WebException exc)
        {
            Tracer.TraceMessage(HttpContext.Current,
                                string.Format("Fallita sincronizzazione con {0} ({1}): errore -> {1}; stack trace -> {2}.",
                                              GUtility.GetLocalServerTypeName(this.SrvID.ToString()), this.IP, exc.Message));
            this.SetSyncLayoutError();
            this.OnSyncError();
        }
    }

    # region Set Layout Methods

    public void SetSyncLayoutComplete()
    {
        this.imgbSync.ImageUrl = "~/IMG/interfaccia/stlc_sync_ok.gif";
    }

    public void SetSyncLayoutError()
    {
        this.imgbSync.ImageUrl = "~/IMG/interfaccia/stlc_sync_no.gif";
    }

    private void SetSyncVisibility()
    {
        if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
        {
            this.lblSTLCVersion.Visible = true;
            this.imgbDelete.Visible = true;

            // Disattiviamo il bottone di Sync STLC, di default. Si pu� riattivare inserendo l'appSetting STLCSyncVisibile a true
            string STLCSyncVisibile = ConfigurationManager.AppSettings["STLCSyncVisibile"];
            bool isSyncVisible = false;

            if (!string.IsNullOrEmpty(STLCSyncVisibile))
            {
                bool.TryParse(STLCSyncVisibile.ToLowerInvariant(), out isSyncVisible);
            }

            this.tdSync.Visible = isSyncVisible && (!string.IsNullOrEmpty(this.IP) && !this.IsOffline);
        }
        else
        {
            this.lblSTLCVersion.Visible = false;
            this.imgbDelete.Visible = false;

            this.tdSync.Visible = false;
        }
    }

    private void SetSyncOnMouseOver()
    {
        this.imgbSync.Attributes["OnMouseOver"] = @"this.src = this.src.replace(/stlc_sync\w*\.gif/, 'stlc_sync_over.gif');";
        this.imgbSync.Attributes["OnMouseOut"] = "this.src = this.src.replace('stlc_sync_over.gif', 'stlc_sync.gif');";
    }

    # endregion

    # region class plumbing

    protected void OnSyncComplete()
    {
        if (this.SyncComplete != null)
        {
            this.SyncComplete(this, null);
        }
    }

    protected void OnChangeMaintenance()
    {
        if (this.ChangeMaintenance != null)
        {
            this.ChangeMaintenance(this, null);
        }
    }

    protected void OnSyncError()
    {
        if (this.SyncError != null)
        {
            this.SyncError(this, null);
        }
    }

    protected void OnServerDeleted()
    {
        if (this.ServerDelete != null)
        {
            this.ServerDelete(this, null);
        }
    }

    # endregion

    # endregion
}
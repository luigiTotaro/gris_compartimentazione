<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationBar.ascx.cs"
    Inherits="Controls_NavigationBar" %>
<%@ Register TagPrefix="gris" TagName="LayeredIcon" Src="~/Controls/LayeredIcon.ascx" %>
<%@ Register TagPrefix="gris" TagName="AckList" Src="~/Controls/AckList.ascx" %>
<asp:Button runat="server" ID="hidpopupAck" Style="display: none" />
<asp:UpdatePanel ID="updNavToolbar" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table class="nb" style="width: 1201px;">
            <tr style="background-image: url(IMG/interfaccia/nav_bg.gif); background-repeat: repeat-x">
                <td style="width: 757px;">
                    <table id="tblNavLink" runat="server" class="nb">
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbItalia" runat="server" CssClass="menuLink" CommandName="Italia"
                                    OnCommand="LinkBtn_Command"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Image ID="sep1" runat="server" ImageUrl="~/IMG/interfaccia/nav_sep.gif" />
                            </td>
                            <td>
                                <gris:LayeredIcon ID="imgCompartimentoSev" runat="server" LayeredIconType="BarraNavigazione"
                                    Visible="False" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbCompartimento" runat="server" CssClass="menuLink" CommandName="Compartimento"
                                    OnCommand="LinkBtn_Command"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Image ID="sep2" runat="server" ImageUrl="~/IMG/interfaccia/nav_sep.gif" />
                            </td>
                            <td>
                                <gris:LayeredIcon ID="imgLineaSev" runat="server" LayeredIconType="BarraNavigazione"
                                    Visible="False" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbLinea" runat="server" CssClass="menuLink" CommandName="Linea"
                                    OnCommand="LinkBtn_Command"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Image ID="sep3" runat="server" ImageUrl="~/IMG/interfaccia/nav_sep.gif" />
                            </td>
                            <td>
                                <gris:LayeredIcon ID="imgStazioneSev" runat="server" LayeredIconType="BarraNavigazione"
                                    Visible="False" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbStazione" runat="server" CssClass="menuLink" CommandName="Stazione"
                                    OnCommand="LinkBtn_Command"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right; width: 354px;">
                    <asp:UpdatePanel ID="updGoTo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table class="nb">
                                <tr>
                                    <td>
                                        <span id="spnNoRowsMessage" class="autocomplete_norows">Nessun Risultato</span>
                                    </td>
                                    <td id="tdSxGoTo" runat="server" style="background-image: url(IMG/interfaccia/goto_sx.gif);
                                        background-repeat: no-repeat; width: 20px;">
                                        &nbsp; &nbsp;
                                    </td>
                                    <td id="tdCenterGoTo" runat="server" style="background-image: url(IMG/interfaccia/goto_bg.gif);
                                        background-repeat: repeat-x;">
                                        <asp:TextBox ID="txtBoxGoToNode" runat="server" Width="230px" Height="20px" CssClass="BoxGoToNode"></asp:TextBox>
                                        <act:TextBoxWatermarkExtender ID="txtBoxGoToNodeWM" runat="server" TargetControlID="txtBoxGoToNode"
                                            WatermarkText=" -- Vai alla stazione" WatermarkCssClass="watermarked" />
                                        <act:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteEx" ID="autoCompBoxGoToNode"
                                            TargetControlID="txtBoxGoToNode" ServicePath="NavigationBarGoToNode.asmx" ServiceMethod="GetCompletionList"
                                            MinimumPrefixLength="2" CompletionInterval="500" EnableCaching="true" CompletionSetCount="20"
                                            FirstRowSelected="true" CompletionListCssClass="autocomplete_completionListElement"
                                            CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem">
                                        </act:AutoCompleteExtender>
                                    </td>
                                    <td id="tdDxGoTo" runat="server">
                                        <asp:Image ID="imgbBoxGoToNode" runat="server" ImageUrl="~/IMG/Interfaccia/goto_dx.gif" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="text-align: center; width: 18px;">
                    <asp:Image runat="server" ID="imgNavSep2" ImageUrl="~/IMG/Interfaccia/nav_sep2.gif"
                        CssClass="imgnb" />
                </td>
                <td style="width: 72px;">
                    <asp:ImageButton ID="imgbSwitchLayout" runat="server" ImageUrl="~/IMG/interfaccia/nav_btn_viewtab.gif"
                        CssClass="imgnb" OnClick="imgbSwitchLayout_Click" />
                    <asp:ImageButton ID="imgbRefreshPage" runat="server" ImageUrl="~/IMG/Interfaccia/btnRefresh_default.gif"
                        CssClass="imgnb" OnClick="imgbRefreshPage_Click" />
                    <asp:Image ID="imgbMenu" runat="server" ImageUrl="~/IMG/Interfaccia/btnMenu.gif"
                        CssClass="imgnb" />
                    <act:PopupControlExtender ID="popexMenu" runat="server" TargetControlID="imgbMenu"
                        PopupControlID="pnlMenu" Position="Bottom" OffsetX="-142" OffsetY="-11" />
                </td>
            </tr>
            <tr runat="server" id="trShadow" visible="true">
                <td colspan="4" style="background-image: url(IMG/interfaccia/nav_shadow_bg.gif);
                    height: 21px; background-repeat: repeat-x;">
                </td>
            </tr>
            <tr runat="server" id="trInfoBar1r" visible="false" style="vertical-align: top">
                <td colspan="4" style="background-image: url(IMG/interfaccia/ibback1.png); height: 57px;
                    background-repeat: no-repeat;">
                    <div style="width: 1112px; padding-left: 20px;">
                        <asp:Image runat="server" ID="imgIBIcon1" EnableViewState="false" Style="border: 0px;
                            padding-left: 0px; padding-top: 4px; display: block; float: left; padding-right: 10px;" />
                        <asp:Label runat="server" ID="lblIBMessage1" Style="color: #FFFFFF; padding-top: 8px;
                            font-size: 13px; display: block; float: left;"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr runat="server" id="trInfoBar2r" visible="false" style="vertical-align: top">
                <td colspan="4" style="background-image: url(IMG/interfaccia/ibback2.png); height: 106px;
                    background-repeat: no-repeat;">
                    <div style="width: 1112px; padding-left: 20px;">
                        <asp:Image runat="server" ID="imgIBIcon21" EnableViewState="false" Style="border: 0px;
                            padding-left: 0px; padding-top: 4px; display: block; float: left; padding-right: 10px;" />
                        <asp:Label runat="server" ID="lblIBMessage21" Style="color: #FFFFFF; padding-top: 8px;
                            font-size: 13px; display: block; float: left;"></asp:Label>
                    </div>
                    <div style="width: 1112px; padding-left: 20px;">
                        <asp:Image runat="server" ID="imgIBIcon22" EnableViewState="false" Style="border: 0px;
                            padding-left: 0px; padding-top: 4px; display: block; float: left; padding-right: 10px;" />
                        <asp:Label runat="server" ID="lblIBMessage22" Style="color: #FFFFFF; padding-top: 8px;
                            font-size: 13px; display: block; float: left;"></asp:Label>
                    </div>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Panel ID="pnlMenu" runat="server" CssClass="popupMenu">
    <asp:UpdatePanel runat="server" ID="upMenu" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMenuItemGroupsAndFormulas" runat="server" Text="Gestione Gruppi e Regole"
                CommandName="GroupsAndFormulas" CssClass="popupMenuLnk" OnCommand="lnkMenuItem_Command" />
            <asp:LinkButton ID="lnkMenuItemAckReport" runat="server" Text="Visualizza Tacitazioni"
                CommandName="AckReport" CssClass="popupMenuLnk" OnCommand="lnkMenuItem_Command" />
            <asp:LinkButton ID="lnkMenuItemGeta" runat="server" Text="GETA" CommandName="Geta"
                CssClass="popupMenuLnk" OnCommand="lnkMenuItem_Command" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<act:ModalPopupExtender runat="server" ID="popupAck" BehaviorID="popupAckBehavior"
    TargetControlID="hidpopupAck" PopupControlID="pnlAck" BackgroundCssClass="ModalBackground"
    DropShadow="True" RepositionMode="RepositionOnWindowScroll">
</act:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="pnlAck" Style="display: none;
    width: 1150px; padding: 10px;">
    <asp:Panel runat="Server" ID="popupAckDH" Style="background-color: #F4F4F4; border: solid 1px Gray;
        font-family: Arial; font-size: 11pt; color: #000000; font-weight: bold; padding: 5px; text-align: center;">
        Lista tacitazioni attive di competenza
    </asp:Panel>
    <asp:UpdatePanel ID="updAckList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <gris:AckList ID="ackList" runat="server" />
            <asp:LinkButton runat="server" ID="lnkHidePopupAck" Text="Chiudi" Style="font-size: 10pt;
                text-decoration: none; color: #fff; display: block; padding-top: 10px; text-align: center;"
                OnClientClick="$find('popupAckBehavior').hide();" ToolTip="Nasconde la lista delle tacitazioni" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>

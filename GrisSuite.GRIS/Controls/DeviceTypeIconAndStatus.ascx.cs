﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_DeviceTypeIconAndStatus : System.Web.UI.UserControl
{
	public event EventHandler DeviceTypeIconAndStatusClick = null;

	public int? SevLevel
	{
		get { return Convert.ToInt32(this.ViewState["SevLevel"] ?? -1); }
		set { this.ViewState["SevLevel"] = value; }
	}

	public string DeviceType
	{
		get { return Convert.ToString(this.ViewState["DeviceType"] ?? String.Empty); }
		set { this.ViewState["DeviceType"] = value; }
	}
	
	public long DevID
	{
		get { return Convert.ToInt64(this.ViewState["DevID"] ?? -1); }
		set { this.ViewState["DevID"] = value; }
	}

	public string StatusDescription
	{
		get { return Convert.ToString(this.ViewState["StatusDescription"] ?? String.Empty); }
		set { this.ViewState["StatusDescription"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected void Page_PreRender(object sender, EventArgs e)
	{
		this.Update();
	}

	private bool IsSeverityValid()
	{
		if (this.SevLevel.HasValue)
		{
			if ((this.SevLevel.Value == -255) || (this.SevLevel.Value == 0) || (this.SevLevel.Value == 1) || (this.SevLevel.Value == 2) ||
			    (this.SevLevel.Value == 255) || (this.SevLevel.Value == 9))
			{
				return true;
			}
		}

		return false;
	}

	public void Update()
	{
		StringBuilder innerContent = new StringBuilder();

		// sfondo
		innerContent.AppendFormat("<div class=\"dtis_bg\"></div>");

		if (this.IsSeverityValid())
		{
			// layer di sfondo stato sfumato della periferica
			innerContent.AppendFormat("<img class=\"dtis_icon_status_down\" src=\"{0}\" alt=\"\" />",
				this.ResolveClientUrl(string.Format("~/IMG/SKC/gui/sk_icon_50_{0}_down.png", this.SevLevel)));

			if (!string.IsNullOrEmpty(this.DeviceType))
			{
				// immagine della periferica
				innerContent.AppendFormat("<img class=\"dtis_icon\" src=\"{0}\" alt=\"\" />",
					this.ResolveClientUrl(this.GetDeviceTypeImageUrl()));
			}

			// layer di primo piano stato sfumato della periferica e immagine cliccabile, con messaggio di stato
			this.imgck.ImageUrl = string.Format("~/IMG/SKC/gui/sk_icon_50_{0}_up.png", this.SevLevel);
			this.imgck.ToolTip = this.StatusDescription;
		}

		this.dtic.Text = innerContent.ToString();
	}

	private string GetDeviceTypeImageUrl()
	{
		const string emptyDeviceTypeImage = "~/IMG/SKC/devices/unknown_50.png";

		if (string.IsNullOrEmpty(this.DeviceType))
		{
			return emptyDeviceTypeImage;
		}

		string imgRelativePath = String.Format("~/IMG/SKC/devices/{0}_50.png", this.DeviceType); 

		if (File.Exists(HttpContext.Current.Server.MapPath(imgRelativePath)))
		{
			return imgRelativePath;
		}

		return emptyDeviceTypeImage;
	}

	protected void imgck_Click(object sender, ImageClickEventArgs e)
	{
		this.OnDeviceTypeIconAndStatusClick();
	}

	[Browsable(true)]
	public void OnDeviceTypeIconAndStatusClick()
	{
		if (this.DeviceTypeIconAndStatusClick != null)
		{
			this.DeviceTypeIconAndStatusClick(this, new EventArgs());
		}
	}
}
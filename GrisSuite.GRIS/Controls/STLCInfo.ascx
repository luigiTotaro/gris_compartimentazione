<%@ Control Language="C#" AutoEventWireup="true" CodeFile="STLCInfo.ascx.cs" Inherits="STLCInfo" %>
<div style="color: White;">
    <div style="width: 100%; background-color: #3f3f3f; text-align: left; height: 30px;
        background-image: url(IMG/interfaccia/riepilogo_bg.gif); background-repeat: repeat-x;">
        <div style="width: 40px; float: left;">
            <asp:UpdatePanel ID="updStatus" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:ImageButton ID="imgbStatus" runat="server" ToolTip="da configurare" AlternateText="da configurare"
                        OnClick="imgbStatus_Click" Style="padding-left: 3px;" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="width: 325px; text-align: left; padding-left: 0px; font-weight: bold;
            font-size: 13px; white-space: nowrap; float: left;">
            <asp:UpdatePanel ID="updHostName" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblHostName" runat="server" CssClass="hostname"/>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="text-align: left; width: 610px; float: left;">
            <asp:UpdatePanel ID="updOfflineMessage" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="OfflineMessage">
                        <asp:Label ID="lblOfflineMessage" runat="server" Style="font-family: Arial; font-size: 12px;
                            white-space: nowrap; padding-left: 20px; display: block; float: left; padding-top: 8px;"></asp:Label>
                        <asp:Label ID="lblLastUpdate" runat="server" Style="font-family: Arial; font-size: 12px;
                            white-space: nowrap; padding-left: 20px; display: block; float: left; padding-top: 8px;"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="text-align: right; width: 190px; float: left;">
            <asp:UpdatePanel ID="updSync" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="tdStlcToolbox" runat="server" style="width: 130px; white-space: nowrap;
                        display: block;">
                        <div style="float: left; width: 90px; height: 30px; padding-right: 10px; display: block;">
                            <asp:Label runat="server" ID="lblSTLCVersion" Text="0.0.0.0" EnableViewState="false"
                                Style="display: block; padding-top: 7px; padding-right: 10px; font-family: Arial;
                                font-size: 12px; white-space: nowrap;" /></div>
                        <div style="float: left; margin: 0px; padding: 0px; width: 30px; height: 30px; display: block;">
                            <asp:ImageButton ID="imgbDelete" runat="server" AlternateText="Elimina" ToolTip="Elimina"
                                ImageUrl="~/IMG/interfaccia/stlc_delete.gif" OnClick="imgbDelete_Click" Style="display: block;
                                width: 30px; height: 30px;" /></div>
                    </div>
                    <div id="tdSync" runat="server" style="width: 60px; height: 30px;">
                        <asp:ImageButton ID="imgbSync" runat="server" Style="display: block; width: 60px;
                            height: 30px;" AlternateText="Sincronizza" ToolTip="Sincronizza" ImageUrl="~/IMG/interfaccia/stlc_sync.gif"
                            OnClick="imgbSync_Click" />
                    </div>
                    <act:ModalPopupExtender ID="mdlpopexDelete" runat="server" TargetControlID="imgbDelete"
                        PopupControlID="divConfirmDelete" OkControlID="btnOkDelete" OnOkScript="okClick();"
                        CancelControlID="btnCancelDelete" OnCancelScript="cancelClick();" BackgroundCssClass="ModalBackground" />
                    <div id="divConfirmDelete" runat="server" class="deleteConfirm" style="display: none">
                        <img src="IMG/Interfaccia/warning.gif" alt="warning" style="padding-right: 10px;
                            vertical-align: middle" />Vuoi eliminare il dispositivo e tutti i dispositivi
                        collegati?<br />
                        <br />
                        <asp:Button ID="btnOkDelete" runat="server" Text="S�" CausesValidation="true" CssClass="confirmButton" />
                        <asp:Button ID="btnCancelDelete" runat="server" Text="No" CausesValidation="false"
                            CssClass="confirmButton" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

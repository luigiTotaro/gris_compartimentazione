﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupsAndFormulas.ascx.cs"
    Inherits="Controls_GroupsAndFormulas" %>
<act:toolkitscriptmanager ID="smMain" runat="server" AllowCustomErrorsRedirect="True"
    EnableScriptGlobalization="true" EnableScriptLocalization="true" AsyncPostBackTimeout="300"
    OnAsyncPostBackError="smMain_AsyncPostBackError">
    <Scripts>
        <asp:ScriptReference Path="~/Js/Common.js" />
    </Scripts>
</act:toolkitscriptmanager>
<style type="text/css">
    #tabs
    {
        margin-top: 1em;
    }
    #tabs li .ui-icon-close
    {
        float: left;
        margin: 0.4em 0.2em 0 0;
        cursor: pointer;
    }
</style>
<script type="text/javascript" language="javascript">
    var triggeredRecalculate = false;
    function pageLoad() {
        $("#tabs").tabs();
        $("#dynTabs").tabs();
        $('.colorSelector').ColorPicker({
            onBeforeShow: function () {
                $(this).ColorPickerSetColor($(this).children().first().css('backgroundColor'));
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(200);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(200);
                return false;
            },
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).children().first().css('backgroundColor', '#' + hex);
                $(el).ColorPickerHide();
                $(el).siblings('input[type=hidden]').val('#' + hex);
                $(el).siblings('input[type=button]').click();
            }
        });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    }
    function toggleCheckboxes(ctrl, chk) {
        $(ctrl).parents("div[id^='dynTabs-']").find(':checkbox[id$=' + chk + ']').attr('checked', ctrl.checked);
    }
    function parentTriggerRefresh() {
        triggeredRecalculate = true;
    }
    function EndRequestHandler(sender, args) {
        if (triggeredRecalculate) {
            parent.triggerRefresh();
        }
        triggeredRecalculate = false;
    }
</script>
<asp:updatepanel ID="updGlobalContainer" runat="server" 
	UpdateMode="Conditional">
    <ContentTemplate>
        <div id="tabs">
            <div style="float: right;">
                <asp:UpdateProgress runat="server" ID="prgUpdate" DynamicLayout="False" DisplayAfter="100">
                    <ProgressTemplate>
                        <img src="IMG/loading.gif" alt="" style="padding-top: 5px; padding-right: 10px;" /></ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <asp:Button ID="btnRecalculate" EnableViewState="False" Style="margin-top: 5px; margin-right: 9px;
                width: 90px; height: 30px; float: right; cursor: pointer;" Text="Ricalcola" runat="server"
                class="ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text"
                ToolTip="Ricalcola lo stato del compartimento corrente" OnClick="btnRecalculate_Click" OnClientClick="parentTriggerRefresh();" />
            <ul>
                <li id="tabGroups" runat="server" visible="False"><a href="#tabs-1">Raggruppamenti</a></li>
                <li id="tabRuels" runat="server" visible="False"><a href="#tabs-2">Regole</a></li>
            </ul>
			<asp:panel id="pnlTabContent1" runat="server">				
				<div id="tabs-1" style="text-align: left;">
					<asp:UpdatePanel ID="updGroups" runat="server" UpdateMode="Conditional">
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="ddlSystem" EventName="SelectedIndexChanged" />
							<asp:AsyncPostBackTrigger ControlID="btnCreateGroup" EventName="Click" />
						</Triggers>
						<ContentTemplate>
							<div style="width: 810px; height: 40px;">
								<span style="float: left; padding-right: 6px; padding-top: 2px;">Sistema</span>
								<asp:DropDownList ID="ddlSystem" runat="server" Style="width: 300px; float: left;"
									DataTextField="SystemDescription" DataValueField="NodeSystemsId" AutoPostBack="True"
									OnDataBound="ddlSystem_DataBound" OnSelectedIndexChanged="ddlSystem_SelectedIndexChanged">
								</asp:DropDownList>
								<div id="divCreateGroup" style="width: 380px; float: right;">
									<span style="padding-right: 6px; padding-top: 2px;">Gruppo</span>
									<asp:DropDownList ID="ddlCreateGroup" runat="server" Style="width: 250px;" />
									<asp:Button ID="btnCreateGroup" runat="server" EnableViewState="False" Text="Crea"
										Style="margin-left: 3px; width: 62px; cursor: pointer;" class="ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text"
										OnClick="btnCreateGroup_Click" />
								</div>
							</div>
							<div id="dynTabs" style="text-align: left;">
								<ul>
									<asp:Repeater ID="rptTabHeaders" runat="server" DataSourceID="odsChildrenVirtualObjects"
										OnItemCommand="rptTabHeaders_ItemCommand" OnItemDataBound="rptTabHeaders_ItemDataBound">
										<ItemTemplate>
											<li><a href='<%# string.Format("#dynTabs-{0}", Container.ItemIndex) %>'>
												<%# Eval("VirtualObjectName")%></a> <span id="spnRemoveGroup" class="ui-icon ui-icon-close"
													runat="server" visible='<%# Eval("VirtualObjectName").ToString() != "Generici" %>'>
													<asp:LinkButton ID="lnkRemoveGroup" runat="server" CommandName="Rimuovi" CommandArgument='<%# Eval("VirtualObjectName") %>'
														ToolTip="Rimuovi Raggruppamento" Style="cursor: pointer;" />
												</span></li>
											<act:ModalPopupExtender ID="mdlpopexDelete" runat="server" TargetControlID="lnkRemoveGroup"
												PopupControlID="divConfirmDelete" OkControlID="btnOkDelete" OnOkScript="okClick2();"
												CancelControlID="btnCancelDelete" OnCancelScript="cancelClick();" BackgroundCssClass="ModalBackground" />
											<div id="divConfirmDelete" runat="server" class="deleteConfirm" style="display: none">
												<img src="IMG/Interfaccia/warning.gif" alt="warning" style="padding-right: 10px;
													vertical-align: middle" />Vuoi eliminare il gruppo
												<%# Eval("VirtualObjectName")%>, spostando tutti gli oggetti figli nel gruppo Generici?<br />
												<br />
												<asp:Button ID="btnOkDelete" runat="server" Text="Sì" CausesValidation="true" CssClass="confirmButton" />
												<asp:Button ID="btnCancelDelete" runat="server" Text="No" CausesValidation="false"
													CssClass="confirmButton" />
											</div>
										</ItemTemplate>
									</asp:Repeater>
								</ul>
								<asp:Repeater ID="rptTabBodies" runat="server" DataSourceID="odsChildrenVirtualObjects"
									OnItemDataBound="rptTabBodies_ItemDataBound" OnItemCommand="rptTabBodies_ItemCommand"
									OnDataBinding="rptTabBodies_DataBinding">
									<ItemTemplate>
										<div id='<%# string.Format("dynTabs-{0}", Container.ItemIndex) %>'>
											<div style="height: 40px;">
												<div style="float: left; padding-top: 5px; font-size: 9pt; font-weight: bold;">
													<%# Eval("VirtualObjectDescription")%></div>
												<div id="divMoveToGroup" style="float: right;">
													<span style="padding-right: 6px; padding-top: 2px;">Gruppo</span>
													<asp:DropDownList ID="ddlMoveToGroup" runat="server" Style="width: 250px;" DataSourceID="odsChildrenVirtualObjects"
														DataTextField="VirtualObjectName" DataValueField="VirtualObjectID" />
													<asp:Button ID="btnMoveToGroup" runat="server" EnableViewState="False" Text="Sposta"
														Style="margin-left: 3px; width: 62px; cursor: pointer;" class="ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text"
														CommandName="Sposta" />
												</div>
											</div>
											<asp:ListView ID="lvwVoDevices" runat="server" DataKeyNames="DevID" OnItemDataBound="lvwVoDevices_ItemDataBound">
												<LayoutTemplate>
													<div style="background-color: #afafaf; width: 780px;">
														<table id="tblHeaders" runat="server" cellpadding="0" cellspacing="0" width="763px"
															class="ListViewHeader" style="font-size: small">
															<tr id="Tr1" runat="server" style="background-color: #afafaf; font-weight: bold;">
																<th id="Th1" runat="server" width="5%">
																	<div style="padding: 2px;">
																		<asp:CheckBox ID="chkSelectAll" runat="server" onclick="toggleCheckboxes(this, 'chkIncluded');"
																			ToolTip="Seleziona / Deseleziona tutto" />
																	</div>
																</th>
																<th id="Th2" runat="server" width="55%">
																	<div style="padding: 2px;">
																		Nome</div>
																</th>
																<th id="Th3" runat="server" width="15%">
																	<div style="padding: 2px; text-align: center;">
																		Tipo</div>
																</th>
																<th id="Th4" runat="server" width="25%">
																	<div style="padding: 2px;">
																		Indirizzo</div>
																</th>
															</tr>
														</table>
													</div>
													<div id="divGridScroller" style="overflow-y: auto; height: 280px; width: 780px; background-color: #4d4d4d;">
														<table id="tblGrid" runat="server" cellpadding="0" cellspacing="0" width="763px"
															style="font-size: small">
															<tr runat="server" id="itemPlaceholder" />
														</table>
													</div>
												</LayoutTemplate>
												<ItemTemplate>
													<tr id="Tr2" runat="server" style="background-color: #4d4d4d;">
														<td width="5%" style="text-align: center;">
															<div style="padding: 2px;">
																<asp:CheckBox ID="chkIncluded" runat="server" />
															</div>
														</td>
														<td width="55%">
															<div style="padding: 2px;">
																<asp:Label ID="lblDevName" runat="server" Text='<%# Eval("Name") %>' />
															</div>
														</td>
														<td width="15%">
															<div style="padding: 2px; text-align: center">
																<asp:Label ID="lblDevType" runat="server" Text='<%# Eval("Type") %>' />
															</div>
														</td>
														<td width="25%">
															<div style="padding: 2px; text-align: center">
																<asp:Label ID="lblDevAddress" runat="server" Text='<%# GUtility.GetStringIP(Eval("Addr").ToString()) %>' />
															</div>
														</td>
													</tr>
												</ItemTemplate>
											</asp:ListView>
										</div>
									</ItemTemplate>
								</asp:Repeater>
								<asp:ObjectDataSource ID="odsChildrenVirtualObjects" runat="server" OldValuesParameterFormatString="original_{0}"
									SelectMethod="GetData" TypeName="GrisSuite.Data.Gris.VirtualObjectDSTableAdapters.ChildrenVirtualObjectTableAdapter"
									OnSelecting="odsChildrenVirtualObjects_Selecting">
									<SelectParameters>
										<asp:Parameter Name="ObjectId" Type="Int64" />
										<asp:Parameter Name="ObjectTypeId" Type="Int32" DefaultValue="4" />
									</SelectParameters>
								</asp:ObjectDataSource>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
			</asp:panel>
			<asp:panel id="pnlTabContent2" runat="server">				
				<div id="tabs-2">
					<asp:UpdatePanel ID="updRules" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:UpdatePanel ID="updRulesTree" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<div id="divTree">
										<asp:TreeView ID="tvwNode" runat="server" SelectedNodeStyle-CssClass="selectedGroupNode"
											OnSelectedNodeChanged="tvwNode_SelectedNodeChanged">
										</asp:TreeView>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
							<asp:UpdatePanel ID="updRulesDetail" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<div id="divForm">
										<div style="width: 560px; display: block; height: 30px;">
											<div style="float: left; width: 120px;">
												Regola Specifica:</div>
											<div style="float: right; width: 440px; height: 20px;">
												<asp:DropDownList ID="ddlCustomFormula" runat="server" Style="width: 430px;" />
											</div>
										</div>
										<div style="width: 560px; padding-top: 5px; padding-bottom: 5px; display: block;
											height: 25px;">
											se la regola non dovesse essere soddisfatta si attiva:</div>
										<div style="width: 560px; display: block; height: 30px;">
											<div style="float: left; width: 120px;">
												Regola Alternativa:</div>
											<div style="float: right; width: 440px; height: 20px;">
												<asp:DropDownList ID="ddlDefaultFormula" runat="server" Style="width: 430px;" />
											</div>
										</div>
										<div style="width: 560px; border-top: #ffffff 1px solid; display: block; margin-top: 10px;"
											runat="server" id="pnlCustomColors">
											<div style="width: 550px; display: block; height: 30px; padding-top: 15px;">
												Regole di visualizzazione:</div>
											<div style="width: 550px; display: block; height: 225px;">
												<asp:ListView ID="lvwVoCustomColors" runat="server" DataKeyNames="ObjectFormulasCustomColorId"
													OnItemCommand="lvwVoCustomColors_ItemCommand" OnItemDataBound="lvwVoCustomColors_ItemDataBound">
													<LayoutTemplate>
														<div style="background-color: #4c4c4c; width: 550px;">
															<table id="tblHeaders" runat="server" cellpadding="0" cellspacing="0" width="533px"
																class="ListViewHeader" style="font-size: small">
																<tr id="Tr3" runat="server" style="background-color: #4c4c4c; font-weight: bold;
																	text-align: center;">
																	<th width="30%">
																		Sistema Audio
																	</th>
																	<th width="30%">
																		Sistema Video
																	</th>
																	<th width="20%">
																		Colore
																	</th>
																	<th width="10%">
																		&nbsp;
																	</th>
																</tr>
															</table>
														</div>
														<div id="divGridScrollerVoCustomColors" style="overflow-y: auto; height: 210px; width: 550px;
															background-color: #4c4c4c;">
															<table id="tblGrid" runat="server" cellpadding="0" cellspacing="0" width="533px"
																style="font-size: small">
																<tr runat="server" id="itemPlaceholder" />
															</table>
														</div>
													</LayoutTemplate>
													<ItemTemplate>
														<tr style="background-color: #4c4c4c; text-align: center;">
															<td width="30%">
																<asp:DropDownList ID="ddlAudioSeverity" runat="server" Width="130px" DataValueField="Key"
																	DataTextField="Value" OnSelectedIndexChanged="ddlSystemSeverity_SelectedIndexChanged"
																	AutoPostBack="True" />
															</td>
															<td width="30%">
																<asp:DropDownList ID="ddlVideoSeverity" runat="server" Width="130px" DataValueField="Key"
																	DataTextField="Value" OnSelectedIndexChanged="ddlSystemSeverity_SelectedIndexChanged"
																	AutoPostBack="True" />
															</td>
															<td width="20%">
																<div class="colorSelector">
																	<div runat="server" id="divColor">
																	</div>
																</div>
																<input id="btnColorSelector" runat="server" type="button" style="display: none" onserverclick="btnColorSelector_Click"
																	causesvalidation="false" />
																<asp:HiddenField ID="hidColorSelector" runat="server" />
															</td>
															<td width="10%">
																<asp:Button ID="btnDeleteColorRow" EnableViewState="False" Style="width: 25px; cursor: pointer;"
																	Text="x" runat="server" class="ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text"
																	ToolTip="Rimuove il caso per la regola di visualizzazione" CommandName="DeleteColorRow" />
																<act:ModalPopupExtender ID="mdlpopexDeleteColorRow" runat="server" TargetControlID="btnDeleteColorRow"
																	PopupControlID="divConfirmDeleteColorRow" OkControlID="btnOkDeleteColorRow" OnOkScript="okClick2();"
																	CancelControlID="btnCancelDeleteColorRow" OnCancelScript="cancelClick();" BackgroundCssClass="ModalBackground" />
																<div id="divConfirmDeleteColorRow" runat="server" class="deleteConfirm" style="display: none">
																	<img src="IMG/Interfaccia/warning.gif" alt="warning" style="padding-right: 10px;
																		vertical-align: middle" />Vuoi eliminare il caso specifico e i relativi colori
																	personalizzati?<br />
																	<br />
																	<asp:Button ID="btnOkDeleteColorRow" runat="server" Text="Sì" CausesValidation="true"
																		CssClass="confirmButton" />
																	<asp:Button ID="btnCancelDeleteColorRow" runat="server" Text="No" CausesValidation="false"
																		CssClass="confirmButton" />
																</div>
															</td>
														</tr>
													</ItemTemplate>
												</asp:ListView>
											</div>
											<asp:Button ID="btnAddColor" EnableViewState="False" Style="margin-top: 5px; width: 25px;
												float: left; cursor: pointer;" Text="+" runat="server" class="ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text"
												ToolTip="Crea una nuovo caso per la regola di visualizzazione" OnClick="btnAddColor_Click" />
										</div>
										<div style="width: 560px; display: block; height: 30px;">
											<asp:Button ID="btnSave" EnableViewState="False" Style="margin-top: 5px; margin-right: 10px;
												width: 90px; float: right; cursor: pointer;" Text="Salva" runat="server" class="ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text"
												ToolTip="Salva le impostazioni per l'oggetto corrente" OnClick="btnSave_Click" />
										</div>
										<asp:Label Style="font-size: 10pt; color: #ff0000; font-weight: bold; height: 50px;
											display: block; width: 560px; text-align: center;" runat="server" ID="lblError"
											EnableViewState="False" Visible="False" />
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
							<div style="clear: both;">
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
			</asp:panel>
        </div>
    </ContentTemplate>
</asp:updatepanel>

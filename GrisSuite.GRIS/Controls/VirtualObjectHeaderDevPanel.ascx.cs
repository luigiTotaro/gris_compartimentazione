﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_VirtualObjectHeaderDevPanel : UserControl
{
    public string VirtualObjectName { get; set; }

    public string VirtualObjectDescription { get; set; }

    public int SevLevel { get; set; }

    public string SevLevelDescription { get; set; }

    public int SevLevelDetailId { get; set; }

    public byte IsComputedByCustomFormula { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {}

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.vohdp.Visible = !string.IsNullOrEmpty(this.VirtualObjectName);

        this.imgWeight.Weight = this.VirtualObjectName;
        this.imgWeight.ImageToolTip = this.VirtualObjectName;
        this.imgWeight.IsComputedByCustomFormula = this.IsComputedByCustomFormula;
    }
}
﻿
using System;
using System.Web.UI;

public partial class Controls_InfoFloatingBalloon : UserControl
{
	# region Properties

	public string OverScript { get; private set; }
	public string OutScript { get; private set; }

	public string Message
	{
		get { return this.lblMsg.Text; }
		set { this.lblMsg.Text = value; }
	}

	public int Top
	{
		get { return (int)( this.ViewState["Top"] ?? 0 ); }
		set { this.ViewState["Top"] = value; this.SetTop(value); }
	}
	
	private void SetTop ( int top )
	{
		if ( !string.IsNullOrEmpty(this.pnlLock.Style[HtmlTextWriterStyle.Top]) ) this.pnlLock.Style.Remove(HtmlTextWriterStyle.Top);
		this.pnlLock.Style.Add(HtmlTextWriterStyle.Top, string.Format("{0}px", top));		
	}

	public int Left
	{
		get { return (int)( this.ViewState["Left"] ?? 0 ); }
		set { this.ViewState["Left"] = value; this.SetLeft(value); }
	}

	private void SetLeft ( int left )
	{
		if ( !string.IsNullOrEmpty(this.pnlLock.Style[HtmlTextWriterStyle.Left]) ) this.pnlLock.Style.Remove(HtmlTextWriterStyle.Left);
		this.pnlLock.Style.Add(HtmlTextWriterStyle.Left, string.Format("{0}px", left));
	}

	public int ZIndex
	{
		get { return (int)( this.ViewState["ZIndex"] ?? 0 ); }
		set { this.ViewState["ZIndex"] = value; this.SetZIndex(value); }
	}

	private void SetZIndex ( int zIndex )
	{
		if ( !string.IsNullOrEmpty(this.pnlLock.Style[HtmlTextWriterStyle.ZIndex]) ) this.pnlLock.Style.Remove(HtmlTextWriterStyle.ZIndex);
		this.pnlLock.Style.Add(HtmlTextWriterStyle.ZIndex, zIndex.ToString());
	}

	# endregion

	protected override void OnInit ( EventArgs e )
	{
		base.OnInit(e);
		this.pnlLock.Style.Add(HtmlTextWriterStyle.Display, "none");
		// oltre a gestire la visibilità del div, switcho anche la position del parent del div da static a relative, e viceversa per l'evento di onmuoseout. Questo per permettere un corretto posizionamento del div rispetto al contenitore padre.
		this.OverScript = string.Format(@" if (document.getElementById('{0}')) {{ document.getElementById('{0}').parentNode.style.position = 'relative'; document.getElementById('{0}').style.display = 'block'; }}", this.pnlLock.ClientID);
		this.OutScript = string.Format(@" if (document.getElementById('{0}')) {{ document.getElementById('{0}').parentNode.style.position = 'static'; document.getElementById('{0}').style.display = 'none'; }}", this.pnlLock.ClientID);
	}
}

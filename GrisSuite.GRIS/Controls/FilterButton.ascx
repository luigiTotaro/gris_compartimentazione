﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterButton.ascx.cs" Inherits="Controls_FilterButton" %>
<asp:updatepanel id="updFlag" runat="server" updatemode="Conditional">
	<contenttemplate>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><asp:imagebutton id="imgbFlag" runat="server" 
						imageurl="~/IMG/Interfaccia/alerts_filter_sx_off.gif" alternatetext="" 
						onclick="Filter_Click" /></td>
				<td style="text-align: center; background-image: url(IMG/Interfaccia/alerts_filter_bg.gif); background-repeat: repeat-x; color: White; font-size: 9pt; padding-left: 5px; padding-right: 5px; vertical-align: top; padding-top: 6px">
					<asp:linkbutton id="lnkbFlagText" runat="server" cssclass="LinkBtnGO" onclick="FilterLbl_Click"></asp:linkbutton></td>
				<td><asp:imagebutton id="imgbSx" runat="server" 
						imageurl="~/IMG/Interfaccia/alerts_filter_dx.gif" alternatetext="" 
						onclick="Filter_Click" /></td>
			</tr>
		</table>		
	</contenttemplate>
</asp:updatepanel>

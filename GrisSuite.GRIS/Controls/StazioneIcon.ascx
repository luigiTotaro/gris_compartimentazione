<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StazioneIcon.ascx.cs" Inherits="StazioneIcon" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="act" %>
<%@ Register src="InfoFloatingBalloon.ascx" tagname="InfoFloatingBalloon" tagprefix="gris" %>
<table border="0" cellpadding="0" cellspacing="0" style="background-color:#4c4c4c; width: 200px; text-align: center;">
    <tr>
        <td></td>
        <td style="height:80px; width: 200px; text-align:center; vertical-align:bottom;">
			<asp:label ID="lblNomeUp" runat="server" Text="Label" Visible="False" CssClass="lblStazioneIcon"></asp:label>
		</td>
        <td></td>
    </tr>
    <tr style="height: 30px;">
        <td style="height: 30px; vertical-align:top; text-align: right;">
			<asp:image ID="StazioneSX" runat="server" 
				ImageUrl="~/IMG/Linea/stazione_sx.gif" Visible="false" height="30" /></td>
        <td style="height: 30px; width: 300px; vertical-align:middle; background-image: url(IMG/Linea/stazione_bg.gif); background-repeat: repeat-x; text-align: center;">
            <asp:imagebutton ID="imgNode" runat="server" OnClientClick="<%# GUtility.GetUpdateProgressDivJsFunction() %>"
				ImageUrl="~/IMG/Linea/stazione_0.gif" OnClick="imgNode_Click" />
			<gris:infofloatingballoon id="InfoBalloon" runat="server" message="Sistema di monitoraggio non raggiungibile" zindex="10" top="50" left="-50" />
		</td>
        <td style="height: 30px; vertical-align:top; text-align: left;">
			<asp:image ID="StazioneDX" runat="server" 
				ImageUrl="~/IMG/Linea/stazione_dx.gif" Visible="false" height="30" /></td>
    </tr>
    <tr>
        <td></td>
        <td style="height:80px; width: 200px; text-align:center; vertical-align:top;">
			<asp:label ID="lblNomeDown" runat="server" Text="Label" Visible="False" 
				CssClass="lblStazioneIcon"></asp:label>
		</td>
        <td></td>
    </tr>
</table>
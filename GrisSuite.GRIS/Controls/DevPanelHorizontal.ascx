﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DevPanelHorizontal.ascx.cs"
    Inherits="DevPanelHorizontal" %>
<%@ Reference Control="~/Controls/SelectorsPanel.ascx" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 1160px; height: 100%;">
    <tr>
        <td style="position: relative;">
            <asp:Panel ID="pnlNebbia" runat="server" Width="1180px" BackImageUrl="~/IMG/interfaccia/perif_nebbia_large.gif" style="background-repeat:repeat; position:absolute; top:0; left:0; z-index:10">
            </asp:Panel>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 200px;
                background-color: #808080;">
                <tr>
                    <td style="text-align: left; vertical-align: top;">
                        <asp:Image ID="imgTopSx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_top_sx.gif"
                            EnableViewState="false"></asp:Image>
                    </td>
                    <td style="background-image: url(IMG/interfaccia/GraphicalPanel/pan_top_bg.gif);
                        background-repeat: repeat-x;">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 1150px; height: 100%;">
                            <tr>
                                <td style="text-align: left; vertical-align: text-top; width: 25px">
                                    <asp:Image ID="imgStatus" runat="server" Style="margin-left: 0px" />
                                </td>
								<td style="text-align: left; vertical-align: text-top;">
									<asp:image id="imgLastStatusEnabled" runat="server" alternatetext="Ultimo stato conosciuto attivato" ToolTip="Ultimo stato conosciuto attivato" imageurl="~/IMG/Interfaccia/GraphicalPanel/perif_laststatus.gif" visible="false" />
								</td>
                                 <td style="width: 98%; text-align: left; vertical-align: top; padding-left: 20px">
                                    <div class="DevPanelTitolo">
                                        <asp:Label ID="lblNome" runat="server" Text="Periferica"></asp:Label>
                                    </div>
                                </td>
                                <td style="text-align: right; vertical-align: text-top;">
					                <asp:imagebutton id="btnInMaintenance" runat="server" imageurl="~/IMG/Interfaccia/GraphicalPanel/pan_inconf_off.gif" OnClick="btnInMaintenance_OnClick" ToolTip="Non attivo" AlternateText="Non attivo"/>
								</td>
                            </tr>
                        </table>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <asp:Image ID="imgTopDx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_top_dx.gif"
                            EnableViewState="false"></asp:Image>
                    </td>
                </tr>
                <tr>
                    <td id="tdMiddle" colspan="3" runat="server" style="background-repeat: repeat-y;
                        vertical-align: top; width: 100%; text-align: center;">
                        <div style="width: 250px; padding-left: 8px; float: left">
                            <table cellspacing="0" cellpadding="0" style="width: 100%; text-align: center;">
                                <tr>
                                    <td>
                                        <asp:Image ID="imgDeviceType" runat="server" ToolTip="Dettagli Periferica" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblType" runat="server" Text="" CssClass="DevPanelLBL"></asp:Label><asp:Label ID="lblRawType" runat="server" Visible="false" CssClass="DevPanelLBL" style="padding-left:5px;" /><br />
                                        <asp:Label ID="lblVendor" runat="server" Text="" CssClass="DevPanelLBL" ForeColor="White"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width: 230px; height: 200px; padding-left: 8px; padding-right: 8px; float: left;
                            vertical-align: top; text-align: left; border-left: solid 1px #4C4C4C; border-right: solid 1px #4C4C4C">
                            <table border="0" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="font-weight: bold; color: #4d4d4d; font-family: Arial; vertical-align: top;">
                                        <table cellspacing="4" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblWhereLabel" runat="server" EnableViewState="false" Text="Ubicazione"
                                                        CssClass="DevPanelLBL"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblWhere" runat="server" Text="..." CssClass="DevPanelContent"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNotesLabel" runat="server" Text="Note" CssClass="DevPanelLBL"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNotes" runat="server" Text="..." CssClass="DevPanelContent"></asp:Label>
                                                </td>
                                            </tr>
											<tr id="trIP" runat="server" visible="false">
												<td>
													<asp:label id="lblIPLabel" runat="server" text="IP" cssclass="DevPanelLBL"></asp:label>
												</td>
											</tr>
											<tr id="trIPValue" runat="server" visible="false">
												<td>
													<asp:label id="lblIP" runat="server" text="..." cssclass="DevPanelContent"></asp:label>
												</td>
											</tr>
											<tr id="trTicket" runat="server" visible="false">
												<td>
													<asp:label id="lblTicketLabel" runat="server" text="Ticket" cssclass="DevPanelLBL"></asp:label>
												</td>
											</tr>
											<tr id="trTicketValue" runat="server" visible="false">
												<td>
													<asp:label id="lblTicket" runat="server" text="..." cssclass="DevPanelContent"></asp:label>
												</td>
											</tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; vertical-align: bottom;">
                                        <asp:ImageButton ID="imgScreenDump" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/perif_info.gif" />&nbsp;
                                    </td>
                                    <td style="text-align: right; vertical-align: bottom;">
                                        <asp:ImageButton ID="imgListenDump" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/arm_play.gif" />
                                    </td>
                                    <td style="text-align: right; vertical-align: bottom;">
                                        <asp:ImageButton ID="imgCommandDump" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/perif_command.gif" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width: 630px; padding-left: 8px; padding-right: 8px; float: left">
                            <table border="0" style="width: 100%; background-color: #808080;" cellpadding="5">
                                <tr>
                                    <td class="DevPanelLBL" style="width: 20%; text-align: right; height: 6px;">
                                        Indirizzo:
                                    </td>
                                    <td class="DetailPanelValue" style="width: 80%; height: 6px;">
                                        <asp:Label ID="lblAddress" runat="server" Text="(Address)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="DevPanelLBL" style="text-align: right;">
                                        Numero seriale:
                                    </td>
                                    <td class="DetailPanelValue">
                                        <asp:Label ID="lblSN" runat="server" Text="(SN)"></asp:Label><asp:label id="lblServerSNValue" runat="server" text="..." style="padding-left:15px"></asp:label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="DevPanelLBL" style="text-align: right;">
                                        Nome porta:
                                    </td>
                                    <td class="DetailPanelValue">
                                        <asp:Label ID="lblPortName" runat="server" Text="(PortName)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="DevPanelLBL" style="text-align: right;">
                                        Tipo porta:
                                    </td>
                                    <td class="DetailPanelValue">
                                        <asp:Label ID="lblPortType" runat="server" Text="(PortType)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="DevPanelLBL" style="text-align: right;">
                                        Stato:
                                    </td>
                                    <td class="DetailPanelValue">
                                        <asp:Label ID="lblSeverityDescription" runat="server" Text="(Stato)"></asp:Label>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><asp:Image ID="imgBotSx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_bot_sx.gif" EnableViewState="false"></asp:Image></td>
                    <td style="background-image: url(IMG/Interfaccia/GraphicalPanel/pan_bot_bg.gif);background-repeat: repeat-x;"></td>
                    <td><asp:Image ID="imgBotDx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_bot_dx.gif" EnableViewState="false"></asp:Image></td>
                </tr>
            </table>
            <asp:Panel ID="pnlTopLimit" runat="server">
            </asp:Panel>
        </td>
    </tr>
</table>

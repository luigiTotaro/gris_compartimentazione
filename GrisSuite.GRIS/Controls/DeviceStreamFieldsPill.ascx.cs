﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_DeviceStreamFieldsPill : System.Web.UI.UserControl
{
	public int? SevLevel { get; set; }
	public string DeviceType { get; set; }
	public long DevID { get; set; }
	public int CountStatusm255 { get; set; }
	public int CountStatus0 { get; set; }
	public int CountStatus1 { get; set; }
	public int CountStatus2 { get; set; }
	public int CountStatus9 { get; set; }
	public int CountStatus255 { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected void Page_PreRender(object sender, EventArgs e)
	{
		this.Update();
	}

	private bool IsSeverityValid()
	{
		if (this.SevLevel.HasValue)
		{
			if ((this.SevLevel.Value == -255) || (this.SevLevel.Value == 0) || (this.SevLevel.Value == 1) || (this.SevLevel.Value == 2) ||
			    (this.SevLevel.Value == 255) || (this.SevLevel.Value == 9))
			{
				return true;
			}
		}

		return false;
	}

	public void Update()
	{
		if (this.IsSeverityValid())
		{
			this.pill.Text = this.HtmlPillBuild(true, this.CountStatusm255, this.CountStatus0, this.CountStatus1, this.CountStatus2, this.CountStatus255);
		}
	}

	// Funzione per costruire il codice HTML per visualizzare un "pillolone" di stato
	// Convertita direttamente da codice PHP dell'SKC Consolle
	private string HtmlPillBuild(bool showValues, int countStatusm255, int countStatus0, int countStatus1, int countStatus2, int countStatus255)
	{
		StringBuilder html = new StringBuilder();

		const int width = 200;
		const int safeWidth = width - 60;

		int statusSum = countStatus255 + countStatus2 + countStatus1 + countStatus0 + countStatusm255;

		int width255 = 0;
		int width2 = 0;
		int width1 = 0;
		int width0 = 0;
		int widthm255 = 0;

		// Calcolo preliminare delle lunghezze fasce pillolone
		if (countStatus255 > 0)
		{
			width255 = 10 + (int) Math.Floor((safeWidth*countStatus255)/(double) statusSum);
		}

		if (countStatus2 > 0)
		{
			width2 = 10 + (int) Math.Floor((safeWidth*countStatus2)/(double) statusSum);
		}

		if (countStatus1 > 0)
		{
			width1 = 10 + (int) Math.Floor((safeWidth*countStatus1)/(double) statusSum);
		}

		if (countStatus0 > 0)
		{
			width0 = 10 + (int) Math.Floor((safeWidth*countStatus0)/(double) statusSum);
		}

		if (countStatusm255 > 0)
		{
			widthm255 = 10 + (int) Math.Floor((safeWidth*countStatusm255)/(double) statusSum);
		}

		// Aggiungo 5 px alla fascia piu' a sinistra
		if (width255 > 0)
		{
			width255 += 5;
		}
		else if (width2 > 0)
		{
			width2 += 5;
		}
		else if (width1 > 0)
		{
			width1 += 5;
		}
		else if (width0 > 0)
		{
			width0 += 5;
		}
		else if (widthm255 > 0)
		{
			widthm255 += 5;
		}

		// Aggiungo 5 px alla fascia piu' a destra
		if (widthm255 > 0)
		{
			widthm255 += 5;
		}
		else if (width0 > 0)
		{
			width0 += 5;
		}
		else if (width1 > 0)
		{
			width1 += 5;
		}
		else if (width2 > 0)
		{
			width2 += 5;
		}
		else if (width255 > 0)
		{
			width255 += 5;
		}

		int widthSum = width255 + width2 + width1 + width0 + widthm255;
		int widthRemainder = width - widthSum;

		// Distribuisco l'eventuale resto sulle fasce
		if (widthRemainder > 0)
		{
			if (widthRemainder%2 == 0)
			{
				if (width255 > 0)
				{
					width255++;
				}
				else if (widthm255 > 0)
				{
					widthm255++;
				}
				else if (width2 > 0)
				{
					width2++;
				}
				else if (width0 > 0)
				{
					width0++;
				}
				else if (width1 > 0)
				{
					width1++;
				}
				else
				{
					widthRemainder++;
				}
				widthRemainder--;
			}

			while (widthRemainder > 0)
			{
				if (widthRemainder%2 == 0)
				{
					if (width255 > 0)
					{
						width255++;
					}
					else if (width2 > 0)
					{
						width2++;
					}
					else if (width1 > 0)
					{
						width1++;
					}
					else if (width0 > 0)
					{
						width0++;
					}
					else if (widthm255 > 0)
					{
						widthm255++;
					}
				}
				else
				{
					if (width0 > 0)
					{
						width0++;
					}
					else if (width1 > 0)
					{
						width1++;
					}
					else if (width2 > 0)
					{
						width2++;
					}
					else if (width255 > 0)
					{
						width255++;
					}
					else if (widthm255 > 0)
					{
						widthm255++;
					}
				}
				widthRemainder--;
			}
		}

		int offset = 0;

		// Fasce colorate del pillolone
		if (width255 > 0)
		{
			html.AppendFormat("<div class=\"pill_color\" style=\"left:{0}px;width:{1}px;background-color:#333333;\"></div>", offset, width255);
			if (showValues)
			{
				html.AppendFormat("<p class=\"pill_number\" style=\"left:{0}px;width:{1}px;\">{2}</p>", offset, width255, countStatus255);
			}
			offset += width255;
		}

		if (width2 > 0)
		{
			html.AppendFormat("<div class=\"pill_color\" style=\"left:{0}px;width:{1}px;background-color:red;\"></div>", offset, width2);
			if (showValues)
			{
				html.AppendFormat("<p class=\"pill_number\" style=\"left:{0}px;width:{1}px;\">{2}</p>", offset, width2, countStatus2);
			}
			offset += width2;
		}

		if (width1 > 0)
		{
			html.AppendFormat("<div class=\"pill_color\" style=\"left:{0}px;width:{1}px;background-color:#cda400;\"></div>", offset, width1);
			if (showValues)
			{
				html.AppendFormat("<p class=\"pill_number\" style=\"left:{0}px;width:{1}px;\">{2}</p>", offset, width1, countStatus1);
			}
			offset += width1;
		}

		if (width0 > 0)
		{
			html.AppendFormat("<div class=\"pill_color\" style=\"left:{0}px;width:{1}px;background-color:green;\"></div>", offset, width0);
			if (showValues)
			{
				html.AppendFormat("<p class=\"pill_number\" style=\"left:{0}px;width:{1}px;\">{2}</p>", offset, width0, countStatus0);
			}
			offset += width0;
		}

		if (widthm255 > 0)
		{
			html.AppendFormat("<div class=\"pill_color\" style=\"left:{0}px;width:{1}px;background-color:gray;\"></div>", offset, widthm255);
			if (showValues)
			{
				html.AppendFormat("<p class=\"pill_number\" style=\"left:{0}px;width:{1}px;\">{2}</p>", offset, widthm255, countStatusm255);
			}
			// offset += widthm255;
		}

		if (width255 == 0 && width2 == 0 && width1 == 0 && width0 == 0 && widthm255 == 0)
		{
			html.AppendFormat("<p class=\"pill_number\" style=\"left:{0}px;width:{1}px;\">{2}</p>", 0, width, "Nessun dato disponibile");
		}

		// Sfondo del pillolone, componenti sinistra, centrale e destra
		html.AppendFormat("<img class=\"pill_panell\" src=\"{0}\" alt=\"\" />", this.ResolveClientUrl("~/IMG/SKC/gui/sk_panel_pill_left.png"));
		html.AppendFormat("<img class=\"pill_panelc\" src=\"{0}\" alt=\"\" />", this.ResolveClientUrl("~/IMG/SKC/gui/sk_panel_pill_bg.png"));
		html.AppendFormat("<img class=\"pill_panelr\" src=\"{0}\" alt=\"\" />", this.ResolveClientUrl("~/IMG/SKC/gui/sk_panel_pill_right.png"));

		return (html.ToString());
	}
}
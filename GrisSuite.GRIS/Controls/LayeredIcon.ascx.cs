﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_LayeredIcon : UserControl
{
    public enum IconType
    {
        Unknown,
        BarraNavigazione,
        VistaTabellare,
        VirtualObjectHeader
    }

    public enum IconTypeAdditionalLayer
    {
        None,
        Offline,
        OkNonCentoPerCento
    }

    public string CssWitdh { get; private set; }

    public string CssHeight { get; private set; }

    public string CssBackgroundColor { get; set; }

    public string AdditionalStyle { get; set; }

    public int? Status { get; set; }

    public string ImageToolTip { get; set; }

    public IconType LayeredIconType { get; set; }

    public IconTypeAdditionalLayer LayeredIconTypeAdditionalLayer { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {}

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SetupByIconType();
    }

    public void Update()
    {
        this.SetupByIconType();
    }

    private void SetupByIconType()
    {
        switch (this.LayeredIconTypeAdditionalLayer)
        {
            case IconTypeAdditionalLayer.None:
                this.iadd.Visible = false;
                break;
            case IconTypeAdditionalLayer.Offline:
                this.iadd.Visible = true;
                this.iadd.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_off.png");
                break;
            case IconTypeAdditionalLayer.OkNonCentoPerCento:
                this.iadd.Visible = true;
                this.iadd.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_dirty.png");
                break;
        }

        switch (this.LayeredIconType)
        {
            case IconType.Unknown:
                this.CssWitdh = "0px";
                this.CssHeight = "0px";
                this.imsk.Visible = false;
                break;
            case IconType.BarraNavigazione:
                this.CssWitdh = "16px";
                this.CssHeight = "30px";
                this.imsk.Visible = true;
                this.imsk.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "nav_sev_tr.png");
                break;
            case IconType.VistaTabellare:
                this.CssWitdh = "30px";
                this.CssHeight = "30px";
                this.imsk.Visible = true;
                this.imsk.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_tr.png");
                break;
            case IconType.VirtualObjectHeader:
                this.CssWitdh = "30px";
                this.CssHeight = "30px";
                this.imsk.Visible = true;
                this.imsk.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_voh.png");
                break;
        }

        if (!string.IsNullOrEmpty(this.ImageToolTip))
        {
            // Ha senso impostare il tooltip solo sull'immagine più alta nei layer, quelli sottostanti non sono visibili
            if (this.iadd.Visible)
            {
                this.iadd.ToolTip = this.ImageToolTip;
            }
            else
            {
                this.imsk.ToolTip = this.ImageToolTip;
            }
        }

        string backgroundColor = string.Empty;

        switch (this.Status)
        {
                // Mappatura colori con vecchie icone statiche
            case 0:
                backgroundColor = "#4CD100";
                break;
            case 1:
                backgroundColor = "#FAC81D";
                break;
            case -1:
                if (this.imsk.Visible)
                {
                    this.imsk.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_-1.gif");
                }
                backgroundColor = "#4C4C4C";
                break;
            case 2:
                backgroundColor = "#E0272D";
                break;
            case 3:
                backgroundColor = "#231F20";
                break;
            case 9:
                backgroundColor = "#AEB2B7";
                break;
            case 255:
                if (this.imsk.Visible)
                {
                    this.imsk.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_255.gif");
                }
                backgroundColor = "#4C4C4C";
                break;
            case -255:
                if (this.imsk.Visible)
                {
                    this.imsk.ImageUrl = string.Format("~/IMG/Interfaccia/{0}", "tab_stato_-255.gif");
                }
                backgroundColor = "#4C4C4C";
                break;
        }

        if ((string.IsNullOrEmpty(this.CssBackgroundColor)) && (string.IsNullOrEmpty(backgroundColor)))
        {
            this.CssBackgroundColor = "Transparent";
        }
        else if ((string.IsNullOrEmpty(this.CssBackgroundColor)) && (!string.IsNullOrEmpty(backgroundColor)))
        {
            // Il colore forzato dallo Status prevale solo se non è esplicitamente impostato il colore di sfondo CSS
            this.CssBackgroundColor = backgroundColor;
        }

        this.cont.Style.Value = string.Format("width:{0};height:{1};position:relative;background-color:{2};{3}", this.CssWitdh,
                                              this.CssHeight, this.CssBackgroundColor,
                                              (string.IsNullOrEmpty(this.AdditionalStyle) ? String.Empty : string.Format("{0};", this.AdditionalStyle)));
        this.imsk.Style.Value = "position:absolute;";
        this.iadd.Style.Value = "position:absolute;";
    }
}
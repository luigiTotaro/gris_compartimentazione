using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using GrisSuite.Common;

public partial class StazioneIcon : GrisUserControl
{
    public event ImageClickEventHandler StazioneClick;

	# region Properties
	public string Stazione
    {
        get
        {
            return this.ViewState["Stazione"].ToString();
        }
        set
        {
            this.ViewState["Stazione"] = value;                
        }
    }

	public int Status
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Status"] ?? -1);
		}
		set
		{
			this.ViewState["Status"] = value;
		}
	}

	public string NodID
	{
		get
		{
			return this.ViewState["NodID"].ToString();
		}
		set
		{
			this.ViewState["NodID"] = value;
		}
	}

	public string Compartimento
	{
		get
		{
			return this.ViewState["Compartimento"].ToString();
		}
		set
		{
			this.ViewState["Compartimento"] = value;
		}
	}

	public string Linea
	{
		get
		{
			return this.ViewState["Linea"].ToString();
		}
		set
		{
			this.ViewState["Linea"] = value;
		}
	}

	public int Index
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Index"] ?? -1);
		}
		set
		{
			this.ViewState["Index"] = value;
		}
	}

	public int Count
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Count"] ?? -1);
		}
		set
		{
			this.ViewState["Count"] = value;
		}
	}

    public int RepeatColumns
    {
        get
        {
            return Convert.ToInt32(this.ViewState["RepeatColumns"] ?? -1);
        }
        set
        {
            this.ViewState["RepeatColumns"] = value;
        }
    }

	# region STLC
	public int STLCOK
	{
		get
		{
			return Convert.ToInt32(this.ViewState["STLCOK"] ?? -1);
		}
		set
		{
			this.ViewState["STLCOK"] = value;
		}
	}

	public int STLCOffline
	{
		get
		{
			return Convert.ToInt32(this.ViewState["STLCOffline"] ?? -1);
		}
		set
		{
			this.ViewState["STLCOffline"] = value;
		}
	}

	public int STLCInMaintenance
	{
		get
		{
			return Convert.ToInt32(this.ViewState["STLCInMaintenance"] ?? -1);
		}
		set
		{
			this.ViewState["STLCInMaintenance"] = value;
		}
	}

	public int STLCNotClassified
	{
		get
		{
			return Convert.ToInt32(this.ViewState["STLCNotClassified"] ?? -1);
		}
		set
		{
			this.ViewState["STLCNotClassified"] = value;
		}
	}

	public int STLCTotalCount
	{
		get
		{
			return Convert.ToInt32(this.ViewState["STLCTotalCount"] ?? -1);
		}
		set
		{
			this.ViewState["STLCTotalCount"] = value;
		}
	}
	# endregion

	# region Devices
	public int Ok
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Ok"] ?? -1);
		}
		set
		{
			this.ViewState["Ok"] = value;
		}
	}

	public int Warning
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Warning"] ?? -1);
		}
		set
		{
			this.ViewState["Warning"] = value;
		}
	}

	public int Errors
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Errors"] ?? -1);
		}
		set
		{
			this.ViewState["Errors"] = value;
		}
	}

	public int Offline
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Offline"] ?? -1);
		}

		set
		{
			this.ViewState["Offline"] = value;
		}
	}

	public int Unknown
	{
		get
		{
			return Convert.ToInt32(this.ViewState["Unknown"] ?? -1);
		}

		set
		{
			this.ViewState["Unknown"] = value;
		}
	}

	public int InMaintenance
	{
		get
		{
			return Convert.ToInt32(this.ViewState["InMaintenance"] ?? -1);
		}

		set
		{
			this.ViewState["InMaintenance"] = value;
		}
	}

	public int NotClassified
	{
		get
		{
			return Convert.ToInt32(this.ViewState["NotClassified"] ?? -1);
		}

		set
		{
			this.ViewState["NotClassified"] = value;
		}
	}

	public int TotalCount
	{
		get
		{
			return Convert.ToInt32(this.ViewState["TotalCount"] ?? -1);
		}

		set
		{
			this.ViewState["TotalCount"] = value;
		}
	}
	# endregion
	# endregion

	protected void Page_Load ( object sender, EventArgs e )
    {
	}

    protected void Page_PreRender(object sender, EventArgs e)
    {
		string nodeName = "";
		
		imgNode.ToolTip = Stazione;

        if (Stazione.Length <= 35)
        {
            nodeName = Stazione;
        }
        else
        {
            nodeName = Stazione.Substring(0, 35) + "...";
        }

		Label visibleNameLabel;
		this.lblNomeUp.Text = this.lblNomeDown.Text = nodeName;
		if ( this.Index % 2 == 0 )
		{
			this.lblNomeUp.Visible = true;
			this.lblNomeDown.Visible = false;
			visibleNameLabel = this.lblNomeUp;
		}
		else
		{
			this.lblNomeUp.Visible = false;
			this.lblNomeDown.Visible = true;
			visibleNameLabel = this.lblNomeDown;
		}

		string displayLockDivOver = "";
		string displayLockDivOut = "";
		// se la stazione non ha STLC1000 online o in maint mode e l'utente non appartiene almeno ad un gruppo di livello 3 non si pu� accedere alla pagina delle periferiche
		if ( !Controllers.Node.CanUserViewDevices(this.NavigationPage.RegID, this.STLCOK, this.STLCInMaintenance, this.STLCTotalCount) )
		{
            // Controlla il posizionamento a sinistra per farlo stare all'interno dell'area della lista in cui � inserito.
            this.InfoBalloon.Left = (this.Index % this.RepeatColumns == 0 && this.Index != 0) ? 0 : -50;
            displayLockDivOver = this.InfoBalloon.OverScript;
			displayLockDivOut = this.InfoBalloon.OutScript;
			this.imgNode.OnClientClick = "return false;";
			this.imgNode.Style[HtmlTextWriterStyle.Cursor] = "default";
		}
		
		this.SetVisualStatus();
		
		// gestione onmouseover sul nodo stazione
		this.imgNode.Attributes["OnMouseOver"] = string.Format("$get('{0}').style.color = '#00baff';{1}", visibleNameLabel.ClientID, displayLockDivOver);
		this.imgNode.Attributes["OnMouseOut"] = string.Format("$get('{0}').style.color = 'white';{1}", visibleNameLabel.ClientID, displayLockDivOut);

        //inserisco l'inizio o fine tratta
		if ( this.Index == 0 ) this.StazioneSX.Visible = true;
        if ( this.Index == this.Count - 1 ) this.StazioneDX.Visible = true;
    }

    protected void OnStazioneClick(ImageClickEventArgs e)
    {
        if (this.StazioneClick != null)
        {
            this.StazioneClick(this, e);
        }
    }

    protected void imgNode_Click(object sender, ImageClickEventArgs e)
    {
        this.OnStazioneClick(e);
    }
    
    private void SetVisualStatus()
    {
    	if ( this.Status == 9 )
    	{
			this.imgNode.ImageUrl = "~/IMG/Linea/stazione_9.gif"; // il maintmode vince su tutto
		}
    	else
    	{
    		if ( this.STLCTotalCount == this.STLCOffline )
    		{
				this.imgNode.ImageUrl = "~/IMG/Linea/stazione_off.gif"; // solo se tutti gli stlc offline
			}
    		else
    		{
				// gli appartenenti ad un gruppo di livello 1 non possono vedere le periferiche in attenzione
				if ( !GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2) && this.Status == 1/* attenzione */ ) this.Status = 0/* ok */;
				if ( this.Status == -1 ) this.Status = 255;
				
				this.imgNode.ImageUrl = string.Format("~/IMG/Linea/stazione_{0}.gif", this.Status);
			}
    	}
	}

}

using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.UI;

public partial class GaugePanel : UserControl
{
    #region Propriet� del controllo

    public string Value
    {
        get { return this.ViewState["Value"].ToString(); }
        set { this.ViewState["Value"] = value; }
    }

    public string ValueRif
    {
        get { return this.ViewState["ValueRif"].ToString(); }
        set { this.ViewState["ValueRif"] = value; }
    }

    public string Delta
    {
        get { return this.ViewState["Delta"].ToString(); }
        set { this.ViewState["Delta"] = value; }
    }

    public string MinValue
    {
        get { return this.ViewState["MinValue"].ToString(); }
        set { this.ViewState["MinValue"] = value; }
    }

    public string MaxValue
    {
        get { return this.ViewState["MaxValue"].ToString(); }
        set { this.ViewState["MaxValue"] = value; }
    }

    public string Header
    {
        get { return this.lblHeader.Text; }
        set { this.lblHeader.Text = value; }
    }

    public string Desc
    {
        get { return this.lblDesc.Text; }
        set { this.lblDesc.Text = extractDesc(value); }
    }

    #endregion

    private float _delta, _val, _rif;

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (this.ViewState.Count > 0)
        {
            this.ShowValue();
        }
    }

    private void ShowValue()
    {
        this.lblValore.Text = this.Value;
        this.lblValoreRif.Text = this.ValueRif;
        this.lblDelta.Text = string.Format("&#x00B1; {0}", this.Delta); // funziona solo su sistemi con cultura Latin
        this.lblValoreMax.Text = this.MaxValue;
        this.lblValoreMin.Text = this.MinValue;
        this.freccia.Visible = false;

        this._delta = extractValue(this.Delta);
        this._val = extractValue(this.Value);
        this._rif = extractValue(this.ValueRif);

        if (this._delta == 0 || this._rif == 0)
        {
            this._rif = (extractValue(this.MaxValue) + extractValue(this.MinValue))/2;
            this._delta = (extractValue(this.MaxValue) - extractValue(this.MinValue))/2;

            this.trDelta.Visible = false;
            this.trRif.Visible = false;

            this.trMax.Visible = true;
            this.trMin.Visible = true;
        }

        if (this._delta == 0)
        {
            this._delta = 0.01F;
        }

        // Il caso in cui il PZ abbia nessun diffusore collegato, l'impedenza assumer� valori molto alti, dalle misurazioni sul campo di: 429496735.90 Ohm e 429496736.00 Ohm
        // Dato che il valore di riferimento ha lo stesso valore rilevato per il PZ, il gauge risulta in stato verde. Con l'espressione regolare configurabile si testa la particolare
        // condizione legata al valore e all'unit� di misura, per forzare l'immagine di "punto interrogativo"
        string regexMaxImpedencePZ = (ConfigurationManager.AppSettings["RegexMaxImpedencePZ"] ?? "429496\\d{3}\\.*\\d{0,2}\\s*Ohm");
        Regex regex = new Regex(regexMaxImpedencePZ,
                                RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.Compiled);

        if (this._delta == 0F || this._rif == 0F || regex.IsMatch(this.Value))
        {
            this.freccia.Visible = false;
            this.divGaugeBg.Style[HtmlTextWriterStyle.BackgroundImage] = "IMG/interfaccia/gauge_d_bg.gif";
        }
        else
        {
            this.freccia.Visible = true;
            this.freccia.Style.Add(HtmlTextWriterStyle.Top, string.Format("{0}px;", GetArrowTopValue(this._val, this._rif, this._delta)));
            this.divGaugeBg.Style[HtmlTextWriterStyle.BackgroundImage] = "IMG/interfaccia/gauge_c_bg.gif";
        }
    }

    private static float GetArrowTopValue(float val, float rif, float delta)
    {
        int top;
        int scostamento = (int) (((val - rif)/delta)*20);

        if (scostamento == int.MinValue)
        {
            scostamento++; // per evitare l'overflow nel calcolo del valore assoluto
        }

        if (Math.Abs(scostamento) <= 50)
        {
            top = (50 + 8) - scostamento;
        }
        else
        {
            top = scostamento > 0 ? 3 : 113;
        }

        return top;
    }

    protected static float extractValue(string value)
    {
        float result = 0;

        if (!string.IsNullOrEmpty(value))
        {
            string temp = value.TrimStart();
            string[] tempValue = temp.Split(' ');

            float.TryParse(tempValue[0], out result);
        }

        return result;
    }

    protected static string extractDesc(string desc)
    {
        string result = string.Empty;
        string[] temp2;
        string[] temp = desc.Split(';');
        if (temp.Length > 0)
        {
            temp2 = temp[0].Split('=');
            if (temp2.Length > 0)
            {
                result = temp2[0];
            }
        }

        return result;
    }
}
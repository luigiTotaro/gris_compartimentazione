using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris.NodeDSTableAdapters;
using GrisSuite.Data.Gris.RegionDSTableAdapters;
using GrisSuite.Data.Gris.ZoneDSTableAdapters;
using Image = System.Web.UI.WebControls.Image;

public partial class Controls_NavigationBar : GrisUserControl
{
    # region Properties

    public MapLayoutMode LayoutMode
    {
        get { return Navigation.CurrentVisualizationMode; }
        set
        {
            Navigation.CurrentVisualizationMode = value;
            this.SetLayoutMode(value);
        }
    }

    public MapLayoutMode OriginalMapLayoutMode
    {
        get { return (MapLayoutMode)(this.ViewState["OriginalMapLayoutMode"] ?? GrisSessionManager.OriginalMapLayoutMode); }
        set
        {
            this.ViewState["OriginalMapLayoutMode"] = value;
            GrisSessionManager.OriginalMapLayoutMode = value;
        }
    }

    public string LinkCompartimentoText
    {
        get { return this.lnkbCompartimento.Text; }
        set { GrisSessionManager.LinkCompartimentoText = this.lnkbCompartimento.Text = value ?? ""; }
    }

    public string LinkCompartimentoTooltip
    {
        get { return this.lnkbCompartimento.ToolTip; }
        set { GrisSessionManager.LinkCompartimentoTooltip = this.lnkbCompartimento.ToolTip = value ?? ""; }
    }

    public string LinkLineaText
    {
        get { return this.lnkbLinea.Text; }
        set { GrisSessionManager.LinkLineaText = this.lnkbLinea.Text = value ?? ""; }
    }

    public string LinkLineaTooltip
    {
        get { return this.lnkbLinea.ToolTip; }
        set { GrisSessionManager.LinkLineaTooltip = this.lnkbLinea.ToolTip = value ?? ""; }
    }

    public string LinkStazioneText
    {
        get { return this.lnkbStazione.Text; }
        set { GrisSessionManager.LinkStazioneText = this.lnkbStazione.Text = value ?? ""; }
    }

    public string LinkStazioneTooltip
    {
        get { return this.lnkbStazione.ToolTip; }
        set { GrisSessionManager.LinkStazioneTooltip = this.lnkbStazione.ToolTip = value ?? ""; }
    }

    private PageDestination _currentNavigationPosition = PageDestination.Italia;

    public PageDestination CurrentNavigationPosition
    {
        get { return this._currentNavigationPosition; }
        set
        {
            this._currentNavigationPosition = value;
            this.NavigationPage.GoToPage(value);
        }
    }

    public bool AreControlsInitialized
    {
        get { return (this.NavigationPage.LayoutMultiView != null && this.NavigationPage.GraphView != null && this.NavigationPage.GridView != null); }
    }

    public void SetCompartimentoSeverita(int severity, string cssBackgroundColor)
    {
        GrisSessionManager.CompartimentoSeverita = severity;
        this.SetObjectSeverity(this.imgCompartimentoSev, severity, cssBackgroundColor);
    }

    public void SetLineaSeverita(int severity, string cssBackgroundColor)
    {
        GrisSessionManager.LineaSeverita = severity;
        this.SetObjectSeverity(this.imgLineaSev, severity, cssBackgroundColor);
    }

    public void SetStazioneSeverita(int severity, string cssBackgroundColor)
    {
        GrisSessionManager.StazioneSeverita = severity;
        this.SetObjectSeverity(this.imgStazioneSev, severity, cssBackgroundColor);
    }

    public bool ClearInformationBarPostVisualization { get; set; }

    # endregion

    # region Events

    public event CommandEventHandler NavigationCommand;

    public event ImageClickEventHandler LayoutSwitched;

    public event ImageClickEventHandler RefreshPageButtonClick;

    # endregion

    # region EventHandlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.tdCenterGoTo.Visible = true;
        this.tdSxGoTo.Visible = true;

        this.trShadow.Visible = true;
        this.trInfoBar1r.Visible = false;
        this.trInfoBar2r.Visible = false;

        // le pagine che includono il controllo di navigazione avranno i PageMethods abilitati per permettere le chiamate ai metodi GoToPage statici.
        if (this.Page != null)
        {
            var scriptManager = ScriptManager.GetCurrent(this.Page);
            if (scriptManager != null)
            {
                scriptManager.EnablePageMethods = true;
            }

            this.ReadNavigationPosition();

            this.RegisterScriptNavigationFunctions();

            if (!this.Page.IsPostBack)
            {
                this.InitializeControlFromSession();

                this.autoCompBoxGoToNode.OnClientItemSelected = "NodeSelected";
                this.autoCompBoxGoToNode.OnClientPopulated = "ShowDisplayNoRowsMessage";
                this.autoCompBoxGoToNode.OnClientHiding = "HideDisplayNoRowsMessage";
            }
        }

        // inietto nella pagina le funzioni javascript necessarie per la visualizzazione del messaggio "Nessun Risultato" del GoTo
        this.RegisterGoToNoResultsClientHandlers();

        if (GrisPermissions.IsUserInAdminGroup())
        {
            this.imgbRefreshPage.Attributes["OnMouseOver"] = "this.src='IMG/interfaccia/btnRefresh_over.gif';";
            this.imgbRefreshPage.Attributes["OnMouseOut"] = "this.src='IMG/interfaccia/btnRefresh_default.gif';";
            this.imgbRefreshPage.ImageUrl = "~/IMG/Interfaccia/btnRefresh_default.gif";
            this.imgbRefreshPage.Enabled = true;
            this.imgbRefreshPage.ToolTip = "Aggiorna Pagina";
        }
        else
        {
            this.imgbRefreshPage.Attributes["OnMouseOver"] = "this.src='IMG/interfaccia/btnEmptyMenu.gif';";
            this.imgbRefreshPage.Attributes["OnMouseOut"] = "this.src='IMG/interfaccia/btnEmptyMenu.gif';";
            this.imgbRefreshPage.ImageUrl = "~/IMG/Interfaccia/btnEmptyMenu.gif";
            this.imgbRefreshPage.Enabled = false;
            this.imgbRefreshPage.ToolTip = string.Empty;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.lnkbItalia.ToolTip = "Pagina Nazionale";

        if (this.OriginalMapLayoutMode == MapLayoutMode.Graph)
        {
            this.lnkbItalia.Text = "Torna alla mappa";
        }
        else if (this.OriginalMapLayoutMode == MapLayoutMode.Grid)
        {
            this.lnkbItalia.Text = "Intera Rete";
        }
        else if (this.OriginalMapLayoutMode == MapLayoutMode.None)
        {
            this.lnkbItalia.Text = (Navigation.CurrentVisualizationMode == MapLayoutMode.Graph) ? "Torna alla mappa" : "Intera Rete";
        }

        switch (Navigation.CurrentVisualizationMode)
        {
            case MapLayoutMode.Grid:
                this.tblNavLink.Visible = true;
                break;
            case MapLayoutMode.Graph:
                break;
        }

        this.LoadInformationBarData();

        this.SetObjectSeverity(this.imgCompartimentoSev, GrisSessionManager.CompartimentoSeverita, string.Empty);
        this.SetObjectSeverity(this.imgLineaSev, GrisSessionManager.LineaSeverita, string.Empty);
        this.SetObjectSeverity(this.imgStazioneSev, GrisSessionManager.StazioneSeverita, string.Empty);

        this.ManageMenuButton();
    }

    private void ManageMenuButton()
    {
        this.SetMenuItemGroupsAndFormulasVisibility(true);
        this.SetMenuItemAckReportVisibility(true);
        this.SetMenuItemGetaVisibility(true);

        switch (this._currentNavigationPosition)
        {
            case PageDestination.Italia:
                this.SetMenuItemAckReportVisibility(false);
                break;
            case PageDestination.Region:
                this.SetMenuItemAckReportVisibility(false);
                break;
            case PageDestination.Zone:
                this.SetMenuItemAckReportVisibility(false);
                break;
            case PageDestination.Node:
                this.SetMenuItemGroupsAndFormulasVisibility(false);
                this.SetMenuItemAckReportVisibility(false);
                this.SetMenuItemGetaVisibility(false);
                break;
            case PageDestination.Device:
                this.SetMenuItemAckReportVisibility(false);
                this.SetMenuItemGetaVisibility(false);
                break;
        }

        this.SetMenuButtonVisibility();
    }

    private void SetMenuItemGroupsAndFormulasVisibility(bool forceHide)
    {
        if (forceHide)
        {
            this.lnkMenuItemGroupsAndFormulas.Visible = false;
        }
        else
        {
            if (GrisPermissions.IsUserInAdminGroup() ||
                GrisPermissions.IsUserInLocalDeviceGroupingOperatorsGroup((RegCodes)this.NavigationPage.RegID) ||
                GrisPermissions.IsUserInLocalAdminGroup((RegCodes)this.NavigationPage.RegID))
            {
                this.lnkMenuItemGroupsAndFormulas.Visible = true;
            }
            else
            {
                this.lnkMenuItemGroupsAndFormulas.Visible = false;
            }
        }
    }

    private void SetMenuItemAckReportVisibility(bool forceHide)
    {
        if (forceHide)
        {
            this.lnkMenuItemAckReport.Visible = false;
        }
        else
        {
            this.ackList.RegID = this.NavigationPage.RegID;
            this.ackList.ZonID = this.NavigationPage.ZonID;
            this.ackList.NodID = this.NavigationPage.NodID;
            this.ackList.DevID = this.NavigationPage.DevID;
            this.ackList.ReBind();
            this.lnkMenuItemAckReport.Visible = this.ackList.HasDataToShow;
        }
    }

    private void SetMenuItemGetaVisibility(bool forceHide)
    {
        if (forceHide)
        {
            this.lnkMenuItemGeta.Visible = false;
        }
        else
        {
            if (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
            {
                string GETAUrltest = ConfigurationManager.AppSettings.Get("GETAUrl");
                if (string.IsNullOrEmpty(GETAUrltest) == false)
                    {
                        this.lnkMenuItemGeta.Visible = true;
                        this.lnkMenuItemGeta.Attributes.Add("OnClick", GUtility.GetGetaUrl(false));
                    }
            }
            else
            {
                this.lnkMenuItemGeta.Visible = false;
            }
        }
    }

    private void SetMenuButtonVisibility()
    {
        if (this.lnkMenuItemGroupsAndFormulas.Visible || this.lnkMenuItemAckReport.Visible || this.lnkMenuItemGeta.Visible)
        {
            #region Gestione dimensioni sfondo menu tools, in base a numero di link visualizzati
            int rows = 0;

            if (this.lnkMenuItemGroupsAndFormulas.Visible)
            {
                rows++;
            }

            if (this.lnkMenuItemAckReport.Visible)
            {
                rows++;
            }

            if (this.lnkMenuItemGeta.Visible)
            {
                rows++;
            }

            if (rows == 3)
            {
                this.pnlMenu.Attributes["style"] = "height:114px;background-image:url(IMG/Interfaccia/menu_background3.png);";
            }
            else if (rows == 2)
            {
                this.pnlMenu.Attributes["style"] = "height:79px;background-image:url(IMG/Interfaccia/menu_background2.png);";
            }
            else if (rows == 1)
            {
                this.pnlMenu.Attributes["style"] = "height:48px;background-image:url(IMG/Interfaccia/menu_background1.png);";
            }
            #endregion

            this.imgbMenu.Attributes["OnMouseOver"] = "this.src='IMG/interfaccia/btnMenu_over.gif';";
            this.imgbMenu.Attributes["OnMouseOut"] = "this.src='IMG/interfaccia/btnMenu.gif';";
            this.imgbMenu.ImageUrl = "~/IMG/Interfaccia/btnMenu.gif";
            this.imgbMenu.Enabled = true;
        }
        else
        {
            this.imgbMenu.Attributes["OnMouseOver"] = "this.src='IMG/interfaccia/btnEmptyMenu.gif';";
            this.imgbMenu.Attributes["OnMouseOut"] = "this.src='IMG/interfaccia/btnEmptyMenu.gif';";
            this.imgbMenu.ImageUrl = "~/IMG/Interfaccia/btnEmptyMenu.gif";
            this.imgbMenu.Enabled = false;
        }
    }

    protected void lnkMenuItem_Command(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "GroupsAndFormulas":
                break;
            case "AckReport":
                if (this.ackList.HasDataToShow)
                {
                    this.popupAck.Show();
                }
                break;
            case "Geta":
                break;
        }

        this.popexMenu.Cancel();
    }

    private void SetObjectSeverity(Controls_LayeredIcon image, int severity, string cssBackgroundColor)
    {
        if (string.IsNullOrEmpty(cssBackgroundColor))
        {
            // gestiamo solo le severit� per cui esistono le immagini. Tutte le severit� impreviste non appaiono
            switch (severity)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 9:
                    image.Visible = true;
                    image.Status = severity;
                    image.Update();
                    break;
                default:
                    image.Visible = false;
                    break;
            }
        }
        else
        {
            image.Visible = true;
            image.CssBackgroundColor = cssBackgroundColor;
            image.Update();
        }
    }

    protected void LinkBtn_Command(object sender, CommandEventArgs e)
    {
        this.OnNavigationCommand(e);

        this.ClearInformationBarData();

        if (this.IsHostedInNavigationPage)
        {
            if (this.OriginalMapLayoutMode != MapLayoutMode.None)
            {
                this.SetCurrentVisualizationMode(this.OriginalMapLayoutMode);
                this.OriginalMapLayoutMode = MapLayoutMode.None;
            }

            string destination = e.CommandName;
            switch (destination)
            {
                case "Italia":
                {
                    this.CurrentNavigationPosition = PageDestination.Italia;
                    break;
                }
                case "Compartimento":
                {
                    this.CurrentNavigationPosition = PageDestination.Region;
                    break;
                }
                case "Linea":
                {
                    this.CurrentNavigationPosition = PageDestination.Zone;
                    break;
                }
                case "Stazione":
                {
                    this.CurrentNavigationPosition = PageDestination.Node;
                    break;
                }
                default:
                {
                    this.CurrentNavigationPosition = PageDestination.Italia;
                    break;
                }
            }
        }
    }

    protected void imgbSwitchLayout_Click(object sender, ImageClickEventArgs e)
    {
        this.ChangeCurrentVisualizationMode();
        this.OriginalMapLayoutMode = this.LayoutMode;

        this.OnLayoutSwitched(e);
    }

    private void ChangeCurrentVisualizationMode()
    {
        this.LayoutMode = this.LayoutMode == MapLayoutMode.Graph ? MapLayoutMode.Grid : MapLayoutMode.Graph;
        Navigation.CurrentVisualizationMode = this.LayoutMode;
    }

    private void SetCurrentVisualizationMode(MapLayoutMode mapLayoutMode)
    {
        this.LayoutMode = mapLayoutMode;
        Navigation.CurrentVisualizationMode = this.LayoutMode;
    }

    protected void imgbRefreshPage_Click(object sender, ImageClickEventArgs e)
    {
        this.OnRefreshPageButtonClick(e);

        if (this.imgbRefreshPage.ImageUrl == "~/IMG/Interfaccia/btnRefresh_default.gif")
        {
            this.imgbRefreshPage.ImageUrl = "~/IMG/Interfaccia/btnRefresh_ok.gif";
        }
    }

    # endregion

    # region Methods

    protected void OnNavigationCommand(CommandEventArgs e)
    {
        if (this.NavigationCommand != null)
        {
            this.NavigationCommand(this, e);
        }
    }

    protected void OnLayoutSwitched(ImageClickEventArgs e)
    {
        if (this.LayoutSwitched != null)
        {
            this.LayoutSwitched(this, e);
        }
    }

    protected void OnRefreshPageButtonClick(ImageClickEventArgs e)
    {
        if (this.RefreshPageButtonClick != null)
        {
            this.RefreshPageButtonClick(this, e);
        }
    }

    private void ReadNavigationPosition()
    {
        if (this.IsHostedInNavigationPage)
        {
            GUtility.EnableLinkButton(this.Page, this.lnkbItalia, true);
            GUtility.EnableLinkButton(this.Page, this.lnkbCompartimento, true);
            GUtility.EnableLinkButton(this.Page, this.lnkbLinea, true);
            GUtility.EnableLinkButton(this.Page, this.lnkbStazione, true);

            if (this.NavigationPage.DevID != -1)
            {
                this._currentNavigationPosition = PageDestination.Device;
                SetCurrentPositionLayout(null, null);
                return;
            }

            if (this.NavigationPage.NodID != -1)
            {
                this._currentNavigationPosition = PageDestination.Node;
                SetCurrentPositionLayout(this.lnkbStazione, null);
                return;
            }

            if (this.NavigationPage.ZonID != -1)
            {
                // rimuovo i dati della stazione dalla sessione
                GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Node);
                this._currentNavigationPosition = PageDestination.Zone;
                SetCurrentPositionLayout(this.lnkbLinea, this.sep3);
                return;
            }

            if (this.NavigationPage.RegID != -1)
            {
                // rimuovo i dati della linea dalla sessione
                GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Zone);
                this._currentNavigationPosition = PageDestination.Region;
                SetCurrentPositionLayout(this.lnkbCompartimento, this.sep2, this.sep3);
                return;
            }

            GrisSessionManager.RemoveNavigationGroup(GrisSessionGroup.Region);
            this._currentNavigationPosition = PageDestination.Italia;
            SetCurrentPositionLayout(this.lnkbItalia, this.sep1, this.sep2, this.sep3);
            this.lnkbCompartimento.Visible = false;
            this.imgCompartimentoSev.Visible = false;
            this.tblNavLink.Visible = false;
        }
    }

    private static void SetCurrentPositionLayout(WebControl link, params Image[] hideSeparators)
    {
        SetDisabledLayout(link);

        if (hideSeparators != null)
        {
            foreach (Image sep in hideSeparators)
            {
                sep.Visible = false;
            }
        }
    }

    private static void SetDisabledLayout(WebControl link)
    {
        if (link != null)
        {
            link.Attributes["OnClick"] = "return false;";
            link.Attributes["Style"] = "cursor:default;";
            link.ForeColor = Color.FromName("#4c4c4c");
            link.Attributes.Remove("OnMouseOver");
            link.Attributes.Remove("OnMouseOut");
        }
    }

    private void SetLayoutMode(MapLayoutMode layout)
    {
        GUtility.EnableImageButton(this.Page, this.imgbSwitchLayout,
                                   (this.IsHostedInNavigationPage && (this.AreControlsInitialized || (this.LayoutSwitched != null))));

        switch (layout)
        {
            case MapLayoutMode.Graph:
            {
                if (this.NavigationPage.DevID != -1)
                {
                    this.imgbSwitchLayout.ImageUrl = "~/IMG/interfaccia/btnEmptyMenu.gif";
                    this.imgbSwitchLayout.Enabled = false;
                    this.imgbSwitchLayout.Attributes.Remove("OnMouseOver");
                    this.imgbSwitchLayout.Attributes.Remove("OnMouseOut");
                }
                else
                {
                    this.imgbSwitchLayout.ImageUrl = "~/IMG/interfaccia/nav_btn_viewtab.gif";
                    this.imgbSwitchLayout.ToolTip = "Modalit� Tabellare";
                    this.imgbSwitchLayout.Attributes["OnMouseOver"] = "this.src = 'IMG/interfaccia/nav_btn_viewtab_over.gif';";
                    this.imgbSwitchLayout.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/nav_btn_viewtab.gif';";
                }

                if (this.IsHostedInNavigationPage && this.AreControlsInitialized)
                {
                    this.NavigationPage.LayoutMultiView.SetActiveView(this.NavigationPage.GraphView);
                }

                break;
            }
            case MapLayoutMode.Grid:
            {
                if ((this.CurrentNavigationPosition == PageDestination.Device) || (this.CurrentNavigationPosition == PageDestination.Node))
                {
                    if (this.NavigationPage.DevID != -1)
                    {
                        this.imgbSwitchLayout.ImageUrl = "~/IMG/interfaccia/btnEmptyMenu.gif";
                        this.imgbSwitchLayout.Enabled = false;
                        this.imgbSwitchLayout.Attributes.Remove("OnMouseOver");
                        this.imgbSwitchLayout.Attributes.Remove("OnMouseOut");
                    }
                    else
                    {
                        this.imgbSwitchLayout.ImageUrl = "~/IMG/interfaccia/nav_btn_viewgraf.gif";
                        this.imgbSwitchLayout.ToolTip = "Modalit� Grafica";
                        this.imgbSwitchLayout.Attributes["OnMouseOver"] = "this.src = 'IMG/interfaccia/nav_btn_viewgraf_over.gif';";
                        this.imgbSwitchLayout.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/nav_btn_viewgraf.gif';";
                    }
                }
                else
                {
                    this.imgbSwitchLayout.ImageUrl = "~/IMG/interfaccia/nav_btn_viewmap.gif";
                    this.imgbSwitchLayout.ToolTip = "Modalit� Grafica";
                    this.imgbSwitchLayout.Attributes["OnMouseOver"] = "this.src = 'IMG/interfaccia/nav_btn_viewmap_over.gif';";
                    this.imgbSwitchLayout.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/nav_btn_viewmap.gif';";
                }

                if (this.IsHostedInNavigationPage && this.AreControlsInitialized)
                {
                    this.NavigationPage.LayoutMultiView.SetActiveView(this.NavigationPage.GridView);
                }

                break;
            }
            default:
            {
                this.imgbSwitchLayout.ImageUrl = "~/IMG/interfaccia/nav_btn_viewgraf.gif";
                this.imgbSwitchLayout.ToolTip = "Modalit� Grafica";
                this.imgbSwitchLayout.Attributes["OnMouseOver"] = "this.src = 'IMG/interfaccia/nav_btn_viewgraf_over.gif';";
                this.imgbSwitchLayout.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/nav_btn_viewgraf.gif';";

                if (this.IsHostedInNavigationPage && this.AreControlsInitialized)
                {
                    this.NavigationPage.LayoutMultiView.SetActiveView(this.NavigationPage.GridView);
                }

                break;
            }
        }

        this.updNavToolbar.Update();
    }

    private void SetInformationBar(InformationBarIcon firstLineIcon, string firstLineMessage, InformationBarIcon secondLineIcon,
                                   string secondLineMessage)
    {
        if ((!string.IsNullOrEmpty(firstLineMessage)) && (!string.IsNullOrEmpty(secondLineMessage)))
        {
            this.trShadow.Visible = false;
            this.trInfoBar1r.Visible = false;
            this.trInfoBar2r.Visible = true;

            this.imgIBIcon21.ImageUrl = this.GetInformationBarIconImageUrl(firstLineIcon);
            this.lblIBMessage21.Text = firstLineMessage;
            this.imgIBIcon22.ImageUrl = this.GetInformationBarIconImageUrl(secondLineIcon);
            this.lblIBMessage22.Text = secondLineMessage;
        }
        else if ((!string.IsNullOrEmpty(firstLineMessage)) && (string.IsNullOrEmpty(secondLineMessage)))
        {
            this.trShadow.Visible = false;
            this.trInfoBar1r.Visible = true;
            this.trInfoBar2r.Visible = false;

            this.imgIBIcon1.ImageUrl = this.GetInformationBarIconImageUrl(firstLineIcon);
            this.lblIBMessage1.Text = firstLineMessage;
        }
        else if ((string.IsNullOrEmpty(firstLineMessage)) && (!string.IsNullOrEmpty(secondLineMessage)))
        {
            this.trShadow.Visible = false;
            this.trInfoBar1r.Visible = true;
            this.trInfoBar2r.Visible = false;

            this.imgIBIcon1.ImageUrl = this.GetInformationBarIconImageUrl(secondLineIcon);
            this.lblIBMessage1.Text = secondLineMessage;
        }
    }

    private void LoadInformationBarData()
    {
        InformationBarIcon firstLineIcon = GrisSessionManager.InformationBarFirstLineIcon;
        string firstLineMessage = GrisSessionManager.InformationBarFirstLineMessage;
        InformationBarIcon secondLineIcon = GrisSessionManager.InformationBarSecondLineIcon;
        string secondLineMessage = GrisSessionManager.InformationBarSecondLineMessage;

        if (string.IsNullOrEmpty(firstLineMessage))
        {
            firstLineIcon = (InformationBarIcon)(this.ViewState["FirstLineIcon"] ?? InformationBarIcon.None);
            firstLineMessage = (string)(this.ViewState["FirstLineMessage"] ?? string.Empty);
        }

        if (string.IsNullOrEmpty(secondLineMessage))
        {
            secondLineIcon = (InformationBarIcon)(this.ViewState["SecondLineIcon"] ?? InformationBarIcon.None);
            secondLineMessage = (string)(this.ViewState["SecondLineMessage"] ?? string.Empty);
        }

        this.SetInformationBar(firstLineIcon, firstLineMessage, secondLineIcon, secondLineMessage);

        if (this.ClearInformationBarPostVisualization)
        {
            this.ClearInformationBarData();
        }
    }

    public void UpdateInformationBarData()
    {
        this.LoadInformationBarData();
    }

    public void SetInformationBarData(InformationBarIcon firstLineIcon, string firstLineMessage, InformationBarIcon secondLineIcon,
                                      string secondLineMessage)
    {
        GrisSessionManager.InformationBarFirstLineIcon = firstLineIcon;
        GrisSessionManager.InformationBarFirstLineMessage = firstLineMessage;
        GrisSessionManager.InformationBarSecondLineIcon = secondLineIcon;
        GrisSessionManager.InformationBarSecondLineMessage = secondLineMessage;

        this.ViewState["FirstLineIcon"] = firstLineIcon;
        this.ViewState["FirstLineMessage"] = firstLineMessage;
        this.ViewState["SecondLineIcon"] = secondLineIcon;
        this.ViewState["SecondLineMessage"] = secondLineMessage;
    }

    public void SetInformationBarData(InformationBarIcon firstLineIcon, string firstLineMessage)
    {
        this.SetInformationBarData(firstLineIcon, firstLineMessage, InformationBarIcon.None, null);
    }

    public void ClearInformationBarData()
    {
        this.SetInformationBarData(InformationBarIcon.None, null, InformationBarIcon.None, null);
    }

    private string GetInformationBarIconImageUrl(InformationBarIcon type)
    {
        switch (type)
        {
            case InformationBarIcon.Exclamation:
                return "~/IMG/Interfaccia/exclamation.png";
            case InformationBarIcon.Info:
                return "~/IMG/Interfaccia/info.png";
            default:
                return "~/IMG/interfaccia/ibempty.png";
        }
    }

    public void EvaluateCurrentLayoutModeByDevicesNumber(int totalDevicesCount)
    {
        this.ClearInformationBarData();

        if (totalDevicesCount > DBSettings.GetTabularViewClippingRows())
        {
            if (this.LayoutMode == MapLayoutMode.Graph)
            {
                this.ChangeCurrentVisualizationMode();
                this.SetInformationBarData(InformationBarIcon.Info, ConfigurationManager.AppSettings["TabularViewClippingRowsMessage"]);

                if (this.OriginalMapLayoutMode == MapLayoutMode.None)
                {
                    this.OriginalMapLayoutMode = MapLayoutMode.Graph;
                }
            }
        }
        else
        {
            if (this.LayoutMode == MapLayoutMode.Grid)
            {
                this.ChangeCurrentVisualizationMode();

                if (this.OriginalMapLayoutMode == MapLayoutMode.None)
                {
                    this.OriginalMapLayoutMode = MapLayoutMode.Grid;
                }
            }
        }
    }

    public void ResetRefreshButtonImage()
    {
        if ((this.imgbRefreshPage.ImageUrl == "~/IMG/Interfaccia/btnRefresh_default.gif") || (this.imgbRefreshPage.ImageUrl == "~/IMG/Interfaccia/btnRefresh_ok.gif"))
        {
            this.imgbRefreshPage.ImageUrl = "~/IMG/Interfaccia/btnRefresh_default.gif";
        }
    }

    # region Client Navigation

    private void RegisterScriptNavigationFunctions()
    {
        const string scriptName = "ScriptNavigationFunctions";
        const string scriptFunctions =
            @"function NodeSelected(source,eventArgs){AreaRedirect('node',eventArgs.get_value());}
function AreaRedirect(targetArea,identifier){PageMethods.ClientGoToPage(targetArea,identifier,ExecuteRedirect);}
function ExecuteRedirect(results,context,methodName){window.location.href=results;}";

        if (!this.Page.ClientScript.IsClientScriptBlockRegistered(scriptName))
        {
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptName, scriptFunctions, true);
        }
    }

    # endregion

    private void RegisterGoToNoResultsClientHandlers()
    {
        const string scriptName = "GoToNoResultsClientHandlers";
        string scriptFunctions =
            @"function ShowDisplayNoRowsMessage(){var autoCompEx=$find('AutoCompleteEx');if(autoCompEx&&autoCompEx._completionListElement.children.length>0){$get('spnNoRowsMessage').style.display='none'}else{$get('spnNoRowsMessage').style.display='inline'}}
            function HideDisplayNoRowsMessage(){var autoCompEx=$find('AutoCompleteEx');var waterMEx=$find('" +
            this.txtBoxGoToNodeWM.ClientID +
            "');if(!autoCompEx._textBoxHasFocus&&autoCompEx._selectIndex<0){waterMEx._applyWatermark();autoCompEx._currentPrefix='';$get('spnNoRowsMessage').style.display='none';return}if(autoCompEx&&autoCompEx._currentPrefix&&autoCompEx._currentPrefix.length>autoCompEx._minimumPrefixLength){if(autoCompEx._selectIndex>=0){$get('spnNoRowsMessage').style.display='none'}else{$get('spnNoRowsMessage').style.display='inline'}}else{$get('spnNoRowsMessage').style.display='none'}}";

        if (!this.Page.ClientScript.IsClientScriptBlockRegistered(scriptName))
        {
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptName, scriptFunctions, true);
        }
    }

    private void InitializeControlFromSession()
    {
        this.SetLayoutMode(this.LayoutMode);

        this.LinkCompartimentoText =
            GrisSessionManager.Session.Get(GrisSessionManager.LinkCompartimentoTextKey, this.GetRegionName, "").Replace("Compartimento di ", "");
        this.LinkCompartimentoTooltip = GrisSessionManager.LinkCompartimentoTooltip;
        this.LinkLineaText = GrisSessionManager.Session.Get(GrisSessionManager.LinkLineaTextKey, this.GetZoneName, "").Replace("Linea ", "");
        this.LinkLineaTooltip = GrisSessionManager.LinkLineaTooltip;
        this.LinkStazioneText = this.GetNodeName() ?? string.Empty;
        this.LinkStazioneTooltip = this.LinkStazioneText;
    }

    private string GetRegionName()
    {
        if (this.IsHostedInNavigationPage && this.NavigationPage.RegID != -1)
        {
            RegionTableAdapter ta = new RegionTableAdapter();
            return ta.GetRegionNameByID(this.NavigationPage.RegID).ToString();
        }

        return null;
    }

    private string GetZoneName()
    {
        if (this.IsHostedInNavigationPage && this.NavigationPage.ZonID != -1)
        {
            ZoneTableAdapter ta = new ZoneTableAdapter();
            return ta.GetLineaNameByID(this.NavigationPage.ZonID).ToString();
        }

        return null;
    }

    private string GetNodeName()
    {
        if (this.IsHostedInNavigationPage && this.NavigationPage.NodID != -1)
        {
            NodeTableAdapter ta = new NodeTableAdapter();

			if (this.NavigationPage.DevID != -1)
	        {
		        // Sulla pagina della periferica, si visualizza il nome della stazione con il relativo nome del nodo con il server che la remotizza
				return ta.GetNodeNameAndRemotizedByDevice(this.NavigationPage.DevID);
			}

			// Visualizzazione normale del nome della stazione
			return ta.GetNodeNameAndRemotizedByID(this.NavigationPage.NodID);
        }

        return null;
    }

    # endregion
}
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectorsPanel.ascx.cs" Inherits="SelectorsPanel" %>
<asp:datalist id="dlstSystems" runat="server" datakeyfield="SystemID" datasourceid="odsSystems" repeatdirection="Horizontal" onitemdatabound="dlstSystems_ItemDataBound" style="margin: 0 auto;">
	<itemtemplate>
		<asp:imagebutton id="imgbSystem" runat="server" commandname="system" commandargument='<%# Eval("SystemID") + "," + Eval("RawTotalDevices") + "," + Eval("NodeSystemsId") %>' tooltip='<%# string.Format("{0} ({1} {2})", Eval("SystemDescription"), Eval("RawTotalDevices"), Convert.ToInt32(Eval("RawTotalDevices")) == 1 ? "periferica monitorata" : "periferiche monitorate") %>' oncommand="imgbSystem_Click" />
	</itemtemplate>
</asp:datalist>
<asp:objectdatasource id="odsSystems" runat="server" oldvaluesparameterformatstring="original_{0}"
	selectmethod="GetData" typename="GrisSuite.Data.Gris.SystemsInfoDSTableAdapters.SystemTableAdapter" onselecting="odsSystems_Selecting">
	<selectparameters>
		<asp:Parameter Name="NodID" Type="Int64" />
		<asp:Parameter Name="FilterSystemID" Type="Int32" />
	</selectparameters>
</asp:objectdatasource>

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;

public partial class Controls_AckList : UserControl
{
    public long RegID
    {
        get { return Convert.ToInt64(this.ViewState["RegID"] ?? -1); }

        set { this.ViewState["RegID"] = value; }
    }

    public long ZonID
    {
        get { return Convert.ToInt64(this.ViewState["ZonID"] ?? -1); }
        set { this.ViewState["ZonID"] = value; }
    }

    public long NodID
    {
        get { return Convert.ToInt64(this.ViewState["NodID"] ?? -1); }
        set { this.ViewState["NodID"] = value; }
    }

    public long DevID
    {
        get { return Convert.ToInt64(this.ViewState["DevID"] ?? -1); }
        set { this.ViewState["DevID"] = value; }
    }

    public void ReBind()
    {
        this.grdAcks.DataBind();
    }

    public event EventHandler DataUpdated;

    protected void OnDataUpdated(EventArgs e)
    {
        if (this.DataUpdated != null)
        {
            this.DataUpdated(this, e);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {}

    protected void sdsAcks_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@RegIDs"].Value = string.Format("|{0}|", string.Join("|",
                                                                                   GrisPermissions.VisibleRegions(PermissionLevel.Level2).Select(
                                                                                       id => id.ToString(CultureInfo.InvariantCulture)).ToArray()));

        e.Command.Parameters["@RegID"].Value = this.RegID;
        e.Command.Parameters["@ZonID"].Value = this.ZonID;
        e.Command.Parameters["@NodID"].Value = this.NodID;
        e.Command.Parameters["@DevID"].Value = this.DevID;
    }

    protected void grdAcks_DataBound(object sender, EventArgs e)
    {
        this.OnDataUpdated(e);
    }

    public bool HasDataToShow
    {
        get
        {
            if (this.grdAcks.Rows.Count > 0)
            {
                return true;
            }

            this.grdAcks.DataBind();
            return (this.grdAcks.Rows.Count > 0);
        }
    }
}
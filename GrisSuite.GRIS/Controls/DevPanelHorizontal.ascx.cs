﻿using System;
using System.Data.SqlClient;
using System.Web.UI;
using Controllers;
using GrisSuite.Common;
using GrisSuite.Data.Gris.DevicesDSTableAdapters;

public partial class DevPanelHorizontal : GrisUserControl
{
    public event EventHandler ChangeMaintenance;

    #region Properties

    public string Name
    {
        get { return this.lblNome.Text; }

        set { this.lblNome.Text = value; }
    }

    public string Type
    {
        get { return this.lblType.Text; }

        set { this.lblType.Text = value; }
    }

    public string RawType
    {
        set
        {
            if ((value.StartsWith("INFSTAZ", StringComparison.OrdinalIgnoreCase)) && (!this.Type.StartsWith("INFSTAZ")))
            {
                this.lblRawType.Text = "(Infostazioni)";
                this.lblRawType.Visible = true;
            }
            else
            {
                this.lblRawType.Visible = false;
            }
        }
    }

    public string Host
    {
        get { return (this.ViewState["Host"] ?? "").ToString(); }

        set { this.ViewState["Host"] = value; }
    }

    public string Address
    {
        get { return this.lblAddress.Text; }

        set { this.lblAddress.Text = value; }
    }

    public string SerialNumber
    {
        get { return this.lblSN.Text; }

        set { this.lblSN.Text = value; }
    }

    public string Building
    {
        get { return (this.ViewState["Building"] ?? "").ToString(); }

        set { this.ViewState["Building"] = value; }
    }

    public string Rack
    {
        get { return (this.ViewState["Rack"] ?? "").ToString(); }

        set { this.ViewState["Rack"] = value; }
    }

    public string BuildingDescription
    {
        get { return (this.ViewState["BuildingDescription"] ?? "").ToString(); }

        set { this.ViewState["BuildingDescription"] = value; }
    }

    public string RackDescription
    {
        get { return (this.ViewState["RackDescription"] ?? "").ToString(); }

        set { this.ViewState["RackDescription"] = value; }
    }

    public string PortName
    {
        get { return this.lblPortName.Text; }

        set { this.lblPortName.Text = value; }
    }

    public string PortType
    {
        get { return this.lblPortType.Text; }

        set { this.lblPortType.Text = value; }
    }

    public string FullHostName
    {
        get { return (this.ViewState["FullHostName"] ?? "").ToString(); }

        set { this.ViewState["FullHostName"] = value; }
    }

    public string Vendor
    {
        get { return this.lblVendor.Text; }

        set { this.lblVendor.Text = value; }
    }

    public string WSUrlPattern
    {
        get { return (this.ViewState["WSUrlPattern"] ?? "").ToString(); }

        set { this.ViewState["WSUrlPattern"] = value; }
    }

    public long DevID
    {
        get { return Convert.ToInt64(this.ViewState["DevID"] ?? -1); }
        set { this.ViewState["DevID"] = value; }
    }

    public int SeverityLevel
    {
        get { return Convert.ToInt32(this.ViewState["SeverityLevel"] ?? -1); }
        set { this.ViewState["SeverityLevel"] = value; }
    }

    public int SeverityLevelDetail
    {
        get { return Convert.ToInt32(this.ViewState["SeverityLevelDetail"] ?? -1); }
        set { this.ViewState["SeverityLevelDetail"] = value; }
    }

    public string SeverityLevelDescription
    {
        get
        {
            //return ( this.ViewState["SeverityLevelDescription"] ?? "" ).ToString();
            return this.lblSeverityDescription.Text;
        }
        set
        {
            this.lblSeverityDescription.Text = value;
            //this.ViewState["SeverityLevelDescription"] = value;
        }
    }

    public int SystemID
    {
        get { return Convert.ToInt32(this.ViewState["SystemID"] ?? -1); }
        set { this.ViewState["SystemID"] = value; }
    }

    public string Ticket
    {
        get { return this.lblTicket.Text; }
        set
        {
            this.lblTicket.Text = value;

            bool showTicketLabel = (!string.IsNullOrEmpty(value) && GrisPermissions.CanUserViewAlerts());
            this.trTicket.Visible = showTicketLabel;
            this.trTicketValue.Visible = showTicketLabel;
        }
    }

    public long SrvID
    {
        set
        {
            if (this.Type == "STLC1000")
            {
                this.lblServerSNValue.Text = string.Format("({0})", Utility.GetSNFromSrvID((ulong)value));
                this.lblServerSNValue.Visible = true;
            }
            else
            {
                this.lblServerSNValue.Visible = false;
            }
        }
    }

    public bool OperatorMaintenance
    {
        get { return Convert.ToBoolean(this.ViewState["OperatorMaintenance"] ?? false); }
        set { this.ViewState["OperatorMaintenance"] = value; }
    }

    public int RackPositionRow
    {
        get { return Convert.ToInt32(this.ViewState["RackPositionRow"] ?? -1); }
        set { this.ViewState["RackPositionRow"] = value; }
    }

    public int RackPositionCol
    {
        get { return Convert.ToInt32(this.ViewState["RackPositionCol"] ?? -1); }
        set { this.ViewState["RackPositionCol"] = value; }
    }

    public string DeviceImage
    {
        get { return (this.ViewState["DeviceImage"] ?? "").ToString(); }
        set { this.ViewState["DeviceImage"] = value; }
    }

    #endregion

    # region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.imgScreenDump.Attributes["OnMouseOver"] = "this.src = 'IMG/Interfaccia/perif_info_over.gif'";
        this.imgScreenDump.Attributes["OnMouseOut"] = "this.src = 'IMG/Interfaccia/perif_info.gif'";

        this.imgListenDump.Attributes["OnMouseOver"] = "this.src = 'IMG/Interfaccia/arm_play_over.gif'";
        this.imgListenDump.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/arm_play.gif'";

        if (!this.IsPostBack)
        {
            this.pnlNebbia.Visible = false;
        }

        if (GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level3))
        {
            if (this.Page.Request.Browser.Type == "IE7")
            {
                this.pnlNebbia.Style[HtmlTextWriterStyle.Height] = "100%";
            }
            else // internet explorer 6 non applica l'altezza al 100%
            {
                if (ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
                {
                    // se è un postback asincrono l'evento onload dell'immagine non viene scatenato, è necessario iniettare lo script
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tendina" + this.ClientID,
                                                        String.Format(
                                                            "if($get('{0}')) {{$get('{0}').style.height = $get('{1}').offsetTop + 'px';}}",
                                                            this.pnlNebbia.ClientID, this.pnlTopLimit.ClientID), true);
                }
                else
                {
                    this.imgDeviceType.Attributes["onload"] =
                        string.Format("if($get('{0}')) {{$get('{0}').style.height = $get('{1}').offsetTop + 'px';}}",
                                      this.pnlNebbia.ClientID, this.pnlTopLimit.ClientID);
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SetDeviceImage();
        this.SetDeviceStatus();

        this.imgDeviceType.ToolTip = "";

        GUtility.EnableImageButton(this.Page, this.btnInMaintenance, Device.CanUserChangeMaintenanceMode());

        // nella versione 1.3.0.0 viene temporaneamente disabilitata l'ombreggiatura sulle periferiche monitorate da un STLC1000 offline.
		//this.pnlNebbia.Visible = this.STLC1000Offline && !GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level3);
        this.pnlNebbia.Visible = false;

        // il pulsante di visualizzazione dello screen dump viene visualizzato solo per le periferiche monitor.
        if (this.SystemID == 2)
        {
            // nel caso che la URL del web service non sia recuperabile per il monitor
            if ((!string.IsNullOrEmpty(this.WSUrlPattern)))
            {
            	this.imgScreenDump.Visible = ScreenDump.CanUserViewScreenDump(this.NavigationPage.RegID, this.SeverityLevel);
            }
            else
            {
                this.imgScreenDump.Visible = false;
            }
        }
        else
        {
            this.imgScreenDump.Visible = false;
        }
        //Screen dump sempre invisibile.... per adesso
        //this.imgScreenDump.Visible = false;

        if (this.imgScreenDump.Visible)
        {
            this.imgScreenDump.OnClientClick = Device.BuildScreenDumpUrl(this.DevID);
        }
        //if (this.Type == DBSettings.GetAwgClientDeviceType()) // "WRKPDL000")
        if (this.Type.StartsWith(DBSettings.GetAwgClientDeviceType())) 
        {
            this.imgListenDump.Visible = StationListen.CanUserViewStationListen(this.NavigationPage.RegID, this.SeverityLevel);
            this.imgListenDump.OnClientClick = Device.BuildListenDumpUrl(this.DevID);
        }
        else
        {
            this.imgListenDump.Visible = false;
        }

        //Il pulsante di controllo solo se questo tipo di device ha comandi associati

        if (GrisPermissions.IsUserInGroupingCommandControl(this.SystemID))
        //if (GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level3) || ((this.SeverityLevel == 0) && GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2)))
        {
            if (DBSettings.HasDeviceCommand(this.Type))
            {
                this.imgCommandDump.Visible = true;
                this.imgCommandDump.OnClientClick = Device.BuildCommandDumpUrl(this.DevID, false);
            }
            else
            {
                this.imgCommandDump.Visible = false;
            }
        }
        else
        {
            this.imgCommandDump.Visible = false;
        }



        if (this.imgScreenDump.Visible)
        {
            //this.imgScreenDump.OnClientClick = Device.BuildScreenDumpUrl(this.DevID);
            this.imgScreenDump.OnClientClick = Device.BuildCommandDumpUrl(this.DevID, true);
        }

        // Compilazione delle etichette principali del pannello
        this.lblWhere.Text = this.ComposeWhereMessage();
        this.lblWhereLabel.Visible = (this.lblWhere.Text.Length > 0);

        if (!string.IsNullOrEmpty(this.RackDescription) && this.RackDescription != "Nessuna")
        {
            this.lblNotesLabel.Visible = true;
            this.lblNotes.Visible = true;

            this.lblNotes.Text = this.RackDescription;
            //string notes = this.RackDescription.ToLower();
            //char[] aNotes = notes.ToCharArray();
            //aNotes[0] = char.ToUpper(aNotes[0]);
            //this.lblNotes.Text = new string(aNotes);
        }
        else
        {
            this.lblNotesLabel.Visible = false;
            this.lblNotes.Visible = false;
            this.lblNotes.Text = this.RackDescription;
        }

        if ((this.OperatorMaintenance) && (!this.lblSeverityDescription.Text.EndsWith(" (Non attivo)")))
        {
            this.lblSeverityDescription.Text += " (Non attivo)";
        }

        if ((this.RackPositionRow >= 0) || (this.RackPositionCol >= 0))
        {
            this.lblWhere.ToolTip = this.lblWhereLabel.ToolTip = string.Format("Posizione: {0}, {1}", (this.RackPositionCol >= 0 ? this.RackPositionCol.ToString() : "N/D"),
                                                    (this.RackPositionRow >= 0 ? this.RackPositionRow.ToString() : "N/D"));
        }
    }

    protected void btnInMaintenance_OnClick(object sender, ImageClickEventArgs e)
    {
        if (Device.CanUserChangeMaintenanceMode())
        {
            try
            {
                devicesTableAdapter ta = new devicesTableAdapter();
                this.OperatorMaintenance = !this.OperatorMaintenance;
                ta.UpdateInMaintenance(this.OperatorMaintenance, this.DevID);
				FormulaEngineService formulaService = new FormulaEngineService();
				formulaService.EvaluateRegion(this.NavigationPage.RegID);
				this.OnChangeMaintenance();
            }
            catch (SqlException)
            {
            }
        }
    }

    # endregion

    # region Methods

    private string ComposeWhereMessage()
    {
        if (this.SystemID == 2)
        {
            return this.Rack;
        }

        return string.Format("{0}<br /><br />{1}", this.Building,
                             ((!string.IsNullOrEmpty(this.Rack) && this.Rack.ToLower().IndexOf("armadio ") == -1)
                                  ? string.Format("Armadio {0}", this.Rack)
                                  : this.Rack));
    }

    private void SetDeviceImage()
    {
        this.imgDeviceType.ImageUrl = Device.GetDeviceImageRelativePath(this.DeviceImage, !string.IsNullOrEmpty(this.Ticket));
    }

    private void SetDeviceStatus()
    {
        this.imgStatus.ImageUrl = Device.GetDeviceStatusImageRelativePath(false, this.SeverityLevel);

        if (!GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2) && this.SeverityLevel == 1)
        {
            this.imgStatus.ImageUrl = this.imgStatus.ImageUrl.Replace("_1.gif", "_0.gif");
        }

        this.btnInMaintenance.ImageUrl = "~/IMG/Interfaccia/GraphicalPanel/pan_inconf_" +
                                         (this.OperatorMaintenance ? "on" : "off") + ".gif";
    }

    protected void OnChangeMaintenance()
    {
        if (this.ChangeMaintenance != null)
        {
            this.ChangeMaintenance(this, null);
        }
    }

    # endregion
}
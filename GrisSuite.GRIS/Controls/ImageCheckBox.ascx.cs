using System;
using System.Web.UI;

public partial class ImageCheckBox : UserControl
{
	public event EventHandler Click;
	
	public string FieldImagePathOn
	{
		get { return ( this.ViewState["FieldImagePathOn"] ?? "" ).ToString(); }
		set { this.ViewState["FieldImagePathOn"] = value; }
	}

	public string FieldImagePathOff
	{
		get { return ( this.ViewState["FieldImagePathOff"] ?? "" ).ToString(); }
		set { this.ViewState["FieldImagePathOff"] = value; }
	}

	public string FieldDescription
    {
		get { return ( this.ViewState["FieldDescription"] ?? "" ).ToString(); }
		
		set 
		{ 
			this.ViewState["FieldDescription"] = value; 
			this.imgbFilteredField.AlternateText = value;
		}
	}
    
    public bool Pressed
    {
		get { return Convert.ToBoolean(this.ViewState["Pressed"] ?? true); }
		
        set
        {
			this.ViewState["Pressed"] = value;
            if (value)
            {
				this.imgbFilteredField.ToolTip = string.Format("Nascondi {0}", this.FieldDescription);
                this.imgbFilteredField.ImageUrl = this.FieldImagePathOn;
            }
            else
            {
				this.imgbFilteredField.ToolTip = string.Format("Visualizza {0}", this.FieldDescription);
				this.imgbFilteredField.ImageUrl = this.FieldImagePathOff;
			}
        }
    }
	
    protected void Page_Load(object sender, EventArgs e)
    {
		if ( this.imgbFilteredField.ImageUrl == "" )
		{
			this.Pressed = true;
		}
    }

    protected void imgbFilteredField_Click(object sender, ImageClickEventArgs e)
    {
		this.Pressed = !this.Pressed;
		if ( this.Click != null ) this.Click(this, null);
    }
}

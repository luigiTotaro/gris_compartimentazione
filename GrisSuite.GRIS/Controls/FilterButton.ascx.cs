﻿using System;
using System.Web.UI;

[ControlValuePropertyAttribute("Active")]
public partial class Controls_FilterButton : UserControl
{
	public event EventHandler Click;
	
	public string Text
	{
		get
		{
			return ( this.ViewState["Text"] ?? "" ).ToString();
		}

		set 
		{
			this.ViewState["Text"] = value; 
		}
	}

	public bool Active
	{
		get 
		{
			return Convert.ToBoolean(this.ViewState["Active"] ?? false);
		}

		set 
		{ 
			this.ViewState["Active"] = value;
			
			string imgStatus = "";
			if ( value )
			{
				imgStatus = "_on";
			}
			else
			{
				imgStatus = "_off";
			}

			this.imgbFlag.ImageUrl = string.Format("~/IMG/Interfaccia/alerts_filter_sx{0}.gif", imgStatus);
		}
	}

	protected void Page_Load ( object sender, EventArgs e )
	{
		this.lnkbFlagText.Text = this.Text;
	}

	protected void Filter_Click ( object sender, ImageClickEventArgs e )
	{
		this.Active = !this.Active;
		this.OnClick();
	}

	protected void FilterLbl_Click ( object sender, EventArgs e )
	{
		this.Active = !this.Active;
		this.OnClick();
	}
	
	protected void OnClick()
	{
		if ( this.Click != null )
		{
			this.Click(this, null);
		}
	}
}

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DevPanel.ascx.cs" Inherits="DevPanel" %>
<%@ Reference Control="~/Controls/SelectorsPanel.ascx" %>
<%@ Register TagPrefix="gris" TagName="VirtualObjectHeaderDevPanel" Src="~/Controls/VirtualObjectHeaderDevPanel.ascx" %>
<div style="width: 7px; background-color: #4C4C4C; display: block; float: left;">
</div>
<div style="width: 200px; background-color: #808080; display: block; float: left; text-align:center;">

    <div>
        <div style="width: 10px; height: 30px; float: left;">
            <asp:Image ID="imgTopSx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_top_sx.gif"
                EnableViewState="false" Style="width: 10px; height: 30px; display: block;"></asp:Image>
        </div>
        <div style="background-image: url(IMG/interfaccia/GraphicalPanel/pan_top_bg.gif);
            height: 30px; width: 180px; background-repeat: repeat-x; float: left;">
            <asp:Image ID="imgStatus" runat="server" Style="width: 30px; height: 30px; float: left;" />
            <asp:ImageButton ID="btnInMaintenance" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_inconf_off.gif"
                OnClick="btnInMaintenance_OnClick" ToolTip="Non attivo" AlternateText="Non attivo"
                Style="width: 16px; height: 30px; float: left;" />
            <div style="float: right; display: inline;">
                <gris:VirtualObjectHeaderDevPanel runat="server" ID="voh" />
            </div>
        </div>
        <div style="width: 10px; height: 30px; float: left;">
            <asp:Image ID="imgTopDx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_top_dx.gif"
                EnableViewState="false" Style="width: 10px; height: 30px; display: block;"></asp:Image>
        </div>
    </div>
    <div>
        <div style="background-repeat: repeat-y;">
            <div class="DevPanelTL" style="height: 36px; margin: 0px auto;">
                <asp:Label ID="lblNome" runat="server" Text="Periferica" Style="width: 180px; display: block;"></asp:Label>
            </div>
            <div>
                <div style="width: 180px; height: 140px; position: relative; display: block;">
                    <asp:Image ID="imgbDeviceType" runat="server" AlternateText="" Style="width: 180px;
                        height: 140px; position: absolute;" />
                    <asp:ImageButton Style="width: 180px; height: 140px; position: relative;" runat="server"
                        ID="imgOverlay" EnableViewState="false" OnClick="imgDeviceType_Click" ToolTip="Dettagli Periferica"
                        AlternateText="Dettagli Periferica" />
                </div>
                <div style="width: 180px;">
                    <asp:Label ID="lblType" runat="server" Text="" CssClass="DevPanelLBL" Style="display: block;
                        height: 15px;"></asp:Label><asp:Label ID="lblRawType" runat="server" CssClass="DevPanelLBL"
                            Style="display: block; height: 15px;" />
                    <asp:Label ID="lblVendor" runat="server" Text="" CssClass="DevPanelLBL" ForeColor="White"
                        Style="display: block; height: 15px;" />
                </div>
                <div style="width: 180px; height: 30px;">
                    <asp:ImageButton ID="imgScreenDump" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/perif_info.gif"
                        Style="float: right; display: inline;" />
                    <asp:ImageButton ID="imgListenDump" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/arm_play.gif"
                        Style="float: right; display: inline;" />
                    <asp:ImageButton ID="imgCommandDump" runat="server" EnableViewState="false" ImageUrl="~/IMG/interfaccia/perif_command.gif"
                        Style="float: right; display: inline;" />
                </div>
            </div>
            <div class="DevPanelDiv" style="height: 55px; margin: 0px auto;">
                <asp:Label ID="lblSeverityDescription" runat="server" Text="" Style="font-size: 11pt;
                    color: #ffffff; font-weight: bold; display: block; width: 180px;" />
            </div>
        </div>
    </div>
    <div>
        <div style="width: 10px; height: 30px; float: left;">
            <asp:Image ID="imgBotSx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_bot_sx.gif"
                EnableViewState="false" Style="width: 10px; height: 30px; display: block;"></asp:Image>
        </div>
        <div style="background-image: url(IMG/Interfaccia/GraphicalPanel/pan_bot_bg.gif);
            height: 30px; width: 180px; background-repeat: repeat-x; float: left;">
        </div>
        <div style="width: 10px; height: 30px; float: left;">
            <asp:Image ID="imgBotDx" runat="server" ImageUrl="~/IMG/Interfaccia/GraphicalPanel/pan_bot_dx.gif"
                EnableViewState="false" Style="width: 10px; height: 30px; display: block;"></asp:Image>
        </div>
    </div>
</div>
<div style="width: 7px; background-color: #4C4C4C; display: block; display: block;
    float: left;">
</div>

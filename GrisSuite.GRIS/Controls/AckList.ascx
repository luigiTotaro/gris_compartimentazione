﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AckList.ascx.cs" Inherits="Controls_AckList" %>
<asp:GridView ID="grdAcks" runat="server" AllowSorting="True" AutoGenerateColumns="False" SkinID="gvwEmpty"
    CellPadding="4" DataKeyNames="DeviceAckID" DataSourceID="sdsAcks" Width="100%"
    ShowFooter="False" PageSize="20" EmptyDataText="Nessuna tacitazione presente"
    AllowPaging="true" OnDataBound="grdAcks_DataBound" CssClass="ackgrid">
    <PagerSettings Mode="NumericFirstLast" Position="Bottom"></PagerSettings>
    <HeaderStyle CssClass="ackgridth"></HeaderStyle>
    <RowStyle CssClass="ackgridrow"></RowStyle>
    <PagerStyle CssClass="ackgridpager"></PagerStyle>
    <AlternatingRowStyle CssClass="ackgridrow"></AlternatingRowStyle>
    <Columns>
        <asp:BoundField DataField="RegionName" HeaderText="Compartimento" ReadOnly="True"
            SortExpression="RegionName"></asp:BoundField>
        <asp:BoundField DataField="ZoneName" HeaderText="Linea" ReadOnly="True" SortExpression="ZoneName">
        </asp:BoundField>
        <asp:BoundField DataField="NodeName" HeaderText="Stazione" ReadOnly="True" SortExpression="NodeName">
        </asp:BoundField>
        <asp:BoundField DataField="DeviceName" HeaderText="Periferica" ReadOnly="True" SortExpression="DeviceName">
        </asp:BoundField>
        <asp:BoundField DataField="StreamName" HeaderText="Stream" ReadOnly="True" SortExpression="StreamName">
        </asp:BoundField>
        <asp:BoundField DataField="StreamFieldName" HeaderText="Stream Field" ReadOnly="True"
            SortExpression="StreamFieldName"></asp:BoundField>
        <asp:BoundField DataField="AckExpiration" HeaderText="Scadenza tacitazione" ReadOnly="True"
            SortExpression="AckExpiration"></asp:BoundField>
        <asp:BoundField DataField="Username" HeaderText="Tacitato da" ReadOnly="True" SortExpression="Username">
        </asp:BoundField>
    </Columns>
</asp:GridView>
<asp:SqlDataSource ID="sdsAcks" runat="server" ConnectionString="<%$ ConnectionStrings:GrisSuite.Data.Properties.Settings.TelefinConnectionString %>"
    SelectCommand="tf_GetDeviceAckByRegions" SelectCommandType="StoredProcedure"
    OnSelecting="sdsAcks_Selecting">
    <SelectParameters>
        <asp:Parameter Name="RegIDs" Type="String" />
        <asp:Parameter Name="RegID" Type="Int64" />
        <asp:Parameter Name="ZonID" Type="Int64" />
        <asp:Parameter Name="NodID" Type="Int64" />
        <asp:Parameter Name="DevID" Type="Int64" />
    </SelectParameters>
</asp:SqlDataSource>

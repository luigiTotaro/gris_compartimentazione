﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualObjectHeader.ascx.cs"
    Inherits="Controls_VirtualObjectHeader" %>
<%@ Register TagPrefix="gris" TagName="LayeredIcon" Src="~/Controls/LayeredIcon.ascx" %>
<%@ Register TagPrefix="gris" TagName="VirtualObjectWeight" Src="~/Controls/VirtualObjectWeight.ascx" %>
<table style="width: 1180px;border-collapse:collapse;">
    <tr>
        <td style="width: 68px; background-color: #333333">
            <gris:LayeredIcon ID="imgStatus" runat="server" LayeredIconType="VirtualObjectHeader"
                AdditionalStyle="float:left" />
        </td>
        <td style="width: 1040px; background-color: #333333">
            <asp:Label runat="server" ID="lblTitle" style="font-size:14pt;color:#cbcbcb;font-weight:bold;"></asp:Label>
        </td>
        <td style="width: 72px; background-color: #333333;padding-right:3px;">
            <gris:VirtualObjectWeight runat="server" ID="imgWeight" VirtualObjectWeightIconType="VistaTabellare" />
        </td>
    </tr>
</table>

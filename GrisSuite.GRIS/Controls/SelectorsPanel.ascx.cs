using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris;

public delegate void SelectorClickHandler(object sender, SelectedSystemEventArgs e);

public delegate void SelectorDataboundHandler(object sender, SelectedSystemEventArgs e);

public partial class SelectorsPanel : GrisUserControl
{
    private double _worstState = -2;
    private bool _justSelectedSystem;

    public event SelectorClickHandler SelectorClick;
    public event SelectorDataboundHandler SelectorDatabound;

    # region Properties

    public long NodID
    {
        get { return Convert.ToInt64(this.ViewState["NodID"] ?? -1); }
        set { this.ViewState["NodID"] = value; }
    }

	public int SelectedSystemId
	{
		get { return ( GrisSessionManager.SelectedSystem > 0 ) ? GrisSessionManager.SelectedSystem : (int) ( this.ViewState["CurrentSelectedSystem"] ?? -1 ); }

		set
		{
			GrisSessionManager.SelectedSystem = value;
			this.ViewState["CurrentSelectedSystem"] = value;
		}
	}

	public long SelectedNodeSystemId
	{
		get { return ( GrisSessionManager.SelectedNodeSystem > 0 ) ? GrisSessionManager.SelectedNodeSystem : Convert.ToInt64( this.ViewState["SelectedNodeSystemId"] ?? -1 ); }

		set
		{
			GrisSessionManager.SelectedNodeSystem = value;
			this.ViewState["SelectedNodeSystemId"] = value;
		}
	}

    public int SystemDevicesCount
    {
        get
        {
            return (GrisSessionManager.SystemDevicesCount > 0)
                       ? GrisSessionManager.SystemDevicesCount : (int)(this.ViewState["CurrentSystemDevicesCount"] ?? -1);
        }

        set
        {
            GrisSessionManager.SystemDevicesCount = value;
            this.ViewState["CurrentSystemDevicesCount"] = value;
        }
    }

    public string SelectedSystemDescription
    {
        get
        {
            return (GrisSessionManager.SelectedSystemDescription.Length > 0)
                       ? GrisSessionManager.SelectedSystemDescription : (string)(this.ViewState["CurrentSelectedSystemDescription"] ?? "");
        }

        set
        {
            GrisSessionManager.SelectedSystemDescription = value;
            this.ViewState["CurrentSelectedSystemDescription"] = value;
        }
    }

    public bool UsedInDevicePage
    {
        get { return Convert.ToBoolean(this.ViewState["UsedInDevicePage"] ?? false); }
        set { this.ViewState["UsedInDevicePage"] = value; }
    }

    private bool SelectedSystemExists
    {
        get { return (this.SelectedSystemId > 0); }
    }

    # endregion

    protected void Page_Load(object sender, EventArgs e)
    {}

    public void Page_Prerender(object sender, EventArgs e)
    {
        this.ChangeSelectedSystemVisualApparence();
    }

    protected void odsSystems_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["NodID"] = this.NodID;
		e.InputParameters["FilterSystemID"] = 0;
    }

    protected void dlstSystems_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        SystemsInfoDS.SystemRow systemRow = ((DataRowView)e.Item.DataItem).Row as SystemsInfoDS.SystemRow;

        if (systemRow != null)
        {
            ImageButton img = (ImageButton)e.Item.FindControl("imgbSystem");

            // il sistema � visibile solo se contiene periferiche. 
            // Non � inoltre visibile il sistema generico agli utenti che non appartengano ai gruppi administrators o operatori gris.
            if (
				// Il sistema nuvola � visibile solo agli admin
				((systemRow.SystemID != Systems.AltrePeriferiche) || GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInGrisOperatorsGroup())
				// Il sistema VPN Verde � visibile solamente agli admin, ai rispettivi visualizzatori di sistema e ai gruppi compartimentali visualizzatori di sistema
				&& ((systemRow.SystemID != Systems.MonitoraggioVPNVerde) || ((systemRow.SystemID == Systems.MonitoraggioVPNVerde) && (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInVPNVerdeItaliaGroup() || GrisPermissions.IsUserInLocalVPNVerdeCompGroup())))
				// Il sistema Web Radio � visibile solamente agli admin, ai rispettivi visualizzatori di sistema e ai gruppi compartimentali visualizzatori di sistema
				&& ((systemRow.SystemID != Systems.WebRadio) || ((systemRow.SystemID == Systems.WebRadio) && (GrisPermissions.IsUserInAdminGroup() || GrisPermissions.IsUserInWebRadioItaliaGroup() || GrisPermissions.IsUserInLocalWebRadioCompGroup())))
				)
            {
                // gli appartenenti ad un gruppo di livello 1 non possono vedere le periferiche in attenzione
                if (!GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2) && systemRow.Status == 1 /* attenzione */)
                {
                    systemRow.Status = 0;
                }

                img.Visible = true;

                // il selettore viene deselezionato
                img.ImageUrl = string.Format("~/IMG/interfaccia/{0}_{1}_off.gif", systemRow.SystemID,
											 (systemRow.SystemID == Systems.AltrePeriferiche) ? (short)3 : systemRow.Status);

                // determinazione del sistema complessivamente nello stato peggiore
                if (this.IsSystemAutoSelected(systemRow))
                {
                    this._worstState = this.AdjustSeverity(systemRow.Status);
					this.SelectedSystemId = systemRow.SystemID;
					this.SelectedNodeSystemId = systemRow.NodeSystemsId;
                    this._justSelectedSystem = true;
                }
                else
                {
                    if (!this.IsPostBack)
                    {
						this.ViewState["CurrentSelectedSystem"] = this.SelectedSystemId;
						this.ViewState["SelectedNodeSystemId"] = this.SelectedNodeSystemId;
                    }
                }
            }
            else
            {
                img.Visible = false;
            }

            // Impostiamo sempre la descrizione per il sistema corrente, per averla aggiornata da base dati (il conteggio pu� cambiare autonomamente su DB)
            if (systemRow.SystemID == this.SelectedSystemId)
            {
                this.SelectedSystemDescription = string.Format("{0} ({1} {2})", systemRow.SystemDescription, systemRow.RawTotalDevices,
                                                               systemRow.RawTotalDevices == 1 ? "periferica monitorata" : "periferiche monitorate");

                this.SystemDevicesCount = systemRow.RawTotalDevices;
            }

            if (e.Item.ItemIndex == ((DataRowView)e.Item.DataItem).DataView.Count - 1)
            {
                this.OnSelectorDataBound();
            }
        }
    }

    private bool IsSystemAutoSelected(SystemsInfoDS.SystemRow system)
    {
        return ( // � l'unico sistema di questa stazione
               (system.Table.Rows.Count == 1) ||
               // siamo nella pagina della stazione, � un sistema con stato e nessun sistema � stato manualmente selezionato
               (!this.UsedInDevicePage && (!this.SelectedSystemExists || this._justSelectedSystem) && this.AdjustSeverity(system.Status) > this._worstState &&
				system.SystemID != Systems.AltrePeriferiche));
    }

    private double AdjustSeverity (int status)
    {
        if (status == 9)
        {
            return 0.5;
        }

        return status;
    }

    protected void imgbSystem_Click(object sender, CommandEventArgs e)
    {
        if (!(sender is ImageButton))
        {
            return;
        }

        ImageButton imgb = (ImageButton)sender;

        if (imgb.CommandArgument != "")
        {
            string[] commandArguments = imgb.CommandArgument.Split(',');

            this.SelectedSystemId = Convert.ToInt32(commandArguments[0]);
			this.SystemDevicesCount = Convert.ToInt32(commandArguments[1]);
			this.SelectedNodeSystemId = Convert.ToInt64(commandArguments[2]);
            this.SelectedSystemDescription = imgb.ToolTip;
            this.OnSelectorClick();
        }
    }

    # region Methods

    public void ChangeSelectedSystemVisualApparence()
    {
        ImageButton imgb = null;
        if (this.SelectedSystemId != -1)
        {
            foreach (DataListItem item in this.dlstSystems.Items)
            {
                foreach (Control ctrl in item.Controls)
                {
                    if ((imgb = ctrl as ImageButton) != null)
                    {
                        if (imgb.CommandArgument.Split(',')[0] == this.SelectedSystemId.ToString())
                        {
                            imgb.ImageUrl = imgb.ImageUrl.Replace("_off", "_on");

                            if (!this.UsedInDevicePage)
                            {
                                GUtility.EnableImageButton(this.Page, imgb, false);
                            }
                        }
                        else
                        {
                            imgb.ImageUrl = imgb.ImageUrl.Replace("_on", "_off");
                            GUtility.EnableImageButton(this.Page, imgb, true);
                        }
                    }
                }
            }
        }
    }

    public override void DataBind()
    {
        base.DataBind();
        this.dlstSystems.DataBind();
    }

    protected void OnSelectorClick()
    {
        if (this.SelectorClick != null)
        {
            this.SelectorClick(this, new SelectedSystemEventArgs(this.SelectedSystemId, this.SelectedNodeSystemId, this.SelectedSystemDescription, this.SystemDevicesCount));
        }
    }

    protected void OnSelectorDataBound()
    {
        if (this.SelectorDatabound != null)
        {
			this.SelectorDatabound(this, new SelectedSystemEventArgs(this.SelectedSystemId, this.SelectedNodeSystemId, this.SelectedSystemDescription, this.SystemDevicesCount));
        }
    }

    # endregion
}

public class SelectedSystemEventArgs : EventArgs
{
	public int SystemId { get; private set; }
	
	public long NodeSystemId { get; private set; }

    public int SystemDevicesCount { get; private set; }

    public string SystemDescription { get; private set; }

    public SelectedSystemEventArgs(int systemId, long nodeSystemId, string systemDescription, int systemDevicesCount)
    {
        this.SystemId = systemId;
    	this.NodeSystemId = nodeSystemId;
        this.SystemDescription = systemDescription;
        this.SystemDevicesCount = systemDevicesCount;
    }
}
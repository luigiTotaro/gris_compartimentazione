using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Web.UI;
using Controllers;
using GrisSuite.Common;
using GrisSuite.Data.Gris.DevicesDSTableAdapters;

public partial class DevPanel : GrisUserControl
{
    public event EventHandler DevPanelClick = null;
    public event EventHandler ChangeMaintenance;

    #region Properties

    public string Name
    {
        get { return this.lblNome.Text; }

        set { this.lblNome.Text = value; }
    }

    public string Type
    {
        get { return this.lblType.Text; }

        set { this.lblType.Text = value; }
    }

    public string RawType
    {
        set
        {
            if ((value.StartsWith("INFSTAZ", StringComparison.OrdinalIgnoreCase)) && (!this.Type.StartsWith("INFSTAZ")))
            {
                this.lblRawType.Text = "(Infostazioni)";
            }
            else
            {
                this.lblRawType.Text = "";
            }
        }
    }

    public string Host
    {
        get { return (this.ViewState["Host"] ?? "").ToString(); }

        set { this.ViewState["Host"] = value; }
    }

    public string Building
    {
        get { return (this.ViewState["Building"] ?? "").ToString(); }

        set { this.ViewState["Building"] = value; }
    }

    public string Rack
    {
        get { return (this.ViewState["Rack"] ?? "").ToString(); }

        set { this.ViewState["Rack"] = value; }
    }

    public string BuildingDescription
    {
        get { return (this.ViewState["BuildingDescription"] ?? "").ToString(); }

        set { this.ViewState["BuildingDescription"] = value; }
    }

    public string RackDescription
    {
        get { return (this.ViewState["RackDescription"] ?? "").ToString(); }

        set { this.ViewState["RackDescription"] = value; }
    }

    public string FullHostName
    {
        get { return (this.ViewState["FullHostName"] ?? "").ToString(); }

        set { this.ViewState["FullHostName"] = value; }
    }

    public string Vendor
    {
        get { return this.lblVendor.Text; }

        set { this.lblVendor.Text = value; }
    }

    public string Ticket { get; set; }

    public string WSUrlPattern
    {
        get { return (this.ViewState["WSUrlPattern"] ?? "").ToString(); }

        set { this.ViewState["WSUrlPattern"] = value; }
    }

    public long DevID
    {
        get { return Convert.ToInt64(this.ViewState["DevID"] ?? -1); }
        set { this.ViewState["DevID"] = value; }
    }

    public int SeverityLevel
    {
        get { return Convert.ToInt32(this.ViewState["SeverityLevel"] ?? -1); }
        set { this.ViewState["SeverityLevel"] = value; }
    }

    public int SeverityLevelDetail
    {
        get { return Convert.ToInt32(this.ViewState["SeverityLevelDetail"] ?? -1); }
        set { this.ViewState["SeverityLevelDetail"] = value; }
    }

    public string SeverityLevelDescription
    {
        get { return (this.ViewState["SeverityLevelDescription"] ?? "").ToString(); }
        set { this.ViewState["SeverityLevelDescription"] = value; }
    }

    public int SystemID
    {
        get { return Convert.ToInt32(this.ViewState["SystemID"] ?? -1); }
        set { this.ViewState["SystemID"] = value; }
    }

    public bool OperatorMaintenance
    {
        get { return Convert.ToBoolean(this.ViewState["OperatorMaintenance"] ?? false); }
        set { this.ViewState["OperatorMaintenance"] = value; }
    }

    public int RackPositionRow
    {
        get { return Convert.ToInt32(this.ViewState["RackPositionRow"] ?? -1); }
        set { this.ViewState["RackPositionRow"] = value; }
    }

    public int RackPositionCol
    {
        get { return Convert.ToInt32(this.ViewState["RackPositionCol"] ?? -1); }
        set { this.ViewState["RackPositionCol"] = value; }
    }

    public string DeviceImage
    {
        get { return (this.ViewState["DeviceImage"] ?? "").ToString(); }
        set { this.ViewState["DeviceImage"] = value; }
    }

    public string VirtualObjectName
    {
        get { return (this.ViewState["VirtualObjectName"] ?? "").ToString(); }
        set { this.ViewState["VirtualObjectName"] = value; }
    }

    public string VirtualObjectDescription
    {
        get { return (this.ViewState["VirtualObjectDescription"] ?? "").ToString(); }
        set { this.ViewState["VirtualObjectDescription"] = value; }
    }

    public byte IsComputedByCustomFormula
    {
        get { return Convert.ToByte(this.ViewState["IsComputedByCustomFormula"] ?? 0); }
        set { this.ViewState["IsComputedByCustomFormula"] = value; }
    }

    #endregion

    # region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.imgScreenDump.Attributes["OnMouseOver"] = "this.src = 'IMG/Interfaccia/perif_info_over.gif'";
        this.imgScreenDump.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/perif_info.gif'";

        this.imgListenDump.Attributes["OnMouseOver"] = "this.src = 'IMG/Interfaccia/arm_play_over.gif'";
        this.imgListenDump.Attributes["OnMouseOut"] = "this.src = 'IMG/interfaccia/arm_play.gif'";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SetDeviceImage();
        this.SetDeviceStatus();

        GUtility.EnableImageButton(this.Page, this.imgOverlay,
                                   (Device.CanUserViewDeviceDetails((NavigationPage)this.Page, this.SeverityLevel, this.SeverityLevelDetail, this.Type)));
        GUtility.EnableImageButton(this.Page, this.btnInMaintenance, Device.CanUserChangeMaintenanceMode());

        // il pulsante di visualizzazione dello screen dump viene visualizzato solo per le periferiche monitor.
        if (this.SystemID == 2)
        {
            // nel caso che la URL del web service non sia recuperabile per il monitor
            this.imgScreenDump.Visible = (!string.IsNullOrEmpty(this.WSUrlPattern)) &&
                                         ScreenDump.CanUserViewScreenDump(this.NavigationPage.RegID, this.SeverityLevel);
        }
        else
        {
            this.imgScreenDump.Visible = false;
        }
        //Screen dump sempre invisibile.... per adesso
        //this.imgScreenDump.Visible = false;

        //if (this.Type == DBSettings.GetAwgClientDeviceType()) // "WRKPDL000")
        if (this.Type.StartsWith(DBSettings.GetAwgClientDeviceType()))
        {
            this.imgListenDump.Visible          = StationListen.CanUserViewStationListen(this.NavigationPage.RegID, this.SeverityLevel);
            this.imgListenDump.OnClientClick    = Device.BuildListenDumpUrl             (this.DevID);
        }
        else
        {
            this.imgListenDump.Visible = false;
        }

        //Il pulsante di controllo solo se questo tipo di device ha comandi associati, ma prima vedo se l'utente � autorizzato per quella tipologia di sistemi

        if (GrisPermissions.IsUserInGroupingCommandControl(this.SystemID))
        //if (GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level3) || ((this.SeverityLevel == 0) && GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2)))
        {
            if (DBSettings.HasDeviceCommand(this.Type))
            {
                this.imgCommandDump.Visible = true;
                this.imgCommandDump.OnClientClick = Device.BuildCommandDumpUrl(this.DevID, false);
            }
            else
            {
                this.imgCommandDump.Visible = false;
            }
        }
        else
        {
            this.imgCommandDump.Visible = false;
        }


        if (this.imgScreenDump.Visible)
        {
            //this.imgScreenDump.OnClientClick = Device.BuildScreenDumpUrl(this.DevID);
            this.imgScreenDump.OnClientClick = Device.BuildCommandDumpUrl(this.DevID, true);
        }


        #region Binding dati oggetto virtuale della periferica

        this.voh.SevLevel                   = this.SeverityLevel;
        this.voh.SevLevelDescription        = this.SeverityLevelDescription;
        this.voh.SevLevelDetailId           = this.SeverityLevelDetail;
        this.voh.VirtualObjectName          = this.VirtualObjectName;
        this.voh.VirtualObjectDescription   = this.VirtualObjectDescription;
        this.voh.IsComputedByCustomFormula  = this.IsComputedByCustomFormula;

        #endregion
    }

    protected void btnInMaintenance_OnClick(object sender, ImageClickEventArgs e)
    {
        if (Device.CanUserChangeMaintenanceMode())
        {
            try
            {
                devicesTableAdapter ta = new devicesTableAdapter();
                this.OperatorMaintenance = !this.OperatorMaintenance;
                ta.UpdateInMaintenance(this.OperatorMaintenance, this.DevID);
                FormulaEngineService formulaService = new FormulaEngineService();
                formulaService.EvaluateRegion(this.NavigationPage.RegID);
                this.OnChangeMaintenance();
            }
            catch (SqlException)
            {}
        }
    }

    protected void imgDeviceType_Click(object sender, ImageClickEventArgs e)
    {
        this.OnDevPanelClick();
    }

    # endregion

    # region Methods

    private void SetDeviceImage()
    {
        this.imgbDeviceType.ImageUrl = Device.GetDeviceImageRelativePath(this.DeviceImage, !string.IsNullOrEmpty(this.Ticket));

        this.imgOverlay.ImageUrl = "~/IMG/interfaccia/perif_over.png";

        if (!string.IsNullOrEmpty(this.Ticket))
        {
            this.imgOverlay.ImageUrl = "~/IMG/Periferiche/hastickets.png";
        }
        else
        {
            if ((this.SeverityLevel == 1) || (this.SeverityLevel == 2) || (this.SeverityLevel == 255))
            {
                this.imgOverlay.ImageUrl = string.Format("~/IMG/interfaccia/perif_over_{0}.png", this.SeverityLevel);
            }
        }
    }

    private void SetDeviceStatus()
    {
        this.imgStatus.ImageUrl = Device.GetDeviceStatusImageRelativePath(false, this.SeverityLevel);

        if (!GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2) && this.SeverityLevel == 1)
        {
            this.imgStatus.ImageUrl = this.imgStatus.ImageUrl.Replace("_1.gif", "_0.gif");
            // la descrizione deve rimanere quella originale anche per gli appartenenti solo a gruppi di livello 1
        }

        this.lblSeverityDescription.Text = this.SeverityLevelDescription;

        if (this.OperatorMaintenance)
        {
            //GUtility.EnableImageButton(this.Page, this.btnInMaintenance, true);
            this.btnInMaintenance.ImageUrl = "~/IMG/Interfaccia/GraphicalPanel/pan_inconf_on.gif";
            this.lblSeverityDescription.Text += " (Non attivo)";
        }
        else
        {
            //GUtility.EnableImageButton(this.Page, this.btnInMaintenance, false);
            this.btnInMaintenance.ImageUrl = "~/IMG/Interfaccia/GraphicalPanel/pan_inconf_off.gif";
        }
    }

    [Browsable(true)]
    public void OnDevPanelClick()
    {
        if (this.DevPanelClick != null)
        {
            this.DevPanelClick(this, new EventArgs());
        }
    }

    [Browsable(true)]
    protected void OnChangeMaintenance()
    {
        if (this.ChangeMaintenance != null)
        {
            this.ChangeMaintenance(this, null);
        }
    }

    # endregion
}
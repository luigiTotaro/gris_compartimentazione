<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfigurationValidation.aspx.cs" Inherits="ConfigurationValidation" Title="GRIS - Gestione Validazione -" Theme="Gris" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" Runat="Server">
	<table cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
                    <tr>
                        <td style="background-image: url(IMG/interfaccia/nav_bg.gif); height: 30px;">
                            &nbsp;
                        </td>
                        <td style="width: 42px;">
                            <asp:Image runat="server" ID="imgNavSep2" ImageUrl="~/IMG/Interfaccia/nav_sep2.gif"
                                CssClass="imgnb" />
                            <asp:ImageButton ID="imgbUpdate" runat="server" ImageUrl="~/IMG/Interfaccia/btnRefresh_default.gif"
                                                CssClass="imgnb" OnClick="imgbUpdate_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background-image: url(IMG/interfaccia/nav_shadow_bg.gif);
                            height: 21px; background-repeat: repeat-x;">
                        </td>
                    </tr>
				</table>			
			</td>
		</tr>
		<tr>
			<td>
				<asp:updatepanel id="updConfigValidation" runat="server">
					<contenttemplate>
						<table cellpadding="8" cellspacing="8" style="width: 100%;">
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" style="width: 98%; background-color: #808080; color: White;">
										<tr>
											<td style="text-align: left; vertical-align: top;"><asp:image id="imgTopSx" runat="server" imageurl="~/IMG/interfaccia/opzioni_topsx.gif" height="20px"></asp:image></td>
											<td style="background-image: url(IMG/interfaccia/opzioni_bg.gif); background-repeat: repeat-x; height: 30px; width: 100%; font-weight: bold;">Elenco richieste di validazione</td>
											<td style="text-align: right; vertical-align: top;"><asp:image id="imgTopDx" runat="server" imageurl="~/IMG/interfaccia/opzioni_topdx.gif" height="20px"></asp:image></td>
										</tr>
										<tr>
											<td></td>
											<td style="text-align: center;">
												<asp:GridView ID="gvwConfigValidation" runat="server" skinid="gvwValidationTable" datakeynames="SrvID" datasourceid="odsServersToValidate" onselectedindexchanged="gvwConfigValidation_SelectedIndexChanged" ondatabound="gvwConfigValidation_DataBound" onrowdatabound="gvwConfigValidation_RowDataBound">
													<headerstyle Font-Size="14px" forecolor="black" />
													<RowStyle Font-Size="14px" forecolor="white" />
													<columns>
														<asp:boundfield datafield="SrvID" headertext="SrvID" readonly="True" sortexpression="SrvID"
															visible="False" />
														<asp:boundfield datafield="FullHostName" headertext="Nome Host" sortexpression="FullHostName">
															<headerstyle width="50%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:boundfield datafield="IP" headertext="Indirizzo IP" sortexpression="IP">
															<headerstyle width="30%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:boundfield datafield="ServerDateValidationRequested" dataformatstring="{0:dd-MM-yyyy HH:mm}"
															headertext="Richiesta il" sortexpression="ServerDateValidationRequested">
															<headerstyle width="20%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:templatefield showheader="False">
															<itemtemplate>
																<asp:imagebutton id="imgbValidate" runat="server" causesvalidation="false" commandname="Select" tooltip="Valida" imageurl="~/IMG/interfaccia/btnConvalida_default.gif" />
															</itemtemplate>
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />												
														</asp:templatefield>
													</columns>
													<emptydatatemplate>
														<asp:label id="lblEmptyGrid" runat="server" text="Nessuna configurazione da validare."></asp:label>
													</emptydatatemplate>
												</asp:GridView>																						
											</td>
											<td></td>
										</tr>
										<tr>
											<td style="height: 11px;"><asp:image id="imgBotSx" runat="server" imageurl="~/IMG/interfaccia/opzioni_botsx.gif"></asp:image></td>
											<td style="background-image: url(IMG/interfaccia/opzioni_botbg.gif); background-repeat: repeat-x; height: 11px;"></td>
											<td style="height: 11px;"><asp:image id="imgBotDx" runat="server" imageurl="~/IMG/interfaccia/opzioni_botdx.gif"></asp:image></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" style="width: 98%; background-color: #808080; color: White;">
										<tr>
											<td style="text-align: left; vertical-align: top;"><asp:image id="Image1" runat="server" imageurl="~/IMG/interfaccia/opzioni_topsx.gif" height="20px"></asp:image></td>
											<td style="background-image: url(IMG/interfaccia/opzioni_bg.gif); background-repeat: repeat-x; height: 30px; width: 100%; font-weight: bold;">Ultime configurazioni validate</td>
											<td style="text-align: right; vertical-align: top;"><asp:image id="Image2" runat="server" imageurl="~/IMG/interfaccia/opzioni_topdx.gif" height="20px"></asp:image></td>
										</tr>
										<tr>
											<td></td>
											<td style="text-align: center;">
												<asp:GridView ID="gvwConfigValidated" runat="server" skinid="gvwValidationTable" datakeynames="SrvID" datasourceid="odsServersValidated" ondatabound="gvwConfigValidated_DataBound">
													<headerstyle Font-Size="14px" forecolor="black" />
													<RowStyle Font-Size="14px" forecolor="white" />
													<columns>
														<asp:boundfield datafield="SrvID" headertext="SrvID" readonly="True" sortexpression="SrvID"
															visible="False" />
														<asp:boundfield datafield="FullHostName" headertext="Nome Host" sortexpression="FullHostName">
															<headerstyle width="50%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:boundfield datafield="IP" headertext="Indirizzo IP" sortexpression="IP">
															<headerstyle width="20%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:boundfield datafield="ServerDateValidationRequested" dataformatstring="{0:dd-MM-yyyy HH:mm}"
															headertext="Richiesta il" sortexpression="ServerDateValidationRequested">
															<headerstyle width="15%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:boundfield datafield="ServerDateValidationObtained" dataformatstring="{0:dd-MM-yyyy HH:mm}"
															headertext="Validata il" sortexpression="ServerDateValidationObtained">
															<headerstyle width="15%" horizontalalign="left" />
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />
														</asp:boundfield>
														<asp:templatefield showheader="False">
															<itemtemplate>
																<asp:image id="imgbValidated" runat="server" tooltip="Valida" imageurl="~/IMG/interfaccia/btnConvalida_ok.gif" />
															</itemtemplate>
															<itemstyle horizontalalign="left" cssclass="CertificationRowSeparator" />												
														</asp:templatefield>
													</columns>
													<emptydatatemplate>
														<asp:label id="lblEmptyGrid" runat="server" text="Nessuna configurazione da validare."></asp:label>
													</emptydatatemplate>
												</asp:GridView>																						
											</td>
											<td></td>
										</tr>
										<tr>
											<td style="height: 11px;"><asp:image id="Image3" runat="server" imageurl="~/IMG/interfaccia/opzioni_botsx.gif"></asp:image></td>
											<td style="background-image: url(IMG/interfaccia/opzioni_botbg.gif); background-repeat: repeat-x; height: 11px;"></td>
											<td style="height: 11px;"><asp:image id="Image4" runat="server" imageurl="~/IMG/interfaccia/opzioni_botdx.gif"></asp:image></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>					
						<asp:label id="lblMessage" runat="server" enableviewstate="false"></asp:label>
					</contenttemplate>
					<triggers>
						<asp:asyncpostbacktrigger controlid="imgbUpdate" eventname="Click" />
					</triggers>
				</asp:updatepanel>
				<asp:objectdatasource id="odsServersToValidate" runat="server"
					oldvaluesparameterformatstring="original_{0}" selectmethod="GetServersToValidate"
					typename="GrisSuite.Data.dsCentralServersTableAdapters.serversTableAdapter">
				</asp:objectdatasource>
				<asp:objectdatasource id="odsServersValidated" runat="server"
					oldvaluesparameterformatstring="original_{0}" selectmethod="GetServersValidated"
					typename="GrisSuite.Data.dsCentralServersTableAdapters.serversTableAdapter">
				</asp:objectdatasource>
			</td>
		</tr>
	</table>
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GroupsAndFormulas.aspx.cs" Inherits="GroupsAndFormulas" %>

<%@ Register src="Controls/GroupsAndFormulas.ascx" tagname="GroupsAndFormulas" tagprefix="gris" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Raggruppamenti e Regole</title>
	<link type="text/css" href="css/custom-theme/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
	<link rel="stylesheet" href="JS/colorpicker/css/colorpicker.css" type="text/css" />
	<link type="text/css" href="css/Gris.css" rel="stylesheet" />	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.min.js"></script>
	<script type="text/javascript" src="JS/colorpicker/js/colorpicker.js"></script>
    <script type="text/javascript" src="JS/colorpicker/js/eye.js"></script>
    <script type="text/javascript" src="JS/colorpicker/js/utils.js"></script>
</head>
<body style="margin: 0px;background-color:#4c4c4c">
    <form id="form1" runat="server">
    <div style="background-color:#4c4c4c">
    	<gris:GroupsAndFormulas id="GroupsAndFormulasControl" runat="server" />
    </div>
    </form>
</body>
</html>

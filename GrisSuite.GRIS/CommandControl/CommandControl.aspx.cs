using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrisSuite.Common;
using GrisSuite.Data.Gris;
using GrisSuite.Data.Gris.ListenDevicesDSTableAdapters;
using System.Collections.Generic;
using System.Web.Script.Serialization; 

public partial class CommandControl : Page {

    #region Properties


    public List<DeviceCommandData> CommandData
    {
        get { return (List<DeviceCommandData>)this.ViewState["CommandData"]; }

        set { this.ViewState["CommandData"] = value; }
    }

    
    public long DevID {
        get { return Convert.ToInt64(this.ViewState["DevID"]); }

        set { this.ViewState["DevID"] = value; }
    }

    public long NodID {
        get { return Convert.ToInt64(this.ViewState["NodID"]); }

        set { this.ViewState["NodID"] = value; }
    }

    public long RegID
    {
        get { return Convert.ToInt64(this.ViewState["RegID"]); }

        set { this.ViewState["RegID"] = value; }
    }

    public long systemID
    {
        get { return Convert.ToInt64(this.ViewState["systemID"]); }

        set { this.ViewState["systemID"] = value; }
    }

    public string DevType
    {
        get { return Convert.ToString(this.ViewState["DevType"]); }
        
        set { this.ViewState["DevType"] = value; }
    } 

    public string stationCode {
        get { return (this.ViewState["stationCode"] ?? "").ToString(); }

        set { this.ViewState["stationCode"] = value; }
    }

    public string baseListenUrl {
        get { return (this.ViewState["baseListenUrl"] ?? "").ToString(); }

        set { this.ViewState["baseListenUrl"] = value; }
    }

    public long baseListenPort {
        get { return Convert.ToInt64(this.ViewState["baseListenPort"]); }

        set { this.ViewState["baseListenPort"] = value; }
    }

    public string baseCommandUrl {
        get { return (this.ViewState["baseCommandUrl"] ?? "").ToString(); }

        set { this.ViewState["baseCommandUrl"] = value; }
    }


    public bool AutoScreenDump {
        get { return Convert.ToBoolean(this.ViewState["AutoScreenDump"] ?? false); }

        set { this.ViewState["AutoScreenDump"] = value; }
    }

    public bool AutoSize {
        get { return Convert.ToBoolean(this.ViewState["AutoSize"] ?? false); }

        set { this.ViewState["AutoSize"] = value; }
    }

    public bool IsRefreshing {
        get { return Convert.ToBoolean(this.ViewState["IsRefreshing"] ?? false); }

        set { this.ViewState["IsRefreshing"] = value; }
    }

    public string Vendor {
        get { return (this.ViewState["Vendor"] ?? "").ToString(); }

        set { this.ViewState["Vendor"] = value; }
    }

    public string Uri {
        get { return (this.ViewState["Uri"] ?? "").ToString(); }

        set { this.ViewState["Uri"] = value; }
    }

    public string Device {
        get { return (this.ViewState["Device"] ?? "").ToString(); }

        set { this.ViewState["Device"] = value; }
    }

    public string Building {
        get { return (this.ViewState["Building"] ?? "").ToString(); }

        set { this.ViewState["Building"] = value; }
    }

    public string Rack {
        get { return (this.ViewState["Rack"] ?? "").ToString(); }

        set { this.ViewState["Rack"] = value; }
    }

    public string RackDescription {
        get { return (this.ViewState["RackDescription"] ?? "").ToString(); }

        set { this.ViewState["RackDescription"] = value; }
    }

    public string IP {
        get { return (this.ViewState["IP"] ?? "").ToString(); }

        set { this.ViewState["IP"] = value; }
    }

    public string Type {
        get { return (this.ViewState["Type"] ?? "").ToString(); }

        set { this.ViewState["Type"] = value; }
    }

    public string NodeName {
        get { return (this.ViewState["NodeName"] ?? "").ToString(); }

        set { this.ViewState["NodeName"] = value; }
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e) {

        //controllo se pu� aprire la pagina
        /*
        if (GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level3) || ((this.SeverityLevel == 0) && GrisPermissions.IsInRole(this.NavigationPage.RegID, PermissionLevel.Level2)))
        {
            this.ViewState["SeverityLevel"]
        }
        */

        

        if (!this.IsPostBack)
        {
            //Prendo i vari parametri
            Boolean autoScreenDump = false;
            if (this.Request.QueryString["autoSD"] != null)
            {
                bool.TryParse(this.Request.QueryString["autoSD"], out autoScreenDump);
            }
            this.AutoScreenDump = autoScreenDump;
             
            long devId;
            if (this.Request.QueryString["dev"] != null && long.TryParse(this.Request.QueryString["dev"], out devId))
            {
                this.DevID = devId;
                devIdHidden.Value = devId.ToString();
                // ottengo gli altri dati relativi alla periferica in DB
                if (this.InitializeFieldsFromData(this.DevID))
                {
                    int SeverityLevel = 255;
                    //controllo se pu� aprire la pagina

                    if (!GrisPermissions.IsUserInGroupingCommandControl(Convert.ToInt32(this.systemID)))
                    //if (GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level3) || ((SeverityLevel == 0) && GrisPermissions.IsInRole(this.RegID, PermissionLevel.Level2)))
                    {
                        disableAll();
                    }
                    else
                    {

                        //prendo tutti i comandi per questo device type
                        this.CommandData = DBSettings.GetDeviceCommandList(this.DevType);

                        //ciclo la lista e riempio la combo con i dati
                        var primo = true;
                        var commandIdScreenDump=""; //mi memorizzo il command id dello screen dump, in caso deve farlo automaticamente

                        foreach (var command in this.CommandData)
                        {
                            cmbSelectCommand.Items.Add(new ListItem(command.Comando, command.CommandID));
                            if (command.Class == "screenDump") commandIdScreenDump = command.CommandID;
                            if (primo)
                            {
                                txtDescrizione.Text = command.Descrizione;
                                if (command.Parametri)
                                {
                                    txtParameters.Disabled = false;
                                    trParametri.Style.Remove("display");
                                }
                                else
                                {
                                    txtParameters.Disabled = true;
                                    trParametri.Style.Add("display", "none");
                                }
                                txtDefaultParameters.Value = command.DefaultParameter;
                                primo = false;
                            }
                        }

                        //devo fare un auto screen dump?
                        HiddenAutoSD.Value = "0";
                        if (this.AutoScreenDump == true)
                        {
                            if (commandIdScreenDump != "")
                            {
                                frmSL.Style.Add("display", "none");
                                HiddenAutoSD.Value = "1";
                                preExecuteCommand(commandIdScreenDump);
                            }
                            else
                            {
                                frmSL.Style.Add("display", "none");
                                testoAttesa.InnerHtml = "Attenzione, ScreenDump non disponibile";
                            }
                           
                        }
                        else sfondo.Style.Add("display", "none");

                    }

                }



            }
            else
            {
                disableAll();
            }


        }
        else
        {
            
        }

        //prendo tutti i comandi inviati per questo device
        //this.CommandData = DBSettings.GetDeviceCommandList(this.DevType);
        List<CommandSentData> CommandSentData = GUtility.GetSentCommandByID(this.DevID.ToString(), true);

        var jsonSerialiser = new JavaScriptSerializer();
        var json = jsonSerialiser.Serialize(CommandSentData);
        jsonTableSent.Value = json;
    }

    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = ((System.Web.UI.WebControls.DropDownList)(sender)).SelectedIndex;
        string  id = cmbSelectCommand.Items[index].Value;
        //devo trovare questo id nella lista e cambiare i campi in accordo
        foreach (var command in this.CommandData)
        {
            if (command.CommandID==id)
            {
                txtDescrizione.Text = command.Descrizione;
                if (command.Parametri)
                {
                    txtParameters.Disabled = false;
                    trParametri.Style.Remove("display");
                }
                else
                {
                    txtParameters.Disabled = true;
                    trParametri.Style.Add("display", "none");
                }
                txtParameters.Value = "";
                txtDefaultParameters.Value = command.DefaultParameter;
            }
        }
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);
        
    }

    private void preExecuteCommand(string id)
    {
        //operazioni preliminari all'esecuzione del comando automatico di screen dump
        checkboxRefresh.Checked = true;
        executeCommand(id);
    }

    protected void Star_Onclick(object sender, EventArgs e)
    {
        //devo spedire all'agent webapp il comando

        int index = ((System.Web.UI.WebControls.DropDownList)(this.cmbSelectCommand)).SelectedIndex;
        string id = cmbSelectCommand.Items[index].Value;
        executeCommand(id);
    }
        
    private void executeCommand(string id)
    {
        //prendo i dati del comando
        string comando = "";
        string parametri = "";
        string versione = "";
        
        //devo trovare questo id nella lista
        foreach (var command in this.CommandData)
        {
            if (command.CommandID == id)
            {
                comando = command.Class;
                versione = command.Versione;
                if (command.Parametri) parametri = txtParameters.Value;
                else parametri = txtDefaultParameters.Value;

            }
        }

        
        //devo inserire nel db la richiesta del comando
        long tableId = GUtility.InsertCommandRequest(id, versione, parametri, this.DevID.ToString(), this.User.Identity.Name);
        string restConnectionString = GUtility.GetRestConnectionString();

        if (restConnectionString == "") restConnectionString = "http://localhost:8916";
        
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(restConnectionString + "/device_command");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";

        try
        {
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    data = new
                    {
                        IdComando = comando,
                        Versione = versione,
                        DestinationRegionId = this.RegID.ToString(),
                        DestinationNodeId = this.NodID.ToString(),
                        DeviceId = this.DevID.ToString(),
                        Parameters = parametri,
                        CommandReplyId = tableId.ToString()
                    }
                });

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                //scrivo la risposta comunque nel db
                //nel caso sia "comando inviato, in attesa di risposta", poi arriver� la risposta finale e verr� sovrascritta
                //altrimenti rimmarr� il messaggio del tipo "webapp agent not availabe, ecc
                GUtility.UpdateCommandRequest(tableId, result);  

            }
        }
        catch (Exception excp)
        {
            //errore, probabile mancanza di connessione
            GUtility.UpdateCommandRequest(tableId, excp.Message);  
        }


        //ricrico tutti i comandi inviati per questo device, dopo quello appena inviato
        this.CommandData = DBSettings.GetDeviceCommandList(this.DevType);
        List<CommandSentData> CommandSentData = GUtility.GetSentCommandByID(this.DevID.ToString(), true);

        var jsonSerialiser = new JavaScriptSerializer();
        var jsonNew = jsonSerialiser.Serialize(CommandSentData);
        jsonTableSent.Value = jsonNew;
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);
        
    }

    protected void Refresh_Onclick(object sender, EventArgs e)
    {


        //ricrico tutti i comandi inviati per questo device, dopo quello appena inviato
        this.CommandData = DBSettings.GetDeviceCommandList(this.DevType);
        List<CommandSentData> CommandSentData = GUtility.GetSentCommandByID(this.DevID.ToString(), true);

        var jsonSerialiser = new JavaScriptSerializer();
        var jsonNew = jsonSerialiser.Serialize(CommandSentData);
        jsonTableSent.Value = jsonNew;
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);

    }

    protected void Page_PreRender(object sender, EventArgs e) 
    {
    }

    protected void manDeviceListen_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        this.HandleError(e.Exception);
    }


    private void disableAll()
    {
        Response.BufferOutput = true;
        Response.Redirect("Unauthorized.aspx");
    }



    private void HandleError(Exception e)
    {
        string message = "";
        string stackTrace = "";

        if (e != null)
        {
            if (e.InnerException != null)
            {
                message = e.InnerException.Message ?? string.Empty;
                stackTrace = e.InnerException.StackTrace ?? string.Empty;
            }
            else
            {
                message = e.Message ?? string.Empty;
                stackTrace = e.StackTrace ?? string.Empty;
            }
        }

        string errorMessage = string.Format("Error -> {0}, Stack Trace -> {1}", message, stackTrace);


    }

    #endregion

    #region Methods

    private bool InitializeFieldsFromData(long devId) {

        string[] data = new string[10];
        
        try
        {
            data = DBSettings.GetinfoDevice(devId);
        }
        catch (Exception exc)
        {
            Tracer.TraceMessage(this.Context, exc);
            return false;
        }

        this.NodID = Convert.ToInt64(data[0]);
        this.RegID = Convert.ToInt64(data[1]);
        this.DevType = data[2];
        this.systemID = Convert.ToInt32(data[5]);

        //seconda parte, cerco il device type giusto

        string newType = "";
        try
        {
            newType = DBSettings.GetRealInfoDevice(this.DevType, devId);
        }
        catch (Exception exc)
        {
            Tracer.TraceMessage(this.Context, exc);
            return false;
        }

        this.DevType = newType;

        this.titolo.Text = "Selezione Comandi per il device '" + data[4] + "' (" + this.DevType + ") della stazione " + data[3];

        return true;
    }

    #endregion


}
﻿<%@ WebHandler Language="C#" Class="commandControlService" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;

public class commandControlService : IHttpHandler
{


        
    public void ProcessRequest (HttpContext context) {


        HttpResponse r = context.Response;

        string action = context.Request.QueryString["action"];

        if (action.Equals("CancellaRiga")) 
        {
            
            var id = context.Request.QueryString["id"];

            int res = GUtility.DeleteHistoryCommandRow(id);
        
        }  
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}


<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CommandControl.aspx.cs" Inherits="CommandControl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GRIS</title>
    <link href="../CSS/Gris.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link rel="icon" href="../favicon.ico" />
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <style type="text/css">
        .style1
        {
            width: 100px;
        }
        .style2
        {
            width: 124px;
        }
    </style>
</head>
<body style="margin: 0px; padding: 0px; border: 0px; background-color: #4c4c4c; width: 100%;color:white;" onload="SetupWindow();" onunload="" >
    <div id="sfondo" style="width: 100%;height: 200px;position: fixed;top: 0px;background-color: #777777;" runat="server">
    <h2 id="testoAttesa" style="text-align:center;margin-top:75px;" runat=server>Attendere... ScreenDump in progress...</h2>
    </div>
    
    <form id="frmSL" runat="server">

    <asp:HiddenField ID="jsonTableSent"        runat = server />
    <asp:HiddenField ID="devIdHidden"        runat = server />
    <asp:HiddenField ID="HiddenAutoSD"        runat = server />
    
    <p><asp:Label ID="titolo" runat=server></asp:Label></p>
 
    <table border="2" cellpadding="0" cellspacing="0" style="width: 100%; text-align: center;background-color: #4c4c4c;color:white;">
        <tr>
            <td style="text-align: left;width: 140px;">
                <asp:Label ID="Label2" runat=server>Comando</asp:Label>
            </td>
            <td style="text-align: left;">
                <asp:DropDownList  name="cmbSelectCommand_" id="cmbSelectCommand" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat=server>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:Label ID="Label3" runat=server>Descrizione</asp:Label>
            </td>
            <td style="text-align: left;">
                <asp:Label id="txtDescrizione" type="text" name="txtDescrizione" runat=server></asp:Label>
            </td>
        </tr>
        <tr id="trParametri" runat=server>
            <td style="text-align: left;">
                <asp:Label ID="Label4" runat=server>Parametri</asp:Label>
            </td>
            <td style="text-align: left;">
                <input id="txtParameters" type="text" name="txtParameters" runat=server size="100">
            </td>
        </tr>


    </table>
    <input id="txtDefaultParameters" type="text" name="txtDefaultParameters" runat=server size="100" style="display:none;">

    <br>
    <asp:Button  ID="sendCommandBtn" runat="server" EnableViewState="false" Text="Invia Comando" OnClick="Star_Onclick"/>
    <asp:Button  ID="refreshBtn" runat="server" EnableViewState="false" Text="Aggiorna" OnClick="Refresh_Onclick" style="float:right;margin-right:10px;"/>
    <div style="float:right;margin-right:10px;"><input type="checkbox" id="checkboxRefresh" name="" value="" runat=server >Auto Refresh</input></div>


    <p>Ultimo comando inviato al device</p>
    <div id="tableContainerLast">
    </div>

    <p style="float:left;margin-right:10px;">Ultimi comandi inviati al device</p>
    <asp:Button  ID="mostraComandiInviati" runat="server" EnableViewState="false" Text="Mostra/Nascondi" OnClientClick="showHideSentCommands(); return false;" style="float:left;margin-right:10px;margin-block-start: 1em;margin-block-end: 1em;"/>
    <div id="tableContainerAll">
    </div>

    </form>

    
  


</body>

<script src="../JS/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="CommandControl.js" type="text/javascript"></script>
<script src="gunzip.min.js" type="text/javascript"></script>
    

</html>

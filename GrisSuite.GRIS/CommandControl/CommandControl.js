﻿var UNKNOWN             = -1;
var IE                  = 0;
var OPERA               = 1;
var FIREFOX             = 2;
var SAFARI              = 3;
var CHROME              = 4;

var currentBrowser      = UNKNOWN;

var selectedElement     = 0;

document.onkeydown      = DisableCtrlN;
document.oncontextmenu = new Function("return false");

var test = false;

function GetBrowser() {

    if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1)      {
        return OPERA; 
    }
    else if (navigator.userAgent.indexOf("Chrome")  != -1)                                       {
        return CHROME; 
    }
    else if (navigator.userAgent.indexOf("Safari")  != -1)                                       {
        return SAFARI; 
    }
    else if (navigator.userAgent.indexOf("Firefox") != -1)                                       {
        return FIREFOX; 
    }
    else if ((navigator.userAgent.indexOf("MSIE")   != -1) || (!!document.documentMode == true)) {
        return IE; 
    }

    return UNKNOWN;
}

function DisplayMsg(sMsg) {

    if (document.getElementById('lblMsg')) {

        if (sMsg != undefined) {
            document.getElementById('lblMsg').innerHTML = sMsg;
        }
    }
}

function showHideSentCommands() {

    if ($("#tableContainerAll").is(":visible")) {
        $("#tableContainerAll").hide();
    }
    else {
        $("#tableContainerAll").show();
    }
}


function DisegnaTabella() {
    //console.log("entrato!!!");
    var jsonStr = document.getElementById('jsonTableSent').value;
    var devId = document.getElementById('devIdHidden').value;
    var autoSD = document.getElementById('HiddenAutoSD').value;
    //console.log(jsonStr);
    var json =JSON.parse(jsonStr);
    //console.log(json);

    
    var self = this;

    //ultimo comando inviato
    var ctr = recuperaDatiComandi(json, true); //solo l'ultimo
    var ctrObj = $(ctr);

    $('#tableContainerLast').html(ctrObj);

    ctrObj.find('[data-role-cancella-riga]').click(function (e) {
        var id = $(e.currentTarget).attr('data-role-cancella-riga');
        self.eliminaRiga(id);
        return;
    });

    ctrObj.find('[data-role-immagine-riga]').click(function (e) {
        var id = $(e.currentTarget).attr('data-role-immagine-riga');
        window.open('../ScreenDumpLocal/ScreenDump.aspx?dev=' + devId + '&row=' + id + '', '_blank', 'height=500,width=700,status=no,toolbar=no,menubar=no,location=no,resizable=yes');
        return false;
    });

    if (autoSD == "1") {
        var IdImmagine = ctrObj.find('[data-role-immagine-riga]').attr('data-role-immagine-riga');
        if (typeof (IdImmagine) != "undefined") {
            window.open('../ScreenDumpLocal/ScreenDump.aspx?dev=' + devId + '&row=' + IdImmagine + '', '_self', 'height=500,width=700,status=no,toolbar=no,menubar=no,location=no,resizable=yes');
        }
    }

    //altri comando inviato
    ctr = "";
    ctr = recuperaDatiComandi(json, false); //gli altri
    var ctrObj = $(ctr);

    $("#tableContainerAll").hide();
    $('#tableContainerAll').html(ctrObj);

    ctrObj.find('[data-role-cancella-riga]').click(function (e) {
        var id = $(e.currentTarget).attr('data-role-cancella-riga');
        self.eliminaRiga(id);
        return;
    });

    ctrObj.find('[data-role-immagine-riga]').click(function (e) {
        var id = $(e.currentTarget).attr('data-role-immagine-riga');
        window.open('../ScreenDumpLocal/ScreenDump.aspx?dev=' + devId + '&row=' + id + '', '_blank', 'height=500,width=700,status=no,toolbar=no,menubar=no,location=no,resizable=yes');
        return false;
    });

}

function recuperaDatiComandi(json, onlyFirst)
{
    var html = "";

    if (json.length == 0) return html;

    var start = 0;
    var end = 1;
    if (!onlyFirst) {
        start = 1;
        end = json.length;
    }
    
    for (var i = start; i < end; i++) {

        var color = "#4D4D4D";
        if (i % 2 == 0) color = "#333333";

        var dataRisposta = "";
        if (json[i].DateReceived != null) dataRisposta = json[i].DateReceived;
        var risposta = "";
        if (json[i].Response != null) risposta = json[i].Response;

        //la risposta potrebbe essere uno screen dump....
        var risposta2Show=risposta;
        var link = "";
        try {
            var rispostaJSON= JSON.parse(risposta);
            if (rispostaJSON.tipo=="screenDump")
            {
                
                //vecchia procedura con immagine mostrata nella finestra comandi
                /*
                //decomprimo
                var gunzip = new Zlib.Gunzip(rispostaJSON.dati);
                var plain = gunzip.decompress();

                // Now go ahead and create an ascii string from all those bytes.
                var asciistring = "";
                for (var t = 0; t < plain.length; t++) {
                    asciistring += String.fromCharCode(plain[t]);
                }

                risposta2Show = '<img data-role-immagine-riga="' + json[i].Command_historyID + '" src="data:image/' + rispostaJSON.formato + ';base64, ' + asciistring + '" alt="image" style="width: 100%;max-width: 500px;"/>';
                */

                //nuova procedura, mostro solo che ci sta uno screen dump
                risposta2Show = '<img data-role-immagine-riga="' + json[i].Command_historyID + '" src="../IMG/screenDumpImage.jpg" alt="image" style="width: 150px;"/>';

            }
        }
        catch(err) {
            //qualche errore nel parse
        }

        html += '<tr style="background-color:' + color + '"><td align="left"><p>' + json[i].DateSent + '</p></td>';
        html += '   <td align="left"><p>' + json[i].CommandName + '</p></td>';
        html += '   <td align="left"><p>' + json[i].Versione + '</p></td>';
        html += '   <td align="left"><p>' + json[i].Parametri + '</p></td>';
        html += '   <td align="left"><p>' + json[i].ExecutionUser + '</p></td>';
        html += '   <td align="left"><p>' + dataRisposta + '</p></td>';
        html += '   <td align="left"><p>' + risposta2Show + '</p></td>';
        html += '   <td align="left"><p><button Text="Cancella" data-role-cancella-riga="' + json[i].Command_historyID + '" style="float:right;margin-right:10px;">Cancella</button></p></td>';
        html += '</tr>';
    }

    var ctr = '<table cellspacing="0" cellpadding="4" border="0" id="tableBroker" style="color:White;font-size:Small;width:100%;border-collapse:collapse;">';
    ctr += '     <tbody>';
    ctr += '         <tr style="color:White;background-color:#AFAFAF;">';
    ctr += '         <th align="left" scope="col">Data Invio</th>';
    ctr += '         <th align="left" scope="col">Comando</th>';
    ctr += '         <th align="left" scope="col">Versione</th>';
    ctr += '         <th align="left" scope="col">Parametri</th>';
    ctr += '         <th align="left" scope="col">Utente</th>';
    ctr += '         <th align="left" scope="col">Data Risposta</th>';
    ctr += '         <th align="left" scope="col">Risposta</th>';
    ctr += '         <th align="left" scope="col">Azioni</th>';
    ctr += '         </tr>' + html + '</tbody></table>';

    return ctr;

}


function eliminaRiga(id) {
    //ritorno ai <p> modificando i campi

    var self = this;
    //alert(id);
    //var id= '<%= refreshBtn.ClientID%>';
    
    //alert("pippo");
    //<%= Page.ClientScript.GetPostBackEventReference(refreshBtn, String.Empty) %>
    //return;

    var obj = {
        'action': 'CancellaRiga',
        'id': id
    }

    $.ajax({
        type: "GET",
        url: "CommandControl.ashx",
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //var data = eval(response);

            $('#refreshBtn').click();

        },
        failure: function (response) {
            //alert(response);

            $('#refreshBtn').click();
        }
    });

    return false;

}

function caricaTabella(id) {
   

    var self = this;

    alert("pippo");
    return;

    var obj = {
        'action': 'CancellaRiga',
        'id': id
    }

    $.ajax({
        type: "GET",
        url: "CommandControl.ashx",
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //var data = eval(response);

            self.loadData();

        },
        failure: function (response) {
            //alert(response);

            self.loadData();
        }
    });

    return false;

}

function SetupWindow() {

    DisegnaTabella();

    currentBrowser = GetBrowser ();

    //DefaultWindowPositioning();

    setInterval(function () {
        if (document.getElementById("checkboxRefresh").checked==true) $('#refreshBtn').click();
        
     }, 3000);

}

function DefaultWindowPositioning() {
    
    CenterWindow            (document.body.offsetWidth, document.body.offsetHeight);

    window.resizeTo         (1000, 800);


}

function CenterWindow(windowWidth, windowHeight) {

    try {
        self.moveTo(Math.round((screen.availWidth - windowWidth) / 2), Math.round(((screen.availHeight - windowHeight) / 2.5)));
    }
    catch (e) { }
}

function DisableCtrlN() {
    try {
        if ((event.altKey) && (event.keyCode == 115/*f4*/)) {
            if (window.opener && !window.opener.closed) {
                window.opener.closeGris();
                window.close();
            }
        }
        else {
            return !((event.ctrlKey) && (event.keyCode == 78 || event.keyCode == 68))
        }
    }
    catch (e) { }
}

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
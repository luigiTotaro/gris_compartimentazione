using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    
	protected void tmrError_Tick ( object sender, EventArgs e )
	{
		if ( GrisSessionManager.LastUrlExists )
		{ 
			GrisSessionManager.Session.Remove(GrisSessionManager.GrisLayoutModeKey); // perch� lo desuma dall' url
		}
		else
		{
			GrisSessionManager.Session.RemoveAll();
		}

		this.Response.Redirect(GrisSessionManager.LastUrl); // rischio di loop infinito in caso di errore immediato, di default restituisce Italia.aspx
	}
}

<%@ Application Language="C#" %>

<script RunAt="server">
    
    void Application_Start(object sender, EventArgs e) {
    }

    void Application_End(object sender, EventArgs e) {
    }

    void Application_Error(object sender, EventArgs e) {
#if (!DEBUG)
        Exception ex = this.Server.GetLastError();

        if (ex != null) {
            string message;
            string stackTrace;
            if (ex.InnerException != null) {
                message = ex.InnerException.Message ?? string.Empty;
                stackTrace = ex.InnerException.StackTrace ?? string.Empty;
            }
            else {
                message = ex.Message ?? string.Empty;
                stackTrace = ex.StackTrace ?? string.Empty;
            }

            // Persiste l'eccezione serializzata in base dati
            if (!Tracer.TraceMessage(this.Context, ex, true)) {
                string errorMessage = string.Format("Error -> {0}, Stack Trace -> {1}, User -> {2}", message, stackTrace, GUtility.GetUserInfos(true));
                // Logga l'eccezione via trace listener (su disco)
                System.Diagnostics.Trace.TraceError(string.Format("{0}, User -> {1}", errorMessage, GUtility.GetUserInfos(true)));
            }
        }
#endif
    }

    void Application_PostAcquireRequestState(object sender, EventArgs e) {
        if (HttpContext.Current != null) {
            Tracer.TraceRequest(HttpContext.Current);
        }
    }

    void Session_Start(object sender, EventArgs e) {
        //Utility.Trace("Start Session:" + Session.SessionID);
    }

    void Session_End(object sender, EventArgs e) {
        //Utility.Trace("End Session:" + Session.SessionID);
    }

    protected void Application_PreRequestHandlerExecute(object sender, EventArgs e) {
        //string sKeys = "(nullo)";
        //if ( HttpContext.Current != null && HttpContext.Current.Session != null )
        //{
        //    sKeys = HttpContext.Current.Session.Keys.Count.ToString();
        //}

        //Utility.Trace("Start Request, Session Keys #" + sKeys);
    }
</script>


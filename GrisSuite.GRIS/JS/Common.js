﻿var _source;
var _popup;

function showConfirm(source, popup) {
    this._source = source;
    this._popup = $find(popup);
    if (this._popup != null) {
        this._popup.show();
    }
}
function okClick() {
    if (this._popup != null) {
        this._popup.hide();
    }
    if (this._source != null) {
        __doPostBack(this._source.name, '');
    }
}
function okClick2() {
    if (this._popup != null) {
        this._popup.hide();
    }
    if (this._source != null) {
        __doPostBack(this._source.id.replace(/_/g,'\$'), '');
    }
}
function cancelClick() {
    if (this._popup != null) {
        this._popup.hide();
        this._popup = null;
    }
    if (this._source != null) {
        this._source = null;
    }
}
function hasSilverlightPlugin() {
    var slplugin = false;
    var browser = navigator.appName;

    if (browser == 'Microsoft Internet Explorer') {
        try {
            var slControl = new ActiveXObject('AgControl.AgControl');
            if (slControl) {
                slplugin = true;
            }
        } catch (e) { }
    }
    else {
        try {
            if (navigator.plugins["Silverlight Plug-In"]) {
                slplugin = true;
            }
        } catch (e) { }
    }
    return slplugin;
}

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
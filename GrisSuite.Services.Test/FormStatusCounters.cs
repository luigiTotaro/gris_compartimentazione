﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GrisSuite.Services.Test.StatusCounters;

namespace GrisSuite.Services.Test
{
    public partial class FormStatusCounters : Form
    {
        public FormStatusCounters()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StatusCountersServiceClient sc = new StatusCountersServiceClient();
            sc.GetRegionCounters(Guid.NewGuid());
        }
    }
}

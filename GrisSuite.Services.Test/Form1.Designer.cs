﻿namespace GrisSuite.Services.Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.devicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fDSDevicesDataSet = new GrisSuite.Services.Test.FDSDevicesDataSet();
            this.devicesTableAdapter = new GrisSuite.Services.Test.FDSDevicesDataSetTableAdapters.devicesTableAdapter();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabFDS = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descrizioneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDescrizioneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.schedeLocaliBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fDSDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnAttrLocaliGroup = new System.Windows.Forms.Button();
            this.lblAttributiLocali = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.nomeDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDescrizioneDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attributiLocaliBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.valueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.attributiRemotiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.schedeRemoteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.nomeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descrizioneDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDescrizioneDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAttrRemotiGroup = new System.Windows.Forms.Button();
            this.lblAttributiRemoti = new System.Windows.Forms.Label();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.nomeDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoDescrizioneDataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.schedeLocaliBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusWSAddress = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnExport = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.devicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fDSDevicesDataSet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabFDS.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedeLocaliBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fDSDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributiLocaliBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributiRemotiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedeRemoteBindingSource)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valueBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedeLocaliBindingSource1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // devicesBindingSource
            // 
            this.devicesBindingSource.DataMember = "devices";
            this.devicesBindingSource.DataSource = this.fDSDevicesDataSet;
            // 
            // fDSDevicesDataSet
            // 
            this.fDSDevicesDataSet.DataSetName = "FDSDevicesDataSet";
            this.fDSDevicesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // devicesTableAdapter
            // 
            this.devicesTableAdapter.ClearBeforeFill = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabFDS);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1145, 646);
            this.tabControl1.TabIndex = 6;
            // 
            // tabFDS
            // 
            this.tabFDS.Controls.Add(this.btnExport);
            this.tabFDS.Controls.Add(this.splitContainer1);
            this.tabFDS.Controls.Add(this.comboBox1);
            this.tabFDS.Controls.Add(this.btnLoad);
            this.tabFDS.Location = new System.Drawing.Point(4, 22);
            this.tabFDS.Name = "tabFDS";
            this.tabFDS.Padding = new System.Windows.Forms.Padding(3);
            this.tabFDS.Size = new System.Drawing.Size(1137, 620);
            this.tabFDS.TabIndex = 0;
            this.tabFDS.Text = "FDS";
            this.tabFDS.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(18, 52);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(1106, 548);
            this.splitContainer1.SplitterDistance = 551;
            this.splitContainer1.TabIndex = 12;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(0, 30);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.btnAttrLocaliGroup);
            this.splitContainer2.Panel2.Controls.Add(this.lblAttributiLocali);
            this.splitContainer2.Panel2.Controls.Add(this.dataGridView3);
            this.splitContainer2.Size = new System.Drawing.Size(548, 515);
            this.splitContainer2.SplitterDistance = 257;
            this.splitContainer2.TabIndex = 10;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeDataGridViewTextBoxColumn,
            this.tipoDataGridViewTextBoxColumn,
            this.descrizioneDataGridViewTextBoxColumn,
            this.statoDataGridViewTextBoxColumn,
            this.statoDescrizioneDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.schedeLocaliBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(548, 257);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            this.nomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoDataGridViewTextBoxColumn
            // 
            this.tipoDataGridViewTextBoxColumn.DataPropertyName = "Tipo";
            this.tipoDataGridViewTextBoxColumn.HeaderText = "Tipo";
            this.tipoDataGridViewTextBoxColumn.Name = "tipoDataGridViewTextBoxColumn";
            this.tipoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // descrizioneDataGridViewTextBoxColumn
            // 
            this.descrizioneDataGridViewTextBoxColumn.DataPropertyName = "Descrizione";
            this.descrizioneDataGridViewTextBoxColumn.HeaderText = "Descrizione";
            this.descrizioneDataGridViewTextBoxColumn.Name = "descrizioneDataGridViewTextBoxColumn";
            this.descrizioneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statoDataGridViewTextBoxColumn
            // 
            this.statoDataGridViewTextBoxColumn.DataPropertyName = "Stato";
            this.statoDataGridViewTextBoxColumn.HeaderText = "Stato";
            this.statoDataGridViewTextBoxColumn.Name = "statoDataGridViewTextBoxColumn";
            this.statoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statoDescrizioneDataGridViewTextBoxColumn
            // 
            this.statoDescrizioneDataGridViewTextBoxColumn.DataPropertyName = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn.HeaderText = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn.Name = "statoDescrizioneDataGridViewTextBoxColumn";
            this.statoDescrizioneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // schedeLocaliBindingSource
            // 
            this.schedeLocaliBindingSource.DataMember = "SchedeLocali";
            this.schedeLocaliBindingSource.DataSource = this.fDSDataBindingSource;
            // 
            // fDSDataBindingSource
            // 
            this.fDSDataBindingSource.DataSource = typeof(GrisSuite.Services.Test.FDS.FDSData);
            // 
            // btnAttrLocaliGroup
            // 
            this.btnAttrLocaliGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAttrLocaliGroup.Location = new System.Drawing.Point(517, 3);
            this.btnAttrLocaliGroup.Name = "btnAttrLocaliGroup";
            this.btnAttrLocaliGroup.Size = new System.Drawing.Size(30, 21);
            this.btnAttrLocaliGroup.TabIndex = 2;
            this.btnAttrLocaliGroup.Text = "><";
            this.toolTip1.SetToolTip(this.btnAttrLocaliGroup, "Cambia il gruppo di attributi");
            this.btnAttrLocaliGroup.UseVisualStyleBackColor = true;
            this.btnAttrLocaliGroup.Click += new System.EventHandler(this.btnAttrLocaliGroup_Click);
            // 
            // lblAttributiLocali
            // 
            this.lblAttributiLocali.Location = new System.Drawing.Point(3, 0);
            this.lblAttributiLocali.Name = "lblAttributiLocali";
            this.lblAttributiLocali.Size = new System.Drawing.Size(517, 27);
            this.lblAttributiLocali.TabIndex = 1;
            this.lblAttributiLocali.Text = "Attributi Principali";
            this.lblAttributiLocali.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeDataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn2,
            this.statoDataGridViewTextBoxColumn4,
            this.statoDescrizioneDataGridViewTextBoxColumn4});
            this.dataGridView3.DataSource = this.attributiLocaliBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(0, 28);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(548, 226);
            this.dataGridView3.TabIndex = 0;
            // 
            // nomeDataGridViewTextBoxColumn4
            // 
            this.nomeDataGridViewTextBoxColumn4.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn4.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn4.Name = "nomeDataGridViewTextBoxColumn4";
            this.nomeDataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Valore";
            this.dataGridViewTextBoxColumn2.HeaderText = "Valore";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // statoDataGridViewTextBoxColumn4
            // 
            this.statoDataGridViewTextBoxColumn4.DataPropertyName = "Stato";
            this.statoDataGridViewTextBoxColumn4.HeaderText = "Stato";
            this.statoDataGridViewTextBoxColumn4.Name = "statoDataGridViewTextBoxColumn4";
            this.statoDataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // statoDescrizioneDataGridViewTextBoxColumn4
            // 
            this.statoDescrizioneDataGridViewTextBoxColumn4.DataPropertyName = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn4.HeaderText = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn4.Name = "statoDescrizioneDataGridViewTextBoxColumn4";
            this.statoDescrizioneDataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // attributiLocaliBindingSource
            // 
            this.attributiLocaliBindingSource.DataSource = this.valueBindingSource;
            // 
            // valueBindingSource
            // 
            this.valueBindingSource.DataMember = "Value";
            this.valueBindingSource.DataSource = this.attributiRemotiBindingSource;
            // 
            // attributiRemotiBindingSource
            // 
            this.attributiRemotiBindingSource.DataMember = "Attributi";
            this.attributiRemotiBindingSource.DataSource = this.schedeRemoteBindingSource;
            // 
            // schedeRemoteBindingSource
            // 
            this.schedeRemoteBindingSource.DataMember = "SchedeRemote";
            this.schedeRemoteBindingSource.DataSource = this.fDSDataBindingSource;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "FDS Locale";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer3.Location = new System.Drawing.Point(3, 30);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.dataGridView2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.btnAttrRemotiGroup);
            this.splitContainer3.Panel2.Controls.Add(this.lblAttributiRemoti);
            this.splitContainer3.Panel2.Controls.Add(this.dataGridView4);
            this.splitContainer3.Size = new System.Drawing.Size(545, 518);
            this.splitContainer3.SplitterDistance = 259;
            this.splitContainer3.TabIndex = 12;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeDataGridViewTextBoxColumn1,
            this.tipoDataGridViewTextBoxColumn1,
            this.descrizioneDataGridViewTextBoxColumn1,
            this.statoDataGridViewTextBoxColumn1,
            this.statoDescrizioneDataGridViewTextBoxColumn1});
            this.dataGridView2.DataSource = this.schedeRemoteBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(545, 259);
            this.dataGridView2.TabIndex = 11;
            this.dataGridView2.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_RowEnter);
            // 
            // nomeDataGridViewTextBoxColumn1
            // 
            this.nomeDataGridViewTextBoxColumn1.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn1.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn1.Name = "nomeDataGridViewTextBoxColumn1";
            this.nomeDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // tipoDataGridViewTextBoxColumn1
            // 
            this.tipoDataGridViewTextBoxColumn1.DataPropertyName = "Tipo";
            this.tipoDataGridViewTextBoxColumn1.HeaderText = "Tipo";
            this.tipoDataGridViewTextBoxColumn1.Name = "tipoDataGridViewTextBoxColumn1";
            this.tipoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // descrizioneDataGridViewTextBoxColumn1
            // 
            this.descrizioneDataGridViewTextBoxColumn1.DataPropertyName = "Descrizione";
            this.descrizioneDataGridViewTextBoxColumn1.HeaderText = "Descrizione";
            this.descrizioneDataGridViewTextBoxColumn1.Name = "descrizioneDataGridViewTextBoxColumn1";
            this.descrizioneDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // statoDataGridViewTextBoxColumn1
            // 
            this.statoDataGridViewTextBoxColumn1.DataPropertyName = "Stato";
            this.statoDataGridViewTextBoxColumn1.HeaderText = "Stato";
            this.statoDataGridViewTextBoxColumn1.Name = "statoDataGridViewTextBoxColumn1";
            this.statoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // statoDescrizioneDataGridViewTextBoxColumn1
            // 
            this.statoDescrizioneDataGridViewTextBoxColumn1.DataPropertyName = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn1.HeaderText = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn1.Name = "statoDescrizioneDataGridViewTextBoxColumn1";
            this.statoDescrizioneDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // btnAttrRemotiGroup
            // 
            this.btnAttrRemotiGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAttrRemotiGroup.Location = new System.Drawing.Point(514, 1);
            this.btnAttrRemotiGroup.Name = "btnAttrRemotiGroup";
            this.btnAttrRemotiGroup.Size = new System.Drawing.Size(30, 21);
            this.btnAttrRemotiGroup.TabIndex = 3;
            this.btnAttrRemotiGroup.Text = "><";
            this.toolTip1.SetToolTip(this.btnAttrRemotiGroup, "Cambia il gruppo di attributi");
            this.btnAttrRemotiGroup.UseVisualStyleBackColor = true;
            this.btnAttrRemotiGroup.Click += new System.EventHandler(this.btnAttrRemotiGroup_Click);
            // 
            // lblAttributiRemoti
            // 
            this.lblAttributiRemoti.Location = new System.Drawing.Point(3, 0);
            this.lblAttributiRemoti.Name = "lblAttributiRemoti";
            this.lblAttributiRemoti.Size = new System.Drawing.Size(515, 23);
            this.lblAttributiRemoti.TabIndex = 2;
            this.lblAttributiRemoti.Text = "Attributi Principali";
            this.lblAttributiRemoti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeDataGridViewTextBoxColumn5,
            this.statoDataGridViewTextBoxColumn5,
            this.statoDescrizioneDataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn4});
            this.dataGridView4.DataSource = this.valueBindingSource1;
            this.dataGridView4.Location = new System.Drawing.Point(0, 26);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.Size = new System.Drawing.Size(545, 229);
            this.dataGridView4.TabIndex = 0;
            // 
            // nomeDataGridViewTextBoxColumn5
            // 
            this.nomeDataGridViewTextBoxColumn5.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn5.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn5.Name = "nomeDataGridViewTextBoxColumn5";
            this.nomeDataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // statoDataGridViewTextBoxColumn5
            // 
            this.statoDataGridViewTextBoxColumn5.DataPropertyName = "Stato";
            this.statoDataGridViewTextBoxColumn5.HeaderText = "Stato";
            this.statoDataGridViewTextBoxColumn5.Name = "statoDataGridViewTextBoxColumn5";
            this.statoDataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // statoDescrizioneDataGridViewTextBoxColumn5
            // 
            this.statoDescrizioneDataGridViewTextBoxColumn5.DataPropertyName = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn5.HeaderText = "StatoDescrizione";
            this.statoDescrizioneDataGridViewTextBoxColumn5.Name = "statoDescrizioneDataGridViewTextBoxColumn5";
            this.statoDescrizioneDataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Valore";
            this.dataGridViewTextBoxColumn4.HeaderText = "Valore";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // valueBindingSource1
            // 
            this.valueBindingSource1.DataMember = "Value";
            this.valueBindingSource1.DataSource = this.attributiRemotiBindingSource;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "FDS Remoto";
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.DataSource = this.devicesBindingSource;
            this.comboBox1.DisplayMember = "Description";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(18, 15);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(938, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.ValueMember = "DevID";
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.Location = new System.Drawing.Point(968, 15);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 6;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // schedeLocaliBindingSource1
            // 
            this.schedeLocaliBindingSource1.DataMember = "SchedeLocali";
            this.schedeLocaliBindingSource1.DataSource = this.fDSDataBindingSource;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusWSAddress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 624);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1145, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusWSAddress
            // 
            this.statusWSAddress.Name = "statusWSAddress";
            this.statusWSAddress.Size = new System.Drawing.Size(0, 17);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(1049, 15);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 13;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 646);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "GrisSuite.Services Tester";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.devicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fDSDevicesDataSet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabFDS.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedeLocaliBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fDSDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributiLocaliBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributiRemotiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedeRemoteBindingSource)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valueBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedeLocaliBindingSource1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FDSDevicesDataSet fDSDevicesDataSet;
        private System.Windows.Forms.BindingSource devicesBindingSource;
        private GrisSuite.Services.Test.FDSDevicesDataSetTableAdapters.devicesTableAdapter devicesTableAdapter;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabFDS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.BindingSource schedeRemoteBindingSource;
        private System.Windows.Forms.BindingSource fDSDataBindingSource;
        private System.Windows.Forms.BindingSource schedeLocaliBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descrizioneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDescrizioneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView3;
        //private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn2;
        //private System.Windows.Forms.DataGridViewTextBoxColumn valoreDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn statoDataGridViewTextBoxColumn2;
        //private System.Windows.Forms.DataGridViewTextBoxColumn statoDescrizioneDataGridViewTextBoxColumn2;
        //private System.Windows.Forms.DataGridViewTextBoxColumn gruppoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource attributiLocaliBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descrizioneDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDescrizioneDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dataGridView4;
        //private System.Windows.Forms.DataGridViewTextBoxColumn gruppoDataGridViewTextBoxColumn1;
        //private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn3;
        //private System.Windows.Forms.DataGridViewTextBoxColumn statoDataGridViewTextBoxColumn3;
        //private System.Windows.Forms.DataGridViewTextBoxColumn statoDescrizioneDataGridViewTextBoxColumn3;
        //private System.Windows.Forms.DataGridViewTextBoxColumn valoreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource attributiRemotiBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDescrizioneDataGridViewTextBoxColumn4;
        private System.Windows.Forms.BindingSource valueBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoDescrizioneDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.BindingSource valueBindingSource1;
        private System.Windows.Forms.BindingSource schedeLocaliBindingSource1;
        private System.Windows.Forms.Label lblAttributiLocali;
        private System.Windows.Forms.Label lblAttributiRemoti;
        private System.Windows.Forms.Button btnAttrLocaliGroup;
        private System.Windows.Forms.Button btnAttrRemotiGroup;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusWSAddress;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}


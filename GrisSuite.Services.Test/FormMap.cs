﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GrisSuite.Services.Test.MAP;
using System.ServiceModel.Description;
using System.ServiceModel.Security;

namespace GrisSuite.Services.Test
{
    public partial class FormMap : Form
    {
        public FormMap()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MapServiceClient sc = new MapServiceClient();
            GrisMapElement[] t = sc.GetMapElementsByLayer(3,1);
            MessageBox.Show("Fatto");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MapServiceClient sc = new MapServiceClient();
            GrisMapLayer[] l = sc.GetMapLayersByClassification(1);
            MessageBox.Show("Fatto");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MapServiceClient sc = new MapServiceClient();
            //sc.ClientCredentials.Windows.ClientCredential.UserName = "d.marchi";
            //sc.ClientCredentials.Windows.ClientCredential.Domain = "rfi.it";
            //sc.ClientCredentials.Windows.ClientCredential.Password = "1qaz\"WSX";
            Classification[] l = sc.GetClassifications();
            MessageBox.Show("Fatto");

        }
    }
}

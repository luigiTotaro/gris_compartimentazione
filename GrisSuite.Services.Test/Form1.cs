﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GrisSuite.Services.Test.FDS;

namespace GrisSuite.Services.Test
{
    public partial class Form1 : Form
    {
        private GruppiAttributi GruppiAttributiLocaliActive = GruppiAttributi.Principale;
        private GruppiAttributi GruppiAttributiRemotiActive = GruppiAttributi.Principale;

        public Form1()
        {
            InitializeComponent();
        }

        private FDSServiceClient CreateFDSServiceClient()
        {
            FDS.FDSServiceClient fdsClient = new FDSServiceClient();
            return fdsClient;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.devicesTableAdapter.Fill(this.fDSDevicesDataSet.devices);
            statusWSAddress.Text = new FDS.FDSServiceClient().Endpoint.Address.Uri.AbsoluteUri;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                fDSDataBindingSource.DataSource = GetFDSData((long)comboBox1.SelectedValue);
                this.Cursor = Cursors.Default;
//                MessageBox.Show("Done!", "FDS Call", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "FDS Call", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private FDSData GetFDSData(long devId)
        {
            FDS.FDSServiceClient fdsClient = CreateFDSServiceClient();
            return fdsClient.Get(devId);
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                GruppiAttributiLocaliActive = GruppiAttributi.Principale;
                lblAttributiLocali.Text = "Attributi gruppo:" + GruppiAttributiLocaliActive.ToString();

                SchedaData sd = (dataGridView1.Rows[e.RowIndex].DataBoundItem as SchedaData);
                attributiLocaliBindingSource.DataSource = sd.Attributi[GruppiAttributiLocaliActive];
                btnAttrLocaliGroup.Enabled = (sd.Attributi.Keys.Count > 1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FDS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                GruppiAttributiRemotiActive = GruppiAttributi.Principale;
                lblAttributiRemoti.Text = "Attributi gruppo:" + GruppiAttributiRemotiActive.ToString();

                SchedaData sd = (dataGridView2.Rows[e.RowIndex].DataBoundItem as SchedaData);
                attributiRemotiBindingSource.DataSource = sd.Attributi[GruppiAttributiRemotiActive];
                btnAttrRemotiGroup.Enabled = (sd.Attributi.Keys.Count > 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FDS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAttrLocaliGroup_Click(object sender, EventArgs e)
        {
            try
            {
                GruppiAttributi nextGroup;
                if (GruppiAttributiLocaliActive == GruppiAttributi.Principale )
                    nextGroup = GruppiAttributi.E1;
                else
                    nextGroup = GruppiAttributi.Principale;

                if (dataGridView1.SelectedRows.Count == 0 
                    || (dataGridView1.SelectedRows[0].DataBoundItem as SchedaData) == null
                    || !((dataGridView1.SelectedRows[0].DataBoundItem as SchedaData).Attributi.Keys.Contains(nextGroup)))
                    return;

                SchedaData sd = (dataGridView1.SelectedRows[0].DataBoundItem as SchedaData);
                attributiLocaliBindingSource.DataSource = sd.Attributi[nextGroup];

                GruppiAttributiLocaliActive = nextGroup;
                lblAttributiLocali.Text = "Attributi gruppo:" + GruppiAttributiLocaliActive.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FDS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAttrRemotiGroup_Click(object sender, EventArgs e)
        {
            try
            {
                GruppiAttributi nextGroup;
                if (GruppiAttributiRemotiActive == GruppiAttributi.Principale)
                    nextGroup = GruppiAttributi.E1;
                else
                    nextGroup = GruppiAttributi.Principale;

                if (dataGridView2.SelectedRows.Count == 0
                    || (dataGridView2.SelectedRows[0].DataBoundItem as SchedaData) == null
                    || !((dataGridView2.SelectedRows[0].DataBoundItem as SchedaData).Attributi.Keys.Contains(nextGroup)))
                    return;

                SchedaData sd = (dataGridView2.SelectedRows[0].DataBoundItem as SchedaData);
                attributiRemotiBindingSource.DataSource = sd.Attributi[nextGroup];

                GruppiAttributiRemotiActive = nextGroup;
                lblAttributiRemoti.Text = "Attributi gruppo:" + GruppiAttributiRemotiActive.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FDS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.FileName = comboBox1.Text.Trim() + ".xml";
                saveFileDialog1.Filter = "*.xml|xml";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.Cursor = Cursors.WaitCursor;
                    FDSData fds = GetFDSData((long)comboBox1.SelectedValue);

                    DumpFDS(fds, saveFileDialog1.FileName);

                    if (Properties.Settings.Default.ExportFDSFields)
                    {
                        string fileFields = Path.GetDirectoryName(saveFileDialog1.FileName) + "\\" +
                                            Path.GetFileNameWithoutExtension(saveFileDialog1.FileName) +
                                            "_Fields" +
                                            Path.GetExtension(saveFileDialog1.FileName);

                        FDSData fdsFromFields = MapFieldsOnFDSData.GetFDSData((long)comboBox1.SelectedValue);

                        DumpFDS(fdsFromFields, fileFields);
                    }

                    fDSDataBindingSource.DataSource = fds;
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "FDS Call", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private static void DumpFDS(FDSData fds, string filename)
        {
            FileInfo f = new FileInfo(filename);
                    
            using (StreamWriter wr = f.CreateText())
            {
                        
                List<string> excludedMembers = new List<string>();
                excludedMembers.AddRange(new[] { "Root", "ExtensionData", "Gruppo", "IsRemote" });
                XmlDumper.Write(fds,10,wr,excludedMembers);
                wr.Close();
            }
        }

    }
}

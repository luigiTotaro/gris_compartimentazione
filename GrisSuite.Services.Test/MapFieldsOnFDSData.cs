﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.Services.Test.FDS;

namespace GrisSuite.Services.Test
{
    class MapFieldsOnFDSData
    {

        public static FDSData GetFDSData(long devid)
        {
           FDSFields ds = new FDSFields();
           FDSFieldsTableAdapters.FieldsTableAdapter ta = new FDSFieldsTableAdapters.FieldsTableAdapter();
           string caseFilter = "|0|1|2|255|";
           ta.Fill(ds.Fields,caseFilter, devid);

           FDSData fds = new FDSData();
           fds.SchedeLocali = new SchedaData[36];
           fds.SchedeRemote = new SchedaData[36];

           FillSchedeAlimetazione(ds.Fields, fds.SchedeLocali, false);
           FillSchedeAlimetazione(ds.Fields, fds.SchedeRemote, true);

           FillSchedeInterfaccia(ds.Fields, fds.SchedeLocali, false);
           FillSchedeInterfaccia(ds.Fields, fds.SchedeRemote, true);

           fds.SchedeLocali[15] = new SchedaData { Nome = "FD80000", Descrizione = "Gestione Ridondanza", Stato = StateEnum.Unknown, Tipo = TipoScheda.FD80000, IsRemote = false };
           fds.SchedeRemote[15] = new SchedaData { Nome = "FD80000", Descrizione = "Gestione Ridondanza", Stato = StateEnum.Unknown, Tipo = TipoScheda.FD80000, IsRemote = true };

            FillCPU(ds.Fields, fds.SchedeLocali, false);
            FillCPU(ds.Fields, fds.SchedeRemote, true);

            FillSchedaFibra(ds.Fields, fds.SchedeLocali, false);
            FillSchedaFibra(ds.Fields, fds.SchedeRemote, true);

           return fds;
        }

        private static void FillSchedeInterfaccia(FDSFields.FieldsDataTable fields, SchedaData[] schede, bool remoto)
        {
            int[] indexes = new[] { 5,6,7,8,9,10,11,12,17,18,19,20,21,22,23,24,29,30,31,32,33,34,35,36 };
            string sistema = remoto ? "remoto" : "locale";
            const string query = "StrID = 4 AND FieldName Like 'Sistema {0} - Scheda {1} %' ";

            FillSchede(indexes, fields, query, sistema, schede, GruppiAttributi.Principale);
        }

        private static void FillSchedeAlimetazione(DataTable fields, SchedaData[] schede, bool remoto)
        {
            int[] indexes = new[] {15,1,2,13,14,25,26};
            string sistema = remoto ? "remoto" : "locale";
            const string query = "StrID = 3 AND FieldName Like 'Sistema {0} - Scheda {1} %' ";

            FillSchede(indexes, fields, query, sistema, schede, GruppiAttributi.Principale);
        }

        private static void FillCPU(DataTable fields, SchedaData[] schede, bool remoto)
        {
            int[] indexes = new[] { 3, 27 };
            string sistema = remoto ? "remoto" : "locale";
            const string query = "StrID = 1 AND FieldName Like 'Sistema {0} - Stato CPU' ";

            FillSchede(indexes, fields, query, sistema, schede, GruppiAttributi.Principale);

            const string query2 = "StrID = 5 AND FieldName Like 'Sistema {0} - %' ";
            FillSchede(indexes, fields, query2, sistema, schede, GruppiAttributi.E1);
        }

        private static void FillSchedaFibra(DataTable fields, SchedaData[] schede, bool remoto)
        {
            int[] indexes = new[] { 2, 26 };
            string sistema = remoto ? "remoto" : "locale";
            const string query = "StrID = 1 AND FieldName Like 'Sistema {0} - Livello segnale fibra' ";

            FillSchede(indexes, fields, query, sistema, schede, GruppiAttributi.Principale);
        }


        private static void FillSchede(int[] indexes, DataTable fields, string query, string sistema, SchedaData[] schede, GruppiAttributi gruppo)
        {
            foreach (int index in indexes)
            {
                DataRow[] rows = fields.Select(string.Format(query, sistema, index));
                if (rows != null && rows.Count() > 0)
                {
                    int ai = 0;
                    SchedaAttributoData[] attributi = new SchedaAttributoData[rows.Count()];
                    foreach (var row in rows)
                    {
                        FDSFields.FieldsRow r = row as FDSFields.FieldsRow;
                        if (r != null)
                            attributi[ai++] = new SchedaAttributoData
                                                  {
                                                      Nome = r.FieldName, 
                                                      StatoDescrizione = 
                                                          r.Description, 
                                                      Valore = r.Value, 
                                                      Stato = ConvSevLevelToStato(r.SevLevel)
                                                  };
                    }

                    if (schede[index - 1] == null)
                        schede[index - 1] = new SchedaData();

                    if (schede[index - 1].Attributi == null)
                        schede[index - 1].Attributi = new Dictionary<GruppiAttributi, SchedaAttributoData[]>();

                    if (schede[index - 1].Attributi.Keys.Contains(gruppo))
                    {
                        schede[index - 1].Attributi[gruppo] = attributi;
                    }
                    else
                    {
                        schede[index - 1].Attributi.Add(gruppo, attributi);
                    }
                }
            }
        }

        private static StateEnum ConvSevLevelToStato(int level)
        {
            return (StateEnum)Enum.Parse(typeof(StateEnum), level.ToString());
        }
    }
}

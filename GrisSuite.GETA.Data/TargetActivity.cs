﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrisSuite.GETA.Data
{
    public partial class TargetActivity
    {
        public Issue GetServerLastIssue(long srvID)
        {
            Issue issue = null;

            var issues = from i in this.Issues
                where i.TargetID == srvID
                    && !i.IssueChildID.HasValue
                    && !i.IsDeleted
                    && !i.TargetActivity.IsObsolete
                orderby i.DateIssue descending, i.DateCreation descending
                select i;

            if (issues.Count() > 0) 
            {
                return issues.First();
            }

            return issue;
        }

        public IList<Issue> GetServerIssues(long srvID)
        {
            return (from i in this.Issues
                     where i.TargetID == srvID
                         && !i.IssueChildID.HasValue
                         && !i.IsDeleted
                         && !i.TargetActivity.IsObsolete
                     select i).ToList();

        }
    }
}

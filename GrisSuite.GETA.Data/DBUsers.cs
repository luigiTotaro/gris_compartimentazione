namespace GrisSuite.GETA.Data
{
    public partial class User
    {
        public string Name
        {
            get { return this.FirstName + " " + this.LastName; }
        }

        public string Nominativo
        {
            get 
            {   
                if (string.IsNullOrEmpty(this.Name.Trim())) {
                    return this.Username;
                } else {
                    return this.Name; 
                }
            }
        }
    }
}
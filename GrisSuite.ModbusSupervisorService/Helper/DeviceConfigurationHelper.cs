﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using GrisSuite.MODBUSSupervisorService.Database;
using System.Xml;
using GrisSuite.MODBUSSupervisorService.Utility;
using System.Globalization;
using System.Security;
using System.IO;
using System.Collections;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Helper
{
    public class DeviceConfigurationHelper : IDisposable
    {
        void    IDisposable.Dispose() { }
        public  void        Dispose() { }

        #region Costanti pubbliche

        public const ushort     TRANSACTION_ID_VALUE    = 0x0000; // Valore di default del transaction id
        public const ushort     PROTOCOL_ID_VALUE       = 0x0000; // Valore di default del protocol id
        public const byte       UNIT_ID_VALUE           = 0x01;   // Valore di default dello unit id

        #endregion

        #region Campi privati

        private ushort  _wTransactionID = TRANSACTION_ID_VALUE;

        private ushort  _wProtocolID    = PROTOCOL_ID_VALUE;

        private byte    _byUnitID       = UNIT_ID_VALUE;

        private XmlNode _root           = null; 

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///   Definizione della periferica
        /// </summary>
        public string   DefinitionFileName    { get; private set; }

        /// <summary>
        /// TransactionID utilizzato nella comunicazione modbus
        /// </summary>
        public ushort   wTransactionID        
        {
            get
            {
                return _wTransactionID;
            }
        }

        /// <summary>
        /// ProtocolID utilizzato nella comunicazione modbus
        /// </summary>
        public ushort   wProtocolID           
        {
            get
            {
                return _wProtocolID;
            }
        }

        /// <summary>
        /// UnitID utilizzato nella comunicazione modbus
        /// </summary>
        public byte     byUnitID              
        {
            get
            {
                return _byUnitID;
            }
        }

        #endregion

        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="sFile">file xml in cui viene salvata la definizione della periferica</param>
        public DeviceConfigurationHelper(string sFile)
        {
            this.DefinitionFileName = sFile;
            this.InitConfiguration();
        }

        #endregion

        public XmlNode root
        {
            get
            {
                return _root;
            }
        }

        #region Metodi pubblici

        /// <summary>
        /// Inizializzazione della lettura del file xml
        /// </summary>
        private void InitConfiguration()
        {
            try
            {
                this._root = this.GetDefinitionXmlRootElement();
                ParseDevice(this._root);
            }
            catch (DeviceConfigurationHelperException exc)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, exc.Message);
            }
        }

        /// <summary>
        /// Carica la definizione, la valida e popola la struttura di Stream e StreamField
        /// </summary>
        /// <returns></returns>
        public Collection<DBStream> LoadConfiguration()
        {
            Collection<DBStream>    streamData  = new Collection<DBStream>();

            if (root == null)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, string.Format(CultureInfo.InvariantCulture, "Collezione degli Stream da caricare nulla. Impossibile caricare la configurazione della periferica.")); 
            }
            else
            {
                foreach (XmlNode streams in root.ChildNodes)
                {
                    foreach (XmlNode stream in streams.ChildNodes)
                    {
                        DBStream streamObj = null;

                        if (!ParseStream(stream, out streamObj))
                            continue;

                        foreach (XmlNode streamField in stream.ChildNodes)
                        {
                            StreamFieldRequestObject    strRequest;
                            List<DBStreamField>         strListField;

                            if (!ParseStreamField(streamField, out strListField, out strRequest))
                                continue;

                            foreach (DBStreamField strFieldObj in strListField)
                            {
                                foreach (XmlNode fieldValueDescription in streamField.ChildNodes)
                                {
                                    DBStreamFieldValueDescription strValueDescriptionObj = null;

                                    if (!ParseFieldValueDescription(fieldValueDescription, strFieldObj, out strValueDescriptionObj))
                                        continue;

                                    strFieldObj.DbStreamFieldValueDescriptions.Add(strValueDescriptionObj);
                                }

                                streamObj.FieldData.Add(strFieldObj);
                            }

                            streamObj.StreamRequests.Add(strRequest);
                        }

                        if (streamObj.FieldData.Count > 0) {

                            streamData.Add(streamObj);      // Lo Stream è aggiunto solo se contiene almeno uno Stream Field
                        }
                    }
                }
            }

            return streamData;
        }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Estrae dal tag device i valori di transactionID, protocolID e unitID
        /// </summary>
        /// <param name="xmlNode">Nodo da analizzare</param>
        /// <returns>true : nodo analizzato correttamente</returns>
        private bool ParseDevice(XmlNode xmlNode)
        {
            if (!xmlNode.Name.Equals("device", StringComparison.OrdinalIgnoreCase))
                return false;

            try {

                this._wTransactionID    = DecodeDeviceTransactionID (xmlNode);
                this._wProtocolID       = DecodeDeviceProtocolID    (xmlNode);
                this._byUnitID          = DecodeDeviceUnitID        (xmlNode);
            }
            catch (DeviceConfigurationHelperException exc)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, exc.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Crea l'oggetto dbstream dagli attributi del tag nel file delle definizioni
        /// </summary>
        /// <param name="xmlNode">Nodo da analizzare</param>
        /// <param name="str">oggetto dbstream creato</param>
        /// <returns>true : dbstream creato correttamente</returns>
        private bool ParseStream(XmlNode xmlNode, out DBStream str)
        {
            str = null;

            if (!xmlNode.Name.Equals("stream", StringComparison.OrdinalIgnoreCase))
                return false;

            try {

                int     streamId                                = DecodeStreamID                                (xmlNode);
                string  sName                                   = DecodeStreamName                              (xmlNode);
                string  sClass                                  = DecodeStreamClass                             (xmlNode);
                bool    streamExcludedFromSeverityComputation   = DecodeStreamExcludedFromSeverityComputation   (xmlNode);

                str = new DBStream(streamId, sName,  sClass, streamExcludedFromSeverityComputation);
            }
            catch (DeviceConfigurationHelperException exc)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, exc.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Crea l'oggetto dbstreamfield dagli attributi del tag nel file delle definizioni
        /// </summary>
        /// <param name="xmlNode">Nodo da analizzare</param>
        /// <param name="str">oggetto dbstreamfield creato</param>
        /// <returns>true : dbstreamfield creato correttamente</returns>
        private bool ParseStreamField(XmlNode xmlNode, out List<DBStreamField> strList, out StreamFieldRequestObject strRequest)
        {
            strRequest  = null;
            strList     = new List<DBStreamField>();

            if (!xmlNode.Name.Equals("streamfield", StringComparison.OrdinalIgnoreCase))
                return false;

            try
            {
                int                         streamFieldId                               = DecodeStreamFieldID                               (xmlNode);
                string                      sName                                       = DecodeStreamFieldName                             (xmlNode);
                byte                        byFunCode                                   = DecodeStreamFieldFunctionCode                     (xmlNode);
                ushort                      wOffset                                     = DecodeStreamFieldOffset                           (xmlNode);
                ushort                      wCount                                      = DecodeStreamFieldCount                            (xmlNode);
                bool                        streamFieldExcludedFromSeverityComputation  = DecodeStreamFieldExcludedFromSeverityComputation  (xmlNode);
                bool                        hideIfNoDataAvailable                       = DecodeStreamFieldHideIfNoDataAvailable            (xmlNode);
                FormatExpression            formatExpression                            = DecodeStreamFieldFormatExpression                 (xmlNode);
                NoDataAvailableForcedState  noDataAvailableForcedState                  = DecodeStreamFieldNoDataAvailableForcedState       (xmlNode);

                for (int j = 0; j < wCount; j++) {
                    
                    strList.Add(new DBStreamField(streamFieldId, byFunCode, sName, wOffset, wCount, j, streamFieldExcludedFromSeverityComputation, noDataAvailableForcedState, hideIfNoDataAvailable, formatExpression));
                }

                strRequest = new StreamFieldRequestObject(streamFieldId, byFunCode, wOffset, wCount);
            }
            catch (DeviceConfigurationHelperException exc)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, exc.Message);
                return false;
            }

            return true;
        }


        /// <summary>
        /// Crea l'oggetto fieldvaluedescription
        /// </summary>
        /// <param name="xmlNode">Nodo da analizzare</param>
        /// <param name="str">oggetto fieldvaluedescription creato</param>
        /// <returns>true : fieldvaluedescription creato correttamente</returns>
        private bool ParseFieldValueDescription(XmlNode xmlNode, DBStreamField streamField, out DBStreamFieldValueDescription str)
        {
            str = null;

            if (!xmlNode.Name.Equals("fieldvaluedescription", StringComparison.OrdinalIgnoreCase))
                return false;

            try {

                string                  sDescription    = DecodeFieldValueDescription       (xmlNode);
                Severity                severity        = DecodeFieldValueSeverity          (xmlNode);
                ushort?                 exactUint       = DecodeFieldValueExactValueInt     (xmlNode);
                ushort?                 maskBit         = DecodeFieldMaskBitValue           (xmlNode);
                bool?                   exactBool       = DecodeFieldValueExactValueBool    (xmlNode);
                DBStreamFieldValueRange valueRange      = DecodeFieldValueRange             (xmlNode);
                bool                    isDefault       = DecodeFieldValueIsDefault         (xmlNode);
                string                  exactString     = DecodeFieldValueExactValueString  (xmlNode);

                str = new DBStreamFieldValueDescription(streamField, isDefault, sDescription, severity, valueRange, exactUint, exactBool, exactString, maskBit);
            }
            catch (DeviceConfigurationHelperException exc)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, exc.Message);
                return false;
            }

            return true;
        }

        #region Validazione degli attributi del nodo device

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito uno TransactionID valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>TransactionID del device trovato</returns>
        private ushort DecodeDeviceTransactionID(XmlNode xmlNode)
        {
            string sTmp;
            ushort wTransactionID = TRANSACTION_ID_VALUE; // Valore di default

            if (ExistAttr(xmlNode, "TransactionID", out sTmp))
            {
                if (sTmp.StartsWith("0x"))
                {
                    sTmp = sTmp.Substring(2);

                    if (!ushort.TryParse(sTmp, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out wTransactionID))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                    "L'attributo 'TransactionID' del nodo 'Device' nel file {0}, non contiene un valore UInt16. Valore contenuto: '{1}'",
                                                                    this.DefinitionFileName, sTmp));
                    }
                }
                else
                {
                    if (!ushort.TryParse(sTmp, out wTransactionID))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                    "L'attributo 'TransactionID' del nodo 'Device' nel file {0}, non contiene un valore UInt16. Valore contenuto: '{1}'",
                                                                    this.DefinitionFileName, sTmp));
                    }
                }
            }

            return wTransactionID;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito uno ProtocolID valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>ProtocolID del device trovato</returns>
        private ushort DecodeDeviceProtocolID(XmlNode xmlNode)
        {
            string sTmp;
            ushort wProtocolID = PROTOCOL_ID_VALUE; // Valore di default

            if (ExistAttr(xmlNode, "ProtocolID", out sTmp))
            {
                if (sTmp.StartsWith("0x"))
                {
                    sTmp = sTmp.Substring(2);

                    if (!ushort.TryParse(sTmp, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out wProtocolID))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                    "L'attributo 'ProtocolID' del nodo 'Device' nel file {0}, non contiene un valore UInt16. Valore contenuto: '{1}'",
                                                                    this.DefinitionFileName, sTmp));
                    }
                }
                else
                {
                    if (!ushort.TryParse(sTmp, out wProtocolID))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                    "L'attributo 'ProtocolID' del nodo 'Device' nel file {0}, non contiene un valore UInt16. Valore contenuto: '{1}'",
                                                                    this.DefinitionFileName, sTmp));
                    }
                }
            }
            
            return wProtocolID;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito uno UnitID valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>UnitID del device trovato</returns>
        private byte DecodeDeviceUnitID(XmlNode xmlNode)
        {
            string  sTmp;
            byte    byUnitID = UNIT_ID_VALUE; // Valore di default

            if (ExistAttr(xmlNode, "UnitID", out sTmp))
            {
                if (sTmp.StartsWith("0x"))
                {
                    sTmp = sTmp.Substring(2);

                    if (!byte.TryParse(sTmp, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byUnitID))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                    "L'attributo 'UnitID' del nodo 'Device' nel file {0}, non contiene un valore Byte. Valore contenuto: '{1}'",
                                                                    this.DefinitionFileName, sTmp));
                    }
                }
                else
                {
                    if (!byte.TryParse(sTmp, out byUnitID))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                    "L'attributo 'UnitID' del nodo 'Device' nel file {0}, non contiene un valore Byte. Valore contenuto: '{1}'",
                                                                    this.DefinitionFileName, sTmp));
                    }
                }
            }

            return byUnitID;
        }

        #endregion

        #region Validazione degli attributi del nodo stream

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito uno streamid valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>ID dello stream trovato</returns>
        private int DecodeStreamID(XmlNode xmlNode)
        {
            int     streamId;
            string  sTmp;

            if (!ExistAttr(xmlNode, "ID", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'ID' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            if (!int.TryParse(sTmp, out streamId))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'ID' del nodo 'Stream' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, sTmp));

            }

            return streamId;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un nome dello stream valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Nome dello stream trovato</returns>
        private string DecodeStreamName(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "Name", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'Name' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            return sTmp; 
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un function code dello stream valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Function code dello stream trovato</returns>
        private byte DecodeStreamFieldFunctionCode(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "function_code", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'function_code' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            if (sTmp.StartsWith("0x"))
            {
                sTmp = sTmp.Substring(2);
            }

            byte byTmp;
            if(byte.TryParse(sTmp, out byTmp))
            {
                if ((byTmp != ModbusMasterObject.FC_READ_COIL)              &&
                    (byTmp != ModbusMasterObject.FC_READ_HOLDING_REGISTER)  &&
                    (byTmp != ModbusMasterObject.FC_READ_INPUT_DISCRETE)    &&
                    (byTmp != ModbusMasterObject.FC_READ_INPUT_REGISTER))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "L'attributo 'function_code' del nodo 'Stream' nel file {0}, non contiene un function code gestito da questo supervisore. Valore contenuto: '{1}'",
                            this.DefinitionFileName, sTmp));
                }
            }
            else
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "L'attributo 'function_code' del nodo 'Stream' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                            this.DefinitionFileName, sTmp));
            return byTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo class dello stream valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo class dello stream trovato</returns>
        private string DecodeStreamClass(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "class", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'class' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            return sTmp; 
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo ExcludedFromSeverityComputation dello stream valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo ExcludedFromSeverityComputation dello stream trovato, ovvero il valore di default 'false'</returns>
        private bool DecodeStreamExcludedFromSeverityComputation(XmlNode xmlNode)
        {
            bool   bTmp = false;
            string sTmp;

            if (ExistAttr(xmlNode, "ExcludedFromSeverityComputation", out sTmp))
            {
                if (!bool.TryParse(sTmp, out bTmp))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'ExcludedFromSeverityComputation' del nodo 'Stream' nel file {0}, non contiene un valore booleano. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, sTmp));
                }
            }

            return bTmp;
        }

        #endregion

        #region Validazione degli attributi del nodo streamfield

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito uno streamfieldid valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>ID dello streamfield trovato</returns>
        private int DecodeStreamFieldID(XmlNode xmlNode)
        {
            int streamId;
            string sTmp;

            if (!ExistAttr(xmlNode, "ID", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'ID' sul nodo 'StreamField', oppure il nome dello StreamField è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            if (!int.TryParse(sTmp, out streamId))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'ID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, sTmp));

            }

            return streamId;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un nome dello streamfield valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Nome dello streamfield trovato</returns>
        private string DecodeStreamFieldName(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "Name", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'Name' sul nodo 'StreamField', oppure il nome dello StreamField è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            return sTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo offset dello streamfield valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo offset dello streamfield trovato</returns>
        private ushort DecodeStreamFieldOffset(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "offset", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'offset' sul nodo 'StreamField', oppure il nome dello StreamField è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            ushort wTmp;

            if (sTmp.StartsWith("0x"))
            {
                sTmp = sTmp.Substring(2);

                if (!ushort.TryParse(sTmp, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out wTmp))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'offset' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, sTmp));
                }
            }
            else
            {
                if (!ushort.TryParse(sTmp, out wTmp))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'offset' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, sTmp));
                }
            }

            return wTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo count dello streamfield valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo count dello streamfield trovato</returns>
        private ushort DecodeStreamFieldCount(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "count", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'count' sul nodo 'StreamField', oppure il nome dello StreamField è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            ushort wTmp;

            if (!ushort.TryParse(sTmp, out wTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'count' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, sTmp));

            }

            return wTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo ExcludedFromSeverityComputation dello streamfield valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo ExcludedFromSeverityComputation dello stream trovato, ovvero il valore di default 'false'</returns>
        private bool DecodeStreamFieldExcludedFromSeverityComputation(XmlNode xmlNode)
        {
            bool bTmp = false;
            string sTmp;

            if (ExistAttr(xmlNode, "ExcludedFromSeverityComputation", out sTmp))
            {
                if (!bool.TryParse(sTmp, out bTmp))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'ExcludedFromSeverityComputation' del nodo 'StreamField' nel file {0}, non contiene un valore booleano. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, sTmp));
                }
            }

            return bTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo HideIfNoDataAvailable dello streamfield valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo HideIfNoDataAvailable dello streamfield trovato, ovvero il valore di default 'false'</returns>
        private bool DecodeStreamFieldHideIfNoDataAvailable(XmlNode xmlNode)
        {
            bool bTmp = false;
            string sTmp;

            if (ExistAttr(xmlNode, "HideIfNoDataAvailable", out sTmp))
            {
                if (!bool.TryParse(sTmp, out bTmp))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'HideIfNoDataAvailable' del nodo 'StreamField' nel file {0}, non contiene un valore booleano. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, sTmp));
                }
            }

            return bTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un attributo HideIfNoDataAvailable dello streamfield valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Attributo HideIfNoDataAvailable dello streamfield trovato</returns>
        private NoDataAvailableForcedState DecodeStreamFieldNoDataAvailableForcedState(XmlNode xmlNode)
        {
            NoDataAvailableForcedState noDataAvailableForcedState = null;

            string sTmp;

            if (ExistAttr(xmlNode, "NoDataAvailableForcedState", out sTmp))
            {
                string[] values = sTmp.Split(';');

                if (values.Length == 3)
                {
                    object severity;

                    try
                    {
                        severity = Enum.Parse(typeof(Severity), values[0].Trim(), true);
                    }
                    catch (ArgumentException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown). Valore contenuto: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   values[0].Trim()));
                    }

                    string value = values[1].Trim();

                    if (string.IsNullOrEmpty(value))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene una stringa per il valore valida, oppure la stringa è vuota. Valore contenuto: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   values[1].Trim()));
                    }

                    string valueDescription = values[2].Trim();

                    if (string.IsNullOrEmpty(valueDescription))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene una stringa per la descrizione del valore valida, oppure la stringa è vuota. Valore contenuto: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   values[2].Trim()));
                    }

                    if ((severity != null) && (severity is Severity))
                    {
                        noDataAvailableForcedState = new NoDataAvailableForcedState((Severity)severity, valueDescription,
                                                                                    value);
                    }
                    else
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown). Valore contenuto: '{1}'",
                                                                                   this.DefinitionFileName,
                                                                                   values[0].Trim()));
                    }
                }
                else
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene tre valori separati da punti e virgola, ad esempio 'Error;0;Non in esecuzione'. Valore contenuto: '{1}'",
                                                                                this.DefinitionFileName,
                                                                                xmlNode.Attributes["NoDataAvailableForcedState"].Value.Trim()));
            }

            return noDataAvailableForcedState;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlNode"></param>
        /// <returns></returns>
        private FormatExpression DecodeStreamFieldFormatExpression(XmlNode xmlNode)
        {
            string              sTmp;
            FormatExpression    formatExpression = null;

            if (ExistAttr(xmlNode, "ValueFormatExpression", out sTmp))
            {
                formatExpression = new FormatExpression(sTmp);
            }

            return formatExpression;
        }

        #endregion

        #region Validazione degli attributi del nodo fieldvaluedescription

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito una descrizione dello fieldvaluedescription valido
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Descrizione dello fieldvaluedescription trovato</returns>
        private string DecodeFieldValueDescription(XmlNode xmlNode)
        {
            string sTmp;

            if (!ExistAttr(xmlNode, "ValueDescription", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'ValueDescription' sul nodo 'FieldValueDescription', oppure il nome dello FieldValueDescription è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            return sTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definita la severità 
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Severità dello fieldvaluedescription trovata</returns>
        private Severity DecodeFieldValueSeverity(XmlNode xmlNode)
        {
            string      sTmp;
            Severity    severity = Severity.Unknown;

            if (!ExistAttr(xmlNode, "Severity", out sTmp))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare l'attributo 'Severity' sul nodo 'FieldValueDescription', oppure il nome dello FieldValueDescription è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                                                                         this.DefinitionFileName, xmlNode.OuterXml));
            }

            try {
                
                object objSeverity = (Severity)Enum.Parse(typeof(Severity), sTmp, true);

                if (objSeverity == null)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
                                                                                                             this.DefinitionFileName, sTmp));
                }

                severity = (Severity)objSeverity;
            }
            catch (ArgumentException)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
                                                                           this.DefinitionFileName, sTmp));
            }

            return severity;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un valore ushort di confronto
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Valore ushort di confronto trovato</returns>
        private ushort? DecodeFieldValueExactValueInt(XmlNode xmlNode)
        {
            string  sTmp;
            ushort? wTmp = null;

            if (ExistAttr(xmlNode, "ExactValueInt", out sTmp))
            {
                ushort exactValueUshort;

                if (!ushort.TryParse(sTmp, out exactValueUshort))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "L'attributo 'ExactValueInt' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                               this.DefinitionFileName, sTmp));
                }

                wTmp = exactValueUshort;
            }

            return wTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definita una maschera di bit di confronto
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Maschera di bit trovata</returns>
        private ushort? DecodeFieldMaskBitValue(XmlNode xmlNode)
        {
            string sTmp;
            ushort? wTmp = null;

            if (ExistAttr(xmlNode, "BitMask", out sTmp))
            {
                ushort maskBit;

                if (sTmp.StartsWith("0x"))
                {
                    sTmp = sTmp.Substring(2);

                    if (!ushort.TryParse(sTmp, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out maskBit))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "L'attributo 'BitMask' del nodo 'FieldValueDescription' nel file {0}, non contiene una maschera di bit valida. Valore contenuto: '{1}'",
                                                                                   this.DefinitionFileName, sTmp));
                    }
                }
                else
                {
                    if (!ushort.TryParse(sTmp, out maskBit))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "L'attributo 'BitMask' del nodo 'FieldValueDescription' nel file {0}, non contiene una maschera di bit valida. Valore contenuto: '{1}'",
                                                                                   this.DefinitionFileName, sTmp));
                    }
                }

                wTmp = maskBit;
            }

            return wTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un valore booleano di confronto
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>Valore booleano di confronto trovato</returns>
        private bool? DecodeFieldValueExactValueBool(XmlNode xmlNode)
        {
            string sTmp;
            bool?  bTmp = null;

            if (ExistAttr(xmlNode, "ExactValueBool", out sTmp))
            {
                bool exactValueBool;

                if (!bool.TryParse(sTmp, out exactValueBool))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "L'attributo 'ExactValueBool' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                               this.DefinitionFileName, sTmp));
                }

                bTmp = exactValueBool;
            }

            return bTmp;
        }

        /// <summary>
        /// Verifica se nel nodo specificato è stato definito un intervallo di valori di confronto
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>intervallo di confronto trovato</returns>
        private DBStreamFieldValueRange DecodeFieldValueRange(XmlNode xmlNode)
        {
            long                    l;
            string                  sTmpMax;
            string                  sTmpMin;
            DBStreamFieldValueRange valueRange = null;

            if (ExistAttr(xmlNode, "RangeValueMin", out sTmpMin) && ExistAttr(xmlNode, "RangeValueMax", out sTmpMax))
            {
                if (!long.TryParse(sTmpMin, out l))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "L'attributo 'RangeValueMin' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore valido. Valore contenuto: '{1}'",
                                                                                                             this.DefinitionFileName, sTmpMin));
                }

                if (!long.TryParse(sTmpMax, out l))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "L'attributo 'RangeValueMax' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore valido. Valore contenuto: '{1}'",
                                                                                                             this.DefinitionFileName, sTmpMax));
                }

                valueRange = new DBStreamFieldValueRange(sTmpMin, sTmpMax);
            }
            else if (!ExistAttr(xmlNode, "RangeValueMin", out sTmpMin) && ExistAttr(xmlNode, "RangeValueMax", out sTmpMax))
            {
                if (!long.TryParse(sTmpMax, out l))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "L'attributo 'RangeValueMax' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore valido. Valore contenuto: '{1}'",
                                                                                                             this.DefinitionFileName, sTmpMax));
                }

                valueRange = new DBStreamFieldValueRange(long.MinValue.ToString(), sTmpMax);
            }
            else if (ExistAttr(xmlNode, "RangeValueMin", out sTmpMin) && !ExistAttr(xmlNode, "RangeValueMax", out sTmpMax))
            {
                if (!long.TryParse(sTmpMin, out l))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture, "L'attributo 'RangeValueMin' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore valido. Valore contenuto: '{1}'",
                                                                                                             this.DefinitionFileName, sTmpMin));
                }

                valueRange = new DBStreamFieldValueRange(sTmpMin, long.MaxValue.ToString());
            }

            return valueRange;
        }

        /// <summary>
        /// Verifica se il nodo specificato è stato definito come valore di default
        /// </summary>
        /// <param name="xmlNode">nodo sul quale fare la ricerca</param>
        /// <returns>true : si tratta del valore di defaut dello stream field</returns>
        private bool DecodeFieldValueIsDefault(XmlNode xmlNode)
        {
            string  sTmp;
            bool    bTmp = false;

            if (ExistAttr(xmlNode, "IsDefault", out sTmp))
            {
                bool exactValueBool;

                if (!bool.TryParse(sTmp, out exactValueBool))
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                               "L'attributo 'IsDefault' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                               this.DefinitionFileName, sTmp));
                }

                bTmp = exactValueBool;
            }

            return bTmp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlNode"></param>
        /// <returns></returns>
        private string DecodeFieldValueExactValueString(XmlNode xmlNode)
        {
            string sTmp;
            string sExactTmp = null;
            
            if (ExistAttr(xmlNode, "ExactValueString", out sTmp))
            {
                sExactTmp = sTmp;
            }

            return sExactTmp;
        }

        #endregion
        
        /// <summary>
        /// Verifica se esiste l'attributo specificato in un determinato nodo e ne restituisce il valore come string
        /// </summary>
        /// <param name="xmlNode">Nodo sul quale fare la ricerca</param>
        /// <param name="sName">Nome dell'attributo da cercare</param>
        /// <param name="sValue">valore dell'attributo trovato</param>
        /// <returns>true : l'attributo è stato trovato</returns>
        private bool ExistAttr(XmlNode xmlNode, string sName, out string sValue)
        {
            sValue = "";
            bool bRet = false;

            try
            {
                IEnumerator en = xmlNode.Attributes.GetEnumerator();

                en.Reset();

                while (en.MoveNext())
                {
                    XmlAttribute xmlAttribue = (XmlAttribute)en.Current;

                    if (xmlAttribue.Name.Equals(sName, StringComparison.OrdinalIgnoreCase))
                    {
                        sValue = xmlAttribue.Value.Trim();
                        bRet = true;
                        break;
                    }
                }
            }
            catch { }

            return bRet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private XmlNode GetDefinitionXmlRootElement()
        {
            if (string.IsNullOrEmpty(this.DefinitionFileName))
            {
                throw new DeviceConfigurationHelperException("Nome del file di configurazione per la periferica non definito");
            }
            if (!FileUtility.CheckFileCanRead(this.DefinitionFileName))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Impossibile leggere il file di configurazione: {0}",
                                                                           this.DefinitionFileName));
            }

            XmlDocument doc = new XmlDocument();
            XmlNode root;

            try
            {
                doc.Load(this.DefinitionFileName);
                root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                  this.DefinitionFileName), ex);
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                  this.DefinitionFileName), ex);
            }
            catch (IOException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                  this.DefinitionFileName), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                  this.DefinitionFileName), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                  this.DefinitionFileName), ex);
            }
            catch (SecurityException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                  this.DefinitionFileName), ex);
            }

            if (root == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                                                           this.DefinitionFileName));
            }

            if (!root.Name.Equals("Device", StringComparison.OrdinalIgnoreCase))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Impossibile trovare l'elemento 'Device' come nodo radice del file di configurazione {0}.",
                                                                           this.DefinitionFileName));
            }

            return root;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using GrisSuite.MODBUSSupervisorService.Core;
using GrisSuite.MODBUSSupervisorService.Database;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;
using GrisSuite.MODBUSSupervisorService.Utility;
using System.Security;

namespace GrisSuite.MODBUSSupervisorService.Helper
{
    public static class SchedulerXmlHelper
    {
        /// <summary>
        ///   File di configurazione (System.xml) da cui caricare i profili di schedulazione
        /// </summary>
        public static string ConfigurationSystemFile { get; private set; }

        /// <summary>
        ///   Recupera la lista dei profili di schedulazione da file di configurazione System.xml
        /// </summary>
        /// <param name = "configurationSystemFile">File di configurazione (System.xml) da cui caricare i profili di schedulazione</param>
        /// <returns>Lista di ProfileObject da utilizzare</returns>
        public static List<ProfileObject> GetProfileList(string configurationSystemFile)
        {
            List<ProfileObject> profiles    = null;
            ConfigurationSystemFile         = configurationSystemFile;

            try
            {
                profiles = LoadProfilesFromSystemXml();
            }
            catch (ConfigurationSystemException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                               String.Format("Errore nel caricamento della lista dei profili di schedulazione {0}", ex.Message));
            }

            return profiles;
        }

        /// <summary>
        ///   Carica dal file di configurazione System.xml i profili di schedulazione
        /// </summary>
        /// <returns>La lista dei profili caricati</returns>
        private static List<ProfileObject> LoadProfilesFromSystemXml()
        {
            XmlNode root = GetDefinitionXmlRootElement();

            List<ProfileObject> profiles = new List<ProfileObject>();

            // Cerco il nodo con i profili di schedulazione                                                                  
            if (root != null)
            {
                foreach (XmlNode rootElement in root.ChildNodes)
                {
                    if (rootElement.Name.Equals("scheduler", StringComparison.OrdinalIgnoreCase))
                    {
                        foreach (XmlNode profileNode in rootElement.ChildNodes)
                        {
                            if (profileNode.NodeType != XmlNodeType.Element)
                                continue;

                            using (ProfileObject profile = new ProfileObject())
                            {
                                profile.Decode(profileNode.Attributes);

                                foreach (XmlNode activityNode in profileNode.ChildNodes)
                                {
                                    if (activityNode.NodeType != XmlNodeType.Element)
                                        continue;

                                    using (ActivityObject activity = new ActivityObject())
                                    {
                                        activity.Decode(activityNode.Attributes);

                                        profile.Activities.Add(activity);
                                    }
                                }

                                profiles.Add(profile);
                            }
                        }
                    }
                }
            }

            return profiles;
        }

        #region Metodi privati

        private static XmlNode GetDefinitionXmlRootElement()
        {
            if (String.IsNullOrEmpty(ConfigurationSystemFile))
            {
                throw new ConfigurationSystemException("Nome del file di configurazione con la lista dei dispositivi da monitorare non definito");
            }

            if (!FileUtility.CheckFileCanRead(ConfigurationSystemFile))
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Impossibile leggere il file di configurazione con la lista dei dispositivi: {0}",
                                                                     ConfigurationSystemFile));
            }

            XmlDocument doc = new XmlDocument();
            XmlNode root;

            try
            {
                doc.Load(ConfigurationSystemFile);
                root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
            }
            catch (ArgumentException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
            }
            catch (IOException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
            }
            catch (SecurityException ex)
            {
                throw new ConfigurationSystemException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
            }

            if (root == null)
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                                                                     ConfigurationSystemFile));
            }

            if (!root.Name.Equals("telefin", StringComparison.OrdinalIgnoreCase))
            {
                throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                     "Impossibile trovare l'elemento 'telefin' come nodo radice del file di configurazione {0}.",
                                                                     ConfigurationSystemFile));
            }

            return root;
        }

        #endregion

    }
}

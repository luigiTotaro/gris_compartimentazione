﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace GrisSuite.MODBUSSupervisorService
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        private static void Main()
        {
#if (!_DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new MainService() 
			};
            ServiceBase.Run(ServicesToRun);
#else
            using (MainService service = new MainService())
            {
                service.Run();
            }

            
#endif
        }

#if(_DEBUG)
        public static bool Run()
        {
            using (MainService service = new MainService())
            {
                service.Run();
            }

            return true;
        }
#endif
    }
}

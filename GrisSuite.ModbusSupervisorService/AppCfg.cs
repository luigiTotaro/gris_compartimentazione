﻿using System;
using System.Configuration;
using Microsoft.Win32;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Diagnostics;
using GrisSuite.MODBUSSupervisorService.Utility;

namespace GrisSuite.AppCfg
{
	public class AppCfg
	{

        #region Nomi dei valori di configurazione da leggere dal file .config o dal registro

        private const string    REGISTRY_KEY_NAME               = "RegistryKey";
        private const string    PATH_LOG_FOLDER_NAME            = "PathLogFolder";
        private const string    XML_SYSTEM_NAME                 = "XMLSystem";

        #endregion

        #region Valori di default dei parametri di configurazione da leggere dal file .config o dal registro

        private const string    PATH_KEY_REGISTRY_HEADER        = "HKEY_LOCAL_MACHINE\\SOFTWARE\\";
        private const string    PATH_KEY_STLC1000               = "Telefin\\STLC1000\\";

        private const string    PATH_LOG_FOLDER_DEFAULT         = ".\\";
        private const string    XML_SYSTEM_NAME_DEFAULT         = "System.xml";

        #endregion

        #region Campi privati

        private string          _sWorkingDir                    = ".\\";
        private string          _sPathKeyOnRegistry             = PATH_KEY_REGISTRY_HEADER + PATH_KEY_STLC1000;

        private string          _sPathLogFolder                 = PATH_LOG_FOLDER_DEFAULT;
        private string          _sXMLSystem                     = XML_SYSTEM_NAME_DEFAULT;

        #endregion

        #region Proprietà pubbliche

        public string sPathKeyOnRegistry
        {
            get
            {
                return _sPathKeyOnRegistry;
            }
        }

        public string sPathLogFolder
        {
            get
            {
                return _sPathLogFolder;
            }
        }

        public string sXMLSystem
        {
            get
            {
                return _sXMLSystem;
            }
        }

        public string sWorkingDir
        {
            get
            {
                return _sWorkingDir;
            }
        }

        #endregion

		#region Creazione Singleton per AppConst

        private static AppCfg _instance;

        /// <summary>
        /// Recupero dell'istanza singleton della classe
        /// </summary>
        public static AppCfg Default
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AppCfg();
                }

                return _instance;
            }
        }

        /// <summary>
        /// Costruttore
        /// </summary>
		protected AppCfg()
		{
			try
			{
                //Inizializazione working dir
                this.InitWorkingDir();

                //Caricamento parametri di configurazione
                this.LoadAllCfg();
			}
			catch (Exception ex)
			{
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, ex.ToString());
			}
		}

        /// <summary>
        /// Recupera la directory di installazione del servizio
        /// </summary>
        private void InitWorkingDir()
        {
            try
            {
                System.IO.FileInfo ff   = new System.IO.FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location);
                this._sWorkingDir       = ff.Directory.ToString() + "\\";
            }
            catch (Exception e)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, String.Format("ECCEZIONE InitWorkingDir(): {0}", e.Message));
            }
        }

		#endregion

        #region Funzioni per la gestione dei parametri di configurazione

        /// <summary>
        /// Caricamento all'interno della classe singleton di tutti i parametri di configurazione necessari
        /// </summary>
        private bool LoadAllCfg()
        {
            string registryKeyPath = "";
            registryKeyPath = getElementFromConfigFile(REGISTRY_KEY_NAME);
            if ((registryKeyPath == null) || (registryKeyPath == ""))
            {
                if (IntPtr.Size == 8 || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
                {
                    registryKeyPath = PATH_KEY_REGISTRY_HEADER + "Wow6432Node\\" + PATH_KEY_STLC1000;
                }
                else
                {
                    registryKeyPath = PATH_KEY_REGISTRY_HEADER + PATH_KEY_STLC1000;
                }
            }

            this._sPathKeyOnRegistry    = registryKeyPath;
            this._sPathLogFolder        = GetCfgValue<string>(this._sPathKeyOnRegistry, PATH_LOG_FOLDER_NAME, _sWorkingDir);
            this._sXMLSystem            = GetCfgValue<string>(this._sPathKeyOnRegistry, XML_SYSTEM_NAME, _sWorkingDir + XML_SYSTEM_NAME_DEFAULT);

            return true;
        }

        /// <summary>
        /// Recupera il valore di un parametro di configurazione secondo il seguente ordine di priorità:
        /// 1° ritorna il valore definito nel file di confugurazione locale -> ".config"
        /// 2° se la ricerca al punto precedente non ha avuto esito, ritorna il valore definito nel registro
        /// 3° se la ricerca al punto precedente non ha avuto esito, ritorna il valore di default
        /// In caso di eccezione nella funzione, ritorna cmq il valore di default
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="valueName"></param>
        /// <param name="defaultValue"></param>
        /// <returns>valore del parametro di configurazione in formato stringa</returns>
        private T GetCfgValue<T>(string keyName, string valueName, object defaultValue)
        {
            T sRet = (T)Convert.ChangeType(defaultValue, typeof(T));

            try
            {
                string elementCfg = "";
                elementCfg = getElementFromConfigFile(valueName);

                if (( elementCfg == null) || (elementCfg == ""))
                {
                    object o = Registry.GetValue(keyName, valueName, null);

                    if (o != null)
                    {
                        sRet = (T)Convert.ChangeType(o, typeof(T));
                    }
                    return sRet;
                }
                sRet = (T)Convert.ChangeType(elementCfg, typeof(T));
            }
            catch (Exception e)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, e.ToString());
            }

            return sRet;
        }

        /// <summary>
        /// Lettura di un singolo elemento dal file di configurazione
        /// </summary>
        private string getElementFromConfigFile(string key)
        {
            string element = "";
            try
            {
                element = ConfigurationManager.AppSettings[key];
            }
            catch
            {
                return "";
            }

            return element;
        }

        /// <summary>
        /// Logga i parametri della configurazione che verranno utilizzati caricati allo start del servizio
        /// </summary>
        public void LogAllCfg()
        {
            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, String.Format("WorkingDir: {0}", GrisSuite.AppCfg.AppCfg.Default.sWorkingDir));
            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, String.Format("RegistryKey: {0}", GrisSuite.AppCfg.AppCfg.Default.sPathKeyOnRegistry));
            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, String.Format("PathLogFolder: {0}", GrisSuite.AppCfg.AppCfg.Default.sPathLogFolder));
            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, String.Format("XMLSystem: {0}", GrisSuite.AppCfg.AppCfg.Default.sXMLSystem));
        }

        #endregion

    }
}
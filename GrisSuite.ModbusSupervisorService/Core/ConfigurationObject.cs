﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class ConfigurationObject
    {
        private const int                   MODBUS_DEFAULT_PORT  = 502; 

        private static ConfigurationObject _instance;

                /// <summary>
        /// Instanza corrente della classe
        /// </summary>
        public static ConfigurationObject Instance
        {
            get {

                if (_instance == null)
                {
                    _instance = new ConfigurationObject();
                }

                return _instance;
            }
        }

         ///<summary>
         ///Costruttore
         ///</summary>
        public ConfigurationObject()
        {
        }

        /// <summary>
        /// Valore di default della porta utilizzata nel protocollo modbus
        /// </summary>
        public int ModbusPort
        {
            get
            {
                if (Settings.Default.ModbusDefaultPort != 0)
                {
                    return Settings.Default.ModbusDefaultPort;
                }

                return MODBUS_DEFAULT_PORT;
            }
        }

    }
}

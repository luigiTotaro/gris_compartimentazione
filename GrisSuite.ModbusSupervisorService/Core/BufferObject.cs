﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class BufferObject
    {
        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        public BufferObject(int lenBuffer)
        {
            byData      = new byte[MsgObject.MAX_LEN_BUF];
            byBuffer    = new byte[MsgObject.MAX_LEN_BUF];
            nLenBuffer  = 0;
            nLenData    = 0;
        }

        #endregion

        #region Proprietà pubbliche

        public byte[]   byBuffer    { get; set; }

        public byte[]   byData      { get; set; }

        public int      nLenBuffer  { get; set; }

        public int      nLenData    { get; set; }

        #endregion


        #region Metodi pubblici

        public void Flush()
        {
            nLenData    = 0;
            nLenBuffer  = 0;
            Array.Clear(byData,     0, MsgObject.MAX_LEN_BUF);
            Array.Clear(byBuffer,   0, MsgObject.MAX_LEN_BUF);
        }

        public override string ToString()
        {
            string sTmp = "";

            for (int j = 0; j < nLenBuffer; j++)
            {
                sTmp += String.Format(" {0:x2}", byBuffer[j]);
            }

            return sTmp;
        }

        #endregion
    }
}

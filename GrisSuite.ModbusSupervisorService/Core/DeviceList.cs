﻿using System;
using System.Collections.Generic;
using System.Text;
using GrisSuite.MODBUSSupervisorService.Helper;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class DeviceList
    {
        private static DeviceList   _instance;
        private List<DeviceObject>  _devices;

        /// <summary>
        /// Istanza corrente della classe
        /// </summary>
        public static DeviceList Instance
        {
            get {

                if (_instance == null)
                {
                    _instance = new DeviceList();
                }

                return _instance;
            }
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        public DeviceList()
        {
            this._devices = new List<DeviceObject>();
        }

        /// <summary>
        /// Lista dei device associati a questo supervisore
        /// </summary>
        public List<DeviceObject> Devices
        {
            get
            {
                if (Instance._devices.Count == 0)
                {
                    ReadDevicesFromFile();
                }

                return this._devices;
            }
        }

        /// <summary>
        /// Lettura da file system.xml della lista periferiche associate al supervisiore modbus
        /// </summary>
        private void ReadDevicesFromFile()
        {
            this._devices = new List<DeviceObject>(SystemXmlHelper.GetMonitorDeviceList(AppCfg.AppCfg.Default.sXMLSystem));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using GrisSuite.MODBUSSupervisorService.Database;
using GrisSuite.MODBUSSupervisorService.Utility;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class ModbusMasterList
    {
        private static ModbusMasterList  _instance;
        private List<ModbusMasterObject> _modbusMasters;

        #region Istanza corrente della classe

        /// <summary>
        /// Istanza corrente della classe
        /// </summary>
        public static ModbusMasterList Instance
        {
            get {

                if (_instance == null)
                {
                    _instance = new ModbusMasterList();
                }

                return _instance;
            }
        }

        #endregion

        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        public ModbusMasterList()
        {
            this._modbusMasters = new List<ModbusMasterObject>();
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Lista dei master associati a questo supervisore
        /// </summary>
        public List<ModbusMasterObject> ModbusMasters
        {
            get
            {
                if (Instance._modbusMasters.Count == 0)
                {
                    LoadMastersFromDevices();
                }

                return this._modbusMasters;
            }
        }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Popola l'elenco dei master modbus in base alla lista dei device
        /// </summary>
        private void LoadMastersFromDevices()
        {
            foreach (DeviceObject device in DeviceList.Instance.Devices)
            {
                this._modbusMasters.Add(new ModbusMasterObject(device, device.Address.ToString(), device.EndPoint.Port));
            }
        }

        /// <summary>
        /// Accoda la lettura di uno o più coil dallo slave
        /// </summary>
        /// <param name="deviceid">identificativo del device corrispondente allo slave</param>
        /// <param name="wRefNumber">offeset iniziale a partire dal quale leggere il valore dei coil</param>
        /// <param name="wBitCount">numero di coil contigui da leggere</param>
        private void ReadCoils(ModbusMasterObject modBusMaster, ushort wRefNumber, ushort wBitCount)
        {
            //FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
            //                               string.Format("ReadCoils Device Id {0}", modBusMaster.DeviceId));


            if (modBusMaster != null)
            {
                modBusMaster.Enqueue(() => modBusMaster.TxReadCoil(wRefNumber, wBitCount));
            }
        }

        /// <summary>
        /// Accoda la lettura di uno o più discrete input dallo slave
        /// </summary>
        /// <param name="deviceid">identificativo del device corrispondente allo slave</param>
        /// <param name="wRefNumber">offset iniziale a partire dal quale leggere il valore dei discrete input</param>
        /// <param name="wBitCount">numero di discrete input da leggere</param>
        private void ReadInputsDiscrete(ModbusMasterObject modBusMaster, ushort wRefNumber, ushort wBitCount)
        {
            if (modBusMaster != null)
            {
                modBusMaster.Enqueue(() => modBusMaster.TxReadInputDiscrete(wRefNumber, wBitCount));
            }
        }

        /// <summary>
        /// Accoda la lettura di uno o più holding register
        /// </summary>
        /// <param name="deviceid">identificativo del device corrispondente allo slave</param>
        /// <param name="wRefNumber">offset iniziale a partire dal quale leggere il valore del holding register</param>
        /// <param name="wWordCount">numero di holding register da leggere</param>
        private void ReadHoldingRegister(ModbusMasterObject modBusMaster, ushort wRefNumber, ushort wWordCount)
        {
            if (modBusMaster != null)
            {
                modBusMaster.Enqueue(() => modBusMaster.TxReadHoldingRegister(wRefNumber, wWordCount));
            }
        }

        /// <summary>
        /// Accoda la lettura di uno o più input register
        /// </summary>
        /// <param name="deviceid">identificativo del device corrispondente allo slave</param>
        /// <param name="wRefNumber">offset iniziale a partire dal quale leggere il valore degli input register</param>
        /// <param name="wWordCount">numero di input register da leggere</param>
        private void ReadInputRegister(ModbusMasterObject modBusMaster, ushort wRefNumber, ushort wWordCount)
        {
            if (modBusMaster != null)
            {
                modBusMaster.Enqueue(() => modBusMaster.TxReadInputRegister(wRefNumber, wWordCount));
            }
        }

        /// <summary>
        /// Accoda la scrittura di un singolo coil
        /// </summary>
        /// <param name="modBusMaster">master che deve inviare la richiesta</param>
        /// <param name="wRefNumber">offset del bit da settare</param>
        /// <param name="bTurn_ON">true : il bit deve essere settato a 1</param>
        private void WriteCoil(ModbusMasterObject modBusMaster, ushort wRefNumber, bool bTurn_ON)
        {
            if (modBusMaster != null)
            {
                modBusMaster.Enqueue(() => modBusMaster.TxWriteCoil(wRefNumber, bTurn_ON));
            }
        }

        /// <summary>
        /// Accoda la scrittura di un singolo registro
        /// </summary>
        /// <param name="modBusMaster">master che deve inviare la richiesta</param>
        /// <param name="wRefNumber">offset del registro da settare</param>
        /// <param name="wValue">valore da settare sul registro</param>
        private void WriteSingleRegister(ModbusMasterObject modBusMaster, ushort wRefNumber, ushort wValue)
        {
            if (modBusMaster != null)
            {
                modBusMaster.Enqueue(() => modBusMaster.TxWriteHoldingRegister(wRefNumber, wValue));
            }
        }

        /// <summary>
        /// Verifica se l'attività specificata può essere effettuata nell'ora
        /// corrente del giorno
        /// </summary>
        /// <param name="activity">attività da controllare</param>
        /// <returns>false : l'attività non va schedulata</returns>
        private bool IsValidTime(ActivityObject activity)
        {
            bool bRet = true;

            if (DateTime.Now.TimeOfDay < activity.tsFrom)
                bRet = false;

            if (DateTime.Now.TimeOfDay > activity.tsTo)
                bRet = false;

            return bRet;
        }

        /// <summary>
        /// Verifica se l'attività specificata può essere effettuata nel giorno 
        /// corrente della settimana
        /// </summary>
        /// <param name="activity">attività da controllare</param>
        /// <returns>false : l'attività non và schedulata</returns>
        private bool IsValidDay(ActivityObject activity)
        {
            bool bRet = true;

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    bRet = activity.DoW.bMonday;
                    break;
                case DayOfWeek.Tuesday:
                    bRet = activity.DoW.bTuesday;
                    break;
                case DayOfWeek.Wednesday:
                    bRet = activity.DoW.bWednesday;
                    break;
                case DayOfWeek.Thursday:
                    bRet = activity.DoW.bThursday;
                    break;
                case DayOfWeek.Friday:
                    bRet = activity.DoW.bFriday;
                    break;
                case DayOfWeek.Saturday:
                    bRet = activity.DoW.bSaturday;
                    break;
                case DayOfWeek.Sunday:
                    bRet = activity.DoW.bSunday;
                    break;
            }

            return bRet;
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Smista le richieste allo slave modbus della periferica
        /// secondo le proprità configurate nell'attivita di schedulazione dello stream
        /// </summary>
        /// <param name="device">periferica associata allo slave modbus</param>
        public void MonitorDevice(DeviceObject device)
        {
            ProfileObject       profile;
            ActivityObject      activity;
            ModbusMasterObject  modBusMaster = ModbusMasters.Find(ModbusMasterObject.ByDeviceId(device.DeviceId));

            //FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose, string.Format("Monitoraggio device {0} -> ", device.Address));


            foreach (DBStream stream in modBusMaster.modbusSlave.StreamData)
            {
                if ((profile = ProfileList.Instance.Profiles.Find(ProfileObject.ById(device.ProfileId))) == null)
                {
                    //FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                    //                                           string.Format("device {0} profile = null", device.Address));
                    continue;
                }

                if ((activity = profile.Activities.Find(ActivityObject.ByClass(stream.sClass))) == null)
                {
                    //FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                    //                                           string.Format("device {0} activity = null", device.Address));
                    continue;
                }

                if (!IsValidTime(activity) || !IsValidDay(activity))
                {
                    //FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                    //                                           string.Format("device {0} IsValidTime InValidDay KO", device.Address));
                    continue;
                }

                if (DateTime.Now < stream.dtNextRequest)
                {
                    //FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                    //                                           string.Format("device {0} DateTime.Now < stream.dtNextRequest", device.Address));
                    continue;
                }

                foreach (StreamFieldRequestObject strRequest in stream.StreamRequests)
                {
                    switch (strRequest.byFunCode)
                    {
                        case ModbusMasterObject.FC_READ_COIL:
                            ReadCoils           (modBusMaster, strRequest.wOffset, strRequest.wCount);
                            break;
                        case ModbusMasterObject.FC_READ_INPUT_DISCRETE:
                            ReadInputsDiscrete  (modBusMaster, strRequest.wOffset, strRequest.wCount);
                            break;
                        case ModbusMasterObject.FC_READ_HOLDING_REGISTER:
                            ReadHoldingRegister (modBusMaster, strRequest.wOffset, strRequest.wCount);
                            break;
                        case ModbusMasterObject.FC_READ_INPUT_REGISTER:
                            ReadInputRegister   (modBusMaster, strRequest.wOffset, strRequest.wCount);
                            break;
                    }
                }
                stream.dtLastRequest = DateTime.Now;

                switch (activity.Mode)
                {
                    case ActivityMode.Continue:
                        stream.dtNextRequest = DateTime.MinValue;
                        break;
                    case ActivityMode.StartUp:
                        stream.dtNextRequest = DateTime.MaxValue;
                        break;
                    case ActivityMode.Interval:
                        stream.dtNextRequest = stream.dtLastRequest.Add(activity.tsInterval);
                        break;
                }
            }

        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using GrisSuite.MODBUSSupervisorService.Utility;
using System.Globalization;
using GrisSuite.MODBUSSupervisorService.Database;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Threading;
using System.Collections.ObjectModel;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public enum DeviceWriteMode
    {
        Insert,
        Update
    };

    public class DatabaseObject
    {
        private static DatabaseObject _instance;

        #region Istanza corrente della classe

        /// <summary>
        /// Instanza corrente della classe
        /// </summary>
        public static DatabaseObject Instance
        {
            get  {

                if (_instance == null)
                {
                    _instance = new DatabaseObject();
                }

                return _instance;
            }
        }

        #endregion

        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        public DatabaseObject(bool? persistOnDatabase = null)
        {
            this.lastException      = null;
            this.persistenceFactory = new DatabaseFactory(persistOnDatabase);  // non inzializzando persistOnDatabase (= null), lasciamo che si usi la configurazione da file
            this.InitializeDb();
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// 
        /// </summary>
        public DatabaseFactory  persistenceFactory  { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public SqlConnection    dbConnection        { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public SqlConnection    dbEventsConnection  { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Exception        lastException       { get; private set; }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Aggiorna o inserisce una periferica sul db
        /// </summary>
        /// <param name="device">periferica da inserire/aggiornare</param>
        /// <param name="deviceWriteMode">specifica se la periferica deve essere inserita o aggiornata</param>
        /// <returns>true = operazione di inserimento/aggiornamento riuscita</returns>
        public bool UpdateInsertDevice(DeviceObject device, DeviceWriteMode deviceWriteMode)
        {
            bool returnValue = false;

            DeviceDatabaseSupport deviceDatabaseSupport = persistenceFactory.GetStationBuildingRackPortData(dbConnection.ConnectionString, device.Topography, device.ServerId, device.PortId);

            if (deviceDatabaseSupport != null)
            {
                if (persistenceFactory.UpdateDevice(dbConnection.ConnectionString, 
                                                    device.DeviceId, 
                                                    device.NodeId, 
                                                    device.ServerId,
                                                    device.Name, 
                                                    device.DeviceType, 
                                                    device.SerialNumber, 
                                                    device.Address,
                                                    deviceDatabaseSupport.PortId, 
                                                    device.ProfileId, 
                                                    device.Active, 
                                                    device.Scheduled,
                                                    0, 
                                                    deviceDatabaseSupport.RackId, 
                                                    device.RackPositionRow, 
                                                    device.RackPositionColumn))
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Periferica correttamente {0}. {1}, {2}",
                                                                                 (deviceWriteMode == DeviceWriteMode.Insert
                                                                                      ? "inserita" : "aggiornata"), device,
                                                                                 deviceDatabaseSupport));

                    returnValue = true;
                }
                else
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Errore nell'{0} della periferica su database. {1}, {2}",
                                                                                 (deviceWriteMode == DeviceWriteMode.Insert
                                                                                      ? "inserimento" : "aggiornamento"), device,
                                                                                 deviceDatabaseSupport));
                }
            }
            else
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                               string.Format(CultureInfo.InvariantCulture,
                                                                             "Impossibile trovare i dati della Stazione/Fabbricato/Armadio/Interfaccia/Server su Database, per poter {0} la periferica. {1}",
                                                                             (deviceWriteMode == DeviceWriteMode.Insert ? "creare" : "aggiornare"),
                                                                             device));
            }

            return returnValue;
        }

        /// <summary>
        /// Cancella una periferica dal database
        /// </summary>
        /// <param name="device">oggetto periferica da cancellare</param>
        /// <returns>true : la cancellazione è riiuscita</returns>
        public bool DeleteDevice(DeviceObject device)
        {
            bool returnValue = false;

            if (persistenceFactory.DeleteDevice(dbConnection.ConnectionString, device.DeviceId))
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                               string.Format(CultureInfo.InvariantCulture, "Periferica correttamente eliminata. {0}",
                                                                             device));

                returnValue = true;
            }
            else
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                               string.Format(CultureInfo.InvariantCulture,
                                                                             "Errore nell'eliminazione della periferica su database. {0}",
                                                                             device));
            }

            return returnValue;
        }

        /// <summary>
        /// Verifica l'esistenza nel database di una periferica
        /// </summary>
        /// <param name="deviceId">id della periferica</param>
        /// <returns>true : la periferica è presente nel db</returns>
        public bool? CheckDeviceExistence(long deviceId)
        {
            return persistenceFactory.CheckDeviceExistence(dbConnection.ConnectionString, deviceId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="severityLevel"></param>
        /// <param name="descriptionStatus"></param>
        /// <param name="offline"></param>
        public void UpdateDeviceStatus(long deviceId, int severityLevel, string descriptionStatus, byte offline)
        {
            persistenceFactory.UpdateDeviceStatus(dbConnection.ConnectionString, deviceId, severityLevel, descriptionStatus, offline);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool DeleteExpiredAck()
        {
            return persistenceFactory.DeleteExpiredAcknowledgements(dbConnection.ConnectionString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<DbDeviceAcknowledgement> GetAckList()
        {
            return persistenceFactory.GetDeviceAcknowledgementsList(dbConnection.ConnectionString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="streamId"></param>
        /// <param name="name"></param>
        /// <param name="severityLevel"></param>
        /// <param name="description"></param>
        public void UpdateStream(long deviceId, int streamId, string name, int severityLevel, string description, DateTime dtValue)
        {
            persistenceFactory.UpdateStream(dbConnection.ConnectionString, deviceId, streamId, name, severityLevel, description, dtValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="streamId"></param>
        /// <param name="fieldId"></param>
        /// <param name="arrayId"></param>
        /// <param name="name"></param>
        /// <param name="severityLevel"></param>
        /// <param name="value"></param>
        /// <param name="description"></param>
        public void UpdateStreamField(long deviceId, int streamId, int fieldId, int arrayId, string name, int severityLevel, string value, string description)
        {
            persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceId, streamId, fieldId, arrayId, name, severityLevel, value, description);
        }

        #endregion

        #region Metodi privati

        private void InitializeDb()
        {
            string configurationSystemFile = AppCfg.AppCfg.Default.sXMLSystem;

            ConnectionStringSettings    localDatabaseConnectionString           = null;
            ConnectionStringSettings    localDatabaseEventsConnectionString     = null;
            lastException                           = null;

            if (string.IsNullOrEmpty(configurationSystemFile))
            {
                lastException = new ConfigurationSystemException("Nome del file di configurazione con la lista dei dispositivi da monitorare non definito");
                FileUtility.AppendStringToFileWithLoggingLevel  (LoggingLevel.Critical, lastException.Message);
                return;
            }

            if (!FileUtility.CheckFileCanRead(configurationSystemFile))
            {
                lastException = new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
                                                                 "Impossibile leggere il file di configurazione con la lista dei dispositivi: {0}",
                                                                 configurationSystemFile));

                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, lastException.Message);
                return;
            }

            try
            {
                localDatabaseConnectionString       = ConfigurationManager.ConnectionStrings["LocalDatabase"];
                localDatabaseEventsConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabaseEvents"];
            }
            catch (ConfigurationErrorsException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, "Configurazione non corretta. " + ex.Message);
                lastException = ex;
            }

            if (localDatabaseConnectionString == null)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida.");
            }
            else
            {
                try
                {
                    dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                                   "La stringa di connessione al database non è valida. " + ex.Message);
                    lastException = ex;
                }
            }

            if (localDatabaseEventsConnectionString == null)
            {
                if (dbConnection != null)
                {
                    dbEventsConnection = dbConnection;
                }
            }
            else
            {
                try
                {
                    dbEventsConnection = new SqlConnection(localDatabaseEventsConnectionString.ConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                                   "La stringa di connessione al database degli eventi non è valida. " + ex.Message);
                    lastException = ex;
                }
            }
        }

        #endregion
    }
}

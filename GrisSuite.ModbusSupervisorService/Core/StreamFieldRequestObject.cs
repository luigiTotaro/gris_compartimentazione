﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class StreamFieldRequestObject
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="FieldId"></param>
        /// <param name="byFunCode"></param>
        /// <param name="wOffset"></param>
        /// <param name="wCount"></param>
        public StreamFieldRequestObject(int FieldId, byte byFunCode, ushort wOffset, ushort wCount)
        {
            this.FieldId    = FieldId;
            this.byFunCode  = byFunCode;
            this.wOffset    = wOffset;
            this.wCount     = wCount;
        }

        /// <summary>
        /// Indica il tipo di richiesta da inviare allo slave
        /// </summary>
        public byte byFunCode { get; private set; }

        /// <summary>
        ///   Id del Field
        /// </summary>
        public int FieldId    { get; private set; }

        /// <summary>
        /// Offset da cui considerare il valore
        /// </summary>
        public ushort wOffset { get; private set; }

        /// <summary>
        /// Numero di coil/registri che compongono il valore
        /// </summary>
        public ushort wCount { get; private set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using GrisSuite.MODBUSSupervisorService.Helper;
using GrisSuite.MODBUSSupervisorService.Database;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class ProfileList
    {
        private static ProfileList  _instance;
        private List<ProfileObject> _profiles;

        /// <summary>
        /// Instanza corrente della classe
        /// </summary>
        public static ProfileList Instance
        {
            get {

                if (_instance == null)
                {
                    _instance = new ProfileList();
                }

                return _instance;
            }
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        public ProfileList()
        {
            this._profiles = new List<ProfileObject>();
        }

        /// <summary>
        /// Lista di tutti i profili caricati
        /// </summary>
        public List<ProfileObject> Profiles
        {
            get
            {
                if (Instance._profiles.Count == 0)
                {
                    ReadSchedulersFromFile();
                }

                return this._profiles;
            }
        }

        /// <summary>
        /// Lettura, se necessaria, di tutti i profili di schedulazione salvati su file
        /// </summary>
        private void ReadSchedulersFromFile()
        {
            this._profiles = SchedulerXmlHelper.GetProfileList(AppCfg.AppCfg.Default.sXMLSystem);
        }
    }
}

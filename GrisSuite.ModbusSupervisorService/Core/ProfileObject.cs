﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Xml;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class ProfileObject : IDisposable
    {
        #region Costruttore

        public ProfileObject()
        {
            this.Activities = new List<ActivityObject>();
        }

        #endregion

        void            IDisposable.Dispose()   { }
        public  void    Dispose()               { }

        #region Proprietà pubbliche

        /// <summary>
        /// Id del profilo di schedulazione
        /// </summary>
        public long ProfileId { get; set; }

        /// <summary>
        /// Nome del profilo di schedulazione
        /// </summary>
        public string sName { get; set; }

        /// <summary>
        /// Lista dei profili di schedulazione associati al profilo
        /// </summary>
        public List<ActivityObject> Activities { get; set; }
        
        #endregion

        #region Metodi pubblici

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Profile Id: {0}, Nome : {1}", this.ProfileId, this.sName);
        }

        /// <summary>
        /// Converte la lista di attributi nei valori delle proprietà della classe
        /// </summary>
        /// <param name="attributes">Lista degli attributi presenti nel tag xml</param>
        public void Decode(XmlAttributeCollection attributes)
        {
            foreach (XmlAttribute att in attributes)
            {
                switch (att.Name.ToLower(CultureInfo.InvariantCulture))
                {
                    case "id":
                        long lTmp;
                        if (long.TryParse(att.Value, out lTmp))
                        {
                            this.ProfileId = lTmp;
                        }
                        break;
                    case "name":
                        this.sName = att.Value;
                        break;
                }
            }
        }

        #endregion

        #region Predicati utilizzati per la ricerca

        /// <summary>
        ///  Esprime il filtro della ricerca per Id
        /// </summary>
        /// <param name="Id">Identificativo sul quale fare la ricerca</param>
        /// <returns>true se l'identificativo corrisponde</returns>
        public static Predicate<ProfileObject> ById(long Id)
        {
            return delegate(ProfileObject profile)
            {
                return profile.ProfileId == Id;
            };
        }

        #endregion

    }
}

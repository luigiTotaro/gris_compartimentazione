﻿using System;
using System.Collections.Generic;
////using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Globalization;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public enum ActivityMode
    {
        StartUp,
        Interval,
        Continue
    }

    public class DoW
    {
        /// <summary>
        /// Maschere di bit utilizzate per interpretare l'attributo 'dow' dell'attività
        /// di schedulazione: I VALORI DI QUESTE MASCHERE SONO DA VERIFICARE !!!!
        /// </summary>
        private const int MASK_DOW_SUNDAY       = 0x01;
        private const int MASK_DOW_MONDAY       = 0x02;
        private const int MASK_DOW_TUESDAY      = 0x04;
        private const int MASK_DOW_WEDNESDAY    = 0x08;
        private const int MASK_DOW_THURSDAY     = 0x10;
        private const int MASK_DOW_FRIDAY       = 0x20;
        private const int MASK_DOW_SATURDAY     = 0x40;
        private const int MASK_DOW_HOLIDAY      = 0x80;

        #region Costruttore

        //public DoW()
        //{
        //    this.bSunday    = true;
        //    this.bMonday    = true;
        //    this.bTuesday   = true;
        //    this.bWednesday = true;
        //    this.bThursday  = true;
        //    this.bFriday    = true;
        //    this.bSaturday  = true;
        //    this.bHolyday   = true;
        //}
        
        public DoW(byte byDow)
        {
            this.bSunday    = ((byDow & MASK_DOW_SUNDAY)    != 0) ? true : false;
            this.bMonday    = ((byDow & MASK_DOW_MONDAY)    != 0) ? true : false;
            this.bTuesday   = ((byDow & MASK_DOW_TUESDAY)   != 0) ? true : false;
            this.bWednesday = ((byDow & MASK_DOW_WEDNESDAY) != 0) ? true : false;
            this.bThursday  = ((byDow & MASK_DOW_THURSDAY)  != 0) ? true : false;
            this.bFriday    = ((byDow & MASK_DOW_FRIDAY)    != 0) ? true : false;
            this.bSaturday  = ((byDow & MASK_DOW_SATURDAY)  != 0) ? true : false;
            this.bHolyday   = ((byDow & MASK_DOW_HOLIDAY)   != 0) ? true : false;
        }
        
        #endregion

        #region Proprietà pubbliche

        public bool bSunday     { get; private set; }

        public bool bMonday     { get; private set; }

        public bool bTuesday    { get; private set; }

        public bool bWednesday  { get; private set; }

        public bool bThursday   { get; private set; }

        public bool bFriday     { get; private set; }

        public bool bSaturday   { get; private set; }

        public bool bHolyday    { get; private set; }

        #endregion
    }

    public class ActivityObject : IDisposable
    {

        #region Costruttore

        public ActivityObject()
        {
            this.Default();
        }

        #endregion

        void        IDisposable.Dispose () { }
        public void Dispose             () { }

        #region Proprietà pubbliche

        /// <summary>
        /// Identificativo dell'attività di schedulazione
        /// </summary>
        public long     ActivityId      { get; set; }

        /// <summary>
        /// Nome dell'attività di schedulazione
        /// </summary>
        public string   sName           { get; set; }

        /// <summary>
        /// Classe dell'attività di schedulazione
        /// </summary>
        public string   sClass          { get; set; }

        /// <summary>
        /// Ora di inizio della schedulazione
        /// </summary>
        public TimeSpan tsFrom          { get; set; }

        /// <summary>
        /// Ora di fine della schedulazione
        /// </summary>
        public TimeSpan tsTo            { get; set; }

        /// <summary>
        /// Maschera di bit con i giorni della settimana
        /// </summary>
        public DoW DoW                  { get; set; }

        /// <summary>
        /// Tipologia di schedulazione (all'avvio del sw, dopo ogni intervallo di tempo specificato, in maniera continua)
        /// </summary>
        public ActivityMode Mode        { get; set; }

        /// <summary>
        /// Intervallo di tempo tra due polling successivi (nel caso in cui la tipologia di schedulazione sia 'interval')
        /// </summary>
        public TimeSpan tsInterval      { get; set; }

        #endregion

        #region Metodi pubblici

        public void Decode(XmlAttributeCollection attributes)
        {
            long        lTmp;
            byte        byDoW;
            TimeSpan    tsTmp;

            foreach (XmlAttribute att in attributes)
            {
                switch (att.Name.ToLower(CultureInfo.InvariantCulture))
                {
                    case "id":
                        if (long.TryParse(att.Value, out lTmp))
                        {
                            this.ActivityId = lTmp;
                        }
                        break;
                    case "name":
                        this.sName = att.Value;
                        break;
                    case "class":
                        this.sClass = att.Value;
                        break;
                    case "from":
                        if (TimeSpan.TryParse(att.Value, out tsTmp))
                        {
                            this.tsFrom = tsTmp;
                        }
                        break;
                    case "to":
                        if (TimeSpan.TryParse(att.Value, out tsTmp))
                        {
                            this.tsTo = tsTmp;
                        }
                        break;
                    case "dow":
                        if (byte.TryParse(att.Value, out byDoW))
                        {
                            this.DoW = new DoW(byDoW);
                        }
                        break;
                    case "mode":
                        this.Mode = this.GetMode(att.Value);
                        break;
                    case "interval":
                        if (TimeSpan.TryParse(att.Value, out tsTmp))
                        {
                            this.tsInterval = tsTmp;
                        }
                        break;
                }
            }
        }

        #endregion

        #region Predicati utilizzati per la ricerca

        /// <summary>
        /// Esprime il filtro della ricerca per Id
        /// </summary>
        /// <param name="Id">Identificativo sul quale fare la ricerca</param>
        /// <returns>true se l'identificativo corrisponde</returns>
        public static Predicate<ActivityObject> ById(long Id)
        {
            return delegate(ActivityObject activity)
            {
                return activity.ActivityId == Id;
            };
        }

        /// <summary>
        /// Esprime il filtro della ricerca per classe
        /// </summary>
        /// <param name="sClass">Classe sulla quale fare la ricerca</param>
        /// <returns>true se l'identificativo corrisponde</returns>
        public static Predicate<ActivityObject> ByClass(string sClass)
        {
            return delegate(ActivityObject activity)
            {
                return activity.sClass == sClass;
            };
        }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Specifica i valori di default
        /// </summary>
        private void Default()
        {
            this.ActivityId     = 0;
            this.Mode           = ActivityMode.Continue;
            this.DoW            = new DoW(0xff);
            this.sName          = "";
            this.sClass         = "";
            this.tsFrom         = new TimeSpan(0, 0, 0);
            this.tsTo           = new TimeSpan(23, 59, 59);
            this.tsInterval     = new TimeSpan(0, 0, 0);
        }

        /// <summary>
        /// Decodifica la tipologia di polling da applicare all'attività di schedulazione
        /// </summary>
        /// <param name="sMode">parametro specificato nel file system.xml</param>
        /// <returns>Valore dell'enumeratico corrispondente</returns>
        private ActivityMode GetMode(string sMode)
        {
            ActivityMode mode = ActivityMode.Continue;

            switch (sMode.ToLower())
            {
                case "interval":
                    mode = ActivityMode.Interval;
                    break;
                case "startup":
                    mode = ActivityMode.StartUp;
                    break;
                case "continue":
                    mode = ActivityMode.Continue;
                    break;
            }

            return mode;
        }

        #endregion
    }
    
}

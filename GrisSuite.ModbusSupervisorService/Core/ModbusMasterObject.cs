﻿using System;
using System.Collections.Generic;
using System.Text;
using GrisSuite.MODBUSSupervisorService.Utility;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Globalization;
using GrisSuite.MODBUSSupervisorService.Ping;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class objSckState
    {
        public Socket       workSocket  = null;
        public const int    BufferSize  = MsgObject.MAX_LEN_BUF;
        public byte[]       buffer      = new byte[BufferSize];
    }

    public class ModbusMasterObject : IDisposable
    {
        #region Costanti pubbliche

        public const byte       FC_READ_COIL                        = 0x01;
        public const byte       FC_READ_INPUT_DISCRETE              = 0x02;
        public const byte       FC_READ_HOLDING_REGISTER            = 0x03;
        public const byte       FC_READ_INPUT_REGISTER              = 0x04;

        public const byte       FC_WRITE_COIL                       = 0x05; // Msg implementato ma mai usato
        public const byte       FC_WRITE_SINGLE_REGISTER            = 0x06; // Msg implementato ma mai usato
        public const byte       FC_GET_COMM_EVENT_COUNTER           = 0x0b; // Msg non implementato
        public const byte       FC_FORCE_MULTIPLE_COILS             = 0x0f; // Msg non implementato
        public const byte       FC_WRITE_MULTIPLE_REGISTERS         = 0x10; // Msg non implementato
        public const byte       FC_WRITE_MASK_REGISTER              = 0x16; // Msg non implementato
        public const byte       FC_READ_WRITE_MULTIPLE_REGISTERS    = 0x17; // Msg non implementato

        #endregion

        #region Campi privati

        private object                          _queueSync      = new object        ();
        private readonly Queue<Action>          _actions        = new Queue<Action> ();

        public delegate void Action();

        private System.Timers.Timer             _aTimerQueue    = null;

        #endregion 

        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="device"></param>
        /// <param name="sIp"></param>
        /// <param name="nPort"></param>
        public ModbusMasterObject(DeviceObject device, string sIp, int nPort) 
        {
            DeviceId            = device.DeviceId;

            bEnabled            = false;

            modbusSlave         = new ModbusSlaveObject(device);

            request             = new MsgObject(modbusSlave.wTransactionID, modbusSlave.wProtocolID, modbusSlave.byUnitID);
            response            = new MsgObject(modbusSlave.wTransactionID, modbusSlave.wProtocolID, modbusSlave.byUnitID);

            hEventConnect       = new AutoResetEvent(false);
            hEventReceive       = new AutoResetEvent(false);
            hEventSend          = new AutoResetEvent(false);

            InitializeEndPoint  (sIp, nPort);
            InitializeActionQue (); 
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="device"></param>
        /// <param name="sIp"></param>
        public ModbusMasterObject(DeviceObject device, string sIp) 
        {
            DeviceId        = device.DeviceId;

            bEnabled        = false;

            modbusSlave     = new ModbusSlaveObject(device);

            request         = new MsgObject(modbusSlave.wTransactionID, modbusSlave.wProtocolID, modbusSlave.byUnitID);
            response        = new MsgObject(modbusSlave.wTransactionID, modbusSlave.wProtocolID, modbusSlave.byUnitID);

            hEventConnect   = new AutoResetEvent(false);
            hEventReceive   = new AutoResetEvent(false);
            hEventSend      = new AutoResetEvent(false);

            InitializeEndPoint  (sIp, ConfigurationObject.Instance.ModbusPort);
            InitializeActionQue ();
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Id del device cui è associato il modbus server
        /// </summary>
        public long                 DeviceId        { get; private set; }

        /// <summary>
        /// Endpoint utilizzato per la connessione modbus
        /// </summary>
        public IPEndPoint           IpEndPoint      { get; private set; }

        /// <summary>
        /// Socket associato alla connessione modbus
        /// </summary>
        public Socket               IdSock          { get; private set; }

        /// <summary>
        /// E' false se il valore passato al costruttore non è un indirizzo
        /// ip valido ...
        /// </summary>
        public bool                 bEnabled        { get; private set; }

        /// <summary>
        /// Rappresenta lo slave modbus con cui questo master sta comunicando
        /// </summary>
        public ModbusSlaveObject    modbusSlave     { get; private set; }

        #endregion

        #region Proprietà private

        /// <summary>
        /// Buffer ricevuto dallo slave modbus
        /// </summary>
        private MsgObject       response        { get; set; }

        /// <summary>
        /// Buffer inviato allo slave modbus
        /// </summary>
        private MsgObject       request         { get; set; }

        /// <summary>
        /// Evento di avvenuto invio di un buffer allo slave
        /// </summary>
        private AutoResetEvent  hEventSend      { get; set; }

        /// <summary>
        /// Evento di avvenuta ricezione di un buffer dallo slave
        /// </summary>
        private AutoResetEvent  hEventReceive   { get; set; }

        /// <summary>
        /// Evento di avvenuta connessione allo slave modbus
        /// </summary>
        private AutoResetEvent  hEventConnect   { get; set; }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Enqueues action to process.
        /// </summary>
        /// <param name="action">Action to enqueue.</param>
        public void Enqueue(Action action)
        {
            lock (_queueSync)
            {
                _actions.Enqueue(action);
            }
        }

        /// <summary>
        /// Clears queue.
        /// </summary>
        public void Clear()
        {
            lock (_queueSync)
            {
                _actions.Clear();
            }
        }

        /// <summary>
        /// Invio allo slave modbus la richiesta di lettura di un determinato numero di coil
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento a partire dal quale leggere i coil</param>
        /// <param name="wBitCount">numero di coil da leggere</param>
        public void TxReadCoil(ushort wRefNumber, ushort wBitCount)
        {
            request.Flush   ();
            response.Flush  ();

            request.byFuncCode = FC_READ_COIL;

            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber >> 8)  & 0xff));   // MSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber)       & 0xff));   // LSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wBitCount >> 8)   & 0xff));   // MSB BIT COUNT 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wBitCount)        & 0xff));   // LSB BIT COUNT

            if (!this.TryToWrite(request.ToBuffer()))
                return;

            if ((modbusSlave.Coils.exceptionCode = this.TryToReceive(FC_READ_COIL)) != MsgObject.ExceptionCode.None)
                return;
            
            List<byte> byBitValues = new List<byte>();

            for (int j = 0; j < response.bufferObj.byData[0]; j++)
            {
                byBitValues.Add(response.bufferObj.byData[j + 1]);
            }

            modbusSlave.dtLastMsgReceived = DateTime.Now;

            modbusSlave.Coils.Update(wRefNumber, wBitCount, byBitValues);
            modbusSlave.LastError = null;
        }

        /// <summary>
        /// Invio della scrittura di un singolo output coil allo slave
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento dell'output da scrivere</param>
        /// <param name="bTurn_ON">true : attiva l'output; false : disattiva l'output</param>
        /// <returns>true : scrittura output avvenuta con successo</returns>
        public bool TxWriteCoil(ushort wRefNumber, bool bTurn_ON)
        {
            request.Flush   ();
            response.Flush  ();

            request.byFuncCode = FC_WRITE_COIL;

            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber >> 8)  & 0xff));           // MSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber)       & 0xff));           // LSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)( bTurn_ON          ? 0xff : 0x00));    // ON/OFF
            request.bufferObj.byData[request.bufferObj.nLenData++] = 0x00;

            if (!this.TryToWrite(request.ToBuffer()))
                return false;

            if ((modbusSlave.Coils.exceptionCode = this.TryToReceive(FC_WRITE_COIL)) != MsgObject.ExceptionCode.None)
                return false;

            return true;
        }

        /// <summary>
        /// Invio allo slave modbus la richiesta di lettura di un determinato numero di discrete input
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento a partire dal quale leggere gli input discrete</param>
        /// <param name="wBitCount">numero di input discrete da leggere</param>
        public void TxReadInputDiscrete(ushort wRefNumber, ushort wBitCount)
        {
            request.Flush   ();
            response.Flush  ();

            request.byFuncCode = FC_READ_INPUT_DISCRETE;

            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber >> 8)  & 0xff));   // MSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber)       & 0xff));   // LSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wBitCount >> 8)   & 0xff));   // MSB BIT COUNT 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wBitCount)        & 0xff));   // LSB BIT COUNT

            if (!this.TryToWrite(request.ToBuffer()))
                return;

            if ((modbusSlave.DiscreteInputs.exceptionCode = this.TryToReceive(FC_READ_INPUT_DISCRETE)) != MsgObject.ExceptionCode.None)
                return;

            List<byte> byBitValues = new List<byte>();

            for (int j = 0; j < response.bufferObj.byData[0]; j++)
            {
                byBitValues.Add(response.bufferObj.byData[j + 1]);
            }

            modbusSlave.DiscreteInputs.Update(wRefNumber, wBitCount, byBitValues);
            modbusSlave.LastError = null;
        }

        /// <summary>
        /// Invio allo slave modbus la richiesta di lettura del contenuto di alcuni holding register
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento a partire dal quale leggere gli holding register</param>
        /// <param name="wWordCount">numero di holding register da leggere</param>
        public void TxReadHoldingRegister(ushort wRefNumber, ushort wWordCount)
        {
            request.Flush();
            response.Flush();

            request.byFuncCode = FC_READ_HOLDING_REGISTER;

            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber >> 8)  & 0xff));   // MSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber)       & 0xff));   // LSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wWordCount >> 8)  & 0xff));   // MSB BIT COUNT 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wWordCount)       & 0xff));   // LSB BIT COUNT

            if (!this.TryToWrite(request.ToBuffer()))
                return;

            if ((modbusSlave.HoldingRegisters.exceptionCode = this.TryToReceive(FC_READ_HOLDING_REGISTER)) != MsgObject.ExceptionCode.None)
                return;

            List<ushort> wRegValues = new List<ushort>();

            for (int j = 1; j < response.bufferObj.byData[0]; j = j + 2)
            {
                wRegValues.Add((ushort)((response.bufferObj.byData[j] << 8) | response.bufferObj.byData[j + 1]));
            }

            modbusSlave.HoldingRegisters.Update(wRefNumber, wWordCount, wRegValues);
            modbusSlave.LastError = null;
        }

        /// <summary>
        /// Invio allo slave modbus la richiesta di lettura del contenuto di alcuni input register
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento a partire dal quale leggere gli input register</param>
        /// <param name="wWordCount">numero di input register da leggere</param>
        public void TxReadInputRegister(ushort wRefNumber, ushort wWordCount)
        {
            request.Flush   ();
            response.Flush  ();

            request.byFuncCode = FC_READ_INPUT_REGISTER;

            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber >> 8)  & 0xff));   // MSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber)       & 0xff));   // LSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wWordCount >> 8)  & 0xff));   // MSB BIT COUNT 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wWordCount)       & 0xff));   // LSB BIT COUNT

            if (!this.TryToWrite(request.ToBuffer()))
                return;

            if ((modbusSlave.InputRegisters.exceptionCode = this.TryToReceive(FC_READ_INPUT_REGISTER)) != MsgObject.ExceptionCode.None)
                return;

            List<ushort> wRegValues = new List<ushort>();

            for (int j = 1; j < response.bufferObj.byData[0]; j = j + 2)
            {
                wRegValues.Add((ushort)((response.bufferObj.byData[j] << 8) | response.bufferObj.byData[j + 1]));
            }

            modbusSlave.InputRegisters.Update(wRefNumber, wWordCount, wRegValues);
            modbusSlave.LastError = null;
        }

        /// <summary>
        /// Invio allo slave la richiesta di scrittura di un singolo holding register
        /// </summary>
        /// <param name="wRefNumber">indirizzo del registro da scrivere</param>
        /// <param name="wValue">valore da scrivere sul registro</param>
        /// <returns>true : la scrittura sul registro è andata a buon fine</returns>
        public bool TxWriteHoldingRegister(ushort wRefNumber, ushort wValue)
        {
            request.Flush   ();
            response.Flush  ();

            request.byFuncCode = FC_WRITE_SINGLE_REGISTER;

            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber >> 8)  & 0xff));   // MSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wRefNumber)       & 0xff));   // LSB REFERENCE NUMBER 
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wValue >> 8)      & 0xff));   // MSB VALUE REGISTER
            request.bufferObj.byData[request.bufferObj.nLenData++] = ((byte)((wValue)           & 0xff));   // LSB VALUE REGISTER

            if (!this.TryToWrite(request.ToBuffer()))
                return false;

            if ((modbusSlave.InputRegisters.exceptionCode = this.TryToReceive(FC_WRITE_SINGLE_REGISTER)) != MsgObject.ExceptionCode.None)
                return false;

            return true;
        }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Inizializza i valori per la comunicazione modbus
        /// </summary>
        /// <param name="sIp">indirizzo ip dello slave modbus</param>
        /// <param name="nPort">porta utilizzata nella comunicazione modbus</param>
        private void InitializeEndPoint(string sIp, int nPort)
        {
            string      sLog;
            IPAddress   ipAddress;

            try
            {
                if (IPAddress.TryParse(sIp, out ipAddress))
                {
                    IpEndPoint  = new IPEndPoint(ipAddress, nPort);
                    bEnabled    = true;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                sLog = string.Format(CultureInfo.InvariantCulture, "L'indirizzo {0} e la porta {1} specificati per la connessione modbus alla periferica non sono validi. Ogni ulteriore operazione relativa alla periferica annullata.", sIp, nPort);

                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, String.Format(CultureInfo.InvariantCulture, sLog));
            }
        }

        /// <summary>
        /// Inizializza la coda dei processi
        /// </summary>
        private void InitializeActionQue()
        {
            this._aTimerQueue = new System.Timers.Timer();
            this._aTimerQueue.Elapsed += new System.Timers.ElapsedEventHandler(OnProcessQue);
            this._aTimerQueue.Interval = 2000;
            this._aTimerQueue.AutoReset = false;
            this._aTimerQueue.Start();
        }

        /// <summary>
        /// Processamento della coda delle richieste da inviare dal modbus master corrente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnProcessQue(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                this._aTimerQueue.Stop();

                Process();

                this._aTimerQueue.Start();
            }
            catch (Exception)
            {
                this._aTimerQueue.Enabled = true;
            }
        }

        /// <summary>
        /// Processamento dei messaggi da inviare allo slave (se possibile)
        /// </summary>
        private void Process()
        {
            if (bEnabled)
            {
                bool bNeedUpdate = false;

                while (true)
                {
                    Action action = null;

                    lock (_queueSync)
                    {
                        if (_actions.Count == 0)
                        {
                            this.CheckDisconnect();
                            break;
                        }
                        else
                            action = _actions.Dequeue();
                    }

                    bNeedUpdate = true;

                    this.CheckReachable ();
                    this.CheckConnect   ();
                    action.Invoke       ();
                }

                if (bNeedUpdate) {
                    modbusSlave.Populate();     // Popolamento dei dati dello slave in base agli stream configurati
                    modbusSlave.UpdateDb();     // Aggiornamento dei dati dello slave sul database
                }
            }
        }

        /// <summary>
        /// Verifica che lo slave sia raggiungibile al ping
        /// </summary>
        private void CheckReachable()
        {
            if (modbusSlave.DeviceIPEndPoint == null)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "DeviceIPEndPoint non definito");
                modbusSlave.IsReachable = false;
                return;
            }

            if (modbusSlave.IsReachable != IcmpDataRetriever.GetPingResponse(modbusSlave.DeviceIPEndPoint))
            {
                modbusSlave.IsReachable = IcmpDataRetriever.GetPingResponse (modbusSlave.DeviceIPEndPoint);
                FileUtility.AppendStringToFileWithLoggingLevel              (modbusSlave.IsReachable ? LoggingLevel.Info : LoggingLevel.Warning, String.Format("modbusSlave.IsReachable {0}", modbusSlave.IsReachable));
            }
        }

        /// <summary>
        /// Verifica che lo slave sia connesso, se non lo è, effettua la connessione
        /// </summary>
        private void CheckConnect()
        {
            if (this.modbusSlave.IsReachable)
            {
                if (this.modbusSlave.IsConnectedModbus == false)
                {
                    this.Connect();
                }
            }
        }

        /// <summary>
        /// Verifica che lo slave sia disconnesso, se lo è, effettua la connessione
        /// </summary>
        private void CheckDisconnect()
        {
            if (this.modbusSlave.IsConnectedModbus == true)
            {
                this.Disconnect();
            }
        }

        /// <summary>
        /// Esegue la connessione socket allo slave modbus
        /// </summary>
        /// <returns>true : la connessione ha avuto successo</returns>
        private void Connect()
        {
            bool bIsEventSet;

            try
            {
                if (!this.bEnabled)
                {
                    this.modbusSlave.IsAliveModbus     = this.modbusSlave.CheckAliveModbus(false);
                    this.modbusSlave.IsConnectedModbus = false;
                    return;
                }

                Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                client.BeginConnect(IpEndPoint, new AsyncCallback(ConnectCallback), client);

                bIsEventSet = hEventConnect.WaitOne(Settings.Default.ToutSocketConnection, false);

                if (!bIsEventSet)
                {
                    //Rilascio il socket (in lettura e in scrittura)
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();

                    this.modbusSlave.IsAliveModbus      = this.modbusSlave.CheckAliveModbus(false); 
                    this.modbusSlave.IsConnectedModbus  = false;
                    return;
                }

                IdSock = client;
            }
            catch 
            {
                this.modbusSlave.IsAliveModbus      = this.modbusSlave.CheckAliveModbus(false);
                this.modbusSlave.IsConnectedModbus  = false;
                return;
            }

            this.modbusSlave.IsConnectedModbus = true;
        }

        /// <summary>
        /// Esegue la disconnessione dallo slave
        /// </summary>
        private void Disconnect()
        {
            try
            {
                //Rilascio il socket (in lettura e in ricezione)
                IdSock.Shutdown(SocketShutdown.Both);
                IdSock.Close();

                this.modbusSlave.IsConnectedModbus = false;
            }
            catch { }
        }

        /// <summary>
        /// Esegue l'invio in modalità asincrona di un buffer allo slave modbus
        /// </summary>
        /// <returns>true : l'invio ha avuto successo</returns>
        private bool TryToWrite(BufferObject bufferTx)
        {
            bool bIsEventSet;

            if (this.modbusSlave.IsConnectedModbus == false)
                return false;

            // Creo l'oggetto di stato
            objSckState state   = new objSckState();
            state.workSocket    = this.IdSock;

            try
            {
                IdSock.BeginSend                (bufferTx.byBuffer, 0, (int)bufferTx.nLenBuffer, 0, new AsyncCallback(SendCallback), state);
                bIsEventSet = hEventSend.WaitOne(Settings.Default.ToutSocketConnection, false);
            }
            catch {
                this.modbusSlave.IsAliveModbus      = this.modbusSlave.CheckAliveModbus(false);
                this.modbusSlave.IsConnectedModbus  = false;
                return false; 
            }

            if (!bIsEventSet)
            {
                this.modbusSlave.IsAliveModbus      = this.modbusSlave.CheckAliveModbus(false);
                this.modbusSlave.IsConnectedModbus  = false;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Esegue la ricezione in modalità asincrona di un buffer ricevuto dallo slave modbus
        /// </summary>
        /// <returns>true : è stato letto almeno un byte</returns>
        private bool TryToRead()
        {
            bool bIsEventSet;

            try
            {
                if (this.modbusSlave.IsConnectedModbus == false)
                    return false;

                // Creo l'oggetto di stato
                objSckState state   = new objSckState();
                state.workSocket    = IdSock;

                // Inizio la ricezione sul socket
                IdSock.BeginReceive                 (state.buffer, 0, objSckState.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                bIsEventSet = hEventReceive.WaitOne (Settings.Default.ToutSocketConnection, false);

                if (!bIsEventSet)
                {
                    this.modbusSlave.IsAliveModbus      = this.modbusSlave.CheckAliveModbus(false);
                    this.modbusSlave.IsConnectedModbus  = false;
                    return false;
                }
            }
            catch 
            {
                this.modbusSlave.IsAliveModbus      = this.modbusSlave.CheckAliveModbus(false);
                this.modbusSlave.IsConnectedModbus  = false;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Analizza il buffer ricevuto dallo slave modbus
        /// </summary>
        /// <param name="byIdMsg">function code della richiesta che era stata inviata allo slave modbus</param>
        /// <returns>true : il buffer ricevuto è correttamente formattato</returns>
        private MsgObject.ExceptionCode TryToReceive(byte byIdMsg)
        {
            MsgObject.ExceptionCode excCode = MsgObject.ExceptionCode.Tout;

            if (this.modbusSlave.IsConnectedModbus == false)
                return excCode;

            for (int j = 0; j < Settings.Default.MaxSocketRetry; j++)
            {
                if (!TryToRead())
                    continue;

                response.DecodeBuffer();

                if (response.wTransactId        != modbusSlave.wTransactionID) // MsgObject.TRANSACTION_ID_VALUE)
                    continue;

                if (response.wProtocolId        != modbusSlave.wProtocolID) // MsgObject.PROTOCOL_ID_VALUE)
                    continue;

                if (response.byUnitId           != modbusSlave.byUnitID) // MsgObject.UNIT_ID_VALUE)
                    continue;

                if (response.byFuncCode         != byIdMsg)
                    continue;

                if ((response.wLen + 6)         != response.bufferObj.nLenBuffer)
                    continue;

                excCode                         = response.byExceptionCode;
                this.modbusSlave.IsAliveModbus  = this.modbusSlave.CheckAliveModbus(true); // La richiesta allo slave è andata a buon fine (annullo ogni eccezione precedentemente memorizzata)
                break;
            }

            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose, string.Format("Rx <- {0}: {1} [Exception code: {2}]", this.modbusSlave.DeviceIPEndPoint, response.bufferObj.ToString(), excCode));
            return excCode;
        }

        #endregion

        #region Callback private

        /// <summary>
        /// Callback avvenuto invio di un buffer al modbus slave
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                objSckState state       = (objSckState)ar.AsyncState;
                Socket      client      = state.workSocket;
                int         bytesSent   = client.EndSend(ar);

                FileUtility.AppendStringToFileWithLoggingLevel  (LoggingLevel.Verbose, string.Format("Tx -> {0}: {1}", this.modbusSlave.DeviceIPEndPoint, request.bufferObj.ToString()));
                hEventSend.Set                                  (); // Segnalo che tutti i byte sono stati inviati sul Sck
            }
            catch (Exception ex)
            {
                string sLog                 = String.Format                 ("Invio buffer a {0} Ko [Error: {1}]", this.modbusSlave.DeviceIPEndPoint, ex.Message);
                this.modbusSlave.LastError  = new ModbusProtocolException   (sLog, ex);
                FileUtility.AppendStringToFileWithLoggingLevel              (LoggingLevel.Verbose, sLog);
            }
        }

        /// <summary>
        /// Callback avvenuta lettura di un buffer dal modbus slave
        /// </summary>
        /// <param name="ar"></param>
        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                objSckState state       = (objSckState)ar.AsyncState;
                Socket      client      = state.workSocket;
                uint        bytesRead   = (uint)client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    int nAlreadyRead = response.bufferObj.nLenBuffer;                                   // Numero di byte già letti precedentemente

                    Array.Copy(state.buffer, 0, response.bufferObj.byBuffer, nAlreadyRead, bytesRead);  // Accodo i byte appena letti al buffer in lettura
                    response.bufferObj.nLenBuffer += (int)bytesRead;

                    hEventReceive.Set();                                                                // Set dell'avvenuta ricezione
                }
            }
            catch (Exception ex) 
            {
                string sLog                 = String.Format                 ("Ricezione buffer da {0} Ko [Error: {1}]", this.modbusSlave.DeviceIPEndPoint, ex.Message);
                this.modbusSlave.LastError  = new ModbusProtocolException   (sLog, ex);
                FileUtility.AppendStringToFileWithLoggingLevel              (LoggingLevel.Verbose, sLog);
            }
        }

        /// <summary>
        /// Callback avvenuta connessione al modbus slave
        /// </summary>
        /// <param name="ar"></param>
        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndConnect(ar);

                hEventConnect.Set(); // Segnalo l'avvenuta connessione
            }
            catch (Exception ex) 
            {
                string sLog                 = String.Format                 ("Connessione a {0} Ko [Error: {1}]", this.modbusSlave.DeviceIPEndPoint, ex.Message);
                this.modbusSlave.LastError  = new ModbusProtocolException   (sLog, ex);
                FileUtility.AppendStringToFileWithLoggingLevel              (LoggingLevel.Verbose, sLog);
            }
        }

        #endregion

        #region Metodi dispose

        void IDisposable.Dispose()
        {
            Clear();
        }

        public void Dispose()
        {
            Clear();
        }

        #endregion

        #region Predicati utilizzati per la ricerca

        /// <summary>
        ///  Esprime il filtro della ricerca per Id
        /// </summary>
        /// <param name="Id">Identificativo della peru sul quale fare la ricerca</param>
        /// <returns>true se l'identificativo corrisponde</returns>
        public static Predicate<ModbusMasterObject> ByDeviceId(long DeviceId)
        {
            return delegate(ModbusMasterObject modbusMaster)
            {
                return modbusMaster.DeviceId == DeviceId;
            };
        }

        #endregion
    }
}

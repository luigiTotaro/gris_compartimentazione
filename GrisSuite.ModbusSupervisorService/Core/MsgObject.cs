﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class MsgObject
    {

        #region Costanti pubbliche

        public const int        MAX_LEN_BUF         = 255;

        #endregion

        #region Costanti private

        private const byte      INVALID_VALUE       = 0xff;

        private const int       TRANSACTION_ID_MSB  = 0;
        private const int       TRANSACTION_ID_LSB  = 1;

        private const int       PROTOCOL_ID_MSB     = 2;
        private const int       PROTOCOL_ID_LSB     = 3;

        private const int       LENGHT_FIELD_MSB    = 4;
        private const int       LENGHT_FIELD_LSB    = 5;

        private const int       UNIT_ID             = 6;

        private const int       FUNCTION_CODE       = 7;

        private const int       EXCEPTION_CODE      = 8;

        private const int       START_DATA          = 8;    

        #endregion

        #region Enumeratico degli errori del protocollo

        public enum ExceptionCode : byte
        {
            None                                    = 0x00,
            Illegal_Function                        = 0x01,
            Illegal_Data_Address                    = 0x02,
            Illegal_Data_Value                      = 0x03,
            Slave_Device_Failure                    = 0x04,
            Acknowledge                             = 0x05,
            Server_Busy                             = 0x06,
            Memory_Parity_Error                     = 0x08,
            Gateway_Path_Unavailable                = 0x0a,
            Gateway_Target_Device_Failed_To_Respond = 0x0b,

            Tout                                    = 0xff
        }

        #endregion

        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        public MsgObject(ushort wTrasactionID, ushort wProtocolID, byte byUnitID)
        {
            this.wTransactId        = wTrasactionID;
            this.wProtocolId        = wProtocolID;
            this.byUnitId           = byUnitID;
            this.byFuncCode         = 0x00;
            this.bufferObj          = new BufferObject(255);
            this.byExceptionCode    = ExceptionCode.None;
        }

        #endregion

        #region Campi privati

        private ushort          _wTransactId;

        private ushort          _wProtocolId;

        private ushort          _wLen;

        private byte            _byUnitId;

        private byte            _byFuncCode;

        private ExceptionCode   _byExceptionCode;

        #endregion

        #region Proprietà pubbliche

        public ushort wTransactId     
        {
            get { return _wTransactId; }

            set
            {
                if (_wTransactId != value)
                {
                    _wTransactId = value;
                }
            }
        }

        public ushort wProtocolId     
        {
            get { return _wProtocolId; }

            set
            {
                if (_wProtocolId != value)
                {
                    _wProtocolId = value;
                }
            }
        }

        public ushort wLen            
        {
            get { return _wLen; }

            set
            {
                if (_wLen != value)
                {
                    _wLen = value;
                }
            }
        }

        public byte byUnitId        
        {
            get { return _byUnitId; }

            set
            {
                if (_byUnitId != value)
                {
                    _byUnitId = value;
                }
            }
        }

        public byte byFuncCode      
        {
            get { return _byFuncCode; }

            set
            {
                if (_byFuncCode != value)
                {
                    _byFuncCode = value;
                }
            }
        }

        public ExceptionCode byExceptionCode
        {
            get { return _byExceptionCode; }
            set
            {
                if (_byExceptionCode != value)
                {
                    _byExceptionCode = value;
                }
            }
        }

        public BufferObject bufferObj       { get; set; }  

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Annulla il buffer in memoria
        /// </summary>
        public void Flush()
        {
            wLen            = 0x0000;
            byFuncCode      = 0x00;
            byExceptionCode = ExceptionCode.None;

            bufferObj.Flush();
        }

        /// <summary>
        /// Costruisce il buffer da inviare allo slave
        /// </summary>
        /// <returns>buffer da inviare</returns>
        public BufferObject ToBuffer()
        {
            wLen = (ushort)(2 + bufferObj.nLenData);

            bufferObj.byBuffer[bufferObj.nLenBuffer++] = (byte)((wTransactId >> 8)  & 0xff);    // MSB TRANSACTION ID 
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = (byte)((wTransactId)       & 0xff);    // LSB TRANSACTION ID 
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = (byte)((wProtocolId >> 8)  & 0xff);    // MSB PROTOCOL ID 
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = (byte)((wProtocolId)       & 0xff);    // LSB PROTOCOL ID 
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = (byte)((wLen >> 8)         & 0xff);    // MSB LEN 
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = (byte)((wLen)              & 0xff);    // LSB LEN
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = byUnitId;                              // UNIT ID
            bufferObj.byBuffer[bufferObj.nLenBuffer++] = byFuncCode;                            // FUNCTION CODE

            for (int j = 0; j < bufferObj.nLenData; j++)
            {
                bufferObj.byBuffer[bufferObj.nLenBuffer++] = bufferObj.byData[j];
            }

            return bufferObj;
        }

        /// <summary>
        /// Estrae tutti gli elementi dal buffer ricevuto dallo slave
        /// </summary>
        public void DecodeBuffer()
        {
            DecodeTransactionId ();
            DecodeProtocolId    ();
            DecodeUnitId        ();
            DecodeFunctionCode  ();
            DecodeLen           ();
            DecodeExceptionCode ();
            DecodeData          ();
        }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Estrae il transaction id dal buffer ricevuto
        /// </summary>
        private void DecodeTransactionId()
        {
            ushort wTmp = INVALID_VALUE;

            if (bufferObj.nLenBuffer >= 2)
            {
                wTmp  = (ushort)((ushort)bufferObj.byBuffer [TRANSACTION_ID_MSB] >> 8);
                wTmp |= (ushort)bufferObj.byBuffer          [TRANSACTION_ID_LSB];
            }

            _wTransactId = wTmp;
        }

        /// <summary>
        /// Estrae il protocol id dal buffer ricevuto
        /// </summary>
        private void DecodeProtocolId()
        {
            ushort wTmp = INVALID_VALUE;

            if (bufferObj.nLenBuffer >= 4)
            {
                wTmp  = (ushort)((ushort)bufferObj.byBuffer [PROTOCOL_ID_MSB] >> 8);
                wTmp |= (ushort)bufferObj.byBuffer          [PROTOCOL_ID_LSB];
            }

            _wProtocolId = wTmp;
        }

        /// <summary>
        /// Estrae la lunghezza del 'payload' dal buffer ricevuto
        /// </summary>
        private void DecodeLen()
        {
            ushort wTmp = INVALID_VALUE;

            if (bufferObj.nLenBuffer >= 6)
            {
                wTmp = (ushort)((ushort)bufferObj.byBuffer  [LENGHT_FIELD_MSB] >> 8);
                wTmp |= (ushort)bufferObj.byBuffer          [LENGHT_FIELD_LSB];
            }

            _wLen = wTmp;
        }

        /// <summary>
        /// Estrae lo unit id dal buffer ricevuto
        /// </summary>
        private void DecodeUnitId()
        {
            byte byTmp = INVALID_VALUE;

            if (bufferObj.nLenBuffer >= 7)
            {
                byTmp = bufferObj.byBuffer[UNIT_ID];
            }

            _byUnitId = byTmp;
        }

        /// <summary>
        /// Estrae il function code dal buffer ricevuto
        /// </summary>
        private void DecodeFunctionCode()
        {
            byte byTmp = INVALID_VALUE;

            if (bufferObj.nLenBuffer >= 8)
            {
                byTmp = bufferObj.byBuffer[FUNCTION_CODE];

                if (Convert.ToBoolean(byTmp & 0x80))
                {
                    _byFuncCode = Convert.ToByte(byTmp & 0x7f);
                    return;
                }
            }

            _byFuncCode = byTmp;
        }

        /// <summary>
        /// Estrae l'eventuale exception code dal buffer ricevuto
        /// </summary>
        private void DecodeExceptionCode()
        {
            ExceptionCode code = ExceptionCode.None;

            if (bufferObj.nLenBuffer >= 9)
            {
                if (Convert.ToBoolean(bufferObj.byBuffer[FUNCTION_CODE] & 0x80))
                {
                    code = (ExceptionCode)(bufferObj.byBuffer[EXCEPTION_CODE] & 0x7f);
                }
            }

            _byExceptionCode = code;
        }

        /// <summary>
        /// Estrae il campo data dal buffer ricevuto
        /// </summary>
        private void DecodeData()
        {
            if (bufferObj.nLenBuffer >= 9)
            {
                bufferObj.nLenData = 0;
                
                for (int j = START_DATA; j < bufferObj.nLenBuffer; j++)
                {
                    bufferObj.byData[bufferObj.nLenData++] = bufferObj.byBuffer[j];
                }
            }
        }

        #endregion
    }
}

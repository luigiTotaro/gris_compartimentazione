﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    [Serializable]
    public class ModbusProtocolException : Exception
    {
        public ModbusProtocolException()
        {
        }

        public ModbusProtocolException(string message) : base(message)
        {
        }

        public ModbusProtocolException(string message, Exception inner) : base(message, inner)
        {
        }

        public ModbusProtocolException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

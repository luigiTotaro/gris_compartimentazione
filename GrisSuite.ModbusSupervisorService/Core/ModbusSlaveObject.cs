﻿using System;
using System.Collections.Generic;
using System.Text;
using GrisSuite.MODBUSSupervisorService.Database;
using GrisSuite.Core;
using System.Collections.ObjectModel;
using System.Net;
using System.Threading;
using System.Collections;
using GrisSuite.MODBUSSupervisorService.Cache;
using GrisSuite.MODBUSSupervisorService.Helper;
using System.IO;
using GrisSuite.MODBUSSupervisorService.Utility;

namespace GrisSuite.MODBUSSupervisorService.Core
{
    public class ModbusSlaveObject : IDevice
    {
        #region Costruttore

        public ModbusSlaveObject(DeviceObject device)
        {
            this._device                            = device;
            this._containsOnlyInfoStreams           = false;
            this._severity                          = Severity.Unknown;
            this._isConnectedModbus                 = false;
            this._isAliveModbus                     = false;
            this._isPopulated                       = false;
            this._nRetryAlive                       = 0;
            this._isExcludedFromSeverityComputation = false;
            this._isReachable                       = true;

            this._coils                             = new CoilsCache            ();
            this._discreteInputs                    = new DiscreteInputsCache   ();
            this._holdingRegisters                  = new HoldingRegistersCache ();
            this._inputRegisters                    = new InputRegistersCache   ();

            this._dtLastMsgReceived                 = DateTime.MinValue;

            this.InitializeDefinition();
        }

        #endregion

        #region Variabili private
        
        private int                                 _nRetryAlive;
        private bool                                _isExcludedFromSeverityComputation;
        private bool                                _isReachable;
        private bool                                _isConnectedModbus;
        private bool                                _isAliveModbus;
        private bool                                _isPopulated;
        private bool                                _deviceContainsStreamWithError;
        private bool                                _containsOnlyInfoStreams;
        private             Severity                _severity;
        private             Collection<DBStream>    _streamData;
        private readonly    String                  _deviceType;
        private readonly    Collection<EventObject> _eventsData;

        private ushort                              _wTransactionID;
        private ushort                              _wProtocolID;
        private byte                                _byUnitID;

        private             DeviceObject            _device;

        private             CoilsCache              _coils;
        private             DiscreteInputsCache     _discreteInputs;
        private             HoldingRegistersCache   _holdingRegisters;
        private             InputRegistersCache     _inputRegisters;

        private             DateTime                _dtLastMsgReceived;

        #endregion

        #region Proprietà pubbliche

        public DateTime dtLastMsgReceived 
        {
            get
            {
                return _dtLastMsgReceived;
            }

            set
            {
                if (_dtLastMsgReceived != value)
                {
                    _dtLastMsgReceived = value;
                }
            }
        }

        /// <summary>
        /// Nome completo della definizione xml della periferica modbus
        /// </summary>
        public string DefinitionFileName                        { get; private set; }

        /// <summary>
        /// TransactionID utilizzato nella comunicazione modbus
        /// </summary>
        public ushort wTransactionID
        {
            get
            {
                return _wTransactionID;
            }
        }

        /// <summary>
        /// ProtocolID utilizzato nella comunicazione modbus
        /// </summary>
        public ushort wProtocolID
        {
            get
            {
                return _wProtocolID;
            }
        }

        /// <summary>
        /// UnitID utilizzato nella comunicazione modbus
        /// </summary>
        public byte byUnitID
        {
            get
            {
                return _byUnitID;
            }
        }

        /// <summary>
        ///   Stato del Device, inteso come Online (raggiungibile almeno via socket) oppure Offline (non raggiungibile e non monitorabile)
        /// </summary>
        public byte Offline
        {
            get
            {
                if (!this.IsReachable)
                    return 1;

                if (!this.IsAliveModbus)
                    return 1;

                if (this.dtLastMsgReceived == DateTime.MinValue)
                    return 1;

                return 0;
            }
        }

        /// <summary>
        ///   Codice univoco della periferica (forzato nel caso di definizioni dinamiche)
        /// </summary>
        public string DeviceType
        {
            get
            {
                // Se esiste un tipo periferica forzato (letto dalle regole dinamiche di caricamento su definizioni dinamiche)
                // restituiamo quello
                if (!string.IsNullOrEmpty(this.ForcedDeviceType))
                {
                    return this.ForcedDeviceType;
                }

                return _deviceType;
            }
        }

        /// <summary>
        /// Severità relativa al device
        /// </summary>
        public Severity SeverityLevel
        {
            get
            {
                return _severity;
            }
        }

        /// <summary>
        ///   Lista degli Stream relativi alla periferica
        /// </summary>
        public Collection<DBStream> StreamData
        {
            get
            {
                if (this.LastError != null)
                {
                    return new Collection<DBStream>();
                }

                return _streamData;
            }
        }

        /// <summary>
        ///   Indica se c'è stata una eccezione durante il caricamento della periferica
        /// </summary>
        public Exception LastError
        {
            get;
            set;
        }

        /// <summary>
        ///   Indica se la periferica è stata tacitata e quindi sarà in stato forzato
        /// </summary>
        public bool ExcludedFromSeverityComputation
        {
            get { return _isExcludedFromSeverityComputation; }
        }

        /// <summary>
        /// Contenitore oggetto periferica, come letto da System.xml
        /// </summary>
        public DeviceObject Device
        {
            get
            {
                return _device;
            }
        }

        /// <summary>
        ///   EndPoint IP della periferica
        /// </summary>
        public IPEndPoint DeviceIPEndPoint
        {
            get
            {
                return _device.EndPoint;
            }
        }

        /// <summary>
        ///   Nome della periferica
        /// </summary>
        public string DeviceName
        {
            get 
            {
                return _device.Name;
            }
        }

        /// <summary>
        ///   Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        public EventObject LastEvent
        {
            get;
            private set;
        }

        /// <summary>
        ///   Lista degli Eventi relativi alla periferica
        /// </summary>
        public Collection<EventObject> EventsData
        {
            get
            {
                return this._eventsData;
            }
        }

        /// <summary>
        ///   Indica se la periferica è raggiungibile via ICMP (ping)
        /// </summary>
        public bool IsReachable
        {
            get
            {
                return _isReachable;
            }

            set
            {
                if (_isReachable != value)
                {
                    _isReachable = value;
                }
            }
        }

        /// <summary>
        /// Indica se la periferica ha risposto alle richieste modbus
        /// </summary>
        public bool IsAliveModbus
        {
            get
            {
                return _isAliveModbus;
            }

            set
            {
                if (_isAliveModbus != value)
                {
                    _isAliveModbus = value;
                }
            }
        }

        /// <summary>
        ///   Indica se la periferica è connessa via socket
        /// </summary>
        public bool IsConnectedModbus
        {
            get
            {
                return _isConnectedModbus;
            }
            set
            {
                if (_isConnectedModbus != value)
                {
                    _isConnectedModbus = value;
                }
            }
        }

        /// <summary>
        ///   <para>Decodifica descrittiva dello stato della periferica</para>
        ///   <para>0 = "In servizio" - Colore verde, significa che la periferica è in servizio e non presenta alcuna anomalia</para>
        ///   <para>1 = "Anomalia lieve" - Colore giallo, significa che ci sono delle anomalie al funzionamento ma non pregiudicano il servizio del sistema</para>
        ///   <para>2 = "Anomalia grave" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
        ///   <para>255 = "Sconosciuto" - Non è stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                string sTmp = Settings.Default.DeviceStatusMessageUnknown;

                if (this.ExcludedFromSeverityComputation)
                {
                    return Settings.Default.DeviceStatusMessageMaintenanceMode; // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                }

                if (this.LastError != null)
                {
                    return Settings.Default.DeviceStatusMessageUnknown;
                }

                if (this.IsReachable && !this.IsAliveModbus)
                {
                    return Settings.Default.DeviceStatusMessageMaintenanceMode;
                }

                if (!this.IsReachable && !this.IsAliveModbus)
                {
                    return Settings.Default.DeviceStatusMessageUnreachable;
                }

                if ((this.IsReachable && this.IsAliveModbus) || (!this.IsReachable && this.IsAliveModbus))
                {
                    switch (this.SeverityLevel)
                    {
                        case Severity.Ok:
                            sTmp = Settings.Default.DeviceStatusMessageOK;
                            break;
                        case Severity.Warning:
                            if (this._deviceContainsStreamWithError)
                            {
                                sTmp = Settings.Default.DeviceStatusMessageIncompleteData;
                                break;
                            }
                            sTmp = Settings.Default.DeviceStatusMessageWarning;
                            break;
                        case Severity.Error:
                            sTmp = Settings.Default.DeviceStatusMessageError;
                            break;
                        case Severity.Unknown:
                            sTmp = (this._containsOnlyInfoStreams ? Settings.Default.DeviceStatusMessageNotApplicable : Settings.Default.DeviceStatusMessageUnknown);
                            break;
                    }
                }

                return sTmp;

                #region Commento
                /*

                if ((this.IsAliveIcmp && this.IsAliveSnmp) || (!this.IsAliveIcmp && this.IsAliveSnmp))
                {
                    // Se sono impostate la severità e la descrizione forzate per la periferica, restiuisce quelle
                    // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
                    // con regole dinamiche che è ricaduta nella MIB-II base
                    if (this.NoMatchDeviceStatus != null)
                    {
                        return this.NoMatchDeviceStatus.NoMatchDeviceStatusDescription;
                    }

                    switch (this.SeverityLevel)
                    {
                        case Severity.Ok:
                            return Settings.Default.DeviceStatusMessageOK;
                        case Severity.Warning:
                            if (this.deviceContainsStreamWithError)
                            {
                                return Settings.Default.DeviceStatusMessageIncompleteData;
                            }

                            if (this.replyOnlyToMIBBase)
                            {
                                return Settings.Default.DeviceStatusMessageMaintenanceModeMIB2Only;
                            }

                            return Settings.Default.DeviceStatusMessageWarning;
                        case Severity.Error:
                            return Settings.Default.DeviceStatusMessageError;
                        case Severity.Unknown:
                            return this._containsOnlyInfoStreams
                                       ? Settings.Default.DeviceStatusMessageNotApplicable
                                       : Settings.Default.DeviceStatusMessageUnknown;
                    }
                }

                if (!this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return Settings.Default.DeviceStatusMessageUnreachable;
                }

                return Settings.Default.DeviceStatusMessageUnknown;
                */
                #endregion
            }
        }

        public CoilsCache Coils 
        { 
            get
            {
                return _coils;
            }
        }

        public DiscreteInputsCache DiscreteInputs
        {
            get
            {
                return _discreteInputs;
            }
        }

        public HoldingRegistersCache HoldingRegisters
        {
            get
            {
                return _holdingRegisters;
            }
        }

        public InputRegistersCache InputRegisters
        {
            get
            {
                return _inputRegisters;
            }
        }

        #endregion

        #region Proprietà private

        /// <summary>
        /// Nome di periferica forzato, che può subentrare, se configurato, nel caso di definizioni a caricamento dinamico
        /// </summary>
        internal string ForcedDeviceType
        {
            get;
            private set;
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOk"></param>
        /// <returns></returns>
        public bool CheckAliveModbus(bool isOk)
        {
            if (!isOk)
            {
                if (this._nRetryAlive > Settings.Default.ModbusMaxRetriesCount)
                {
                    return false;
                }

                this._nRetryAlive++;

                return true;
            }

            this._nRetryAlive = 0;

            return true;
        }

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        public void Populate()
        {
            this.LastError = null;

            this.SetDeviceAcknowledgements();

            if (this.IsReachable)
            {
                this.RetrieveStreamsData();
            }

            if (this.LastError == null)
            {
                this._isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate(this.IsReachable);
                    }
                }

                if (Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem > 0)
                {
                    // E' attivo il tentativo di recupero in caso di severità non normale della periferica
                    // Tenta di recuperare una periferica in condizione di errore, ma non permanente (fluttuazione di stato)

                    // C'è già stato un tentativo, quello principale, quindi il prossimo sarà il secondo
                    ushort retryCounter = 2;

                    while (retryCounter <= Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem)
                    {
                        if (this.SeverityLevel != Severity.Ok)
                        {
                            Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenRetriesToRecoverFromDeviceSeverityProblem);
                            this.PopulateInternal();
                            retryCounter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                this._isPopulated = false;
            }

            // Il calcolo della severità permette di avere tutti gli stati possibili impostati e gli stream valutati
            this._severity = this.CalculateSeverity();
        }

        /// <summary>
        /// Aggiorna i dati della periferica sul database
        /// </summary>
        public void UpdateDb()
        {
            bool? deviceExistence = DatabaseObject.Instance.CheckDeviceExistence(Device.DeviceId);

            if (!deviceExistence.HasValue)
                return;
            if (!deviceExistence.Value)
                return;

            DatabaseObject.Instance.UpdateDeviceStatus(Device.DeviceId, (int)SeverityLevel, ValueDescriptionComplete, Offline);

            if (Offline != 1)
            {
                foreach (DBStream stream in StreamData)
                {
                    DatabaseObject.Instance.UpdateStream(Device.DeviceId, stream.StreamId, stream.Name, (int)stream.SeverityLevel, stream.ValueDescriptionComplete, dtLastMsgReceived);

                    foreach (DBStreamField field in stream.FieldData)
                    {
                        if (field.PersistOnDatabase)
                        {
                            DatabaseObject.Instance.UpdateStreamField(Device.DeviceId, stream.StreamId, field.FieldId, field.ArrayId, field.Name, (int)field.SeverityLevel,
                                                                      field.Value, field.ValueDescriptionComplete);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///   Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
        /// </summary>
        /// <returns>Rappresentazione completa e descrittiva della periferica a fini diagnostici</returns>
        public string Dump()
        {
            return "";
        }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Carica il file delle definizioni della periferica
        /// </summary>
        private void InitializeDefinition()
        {
            try
            {
                string sTypeFile = String.Format("{0}.xml", this._device.DeviceType.ToUpper());

#if (DEBUG)
                this.DefinitionFileName = FileUtility.GetDefinitionFile(sTypeFile);
#else
                this.DefinitionFileName = FileUtility.GetDefinitionFile(sTypeFile);
#endif
                using (DeviceConfigurationHelper CfgHelper = new DeviceConfigurationHelper(DefinitionFileName))
                {
                    this._wTransactionID = CfgHelper.wTransactionID;
                    this._wProtocolID    = CfgHelper.wProtocolID;
                    this._byUnitID       = CfgHelper.byUnitID;
                    this._streamData     = CfgHelper.LoadConfiguration();
                }
            }
            catch (DeviceConfigurationHelperException ex)
            {
                this.LastError = ex;
            }
            catch (ArgumentException ex)
            {
                this.LastError = ex;
            }
        }

        private void RetrieveStreamsData()
        {
            foreach (DBStream stream in this.StreamData)
            {
                RetriveFieldsData(stream);
            }
        }

        private void RetriveFieldsData(DBStream stream)
        {
            foreach (DBStreamField field in stream.FieldData)
            {
                switch (field.byFunCode)
                {
                    case ModbusMasterObject.FC_READ_COIL:
                        field.exceptionCode = this.Coils.exceptionCode;
                        field.RawData       = this.Coils.ToRawData              (field.wOffset, field.wCount);
                        break;
                    case ModbusMasterObject.FC_READ_INPUT_DISCRETE:
                        field.exceptionCode = this.DiscreteInputs.exceptionCode;
                        field.RawData       = this.DiscreteInputs.ToRawData     (field.wOffset, field.wCount);
                        break;
                    case ModbusMasterObject.FC_READ_HOLDING_REGISTER:
                        field.exceptionCode = this.HoldingRegisters.exceptionCode;
                        field.RawData       = this.HoldingRegisters.ToRawData   (field.wOffset, field.wCount);
                        break;
                    case ModbusMasterObject.FC_READ_INPUT_REGISTER:
                        field.exceptionCode = this.InputRegisters.exceptionCode;
                        field.RawData       = this.InputRegisters.ToRawData     (field.wOffset, field.wCount);
                        break;
                }
            }
        }

        /// <summary>
        ///   Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        private void PopulateInternal()
        {
            this.LastError = null;

            if (this.IsReachable)
            {
                this.RetrieveStreamsData();
            }

            if (this.LastError == null)
            {
                this._isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate(this.IsReachable);
                    }
                }
            }
            else
            {
                this._isPopulated = false;
            }
        }

        /// <summary>
        /// Calcola la severità della periferica e ne valorizza gli stati possibili
        /// </summary>
        /// <returns></returns>
        private Severity CalculateSeverity()
        {
            if (this.ExcludedFromSeverityComputation)
            {
                // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                return Severity.MaintenanceMode;
            }

            if (this.LastError != null)
            {
                return Severity.Unknown;
            }

            if (this.IsReachable && !this.IsAliveModbus)
            {
                return Severity.Warning;
            }

            // Se sono impostate la severità e la descrizione forzate per la periferica, restituisce quelle
            // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
            // con regole dinamiche che è ricaduta nella MIB-II base
            //if (this.NoMatchDeviceStatus != null)
            //{
            //    // la periferica può forzare uno stato SNMP, solo nel caso che SNMP funzioni
            //    if (this.IsAliveModbus)
            //    {
            //        return this.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity;
            //    }

            //    return Severity.Warning;
            //}

            
            Severity streamSeverity = Severity.Info;

            if (this.StreamData.Count > 0)
            {
                foreach (DBStream stream in this.StreamData)
                {
                    if (stream.ExcludedFromSeverityComputation)
                        continue;

                    if (streamSeverity < stream.SeverityLevel)
                    {
                        streamSeverity = stream.SeverityLevel;
                    }

                    if ((!this._deviceContainsStreamWithError) && (stream.ContainsStreamFieldWithError))
                    {
                        this._deviceContainsStreamWithError = true;

                        foreach (DBStreamField streamField in stream.FieldData)
                        {
                            if (streamField.ExcludedFromSeverityComputation)
                                continue;

                            if (streamSeverity < streamField.SeverityLevel)
                            {
                                streamSeverity = streamField.SeverityLevel;
                            }
                        }
                    }
                }
            }
            else
            {
                streamSeverity = Severity.Unknown;
            }
            
            return streamSeverity;
        }

        /// <summary>
        /// Imposta le tacitazioni su periferica, stream e stream field, in base alla configurazione ricevuta da base dati
        /// </summary>
        private void SetDeviceAcknowledgements()
        {
            if (this.Device.DeviceAck != null)
            {
                #region Tacitazione di periferica

                if (this.Device.DeviceAck.IsDeviceAcknowledged)
                {
                    this._isExcludedFromSeverityComputation = true;
                }

                #endregion

                #region Tacitazione stream

                foreach (DeviceStreamAcknowledgement streamAcknowledgement in this.Device.DeviceAck.DeviceStreamAcknowledgements)
                {
                    foreach (DBStream stream in this.StreamData)
                    {
                        if (stream.StreamId == streamAcknowledgement.StreamId)
                        {
                            stream.ExcludedFromSeverityComputation = true;
                            break;
                        }
                    }
                }

                #endregion

                #region Tacitazione stream field

                foreach (DeviceStreamFieldAcknowledgement streamFieldAcknowledgement in this.Device.DeviceAck.DeviceStreamFieldAcknowledgements)
                {
                    foreach (DBStream stream in this.StreamData)
                    {
                        if (stream.StreamId == streamFieldAcknowledgement.StreamId)
                        {
                            foreach (DBStreamField streamField in stream.FieldData)
                            {
                                if (streamField.FieldId == streamFieldAcknowledgement.StreamFieldId)
                                {
                                    streamField.ExcludedFromSeverityComputation = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                #endregion
            }
        }

        #endregion

        #region Predicati utilizzati per la ricerca

        /// <summary>
        ///  Esprime il filtro della ricerca per Id
        /// </summary>
        /// <param name="Id">Identificativo della peru sul quale fare la ricerca</param>
        /// <returns>true se l'identificativo corrisponde</returns>
        public static Predicate<ModbusSlaveObject> ByDeviceId(long DeviceId)
        {
            return delegate(ModbusSlaveObject modbusSlave)
            {
                return modbusSlave.Device.DeviceId == DeviceId;
            };
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Cache
{
    public class HoldingRegistersCache
    {
        #region Costruttore

        public HoldingRegistersCache()
        {
            _bUpdated           = false;
            _exceptionCode      = MsgObject.ExceptionCode.None;
            _HoldingRegisters   = new Dictionary<ushort, ushort>();
        }

        #endregion

        #region Campi privati

        private bool                        _bUpdated;
        private MsgObject.ExceptionCode     _exceptionCode;
        private Dictionary<ushort, ushort>  _HoldingRegisters;

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Lista degli holding register salvati in RAM
        /// </summary>
        public Dictionary<ushort, ushort> HoldingRegisters
        {
            get
            {
                return _HoldingRegisters;
            }
        }

        /// <summary>
        /// Specifica se dall'ultima lettura su modbus è stato modificato il valore di almeno uno degli holding register in RAM
        /// </summary>
        public bool bUpdated
        {
            get
            {
                return _bUpdated;
            }
        }

        /// <summary>
        /// Specifica se nell'ultima comunicazione lo slave modbus ha comunicato un'anomalia 
        /// di funzionamento
        /// </summary>
        public MsgObject.ExceptionCode exceptionCode
        {
            get { return _exceptionCode; }
            set
            {
                if (_exceptionCode != value)
                {
                    _exceptionCode = value;
                }
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Aggiorna in RAM, se necessario, i valori degli holding register ricevuti dallo slave modbus
        /// </summary>
        /// <param name="wRefNumber">offset di partenza degli holding register ricevuti nell'ultimo polling dallo slave</param>
        /// <param name="wWordCount">numero di holding register letti</param>
        /// <param name="byRegValues">valore degli holding register letti</param>
        public void Update(ushort wRefNumber, ushort wWordCount, List<ushort> wRegValues)
        {
            bool        bTemp   = false;
            IEnumerator en      = wRegValues.GetEnumerator();

            en.Reset();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wWordCount); wKey++)
            {
                en.MoveNext();

                ushort tempVal = (ushort)en.Current;

                if (this._HoldingRegisters.ContainsKey(wKey))
                {
                    if (this._HoldingRegisters[wKey] != tempVal)
                    {
                        this._HoldingRegisters[wKey]    = tempVal;
                        bTemp                           = true;
                    }
                }
                else
                {
                    this._HoldingRegisters.Add(wKey, tempVal);
                    bTemp = true;
                }
            }

            _bUpdated = bTemp;
        }

        /// <summary>
        /// Converte l'intervallo di holding register specificato in una stringa da salvare su db
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wBitCount">numero di holding register da salvare</param>
        /// <returns>stringa ottenuta dalla concatenazione degli holding register</returns>
        public string ToString(ushort wRefNumber, ushort wWordCount)
        {
            string sTmp = "";

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wWordCount); wKey++)
            {
                if (!this._HoldingRegisters.ContainsKey(wKey))
                    continue;

                sTmp += String.Format("{0:x4}", this._HoldingRegisters[wKey]);
            }

            return sTmp;
        }

        /// <summary>
        /// Converte l'intervallo di holding register specificato in un'array di byte grezzi
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wWordCount">numero di holding register da convertire</param>
        /// <returns>lista di registri ottenuta dalla conversione</returns>
        public List<ushort> ToRawData(ushort wRefNumber, ushort wWordCount)
        {
            List<ushort> wList = new List<ushort>();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wWordCount); wKey++)
            {
                if (!this._HoldingRegisters.ContainsKey(wKey))
                    continue;

                //byList.AddRange(BitConverter.GetBytes(this._HoldingRegisters[wKey]));
                wList.Add(this._HoldingRegisters[wKey]);
            }

            return wList;
        }

        #endregion
    }
}

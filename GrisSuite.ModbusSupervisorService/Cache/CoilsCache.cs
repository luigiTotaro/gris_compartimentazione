﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Cache
{
    public class CoilsCache
    {
        #region Costruttore

        public CoilsCache()
        {
            _bUpdated       = false;
            _exceptionCode  = MsgObject.ExceptionCode.None;
            _Coils          = new Dictionary<ushort, bool>();
        }

        #endregion

        #region Campi privati

        private bool                        _bUpdated;
        private MsgObject.ExceptionCode     _exceptionCode;
        private Dictionary<ushort, bool>    _Coils;

        #endregion

        #region Proprità pubbliche

        /// <summary>
        /// Lista dei coil salvati in RAM
        /// </summary>
        public Dictionary<ushort, bool> Coils
        {
            get
            {
                return _Coils;
            }
        }

        /// <summary>
        /// Specifica se dall'ultima lettura su modbus è stato modificato il valore di almeno uno dei coil in RAM
        /// </summary>
        public bool bUpdated
        {
            get
            {
                return _bUpdated;
            }
        }

        /// <summary>
        /// Specifica se nell'ultima comunicazione lo slave modbus ha comunicato un'anomalia 
        /// di funzionamento
        /// </summary>
        public MsgObject.ExceptionCode exceptionCode
        {
            get { return _exceptionCode; }
            set
            {
                if (_exceptionCode != value)
                {
                    _exceptionCode = value;
                }
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Aggiorna in RAM, se necessario, i valori dei coil ricevuti dallo slave modbus
        /// </summary>
        /// <param name="wRefNumber">offset di partenza dei coil ricevuti nell'ultimo polling allo slave</param>
        /// <param name="wBitCount">numero di coil letti</param>
        /// <param name="byBitValues">valore dei coil letti</param>
        public void Update(ushort wRefNumber, ushort wBitCount, List<byte> byBitValues)
        {
            bool        bTemp   = false;
            IEnumerator en      = new BitArray(byBitValues.ToArray()).GetEnumerator();

            en.Reset();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wBitCount); wKey++)
            {
                en.MoveNext();

                bool tempVal = (bool)en.Current;

                if (this._Coils.ContainsKey(wKey))
                {
                    if (this._Coils[wKey] != tempVal)
                    {
                        this._Coils[wKey]   = tempVal;
                        bTemp               = true;
                    }
                }
                else
                {
                    this._Coils.Add(wKey, tempVal);
                    bTemp = true;
                }
            }

            _bUpdated = bTemp;
        }

        /// <summary>
        /// Converte l'intervallo di coil specificato in una stringa da salvare su db
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wBitCount">numero di coil da salvare</param>
        /// <returns>stringa ottenuta dalla concatenazione dei coil</returns>
        public string ToString(ushort wRefNumber, ushort wBitCount)
        {
            string sTmp = "";

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wBitCount); wKey++)
            {
                if (!this._Coils.ContainsKey(wKey))
                    continue;

                if (this._Coils[wKey])
                    sTmp += "1";
                else
                    sTmp += "0";
            }

            return sTmp;
        }

        /// <summary>
        /// Converte l'intervallo di coil specificato in un'array di byte grezzi
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wBitCount">numero di coil da convertire</param>
        /// <returns>lista di valori ottenuta dalla conversione</returns>
        public List<ushort> ToRawData(ushort wRefNumber, ushort wBitCount)
        {
            List<ushort> wList = new List<ushort>();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wBitCount); wKey++)
            {
                if (!this._Coils.ContainsKey(wKey))
                    continue;

                if (this._Coils[wKey])
                    wList.Add(0x0001);
                else
                    wList.Add(0x0000);
            }

            return wList;
        }

        #endregion
    }
}

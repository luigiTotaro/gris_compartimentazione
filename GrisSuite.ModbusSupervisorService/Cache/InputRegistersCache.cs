﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Cache
{
    public class InputRegistersCache
    {
        #region Costruttore

        public InputRegistersCache()
        {
            _bUpdated           = false;
            _exceptionCode      = MsgObject.ExceptionCode.None;
            _InputRegisters     = new Dictionary<ushort, ushort>();
        }

        #endregion

        #region Campi privati

        private bool                        _bUpdated;
        private MsgObject.ExceptionCode     _exceptionCode;
        private Dictionary<ushort, ushort>  _InputRegisters;

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Lista degli input register salvati in RAM
        /// </summary>
        public Dictionary<ushort, ushort> InputRegisters
        {
            get
            {
                return _InputRegisters;
            }
        }

        /// <summary>
        /// Specifica se dall'ultima lettura su modbus è stato modificato almeno uno degli input register in RAM
        /// </summary>
        public bool bUpdated
        {
            get
            {
                return _bUpdated;
            }
        }

        /// <summary>
        /// Specifica se nell'ultima comunicazione lo slave modbus ha comunicato un'anomalia 
        /// di funzionamento
        /// </summary>
        public MsgObject.ExceptionCode exceptionCode
        {
            get { return _exceptionCode; }
            set
            {
                if (_exceptionCode != value)
                {
                    _exceptionCode = value;
                }
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Aggiorna in RAM, se necessario, i valori degli input register ricevuti dallo slave modbus
        /// </summary>
        /// <param name="wRefNumber">numero di input register letti</param>
        /// <param name="wWordCount">numero di input register letti</param>
        /// <param name="wRegValues">valore degli input register letti</param>
        public void Update(ushort wRefNumber, ushort wWordCount, List<ushort> wRegValues)
        {
            bool        bTemp       = false;
            IEnumerator en          = wRegValues.GetEnumerator();

            en.Reset();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wWordCount); wKey++)
            {
                en.MoveNext();

                ushort tempVal = (ushort)en.Current;

                if (this._InputRegisters.ContainsKey(wKey))
                {
                    if (this._InputRegisters[wKey] != tempVal)
                    {
                        this._InputRegisters[wKey] = tempVal;
                        bTemp = true;
                    }
                }
                else
                {
                    this._InputRegisters.Add(wKey, tempVal);
                    bTemp = true;
                }
            }

            _bUpdated = bTemp;
        }

        /// <summary>
        /// Converte l'intervallo di input register specificato in una stringa da salvare su db
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wWordCount">numero di input register da salvare</param>
        /// <returns>stringa ottenuta dalla concatenazione degli input register</returns>
        public string ToString(ushort wRefNumber, ushort wWordCount)
        {
            string sTmp = "";

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wWordCount); wKey++)
            {
                if (!this._InputRegisters.ContainsKey(wKey))
                    continue;

                sTmp += String.Format("{0:x4}", this._InputRegisters[wKey]);
            }

            return sTmp;
        }

        /// <summary>
        /// Converte l'intervallo di input register specificato in un'array di byte grezzi
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wWordCount">numero di input register da convertire</param>
        /// <returns>lista di registri ottenuta dalla conversione</returns>
        public List<ushort> ToRawData(ushort wRefNumber, ushort wWordCount)
        {
            List<ushort> wList = new List<ushort>();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wWordCount); wKey++)
            {
                if (!this._InputRegisters.ContainsKey(wKey))
                    continue;

                wList.Add(this._InputRegisters[wKey]);
            }

            return wList;
        }

        #endregion
    }
}

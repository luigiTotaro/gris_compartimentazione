﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Cache
{
    public class DiscreteInputsCache
    {
        #region Costruttore

        public DiscreteInputsCache()
        {
            _bUpdated       = false;
            _exceptionCode  = MsgObject.ExceptionCode.None;
            _DiscreteInputs = new Dictionary<ushort, bool>();
        }

        #endregion

        #region Campi privati

        private bool                        _bUpdated;
        private MsgObject.ExceptionCode     _exceptionCode;
        private Dictionary<ushort, bool>    _DiscreteInputs;

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Lista dei discrete inputs salvati in RAM
        /// </summary>
        public Dictionary<ushort, bool> DiscreteInputs
        {
            get
            {
                return _DiscreteInputs;
            }
        }

        /// <summary>
        /// Specifica se dall'ultima lettura su modbus è stato modificato il valore di almenno uno dei discrete input in RAM
        /// </summary>
        public bool bUpdated
        {
            get
            {
                return _bUpdated;
            }
        }

        /// <summary>
        /// Specifica se nell'ultima comunicazione lo slave modbus ha comunicato un'anomalia 
        /// di funzionamento
        /// </summary>
        public MsgObject.ExceptionCode exceptionCode
        {
            get { return _exceptionCode; }
            set
            {
                if (_exceptionCode != value)
                {
                    _exceptionCode = value;
                }
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Aggiorna in RAM, se necessario, i valori degli input discreti ricevuti dallo slave modbus
        /// </summary>
        /// <param name="wRefNumber">offset di partenza degli input discreti ricevuti nell'ultimo polling dallo slave</param>
        /// <param name="wBitCount">numero degli input discreti letti</param>
        /// <param name="byBitValues">valore degli input register letti</param>
        public void Update(ushort wRefNumber, ushort wBitCount, List<byte> byBitValues)
        {
            bool        bTemp   = false;
            IEnumerator en      = new BitArray(byBitValues.ToArray()).GetEnumerator();

            en.Reset();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wBitCount); wKey++)
            {
                en.MoveNext();

                bool tempVal = (bool)en.Current;

                if (this._DiscreteInputs.ContainsKey(wKey))
                {
                    if (this._DiscreteInputs[wKey] != tempVal)
                    {
                        this._DiscreteInputs[wKey]  = tempVal;
                        bTemp                       = true;
                    }
                }
                else
                {
                    this._DiscreteInputs.Add(wKey, tempVal);
                    bTemp = true;
                }
            }

            _bUpdated = bTemp;
        }

        /// <summary>
        /// Converte l'intervallo di input discreti specificato in una stringa da salvare su db
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wBitCount">numero di input discreti da salvare</param>
        /// <returns>stringa ottenuta dalla concatenazione degli input discreti</returns>
        public string ToString(ushort wRefNumber, ushort wBitCount)
        {
            string sTmp = "";

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wBitCount); wKey++)
            {
                if (!this._DiscreteInputs.ContainsKey(wKey))
                    continue;

                if (this._DiscreteInputs[wKey])
                    sTmp += "1";
                else
                    sTmp += "0";
            }

            return sTmp;
        }

        /// <summary>
        /// Converte l'intervallo di input discreti specificato in un'array di byte grezzi
        /// </summary>
        /// <param name="wRefNumber">offset di riferimento</param>
        /// <param name="wBitCount">numero di input discreti da convertire</param>
        /// <returns>lista di valori ottenuta dalla conversione</returns>
        public List<ushort> ToRawData(ushort wRefNumber, ushort wBitCount)
        {
            List<ushort> wList = new List<ushort>();

            for (ushort wKey = wRefNumber; wKey < Convert.ToUInt32(wRefNumber + wBitCount); wKey++)
            {
                if (!this._DiscreteInputs.ContainsKey(wKey))
                    continue;

                if (this._DiscreteInputs[wKey])
                    wList.Add(0x0001);
                else
                    wList.Add(0x0000);
            }

            return wList;
        }

        #endregion
    }
}

﻿using System;
using System.ServiceProcess;
using GrisSuite.MODBUSSupervisorService.Utility;
using GrisSuite.MODBUSSupervisorService.Database;
using System.Globalization;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using GrisSuite.MODBUSSupervisorService.Core;
using System.Timers;
using System.Collections.ObjectModel;

namespace GrisSuite.MODBUSSupervisorService
{
    public partial class MainService : ServiceBase
    {
        #region Campi privati

        //private static string                               configurationSystemFile;
        //private static System.Timers.Timer                  startupTimer;
        //private static SqlConnection                        dbConnection;
        //private static SqlConnection                        dbEventsConnection;
        //private static DatabaseFactory                      persistenceFactory;
        //private static ReadOnlyCollection<DeviceObject>     devices;
        //private static ReadOnlyCollection<ProfileObject>    profiles;

        private static System.Timers.Timer                  _aTimerPolling = null;
        private static System.Timers.Timer                  _aTimerStartup = null;
        
        #endregion

        #region Costruttore

        public MainService()
        {
            InitializeComponent();
            GrisSuite.AppCfg.AppCfg.Default.LogAllCfg();
        }

        #endregion

        #region Start del servizio

        protected override void OnStart(string[] args)
        {
            try { 

                InitializeTimers();
                InitializeStart(AppCfg.AppCfg.Default.sXMLSystem);
            }
            catch (Exception exc)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, String.Format("OnStart ERROR {0}",  exc.Message));
            }
        }

        #endregion

        #region Inizializzazione dei timer

        private void InitializeTimers()
        {
            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose, "InitializeTimers ...");

            MainService._aTimerStartup              = new System.Timers.Timer();
            MainService._aTimerStartup.Elapsed     += new ElapsedEventHandler(OnStartupTimerElapsed);
            MainService._aTimerStartup.Interval     = (Settings.Default.StartupDelaySeconds * 1000);
            MainService._aTimerStartup.AutoReset    = false;


            MainService._aTimerPolling              = new System.Timers.Timer();
            MainService._aTimerPolling.Elapsed     += new ElapsedEventHandler(OnPolling);
            MainService._aTimerPolling.Interval     = (Settings.Default.PollingIntervalSeconds * 1000);
            MainService._aTimerPolling.AutoReset    = false;
        }

        #endregion

        #region Inizializzazione, verifica configurazione ed avvio monitoraggio

        public Exception InitializeStart(string configurationSystemFilename) //, bool? persistOnDatabase)
        {
            Exception lastException = null;

            if ((DatabaseObject.Instance.lastException == null) && (DatabaseObject.Instance.dbConnection != null) && (DatabaseObject.Instance.dbEventsConnection != null))
            {
                if (Settings.Default.StartupDelaySeconds > 0)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                                   string.Format(CultureInfo.InvariantCulture,
                                                                                 "Avvio servizio con attesa {0} secondi per primo monitoraggio.",
                                                                                 Settings.Default.StartupDelaySeconds));

                    MainService._aTimerStartup.Start();
                }
                else
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format(CultureInfo.InvariantCulture, "Avvio servizio."));

                    CleanupAndCreateDevices();
                }

#if (DEBUG)
                // In fase di debug lasciamo vari secondi per il completamento dei thread, da aumentare all'occorrenza
                Thread.Sleep(Settings.Default.StartupDelaySeconds * 1000);
                Thread.Sleep(((int)Settings.Default.PollingIntervalSeconds - 1) * 1000);
#endif
            }
            else
            {
                lastException = new ConfigurationErrorsException("Le stringhe di connessione al database o al database degli eventi non sono disponibili.");
            }

            return lastException;
        }

        #endregion

        #region Gestione eventi tick dei timer

        private void OnStartupTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_aTimerStartup != null)
            {
                _aTimerStartup.Stop     ();
                _aTimerStartup.Dispose  ();
            }

            CleanupAndCreateDevices();
        }

        private void OnPolling(object sender, ElapsedEventArgs e)
        {
            try
            {
                MainService._aTimerPolling.Stop ();

                if (DeviceList.Instance.Devices.Count > 0)
                {
                    PollDevices();
                }

                MainService._aTimerPolling.Start();
            }
            catch (Exception ex)
            {
                MainService._aTimerPolling.Enabled = true;

                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
                                                               string.Format(CultureInfo.InvariantCulture, "{0}\r\n{1}\r\n{2}", ex.Message,
                                                                             ex.StackTrace, ex.InnerException));
            }
        }

        #endregion

        #region CleanupAndCreateDevices - Carica le periferiche da configurazione, ripulisce la base dati e ricrea le periferiche

        private void CleanupAndCreateDevices()
        {
            string  sLog            = "";
            bool    isMonitoring    = false;
            
            foreach (DeviceObject device in DeviceList.Instance.Devices)
            {
                device.MonitoringEnabled = false;

                if (device.Active != 1) {

                    sLog = string.Format                            (CultureInfo.InvariantCulture, "Periferica caricata da System.xml, ma non attiva. Ogni ulteriore operazione relativa alla periferica annullata. {0}", device);
                    FileUtility.AppendStringToFileWithLoggingLevel  (LoggingLevel.Info, sLog);
                    continue;
                }

                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose, device.ToString());

                bool? deviceExistence = DatabaseObject.Instance.CheckDeviceExistence(device.DeviceId);

                if (deviceExistence.HasValue)
                {
                    if (deviceExistence.Value)
                    {
                        if (Settings.Default.ShouldDeleteExistingDevicesAtStartup)
                        {
                            if (DatabaseObject.Instance.DeleteDevice(device))
                            {
                                device.MonitoringEnabled = DatabaseObject.Instance.UpdateInsertDevice(device, DeviceWriteMode.Insert);

                                if (device.MonitoringEnabled)
                                {
                                    isMonitoring = true;
                                }
                            }
                            else
                            {
                                // non è stato possibile cancellare la periferica esistente
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                                               string.Format(CultureInfo.InvariantCulture,
                                                                                             "Periferica presente in base dati e richiesta eliminazione all'avvio del servizio, ma eliminazione fallita. Ogni ulteriore operazione relativa alla periferica annullata. {0}",
                                                                                             device));
                                device.MonitoringEnabled = false;
                            }
                        }
                        else
                        {
                            // aggiorna la periferica esistente
                            device.MonitoringEnabled = DatabaseObject.Instance.UpdateInsertDevice(device, DeviceWriteMode.Update);

                            if (device.MonitoringEnabled)
                            {
                                isMonitoring = true;
                            }
                        }
                    }
                    else
                    {
                        if (Settings.Default.ShouldCreateNewDevices)
                        {
                            device.MonitoringEnabled = DatabaseObject.Instance.UpdateInsertDevice(device, DeviceWriteMode.Insert);

                            if (device.MonitoringEnabled)
                            {
                                isMonitoring = true;
                            }
                        }
                        else
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                                                                           string.Format(CultureInfo.InvariantCulture,
                                                                                         "Periferica non presente in base dati, ma creazione di nuove periferiche disattivata da configurazione (modificare flag di configurazione 'ShouldCreateNewDevices'). Non sarà possibile creare gli Stream / Stream Fields per la periferica. {0}",
                                                                                         device));
                        }
                    }
                }
            }

#if (_DEBUG)
            isMonitoring = true; // solo per primo test
#endif

            if (isMonitoring)
            {
#if (!_DEBUG)
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Avvio monitoraggio.");

                MainService._aTimerPolling.Start();
#else
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Avvio monitoraggio.");

                if ((DeviceList.Instance.Devices != null) && (DeviceList.Instance.Devices.Count > 0))
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning, "Esecuzione recupero singolo di dati (modalità debug).");
                    PollDevices();
                }
#endif
            }
            else
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
                                                               "Tutte le periferiche da monitorare hanno generato errori in fase di aggiornamento/inserimento iniziale. Nessun monitoraggio in corso. Verificare i problemi delle periferiche e riavviare il servizio.");
            }
        }

        #endregion

        private void PollDevices()
        {
            DeviceAcknowledgement                       deviceAcknowledgement;
            ReadOnlyCollection<DbDeviceAcknowledgement> deviceAcknowledgementsList = null;

            if (FileUtility.LogLevel >= LoggingLevel.Verbose) {
                
                FileUtility.RecycleLogFile();

                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
                                                               string.Format(CultureInfo.InvariantCulture, "Monitoraggio {0} periferiche.",
                                                                             DeviceList.Instance.Devices.Count));
            }

            if (DeleteExpiredAck())
            {
                deviceAcknowledgementsList = GetAckList(); // Carichiamo le tacitazioni solamente se il comando du pulizia ha funzionato, per evitare di avere dati spuri e riverificare la scadenza qui
            }
            
            foreach (DeviceObject device in DeviceList.Instance.Devices)
            {
                if (!device.MonitoringEnabled)
                    continue;

                deviceAcknowledgement = new DeviceAcknowledgement(device.DeviceId);
                device.DeviceAck      = deviceAcknowledgement;                          // Reimpostiamo l'oggetto che contiene gli Ack, in modo che, ad ogni iterazione, siano azzerati per la periferica

                if ((deviceAcknowledgementsList != null) && (deviceAcknowledgementsList.Count > 0))
                {
                    bool existsAckByDevice = false;

                    foreach (DbDeviceAcknowledgement dbDeviceAcknowledgement in deviceAcknowledgementsList)
                    {
                        if (dbDeviceAcknowledgement.DevID == device.DeviceId)
                        {
                            deviceAcknowledgement.AddDeviceAcknowledgement(dbDeviceAcknowledgement.StrID,
                                                                           dbDeviceAcknowledgement.FieldID);
                            existsAckByDevice = true;
                        }
                    }

                    if (existsAckByDevice)
                    {
                        device.DeviceAck = deviceAcknowledgement;

                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
                                                                       string.Format(CultureInfo.InvariantCulture,
                                                                                     "Periferica {0},\r\nassegnate tacitazioni {1}.",
                                                                                     device, deviceAcknowledgement));
                    }
                }

                if (device.Scheduled != 0)
                {
                    ModbusMasterList.Instance.MonitorDevice(device);
                }
            }
        }

        /// <summary>
        /// Pulisce dal db le tacitazioni scadute
        /// </summary>
        private bool DeleteExpiredAck()
        {
            bool expiredAcknowledgementsDeleted = DatabaseObject.Instance.DeleteExpiredAck();

            if (expiredAcknowledgementsDeleted)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
                                                               string.Format(CultureInfo.InvariantCulture,
                                                                             "Ripulite tacitazioni scadute da database."));
            }

            return expiredAcknowledgementsDeleted;
        }

        /// <summary>
        /// Carica le tacitazioni dal db
        /// </summary>
        /// <returns></returns>
        private ReadOnlyCollection<DbDeviceAcknowledgement> GetAckList()
        {
            ReadOnlyCollection<DbDeviceAcknowledgement> deviceAcknowledgementsList = null;

            deviceAcknowledgementsList = DatabaseObject.Instance.GetAckList();

            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
                                                           string.Format(CultureInfo.InvariantCulture,
                                                                         "Caricat{0} {1} tacitazion{2} da database.",
                                                                         (deviceAcknowledgementsList.Count == 1 ? "a" : "e"),
                                                                         deviceAcknowledgementsList.Count,
                                                                         (deviceAcknowledgementsList.Count == 1 ? "e" : "i")));
            return deviceAcknowledgementsList;
        }

        protected override void OnStop()
        {
        }

#if (DEBUG)
        public void Run()
        {
            this.OnStart(null);
            //this.OnStop();

            while(true)
                Thread.Sleep(60000);
        }
#endif
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Threading;
using GrisSuite.MODBUSSupervisorService.Properties;
using GrisSuite.MODBUSSupervisorService.Utility;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///     Rappresenta la classe di utility per l'accesso al database
    /// </summary>
    public static class SqlDatabaseUtility
    {
        private const string DEFINITION_VERSION = "1.00";
        private const string PROTOCOL_DEFINITION_VERSION = "1.00";

        #region Metodi pubblici

        #region UpdateDeviceStatus - Aggiorna lo stato della periferica

        /// <summary>
        ///     Aggiorna lo stato della periferica
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="severityLevel">Livello di severità del Device</param>
        /// <param name="descriptionStatus">Stato del Device</param>
        /// <param name="offline">Indica se il Device è fuori linea o non raggiungibile</param>
        public static void UpdateDeviceStatus(string dbConnectionString, long deviceId, int severityLevel, string descriptionStatus, byte offline)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "IF EXISTS (SELECT [DevID] FROM [device_status] WHERE [DevID] = @DevID) BEGIN UPDATE [device_status] SET [SevLevel] = @SevLevel, [Description] = @Description, [Offline] = @Offline WHERE [DevID] = @DevID; END ELSE BEGIN INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline]) VALUES (@DevID, @SevLevel, @Description, @Offline); END";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, severityLevel));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, descriptionStatus));
                cmd.Parameters.Add(new SqlParameter("@Offline", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null, DataRowVersion.Current, offline));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        #endregion

        #region UpdateDeviceType - Aggiorna il tipo della periferica

        /// <summary>
        ///     Aggiorna il tipo della periferica
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="newDeviceType">Nuovo tipo del Device</param>
        public static void UpdateDeviceType(string dbConnectionString, long deviceId, string newDeviceType)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = Settings.Default.IsOnClientDatabase ? "UPDATE devices SET DiscoveredType = @DiscoveredType WHERE (DevID = @DevID)" : "UPDATE devices SET Type = @DiscoveredType WHERE (DevID = @DevID)";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@DiscoveredType", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, newDeviceType));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        #endregion

        #region UpdateDeviceSerialNumber - Aggiorna il numero di serie della periferica

        /// <summary>
        ///     Aggiorna il numero di serie della periferica
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="newDeviceSerialNumber">Nuovo numero di serie del Device</param>
        public static void UpdateDeviceSerialNumber(string dbConnectionString, long deviceId, string newDeviceSerialNumber)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "UPDATE devices SET SN = @SN WHERE (DevID = @DevID)";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, newDeviceSerialNumber));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        #endregion

        #region UpdateStream - Aggiorna lo Stream

        /// <summary>
        ///     Aggiorna lo Stream
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="streamId">Id dello Stream</param>
        /// <param name="name">Nome dello Stream</param>
        /// <param name="severityLevel">Livello di severità dello Stream</param>
        /// <param name="description">Descrizione del valore dello Stream</param>
        public static void UpdateStream(string dbConnectionString, long deviceId, int streamId, string name, int severityLevel, string description, DateTime dtValue)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                /*
                cmd.CommandText = "IF EXISTS (SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID)) BEGIN UPDATE [streams] SET [Name] = @Name, [Data] = NULL, [DateTime] = GetDate(), [SevLevel] = @SevLevel, [Description] = @Description WHERE (([DevID] = @DevID) AND ([StrID] = @StrID)); END ELSE BEGIN INSERT INTO [streams] ([DevID], [StrID], [Name], [Data], [DateTime], [SevLevel], [Description], [Visible]) VALUES (@DevID, @StrID, @Name, NULL, GetDate(), @SevLevel, @Description, 1) END";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, streamId));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, name));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, severityLevel));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, description));
                */

                cmd.CommandText = "IF EXISTS (SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID)) BEGIN UPDATE [streams] SET [Name] = @Name, [Data] = NULL, [DateTime] = @DateTime, [SevLevel] = @SevLevel, [Description] = @Description WHERE (([DevID] = @DevID) AND ([StrID] = @StrID)); END ELSE BEGIN INSERT INTO [streams] ([DevID], [StrID], [Name], [Data], [DateTime], [SevLevel], [Description], [Visible]) VALUES (@DevID, @StrID, @Name, NULL, GetDate(), @SevLevel, @Description, 1) END";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, streamId));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, name));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, severityLevel));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, description));

                SqlParameter paramDTime  = new SqlParameter("@DateTime", SqlDbType.DateTime);
                paramDTime.Value         = dtValue;

                cmd.Parameters.Add(paramDTime);


                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        #endregion

        #region UpdateStreamField - Aggiorna il Field

        /// <summary>
        ///     Aggiorna il Field
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="streamId">Id dello Stream</param>
        /// <param name="fieldId">Id del Field</param>
        /// <param name="arrayId">Id del vettore dei valori del Field</param>
        /// <param name="name">Nome del Field</param>
        /// <param name="severityLevel">Livello di severità del Field</param>
        /// <param name="value">Valore del Field</param>
        /// <param name="description">Descrizione del valore del Field</param>
        public static void UpdateStreamField(string dbConnectionString, long deviceId, int streamId, int fieldId, int arrayId, string name, int severityLevel, string value, string description)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "IF EXISTS (SELECT FieldID FROM stream_fields WHERE (DevID = @DevID) AND (StrID = @StrID) AND (FieldID = @FieldID) AND (ArrayID = @ArrayID)) BEGIN UPDATE [stream_fields] SET [Name] = @Name, [SevLevel] = @SevLevel, [Value] = @Value, [Description] = @Description WHERE (([DevID] = @DevID) AND ([StrID] = @StrID) AND ([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID)); END ELSE BEGIN INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], ReferenceID) VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, 1, NULL); END";
                cmd.Parameters.Add(new SqlParameter("@FieldID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, fieldId));
                cmd.Parameters.Add(new SqlParameter("@ArrayID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, arrayId));
                cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, streamId));
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, name));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, severityLevel));
                cmd.Parameters.Add(new SqlParameter("@Value", SqlDbType.VarChar, 1024, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, value));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, description));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        #endregion

        #region CheckDeviceExistence - Verifica l'esistenza di una periferica in base dati

        /// <summary>
        ///     Verifica l'esistenza di una periferica in base dati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <returns>Ritorna null nel caso ci sia un errore nell'accesso alla base dati o nell'esecuzione del comando, true se la periferica esiste, false altrimenti</returns>
        public static bool? CheckDeviceExistence(string dbConnectionString, long deviceId)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDevices = null;
            bool? deviceExists;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT DevID FROM [devices] WHERE (DevID = @DevID)";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevices = cmd.ExecuteReader();

                if (drDevices.Read())
                {
                    int colDevID = drDevices.GetOrdinal("DevID");

                    do
                    {
                        if (drDevices.IsDBNull(colDevID))
                        {
                            deviceExists = false;
                        }
                        else
                        {
                            long deviceID = drDevices.GetSqlInt64(colDevID).Value;

                            deviceExists = deviceID > 0;
                        }
                    } while (drDevices.Read());
                }
                else
                {
                    deviceExists = false;
                }
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
                deviceExists = null;
            }
            catch (IndexOutOfRangeException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando CheckDeviceExistence. " + ex.Message);
                deviceExists = null;
            }
            catch (ThreadAbortException)
            {
                deviceExists = null;
            }
            finally
            {
                if (drDevices != null && !drDevices.IsClosed)
                {
                    drDevices.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deviceExists;
        }

        #endregion

        #region GetStationBuildingRackPortData - Recupera i dati di lookup per poter creare una periferica su database

        /// <summary>
        ///     Recupera i dati di lookup per poter creare una periferica su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="location">Dati della topografia della periferica</param>
        /// <param name="serverId">ID del Server da System.xml</param>
        /// <param name="portId">ID dell'interfaccia da System.xml</param>
        /// <returns>Dati di lookup con l'Id di Stazione, Fabbricato, Armadio e Interfaccia</returns>
        public static DeviceDatabaseSupport GetStationBuildingRackPortData(string dbConnectionString, TopographyLocation location, int serverId, int portId)
        {
            DeviceDatabaseSupport deviceDatabaseSupport = null;

            if (location != null)
            {
                SqlConnection dbConnection = null;
                SqlCommand cmd = new SqlCommand();
                SqlDataReader drDeviceDatabaseSupport = null;

                try
                {
                    try
                    {
                        dbConnection = new SqlConnection(dbConnectionString);
                    }
                    catch (ArgumentException ex)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                        return null;
                    }

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                    cmd.CommandText = "DECLARE @StationID uniqueidentifier; DECLARE @BuildingID uniqueidentifier; DECLARE @RackID uniqueidentifier; DECLARE @PortID uniqueidentifier; SELECT TOP (1) @StationID = station.StationID, @BuildingID = building.BuildingID, @RackID = rack.RackID FROM station INNER JOIN building ON station.StationID = building.StationID INNER JOIN rack ON building.BuildingID = rack.BuildingID WHERE (station.StationName = @StationName) AND (building.BuildingName = @BuildingName) AND (building.BuildingXMLID = @BuildingXMLID) AND (building.BuildingDescription = @BuildingDescription) AND (rack.RackName = @RackName) AND (rack.RackXMLID = @RackXMLID) AND (rack.RackType = @RackType) AND (rack.RackDescription = @RackDescription) AND (building.Removed = 0) AND (rack.Removed = 0) AND (station.Removed = 0); SELECT TOP (1) @PortID = port.PortID FROM port WHERE (SrvID = @SrvID) AND (PortXMLID = @PortXMLID) SELECT @StationID AS StationID, @BuildingID AS BuildingID, @RackID AS RackID, @PortID AS PortID";
                    cmd.Parameters.Add(new SqlParameter("@StationName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.StationName));
                    cmd.Parameters.Add(new SqlParameter("@BuildingName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.BuildingName));
                    cmd.Parameters.Add(new SqlParameter("@BuildingXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, location.BuildingId));
                    cmd.Parameters.Add(new SqlParameter("@RackName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.LocationName));
                    cmd.Parameters.Add(new SqlParameter("@RackXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, location.LocationId));
                    cmd.Parameters.Add(new SqlParameter("@RackType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.LocationType));
                    cmd.Parameters.Add(new SqlParameter("@RackDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.LocationNotes));
                    cmd.Parameters.Add(new SqlParameter("@BuildingDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.BuildingNotes));
                    cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, serverId));
                    cmd.Parameters.Add(new SqlParameter("@PortXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, portId));

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    drDeviceDatabaseSupport = cmd.ExecuteReader();

                    if (drDeviceDatabaseSupport.Read())
                    {
                        int colStationId = drDeviceDatabaseSupport.GetOrdinal("StationID");
                        int colBuildingId = drDeviceDatabaseSupport.GetOrdinal("BuildingID");
                        int colRackId = drDeviceDatabaseSupport.GetOrdinal("RackID");
                        int colPortId = drDeviceDatabaseSupport.GetOrdinal("PortID");

                        do
                        {
                            if ((!drDeviceDatabaseSupport.IsDBNull(colStationId)) && (!drDeviceDatabaseSupport.IsDBNull(colBuildingId)) && (!drDeviceDatabaseSupport.IsDBNull(colRackId)) && (!drDeviceDatabaseSupport.IsDBNull(colPortId)))
                            {
                                deviceDatabaseSupport = new DeviceDatabaseSupport(drDeviceDatabaseSupport.GetSqlGuid(colStationId).Value, drDeviceDatabaseSupport.GetSqlGuid(colBuildingId).Value, drDeviceDatabaseSupport.GetSqlGuid(colRackId).Value, drDeviceDatabaseSupport.GetSqlGuid(colPortId).Value);
                            }
                        } while (drDeviceDatabaseSupport.Read());
                    }
                }
                catch (SqlException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
                }
                catch (IndexOutOfRangeException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando GetStationBuildingRackPortData. " + ex.Message);
                }
                finally
                {
                    if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                    {
                        drDeviceDatabaseSupport.Close();
                    }

                    if (dbConnection != null)
                    {
                        dbConnection.Close();
                    }

                    cmd.Dispose();
                }
            }

            return deviceDatabaseSupport;
        }

        #endregion

        #region UpdateDevice - Aggiorna o inserisce i dati della periferica su database

        /// <summary>
        ///     Aggiorna o inserisce i dati della periferica su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <param name="nodeId">Id del Nodo</param>
        /// <param name="serverId">Id del Server</param>
        /// <param name="deviceName">Nome della periferica</param>
        /// <param name="deviceType">Tipo della periferica</param>
        /// <param name="serialNumber">Numero di serie della periferica</param>
        /// <param name="deviceAddress">Indirizzo IP della periferica</param>
        /// <param name="portId">Id dell'interfaccia della periferica</param>
        /// <param name="profileId">Id del profilo della periferica</param>
        /// <param name="active">Stato della periferica</param>
        /// <param name="scheduled">Stato di schedulazione della periferica</param>
        /// <param name="removed">Stato di rimozione della periferica</param>
        /// <param name="rackId">Id dell'Armadio</param>
        /// <param name="rackPositionRow">Indice della posizione di riga nell'Armadio</param>
        /// <param name="rackPositionCol">Indice della posizione di colonna nell'Armadio</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool UpdateDevice(string dbConnectionString, long deviceId, long nodeId, int serverId, string deviceName, string deviceType, string serialNumber, IPAddress deviceAddress, Guid portId, int profileId, Byte active, Byte scheduled, Byte removed, Guid rackId, int rackPositionRow, int rackPositionCol)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDevice = null;
            bool writeOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.CommandText = Settings.Default.IsOnClientDatabase ? "IF NOT EXISTS (SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID)) BEGIN INSERT INTO [devices] ([DevID], [NodID], [SrvID], [Name], [Type], [SN], [Addr], [PortId], [ProfileID], [Active], [Scheduled], [Removed], [RackID], [RackPositionRow], [RackPositionCol], [DefinitionVersion], [ProtocolDefinitionVersion], [DiscoveredType]) VALUES (@DevID, @NodID, @SrvID, @Name, @Type, @SN, @Addr, @PortId, @ProfileID, @Active, @Scheduled, @Removed, @RackID, @RackPositionRow, @RackPositionCol, @DefinitionVersion, @ProtocolDefinitionVersion, NULL) END ELSE BEGIN UPDATE [devices] SET [NodID] = @NodID, [SrvID] = @SrvID, [Name] = @Name, [Type] = @Type, [SN] = @SN, [Addr] = @Addr, [PortId] = @PortId, [ProfileID] = @ProfileID, [Active] = @Active, [Scheduled] = @Scheduled, [Removed] = @Removed, [RackID] = @RackID, [RackPositionRow] = @RackPositionRow, [RackPositionCol] = @RackPositionCol, [DiscoveredType] = NULL WHERE ([DevID] = @DevID) END; SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID);" : "IF NOT EXISTS (SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID)) BEGIN INSERT INTO [devices] ([DevID], [NodID], [SrvID], [Name], [Type], [SN], [Addr], [PortId], [ProfileID], [Active], [Scheduled], [RackID], [RackPositionRow], [RackPositionCol], [DefinitionVersion], [ProtocolDefinitionVersion]) VALUES (@DevID, @NodID, @SrvID, @Name, @Type, @SN, @Addr, @PortId, @ProfileID, @Active, @Scheduled, @RackID, @RackPositionRow, @RackPositionCol, @DefinitionVersion, @ProtocolDefinitionVersion) END ELSE BEGIN UPDATE [devices] SET [NodID] = @NodID, [SrvID] = @SrvID, [Name] = @Name, [Type] = @Type, [SN] = @SN, [Addr] = @Addr, [PortId] = @PortId, [ProfileID] = @ProfileID, [Active] = @Active, [Scheduled] = @Scheduled, [RackID] = @RackID, [RackPositionRow] = @RackPositionRow, [RackPositionCol] = @RackPositionCol WHERE ([DevID] = @DevID) END; SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID);";

                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@NodID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, nodeId));
                cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, serverId));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, deviceName));
                cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, deviceType));
                cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, serialNumber));
                cmd.Parameters.Add(new SqlParameter("@Addr", SqlDbType.VarChar, 32, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, DataUtility.EncodeIPAddress(deviceAddress)));
                cmd.Parameters.Add(new SqlParameter("@PortId", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, portId));
                cmd.Parameters.Add(new SqlParameter("@ProfileID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, profileId));
                cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null, DataRowVersion.Current, active));
                cmd.Parameters.Add(new SqlParameter("@Scheduled", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null, DataRowVersion.Current, scheduled));
                cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null, DataRowVersion.Current, removed));
                cmd.Parameters.Add(new SqlParameter("@RackID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, rackId));
                cmd.Parameters.Add(new SqlParameter("@RackPositionRow", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, rackPositionRow));
                cmd.Parameters.Add(new SqlParameter("@RackPositionCol", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, rackPositionCol));
                cmd.Parameters.Add(new SqlParameter("@DefinitionVersion", SqlDbType.VarChar, 8, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, DEFINITION_VERSION));
                cmd.Parameters.Add(new SqlParameter("@ProtocolDefinitionVersion", SqlDbType.VarChar, 8, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, PROTOCOL_DEFINITION_VERSION));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevice = cmd.ExecuteReader();

                if (drDevice.Read())
                {
                    int colDevID = drDevice.GetOrdinal("DevID");

                    do
                    {
                        if ((!drDevice.IsDBNull(colDevID)) && (drDevice.GetSqlInt64(colDevID).Value == deviceId))
                        {
                            writeOperationSuccessful = true;
                        }
                    } while (drDevice.Read());
                }
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (IndexOutOfRangeException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando UpdateDevice. " + ex.Message);
            }
            finally
            {
                if (drDevice != null && !drDevice.IsClosed)
                {
                    drDevice.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return writeOperationSuccessful;
        }

        #endregion

        #region DeleteDevice - Elimina i dati della periferica su database

        /// <summary>
        ///     Elimina i dati della periferica su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool DeleteDevice(string dbConnectionString, long deviceId)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            bool deleteOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.CommandText = Settings.Default.IsOnClientDatabase ? "DELETE FROM [stream_fields] WHERE (DevID = @DevID); DELETE FROM [streams] WHERE (DevID = @DevID); DELETE FROM [events] WHERE (DevID = @DevID); DELETE FROM [procedures] WHERE (DevID = @DevID); DELETE FROM [device_status] WHERE (DevID = @DevID); DELETE FROM [devices] WHERE (DevID = @DevID);" : "DELETE FROM [stream_fields] WHERE (DevID = @DevID); DELETE FROM [streams] WHERE (DevID = @DevID); DELETE FROM [procedures] WHERE (DevID = @DevID); DELETE FROM [device_status] WHERE (DevID = @DevID); DELETE FROM [devices] WHERE (DevID = @DevID);";

                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, deviceId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();

                deleteOperationSuccessful = true;
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deleteOperationSuccessful;
        }

        #endregion

        #region GetDeviceAcknowledgementsList - Recupera i dati delle tacitazioni relative al supervisore corrente

        /// <summary>
        ///     Recupera i dati delle tacitazioni relative al supervisore corrente
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="snmpSupervisorID">Id del supervisore corrente</param>
        /// <returns>Lista di tacitazioni</returns>
        public static ReadOnlyCollection<DbDeviceAcknowledgement> GetDeviceAcknowledgementsList(string dbConnectionString, int snmpSupervisorID)
        {
            List<DbDeviceAcknowledgement> deviceAcknowledgementsList = new List<DbDeviceAcknowledgement>();

            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username FROM device_ack WHERE (SupervisorID = @SupervisorID)";
                cmd.Parameters.Add(new SqlParameter("@SupervisorID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, snmpSupervisorID));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int colDeviceAckID          = drDeviceDatabaseSupport.GetOrdinal("DeviceAckID");
                    int colDevID                = drDeviceDatabaseSupport.GetOrdinal("DevID");
                    int colStrID                = drDeviceDatabaseSupport.GetOrdinal("StrID");
                    int colFieldID              = drDeviceDatabaseSupport.GetOrdinal("FieldID");
                    int colAckDate              = drDeviceDatabaseSupport.GetOrdinal("AckDate");
                    int colAckDurationMinutes   = drDeviceDatabaseSupport.GetOrdinal("AckDurationMinutes");
                    int colSupervisorID         = drDeviceDatabaseSupport.GetOrdinal("SupervisorID");
                    int colUsername             = drDeviceDatabaseSupport.GetOrdinal("Username");

                    do
                    {
                        if (!drDeviceDatabaseSupport.IsDBNull(colDeviceAckID))
                        {
                            deviceAcknowledgementsList.Add(new DbDeviceAcknowledgement(drDeviceDatabaseSupport.GetSqlGuid(colDeviceAckID).Value, drDeviceDatabaseSupport.GetSqlInt64(colDevID).Value, drDeviceDatabaseSupport.IsDBNull(colStrID) ? (int?) null : drDeviceDatabaseSupport.GetSqlInt32(colStrID).Value, drDeviceDatabaseSupport.IsDBNull(colFieldID) ? (int?) null : drDeviceDatabaseSupport.GetSqlInt32(colFieldID).Value, drDeviceDatabaseSupport.GetSqlDateTime(colAckDate).Value, drDeviceDatabaseSupport.GetSqlInt32(colAckDurationMinutes).Value, drDeviceDatabaseSupport.GetSqlByte(colSupervisorID).Value, drDeviceDatabaseSupport.GetSqlString(colUsername).Value));
                        }
                    } while (drDeviceDatabaseSupport.Read());
                }
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (IndexOutOfRangeException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando GetDeviceAcknowledgementsList. " + ex.Message);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deviceAcknowledgementsList.AsReadOnly();
        }

        #endregion

        #region DeleteExpiredAcknowledgements - Elimina le tacitazioni scadute (rispetto al momento corrente) e al supervisore in esecuzione

        /// <summary>
        ///     Elimina le tacitazioni scadute (rispetto al momento corrente) e al supervisore in esecuzione
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="snmpSupervisorID">Id del supervisore corrente</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool DeleteExpiredAcknowledgements(string dbConnectionString, int snmpSupervisorID)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            bool deleteOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "DELETE FROM device_ack WHERE (DATEADD(minute, AckDurationMinutes, AckDate) <= GetDate()) AND (SupervisorID = @SupervisorID);";
                cmd.Parameters.Add(new SqlParameter("@SupervisorID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, snmpSupervisorID));
                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();

                deleteOperationSuccessful = true;
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deleteOperationSuccessful;
        }

        #endregion

        #endregion
    }
}
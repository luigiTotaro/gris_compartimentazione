﻿namespace GrisSuite.MODBUSSupervisorService.Database
{
    public class DeviceStreamAcknowledgement
    {
        /// <summary>
        /// Stream Id
        /// </summary>
        public int StreamId { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="streamId">Stream Id</param>
        public DeviceStreamAcknowledgement(int streamId)
        {
            this.StreamId = streamId;
        }
    }
}
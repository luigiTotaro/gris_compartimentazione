﻿
namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///   Indica le informazioni per la valorizzazione dello StreamField quando non sono restituiti dati nelle query
    /// </summary>
    public class NoDataAvailableForcedState
    {
        /// <summary>
        ///   Severità forzata per lo StreamField
        /// </summary>
        public Severity ForcedSeverity { get; private set; }

        /// <summary>
        ///   Descrizione del valore forzato per lo StreamField
        /// </summary>
        public string ForcedValueDescription { get; private set; }

        /// <summary>
        ///   Valore forzato per lo StreamField
        /// </summary>
        public string ForcedValue { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "forcedSeverity">Severità forzata per lo StreamField</param>
        /// <param name = "valueDescription">Descrizione del valore forzato per lo StreamField</param>
        /// <param name = "value">Valore forzato per lo StreamField</param>
        public NoDataAvailableForcedState(Severity forcedSeverity, string valueDescription, string value)
        {
            this.ForcedSeverity         = forcedSeverity;
            this.ForcedValueDescription = valueDescription;
            this.ForcedValue            = value;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text.RegularExpressions;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///   Rappresenta il singolo Field da persistere su database
    /// </summary>
    public class DBStreamField
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="fieldId">Id del Field</param>
        /// <param name="fieldName">Nome del Field</param>
        /// <param name="wOffset"></param>
        /// <param name="wCount"></param>
        /// <param name="excludedFromSeverityComputation">Indica se lo stream field e la sua severity devono essere tenuti in considerazione nel calcolo della severità dello stream padre</param>
        /// <param name="noDataAvailableForcedState">Informazioni per la valorizzazione dello Stream nel caso che non siano restituiti dati nelle richieste modbus</param>
        /// <param name="hideIfNoDataAvailable">Indica se lo stream sarà mantenuto su database quando la richiesta allo slave modbus non ritorna un valore</param>
        public DBStreamField(int fieldId, byte byFunCode, string fieldName, ushort wOffset, ushort wCount, int ArrayId, bool excludedFromSeverityComputation, NoDataAvailableForcedState noDataAvailableForcedState, bool hideIfNoDataAvailable, FormatExpression formatExpression)
        {
            this.FieldId                            = fieldId;
            this.Name                               = fieldName;
            this.byFunCode                          = byFunCode;
            this.wOffset                            = wOffset;
            this.wCount                             = wCount;
            this.ArrayId                            = ArrayId;
            this.ValueFormatExpression              = formatExpression;
            this.PersistOnDatabase                  = true;
            this.ExcludedFromSeverityComputation    = excludedFromSeverityComputation;
            this.NoModbusDataAvailableForcedState   = noDataAvailableForcedState;
            this.HideIfNoDataAvailable              = hideIfNoDataAvailable;
            this.DbStreamFieldValueDescriptions     = new Collection<DBStreamFieldValueDescription>();
            this.exceptionCode                      = MsgObject.ExceptionCode.None;
        }

        #region Proprietà pubbliche

        /// <summary>
        ///   Severità relativa al Field
        /// </summary>
        public Severity SeverityLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Indica il tipo di richiesta da inviare allo slave
        /// </summary>
        public byte byFunCode { get; private set; }

        /// <summary>
        ///   Decodifica descrittiva del valore del Field
        /// </summary>
        public string ValueDescription
        {
            private get;
            set;
        }

        /// <summary>
        ///   Decodifica descrittiva del valore del Field, concatenata alla severità (solo nel caso non sia sconosciuta)
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                if (this.SeverityLevel == Severity.Unknown)
                {
                    return this.ValueDescription;
                }

                return this.ValueDescription + "=" + (int) this.SeverityLevel;
            }
        }

        /// <summary>
        ///   Id del Field
        /// </summary>
        public int FieldId
        {
            get;
            private set;
        }

        /// <summary>
        ///   Valore del Field, come letto dallo slave modbus
        /// </summary>
        public string Value
        {
            get;
            set;
        }

        /// <summary>
        ///   Lista di valori
        /// </summary>
        public IList<UInt16> RawData
        {
            private get;
            set;
        }

        /// <summary>
        ///   Posizione del valore nel vettore
        /// </summary>
        public int ArrayId
        {
            get;
            private set;
        }

        /// <summary>
        /// Offset da cui considerare il valore
        /// </summary>
        public ushort wOffset
        {
            get;
            private set;
        }

        /// <summary>
        /// Numero di coil/registri che compongono il valore
        /// </summary>
        public ushort wCount
        {
            get;
            private set;
        }

        /// <summary>
        ///   Nome del Field
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        ///   Lista di descrizioni per la decodifica del valore e della severità
        /// </summary>
        public Collection<DBStreamFieldValueDescription> DbStreamFieldValueDescriptions
        {
            get;
            private set;
        }

        /// <summary>
        ///   Espressione di formattazione da applicare al valore
        /// </summary>
        public FormatExpression ValueFormatExpression
        {
            get;
            private set;
        }

        /// <summary>
        ///   Indica se lo stream field deve essere salvato sul database
        /// </summary>
        /// <remarks>
        ///   E' possibile che uno stream field di una tabella dinamica non abbia senso,
        ///   a fronte di una valutazione del contatore che riguarda le righe della tabella
        /// </remarks>
        public bool PersistOnDatabase
        {
            get;
            set;
        }

        /// <summary>
        ///   Indica se lo stream field e la sua severity devono essere tenuti in considerazione nel calcolo della severità dello stream padre
        /// </summary>
        public bool ExcludedFromSeverityComputation
        {
            get;
            set;
        }

        /// <summary>
        ///   Informazioni per la valorizzazione dello Stream nel caso che non siano restituiti dati nelle richieste modbus
        /// </summary>
        public NoDataAvailableForcedState NoModbusDataAvailableForcedState
        {
            get;
            private set;
        }

        /// <summary>
        ///   Contiene l'eventuale eccezione generata durante il popolamento con dati del field
        /// </summary>
        public Exception LastError
        {
            get;
            set;
        }

        /// <summary>
        /// Specifica se nell'ultima comunicazione lo slave modbus ha comunicato un'anomalia 
        /// di funzionamento
        /// </summary>
        public MsgObject.ExceptionCode exceptionCode
        {
            get;
            set;
        }

        /// <summary>
        ///   Indica se lo stream sarà persistito su database quando la query SNMP non ritorna un valore
        /// </summary>
        public bool HideIfNoDataAvailable
        {
            get;
            private set;
        }

        #endregion

        private enum EvaluationNeeded
        {
            Unknown,
            StringComparison, // non gestito
            BoolComparison,
            BitComparison,
            IntComparison,
            RangeComparison,
            RegexMatch        // non gestito
        }

        private EvaluationNeeded ChooseEvaluationMethod()
        {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
            {
                if (this.DbStreamFieldValueDescriptions.Count == 1)
                {
                    evaluationNeeded = GetEvaluationNeeded(this.DbStreamFieldValueDescriptions[0]);
                }
                else
                {
                    foreach (DBStreamFieldValueDescription dbStreamFieldValueDescription in
                        this.DbStreamFieldValueDescriptions)
                    {
                        if (!dbStreamFieldValueDescription.IsDefault)
                        {
                            // Consideriamo solo il primo tipo - casi di tipi di valori etereogenei non sono supportati
                            evaluationNeeded = GetEvaluationNeeded(dbStreamFieldValueDescription);
                            break;
                        }
                    }
                }
            }

            return evaluationNeeded;
        }

        private static EvaluationNeeded GetEvaluationNeeded(DBStreamFieldValueDescription dbStreamFieldValueDescription)
        {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;
            
            if ((dbStreamFieldValueDescription.RangeValueToCompare != null) &&
                ((dbStreamFieldValueDescription.RangeValueToCompare.MinValue.HasValue) ||
                 (dbStreamFieldValueDescription.RangeValueToCompare.MaxValue.HasValue)))
            {
                evaluationNeeded = EvaluationNeeded.RangeComparison;
            }
            else if (dbStreamFieldValueDescription.ExactIntValueToCompare.HasValue)
            {
                evaluationNeeded = EvaluationNeeded.IntComparison;
            }
            else if (dbStreamFieldValueDescription.ExactBoolValueToCompare.HasValue)
            {
                evaluationNeeded = EvaluationNeeded.BoolComparison;
            }
            else if (dbStreamFieldValueDescription.MaskBitToCompare.HasValue)
            {
                evaluationNeeded = EvaluationNeeded.BitComparison;
            }
            else if (!string.IsNullOrEmpty(dbStreamFieldValueDescription.ExactStringValueToCompare))
            {
                evaluationNeeded = EvaluationNeeded.StringComparison;
            }
            
            return evaluationNeeded;
        }

        /// <summary>
        /// Esegue la valutazione del valore contenuto nel campo, in base alle regole configurate
        /// </summary>
        /// <param name="isReachable">specifica se lo slave modbus è pingabile</param>
        public void Evaluate(bool isReachable)
        {
            if (this.DbStreamFieldValueDescriptions == null)
                return;
            if (this.DbStreamFieldValueDescriptions.Count <= 0)
                return;

            if (this.DbStreamFieldValueDescriptions.Count == 1)
            {
                this.ParseDefaultSingleValue(this.DbStreamFieldValueDescriptions[0].DescriptionIfMatch(String.Empty),
                                             this.DbStreamFieldValueDescriptions[0].SeverityLevelIfMatch,
                                             isReachable);
            }
            else
            {
                switch (this.ChooseEvaluationMethod())
                {
                    case EvaluationNeeded.Unknown:
                        this.ParseStringValue   (isReachable);
                        break;
                    case EvaluationNeeded.IntComparison:
                        this.ParseLongValue     (isReachable);
                        break;
                    case EvaluationNeeded.BoolComparison:
                        this.ParseBoolValue     (isReachable);
                        break;
                    case EvaluationNeeded.RangeComparison:
                        this.ParseRangeValue    (isReachable);
                        break;
                    case EvaluationNeeded.BitComparison:
                        this.ParseMaskBitValue  (isReachable);
                        break;
                }
            }
        }

        #region Metodi privati per l'elaborazione dei dati relativi ai vari Fields degli Stream, con decodifica di descrizione e severità

        /// <summary>
        /// Elabora i dati di tipo stringa relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isReachable">Specifica se lo slave è raggiungibile al ping</param>
        private void ParseStringValue(bool isReachable)
        {
            string valueString;

            if (!this.ExistValidData(isReachable))
                return;

            valueString = this.RawData[this.ArrayId].ToString();

            if (valueString == null)
            {
                this.SetFieldStateToUnknown(isReachable);
            }
            else
            {
                DBStreamFieldValueDescription   defaultDescription  = null;
                bool                            fieldValueSet       = false;

                foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                {
                    if (String.Equals(valueString, description.ExactStringValueToCompare, StringComparison.OrdinalIgnoreCase))
                    {
                        this.SetFieldDataString(description.DescriptionIfMatch(valueString), valueString, description.SeverityLevelIfMatch);
                        fieldValueSet = true;
                        break;
                    }

                    if (description.IsDefault)
                    {
                        // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                        defaultDescription = description;
                    }
                }

                if (!fieldValueSet)
                {
                    if (defaultDescription != null)
                    {
                        this.SetFieldDataString(defaultDescription.DescriptionIfMatch(valueString), valueString,
                                                defaultDescription.SeverityLevelIfMatch);
                    }
                    else
                    {
                        this.SetFieldStateToUnknown(isReachable);
                    }
                }
            }
        }

        /// <summary>
        /// Elabora i dati di tipo intervallo relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isReachable">Specifica se lo slave è raggiungibile al ping</param>
        private void ParseRangeValue(bool isReachable)
        {
            if (!this.ExistValidData(isReachable))
                return;

            long value = (long)this.RawData[this.ArrayId];

            bool                            fieldValueSet       = false;
            string                          valueString         = value.ToString();
            DBStreamFieldValueDescription   defaultDescription  = null;

            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
            {
                if ((value >= description.RangeValueToCompare.MinValue.Value) && (value <= description.RangeValueToCompare.MaxValue.Value))
                {
                    this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value, description.SeverityLevelIfMatch);
                    
                    fieldValueSet = true;
                    break;
                }

                if (description.IsDefault) {
                    
                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                    defaultDescription = description;
                }
            }

            if (!fieldValueSet)
            {
                if (defaultDescription != null)
                {
                    this.SetFieldDataLong(defaultDescription.DescriptionIfMatch(value.ToString()), value,  defaultDescription.SeverityLevelIfMatch);
                }
                else
                {
                    this.SetFieldStateToUnknown(isReachable);
                }
            }
        }

        /// <summary>
        /// Elabora i dati di tipo intero relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isReachable">Specifica se lo slave è raggiungibile al ping</param>
        private void ParseLongValue(bool isReachable)
        {
            if (!this.ExistValidData(isReachable))
                return;

            long value = (long)this.RawData[this.ArrayId];

            bool                            fieldValueSet       = false;
            string                          valueString         = value.ToString();
            DBStreamFieldValueDescription   defaultDescription  = null;

            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
            {
                if (value == description.ExactIntValueToCompare)
                {
                    this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value, description.SeverityLevelIfMatch);
                    fieldValueSet = true;
                    break;
                }

                if (description.IsDefault)
                {
                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                    defaultDescription = description;
                }
            }

            if (!fieldValueSet)
            {
                if (defaultDescription != null)
                {
                    this.SetFieldDataLong(defaultDescription.DescriptionIfMatch(value.ToString()), value, defaultDescription.SeverityLevelIfMatch);
                }
                else
                {
                    this.SetFieldStateToUnknown(isReachable);
                }
            }
        }

        /// <summary>
        /// Elabora i dati di tipo intero relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isReachable">Specifica se lo slave è raggiungibile al ping</param>
        private void ParseMaskBitValue(bool isReachable)
        {
            if (!this.ExistValidData(isReachable))
                return;

            long value = (long)this.RawData[this.ArrayId];

            bool fieldValueSet = false;
            string valueString = value.ToString();
            DBStreamFieldValueDescription defaultDescription = null;

            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
            {
                if (description.MaskBitToCompare.HasValue)
                {
                    if (Convert.ToBoolean(value & description.MaskBitToCompare.Value))
                    {
                        this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value, description.SeverityLevelIfMatch);
                        fieldValueSet = true;
                        break;
                    }
                }

                if (description.IsDefault)
                {
                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                    defaultDescription = description;
                }
            }

            if (!fieldValueSet)
            {
                if (defaultDescription != null)
                {
                    this.SetFieldDataLong(defaultDescription.DescriptionIfMatch(value.ToString()), value, defaultDescription.SeverityLevelIfMatch);
                }
                else
                {
                    this.SetFieldStateToUnknown(isReachable);
                }
            }
        }

        /// <summary>
        /// Elabora i dati di tipo booleano relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isReachable">Specifica se lo slave è raggiungibile al ping</param>
        private void ParseBoolValue(bool isReachable)
        {
            if (!this.ExistValidData(isReachable))
                return;

            bool value;

            if (this.RawData[this.ArrayId] != 0)
                value = true;
            else
                value = false;

            bool                            fieldValueSet       = false;
            string                          valueString         = value.ToString();
            DBStreamFieldValueDescription   defaultDescription  = null;

            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
            {
                if (value == description.ExactBoolValueToCompare)
                {
                    this.SetFieldDataBoolean(description.DescriptionIfMatch(value.ToString()), value, description.SeverityLevelIfMatch);
                    fieldValueSet = true;
                    break;
                }

                if (description.IsDefault)
                {
                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                    defaultDescription = description;
                }
            }

            if (!fieldValueSet)
            {
                if (defaultDescription != null)
                {
                    this.SetFieldDataBoolean(defaultDescription.DescriptionIfMatch(value.ToString()), value, defaultDescription.SeverityLevelIfMatch);
                }
                else
                {
                    this.SetFieldStateToUnknown(isReachable);
                }
            }
        }

        /// <summary>
        ///   Elabora un dato unico relativo al Field degli Streams
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore passato</param>
        /// <param name="severity">Severità da attribuire allo stream field</param>
        /// <param name="isReachable">Indica se la periferica è raggiungibile ping</param>
        private void ParseDefaultSingleValue(string valueDescription, Severity severity, bool isReachable)
        {
            string valueString;

            if (!this.ExistValidData(isReachable))
                return;
            
            valueString = this.RawData[this.ArrayId].ToString();

            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.Long)
                {
                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown(isReachable);
                    }
                    else
                    {
                        long valueLong;
                        if (long.TryParse(valueString, out valueLong))
                        {
                            this.SetFieldDataInternal(valueDescription, (valueLong * this.ValueFormatExpression.Factor + this.ValueFormatExpression.Addend).ToString(this.ValueFormatExpression.Format), severity);
                        }
                        else
                        {
                            this.SetFieldDataInternal(valueDescription, valueString + this.ValueFormatExpression.Format, severity);
                        }
                    }
                }
                else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, valueString + this.ValueFormatExpression.Format, severity);
                }
            }
            else
            {
                this.SetFieldDataString(valueDescription, valueString, severity);
            }
        }

        /// <summary>
        /// Verifica se vi sono dati disponibili
        /// </summary>
        /// <returns>true : esistono dati per l'arrayid configurato</returns>
        private bool ExistValidData(bool isReachable)
        {
            //if (!isReachable) {
            //    this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue, Severity.Unknown);
            //    return false;
            //}

            if (this.RawData == null)
            {
                this.SetFieldStateToUnknown(isReachable);
                return false;
            }
            if (this.RawData.Count <= 0)
            {
                this.SetFieldStateToUnknown(isReachable);
                return false;
            }
            if (this.ArrayId >= this.RawData.Count)
            {
                this.SetFieldStateToInvalidArrayIndex();
                return false;
            }

            return true;
        }

        /// <summary>
        ///   Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore da impostare</param>
        /// <param name = "value">Valore da impostare</param>
        /// <param name = "severity">Severità da impostare</param>
        private void SetFieldDataLong(string valueDescription, long value, Severity severity)
        {
            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.Long)
                {
                    this.SetFieldDataInternal(valueDescription,
                                              (value*this.ValueFormatExpression.Factor + this.ValueFormatExpression.Addend).ToString(
                                                  this.ValueFormatExpression.Format), severity);
                }
                else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity);
                }
            }
            else
            {
                this.SetFieldDataInternal(valueDescription, value.ToString(), severity);
            }
        }

        /// <summary>
        ///   Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore da impostare</param>
        /// <param name = "value">Valore da impostare</param>
        /// <param name = "severity">Severità da impostare</param>
        private void SetFieldDataString(string valueDescription, string value, Severity severity)
        {
            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity);
                }
            }
            else
            {
                this.SetFieldDataInternal(valueDescription, value, severity);
            }
        }

        /// <summary>
        /// Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="severity">Severità da impostare</param>
        private void SetFieldDataBoolean(string valueDescription, bool value, Severity severity)
        {
            SetFieldDataInternal(valueDescription, value.ToString(), severity);
        }

        /// <summary>
        ///   Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name = "valueDescription">Descrizione del valore da impostare</param>
        /// <param name = "value">Valore da impostare</param>
        /// <param name = "severity">Severità da impostare</param>
        private void SetFieldDataInternal(string valueDescription, string value, Severity severity)
        {
            this.SeverityLevel      = severity;
            this.ValueDescription   = valueDescription;
            this.Value              = value;
        }

        /// <summary>
        /// Imposta il valore del Field nel caso il valore sia nullo, usando i dati di forzatura se presenti
        /// </summary>
        /// <param name="isReachable">Indica se la periferica è raggiungibile via ping</param>
        private void SetFieldStateToUnknown(bool isReachable)
        {
            if ((this.exceptionCode != MsgObject.ExceptionCode.None) && (this.exceptionCode != MsgObject.ExceptionCode.Acknowledge))
            {
                this.SetFieldDataInternal(Settings.Default.ErrorOnDataRetrievalMessage, Settings.Default.ErrorOnDataRetrievalValue, Severity.Warning);
                return;
            }
            if (this.LastError != null)
            {
                this.SetFieldDataInternal(Settings.Default.ErrorOnDataRetrievalMessage, Settings.Default.ErrorOnDataRetrievalValue, Severity.Warning);
            }
            else if (this.NoModbusDataAvailableForcedState != null)
            {
                if (isReachable)
                {
                    this.SetFieldDataInternal(this.NoModbusDataAvailableForcedState.ForcedValueDescription,
                                              this.NoModbusDataAvailableForcedState.ForcedValue,
                                              this.NoModbusDataAvailableForcedState.ForcedSeverity);
                }
                else
                {
                    this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue, Severity.Unknown);
                }
            }
            else
            {
                this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue, Severity.Unknown);
            }
        }

        /// <summary>
        ///   Imposta il valore del Field nel caso si sia richiesto un valore con indice non disponibile
        /// </summary>
        private void SetFieldStateToInvalidArrayIndex()
        {
            this.SetFieldDataInternal(
                string.Format(CultureInfo.InvariantCulture, "L'indice dell'array {0} non è valido per la dimensione dei dati disponibili {1}",
                              this.ArrayId, this.RawData.Count), Settings.Default.NoDataAvailableValue, Severity.Unknown);
        }

        #endregion
    }
}
﻿using System;
using System.Globalization;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///   Classe di supporto per la persistenza di una periferica su database
    /// </summary>
    public class DeviceDatabaseSupport
    {
        /// <summary>
        ///   Id della Stazione
        /// </summary>
        public Guid StationId { get; private set; }

        /// <summary>
        ///   Id del Fabbricato
        /// </summary>
        public Guid BuildingId { get; private set; }

        /// <summary>
        ///   Id dell'Armadio
        /// </summary>
        public Guid RackId { get; private set; }

        /// <summary>
        ///   Id dell'Interfaccia
        /// </summary>
        public Guid PortId { get; private set; }

        public DeviceDatabaseSupport(Guid stationId, Guid buildingId, Guid rackId, Guid portId)
        {
            this.StationId = stationId;
            this.BuildingId = buildingId;
            this.RackId = rackId;
            this.PortId = portId;
        }

        /// <summary>
        ///   Produce una rappresentazione completa dei dati di supporto per la persistenza della periferica
        /// </summary>
        /// <returns>Rappresentazione testuale dei dati di supporto per la persistenza della periferica</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Station Id: {0}, Building Id: {1}, Rack Id: {2}, Port Id: {3}", this.StationId, this.BuildingId, this.RackId,
                                 this.PortId);
        }
    }
}
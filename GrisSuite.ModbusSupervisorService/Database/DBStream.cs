﻿using System.Collections.ObjectModel;
using System.Text;
using System;
using GrisSuite.MODBUSSupervisorService.Core;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///   Rappresenta il singolo Stream da persistere su database
    /// </summary>
    public class DBStream
    {
        #region Variabili private

        private readonly Collection<DBStreamField>              streamData;
        private readonly Collection<StreamFieldRequestObject>   streamRequests;

        #endregion

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="streamId">Id dello Stream</param>
        /// <param name="name">Nome dello Stream</param>
        /// <param name="byFunCode">Tipo di richiesta modbus associata allo stream</param>
        /// <param name="sClass">Classe da cui risalire alla tipologia di schedulazione del polling</param>
        /// <param name="wOffset">Offset a partire dal quale leggere lo stato di coil/registri</param>
        /// <param name="wCount">Numero di coil/registri da leggere</param>
        /// <param name="excludedFromSeverityComputation">Indica se lo stream e la sua severity devono essere tenuti in considerazione nel calcolo della severità del device</param>
        public DBStream(int streamId, string name, string sClass, /*ushort wOffset, ushort wCount,*/ bool excludedFromSeverityComputation)
        {
            this.StreamId                           = streamId;
            this.Name                               = name;
            this.sClass                             = sClass;
            this.ContainsStreamFieldWithError       = false;
            this.ExcludedFromSeverityComputation    = excludedFromSeverityComputation;
            this.dtLastRequest                      = DateTime.MinValue;
            this.dtNextRequest                      = DateTime.MinValue;
            this.streamData                         = new Collection<DBStreamField>             ();
            this.streamRequests                     = new Collection<StreamFieldRequestObject>  ();
        }

        #region Proprietà pubbliche

        /// <summary>
        ///   Nome dello Stream
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///   Lista dei Field contenuti nello Stream
        /// </summary>
        public Collection<DBStreamField> FieldData
        {
            get { return this.streamData; }
        }

        /// <summary>
        /// Lista delle richieste singole da inviare allo slave
        /// </summary>
        public Collection<StreamFieldRequestObject> StreamRequests
        {
            get { return this.streamRequests; }
        }

        /// <summary>
        ///   Id dello Stream
        /// </summary>
        public int StreamId { get; private set; }

        /// <summary>
        /// Data/ora dell'ultima volta che lo stream è stato richiesto allo slave modbus
        /// </summary>
        public DateTime dtLastRequest { get; set; }

        /// <summary>
        /// Data/ora della prossima richiesta dello stream allo slave modbus (valore utilizzato modalità di polling 'interval')
        /// </summary>
        public DateTime dtNextRequest { get; set; }

        /// <summary>
        /// Indica la classe da cui risalire al profilo di schedulazione
        /// </summary>
        public string sClass { get; private set; }

        /// <summary>
        ///   Indica se lo stream contiene almeno uno stream field che ha generato eccezione nel caricamento dei dati
        /// </summary>
        public bool ContainsStreamFieldWithError { get; private set; }

        /// <summary>
        ///   Severità relativa allo Stream (come aggregata dei Field contenuti)
        /// </summary>
        public Severity SeverityLevel
        {
            get
            {
                Severity streamSeverity = Severity.Unknown;

                foreach (DBStreamField field in this.streamData)
                {
                    // occorre escludere i campi che non saranno poi salvati sul database, perché la loro severità
                    // sebbene effettiva, non deve essere contemplata
                    if ((field.SeverityLevel != Severity.Unknown) && (field.PersistOnDatabase) && (!field.ExcludedFromSeverityComputation) &&
                        ((streamSeverity == Severity.Unknown) || ((int) field.SeverityLevel > (int) streamSeverity)))
                    {
                        streamSeverity = field.SeverityLevel;
                    }

                    if ((!this.ContainsStreamFieldWithError) && (field.LastError != null))
                    {
                        this.ContainsStreamFieldWithError = true;
                    }
                }

                return streamSeverity;
            }
        }

        /// <summary>
        ///   Decodifica descrittiva del valore dello Stream, come concatenazione delle descrizioni dei singoli Field, se la loro severità è non sconosciuta
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                StringBuilder deviceDescription = new StringBuilder();

                int fieldCounter = 0;

                foreach (DBStreamField field in this.streamData)
                {
                    // escludiamo dall'aggregazione i campi che non saranno salvati sul database
                    if ((field.SeverityLevel != Severity.Unknown) && (field.PersistOnDatabase))
                    {
                        if (fieldCounter > 0)
                        {
                            deviceDescription.Append(";" + field.ValueDescriptionComplete);
                        }
                        else
                        {
                            deviceDescription.Append(field.ValueDescriptionComplete);
                        }
                        fieldCounter++;
                    }
                }

                return deviceDescription.ToString();
            }
        }

        /// <summary>
        ///   Indica se lo stream  e la sua severity devono essere tenuti in considerazione nel calcolo della severità della device
        /// </summary>
        public bool ExcludedFromSeverityComputation { get; set; }

        #endregion
    }
}
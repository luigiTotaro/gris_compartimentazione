﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
//using Lextm.SharpSnmpLib;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    /// Indica la modalità di recupero della tabella SNMP
    /// </summary>
    public enum TableMode
    {
        /// <summary>
        /// Sconosciuto
        /// </summary>
        Unknown,
        /// <summary>
        /// Modalità normale
        /// </summary>
        Standard,
        /// <summary>
        /// Modalità per tabelle sparse
        /// </summary>
        Sparse
    }

    /// <summary>
    /// Gestisce gli attributi TableOid e CorrelationTableOid sullo StreamField, che possono essere Oid tabellari singoli o in formato specifico per
    /// gestione tabelle sparse (formato: Oid tabella;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]
    /// </summary>
    public class TableOidAndSupportData
    {
        /// <summary>
        ///   OID della tabella SNMP per la query
        /// </summary>
        //public ObjectIdentifier TableOid { get; private set; }

        /// <summary>
        ///   Id oggetto della table entry (normalmente 1) - per tabelle sparse
        /// </summary>
        public uint? TableEntry { get; private set; }

        /// <summary>
        ///   Numero di colonne della tabella SNMP - per tabelle sparse
        /// </summary>
        public int? TableColumnCount { get; private set; }

        /// <summary>
        /// Lista di Id di colonna (relativi alla tabella SNMP, con base 1) che contengono gli indici
        /// </summary>
        public ReadOnlyCollection<uint> TableIndexColumns { get; private set; }

        /// <summary>
        /// Indica se l'oggetto è stato correttamente inizializzato e se contiene dati validi
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// Indica la modalità di recupero della tabella SNMP, in base ai dati forniti durante l'inizializzazione
        /// </summary>
        public TableMode TableQueryMode { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="attributeName">Nome dell'attributo XML che stiamo valutando</param>
        /// <param name="tableData">Stringa che contiene i dati per la query SNMP tabellare, su cui eseguire il parsing. Ad esempio: 1.3.6.1.2.1.4.22;1;4;1|3 (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), 1.3.6.1.2.1.2.2;1;22;1 (tabella sparsa con 22 colonne, indice su colonna 1), 1.3.6.1.2.1.4.20 (tabella standard)</param>
        public TableOidAndSupportData(string attributeName, string tableData)
        {
            int tableDataElementsCount = -1;
            //ObjectIdentifier tableOid = null;
            int tableEntry = -1;
            int tableColumnCount = -1;
            List<uint> tableIndexColumnsList = new List<uint>();
            this.IsValid = false;
            this.TableQueryMode = TableMode.Unknown;

            if (!string.IsNullOrEmpty(tableData))
            {
                string tableDataString = tableData.Trim();

                if (!string.IsNullOrEmpty(tableDataString))
                {
                    string[] tableDataValues = tableDataString.Split(';');

                    if ((tableDataValues.Length == 1) || (tableDataValues.Length == 4))
                    {
                        tableDataElementsCount = tableDataValues.Length;

                        #region Parsing Oid tabella SNMP

                        string tableOidString = tableDataValues[0].Trim();

                        if (!string.IsNullOrEmpty(tableOidString))
                        {
                            //tableOid = SnmpUtility.TryParseOid(tableOidString);

                            //if (tableOid == null)
                            //{
                            //    throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                            //                                              "La prima parte dell'attributo '{0}', non contiene un Oid SNMP valido. Valore contenuto: '{1}'",
                            //                                              attributeName, tableDataValues[0].Trim()));
                            //}
                        }
                        else
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La prima parte dell'attributo '{0}', non contiene un Oid SNMP valido. Valore contenuto: '{1}'",
                                                                      attributeName, tableDataValues[0].Trim()));
                        }

                        #endregion

                        if (tableDataValues.Length > 1)
                        {
                            // Siamo nel caso di espressione a 4 elementi

                            #region tableEntry parsing

                            string tableEntryString = tableDataValues[1].Trim();

                            if (!int.TryParse(tableEntryString, out tableEntry))
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La seconda parte dell'attributo '{0}', non contiene un valore numerico intero positivo valido, come Id di table entry. Valore contenuto: '{1}'",
                                                                          attributeName, tableDataValues[1].Trim()));
                            }

                            if (tableEntry <= 0)
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La seconda parte dell'attributo '{0}', non contiene un valore numerico intero positivo valido, come Id di table entry. Valore contenuto: '{1}'",
                                                                          attributeName, tableDataValues[1].Trim()));
                            }

                            #endregion

                            #region tableColumnCount parsing

                            string tableColumnCountString = tableDataValues[2].Trim();

                            if (!int.TryParse(tableColumnCountString, out tableColumnCount))
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La terza parte dell'attributo '{0}', non contiene un valore numerico intero positivo valido, come numero di colonne della tabella SNMP. Valore contenuto: '{1}'",
                                                                          attributeName, tableDataValues[2].Trim()));
                            }

                            if (tableColumnCount <= 0)
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La terza parte dell'attributo '{0}', non contiene un valore numerico intero positivo valido, come numero di colonne della tabella SNMP. Valore contenuto: '{1}'",
                                                                          attributeName, tableDataValues[2].Trim()));
                            }

                            #endregion

                            #region tableIndexColumns parsing

                            string tableIndexColumnsString = tableDataValues[3].Trim();

                            if (string.IsNullOrEmpty(tableIndexColumnsString))
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La quarta parte dell'attributo '{0}', non contiene una lista di valori numerici interi positivi validi, eventualmente separati da carattere '|', con la lista delle colonne che contengono l'indice della tabella SNMP. Valore contenuto: '{1}'",
                                                                          attributeName, tableDataValues[3].Trim()));
                            }

                            string[] tic = tableIndexColumnsString.Split('|');

                            if (tic.Length > 0)
                            {
                                foreach (string tableIndexColumn in tic)
                                {
                                    uint ti;

                                    if (uint.TryParse(tableIndexColumn.Trim(), out ti))
                                    {
                                        if (ti > 0)
                                        {
                                            // Evitiamo di aggiungere valori doppi
                                            if (tableIndexColumnsList.Contains(ti))
                                            {
                                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                                          "La quarta parte dell'attributo '{0}', non contiene una lista di valori numerici interi positivi validi univoci, eventualmente separati da carattere '|', con la lista delle colonne che contengono l'indice della tabella SNMP. Valore contenuto: '{1}'",
                                                                                          attributeName, tableDataValues[3].Trim()));
                                            }

                                            tableIndexColumnsList.Add(ti);
                                        }
                                        else
                                        {
                                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                                      "La quarta parte dell'attributo '{0}', non contiene una lista di valori numerici interi positivi validi, eventualmente separati da carattere '|', con la lista delle colonne che contengono l'indice della tabella SNMP. Valore contenuto: '{1}'",
                                                                                      attributeName, tableDataValues[3].Trim()));
                                        }
                                    }
                                    else
                                    {
                                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                                  "La quarta parte dell'attributo '{0}', non contiene una lista di valori numerici interi positivi validi, eventualmente separati da carattere '|', con la lista delle colonne che contengono l'indice della tabella SNMP. Valore contenuto: '{1}'",
                                                                                  attributeName, tableDataValues[3].Trim()));
                                    }
                                }
                            }
                            else
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La quarta parte dell'attributo '{0}', non contiene una lista di valori numerici interi positivi validi, eventualmente separati da carattere '|', con la lista delle colonne che contengono l'indice della tabella SNMP. Valore contenuto: '{1}'",
                                                                          attributeName, tableDataValues[3].Trim()));
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                  "L'attributo '{0}' non è nel formato atteso 'Oid tabella[;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]]', ad esempio '1.3.6.1.2.1.4.22;1;4;1|3' (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), '1.3.6.1.2.1.2.2;1;22;1' (tabella sparsa con 22 colonne, indice su colonna 1), '1.3.6.1.2.1.4.20' (tabella standard). Valore contenuto: '{1}'",
                                                                  attributeName, tableData));
                    }
                }
            }
            else
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                          "L'attributo '{0}' non è nel formato atteso 'Oid tabella[;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]]', ad esempio '1.3.6.1.2.1.4.22;1;4;1|3' (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), '1.3.6.1.2.1.2.2;1;22;1' (tabella sparsa con 22 colonne, indice su colonna 1), '1.3.6.1.2.1.4.20' (tabella standard). Valore contenuto: '{1}'",
                                                          attributeName, tableData));
            }

            if (tableDataElementsCount == 1)
            {
                // Caso di stringa che contiene solamente l'Oid tabellare
                //if (tableOid != null)
                //{
                //    this.IsValid = true;
                //    this.TableQueryMode = TableMode.Standard;
                //    //this.TableOid = tableOid;
                //    this.TableEntry = null;
                //    this.TableColumnCount = null;
                //    this.TableIndexColumns = null;
                //}
            }

            if (tableDataElementsCount == 4)
            {
                // Caso di stringa che contiene le 4 componenti per le query su tabelle sparse
                //if ((tableOid != null) && (tableEntry > 0) && (tableColumnCount > 0) && (tableIndexColumnsList.Count > 0))
                //{
                //    this.IsValid = true;
                //    this.TableQueryMode = TableMode.Sparse;
                //    //this.TableOid = tableOid;
                //    this.TableEntry = (uint) tableEntry;
                //    this.TableColumnCount = tableColumnCount;
                //    this.TableIndexColumns = tableIndexColumnsList.AsReadOnly();
                //}
            }
        }
    }
}
﻿namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///   Rappresenta la severità dei valori ricevuti
    /// </summary>
    public enum Severity
    {
        /// <summary>
        ///   Valore informativo (valido solo per stream e stream field)
        /// </summary>
        Info = -255,

        /// <summary>
        ///   Stato normale
        /// </summary>
        Ok = 0,

        /// <summary>
        ///   Allarme lieve
        /// </summary>
        Warning = 1,

        /// <summary>
        ///   Allarme grave
        /// </summary>
        Error = 2,

        /// <summary>
        /// Stato da configurare
        /// </summary>
        MaintenanceMode = 9,

        /// <summary>
        ///   Sconosciuto o nessuna risposta
        /// </summary>
        Unknown = 255
    }
}
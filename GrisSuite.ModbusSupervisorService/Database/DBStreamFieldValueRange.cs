﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using GrisSuite.MODBUSSupervisorService.Helper;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    ///   Rappresenta il tipo di valori ad intervallo Minimo-Massimo
    /// </summary>
    public class DBStreamFieldValueRange
    {
        #region Costruttore

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "minValue">Valore minimo</param>
        /// <param name = "maxValue">Valore massimo</param>
        public DBStreamFieldValueRange(string minValue, string maxValue)
        {
            if (minValue != null)
            {
                this.ParseValue(minValue, ValueType.Min);
            }

            if (maxValue != null)
            {
                this.ParseValue(maxValue, ValueType.Max);
            }
        }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "value">Valore singolo (inteso come minimo e massimo uguali)</param>
        public DBStreamFieldValueRange(string value)
        {
            if (value != null)
            {
                this.ParseValue(value, ValueType.Min);
                this.ParseValue(value, ValueType.Max);
            }
        }

        #endregion

        #region Metodi pubblici

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///   Valore minimo
        /// </summary>
        public long? MinValue { get; private set; }

        /// <summary>
        ///   Valore massimo
        /// </summary>
        public long? MaxValue { get; private set; }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Tipo del valore del range
        /// </summary>
        private enum ValueType
        {
            /// <summary>
            /// Valore minimo
            /// </summary>
            Min,

            /// <summary>
            /// Valore massimo
            /// </summary>
            Max
        };

        /// <summary>
        /// Stabilisce il tipo di dato in ingresso, in base ad una codifica e imposta i campi appropriati in base al tipo di valore
        /// </summary>
        /// <param name="value">Valore stringa in ingressp</param>
        /// <param name="type">Tipo del valore del range</param>
        private void ParseValue(string value, ValueType type)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Il valore indicato per l'intervallo di valutazione è vuoto. Valore contenuto: '{0}'",
                                                                           value));
            }

            switch (type)
            {
                case ValueType.Min:
                    long lMin;
                    if (long.TryParse(value, out lMin))
                        this.MinValue = lMin;
                    break;
                case ValueType.Max:
                    long lMax;
                    if (long.TryParse(value, out lMax))
                        this.MaxValue = lMax;
                    break;
            }
        }

        /// <summary>
        /// Estrae i dati dal valore dell'attributo, in base al nome della componente. Le varie componenti sono separate da §
        /// </summary>
        /// <param name="paramName">Nome della componente</param>
        /// <param name="paramData">Stringa che contiene tutti i dati dell'attributo</param>
        /// <returns>Valore indicato sulla singola componente</returns>
        private string GetParamData(string paramName, string paramData)
        {
            Regex regex = new Regex(string.Format("{0}=(.*?)(?:§|$)", paramName), RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            Match data = regex.Match(paramData);

            return data.Groups[1].Value;
        }

        /// <summary>
        /// In base al tipo di valore del range, restituisce un nome leggibile
        /// </summary>
        /// <param name="type">Tipo di valore del range (minimo / massimo)</param>
        /// <returns>Stringa con il nome leggibile dell'attributo del range</returns>
        private string GetComponentName(ValueType type)
        {
            switch (type)
            {
                case ValueType.Min:
                    return "RangeValueMin";
                case ValueType.Max:
                    return "RangeValueMax";
                default:
                    return string.Empty;
            }
        }

        #endregion
    }
}
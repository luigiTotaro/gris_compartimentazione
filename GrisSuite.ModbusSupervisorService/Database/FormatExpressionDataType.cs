﻿namespace GrisSuite.MODBUSSupervisorService.Database
{
    // Tipo di dati supportato dalla stringa di formattazione
    public enum FormatExpressionDataType
    {
        /// <summary>
        ///   Non definito
        /// </summary>
        Unknown,
        /// <summary>
        ///   Stringa
        /// </summary>
        String,
        /// <summary>
        ///   Intero a 64 bit
        /// </summary>
        Long
    } ;
}
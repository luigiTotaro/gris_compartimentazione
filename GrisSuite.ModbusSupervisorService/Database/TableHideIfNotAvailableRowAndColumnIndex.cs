﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace GrisSuite.MODBUSSupervisorService.Database
{
    /// <summary>
    /// Gestisce l'attributo TableHideIfNotAvailableRowAndColumnIndex sullo StreamField che permette di non persistere in base dati uno stream field, se non si trova,
    /// data la regex di ricerca e la relativa colonna su cui cercare nella tabella SNMP, il valore relativo nelle righe della tabella
    /// </summary>
    public class TableHideIfNotAvailableRowAndColumnIndex
    {
        /// <summary>
        ///   Espressione regolare da usare per la ricerca nella tabella dinamica a cui questo stream field si riferisce, che sarà da non persistere se non esiste la riga nella tabella
        /// </summary>
        public Regex TableHideIfNotAvailableRowRegex { get; private set; }

        /// <summary>
        ///   Indice della colonna della tabella SNMP sui cui eseguire le ricerche (normalmente è la colonna 0 delle tabelle, che contiene l'indice)
        /// </summary>
        public int? TableHideIfNotAvailableColumnIndex { get; private set; }

        /// <summary>
        ///   OID con dati di supporto della tabella SNMP su cui effettuare la ricerca
        /// </summary>
        public TableOidAndSupportData TableHideIfNotAvailableFieldTableOid { get; private set; }

        /// <summary>
        ///   Seconda espressione regolare da usare per la ricerca nella tabella dinamica a cui questo stream field si riferisce, che sarà da non persistere se non esiste la riga nella tabella
        /// </summary>
        public Regex TableHideIfNotAvailableRowSecondRegex { get; private set; }

        /// <summary>
        ///   Secondo indice della colonna della tabella SNMP sui cui eseguire le ricerche (normalmente è la colonna 0 delle tabelle, che contiene l'indice)
        /// </summary>
        public int? TableHideIfNotAvailableSecondColumnIndex { get; private set; }

        /// <summary>
        /// Indica se l'oggetto è stato correttamente inizializzato e se contiene dati validi
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="tableHideIfNotAvailableRowAndColumnIndex">Stringa che contiene il valore dell'attributo, su cui eseguire il parsing</param>
        public TableHideIfNotAvailableRowAndColumnIndex(string tableHideIfNotAvailableRowAndColumnIndex)
        {
            Regex tableHideIfNotAvailableRowRegex = null;
            int tableHideIfNotAvailableColumnIndex = -1;
            TableOidAndSupportData tableHideIfNotAvailableFieldTableOid = null;
            Regex secondTableHideIfNotAvailableRowRegex = null;
            int secondTableHideIfNotAvailableColumnIndex = -1;
            this.IsValid = false;

            if (!string.IsNullOrEmpty(tableHideIfNotAvailableRowAndColumnIndex))
            {
                string tableHideIfNotAvailableRowAndColumnIndexString = tableHideIfNotAvailableRowAndColumnIndex.Trim();

                if (!string.IsNullOrEmpty(tableHideIfNotAvailableRowAndColumnIndexString))
                {
                    string[] values = tableHideIfNotAvailableRowAndColumnIndexString.Split(',');

                    if ((values.Length == 3) || (values.Length == 5))
                    {
                        #region Parsing espressione regolare per valore di ricerca

                        if (values[0].Trim().Length == 0)
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La prima parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un'espressione regolare valida. Valore contenuto nullo"));
                        }

                        try
                        {
                            tableHideIfNotAvailableRowRegex = new Regex(values[0].Trim(), RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                        }
                        catch (ArgumentNullException)
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La prima parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un'espressione regolare valida. Valore contenuto: '{0}'",
                                                                      values[0].Trim()));
                        }
                        catch (ArgumentException)
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La prima parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un'espressione regolare valida. Valore contenuto: '{0}'",
                                                                      values[0].Trim()));
                        }

                        #endregion

                        #region Parsing indice di colonna per tabella dinamica

                        string tableCorrelationFilterColumnIndexString = values[1].Trim();

                        if (!int.TryParse(tableCorrelationFilterColumnIndexString, out tableHideIfNotAvailableColumnIndex))
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La seconda parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un valore numerico intero valido, come indice di colonna. Valore contenuto: '{0}'",
                                                                      values[1].Trim()));
                        }

                        #endregion

                        #region Parsing Oid e dati di supporto relativi alla tabella su cui eseguire le ricerche

                        string tableOidString = values[2].Trim();

                        if (!string.IsNullOrEmpty(tableOidString))
                        {
                            TableOidAndSupportData tableOidAndSupportDataLocal = new TableOidAndSupportData(
                                "TableHideIfNotAvailableRowAndColumnIndex", tableOidString);

                            if (!tableOidAndSupportDataLocal.IsValid)
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La terza parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non è nel formato atteso 'Oid tabella[;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]]', ad esempio '1.3.6.1.2.1.4.22;1;4;1|3' (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), '1.3.6.1.2.1.2.2;1;22;1' (tabella sparsa con 22 colonne, indice su colonna 1), '1.3.6.1.2.1.4.20' (tabella standard). Valore contenuto: '{0}'",
                                                                          values[2].Trim()));
                            }

                            tableHideIfNotAvailableFieldTableOid = tableOidAndSupportDataLocal;
                        }
                        else
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La terza parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non è nel formato atteso 'Oid tabella[;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]]', ad esempio '1.3.6.1.2.1.4.22;1;4;1|3' (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), '1.3.6.1.2.1.2.2;1;22;1' (tabella sparsa con 22 colonne, indice su colonna 1), '1.3.6.1.2.1.4.20' (tabella standard). Valore contenuto: '{0}'",
                                                                      values[2].Trim()));
                        }

                        #endregion

                        if (values.Length == 5)
                        {
                            // Siamo nel caso di espressione a 5 elementi

                            #region Parsing seconda espressione regolare per valore di ricerca

                            if (values[3].Trim().Length == 0)
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La quarta parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un'espressione regolare valida. Valore contenuto nullo"));
                            }

                            try
                            {
                                secondTableHideIfNotAvailableRowRegex = new Regex(values[3].Trim(),
                                                                                  RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                            }
                            catch (ArgumentNullException)
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La quarta parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un'espressione regolare valida. Valore contenuto: '{0}'",
                                                                          values[3].Trim()));
                            }
                            catch (ArgumentException)
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La quarta parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un'espressione regolare valida. Valore contenuto: '{0}'",
                                                                          values[3].Trim()));
                            }

                            #endregion

                            #region Parsing secondo indice di colonna per tabella dinamica

                            string secondTableCorrelationFilterColumnIndexString = values[4].Trim();

                            if (!int.TryParse(secondTableCorrelationFilterColumnIndexString, out secondTableHideIfNotAvailableColumnIndex))
                            {
                                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                          "La quinta parte dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex', non contiene un valore numerico intero valido, come indice di colonna. Valore contenuto: '{0}'",
                                                                          values[4].Trim()));
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                  "L'attributo 'TableHideIfNotAvailableRowAndColumnIndex' non è nel formato atteso 'Valore da ricercare su righe, colonna tabella SNMP, Oid tabella SNMP su cui effettuare la ricerca', ad esempio '1,0,1.3.6.1.4.1.12.43.13.384.3.1.4', oppure nel formato 'Valore da ricercare su righe, colonna tabella SNMP, Oid tabella SNMP su cui effettuare la ricerca, secondo valore da ricercare sulla riga, seconda colonna per ricerca tabella SNMP', ad esempio '1,0,1.3.6.1.4.1.12.43.13.384.3.1.4,^A$,3'. Valore contenuto: '{0}'",
                                                                  tableHideIfNotAvailableRowAndColumnIndex));
                    }
                }
            }
            else
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                          "L'attributo 'TableHideIfNotAvailableRowAndColumnIndex' non è nel formato atteso 'Valore da ricercare su righe, colonna tabella SNMP, Oid tabella SNMP su cui effettuare la ricerca', ad esempio '1,0,1.3.6.1.4.1.12.43.13.384.3.1.4'. Valore contenuto: '{0}'",
                                                          tableHideIfNotAvailableRowAndColumnIndex));
            }

            if ((tableHideIfNotAvailableRowRegex != null) && (tableHideIfNotAvailableColumnIndex >= 0) &&
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
// L'espressione è sempre vera perché non possiamo arrivare qui senza eccezioni. Lasciata per usi futuri.
                (tableHideIfNotAvailableFieldTableOid != null))
// ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                // I secondi valori di espressione regolare e indice di colonna sono opzionali e saranno presi in considerazione solamente
                // se sono valorizzati i valori principali

                this.TableHideIfNotAvailableRowRegex = tableHideIfNotAvailableRowRegex;
                this.TableHideIfNotAvailableColumnIndex = tableHideIfNotAvailableColumnIndex;
                this.TableHideIfNotAvailableFieldTableOid = tableHideIfNotAvailableFieldTableOid;

                if ((secondTableHideIfNotAvailableRowRegex != null) && (secondTableHideIfNotAvailableColumnIndex >= 0))
                {
                    this.TableHideIfNotAvailableRowSecondRegex = secondTableHideIfNotAvailableRowRegex;
                    this.TableHideIfNotAvailableSecondColumnIndex = secondTableHideIfNotAvailableColumnIndex;
                }

                this.IsValid = true;
            }
        }
    }
}
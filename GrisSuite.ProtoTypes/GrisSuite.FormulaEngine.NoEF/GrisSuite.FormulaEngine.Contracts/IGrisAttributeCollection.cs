﻿
using System.Collections;

namespace GrisSuite.FormulaEngine.Contracts
{
	public interface IGrisAttributeCollection : IDictionary, INeedInitialization, IStorable
	{
		void SyncOriginalEntities ();
		void ForceAttribute ( string attributeName, dynamic value );
	}
}

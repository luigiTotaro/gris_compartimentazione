﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Contracts
{
	public interface IEvaluableObject
	{
		IEvaluationFormula DefaultEvaluationFormula { get; }
		SortedSet<IEvaluationFormula> CustomEvaluationFormulas { get; }
		IEvaluationContext EvaluationContext { get; set; }
		void Evaluate ();
		void Evaluate ( bool force );
		void ForceEvaluationOnDependentObjects();
		bool IsEvaluated { get; }
		bool IsSeverityEstabilished { get; }
		IEvaluableObject CallerObject { get; set; }
		Dictionary<string, object> EvaluationState { get; set; }
		IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<IAttribute> attributes, Dictionary<string, object> state );
	}
}
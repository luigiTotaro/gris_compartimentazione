﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Contracts
{
	public interface IEntityCollection<TEntity> : ICollection<TEntity>, IEntityEnd<TEntity> {}

	public interface IEntityEnd<out TEntity>
	{
		void Load ();
		void Load ( IGrisObject grisObject, bool lookupInCacheOnly = false );
		bool IsLoaded { get; }
		TEntity Value { get; }
	}
}

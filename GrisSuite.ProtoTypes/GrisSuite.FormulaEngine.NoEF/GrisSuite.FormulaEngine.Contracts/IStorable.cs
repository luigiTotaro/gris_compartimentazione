﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Contracts
{
	public interface IStorable
	{
		IList<IStorable> StorableNodes { get; }
		string GetUpdateCommand ();
	}
}

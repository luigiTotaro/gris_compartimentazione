﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GrisSuite.FormulaEngine.Contracts.Severities;

namespace GrisSuite.FormulaEngine.Contracts
{
	public interface IGrisObject : IEvaluableObject, IStorable, INeedInitialization
	{
		event EventHandler NeedExternalInitialization;

		#region Primitive Properties

		/// <summary>
		/// Identificativo del GrisObject.
		/// </summary>
		[DataMember]
		Guid Id { get; set; }

		/// <summary>
		/// Nome del GrisObject.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Identificativo del GrisObject specifico per tipo.
		/// </summary>
		[DataMember]
		long SpecificId { get; set; }

		/// <summary>
		/// Identificativo del tipo di GrisObject.
		/// </summary>
		[DataMember]
		int TypeId { get; set; }

		/// <summary>
		/// Severità dello stato.
		/// </summary>
		[DataMember]
		int SevLevel { get; set; }

		/// <summary>
		/// Severità di dettaglio dello stato.
		/// </summary>
		[DataMember]
		int? SevLevelDetail { get; set; }

		/// <summary>
		/// Identificativo del GrisObject padre.
		/// </summary>
		[DataMember]
		Guid? ParentId { get; set; }

		/// <summary>
		/// Indica se è in mantenimento.
		/// </summary>
		[DataMember]
		bool InMaintenance { get; set; }

		/// <summary>
		/// Severità dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		[DataMember]
		int? SevLevelReal { get; set; }

		/// <summary>
		/// Severità di dettaglio dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		[DataMember]
		int? SevLevelDetailReal { get; set; }

		/// <summary>
		/// Ultima severità dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		[DataMember]
		int? SevLevelLast { get; set; }

		/// <summary>
		/// Ultima severità di dettaglio dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		[DataMember]
		int? SevLevelDetailLast { get; set; }

		/// <summary>
		/// Stamp dell' ultimo aggiornamento.
		/// </summary>
		[DataMember]
		Guid? LastUpdateGuid { get; set; }

		/// <summary>
		/// Indica se il GrisObject è escluso dal calcolo dello stato del padre.
		/// </summary>
		[DataMember]
		bool ExcludeFromParentStatus { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[DataMember]
		bool ForcedByUser { get; set; }

		#endregion

		#region Builtin Severity properties

		int Severity { get; set; }

		ObjectSeverityDetail SeverityDetail { get; set; }

		bool ExcludedFromParentStatus { get; set; }

		Color Color { get; set; }

		void ForceSeverity ( int severity );

		void ForceSeverity ( ObjectSeverityDetail severityDetail );

		#endregion

		#region Aggregations

		#region Count aggregation methods

		int CountChildrenInSeverity ( int severity );

		int CountChildrenInSeverity ( ObjectSeverityDetail severityDetail );

		int CountChildrenAtLeastInSeverity ( int severity );

		int CountChildrenAtLeastInSeverity ( ObjectSeverityDetail severityDetail );

		#endregion

		#region Get aggregation methods

		int GetChildrenWorstSeverity ();

		ObjectSeverityDetail GetChildrenWorstSeverityDetail ();

		int GetChildrenBestSeverity ();

		ObjectSeverityDetail GetChildrenBestSeverityDetail ();

		#endregion

		#region Boolean aggregation methods

		bool IsAnyChildAtLeastInSeverity ( int severity );

		bool IsAnyChildAtLeastInSeverity ( ObjectSeverityDetail severityDetail );

		bool AreAllChildrenAtLeastInSeverity ( int severity );

		bool AreAllChildrenAtLeastInSeverity ( ObjectSeverityDetail severityDetail );

		#endregion

		#endregion

		#region Navigation members

		IEntityCollection<IGrisObject> Children { get; }

		IGrisObject GetChildByName ( string name );

		T GetChildByName<T> ( string name ) where T : IGrisObject;

		IList<IGrisObject> GetChildrenByName ( string name );

		IList<T> GetChildrenByName<T> ( string name ) where T : IGrisObject;

		IGrisAttributeCollection Attributes { get; set; }

		#endregion
	}
}

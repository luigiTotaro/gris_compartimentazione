﻿
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IVirtualObject : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		[DataMember]
		long VirtualObjectId { get; set; }

		#endregion
	}
}

﻿

using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IRegion : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Nome del compartimento.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Identificativo specifico del compartimento.
		/// </summary>
		[DataMember]
		long RegionId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanze delle Linee appartenenti al compartimento.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IZone> Zones { get; set; }

		#endregion
	}
}
﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IStreamField : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo di periferica.
		/// </summary>
		[DataMember]
		long DevId { get; set; }

		/// <summary>
		/// Identificativo di Stream.
		/// </summary>
		[DataMember]
		int StreamId { get; set; }

		/// <summary>
		/// Identificativo di Field.
		/// </summary>
		[DataMember]
		int FieldId { get; set; }

		/// <summary>
		/// Identificativo di array.
		/// </summary>
		[DataMember]
		int ArrayId { get; set; }

		/// <summary>
		/// Nome dello StreamField.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Severità come riportata dal collettore..
		/// </summary>
		[DataMember]
		int? ActualSevLevel { get; set; }

		/// <summary>
		/// Valore dello StreamField.
		/// </summary>
		[DataMember]
		string Value { get; set; }

		/// <summary>
		/// Descrizione dello StreamField.
		/// </summary>
		[DataMember]
		String Description { get; set; }

		/// <summary>
		/// Visibilità dello StreamField.
		/// </summary>
		[DataMember]
		byte? Visible { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[DataMember]
		Guid? ReferenceID { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IStream Stream { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IStreamFieldHistory> StreamFieldsHistory { get; set; }

		#endregion
	}
}

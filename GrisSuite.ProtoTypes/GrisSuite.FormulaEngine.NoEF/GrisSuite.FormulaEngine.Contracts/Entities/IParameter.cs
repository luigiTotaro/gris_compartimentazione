﻿
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IParameter
	{
		#region Primitive Properties

		/// <summary>
		/// Nome del parametro.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Valore del parametro.
		/// </summary>
		[DataMember]
		string Value { get; set; }

		/// <summary>
		/// Descrizione del parametro.
		/// </summary>
		[DataMember]
		string Description { get; set; }

		IParameter GetByName ( string name );

		#endregion
	}
}

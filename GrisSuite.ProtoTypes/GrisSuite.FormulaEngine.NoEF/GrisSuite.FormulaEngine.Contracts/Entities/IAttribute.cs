﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IAttribute
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell' attributo.
		/// </summary>
		[DataMember]
		short Id { get; set; }

		/// <summary>
		/// Nome dell' attributo.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanze di Attributo concrete associate ai GrisObjects.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IObjectAttribute> ObjectAttributes { get; set; }

		#endregion

		IList<IAttribute> GetAll ();

		IAttribute GetById ( short id );
	}
}

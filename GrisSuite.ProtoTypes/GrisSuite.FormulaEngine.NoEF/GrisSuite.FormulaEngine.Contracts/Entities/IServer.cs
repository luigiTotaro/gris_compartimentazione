﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IServer : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Nome Host del server STLC.
		/// </summary>
		[DataMember]
		string Host { get; set; }

		/// <summary>
		/// Nome Host completo del server STLC.
		/// </summary>
		[DataMember]
		string FullHostName { get; set; }

		/// <summary>
		/// Nome Host completo del server STLC.
		/// </summary>
		[DataMember]
		string IpAddress { get; set; }

		/// <summary>
		/// Data e ora dell' ultimo messaggio ricevuto.
		/// </summary>
		[DataMember]
		DateTime? LastUpdate { get; set; }

		/// <summary>
		/// Tipo dell'ultimo messaggio ricevuto.
		/// </summary>
		[DataMember]
		String LastMessageType { get; set; }

		/// <summary>
		/// Versione del server STLC.
		/// </summary>
		[DataMember]
		string ServerVersion { get; set; }

		/// <summary>
		/// Indirizzo MAC del server STLC.
		/// </summary>
		[DataMember]
		string MacAddress { get; set; }

		/// <summary>
		/// Indica se il server STLC è stato cancellato.
		/// </summary>
		[DataMember]
		bool? IsDeleted { get; set; }

		/// <summary>
		/// Identificativo specifico del server STLC.
		/// </summary>
		[DataMember]
		long ServerId { get; set; }

		/// <summary>
		/// Identificativo della stazione in cui si trova il server STLC.
		/// </summary>
		[DataMember]
		Guid? NodeId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della stazione in cui si trova il server STLC.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		INode Node { get; set; }

		/// <summary>
		/// Istanze delle periferiche collegate al server STLC.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IDevice> Devices { get; set; }

		#endregion
	}
}
﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IObjectAttribute
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell' attributo.
		/// </summary>
		[DataMember]
		long Id { get; set; }

		/// <summary>
		/// Tipo di attributo.
		/// </summary>
		[DataMember]
		short TypeId { get; set; }

		/// <summary>
		/// Identificativo del GrisObject associato.
		/// </summary>
		[DataMember]
		Guid GrisObjectId { get; set; }

		/// <summary>
		/// Valore dell' attributo.
		/// </summary>
		[DataMember]
		String Value { get; set; }

		/// <summary>
		/// Identificativo del GrisObject di Formula che ha determinato il valore.
		/// </summary>
		[DataMember]
		Guid? ComputedByFormulaObjectStatusId { get; set; }

		/// <summary>
		/// Indice del GrisObject di Formula che ha determinato il valore.
		/// </summary>
		[DataMember]
		byte? ComputedByFormulaIndex { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza del Tipo di Attributo.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IAttribute Type { get; set; }

		/// <summary>
		/// Istanza del GrisObject associato.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IGrisObject GrisObject { get; set; }

		/// <summary>
		/// Istanza della formula che ha determinato il valore.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IObjectFormula ComputedByObjectFormula { get; set; }

		#endregion
	}
}
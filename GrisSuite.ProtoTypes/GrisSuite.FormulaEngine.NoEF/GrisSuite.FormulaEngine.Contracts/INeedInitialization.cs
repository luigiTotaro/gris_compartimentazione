﻿
namespace GrisSuite.FormulaEngine.Contracts
{
	public interface INeedInitialization
	{
		void Initialize();
		bool IsInitialized { get; }
	}
}

﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Contracts;
using Microsoft.Scripting.Hosting;

namespace GrisSuite.FormulaEngine.Library
{
	public class FormulaContext : IEvaluationContext
	{
		private readonly ScriptEngine _scriptEngine;
		private static readonly Dictionary<string, CompiledCode> _scriptSources = new Dictionary<string, CompiledCode>(10000);
		private static readonly object _locker = new object();

		public FormulaContext ( ScriptEngine scriptEngine )
		{
			if ( scriptEngine == null ) throw new ArgumentException("L' oggetto FormulaContext non può essere inizializzato con uno ScriptEngine nullo.", "scriptEngine");

			this._scriptEngine = scriptEngine;
		}

		public void Evaluate ( IEvaluableObject evalObject )
		{
			this.Evaluate(evalObject, null);
		}

		public void Evaluate ( IEvaluableObject evalObject, Dictionary<string, object> state )
		{
			foreach ( var customFormula in evalObject.CustomEvaluationFormulas )
			{
				this.ExecuteFormula(customFormula, state);
			}

			if ( !evalObject.IsSeverityEstabilished ) this.ExecuteFormula(evalObject.DefaultEvaluationFormula, state);
		}

		private void ExecuteFormula ( IEvaluationFormula formula, Dictionary<string, object> state )
		{
			if ( formula.CanEvaluate )
			{
				CompiledCode source = this.GetCompiledSource(formula);

				if ( source != null )
				{
					ScriptScope scope = this._scriptEngine.CreateScope();
					scope.SetVariable("obj", formula.Owner);
					scope.SetVariable("state", state);

					//if ( formula.Owner is Model.Entities.NodeSystem ) System.Diagnostics.Debugger.Break();

					try
					{
						source.Execute(scope);
					}
					catch ( Exception e )
					{
						ExceptionOperations eo = this._scriptEngine.GetService<ExceptionOperations>();
						formula.EvaluationError = eo.FormatException(e);
					}
				}
			}
		}

		private CompiledCode GetCompiledSource ( IEvaluationFormula evalFormula )
		{
			if ( evalFormula != null && evalFormula.CanEvaluate )
			{
				if ( _scriptSources.ContainsKey(evalFormula.ScriptPath) )
				{
					return _scriptSources[evalFormula.ScriptPath];
				}

				//string scriptPath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + "\\bin" + evalFormula.ScriptPath);
				CompiledCode cc = this._scriptEngine.CreateScriptSourceFromFile(evalFormula.ScriptPath).Compile();

				lock ( _locker ) //TODO: è necessario?
				{
					if ( !_scriptSources.ContainsKey(evalFormula.ScriptPath) )
					{
						_scriptSources.Add(evalFormula.ScriptPath, cc);
					}
				}

				return cc;
			}

			return null;
		}
	}
}

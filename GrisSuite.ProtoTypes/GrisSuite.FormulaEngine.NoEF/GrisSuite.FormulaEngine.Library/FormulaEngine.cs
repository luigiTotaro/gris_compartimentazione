﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Model;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace GrisSuite.FormulaEngine.Library
{
	public class FormulaEngine : IFormulaEngine
	{
		private readonly ICollection<string> _searchPaths;
		private ScriptEngine _scriptEng;
		private readonly AppDomain _scriptAppDomain;
		private IList<EvaluablePackage> _objectsToEvalCache;

		public FormulaEngine () {}

		public FormulaEngine ( ICollection<string> searchPaths )
		{
			_searchPaths = searchPaths;
		}

		private void InitializeEngine ()
		{
			Dictionary<string, object> options = new Dictionary<string, object>();
			//options["Debug"] = true;
			options["LightweightScopes"] = true;
			this._scriptEng = Python.CreateEngine(options);
		}

		public void Eval ( IList<EvaluablePackage> objectsToEval )
		{
			this.InitializeEngine();

			this._objectsToEvalCache = objectsToEval;
			this._scriptEng.Runtime.LoadAssembly(typeof(IGrisObject).Assembly);
			this._scriptEng.Runtime.LoadAssembly(typeof(GrisObject).Assembly);
			if ( this._searchPaths != null ) this._scriptEng.SetSearchPaths(this._searchPaths);

			var factory = new ObjectFactory();

			var attributes = factory.GetAttribute().GetAll();
			objectsToEval.ToList().ForEach(oe =>
			{
				oe.GrisObject.NeedExternalInitialization += SetContext;
			});

			foreach ( var objectToEval in objectsToEval ) objectToEval.GrisObject.InitializeForEvaluation(
																								new FormulaContext(this._scriptEng),
																								attributes,
																								objectToEval.State
																								).Evaluate();

			//Parallel.ForEach(objectsToEval,
			//                 objectToEval =>
			//                 objectToEval.GrisObject.InitializeForEvaluation(new FormulaContext(this._scriptEng), attributes,
			//                                                                 objectToEval.State).Evaluate());

			this._scriptEng.Runtime.Shutdown();
			//AppDomain.Unload(this._scriptAppDomain);
		}

		private void SetContext ( object sender, EventArgs e )
		{
			if ( sender != null && sender is GrisObject )
			{
				var go = (GrisObject) sender;
				var cachedGo = this._objectsToEvalCache.SingleOrDefault(gp => gp.GrisObject.Id == go.Id);
				//var attributes = this._dbContext.Attributes.ToList();
				var attributes = new ObjectFactory().GetAttribute().GetAll();

				go.InitializeForEvaluation(new FormulaContext(this._scriptEng), attributes,
										   ( ( cachedGo != null ) ? cachedGo.State : null ));
			}
		}
	}
}

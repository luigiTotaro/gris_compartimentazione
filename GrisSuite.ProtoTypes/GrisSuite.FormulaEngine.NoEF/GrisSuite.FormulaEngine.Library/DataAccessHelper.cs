﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.Library
{
	public class DataAccessHelper
	{
		public static IList<EvaluablePackage> GetAllObjects ()
		{
			var factory = new ObjectFactory();

			int serverMinuteTimeout;
			IParameter param = factory.GetParameter().GetByName("ServerMinuteTimeout");
			if ( param == null || !int.TryParse(param.Value, out serverMinuteTimeout) )
			{
				serverMinuteTimeout = 60;
			}

			var objs = GrisObject.GetAll();
			GrisObjectsCache.Instance.Reset();
			GrisObjectsCache.Instance.AddObjects(objs);
			ObjectContext.AreAllObjectsInCache = true;
			return objs.ConvertAll(go =>
			{
				if ( go is Server )
				{
					return new EvaluablePackage(go, new Dictionary<string, object>
			                                                                {
			                                                                    {"serverMinuteTimeout", serverMinuteTimeout}
			                                                                });
				}

				return new EvaluablePackage(go, null);
			});
		}

		public static void SyncObjects ()
		{
			try
			{
				GrisObject.SyncObjects();
			}
			catch (Exception exc)
			{
				//TODO: Log
			}
		}

		public static void SaveBranch ( IStorable branchRoot )
		{
			try
			{
				ObjectContext.SaveBranch(branchRoot);
			}
			catch ( Exception exc )
			{
				//TODO: Log
			}
		}
	}
}

﻿
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Model
{
	public class ObjectFactory : IObjectFactory
	{
		public IAttribute GetAttribute ()
		{
			return new Entities.Attribute();
		}

		public IObjectAttribute GetObjectAttribute ()
		{
			return new Entities.ObjectAttribute();
		}

		public IGrisObject GetGrisObject ()
		{
			return new GrisObject();
		}

		public IServer GetServer ()
		{
			return new Entities.Server();
		}

		public IParameter GetParameter ()
		{
			return new Entities.Parameter();
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using GrisSuite.FormulaEngine.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	public class GrisObjectsCache
	{
		private static readonly object _locker = new object();
		private static volatile GrisObjectsCache _instance;
		private readonly IDictionary<Guid, IGrisObject> _objsDict = new Dictionary<Guid, IGrisObject>(50000);
		private readonly IDictionary<Guid, IDictionary<string, Guid>> _parentsDict = new Dictionary<Guid, IDictionary<string, Guid>>(50000);
		private readonly IDictionary<Guid, IDictionary<string, List<Guid>>> _childrenDict = new Dictionary<Guid, IDictionary<string, List<Guid>>>(50000);

		private GrisObjectsCache () { }

		public static GrisObjectsCache Instance
		{
			get
			{
				if ( _instance == null )
				{
					lock ( _locker )
					{
						if ( _instance == null )
						{
							_instance = new GrisObjectsCache();
						}
					}
				}

				return _instance;
			}
		}

		public bool HasObject ( Guid id )
		{
			return this._objsDict.ContainsKey(id);
		}

		public IGrisObject Get ( Guid id )
		{
			if ( this._objsDict.ContainsKey(id) )
			{
				return this._objsDict[id];
			}

			return null;
		}

		public IGrisObject GetParent ( IGrisObject grisObject, string propertyName )
		{
			IDictionary<string, Guid> propParent;
			this._parentsDict.TryGetValue(grisObject.Id, out propParent);
			if ( propParent != null )
			{
				Guid parentId;
				propParent.TryGetValue(propertyName, out parentId);
				if ( parentId != Guid.Empty )
				{
					return this._objsDict[parentId];					
				}
			}

			//return ( from propParents in _parentsDict[grisObject.Id]
			//         join grisObjectEntry in _objsDict on propParents.Value equals grisObjectEntry.Key
			//         where propParents.Key == propertyName
			//         select grisObjectEntry.Value ).SingleOrDefault();

			return null;
		}

		public List<IGrisObject> GetChildren ( IGrisObject grisObject, string propertyName )
		{
			IDictionary<string, List<Guid>> propChildren;
			this._childrenDict.TryGetValue(grisObject.Id, out propChildren);
			if ( propChildren != null )
			{
				List<Guid> childrenIds;
				propChildren.TryGetValue(propertyName, out childrenIds);
				if ( childrenIds != null )
				{
					var objects = new List<IGrisObject>();
					foreach (var childrenId in childrenIds)
					{
						objects.Add(this._objsDict[childrenId]);
					}
					return objects;
				}
			}

			//return ( from propChildren in _childrenDict[grisObject.Id]
			//         from childId in propChildren.Value
			//         join grisObjectEntry in _objsDict on childId equals grisObjectEntry.Key
			//         where propChildren.Key == propertyName
			//         select grisObjectEntry.Value ).ToList();

			return new List<IGrisObject>(0);
		}

		public void AddObject ( IGrisObject grisObject )
		{
			foreach ( var memberInfo in grisObject.GetType().GetMembers() )
			{
				if ( memberInfo is PropertyInfo )
				{
					foreach ( var customAttribute in memberInfo.GetCustomAttributes(false) )
					{
						if ( customAttribute is ParentIdPropertyAttribute )
						{
							var property = (PropertyInfo) memberInfo;
							var parentAttribute = (ParentIdPropertyAttribute) customAttribute;

							object propValue = property.GetValue(grisObject, null);

							if ( propValue != null )
							{
								Guid parentId;
								if ( !Guid.TryParse(propValue.ToString(), out parentId) )
								{
									throw new InvalidCastException(string.Format("Il tipo restituito dalla proprietà {0} dell' oggetto {1} non è di tipo Guid.", property.Name, grisObject.GetType()));
								}

								lock ( _locker )
								{
									IDictionary<string, Guid> propParentId;
									if ( this._parentsDict.TryGetValue(grisObject.Id, out propParentId) )
									{
										propParentId[parentAttribute.ParentProperty] = parentId;
									}
									else
									{
										this._parentsDict.Add(grisObject.Id, new Dictionary<string, Guid> { { parentAttribute.ParentProperty, parentId } });
									}
								}

								lock ( _locker )
								{
									IDictionary<string, List<Guid>> propChildrenIdList;
									if ( this._childrenDict.TryGetValue(parentId, out propChildrenIdList) )
									{
										List<Guid> childrenIdList;
										if ( propChildrenIdList.TryGetValue(parentAttribute.RelatedChildrenProperty, out childrenIdList) )
										{
											childrenIdList.Add(grisObject.Id);
										}
										else
										{
											propChildrenIdList.Add(parentAttribute.RelatedChildrenProperty, new List<Guid> { grisObject.Id });
										}
									}
									else
									{
										this._childrenDict.Add(parentId, new Dictionary<string, List<Guid>> { { parentAttribute.RelatedChildrenProperty, new List<Guid> { grisObject.Id } } });
									}
								}								
							}
						}
					}
				}
			}

			lock ( _locker )
			{
				this._objsDict[grisObject.Id] = grisObject;
			}
		}

		public void AddObjects ( IEnumerable<IGrisObject> grisObjects )
		{
			//foreach ( var grisObject in grisObjects ) this.AddObject(grisObject);

			Parallel.ForEach(grisObjects, this.AddObject);
		}

		public ICollection<IGrisObject> CachedObjects
		{
			get { return this._objsDict.Values; }
		}

		public void Reset ()
		{
			this._objsDict.Clear();
			this._parentsDict.Clear();
			this._childrenDict.Clear();
		}
	}
}

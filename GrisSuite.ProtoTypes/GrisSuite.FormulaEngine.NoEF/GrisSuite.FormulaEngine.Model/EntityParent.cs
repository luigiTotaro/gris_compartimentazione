﻿
using System;
using GrisSuite.FormulaEngine.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	public class EntityParent<TEntity> : IEntityEnd<TEntity>
	{
		private TEntity _value;
		private readonly string _propertyName;
		private readonly Func<TEntity> _entitiesGetter;

		public EntityParent ( string propertyName, TEntity entity )
		{
			if ( entity == null ) throw new ArgumentException("Il parametro 'entity' non può essere nullo", "entity");
			this._propertyName = propertyName;
			this.IsLoaded = true;
			this._value = entity;
		}

		public EntityParent ( string propertyName, Func<TEntity> entitiesGetter )
		{
			this._propertyName = propertyName;
			this._entitiesGetter = entitiesGetter;
		}

		public void Load ()
		{
			if ( !this.IsLoaded && this._entitiesGetter != null )
			{
				this.IsLoaded = true;
				this._value = this._entitiesGetter();
			}
		}

		public void Load ( IGrisObject grisObject, bool lookupInCacheOnly = false )
		{
			if ( !this.IsLoaded && grisObject != null )
			{
				this.IsLoaded = true;
				var cachedEntity = GrisObjectsCache.Instance.GetParent(grisObject, this._propertyName);
				if ( cachedEntity != null )
				{
					this._value = (TEntity) cachedEntity;
				}
				else
				{
					if ( !lookupInCacheOnly && this._entitiesGetter != null )
					{
						var dbEntity = this._entitiesGetter();
						if ( dbEntity != null )
						{
							GrisObjectsCache.Instance.AddObject((IGrisObject)dbEntity);
							this._value = dbEntity;
						}
					}
				}
			}
		}

		public bool IsLoaded { get; private set; }

		public TEntity Value
		{
			get { return this._value; }
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	public class EntityCollection<TEntity> : List<TEntity>, IEntityCollection<TEntity>
	{
		private readonly string _propertyName;
		private readonly Func<List<TEntity>> _entitiesGetter;

		public EntityCollection ( string propertyName, IEnumerable<TEntity> entities )
		{
			if ( entities == null ) throw new ArgumentException("Il parametro 'entities' non può essere nullo", "entities");
			this._propertyName = propertyName;
			this.IsLoaded = true;
			this.AddRange(entities);
		}

		public EntityCollection ( string propertyName, Func<List<TEntity>> entitiesGetter )
		{
			this._propertyName = propertyName;
			this._entitiesGetter = entitiesGetter;
		}

		public void Load ()
		{
			if ( !this.IsLoaded && this._entitiesGetter != null )
			{
				this.IsLoaded = true;
				this.AddRange(this._entitiesGetter());
			}
		}

		public void Load ( IGrisObject grisObject, bool lookupInCacheOnly = false )
		{
			if ( !this.IsLoaded && grisObject != null )
			{
				this.IsLoaded = true;
				var cachedEntities = GrisObjectsCache.Instance.GetChildren(grisObject, this._propertyName);
				if ( cachedEntities.Count > 0 )
				{
					this.AddRange(cachedEntities.ConvertAll(ent => (TEntity) ent));
				}
				else
				{
					if ( !lookupInCacheOnly && this._entitiesGetter != null )
					{
						var dbEntities = this._entitiesGetter();
						GrisObjectsCache.Instance.AddObjects(dbEntities.ConvertAll(dbe => (IGrisObject) dbe));
						this.AddRange(dbEntities);
					}
				}
			}
		}

		public bool IsLoaded { get; private set; }

		public TEntity Value
		{
			get { throw new NotImplementedException(); }
		}
	}
}

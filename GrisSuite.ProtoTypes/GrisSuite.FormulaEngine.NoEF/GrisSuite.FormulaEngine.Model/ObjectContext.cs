﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model
{
	public static class ObjectContext
	{
		public static bool AreAllObjectsInCache;

		public static List<IServer> GetServers ( bool lookupInCacheOnly = false )
		{
			var servers = from cachedObject in GrisObjectsCache.Instance.CachedObjects
						  where cachedObject is IServer
						  select (IServer) cachedObject;

			if ( servers.Count() == 0 && !lookupInCacheOnly )
			{
				servers = Entities.Server.GetAll();
			}

			return servers.ToList();
		}

		public static void SaveBranch ( IStorable branchRoot )
		{
			List<string> updateCommands = new List<string>(30000);

			FetchAndGetUpdate(branchRoot, updateCommands);

			if ( updateCommands.Count > 0 )
			{
				using ( TransactionScope transaction = new TransactionScope() )
				{
					new Ds().Entity().Exec(string.Join(Environment.NewLine, updateCommands));
					transaction.Complete();
				}
			}
		}

		private static void FetchAndGetUpdate ( IStorable node, IList<string> updateCommands )
		{
			if ( node != null )
			{
				updateCommands.Add(node.GetUpdateCommand());

				var storableNodes = node.StorableNodes;
				if ( storableNodes != null && storableNodes.Count > 0 )
				{
					foreach ( var subNode in storableNodes )
					{
						FetchAndGetUpdate(subNode, updateCommands);
					}
				}
			}
		}
	}
}

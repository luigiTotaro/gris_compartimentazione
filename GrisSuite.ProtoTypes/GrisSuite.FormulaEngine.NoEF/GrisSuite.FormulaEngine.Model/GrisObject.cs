﻿
using System;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Contracts.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model
{
	public class GrisObject : IGrisObject
	{
		private static readonly object _locker = new object();
		public event EventHandler NeedExternalInitialization;
		public event EventHandler<PropertyChangedEventArgs> PropertyChanged;

		public IGrisAttributeCollection Attributes { get; set; }

		public GrisObject ()
		{
			this.PropertyChanged += Object_PropertyChanged;
		}

		#region Entity Members

		/// <summary>
		/// Identificativo del GrisObject.
		/// </summary>
		private Guid _id;
		public Guid Id
		{
			get { return this._id; }
			set
			{
				this._id = value;
				this.RaisePropertyChanged("Id", value);
			}
		}

		/// <summary>
		/// Nome del GrisObject.
		/// </summary>
		private string _name;
		public string Name
		{
			get { return this._name; }
			set
			{
				this._name = value;
				this.RaisePropertyChanged("Name", value);
			}
		}

		/// <summary>
		/// Identificativo del GrisObject specifico per tipo.
		/// </summary>
		private long _specificId;
		public long SpecificId
		{
			get { return this._specificId; }
			set
			{
				this._specificId = value;
				this.RaisePropertyChanged("SpecificId", value);
			}
		}

		/// <summary>
		/// Identificativo del tipo di GrisObject.
		/// </summary>
		public int TypeId { get; set; }

		/// <summary>
		/// Severità dello stato.
		/// </summary>
		private int _sevLevel;
		public int SevLevel
		{
			get { return this._sevLevel; }
			set
			{
				this._sevLevel = value;
				this.RaisePropertyChanged("SevLevel", value);
			}
		}

		/// <summary>
		/// Severità di dettaglio dello stato.
		/// </summary>
		private int? _sevLevelDetail;
		public int? SevLevelDetail
		{
			get { return this._sevLevelDetail; }
			set
			{
				this._sevLevelDetail = value;
				this.RaisePropertyChanged("SevLevelDetail", value);
			}
		}

		private Guid? _parentId;
		/// <summary>
		/// Identificativo del GrisObject padre.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Parent", RelatedChildrenProperty = "Children")]
		public Guid? ParentId
		{
			get { return this._parentId; }
			set
			{
				this._parentId = value;
				this.RaisePropertyChanged("ParentId", value);
			}
		}

		/// <summary>
		/// Indica se è in mantenimento.
		/// </summary>
		public bool InMaintenance { get; set; }

		/// <summary>
		/// Severità dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		public int? SevLevelReal { get; set; }

		/// <summary>
		/// Severità di dettaglio dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		public int? SevLevelDetailReal { get; set; }

		/// <summary>
		/// Ultima severità dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		public int? SevLevelLast { get; set; }

		/// <summary>
		/// Ultima severità di dettaglio dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		public int? SevLevelDetailLast { get; set; }

		/// <summary>
		/// Stamp dell' ultimo aggiornamento.
		/// </summary>
		public Guid? LastUpdateGuid { get; set; }

		/// <summary>
		/// Indica se il GrisObject è escluso dal calcolo dello stato del padre.
		/// </summary>
		public bool ExcludeFromParentStatus { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public bool ForcedByUser { get; set; }

		#region Navigation Properties

		/// <summary>
		/// Collezione delle formule correlate.
		/// </summary>
		private IEntityCollection<IObjectFormula> _objectFormulas;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IEntityCollection<IObjectFormula> ObjectFormulas
		{
			get
			{
				this._objectFormulas.Load();
				return this._objectFormulas;
			}
			set
			{
				this._objectFormulas = value;
			}
		}

		private IEntityCollection<IGrisObject> _children;
		/// <summary>
		/// Collezione degli oggetti figlio.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IEntityCollection<IGrisObject> Children
		{
			get
			{
				this._children.Load(this, ObjectContext.AreAllObjectsInCache);
				return _children;
			}
			set { _children = value; }
		}

		/// <summary>
		/// Oggetto padre.
		/// </summary>
		private IGrisObject _parent;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IGrisObject Parent
		{
			get { return this._parent ?? ( this._parent = this.ParentReference.Value ); }
			set { this._parent = value; }
		}

		private IEntityEnd<IGrisObject> _parentRef;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IEntityEnd<IGrisObject> ParentReference
		{
			get
			{
				this._parentRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._parentRef;
			}
			set { this._parentRef = value; }
		}

		/// <summary>
		/// Collezione degli attributi.
		/// </summary>
		private IEntityCollection<IObjectAttribute> _objectAttributes;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IEntityCollection<IObjectAttribute> ObjectAttributes
		{
			get
			{
				this._objectAttributes.Load();
				return this._objectAttributes;
			}
			set
			{
				this._objectAttributes = value;
			}
		}

		#endregion

		#region Initialization

		protected void RaisePropertyChanged ( string propertyName, object propertyValue )
		{
			if ( this.PropertyChanged != null ) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName, propertyValue));
		}

		protected virtual void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._objectAttributes = new EntityCollection<IObjectAttribute>("ObjectAttributes", () => Entities.ObjectAttribute.GetObjectAttributesByGrisObjectId((Guid) e.NewValue));
							break;
						}
					case "ParentId":
						{
							this._children = new EntityCollection<IGrisObject>("Children", this.GetChildren);
							break;
						}
				}
			}
		}

		#endregion

		#endregion

		#region Builtin Severity properties

		public int Severity
		{
			get
			{
				int status = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("Severity") &&
					int.TryParse(this.Attributes["Severity"].ToString(), out status) )
				{
					return status;
				}
				return -1;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["Severity"] = value.ToString();

					lock ( _locker )
					{
						this.SevLevel = value;
					}

					this.IsSeverityEstabilished = true;
				}
			}
		}

		public ObjectSeverityDetail SeverityDetail
		{
			get
			{
				int detailedStatus = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("SeverityDetail") &&
					int.TryParse(this.Attributes["SeverityDetail"].ToString(), out detailedStatus) )
				{
					return (ObjectSeverityDetail) detailedStatus;
				}

				return NotAvailable.SeverityDetail;
			}
			set
			{
				if ( this.Attributes != null )
				{
					lock ( _locker )
					{
						this.Attributes["SeverityDetail"] = value.ToString();

						this.SevLevelDetail = value;

						this.IsSeverityEstabilished = true;
					}

					this.Severity = value.RelatedSeverity;
				}
			}
		}

		public bool ExcludedFromParentStatus
		{
			get
			{
				bool excludedFromParentStatus = false;
				if ( this.Attributes != null && this.Attributes.Contains("ExcludeFromParentStatus") &&
					 bool.TryParse(this.Attributes["ExcludeFromParentStatus"].ToString().ToLowerInvariant(), out excludedFromParentStatus) )
				{
					return excludedFromParentStatus;
				}

				return false;
			}
			set
			{
				if ( this.Attributes != null )
				{
					lock ( _locker )
					{
						this.Attributes["ExcludeFromParentStatus"] = value.ToString();
					}

					this.ExcludeFromParentStatus = value;
				}
			}
		}

		public Color Color
		{
			get
			{
				if ( this.Attributes != null &&
					this.Attributes.Contains("Color") )
				{
					try
					{
						return ColorTranslator.FromHtml(this.Attributes["Color"].ToString());
					}
					catch ( FormatException )
					{
						return Color.Empty;
					}
				}
				return Color.Empty;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["Color"] = ColorTranslator.ToHtml(value);

					// TODO: persistere l'attributo
				}
			}
		}

		public void ForceSeverity ( int severity )
		{
			this.Attributes.ForceAttribute("Severity", severity);
		}

		public void ForceSeverity ( ObjectSeverityDetail severityDetail )
		{
			this.Attributes.ForceAttribute("SeverityDetail", severityDetail);
		}

		#endregion

		#region Evaluation

		public IEvaluationContext EvaluationContext { get; set; }

		public Dictionary<string, object> EvaluationState { get; set; }

		public void Evaluate ()
		{
			this.Evaluate(false);
		}

		public void Evaluate ( bool force )
		{
			// se il calcolo non è forzato
			// non vengono valutati gli oggetti già valutati o in via di valutazione
			if ( ( !this.IsEvaluated && this != this.CallerObject ) || force )
			{
				if ( !this.IsInitialized ) this.Initialize();

				if ( this.EvaluationContext != null )
				{
					// l'oggetto da valutare imposta se stesso come oggetto che ha invocato la valutazione
					this.CallerObject = this;

					lock ( _locker )
					{
						if ( !this.IsEvaluated || force ) // double-checking
						{
							this.IsEvaluated = false;
							this.IsSeverityEstabilished = false;
							this.EvaluationContext.Evaluate(this, this.EvaluationState);
							this.IsEvaluated = true;
						}
					}
				}
			}
		}

		public void ForceEvaluationOnDependentObjects ()
		{
			foreach ( var dependentObject in ObjectDependencies.Instance.GetDependentObjects(this) )
			{
				dependentObject.Evaluate(true);
			}
		}

		public bool IsEvaluated { get; private set; }

		public bool IsSeverityEstabilished { get; private set; }

		public IEvaluableObject CallerObject { get; set; }

		public bool IsInitialized { get; private set; }

		public void Initialize ()
		{
			if ( this.NeedExternalInitialization != null ) this.NeedExternalInitialization(this, null);

			this.IsInitialized = true;
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<IAttribute> attributes )
		{
			return this.InitializeForEvaluation(evalContext, attributes, null);
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<IAttribute> attributes, Dictionary<string, object> state )
		{
			this.EvaluationContext = evalContext;

			if ( this.Attributes == null )
			{
				this.Attributes = new GrisAttributeCollection(this, attributes);
			}

			if ( this.ObjectFormulas != null )
			{
				this._customFormulas = new SortedSet<IEvaluationFormula>();
				foreach ( var objectFormula in this.ObjectFormulas )
				{
					var formula = new EvaluationFormula(objectFormula);
					if ( formula.IsDefault )
					{
						this._defaultEvaluationFormula = formula;
					}
					else
					{
						this._customFormulas.Add(formula);
					}
				}

				if ( this._defaultEvaluationFormula == null ) this._defaultEvaluationFormula = new EvaluationFormula(null);
			}

			this.EvaluationState = state;

			this.IsInitialized = true;
			return this;
		}

		#endregion

		#region Formulas

		private IEvaluationFormula _defaultEvaluationFormula;
		[DataMember]
		public IEvaluationFormula DefaultEvaluationFormula
		{
			get { return this._defaultEvaluationFormula; }
			set { this._defaultEvaluationFormula = value; }
		}

		private SortedSet<IEvaluationFormula> _customFormulas;
		[DataMember]
		public SortedSet<IEvaluationFormula> CustomEvaluationFormulas
		{
			get { return this._customFormulas; }
			set { this._customFormulas = value; }
		}

		#endregion

		#region Aggregations

		#region Count aggregation methods

		public int CountChildrenInSeverity ( int severity )
		{
			return this.Children.Count(c => c.Severity == severity);
		}

		public int CountChildrenInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return this.Children.Count(c => c.SeverityDetail == severityDetail);
		}

		public int CountChildrenAtLeastInSeverity ( int severity )
		{
			return this.Children.Count(c => c.Severity >= severity);
		}

		public int CountChildrenAtLeastInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return this.Children.Count(c => c.SeverityDetail >= severityDetail);
		}

		#endregion

		#region Get aggregation methods

		public int GetChildrenWorstSeverity ()
		{
			return this.Children.Max(c => c.Severity);
		}

		public ObjectSeverityDetail GetChildrenWorstSeverityDetail ()
		{
			return this.Children.Max(c => c.SeverityDetail);
		}

		public int GetChildrenBestSeverity ()
		{
			return this.Children.Min(c => c.Severity);
		}

		public ObjectSeverityDetail GetChildrenBestSeverityDetail ()
		{
			return this.Children.Min(c => c.SeverityDetail);
		}

		#endregion

		#region Boolean aggregation methods

		public bool IsAnyChildAtLeastInSeverity ( int severity )
		{
			return ( this.GetChildrenWorstSeverity() >= severity );
		}

		public bool IsAnyChildAtLeastInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return ( this.GetChildrenWorstSeverityDetail() >= severityDetail );
		}

		public bool AreAllChildrenAtLeastInSeverity ( int severity )
		{
			return ( this.CountChildrenAtLeastInSeverity(severity) == this.Children.Count );
		}

		public bool AreAllChildrenAtLeastInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return ( this.CountChildrenAtLeastInSeverity(severityDetail) == this.Children.Count );
		}

		#endregion

		#endregion

		#region Navigation methods

		public IGrisObject GetChildByName ( string name )
		{
			return ( from grisObject in this.Children where grisObject.Name == name select grisObject ).SingleOrDefault();
		}

		public TEntity GetChildByName<TEntity> ( string name ) where TEntity : IGrisObject
		{
			return (TEntity) this.GetChildByName(name);
		}

		public IList<IGrisObject> GetChildrenByName ( string name )
		{
			return ( from grisObject in this.Children where grisObject.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1 select grisObject ).ToList();
		}

		public IList<TEntity> GetChildrenByName<TEntity> ( string name ) where TEntity : IGrisObject
		{
			return this.GetChildrenByName(name).Cast<TEntity>().ToList();
		}

		#endregion

		#region Virtual Objects

		public IVirtualObject GetVirtualObjectByName ( string name )
		{
			return this.VirtualObjects.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<IVirtualObject> GetVirtualObjectsByName ( string name )
		{
			return this.VirtualObjects.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		private IEntityCollection<IVirtualObject> _virtualObjects;
		public IEntityCollection<IVirtualObject> VirtualObjects
		{
			get
			{
				return this._virtualObjects ?? ( this._virtualObjects = new EntityCollection<IVirtualObject>("VirtualObjects", this.Children.OfType <IVirtualObject>()) );
			}
			set { this._virtualObjects = value; }
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline],
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity,
    null as DevGNodID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName,
    s.SrvID, s.Name as SrvName, s.Host, s.FullHostName, s.IP, s.LastUpdate, s.LastMessageType, s.NodID as SrvNodID, s.ServerVersion, s.MAC, s.IsDeleted, ossn.ObjectStatusId as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    0 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join [servers] s on s.SrvID = os.ObjectId and os.ObjectTypeId = 5
	left join object_status ossn on s.NodID = ossn.ObjectId and ossn.ObjectTypeId = 3	
{0}{2}
union all
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    d.DevID, d.NodID as DevNodID, d.SrvID as DevSrvID, d.Name as DevName, d.[Type], d.SN, d.Addr, d.PortId as DevPortID, d.Active, d.Scheduled, d.DefinitionVersion, d.ProtocolDefinitionVersion,
    ds.SevLevel as ActualSevLevel, ds.[Description], ds.[Offline], 
    dt.DeviceTypeDescription, dt.Obsolete, dt.DefaultName, dt.PortType, dt.SnmpCommunity,
    osdn.ObjectStatusId as DevGNodID, osds.ObjectStatusId as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    1 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join devices d on d.DevID = os.ObjectId and os.ObjectTypeId = 6
	left join device_status ds on ds.DevID = d.DevID
	left join device_type dt on dt.DeviceTypeID = d.[Type]
	left join object_status osdn on d.NodID = osdn.ObjectId and osdn.ObjectTypeId = 3
	left join object_status osds on d.SrvID = osds.ObjectId and osds.ObjectTypeId = 5
{0}{1}
union all
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline],
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity,
    null as DevGNodID, null as DevGSrvID,
    v.VirtualObjectID, v.VirtualObjectName,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    2 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join virtual_objects v on v.VirtualObjectID = os.ObjectId and os.ObjectTypeId = 7
{0}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline],
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity,
    null as DevGNodID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    ns.NodeSystemsId, ns.NodId as SysNodID, ns.SystemId, sy.SystemDescription as SystemName, osnsn.ObjectStatusId as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    3 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join node_systems ns on ns.NodeSystemsId = os.ObjectId and os.ObjectTypeId = 4
	inner join device_type dt on ns.SystemId = dt.SystemID
	inner join devices d on ns.NodId = d.NodID and d.[Type] = dt.DeviceTypeID
	left join object_status osnsn on ns.NodId = osnsn.ObjectId and osnsn.ObjectTypeId = 3	
	left join systems sy on sy.SystemID = ns.SystemId
{0}{1}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline],
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity,
    null as DevGNodID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    n.NodID, n.ZonID as NodZonID, n.Name as NodName, n.Meters as NodMeters, osnz.ObjectStatusId as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    4 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join nodes n on n.NodID = os.ObjectId and os.ObjectTypeId = 3
	inner join devices d on n.NodID = d.NodID
	left join object_status osnz on n.ZonID = osnz.ObjectId and osnz.ObjectTypeId = 2
{0}{1}
union all
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline],
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity,
    null as DevGNodID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    z.ZonID, z.RegID as ZonRegID, z.Name as ZonName, oszr.ObjectStatusId as ZonGRegID,
    null as RegID, null as RegName,
    5 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join zones z on z.ZonID = os.ObjectId and os.ObjectTypeId = 2
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
	left join object_status oszr on z.RegID = oszr.ObjectId and oszr.ObjectTypeId = 1
{0}{1}
union all
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline],
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity,
    null as DevGNodID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    r.RegID, r.Name as RegName,
    6 as CustomOrder
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join regions r on r.RegID = os.ObjectId and os.ObjectTypeId = 1
	inner join zones z on r.RegID = z.RegID
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
{0}{1}
order by CustomOrder;
";

		public static List<IGrisObject> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "", "where ([Type] <> 'NoDevice' or [Type] is null)", "where os.SevLevelDetailId <> 11"); //TODO: Attenzione! Rivedere se l'esclusione degli storici ha senso (attualmente forza una lookup in db)
			return new Ds().Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public static IGrisObject GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id", "", "");
			return new Ds { { "Id", id } }.Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		public virtual IGrisObject GetParent ()
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @ParentId", "", "");
			return new Ds { { "ParentId", this.ParentId } }.Entity().Get<GrisObject, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		public virtual List<IGrisObject> GetChildren ()
		{
			string cmd = string.Format(MAIN_CMD, "where os.ParentObjectStatusId = @ParentId", "", "");
			return new Ds { { "ParentId", this.Id } }.Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public static void SyncObjects ()
		{
			new Ds().Entity().Exec("tf_InsPopulateObjectStatus");
		}

		internal static GrisObject Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();
				int type;
				int.TryParse(firstRow["ObjectTypeId"].ToString(), out type);

				switch ( type )
				{
					case 1:
						{
							return Entities.Region.Map(groupedEntity);
						}
					case 2:
						{
							return Entities.Zone.Map(groupedEntity);
						}
					case 3:
						{
							return Entities.Node.Map(groupedEntity);
						}
					case 4:
						{
							return Entities.NodeSystem.Map(groupedEntity);
						}
					case 5:
						{
							return Entities.Server.Map(groupedEntity);
						}
					case 6:
						{
							return Entities.Device.Map(groupedEntity);
						}
					case 7:
						{
							return Entities.VirtualObject.Map(groupedEntity);
						}
					default:
						{
							var go = new GrisObject();

							PopulateObject(firstRow, go, groupedEntity.Key);

							// attributes
							PopulateAttributes(groupedEntity, go);

							// formulas
							PopulateFormulas(groupedEntity, go);

							return go;
						}
				}
			}

			return null;
		}

		internal static void PopulateObject ( DataRow source, GrisObject grisObject, Guid key )
		{
			grisObject.Id = key;
			grisObject.SpecificId = source.GetInt64("ObjectId");
			grisObject.TypeId = source.GetInt32("ObjectTypeId");
			grisObject.SevLevel = source.GetInt32("SevLevel");
			grisObject.SevLevelDetail = source.GetNullableInt32("SevLevelDetailId");
			grisObject.SevLevelReal = source.GetNullableInt32("SevLevelReal");
			grisObject.SevLevelDetailReal = source.GetNullableInt32("SevLevelDetailIdReal");
			grisObject.SevLevelLast = source.GetNullableInt32("SevLevelLast");
			grisObject.SevLevelDetailLast = source.GetNullableInt32("SevLevelDetailIdLast");
			grisObject.ParentId = source.GetNullableGuid("ParentObjectStatusId");
			grisObject.InMaintenance = source.GetBoolean("InMaintenance");
			grisObject.ForcedByUser = source.GetBoolean("ExcludeFromParentStatus");
			grisObject.ForcedByUser = source.GetBoolean("ForcedByUser");
			grisObject.LastUpdateGuid = source.GetNullableGuid("LastUpdateGuid");
		}

		internal static void PopulateAttributes ( IGrouping<Guid, DataRow> groupedEntity, GrisObject grisObject )
		{
			var attrTypes = ( from gEnt in groupedEntity
							  group gEnt by gEnt.GetInt16("AttributeTypeId")
								  into attrTyps
								  select Entities.Attribute.Map(attrTyps) ).Distinct();

			var attrs = ( from gEnt in groupedEntity
						  where !gEnt.IsNull("AttributeId")
						  group gEnt by gEnt.GetInt64("AttributeId")
							  into attrz
							  select Entities.ObjectAttribute.Map(attrz) ).Distinct();

			foreach ( var objectAttribute in attrs )
			{
				IObjectAttribute oAttribute = objectAttribute;
				objectAttribute.GrisObject = grisObject;
				objectAttribute.Type = attrTypes.SingleOrDefault(at => at.Id == oAttribute.TypeId);
			}

			grisObject.ObjectAttributes = new EntityCollection<IObjectAttribute>("ObjectAttributes", attrs);
		}

		internal static void PopulateFormulas ( IGrouping<Guid, DataRow> groupedEntity, GrisObject grisObject )
		{
			var formulas = ( from gEnt in groupedEntity
							 group gEnt by Tuple.Create(gEnt.GetGuid("ObjectStatusId"), gEnt.GetByte("FormulaIndex"))
								 into formz
								 select Entities.ObjectFormula.Map(formz) ).Distinct().ToList();

			foreach ( var objectFormula in formulas )
			{
				IObjectFormula oFormula = objectFormula;
				oFormula.GrisObject = grisObject;
			}

			grisObject.ObjectFormulas = new EntityCollection<IObjectFormula>("ObjectFormulas", formulas);
		}

		#endregion

		#region Persistence

		public virtual IList<IStorable> StorableNodes
		{
			get
			{
				var stors = new List<IStorable>(300);
				if ( this.IsEvaluated ) stors.Add(this.Attributes);
				stors.AddRange(this.Children);
				return stors;
			}
		}

		public string GetUpdateCommand ()
		{
			const string UPD_CMD = "update object_status set SevLevelDetailId = {0}, SevLevel = {1} where ObjectStatusId = '{2}';";
			return string.Format(UPD_CMD,
							this.SevLevelDetail.HasValue ? this.SevLevelDetail.ToString() : "NULL",
							this.SevLevel,
							this.Id);
		}

		#endregion
	}
}

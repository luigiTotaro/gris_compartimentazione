﻿
using System.Runtime.Serialization;
using GrisSuite.FormulaEngine.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Model.DataAccess
{
	public class VirtualObject : GrisObject, IVirtualObject
	{
		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		[DataMember]
		public long VirtualObjectId { get; set; }
	}
}

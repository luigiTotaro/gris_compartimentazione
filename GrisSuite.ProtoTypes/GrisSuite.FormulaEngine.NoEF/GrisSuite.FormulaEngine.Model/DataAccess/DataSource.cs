﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace GrisSuite.FormulaEngine.Model.DataAccess
{
	interface IDataEntity
	{
		IDictionary<string, object> Parameters { get; set; }
		IEnumerable<T> Get<T> ( string command, Func<IDataReader, T> populateAction );
		IEnumerable<T> Get<T> ( string command, Func<IDataReader, T> populateAction, IDictionary<string, object> parameters );
		T Get<T> ( string command, T tds ) where T : DataSet;
		T Get<T> ( string command, T tds, IDictionary<string, object> parameters ) where T : DataSet;
		IEnumerable<TEntity> Get<TEntity, TKey> ( string command, string key, Func<IGrouping<TKey, DataRow>, TEntity> populateAction );
		IEnumerable<TEntity> Get<TEntity, TKey> ( string command, string key, Func<IGrouping<TKey, DataRow>, TEntity> populateAction, IDictionary<string, object> parameters );
		void Exec ( string command );
		void Exec ( string command, IDictionary<string, object> parameters );
	}

	sealed class DataSource : IDataEntity
	{
		public IDictionary<string, object> Parameters { get; set; }

		public IEnumerable<TEntity> Get<TEntity> ( string command, Func<IDataReader, TEntity> populateAction )
		{
			return this.Get(command, populateAction, this.Parameters);
		}

		public IEnumerable<TEntity> Get<TEntity> ( string command, Func<IDataReader, TEntity> populateAction, IDictionary<string, object> parameters )
		{
			using ( var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString) )
			using ( var cmd = new SqlCommand(command, conn) )
			{
				IDataReader reader = null;
				IList<TEntity> results = new List<TEntity>();

				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value)).ToArray());

				conn.Open();

				try
				{
					reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

					while ( reader.Read() )
					{
						results.Add(populateAction(reader));
					}

					return results;
				}
				finally
				{
					if ( reader != null ) reader.Dispose();
				}
			}
		}

		public TEntity Get<TEntity> ( string command, TEntity tds ) where TEntity : DataSet
		{
			return this.Get(command, tds, this.Parameters);
		}

		public TEntity Get<TEntity> ( string command, TEntity tds, IDictionary<string, object> parameters ) where TEntity : DataSet
		{
			using ( var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString) )
			using ( var cmd = new SqlCommand(command, conn) )
			{
				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value)).ToArray());

				IDataAdapter da = new SqlDataAdapter(cmd);

				da.Fill(tds);
				return tds;
			}
		}

		public IEnumerable<TEntity> Get<TEntity, TKey> ( string command, string key, Func<IGrouping<TKey, DataRow>, TEntity> populateAction )
		{
			return this.Get(command, key, populateAction, this.Parameters);
		}

		public IEnumerable<TEntity> Get<TEntity, TKey> ( string command, string key, Func<IGrouping<TKey, DataRow>, TEntity> populateAction, IDictionary<string, object> parameters )
		{
			using ( var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString) )
			using ( var cmd = new SqlCommand(command, conn) )
			{
				IDataAdapter adapter = new SqlDataAdapter(cmd);
				DataSet tempDs = new DataSet();

				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value)).ToArray());

				adapter.Fill(tempDs);
				//try
				//{
				//    adapter.Fill(tempDs);

				//}
				//catch ( Exception exc )
				//{
				//    string s = exc.Message;
				//    throw;
				//}

				var groupedEntities = (from DataRow row in tempDs.Tables[0].Rows//.AsParallel()
									  group row by (TKey) row[key] into groupedEntity
									  select groupedEntity);

				var results = new BlockingCollection<TEntity>();
				//var results = new List<TEntity>();

				Parallel.ForEach(groupedEntities, groupedEntry => results.Add(populateAction(groupedEntry)));

				//foreach ( var groupedEntity in groupedEntities )
				//{
				//    results.Add(populateAction(groupedEntity));
				//}

				return results.ToList();
			}
		}

		public void Exec ( string command )
		{
			this.Exec(command, this.Parameters);
		}

		public void Exec ( string command, IDictionary<string, object> parameters )
		{
			using ( var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString) )
			using ( var cmd = new SqlCommand(command, conn) )
			{
				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value)).ToArray());

				conn.Open();
				cmd.ExecuteNonQuery();
				conn.Close();
			}
		}
	}

	sealed class Ds : Dictionary<string, object>
	{
		public IDataEntity Entity ()
		{
			return new DataSource { Parameters = this };
		}
	}
}
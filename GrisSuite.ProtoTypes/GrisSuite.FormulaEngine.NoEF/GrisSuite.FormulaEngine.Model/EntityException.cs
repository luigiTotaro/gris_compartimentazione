﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Model
{
	[Serializable]
	public class EntityException : Exception
	{
		public EntityException ()
        {
        }

		public EntityException ( string message )
			: base(message)
        {
        }

		public EntityException ( string message, Exception inner )
			: base(message, inner)
        {
        }

		protected EntityException ( SerializationInfo info, StreamingContext context )
			: base(info, context)
        {
        }
	}
}

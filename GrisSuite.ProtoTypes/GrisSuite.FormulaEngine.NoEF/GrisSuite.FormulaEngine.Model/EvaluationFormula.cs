﻿
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Model
{
	public class EvaluationFormula : IEvaluationFormula
	{
		private readonly IEvaluableObject _owner;
		private readonly string _scriptPath;
		private readonly bool _isDefault;

		public EvaluationFormula ( IObjectFormula objectFormula )
		{
			if ( objectFormula != null )
			{
				_owner = objectFormula.GrisObject;
				_scriptPath = objectFormula.ScriptPath;
				_isDefault = objectFormula.Index == 0;				
			}
		}

		#region Implementation of IEvaluationFormula

		/// <summary>
		/// Il path in cui si trova il file di script per la determinazione degli attributi dell' oggetto.
		/// </summary>
		public string ScriptPath
		{
			get { return this._scriptPath; }
		}

		/// <summary>
		/// Indica se la formula è una formula di default.
		/// </summary>
		public bool IsDefault
		{
			get { return this._isDefault; }
		}

		/// <summary>
		/// Indica se la formula è in uno stato valido e può essere valutata.
		/// </summary>
		public bool CanEvaluate
		{
			get { return !string.IsNullOrEmpty(this.ScriptPath); }
		}

		/// <summary>
		/// Indica se sono avvenuti errori durante la valutazione della formula.
		/// </summary>
		public bool HasErrorInEvaluation
		{
			get { return !string.IsNullOrEmpty(this.EvaluationError); }
		}

		/// <summary>
		/// Restituisce l'errore avvenuto durante la valutazione della formula.
		/// </summary>
		public string EvaluationError { get; set; }

		/// <summary>
		/// Riferimento all' oggetto possessore della formula.
		/// </summary>
		public IEvaluableObject Owner
		{
			get { return this._owner; }
		}

		#endregion
	}
}
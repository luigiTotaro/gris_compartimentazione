﻿
using System;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class ObjectFormula : IObjectFormula
	{
		/// <summary>
		/// Identificativo del GrisObject associato.
		/// </summary>
		public Guid ObjectId { get; set; }

		/// <summary>
		/// Indice di Formula del GrisObject.
		/// </summary>
		public byte Index { get; set; }

		/// <summary>
		/// Path al file contenente lo script della Formula.
		/// </summary>
		public string ScriptPath { get; set; }

		/// <summary>
		/// Indice globale di Formula.
		/// </summary>
		public long FormulaGlobalIndex { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IGrisObject GrisObject { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IEntityCollection<IObjectAttribute> ComputedObjectAttributes { get; set; }

		#region Data Access
		#endregion

		internal static IObjectFormula Map ( IGrouping<Tuple<Guid, byte>, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.First();
				return new ObjectFormula
				{
					ObjectId = groupedEntity.Key.Item1,
					Index = groupedEntity.Key.Item2,
					ScriptPath = firstRow.GetNullableString("ScriptPath"),
					FormulaGlobalIndex = 0
				};
			}

			return null;
		}
	}
}

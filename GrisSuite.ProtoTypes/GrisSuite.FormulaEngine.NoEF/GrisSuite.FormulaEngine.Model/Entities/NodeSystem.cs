﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Contracts.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class NodeSystem : GrisObject, INodeSystem
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo specifico del NodeSystem.
		/// </summary>
		public long? NodeSystemId { get; set; }

		private Guid _nodeId;
		/// <summary>
		/// Identificativo della stazione correlata al NodeSystem.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Node", RelatedChildrenProperty = "Systems")]
		public Guid NodeId
		{
			get { return _nodeId; }
			set
			{
				_nodeId = value;
				this.RaisePropertyChanged("NodeId", value);
			}
		}

		/// <summary>
		/// Identificativo del sistema.
		/// </summary>
		public int SystemId { get; set; }

		#endregion

		#region Navigation Members

		private INode _node;
		/// <summary>
		/// Istanza della stazione relativa al NodeSystem.
		/// </summary>
		public INode Node
		{
			get { return this._node ?? ( this._node = this.NodeReference.Value ); }
			set { this._node = value; }
		}

		private IEntityEnd<INode> _nodeRef;
		public IEntityEnd<INode> NodeReference
		{
			get
			{
				this._nodeRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._nodeRef;
			}
			set { this._nodeRef = value; }
		}

		private IEntityCollection<IDevice> _devices;
		public IEntityCollection<IDevice> Devices
		{
			get
			{
				this._devices.Load(this, ObjectContext.AreAllObjectsInCache);
				return _devices;
			}
			set { _devices = value; }
		}

		#endregion

		#region Aggregation Members

		#region Devices

		public int CountDevicesInSeverity ( int severity )
		{
			return this.Devices.Count(d => d.ActualSevLevel == severity);
		}

		public IList<IDevice> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int GetWorstSeverityForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			var max = this.Devices.Where(d => deviceTypes.Contains(d.Type)).Max(d => d.ActualSevLevel);
			return max.HasValue ? max.Value : -1;
		}

		public IDevice GetDeviceByName ( string name )
		{
			return this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<IDevice> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
			return ( from device in this.Devices select device.Server ).Distinct().ToList();
		}

		public IList<IServer> GetMonitoringServersInSeverity ( int severity )
		{
			return ( from device in this.Devices where device.Server.SevLevel == severity select device.Server ).Distinct().ToList();
		}

		public IList<IServer> GetMonitoringServersInSeverity ( ObjectSeverityDetail detailedSeverity )
		{
			return ( from device in this.Devices
					 where device.Server.SevLevelDetail.HasValue &&
					 device.Server.SevLevelDetail.Value == detailedSeverity
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._devices = new EntityCollection<IDevice>("Devices", () => this.GetChildren().ConvertAll(go => (IDevice) go));
							break;
						}
					case "NodeId":
						{
							this._nodeRef = new EntityParent<INode>("Node", () => (INode) this.GetParent());
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    ns.NodeSystemsId, ns.NodId as SysNodID, ns.SystemId, sy.SystemDescription as SystemName, osnsn.ObjectStatusId as NodSysGNodID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join node_systems ns on ns.NodeSystemsId = os.ObjectId and os.ObjectTypeId = 4
	inner join device_type dt on ns.SystemId = dt.SystemID
	inner join devices d on ns.NodId = d.NodID and d.[Type] = dt.DeviceTypeID
	left join object_status osnsn on ns.NodId = osnsn.ObjectId and osnsn.ObjectTypeId = 3	
	left join systems sy on sy.SystemID = ns.SystemId
{0};
";

		public new static List<INodeSystem> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<INodeSystem, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static INodeSystem GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
			return new Ds { { "Id", id } }.Entity().Get<INodeSystem, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		internal new static NodeSystem Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var nodeSys = new NodeSystem();

				PopulateObject(firstRow, nodeSys, groupedEntity.Key);
				PopulateNodeSystem(firstRow, nodeSys);
				// attributes
				PopulateAttributes(groupedEntity, nodeSys);
				// formulas
				PopulateFormulas(groupedEntity, nodeSys);

				return nodeSys;
			}

			return null;
		}

		internal static void PopulateNodeSystem ( DataRow source, INodeSystem nodeSystem )
		{
			nodeSystem.NodeSystemId = source.GetInt64("NodeSystemsId");
			nodeSystem.NodeId = source.GetGuid("NodSysGNodID");
			nodeSystem.SystemId = source.GetInt32("SystemId");
			nodeSystem.Name = source.GetString("SystemName");
		}

		#endregion
	}
}

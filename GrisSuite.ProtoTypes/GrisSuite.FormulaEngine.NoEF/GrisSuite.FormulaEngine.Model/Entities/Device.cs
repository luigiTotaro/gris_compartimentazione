﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Device : GrisObject, IDevice
	{
		#region Primitive Members
		
		/// <summary>
		/// Il tipo della periferica.
		/// </summary>
		public string Type { get; set; }

		/// <summary>
		/// Numero Seriale della periferica.
		/// </summary>
		public string SerialNumber { get; set; }

		/// <summary>
		/// Indirizzo IP o seriale.
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Indica se la diagnostica è attiva.
		/// </summary>
		public byte? IsActive { get; set; }

		/// <summary>
		/// Indica se è schedulata.
		/// </summary>
		public byte? IsScheduled { get; set; }

		/// <summary>
		/// Versione della definizione.
		/// </summary>
		public string DefinitionVersion { get; set; }

		/// <summary>
		/// Versione della definizione di protocollo.
		/// </summary>
		public string ProtocolDefinitionVersion { get; set; }

		/// <summary>
		/// Severità restituita dalla periferia.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Descrizione.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Indica se la periferica è offline.
		/// </summary>
		public byte? IsOffline { get; set; }

		/// <summary>
		/// Descrizione del tipo di periferica.
		/// </summary>
		public string DeviceTypeDescription { get; set; }

		/// <summary>
		/// Url a cui risponde il Web Service che restituisce informazioni diagnostiche sulla periferica.
		/// </summary>
		public string WSUrlPattern { get; set; }

		/// <summary>
		/// Identificativo di Tecnologia.
		/// </summary>
		public int? TechnologyId { get; set; }

		/// <summary>
		/// Identificativo specifico di periferica.
		/// </summary>
		public long? DeviceId { get; set; }

		private Guid _serverId;
		/// <summary>
		/// Identificativo del Server.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Server", RelatedChildrenProperty = "Devices")]
		public Guid ServerId
		{
			get { return this._serverId; }
			set
			{
				this._serverId = value;
				this.RaisePropertyChanged("ServerId", value);
			}
		}

		private Guid _nodeSystemId;
		/// <summary>
		/// Identificativo del Sistema.
		/// </summary>
		[ParentIdProperty(ParentProperty = "System", RelatedChildrenProperty = "Devices")]
		public Guid NodeSystemId
		{
			get { return _nodeSystemId; }
			set
			{
				_nodeSystemId = value;
				this.RaisePropertyChanged("NodeSystemId", value);
			}
		}

		private Guid _nodeId;
		/// <summary>
		/// Identificativo della stazione.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Node", RelatedChildrenProperty = "Devices")]
		public Guid NodeId
		{
			get { return _nodeId; }
			set
			{
				_nodeId = value;
				this.RaisePropertyChanged("NodeId", value);
			}
		}

		public Guid? PortId { get; set; }

		#endregion

		#region Navigation Members

		private IServer _server;
		/// <summary>
		/// Istanza del Server che monitora la periferica.
		/// </summary>
		public IServer Server
		{
			get { return this._server ?? ( this._server = this.ServerReference.Value ); }
			set { this._server = value; }
		}

		private IEntityEnd<IServer> _serverRef;
		public IEntityEnd<IServer> ServerReference
		{
			get
			{
				this._serverRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._serverRef;
			}
			set { this._serverRef = value; }
		}

		private INodeSystem _system;
		/// <summary>
		/// Istanza del Sistema sotto cui è compresa la periferica.
		/// </summary>
		public INodeSystem System
		{
			get { return this._system ?? ( this._system = this.SystemReference.Value ); }
			set { this._system = value; }
		}

		private IEntityEnd<INodeSystem> _systemRef;
		public IEntityEnd<INodeSystem> SystemReference
		{
			get
			{
				this._systemRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._systemRef;
			}
			set { this._systemRef = value; }
		}

		private INode _node;
		/// <summary>
		/// Istanza della Stazione sotto cui è compresa la periferica.
		/// </summary>
		public INode Node
		{
			get { return this._node ?? ( this._node = this.NodeReference.Value ); }
			set { this._node = value; }
		}

		private IEntityEnd<INode> _nodeRef;
		public IEntityEnd<INode> NodeReference
		{
			get
			{
				this._nodeRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._nodeRef;
			}
			set { this._nodeRef = value; }
		}

		public IList<IStream> Streams { get; set; }
		public IPort Port { get; set; }

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "ServerId":
						{
							this._serverRef = new EntityParent<IServer>("Server", () => Entities.Server.GetById((Guid) e.NewValue));
							break;
						}
				}
			}
		}

		#endregion

		#region Aggregation Members

		#region Streams

		public IStream GetStreamByName ( string name )
		{
			return this.GetChildByName(name);
		}

		public IList<IStream> GetStreamsByName ( string name )
		{
			return this.Streams.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		// necessario per python
		public new IStream GetChildByName ( string name )
		{
			return this.Streams.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		#endregion

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    d.DevID, d.NodID as DevNodID, d.SrvID as DevSrvID, d.Name as DevName, d.[Type], d.SN, d.Addr, d.PortId as DevPortID, d.Active, d.Scheduled, d.DefinitionVersion, d.ProtocolDefinitionVersion,
    ds.SevLevel as ActualSevLevel, ds.[Description], ds.[Offline], 
    dt.DeviceTypeDescription, dt.Obsolete, dt.DefaultName, dt.PortType, dt.SnmpCommunity,
    osdn.ObjectStatusId as DevGNodID, osds.ObjectStatusId as DevGSrvID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join devices d on d.DevID = os.ObjectId and os.ObjectTypeId = 6
	left join device_status ds on ds.DevID = d.DevID
	left join device_type dt on dt.DeviceTypeID = d.[Type]
	left join object_status osdn on d.NodID = osdn.ObjectId and osdn.ObjectTypeId = 3
	left join object_status osds on d.SrvID = osds.ObjectId and osds.ObjectTypeId = 5
{0};
";

		public new static List<IDevice> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "where [Type] <> 'NoDevice'");
			return new Ds().Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static IDevice GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, @"
where os.ObjectStatusId = @Id
	and [Type] <> 'NoDevice'
");
			return new Ds { { "Id", id } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		public static List<IDevice> GetDevicesByServer ( Guid srvId )
		{
			string cmd = string.Format(MAIN_CMD, "where osds.ObjectStatusId = @ServerId and d.[Type] <> 'NoDevice'");
			return new Ds { { "ServerId", srvId } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public static List<IDevice> GetDevicesByNode ( Guid nodeId )
		{
			string cmd = string.Format(MAIN_CMD, "where osdn.ObjectStatusId = @NodeId and d.[Type] <> 'NoDevice'");
			return new Ds { { "NodeId", nodeId } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public override List<IGrisObject> GetChildren ()
		{
			throw new NotSupportedException();
		}

		internal new static Device Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var dev = new Device();

				PopulateObject(firstRow, dev, groupedEntity.Key);
				PopulateDevice(firstRow, dev);
				// attributes
				PopulateAttributes(groupedEntity, dev);
				// formulas
				PopulateFormulas(groupedEntity, dev);

				return dev;
			}

			return null;
		}

		internal static void PopulateDevice ( DataRow source, IDevice device )
		{
			try
			{
				device.DeviceId = source.GetInt64("DevID");
				device.ServerId = source.GetGuid("DevGSrvID");
				device.NodeId = source.GetGuid("DevGNodID");
				device.NodeSystemId = source.GetGuid("ParentObjectStatusId");
				device.Name = source.GetString("DevName");
				device.Type = source.GetString("Type");
				device.SerialNumber = source.GetString("SN");
				device.Address = source.GetString("Addr");
				device.PortId = source.GetNullableGuid("DevPortID");
				device.IsActive = source.GetNullableByte("Active");
				device.IsScheduled = source.GetNullableByte("Scheduled");
				device.DefinitionVersion = source.GetString("DefinitionVersion");
				device.ProtocolDefinitionVersion = source.GetString("ProtocolDefinitionVersion");
				device.ActualSevLevel = source.GetInt32("ActualSevLevel");
				device.Description = source.GetString("Description");
				device.IsOffline = source.GetNullableByte("Offline");
			}
			catch ( InvalidCastException exc )
			{
				// log
			}
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				return new List<IStorable>(0);
			}
		}

		#endregion
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Server : GrisObject, IServer
	{

		#region Primitive Members

		/// <summary>
		/// Nome Host del server STLC.
		/// </summary>
		public string Host { get; set; }

		/// <summary>
		/// Nome Host completo del server STLC.
		/// </summary>
		public string FullHostName { get; set; }

		/// <summary>
		/// Nome Host completo del server STLC.
		/// </summary>
		public string IpAddress { get; set; }

		/// <summary>
		/// Data e ora dell' ultimo messaggio ricevuto.
		/// </summary>
		public DateTime? LastUpdate { get; set; }

		/// <summary>
		/// Tipo dell'ultimo messaggio ricevuto.
		/// </summary>
		public string LastMessageType { get; set; }

		/// <summary>
		/// Versione del server STLC.
		/// </summary>
		public string ServerVersion { get; set; }

		/// <summary>
		/// Indirizzo MAC del server STLC.
		/// </summary>
		public string MacAddress { get; set; }

		/// <summary>
		/// Indica se il server STLC è stato cancellato.
		/// </summary>
		public bool? IsDeleted { get; set; }

		/// <summary>
		/// Identificativo specifico del server STLC.
		/// </summary>
		public long ServerId { get; set; }

		private Guid? _nodeId;
		/// <summary>
		/// Identificativo della stazione in cui si trova il server STLC.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Node", RelatedChildrenProperty = "Servers")]
		public Guid? NodeId
		{
			get
			{
				return this._nodeId;
			}
			set
			{
				this._nodeId = value;
				this.RaisePropertyChanged("NodeId", value);
			}
		}

		#endregion

		#region Navigation Members

		private INode _node;
		/// <summary>
		/// Istanza della stazione in cui si trova il server STLC.
		/// </summary>
		public INode Node
		{
			get { return this._node ?? ( this._node = this.NodeReference.Value ); }
			set { _node = value; }
		}

		private IEntityEnd<INode> _nodeRef;
		public IEntityEnd<INode> NodeReference
		{
			get
			{
				this._nodeRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._nodeRef;
			}
			set { this._nodeRef = value; }
		}


		/// <summary>
		/// Istanze delle periferiche collegate al server STLC.
		/// </summary>
		private IEntityCollection<IDevice> _devices;
		public IEntityCollection<IDevice> Devices
		{
			get
			{
				this._devices.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._devices;
			}
			set { this._devices = value; }
		}

		#endregion

		#region Aggregation Members

		#region Servers

		public bool HasDuplicateIpAddress ()
		{
			if ( string.IsNullOrEmpty(this.IpAddress) ) return false;

			//int sameIpCount = Server.GetServersWithIpAddress(this.IpAddress).Count();
			int sameIpCount = ObjectContext.GetServers(ObjectContext.AreAllObjectsInCache).Count(srv => (srv.IpAddress == null ? "" : srv.IpAddress.Trim()) == this.IpAddress.Trim());

			if ( sameIpCount == 0 ) throw new EntityException(string.Format("Nessun server trovato nella tabella servers con lo stesso indirizzo IP dell'oggetto corrente. Id: {0} e IP: {1}", this.Id, this.IpAddress));

			return ( sameIpCount > 1 );
		}

		#endregion

		#region Devices

		public IList<IDevice> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int GetWorstSeverityForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			var max = this.Devices.Where(d => deviceTypes.Contains(d.Type)).Max(d => d.ActualSevLevel);
			return max.HasValue ? max.Value : -1;
		}

		public IDevice GetDeviceByName ( string name )
		{
			return this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<IDevice> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#endregion

		#region Initialization

		protected override void  Object_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
 			 base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._devices = new EntityCollection<IDevice>("Devices", () => Device.GetDevicesByServer((Guid) e.NewValue));
 							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    s.SrvID, s.Name as SrvName, s.Host, s.FullHostName, s.IP, s.LastUpdate, s.LastMessageType, s.NodID as SrvNodID, s.ServerVersion, s.MAC, s.IsDeleted,
    ossn.ObjectStatusId as SrvGNodID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join [servers] s on s.SrvID = os.ObjectId and os.ObjectTypeId = 5
	left join object_status ossn on s.NodID = ossn.ObjectId and ossn.ObjectTypeId = 3
{0};
";

		public new static List<IServer> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IServer, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static IServer GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id");
			return new Ds { { "Id", id } }.Entity().Get<IServer, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		public static List<IServer> GetServersByNode ( Guid nodeId )
		{
			string cmd = string.Format(MAIN_CMD, "where ossn.ObjectStatusId = @NodeId");
			return new Ds { { "NodeId", nodeId } }.Entity().Get<IServer, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public override IGrisObject GetParent ()
		{
			throw new NotSupportedException();
		}

		public override List<IGrisObject> GetChildren ()
		{
			return Device.GetDevicesByServer(this.Id).ConvertAll(dev => (IGrisObject) dev);
		}

		//public static List<IServer> GetServersWithIpAddress ( string ipAddress )
		//{
		//    string cmd = string.Format(MAIN_CMD, "where s.IP = @Ip");
		//    return new Ds { { "Ip", ipAddress } }.Entity().Get<IServer, Guid>(cmd, "ObjectStatusId", Map).ToList();
		//}

		internal new static Server Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var srv = new Server();

				PopulateObject(firstRow, srv, groupedEntity.Key);
				PopulateServer(firstRow, srv);
				// attributes
				PopulateAttributes(groupedEntity, srv);
				// formulas
				PopulateFormulas(groupedEntity, srv);

				return srv;
			}

			return null;
		}

		internal static void PopulateServer ( DataRow source, IServer server )
		{
			server.ServerId = source.GetInt64("SrvID");
			server.NodeId = source.GetNullableGuid("SrvGNodID");
			server.Name = source.GetNullableString("SrvName");
			server.Host = source.GetNullableString("Host");
			server.FullHostName = source.GetNullableString("FullHostName");
			server.IpAddress = source.GetNullableString("IP");
			server.LastUpdate = source.GetNullableDateTime("LastUpdate");
			server.LastMessageType = source.GetNullableString("LastMessageType");
			server.ServerVersion = source.GetNullableString("ServerVersion");
			server.MacAddress = source.GetNullableString("MAC");
			server.IsDeleted = source.GetNullableBoolean("IsDeleted");
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				return new List<IStorable>(0);
			}
		}

		#endregion
	}
}

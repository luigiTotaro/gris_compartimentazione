﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class ObjectAttribute : IObjectAttribute
	{
		/// <summary>
		/// Identificativo dell' attributo.
		/// </summary>
		public long Id { get; set; }

		/// <summary>
		/// Tipo di attributo.
		/// </summary>
		public short TypeId { get; set; }

		/// <summary>
		/// Identificativo del GrisObject associato.
		/// </summary>
		public Guid GrisObjectId { get; set; }

		/// <summary>
		/// Valore dell' attributo.
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Identificativo del GrisObject di Formula che ha determinato il valore.
		/// </summary>
		public Guid? ComputedByFormulaObjectStatusId { get; set; }

		/// <summary>
		/// Indice del GrisObject di Formula che ha determinato il valore.
		/// </summary>
		public byte? ComputedByFormulaIndex { get; set; }

		/// <summary>
		/// Istanza del Tipo di Attributo.
		/// </summary>
		private IAttribute _type;
		public IAttribute Type
		{
			get { return this._type ?? (this._type = Attribute.GetAttributeById(this.TypeId)); } 
			set { this._type = value; }
		}

		/// <summary>
		/// Istanza del GrisObject associato.
		/// </summary>
		private IGrisObject _grisObject;
		public IGrisObject GrisObject
		{
			get { return this._grisObject ?? ( this._grisObject = Model.GrisObject.GetById(this.GrisObjectId) ); }
			set { this._grisObject = value; }
		}

		/// <summary>
		/// Istanza della formula che ha determinato il valore.
		/// </summary>
		public IObjectFormula ComputedByObjectFormula
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		#region Data Access

		public List<IObjectAttribute> GetByGrisObjectId ()
		{
			return GetObjectAttributesByGrisObjectId(this.GrisObjectId);
		}

		public static List<IObjectAttribute> GetObjectAttributesByGrisObjectId ( Guid grisObjectId )
		{
			string cmd = @"
select
	oa.AttributeId, oa.AttributeTypeId, oa.ObjectStatusId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex
from object_attributes oa
where oa.ObjectStatusId = @ObjectId
							";
			return new Ds { { "ObjectId", grisObjectId } }.Entity().Get(cmd, Map).ToList();
		}

		#endregion

		#region Mapping

		internal static IObjectAttribute Map ( IDataReader reader )
		{
			return new ObjectAttribute
			{
				Id = reader.GetInt32("AttributeId"),
				TypeId = reader.GetInt16("AttributeTypeId"),
				GrisObjectId = reader.GetGuid("ObjectStatusId"),
				Value = reader.GetString("AttributeValue"),
				ComputedByFormulaObjectStatusId = reader.GetGuid("ComputedByFormulaObjectStatusId"),
				ComputedByFormulaIndex = reader.GetByte("ComputedByFormulaIndex"),
			};
		}

		internal static IObjectAttribute Map ( IGrouping<long, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.First();
				return new ObjectAttribute
				       	{
				       		Id = groupedEntity.Key,
							TypeId = firstRow.GetInt16("AttributeTypeId"),
							GrisObjectId = firstRow.GetGuid("ObjectStatusId"),
							Value = firstRow.GetString("AttributeValue"),
							ComputedByFormulaObjectStatusId = firstRow.GetNullableGuid("ComputedByFormulaObjectStatusId"),
							ComputedByFormulaIndex = firstRow.GetNullableByte("ComputedByFormulaIndex"),
				       	};
			}

			return null;
		}

		#endregion

	}
}

﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Contracts.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Zone: GrisObject, IZone
	{
		#region Primitive Members

		private Guid _regionId;
		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Region", RelatedChildrenProperty = "Zones")]
		public Guid RegionId
		{
			get { return _regionId; }
			set
			{
				this._regionId = value;
				this.RaisePropertyChanged("RegionId", value);
			}
		}

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public long ZoneId { get; set; }

		#endregion

		#region Navigation Members

		private IRegion _region;
		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IRegion Region
		{
			get { return this._region ?? ( this._region = this.RegionReference.Value ); }
			set { _region = value; }
		}

		private IEntityEnd<IRegion> _regionRef;
		public IEntityEnd<IRegion> RegionReference
		{
			get
			{
				this._regionRef.Load(this);
				return this._regionRef;
			}
			set { this._regionRef = value; }
		}

		private IEntityCollection<INode> _nodes;
		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IEntityCollection<INode> Nodes
		{
			get
			{
				this._nodes.Load(this, ObjectContext.AreAllObjectsInCache);
				return _nodes;
			}
			set { _nodes = value; }
		}

		#endregion

		#region Aggregation Members

		#region Nodes

		public int CountNodesInSeverity ( int severity )
		{
			return this.Nodes.Count(s => s.Severity == severity);
		}

		public INode GetNodeByName ( string name )
		{
			return this.Nodes.Where(n => n.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<INode> GetNodesByName ( string name )
		{
			return this.Nodes.Where(n => n.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
			return ( from node in this.Nodes
					 from device in node.Devices
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._nodes = new EntityCollection<INode>("Nodes", () => Node.GetNodesByZone((Guid) e.NewValue));
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    z.ZonID, z.RegID as ZonRegID, z.Name as ZonName, oszr.ObjectStatusId as ZonGRegID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join zones z on z.ZonID = os.ObjectId and os.ObjectTypeId = 2
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
	left join object_status oszr on z.RegID = oszr.ObjectId and oszr.ObjectTypeId = 1
{0};
";

		public new static List<IZone> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<IZone, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static IZone GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
			return new Ds { { "Id", id } }.Entity().Get<IZone, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		public static List<IZone> GetZonesByRegion ( Guid regionId )
		{
			string cmd = string.Format(MAIN_CMD, "where oszr.ObjectStatusId = @RegionId and d.[Type] <> 'NoDevice'");
			return new Ds { { "RegionId", regionId } }.Entity().Get<IZone, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		internal new static Zone Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var zone = new Zone();

				PopulateObject(firstRow, zone, groupedEntity.Key);
				PopulateNode(firstRow, zone);
				// attributes
				PopulateAttributes(groupedEntity, zone);
				// formulas
				PopulateFormulas(groupedEntity, zone);

				return zone;
			}

			return null;
		}

		internal static void PopulateNode ( DataRow source, IZone zone )
		{
			zone.ZoneId = source.GetInt64("ZonID");
			zone.RegionId = source.GetGuid("ZonGRegID");
			zone.Name = source.GetString("ZonName");
		}

		#endregion
	}
}

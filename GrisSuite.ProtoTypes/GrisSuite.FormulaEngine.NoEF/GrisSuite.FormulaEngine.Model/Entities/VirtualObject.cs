﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class VirtualObject : GrisObject, IVirtualObject
	{
		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		public long VirtualObjectId { get; set; }

		#region Data Access

		private const string MAIN_CMD = @"
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    v.VirtualObjectID, v.VirtualObjectName
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join virtual_objects v on v.VirtualObjectID = os.ObjectId and os.ObjectTypeId = 7
{0};
";

		public new static List<IVirtualObject> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IVirtualObject, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static IVirtualObject GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id");
			return new Ds { { "Id", id } }.Entity().Get<IVirtualObject, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		internal new static VirtualObject Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var vo = new VirtualObject();

				PopulateObject(firstRow, vo, groupedEntity.Key);
				PopulateVirtualObject(firstRow, vo);
				// attributes
				PopulateAttributes(groupedEntity, vo);
				// formulas
				PopulateFormulas(groupedEntity, vo);

				return vo;
			}

			return null;
		}

		internal static void PopulateVirtualObject ( DataRow source, IVirtualObject virtualObject )
		{
			try
			{
				virtualObject.VirtualObjectId = source.GetInt64("VirtualObjectID");
				virtualObject.Name = source.GetString("VirtualObjectName");
			}
			catch ( InvalidCastException exc )
			{
				// log
			}
		}

		#endregion
	}
}

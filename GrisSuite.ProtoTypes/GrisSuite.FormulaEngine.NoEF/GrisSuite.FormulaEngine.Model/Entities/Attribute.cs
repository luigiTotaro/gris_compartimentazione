﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Attribute : IAttribute
	{
		/// <summary>
		/// Identificativo dell' attributo.
		/// </summary>
		public short Id { get; set; }

		/// <summary>
		/// Nome dell' attributo.
		/// </summary>
		public string Name { get; set; }

		public IEntityCollection<IObjectAttribute> ObjectAttributes
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		#region Data Access

		public IList<IAttribute> GetAll ()
		{
			return GetAllAttributes();
		}

		public static IList<IAttribute> GetAllAttributes ()
		{
			return new Ds().Entity().Get("select AttributeTypeId, AttributeName from object_attribute_types order by AttributeName", Map).ToList();
		}

		public IAttribute GetById ( short id )
		{
			return GetAttributeById(id);
		}

		public static IAttribute GetAttributeById ( short id )
		{
			return new Ds { { "AttributeTypeId", id } }.Entity().Get("select AttributeTypeId, AttributeName from object_attribute_types where AttributeTypeId = @AttributeTypeId", Map).SingleOrDefault();
		}

		#endregion

		#region Mapping

		internal static IAttribute Map ( IDataReader reader )
		{
			return new Attribute
			{
				Id = reader.GetInt16(reader.GetOrdinal("AttributeTypeId")),
				Name = reader.GetString(reader.GetOrdinal("AttributeName"))
			};
		}

		internal static IAttribute Map ( IGrouping<short, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.First();
				return new Attribute
				{
					Id = groupedEntity.Key,
					Name = firstRow.GetString("AttributeName")
				};
			}

			return null;
		}

		#endregion
	}
}

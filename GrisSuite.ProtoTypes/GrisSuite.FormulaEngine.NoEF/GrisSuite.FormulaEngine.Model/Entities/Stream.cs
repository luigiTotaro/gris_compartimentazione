﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Stream : GrisObject, IStream
	{
		/// <summary>
		/// Identificativo dello Stream.
		/// </summary>
		public int StreamId { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		public long DevId { get; set; }

		/// <summary>
		/// Visibilità dello Stream.
		/// </summary>
		public byte? Visible { get; set; }

		/// <summary>
		/// Dati binari dello Stream.
		/// </summary>
		public byte[] Data { get; set; }

		/// <summary>
		/// Data di ricezione dello Stream.
		/// </summary>
		public DateTime DateReceived { get; set; }

		/// <summary>
		/// Severità dello Stream come ricevuta dal collettore.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Indica se lo Stream è stato processato.
		/// </summary>
		public byte? Processed { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		public Guid DeviceId { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IDevice Device { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IEntityCollection<IStreamField> StreamFields { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IEntityCollection<IStreamHistory> StreamsHistory { get; set; }

		#region Streams

		public IStreamField GetStreamFieldByName ( string name )
		{
			return this.GetChildByName(name);
		}

		public IList<IStreamField> GetStreamFieldsByName ( string name )
		{
			return this.StreamFields.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		// necessario per python
		public new IStreamField GetChildByName ( string name )
		{
			return this.StreamFields.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		#endregion
	}
}

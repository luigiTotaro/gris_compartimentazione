﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Contracts.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Region : GrisObject, IRegion
	{
		/// <summary>
		/// Identificativo specifico del compartimento.
		/// </summary>
		public long RegionId { get; set; }

		private IEntityCollection<IZone> _zones;
		/// <summary>
		/// Istanze delle Linee appartenenti al compartimento.
		/// </summary>
		public IEntityCollection<IZone> Zones
		{
			get
			{
				this._zones.Load(this, ObjectContext.AreAllObjectsInCache);
				return _zones;
			}
			set { _zones = value; }
		}

		#region Aggregation Members

		#region Zones

		public int CountZonesInSeverity ( int severity )
		{
			return this.Zones.Count(z => z.Severity == severity);
		}

		public IZone GetZoneByName ( string name )
		{
			return this.Zones.Where(z => z.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<IZone> GetZonesByName ( string name )
		{
			return this.Zones.Where(z => z.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}


		#endregion

		#region Nodes
		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
			return ( from zone in this.Zones
					 from node in zone.Nodes
					 from device in node.Devices
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._zones = new EntityCollection<IZone>("Zones", () => Zone.GetZonesByRegion((Guid) e.NewValue));
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    r.RegID, r.Name as RegName
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join regions r on r.RegID = os.ObjectId and os.ObjectTypeId = 1
	inner join zones z on r.RegID = z.RegID
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
{0};
";

		public new static List<IRegion> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<IRegion, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static IRegion GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
			return new Ds { { "Id", id } }.Entity().Get<IRegion, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		internal new static Region Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var region = new Region();

				PopulateObject(firstRow, region, groupedEntity.Key);
				PopulateNode(firstRow, region);
				// attributes
				PopulateAttributes(groupedEntity, region);
				// formulas
				PopulateFormulas(groupedEntity, region);

				return region;
			}

			return null;
		}

		internal static void PopulateNode ( DataRow source, IRegion region )
		{
			region.RegionId = source.GetInt64("RegID");
			region.Name = source.GetString("RegName");
		}

		#endregion
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Contracts.Entities;
using GrisSuite.FormulaEngine.Contracts.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Node : GrisObject, INode
	{
		#region Primitive Members

		/// <summary>
		/// Chilometrica associata alla stazione.
		/// </summary>
		public int Meters { get; set; }

		/// <summary>
		/// Identificativo specifico della stazione.
		/// </summary>
		public long NodeId { get; set; }

		private Guid _zoneId;
		/// <summary>
		/// Identificativo della linea a cui appartiene la stazione.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Zone", RelatedChildrenProperty = "Nodes")]
		public Guid ZoneId
		{
			get { return this._zoneId; }
			set
			{
				this._zoneId = value;
				this.RaisePropertyChanged("ZoneId", value);
			}
		}

		#endregion

		#region Navigation Members

		private IZone _zone;
		/// <summary>
		/// Istanza della linea a cui appartiene la stazione.
		/// </summary>
		public IZone Zone
		{
			get { return this._zone ?? ( this._zone = this.ZoneReference.Value ); }
			set { _zone = value; }
		}

		private IEntityEnd<IZone> _zoneRef;
		public IEntityEnd<IZone> ZoneReference
		{
			get
			{
				this._zoneRef.Load(this, ObjectContext.AreAllObjectsInCache);
				return this._zoneRef;
			}
			set { this._zoneRef = value; }
		}

		private IEntityCollection<INodeSystem> _systems;
		/// <summary>
		/// Istanze dei Sistemi presenti nella stazione.
		/// </summary>
		public IEntityCollection<INodeSystem> Systems
		{
			get
			{
				this._systems.Load(this, ObjectContext.AreAllObjectsInCache);
				return _systems;
			}
			set { _systems = value; }
		}

		private IEntityCollection<IServer> _servers;
		/// <summary>
		/// Istanze dei Server presenti nella stazione.
		/// </summary>
		public IEntityCollection<IServer> Servers
		{
			get
			{
				this._servers.Load(this, ObjectContext.AreAllObjectsInCache);
				return _servers;
			}
			set { _servers = value; }
		}

		private IEntityCollection<IDevice> _devices;
		/// <summary>
		/// Istanze delle Periferiche presenti nella stazione.
		/// </summary>
		public IEntityCollection<IDevice> Devices
		{
			get
			{
				this._devices.Load(this, ObjectContext.AreAllObjectsInCache);
				return _devices;
			}
			set { _devices = value; }
		}

		#endregion

		#region Aggregation Members

		#region NodeSystems

		public int CountSystemsInSeverity ( int severity )
		{
			return this.Systems.Count(s => s.Severity == severity);
		}

		public INodeSystem GetSystemByName ( string name )
		{
			return this.Systems.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<INodeSystem> GetSystemsByName ( string name )
		{
			return this.Systems.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Devices

		public IList<IDevice> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int GetWorstSeverityForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			var max = this.Devices.Where(d => deviceTypes.Contains(d.Type)).Max(d => d.ActualSevLevel);
			return max.HasValue ? max.Value : -1;
		}

		public IDevice GetDeviceByName ( string name )
		{
			return this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<IDevice> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
			return ( from device in this.Devices select device.Server ).Distinct().ToList();
		}

		public IList<IServer> GetMonitoringServersInSeverity ( int severity )
		{
			return ( from device in this.Devices where device.Server.SevLevel == severity select device.Server ).Distinct().ToList();
		}

		public IList<IServer> GetMonitoringServersInSeverity ( ObjectSeverityDetail detailedSeverity )
		{
			return ( from device in this.Devices
					 where device.Server.SevLevelDetail.HasValue &&
					 device.Server.SevLevelDetail.Value == detailedSeverity
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._devices = new EntityCollection<IDevice>("Devices", () => Device.GetDevicesByNode((Guid) e.NewValue));
							this._systems = new EntityCollection<INodeSystem>("Systems", () => this.GetChildren().ConvertAll(sys => (INodeSystem) sys));
							this._servers = new EntityCollection<IServer>("Servers", () => Server.GetServersByNode((Guid) e.NewValue));
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, 
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    n.NodID, n.ZonID as NodZonID, n.Name as NodName, n.Meters as NodMeters, osnz.ObjectStatusId as NodGZonID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join nodes n on n.NodID = os.ObjectId and os.ObjectTypeId = 3
	inner join devices d on n.NodID = d.NodID
	left join object_status osnz on n.ZonID = osnz.ObjectId and osnz.ObjectTypeId = 2
{0};
";

		public new static List<INode> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<INode, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		public new static INode GetById ( Guid id )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
			return new Ds { { "Id", id } }.Entity().Get<INode, Guid>(cmd, "ObjectStatusId", Map).SingleOrDefault();
		}

		public static List<INode> GetNodesByZone ( Guid zoneId )
		{
			string cmd = string.Format(MAIN_CMD, "where osnz.ObjectStatusId = @ZoneId and d.[Type] <> 'NoDevice'");
			return new Ds { { "ZoneId", zoneId } }.Entity().Get<INode, Guid>(cmd, "ObjectStatusId", Map).ToList();
		}

		internal new static Node Map ( IGrouping<Guid, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var node = new Node();

				PopulateObject(firstRow, node, groupedEntity.Key);
				PopulateNode(firstRow, node);
				// attributes
				PopulateAttributes(groupedEntity, node);
				// formulas
				PopulateFormulas(groupedEntity, node);

				return node;
			}

			return null;
		}

		internal static void PopulateNode ( DataRow source, INode node )
		{
			node.NodeId = source.GetInt64("NodID");
			node.ZoneId = source.GetGuid("NodGZonID");
			node.Name = source.GetString("NodName");
			node.Meters = source.GetInt32("NodMeters");
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				var stors = new List<IStorable>(300);
				if ( this.IsEvaluated ) stors.Add(this.Attributes);
				stors.AddRange(this.Children);
				stors.AddRange(this.Servers);
				return stors;
			}
		}

		#endregion
	}
}

﻿
using System;

namespace GrisSuite.FormulaEngine.Model
{
	internal class ParentIdPropertyAttribute : Attribute
	{
		public string ParentProperty { get; set; }
		public string RelatedChildrenProperty { get; set; }
	}
}

﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	public class ObjectDependencies
	{
		private static readonly object _locker = new object();
		private static volatile ObjectDependencies _instance;
		private readonly IDictionary<IEvaluableObject, IList<IEvaluableObject>> _depenDict = new Dictionary<IEvaluableObject, IList<IEvaluableObject>>(40000);
		
		private ObjectDependencies() {}

		public static ObjectDependencies Instance
		{
			get
			{
				if ( _instance == null )
				{
					lock ( _locker )
					{
						if ( _instance == null )
						{
							_instance = new ObjectDependencies();
						}
					}
				}

				return _instance;
			}
		}

		public IList<IEvaluableObject> GetDependentObjects ( IEvaluableObject evaluableObject )
		{
			if ( !this._depenDict.ContainsKey(evaluableObject) ) return new List<IEvaluableObject>(0);
			return this._depenDict[evaluableObject];
		}

		public void AddObjectDependencies ( IEvaluableObject evaluableObject, IEvaluableObject dependantObject )
		{
			if ( this._depenDict.ContainsKey(evaluableObject) )
			{
				var dependantObjects = this.GetDependentObjects(evaluableObject);
				if ( !dependantObjects.Contains(dependantObject) )
				{
					lock ( _locker )
					{
						dependantObjects.Add(dependantObject);
						this._depenDict[evaluableObject] = dependantObjects;
					}
				}
			}
			else
			{
				lock ( _locker )
				{
					this._depenDict.Add(evaluableObject, new List<IEvaluableObject>{ dependantObject });
				}
			}
		}

		public void Reset ()
		{
			this._depenDict.Clear();
		}
	}
}

﻿
using System;
using System.Runtime.Serialization;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.Data.Model
{
	[Serializable]
	[DataContract]
	public class EvaluationFormula : IEvaluationFormula
	{
		private readonly ObjectFormula _dbFormula;

		public EvaluationFormula ( ObjectFormula dbFormula )
		{
			_dbFormula = dbFormula;
			this.Initialize();
		}

		public string ScriptPath { get; private set; }

		public bool IsDefault { get; private set; }

		public bool CanEvaluate { get; private set; }

		[DataMember]
		public bool HasErrorInEvaluation { get; private set; }

		private string _evaluationError;
		[DataMember]
		public string EvaluationError
		{
			get { return this._evaluationError; }
			set
			{
				if (string.IsNullOrEmpty(value)) return;
				this.HasErrorInEvaluation = true;
				this._evaluationError = value;
			}
		}

		public IEvaluableObject Owner
		{
			get { return this._dbFormula.GrisObject; }
		}

		public void Initialize ()
		{
			if ( this._dbFormula != null )
			{
				this.ScriptPath = this._dbFormula.ScriptPath;
				this.IsDefault = this._dbFormula.Index == 0;
				this.CanEvaluate = !string.IsNullOrEmpty(this._dbFormula.ScriptPath);
			}
		}
	}
}
﻿
using System;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GrisSuite.Data.Model.DynamicEvaluation;
using GrisSuite.Data.Model.Severities;

namespace GrisSuite.Data.Model
{
	public abstract partial class GrisObject : IEvaluableObject, IStorable, INeedInitialization
	{
		private static readonly object _locker = new object();
		public event EventHandler NeedExternalInitialization;

		public GrisAttributeCollection Attributes { get; set; }

		#region Builtin Severity properties

		public int Severity
		{
			get
			{
				int status = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("Severity") &&
					int.TryParse(this.Attributes["Severity"].ToString(), out status) )
				{
					return status;
				}
				return -1;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["Severity"] = value.ToString();

					lock ( _locker )
					{
						this.SevLevel = value;
					}

					this.IsSeverityEstabilished = true;
				}
			}
		}

		public ObjectSeverityDetail SeverityDetail
		{
			get
			{
				int detailedStatus = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("SeverityDetail") &&
					int.TryParse(this.Attributes["SeverityDetail"].ToString(), out detailedStatus) )
				{
					return (ObjectSeverityDetail) detailedStatus;
				}

				return NotAvailable.SeverityDetail;
			}
			set
			{
				if ( this.Attributes != null )
				{
					lock ( _locker )
					{
						this.Attributes["SeverityDetail"] = value.ToString();

						this.SevLevelDetail = value;

						this.IsSeverityEstabilished = true;
					}

					this.Severity = value.RelatedSeverity;
				}
			}
		}

		public bool ExcludedFromParentStatus
		{
			get
			{
				bool excludedFromParentStatus = false;
				if ( this.Attributes != null && this.Attributes.Contains("ExcludeFromParentStatus") &&
					 bool.TryParse(this.Attributes["ExcludeFromParentStatus"].ToString().ToLowerInvariant(), out excludedFromParentStatus) )
				{
					return excludedFromParentStatus;
				}

				return false;
			}
			set
			{
				if ( this.Attributes != null )
				{
					lock ( _locker )
					{
						this.Attributes["ExcludeFromParentStatus"] = value.ToString();
					}

					this.ExcludeFromParentStatus = value;
				}
			}
		}

		public Color Color
		{
			get
			{
				if ( this.Attributes != null &&
					this.Attributes.Contains("Color") )
				{
					try
					{
						return ColorTranslator.FromHtml(this.Attributes["Color"].ToString());
					}
					catch (FormatException)
					{
						return Color.Empty;
					}
				}
				return Color.Empty;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["Color"] = ColorTranslator.ToHtml(value);

					// TODO: persistere l'attributo
				}
			}
		}

		public void ForceSeverity ( int severity )
		{
			this.Attributes.ForceAttribute("Severity", severity);
		}

		public void ForceSeverity ( ObjectSeverityDetail severityDetail )
		{
			this.Attributes.ForceAttribute("SeverityDetail", severityDetail);
		}

		#endregion

		#region Evaluation

		public IEvaluationContext EvaluationContext { get; set; }

		public Dictionary<string, object> EvaluationState { get; set; }

		public Entities DBContext { get; set; }

		public void Evaluate ()
		{
			this.Evaluate(false);
		}

		public void Evaluate ( bool force )
		{
			// se il calcolo non è forzato
			// non vengono valutati gli oggetti già valutati o in via di valutazione
			if ( (!this.IsEvaluated && this != this.CallerObject) || force )
			{
				if ( !this.IsInitialized ) this.Initialize();

				if ( this.EvaluationContext != null )
				{
					// l'oggetto da valutare imposta se stesso come oggetto che ha invocato la valutazione
					this.CallerObject = this;

					lock ( _locker )
					{
						if ( !this.IsEvaluated || force ) // double-checking
						{
							this.IsEvaluated = false;
							this.IsSeverityEstabilished = false;
							this.EvaluationContext.Evaluate(this, this.EvaluationState);
							this.IsEvaluated = true;
						}
					}
				}
			}
		}

		public void ForceEvaluationOnDependentObjects ()
		{
			foreach ( var dependentObject in ObjectDependencies.Instance.GetDependentObjects(this) )
			{
				dependentObject.Evaluate(true);
			}
		}

		public bool IsEvaluated { get; private set; }

		public bool IsSeverityEstabilished { get; private set; }

		public IEvaluableObject CallerObject { get; set; }

		public bool IsInitialized { get; private set; }

		public void Initialize ()
		{
			if ( this.NeedExternalInitialization != null ) this.NeedExternalInitialization(this, null);

			this.IsInitialized = true;
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<Attribute> attributes )
		{
			return this.InitializeForEvaluation(evalContext, attributes, null, null);
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<Attribute> attributes, Entities dbContext )
		{
			return this.InitializeForEvaluation(evalContext, attributes, dbContext, null);
		}

		// è necessaria un' inizializzazione successiva al costruttore perchè quando l'oggetto viene costruito le sue collections figlie non sono ancora disponibili
		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<Attribute> attributes, Entities dbContext, Dictionary<string, object> state )
		{
			this.EvaluationContext = evalContext;
			this.DBContext = dbContext;

			if ( this.Attributes == null )
			{
				this.Attributes = new GrisAttributeCollection(this, attributes);
			}

			if ( this.ObjectFormulas != null )
			{
				this._customFormulas = new SortedSet<EvaluationFormula>();
				foreach ( var objectFormula in this.ObjectFormulas )
				{
					var formula = new EvaluationFormula(objectFormula);
					if ( formula.IsDefault )
					{
						this._defaultEvaluationFormula = formula;
					}
					else
					{
						this._customFormulas.Add(formula);
					}
				}

				if ( this._defaultEvaluationFormula == null ) this._defaultEvaluationFormula = new EvaluationFormula(null);
			}

			this.EvaluationState = state;

			this.IsInitialized = true;
			return this;
		}

		#endregion

		#region Formulas

		private EvaluationFormula _defaultEvaluationFormula;
		[DataMember]
		public EvaluationFormula DefaultEvaluationFormula
		{
			get { return this._defaultEvaluationFormula; }
			set { this._defaultEvaluationFormula = value; }
		}

		private SortedSet<EvaluationFormula> _customFormulas;
		[DataMember]
		public SortedSet<EvaluationFormula> CustomEvaluationFormulas
		{
			get { return this._customFormulas; }
			set { this._customFormulas = value; }
		}

		#endregion

		#region Aggregations

		#region Count aggregation methods

		public int CountChildrenInSeverity ( int severity )
		{
			return this.Children.Count(c => c.Severity == severity);
		}

		public int CountChildrenInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return this.Children.Count(c => c.SeverityDetail == severityDetail);
		}

		public int CountChildrenAtLeastInSeverity ( int severity )
		{
			return this.Children.Count(c => c.Severity >= severity);
		}

		public int CountChildrenAtLeastInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return this.Children.Count(c => c.SeverityDetail >= severityDetail);
		}

		#endregion

		#region Get aggregation methods

		public int GetChildrenWorstSeverity ()
		{
			return this.Children.Max(c => c.Severity);
		}

		public ObjectSeverityDetail GetChildrenWorstSeverityDetail ()
		{
			return this.Children.Max(c => c.SeverityDetail);
		}

		public int GetChildrenBestSeverity ()
		{
			return this.Children.Min(c => c.Severity);
		}

		public ObjectSeverityDetail GetChildrenBestSeverityDetail ()
		{
			return this.Children.Min(c => c.SeverityDetail);
		}

		#endregion

		#region Boolean aggregation methods

		public bool IsAnyChildAtLeastInSeverity ( int severity )
		{
			return ( this.GetChildrenWorstSeverity() >= severity );
		}

		public bool IsAnyChildAtLeastInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return ( this.GetChildrenWorstSeverityDetail() >= severityDetail );
		}

		public bool AreAllChildrenAtLeastInSeverity ( int severity )
		{
			return ( this.CountChildrenAtLeastInSeverity(severity) == this.Children.Count );
		}

		public bool AreAllChildrenAtLeastInSeverity ( ObjectSeverityDetail severityDetail )
		{
			return ( this.CountChildrenAtLeastInSeverity(severityDetail) == this.Children.Count );
		}

		#endregion

		#endregion

		#region Navigation methods

		public virtual GrisObject GetChildByName ( string name )
		{
			foreach ( var grisObject in this.Children )
			{
				if ( grisObject is Server && ( (Server) grisObject ).Name == name ) return grisObject;
				if ( grisObject is Device && ( (Device) grisObject ).Name == name ) return grisObject;
				if ( grisObject is NodeSystem && ( (NodeSystem) grisObject ).Name == name ) return grisObject;
				if ( grisObject is Node && ( (Node) grisObject ).Name == name ) return grisObject;
				if ( grisObject is Zone && ( (Zone) grisObject ).Name == name ) return grisObject;
				if ( grisObject is Region && ( (Region) grisObject ).Name == name ) return grisObject;
			}

			return null;
		}

		public virtual IList<GrisObject> GetChildrenByName ( string name )
		{
			List<GrisObject> children = new List<GrisObject>();
			foreach ( var grisObject in this.Children )
			{
				if ( grisObject is Server && ( (Server) grisObject ).Name.IndexOf(name) > -1 ) children.Add(grisObject);
				if ( grisObject is Device && ( (Device) grisObject ).Name.IndexOf(name) > -1 ) children.Add(grisObject);
				if ( grisObject is NodeSystem && ( (NodeSystem) grisObject ).Name.IndexOf(name) > -1 ) children.Add(grisObject);
				if ( grisObject is Node && ( (Node) grisObject ).Name.IndexOf(name) > -1 ) children.Add(grisObject);
				if ( grisObject is Zone && ( (Zone) grisObject ).Name.IndexOf(name) > -1 ) children.Add(grisObject);
				if ( grisObject is Region && ( (Region) grisObject ).Name.IndexOf(name) > -1 ) children.Add(grisObject);
			}

			return children;
		}

		#endregion

		#region Virtual Objects

		public VirtualObject GetVirtualObjectByName ( string name )
		{
			return this.VirtualObjects.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public IList<VirtualObject> GetVirtualObjectsByName ( string name )
		{
			return this.VirtualObjects.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		public IList<VirtualObject> VirtualObjects
		{
			get { return this.Children.OfType<VirtualObject>().ToList(); }
		}

		#endregion

		#region Persistence

		public virtual IList<IStorable> StorableNodes
		{
			get
			{
				var stors = new List<IStorable>(300);
				if ( this.IsEvaluated ) stors.Add(this.Attributes);
				stors.AddRange(this.Children);
				stors.AddRange(this.VirtualObjects);
				return stors;
			}
		}

		public string GetUpdateCommand ()
		{
			const string UPD_CMD = "update object_status set SevLevelDetailId = {0}, SevLevel = {1} where ObjectStatusId = '{2}';";
			return string.Format(UPD_CMD, 
							this.SevLevelDetail.HasValue ? this.SevLevelDetail.ToString() : "NULL",
							this.SevLevel,
							this.Id);
		}

		#endregion
	}
}

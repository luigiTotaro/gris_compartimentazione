﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.Data.Model
{
	[Serializable]
	[DataContract]
	public class GrisAttributeCollection : IDictionary, INeedInitialization, IStorable
	{
		private readonly GrisObject _grisObject;
		private Dictionary<string, GrisAttribute> _internalDict;
		private readonly List<string> _modifiedAttributes = new List<string>(20);
		private readonly IList<Attribute> _attributes;

		public GrisAttributeCollection ( GrisObject grisObject, IList<Attribute> attributes )
		{
			if ( grisObject == null ) throw new ArgumentException("L' oggetto GrisAttributeCollection non può essere inizializzato con uno GrisObject nullo.", "grisObject");
			_grisObject = grisObject;
			_attributes = attributes;
		}

		public dynamic this[string key]
		{
			get
			{
				if ( !this.IsInitialized ) this.Initialize();
				this._grisObject.Evaluate();
				if ( this._grisObject.CallerObject != null && this._grisObject != this._grisObject.CallerObject )
				{
					// stiamo accedendo agli attributi di un oggetto invocato dalla formula di un altro oggetto: memorizzo la dipendenza
					ObjectDependencies.Instance.AddObjectDependencies(this._grisObject, this._grisObject.CallerObject);
				}
				return this.Contains(key) && this._internalDict[key].HasValue ? this._internalDict[key].Value : "";
			}
			set
			{
				if ( !this.IsInitialized ) this.Initialize();
				this._internalDict[key].Value = value;
				this._internalDict[key].HasValue = true;
				if ( !this._modifiedAttributes.Contains(key) ) this._modifiedAttributes.Add(key);
			}
		}

		public void ForceAttribute ( string attributeName, dynamic value )
		{
			this._grisObject.Evaluate();
			this[attributeName] = value;
			// TODO: implementare un flag che distingua i tipi di attributo che comportano ricalcolo dagli altri
			this._grisObject.ForceEvaluationOnDependentObjects();
		}

		public bool Contains ( string key )
		{
			if ( !this.IsInitialized ) this.Initialize();
			if ( this._internalDict == null ) return false;
			return this._internalDict.ContainsKey(key);
		}

		public void Initialize ()
		{
			if ( _attributes.Any() )
			{
				_count = this._attributes.Count();
				_internalDict = new Dictionary<string, GrisAttribute>(_attributes.Count);

				foreach ( var attribute in _attributes )
				{
					var gAttribute = new GrisAttribute();
					var oAttribute = this._grisObject.ObjectAttributes.SingleOrDefault(oa => oa.TypeId == attribute.Id);

					if ( oAttribute != null )
					{
						gAttribute.Value = oAttribute.Value; //TODO: saran sempre stringhe, c'asimmetria tra gli oggetti salvati e restituiti
						gAttribute.HasValue = true;
					}
					else
					{
						gAttribute.HasValue = false;
					}

					this._internalDict.Add(attribute.Name, gAttribute);
				}
				this.IsInitialized = true;
			}
			else
			{
				this.IsInitialized = false;				
			}
		}

		public bool IsInitialized { get; private set;  }

		public void SyncOriginalEntities ()
		{
			if ( this._grisObject.ObjectAttributes.IsLoaded )
			{
				var allAttributes = from attribute in _attributes
				                          join objectAttribute in this._grisObject.ObjectAttributes on attribute.Id equals
				                          	objectAttribute.TypeId into allAttribs
				                          from oAtt in allAttribs.DefaultIfEmpty()
				                          where this._modifiedAttributes.Contains(attribute.Name)
				                          select new { Att = attribute, OAtt = oAtt };

				foreach ( var attPair in allAttributes )
				{
					if ( attPair.OAtt == null )
					{
						var newObjAtt = new ObjectAttribute();
						newObjAtt.TypeId = attPair.Att.Id;
						newObjAtt.Value = this._internalDict[attPair.Att.Name].ToString();
						this._grisObject.ObjectAttributes.Add(newObjAtt);
					}
					else
					{
						attPair.OAtt.Value = this._internalDict[attPair.Att.Name].ToString();//TODO: per ora vengono memorizzati come stringa
					}
				}
			}
		}

		public IList<IStorable> StorableNodes
		{
			get { return null; }
		}

		public string GetUpdateCommand ()
		{
			// il metodo esegue anche la logica propria del metodo SyncOriginalEntities
			const string UPD_CMD = "update object_attributes set AttributeValue = {0} where AttributeId = {1};";
			const string INS_CMD = "insert into object_attributes (AttributeTypeId, ObjectStatusId, AttributeValue) values ({0}, '{1}', {2});";
			var updateCmds = new List<string>(20);
			if ( this._grisObject.ObjectAttributes.IsLoaded )
			{
				var allAttributes = from attribute in _attributes
									join objectAttribute in this._grisObject.ObjectAttributes on attribute.Id equals
									  objectAttribute.TypeId into allAttribs
									from oAtt in allAttribs.DefaultIfEmpty()
									where this._modifiedAttributes.Contains(attribute.Name)
									select new { Att = attribute, OAtt = oAtt };

				foreach ( var attPair in allAttributes )
				{
					if ( attPair.OAtt == null )
					{
						var newObjAtt = new ObjectAttribute();
						newObjAtt.TypeId = attPair.Att.Id;
						newObjAtt.Value = this[attPair.Att.Name].ToString();
						this._grisObject.ObjectAttributes.Add(newObjAtt);
						updateCmds.Add(string.Format(INS_CMD, attPair.Att.Id, this._grisObject.Id, this[attPair.Att.Name]));
					}
					else
					{
						attPair.OAtt.Value = this[attPair.Att.Name].ToString();
						updateCmds.Add(string.Format(UPD_CMD, this[attPair.Att.Name], attPair.OAtt.Id));
					}
				}

				if ( updateCmds.Count > 0 ) return string.Join(Environment.NewLine, updateCmds);
			}

			return "";
		}

		public bool Contains ( object key )
		{
			return Contains(key.ToString());
		}

		public void Add ( object key, object value )
		{
			if ( this._internalDict != null && value is GrisAttribute )
			{
				this._internalDict.Add(key.ToString(), (GrisAttribute) value);
			}
		}

		public void Clear ()
		{
			if ( this._internalDict != null ) this._internalDict.Clear();
		}

		public IDictionaryEnumerator GetEnumerator ()
		{
			return this._internalDict.GetEnumerator();
		}

		public void Remove ( object key )
		{
			if ( this._internalDict != null ) this._internalDict.Remove(key.ToString());
		}

		object IDictionary.this[object key]
		{
			get { return this[key.ToString()]; }
			set { this[key.ToString()] = value; }
		}

		public ICollection Keys
		{
			get
			{
				if ( this._internalDict != null )
				{
					return this._internalDict.Keys;
				}

				return new List<string>();
			}
		}

		public ICollection Values
		{
			get
			{
				if ( this._internalDict != null )
				{
					return this._internalDict.Values;
				}

				return new List<string>();
			}
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool IsFixedSize
		{
			get { return false; }
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return GetEnumerator();
		}

		public void CopyTo ( Array array, int index )
		{
			throw new NotImplementedException();
		}

		private int _count;
		public int Count
		{
			get { return this._count; }
		}

		public object SyncRoot
		{
			get { throw new NotImplementedException(); }
		}

		public bool IsSynchronized
		{
			get { throw new NotImplementedException(); }
		}
	}

	class GrisAttribute
	{
		public dynamic Value;
		public bool HasValue;
	}
}

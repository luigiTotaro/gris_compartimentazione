﻿
namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface INeedInitialization
	{
		void Initialize();
		bool IsInitialized { get; }
	}
}

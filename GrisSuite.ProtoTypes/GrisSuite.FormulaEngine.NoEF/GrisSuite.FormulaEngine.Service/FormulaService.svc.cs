﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Service
{
	public class FormulaService : IFormulaService
	{
		public IList<IGrisObject> EvaluateObjects ( IList<EvaluablePackage> objectsToEval )
		{
			var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });
			engine.Eval(objectsToEval);
			return ( from evaluablePackage in objectsToEval select evaluablePackage.GrisObject ).ToList();
		}

		public void EvaluateAndSaveAllObjects ()
		{
			DataAccessHelper.SyncObjects();
			var objectPcks = DataAccessHelper.GetAllObjects();

			var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });
			engine.Eval(objectPcks);

			//var regs = from evaluablePackage in objectPcks
			//           where evaluablePackage.GrisObject is Region
			//           select evaluablePackage.GrisObject;

			//foreach ( GrisObject region in regs ) DataAccessHelper.SaveBranch(region);

			//Parallel.ForEach(regs, reg => DataAccessHelper.SaveBranch(reg));
		}

		public IList<IGrisObject> EvaluateAllObjects()
		{
			var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });
			var objectPcks = DataAccessHelper.GetAllObjects();
			engine.Eval(objectPcks);
			return ( from evaluablePackage in objectPcks select evaluablePackage.GrisObject ).ToList();
		}
	}
}

﻿
using System.Collections.Generic;
using System.ServiceModel;
using GrisSuite.FormulaEngine.Contracts;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Service
{
	[ServiceContract]
	public interface IFormulaService
	{
		[OperationContract]
		IList<IGrisObject> EvaluateObjects ( IList<EvaluablePackage> objectsToEval );

		[OperationContract]
		void EvaluateAndSaveAllObjects ();

		[OperationContract]
		IList<IGrisObject> EvaluateAllObjects ();
	}
}

# script di calcolo dello stato di default per gli oggetti Server
import clr
import sys
import time
#clr.AddReferenceToFile('GrisSuite.Data.Model.dll')
#clr.AddReference("System")
import System
from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Contracts import *
from GrisSuite.FormulaEngine.Contracts.Severities import *
from datetime import date, datetime, timedelta

from Infrastructure.GrisSystemFormulas import *
from Infrastructure.MetaClasses import *

from System import DateTime, Nullable

try:
    
    if isinstance(obj, Server):
        clr.Convert(obj, Server)

        obj = GrisObjectPxy(obj, state)

        lastUpdate = datetime(2000, 1, 1)
        serverMinuteTimeout = timedelta(minutes=state["serverMinuteTimeout"])

        if not obj.LastUpdate == None:
            lastUpdate = datetime(obj.LastUpdate)

        ####
        
        if obj.IsDeleted and obj.Devices.Count == 0:
            obj.SeverityDetail = Old.SeverityDetail

        elif not obj.IsDeleted and obj.Devices.Count == 0:
            obj.SeverityDetail = IncompleteServerData.SeverityDetail

        elif (datetime.now() - lastUpdate) > serverMinuteTimeout:
            obj.SeverityDetail = Offline.SeverityDetail

        elif IsNullOrEmpty(obj.IpAddress):
            obj.SeverityDetail = NoIPAddress.SeverityDetail

        elif IsNullOrEmpty(obj.FullHostName):
            obj.SeverityDetail = NoHostName.SeverityDetail

        elif IsNullOrEmpty(obj.MacAddress):
            obj.SeverityDetail = NoMACAddress.SeverityDetail

        elif obj.InMaintenance:
            obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

        elif obj.HasDuplicateIpAddress():
            obj.SeverityDetail = DuplicateIPAddress.SeverityDetail

        else:
            obj.SeverityDetail = Ok.SeverityDetail
except Exception, exc:
    type, value = sys.exc_info()[:2]
    print type
    print value
    raise exc
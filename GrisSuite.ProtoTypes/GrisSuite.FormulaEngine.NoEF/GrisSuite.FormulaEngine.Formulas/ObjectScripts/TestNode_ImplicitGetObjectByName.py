
import sys
import clr
import System

sys.path.append('..')

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, Node):
    clr.Convert(obj, Node)

    obj = GrisObjectPxy(obj, state)
        
    if obj.S0.SeverityDetail == Error.SeverityDetail:
        obj.Severity = Error.Severity
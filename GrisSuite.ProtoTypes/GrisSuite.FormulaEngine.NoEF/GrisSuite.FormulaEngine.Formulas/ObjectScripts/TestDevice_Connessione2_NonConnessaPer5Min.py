
import sys
import clr
import System

sys.path.append('..')

from datetime import date, datetime, timedelta

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, Device):
    clr.Convert(obj, Device)

    obj = GrisObjectPxy(obj, state)
        
    if obj.InformazioniPorteSwitch.ConnessionePortaPrimaria2.IsInConditionForTimeSpan('Non connessa', System.TimeSpan(0, 5, 0)):
        obj.Node.Dev1ConnessaAPorta2.SeverityDetail = Offline.SeverityDetail
        obj.Node.Dev2ConnessaAPorta2.SeverityDetail = Offline.SeverityDetail
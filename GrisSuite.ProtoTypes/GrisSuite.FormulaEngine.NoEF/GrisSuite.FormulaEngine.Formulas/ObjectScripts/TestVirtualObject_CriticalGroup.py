
import sys
import clr
import System

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.IsAnyChildAtLeastInSeverity(Error.Severity):
        obj.SeverityDetail = obj.GetChildrenWorstSeverityDetail()
        obj.Parent.ForceSeverity(obj.Severity) # Node
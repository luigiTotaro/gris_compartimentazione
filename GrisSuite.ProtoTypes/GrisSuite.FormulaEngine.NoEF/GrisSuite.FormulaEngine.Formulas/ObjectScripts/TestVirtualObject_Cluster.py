
import sys
import clr

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.AreAllChildrenAtLeastInSeverity(Warning.Severity):
        obj.Severity = obj.GetChildrenBestSeverity()
        obj.Parent.ForceSeverity(obj.Severity) # sistema
        obj.Parent.Parent.ForceSeverity(obj.Severity) # stazione
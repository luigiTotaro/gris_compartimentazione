# script di calcolo dello stato di default per gli oggetti NodeSystem
import clr
import sys

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Contracts import *
from GrisSuite.FormulaEngine.Contracts.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage
from Infrastructure.MetaClasses import *

try:
    if isinstance(obj, NodeSystem):
        clr.Convert(obj, NodeSystem)

        obj = GrisObjectPxy(obj, state)
        
        if obj.SystemId != 99:  
            if obj.GetMonitoringServersInSeverity(IncompleteServerData.SeverityDetail).Count > 0:
                obj.Severity = Error.Severity
            else:
                if obj.Node.AreAllNodeServersOffline():
                    obj.Severity = Unknown.Severity
                else:
                    worstStatus = obj.GetWorstSeverityForDevicesOfType(["TT10210", "TT10210V3", "TT10220"])
                    if worstStatus in [1, 2, 255]:
                        if worstStatus > 2:
                            obj.Severity = Error.Severity
                        else:
                            obj.Severity = worstStatus
                    else:
                        obj.Severity = GetWeightedAverage(
                            obj.CountDevicesInSeverity(Ok.Severity), 
                            obj.CountDevicesInSeverity(Warning.Severity), 
                            obj.CountDevicesInSeverity(Error.Severity) + obj.CountDevicesInSeverity(Offline.Severity) + obj.CountDevicesInSeverity(Unknown.Severity), 
                            obj.CountDevicesInSeverity(NotActive.Severity))
        else:
            obj.Severity = NotAvailable.Severity ##TODO: da verificare
except Exception, exc:
    type, value = sys.exc_info()[:2]
    print type
    print value
    raise exc

from Infrastructure.GrisSystemFormulas import GetDividedName
from GrisSuite.FormulaEngine.Model import *

class GrisObjectPxy(object):
    __obj = None
    __state = None
    def __init__(self, obj, state): # ctor
        self.__obj = obj
        self.__state = state
    def __getattr__(self, name):
        if self.__obj:
            if hasattr(self.__obj, name): # ha un membro con questo nome
                if self.__state and name in self.__state: #mocking via funzione passata attraverso lo stato
                    return self.__state[name]
                else:
                    attr = getattr(self.__obj, name) # viene invocato il membro e settato il callerobject
                    if isinstance(attr, GrisObject): 
                        attr.CallerObject = self.__obj
                        attr = GrisObjectPxy(attr, self.__state)
                    return attr
            else:                         # non ha un membro con questo nome
                if hasattr(self.__obj, 'Attributes') and self.__obj.Attributes and name in self.__obj.Attributes:
                    # ha un attributo con questo nome
                    return self.__obj.Attributes[name]
                else:
                    subObj = self.__obj.GetChildByName(name)
                    if subObj:
                        # ha un figlio con questo nome
                        return GrisObjectPxy(subObj, self.__state)
                    else:
                        # riprovo introducendo gli spazi
                        subObj = self.__obj.GetChildByName(GetDividedName(name))
                        if subObj:
                            return GrisObjectPxy(subObj, self.__state)
                        else:
                            raise NameError("L' oggetto non possiede questo membro: " + name)
        else:
            object.__getattr__(self, name)
    def __setattr__(self, name, value):
        if name != '_GrisObjectPxy__obj' and name != '_GrisObjectPxy__state':
            if self.__obj:
                if hasattr(self.__obj, name):
                    setattr(self.__obj, name, value)
                else:
                    if hasattr(self.__obj, 'Attributes') and self.__obj.Attributes and name in self.__obj.Attributes:
                        self.__obj.Attributes[name] = value;
                    else:
                        object.__setattr__(self, name, value)
            else:
                object.__setattr__(self, name, value)
        else:
            object.__setattr__(self, name, value)

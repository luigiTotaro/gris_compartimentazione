﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ZoomPng
{
    public partial class Page : UserControl
    {
        //private double imageWidth = 2644;
        //private double imageHeight = 925;

        //private double monitorX = 40;
        //private double monitorY = 40;

        TransformGroup tgImage;
        TransformGroup tgStation;
        public Page()
        {
            InitializeComponent();
            tgImage = (TransformGroup)Helper.ResourceHelper.FindResource(this, "TransformImage");
            tgStation = (TransformGroup)Helper.ResourceHelper.FindResource(this, "TransformStation");

            TraceInfo(monitorSalaAttesa.X, monitorSalaAttesa.Y);
        }

        private void TraceInfo(double x, double y)
        {
            info.Text = String.Format("x={0} y={1}", x.ToString(), y.ToString());
        }

        private void Slider_ValueChanged(object sender, RoutedEventArgs e)
        {
            //((ScaleTransform)tgImage.Children[0]).ScaleX = (1 + slider.Value);
            //((ScaleTransform)tgImage.Children[0]).ScaleY = (1 + slider.Value);

            //System.Diagnostics.Debug.WriteLine(String.Format("slider changing x={0} y={1}", monitorSalaAttesa.X.ToString(), monitorSalaAttesa.Y.ToString()));
            //System.Diagnostics.Debug.WriteLine(String.Format("canvas position x={0} y={1}", monitorSalaAttesa.GetValue(Canvas.LeftProperty).ToString(),
            //monitorSalaAttesa.GetValue(Canvas.TopProperty).ToString()));

            //((TranslateTransform)tgStation.Children[3]).X = monitorSalaAttesa.X + (monitorSalaAttesa.X * slider.Value);
            //((TranslateTransform)tgStation.Children[3]).Y = monitorSalaAttesa.Y + (monitorSalaAttesa.Y * slider.Value);
            //System.Diagnostics.Debug.WriteLine(String.Format("slider changed x={0} y={1}", monitorSalaAttesa.X.ToString(), monitorSalaAttesa.Y.ToString()));
            //monitorSalaAttesa.X=((TranslateTransform)tgStation.Children[3]).X;
            //monitorSalaAttesa.Y=((TranslateTransform)tgStation.Children[3]).Y;
        }

        private double currentWidth = 0.0;
        private double currentHeigth = 0.0;
        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (currentHeigth == 0)
            {
                currentWidth = this.ActualWidth;
                currentHeigth = this.ActualHeight;
            }

            LayoutRoot.Width = this.ActualWidth;
            LayoutRoot.Height = this.ActualHeight;
            monitorSalaAttesa.X = monitorSalaAttesa.savedX * (this.ActualWidth / currentWidth);
            monitorSalaAttesa.Y = monitorSalaAttesa.savedY * (this.ActualHeight / currentHeigth);
            monitorSalaAttesa.SetPosition();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            monitorSalaAttesa.savedX = 40;
            monitorSalaAttesa.savedY = 40;
            monitorSalaAttesa.X = monitorSalaAttesa.savedX;
            monitorSalaAttesa.Y = monitorSalaAttesa.savedY;

            monitorSalaAttesa.SetPosition();
        }

        private void bottone1_MouseEnter(object sender, MouseEventArgs e)
        {
            VisualStateManager.GoToState(bottone_modifica, "over", false);
        }

        private void bottone_modifica_MouseLeave(object sender, MouseEventArgs e)
        {
            VisualStateManager.GoToState(bottone_modifica, "normale", false);

        }

        private void bottone_modifica_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(bottone_modifica, "premuto", false);
            monitorSalaAttesa.EditMode = !monitorSalaAttesa.EditMode;
        }

        private void bottone_modifica_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(bottone_modifica, "normale", false);
        }

        private void monitorSalaAttesa_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void monitorSalaAttesa_MouseLeave(object sender, MouseEventArgs e)
        {

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Cap.Ima.Web.CapControls
{
    public partial class CapChooseColumns : UserControl
    {
        public CapChooseColumns()
        {
            InitializeComponent();
        }

        bool isMouseCaptured;
        Point mousePosition;

        void DrawShadow(FrameworkElement element)
        { 
        
        }

        TextBlock _textBlock;
        void HandleMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement item = sender as FrameworkElement;

            //TextBlock tb = ((ListBoxItem)sender).Content as TextBlock;
            TextBlock tb =(TextBlock) sender;
            _textBlock = new TextBlock();
            _textBlock.Text = tb.Text;
            shadowCanvas.Children.Add(_textBlock);


            GeneralTransform transform = item.TransformToVisual((UIElement)Application.Current.RootVisual);
            Point transformedPoint = transform.Transform(new Point(0, 0));

            GeneralTransform trans = canvas.TransformToVisual((UIElement)Application.Current.RootVisual);
            Point transf = trans.Transform(new Point(0, 0));  

            // Set new position of object.
            shadowCanvas.SetValue(Canvas.TopProperty, transformedPoint.Y - transf.Y);
            shadowCanvas.SetValue(Canvas.LeftProperty, transformedPoint.X);

            mousePosition = e.GetPosition(null);
            isMouseCaptured = true;
            item.CaptureMouse();
            item.Cursor = Cursors.Hand;
        }
        void HandleMouseMove(object sender, MouseEventArgs e)
        {
            FrameworkElement item = sender as FrameworkElement;
            if (isMouseCaptured)
            {

                // Calculate the current position of the object.
                double deltaV = e.GetPosition(null).Y - mousePosition.Y;
                double deltaH = e.GetPosition(null).X - mousePosition.X;
                double newTop = deltaV + (double)shadowCanvas.GetValue(Canvas.TopProperty);
                double newLeft = deltaH + (double)shadowCanvas.GetValue(Canvas.LeftProperty);

                // Set new position of object.
                shadowCanvas.SetValue(Canvas.TopProperty, newTop);
                shadowCanvas.SetValue(Canvas.LeftProperty, newLeft);

                // Update position global variables.
                mousePosition = e.GetPosition(null);
            }
        }
        void HandleMouseUp(object sender, MouseButtonEventArgs e)
        {
            //shadowCanvas.Visibility = Visibility.Collapsed;
            shadowCanvas.Children.Clear();
            FrameworkElement item = sender as FrameworkElement;
            isMouseCaptured = false;
            item.ReleaseMouseCapture();
            mousePosition.X = mousePosition.Y = 0;
            item.Cursor = null;

            GeneralTransform trans = destinationList.TransformToVisual((UIElement)Application.Current.RootVisual);
            Point transf = trans.Transform(new Point(0, 0));
            if ((e.GetPosition(null).X > transf.X) && (e.GetPosition(null).X < transf.X + destinationList.ActualWidth) &&
                (e.GetPosition(null).Y > transf.Y) && (e.GetPosition(null).Y < transf.Y + destinationList.ActualHeight))
            { 
                destinationList.Items.Add(_textBlock);
            }
        }
        private void destinationList_MouseEnter(object sender, MouseEventArgs e)
        {
            if (isMouseCaptured)
            {
            }
        }
    }
}

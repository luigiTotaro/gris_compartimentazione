﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Helper
{
    public static class ResourceHelper
    {
        public static object FindResource(Application App, string name)
        {
            if (App.Resources.Contains(name))
            {
                return App.Resources[name];
            }
            else
            {
                FrameworkElement root = App.RootVisual as FrameworkElement;
                return root.FindResource(name);
            }
            return null;
        }

        internal static object FindResource(this FrameworkElement root, string name)
        {
            if (root != null && root.Resources.Contains(name))
            {
                return root.Resources[name];
            }
            else
            {
                try
                {
                    return root.FindName(name);
                }
                catch { }
            }

            return null;
        }

        public static object FindResource(UserControl root, string name)
        {
            if (root != null && root.Resources.Contains(name))
            {
                return root.Resources[name];
            }
            else
            {
                try
                {
                    return root.FindName(name);
                }
                catch { }
            }

            return null;
        }
    }
}

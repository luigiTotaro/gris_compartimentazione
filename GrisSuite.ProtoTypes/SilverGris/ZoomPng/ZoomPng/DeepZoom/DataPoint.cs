﻿/* ****************************************************************************
 *
 * Copyright (c) Microsoft Corporation. 
 *
 * This source code is subject to terms and conditions of the Microsoft Public License. A
 * copy of the license can be found at http://go.microsoft.com/fwlink/?LinkID=131993. If
 * you cannot locate the Microsoft Public License, please send an email to
 * mixon@microsoft.com. By using this source code in any fashion, you are agreeing to
 * be bound by the terms of the Microsoft Public License.
 *
 * You must not remove this notice, or any other, from this software.
 *
 *
 * ***************************************************************************/

using System;
using System.Windows;

namespace MIXOnline.Descry
{
    public class DataPoint
    {
        private Point _point = new Point();

        /// <summary>
        /// Indicates the X,Y coordinate of the Pin in pixels and
        /// relates to the ReferenceWidth property
        /// </summary>
        public Point Location
        {
            get { return _point; }
            set { _point = value; }
        }

        private string _name;

        /// <summary>
        /// A unique pin name.
        /// Not checked to be unique
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        //private string _imagePath;

        ///// <summary>
        ///// The default image to show for the Pin
        ///// </summary>
        //public string ImagePath
        //{
        //    get { return _imagePath; }
        //    set { _imagePath = value; }
        //}

        //private string _imagePathMouseOver;

        ///// <summary>
        ///// The image to display when a mouse is over the image
        ///// </summary>
        //public string ImagePathMouseOver
        //{
        //    get { return _imagePathMouseOver; }
        //    set { _imagePathMouseOver = value; }
        //}

        private string _type;

        /// <summary>
        /// Specifies the DataPoint type: Currently the 3 valid values are:
        /// ProcessMarker
        /// PopMarker
        /// CharacterMarker
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private Nullable<double> _width;

        /// <summary>
        /// The pixel width the image should be displayed at
        /// </summary>
        public Nullable<double> Width
        {
            get { return _width; }
            set { _width = value; }
        }

        private Nullable<double> _height;

        /// <summary>
        /// The Pixel height the image should be displayed at
        /// </summary>
        public Nullable<double> Height
        {
            get { return _height; }
            set { _height = value; }
        }

        private Nullable<int> _index;
        
        /// <summary>
        /// Used for ProcessMarker sequence. 
        /// Should be missing or Zero if it is not a ProcessMarker
        /// </summary>
        public Nullable<int> Index
        {
            get { return _index; }
            set { _index = value; }
        }

        private string _text;

        /// <summary>
        /// The text that will display inside the Marker dialogs
        /// </summary>
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private Point _scaleCenter;

        /// <summary>
        /// This value is a pixel coordinate within the Width and Height
        /// bounds set in Width and Height of the image.
        /// The idea is that the image scales about this point so that it can
        /// Accomodate for example a picture of a thumtack, where the image zooms about
        /// the pin point, for accurate placement on a map.
        /// </summary>
        public Point ScaleCenter
        {
            get { return _scaleCenter; }
            set { _scaleCenter = value; }
        }

        private double _referenceWidth;

        /// <summary>
        /// This value specifies the entire width of the full scale DeepZoom image
        /// and is needed help calculate logical coordinates [0..1] from the Location
        /// Property. Currently this value is the same for every "Point" in the XML
        /// file and so is inefficient. The only reason this was left was because changing
        /// this would have broke the Schemaa and would have taken more time than we wanted to 
        /// spend fixing
        /// </summary>
        public double ReferenceWidth 
        {
            get { return _referenceWidth; }
            set { _referenceWidth = value; }
        }

    }
}

﻿/* ****************************************************************************
 *
 * Copyright (c) Microsoft Corporation. 
 *
 * This source code is subject to terms and conditions of the Microsoft Public License. A
 * copy of the license can be found at http://go.microsoft.com/fwlink/?LinkID=131993. If
 * you cannot locate the Microsoft Public License, please send an email to
 * mixon@microsoft.com. By using this source code in any fashion, you are agreeing to
 * be bound by the terms of the Microsoft Public License.
 *
 * You must not remove this notice, or any other, from this software.
 *
 *
 * ***************************************************************************/

using System.IO;
using System.Xml;

namespace MIXOnline.Descry
{
    public class XmlHelper
    {
        static public XmlReader GetXmlReaderFromString(string xml)
        {
            StringReader sr = new StringReader(xml);
            XmlReaderSettings settings = new XmlReaderSettings();
            return XmlReader.Create(sr, settings);
        }

    }
}

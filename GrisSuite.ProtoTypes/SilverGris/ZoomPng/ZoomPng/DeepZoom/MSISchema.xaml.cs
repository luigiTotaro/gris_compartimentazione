﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Linq;
using gris;
using MIXOnline.Descry;
using PositionsReference.PositionsReference;
using System.Data.Services.Client;

namespace ZoomPng
{
    public partial class MSISchema : UserControl
    {
        //
        // Based on prior work done by Lutz Gerhard, Peter Blois, and Scott Hanselman
        //
        double zoom = 1;
        Point lastMouseDownPos = new Point();
        Point lastMousePos = new Point();
        Point lastMouseViewPort = new Point();

        bool _isDragging = false;
        double _scaleFactor = 0;
        DateTime startTime;
        Storyboard _pinSyncAnimation;
        bool ranInitialTrackAnimationMethod = false;
        double _imageScaleFactor = 1;
        List<DataPoint> dataPoints = new List<DataPoint>();
        TimeSpan animationDuration = new TimeSpan(0, 0, 0, 0, 1500); // make the animation last 1500 ms, which is the length of the animation in Deep Zoom

        public double ZoomFactor
        {
            get { return zoom; }
            set { zoom = value; }
        }

        /// <summary>
        /// Load the file that contains the "A Website Named Desire" Points of Interest
        /// </summary>
        /// <param name="filename">A relative path to a file.</param>
        //private void LoadFile(string filename)
        //{
        //    WebClient client = new WebClient();
        //    client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(File_Loaded);
        //    client.DownloadStringAsync(new Uri(filename, UriKind.Relative));
        //}

        public MSISchema()
        {
            InitializeComponent();

            SubscribeToEvents();

            //
            // Firing an event when the MultiScaleImage is Loaded
            //
            this.msi.Loaded += new RoutedEventHandler(msi_Loaded);

            //
            // Firing an event when all of the images have been Loaded
            //
            this.msi.ImageOpenSucceeded += new RoutedEventHandler(msi_ImageOpenSucceeded);

            LoadPoints();
        }

        private void LoadPoints()
        {
            GrisPositionsEntities context = new GrisPositionsEntities(new Uri("GrisPositionsDataService.svc", UriKind.Relative));

            var devs = from d in context.devices.Expand("device_locations") select d;
			DataServiceQuery<devices> devsQuery = (DataServiceQuery<devices>)devs;

        	try
        	{
				devsQuery.BeginExecute(new AsyncCallback(PointsLoaded), devsQuery);
        	}
        	catch (Exception exc)
        	{
        		string s = exc.Message;
        		throw;
        	}
        }

        Point CalculateLogicalPosition(double referenceWidth, double x, double y)
        {
            Point LogicalLocation = new Point(x / referenceWidth, y / referenceWidth);
            return LogicalLocation;
        }


        void PointsLoaded(IAsyncResult result)
        {
			//DataServiceQuery<devices> query =
			//    (DataServiceQuery<devices>)result.AsyncState;

            try
            {



				//List<DataPoint> points = (from devs in query.EndExecute(result).ToList()
				//                          select new DataPoint()
				//                          {
				//                              Name = devs.Name,
				//                              Text = devs.Addr,
											  //Location = CalculateLogicalPosition(16064, devs.device_locations.XLocation.HasValue ? devs.device_locations.YLocation.Value : 0.0,
											  //    devs.device_locations.YLocation.HasValue ? devs.device_locations.YLocation.Value : 0.0),
				//                              Index = 100,
				//                              Width = 40,
				//                              Height = 40,
				//                              ScaleCenter = new Point(0, 0)
				//                          }).ToList();

            	List<DataPoint> points = new List<DataPoint>
            	                         	{
            	                         		new DataPoint {Name = "Gigio", Text = "Prima punto",
															  Location = CalculateLogicalPosition(16064, 5101.3, 2342.5),		
															  Index = 0,
															  Width=40, Height=40,
															  ScaleCenter = new Point(0.5, 0.5)
												},
            	                         		new DataPoint {Name = "Topo", Text = "Secondo punto",
															  Location = CalculateLogicalPosition(16064, 1211.3, 142.5),		
															  Index = 0,
															  Width=40, Height=40,
															  ScaleCenter = new Point(0.5, 0.5)
												},
            	                         		new DataPoint {Name = "Merlino", Text = "Terzo punto",
															  Location = CalculateLogicalPosition(16064, 601.9, 3467.5),		
															  Index = 0,
															  Width=40, Height=40,
															  ScaleCenter = new Point(0.5, 0.5)
												}
            	                         	};

                if (points != null)
                {
                    foreach (DataPoint point in points)
                    {
                        dataPoints.Add(point); // add to the list
                        AddPinToCanvas(canvasPinnedObjects, point); // position object on canvas
                    }

                    GoHome(); // center DeepZoom at initial position

                    //this.Source = "/TorinoSchematico/dzc_output.xml"; // set the source *after* doing the ZoomImage resize, otherwise animations will display in new zoom.
                }

                // Prepare the Pins for the initial animation by moving them off screen (up 1500px)
                foreach (UIElement uie in canvasPinnedObjects.Children)
                {
                    pallino_stazione pin = uie as pallino_stazione;
                    Point l = GetPinScreenCoordinates(pin);
                    Canvas.SetTop(pin, l.Y); // set up for animation.
                    Canvas.SetLeft(pin, l.X);
                }

                // Hook up any events you want when the image has successfully been opened
            }
            catch (Exception)
            {
            }
        }

        ///// <summary>
        ///// Fires when the file containing the DataPoint data has finished loading or has failed to load
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void File_Loaded(object sender, DownloadStringCompletedEventArgs e)
        //{
        //    if (e.Error == null)
        //    {
        //        string xmlData = e.Result;
        //        XElement root = XElement.Load(XmlHelper.GetXmlReaderFromString(xmlData));

        //        IEnumerable<XElement> points = root.Elements("Point");

        //        foreach (XElement point in points)
        //        {
        //            DataPoint dp = CreateDataPoint(point); // create a DataPoint object for every entry in XML

        //            if (dp != null)
        //            {
        //                dataPoints.Add(dp); // add to the list
        //                AddPinToCanvas(canvasPinnedObjects, dp); // position object on canvas
        //            }
        //        }

        //        GoHome(); // center DeepZoom at initial position

        //        //this.Source = "/TorinoSchematico/dzc_output.xml"; // set the source *after* doing the ZoomImage resize, otherwise animations will display in new zoom.
        //    }

        //    // Prepare the Pins for the initial animation by moving them off screen (up 1500px)
        //    foreach (UIElement uie in canvasPinnedObjects.Children)
        //    {
        //        pallino_stazione pin = uie as pallino_stazione;
        //        Point l = GetPinScreenCoordinates(pin);
        //        Canvas.SetTop(pin, l.Y); // set up for animation.
        //        Canvas.SetLeft(pin, l.X);
        //    }

        //    // Hook up any events you want when the image has successfully been opened
        //}

        private void SubscribeToEvents()
        {
            // inline event handler for Mouse Zoom
            new MouseWheelHelper(this).Moved += delegate(object sender, MouseWheelEventArgs e)
            {
                //processMarker.Visibility = Visibility.Collapsed;
                //popMarker.Visibility = Visibility.Collapsed;
                //characterMarker.Visibility = Visibility.Collapsed;
                //bShowProcessMarker = false;

                double newzoom = zoom;
                if (e.Delta > 0)
                    newzoom /= 1.3;
                else
                    newzoom *= 1.3;
                Point logicalPoint = this.msi.ElementToLogicalPoint(this.lastMousePos);
                Zoom(newzoom, this.msi.ElementToLogicalPoint(this.lastMousePos));
                TrackAnimation();
            };
            this.SizeChanged += new SizeChangedEventHandler(AttachableMultiScaleImage_SizeChanged);
        }

        /// <summary>
        /// If the control is resized, clip off the parts of the pinned objects residing
        /// outside of the control bounds and reset AWND to original position. Lastly move
        /// the markers to their new position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachableMultiScaleImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // clip off anything that is outside of the bounds of this rectangle
            _scaleFactor = canvasPinnedObjects.ActualWidth;
            RectangleGeometry r = new RectangleGeometry();
            r.Rect = new Rect(0, 0, _scaleFactor, canvasPinnedObjects.ActualHeight);
            canvasPinnedObjects.Clip = r;

            // this cannot fire at startup because it will screw up the initial animation.
            if (ranInitialTrackAnimationMethod)
            {
                GoHome();
                TrackAnimation();
            }
        }

        /// <summary>
        /// Animation Stuff: The idea is to run a 0 length animation throughout 
        /// the animation of deep zoom that tracks the motion of the map we then 
        /// synchronize as fast as we can the positioning of other elements on screen
        /// </summary>
        public void TrackAnimation()
        {
            if (_pinSyncAnimation == null)
            {
                _pinSyncAnimation = new Storyboard();
                _pinSyncAnimation.Duration = new Duration(new TimeSpan(0, 0, 0));
                _pinSyncAnimation.Completed += new EventHandler(pinSyncAnimation_Completed);
            }
            startTime = DateTime.Now;

            _pinSyncAnimation.Begin();
        }

        /// <summary>
        /// When a PinSync Animation completes we call it again recursively until 4 seconds have passed.
        /// This allows for smooth animation of the Pins as the DeepZoom image is resized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pinSyncAnimation_Completed(object sender, EventArgs e)
        {
            double progress = (DateTime.Now - this.startTime).TotalSeconds / animationDuration.TotalSeconds;

            if (progress >= 4)
            {
                _pinSyncAnimation.Completed -= new EventHandler(pinSyncAnimation_Completed); // remove handler so GC can destroy object
                _pinSyncAnimation = null; // destroy animation if time is greater than 4 seconds
            }
            else
            {
                // do what you need to do to synchronize the animation here
                foreach (FrameworkElement fe in canvasPinnedObjects.Children)
                {
                    if (fe is pallino_stazione)
                    {
                        SyncPin(fe as pallino_stazione);
                    }
                }

                _pinSyncAnimation.Begin();
            }

            // Display and Animate ProcessMarker, if it needs to be displayed
            //if (bShowProcessMarker)
            //    PositionProcessMarker();
        }


        /// <summary>
        /// Draw a pin with "powerlaw scaling" relative to the position of the map multi scale image.
        /// Note that the Pin itself or its contents are not being scaled as might be thought by the name
        /// </summary>
        /// <param name="pinToDraw"></param>
        private void SyncPin(pallino_stazione pinToDraw)
        {
            Point where = pinToDraw.Location;
            Nullable<Point> ScaleCenter = pinToDraw.ScaleCenter;

            // figure out the origin of the multiscale image in normalized [0..1] coordinate space
            Rect msiRect = GetImageRect(this.msi);

            // need the ZoomFactor to figure out where to layout the pin relative to the normalized origin of the MSI
            double zoomFactor = Math.Log(1 / this.msi.ViewportWidth, 2);

            //System.Diagnostics.Debug.WriteLine(zoomFactor.ToString());

            // take into account zoom factor when figuring out the layout
            Point pinOrigin = new Point(msiRect.X + Math.Pow(2, zoomFactor) * where.X, msiRect.Y + Math.Pow(2, zoomFactor) * where.Y);

            // use "powerlaw scaling" to position the pin
            DrawPin(pinToDraw, pinOrigin, Math.Pow(0.02 * (zoomFactor + 1), 2) + 0.01, ScaleCenter);


            //// Scale Pin using same ZoomFactor
            //if ((zoomFactor + 1.2) < 1) 
            //    _imageScaleFactor = 1;
            //else
            //    _imageScaleFactor = zoomFactor + 1.5;


            // Scale Pin using same ZoomFactor
            if ((zoomFactor + 1.5) < 1)
                _imageScaleFactor = 1;
            else
                _imageScaleFactor = zoomFactor + 1.5;


            //pinToDraw.image.Width = (pinToDraw.DataContext as DataPoint).Width.Value * _imageScaleFactor;
            //pinToDraw.image.Height = (pinToDraw.DataContext as DataPoint).Height.Value * _imageScaleFactor;
        }

        /// <summary>
        /// Draw the pin in normalized [0..1] space to match the MSI behavior
        /// </summary>
        /// <param name="element"></param>
        /// <param name="p"></param>
        /// <param name="w"></param>
        /// <param name="ScaleCenter"></param>
        private void DrawPin(FrameworkElement element, Point p, double w, Nullable<Point> ScaleCenter)
        {
            if (ScaleCenter != new Point())
            {
                // ScaleCenter takes into account the fact that the tip of the pin
                // doesn't align exactly with the edges of the image
                Canvas.SetLeft(element, (p.X - ScaleCenter.Value.X * w) * _scaleFactor);
                Canvas.SetTop(element, (p.Y - ScaleCenter.Value.Y * w) * _scaleFactor);
            }
            else
            {
                Canvas.SetLeft(element, (p.X * _scaleFactor));
                Canvas.SetTop(element, (p.Y * _scaleFactor));
            }
        }

        /// <summary>
        /// Create a FrameworkElement specific to the datapoint and add it to a panel
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="datapoint"></param>
        private void AddPinToCanvas(Panel panel, DataPoint datapoint)
        {
            pallino_stazione pallino = null;// panel.Children.E_FindChild(datapoint.Name) as Pin; // try to get child by name. 

            if (pallino == null) // don't add a new "Pin" if it already exists
            {
                pallino = new pallino_stazione()
                {
                    Name = datapoint.Name,
                    ScaleCenter = datapoint.ScaleCenter,
                    Location = datapoint.Location,
                    DataContext = datapoint,
                };

                // since the Pin.xaml file in this project happens to reside in the Objects folder,
                // any resource that it references must go up one directory when making file requests. 
                // Hence the ../ prefix on the path in the XML file.
                //
                //if (!string.IsNullOrEmpty(datapoint.ImagePath))
                //    pallino.image.Source = new BitmapImage(new Uri(datapoint.ImagePath, UriKind.Relative));

                //if (datapoint.Width != null)
                //    pallino.image.Width = datapoint.Width.Value;

                //if (datapoint.Height != null)
                //    pallino.image.Height = datapoint.Height.Value;

                if (datapoint.Index != null)
                    Canvas.SetZIndex(pallino, datapoint.Index.Value);

                //if (!string.IsNullOrEmpty(datapoint.Text))
                //    ToolTipService.SetToolTip(pin, datapoint.Index.Value.ToString());

                //pallino.MouseLeftButtonDown += new MouseButtonEventHandler(pin_MouseLeftButtonDown);
                //pallino.MouseEnter += new MouseEventHandler(pin_MouseEnter);
                //pallino.MouseLeave += new MouseEventHandler(pin_MouseLeave);

                //if (datapoint.ImagePathMouseOver != null)
                //{
                //    pallino.MouseEnter += delegate(object sender, MouseEventArgs e)
                //    {
                //        pallino.image.Source = new BitmapImage(new Uri(datapoint.ImagePathMouseOver, UriKind.Relative));
                //    };
                //}

                //if (datapoint.ImagePath != null)
                //{
                //    pallino.MouseLeave += delegate(object sender, MouseEventArgs e)
                //    {
                //        pallino.image.Source = new BitmapImage(new Uri(datapoint.ImagePath, UriKind.Relative));
                //    };
                //}
                panel.Children.Add(pallino);
            }

        }

        void msi_ImageOpenSucceeded(object sender, RoutedEventArgs e)
        {
            //If collection, this gets you a list of all of the MultiScaleSubImages
            //
            //foreach (MultiScaleSubImage subImage in msi.SubImages)
            //{
            //    // Do something
            //}

            msi.ViewportWidth = 1;
        }

        /// <summary>
        /// Determines the absolute postion of a Pin based on the users current 
        /// position and zoom factor of the MultiScaleImage.
        /// </summary>
        /// <param name="pinToDraw"></param>
        /// <returns></returns>
        private Point GetPinScreenCoordinates(pallino_stazione pinToDraw)
        {
            Point where = pinToDraw.Location;
            Nullable<Point> ScaleCenter = pinToDraw.ScaleCenter;

            // Figure out the origin of the MultiScaleImage in normalized [0..1] coordinate space
            Rect msiRect = GetImageRect(this.msi);

            // need the ZoomFactor to figure out where to layout the pin relative to the normalized origin of the MSI
            double zoomFactor = Math.Log(1 / this.msi.ViewportWidth, 2);

            // take into account zoom factor when figuring out the layout
            Point pinOrigin = new Point(msiRect.X + Math.Pow(2, zoomFactor) * where.X, msiRect.Y + Math.Pow(2, zoomFactor) * where.Y);

            // use powerlaw scaling for the size of the pin
            return GetPinScreenCoordinatesBasedOnScaleCenter(pinToDraw, pinOrigin, Math.Pow(0.02 * (zoomFactor + 1), 2) + 0.01, ScaleCenter);
        }


        /// <summary>
        /// convert the MSI coordinate space to a rect space
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private Rect GetImageRect(MultiScaleImage img)
        {
            return new Rect(-img.ViewportOrigin.X / img.ViewportWidth, -img.ViewportOrigin.Y / img.ViewportWidth, 1 / img.ViewportWidth, 1 / img.ViewportWidth * img.AspectRatio);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="p"></param>
        /// <param name="w"></param>
        /// <param name="ScaleCenter"></param>
        /// <returns></returns>
        private Point GetPinScreenCoordinatesBasedOnScaleCenter(FrameworkElement element, Point p, double w, Nullable<Point> ScaleCenter)
        {
            if (ScaleCenter != new Point())
            {
                return new Point((p.X - ScaleCenter.Value.X * w) * _scaleFactor, (p.Y - ScaleCenter.Value.Y * w) * _scaleFactor);
            }
            else
            {
                return new Point(p.X * _scaleFactor, p.Y * _scaleFactor);
            }
        }

        #region DependencyProperty

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(string), typeof(MSISchema),
            new PropertyMetadata((d, e) => ((MSISchema)d).OnSourceChanged((string)e.OldValue, (string)e.NewValue)));

        public string Source
        {
            get { return (string)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        private void OnSourceChanged(string oldValue, string newValue)
        {
            msi.Source = new DeepZoomImageTileSource(new Uri(newValue, UriKind.Relative));
        }

        #endregion DependencyProperty

        private DataPoint CreateDataPoint(XElement node)
        {
            Point LogicalLocation = new Point();

            XElement ReferenceWidth = node.Element("ReferenceWidth");
            XElement X = node.Element("X");
            XElement Y = node.Element("Y");
            XElement Name = node.Element("Name");
            XElement Index = node.Element("Index");
            XElement Text = node.Element("Text");
            XElement ImagePath = node.Element("ImagePath");
            XElement ImagePathMouseOver = node.Element("ImagePathMouseOver");
            XElement Width = node.Element("Width");
            XElement Height = node.Element("Height");
            XElement ScaleCenterX = node.Element("ScaleCenterX");
            XElement ScaleCenterY = node.Element("ScaleCenterY");
            XElement Type = node.Element("Type");
            XElement RefCharacterMarkerName = node.Element("RefCharacterMarkerName");

            if (ReferenceWidth != null && X != null && Y != null && !string.IsNullOrEmpty(X.Value) && !string.IsNullOrEmpty(Y.Value))
            {
                double referenceWidth = double.Parse(ReferenceWidth.Value);
                double x = double.Parse(X.Value);
                double y = double.Parse(Y.Value);
                LogicalLocation = new Point(x / referenceWidth, y / referenceWidth);
            }
            else throw new Exception("no location data found");

            if (Name == null)
                throw new Exception("'Name' cannot be null");

            if (Index == null)
            {
                Index = new XElement("Index");
                Index.Value = "0";
            }

            // create a new object for each item in collection
            DataPoint dp = new DataPoint()
            {
                Location = LogicalLocation,
                Name = Name.Value,
                Index = int.Parse(Index.Value),
            };

            if (Text != null)
                dp.Text = Text.Value;

            //if (ImagePath != null)
            //    dp.ImagePath = ImagePath.Value;

            //if (ImagePathMouseOver != null)
            //    dp.ImagePathMouseOver = ImagePathMouseOver.Value;

            if (Width != null && !string.IsNullOrEmpty(Width.Value))
                dp.Width = double.Parse(Width.Value);

            if (Height != null && !string.IsNullOrEmpty(Height.Value))
                dp.Height = double.Parse(Height.Value);

            //if (RefCharacterMarkerName != null && !string.IsNullOrEmpty(RefCharacterMarkerName.Value))
            //    dp.RefCharacterMarkerName = RefCharacterMarkerName.Value;

            if (ScaleCenterX != null && ScaleCenterY != null && !string.IsNullOrEmpty(ScaleCenterX.Value) && !string.IsNullOrEmpty(ScaleCenterY.Value))
            {
                double imageWidthFactor = double.Parse(Width.Value);
                double imageHeightFactor = double.Parse(Height.Value);
                double xScale = double.Parse(ScaleCenterX.Value) / imageWidthFactor;
                double yScale = double.Parse(ScaleCenterY.Value) / imageHeightFactor;
                dp.ScaleCenter = new Point(xScale, yScale);
            }

            if (Type != null && !string.IsNullOrEmpty(Type.Value))
            {
                dp.Type = Type.Value;
            }

            return dp;
        }

        void msi_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Zoom(double newzoom, Point p)
        {
            if (newzoom < 0.5)
            {
                newzoom = 0.5;
            }

            msi.ZoomAboutLogicalPoint(newzoom / zoom, p.X, p.Y);
            zoom = newzoom;
        }

        private void ZoomInClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Zoom(zoom * 1.3, msi.ElementToLogicalPoint(new Point(.5 * msi.ActualWidth, .5 * msi.ActualHeight)));
            TrackAnimation();
        }

        private void ZoomOutClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Zoom(zoom / 1.3, msi.ElementToLogicalPoint(new Point(.5 * msi.ActualWidth, .5 * msi.ActualHeight)));
            TrackAnimation();
        }

        private void GoHomeClick(object sender, System.Windows.RoutedEventArgs e)
        {
            GoHome();
            TrackAnimation();
        }

        /// <summary>
        /// Centers and Scales ZoomImage to initial position
        /// Also does this based on what aspect the browswer is at.
        /// Behavior between IE and FF is slightly diff since they have slightly different aspects due
        /// to chrome differences.
        /// </summary>
        public void GoHome()
        {
            // hide all markers

            //popMarker.Visibility = Visibility.Collapsed;
            //characterMarker.Visibility = Visibility.Collapsed;
            //processMarker.Visibility = Visibility.Collapsed;
            //bShowProcessMarker = false;

            double windowAspect = this.ActualWidth / this.ActualHeight;
            if (windowAspect > 1.5)
            {
                this.msi.ViewportWidth = 1.7; // 1.45; //a bigger ViewportWidth means smaller or more zoomed out
                this.msi.ViewportOrigin = new Point(-.35, -.075); //new Point(-.2, -.02);
            }
            else
            {
                this.msi.ViewportWidth = 1.2;
                this.msi.ViewportOrigin = new Point(-.1, -.05);
            }
            zoom = 1;
        }

        private void GoFullScreenClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!Application.Current.Host.Content.IsFullScreen)
            {
                Application.Current.Host.Content.IsFullScreen = true;
            }
            else
            {
                Application.Current.Host.Content.IsFullScreen = false;
            }
        }

        // Handling the VSM states
        private void LeaveMovie(object sender, System.Windows.Input.MouseEventArgs e)
        {
            VisualStateManager.GoToState(this, "FadeOut", true);
        }

        private void EnterMovie(object sender, System.Windows.Input.MouseEventArgs e)
        {
            VisualStateManager.GoToState(this, "FadeIn", true);
        }

        private void ZoomImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // hide any processMarker that might be showing
            //processMarker.Visibility = Visibility.Collapsed;
            //bShowProcessMarker = false;

            // note last mouse position and DeepZoom ViewportOrigin
            lastMouseDownPos = e.GetPosition(this.msi);
            lastMouseViewPort = this.msi.ViewportOrigin;
            _isDragging = true;

            TrackAnimation();
        }

        private void ZoomImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isDragging = false;
        }

        private void ZoomImage_MouseMove(object sender, MouseEventArgs e)
        {
            // move DeepZoom Image around
            lastMousePos = e.GetPosition(this.msi);
            if (_isDragging)
            {
                Point newPoint = lastMouseViewPort;
                newPoint.X += (lastMouseDownPos.X - lastMousePos.X) / this.msi.ActualWidth * this.msi.ViewportWidth;
                newPoint.Y += (lastMouseDownPos.Y - lastMousePos.Y) / this.msi.ActualWidth * this.msi.ViewportWidth;
                this.msi.ViewportOrigin = newPoint;
                TrackAnimation();
            }
        }

        // unused functions that show the inner math of Deep Zoom
        public Rect getImageRect()
        {
            return new Rect(-msi.ViewportOrigin.X / msi.ViewportWidth, -msi.ViewportOrigin.Y / msi.ViewportWidth, 1 / msi.ViewportWidth, 1 / msi.ViewportWidth * msi.AspectRatio);
        }

        public Rect ZoomAboutPoint(Rect img, double zAmount, Point pt)
        {
            return new Rect(pt.X + (img.X - pt.X) / zAmount, pt.Y + (img.Y - pt.Y) / zAmount, img.Width / zAmount, img.Height / zAmount);
        }

        public void LayoutDZI(Rect rect)
        {
            double ar = msi.AspectRatio;
            msi.ViewportWidth = 1 / rect.Width;
            msi.ViewportOrigin = new Point(-rect.Left / rect.Width, -rect.Top / rect.Width);
        }

        private void editModeButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
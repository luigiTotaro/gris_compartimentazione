﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace gris
{
    public class DragableUserControl : UserControl
    {
        private Point _location;

        public Point Location
        {
            get { return _location; }
            set { _location = value; }
        }

        private Point _scaleCenter;

        public Point ScaleCenter
        {
            get { return _scaleCenter; }
            set { _scaleCenter = value; }
        }


        public bool EditMode { get; set; }

        public double savedX { get; set; }
        public double savedY { get; set; }

        public double X { get; set; }
        public double Y { get; set; }

        public DragableUserControl()
        {
            EditMode = false;
            this.MouseLeftButtonDown += new MouseButtonEventHandler(DragableUserControl_MouseLeftButtonDown);
            this.MouseLeftButtonUp += new MouseButtonEventHandler(DragableUserControl_MouseLeftButtonUp);
            this.MouseMove += new MouseEventHandler(DragableUserControl_MouseMove);
            this.MouseEnter += new MouseEventHandler(DragableUserControl_MouseEnter);
            this.MouseLeave += new MouseEventHandler(DragableUserControl_MouseLeave);
        }

        void DragableUserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            VisualStateManager.GoToState(this, "normale", false);
        }

        void DragableUserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            if (EditMode)
                VisualStateManager.GoToState(this, "over", false);
        }

        public void SetPosition()
        {
            this.SetValue(Canvas.LeftProperty, X);
            this.SetValue(Canvas.TopProperty, Y);
        }

        void DragableUserControl_MouseMove(object sender, MouseEventArgs e)
        {
            DragHandler(sender, e);
        }

        void DragableUserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(this, "normale", false);
            DropHandler(sender);
        }

        void DragableUserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StartDragHandler(sender, e); ;
        }

        #region Drag&Drop

        bool isDragging = false;
        Point offset;

        private void StartDragHandler(object sender, MouseButtonEventArgs e)
        {
            if (EditMode && sender is DragableUserControl)
            {
                // Mark that we're doing a drag
                isDragging = true;

                // Ensure that the mouse can't leave the dragon
                this.CaptureMouse();

                // Determine where the mouse 'grabbed' 
                // to use during MouseMove
                offset = e.GetPosition(this);
            }
        }

        private void DropHandler(object sender)
        {
            if (EditMode && sender is DragableUserControl)
            {
                if (isDragging)
                {
                    // Turn off Drag and Drop
                    isDragging = false;

                    // Free the Mouse
                    this.ReleaseMouseCapture();
                }
            }
        }

        private void DragHandler(object sender, MouseEventArgs e)
        {
            if (EditMode && sender is DragableUserControl)
            {
                if (isDragging)
                {
                    VisualStateManager.GoToState(this, "drag", false);
                    // Where is the mouse now?
                    //TODO : Gestire correttamente il Canvas contenitore
                    Point newPosition = e.GetPosition((UIElement)this.Parent);
                    System.Diagnostics.Debug.WriteLine(String.Format("drag x={0} y={1}", newPosition.X.ToString(), newPosition.Y.ToString()));
                    X = newPosition.X - offset.X;
                    Y = newPosition.Y - offset.Y;
                    // Move the dragon via the new position less the offset
                    this.SetValue(Canvas.LeftProperty, X);
                    this.SetValue(Canvas.TopProperty, Y);
                    System.Diagnostics.Debug.WriteLine(String.Format("dropped x={0} y={1}", X.ToString(), Y.ToString()));
                    savedX = X;
                    savedY = Y;

                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MIXOnline.Descry;


namespace GrisTestProject
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ToolboxUnitTest
    {
        public ToolboxUnitTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        //Task:Caricamento toolbox
        //Gestire la generazione della lista delle periferiche e armadi posizionabili che non 
        //abbiano già una posizione.  (Assegnato a Cristian)



        [TestMethod]
        public void LoadDevicesInToolBoxTest()
        {
            //DataAccess.MSSEntities context = new MSSEntities();
            //List<tessere> deviceList = (from dev in context.tessere
            //                            where dev.abilitata_outdoor == true
            //                            select dev).ToList();
            //Assert.IsNotNull(deviceList);
        }


        [TestMethod]
        public void LoadDevicesInMapTest()
        {

			//PositionsReference.PositionsReference.GrisPositionsEntities context = new
			//    PositionsReference.PositionsReference.GrisPositionsEntities(new Uri("GrisPositionsDataService.svc", UriKind.Relative));
			//List<DataPoint> devices = (from dev in context.Devices.Expand("Table_2")
			//                          select new DataPoint()
			//                          {
			//                              Text = dev.Name
			//                          }).ToList();
			//Assert.IsNotNull(devices);
        }
    }
}

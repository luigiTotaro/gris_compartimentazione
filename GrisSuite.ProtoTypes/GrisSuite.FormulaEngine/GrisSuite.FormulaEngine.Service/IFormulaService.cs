﻿
using System.Collections.Generic;
using System.ServiceModel;
using GrisSuite.Data.Model;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Service
{
	[ServiceContract]
	public interface IFormulaService
	{
		[OperationContract]
		IList<GrisObject> EvaluateObjects ( IList<EvaluablePackage> objectsToEval );

		[OperationContract]
		void EvaluateAndSaveAllObjects ();

		[OperationContract]
		IList<GrisObject> EvaluateAllObjects ();
	}
}

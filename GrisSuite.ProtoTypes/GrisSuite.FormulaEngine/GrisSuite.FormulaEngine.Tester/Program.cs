﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GrisSuite.Data.Model;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Tester
{
	class Program
	{
		static void Main ( string[] args )
		{
			Console.WriteLine("Premere invio per il ricalcolo dello stato.");

			while ( Console.ReadKey().Key == ConsoleKey.Enter )
			{
				DoEval();
			} 
		}

		private static void DoEval ()
		{
			Console.WriteLine("Preparazione degli oggetti da valutare...");

			Stopwatch sw = new Stopwatch();
			sw.Start();

			DataAccessHelper.SyncObjects();
			var objectPcks = DataAccessHelper.GetAllObjects();

			sw.Stop();
			Console.WriteLine("--- Oggetti preparati in {0} millisecondi ---", sw.ElapsedMilliseconds);
			sw.Start();

			var formulaEngine = new Library.FormulaEngine();

			Console.WriteLine("Fine preparazione.");
			Console.WriteLine("Inizio valutazione...");

			try
			{
				formulaEngine.Eval(objectPcks);

				var regs = from evaluablePackage in objectPcks
						   where evaluablePackage.GrisObject is Region
						   select evaluablePackage.GrisObject;

				foreach ( GrisObject region in regs ) DataAccessHelper.SaveBranch(region);

				//Parallel.ForEach(regs, reg => DataAccessHelper.SaveBranch(reg));
			}
			catch ( AggregateException aExc )
			{
				foreach (var innerException in aExc.InnerExceptions)
				{
					Console.WriteLine(innerException.Message);
				}
			}
			catch ( Exception exc )
			{
				Console.WriteLine(exc.Message);
			}

			sw.Stop();

			foreach ( var pck in objectPcks )
			{
				GrisObject go = pck.GrisObject;
				dynamic sd = go.Attributes.Contains("SeverityDetail") ? go.Attributes["SeverityDetail"] : "";
				dynamic s = go.Attributes.Contains("Severity") ? go.Attributes["Severity"] : "";

				List<string> errors = new List<string>(20);
				if ( go.CustomEvaluationFormulas != null )
					foreach (var customEvaluationFormula in go.CustomEvaluationFormulas)
						if (customEvaluationFormula.HasErrorInEvaluation) errors.Add(customEvaluationFormula.EvaluationError);

				if ( go.DefaultEvaluationFormula.CanEvaluate && go.DefaultEvaluationFormula.HasErrorInEvaluation ) errors.Add(go.DefaultEvaluationFormula.EvaluationError);

				Console.WriteLine("Object type: {0}, Object id: {1}, severity: {2}, severitydetail: {3}, error: {4}", go.GetType(), go.SpecificId, s, sd, string.Join(", ", errors));
			}

			Console.WriteLine("--- {0} Oggetti valutati in {1} millisecondi ---", objectPcks.Count, sw.ElapsedMilliseconds);
		}
	}
}

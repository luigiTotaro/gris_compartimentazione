﻿
using System.Collections.Generic;
using System.Linq;

using GrisSuite.Data.Model;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Tester
{
	class Utility
	{
		public static IList<EvaluablePackage> GetObjects ()
		{
			using ( var context = new Entities() )
			{
				//TODO: Togliere dal conteggio le perif del sistema 99 e le "NoDevice"
				// return ( from go in context.GrisObjects.Include("ObjectAttributes") select go;
				// return ( from go in context.GrisObjects.OfType<Device>().Include("ObjectAttributes").Include("Server") select go ).Take(10).Cast<GrisObject>().ToList();
				int serverMinuteTimeout;
				Parameter param;
				var paramz = from parameter in context.Parameters where parameter.Name == "" select parameter;
				if ( paramz.Count() == 0 || ( param = paramz.SingleOrDefault() ) == null || !int.TryParse(param.Value, out serverMinuteTimeout) )
				{
					serverMinuteTimeout = 60;
				}

				var nodes = from grisObj in context.GrisObjects.Include("ObjectAttributes").OfType<Node>()
							where grisObj.Name.IndexOf("bologna") != -1
							select grisObj;

				var zones = from grisObj in context.GrisObjects.Include("ObjectAttributes").OfType<Zone>()
							where nodes.Select(n => n.Zone.Id).Contains(grisObj.Id)
							select grisObj;

				var regs = from grisObj in context.GrisObjects.Include("ObjectAttributes").OfType<Region>()
						   where zones.Select(z => z.Region).Contains(grisObj)
						   select grisObj;

				var syss = from grisObj in context.GrisObjects.Include("ObjectAttributes").OfType<NodeSystem>()
						   where nodes.Contains(grisObj.Node)
						   select grisObj;

				var devs = from grisObj in context.GrisObjects.Include("ObjectAttributes").Include("Server.ObjectAttributes").OfType<Device>()
						   where nodes.Contains(grisObj.Node)
						   select grisObj;

				var srvs = ( from grisObj in context.GrisObjects.Include("ObjectAttributes").OfType<Server>()
							 where devs.Select(d => d.Server).Contains(grisObj)
							 select grisObj ).Distinct();

				var objs = new List<GrisObject>();
				objs.AddRange(srvs);
				objs.AddRange(devs);
				objs.AddRange(syss);
				objs.AddRange(nodes);
				objs.AddRange(zones);
				objs.AddRange(regs);
				return objs.ConvertAll(go =>
				                       	{
				                       		if ( go is Server )
											{
												return new EvaluablePackage(go, new Dictionary<string, object>
												                         	{
												                         		{"serverMinuteTimeout", serverMinuteTimeout}
												                         	});
											}

				                       		return new EvaluablePackage(go, null);
				                       	});
			}
		}
	}
}

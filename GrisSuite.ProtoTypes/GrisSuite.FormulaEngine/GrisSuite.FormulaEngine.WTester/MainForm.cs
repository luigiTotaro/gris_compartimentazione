﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using GrisSuite.FormulaEngine.WTester.FormulaService;

namespace GrisSuite.FormulaEngine.WTester
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            this.InitializeComponent();
        }

        private void btnCalcola_Click(object sender, EventArgs e)
        {
			this.btnCalcola.Enabled = false;
			
			try
        	{
				FileUtility.LogLevel = (LoggingLevel) Enum.Parse(typeof(LoggingLevel), ( (int) this.numLogLevel.Value ).ToString());

				FileUtility.RemoveLogFile();

				Stopwatch sw = new Stopwatch();
				sw.Start();

				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Inizio valutazione...");
        		GrisObject[] evaluatedObjects = new GrisObject[0];

				var formulaService = new FormulaServiceClient();
				evaluatedObjects = formulaService.EvaluateAllObjects();

				sw.Stop();

				foreach ( var go in evaluatedObjects )
				{
					List<string> errors = new List<string>(20);
					if ( go.CustomEvaluationFormulas != null )
						foreach ( var customEvaluationFormula in go.CustomEvaluationFormulas )
							if ( customEvaluationFormula.HasErrorInEvaluation ) errors.Add(customEvaluationFormula.EvaluationError);

					if ( go.DefaultEvaluationFormula.HasErrorInEvaluation ) errors.Add(go.DefaultEvaluationFormula.EvaluationError);

					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format("Object type: {0}, Object id: {1}, severity: {2}, severitydetail: {3}, error: {4}", go.GetType(), go.SpecificId, go.SevLevel, go.SevLevelDetail, string.Join(", ", errors)));
				}

				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format("--- {0} Oggetti valutati in {1} millisecondi ---", evaluatedObjects.Length, sw.ElapsedMilliseconds));

        	    FileUtility.FlushLogToFile ();

				this.webBrowser1.Navigate(FileUtility.LogFile);
			}
        	catch (Exception exception)
        	{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format("Eccezione: {0}\r\nStack Trace: {1}\r\nInner Exception: {2}", exception.Message, exception.StackTrace, exception.InnerException));

                FileUtility.FlushLogToFile();

                this.webBrowser1.Navigate(FileUtility.LogFile);
            }
			finally
			{
				this.btnCalcola.Enabled = true;
			}
        }
    }
}
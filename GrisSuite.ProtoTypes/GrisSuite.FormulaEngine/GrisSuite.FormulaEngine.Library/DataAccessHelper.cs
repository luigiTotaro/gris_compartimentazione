﻿
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.FormulaEngine.Library
{
	public class DataAccessHelper
	{
		public static IList<EvaluablePackage> GetAllObjects ()
		{
			using ( var context = new Entities() )
			{
				int serverMinuteTimeout;
				Parameter param;
				var paramz = from parameter in context.Parameters where parameter.Name == "" select parameter;
				if ( paramz.Count() == 0 || ( param = paramz.SingleOrDefault() ) == null || !int.TryParse(param.Value, out serverMinuteTimeout) )
				{
					serverMinuteTimeout = 60;
				}

				var nodes = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Systems").Include("Servers").Include("Devices").OfType<Node>()
							//where grisObj.Name.IndexOf("teano") != -1
							select grisObj;

				var zones = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Nodes").OfType<Zone>()
							where nodes.Select(n => n.Zone.Id).Contains(grisObj.Id)
							select grisObj;

				var regs = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Zones.Nodes").OfType<Region>()
						   where zones.Select(z => z.Region).Contains(grisObj)
						   select grisObj;

				var syss = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Devices").OfType<NodeSystem>()
						   where nodes.Contains(grisObj.Node)
						   select grisObj;

				var devs = from grisObj in context.GrisObjects.Include("ObjectAttributes").Include("ObjectFormulas").Include("Server.ObjectAttributes").OfType<Device>()
						   where nodes.Contains(grisObj.Node)
						   select grisObj;

				var srvs = ( from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Devices").OfType<Server>()
							 where devs.Select(d => d.Server).Contains(grisObj)
							 select grisObj ).Distinct();

				var objs = new List<GrisObject>();
				objs.AddRange(srvs);
				objs.AddRange(devs);
				objs.AddRange(syss);
				objs.AddRange(nodes);
				objs.AddRange(zones);
				objs.AddRange(regs);
				return objs.ConvertAll(go =>
				{
					if ( go is Server )
					{
						return new EvaluablePackage(go, new Dictionary<string, object>
												                         	{
												                         		{"serverMinuteTimeout", serverMinuteTimeout}
												                         	});
					}

					return new EvaluablePackage(go, null);
				});
			}
		}

		public static void SyncObjects ()
		{
			using ( var dbContext = new Entities() )
			{
				dbContext.PopulateObjectStatus();
			}
		}

		public static void SaveBranch ( IStorable branchRoot )
		{
			using ( var dbContext = new Entities() )
			{
				dbContext.SaveBranch(branchRoot);
			}
		}
	}
}

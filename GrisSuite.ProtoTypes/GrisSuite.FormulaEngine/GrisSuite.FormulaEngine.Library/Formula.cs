﻿
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.FormulaEngine.Library
{
	public class Formula : IEvaluationFormula
	{
		private readonly string _text;
		private readonly string _scriptPath;

		public Formula ( string text, string scriptPath )
		{
			_text = text;
			_scriptPath = scriptPath;
		}

		public string ScriptPath
		{
			get { return this._scriptPath; }
		}

		public string Text
		{
			get { return this._text; }
		}

		public string EvaluationError { get; set; }

		public bool HasErrorInEvaluation { get; set; }
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.DynamicEvaluation;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace GrisSuite.FormulaEngine.Library
{
	public class FormulaEngine : IFormulaEngine
	{
		private static readonly object _locker = new object();
		private ScriptEngine _scriptEng;
		private readonly AppDomain _scriptAppDomain;
		private IList<EvaluablePackage> _objectsToEvalCache;
		private Entities _dbContext;

		private void InitializeEngine ()
		{
			//this._scriptAppDomain = AppDomain.CreateDomain("FormulaAppDomain");
			//this._scriptEng = Python.CreateEngine(this._scriptAppDomain, options);

			Dictionary<string, object> options = new Dictionary<string, object>();
			options["Debug"] = true;
			options["LightweightScopes"] = true;
			this._scriptEng = Python.CreateEngine(options);

			//this._scriptEng = Python.CreateEngine();
		}

		public void Eval ( IList<EvaluablePackage> objectsToEval )
		{
			this.InitializeEngine();

			this._objectsToEvalCache = objectsToEval;
			this._scriptEng.Runtime.LoadAssembly(typeof(IEvaluableObject).Assembly);

			ObjectDependencies.Instance.Reset();

			using ( this._dbContext = new Entities() )
			{
				var attributes = this._dbContext.Attributes.ToList();
				objectsToEval.ToList().ForEach(oe =>
				{
					this._dbContext.Attach(oe.GrisObject);
					oe.GrisObject.NeedExternalInitialization += SetContext;
				});
				foreach ( var objectToEval in objectsToEval ) objectToEval.GrisObject.InitializeForEvaluation(
																									new FormulaContext(this._scriptEng),
																									attributes,
																									this._dbContext,
																									objectToEval.State
																									).Evaluate();

				//Parallel.ForEach(objectsToEval, objectToEval => objectToEval.GrisObject.InitializeForEvaluation(
				//                                                                                    new FormulaContext(this._scriptEng),
				//                                                                                      attributes,
				//                                                                                    this._dbContext,
				//                                                                                    objectToEval.State
				//                                                                                    ).Evaluate());
			}

			this._scriptEng.Runtime.Shutdown();
			//AppDomain.Unload(this._scriptAppDomain);
		}

		private void SetContext ( object sender, EventArgs e )
		{
			if ( sender != null && sender is GrisObject )
			{
				var go = (GrisObject) sender;
				var cachedGo = this._objectsToEvalCache.SingleOrDefault(gp => gp.GrisObject.Id == go.Id);
				var attributes = this._dbContext.Attributes.ToList();

				go.InitializeForEvaluation(new FormulaContext(this._scriptEng), attributes, this._dbContext,
				                           ((cachedGo != null) ? cachedGo.State : null));
			}
		}
	}
}

using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Library
{
	public interface IFormulaEngine
	{
		void Eval ( IList<EvaluablePackage> objectsToEval );
	}
}
﻿
using System.Linq;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.FormulaEngine.Library
{
	public class EvaluableAttribute : IEvaluableAttribute
	{
		private readonly int _id;
		private readonly string _name;
		private readonly IEvaluationFormula _formula;
		private readonly GrisObject _grisObject;
		private bool _isEvaluating;

		public EvaluableAttribute ( int id, string name, IEvaluationFormula formula, GrisObject grisObject )
		{
			_id = id;
			_name = name;
			_formula = formula;
			_grisObject = grisObject;
		}

		public void Evaluate()
		{
			if ( !string.IsNullOrEmpty(this._formula.Text) || !string.IsNullOrEmpty(this._formula.ScriptPath) )
			{
				// TODO: ragionare sulla situazione in multithreading
				this._isEvaluating = true;
				this._grisObject.Context.Evaluate(this._formula);
				this._isEvaluating = false;
			}
		}

		public int Id
		{
			get { return this._id; }
		}

		public string Name
		{
			get { return this._name; }
		}

		private dynamic _value;
		public dynamic Value
		{
			get
			{
				if ( !this._isEvaluating ) this.Evaluate();
				return this._value;
			}
			set
			{
				var objAttribute = this._grisObject.ObjectAttributes.SingleOrDefault(oAtt => oAtt.Id == this._id);
				if ( objAttribute != null )
				{
					this._value = value;
					objAttribute.Value = this._value.ToString();
				}
				else
				{
					throw new FormulaEngineException("Impossibile assegnare un valore a questo attributo. Attributo non memorizzato in database.");
				}
			}
		}

		public IEvaluationFormula Formula
		{
			get { return this._formula; }
		}

		public IEvaluableAttributesContainer Container
		{
			get { return this._grisObject; }
		}
	}
}

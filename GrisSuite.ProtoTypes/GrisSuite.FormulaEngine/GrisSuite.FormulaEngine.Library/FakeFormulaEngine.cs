﻿using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.DynamicEvaluation;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Attribute = GrisSuite.Data.Model.Attribute;

namespace GrisSuite.FormulaEngine.Library
{
	public class FakeFormulaEngine : IFormulaEngine
	{
		private ScriptEngine _scriptEng;
		private IList<EvaluablePackage> _objectsToEvalCache;

		private void InitializeEngine ()
		{
			Dictionary<string, object> options = new Dictionary<string, object>();
			options["Debug"] = true;
			this._scriptEng = Python.CreateEngine(options);
		}

		public void Eval ( IList<EvaluablePackage> objectsToEval )
		{
			this.InitializeEngine();

			this._objectsToEvalCache = objectsToEval;
			this._scriptEng.Runtime.LoadAssembly(typeof(IEvaluableObject).Assembly);
			this._scriptEng.SetSearchPaths(new List<string> { ".\\Lib" });

			objectsToEval.ToList().ForEach(oe => { oe.GrisObject.NeedExternalInitialization += SetContext; });
			foreach ( var objectToEval in objectsToEval ) objectToEval.GrisObject.InitializeForEvaluation(
																								new FormulaContext(this._scriptEng),
																								this.BuildAttributes(),
																								null,
																								objectToEval.State
																								).Evaluate();

			this._scriptEng.Runtime.Shutdown();
			//AppDomain.Unload(this._scriptAppDomain);
		}

		public void SetContext ( object sender, EventArgs e )
		{
			if ( sender != null && sender is GrisObject )
			{
				var go = (GrisObject) sender;
				var cachedGo = this._objectsToEvalCache.SingleOrDefault(gp => gp.GrisObject.Id == go.Id);

				go.InitializeForEvaluation(new FormulaContext(this._scriptEng), this.BuildAttributes(), null,
										   ( ( cachedGo != null ) ? cachedGo.State : null ));
			}
		}

		public IList<Attribute> BuildAttributes ()
		{
			return new List<Attribute>
			       	{
			       		new Attribute
			       			{
			       				Id = 1,
			       				Name = "Severity"
			       			},			       		
							new Attribute
			       			{
			       				Id = 2,
			       				Name = "SeverityDetail"
			       			},
                            new Attribute
			       			{
			       				Id = 3,
			       				Name = "ExcludeFromParentStatus"
			       			},
                            new Attribute
			       			{
			       				Id = 4,
			       				Name = "Color"
			       			}
			       	};
		}
	}
}
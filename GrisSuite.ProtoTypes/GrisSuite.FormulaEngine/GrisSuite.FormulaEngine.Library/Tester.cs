﻿
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Model;

namespace GrisSuite.FormulaEngine.Library
{
	public class Tester
	{
		public static IList<EvaluablePackage> GetObjects ()
		{
			using ( var context = new Entities() )
			{
				//TODO: Togliere dal conteggio le perif del sistema 99 e le "NoDevice"
				// return ( from go in context.GrisObjects.Include("ObjectAttributes") select go;
				// return ( from go in context.GrisObjects.OfType<Device>().Include("ObjectAttributes").Include("Server") select go ).Take(10).Cast<GrisObject>().ToList();
				int serverMinuteTimeout;
				Parameter param;
				var paramz = from parameter in context.Parameters where parameter.Name == "" select parameter;
				if ( paramz.Count() == 0 || ( param = paramz.SingleOrDefault() ) == null || !int.TryParse(param.Value, out serverMinuteTimeout) )
				{
					serverMinuteTimeout = 60;
				}

				var nodes = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Systems").Include("Servers").Include("Devices").OfType<Node>()
							//where grisObj.Name.IndexOf("teano") != -1
							select grisObj;

				var zones = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Nodes").OfType<Zone>()
							where nodes.Select(n => n.Zone.Id).Contains(grisObj.Id)
							select grisObj;

				var regs = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Zones.Nodes").OfType<Region>()
						   where zones.Select(z => z.Region).Contains(grisObj)
						   select grisObj;

				var syss = from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Devices").OfType<NodeSystem>()
						   where nodes.Contains(grisObj.Node)
						   select grisObj;

				var devs = from grisObj in context.GrisObjects.Include("ObjectAttributes").Include("ObjectFormulas").Include("Server.ObjectAttributes").OfType<Device>()
						   where nodes.Contains(grisObj.Node)
						   select grisObj;

				var srvs = ( from grisObj in context.GrisObjects.Include("Children").Include("ObjectAttributes").Include("ObjectFormulas").Include("Devices").OfType<Server>()
							 where devs.Select(d => d.Server).Contains(grisObj)
							 select grisObj ).Distinct();

				//var sysVirts = from sys in syss
				//               join sysVirt in context.GrisObjects.Include("ObjectAttributes").Include("ObjectFormulas").OfType<VirtualObject>() on sys.Id equals sysVirt.ParentId 
				//               select sysVirt;

				//var nodeVirts = from grisObj in context.GrisObjects.Include("ObjectAttributes").Include("ObjectFormulas").OfType<VirtualObject>()
				//                from node in nodes
				//                from nodeVirt in node.Children.OfType<VirtualObject>()
				//                where grisObj.Id == nodeVirt.Id
				//                select nodeVirt;

				//var zoneVirts = from grisObj in context.GrisObjects.Include("ObjectAttributes").Include("ObjectFormulas").OfType<VirtualObject>()
				//                from zone in zones
				//                from zoneVirt in zone.Children.OfType<VirtualObject>()
				//                where grisObj.Id == zoneVirt.Id
				//                select zoneVirt;

				//var regVirts = from grisObj in context.GrisObjects.Include("ObjectAttributes").Include("ObjectFormulas").OfType<VirtualObject>()
				//                from reg in regs
				//                from regVirt in reg.Children.OfType<VirtualObject>()
				//                where grisObj.Id == regVirt.Id
				//                select regVirt;

				var objs = new List<GrisObject>();
				objs.AddRange(srvs);
				objs.AddRange(devs);
				//objs.AddRange(sysVirts);
				objs.AddRange(syss);
				//objs.AddRange(nodeVirts);
				objs.AddRange(nodes);
				//objs.AddRange(zoneVirts);
				objs.AddRange(zones);
				//objs.AddRange(regVirts);
				objs.AddRange(regs);
				return objs.ConvertAll(go =>
				{
					if ( go is Server )
					{
						return new EvaluablePackage(go, new Dictionary<string, object>
												                         	{
												                         		{"serverMinuteTimeout", serverMinuteTimeout}
												                         	});
					}

					return new EvaluablePackage(go, null);
				});
			}
		}
	}
}

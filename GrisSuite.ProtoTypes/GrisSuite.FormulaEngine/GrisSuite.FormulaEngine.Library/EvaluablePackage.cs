﻿
using System;
using System.Collections.Generic;
using GrisSuite.Data.Model;

namespace GrisSuite.FormulaEngine.Library
{
	[Serializable]
	public class EvaluablePackage
	{
		private readonly GrisObject _grisObject;
		private readonly Dictionary<string, object> _state;

        public EvaluablePackage(GrisObject grisObject)
        {
            _grisObject = grisObject;
            _state = null;
        }

		public EvaluablePackage ( GrisObject grisObject, Dictionary<string, object> state )
		{
			_grisObject = grisObject;
			_state = state;
		}

		public GrisObject GrisObject
		{
			get { return _grisObject; }
		}

		public Dictionary<string, object> State
		{
			get { return _state; }
		}
	}
}

﻿
using System.Linq;
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	/*
	 * todo andare in db
	 * */

	public sealed class NotAvailable : ObjectStatus
	{
		private NotAvailable ()
		{
			Id = -1;
			this.Description = "Stato non disponibile";
			this.Severity = GrisSeverities.NotClassified;
		}

		public static NotAvailable Status
		{
			get { return new NotAvailable(); }
		}
	}

	public sealed class Ok : ObjectStatus
	{
		private Ok ()
		{
			Id = 0;
			this.Description = "In servizio";
			this.Severity = GrisSeverities.Ok;
		}

		public static Ok Status
		{
			get { return new Ok(); }
		}
	}

	public sealed class Warning : ObjectStatus
	{
		private Warning ()
		{
			Id = 1;
			this.Description = "Anomalia lieve";
			this.Severity = GrisSeverities.Warning;
		}

		public static Warning Status
		{
			get { return new Warning(); }
		}
	}

	public sealed class Error : ObjectStatus
	{
		private Error ()
		{
			Id = 2;
			this.Description = "Anomalia grave";
			this.Severity = GrisSeverities.Error;
		}

		public static Error Status
		{
			get { return new Error(); }
		}
	}

	public sealed class Offline : ObjectStatus
	{
		private Offline ()
		{
			Id = 3;
			this.Description = "Offline";
			this.Severity = GrisSeverities.Offline;
		}

		public static Offline Status
		{
			get { return new Offline(); }
		}
	}

	public sealed class NotActive : ObjectStatus
	{
		private NotActive ()
		{
			Id = 4;
			this.Description = "Non attivo";
			this.Severity = GrisSeverities.NotActive;
		}

		public static NotActive Status
		{
			get { return new NotActive(); }
		}
	}

	public sealed class Unknown : ObjectStatus
	{
		private Unknown ()
		{
			Id = 5;
			this.Description = "Sconosciuto";
			this.Severity = GrisSeverities.Unknown;
		}

		public static Unknown Status
		{
			get { return new Unknown(); }
		}
	}

	public sealed class DiagnosticNotAvailable : ObjectStatus
	{
		private DiagnosticNotAvailable ()
		{
			Id = 6;
			this.Description = "Diagnostica non disponibile";
			this.Severity = GrisSeverities.NotActive;
		}

		public static DiagnosticNotAvailable Status
		{
			get { return new DiagnosticNotAvailable(); }
		}
	}

	public sealed class DiagnosticDisabledFromOperator : ObjectStatus
	{
		private DiagnosticDisabledFromOperator ()
		{
			Id = 7;
			this.Description = "Diagnostica disattivata dall'operatore";
			this.Severity = GrisSeverities.NotActive;
		}

		public static DiagnosticDisabledFromOperator Status
		{
			get { return new DiagnosticDisabledFromOperator(); }
		}
	}

	public sealed class DuplicatedIPAddress : ObjectStatus
	{
		private DuplicatedIPAddress ()
		{
			Id = 8;
			this.Description = "Ip duplicato";
			this.Severity = GrisSeverities.NotActive;
		}

		public static DuplicatedIPAddress Status
		{
			get { return new DuplicatedIPAddress(); }
		}
	}

	public sealed class ServerNotActive : ObjectStatus
	{
		private ServerNotActive ()
		{
			Id = 9;
			this.Description = "Collettore STLC1000 non attivo";
			this.Severity = GrisSeverities.NotActive;
		}

		public static ServerNotActive Status
		{
			get { return new ServerNotActive(); }
		}
	}

	public sealed class ServerOffline : ObjectStatus
	{
		private ServerOffline ()
		{
			Id = 10;
			this.Description = "Collettore STLC1000 non raggiungibile (offline)";
			this.Severity = GrisSeverities.Unknown;
		}

		public static ServerOffline Status
		{
			get { return new ServerOffline(); }
		}
	}

	public sealed class Old : ObjectStatus
	{
		private Old ()
		{
			Id = 11;
			this.Description = "Storico";
			this.Severity = GrisSeverities.NotClassified;
		}

		public static Old Status
		{
			get { return new Old(); }
		}
	}

	public sealed class WithoutIPAddress : ObjectStatus
	{
		private WithoutIPAddress ()
		{
			Id = 12;
			this.Description = "Senza indirizzo IP";
			this.Severity = GrisSeverities.NotActive;
		}

		public static WithoutIPAddress Status
		{
			get { return new WithoutIPAddress(); }
		}
	}

	public sealed class MIB2Only : ObjectStatus
	{
		private MIB2Only ()
		{
			Id = 13;
			this.Description = "Dati diagnostici limitati alla sola sezione MIB-II";
			this.Severity = GrisSeverities.NotActive;
		}

		public static MIB2Only Status
		{
			get { return new MIB2Only(); }
		}
	}

	public sealed class WithoutHostName : ObjectStatus
	{
		private WithoutHostName ()
		{
			Id = 14;
			this.Description = "Senza FullHostName";
			this.Severity = GrisSeverities.NotActive;
		}

		public static WithoutHostName Status
		{
			get { return new WithoutHostName(); }
		}
	}

	public sealed class WithoutMACAddress : ObjectStatus
	{
		private WithoutMACAddress ()
		{
			Id = 15;
			this.Description = "Senza MAC Address";
			this.Severity = GrisSeverities.NotActive;
		}

		public static WithoutMACAddress Status
		{
			get { return new WithoutMACAddress(); }
		}
	}

	public sealed class InconsistentSNMPData : ObjectStatus
	{
		private InconsistentSNMPData ()
		{
			Id = 16;
			this.Description = "Dati SNMP incongruenti";
			this.Severity = GrisSeverities.NotActive;
		}

		public static InconsistentSNMPData Status
		{
			get { return new InconsistentSNMPData(); }
		}
	}

	public sealed class MIB2OnlyWarning : ObjectStatus
	{
		private MIB2OnlyWarning ()
		{
			Id = 17;
			this.Description = "Dati diagnostici limitati alla sola sezione MIB-II";
			this.Severity = GrisSeverities.Warning;
		}

		public static MIB2OnlyWarning Status
		{
			get { return new MIB2OnlyWarning(); }
		}
	}

	public sealed class InconsistentSNMPDataWarning : ObjectStatus
	{
		private InconsistentSNMPDataWarning ()
		{
			Id = 18;
			this.Description = "Dati SNMP incongruenti";
			this.Severity = GrisSeverities.Warning;
		}

		public static InconsistentSNMPDataWarning Status
		{
			get { return new InconsistentSNMPDataWarning(); }
		}
	}

	public sealed class DiagnosticOffline : ObjectStatus
	{
		private DiagnosticOffline ()
		{
			Id = 19;
			this.Description = "Diagnostica non raggiungibile";
			this.Severity = GrisSeverities.Unknown;
		}

		public static DiagnosticOffline Status
		{
			get { return new DiagnosticOffline(); }
		}
	}

	public sealed class DiagnosticNotAvailableWarning : ObjectStatus
	{
		private DiagnosticNotAvailableWarning ()
		{
			Id = 20;
			this.Description = "Diagnostica non disponibile";
			this.Severity = GrisSeverities.Warning;
		}

		public static DiagnosticNotAvailableWarning Status
		{
			get { return new DiagnosticNotAvailableWarning(); }
		}
	}

	public sealed class IncompleteDiagnosticalData : ObjectStatus
	{
		private IncompleteDiagnosticalData ()
		{
			Id = 21;
			this.Description = "Dati di monitoraggio incompleti";
			this.Severity = GrisSeverities.Warning;
		}

		public static IncompleteDiagnosticalData Status
		{
			get { return new IncompleteDiagnosticalData(); }
		}
	}

	public sealed class IncompleteServerData : ObjectStatus
	{
		private IncompleteServerData ()
		{
			Id = 22;
			this.Description = "Dati del collettore incompleti";
			this.Severity = GrisSeverities.Error;
		}

		public static IncompleteServerData Status
		{
			get { return new IncompleteServerData(); }
		}
	}

	public sealed class NA : ObjectStatus
	{
		private NA ()
		{
			Id = 23;
			this.Description = "Non applicabile";
			this.Severity = GrisSeverities.Unknown;
		}

		public static NA Status
		{
			get { return new NA(); }
		}
	}

	public class ObjectStatus
	{
		public ObjectStatus ()
		{
			this.Id = -1;
			this.Description = "Stato non disponibile";
			this.Severity = GrisSeverities.NotClassified;
		}

		public int Id { get; protected set; }
		public string Description { get; protected set; }
		public GrisSeverities Severity { get; protected set; }
		public static IList<ObjectStatus> KnownTypes
		{
			get
			{
				return new List<ObjectStatus>
				            	{
				            		NotAvailable.Status, Ok.Status, Warning.Status, Error.Status, Offline.Status,
									NotActive.Status, Unknown.Status, DiagnosticNotAvailable.Status, DiagnosticDisabledFromOperator.Status,
									DuplicatedIPAddress.Status, ServerNotActive.Status, ServerOffline.Status, Old.Status, WithoutIPAddress.Status,
									MIB2Only.Status, WithoutHostName.Status, WithoutMACAddress.Status, InconsistentSNMPData.Status, MIB2OnlyWarning.Status,
									DiagnosticOffline.Status, DiagnosticNotAvailableWarning.Status, IncompleteDiagnosticalData.Status, IncompleteServerData.Status, NA.Status
				            	};
			}
		}

		public static ObjectStatus FromInt32 ( int status )
		{
			return KnownTypes.SingleOrDefault(os => os.Id == status);
		}

		public static implicit operator int ( ObjectStatus status )
		{
			return status.Id;
		}

		public static explicit operator ObjectStatus ( int status )
		{
			return FromInt32(status);
		}

		public override string ToString ()
		{
			return this.Description;
		}
	}

}

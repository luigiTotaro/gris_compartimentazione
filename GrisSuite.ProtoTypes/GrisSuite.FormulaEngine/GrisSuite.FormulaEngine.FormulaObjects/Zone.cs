﻿
using System;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	public class Zone : GrisObject
	{
		public Zone ( Guid objectId )
			: base(objectId)
		{
			base.Type = ObjectTypes.Zone;
		}
	}
}
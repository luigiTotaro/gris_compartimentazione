﻿
using System;
using System.Data;
using System.Data.SqlClient;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	public class Server : GrisObject
	{
		public long ServerId { get; set; }

		public Server ( Guid objectId )
			: base(objectId)
		{
			this.Type = ObjectTypes.Server;
		}

		public static Server GetByDevice ( Device device )
		{
			const string QRY = @"
select distinct os.ObjectStatusId, os.ObjectId, os.SevLevelDetailId 
from object_devices od 
inner join devices d on od.ObjectId = d.DevID
inner join servers s on d.SrvID = s.SrvID
inner join object_servers os on s.SrvID = os.ObjectId
where od.ObjectStatusId = @ObjectStatusId";

			using ( SqlConnection cnn = new SqlConnection("Data Source=(local);Initial Catalog=GrisSuite.DB.Server;Integrated Security=True;") )
			using ( SqlCommand cmd = new SqlCommand(QRY, cnn) )
			{
				cmd.Parameters.Add("ObjectStatusId", SqlDbType.UniqueIdentifier).Value = device.Id;

				try
				{
					cnn.Open();
					var reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.SingleResult);

					Server grisObject = null;
					if ( reader.Read() )
					{
						grisObject = new Server(reader.GetGuid(0));

						grisObject.SpecificId = reader.GetInt64(1);
						grisObject.Status = (ObjectStatus) reader.GetInt32(2);
					}
					else
					{
						throw new FormulaObjectsException(string.Format("Nessun server in db padre di una periferica con id '{0}'", device.Id));
					}
					cnn.Close();

					return grisObject;
				}
				catch ( SqlException exc )
				{
					throw new FormulaObjectsException(string.Format("Errore durante la richiesta dei dati attraverso il metodo 'GetByDevice' con parametro '{0}'", device.Id), exc);
				}
			}
		}
	}
}
﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
    public class Device : GrisObject
    {
		public Device ( Guid objectId )
			: base(objectId)
        {
            this.Type = ObjectTypes.Device;
			this.Attributes = new Dictionary<string, GrisObjectAttribute>
			                    {
			                        {
			                            "Status",
			                            new GrisObjectAttribute(new EvaluationFormula(this, null, @"..\..\..\GrisSuite.FormulaEngine.Formulas\DeviceDefaultStatus.py"))
			                        }
			                    };

			//TODO: Caricamento attributi da db => un attributo definito in db con lo stesso nome sovrascrive quello di default dichiarato staticamente


        }

		public long DeviceId { get { return this.SpecificId; } }
    	public Guid? ServerId { get; private set; }

		private Server _server;
		public Server Server
    	{
    		get { return this._server = this._server ?? new ServerRelation(this).GetParent(this.ServerId); }
    	}

		private string _name;
		public string Name
    	{
    		get {
    			return _name;
    		}
    	}

    	public override void Load ()
		{
			var gObj = this.GetFromDb(this.Id);
			this.SpecificId = gObj.SpecificId;
			this.Status = gObj.Status;
			this.ServerId = gObj.ServerId;
		}

		private Device GetFromDb ( Guid objectId )
		{
			const string QRY = @"
select od.ObjectId, od.SevLevel, os.ObjectStatusId
from object_devices od 
left join devices d on d.DevID = od.ObjectId 
left join object_servers os on os.ObjectId = d.SrvID
where od.ObjectStatusId = @ObjectStatusId";

			using ( SqlConnection cnn = new SqlConnection("Data Source=(local);Initial Catalog=GrisSuite.DB.Server;Integrated Security=True;") )
			using ( SqlCommand cmd = new SqlCommand(QRY, cnn) )
			{
				cmd.Parameters.Add("ObjectStatusId", SqlDbType.UniqueIdentifier).Value = objectId;

				try
				{
					cnn.Open();
					var reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.SingleResult | CommandBehavior.CloseConnection);

					Device grisObject = null;
					if ( reader.Read() )
					{
						grisObject = new Device(objectId);

						grisObject.SpecificId = reader.GetInt64(0);
						grisObject.Status = (ObjectStatus) reader.GetInt32(1);
						grisObject.ServerId = reader.IsDBNull(2) ? null : (Guid?)reader.GetGuid(2);
					}
					else
					{
						throw new FormulaObjectsException(string.Format("Nessun oggetto in db con id '{0}'", objectId));
					}

					return grisObject;
				}
				catch ( SqlException exc )
				{
					throw new FormulaObjectsException(string.Format("Errore durante la richiesta dei dati dell' oggetto con id '{0}'", objectId), exc);
				}
			}
		}
	}

	public class ServerRelation
	{
		private readonly Device _deviceReference;
		private static readonly Dictionary<Guid, Server> _internalDict = new Dictionary<Guid, Server>(1000);

		public ServerRelation ( Device device )
		{
			_deviceReference = device;
		}

		public Server GetParent ( Guid? serverId )
		{
			if ( serverId.HasValue )
			{
				if ( _internalDict.ContainsKey(serverId.Value) )
				{
					return _internalDict[serverId.Value];
				}

				var server = Server.GetByDevice(this._deviceReference);

				if ( serverId.Value != server.Id ) throw new Exception(); //TODO: finire
				_internalDict.Add(server.Id, server); //TODO: gestire thread safety

				return server;
			}
			else
			{
				throw new FormulaObjectsException(""); //TODO:
			}
		}
	}
}
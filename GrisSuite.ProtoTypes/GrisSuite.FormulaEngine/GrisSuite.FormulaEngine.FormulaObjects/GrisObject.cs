﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	public class GrisObject : IGrisObject, IEvaluableObject
	{
		private readonly Guid _objectId;

		public Guid Id { get { return this._objectId; } }
		public ObjectStatus Status { get; set; }
		public ObjectTypes Type { get; protected set; }
		public long SpecificId { get; set; }
		public Dictionary<string, GrisObjectAttribute> Attributes { get; set; }
		public bool InMaintenance { get; set; }

		public GrisObject ( Guid objectId )
		{
			if ( objectId == Guid.Empty )
			{
				throw new FormulaObjectsException("Id oggetto obbligatorio");
			}

			this._objectId = objectId;
			this.Status = NotAvailable.Status;

			//this.Attributes = new Dictionary<string, GrisObjectAttribute>
			//                    {
			//                        {
			//                            "Status",
			//                            new GrisObjectAttribute(new EvaluationFormula(this, null, @"..\..\..\GrisSuite.FormulaEngine.Formulas\\DefaultStatus.py"))
			//                        }
			//                    };

			this.Attributes = new Dictionary<string, GrisObjectAttribute>(0);
		}

		public static GrisObject GetSpecificObject ( ObjectTypes objectType, Guid objectId, bool loadData = false )
		{
			GrisObject grisObject;
			switch ( objectType )
			{
				case ObjectTypes.Device:
					{
						grisObject = new Device(objectId);
						break;
					}
				case ObjectTypes.NodeSystems:
					{
						grisObject = new NodeSystem(objectId);
						break;
					}
				case ObjectTypes.Server:
					{
						grisObject = new Server(objectId);
						break;
					}
				case ObjectTypes.Node:
					{
						grisObject = new Node(objectId);
						break;
					}
				case ObjectTypes.Zone:
					{
						grisObject = new Zone(objectId);
						break;
					}
				case ObjectTypes.Region:
					{
						grisObject = new Region(objectId);
						break;
					}
				default:
					{
						grisObject = new GrisObject(objectId);
						break;
					}
			}

			if ( loadData ) grisObject.Load();
			return grisObject;
		}

		public virtual void Load ()
		{
			var gObj = GetById(this.Id);
			this.Type = gObj.Type;
			this.SpecificId = gObj.SpecificId;
			this.Status = gObj.Status;
		}

		public static GrisObject GetById ( Guid objectId )
		{
			const string QRY = "select ObjectId, ObjectTypeId, SevLevel, InMaintenance from object_status where ObjectStatusId = @ObjectStatusId";
			using ( SqlConnection cnn = new SqlConnection("Data Source=(local);Initial Catalog=GrisSuite.DB.Server;Integrated Security=True;") )
			using ( SqlCommand cmd = new SqlCommand(QRY, cnn) )
			{
				cmd.Parameters.Add("ObjectStatusId", SqlDbType.UniqueIdentifier).Value = objectId;

				try
				{
					cnn.Open();
					var reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.SingleResult);

					GrisObject grisObject = null;
					if ( reader.Read() )
					{
						grisObject = new GrisObject(objectId);

						grisObject.SpecificId = reader.GetInt64(0);
						grisObject.Type = (ObjectTypes) reader.GetInt32(1);
						grisObject.Status = (ObjectStatus) reader.GetInt32(2);
						grisObject.InMaintenance = reader.GetBoolean(3);
					}
					else
					{
						throw new FormulaObjectsException(string.Format("Nessun oggetto in db con id '{0}'", objectId));
					}
					cnn.Close();

					return grisObject;
				}
				catch ( SqlException exc )
				{
					throw new FormulaObjectsException(string.Format("Errore durante la richiesta dei dati dell' oggetto con id '{0}'", objectId), exc);
				}
			}
		}

		public void Evaluate ( IEvaluationContext context )
		{
			if ( this.Attributes != null && this.Attributes.Count > 0 )
			{
				foreach (var attribute in Attributes)
				{
					if ( attribute.Value != null && attribute.Value.AttributeFormula != null ) attribute.Value.AttributeFormula.Evaluate(context);
				}
			}
		}
	}
}
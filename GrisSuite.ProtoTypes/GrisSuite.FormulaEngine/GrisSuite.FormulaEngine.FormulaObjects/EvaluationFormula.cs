﻿
namespace GrisSuite.FormulaEngine.FormulaObjects
{
	public class EvaluationFormula : IEvaluableObject
	{
		public string Formula { get; private set; }

		private readonly bool _isValid;

		public bool IsValid
		{
			get { return this._isValid; }
		}

		public bool HasErrorInEvaluation { get; set; }
		public string EvaluationError { get; set; }
		public string PythonFilePath { get; private set; }
		public GrisObject Owner { get; private set; }

		public EvaluationFormula ( GrisObject owner, string formulaCode, string pythonFilePath )
		{
			if ( owner == null )
			{
				throw new FormulaObjectsException("L'oggetto contenitore della formula è obbligatorio");
			}

			if ( string.IsNullOrWhiteSpace(formulaCode) && string.IsNullOrWhiteSpace(pythonFilePath) )
			{
				throw new FormulaObjectsException("Codice della formula obbligatorio");
			}

			this.Owner = owner;
			this.Formula = formulaCode;
			this._isValid = true;
			this.EvaluationError = string.Empty;
			this.PythonFilePath = pythonFilePath;
			this.HasErrorInEvaluation = false;
		}

		public void Evaluate ( IEvaluationContext context )
		{
			context.Evaluate(this);
		}
	}

	public interface IEvaluableObject
	{
		void Evaluate ( IEvaluationContext context );
	}

	public interface IEvaluationContext
	{
		void Evaluate ( EvaluationFormula formula );
	}
}
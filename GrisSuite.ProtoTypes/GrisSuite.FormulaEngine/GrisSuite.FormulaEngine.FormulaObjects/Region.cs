﻿
using System;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
    public class Region : GrisObject
    {
		public Region ( Guid objectId )
			: base(objectId)
        {
            base.Type = ObjectTypes.Region;
        }
    }
}
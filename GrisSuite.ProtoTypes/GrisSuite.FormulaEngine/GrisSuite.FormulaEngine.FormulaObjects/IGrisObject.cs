﻿
using System;
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	public interface IGrisObject
	{
		Guid Id { get; }
		ObjectStatus Status { get; set; }
		ObjectTypes Type { get; }
		long SpecificId { get; set; }
		Dictionary<string, GrisObjectAttribute> Attributes { get; set; }
		bool InMaintenance { get; set; }

		void Load();
	}
}

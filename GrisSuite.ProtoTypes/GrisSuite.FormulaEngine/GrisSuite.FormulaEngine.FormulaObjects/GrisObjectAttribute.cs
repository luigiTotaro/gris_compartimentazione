﻿
namespace GrisSuite.FormulaEngine.FormulaObjects
{
    public class GrisObjectAttribute
    {
        public EvaluationFormula AttributeFormula { get; private set; }

        public dynamic AttributeFormulaValue { get; set; }

        public GrisObjectAttribute(EvaluationFormula attributeFormula)
        {
            if (attributeFormula == null)
            {
                throw new FormulaObjectsException("Formula per l'attributo obbligatoria");
            }

            this.AttributeFormula = attributeFormula;
            this.AttributeFormulaValue = null;
        }
    }
}
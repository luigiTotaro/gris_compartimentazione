﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	[Serializable]
	public class FormulaObjectsException : Exception
	{
		public FormulaObjectsException ()
		{
		}

		public FormulaObjectsException ( string message )
			: base(message)
		{
		}

		public FormulaObjectsException ( string message, Exception inner )
			: base(message, inner)
		{
		}

		protected FormulaObjectsException ( SerializationInfo info, StreamingContext context )
			: base(info, context)
		{
		}
	}
}
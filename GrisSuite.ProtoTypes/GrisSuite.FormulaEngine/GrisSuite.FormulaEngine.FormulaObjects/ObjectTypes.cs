﻿
namespace GrisSuite.FormulaEngine.FormulaObjects
{
    /// <summary>
    ///   Mappa con la tabella object_type in base dati
    /// </summary>
    public enum ObjectTypes
    {
        Unknown = 0,
        Region = 1,
        Zone = 2,
        Node = 3,
        NodeSystems = 4,
        Server = 5,
        Device = 6
    }
}
﻿
namespace GrisSuite.FormulaEngine.FormulaObjects
{
    /// <summary>
    ///   Rappresenta la severità dei valori ricevuti
    /// </summary>
    public enum GrisSeverities
    {
        /// <summary>
        ///   Valore informativo (valido solo per stream e stream field)
        /// </summary>
        Info = -255,
		/// <summary>
		///   Non classificato
		/// </summary>
		NotClassified = -1,
		/// <summary>
		///   Stato normale
		/// </summary>
		Ok = 0,
		/// <summary>
        ///   Allarme lieve
        /// </summary>
        Warning = 1,
		/// <summary>
		///   Allarme grave
		/// </summary>
		Error = 2,
		/// <summary>
		///   Offline
		/// </summary>
		Offline = 3,
		/// <summary>
		///   Non attivo
		/// </summary>
		NotActive = 9,
		/// <summary>
        ///   Sconosciuto o nessuna risposta
        /// </summary>
        Unknown = 255
    }
}
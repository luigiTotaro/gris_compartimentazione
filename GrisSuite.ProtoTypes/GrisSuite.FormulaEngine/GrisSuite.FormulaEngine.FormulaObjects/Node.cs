﻿
using System;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
    public class Node : GrisObject
    {
		public Node ( Guid objectId )
			: base(objectId)
        {
            base.Type = ObjectTypes.Node;
        }
    }
}
﻿
using System.Data.Objects;

namespace GrisSuite.FormulaEngine.FormulaObjects.DB
{
	public class DataContext : ObjectContext
	{
		public DataContext ()
			: base("name=GrisSuiteServerEntities", "GrisSuiteServerEntities")
		{
			_grisObjects = CreateObjectSet<GrisObject>();
			_devices = CreateObjectSet<Device>();
			_servers = CreateObjectSet<Server>();
		}

		private readonly ObjectSet<GrisObject> _grisObjects;
		public ObjectSet<GrisObject> GrisObjects
		{
			get { return this._grisObjects; }
		}

		private readonly ObjectSet<Device> _devices;
		public ObjectSet<Device> Devices
		{
			get { return this._devices; }
		}

		private readonly ObjectSet<Server> _servers;
		public ObjectSet<Server> Servers
		{
			get { return this._servers; }
		}
	}
}

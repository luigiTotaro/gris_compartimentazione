﻿
using System;

namespace GrisSuite.FormulaEngine.FormulaObjects
{
	public class NodeSystem : GrisObject
	{
		public NodeSystem ( Guid objectId )
			: base(objectId)
		{
			base.Type = ObjectTypes.NodeSystems;
		}
	}
}
﻿
using System;
using System.Collections.Generic;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.Severities;
using GrisSuite.FormulaEngine.Library;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	[TestFixture]
	public class ConsistenceTests
	{
		[Test]
		public void QuandoPriferica_NonAttivaPercheServerNonAttivo_InSeguito_FormulaCustomImpostaServerOffline_MiAspettoPerifericaOffline ()
		{
			// situazione non realistica, ipotizzata solo per riprodurre il ricalcolo oggetti dipendenti in cascata
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev1 = new Device();
			var srv = new Server();
			var virtualObj = new VirtualObject();
			var dev2 = new Device();

			// attributes
			Utility.SetupEntities(dev1, srv, virtualObj, dev2);

			// entity data
			srv.SeverityDetail = NotActive.SeverityDetail;
			dev2.IsOffline = Convert.ToByte(true);
			dev2.SeverityDetail = Offline.SeverityDetail;

			// relations
			dev1.Server = srv;
			virtualObj.Parent = srv;
			virtualObj.Children.Add(dev2);

			// formulas
			var formulaDev1 = new ObjectFormula();

			formulaDev1.Index = (int) EvaluationFormulaType.Default;
			formulaDev1.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formulaDev1.GrisObject = dev1;
			formulaDev1.ObjectId = dev1.Id;
			dev1.ObjectFormulas.Add(formulaDev1);

			var formulaSrv = new ObjectFormula();

			formulaSrv.Index = (int) EvaluationFormulaType.Default;
			formulaSrv.ScriptPath = @".\Lib\ServerDefaultStatus.py";
			formulaSrv.GrisObject = srv;
			formulaSrv.ObjectId = srv.Id;
			srv.ObjectFormulas.Add(formulaSrv);

			var formulaVirtualObj = new ObjectFormula();

			formulaVirtualObj.Index = (int) EvaluationFormulaType.Custom;
			formulaVirtualObj.ScriptPath = @".\Lib\ObjectScripts\TestVirtualObject_CriticalGroup_ForDependencies.py";
			formulaVirtualObj.GrisObject = virtualObj;
			formulaVirtualObj.ObjectId = virtualObj.Id;
			virtualObj.ObjectFormulas.Add(formulaVirtualObj);

			// il server deve risultare calcolato, forzo lo stato desiderato perchè non impostabile via formula
			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(srv) });
			srv.ForceSeverity(NotActive.SeverityDetail);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev1) });

			foreach ( var customEvaluationFormula in dev1.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(dev1.DefaultEvaluationFormula.HasErrorInEvaluation, dev1.DefaultEvaluationFormula.EvaluationError);

			Assert.AreEqual(Severity.NotActive, srv.Severity);
			Assert.AreEqual(ServerNotActive.SeverityDetail, dev1.SeverityDetail);

			// fase 2 //
			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(virtualObj) });

			foreach ( var customEvaluationFormula in virtualObj.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(virtualObj.DefaultEvaluationFormula.HasErrorInEvaluation, virtualObj.DefaultEvaluationFormula.EvaluationError);

			Assert.AreEqual(Severity.Offline, srv.Severity);
			Assert.AreEqual(ServerOffline.SeverityDetail, dev1.SeverityDetail);
			Assert.AreEqual(Offline.SeverityDetail, virtualObj.SeverityDetail);
			Assert.AreEqual(Offline.SeverityDetail, dev2.SeverityDetail);
		}

		[Test]
		public void QuandoStazione_AccedeASistemaComeSottoOggetto_EAssegnaStatoErrore_MiAspettoStazioneInErrore ()
		{
			var formulaEng = new FakeFormulaEngine();

			var system = new NodeSystem();
			var node = new Node();

			// attributes
			Utility.SetupEntities(system, node);

			// entity data
			system.Name = "S0";
			system.SeverityDetail = Error.SeverityDetail;

			// relations
			node.Systems.Add(system);

			// formulas
			var formulaNode = new ObjectFormula();

			formulaNode.Index = (int) EvaluationFormulaType.Custom;
			formulaNode.ScriptPath = @".\Lib\ObjectScripts\TestNode_ImplicitGetObjectByName.py";
			formulaNode.GrisObject = node;
			formulaNode.ObjectId = node.Id;
			node.ObjectFormulas.Add(formulaNode);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(node) });

			foreach ( var customEvaluationFormula in node.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(node.DefaultEvaluationFormula.HasErrorInEvaluation, node.DefaultEvaluationFormula.EvaluationError);

			Assert.AreEqual(Severity.Error, node.Severity);
		}
	}
}

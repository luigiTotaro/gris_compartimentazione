﻿
using System;
using System.Collections.Generic;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.Severities;
using GrisSuite.FormulaEngine.Library;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	[TestFixture]
	public class NodeSystemTests
	{
		[Test]
		public void QuandoNodeSystemHa_PZPiuImportanteInAttenz_E_PZMenoImportanteInError_MiAspettoNodeSystemInAttenz ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev1 = new Device();
			var dev2 = new Device();
			var system = new NodeSystem();
			var node = new Node();
			var formula = new ObjectFormula();

			Utility.SetupEntities(dev1, dev2, system, node);

			dev1.Name = "PZ_guz";
			dev2.Name = "PZ_ovetz";
			formula.Index = (int) EvaluationFormulaType.Custom;
			formula.ScriptPath = @".\Lib\ObjectScripts\TestSystem_PZWithDifferentImportance.py";

			// severities
			dev1.SeverityDetail = Warning.SeverityDetail;
			dev2.SeverityDetail = Error.SeverityDetail;
			node.Severity = Severity.NotClassified;

			// relations
			system.Devices.Add(dev1);
			system.Devices.Add(dev2);
			system.Parent = node;

			system.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(system) });

			foreach ( var customEvaluationFormula in system.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(system.DefaultEvaluationFormula.HasErrorInEvaluation, system.DefaultEvaluationFormula.EvaluationError);

			Assert.AreEqual(system.Severity, Severity.Warning);
			Assert.AreEqual(node.Severity, Severity.Warning);
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.Severities;
using GrisSuite.FormulaEngine.Library;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	[TestFixture]
	public class VirtualObjectTests
	{
		#region Cluster di PZ

		[Test]
		public void QuandoVirtualObjectèClusterdi_PerifInAttenz_E_PerifInError_MiAspettoVirtualObjectInAttenzESistemaInAttenzEStazioneInAttenz ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev1 = new Device();
			var dev2 = new Device();
			var cluster = new VirtualObject();
			var system = new NodeSystem();
			var node = new Node();
			var formula = new ObjectFormula();

			Utility.SetupEntities(dev1, dev2, cluster, system, node);

			formula.Index = (int) EvaluationFormulaType.Custom;
			formula.ScriptPath = @".\Lib\ObjectScripts\TestVirtualObject_Cluster.py";

			// severities
			dev1.SeverityDetail = Warning.SeverityDetail;
			dev2.SeverityDetail = Error.SeverityDetail;

			// relations
			cluster.Children.Add(dev1);
			cluster.Children.Add(dev2);
			cluster.Parent = system;
			system.Parent = node;

			cluster.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(cluster) });

			foreach ( var customEvaluationFormula in cluster.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(cluster.DefaultEvaluationFormula.HasErrorInEvaluation);

			Assert.AreEqual(Severity.Warning, cluster.Severity);
			Assert.AreEqual(Severity.Warning, system.Severity);
			Assert.AreEqual(Severity.Warning, node.Severity);
		}

		#endregion

		#region Gruppo critico

		[Test]
		public void QuandoVirtualObjectèRaggruppamentoDi_AlmenoUnaPerifInErroreOSuperioreDiCuiLaPeggioreInOffline_MiAspettoVirtualObjectInOfflineEStazioneInOffline ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev1 = new Device();
			var dev2 = new Device();
			var dev3 = new Device();
			var system = new NodeSystem();
			var gruppoCritico = new VirtualObject();
			var node = new Node();
			var formula = new ObjectFormula();

			Utility.SetupEntities(dev1, dev2, dev3, gruppoCritico, system, node);

			formula.Index = (int) EvaluationFormulaType.Custom;
			formula.ScriptPath = @".\Lib\ObjectScripts\TestVirtualObject_CriticalGroup.py";

			// severities
			dev1.SeverityDetail = Ok.SeverityDetail;
			dev2.SeverityDetail = Ok.SeverityDetail;
			dev3.SeverityDetail = Offline.SeverityDetail;
			system.SeverityDetail = Error.SeverityDetail;

			// relations
			gruppoCritico.Children.Add(dev1);
			gruppoCritico.Children.Add(dev2);
			gruppoCritico.Children.Add(dev3);
			gruppoCritico.Children.Add(system);
			gruppoCritico.Parent = node;

			gruppoCritico.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(gruppoCritico) });

			foreach ( var customEvaluationFormula in gruppoCritico.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(gruppoCritico.DefaultEvaluationFormula.HasErrorInEvaluation);

			Assert.AreEqual(Offline.SeverityDetail, gruppoCritico.SeverityDetail);
			Assert.AreEqual(Severity.Offline, node.Severity);
		}

		#endregion
	}
}

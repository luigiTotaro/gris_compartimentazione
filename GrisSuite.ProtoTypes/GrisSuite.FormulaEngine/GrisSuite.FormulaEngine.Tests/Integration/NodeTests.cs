﻿
using System.Collections.Generic;
using System.Drawing;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.Severities;
using GrisSuite.FormulaEngine.Library;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	[TestFixture]
	public class NodeTests
	{
		[Test]
		public void QuandoStazioneHa_SistemaAudioInErrore_E_SistemaVideoInErrore_MiAspettoSistemaAudioDiColoreLimeESistemaVideoDiColoreViola ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var devVideo1 = new Device();
			var devVideo2 = new Device();
			var devPZ = new Device();
			var virtObj = new VirtualObject();
			var audioSystem = new NodeSystem();
			var videoSystem = new NodeSystem();
			var node = new Node();
			var formulaAudioSystem = new ObjectFormula();
			var formulaVirtObj = new ObjectFormula();

			Color lime = ColorTranslator.FromHtml("#BFFF00");
			Color violet = ColorTranslator.FromHtml("#7F00FF");

			Utility.SetupEntities(devVideo1, devVideo2, devPZ, virtObj, audioSystem, videoSystem, node);

			// entity data
			devPZ.Name = "PZ1";

			// formulas
			formulaAudioSystem.Index = (int) EvaluationFormulaType.Custom;
			formulaAudioSystem.ScriptPath = @".\Lib\ObjectScripts\TestSystem_AudioLime.py";

			formulaVirtObj.Index = (int) EvaluationFormulaType.Custom;
			formulaVirtObj.ScriptPath = @".\Lib\ObjectScripts\TestVirtualObject_VideoViolet.py";

			audioSystem.ObjectFormulas.Add(formulaAudioSystem);
			virtObj.ObjectFormulas.Add(formulaVirtObj);

			// severities
			devVideo1.SeverityDetail = Ok.SeverityDetail;
			devVideo2.SeverityDetail = Offline.SeverityDetail;
			devPZ.SeverityDetail = Error.SeverityDetail;

			// relations
			virtObj.Children.Add(devVideo1);
			virtObj.Children.Add(devVideo2);
			virtObj.Parent = videoSystem;
			videoSystem.Node = node;
			devPZ.System = audioSystem;
			audioSystem.Node = node;

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(audioSystem) });
			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(virtObj) });

			foreach ( var customEvaluationFormula in audioSystem.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(audioSystem.DefaultEvaluationFormula.HasErrorInEvaluation, audioSystem.DefaultEvaluationFormula.EvaluationError);

			foreach ( var customEvaluationFormula in virtObj.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(virtObj.DefaultEvaluationFormula.HasErrorInEvaluation, virtObj.DefaultEvaluationFormula.EvaluationError);

			Assert.AreEqual(lime, audioSystem.Color);
			Assert.AreEqual(violet, videoSystem.Color);
		}
	}
}

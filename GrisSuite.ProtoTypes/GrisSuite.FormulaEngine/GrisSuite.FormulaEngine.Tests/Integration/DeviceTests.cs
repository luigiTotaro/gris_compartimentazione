﻿using System;
using System.Collections.Generic;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.Severities;
using GrisSuite.FormulaEngine.Library;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	[TestFixture]
	public class DeviceTests
	{
		#region Device STLC1000 non fisica (senza IP) - esclusa dal calcolo

		[Test]
		public void QuandoDevice_DiTipoSTLC1000_IPNonInizializzato_MiAspettoEsclusaDalCalcoloDelParent ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(dev);

			// entity data
			dev.Id = Guid.NewGuid();
			dev.Type = "STLC1000";
			dev.Address = "0";

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);

			Assert.IsTrue(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device STLC1000 non fisica (senza porta) - esclusa dal calcolo

		[Test]
		public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_MiAspettoEsclusaDalCalcoloDelParent ()
		{
			//if obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None):
			//obj.ExcludedFromParentStatus = True
			//obj.SeverityDetail = Ok.SeverityDetail

			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(dev);

			// entity data
			dev.Id = Guid.NewGuid();
			dev.Type = "STLC1000";
			dev.Port = null;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);

			Assert.IsTrue(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device in manutenzione - diagnostica disattivata dall'operatore

		[Test]
		public void QuandoDevice_InMaintenance_MiAspettoDiagnosticaDisattivataDallOperatore ()
		{
			//if obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None):
			//obj.ExcludedFromParentStatus = True
			//obj.SeverityDetail = Ok.SeverityDetail

			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(dev);

			// entity data
			dev.Id = new Guid("A4A53253-4628-DF11-93A6-001E0B6CA3E0");
			dev.Type = "TT10210V3";
			dev.Port = new Port();
			dev.InMaintenance = true;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.NotActive, dev.Severity);
			Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device non attivo - disattivazione sull'STLC1000 non mascherabile

		[Test]
		public void QuandoDevice_NonAttivo_MiAspettoDiagnosticaDisattivataDallOperatore ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(dev);

			// entity data
			dev.Id = Guid.NewGuid();
			dev.Type = "TT10210V3";
			dev.Port = new Port();
			dev.IsActive = 0;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.NotActive, dev.Severity);
			Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device con server di monitoraggio in configurazione

		[Test]
		public void QuandoDevice_ConServerDiMonitoraggioInConfigurazione_MiAspettoCollettoreSTLC1000NonAttivo ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();

			dev.Id = Guid.NewGuid();
			server.Severity = Severity.NotActive;
			server.Devices.Add(dev);

			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.NotActive, dev.Severity);
			Assert.AreEqual(ServerNotActive.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device con server di monitoraggio non raggiungibile (offline)

		[Test]
		public void QuandoDevice_ConServerDiMonitoraggioNonRaggiungibile_MiAspettoCollettoreSTLC1000NonRaggiungibile ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();

			dev.Id = Guid.NewGuid();
			server.Severity = Severity.Offline;
			server.Devices.Add(dev);

			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(ServerOffline.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device sconosciuto [Offline uniformato a Sconosciuto, by design]

		[Test]
		public void QuandoDevice_Offline_E_DescrizioneDiversaDaDiagnosticaNonRaggiungibile_MiAspettoSconosciuto ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 1;
			dev.Description = "In funzione";

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Diagnostica non raggiungibile

		[Test]
		public void QuandoDevice_Offline_E_DescrizioneUgualeADiagnosticaNonRaggiungibile_MiAspettoDiagnosticaNonRaggiungibile ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 1;
			dev.Description = "diaGNOstica non raggiungibile"; // Verifichiamo controllo stringa case insensitive

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(DiagnosticOffline.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Device in servizio

		[Test]
		public void QuandoDevice_Ok_MiAspettoDiagnosticaOk ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "In servizio";
			dev.ActualSevLevel = Severity.Ok;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Ok, dev.Severity);
			Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Dati SNMP incongruenti

		[Test]
		public void QuandoDevice_AnomaliaLieve_E_DatiSNMPIncongruenti_MiAspettoDatiSNMPIncongruenti ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Dati SNMP incongruenti";
			dev.ActualSevLevel = Severity.Warning;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Diagnostica non disponibile

		[Test]
		public void QuandoDevice_AnomaliaLieve_E_DiagnosticaNonDisponibile_MiAspettoDiagnosticaNonDisponibile ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Diagnostica non disponibile";
			dev.ActualSevLevel = Severity.Warning;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(DiagnosticNotAvailableWarning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Dati diagnostici limitati alla sola sezione MIB-II

		[Test]
		public void QuandoDevice_AnomaliaLieve_E_DatiDiagnosticiLimitatiAllaSolaSezioneMIBII_MiAspettoDatiDiagnosticiLimitatiAllaSolaSezioneMIBII ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Dati diagnostici limitati alla sola sezione MIB-II";
			dev.ActualSevLevel = Severity.Warning;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Dati di monitoraggio incompleti

		[Test]
		public void QuandoDevice_AnomaliaLieve_E_DatiDiMonitoraggioIncompleti_MiAspettoDatiDiMonitoraggioIncompleti ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Dati di monitoraggio incompleti";
			dev.ActualSevLevel = Severity.Warning;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(IncompleteDiagnosticalData.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Anomalia lieve

		[Test]
		public void QuandoDevice_AnomaliaLieve_MiAspettoAnomaliaLieve ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Anomalia lieve";
			dev.ActualSevLevel = Severity.Warning;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(Warning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Anomalia lieve e messaggio non previsto

		[Test]
		public void QuandoDevice_AnomaliaLieve_E_MessaggioImprevisto_MiAspettoAnomaliaLieve ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Attenzione";
			dev.ActualSevLevel = Severity.Warning;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(Warning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Anomalia grave

		[Test]
		public void QuandoDevice_AnomaliaGrave_MiAspettoAnomaliaGrave ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Anomalia grave";
			dev.ActualSevLevel = Severity.Error;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Error, dev.Severity);
			Assert.AreEqual(Error.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Anomalia grave e messaggio non previsto

		[Test]
		public void QuandoDevice_AnomaliaGrave_E_MessaggioImprevisto_MiAspettoAnomaliaGrave ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Errore";
			dev.ActualSevLevel = Severity.Error;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Error, dev.Severity);
			Assert.AreEqual(Error.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Dati diagnostici limitati alla sola sezione MIB-II (periferica non attiva)

		[Test]
		public void
			QuandoDevice_InSeveritaNonAttiva_E_DatiDiagnosticiLimitatiAllaSolaSezioneMIBII_MiAspettoDatiDiagnosticiLimitatiAllaSolaSezioneMIBII ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Dati diagnostici limitati alla sola sezione MIB-II";
			dev.ActualSevLevel = Severity.NotActive;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Dati SNMP incongruenti (periferica non attiva)

		[Test]
		public void QuandoDevice_InSeveritaNonAttiva_E_DatiSNMPIncongruenti_MiAspettoDatiSNMPIncongruenti ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Dati SNMP incongruenti";
			dev.ActualSevLevel = Severity.NotActive;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Dati diagnostici limitati alla sola sezione MIB-II (InfoStazioni) (periferica non attiva)

		[Test]
		public void QuandoDevice_InSeveritaNonAttiva_E_InfoStazioni_MiAspettoDatiDiagnosticiLimitatiAllaSolaSezioneMIBII ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "InfoStazioni";
			dev.ActualSevLevel = Severity.NotActive;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Warning, dev.Severity);
			Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Diagnostica non disponibile

		[Test]
		public void QuandoDevice_InSeveritaNonAttiva_MiAspettoDiagnosticaNonDisponibile ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Non attiva";
			dev.ActualSevLevel = Severity.NotActive;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.NotActive, dev.Severity);
			Assert.AreEqual(DiagnosticNotAvailable.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Periferica in stato sconosciuto, che contiene solo stream e stream field in stato Informativo

		[Test]
		public void QuandoDevice_InSeveritaSconosciuta_E_DescrizioneNonApplicabile_SoloConStreamInformativi_MiAspettoNonApplicabile ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Non applicabile";
			dev.ActualSevLevel = Severity.Unknown;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(NA.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Sconosciuto

		[Test]
		public void QuandoDevice_InSeveritaSconosciuta_MiAspettoSconosciuto ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "";
			dev.ActualSevLevel = Severity.Unknown;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Sconosciuto con descrizione non prevista

		[Test]
		public void QuandoDevice_InSeveritaSconosciuta_E_DescrizioneImprevista_MiAspettoSconosciuto ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = "Sconosciuto";
			dev.ActualSevLevel = Severity.Unknown;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Sconosciuto con descrizione nulla

		[Test]
		public void QuandoDevice_InSeveritaSconosciuta_E_DescrizioneNulla_MiAspettoSconosciuto ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.IsOffline = 0;
			dev.Description = null;
			dev.ActualSevLevel = Severity.Unknown;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.Unknown, dev.Severity);
			Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Non classificato

		[Test]
		public void QuandoDevice_NonClassificato_MiAspettoNonClassificato ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.ActualSevLevel = Severity.NotClassified;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.NotClassified, dev.Severity);
			Assert.AreEqual(NotAvailable.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Non classificato, device offline

		[Test]
		public void QuandoDevice_Offline_MiAspettoNonClassificato ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev = new Device();
			var formula = new ObjectFormula();

			// attributes
			Utility.InitializeAttributes(server, dev);

			// entity data
			server.Id = Guid.NewGuid();
			server.Severity = Severity.Ok;
			server.Devices.Add(dev);
			dev.Id = Guid.NewGuid();
			dev.ActualSevLevel = Severity.Offline;

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\DeviceDefaultStatus.py";
			formula.GrisObject = dev;
			formula.ObjectId = dev.Id;
			dev.ObjectFormulas.Add(formula);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(dev) });

			foreach ( var customEvaluationFormula in dev.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(dev.DefaultEvaluationFormula.HasErrorInEvaluation, dev.DefaultEvaluationFormula.EvaluationError);
			Assert.IsFalse(dev.ExcludedFromParentStatus);
			Assert.AreEqual(Severity.NotClassified, dev.Severity);
			Assert.AreEqual(NotAvailable.SeverityDetail, dev.SeverityDetail);
		}

		#endregion

		#region Connessione porta primaria 2 non connessa negli ultimi 5 minuti

		[Test]
		public void QuandoPeriferica_ContieneStreamFieldsConnessionePortaPrimaria2_ConsecutivamenteNonConnessaDaAlmeno5Minuti_MiAspettoTutteLePerifericheInCascataInOffline ()
		{
			#region initialization
			var formulaEng = new FakeFormulaEngine();

			var node = new Node();
			var devMain = new Device();
			var dev1ConnectedToPrimaryConn2 = new Device();
			var dev2ConnectedToPrimaryConn2 = new Device();

			var streamInformazioniPorteSwitch = new Stream();
			var streamHist0InformazioniPorteSwitch = new StreamHistory();
			var streamHist1InformazioniPorteSwitch = new StreamHistory();
			var streamHist2InformazioniPorteSwitch = new StreamHistory();
			var streamHist3InformazioniPorteSwitch = new StreamHistory();
			var streamHist4InformazioniPorteSwitch = new StreamHistory();
			var streamHist5InformazioniPorteSwitch = new StreamHistory();
			var streamHist6InformazioniPorteSwitch = new StreamHistory();
			var streamHist7InformazioniPorteSwitch = new StreamHistory();
			var streamHist8InformazioniPorteSwitch = new StreamHistory();
			var streamHist9InformazioniPorteSwitch = new StreamHistory();

			var streamFieldConnessionePortaPrimaria2 = new StreamField();
			var streamFieldHist0ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist1ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist2ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist3ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist4ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist5ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist6ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist7ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist8ConnessionePortaPrimaria2 = new StreamFieldHistory();
			var streamFieldHist9ConnessionePortaPrimaria2 = new StreamFieldHistory();

			Utility.SetupEntities(node, devMain, dev1ConnectedToPrimaryConn2, dev2ConnectedToPrimaryConn2);
			#endregion

			#region formulas
			var formula = new ObjectFormula();
			formula.Index = (int) EvaluationFormulaType.Custom;
			formula.ScriptPath = @".\Lib\ObjectScripts\TestDevice_Connessione2_NonConnessaPer5Min.py";
			devMain.ObjectFormulas.Add(formula);
			#endregion

			#region relations
			devMain.Node = node;
			dev1ConnectedToPrimaryConn2.Node = node;
			dev2ConnectedToPrimaryConn2.Node = node;
			devMain.Streams.Add(streamInformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist0InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist1InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist2InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist3InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist4InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist5InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist6InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist7InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist8InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamsHistory.Add(streamHist9InformazioniPorteSwitch);
			streamInformazioniPorteSwitch.StreamFields.Add(streamFieldConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist0ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist1ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist2ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist3ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist4ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist5ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist6ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist7ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist8ConnessionePortaPrimaria2);
			streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHist9ConnessionePortaPrimaria2);
			#endregion

			#region entities data

			devMain.Name = "Periferica principale";
			dev1ConnectedToPrimaryConn2.Name = "Dev 1 connessa a porta 2";
			dev2ConnectedToPrimaryConn2.Name = "Dev 2 connessa a porta 2";

			streamInformazioniPorteSwitch.Name = "Informazioni porte switch";

			streamFieldConnessionePortaPrimaria2.Name = "Connessione porta primaria 2";

			var initialDateTime = DateTime.Now;
			foreach ( var streamHistory in streamInformazioniPorteSwitch.StreamsHistory )
			{
				streamHistory.DateTime = initialDateTime.AddSeconds(30);
			}

			foreach ( var streamFieldHistory in streamFieldConnessionePortaPrimaria2.StreamFieldsHistory )
			{
				streamFieldHistory.Description = "Non connessa";
			}

			#endregion

			#region evaluation
			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(devMain) });
			#endregion

			foreach ( var customEvaluationFormula in devMain.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(devMain.DefaultEvaluationFormula.HasErrorInEvaluation);

			Assert.AreEqual(Offline.SeverityDetail, dev1ConnectedToPrimaryConn2.SeverityDetail);
			Assert.AreEqual(Offline.SeverityDetail, dev2ConnectedToPrimaryConn2.SeverityDetail);
		}

		#endregion
	}
}
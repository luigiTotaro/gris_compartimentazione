﻿
using System;
using System.Collections.Generic;
using GrisSuite.Data.Model;
using GrisSuite.Data.Model.Severities;
using GrisSuite.FormulaEngine.Library;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	[TestFixture]
	public class ServerTests
	{
		#region Storico, Dato archiviato

		[Test]
		public void QuandoServer_SenzaDeviceECancellato_MiAspettoServerStoricoDatoArchiviato ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server);

			// entity data
			server.Id = new Guid("FAF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 87162882;
			server.Name = "Server STLC1000";
			server.Host = "STLC0C8AF9";
			server.FullHostName = "STLC0C8AF9";
			server.IpAddress = "10.102.143.73";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "IsAlive";
			server.NodeId = new Guid("91EB6EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = "00E04B0C8AF9";
			server.IsDeleted = true;

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.NotClassified, server.Severity);
			Assert.AreEqual(Old.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Dati del collettore incompleti

		[Test]
		public void QuandoServer_SenzaDeviceENonCancellato_MiAspettoServerDatiCollettoreIncompleti ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server);

			// entity data
			server.Id = new Guid("FAF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 87162882;
			server.Name = "Server STLC1000";
			server.Host = "STLC0C8AF9";
			server.FullHostName = "STLC0C8AF9";
			server.IpAddress = "10.102.143.73";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "IsAlive";
			server.NodeId = new Guid("91EB6EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = "00E04B0C8AF9";
			server.IsDeleted = false;

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.Error, server.Severity);
			Assert.AreEqual(IncompleteServerData.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Non raggiungibile (Offline)

		[Test]
		public void QuandoServer_UltimoAggiornamentoOltreUnOraFa_ESenzaMAC_MiAspettoServerNonRaggiungibile ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev1 = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("B4F56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 120783119;
			server.Name = "Server STLC1000 Candia Canavese";
			server.Host = "TELEFIN-6E8130X";
			server.FullHostName = "TELEFIN-6E8130X";
			server.IpAddress = "10.102.200.119";
			server.LastUpdate = DateTime.Now.AddMinutes(-70);
			server.LastMessageType = null;
			server.NodeId = new Guid("85E86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = null;
			server.IsDeleted = false;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.Offline, server.Severity);
			Assert.AreEqual(Offline.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Un STLC per essere considerato valido deve avere un ip

		[Test]
		public void QuandoServer_SenzaIP_E_AlmenoUnDevice_MiAspettoServerSenzaIP ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var dev1 = new Device();
			var server = new Server();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("FAF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 87162882;
			server.Name = "Server STLC1000";
			server.Host = "STLC0C8AF9";
			server.FullHostName = "STLC0C8AF9";
			server.IpAddress = null;
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "IsAlive";
			server.NodeId = new Guid("91EB6EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = "00E04B0C8AF9";
			server.IsDeleted = false;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.NotActive, server.Severity);
			Assert.AreEqual(NoIPAddress.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Diagnostica disattivata dall'operatore (Server side)

		[Test]
		public void QuandoServer_InManutenzione_MiAspettoDiagnosticaDisattivataDallOperatore ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev1 = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("BEF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 103219278;
			server.Name = "Server STLC1000";
			server.Host = "STLC072833";
			server.FullHostName = "STLC072833";
			server.IpAddress = "10.102.151.21";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "UpdateStatus";
			server.NodeId = new Guid("DBE76EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = "00E04B072833";
			server.IsDeleted = false;
			server.InMaintenance = true;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.NotActive, server.Severity);
			Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Un STLC per essere considerato valido deve avere un MAC

		[Test]
		public void QuandoServer_SenzaMAC_MiAspettoServerSenzaMAC ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev1 = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("9FF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 117899289;
			server.Name = "Server STLC1000";
			server.Host = "STLC0283";
			server.FullHostName = "STLC0283.rfiservizi.corp";
			server.IpAddress = "10.102.180.6";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "UpdateStatus";
			server.NodeId = new Guid("DCE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = null;
			server.IsDeleted = false;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.NotActive, server.Severity);
			Assert.AreEqual(NoMACAddress.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Un STLC per essere considerato valido deve avere un FullHostName

		[Test]
		public void QuandoServer_SenzaFullHostName_MiAspettoServerSenzaFullHostName ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev1 = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("9FF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 117899289;
			server.Name = "Server STLC1000";
			server.Host = "STLC0283";
			server.FullHostName = null;
			server.IpAddress = "10.102.180.6";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "UpdateStatus";
			server.NodeId = new Guid("DCE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = null;
			server.IsDeleted = false;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.NotActive, server.Severity);
			Assert.AreEqual(NoHostName.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Ip duplicato

		[Test]
		public void QuandoServer_ConAltroServerUgualeIP_MiAspettoServerIPDuplicato ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev1 = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("9FF56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 117899289;
			server.Name = "Server STLC1000";
			server.Host = "STLC0283";
			server.FullHostName = "STLC0283";
			server.IpAddress = "10.102.180.6";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "UpdateStatus";
			server.NodeId = new Guid("DCE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = "00E04B0C8AF9";
			server.IsDeleted = false;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			// methods mocking
			Func<bool> mock = () => true;
			state.Add("HasDuplicateIpAddress", mock);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.NotActive, server.Severity);
			Assert.AreEqual(DuplicateIPAddress.SeverityDetail, server.SeverityDetail);
		}

		#endregion

		#region Server Ok

		[Test]
		public void QuandoServer_Normale_MiAspettoServerOk ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var server = new Server();
			var dev1 = new Device();
			var formula = new ObjectFormula();

			formula.Index = (int) EvaluationFormulaType.Default;
			formula.ScriptPath = @".\Lib\ServerDefaultStatus.py";

			// attributes
			Utility.InitializeAttributes(server, dev1);

			// entity data
			server.Id = new Guid("A0F56EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerId = 117899290;
			server.Name = "Server STLC1000";
			server.Host = "STLC1520B9";
			server.FullHostName = "STLC1520B9";
			server.IpAddress = "10.182.71.18";
			server.LastUpdate = DateTime.Now.AddMinutes(-20);
			server.LastMessageType = "ReplaceStatus";
			server.NodeId = new Guid("BAE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
			server.ServerVersion = null;
			server.MacAddress = "00E04B1520B9";
			server.IsDeleted = false;

			dev1.Id = Guid.NewGuid();
			dev1.ServerId = server.Id;
			server.Devices.Add(dev1);

			formula.GrisObject = server;
			formula.ObjectId = server.Id;
			server.ObjectFormulas.Add(formula);

			// state setting
			Dictionary<string, object> state = new Dictionary<string, object>();
			state.Add("serverMinuteTimeout", 60);

			// methods mocking
			Func<bool> mock = () => false;
			state.Add("HasDuplicateIpAddress", mock);

			formulaEng.Eval(new List<EvaluablePackage> { new EvaluablePackage(server, state) });

			foreach ( var customEvaluationFormula in server.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}

			Assert.IsFalse(server.DefaultEvaluationFormula.HasErrorInEvaluation, server.DefaultEvaluationFormula.EvaluationError);
			Assert.AreEqual(Severity.Ok, server.Severity);
			Assert.AreEqual(Ok.SeverityDetail, server.SeverityDetail);
		}

		#endregion
	}
}
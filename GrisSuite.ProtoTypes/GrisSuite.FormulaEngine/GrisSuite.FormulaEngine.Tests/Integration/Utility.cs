﻿
using System;
using GrisSuite.Data.Model;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	public static class Utility
	{
		public static void SetupEntities ( params GrisObject[] grisObjects )
		{
			foreach ( var grisObject in grisObjects )
			{
				grisObject.Id = Guid.NewGuid();
				InitializeAttributes(grisObject);
			}
		}

		public static void InitializeAttributes ( params GrisObject[] grisObjects )
		{
			foreach ( var grisObject in grisObjects )
			{
				grisObject.Attributes = new GrisAttributeCollection(grisObject, new FakeFormulaEngine().BuildAttributes());
				grisObject.Attributes.Initialize();
			}
		}
	}
}

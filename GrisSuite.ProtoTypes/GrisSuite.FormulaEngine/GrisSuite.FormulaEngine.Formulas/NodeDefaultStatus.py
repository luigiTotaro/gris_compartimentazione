# script di calcolo dello stato di default per gli oggetti Node
import clr
import sys

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage
from Infrastructure.MetaClasses import *

try:
    if isinstance(obj, Node):
        clr.Convert(obj, Node)

        obj = GrisObjectPxy(obj, state)
        
        if obj.GetMonitoringServersInDetailedStatus(IncompleteServerData.SeverityDetail).Count > 0:
            obj.Severity = Error.Severity
        else:
            if obj.AreAllNodeServersOffline():
                obj.Severity = Error.Severity
            else:
                worstStatus = obj.GetWorstSeverityForDevicesOfType(["TT10210", "TT10210V3", "TT10220"])
                if worstStatus in [1, 2, 255]:
                    if worstStatus > 2:
                        obj.Severity = Error.Severity
                    else:
                        obj.Severity = worstStatus
                else:
                    obj.Severity = GetWeightedAverage(
                        obj.CountSystemsInSeverity(Ok.Severity), 
                        obj.CountSystemsInSeverity(Warning.Severity), 
                        obj.CountSystemsInSeverity(Error.Severity) + obj.CountSystemsInSeverity(Offline.Severity) + obj.CountSystemsInSeverity(Unknown.Severity), 
                        obj.CountSystemsInSeverity(NotActive.Severity))
except Exception, exc:
    type, value = sys.exc_info()[:2]
    print type
    print value
    raise exc
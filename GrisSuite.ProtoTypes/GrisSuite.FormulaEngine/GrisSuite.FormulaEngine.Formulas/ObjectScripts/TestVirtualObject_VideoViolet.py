
import sys
import clr
import System

sys.path.append('..')
clr.AddReference('System.Drawing')

from System.Drawing import Color, ColorTranslator

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.IsAnyChildAtLeastInSeverity(Error.Severity):
        obj.SeverityDetail = obj.GetChildrenWorstSeverityDetail()
        system = GrisObjectPxy(obj.Parent, state)
        system.ForceSeverity(obj.Severity)
        system.Color = ColorTranslator.FromHtml('#7F00FF');
        node = system.Node
        node.ForceSeverity(obj.Severity)

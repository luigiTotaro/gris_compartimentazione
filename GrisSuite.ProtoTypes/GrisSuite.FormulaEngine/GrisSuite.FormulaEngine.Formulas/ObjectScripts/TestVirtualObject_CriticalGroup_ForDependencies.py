# test non realistico, solo allo scopo di testare il ricalcolo delle dipendenze
import sys
import clr

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.IsAnyChildAtLeastInSeverity(Offline.Severity):
        obj.SeverityDetail = Offline.SeverityDetail
        obj.Parent.ForceSeverity(obj.Severity) # server
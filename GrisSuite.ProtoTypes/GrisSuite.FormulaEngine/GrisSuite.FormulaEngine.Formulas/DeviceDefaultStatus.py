# script di calcolo dello stato di default per gli oggetti Device
import clr
import sys
#clr.AddReferenceToFile('GrisSuite.Data.Model.dll')
from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.MetaClasses import *

try:
    if isinstance(obj, Device):
        clr.Convert(obj, Device)

        obj = GrisObjectPxy(obj, state)
        
        ## Test stato periferiche STLC1000 virtuali (non fisiche, monitorate via SNMP, prive di IP o Porta)
        if obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None):
            obj.ExcludedFromParentStatus = True
            obj.SeverityDetail = Ok.SeverityDetail

        elif obj.InMaintenance:
            obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail
        
        elif obj.IsActive == 0:
            obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

        elif obj.Server.Severity == NotActive.Severity:
            obj.SeverityDetail = ServerNotActive.SeverityDetail

        elif obj.Server.Severity == Offline.Severity:
            obj.SeverityDetail = ServerOffline.SeverityDetail

        elif obj.IsOffline and not ("diagnostica non raggiungibile" in (obj.Description or "").lower()):
            obj.SeverityDetail = Unknown.SeverityDetail

        elif obj.IsOffline and "diagnostica non raggiungibile" in (obj.Description or "").lower():
            obj.SeverityDetail = DiagnosticOffline.SeverityDetail

        elif obj.ActualSevLevel == Ok.Severity:
            obj.SeverityDetail = Ok.SeverityDetail

        elif obj.ActualSevLevel == Warning.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
            obj.SeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

        elif obj.ActualSevLevel == Warning.Severity and "diagnostica non disponibile" in (obj.Description or "").lower():
            obj.SeverityDetail = DiagnosticNotAvailableWarning.SeverityDetail

        elif obj.ActualSevLevel == Warning.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
            obj.SeverityDetail = MIB2OnlyWarning.SeverityDetail

        elif obj.ActualSevLevel == Warning.Severity and "dati di monitoraggio incompleti" in (obj.Description or "").lower():
            obj.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail

        elif obj.ActualSevLevel == Warning.Severity:
            obj.SeverityDetail = Warning.SeverityDetail

        elif obj.ActualSevLevel == Error.Severity:
            obj.SeverityDetail = Error.SeverityDetail

        elif obj.ActualSevLevel == NotActive.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
            obj.SeverityDetail = MIB2OnlyWarning.SeverityDetail

        elif obj.ActualSevLevel == NotActive.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
            obj.SeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

        elif obj.ActualSevLevel == NotActive.Severity and "infostazioni" in (obj.Description or "").lower():
            obj.SeverityDetail = MIB2OnlyWarning.SeverityDetail

        elif obj.ActualSevLevel == NotActive.Severity:
            obj.SeverityDetail = DiagnosticNotAvailable.SeverityDetail

        elif obj.ActualSevLevel == Unknown.Severity and "non applicabile" in (obj.Description or "").lower():
            obj.SeverityDetail = NA.SeverityDetail

        elif obj.ActualSevLevel == Unknown.Severity:
            obj.SeverityDetail = Unknown.SeverityDetail

        else:
            obj.SeverityDetail = NotAvailable.SeverityDetail
except Exception, exc:
    type, value = sys.exc_info()[:2]
    print type
    print value
    raise exc
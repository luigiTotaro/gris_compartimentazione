# script di calcolo dello stato di default per gli oggetti Region
import clr
import sys

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage
from Infrastructure.MetaClasses import *

try:
    if isinstance(obj, Region): 
        clr.Convert(obj, Region)

        obj = GrisObjectPxy(obj, state)
        
        if obj.AreAllNodeServersOffline():
            obj.Severity = Unknown.Severity
        else:
            obj.Severity = GetWeightedAverage(
                obj.CountZonesInSeverity(Ok.Severity), 
                obj.CountZonesInSeverity(Warning.Severity), 
                obj.CountZonesInSeverity(Error.Severity) + obj.CountZonesInSeverity(Offline.Severity) + obj.CountZonesInSeverity(Unknown.Severity), 
                obj.CountZonesInSeverity(NotActive.Severity))
except Exception, exc:
    type, value = sys.exc_info()[:2]
    print type
    print value
    raise exc
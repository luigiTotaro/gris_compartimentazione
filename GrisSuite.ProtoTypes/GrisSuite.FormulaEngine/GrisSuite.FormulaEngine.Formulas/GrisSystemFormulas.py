# script che raccogle tutte le funzioni di calcolo di sistema di Gris utilizzabili da altri scripts
import sys

def GetWeightedAverage (numOk, numWarning, numError, numMaintenance):
    try:
        tot = numOk + numWarning + numError
        
        if tot > 0:
            avgValue = ((numOk + .0) / (tot + .0) * 0.0) + ((numWarning + .0) / (tot + .0) * 5.0) + ((numError + .0) / (tot + .0) * 10.0)
            if avgValue >= 0 and avgValue <= 2:
                return 0
            elif avgValue > 2 and avgValue <= 5:
                return 1
            elif avgValue > 5 and avgValue <= 10:
                return 2
        
        if numMaintenance > 0:
            return 9
            
        return -1
    except:
        type, value = sys.exc_info()[:2]
        print type
        print value
        print 1
        
def IsNotNullOrEmpty(value):
    return value is not None and len(value) > 0
        
def IsNullOrEmpty(value):
    return not IsNotNullOrEmpty(value)
# script di calcolo dello stato di default per gli oggetti Region
import clr
import sys

from GrisSuite.Data.Model import *
from GrisSuite.Data.Model.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage
from Infrastructure.MetaClasses import *

try:
    if isinstance(obj, Zone): 
        clr.Convert(obj, Zone)

        obj = GrisObjectPxy(obj, state)
        
        if obj.AreAllNodeServersOffline():
            obj.Severity = Unknown.Severity
        else:
            obj.Severity = GetWeightedAverage(
                obj.CountNodesInSeverity(Ok.Severity), 
                obj.CountNodesInSeverity(Warning.Severity), 
                obj.CountNodesInSeverity(Error.Severity) + obj.CountNodesInSeverity(Offline.Severity) + obj.CountNodesInSeverity(Unknown.Severity), 
                obj.CountNodesInSeverity(NotActive.Severity))
except Exception, exc:
    type, value = sys.exc_info()[:2]
    print type
    print value
    raise exc
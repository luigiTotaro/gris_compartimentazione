﻿
using System;
using System.Linq;

namespace GrisSuite.Data.Model
{
	public partial class StreamField
	{
		public bool IsInConditionForTimeSpan ( string condition, TimeSpan forPeriod )
		{
			var now = DateTime.Now;
			var streamFieldsHist = from streamHistory in this.Stream.StreamsHistory
								   where streamHistory.DateTime <= now && streamHistory.DateTime >= now - forPeriod
								   from streamField in streamHistory.Stream.StreamFields
								   from streamFieldHistory in streamField.StreamFieldsHistory
								   select streamFieldHistory;
			return streamFieldsHist.All(sfh => sfh.Description == condition);
		}
	}
}

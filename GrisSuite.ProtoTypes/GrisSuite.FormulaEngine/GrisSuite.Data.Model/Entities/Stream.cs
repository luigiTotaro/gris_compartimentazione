﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace GrisSuite.Data.Model
{
	public partial class Stream
	{
		#region Streams

		public StreamField GetStreamFieldByName ( string name )
		{
			return this.GetChildByName(name);
		}

		public IList<StreamField> GetStreamFieldsByName ( string name )
		{
			return this.StreamFields.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		// necessario per python
		public new StreamField GetChildByName ( string name )
		{
			return this.StreamFields.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		#endregion
	}
}

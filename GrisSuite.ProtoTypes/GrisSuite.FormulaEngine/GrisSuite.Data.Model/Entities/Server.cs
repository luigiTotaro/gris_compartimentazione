﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace GrisSuite.Data.Model
{
	public partial class Server
	{
		#region Servers

		public Server GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Server>().SingleOrDefault(s => s.Id == id);
		}

		public Server GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		public bool HasDuplicateIpAddress ()
		{
			if ( string.IsNullOrEmpty(this.IpAddress) ) return false;

			int sameIpCount = this.DBContext.GrisObjects.OfType<Server>().Count(srv => srv.IpAddress.Trim() == this.IpAddress.Trim());

			if ( sameIpCount == 0 ) throw new DataQueryException(string.Format("Nessun server trovato nella tabella servers con lo stesso indirizzo IP dell'oggetto corrente. Id: {0} e IP: {1}", this.Id, this.IpAddress));

			return ( sameIpCount > 1 );
		}

		#endregion

		#region Devices

		public IList<Device> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int GetWorstSeverityForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			var max = this.Devices.Where(d => deviceTypes.Contains(d.Type)).Max(d => d.ActualSevLevel);
			return max.HasValue ? max.Value : -1;
		}

		public Device GetDeviceByName ( string name )
		{
			return this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public IList<Device> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Navigation

		public override GrisObject GetChildByName ( string name )
		{
			return this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public override IList<GrisObject> GetChildrenByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).Cast<GrisObject>().ToList();
		}

		#endregion
	}
}

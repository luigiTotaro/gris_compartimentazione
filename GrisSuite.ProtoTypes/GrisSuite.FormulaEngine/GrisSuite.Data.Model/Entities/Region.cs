﻿
using System;
using System.Linq;
using System.Collections.Generic;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.Data.Model
{
	public partial class Region
	{
		#region Regions

		public Region GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Region>().SingleOrDefault(r => r.Id == id);
		}

		public Region GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion

		#region Zones

		public int CountZonesInSeverity ( int severity )
		{
			return this.Zones.Count(z => z.Severity == severity);
		}

		public Zone GetZoneByName ( string name )
		{
			return this.Zones.Where(z => z.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public IList<Zone> GetZonesByName ( string name )
		{
			return this.Zones.Where(z => z.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}


		#endregion

		#region Nodes
		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<Server> GetMonitoringServers ()
		{
			return ( from zone in this.Zones
					 from node in zone.Nodes
					 from device in node.Devices
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Severities.Severity.Offline);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion

		#region Navigation

		public override GrisObject GetChildByName ( string name )
		{
			return this.Zones.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public override IList<GrisObject> GetChildrenByName ( string name )
		{
			return this.Zones.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).Cast<GrisObject>().ToList();
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				if ( this.Zones.IsLoaded )
				{
					var stors = new List<IStorable> { this.Attributes };
					stors.AddRange(this.Zones);
					//if ( this.VirtualObjects. ) stors.AddRange(this.VirtualObjects);
					return stors;
				}

				return base.StorableNodes;
			}
		}

		#endregion
	}
}

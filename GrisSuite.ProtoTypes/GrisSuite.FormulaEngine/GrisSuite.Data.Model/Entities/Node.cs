﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Model.DynamicEvaluation;
using GrisSuite.Data.Model.Severities;

namespace GrisSuite.Data.Model
{
	public partial class Node
	{
		#region Nodes

		public Node GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Node>().SingleOrDefault(n => n.Id == id);
		}

		public Node GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion

		#region NodeSystems

		public int CountSystemsInSeverity ( int severity )
		{
			return this.Systems.Count(s => s.Severity == severity);
		}

		public NodeSystem GetSystemByName ( string name )
		{
			return this.Systems.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public IList<NodeSystem> GetSystemsByName ( string name )
		{
			return this.Systems.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Devices

		public IList<Device> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int GetWorstSeverityForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			var max = this.Devices.Where(d => deviceTypes.Contains(d.Type)).Max(d => d.ActualSevLevel);
			return max.HasValue ? max.Value : -1;
		}

		public Device GetDeviceByName ( string name )
		{
			return this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public IList<Device> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Servers

		public IList<Server> GetMonitoringServers ()
		{
			return (from device in this.Devices select device.Server).Distinct().ToList();
		}

		public IList<Server> GetMonitoringServersInSeverity ( int severity )
		{
			return ( from device in this.Devices where device.Server.SevLevel == severity select device.Server ).Distinct().ToList();
		}

		public IList<Server> GetMonitoringServersInSeverity ( ObjectSeverityDetail detailedSeverity )
		{
			return ( from device in this.Devices 
						where device.Server.SevLevelDetail.HasValue &&
						device.Server.SevLevelDetail.Value == detailedSeverity 
						select device.Server ).Distinct().ToList();
		}
		
		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
			           where server.SevLevel != 9 && server.SevLevel != -1
			           select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Severities.Severity.Offline);
			return (numOffline > 0 && numOffline == numTot) ? true : false;
		}

		#endregion

		#region Navigation

		public override GrisObject GetChildByName ( string name )
		{
			GrisObject grisObj = this.Systems.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault() ??
			                     (GrisObject) this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

			return grisObj;
		}

		public override IList<GrisObject> GetChildrenByName ( string name )
		{
			IList<GrisObject> grisObjs = this.Systems.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).Cast<GrisObject>().ToList();
			if ( grisObjs.Count == 0 ) grisObjs = this.Devices.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).Cast<GrisObject>().ToList();

			return grisObjs;
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				var stors = new List<IStorable> { this.Attributes };
				stors.AddRange(this.Servers);
				stors.AddRange(this.Systems.IsLoaded ? this.Systems.Cast<GrisObject>() : this.Children);
				//stors.AddRange(this.VirtualObjects);
				return stors;
			}
		}

		#endregion
	}
}

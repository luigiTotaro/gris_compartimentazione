﻿
using System;
using System.Linq;
using System.Collections.Generic;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.Data.Model
{
	public partial class Zone
	{
		#region Zones

		public Zone GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Zone>().SingleOrDefault(z => z.Id == id);
		}

		public Zone GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion

		#region Nodes

		public int CountNodesInSeverity ( int severity )
		{
			return this.Nodes.Count(s => s.Severity == severity);
		}

		public Node GetNodeByName ( string name )
		{
			return this.Nodes.Where(n => n.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public IList<Node> GetNodesByName ( string name )
		{
			return this.Nodes.Where(n => n.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<Server> GetMonitoringServers ()
		{
			return ( from node in this.Nodes
					 from device in node.Devices
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Severities.Severity.Offline);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion

		#region Navigation

		public override GrisObject GetChildByName ( string name )
		{
			return this.Nodes.Where(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		public override IList<GrisObject> GetChildrenByName ( string name )
		{
			return this.Nodes.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).Cast<GrisObject>().ToList();
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				if ( this.Nodes.IsLoaded )
				{
					var stors = new List<IStorable> { this.Attributes };
					stors.AddRange(this.Nodes);
					//stors.AddRange(this.VirtualObjects);
					return stors;
				}

				return base.StorableNodes;
			}
		}

		#endregion
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.Data.Model
{
	public partial class Device
	{
		#region Devices

		public Device GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Device>().SingleOrDefault(d => d.Id == id);
		}

		public Device GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion

		#region Streams

		public Stream GetStreamByName ( string name )
		{
			return this.GetChildByName(name);
		}

		public IList<Stream> GetStreamsByName ( string name )
		{
			return this.Streams.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		// necessario per python
		public new Stream GetChildByName ( string name )
		{
			return this.Streams.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ).SingleOrDefault();
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				return new List<IStorable>(0);
			}
		}

		#endregion
	}
}

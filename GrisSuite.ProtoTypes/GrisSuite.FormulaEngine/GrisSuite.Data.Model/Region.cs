﻿
using System;
using System.Linq;
using System.Collections.Generic;
using GrisSuite.Data.Model.Severities;

namespace GrisSuite.Data.Model
{
	public partial class Region
	{
		#region Regions

		public Region GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Region>().SingleOrDefault(r => r.Id == id);
		}

		public Region GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion

		#region Zones

		public int CountZonesInStatus ( int severity )
		{
			return this.Zones.Count(z => z.Status == severity);
		}

		public Zone GetZoneByName ( string name )
		{
			return this.Zones.Where(z => z.Name == name).SingleOrDefault();
		}

		public IList<Zone> GetZonesByName ( string name )
		{
			return this.Zones.Where(z => z.Name.IndexOf(name) > -1).ToList();
		}


		#endregion

		#region Nodes
		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<Server> GetMonitoringServers ()
		{
			return ( from zone in this.Zones
					 from node in zone.Nodes
					 from device in node.Devices
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Severity.Offline);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion
	}
}

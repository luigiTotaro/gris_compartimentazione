﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.Data.Model
{
    [Serializable]
	public class DataQueryException : Exception
	{
		public DataQueryException ()
        {
        }

		public DataQueryException ( string message )
			: base(message)
        {
        }

		public DataQueryException ( string message, Exception inner )
			: base(message, inner)
        {
        }

		protected DataQueryException ( SerializationInfo info, StreamingContext context )
			: base(info, context)
        {
        }
	}
}

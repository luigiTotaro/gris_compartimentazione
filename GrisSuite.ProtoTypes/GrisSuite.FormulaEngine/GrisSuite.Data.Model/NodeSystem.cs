﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.Data.Model.Severities;

namespace GrisSuite.Data.Model
{
	public partial class NodeSystem
	{
		#region NodeSystems

		public NodeSystem GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<NodeSystem>().SingleOrDefault(n => n.Id == id);
		}

		public NodeSystem GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion

		#region Devices

		public int CountDevicesInStatus ( int severity )
		{
			return this.Devices.Count(d => d.ActualSevLevel == severity);
		}

		public IList<Device> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int GetWorstStatusForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			var max = this.Devices.Where(d => deviceTypes.Contains(d.Type)).Max(d => d.ActualSevLevel);
			return max.HasValue ? max.Value : -1;
		}

		public Device GetDeviceByName ( string name )
		{
			return this.Devices.Where(d => d.Name == name).SingleOrDefault();
		}

		public IList<Device> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name) > -1).ToList();
		}

		#endregion

		#region Servers

		public IList<Server> GetMonitoringServers ()
		{
			return ( from device in this.Devices select device.Server ).Distinct().ToList();
		}

		public IList<Server> GetMonitoringServersInStatus ( int status )
		{
			return ( from device in this.Devices where device.Server.SevLevel == status select device.Server ).Distinct().ToList();
		}

		public IList<Server> GetMonitoringServersInDetailedStatus ( ObjectStatus detailedStatus )
		{
			return ( from device in this.Devices
					 where device.Server.SevLevelDetail.HasValue &&
					 device.Server.SevLevelDetail.Value == detailedStatus
					 select device.Server ).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = from server in this.GetMonitoringServers()
					   where server.SevLevel != 9 && server.SevLevel != -1
					   select server;

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Severity.Offline);
			return ( numOffline > 0 && numOffline == numTot ) ? true : false;
		}

		#endregion
	}
}

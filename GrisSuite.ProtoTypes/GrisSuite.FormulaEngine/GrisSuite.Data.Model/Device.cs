﻿
using System;
using System.Linq;

namespace GrisSuite.Data.Model
{
	public partial class Device
	{
		#region Devices

		public Device GetById ( Guid id )
		{
			return this.DBContext.GrisObjects.OfType<Device>().SingleOrDefault(d => d.Id == id);
		}

		public Device GetById ( string guid )
		{
			Guid id;
			if ( Guid.TryParse(guid, out id) )
			{
				return this.GetById(id);
			}

			return null;
		}

		#endregion
	}
}

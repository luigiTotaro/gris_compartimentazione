﻿
namespace GrisSuite.Data.Model
{
	public interface IEvaluableObject
	{
		void Evaluate ( IEvaluationContext context );
	}

	public interface IEvaluationContext
	{
		void Evaluate ( EvaluationFormula formula );
	}
}
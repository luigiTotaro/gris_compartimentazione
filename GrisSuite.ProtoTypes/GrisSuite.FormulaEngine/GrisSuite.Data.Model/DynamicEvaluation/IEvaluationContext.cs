﻿
using System.Collections.Generic;

namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface IEvaluationContext
	{
		void Evaluate ( IEvaluableObject evalObject );
		void Evaluate ( IEvaluableObject evalObject, Dictionary<string, object> state );
	}
}
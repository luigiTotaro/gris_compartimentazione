﻿
namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface IEvaluableAttribute : IEvaluableObject
	{
		int Id { get; }
		string Name { get; }
		dynamic Value { get; set; }
		IEvaluationFormula Formula { get; }
		IEvaluableAttributesContainer Container { get; }
	}

	public interface IEvaluationFormula
	{
		string ScriptPath { get; }
		string Text { get; }
		string EvaluationError { get; set; }
		bool HasErrorInEvaluation { get; set; }
	}
}

﻿
using System.Collections.Generic;

namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface IEvaluableAttributesContainer : IEvaluableObject
	{
		Dictionary<string, IEvaluableAttribute> Attributes { get; set; }
		IEvaluationContext Context { get; set; }
	}
}

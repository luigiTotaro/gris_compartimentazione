﻿
using System;
using System.Collections.Generic;

namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface IEvaluableObject
	{
		Guid Id { get; }
		EvaluationFormula DefaultEvaluationFormula { get; }
		SortedSet<EvaluationFormula> CustomEvaluationFormulas { get; }
		IEvaluationContext EvaluationContext { get; set; }
		void Evaluate ();
		void Evaluate ( bool force );
		void ForceEvaluationOnDependentObjects();
		bool IsEvaluated { get; }
		bool IsSeverityEstabilished { get; }
		IEvaluableObject CallerObject { get; set; }
	}
}
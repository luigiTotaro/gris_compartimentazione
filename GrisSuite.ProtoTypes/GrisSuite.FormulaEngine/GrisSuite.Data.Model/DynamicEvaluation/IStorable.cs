﻿
using System.Collections.Generic;

namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface IStorable
	{
		IList<IStorable> StorableNodes { get; }
		string GetUpdateCommand ();
	}
}

﻿
namespace GrisSuite.Data.Model.DynamicEvaluation
{
	public interface IEvaluationFormula
	{
		string ScriptPath { get; }
		bool IsDefault { get; }
		bool CanEvaluate { get; }
		bool HasErrorInEvaluation { get; }
		string EvaluationError { get; set; }
		IEvaluableObject Owner { get; }
	}
}

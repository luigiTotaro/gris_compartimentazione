﻿
using System;
using System.Collections.Generic;
using System.Transactions;
using GrisSuite.Data.Model.DynamicEvaluation;

namespace GrisSuite.Data.Model
{
	public partial class Entities
	{
		public void SaveBranch ( IStorable root )
		{
			List<string> updateCommands = new List<string>(30000);
	
			this.FetchAndGetUpdate(root, updateCommands);

			if ( updateCommands.Count > 0 )
			{
				using ( TransactionScope transaction = new TransactionScope() )
				{
					this.ExecuteStoreCommand(string.Join(Environment.NewLine, updateCommands));	
					transaction.Complete();
				}
			}
		}

		private void FetchAndGetUpdate ( IStorable node, IList<string> updateCommands )
		{
			if ( node != null )
			{
				updateCommands.Add(node.GetUpdateCommand());

				var storableNodes = node.StorableNodes;
				if ( storableNodes != null && storableNodes.Count > 0 )
				{
					foreach ( var subNode in storableNodes )
					{
						this.FetchAndGetUpdate(subNode, updateCommands);
					}
				}
			}
		}
	}
}

<?xml version="1.0" encoding="utf-8"?><ShownData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://tsf.it/xsd/OperationLog.xsd"><Formato>HTML</Formato><Dati><html xmlns:b="http://tsf.it/xsd" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:myfunct="urn:myfunct-scripts">
<head>
<META http-equiv="Content-Type" content="text/html; charset=UTF-16">
<script type="text/javascript">
	function TrovaMese(valMese)
	{
		var temp;
		switch (valMese)
		{
			case 0:		temp="GEN";	
				break;
			case 1:		temp="FEB";	
				break;
			case 2:		temp="MAR";	
				break;
			case 3:		temp="APR";	
				break;
			case 4:		temp="MAG";	
				break;
			case 5:		temp="GIU";	
				break;
			case 6:		temp="LUG";	
				break;
			case 7:		temp="AGO";	
				break;
			case 8:		temp="SET";	
				break;
			case 9:		temp="OTT";	
				break;
			case 10:	temp="NOV";	
				break;
			case 11:	temp="DIC";	
				break;
		}
		return temp;
	}

	function TrovaGSett(valGiorno)
	{
		var temp;
		switch (valGiorno)
		{
			case 0:		temp="DOM";	
				break;
			case 1:		temp="LUN";	
				break;
			case 2:		temp="MAR";	
				break;
			case 3:		temp="MER";	
				break;
			case 4:		temp="GIO";	
				break;
			case 5:		temp="VEN";	
				break;
			case 6:		temp="SAB";	
				break;
		}
		return temp;
	}
	
	function OraLegaleSolare(dataNow)
	{
		var ret = 0;
		var ultimaMarzo = UltimaDomenica(dataNow.getFullYear(), 2, 1);
		var primaAprile = PrimaDomenica(dataNow.getFullYear(), 3);
		var ultimaOttobre1 = UltimaDomenica(dataNow.getFullYear(), 9, 1);
		var ultimaOttobre2 = UltimaDomenica(dataNow.getFullYear(), 9, 2);
		if (dataNow >= ultimaMarzo) 
		{
			if (dataNow < primaAprile)
				
				ret = 1
		}
		if (dataNow >= ultimaOttobre1)
		{
			if (dataNow <= ultimaOttobre2)
				
				ret = 1
		}
		return ret;
	}
	
	function UltimaDomenica(anno, mese, ora)
	{
		var data = new Date (anno,mese,25,ora,59,59);
		var ret;
		switch (data.getDay())
		{
			case 0:		ret = data;
				break;
			case 1:		ret = new Date (anno,mese,31,ora,59,59);
				break;
			case 2:		ret = new Date (anno,mese,30,ora,59,59);
				break;
			case 3:		ret = new Date (anno,mese,29,ora,59,59);
				break;
			case 4:		ret = new Date (anno,mese,28,ora,59,59);
				break;
			case 5:		ret = new Date (anno,mese,27,ora,59,59);
				break;
			case 6:		ret = new Date (anno,mese,26,ora,59,59);
				break;
		}
		return ret;
	}

	function PrimaDomenica(anno, mese)
	{
		var data = new Date (anno,mese,1,2,59,59);
		var ret;
		switch (data.getDay())
		{
			case 0:		ret = data;
				break;
			case 1:		ret = new Date (anno,mese,7,2,59,59);
				break;
			case 2:		ret = new Date (anno,mese,6,2,59,59);
				break;
			case 3:		ret = new Date (anno,mese,5,2,59,59);
				break;
			case 4:		ret = new Date (anno,mese,4,2,59,59);
				break;
			case 5:		ret = new Date (anno,mese,3,2,59,59);
				break;
			case 6:		ret = new Date (anno,mese,2,2,59,59);
				break;
		}
		return ret;
	}

	function CalcolaData()
	{
		var ore = new Date;
		var Hh, Mm;
		var temp;
		Hh = ore.getHours() + OraLegaleSolare(ore);
		Mm = ore.getMinutes();
		if (Hh > 24)
			Hh = Hh - 24;
		if (Hh < 10)
			Hh = "0" + Hh;
		if (Hh == 24)
			Hh = "00";
		if (Mm < 10)
			Mm = '0' + Mm;
		temp=ore.getDate();
		if (temp < 10)
			temp = '0' + temp;
		orario.innerText= Hh + ':' + Mm;
		giorno.innerText=temp;
		mese.innerText=TrovaMese(ore.getMonth());
		gsett.innerText=TrovaGSett(ore.getDay());
	}
	
	function getDataOra()
	{
		var day, month, year, date, Hh, Mm, time;
		var ore = new Date;
		day = ore.getDate();
		month = ore.getMonth()+1;
		year = ore.getYear();
		if (day < 10)
		day = '0' + day;
		if (month < 10)
		month = '0' + month;
		date = day + '-' + month + '-' + year;
		Hh = ore.getHours() + OraLegaleSolare(ore);
		Mm = ore.getMinutes();
		if (Hh < 10) 
		Hh = '0' + Hh;
		if (Mm < 10)
		Mm = '0' + Mm;
		time = Hh + ":" + Mm;
		Orario.innerText = time;
		Data.innerText = date;
	}
		
</script></head>
<body scroll="no" BGCOLOR="black" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onload="setInterval('getDataOra()',1000)">
<div id="ImgVettore1" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore1" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica1" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica1" src="Images/Es_1.gif"></div>
<div id="Treno1" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">9555</div>
<div id="Loc1" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">ROMA TERMINI</div>
<div id="Ora1" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">10:47</div>
<div id="Rit1" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">25'
										</div>
<div id="InfoTreno1" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: FIRENZE C.M. (11:25) - ROMA TERMINI (12:50) -  PRIMA CLASSE IN CODA - </marquee></div>
<div id="Bin1" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">.</div>
<div id="Lampeggio1" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:0px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore2" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore2" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica2" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica2" src="Images/Es_1.gif"></div>
<div id="Treno2" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">9550</div>
<div id="Loc2" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">TORINO P.N.</div>
<div id="Ora2" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">10:48</div>
<div id="Rit2" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">10'
										</div>
<div id="InfoTreno2" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: MILANO P.GAR (11:55) - TORINO P.N. (12:50) -  PRIMA CLASSE IN TESTA - </marquee></div>
<div id="Bin2" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">4</div>
<div id="Lampeggio2" class="lampon" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:12px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										10
									</div>
<div id="ImgVettore3" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore3" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica3" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica3" src="Images/Reg_1.gif"></div>
<div id="Treno3" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">6486</div>
<div id="Loc3" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">PARMA</div>
<div id="Ora3" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">10:52</div>
<div id="Rit3" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">5'
										</div>
<div id="InfoTreno3" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: ANZOLA EMIL. (11.03) - SAMOGGIA (11.08) - CASTELFRANCO E. (11.14) - MODENA (11.23) - RUBIERA (11.32) - REGGIO EMIL. (11.40) - S.ILARIO D'E (11.53) - PARMA (12.03) - </marquee></div>
<div id="Bin3" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">2 OVEST</div>
<div id="Lampeggio3" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:24px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore4" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore4" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica4" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica4" src="Images/Es_1.gif"></div>
<div id="Treno4" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">9453</div>
<div id="Loc4" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">ROMA TERMINI</div>
<div id="Ora4" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:00</div>
<div id="Rit4" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">5'
										</div>
<div id="InfoTreno4" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: FIRENZE C.M. (11:36) - ROMA TERMINI (13:05) -  PRIMA CLASSE IN TESTA - </marquee></div>
<div id="Bin4" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">3</div>
<div id="Lampeggio4" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:36px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore5" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore5" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica5" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica5" src="Images/Reg_1.gif"></div>
<div id="Treno5" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">6349</div>
<div id="Loc5" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">PORRETTA T.</div>
<div id="Ora5" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:04</div>
<div id="Rit5" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">
</div>
<div id="InfoTreno5" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: BORGO PANIG. (11.10) - CASTELDEBOLE (11.13) - CASALECCHIO G. (11.17) - CASALECCHIO (11.20) - BORGONUOVO (11.24) - SASSO MARC. (11.29) - LAMA DI RENO (11.35) - MARZABOTTO (11.38) - PIAN DI VENOLA (11.41) - PIOPPE SALV. (11.46) - VERGATO (11.53) - RIOLA (12.01) - SILLA (12.09) - PORRETTA T. (12.14) - </marquee></div>
<div id="Bin5" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">5 OVEST</div>
<div id="Lampeggio5" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:48px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore6" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore6" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica6" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica6" src="Images/Reg_1.gif"></div>
<div id="Treno6" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">3001</div>
<div id="Loc6" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">RAVENNA</div>
<div id="Ora6" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:06</div>
<div id="Rit6" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">
</div>
<div id="InfoTreno6" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: S.LAZZAR (11.11) - OZZANO DELL'EMILIA (11.17) - VARIGNANA (11.21) - CASTEL S. P. (11.27) - IMOLA (11.35) - CASTELBOLOG. (11.41) - SOLAROLO (11.47) - LUGO (11.58) - BAGNACAVALLO (12.04) - RUSSI (12.10) - GODO (12.14) - RAVENNA (12.25) - </marquee></div>
<div id="Bin6" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">10</div>
<div id="Lampeggio6" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:60px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore7" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore7" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica7" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica7" src="Images/Reg_1.gif"></div>
<div id="Treno7" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">11433</div>
<div id="Loc7" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">PRATO</div>
<div id="Ora7" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:09</div>
<div id="Rit7" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">
</div>
<div id="InfoTreno7" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: BOLOGNA S.R. (11.15) - RASTIGNANO (11.19) - MUSIANO-PDM (11.24) - PIANORO (11.27) - MONZUNO-VADO (11.40) - GRIZZANA (11.48) - S.BENEDETTO VAL DI SAMBRO (11.53) - VERNIO (12.05) - VAIANO (12.14) - PRATO (12.23) - </marquee></div>
<div id="Bin7" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">1 EST</div>
<div id="Lampeggio7" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:72px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore8" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore8" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica8" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica8" src="Images/Es_1.gif"></div>
<div id="Treno8" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">9404</div>
<div id="Loc8" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">VENEZIA S. L.</div>
<div id="Ora8" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:10</div>
<div id="Rit8" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">
</div>
<div id="InfoTreno8" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: ROVIGO (11:43) - PADOVA (12:05) - VENEZIA MESTRE (12:21) - VENEZIA S. L. (12:33) -  PRIMA CLASSE IN TESTA - </marquee></div>
<div id="Bin8" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">7</div>
<div id="Lampeggio8" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:84px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore9" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore9" src="Images/Trenitalia_1.gif"></div>
<div id="ImgClassifica9" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica9" src="Images/Reg_1.gif"></div>
<div id="Treno9" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">6398</div>
<div id="Loc9" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">VERONA P.N.</div>
<div id="Ora9" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:10</div>
<div id="Rit9" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">
</div>
<div id="InfoTreno9" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: CALDERARA-BARGELLINO (11.21) - OSTERIA NUOVA (11.26) - S.GIOV.PERS. (11.32) - CREVALCORE (11.39) - S.FELICE P. (11.48) - MIRANDOLA (11.54) - POGGIO RUSCO (12.02) - OSTIGLIA (12.09) - NOGARA (12.19) - ISOLA D. SC. (12.28) - VERONA P.N. (12.43) - </marquee></div>
<div id="Bin9" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">3 OVEST</div>
<div id="Lampeggio9" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:96px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="ImgVettore10" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:0px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgVettore10" src="Images/Fer_1.gif"></div>
<div id="ImgClassifica10" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:31px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px;WIDTH:25px;"><img name="ImgClassifica10" src="Images/Reg_1.gif"></div>
<div id="Treno10" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:56px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:56px">11457</div>
<div id="Loc10" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:113px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:153px">VIGNOLA</div>
<div id="Ora10" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:266px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:70px">11:16</div>
<div id="Rit10" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:336px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:54px">
</div>
<div id="InfoTreno10" style="COLOR: white; FONT-FAMILY: RFI8x5; LEFT:390px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;WIDTH:190px;"><marquee scrolldelay="10" scrollamount="1">FERMA A: BORGO PANIG. (11.21) - CASTELDEBOLE (11.25) - CASALECCHIO G. (11.31) - CERETOLO (11.37) - CAS.PORT (11.42) - RIATE (11.47) - PILASTRINO (11.52) - ZOLA CENTRO (11.57) - VIA LUNGA (12.02) - CRESPELLANO (12.05) - MUFFA (12.08) - </marquee></div>
<div id="Bin10" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:594px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:12px;  WIDTH:81px; TEXT-ALIGN: right">7 OVEST</div>
<div id="Lampeggio10" class="lampoff" style="COLOR: white; FONT-FAMILY: RFI12x7; LEFT:300px; TOP:108px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; display:none">
										0
									</div>
<div id="Info0" style="COLOR: white; background-color:black; FONT-FAMILY: RFI12x7-BOLD;font-size: 12px ; LEFT:  133px; TOP: 120px; WIDTH:567; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute;HEIGHT:12px;WIDTH:576px">ATTENZIONE ! E' SEVERAMENTE VIETATO OLTREPASSARE LA LINEA   </div>
<div id="Info1" style="COLOR: white; background-color:black; FONT-FAMILY: RFI12x7-BOLD; font-size: 12px ; LEFT:  133px; TOP: 132px; WIDTH:567; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute;HEIGHT:12px;WIDTH:576px">GIALLA IN ATTESA DEI TRENI</div>
<div id="Orario" style="position:absolute; COLOR: white; FONT-FAMILY: RFI16x9; LEFT:10px; TOP:158px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:16px; WIDTH:60px">
</div>
<div id="Data" style="position:absolute; COLOR: white; FONT-FAMILY: RFI8x5; LEFT:70px; TOP:162px; FONT-WEIGHT: normal; LETTER-SPACING: 1pt; POSITION: absolute; HEIGHT:8px; WIDTH:55px">
</div>
</body>
</html>
</Dati></ShownData>
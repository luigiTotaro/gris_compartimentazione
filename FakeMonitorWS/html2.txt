<?xml version="1.0"?>
<ShownData><Formato>HTML</Formato><Dati>&lt;html xmlns:b="http://tsf.it/xsd" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:myfunct="urn:myfunct-scripts"&gt;
&lt;head&gt;
&lt;/head&gt;
&lt;body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgcolor="Black"&gt;
&lt;table height="72" cellSpacing="0" cellPadding="0"&gt;
&lt;tr style="COLOR: white; HEIGHT: 72px; BACKGROUND-COLOR: black"&gt;
&lt;td width="8" style="WIDTH:	 8px"&gt;
&lt;/td&gt;
&lt;td style="FONT-SIZE: 36pt;  FONT-FAMILY: FuturaSerieBQ-Bold;font-weight:bold; TEXT-ALIGN: left" width="672"&gt;Partenze
					&lt;font face="FuturaSerieBQ-Light; font-weight:lighter"&gt;Departures&lt;/font&gt;
&lt;/td&gt;
&lt;td width="100" style="VERTICAL-ALIGN: middle; WIDTH: 100px; ALIGN: left"&gt;
&lt;IMG height="38" src="Images/Rfi_1.gif" width="96" /&gt;
&lt;/td&gt;
&lt;td width="21" style="WIDTH:	 21px"&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;table cellSpacing="0" cellPadding="0" height="432"&gt;
&lt;TR style="COLOR: black;  HEIGHT: 72px; BACKGROUND-COLOR: gold"&gt;
&lt;TD width="8" height="72"&gt;&lt;/TD&gt;
&lt;TD width="102" height="72"&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Bold;font-weight:bold; FONT-SIZE: 17pt; TEXT-ALIGN: left"&gt;&lt;B&gt;treno&lt;/B&gt;
&lt;/DIV&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Light; font-weight:lighter; FONT-SIZE: 16pt; TEXT-ALIGN: left"&gt;train
					&lt;/DIV&gt;
&lt;/TD&gt;
&lt;TD width="148" height="72"&gt;&lt;/TD&gt;
&lt;TD width="230" height="72"&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Bold;font-weight:bold ; FONT-SIZE: 17pt; TEXT-ALIGN: left"&gt;&lt;B&gt;destinazione&lt;/B&gt;
&lt;/DIV&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Light; font-weight:lighter; FONT-SIZE: 16pt; TEXT-ALIGN: left"&gt;destination
					&lt;/DIV&gt;
&lt;/TD&gt;
&lt;TD width="102" height="72"&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Bold;font-weight:bold ; FONT-SIZE: 17pt; TEXT-ALIGN: left"&gt;&lt;B&gt;orario&lt;/B&gt;
&lt;/DIV&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Light; font-weight:lighter; FONT-SIZE: 16pt; TEXT-ALIGN: left"&gt;time
					&lt;/DIV&gt;
&lt;/TD&gt;
&lt;TD width="102" height="72"&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Bold;font-weight:bold; FONT-SIZE: 17pt; TEXT-ALIGN: left"&gt;&lt;B&gt;ritardo&lt;/B&gt;
&lt;/DIV&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Light; font-weight:lighter; FONT-SIZE: 16pt; TEXT-ALIGN: left"&gt;delay
					&lt;/DIV&gt;
&lt;/TD&gt;
&lt;TD width="67" height="72"&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Bold;font-weight:bold; FONT-SIZE: 17pt; TEXT-ALIGN: left"&gt;&lt;B&gt;binario&lt;/B&gt;
&lt;/DIV&gt;
&lt;DIV style="FONT-FAMILY: FuturaSerieBQ-Light; font-weight:lighter; FONT-SIZE: 16pt; TEXT-ALIGN: left"&gt;platform&lt;/DIV&gt;
&lt;/TD&gt;
&lt;TD width="38" height="72"&gt;&lt;/TD&gt;
&lt;/TR&gt;
&lt;tr style="COLOR: #fee581; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #565655"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_2.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_2.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;5492&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VERONA P.N.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;12:18&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;4&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #565655; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #fee581"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_1S.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_1S.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;2098&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;MILANO C.LE&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;12:37&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;4&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #fee581; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #565655"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_2.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_2.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;5493&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VENEZIA S.L.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;13:09&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;3&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #565655; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #fee581"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_1S.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_1S.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;20572&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VERONA P.N.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;13:18&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;4&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #fee581; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #565655"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_2.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_2.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;2095&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VENEZIA S.L.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;13:23&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;3&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #565655; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #fee581"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_1S.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_1S.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;20557&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;PADOVA&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;13:41&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;3&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #fee581; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #565655"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_2.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_2.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;20614&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VERONA P.N.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;14:18&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;4&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #565655; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #fee581"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_1S.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_1S.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;5496&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VERONA P.N.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;14:40&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;4&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #fee581; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #565655"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_2.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_2.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;5495&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VENEZIA S.L.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;14:41&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;3&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #565655; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #fee581"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_1S.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_1S.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;5498&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VERONA P.N.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;15:40&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;4&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #fee581; FONT-FAMILY: Zurich cn bt; HEIGHT: 28px; BACKGROUND-COLOR: #565655"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/Trenitalia_2.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/Reg_2.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;5497&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;VENEZIA S.L.&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;15:41&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;" /&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;3&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="38" /&gt;
&lt;/tr&gt;
&lt;tr style="COLOR: #565655; FONT-FAMILY: Zurich cn bt; HEIGHT: 36px; BACKGROUND-COLOR: #fee581"&gt;
&lt;td width="8" height="38"&gt;
&lt;/td&gt;
&lt;td width="102" height="38"&gt;
&lt;img name="ImgVettore" src="Images/NotFound_1.gif" height="24" width="100" /&gt;
&lt;/td&gt;
&lt;td width="148" height="38"&gt;
&lt;img name="ImgClassifica" src="Images/NotFound_1.gif" height="24" width="37" /&gt;
&lt;span id="Numero" style="FONT-SIZE: 22pt; TEXT-ALIGN: center"&gt;
&lt;/span&gt;
&lt;/td&gt;
&lt;td ID="Destinazione" width="230" height="38" style="FONT-SIZE: 22pt;"&gt;
&lt;/td&gt;
&lt;td ID="Orario" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;
&lt;/td&gt;
&lt;td ID="Ritardo" width="102" height="38" style="FONT-SIZE: 22pt;"&gt;
&lt;/td&gt;
&lt;td ID="Binario" width="67" height="38" style="FONT-SIZE: 22pt;"&gt;
&lt;/td&gt;
&lt;td height="38" ID="Lampeggio" width="40"&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/body&gt;
&lt;/html&gt;
</Dati></ShownData>

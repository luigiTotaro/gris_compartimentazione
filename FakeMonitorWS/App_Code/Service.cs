using System;
using System.Collections.Generic;
using System.Web.Services;
using System.IO;
using System.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service : WebService {
    [WebMethod]
    public string RequestDaemonRestart() {
        return "Daemon Restarted.";
    }

    [WebMethod]
    public int GetDaemonStatus() {
        return 1;
    }

    [WebMethod]
    public string GetDaemonConfiguration() {
        return "Daemon Configuration.";
    }

    [WebMethod]
    public string GetShownData() {
        List<string> screenDumpTestFileList = new List<string> {"html.txt", "html2.txt", "html3.txt"};

        #region Genera un contatore in base alla decina dei minuti, per restituire uno ScreenDump casuale in base all'ora

        int minutes = DateTime.Now.Minute;
        int sdCounter = 0;

        if ((minutes >= 0) && (minutes <= 9)) {
            sdCounter = 0;
        }
        else if ((minutes >= 10) && (minutes <= 19)) {
            sdCounter = 1;
        }
        else if ((minutes >= 20) && (minutes <= 29)) {
            sdCounter = 2;
        }
        else if ((minutes >= 30) && (minutes <= 39)) {
            sdCounter = 0;
        }
        else if ((minutes >= 40) && (minutes <= 49)) {
            sdCounter = 1;
        }
        else if ((minutes >= 50) && (minutes <= 59)) {
            sdCounter = 2;
        }

        #endregion

        string html;
        using (StreamReader sr = File.OpenText(this.Server.MapPath(screenDumpTestFileList[sdCounter]))) {
            html = sr.ReadToEnd();
            sr.Close();
        }

        return html.Replace("src=\"Images", "src=\"" + HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter +
                                  HttpContext.Current.Request.Url.Authority +
                                  (HttpContext.Current.Request.ApplicationPath.EndsWith("/")
                                       ? HttpContext.Current.Request.ApplicationPath
                                       : HttpContext.Current.Request.ApplicationPath + "/") + "Images");
    }

    [WebMethod]
    public string GetScreenDump() {
        List<string> screenDumpTestFileList = new List<string> {
                                                               "SolariLed.txt",
                                                               "SolariTFT.txt",
                                                               "SyscoCRT.txt",
                                                               "SyscoLed.txt",
                                                               "AesysLed.txt",
                                                               "AesysTFT.txt"
                                                           };

        #region Genera un contatore in base alla decina dei minuti, per restituire uno ScreenDump casuale in base all'ora

        int minutes = DateTime.Now.Minute;
        int sdCounter = 0;

        if ((minutes >= 0) && (minutes <= 9)) {
            sdCounter = 0;
        }
        else if ((minutes >= 10) && (minutes <= 19)) {
            sdCounter = 1;
        }
        else if ((minutes >= 20) && (minutes <= 29)) {
            sdCounter = 2;
        }
        else if ((minutes >= 30) && (minutes <= 39)) {
            sdCounter = 3;
        }
        else if ((minutes >= 40) && (minutes <= 49)) {
            sdCounter = 4;
        }
        else if ((minutes >= 50) && (minutes <= 59)) {
            sdCounter = 5;
        }

        #endregion

        string content;
        using (StreamReader sr = File.OpenText(this.Server.MapPath(screenDumpTestFileList[sdCounter]))) {
            content = sr.ReadToEnd();
            sr.Close();
        }

        return content;
    }

    [WebMethod]
    public string GetOperationLog(int numeOperazioni) {
        return "The log.";
    }
}
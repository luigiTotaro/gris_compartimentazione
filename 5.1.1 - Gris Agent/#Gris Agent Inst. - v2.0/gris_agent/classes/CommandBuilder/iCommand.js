'use strict';

class iCommand {

	constructor() {
		//this.nextCommand = null;
	}




	run(context, on_complete) {

		throw 'no implementation';

	}

    getVersion() {
        throw 'no implementation';
    }   

	execute(context, on_complete) {

		let self = this;

		self.run(context, (err) => {
			
			if(err) {
				on_complete(err, null);
			} else if(self.nextCommand) {
				self.nextCommand.execute(context, on_complete);
			} else {
				on_complete(null, context);
			}
		});

	}

}


module.exports = iCommand;
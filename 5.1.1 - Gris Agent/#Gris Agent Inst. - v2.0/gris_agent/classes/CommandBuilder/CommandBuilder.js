'use strict';
const fs = require("fs");
const path = require('path');
const recursive = require("recursive-readdir");
const url = require('url');
const http = require('https');
const request = require('request');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
const decache = require('decache');
const broker = require('../broker/broker.js');
const config = require('../../config.js');

module.exports =  (function() {

	console.log("####","CommandBuilder","####");
	
	const commands_path = path.normalize(path.join(__dirname, './implementations'));

	if(!fs.existsSync(commands_path)) {
		fs.mkdirSync(commands_path);
	}

	var concrete_commands = [];

	recursive(commands_path, (err, files) => {

		files.forEach((file) => {

			let filename = path.basename(file);

			let class_name = filename
				.replace('.js','');

			concrete_commands[class_name] = require(file);

			console.log("Instanziata classe dei comandi: " + class_name);

		});

	});

	var _getCommandArray = function() {
		return concrete_commands;
	}
	
	var _getCommandPath = function() {
		var mypath = path.normalize(path.join(__dirname, './implementations'));
		return mypath;  
	}	

	var _getCommand = function(type) {
		return new concrete_commands[type]();
	}

	var _processa = function(messaggio) {
		var self=this;
		
		console.log(messaggio);

		//controllo se quella classe è presente
		if (!(messaggio.IdComando in concrete_commands))
		{
			//comando non presente, devo scaricarlo dal server centrale
			console.log("comando non presente, devo scaricarlo dal server centrale");

			self.checkNewFile(messaggio, true);
			
		}
		else
		{
			console.log("comando presente, controllo la versione");
			//comando presente
			//controllo la versione
			let comando=self.getCommand(messaggio.IdComando);
			if (comando.getVersion()==messaggio.Versione)
			{
				//stessa versione, tutto ok
				console.log("stessa versione, tutto ok");
				//creo il parametro da passare
				let parameter = {
					parameters:messaggio.Parameters,
					deviceId:messaggio.DeviceId
				}
				comando.run(JSON.stringify(parameter), (result,err) => {

					if(err) {
						console.log(err);
					}
					self.sendReply (messaggio.CommandReplyId, result, config.ServerID, messaggio.DeviceId);
					
				});
			}
			else
			{
				//versione diversa, devo scaricarlo dal server centrale
				console.log("versione diversa, devo scaricarlo dal server centrale");

				self.checkNewFile(messaggio, false);


			}
		}
		
	}

	var _checkNewFile = function(command, isNew)
	{
		var self = this;
		var file_url = config.commandRepositoryUrl + command.IdComando + ".js";

		var options = {
			host: url.parse(file_url).host,
			//port: 80,
			path: url.parse(file_url).pathname,
			method:'HEAD',
			rejectUnauthorized: false
		};

		let req = http.request(options, function(r) {
			if (r.statusCode==200)
			{
				//il file è presente
				self.downloadNewFile(command, isNew);
			}
			else
			{
				//problema col file
				self.sendReply (command.CommandReplyId, "Errore durante il download della classe del messaggio - errCode: " + r.statusCode + " - errMessage: " + r.statusMessage, null, null);
			}
		});
		
		req.on('error', (e) => {
			console.log("Errore durante il download della classe del messaggio: " + e.message);
			self.sendReply (command.CommandReplyId, "Errore durante il download della classe del messaggio: " + e.message, null, null);
		});

		req.end();

	}

	var _downloadNewFile = function(command, isNew)
	{
		var self = this;
		// App variables
		var file_url = config.commandRepositoryUrl + command.IdComando + ".js";
		var DOWNLOAD_DIR = './classes/CommandBuilder/implementations/';

		var options = {
			host: url.parse(file_url).host,
			//port: 80,
			path: url.parse(file_url).pathname,
			rejectUnauthorized: false
		};

		var file_name = url.parse(file_url).pathname.split('/').pop();
		var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

		http.get(options, function(res) {
			res.on('data', function(data) {
					//controllo se ha trovato il file
					file.write(data);
				}).on('end', function() {
					file.end(function() {
						console.log(file_name + ' downloaded to ' + DOWNLOAD_DIR);
						self.newRequire(command, file_name, isNew);
					});
					//console.log(file_name + ' downloaded to ' + DOWNLOAD_DIR);
				
					//self.newRequire(command, file_name, isNew);

				}).on('error', function(e) {
					console.log("Errore durante il download della classe del messaggio: " + e.message);
					self.sendReply (command.CommandReplyId, "Errore durante il download della classe del messaggio: " + e.message, null, null);
				});
		}).on('error', function(e) {
			console.log("Errore durante il download della classe del messaggio: " + e.message);
			self.sendReply (command.CommandReplyId, "Errore durante il download della classe del messaggio: " + e.message, null, null);
		});
		

	}



	var _newRequire = function(command, filename, isNew) {
		
		var self = this;
		var mypath = this.getCommandPath() + "\\" + filename;
		
		if (!isNew)
		{
			//console.log("faccio il decache");
			decache(mypath);
		}
		
		//la includo
		//console.log("la includo");
		concrete_commands[command.IdComando] = require(mypath);

		//console.log("la prendo");
		let comando=this.getCommand(command.IdComando);
		if (comando.getVersion()==command.Versione)
		{
			//stessa versione, tutto ok, e dovrebbe esserlo, visto che ho appena caricato il file......
			console.log("stessa versione, tutto ok");
			//creo il parametro da passare
			let parameter = {
				parameters:command.Parameters,
				deviceId:command.DeviceId
			}
			comando.run(JSON.stringify(parameter), (result,err) => {

				if(err) {
					console.log(err);
				}
				self.sendReply (command.CommandReplyId, result, config.ServerID, command.DeviceId);
				
			});
		}
		else
		{
			//Problema, file appena scaricato ma versione errata
			console.log("Versione erata nel file appena scaricato.....");
			self.sendReply (command.CommandReplyId, "Classe Comando su server  - Versione errata - Richiesta: " + command.Versione + " - Presente: " + comando.getVersion(), null, null);
		
		}

	}	

	var _sendReply = function(idReply, response, serverId, deviceId) {
		
		//spedisco la risposta
		var commandReply = new Object();
		commandReply.commandReplyId = idReply;
		commandReply.result=response;
		commandReply.serverId=serverId;
		commandReply.deviceId=deviceId;
		if (config.testCommandControl)
		{
			console.log("Spedisco risposta al webapp agent:")
			console.log(commandReply)
		}
		else
		{
			broker.publish('PUBLISHER_COMMAND_REPLY',commandReply);
		}

	}		




	return {
		getCommand: _getCommand,
		processa: _processa,
		downloadNewFile: _downloadNewFile,
		checkNewFile: _checkNewFile,
		getCommandArray: _getCommandArray,
		getCommandPath: _getCommandPath,
		newRequire: _newRequire,
		sendReply: _sendReply
	}

})();


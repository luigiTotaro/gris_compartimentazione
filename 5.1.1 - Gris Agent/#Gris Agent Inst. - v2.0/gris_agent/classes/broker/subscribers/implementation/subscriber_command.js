"use strict";

const iSubscriber = require('../iSubscriber.js');
const config = require('../../../../config.js');
const commandBuilder = require('../../../CommandBuilder/CommandBuilder.js');


class subscriber_command extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/command_' + config.NodID
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_command event', topic, message.toString());

		var messaggio = JSON.parse(message.toString());
		commandBuilder.processa(messaggio);
	}

}

module.exports = subscriber_command;
"use strict";

const iSubscriber = require('../iSubscriber.js');


class subscriber_get_time extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/get_time'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_get_time event', topic, message.toString());
	}

}

module.exports = subscriber_get_time;
/**
 * Interfaccia per la gestione delle classi che implementano i publisher verso il broker. Tutte le 
 * classi che implementano questa interfaccia sono responsabili della sottoscrizione a determinati topic
 * verso il broker
 * @interface iPublisher
 */

'use strict';

const config = require('../../../config.js');
const restInterface = require('../../../classes/services/restInterface.js');
const path = require('path');
const fs = require('fs');
const http = require("http");
const querystring = require('querystring');


class iPublisher {

	constructor(opts) {
		
		this.opts = opts;
		
		if(!this.opts.topic) {
			throw 'parametro "topic" obbligatorio';
		}

		if(!this.opts.key) {
			throw 'parametro "key" obbligatorio';
		}

	}


	get_key() {

		return this.opts.key;
	
	}



	publish(client, message) {
		
		if(client.constructor.name !== 'MqttClient') {
			throw 'parametro client non di tipo MqttClient';
		}

		let _msg = null;

		if(typeof message === 'object') {
			_msg = JSON.stringify(message);
		} else if (typeof message === 'string') {
			_msg = message;
		} else {
			throw 'tipo di messaggio ' + (typeof message) + ' non gestito';
		}
		
		//let compressed = zlib.gzipSync(_msg);


		client.publish(config.preTopic + this.opts.topic, _msg, config.broker_publish_ops, (err) => {
		//client.publish(this.opts.topic, compressed, config.broker_publish_ops, (err) => {
		});


	}

	aggiornaDataOra() {
		// e aggiorno la data/ora dell'ultimo invio, da usarsi per il IsAlive
		global.lastMessageSent = Date.now();
		let data = new Date(global.lastMessageSent);
		//data = global.lastMessageSent;
		console.log("aggiornato data/ora dell'ultimo messaggio inviato a: " + data.getHours().toString() + ":" + data.getMinutes().toString()+ ":" + data.getSeconds().toString());
	}

	writeTestFile(message) {

		if (config.testMode)
		{
			let _msg = null;

			if(typeof message === 'object') {
				_msg = JSON.stringify(message, null, 2);
			} else if (typeof message === 'string') {
				_msg = message;
			} else {
				_msg = 'tipo di messaggio ' + (typeof message) + ' non gestito';
			}

			let x = new Date();
			let y = x.getFullYear().toString();
			let m = (x.getMonth() + 1).toString();
			let d = x.getDate().toString();
			let h = x.getHours().toString();
			let mi = x.getMinutes().toString();
			let s = x.getSeconds().toString();
			let ms = x.getMilliseconds().toString();

			(d.length == 1) && (d = '0' + d);
			(m.length == 1) && (m = '0' + m);
			(h.length == 1) && (h = '0' + h);
			(mi.length == 1) && (mi = '0' + mi);
			(s.length == 1) && (s = '0' + s);
			(ms.length == 1) && (ms = '000' + ms);
			(ms.length == 2) && (ms = '00' + ms);
			(ms.length == 3) && (ms = '0' + ms);

			var dateformat = y + m + d + "_" + h + mi + s + ms;
			let topic = this.opts.topic.replace(/\//g, '_');
			
			let root_folder = path.join(__dirname, '..', '..', '..', "testFolder");
			let file_name = path.join(root_folder, dateformat + '_' + topic + '.json');
	
			if(!fs.existsSync(root_folder)) {
				fs.mkdirSync(root_folder);
			}
	
			if(fs.existsSync(file_name)) {
				fs.unlinkSync(file_name);
			}
	
			fs.writeFileSync(file_name, _msg);
		}		
	
	}

	sentToRestService(message,isCompressed) {
		
		if (!config.sendMessagesToRestService) return;

		let topic = this.opts.topic.replace(/\//g, '_');
		//tolgo il primo _ dal topic
		topic = topic.substring(1);
		let toSend={message:message, isCompressed:isCompressed,topic,topic};
		//console.log(toSend);
		restInterface.addToQueue(toSend);		
	}



}

module.exports = iPublisher;












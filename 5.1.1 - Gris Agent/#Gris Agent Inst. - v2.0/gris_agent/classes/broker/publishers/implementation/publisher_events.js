"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_events extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_EVENTS',
	    	topic: '/events'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_EVENTS Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);		
		super.sentToRestService(message, false);		
		super.aggiornaDataOra();		
	}
	

}

module.exports = publisher_events;
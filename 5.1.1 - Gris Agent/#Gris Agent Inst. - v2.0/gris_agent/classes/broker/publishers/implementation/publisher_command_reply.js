"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_command_reply extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_COMMAND_REPLY',
	    	topic: '/commandReply'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_COMMAND_REPLY Invio messaggio.....");
		super.publish(client, message);
	
	}
	

}

module.exports = publisher_command_reply;
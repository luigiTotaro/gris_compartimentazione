"use strict";
const path = require('path');
const fs = require('fs');
const save_dir = 'device_status_cache';
const config = require('../../config.js');

/**
 * Classe che rappresenta una riga di device_status
 * @param {int} dev_id Device ID,preso dal database
 * @param {int} sev_level Severity level,preso dal database
 * @param {string} description description,preso dal database
 * @param {int} offline offline,preso dal database
 * @param {int} ack_flag ack_flag,preso dal database
 * @param {string} ack_date ack_date,preso dal database
 * @param {int} should_send_notification_by_email should_send_notification_by_email,preso dal database
 *
 * @constructor
 */
class device_status {

    constructor(dev_id, sev_level, description, offline, ack_flag, ack_date, should_send_notification_by_email) {

        this.json = {
            DevID: dev_id,
            SevLevel: sev_level,
            Description: description,
            Offline: offline,
            AckFlag: ack_flag,
            AckDate: ack_date,
            ShouldSendNotificationByEmail: should_send_notification_by_email,
            streams: [],
            stream_fields: []
        };

    }

  /**
   * Aggiunge a quel device status uno stream associato
   * @param {int} str_id Stream Id
   * @param {String} name name
   * @param {int} visible visible
   * @param {Buffer} data data
   * @param {String} date_time Data e ora
   * @param {int} sev_level Severity level
   * @param {String} description description
   * @param {boolean} processed processed
   */
    add_stream(str_id, name, visible, data, date_time, sev_level, description, processed) {
        this.json.streams.push({
            StrID: str_id,
            Name: name,
            Visible: visible,
            Data: data,
            DateTime: date_time,
            SevLevel: sev_level,
            Description: description,
            Processed: processed
        });
    }
  
    /**
    * Aggiunge a quel device status uno stream field associato
    * @param {int} str_id Stream Id
    * @param {int} field_id Stream Field Id
    * @param {int} array_id Array Id
    * @param {String} name name
    * @param {int} sev_level Severity level
    * @param {Buffer} value value
    * @param {String} description description
    * @param {int} visible visible
    * @param {int} reference_id reference Id
    * @param {int} ack_flag ack_flag
    * @param {string} ack_date ack_date
    * @param {boolean} is_table is_table
    * @param {boolean} should_send_notification_by_email should_send_notification_by_email
    */
    add_stream_field(str_id, field_id, array_id, name, sev_level, value, description, visible, reference_id, ack_flag, ack_date, is_table, should_send_notification_by_email) {
        this.json.stream_fields.push({
            StrID: str_id,
            FieldID: field_id,
            ArrayID: array_id,
            Name: name,
            SevLevel: sev_level,
            Value: value,
            Description: description,
            Visible: visible,
            ReferenceID: reference_id,
            AckFlag: ack_flag,
            AckDate: ack_date,
            IsTable: is_table,
            ShouldSendNotificationByEmail: should_send_notification_by_email
        });
    }

    get_json() {
        return this.json;        
    }

    get_only_device_status() {
        return {
                DevID: this.json.DevID,
                SevLevel: this.json.SevLevel,
                Description: this.json.Description,
                Offline: this.json.Offline,
                AckFlag: this.json.AckFlag,
                AckDate: this.json.AckDate,
                ShouldSendNotificationByEmail: this.json.ShouldSendNotificationByEmail
        }
    }

    get_streams() {
        let ret = this.json.streams;
        for (let i=0;i<ret.length;i++) ret[i].DevID = this.json.DevID;
        return ret;
    }
    get_stream_fields() {
        let ret = this.json.stream_fields;
        for (let i=0;i<ret.length;i++) ret[i].DevID = this.json.DevID;
        return ret;
    }

    /** 
 	* @function 'save_to_file' - persistenza su file
	* @param {device_status} device_status - oggetto da persistere sul filesystem
	*/	
   static save_to_file(device_status) {

        let root_folder = path.join(__dirname, '..', '..', save_dir);
        let cache_file = path.join(root_folder, device_status.json.DevID + '.json');

        if(!fs.existsSync(root_folder)) {
            fs.mkdirSync(root_folder);
        }

        if(fs.existsSync(cache_file)) {
            fs.unlinkSync(cache_file);
        }

        fs.writeFileSync(cache_file, JSON.stringify(device_status.json, null, 2));

    }


    /** 
 	* @function 'load_from_file' - carico da file
	* @param {string} DevID - parametro per comporre il nome del file da caricare
	*/
    static load_from_file(DevID) {

        let shadow_item = null;

        DevID = DevID.replace(/["']{1}/gi,"");
        let root_folder = path.join(__dirname, '..', '..', save_dir);
        let cache_file = path.join(root_folder, DevID + '.json');

        if(fs.existsSync(root_folder) && fs.existsSync(cache_file)) {
                                    
            try {

                let data = JSON.parse(fs.readFileSync(cache_file));

                shadow_item = new device_status(
                    data.DevID,
                    data.SevLevel,
                    data.Description,
                    data.Offline,
                    data.AckFlag,
                    data.AckDate,
                    data.ShouldSendNotificationByEmail
                );

                for(var j in data.streams) {
                    shadow_item.add_stream(
                        data.streams[j]['StrID'],
                        data.streams[j]['Name'],
                        data.streams[j]['Visible'],
                        data.streams[j]['Data'],
                        data.streams[j]['DateTime'],
                        data.streams[j]['SevLevel'],
                        data.streams[j]['Description'],
                        data.streams[j]['Processed']
                    );
                }
                
                for(var j in data.stream_fields) {
                    shadow_item.add_stream_field(
                        data.stream_fields[j]['StrID'],
                        data.stream_fields[j]['FieldID'],
                        data.stream_fields[j]['ArrayID'],
                        data.stream_fields[j]['Name'],
                        data.stream_fields[j]['SevLevel'],
                        data.stream_fields[j]['Value'],
                        data.stream_fields[j]['Description'],
                        data.stream_fields[j]['Visible'],
                        data.stream_fields[j]['ReferenceID'],
                        data.stream_fields[j]['AckFlag'],
                        data.stream_fields[j]['AckDate'],
                        data.stream_fields[j]['IsTable'],
                        data.stream_fields[j]['ShouldSendNotificationByEmail']
                    );
                }

            } catch(err) {
                console.log(err);
            }

        }

        return shadow_item;

    }

    /** 
 	* @function 'compare' - opera una comparazione tra due oggetti device_status
	* @param {device_status} device_status0 - primo oggetto da comparare
	* @param {device_status} device_status1 - secondo oggetto da comparare
	* @returns {object} Oggetto contenente le differenze tra i due oggetti
	*/
    static compare(device_status0, device_status1) {

        let delta = {};
        let jsonNew = device_status0.get_json();
        let jsonCached = device_status1.get_json();

        //comparo il device status, ma solo delle colonne su cui devo farlo
        var columnToCompare = device_status.extractCompareFields("devicestatus");
        if ((((columnToCompare.indexOf("AckDate")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (jsonNew.AckDate!=jsonCached.AckDate)) ||
        (((columnToCompare.indexOf("AckFlag")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (jsonNew.AckFlag!=jsonCached.AckFlag)) ||
        (((columnToCompare.indexOf("Description")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (jsonNew.Description!=jsonCached.Description)) ||
        (((columnToCompare.indexOf("Offline")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (jsonNew.Offline!=jsonCached.Offline)) ||
        (((columnToCompare.indexOf("SevLevel")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (jsonNew.SevLevel!=jsonCached.SevLevel)) ||
        (((columnToCompare.indexOf("ShouldSendNotificationByEmail")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (jsonNew.ShouldSendNotificationByEmail!=jsonCached.ShouldSendNotificationByEmail)))
        {
            delta.device_status={
                DevID: jsonNew.DevID,
                SevLevel: jsonNew.SevLevel,
                Description: jsonNew.Description,
                Offline: jsonNew.Offline,
                AckFlag: jsonNew.AckFlag,
                AckDate: jsonNew.AckDate,
                ShouldSendNotificationByEmail: jsonNew.ShouldSendNotificationByEmail
            }
        }

        //comparo le righe streams, per cecare stream nuovi o modificati
        for (let i=0;i<jsonNew.streams.length;i++)
        {
            //devo vedere se questo stream esiste nella versione cachata o se esiste ma è diverso....
            var toChange = device_status.checkStreamComparison(jsonNew.streams[i], jsonCached.streams);
            if (toChange)
            {
                if (!delta.streams) delta.streams=[];
                let stream = jsonNew.streams[i];
                stream.DevID = jsonNew.DevID;               
                delta.streams.push(stream);
            }
        }
        //comparo le righe di streams, per cercare streams cancellati
        for (let i=0;i<jsonCached.streams.length;i++)
        {
            //devo vedere se questo stream esiste nella versione attuale
            var toDelete = device_status.checkStreamDeleted(jsonCached.streams[i], jsonNew.streams);
            if (toDelete)
            {
                if (!delta.streams) delta.streams=[];
                let stream = jsonCached.streams[i];
                stream.DevID = jsonCached.DevID;               
                stream.grisAskRemoval=true; //per indicare che la deve rimuovere               
                delta.streams.push(stream);
            }
        }

        
        //comparo le righe stream_fields, per cecare stream_fields nuovi o modificati
        for (let i=0;i<jsonNew.stream_fields.length;i++)
        {
            //devo vedere se questo stream_field esiste nella versione cachata o se esiste ma è diverso....
            var toChange = device_status.checkStreamFieldComparison(jsonNew.stream_fields[i], jsonCached.stream_fields);
            if (toChange)
            {
                if (!delta.stream_fields) delta.stream_fields=[];
                let stream_field = jsonNew.stream_fields[i];
                stream_field.DevID = jsonNew.DevID;               
                delta.stream_fields.push(stream_field);
            }
        } 
        //comparo le righe stream_fields, per cercare stream_fields cancellati
        for (let i=0;i<jsonCached.stream_fields.length;i++)
        {
            //devo vedere se questo stream_field esiste nella versione attuale
            var toDelete = device_status.checkStreamFieldDeleted(jsonCached.stream_fields[i], jsonNew.stream_fields);
            if (toDelete)
            {
                if (!delta.stream_fields) delta.stream_fields=[];
                let stream_field = jsonCached.stream_fields[i];
                stream_field.DevID = jsonCached.DevID;               
                stream_field.grisAskRemoval=true; //per indicare che la deve rimuovere               
                delta.stream_fields.push(stream_field);
            }
        }         

        return delta;
    }

    /** 
 	* @function 'checkStreamComparison' - sezione di compare che si occupa di comparare gli streams
	* @param {object} streamNew - stream da ricercare tra quelli in cache
	* @param {array} streamsCached - array di streams nel quale cercare
	* @returns {boolean} true/false, indica se è da inserire nei delta o meno
    */
    static checkStreamComparison(streamNew, streamsCached) {
        var trovato=false;
        var diverso=false;
        for (let i=0;i<streamsCached.length;i++)
        {
            if (streamsCached[i].StrID==streamNew.StrID)
            {
                trovato=true;
                //vedo se è diverso

                var columnToCompare = device_status.extractCompareFields("streams");
                if ((((columnToCompare.indexOf("Name")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamNew.Name!=streamsCached[i].Name)) ||
                (((columnToCompare.indexOf("Visible")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamNew.Visible!=streamsCached[i].Visible)) ||
                (((columnToCompare.indexOf("Data")!=-1) || (columnToCompare.indexOf("*")!=-1)) && ((streamNew.Data!=null && streamNew.Data.toISOString()!=streamsCached[i].Data) || (streamNew.Data==null && streamsCached[i].Data!=null))) ||
                (((columnToCompare.indexOf("DateTime")!=-1) || (columnToCompare.indexOf("*")!=-1)) && ((streamNew.DateTime!=null && streamNew.DateTime.toISOString()!=streamsCached[i].DateTime) || (streamNew.DateTime==null && streamsCached[i].DateTime!=null))) ||
                (((columnToCompare.indexOf("SevLevel")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamNew.SevLevel!=streamsCached[i].SevLevel)) ||
                (((columnToCompare.indexOf("Description")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamNew.Description!=streamsCached[i].Description)) ||
                (((columnToCompare.indexOf("Processed")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamNew.Processed!=streamsCached[i].Processed))) 
                {
                    diverso=true;
                }
                break;
            }
        }
        if ((!trovato) || (trovato && diverso)) return true;
        else return false;
    
    }

    /** 
 	* @function 'checkStreamFieldComparison' - sezione di compare che si occupa di comparare gli stream fields
	* @param {object} streamFieldNew - stream field da ricercare tra quelli in cache
	* @param {array} streamFieldsCached - array di stream fields nel quale cercare
	* @returns {boolean} true/false, indica se è da inserire nei delta o meno
    */
    static checkStreamFieldComparison(streamFieldNew, streamFieldsCached) {
        var trovato=false;
        var diverso=false;
        for (let i=0;i<streamFieldsCached.length;i++)
        {
            if ((streamFieldsCached[i].StrID==streamFieldNew.StrID) && (streamFieldsCached[i].FieldID==streamFieldNew.FieldID) && (streamFieldsCached[i].ArrayID==streamFieldNew.ArrayID))
            {
                trovato=true;
                //vedo se è diverso

                var columnToCompare = device_status.extractCompareFields("streamfields");
                if (
                (((columnToCompare.indexOf("Name")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.Name!=streamFieldsCached[i].Name)) ||
                (((columnToCompare.indexOf("SevLevel")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.SevLevel!=streamFieldsCached[i].SevLevel)) ||
                (((columnToCompare.indexOf("Value")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.Value!=streamFieldsCached[i].Value)) ||
                (((columnToCompare.indexOf("Description")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.Description!=streamFieldsCached[i].Description)) ||
                (((columnToCompare.indexOf("Visible")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.Visible!=streamFieldsCached[i].Visible)) ||
                (((columnToCompare.indexOf("ReferenceID")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.ReferenceID!=streamFieldsCached[i].ReferenceID)) ||
                (((columnToCompare.indexOf("AckFlag")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.AckFlag!=streamFieldsCached[i].AckFlag)) ||
                (((columnToCompare.indexOf("AckDate")!=-1) || (columnToCompare.indexOf("*")!=-1)) && ((streamFieldNew.AckDate!=null && streamFieldNew.AckDate.toISOString()!=streamFieldsCached[i].AckDate) || (streamFieldNew.AckDate==null && streamFieldsCached[i].AckDate!=null))) ||
                (((columnToCompare.indexOf("IsTable")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.IsTable!=streamFieldsCached[i].IsTable)) ||
                (((columnToCompare.indexOf("ShouldSendNotificationByEmail")!=-1) || (columnToCompare.indexOf("*")!=-1)) && (streamFieldNew.ShouldSendNotificationByEmail!=streamFieldsCached[i].ShouldSendNotificationByEmail))) 
                {
                    diverso=true;
                }
                break;
            }
        }
        if ((!trovato) || (trovato && diverso)) return true;
        else return false;
    
    }

    /** 
 	* @function 'extractCompareFields' - estrae le colonne da confrontare, nelle operazioni di compare
	* @param {string} tableName - nome della tabella da ricercare tra i campi di config.SyncDataset_devicestatus
	* @returns {array} array di stringhe da usare per i confronti
    */
    static extractCompareFields(tableName) {
		var ret = new Array();
		var colonne="";
		for (var i=0;i<config.SyncDataset_devicestatus.length;i++)
		{
			if (config.SyncDataset_devicestatus[i].name==tableName) colonne=config.SyncDataset_devicestatus[i].compareColumn;
		}
		return colonne.split(",");    
    }


    /** 
 	* @function 'checkStreamDeleted' - sezione di compare che si occupa di verificare se uno stream è stato cancellato
	* @param {object} streamsCached - stream da ricercare tra quelli attuali
	* @param {array} streamNew - array di streams nel quale cercare
	* @returns {boolean} true/false, indica se è da inserire nei delta o meno (è stato cancellato)
    */
    static checkStreamDeleted(streamsCached, streamNew) {
        var trovato=false;

        for (let i=0;i<streamNew.length;i++)
        {
            if (streamNew[i].StrID==streamsCached.StrID)
            {
                trovato=true;
                break;
            }
        }
        if (!trovato) return true;
        else return false;

    }

        /** 
 	* @function 'checkStreamFieldDeleted' - sezione di compare che si occupa di verificare se uno stream field è stato cancellato
	* @param {object} streamsFieldCached - streamfield da ricercare tra quelli attuali
	* @param {array} streamFieldNew - array di streamfields nel quale cercare
	* @returns {boolean} true/false, indica se è da inserire nei delta o meno (è stato cancellato)
    */
    static checkStreamFieldDeleted(streamsFieldCached, streamFieldNew) {
        var trovato=false;

        for (let i=0;i<streamFieldNew.length;i++)
        {

            if ((streamFieldNew[i].StrID==streamsFieldCached.StrID) && (streamFieldNew[i].FieldID==streamsFieldCached.FieldID) && (streamFieldNew[i].ArrayID==streamsFieldCached.ArrayID))
            {
                trovato=true;
                break;
            }
        }
        if (!trovato) return true;
        else return false;

    }

}


module.exports = device_status;
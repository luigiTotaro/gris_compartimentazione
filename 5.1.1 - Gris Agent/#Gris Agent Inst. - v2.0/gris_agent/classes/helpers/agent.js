'use strict';
const config = require('../../config.js');
const CommonDAO = require('../database/CommonDAO.js');
const StreamDAO = require('../database/StreamDAO.js');
const broker = require('../broker/broker.js');
const fs = require('fs');
const logger = require('../utils/logger.js');
const utils = require('../utils/agentUtils.js');
const device_status = require('../beans/device_status.js');
//var zlib = require('zlib');
const path = require('path');
//const win = require('node-windows');
const child = require('child_process');

let _root_cache_folder = path.join(__dirname, '..', '..', "cacheFiles");
let _fileDeviceStatusCache = path.join(_root_cache_folder, 'DeviceStatusCache.json');
let _fileConfigCache = path.join(_root_cache_folder, 'ConfigCache.json');

//var _fileDeviceStatusCache = "./classes/cacheFiles/DeviceStatusCache.json";
//var _fileConfigCache = "./classes/cacheFiles/ConfigCache.json";
var _isConfigValid;
var _isStatusValid;
var _srvID;
var _groupingIndex=0; //mi dice quale messaggio dell'insieme di messaggi sto inviando
var _groupingTotal=0; //mi dice il totale dei messaggi che sto inviando

module.exports = (() => {

	/** 
 	* @function 'initialize' - Inizializza la classe Agent, recuperando il Server ID dal db. In caso di errore, il sistema non può partire
 	* @param {callback} on_complete - function(err)
 	*/
	 var _initialize = (on_complete) => {

	    CommonDAO.getServerData(null, (err, result) => {
	        	
			if (err)
			{
				on_complete("Errore, riga relativa al server non trovata nel db. Server non configurato");
			}
			else
			{
				if (result != null && result.length > 0)
				{
					_srvID = result[0].SrvID;
					config.ServerID=_srvID;
					logger.log('info', 'InitializeServers: ServerId= ' + _srvID);
					//console.log("InitializeServers: ServerId= " + _srvID);
					on_complete(null);
				}
				else
				{
					on_complete("Errore, riga relativa al server non trovata nel db. Server non configurato");
				}
			}
		});
	}


	/** 
 	* @function 'clearCache' - Funzione che si occupa di cancellare i file di cache memorizzati nella macchina, che vengono utilizzati per trovare le differenze nel caso di un invio del solo delta
 	* @param {callback} on_complete - function(err)
 	*/
	var _clearCache = (on_complete) => {

		logger.log('info','Agent: clearCache');
		
        if(!fs.existsSync(_root_cache_folder)) {
			fs.mkdirSync(_root_cache_folder);
			on_complete(null);
		}
		else
		{
			fs.unlink(_fileDeviceStatusCache, (err) => {
				if (err){console.log(_fileDeviceStatusCache + ' non trovato');}
				else {console.log(_fileDeviceStatusCache + ' cancellato');}
				fs.unlink(_fileConfigCache, (err) => {
					if (err){console.log(_fileConfigCache + ' non trovato');}
					else {console.log(_fileConfigCache + ' cancellato');}
					on_complete(null);
				});
			});
		}
		
		
		//on_complete(null);
	}


	/** 
 	* @function 'sendConfigToCentral' - Manda la configurazione del sistema al server centrale
 	* @param {boolean} only_changes - indica se inviare la configurazione completa o solo i cambiamenti
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendConfigToCentral = (only_changes, on_complete) => {

		logger.log('info','Agent: sendConfigToCentral');

		CommonDAO.getConfigData((err, result) => {
			if (err)
			{
				on_complete("Error getting config data from DB");
				return;
			}

			//ho recuperato i dati di configurazione
			//il config risultante da una chiamata dao risulta diverso dallo stesso dopo l'operazione di stringify per salvarlo in un file e successivo parse
			//dopo averlo ricaricato, dando così delle differenze che in realtà non ci sono. Per questo motivo faccio uno stringify e parse subito		
			//console.log(result);
			let configNewTemp=JSON.stringify(result);
			let configNew;
			try {
				configNew=JSON.parse(configNewTemp, utils.bufferReviver);
			}
			catch(err) {
				_isConfigValid = false; // invalido la configurazione
				on_complete("Error parsing config data from DB");		
				return;
			}			

			//carico il file in cache, se esiste
			fs.readFile(_fileConfigCache, 'utf8', (err, data) => {
				var configCache=new Object();
				if (err == null)
				{
					try {
						configCache=JSON.parse(data, utils.bufferReviver);
					}
					catch(err2) {
						_isConfigValid = false; // invalido la configurazione
						on_complete("Error parsing config data from cache file");
						return;
					}
				}

				//let object0 = Object.assign({}, result);
				//let object1 = Object.assign({}, result);
				var configChange=new Object();

				//only_changes=true; //per debug

				if ((err == null) && (only_changes))
				{
					//trovo le differenze
					var retObj = utils.mergeDataSet(configNew, configCache ,config.SyncDataset_config);
					configCache=retObj.cache;
					configChange=retObj.change;
				}
				else
				{
					console.log('Initialize config cache');
					configCache = configNew;
					configChange = configNew;
				}

				//nell'oggetto da spedire ci sono dei valori?
				if (Object.keys(configChange).length === 0 && configChange.constructor === Object)
				{
					logger.log('info','Agent: No config change');
					//console.log(configChange);
				}
				else
				{
					//controllo configurazione non valida
					if (configChange.hasOwnProperty('servers') && configChange['servers'].length>0 && configChange['servers'][0].SrvID==0)
					{
                        _isConfigValid = false; // invalido la configurazione
						on_complete("SrvID = 0, Centralization Stopped");
						return;
					}
					else
					{					
						var numRows=utils.getNumRows(configChange);
						//console.log("spedisco i dati della configurazione")
						//aggiungo i dati extra
						configChange.msgInfo = {
							mac: config.machineMac,
							SrvId: _srvID,
							externalIP: config.MachineExternalIP
						};
						
						if (only_changes)
						{
							configChange.msgInfo.msgType="UpdateConfig";
							logger.log('info','Agent: UpdateConfig, '+numRows+' rows');
							broker.publish('PUBLISHER_CONFIG_DELTA',configChange);
						}
						else
						{
							configChange.msgInfo.msgType="ReplaceConfig";
							logger.log('info','Agent: ReplaceConfig, '+numRows+' rows');
							broker.publish('PUBLISHER_CONFIG_FULL',configChange);
						}
					}
				}
				fs.writeFile(_fileConfigCache, JSON.stringify(configCache), 'utf8', (err) => {
					if (err) _isConfigValid=false;
					else _isConfigValid=true;
					console.log('Il file di cache della configurazione è stato salvato');
					on_complete(null);
					
				});
				
			
			});
		});

	}


	/** 
 	* @function '_process_group_list' - Funzione ricorsiva che processa uno dei gruppi dei device_status, associa streams e stream fields e manda il messaggio
 	* @param {array} groupings - array di device_status
 	* @param {boolean} only_changes - indica se deve mandare solo le differenze o lo stato completo
 	* @param {callback} on_complete - function(err)
 	*/
	var _process_group_list = (groupings, only_changes, on_complete) => {

		if(groupings.length === 0) {

			//fine
			on_complete();

		} else {

			//let grouping = [];
			let grouping = groupings.shift();

			StreamDAO.fillDeviceStatusGroup(grouping, () => {

				// grouping è un array associativo di device_status

				let toSend = {
					device_status: [],
					streams:[],
					stream_fields:[]
				};

				for(var DevID in grouping) {
					
					if(only_changes === false) {

						toSend.device_status.push( grouping[DevID].get_only_device_status() );
						toSend.streams = toSend.streams.concat(grouping[DevID].get_streams());
						toSend.stream_fields = toSend.stream_fields.concat(grouping[DevID].get_stream_fields());

					} else {

						let copy_from_cache = device_status.load_from_file(DevID);
						if (copy_from_cache==null) //non vi era una versione in cache, prendo tutto
						{
							toSend.device_status.push( grouping[DevID].get_only_device_status() );
							toSend.streams = toSend.streams.concat(grouping[DevID].get_streams());
							toSend.stream_fields = toSend.stream_fields.concat(grouping[DevID].get_stream_fields());
						}
						else
						{
							// compara le due versioni e restituisce la differenza
							let delta = device_status.compare(grouping[DevID], copy_from_cache);
							if (delta.device_status) toSend.device_status.push( delta.device_status );
							if (delta.streams) toSend.streams = toSend.streams.concat( delta.streams );
							if (delta.stream_fields) toSend.stream_fields = toSend.stream_fields.concat( delta.stream_fields );
						}
						
					}
					// e salvo anche sul file come cache la versione appena presa
					device_status.save_to_file(grouping[DevID]);

				}

				//se non devo spedire solo i cambiamenti, la parte da spedire e quella da memorizzare coincidono
				//let toSave = toSend;

				//ho tutta la roba da mandare
				var numRows=utils.getNumRows(toSend);
				toSend.msgInfo = {
					mac: config.machineMac,
					SrvId: _srvID,
					externalIP: config.MachineExternalIP,
					msgTotal:_groupingTotal,
					msgIndex:(_groupingTotal-groupings.length),
				};

				if (only_changes) //se devo mandare solo i cambiamenti, non ha senso mettere il numero dei pacchetti e il progressivo
				{
					toSend.msgInfo = {
						mac: config.machineMac,
						SrvId: _srvID,
						externalIP: config.MachineExternalIP						
					};
				}
				else
				{
					toSend.msgInfo = {
						mac: config.machineMac,
						SrvId: _srvID,
						externalIP: config.MachineExternalIP,
						msgTotal:_groupingTotal,
						msgIndex:(_groupingTotal-groupings.length)
					};					
				}
				//console.log("***************++++++++++++++");
				//console.log(toSend);
				
				if (numRows>0)
				{
					if (only_changes)
					{
						toSend.msgInfo.msgType="UpdateStatus";
						logger.log('info','Agent: UpdateStatus, '+numRows+' rows');
						broker.publish('PUBLISHER_DEVICESTATUS_DELTA',toSend);
					}
					else
					{
						toSend.msgInfo.msgType="ReplaceStatus";
						logger.log('info','Agent: ReplaceStatus, '+numRows+' rows - Invio '+toSend.msgInfo.msgIndex+' di '+toSend.msgInfo.msgTotal);
						broker.publish('PUBLISHER_DEVICESTATUS_FULL',toSend);
					}
				}
				else
				{
					logger.log('info','Agent: No Status change');
					//console.log(configChange);					
				}
				//fs.writeFileSync("devicestatustest", JSON.stringify(toSend, null, 2));

				_process_group_list(groupings, only_changes, on_complete);

				//delete grouping;

			});
		}
	}	

	/** 
 	* @function 'sendDeviceStatusToCentral' - Manda la i dati di device status al server centrale
 	* @param {boolean} only_changes - indica se inviare lo stato completo o solo i cambiamenti
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendDeviceStatusToCentral = (only_changes, on_complete) => {

		logger.log('info','Agent: SendDeviceStatusToCentral - only_changes: ' + only_changes);

		if (_isConfigValid==false)
		{
			on_complete ("Can't send Device Status when Config is not valid, recall SendDeviceStatusToCentral");
			return;
		}

		
		let DEVICE_SIZE = 50;

		// recupero un array di gruppi di 'device_status'
		StreamDAO.getDeviceStatusList(DEVICE_SIZE, (err, groupings) => {
			

			_groupingIndex=1;
			_groupingTotal=groupings.length;

			_process_group_list(groupings, only_changes, () => {

				console.log("fine");

				//ha finito di mandare i device status, ma è possibile che qualche device status sia stato cancellato, devo gestire la situazione
				//se è un invio full, mi salvo sul filesystem una lista di tutti i deviceID della tabella device status (un device status per ogni device)
				//se è un invio delta, vedo dalla lista full se ci sono degli eliminati, e prepara il messaggio opportuno
				
				
				CommonDAO.getDeviceStatusList((err, result) => {
					if (err)
					{
						//console.log(err);
						on_complete("Error getting device status data from DB");
						return;
					}

					if (!only_changes)
					{
						//invio full, scrivo solo il file
						fs.writeFile(_fileDeviceStatusCache, JSON.stringify(result), 'utf8', (err) => {
							console.log('Il file di cache dei device status è stato salvato');
							//posso uscire dalla funzione, non devo confrontare niente
							on_complete(null);
							return;
						});
					}
					else
					{

						//invio delta, confronto per veder se ci sono cancellazioni

						let statusActual=result;
						fs.readFile(_fileDeviceStatusCache, 'utf8', (err, data) => {
							var statusCache=new Object();
							if (err == null)
							{
								try {
									statusCache=JSON.parse(data, utils.bufferReviver);
								}
								catch(err2) {
									on_complete("Error parsing config data from cache file");
									return;
								}
							}


							var statusChange=new Object();

							let toSend = {
								device_status: []
							};

							//vedo, per ogni statusCache, se esiste nello statusActual.
							//se non lo trovo è stato cancellato
							let statusCacheTemp=statusCache;
							for (let i=0;i<statusCacheTemp.length;i++)
							{
								let devId= statusCacheTemp[i].DevID;
								//lo cerco in quello attuale
								let trovato=false;
								for (let t=0;t<statusActual.length;t++)
								{
									if (devId == statusActual[t].DevID)
									{
										trovato=true;
										break;
									}
								}
								if (!trovato)
								{
									//creo il messaggio da mandare
									statusCacheTemp[i].grisAskRemoval=true; //per indicare che la deve rimuovere
									toSend.device_status.push(statusCacheTemp[i]);
					
									//cancello l'elemento da statusCache
									for (var p=0;p<statusCache.length;p++)
									{
										if (statusCache[p].DevID==devId)
										{
											statusCache.splice(p,1)
											break;
										}	
									}

								}
							}

							
							//ho finito di processare, ci sono messaggi da mandara (cioè righe da cancellare)?
							if (toSend.device_status.length>0)
							{
								var numRows=utils.getNumRows(toSend);
								toSend.msgInfo = {
									mac: config.machineMac,
									SrvId: _srvID,
									externalIP: config.MachineExternalIP,
									msgType: "UpdateStatus"					
								};								
								
								logger.log('info','Agent: UpdateStatus, '+numRows+' rows');
								broker.publish('PUBLISHER_DEVICESTATUS_DELTA',toSend);

								fs.writeFile(_fileDeviceStatusCache, JSON.stringify(statusCache), 'utf8', (err) => {
									console.log('Il file di cache dei device status è stato salvato');
									on_complete(null);
									return;
									
								});								

							}
							
							
						});						

					}

				});


				
				
				on_complete(err);

			});
			

		});
		
	}



	/** 
 	* @function 'sendAcks' - Manda i dati presi dalla tabella device_ack
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendAcks = (on_complete) => {

		logger.log('info','Agent: sendAcks');

		var toSendAck=false;
		//se sono passati più di xxx minuti dall'ultimo Ack inviato, lo posso mandare
		if (global.lastAckSent==null) //mai mandato, lo mando
		{
			logger.log('info','Agent: primo invio di un Ack, lo posso mandare');
			toSendAck=true;
		}
		else
		{
			var adesso = Date.now();
			var target = global.lastAckSent + config.SendAckMinutes*60000;
			if (adesso > target)
			{
				logger.log('info','Agent: sono passati più di '+config.SendAckMinutes+' minuti dall\'ultimo invio di un Ack, lo posso mandare');
				toSendAck=true;
			}
			else
			{
				logger.log('info','Agent: sono passati meno di '+config.SendAckMinutes+' minuti dall\'ultimo invio di un Ack, NON lo mando');
				toSendAck=false;
			}
		}

		if (toSendAck)
		{

			CommonDAO.getAcksData((err, result) => {
				if (err)
				{
					//console.log(err);
					on_complete("Error getting device acks data from DB");
					return;
				}

				//controllo se il messaggio da mandare è uguale all'ultimo mandato
				//nel qual caso non lo mando
				let messageToCheck=JSON.stringify(result);
				if (messageToCheck==global.lastAckMessageSentToCheck)
				{
					on_complete("Messaggio Ack uguale all'ultimo inviato, non lo invio");
					return;					
				}
				
				global.lastAckMessageSentToCheck=messageToCheck;
				result.msgInfo = {
					mac: config.machineMac,
					SrvId: _srvID,
					externalIP: config.MachineExternalIP,
					msgType: "UpdateAcks"
				};

				//console.log(JSON.stringify(result));
				broker.publish('PUBLISHER_ACKS',result);
				global.lastAckSent = Date.now();
				on_complete(null);

			});
		}
		else
		{
			on_complete(null);
		}
	}


	/** 
 	* @function 'Object.compare' - override della funzione compare di Object, permette di comparare due oggetti
 	* @param {Object} obj1 - Primo oggetto da comparare
 	* @param {Object} obj2 - Secondo oggetto da comparare
	* @returns {boolean} true/false se i 2 oggetti sono uguali o meno
 	*/
	Object.compare = function (obj1, obj2) {
		//Loop through properties in object 1
		for (var p in obj1) {
			//Check property exists on both objects
			if (obj1.hasOwnProperty(p) !== obj2.hasOwnProperty(p)) return false;
	 
			switch (typeof (obj1[p])) {
				//Deep compare objects
				case 'object':
					if (!Object.compare(obj1[p], obj2[p])) return false;
					break;
				//Compare function code
				case 'function':
					if (typeof (obj2[p]) == 'undefined' || (p != 'compare' && obj1[p].toString() != obj2[p].toString())) return false;
					break;
				//Compare values
				default:
					if (p != "offset")
					{
						if (obj1[p] != obj2[p]) return false;
					}
				}
		}
	 
		//Check object 2 for any extra properties
		for (var p in obj2) {
			if (typeof (obj1[p]) == 'undefined') return false;
		}
		return true;
	};




	/** 
 	* @function 'sendIsAlive' - Funzione che manda al server centrale un messaggio di IsAlive in modo da far sapere - al momento non implementata
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendIsAlive = (on_complete) => {

		console.log("sendIsAlive");

		on_complete(null);
	}


	/** 
 	* @function 'sendEvents' - Manda gli eventi, recuperati dalla tabella events, al server centrale
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendEvents = (on_complete) => {

		logger.log('info','Agent: sendEvents');

		CommonDAO.getEventsData((err, result) => {

			if (err)
			{
				on_complete("Error retrieving Events Data from DB");
				return;
			}

			var numRows = utils.getNumRows(result);

			console.log("spedisco i dati degli eventi")
			//aggiungo i dati extra
			result.msgInfo = {
				mac: config.machineMac,
				SrvId: _srvID,
				externalIP: config.MachineExternalIP,
				msgType: "UpdateEvents"				
			};


			//console.log(JSON.stringify(result));			

			logger.log('info','Agent: Event to send, '+numRows+' rows');
			broker.publish('PUBLISHER_EVENTS',result);

			//cancello le righe appena mandate
			var inclusion = [];
			for (var i=0;i<result.events.length;i++)
			{
				inclusion.push("'"+result.events[i].EventID+"'");
			}

			//cancella gli eventi spediti... (o li marca come processati)
			if (!config.testMode && inclusion.length > 0)
			{
				if (config.MarkEventsAsProcessed==true)
				{
					CommonDAO.setEventsDataAsProcessed(inclusion, (err, result) => {

						if (err) on_complete("Error setting Events Data as processed: " + err);
						else on_complete(null);
						
					});
				}
				else
				{
					CommonDAO.deleteEventsData(inclusion, (err, result) => {

						if (err) on_complete("Error deleting Events Data from DB: " + err);
						else on_complete(null);
						
					});
				}
				
			}
			else
			{
				on_complete(null);
			}
		});


	}	


	/** 
 	* @function 'sendSTLCParameters' - Manda gli i parametri, recuperati dalla tabella stlc_parameters, al server centrale
 	* @param {callback} on_complete - function(err)
 	*/
	 var _sendSTLCParameters = (on_complete) => {

		CommonDAO.getSTLCData((err, result) => {

			if (err) {
				on_complete("Error retrieving STLC Parameters Data from DB");
				return;
			}

			result.msgInfo = {
				mac: config.machineMac,
				SrvId: _srvID,
				externalIP: config.MachineExternalIP,
				msgType: "ReplaceSTLCParameters"				
			};

			//console.log(JSON.stringify(result));
			broker.publish('PUBLISHER_STLC_PARAM', result);
			
			on_complete(null);

		});

	}	


	/** 
 	* @function 'syncronizeTime' - Funzione che chiede l'ora del sistema centrale, per sincronizzarsi. L'agent è anche subscriber di un metodo SUBSCRIBER_GET_TIME dove viene restituita l'ora che serve per l'aggiornamento
 	* @param {callback} on_complete - function(err)
 	*/
	 var _syncronizeTime = (on_complete) => {

		logger.log('info','Agent: syncronizeTime - ask for time');

		var toSend= new Object();
		toSend.msgInfo = {
			mac: config.machineMac,
			SrvId: _srvID,
			msgType: "AskTime"				
		};
		console.log(JSON.stringify(toSend));

		broker.publish('PUBLISHER_ASK_TIME',toSend);

		on_complete(null);

	}	

	 var _testTime = () => {

		console.log('Agent: testTime - send time message');

		var toSend= new Object();
		toSend = {
			SrvId: _srvID,
			msgType: "GetTime",
			date: "05-09-2017",
			time: "12:14:30"			
		};

		//broker.publish('PUBLISHER_GET_TIME',toSend);
		_setTime (toSend);
	}	
	

	var _setTime = (timeObj) => {
		let dateCmd= `cmd /c date `+timeObj.date;
		let timeCmd= `cmd /c time `+timeObj.time;

		child.exec(dateCmd, (error, stdout, stderr) => {
			if (error) {
				console.log(error)
			} else {
				child.exec(timeCmd, (error, stdout, stderr) => {
					if (error) {
						console.log(error)
					} else {
						console.log(stdout) 
					}
				});
			}
		});


	}

	var _isAlive = () => {

		console.log('Agent: send isAlive');

		var toSend= new Object();
		toSend = {
			mac: config.machineMac,
			SrvId: _srvID,
			externalIP: config.MachineExternalIP,
			msgType: "isAlive"
		};

		broker.publish('PUBLISHER_IS_ALIVE',toSend);
	}	

	return {
		initialize:_initialize,
		clearCache:_clearCache,
		sendConfigToCentral:_sendConfigToCentral,
		sendDeviceStatusToCentral:_sendDeviceStatusToCentral,
		sendAcks:_sendAcks,
		sendIsAlive:_sendIsAlive,
		sendEvents:_sendEvents,
		sendSTLCParameters:_sendSTLCParameters,
		syncronizeTime:_syncronizeTime,
		testTime:_testTime,
		isAlive:_isAlive

		//send_delta_conf_to_central:_send_delta_conf_to_central
	}


})();
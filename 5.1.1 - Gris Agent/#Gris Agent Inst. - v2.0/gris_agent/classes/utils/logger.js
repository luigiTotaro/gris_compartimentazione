"use strict";

const config = require('../../config.js');
const winston = require('winston'); 

module.exports = (() => {

	winston.add(winston.transports.File, { 
		filename: config.logfile.file
	});

	const logger = new (winston.Logger)({
		
		level: config.logfile.level,

		//format: format.simple(), 
		transports: [

			new winston.transports.File({ 
				tailable: true,
				timestamp: function() {
					var data = new Date();
					var mm = data.getMonth() + 1; // getMonth() is zero-based
					var dd = data.getDate();
				  
					var hh = data.getHours();
					var min = data.getMinutes();
					var ss = data.getSeconds();
					var ret = data.getFullYear() + "-" + (mm>9 ? '' : '0') + mm + "-" + (dd>9 ? '' : '0') + dd + " " + (hh>9 ? '' : '0') + hh + ":" + (min>9 ? '' : '0') + min + ":" +(ss>9 ? '' : '0') + ss;
					return ret;
					/*
					return [data.getFullYear(),
							(mm>9 ? '' : '0') + mm,
							(dd>9 ? '' : '0') + dd,
							(hh>9 ? '' : '0') + hh,
							(min>9 ? '' : '0') + min,
							(ss>9 ? '' : '0') + ss,
						   ].join('');
					*/

					
					//return new Date().toLocaleString(); 
				
				
				},
				json:false,
				prettyPrint:false,
				filename: config.logfile.file,
				maxsize: config.logfile.maxsize,
				maxFiles: config.logfile.maxfiles
			 

			})

		]

	});
	


	var _log = function(level, msg) {

		console.log(new Date().toString() + " - " + msg);

		logger.log(level || 'debug',msg);

	}

	return {
		log:_log
	}


})();
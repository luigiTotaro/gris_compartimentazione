const config = require('../../config.js');
const os = require('os');

module.exports = (() => {



	/** 
	* Il MAC address per la centralizzazione può essere forzato da configurazione
	* per permettere di centralizzare da due STLC differenti, sullo stesso server in centrale
	* In periferia i dati sulla tabella server e STLC Parameters (MAC2) restano quelli effettivi,
	* lo spoofing è effettivo solo in fase di centralizzazione
	* Tale valore è preso nell'ordine: file di configurazione -> Chiave di rgistro -> MAC address della scheda di rete.
	* MODIFICA AL COMPORTAMENTO INIZIALE. UN SOLO MAC ADDRESS, preso dalla scheda di rete
 	* @function 'getMac' - recupera l'indirizzo mac 
 	* @param {boolean} mainEth Indica di quale scheda deve prendere il mac
 	* @returns {string} 
 	*/
	var _getMac = function(ethName) {

		var ret="";
		var ni = os.networkInterfaces();
		var macAddresses=new Array();
		var nameobj=Object.keys(ni);
		var rightname="";
		for (var i = 0, len = nameobj.length; i < len; i++) {
			
			if(nameobj[i].toUpperCase()== ethName.toUpperCase())
			{
					
				var ethN= ni[nameobj[i]];
				for (var t=0;t<ethN.length;t++)
				{
					if (ethN[t].family=="IPv4") ret=ethN[t].mac;
				}
				break;
			}
			
		}
		return ret.replace(new RegExp(':', 'g'), '');

	}

	/** 
 	* @function 'getIP' - recupera l'indirizzo IP
 	* @param {string} mainEethNameth Indica di quale scheda deve prendere l'IP 
 	* @returns {string} 
 	*/
	var _getIP = function(ethName) {

		var ret="";
		var ni = os.networkInterfaces();
		var ipAddresses=new Array();
		var nameobj=Object.keys(ni);
		var rightname="";
		for (var i = 0, len = nameobj.length; i < len; i++) {
			
			if(nameobj[i].toUpperCase()== ethName.toUpperCase())
			{
				var ethN= ni[nameobj[i]];
				for (var t=0;t<ethN.length;t++)
				{
					if (ethN[t].family=="IPv4") ret=ethN[t].address;
				}
				break;
			}
			
		}
		return ret;
	}

	/** 
 	* @function 'getHostName' - recupera l'hostname del sistema
 	* @returns {string} 
 	*/
	var _getFullHostName = function() {
		
	
		var hostname=os.hostname();
		var fullhostname="";
		var execSync = require('child_process').execSync;
		var wmic_computersystem = (execSync('wmic computersystem get domain /VALUE').toString());
		var sub=wmic_computersystem.replace(/^\s*[\r\n]/gm,'');
		sub=sub.substring(sub.indexOf('=')+1,sub.lenght);
		if(sub.toUpperCase().includes('WORKGROUP')|| typeof sub == undefined||!sub.length > 0||!sub.includes('.'))
		fullhostname=hostname;
		else
		fullhostname=hostname+'.'+sub;
		console.log(fullhostname);
		return fullhostname;
		
		
		// if (process.env.USERDNSDOMAIN==undefined) return os.hostname();
		// else return os.hostname()+"."+process.env.USERDNSDOMAIN;
		// //return os.hostname();
	}	

	return {
		getMac:_getMac,
		getIP:_getIP,
		getFullHostName:_getFullHostName
	}


})();
"use strict";
const db_conn = require('./db_connection.js');
const device_status = require('../beans/device_status.js');

module.exports = {

    /** 
    * @function 'getDeviceStatusList' - Restituisce una lista di raggruppamenti di tipo 'device_status'
    * @param {int} partition_size - dimensione massima di un raggruppamenti
 	* @param {callback} on_complete - function(err, lista)
 	*/	
    getDeviceStatusList: function(partition_size, on_complete) {

        db_conn.select(`SELECT CAST(DevID as varchar(max)) DevID, SevLevel, Description, Offline, AckFlag, AckDate, ShouldSendNotificationByEmail 
        FROM device_status ORDER BY DevID`, (err, result) => {

            let partitions = [];

            if(err) {
                
                on_complete(err, null);

            } else {

                result.forEach(row => {

                    if(partitions.length === 0 || Object.keys(partitions[partitions.length - 1]).length === partition_size) {
                        partitions.push([]);
                    }

                    let devId = "'" + row['DevID'] + "'";

                    partitions[partitions.length - 1][devId] = 
                        new device_status(
                            row['DevID'], 
                            row['SevLevel'], 
                            row['Description'], 
                            row['Offline'], 
                            row['AckFlag'], 
                            row['AckDate'], 
                            row['ShouldSendNotificationByEmail']
                        );

                });

                on_complete(null, partitions);
            }        	
    
        });

    },


    /** 
    * @function 'getDeviceStatusList' - riempie un gruppo di device_status con gli stream e gli stream fields associati
    * @param {array} grouping - array di oggetti device_status
 	* @param {callback} on_complete - function(err, lista)
 	*/
    fillDeviceStatusGroup: function(grouping, on_complete) {

        let device_id_list = [];
        let device_id_query = "";

        for(var DevID in grouping) {
            device_id_list.push(DevID);
        }

        device_id_query = device_id_list.join(",");

        db_conn.multiple_select([
            `SELECT CAST(DevID as varchar(max)) DevID, StrID, Name, Visible, Data, DateTime, SevLevel, Description, Processed FROM streams WHERE DevID IN (${device_id_query}) ORDER BY DevID, StrID`,
            `SELECT CAST(DevID as varchar(max)) DevID, StrID, FieldID, ArrayID, Name, SevLevel, Value, Description, Visible, ReferenceID, AckFlag, AckDate, IsTable, ShouldSendNotificationByEmail FROM stream_fields WHERE DevID IN (${device_id_query}) ORDER BY DevID, StrID, FieldID`
        ], (err, results) => {

            // streams
            results.recordsets[0].forEach(row => {

                let devId = "'" + row['DevID'] + "'";

                grouping[devId].add_stream(
                    row['StrID'],
                    row['Name'],
                    row['Visible'],
                    row['Data'],
                    row['DateTime'],
                    row['SevLevel'],
                    row['Description'],
                    row['Processed']
                );

            });

            // stream_fields
            results.recordsets[1].forEach(row => {

                let devId = "'" + row['DevID'] + "'";

                grouping[devId].add_stream_field(
                    row['StrID'],
                    row['FieldID'],
                    row['ArrayID'],
                    row['Name'],
                    row['SevLevel'],
                    row['Value'],
                    row['Description'],
                    row['Visible'],
                    row['ReferenceID'],
                    row['AckFlag'],
                    row['AckDate'],
                    row['IsTable'],
                    row['ShouldSendNotificationByEmail']
                );
            });


            on_complete();
		});	


        
    }


}
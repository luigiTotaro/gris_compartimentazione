"use strict";

const iScheduleCommand = require('../iScheduleCommand.js');
const agentService = require('../../services/agentService.js');
const logger = require('../../utils/logger.js');
const config = require('../../../config.js');

class ScheduleCommandSyncTime extends iScheduleCommand {

	constructor() {
	    super({
	    	topic: '/gamma/delta'
	    });
	}

	run() {
		var delayMax=config.schedulers["scheduler_synctime"].randomDelaySeconds;
		var delay=Math.floor(Math.random() * (delayMax +1));
		logger.log('info', '--- Scheduler sync Time - Extra Delay (seconds): ' + delay);

		setTimeout(function() {
			agentService.syncTime((err) => {
				if (err)
				{
					logger.log('error',err);
				}
			});
		}, delay*1000);

/*
		logger.log('info', '--- Scheduler sync Time');
		
		agentService.syncTime((err) => {
			if (err)
			{
				logger.log('error',err);
			}
		});
*/			
	}

}

module.exports = ScheduleCommandSyncTime;
"use strict";

const iScheduleCommand = require('../iScheduleCommand.js');
const agentService = require('../../services/agentService.js');
const logger = require('../../utils/logger.js');
const config = require('../../../config.js');


class ScheduleCommandIsAlive extends iScheduleCommand {

	constructor() {
	    super({
	    	topic: '/isAlive'
	    });
	}

	run() {
		var delayMax=config.schedulers["scheduler_isAlive"].randomDelaySeconds;
		var delay=Math.floor(Math.random() * (delayMax +1));
		logger.log('info', '--- Scheduler isAlive - Extra Delay (seconds): ' + delay);

		setTimeout(function() {
			agentService.isAlive((err) => {
				if (err)
				{
					logger.log('error',err);
				}
			});
		}, delay*1000);


/*
		logger.log('info', '--- Scheduler isAlive');
		
		agentService.isAlive((err) => {
			if (err)
			{
				logger.log('error',err);
			}
		});
*/			
	}

}

module.exports = ScheduleCommandIsAlive;
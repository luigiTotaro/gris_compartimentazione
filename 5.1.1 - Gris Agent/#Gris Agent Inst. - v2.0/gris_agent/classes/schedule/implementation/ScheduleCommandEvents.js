"use strict";

const iScheduleCommand = require('../iScheduleCommand.js');
const agent = require('../../helpers/agent.js');
const agentService = require('../../services/agentService.js');
const logger = require('../../utils/logger.js');
const config = require('../../../config.js');

class ScheduleCommandEvents extends iScheduleCommand {

	constructor() {
	    super({
	    	topic: '/gamma/delta'
	    });
	}

	run() {
		var delayMax=config.schedulers["scheduler_events"].randomDelaySeconds;
		var delay=Math.floor(Math.random() * (delayMax +1));
		logger.log('info', '--- Scheduler Events - Extra Delay (seconds): ' + delay);

		setTimeout(function() {
			agentService.sendEventsToCentral((err) => {
				if (err)
				{
					logger.log('error',err);
				}
			});	
		}, delay*1000);

/*
		logger.log('info', '--- Scheduler Events');
		
		agentService.sendEventsToCentral((err) => {
			if (err)
			{
				logger.log('error',err);
			}
		});	
*/
	}

}

module.exports = ScheduleCommandEvents;
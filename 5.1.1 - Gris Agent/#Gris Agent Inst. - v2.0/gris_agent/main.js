'use strict';
var path = require('path');
const broker = require('./classes/broker/broker.js');
const db_conn = require('./classes/database/db_connection.js');
const config = require('./config.js');
const logger = require('./classes/utils/logger.js');

const url = require('url');
//const customHttpClient = require('./classes/utils/CustomHttpClient.js');
var CryptoJS = require("crypto-js");
const restInterface = require('./classes/services/restInterface.js');
const scheduler = require('./classes/schedule/scheduler.js');
const agentService = require('./classes/services/agentService.js');
const commandBuilder = require('./classes/CommandBuilder/CommandBuilder.js');
var windows = require("windows");

/**/
//logger.log('debug', 'owijsiwosjiow');

/**
 * main module. entry point per il servizio agent
 * avviare con: node main.js 
 * @module main
 */

global.firstStart = true;
global.updateRegistryKey=true; //normalmente la deve aggiornare, sa cambiano i dati di connessione o è troppo che è offline non la aggiorna più, per farsi resettare
global.reconnectRetryNumber=0;
global.lastAckSent=null; //verrà valorizzato al primo ack mandato
global.lastAckMessageSentToCheck=""; //per controllare l'eventuale invio di 2 messaggi ack identici

restInterface.init();
 
console.log("This is pid " + process.pid);
setInterval(function() {

	console.log("LastAutoCheck");
	if (global.updateRegistryKey)
	{
		var v = windows.registry(config.registryFolder);
		var timestamp=Number((new Date()).getTime());
		timestamp=Math.floor(timestamp/1000);
		var test = v.add(config.registryKey, timestamp);
		logger.log('info',"Aggiornato LastAutoCheck: " + timestamp + " - " + test);
	}
}, config.registryTimeUpdate*60*1000);


db_conn.connect((err) => {

	if(err) {
		console.log(err);
		logger.log('error','Connessione al DB fallita - Errore:'+ JSON.stringify(err));
		setTimeout(function() {
			process.exit();
		}, 2000); //devo dare il tempo di scrivere il log
		
	}
	else
	{
		if (config.testCommandControl)
		{
			setTimeout(function() {
				commandBuilder.processa(config.testCommandControlInfo);
			}, 2000); //devo dare il tempo di instnziare le classi dei comandi
		}
		else
		{			
			//prima di fare la connessione al broker, e instanziare publisher e subscribers, mi devo recuperare il nodID di questo agent
			db_conn.getNodeId((err, result) => {
						
				if(err)
				{
					//non ha un nodeId, vado comunque avanti
				}
				else
				{
					if (result==null) //non è riuscito a recuperare il nodID
					{}
					else config.NodID = result;
				}
		
				//per debug
				//config.NodID = "7387422851074"
		
				//continuo
				logger.log('info','il Node ID di questo agent è:'+config.NodID);
				preBrokerConnect();
		
			}); 
		}
	}
	
});




function preBrokerConnect()
{
	if (!config.getBrokerInfoFromServer)
	{
		//si connetterà con i dati di default
		logger.log('info',"Utilizzo i dati in config per la connessione al broker");
		brokerConnect();		
	}
	else
	{
		logger.log('info',"Utilizzo i dati da un servizio REST per la connessione al broker");
		config.connectionRetryNumber++;
		db_conn.getRegionId((err, result) => {
				
			if(err)
			{
				//errore, riprovo
				logger.log('warn',"Errore durante il recupero del RegionID: " + err);

				var waitTime=getTimeByRetryNumber(config.connectionRetryNumber);
				logger.log('info','Riprovo la connessione tra '+ waitTime +' secondi');
				setTimeout(function() {
					preBrokerConnect();	
				}, waitTime*1000);
			}
			else
			{
				if (result==null) //non è riuscito a recuperare il regionId
				{
					//errore, riprovo
					logger.log('warn',"Errore - Il RegionID recuperato dal DB è NULL");

					var waitTime=getTimeByRetryNumber(config.connectionRetryNumber);
					logger.log('info','Riprovo la connessione tra '+ waitTime +' secondi');
					setTimeout(function() {
						preBrokerConnect();	
					}, waitTime*1000);
	
				}
				else
				{

					var rootCas = require('ssl-root-cas/latest').create();
					rootCas.addFile(__dirname +  '/certs/Chain_RFISUBCA01PRO_Win.pem');
					const https = require("https");
					https.globalAgent.options.ca = rootCas;


					let regId=result; //normalmente recuperato dal db dalla tabella regions
					let myurl=config.brokerInfoAddress+"?regId="+regId;
					let options = {
						hostname: url.parse(myurl).hostname,
						path: url.parse(myurl).path,
						rejectUnauthorized: true,
					  };

					  //mi memorizzo le opzioni di connessione al servizio rest
					  //per utilizzarle in seguito per il controllo se il broker è sempre attuale
					  global.restConnectionOption=options;
					
					  let request = https.get(options, function (response) {
						// data is streamed in chunks from the server
						// so we have to handle the "data" event    
						let buffer = ""; 
					
						response.on("data", function (chunk) {
							buffer += chunk;
						}); 
					
						response.on("end", function (err) {
							if (err)
							{
								//errore, riprovo
								logger.log('warn',"Errore alla fine della chiamata al servizio REST per il recupero dei dati di connessione al Broker: " + err);

								var waitTime=getTimeByRetryNumber(config.connectionRetryNumber);
								logger.log('info','Riprovo la connessione tra '+ waitTime +' secondi');
								setTimeout(function() {
									preBrokerConnect();	
								}, waitTime*1000);

							}
							else
							{
								// finished transferring data
								let data = JSON.parse(buffer);
								//console.log(data);
								var password = data.password;
				
								//Creating the Vector Key
								var iv = CryptoJS.enc.Hex.parse('e84ad660c4721ae0e84ad660c4721ae0');
								//Encoding the Password in from UTF8 to byte array
								var Pass = CryptoJS.enc.Utf8.parse('chiavediprova');
								//Encoding the Salt in from UTF8 to byte array
								var Salt = CryptoJS.enc.Utf8.parse("insight123resultxyz");
								//Creating the key in PBKDF2 format to be used during the decryption
								var key128Bits1000Iterations = CryptoJS.PBKDF2(Pass.toString(CryptoJS.enc.Utf8), Salt, { keySize: 128 / 32, iterations: 1000 });
								//Enclosing the test to be decrypted in a CipherParams object as supported by the CryptoJS libarary
								var cipherParams = CryptoJS.lib.CipherParams.create({
									ciphertext: CryptoJS.enc.Base64.parse(password)
								});
				
								//Decrypting the string contained in cipherParams using the PBKDF2 key
								var decrypted = CryptoJS.AES.decrypt(cipherParams, key128Bits1000Iterations, { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
								var passDecrypt = decrypted.toString(CryptoJS.enc.Utf8);
				
								//console.log(passDecrypt);
								console.log("Broker IP: " + data.brokerIp);
								console.log("Broker Username: " + data.username);
								console.log("Broker Password: " +passDecrypt);
								console.log("Broker Port: " + data.port);
								//sostituisco i dati a quelli di default
								config.broker_srv_opts.host = data.brokerIp;
								config.broker_srv_opts.username = data.username;
								config.broker_srv_opts.password = passDecrypt;
								config.broker_srv_opts.port = data.port;
								config.MachineExternalIP=data.requestIp;
								brokerConnect();
							}
						}); 
					}).on("error", function (e) {
						console.log("Got error: " + e.message);
						//errore, riprovo
						logger.log('warn',"Errore durante la chiamata al servizio REST per il recupero dei dati di connessione al Broker: " + e.message);

						var waitTime=getTimeByRetryNumber(config.connectionRetryNumber);
						logger.log('info','Riprovo la connessione tra '+ waitTime +' secondi');
						setTimeout(function() {
							preBrokerConnect();	
						}, waitTime*1000);
		
					});

					request.setTimeout(15000, function( ) {
						logger.log('warn',"Timeout durante la chiamata al servizio REST per il recupero dei dati di connessione al Broker, chiudo il socket di comunicazione");
						request.abort( );
					});
				
				}
			}

		}); 
	}
}

function getTimeByRetryNumber(retry)
{
	if (retry==1) return 10;
	else if (retry==2) return 30;
	else if (retry==3) return 60;
	else return 300;
}


function brokerConnect()
{
	setTimeout(function() {
		config.ignoreReceivedCommand=false;
		logger.log('info','Ricomincio a considerare i comandi in entrata');
	}, 20000);

	logger.log('info','Connessione al Broker - Broker IP: '+config.broker_srv_opts.host+ " - Broker Port: " + config.broker_srv_opts.port + " - Broker Username: " + config.broker_srv_opts.username);
	broker.connect({
		on_connect: () => {
			console.log("Connesso");
			
			if (!config.overrideBrokerConnection) //non ha fatto l'init, lo devo fare
			{
				console.log("Init");
				init();

				//faccio partire anche il timer che ogni tot minuti controlla se i parametri di connessione sono sepre gli stessi
				if (config.getBrokerInfoFromServer)
				{
					global.timerUpdateBrokerInfo = setInterval(function() {
						checkBrokerConnectionData();
					
					}, config.updateBrokerInfo*60*1000);
				}
			

			}

		},
		on_offline: () => {
			console.log("Offline");
			
			//per sicurezza controllo i parametri del broker
			checkBrokerConnectionData();
			
			/*
			if (!config.overrideBrokerConnection) //non ha fatto l'init, lo devo fare
			{
				init(); 
			}
			*/
		}
	});

	if (config.overrideBrokerConnection)
	{
		init(); //devo fare comunque la connessione al broker fatta prima, per instanzioare i publisher e i subscribers
	}
}

function checkBrokerConnectionData()
{
	logger.log('info',"Controllo i dati di connessione al Broker");

	var rootCas = require('ssl-root-cas/latest').create();
	rootCas.addFile(__dirname +  '/certs/Chain_RFISUBCA01PRO_Win.pem');
	const https = require("https");
	https.globalAgent.options.ca = rootCas;


	let request = https.get(global.restConnectionOption, function (response) {
	// data is streamed in chunks from the server
	// so we have to handle the "data" event    
	let buffer = ""; 

	response.on("data", function (chunk) {
		buffer += chunk;
	}); 

	response.on("end", function (err) {
		if (err)
		{
			//errore, riprovo
			logger.log('warn',"Errore alla fine della chiamata al servizio REST per il controllo dei dati di connessione al Broker: " + err);
		}
		else
		{
			// finished transferring data
			let data = JSON.parse(buffer);
			//console.log(data);
			var password = data.password;

			//Creating the Vector Key
			var iv = CryptoJS.enc.Hex.parse('e84ad660c4721ae0e84ad660c4721ae0');
			//Encoding the Password in from UTF8 to byte array
			var Pass = CryptoJS.enc.Utf8.parse('chiavediprova');
			//Encoding the Salt in from UTF8 to byte array
			var Salt = CryptoJS.enc.Utf8.parse("insight123resultxyz");
			//Creating the key in PBKDF2 format to be used during the decryption
			var key128Bits1000Iterations = CryptoJS.PBKDF2(Pass.toString(CryptoJS.enc.Utf8), Salt, { keySize: 128 / 32, iterations: 1000 });
			//Enclosing the test to be decrypted in a CipherParams object as supported by the CryptoJS libarary
			var cipherParams = CryptoJS.lib.CipherParams.create({
				ciphertext: CryptoJS.enc.Base64.parse(password)
			});

			//Decrypting the string contained in cipherParams using the PBKDF2 key
			var decrypted = CryptoJS.AES.decrypt(cipherParams, key128Bits1000Iterations, { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
			var passDecrypt = decrypted.toString(CryptoJS.enc.Utf8);

			//console.log(passDecrypt);
			console.log("Broker IP controllato: " + data.brokerIp);
			console.log("Broker Username controllato: " + data.username);
			console.log("Broker Password controllato: " +passDecrypt);
			console.log("Broker Port controllato: " + data.port);
			//sostituisco i dati a quelli di default
			if ((config.broker_srv_opts.host != data.brokerIp) ||
				(config.broker_srv_opts.username != data.username) ||
				(config.broker_srv_opts.password != passDecrypt) ||
				(config.broker_srv_opts.port != data.port))
			{
				//qualche dato è cambiato
				logger.log('warn',"Qualche dato dal servizio REST per la connessione è cambiato, non aggiorno più la chiave del registro LastAutoCheck");
				//non aggiorno più la chiave sul registro
				global.updateRegistryKey=false;
				//stoppo il timer
				clearInterval(global.timerUpdateBrokerInfo);	
			}
			
		}
	}); 
	}).on("error", function (e) {
		console.log("Got error: " + e.message);
		//errore, riprovo
		logger.log('warn',"Errore durante la chiamata al servizio REST per il controllo dei dati di connessione al Broker: " + e.message);

	});


}


function init()
{
	//poi sarà fatto dall'onconnect del broker
	agentService.initialize((err) => {
        	
		if (err) {
			logger.log('error',err);
			process.exit();
		} else {
			if (!config.testMode)
			{
				//esecuzione standard
				scheduler.run("scheduler_devicestatus");
				scheduler.run("scheduler_events");
				scheduler.run("scheduler_synctime");
				scheduler.run("scheduler_isAlive");
			}
			else
			{
				
				
				
				
				//simulo il primo timer immediatamente
				agentService.sendDeviceStatusToCentral((err) => {
					if (err)
					{
						logger.log('error',err);
					}
				});
				//simulo un altro tick del timer dopo 1 minuto
				setInterval(function() {
					agentService.sendDeviceStatusToCentral((err) => {
						if (err)
						{
							logger.log('error',err);
						}
					});
				}, 60000);
				//simulo il secondo timer dopo 2 minuti
				setTimeout(function() {
					agentService.sendEventsToCentral((err) => {
						if (err)
						{
							logger.log('error',err);
						}
					});	
				}, 90000);
				//simulo il terzo timer dopo 2 minuti e 10 secondi
				setTimeout(function() {
					agentService.syncTime((err) => {
						if (err)
						{
							logger.log('error',err);
						}
					});	
				}, 100000);
				

				//TEST set orario
				//agentService.testTime();

				//test isAlive
				scheduler.run("scheduler_isAlive");


			}

			
			

		}

	}); 

}
	




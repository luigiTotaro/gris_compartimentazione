USE [Telefin]
GO

/****** Object:  StoredProcedure [dbo].[tf_GetEventsToSend]    Script Date: 31/10/2019 15:36:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Stored procedure
ALTER PROCEDURE [dbo].[tf_GetEventsToSend]
AS
SET NOCOUNT ON;
SELECT TOP (1000) EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory
FROM events WHERE MQTTProcessed=1
ORDER BY Created
GO




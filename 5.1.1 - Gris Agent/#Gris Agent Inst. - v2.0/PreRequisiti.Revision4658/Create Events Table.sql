USE [Telefin]
GO

ALTER TABLE [dbo].[events] DROP CONSTRAINT [FK_events_devices]
GO

/****** Object:  Table [dbo].[events]    Script Date: 31/10/2019 15:33:26 ******/
DROP TABLE [dbo].[events]
GO

/****** Object:  Table [dbo].[events]    Script Date: 31/10/2019 15:33:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[events](
	[EventID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_events_EventID]  DEFAULT (newid()),
	[DevID] [bigint] NOT NULL,
	[EventData] [varbinary](max) NOT NULL,
	[Created] [datetime] NULL CONSTRAINT [DF_events_Created]  DEFAULT (getdate()),
	[Requested] [datetime] NOT NULL CONSTRAINT [DF_events_Requested]  DEFAULT (getdate()),
	[ToBeDeleted] [bit] NOT NULL CONSTRAINT [DF_events_ToBeDeleted]  DEFAULT ((0)),
	[EventCategory] [tinyint] NOT NULL,
	[MQTTProcessed] [bit] NULL CONSTRAINT [DF_events_MQTTProcessed]  DEFAULT ((0)),
 CONSTRAINT [PK_events] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[events]  WITH CHECK ADD  CONSTRAINT [FK_events_devices] FOREIGN KEY([DevID])
REFERENCES [dbo].[devices] ([DevID])
GO

ALTER TABLE [dbo].[events] CHECK CONSTRAINT [FK_events_devices]
GO




USE [master]
GO
CREATE LOGIN [mqttagentuser] WITH PASSWORD=N'12345', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
USE [Telefin]
GO
CREATE USER [mqttagentuser] FOR LOGIN [mqttagentuser]
GO
ALTER ROLE [db_datareader] ADD MEMBER [mqttagentuser]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [mqttagentuser]
GO

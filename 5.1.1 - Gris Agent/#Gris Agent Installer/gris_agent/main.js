'use strict';
const broker = require('./classes/broker/broker.js');
const db_conn = require('./classes/database/db_connection.js');
const config = require('./config.js');
const logger = require('./classes/utils/logger.js');
const http = require("http");
//const customHttpClient = require('./classes/utils/CustomHttpClient.js');
var CryptoJS = require("crypto-js");
const restInterface = require('./classes/services/restInterface.js');
const scheduler = require('./classes/schedule/scheduler.js');
const agentService = require('./classes/services/agentService.js');

/**/
//logger.log('debug', 'owijsiwosjiow');

/**
 * main module. entry point per il servizio agent
 * avviare con: node main.js 
 * @module main
 */

global.firstStart = true;

restInterface.init();
 
db_conn.connect((err) => {

    if(err) {
        console.log(err);
        process.exit();
    }

	if (!config.getBrokerInfoFromServer)
	{
		//si connetterà con i dati di default
		brokerConnect();		
	}
	else
	{
		db_conn.getRegionId((err, result) => {
				
			if(err)
			{
				//si connetterà con i dati di default
				brokerConnect();	
			}
			else
			{
				if (result==null) //non è riuscito a recuperare il regionId
				{
					//si connetterà con i dati di default
					brokerConnect();	
				}
				else
				{
					let regId=result; //normalmente recuperato dal db dalla tabella regions
					let url=config.brokerInfoAddress+"?regId="+regId;
					let request = http.get(url, function (response) {
						// data is streamed in chunks from the server
						// so we have to handle the "data" event    
						let buffer = ""; 
					
						response.on("data", function (chunk) {
							buffer += chunk;
						}); 
					
						response.on("end", function (err) {
							if (err)
							{
								//si connetterà con i dati di default
								brokerConnect();
							}
							else
							{
								// finished transferring data
								let data = JSON.parse(buffer);
								//console.log(data);
								var password = data.password;
				
								//Creating the Vector Key
								var iv = CryptoJS.enc.Hex.parse('e84ad660c4721ae0e84ad660c4721ae0');
								//Encoding the Password in from UTF8 to byte array
								var Pass = CryptoJS.enc.Utf8.parse('chiavediprova');
								//Encoding the Salt in from UTF8 to byte array
								var Salt = CryptoJS.enc.Utf8.parse("insight123resultxyz");
								//Creating the key in PBKDF2 format to be used during the decryption
								var key128Bits1000Iterations = CryptoJS.PBKDF2(Pass.toString(CryptoJS.enc.Utf8), Salt, { keySize: 128 / 32, iterations: 1000 });
								//Enclosing the test to be decrypted in a CipherParams object as supported by the CryptoJS libarary
								var cipherParams = CryptoJS.lib.CipherParams.create({
									ciphertext: CryptoJS.enc.Base64.parse(password)
								});
				
								//Decrypting the string contained in cipherParams using the PBKDF2 key
								var decrypted = CryptoJS.AES.decrypt(cipherParams, key128Bits1000Iterations, { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
								var passDecrypt = decrypted.toString(CryptoJS.enc.Utf8);
				
								//console.log(passDecrypt);
								console.log("Broker IP: " + data.brokerIp);
								console.log("Broker Username: " + data.username);
								console.log("Broker Password: " +passDecrypt);
								console.log("Broker Port: " + data.port);
								//sostituisco i dati a quelli di default
								config.broker_srv_opts.host = data.brokerIp;
								config.broker_srv_opts.username = data.username;
								config.broker_srv_opts.password = passDecrypt;
								config.broker_srv_opts.port = data.port;
								config.MachineExternalIP=data.requestIp;
								brokerConnect();
							}
						}); 
					}).on("error", function (e) {
						console.log("Got error: " + e.message);
						//si connetterà con i dati di default
						brokerConnect();						
					});
				
				}
			}

		}); 
	}
});



function brokerConnect()
{
	
	logger.log('info','Connessione al Broker - Broker IP: '+config.broker_srv_opts.host+ " - Broker Port: " + config.broker_srv_opts.port + " - Broker Username: " + config.broker_srv_opts.username);
	broker.connect({
		on_connect: () => {
			console.log("Connesso");
			
			if (!config.overrideBrokerConnection) //non ha fatto l'init, lo devo fare
			{
				init(); 
			}

		},
		on_offline: () => {
			console.log("Offline");

			if (!config.overrideBrokerConnection) //non ha fatto l'init, lo devo fare
			{
				init(); 
			}
		}
	});

	if (config.overrideBrokerConnection)
	{
		init(); //devo fare comunque la connessione al broker fatta prima, per instanzioare i publisher e i subscribers
	}
}


function init()
{
	//poi sarà fatto dall'onconnect del broker
	agentService.initialize((err) => {
        	
		if (err) {
			logger.log('error',err);
			process.exit();
		} else {
			if (!config.testMode)
			{
				//esecuzione standard
				scheduler.run("scheduler_devicestatus");
				scheduler.run("scheduler_events");
				scheduler.run("scheduler_synctime");
				scheduler.run("scheduler_isAlive");
			}
			else
			{
				
				//simulo il primo timer immediatamente
				agentService.sendDeviceStatusToCentral((err) => {
					if (err)
					{
						logger.log('error',err);
					}
				});
				//simulo un altro tick del timer dopo 1 minuto
				setInterval(function() {
					agentService.sendDeviceStatusToCentral((err) => {
						if (err)
						{
							logger.log('error',err);
						}
					});
				}, 60000);
				//simulo il secondo timer dopo 2 minuti
				setTimeout(function() {
					agentService.sendEventsToCentral((err) => {
						if (err)
						{
							logger.log('error',err);
						}
					});	
				}, 90000);
				//simulo il terzo timer dopo 2 minuti e 10 secondi
				setTimeout(function() {
					agentService.syncTime((err) => {
						if (err)
						{
							logger.log('error',err);
						}
					});	
				}, 100000);
				

				//TEST set orario
				//agentService.testTime();

				//test isAlive
				scheduler.run("scheduler_isAlive");


			}

			
			

		}

	}); 

}
	




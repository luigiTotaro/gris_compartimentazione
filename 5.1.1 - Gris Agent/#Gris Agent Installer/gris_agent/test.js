'use strict';
const broker = require('./classes/broker/broker.js');
const db_conn = require('./classes/database/db_connection.js');
const logger = require('./classes/utils/logger.js');
 
const agent = require('./classes/helpers/agent.js');
const scheduler = require('./classes/schedule/scheduler.js');
const agentService = require('./classes/services/agentService.js');

/**/
//logger.log('debug', 'owijsiwosjiow');

/**
 * main module. entry point per il servizio agent
 * avviare con: node main.js 
 * @module main
 */

global.firstStart = true;


 
db_conn.connect((err) => {

    if(err) {
        console.log(err);
        process.exit();
    }

    agent.send_delta_conf_to_central(true, (err, delta) => {

        if(err) {
            console.log(err);
            return;
        }

        console.log(delta);
        
    });

 

});

 
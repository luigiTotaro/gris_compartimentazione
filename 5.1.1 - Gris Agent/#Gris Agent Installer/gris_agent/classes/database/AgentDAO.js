"use strict";
const db_conn = require('./db_connection.js');

module.exports = {

	/** 
 	* @function 'update_stlc_parameter_hw_profile' - aggiornamento tabella 'stlc_parameters'
 	* @param {string} parameter_value - valore del paramentro da modificare
 	* @param {string} parameter_name - nome del paramentro da modificare
 	* @param {callback} on_complete - function(err, result)
	 */	
	update_stlc_parameter: function(parameter_value, parameter_name, on_complete) {

		let update = `UPDATE stlc_parameters set ParameterValue='${parameter_value}' WHERE ParameterName='${parameter_name}'`;

		db_conn.update(update, (err, result) => {
			
	    	if(err) {
	    		on_complete(err, null);
	    	} else {
	    		on_complete(null, result);
	    	}

		}); 

	},

	/** 
 	* @function 'insert_stlc_parameter' - aggiornamento tabella 'stlc_parameters', per inserimento di un nuovo valore
 	* @param {string} parameter_value - valore del paramentro da aggiungere
 	* @param {string} parameter_name - nome del paramentro da aggiungere
 	* @param {string} description - descrizione del paramentro da aggiungere
 	* @param {callback} on_complete - function(err, result)
	 */	
	insert_stlc_parameter: function(parameter_value, parameter_name, description, on_complete) {

		let insert = `INSERT INTO stlc_parameters(ParameterName, ParameterValue, ParameterDescription) 
		VALUES ('${parameter_name}','${parameter_value}','${description}'`;

		db_conn.insert(insert, (err, result) => {
			
	    	if(err) {
	    		on_complete(err, null);
	    	} else {
	    		on_complete(null, result);
	    	}

		}); 
	},

	/** 
 	* @function 'update_servers_table' - aggiornamento tabella 'servers'
 	* @param {array} updateData - array di chiavi e valori da aggiornare
 	* @param {callback} on_complete - function(err, result)
	 */	
	update_servers_table: function(updateData, on_complete) {

		let fields_str = updateData.join(',');

		let update = `UPDATE servers set ${fields_str}`;

		db_conn.update(update, (err, result) => {
			
	    	if(err) {
	    		on_complete(err, null);
	    	} else {
	    		on_complete(null, result);
	    	}

		}); 
	}

}
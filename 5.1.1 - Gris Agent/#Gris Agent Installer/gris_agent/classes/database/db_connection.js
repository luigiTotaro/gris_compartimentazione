const config = require('../../config.js');
const sql = require('mssql');
//const sql = require('mssql/msnodesqlv8');
const child = require('child_process');

/**
 * Modulo per la gestione della connessione al database. instanzia un pool di connessioni ed espone le primitive
 * per le operazioni verso il database
 * @module classes/database
 */
module.exports = (() => {

	var pool1 = null;
	
	var _connect = (on_complete) => {
		
		var _create_pool = (on_complete) => {
			pool1 = new sql.ConnectionPool(config.database, err => { 
				on_complete(err);
			});
		}
		
		if(process.platform === 'win32' && config.database.start_local_sql_browser === true) {
			child.exec('net start "SQL Server Browser"', (error, stdout, stderr) => {
				_create_pool(on_complete);
			});
		} else {
			_create_pool(on_complete);
		}

	};

	/** 
 	* @function 'select' - esecuzione (semplice) di una query verso il db mssql
 	* @param {string} sql - select
 	* @param {callback} on_complete - function(err, result)
 	*/
	var _select = (sql, on_complete) => {

		var request = pool1.request();

	    request.query(sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result.recordset)
        	}

	    });
 
	}


	/**
	 * 
	 * @param {array} sql_list - lista di select
	 * @param {function} on_complete - function(err, result_list)
	 */
	var _multi_select = (sql_list, on_complete) => {

		if(!Array.isArray(sql_list)) {
			throw `Il parametro 'sql_list' non è un array`;
		}
		
		var request = pool1.request();
		request.multiple = true;

		request.query(sql_list.join('; '), (err, result_list) => {
			if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result_list);
        	}
		});

	}


	/** 
 	* @function 'insert' - esecuzione di una insert (semplice, senza transazione) verso il db mssql
 	* @param {string} insert_sql - insert
 	* @param {callback} on_complete - function(err, result)
 	*/
	var _insert = (insert_sql, on_complete) => {

		var request = pool1.request();

	    request.query(insert_sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result);
        	}

	    });

	}



	/** 
 	* @function 'delete' - esecuzione di una query di cancellazione (semplice senza transazione)
 	* @param {string} delete_sql - delete
 	* @param {callback} on_complete - function(err, result)
 	*/
 	
	var _delete = (delete_sql, on_complete) => {

		var request = pool1.request();

	    request.query(delete_sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result);
        	}

	    });

	}
	


	/** 
 	* @function 'update' - esecuzione di una query di aggiornamento (semplice senza transazione)
 	* @param {string} update_sql - update
 	* @param {callback} on_complete - function(err, result)
 	*/
	var _update = (update_sql, on_complete) => {

		var request = pool1.request();

	    request.query(update_sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result);
        	}

	    });

	}

	/** 
 	* @function 'getRegionId' - recupero della regionID dal database
 	* @param {callback} on_complete - function(err, result)
 	*/
	 var _getRegionId = (on_complete) => {

		var request = pool1.request();
		var sql = "SELECT TOP 1 RegId FROM regions"

	    request.query(sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
				if (result.recordset.length==0) on_complete(null, null)
				else on_complete(null, result.recordset[0].RegId)
        	}

	    });
 
	}	

	return {
		connect:_connect,
		select:_select,
		multiple_select: _multi_select,
		insert:_insert,
		delete:_delete,
		update:_update,
		getRegionId:_getRegionId
	}


})();
"use strict";
const config = require('../../config.js');
const logger = require('../utils/logger.js');
const http = require("http");
const querystring = require('querystring');

/**
 * Modulo per la gestione della connessione verso un servizio rest e invio dei dati
 * @module classes/services/restInterface
 */
module.exports = (() => {

	var queue = null; //code che contiene i messaggi da inviare
	var sendingInProgress=false;

 	/** 
 	* @function 'init' - 
 	*/
	 var _init = () => {
		queue = new Array();
	};

	var _addToQueue= (oggetto) => {
		queue.push(oggetto);
		logger.log('info', `Messages in queue to send to REST service: ${queue.length}`);
		if (!sendingInProgress) _processaQueue();
	};


	var _processaQueue = () => {

		logger.log('info', `Process Queue - Messages in queue to send to REST service: ${queue.length}`);
		if (queue.length==0)
		{
			sendingInProgress=false;
			return;
		}

		let oggettoToSend=queue[0];

		let messageStrEnc;
		if (oggettoToSend.isCompressed)
		{
			let messageStr = oggettoToSend.message.toString('binary');
			messageStrEnc = encodeURIComponent(messageStr);
			//let revert = new Buffer(messageStr, 'binary');
		}
		else
		{
			messageStrEnc = encodeURIComponent(JSON.stringify(oggettoToSend.message));
		}
		
		var data = querystring.stringify({
			data: messageStrEnc
		});
		
		let topic = oggettoToSend.topic.replace(/\//g, '_');
		//topic = "get_used_slots";
		const options = {
			hostname: config.restServiceAddress, //"localhost",
			port: config.restServicePort, //8916,
			path: "/"+topic, //"/post",
			method: 'POST',
			headers: {
				'Content-Type':  "application/x-www-form-urlencoded",  // "application/json" , 'multipart/form-data', "text/html",
				'Content-Length': Buffer.byteLength(data)
			}
		};
		
		var req = http.request(options, (res) => {
			//console.log(`**************************************************************`);
			logger.log('info', `Return from REST service for message ()`);
			console.log(`STATUS: ${res.statusCode}`);
			//console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
			res.setEncoding('utf8');
			res.on('data', (chunk) => {
				//console.log(`BODY: ${chunk}`);
			});
			res.on('end', () => {
				//console.log('No more data in response.')
				logger.log('info', `Message () sent correctly to REST service`);
				queue.splice(0, 1);
				_processaQueue();
			})
		});

		req.on('error', (e) => {
			//console.log(`**************************************************************`);
			console.log(`problem with request: ${e.message}`);
			logger.log('info', `Error sending Message () to REST service: ${e.message}`);
			queue.splice(0, 1);
			_processaQueue();
		});

		// write data to request body
		//console.log(`**************************************************************`);
		logger.log('info', `Send message (${topic}) to REST service`);
		sendingInProgress=true;
		req.write(data);
		req.end();

	};



	return {
		init:_init,
		addToQueue:_addToQueue
	}

})();



"use strict";
//solo per test di invio del messaggio... questo pubblisher dovrebbe stare solo sul server centrale
const iPublisher = require('../iPublisher.js');


class publisher_get_time extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_GET_TIME',
	    	topic: '/get_time'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_GET_TIME Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);		
	}
	

}

module.exports = publisher_get_time;
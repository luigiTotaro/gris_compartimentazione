"use strict";

const iPublisher = require('../iPublisher.js');


class publisher_acks extends iPublisher {

	constructor() {
	    super({
	    	key: 'PUBLISHER_ACKS',
	    	topic: '/acks'
	    });
	}

	publish(client, message) {

		console.log("PUBLISHER_ACKS Invio messaggio.....");
		super.publish(client, message);
		super.writeTestFile(message);
		super.sentToRestService(message, false);		
	}
	

}

module.exports = publisher_acks;
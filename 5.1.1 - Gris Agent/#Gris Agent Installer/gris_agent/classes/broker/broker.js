"use strict";
const config = require('../../config.js');
const mqtt = require('mqtt');
const fs = require("fs");
const path = require('path');
const iSubscriber = require('./subscribers/iSubscriber.js');
const iPublisher = require('./publishers/iPublisher.js');
const logger = require('../utils/logger.js');

/**
 * Modulo per la gestione della connessione e degli eventi di sottoscrizione/pubblicazione verso un broker
 * @module classes/broker
 */
module.exports = (() => {

	var _client = null;

	var _subscribers = [];
	var _publishers = [];


	var _register_subscribers = () => {
		
		// caricamento di tutti le classi presenti nel folder 'subscribers/implementation'
		const subscribers_class_path = path.normalize(path.join(__dirname, 'subscribers', 'implementation'));

		fs.readdirSync(subscribers_class_path).forEach(file => {

			try {

				let cl = require(path.join(subscribers_class_path, file));

				let instance = new cl();

				if(!(instance instanceof iSubscriber)) {
					throw 'la classe ' + file + ' deve essere una istanza di iSubscriber';
				}

				instance.register(_client);
				_subscribers[instance.get_topic()] = instance;
			
			} catch(e) {

				console.log(e, 'nel file', file);

			}

		});

	};



	var _register_publishers = () => {

		// caricamento di tutti le classi presenti nel folder 'publishers/implementation'
		const publishers_class_path = path.normalize(path.join(__dirname, 'publishers', 'implementation'));

		fs.readdirSync(publishers_class_path).forEach(file => {

			try {
				
				let cl = require(path.join(publishers_class_path, file));

				let instance = new cl();

				if(!(instance instanceof iPublisher)) {
					throw 'la classe ' + file + ' deve essere una istanza di iPublisher';
				}
				
				_publishers[instance.get_key()] = instance;

				/*
				instance.register(_client);
				
				*/
			
			} catch(e) {

				console.log(e, 'nel file', file);

			}

		});

	};

 
 	/** 
 	* @function 'connect' - connessione verso il broker. instanzia gli eventi tipici del 
	 * protocollo mqtt (error/connect/ecc...)
	 * @param {callback} events.on_connect - function chiamata UNA SOLA VOLTA appena sento la prima connessione al broker...
	 * @param {callback} events.on_offline - function chiamata UNA SOLA VOLTA appena sento il primo evento offline...
 	*/
	var _connect = (events) => {

		let has_run_first_connection_callback = false;
		let has_run_first_offline_callback = false;

		_client  = mqtt.connect(config.broker_srv_opts);

		_client.on('error', (err) => {
			logger.log('error', err);
		});

		_client.on('offline', () => {
			if(has_run_first_offline_callback === false) {
				has_run_first_offline_callback = true;
				if(events && events.on_offline) {
					events.on_offline();
				}			
			}
			logger.log('warn','broker offline');
		});

		_client.on('connect', () => {
			//console.log('connesso');
			if(has_run_first_connection_callback === false) {
				has_run_first_connection_callback = true;
				if(events && events.on_connect) {
					events.on_connect();
				}			
			}
			logger.log('info','broker connesso');
		});

		_client.on('reconnect', () => {
			logger.log('warn','riconnessione al broker');
		});

		_client.on('message', (topic, message) => {
			// delego la gestione dell'evento al metodo 'on_message' della
			// rispettiva classe (che estende da iSubscriber) caricata dinamicamente
			// all'avvio.
			_subscribers[topic].on_message(topic, message);
		});

		_register_subscribers();
		_register_publishers();
	};






	var _publish = (key, message) => {

  		if(!message) {
  			throw 'message vuoto';
  		}

  		if(!_publishers[key]) {
  			throw 'handler per il publish con chiave "' + key + '" mancante';
  		}

  		_publishers[key].publish(_client, message);

	};


	



	return {
		connect:_connect,
		publish:_publish
	}

})();



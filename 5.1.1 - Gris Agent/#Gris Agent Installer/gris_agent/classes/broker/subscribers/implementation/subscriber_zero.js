"use strict";

const iSubscriber = require('../iSubscriber.js');


class subscriber_zero extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/alpha/beta'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_zero event', topic, message.toString());
	}

}

module.exports = subscriber_zero;
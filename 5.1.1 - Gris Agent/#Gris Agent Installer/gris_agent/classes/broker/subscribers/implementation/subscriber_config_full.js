"use strict";

const iSubscriber = require('../iSubscriber.js');


class subscriber_config_full extends iSubscriber {

	constructor() {
	    super({
	    	topic: '/config/full'
	    });
	}

	on_message(topic, message) {
		console.log('subscriber_config_full event', topic, message.toString());
	}

}

module.exports = subscriber_config_full;
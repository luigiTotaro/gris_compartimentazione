"use strict";

const iScheduleCommand = require('../iScheduleCommand.js');
const agent = require('../../helpers/agent.js');
const agentService = require('../../services/agentService.js');
const logger = require('../../utils/logger.js');

class ScheduleCommandEvents extends iScheduleCommand {

	constructor() {
	    super({
	    	topic: '/gamma/delta'
	    });
	}

	run() {
		logger.log('info', '--- Scheduler Events');
		
		agentService.sendEventsToCentral((err) => {
			if (err)
			{
				logger.log('error',err);
			}
		});	

	}

}

module.exports = ScheduleCommandEvents;
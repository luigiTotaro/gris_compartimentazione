"use strict";
const config = require('../../config.js');
const fs = require("fs");
const path = require('path');
const iScheduleCommand = require('./iScheduleCommand.js');
const schedule = require('node-schedule');
const logger = require('../utils/logger.js');

/**
 * Modulo per la gestione delle operazioni schedulate su base temporale, secondo la convenzione di cron {@link https://www.npmjs.com/package/node-schedule}
 * la definizione delle classi da caricare ed instanziare deve essere fatta nel file config.js
 * possono essere definiti più comandi specificando la proprietà 'cron' (la quale indica l'intervallo temporale) e 
 * l'attributo 'class' che indica sia il nome del file che la classe da instanziare. Il file di implementazione deve risiedere
 * in ./implementation e la relativa classe deve avere come nome il nome del file stesso (esclusa estensione .js) e deve implementare
 * l'interfaccia iScheduleCommand
 * @module classes/scheduler
 */
module.exports = (() => {

	var _schedules = [];
	const schedulers_class_path = path.normalize(path.join(__dirname, 'implementation'));

	var _instantiate = function() {

		for(var scheduler_id in config.schedulers) {

			if(!config.schedulers[scheduler_id].cron) {
				throw 'parametro "cron" assente in "schedulers.' + scheduler_id + '"';
			}

			if(!config.schedulers[scheduler_id].class) {
				throw 'parametro "class" assente in "schedulers.' + scheduler_id + '"';
			}

			if(config.schedulers[scheduler_id].autostart==undefined) {
				throw 'parametro "autostart" assente in "schedulers.' + scheduler_id + '"';
			}

			if(config.schedulers[scheduler_id].autostart) 
			{			
				//_run(scheduler_id);
				
				let filename = config.schedulers[scheduler_id].class + '.js';
				let full_class_path = path.join(schedulers_class_path, filename);

				if(!fs.existsSync(full_class_path)) {
					throw 'file "' + full_class_path + '" non trovato per lo scheduler "schedulers.' + scheduler_id + '"';
				}

				let cl = require(full_class_path);

				let instance = new cl();

				if(!(instance instanceof iScheduleCommand)) {
					throw 'la classe ' + filename + ' deve essere una istanza di iSchedulerCommand';
				}

				_schedules[scheduler_id] = schedule.scheduleJob(config.schedulers[scheduler_id].cron, function() {

					try {

						instance.run();

					} catch(e) {
						
						console.log(e);
						logger.log('error', e);

					}
					
				});
				
			}

		}

	}

	_instantiate();


	let _run = function(scheduler_id) {

		let filename = config.schedulers[scheduler_id].class + '.js';
		let full_class_path = path.join(schedulers_class_path, filename);

		if(!fs.existsSync(full_class_path)) {
			throw 'file "' + full_class_path + '" non trovato per lo scheduler "schedulers.' + scheduler_id + '"';
		}

		let cl = require(full_class_path);

		let instance = new cl();

		if(!(instance instanceof iScheduleCommand)) {
			throw 'la classe ' + filename + ' deve essere una istanza di iSchedulerCommand';
		}

		_schedules[scheduler_id] = schedule.scheduleJob(config.schedulers[scheduler_id].cron, function() {

			try {

				instance.run();

			} catch(e) {
				
				console.log(e);
				logger.log('error', e);

			}
			
		});

  	};

	return {
		run:_run
	}

})();
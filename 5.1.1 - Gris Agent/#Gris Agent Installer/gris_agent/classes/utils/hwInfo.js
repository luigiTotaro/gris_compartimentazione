const config = require('../../config.js');
const os = require('os');

module.exports = (() => {



	/** 
	* Il MAC address per la centralizzazione può essere forzato da configurazione
	* per permettere di centralizzare da due STLC differenti, sullo stesso server in centrale
	* In periferia i dati sulla tabella server e STLC Parameters (MAC2) restano quelli effettivi,
	* lo spoofing è effettivo solo in fase di centralizzazione
	* Tale valore è preso nell'ordine: file di configurazione -> Chiave di rgistro -> MAC address della scheda di rete.
	* MODIFICA AL COMPORTAMENTO INIZIALE. UN SOLO MAC ADDRESS, preso dalla scheda di rete
 	* @function 'getMac' - recupera l'indirizzo mac 
 	* @param {boolean} mainEth Indica di quale scheda deve prendere il mac
 	* @returns {string} 
 	*/
	var _getMac = function(ethName) {

		var ret="";
		var ni = os.networkInterfaces();
		var macAddresses=new Array();
		if (ni.hasOwnProperty(ethName))
		{
			var ethN= ni[ethName];
			for (var t=0;t<ethN.length;t++)
			{
				if (ethN[t].family=="IPv4") ret=ethN[t].mac;
			}
		}
		return ret.replace(new RegExp(':', 'g'), '');

	}

	/** 
 	* @function 'getIP' - recupera l'indirizzo IP
 	* @param {string} mainEethNameth Indica di quale scheda deve prendere l'IP 
 	* @returns {string} 
 	*/
	var _getIP = function(ethName) {

		var ret="";
		var ni = os.networkInterfaces();
		var ipAddresses=new Array();
		if (ni.hasOwnProperty(ethName))
		{
			var ethN= ni[ethName];
			for (var t=0;t<ethN.length;t++)
			{
				if (ethN[t].family=="IPv4") ret=ethN[t].address;
			}
		}
		return ret;
	}

	/** 
 	* @function 'getHostName' - recupera l'hostname del sistema
 	* @returns {string} 
 	*/
	var _getFullHostName = function() {
		return os.hostname();
	}	

	return {
		getMac:_getMac,
		getIP:_getIP,
		getFullHostName:_getFullHostName
	}


})();
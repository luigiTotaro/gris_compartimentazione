﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.dsRulesTableAdapters;
using System.Text.RegularExpressions;

namespace GrisSuite.Data
{
    public partial class dsRules
    {
        public partial class RulesRow : DataRow
        {
            public SqlParameter[] RuleSQLParameters
            {
                get
                {
                    try
                    {
                        SqlParameter[] sqlParameters = new SqlParameter[] {};
                        if (!string.IsNullOrEmpty(this.RuleParameters))
                        {
                            string sParameters = this.RuleParameters.Trim();
                            if (sParameters.EndsWith(";"))
                            {
                                sParameters = sParameters.Substring(0, sParameters.Length - 1);
                            }

                            string[] aParameters = sParameters.Split(';');
                            if (aParameters.Length > 0)
                            {
                                sqlParameters = new SqlParameter[aParameters.Length];
                                for (int i = 0; i <= aParameters.Length - 1; i++)
                                {
                                    sqlParameters[i] = this.ParseParameter(aParameters[i]);
                                }
                            }
                        }
                        return sqlParameters;
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error on parse rule parameters " + this.RuleParameters + " Error:" + ex.Message);
                    }
                }
            }

            private SqlParameter ParseParameter(string sParamDef)
            {
                SqlParameter sqlParam = new SqlParameter();

                string sType = Regex.Match(sParamDef, "\\(.*\\)").Value.Replace("(", string.Empty).Replace(")", string.Empty);
                if (sType.Length == 0)
                {
                    sqlParam.SqlDbType = SqlDbType.VarChar;
                }
                else
                {
                    sqlParam.SqlDbType = (SqlDbType) Enum.Parse(typeof (SqlDbType), sType, true);
                }

                sParamDef = Regex.Replace(sParamDef, "\\(.*\\)", "");

                sqlParam.ParameterName = Regex.Match(sParamDef, ".*=").Value.Replace("=", string.Empty);
                sqlParam.Value = Regex.Match(sParamDef, "=.*").Value.Replace("=", string.Empty);
                return sqlParam;
            }
        }

        public static int Fill(dsRules rules)
        {
            dsRulesDataSetAdaper daRules = new dsRulesDataSetAdaper();
            return daRules.Fill(rules);
        }

        public static void ExecuteRules()
        {
            ExecuteRules(30);
        }

        public static void ExecuteRules(int commandTimeout)
        {
            dsRules ds = new dsRules();
            dsRulesDataSetAdaper da = new dsRulesDataSetAdaper();
            da.Fill(ds);

            using (SqlConnection cn = new SqlConnection(da.Connection.ConnectionString))
            {
                SqlCommand cmdRule = new SqlCommand();
                cmdRule.CommandType = CommandType.Text;
                cmdRule.Connection = cn;
                cmdRule.CommandTimeout = commandTimeout;

                foreach (RulesRow rule in ds.Rules.Rows)
                {
                    cn.Open();
                    cmdRule.CommandText = rule.RuleTemplatesRow.RuleTemplateExpression;
                    cmdRule.CommandType = rule.RuleTemplatesRow.RuleTemplateExpressionType == 1 ? CommandType.StoredProcedure : CommandType.Text;
                    cmdRule.Parameters.Clear();
                    cmdRule.Parameters.AddRange(rule.RuleSQLParameters);
                    cmdRule.ExecuteNonQuery();
                    cn.Close();
                }
            }
        }

        public static int Update(dsRules rules)
        {
            dsRulesDataSetAdaper daRules = new dsRulesDataSetAdaper();
            return daRules.Update(rules);
        }

        internal class dsRulesDataSetAdaper
        {
            private readonly RulesTableAdapter taRules;
            private readonly RuleTemplatesTableAdapter taRuleTemplates;

            internal SqlConnection Connection
            {
                get
                {
                    SqlConnection cn = this.taRules.Connection;
                    if (cn == null)
                    {
                        cn = this.taRuleTemplates.Connection;
                    }
                    return cn;
                }
                set
                {
                    this.taRules.Connection = value;
                    this.taRuleTemplates.Connection = value;
                }
            }

            internal SqlTransaction Transaction
            {
                get
                {
                    SqlTransaction tn = this.taRules.Transaction;
                    if (tn == null)
                    {
                        tn = this.taRuleTemplates.Transaction;
                    }
                    return tn;
                }
                set
                {
                    if (value != null)
                    {
                        this.Connection = value.Connection;
                    }
                    this.taRules.Transaction = value;
                    this.taRuleTemplates.Transaction = value;
                }
            }

            internal dsRulesDataSetAdaper()
            {
                this.taRules = new RulesTableAdapter();
                this.taRuleTemplates = new RuleTemplatesTableAdapter();

                this.Connection = this.taRules.Connection;
            }

            internal int Fill(dsRules rules)
            {
                int i = 0;
                i += this.taRules.Fill(rules.Rules);
                i += this.taRuleTemplates.Fill(rules.RuleTemplates);
                return i;
            }

            internal int Update(dsRules rules)
            {
                int i = 0;
                i += this.taRuleTemplates.Update(rules.RuleTemplates);
                i += this.taRules.Update(rules.Rules);
                return i;
            }
        }
    }
}

namespace GrisSuite.Data.dsRulesTableAdapters
{
    public partial class QueriesTableAdapter
    {
        public int CommandTimeout
        {
            get { return this.CommandCollection[0].CommandTimeout; }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
        }
    }
}
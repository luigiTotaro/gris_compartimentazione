﻿using System;
using System.Data;
using System.Text;

namespace GrisSuite.Data.Reports.HistoryStreamFieldsValueTableAdapters
{
    partial class HistoryValueTableAdapter
    {
        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }

        }

        public HistoryStreamFieldsValue.HistoryValueDataTable GetBigData(DateTime? DataInizio, DateTime? DataFine, string GiorniDellaSettimana, DateTime? OraInizio, DateTime? OraFine, string ListaSistemi, string Stazione, string Linea, string Compartimento, int? DurataMin, int? DurataMax, string ListaSevLevel, string ListaTipiCampi)
        {
            this.CommandTimeout = 60 * 5;
            return this.GetData(DataInizio, DataFine, GiorniDellaSettimana, OraInizio, OraFine, ListaSistemi, Stazione, Linea, Compartimento, DurataMin, DurataMax, ListaSevLevel, ListaTipiCampi);
        }

    }

    partial class HistoryValueHeaderTableAdapter
    {

        public HistoryStreamFieldsValue.HistoryValueHeaderDataTable GetDataFilter(string Stazione, string Linea, string Compartimento, string ListaSevLevel, string ListaTipiCampi, DateTime? DataInizio, DateTime? DataFine, DateTime? OraInizio, DateTime? OraFine, int DurataMin, string DurataMinUdm, int DurataMax, string DurataMaxUdm, string GiorniDellaSettimana, string ListaSistemi)
        {
            const string sep = " ";
            StringBuilder sb = new StringBuilder();


            if (!string.IsNullOrEmpty(Compartimento))
            {
                sb.AppendFormat("<b>Compartimento:</b>{0}{1}", Compartimento, sep);
            }
            if (!string.IsNullOrEmpty(Linea))
            {
                sb.AppendFormat("<b>Linea:</b>{0}{1}", Linea, sep);
            }
            if (!string.IsNullOrEmpty(Stazione))
            {
                sb.AppendFormat("<b>Stazione:</b>{0}{1}", Stazione, sep);
            }
            if (!string.IsNullOrEmpty(ListaSevLevel))
            {
                sb.AppendFormat("<b>Stati:</b>{0}{1}", ListaSevLevel, sep);
            }
            if (!string.IsNullOrEmpty(ListaTipiCampi))
            {
                sb.AppendFormat("<b>Tipi Valore:</b>{0}{1}", ListaTipiCampi, sep);
            }
            if (DataInizio != null && DataFine != null)
            {
                sb.AppendFormat("<b>Data da:</b>{0:dd/MM/yyyy} <b>a:</b>{1:dd/MM/yyyy}{2}", DataInizio, DataFine, sep);
            }
            if (DataInizio != null && DataFine == null)
            {
                sb.AppendFormat("<b>Data da:</b>{0:dd/MM/yyyy}{1}", DataInizio, sep);
            }
            if (DataInizio == null && DataFine != null)
            {
                sb.AppendFormat("<b>Data fino a:</b>{0:dd/MM/yyyy}{1}", DataFine, sep);
            }
            if (OraInizio != null && OraFine != null)
            {
                sb.AppendFormat("<b>Fascia oraria dalle:</b>{0:HH:mm} <b>alle:</b>{1:HH:mm}{2}", OraInizio, OraFine, sep);
            }
            if (OraInizio != null && OraFine == null)
            {
                sb.AppendFormat("<b>Fascia oraria dalle:</b>{0:HH:mm} <b>alla mezzanotte</b>{1}", OraInizio, sep);
            }
            if (OraInizio == null && OraFine != null)
            {
                sb.AppendFormat("<b>Fascia oraria da mezzanotte fino alle:</b>{0:HH:mm}{1}", OraFine, sep);
            }
            if (DurataMin > 0 && DurataMax > 0)
            {
                sb.AppendFormat("<b>Durata da:</b> {0} {1} <b>fino a:</b> {2} {3}{4}", DurataMin, DurataMinUdm, DurataMax, DurataMaxUdm, sep);
            }
            if (DurataMin > 0 && DurataMax <= 0)
            {
                sb.AppendFormat("<b>Durata maggiore di:</b> {0} {1}{2}", DurataMin, DurataMinUdm, sep);
            }
            if (DurataMin <= 0 && DurataMax > 0)
            {
                sb.AppendFormat("<b>Durata minore di:</b> {0} {1}{2}", DurataMax, DurataMaxUdm, sep);
            }
            if (!string.IsNullOrEmpty(GiorniDellaSettimana))
            {
                sb.AppendFormat("<b>Giorni della settimana:</b> {0}{1}", GiorniDellaSettimana, sep);
            }
            if (!string.IsNullOrEmpty(ListaSistemi))
            {
                sb.AppendFormat("<b>Sistemi:</b> {0}{1}", ListaSistemi, sep);
            }

            sb.Insert(0,
                      sb.Length == 0
                          ? string.Format("<b>Estrazione del {0:dd/MM/yyyy HH:mm:ss}</b>", DateTime.Now)
                          : string.Format("<b>Estrazione del {0:dd/MM/yyyy HH:mm:ss}, Filtri impostati:</b>", DateTime.Now));

            HistoryStreamFieldsValue.HistoryValueHeaderDataTable t = new HistoryStreamFieldsValue.HistoryValueHeaderDataTable();
            t.AddHistoryValueHeaderRow(sb.ToString());

            return t;
        }
    }
}


namespace GrisSuite.Data.Reports {
    
    
    public partial class HistoryStreamFieldsValue {
        partial class HistoryValueDataTable
        {
        }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using GrisSuite.Data.dsCacheLogTableAdapters;

namespace GrisSuite.Data
{
    public partial class dsCacheLog
    {
        partial class StreamsDataTable
        {

        }

        partial class StreamFieldCasesDataTable
        {

        }

        partial class LogMessagesDataTable
        {

        }

        public void Update()
        {
            dsCacheLogDataSetAdaper dsa = new dsCacheLogDataSetAdaper();

            using (SqlConnection cn = dsa.Connection)
            {
                cn.Open();
                dsa.Transaction = cn.BeginTransaction(IsolationLevel.ReadCommitted);

                try
                {
                    dsa.Update(this);
                }
                catch
                {
                    if (dsa.Transaction != null)
                        dsa.Transaction.Rollback();
                    throw;
                }
                finally
                {
                    if (cn.State != ConnectionState.Closed)
                        cn.Close();
                }
            }
        }

    }

    public class dsCacheLogDataSetAdaper
    {

        private LogMessagesTableAdapter logMessageTableAdapter = new LogMessagesTableAdapter();
        private DeviceStatusTableAdapter deviceStatusTableAdapter = new DeviceStatusTableAdapter();
        private StreamsTableAdapter streamsTableAdapter = new StreamsTableAdapter();
        private ReferenceTableAdapter referenceTableAdapter = new ReferenceTableAdapter();
        private StreamFieldsTableAdapter streamFieldsTableAdapter = new StreamFieldsTableAdapter();
        private DevicesTableAdapter devicesTableAdapter = new DevicesTableAdapter();

        public dsCacheLogDataSetAdaper()
        {
            logMessageTableAdapter = new LogMessagesTableAdapter();
            deviceStatusTableAdapter = new DeviceStatusTableAdapter();
            streamsTableAdapter = new StreamsTableAdapter();
            referenceTableAdapter = new ReferenceTableAdapter();
            streamFieldsTableAdapter = new StreamFieldsTableAdapter();
            devicesTableAdapter = new DevicesTableAdapter();

            int updateBatchSize = Properties.Settings.Default.UpdateBatchSize;

            logMessageTableAdapter.UpdateBatchSize = updateBatchSize;
            deviceStatusTableAdapter.UpdateBatchSize = updateBatchSize;
            streamsTableAdapter.UpdateBatchSize = updateBatchSize;
            referenceTableAdapter.UpdateBatchSize = updateBatchSize;
            streamFieldsTableAdapter.UpdateBatchSize = updateBatchSize;
            devicesTableAdapter.UpdateBatchSize = updateBatchSize;

            this.Connection = logMessageTableAdapter.Connection;
        }

        public SqlConnection Connection
        {
            get
            {
                SqlConnection cn = logMessageTableAdapter.Connection;
                if (cn == null) cn = deviceStatusTableAdapter.Connection;
                if (cn == null) cn = streamsTableAdapter.Connection;
                if (cn == null) cn = referenceTableAdapter.Connection;
                if (cn == null) cn = streamFieldsTableAdapter.Connection;
                if (cn == null) cn = devicesTableAdapter.Connection;
                return cn;
            }
            set
            {
                logMessageTableAdapter.Connection = value;
                deviceStatusTableAdapter.Connection = value;
                streamsTableAdapter.Connection = value;
                referenceTableAdapter.Connection = value;
                streamFieldsTableAdapter.Connection = value;
                devicesTableAdapter.Connection = value;
            }
        }

        public SqlTransaction Transaction
        {
            get
            {
                SqlTransaction tn = logMessageTableAdapter.Transaction;
                if (tn == null) tn = deviceStatusTableAdapter.Transaction;
                if (tn == null) tn = streamsTableAdapter.Transaction;
                if (tn == null) tn = referenceTableAdapter.Transaction;
                if (tn == null) tn = streamFieldsTableAdapter.Transaction;
                if (tn == null) tn = devicesTableAdapter.Transaction;
                return tn;
            }
            set
            {
                if (value != null)
                    this.Connection = value.Connection;
                logMessageTableAdapter.Transaction = value;
                deviceStatusTableAdapter.Transaction = value;
                streamsTableAdapter.Transaction = value;
                referenceTableAdapter.Transaction = value;
                streamFieldsTableAdapter.Transaction = value;
                devicesTableAdapter.Transaction = value;
            }
        }

        public int Update(dsCacheLog ds)
        {
            int i = 0;
            i += logMessageTableAdapter.Update(ds.LogMessages);
            i += deviceStatusTableAdapter.Update(ds.DeviceStatus);
            i += streamsTableAdapter.Update(ds.Streams);
            i += referenceTableAdapter.Update(ds.Reference);
            i += streamFieldsTableAdapter.Update(ds.StreamFields);
            i += devicesTableAdapter.Update(ds.Devices);
            return i;
        }
    }

}

namespace GrisSuite.Data.dsCacheLogTableAdapters
{
    public partial class QueriesCacheLog
    {
        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }

        }
    }

    public partial class LogMessagesTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }
    }

    public partial class DeviceStatusTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }
    }

    public partial class StreamsTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }
    }

    public partial class ReferenceTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }
    }

    public partial class StreamFieldsTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }
    }

    public partial class DevicesTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }
    }


}

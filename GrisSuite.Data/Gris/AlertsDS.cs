﻿using System.Linq;
using System.Data;
using System.Data.SqlClient;

using GrisSuite.Common;

namespace GrisSuite.Data.Gris
{
    public partial class AlertsDS
    {
        partial class ViolatedRulesDataTable
        {
        }
    }
}

namespace GrisSuite.Data.Gris.AlertsDSTableAdapters
{
    public partial class AlertTicketsTableAdapter
    {
        public AlertsPresence AlertsPresent(int cacheLogMessagesObsoleteTimespanMinutes, bool isUserInAdminGroup)
        {
            try
            {
                if (isUserInAdminGroup)
                {
                    // Solo gli amministratori di sito possono vedere la segnalazione di motore di cache fermo
                    byte isMostRecentCacheLogMessageOlderThan = (byte)this.IsMostRecentCacheLogMessageOlderThan(cacheLogMessagesObsoleteTimespanMinutes);

                    if (isMostRecentCacheLogMessageOlderThan != 0)
                    {
                        return AlertsPresence.CacheEngineError;
                    }
                }

                int? openAlertCount = this.GetAlertsCountWithPermissions("", "", "", true, false, false, -1, -1);

                if (openAlertCount.HasValue && openAlertCount >= 0)
                {
                    if (openAlertCount == 0) return AlertsPresence.NotPresent;

                    return AlertsPresence.Present;
                }

                return AlertsPresence.Error;
            }
            catch (SqlException)
            {
                return AlertsPresence.Error; // l'eccezione non deve essere visualizzata, non è una interrogazione critica
            }
        }

        public virtual int? GetAlertsCountWithPermissions(string TicketName, string Location, string Type, bool New, bool Processed, bool Discarded, int SrvID, int AlertRuleID)
        {
            if (this.CommandCollection != null && this.CommandCollection.Count() >= 2)
            {
                this.AppendPermissionFilter(this.CommandCollection[1]);
                return this.GetAlertsCount(TicketName, Location, Type, New, Processed, Discarded, SrvID, AlertRuleID);
            }

            return -1;
        }

        public virtual AlertsDS.AlertTicketsDataTable GetDataWithPermissions(string TicketName, string Location, string Type, bool New, bool Processed, bool Discarded, int SrvID, int AlertRuleID, int StartRowIndex, int MaximumRows, string Order)
        {
            if (this.CommandCollection != null && this.CommandCollection.Count() >= 1)
            {
                this.AppendPermissionFilter(this.CommandCollection[0]);
                if (!string.IsNullOrEmpty(Order)) this.ApplySortOrder(this.CommandCollection[0], Order);
                this.CommandCollection[0].CommandTimeout = 180;
                return this.GetData(TicketName, Location, Type, New, Processed, Discarded, SrvID, AlertRuleID, StartRowIndex, MaximumRows);
            }

            return null;
        }

        private void AppendPermissionFilter(IDbCommand command)
        {
            // applico dei filtri al result set a livello applicativo perchè al momento il db è permission agnostic
            string appLevelFilters = "";
            string visibleRegions = string.Join(", ", GrisPermissions.VisibleRegions(PermissionLevel.Level2).Select(id => id.ToString()).ToArray());
            string compPermissions = string.Format("regions.RegID IN ({0})", (visibleRegions.Length > 0) ? visibleRegions : "-1");
            string ruleSevLevelPermissions = "RuleSevLevel > 1";

            if (!GrisPermissions.IsUserInAdminGroup() && !GrisPermissions.IsUserInGrisOperatorsGroup())
            {
                appLevelFilters = string.Format(" AND {0} AND {1}", compPermissions, ruleSevLevelPermissions);
            }
            else
            {
                appLevelFilters = string.Format(" AND {0}", compPermissions);
            }

            // la query degli allarmi così com'è non ha bisogno di order by
            command.CommandText = command.CommandText.Replace("(device_status.SevLevel <> 255)",
                                                              string.Format("(device_status.SevLevel <> 255){0} ",
                                                                            appLevelFilters));
        }

        private void ApplySortOrder(IDbCommand command, string sortString)
        {
            // originariamente non era possibile applicare un ordinamento diverso da quello di default (CreationDate DESC)
            // è possibile che le query eseguite con un ordinamento diverso da quello di default abbiano una minore efficienza perchè non ci sono indici adeguati, TODO: rivedere gli indici disponibili

            command.CommandText = command.CommandText.Replace("(ORDER BY CreationDate DESC, NodeName, DeviceName)", string.Format("(ORDER BY {0})", sortString));
        }
    }
}

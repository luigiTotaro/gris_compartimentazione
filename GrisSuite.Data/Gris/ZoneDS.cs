﻿
using GrisSuite.Common;

namespace GrisSuite.Data.Gris {
    
    
    public partial class ZoneDS 
    {
		public partial class ZoneRow : IEntityWithSeverity
		{
			public int SeverityValue
			{
				get { return this.Status; }
				set { throw new System.NotImplementedException(); }
			}

			public string SeverityDescription
			{
				get { return this.StatusDescription; }
				set { throw new System.NotImplementedException(); }
			}
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace GrisSuite.Data
{
    public enum AnalyzedEnum
    { 
        NotAnalyzed = 0,
        Analyzed = 1,
        Wrong = 2
    }
}

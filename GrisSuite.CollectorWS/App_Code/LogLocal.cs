using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web;
using GrisSuite.Common;

internal class LogLocal
{
    private const string LOG_SOURCE = "GrisSuite.CentralWS";
    private string _userHostAddress = string.Empty;

    internal LogLocal()
    {
    }

    internal LogLocal(string userHostAddress)
    {
        this._userHostAddress = userHostAddress;
    }

    internal string UserHostAddress
    {
        get { return this._userHostAddress; }
        set { this._userHostAddress = value; }
    }

    internal void LogSqlErrors(SqlException exception, string procedureName)
    {
        this.LogSqlErrors(exception, procedureName, EventLogEntryType.Error, (int) GrisDatabaseEvents.QueryInfo);
    }

    internal void LogSqlErrors(SqlException exception, string procedureName, EventLogEntryType logEntryType, int eventId)
    {
        // Ignoriamo le violazioni di chiave dovute a invii non in ordine di dati, che si correggono al giro successivo
        // ErrorNumber 547 Error System.Data.SqlClient.SqlException: The INSERT statement conflicted with the FOREIGN KEY constraint
        if (exception.Number != 547)
        {
            EventLog.WriteEntry(LOG_SOURCE,
                string.Format("Error SQL on {0} from Host: {1} ErrorNumber {2} Error {3}", procedureName, this.UserHostAddress,
                    exception.Number.ToString(), exception), logEntryType, eventId, (short) GrisEventCategory.DatabaseEvents);
        }
    }

    internal void LogErrors(Exception exception, string procedureName)
    {
        if (exception is SqlException)
        {
            this.LogSqlErrors((SqlException) exception, procedureName, EventLogEntryType.Error, (int) GrisDatabaseEvents.QueryInfo);
        }
        else
        {
            this.LogErrors(exception, procedureName, EventLogEntryType.Error, (int) GrisSystemEvents.Error, GrisEventCategory.SystemEvents);
        }
    }

    internal void LogErrors(Exception exception, string procedureName, EventLogEntryType logEntryType, int eventId, GrisEventCategory category)
    {
        const string error = "Error on {0} from Host: {1} Error {2}";
        EventLog.WriteEntry(LOG_SOURCE, string.Format(error, procedureName, this.UserHostAddress, exception.ToString()), logEntryType, eventId,
            (short) category);
    }

    internal void WriteDebugLogToFile(string text)
    {
        try
        {
            string sfile = this.GetFileName();
            using (TextWriter wr = new StreamWriter(sfile, true))
            {
                wr.WriteLine("{0}: {1}<br/>", DateTime.Now.ToString(), this.HtmlNewLine(text));
                wr.Close();
            }
        }
        catch (Exception ex)
        {
            EventLog.WriteEntry(LOG_SOURCE, "Error on write debug log:" + ex.Message + Environment.NewLine + ex.StackTrace, EventLogEntryType.Warning);
        }
    }

    internal string HtmlNewLine(string input)
    {
        if (!String.IsNullOrEmpty(input))
        {
            return HttpUtility.HtmlEncode(input).Replace("\n", "<br/>");
        }
        return string.Empty;
    }

    private string GetFileName()
    {
        string sfile = string.Format("{0}\\{1}.log.htm", AppDomain.CurrentDomain.BaseDirectory.ToString(),
            LOG_SOURCE);

        if (File.Exists(sfile))
        {
            FileInfo f = new FileInfo(sfile);
            const long debugLogMaxSize = 2097152;
            if (f.Length > debugLogMaxSize)
            {
                string sfileOld = sfile.Replace(".log.htm", "_old.log.htm");

                if (File.Exists(sfileOld))
                {
                    File.Delete(sfileOld);
                }

                f.MoveTo(sfileOld);
            }
        }

        return sfile;
    }
}
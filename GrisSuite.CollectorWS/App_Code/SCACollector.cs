// #define DEBUG_LOG

using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using GrisSuite;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.dsCentralServersTableAdapters;
using GrisSuite.Data.dsConfigTableAdapters;

[WebService(Namespace = "http://Telefin.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SCACollector : WebService
{
    private static readonly object _lockReplace = new object();
    private readonly int _lockTimeout = 1000;
    private const string _errorServerBusy = "Server busy";
    private readonly LogLocal _log;
    private readonly bool _logServerIPWarning = true;
    private readonly bool _enableLockOnReplaceCommands;

    public SCACollector()
    {
        if (!int.TryParse(ConfigurationManager.AppSettings["LockTimeout"], out this._lockTimeout))
        {
            this._lockTimeout = 1000;
        }

        if (!bool.TryParse(ConfigurationManager.AppSettings["ServerIPWarning"], out this._logServerIPWarning))
        {
            this._logServerIPWarning = true;
        }

        if (!bool.TryParse(ConfigurationManager.AppSettings["EnableLockOnReplaceCommands"], out this._enableLockOnReplaceCommands))
        {
            this._enableLockOnReplaceCommands = false;
        }

        this._log = new LogLocal(this.Context.Request.UserHostAddress);
    }

    #region Metodi esportati

    [WebMethod]
    public Guid UpdateStatus(byte[] cds, string mac)
    {
        try
        {
            string[] tables = ConfigurationManager.AppSettings["Tables_DeviceStatus"].Split(',');
            dsDeviceStatus dsStatus = new dsDeviceStatus();
            Util.FillDataSetFromCompressed(dsStatus, cds);
            Guid guid = dsDeviceStatus.UpdateWithLog(dsStatus, this.Context.Request.UserHostAddress, tables, mac);
            this.SetLastServerUpdate("UpdateStatus", this.Context.Request.UserHostAddress, mac, 0);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateStatus");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateStatus");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid UpdateStatusById(byte[] cds, string mac, int srvId)
    {
        try
        {
            string[] tables = ConfigurationManager.AppSettings["Tables_DeviceStatus"].Split(',');
            dsDeviceStatus dsStatus = new dsDeviceStatus();
            Util.FillDataSetFromCompressed(dsStatus, cds);
            Guid guid = dsDeviceStatus.UpdateWithLog(dsStatus, this.Context.Request.UserHostAddress, tables, mac);
            this.SetLastServerUpdate("UpdateStatus", this.Context.Request.UserHostAddress, mac, srvId);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateStatusById");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateStatusById");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid ReplaceStatus(byte[] cds, string mac)
    {
        if (this._enableLockOnReplaceCommands)
        {
            #region Versione con mutex

            if (Monitor.TryEnter(_lockReplace, this._lockTimeout))
            {
                try
                {
                    return this.DoReplaceStatus(cds, mac);
                }
                catch (SqlException sqlEx)
                {
                    this._log.LogSqlErrors(sqlEx, "ReplaceStatus");
                    throw new SCException(sqlEx.Message);
                }
                catch (Exception ex)
                {
                    this._log.LogErrors(ex, "ReplaceStatus");
                    throw new SCException(ex.Message);
                }
                finally
                {
                    Monitor.Exit(_lockReplace);
                }
            }
            else
            {
                SCException ex = new SCException(_errorServerBusy);
                this._log.LogErrors(ex, "ReplaceStatus", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                throw ex;
            }

            #endregion
        }
        else
        {
            #region Versione normale

            try
            {
                return this.DoReplaceStatus(cds, mac);
            }
            catch (SqlException sqlEx)
            {
                this._log.LogSqlErrors(sqlEx, "ReplaceStatus");
                throw new SCException(sqlEx.Message);
            }
            catch (Exception ex)
            {
                this._log.LogErrors(ex, "ReplaceStatus");
                throw new SCException(ex.Message);
            }

            #endregion
        }
    }

    [WebMethod]
    public Guid ReplaceStatusById(byte[] cds, string mac, int srvId)
    {
        if (this._enableLockOnReplaceCommands)
        {
            #region Versione con mutex

            if (Monitor.TryEnter(_lockReplace, this._lockTimeout))
            {
                try
                {
                    return this.DoReplaceStatus(cds, mac, srvId);
                }
                catch (SqlException sqlEx)
                {
                    this._log.LogSqlErrors(sqlEx, "ReplaceStatusById");
                    throw new SCException(sqlEx.Message);
                }
                catch (Exception ex)
                {
                    this._log.LogErrors(ex, "ReplaceStatusById");
                    throw new SCException(ex.Message);
                }
                finally
                {
                    Monitor.Exit(_lockReplace);
                }
            }
            else
            {
                SCException ex = new SCException(_errorServerBusy);
                this._log.LogErrors(ex, "ReplaceStatusById", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                throw ex;
            }

            #endregion
        }
        else
        {
            #region Versione normale

            try
            {
                return this.DoReplaceStatus(cds, mac, srvId);
            }
            catch (SqlException sqlEx)
            {
                this._log.LogSqlErrors(sqlEx, "ReplaceStatusById");
                throw new SCException(sqlEx.Message);
            }
            catch (Exception ex)
            {
                this._log.LogErrors(ex, "ReplaceStatusById");
                throw new SCException(ex.Message);
            }

            #endregion
        }
    }

    [WebMethod]
    public Guid UpdateConfig(byte[] cds, string mac)
    {
        try
        {
            string[] tables = ConfigurationManager.AppSettings["Tables_Config"].Split(',');
            dsConfig dsConfig = DecompressConfig(cds);
            Guid guid = dsConfig.UpdateWithLog(dsConfig, this.Context.Request.UserHostAddress, tables, mac);
            this.SetLastServerUpdate("UpdateConfig", this.Context.Request.UserHostAddress, mac, 0);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateConfig");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateConfig");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid UpdateConfigById(byte[] cds, string mac, int srvId)
    {
        try
        {
            string[] tables = ConfigurationManager.AppSettings["Tables_Config"].Split(',');
            dsConfig dsConfig = DecompressConfig(cds);
            Guid guid = dsConfig.UpdateWithLog(dsConfig, this.Context.Request.UserHostAddress, tables, mac);
            this.SetLastServerUpdate("UpdateConfig", this.Context.Request.UserHostAddress, mac, srvId);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateConfigById");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateConfigById");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid ReplaceConfig(byte[] cds, string mac)
    {
        if (this._enableLockOnReplaceCommands)
        {
            #region Versione con mutex

            if (Monitor.TryEnter(_lockReplace, this._lockTimeout))
            {
                try
                {
                    return this.ReplaceConfigDo(cds, mac);
                }
                catch (SqlException sqlEx)
                {
                    this._log.LogSqlErrors(sqlEx, "ReplaceConfig");
                    throw new SCException(sqlEx.Message);
                }
                catch (Exception ex)
                {
                    this._log.LogErrors(ex, "ReplaceConfig");
                    throw new SCException(ex.Message);
                }
                finally
                {
                    Monitor.Exit(_lockReplace);
                }
            }
            else
            {
                SCException ex = new SCException(_errorServerBusy);
                this._log.LogErrors(ex, "ReplaceConfig", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                throw ex;
            }

            #endregion
        }
        else
        {
            #region Versione normale

            try
            {
                return this.ReplaceConfigDo(cds, mac);
            }
            catch (SqlException sqlEx)
            {
                this._log.LogSqlErrors(sqlEx, "ReplaceConfig");
                throw new SCException(sqlEx.Message);
            }
            catch (Exception ex)
            {
                this._log.LogErrors(ex, "ReplaceConfig");
                throw new SCException(ex.Message);
            }

            #endregion
        }
    }

    [WebMethod]
    public Guid ReplaceConfigById(byte[] cds, string mac, int srvId)
    {
        if (this._enableLockOnReplaceCommands)
        {
            #region Versione con mutex

            if (Monitor.TryEnter(_lockReplace, this._lockTimeout))
            {
                try
                {
                    return this.ReplaceConfigDo(cds, mac, srvId);
                }
                catch (SqlException sqlEx)
                {
                    this._log.LogSqlErrors(sqlEx, "ReplaceConfigById");
                    throw new SCException(sqlEx.Message);
                }
                catch (Exception ex)
                {
                    this._log.LogErrors(ex, "ReplaceConfigById");
                    throw new SCException(ex.Message);
                }
                finally
                {
                    Monitor.Exit(_lockReplace);
                }
            }
            else
            {
                SCException ex = new SCException(_errorServerBusy);
                this._log.LogErrors(ex, "ReplaceConfigById", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                throw ex;
            }

            #endregion
        }
        else
        {
            #region Versione normale

            try
            {
                return this.ReplaceConfigDo(cds, mac, srvId);
            }
            catch (SqlException sqlEx)
            {
                this._log.LogSqlErrors(sqlEx, "ReplaceConfigById");
                throw new SCException(sqlEx.Message);
            }
            catch (Exception ex)
            {
                this._log.LogErrors(ex, "ReplaceConfigById");
                throw new SCException(ex.Message);
            }

            #endregion
        }
    }

    [WebMethod]
    public Guid UpdateEvents(byte[] cds, string mac)
    {
        try
        {
            dsEvents ds = new dsEvents();
            Util.FillDataSetFromCompressed(ds, cds);
            Guid guid = dsEvents.UpdateWithLog(ds, this.Context.Request.UserHostAddress, mac);
            this.SetLastServerUpdate("UpdateEvents", this.Context.Request.UserHostAddress, mac, 0);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateEvents");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateEvents");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid UpdateEventsById(byte[] cds, string mac, int srvId)
    {
        try
        {
            dsEvents ds = new dsEvents();
            Util.FillDataSetFromCompressed(ds, cds);
            Guid guid = dsEvents.UpdateWithLog(ds, this.Context.Request.UserHostAddress, mac);
            this.SetLastServerUpdate("UpdateEvents", this.Context.Request.UserHostAddress, mac, srvId);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateEventsById");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateEventsById");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid UpdateAcks(byte[] cds, string mac)
    {
        try
        {
            dsDeviceAcks ds = new dsDeviceAcks();
            Util.FillDataSetFromCompressed(ds, cds);
            Guid guid = dsDeviceAcks.UpdateWithLog(ds, this.Context.Request.UserHostAddress, mac, null);
            this.SetLastServerUpdate("UpdateAcks", this.Context.Request.UserHostAddress, mac, 0);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateAcks");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateAcks");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid UpdateAcksById(byte[] cds, string mac, int srvId)
    {
        try
        {
            dsDeviceAcks ds = new dsDeviceAcks();
            Util.FillDataSetFromCompressed(ds, cds);
            Guid guid = dsDeviceAcks.UpdateWithLog(ds, this.Context.Request.UserHostAddress, mac, srvId);
            this.SetLastServerUpdate("UpdateAcks", this.Context.Request.UserHostAddress, mac, srvId);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "UpdateAcksById");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateAcksById");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid ReplaceSTLCParameters(byte[] cds, string mac)
    {
        try
        {
            dsSTLCParameters ds = new dsSTLCParameters();
            Util.FillDataSetFromCompressed(ds, cds);
            Guid guid = dsSTLCParameters.ReplaceWithLog(ds, this.Context.Request.UserHostAddress, mac);
            this.SetLastServerUpdate("ReplaceSTLCParameters", this.Context.Request.UserHostAddress, mac, 0);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "ReplaceSTLCParameters");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "ReplaceSTLCParameters");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid ReplaceSTLCParametersById(byte[] cds, string mac, int srvId)
    {
        try
        {
            dsSTLCParameters ds = new dsSTLCParameters();
            Util.FillDataSetFromCompressed(ds, cds);
            Guid guid = dsSTLCParameters.ReplaceWithLog(ds, this.Context.Request.UserHostAddress, mac);
            this.SetLastServerUpdate("ReplaceSTLCParameters", this.Context.Request.UserHostAddress, mac, srvId);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogSqlErrors(sqlEx, "ReplaceSTLCParametersById");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "ReplaceSTLCParametersById");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid IsAlive(string mac)
    {
        try
        {
#if DEBUG_LOG
            this._log.WriteDebugLogToFile(String.Format("IsAlive - MAC: {0}", mac));
#endif

            Guid guid = dsLog.InsLogMessage(null, "IsAlive", this.Context.Request.UserHostAddress, mac);
            this.SetLastServerUpdate("IsAlive", this.Context.Request.UserHostAddress, mac, 0);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogErrors(sqlEx, "IsAlive");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "IsAlive");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid IsAliveById(int srvId)
    {
        try
        {
#if DEBUG_LOG
            this._log.WriteDebugLogToFile(String.Format("IsAliveById - ServerID: {0}", srvId.ToString()));
#endif

            Guid guid = dsLog.InsLogMessage(null, "IsAlive", this.Context.Request.UserHostAddress);
            this.SetLastServerUpdate("IsAlive", this.Context.Request.UserHostAddress, String.Empty, srvId);
            return guid;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogErrors(sqlEx, "IsAliveById");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "IsAliveById");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public Guid UpdateParkedServer(long? srvId,
        string host,
        string name,
        string type,
        string clientRegId,
        string clientZonId,
        string clientNodId,
        string hardwareProfile,
        byte parkingType)
    {
        try
        {
            this.DoUpdateParkedServer(this.Context.Request.UserHostAddress, srvId, host, name, type, clientRegId, clientZonId, clientNodId,
                hardwareProfile, parkingType);
            return Guid.Empty;
        }
        catch (SqlException sqlEx)
        {
            this._log.LogErrors(sqlEx, "UpdateParkedServer");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "UpdateParkedServer");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public byte IsServerLicensed(long srvId, string hardwareProfile)
    {
        try
        {
            return this.DoIsServerLicensed(this.Context.Request.UserHostAddress, srvId, hardwareProfile);
        }
        catch (SqlException sqlEx)
        {
            this._log.LogErrors(sqlEx, "IsServerLicensed");
            throw new SCException(sqlEx.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "IsServerLicensed");
            throw new SCException(ex.Message);
        }
    }

    #endregion

    private Guid DoReplaceStatus(byte[] cds, string mac)
    {
        return this.DoReplaceStatus(cds, mac, 0);
    }

    private Guid DoReplaceStatus(byte[] cds, string mac, int srvId)
    {
        string[] tables = ConfigurationManager.AppSettings["Tables_DeviceStatus"].Split(',');
        dsDeviceStatus dsStatus = new dsDeviceStatus();
        Util.FillDataSetFromCompressed(dsStatus, cds);
        Guid guid = dsDeviceStatus.ReplaceWithLog(dsStatus, this.Context.Request.UserHostAddress, tables, mac);
        this.SetLastServerUpdate("ReplaceStatus", this.Context.Request.UserHostAddress, mac, srvId);
        return guid;
    }

    private Guid ReplaceConfigDo(byte[] cds, string mac)
    {
        return this.ReplaceConfigDo(cds, mac, 0);
    }

    private Guid ReplaceConfigDo(byte[] cds, string mac, int srvId)
    {
        string[] tables = ConfigurationManager.AppSettings["Tables_Config"].Split(',');
        dsConfig dsConfig = DecompressConfig(cds);
        Guid guid = dsConfig.ReplaceWithLog(dsConfig, this.Context.Request.UserHostAddress, tables, mac);
        this.SetLastServerUpdate("ReplaceConfig", this.Context.Request.UserHostAddress, mac, srvId);
        return guid;
    }

    private static dsConfig DecompressConfig(byte[] cds)
    {
        dsConfig dsConfig = new dsConfig();
        Util.FillDataSetFromCompressed(dsConfig, cds);
        return dsConfig;
    }

    private void SetLastServerUpdate(string messageType, string ip, string mac, int srvId)
    {
        ServersTableAdapter sta = new ServersTableAdapter();

#if DEBUG_LOG
        this._log.WriteDebugLogToFile(String.Format("SetLastServerUpdate - MessageType: {0}, IP: {1}, MAC: {2}, ServerID: {3}", messageType, ip, mac,
            srvId.ToString()));
#endif

        if (srvId > 0)
        {
            #region Logica per ServerID

#if DEBUG_LOG
            this._log.WriteDebugLogToFile(String.Format("UpdServerStatusBySrvID - MessageType: {0}, IP: {1}, MAC: {2}, ServerID: {3}", messageType, ip, mac,
                srvId.ToString()));
#endif

            int serverCount = Convert.ToInt32(sta.UpdServerStatusBySrvID(srvId, ip, DateTime.Now, messageType));
            if (this._logServerIPWarning)
            {
                if (serverCount == 0)
                {
#if DEBUG_LOG
                    this._log.WriteDebugLogToFile(String.Format("SrvID sender ({0}) not found in Servers table.", mac));
#endif

                    this._log.LogErrors(new Exception(string.Format("SrvID sender ({0}) not found in Servers table.", mac)), "SetLastServerUpdate",
                        EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                }
            }

            #endregion
        }
        else if (!string.IsNullOrEmpty(mac))
        {
            #region Logica per MAC

#if DEBUG_LOG
            this._log.WriteDebugLogToFile(String.Format("UpdServerStatusByMAC - MessageType: {0}, IP: {1}, MAC: {2}, ServerID: {3}", messageType, ip, mac,
            srvId.ToString()));
#endif

            int serverCount = Convert.ToInt32(sta.UpdServerStatusByMAC(mac, ip, DateTime.Now, messageType));
            if (this._logServerIPWarning)
            {
                if (serverCount == 0)
                {
#if DEBUG_LOG
                    this._log.WriteDebugLogToFile(String.Format("MAC sender ({0}) not found in Servers table.", mac));
#endif

                    this._log.LogErrors(new Exception(string.Format("MAC sender ({0}) not found in Servers table.", mac)), "SetLastServerUpdate",
                        EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                }
                else if (serverCount > 1)
                {
#if DEBUG_LOG
                    this._log.WriteDebugLogToFile(String.Format("Found {0} MACs ({1}) in Servers table, where 1 is expected.", serverCount, mac));
#endif

                    this._log.LogErrors(
                        new Exception(string.Format("Found {0} MACs ({1}) in Servers table, where 1 is expected.", serverCount, mac)),
                        "SetLastServerUpdate", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                }
            }

            #endregion
        }
        else
        {
            #region Logica per IP

            int serverCount = Convert.ToInt32(sta.UpdServerStatusByIP(ip, DateTime.Now, messageType));

#if DEBUG_LOG
            this._log.WriteDebugLogToFile(String.Format("UpdServerStatusByIP - MessageType: {0}, IP: {1}, MAC: {2}, ServerID: {3}", messageType, ip, mac,
            srvId.ToString()));
#endif

            if (this._logServerIPWarning)
            {
                if (serverCount == 0)
                {
#if DEBUG_LOG
                    this._log.WriteDebugLogToFile(String.Format("IP sender ({0}) not found in Servers table.", ip));
#endif

                    this._log.LogErrors(new Exception(string.Format("IP sender ({0}) not found in Servers table.", ip)), "SetLastServerUpdate",
                        EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                }
                else if (serverCount > 1)
                {
#if DEBUG_LOG
                    this._log.WriteDebugLogToFile(String.Format("Found {0} IPs ({1}) in Servers table, where 1 is expected.", serverCount, ip));
#endif

                    this._log.LogErrors(new Exception(string.Format("Found {0} IPs ({1}) in Servers table, where 1 is expected.", serverCount, ip)),
                        "SetLastServerUpdate", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning, GrisEventCategory.SystemEvents);
                }
            }

            #endregion
        }
    }

    private void DoUpdateParkedServer(string ip,
        long? srvId,
        string host,
        string name,
        string type,
        string clientRegId,
        string clientZonId,
        string clientNodId,
        string hardwareProfile,
        byte parkingType)
    {
        serversTableAdapter sta = new serversTableAdapter();

        if (!String.IsNullOrEmpty(ip))
        {
            sta.UpdateServerParking(ip, srvId, host, name, type, clientRegId, clientZonId, clientNodId, hardwareProfile, parkingType);
        }
        else
        {
            this._log.LogErrors(
                new Exception(string.Format("Unable to save server parking data, missing ip. SrvID: {0}, Host: {1}, Name: {2}, HardwareProfile: {3}",
                    srvId.ToString(), host, name, hardwareProfile)), "UpdateParkedServer", EventLogEntryType.Warning, (int) GrisSystemEvents.Warning,
                GrisEventCategory.SystemEvents);
        }
    }

    private byte DoIsServerLicensed(string ip, long srvId, string hardwareProfile)
    {
        IsServerLicensedTableAdapter slta = new IsServerLicensedTableAdapter();
        dsCentralServers.IsServerLicensedDataTable dt = slta.GetData(srvId, ip, hardwareProfile);

        if ((dt != null) && (dt.Rows.Count > 0))
        {
            return dt[0].IsLicensed;
        }

        return 0;
    }
}
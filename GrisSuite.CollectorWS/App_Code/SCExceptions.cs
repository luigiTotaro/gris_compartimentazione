using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// SCException for SC web services
/// </summary>
public class SCException : Exception
{
        public SCException() : base() { }
        public SCException(string message) : base(message) { }
        public SCException(string message, Exception inner) : base(message, inner) { }

        public override string Message
        {
            get
            {
                return string.Format("Error on {0}/GrisSuite.CentralWS:{2}", System.Net.Dns.GetHostName(), System.Web.HttpContext.Current.Request.Path , base.Message);
            }
        }

        public override string ToString()
        {
            return this.Message;
        }
}

﻿using System.Windows;
using GrisSuite.FDS.UI.BO;
using GrisSuite.FDS.UI.UISchede;

namespace GrisSuite.FDS.UI
{
    public partial class UISchedaSmallLoader
    {
        private SchedaData scheda;

        public delegate void ClickHandler(object sender, RoutedEventArgs e);

        public event ClickHandler Click;

        public UISchedaSmallLoader()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scheda = this.DataContext as SchedaData;

            if (scheda != null)
            {
                FrameworkElement uischeda = new UISchedaSmallEmpty();
                switch (scheda.Tipo)
                {
                    case TipoScheda.FD11000:
                        uischeda = new UISchedaSmallFD11000();
                        break;
                    case TipoScheda.FD11100:
                        uischeda = new UISchedaSmallFD11100();
                        break;
                    case TipoScheda.FD11200:
                        uischeda = new UISchedaSmallFD11200();
                        break;
                    case TipoScheda.FD13000:
                        uischeda = new UISchedaSmallFD13000();
                        break;
                    case TipoScheda.FD20000:
                        uischeda = new UISchedaSmallFD20000();
                        break;
                    case TipoScheda.FD39200:
                        uischeda = new UISchedaSmallFD39200();
                        break;
                    case TipoScheda.FD40100:
                        uischeda = new UISchedaSmallFD40100();
                        break;
                    case TipoScheda.FD60X00:
                        uischeda = new UISchedaSmallFD60X00();
                        break;
                    case TipoScheda.FD80000:
                        uischeda = new UISchedaSmallFD80000();
                        break;
                    case TipoScheda.FD90000:
                        uischeda = new UISchedaSmallFD90000();
                        break;
                    case TipoScheda.FD91000:
                        uischeda = new UISchedaSmallFD91000();
                        break;
                    case TipoScheda.FD92000:
                        uischeda = new UISchedaSmallFD92000();
                        break;
                    case TipoScheda.Unknown:
                        uischeda = new UISchedaSmallUnknown();
                        break;
                }

                this.SchedaControl.Content = uischeda;

                scheda.PropertyChanged += scheda_PropertyChanged;

                StandardContent.Visibility = Visibility.Visible;

                VisualStateManager.GoToState(this, scheda.Stato.ToString(), false);

                if (scheda.Tipo == TipoScheda.Empty) // || scheda.Tipo == TipoScheda.Unknown
                    this.IsEnabled = false;
            }
        }

        private void scheda_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((scheda != null) && e.PropertyName == "Tipo" &&
                (scheda.Tipo == TipoScheda.Empty || scheda.Tipo == TipoScheda.Unknown))
                this.IsEnabled = false;

            if ((scheda != null) && e.PropertyName == "Stato")
                VisualStateManager.GoToState(this, scheda.Stato.ToString(), true);

            if (!this.IsEnabled)
                return;

            if ((scheda != null) && e.PropertyName == "IsHighlighted")
                VisualStateManager.GoToState(this, (scheda.IsHighlighted ? "Highlight" : "Defautlight"), true);

            if ((scheda != null) && e.PropertyName == "IsRemote")
                VisualStateManager.GoToState(this, (scheda.IsRemote ? "Remote" : "Local"), true);

            if ((scheda != null) && e.PropertyName == "IsObserved")
                VisualStateManager.GoToState(this, (scheda.IsObserved ? "Observed" : "NotObserved"), true);
        }

        private void StatesBorder_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (scheda.IsSelectable)
            {
                if (this.Click != null)
                {
                    this.Click(this, e);
                }
            }
        }

        private void StatesBorder_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (scheda != null)
                scheda.IsHighlighted = true;
        }

        private void StatesBorder_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (scheda != null)
                scheda.IsHighlighted = false;
        }
    }
}
﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Browser;

namespace GrisSuite.FDS.UI.BO
{
    public partial class FDSData
    {
        private ObservableCollection<SchedaData> _schedeObserved;

        public ObservableCollection<SchedaData> SchedeObserved
        {
            get { return _schedeObserved; }
        }

        public void InitObservation()
        {
            int schedaCounter = 1;

            _schedeObserved = new ObservableCollection<SchedaData>();
            if (this.SchedeLocali != null)
            {
                foreach (SchedaData scheda in this.SchedeLocali)
                {
                    scheda.Posizione = schedaCounter;
                    schedaCounter++;

                    scheda.PropertyChanged += SchedaPropertyChanged;
                }
                this.SchedeLocali.CollectionChanged += Schede_CollectionChanged;
            }


            if (this.SchedeRemote != null)
            {
                schedaCounter = 1;

                foreach (SchedaData scheda in this.SchedeRemote)
                {
                    scheda.Posizione = schedaCounter;
                    schedaCounter++;

                    scheda.PropertyChanged += SchedaPropertyChanged;
                }
                this.SchedeRemote.CollectionChanged += Schede_CollectionChanged;
            }
        }

        private void Schede_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (SchedaData scheda in e.NewItems)
                {
                    scheda.PropertyChanged += SchedaPropertyChanged;
                }
            }
        }

        private void SchedaPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            SchedaData sc = (SchedaData) sender;
            if (e.PropertyName == "IsObserved")
            {
                if (sc.IsObserved)
                {
                    if (!_schedeObserved.Contains(sc))
                    {
                        _schedeObserved.Insert(0, sc);
                    }
                }
                else
                {
                    if (_schedeObserved.Contains(sc))
                    {
                        _schedeObserved.Remove(sc);
                    }
                }
            }
        }
    }


    public partial class SchedaData
    {
        private bool _isObserverd;
        private bool _isHighlighted;

        [System.Runtime.Serialization.DataMemberAttribute]
        public bool IsObserved
        {
            get { return _isObserverd; }
            set
            {
                if ((this.IsObserved.Equals(value) != true))
                {
                    _isObserverd = value;
                    this.RaisePropertyChanged("IsObserved");
                }
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute]
        public bool IsHighlighted
        {
            get { return _isHighlighted; }
            set
            {
                if ((_isHighlighted.Equals(value) != true))
                {
                    _isHighlighted = value;
                    this.RaisePropertyChanged("IsHighlighted");
                }
            }
        }

        public ObservableCollection<SchedaAttributoData> AttributiPrincipali
        {
            get
            {
                if (this.Attributi != null && this.Attributi.ContainsKey(GruppiAttributi.Principale))
                {
                    return this.Attributi[GruppiAttributi.Principale];
                }
                return null;
            }
        }

        public ObservableCollection<SchedaAttributoData> AttributiE1
        {
            get
            {
                if (this.Attributi != null && this.Attributi.ContainsKey(GruppiAttributi.E1))
                {
                    return this.Attributi[GruppiAttributi.E1];
                }
                return null;
            }
        }

        public int Posizione { get; set; }

        public string PosizioneDescrizione
        {
            get
            {
                return string.Format("{0} ({1}, {2})", this.Posizione, ((this.Posizione - 1)/12) + 1,
                                     ((this.Posizione - 1)%12) + 1);
            }
        }
    }
}
﻿using System.Windows;
using System.Windows.Controls;
using GrisSuite.FDS.UI.BO;

namespace GrisSuite.FDS.UI
{
	public partial class UISchedaPopup
	{


        public double PopupWidth
        {
            get { return (double)GetValue(PopupWidthProperty); }
            set { SetValue(PopupWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PopupWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PopupWidthProperty =
            DependencyProperty.Register("PopupWidth", typeof(double), typeof(UISchedaPopup), null);



        public double PopupHeight
        {
            get { return (double)GetValue(PopupHeightProperty); }
            set { SetValue(PopupHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PopupHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PopupHeightProperty =
            DependencyProperty.Register("PopupHeight", typeof(double), typeof(UISchedaPopup), null);


		public UISchedaPopup()
		{
			// Required to initialize variables
			InitializeComponent();
            VisualStateManager.GoToState(this, "Destra", false);
		}

        private void infoPopup_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.PopupHeight = infoPopup.ActualHeight;
            this.PopupWidth = infoPopup.ActualWidth;
        }




	}
}
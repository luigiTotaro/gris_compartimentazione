﻿
using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows;

using GrisSuite.FDS.UI.BO;

namespace GrisSuite.FDS.UI
{
	public static class ProxiesFactory
	{
        public static FDSServiceClient GetFDSService()
		{
#if (DEBUG)
            return new FDSServiceClient();
#else
            FDSServiceClient ms = new FDSServiceClient();
            ServiceEndpoint ep = ms.Endpoint;
            ep.Address = new EndpointAddress(AdaptUrl(ms.Endpoint.Address.ToString()));
            return new FDSServiceClient("CustomBinding_IFDSService", ep.Address);
#endif
        }


        private static string AdaptUrl(string originalUriString)
        {
            Uri originalUri = new Uri(originalUriString);

            string part1 = Application.Current.Host.Source.GetComponents(UriComponents.AbsoluteUri, UriFormat.UriEscaped);
            part1 = part1.Substring(0, part1.IndexOf("ClientBin", 0,StringComparison.InvariantCultureIgnoreCase)-1);
            string part2 = originalUri.GetComponents(UriComponents.PathAndQuery, UriFormat.UriEscaped);

            // return part1 + "/Services" + part2;
            return part1 + part2;
        }
	}
}

﻿using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using GrisSuite.FDS.UI.BO;
using System;

namespace GrisSuite.FDS.UI
{
    public partial class Page
    {
        private readonly long devID;
        private readonly string htmlControlHostId;
        private readonly int autoRefreshTimeoutSeconds;
        private readonly TipoFDS fdsType;
        private readonly DispatcherTimer autoRefreshTimer;

        private FDSData fds;

        public Page() : this(0, string.Empty, 0, TipoFDS.NormaleLocale)
        {
        }

        public Page(long devID, string htmlControlHostId, int autoRefreshTimeoutSeconds, TipoFDS FDSType)
        {
            this.devID = devID;
            this.htmlControlHostId = htmlControlHostId;
            this.autoRefreshTimeoutSeconds = autoRefreshTimeoutSeconds;
            this.fdsType = FDSType;

            InitializeComponent();

            if (this.autoRefreshTimeoutSeconds > 0)
            {
                this.autoRefreshTimer = new DispatcherTimer
                                            {Interval = new TimeSpan(0, 0, this.autoRefreshTimeoutSeconds)};
                this.autoRefreshTimer.Tick += this.autoRefreshTimer_Tick;
                this.autoRefreshTimer.Start();
            }

            this.LoadData();
        }

        public long DevId
        {
            get { return devID; }
        }

        private void LoadData()
        {
            if (devID > 0)
            {
                FDSServiceClient fdsClient = ProxiesFactory.GetFDSService();
                fdsClient.GetCompleted += fdsClient_GetCompleted;
                fdsClient.GetAsync(this.devID, this.fdsType);
            }
        }

        private void fdsClient_GetCompleted(object sender, GetCompletedEventArgs e)
        {
            if (e.Error == null && e.Result != null)
            {
                this.fds = e.Result;
                this.fds.InitObservation();
                this.DataContext = fds;
                this.FDSLocal.DataContext = fds.SchedeLocali;
                this.FDSRemote.DataContext = fds.SchedeRemote;
                this.FDSLocal.LastUpdate = (fds.LastUpdate > DateTime.MinValue
                                                ? fds.LastUpdate.ToString("dd/MM/yyyy HH:mm")
                                                : string.Empty);
                this.FDSRemote.LastUpdate = (fds.LastUpdate > DateTime.MinValue
                                                 ? fds.LastUpdate.ToString("dd/MM/yyyy HH:mm")
                                                 : string.Empty);
            }
            else if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }

            this.ResizeHtmlHostControl();
        }

        private void Scheda_MouseEnter(object sender, MouseEventArgs e)
        {
            SchedaData scheda = ((Control) sender).DataContext as SchedaData;

            if (scheda == null)
            {
                this.infoPopup.Visibility = Visibility.Collapsed;
            }
            else
            {
                Point position = e.GetPosition(Application.Current.RootVisual);
                movePopup(position);
                this.infoPopup.DataContext = ((Control) sender).DataContext;
                this.infoPopup.Visibility = Visibility.Visible;
            }
        }

        private void Scheda_MouseLeave(object sender, MouseEventArgs e)
        {
            this.infoPopup.Visibility = Visibility.Collapsed;
        }

        private void Scheda_MouseMove(object sender, MouseEventArgs e)
        {
            Point position = e.GetPosition(Application.Current.RootVisual);
            movePopup(position);
        }


        private void Scheda_Click(object sender, RoutedEventArgs e)
        {
            SchedaData sc = ((SchedaData) ((Control) sender).DataContext);
            sc.IsObserved = !sc.IsObserved;

            this.ResizeHtmlHostControl();
        }

        private void movePopup(Point popLoc)
        {
            Point locFSDPanel =
                this.FDSPanel.TransformToVisual(Application.Current.RootVisual).Transform(new Point(0, 0));

            if (this.infoPopup.PopupWidth > (locFSDPanel.X + this.FDSPanel.ActualWidth - popLoc.X))
            {
                VisualStateManager.GoToState(this.infoPopup, "Sinistra", true);
            }
            else if (popLoc.X < locFSDPanel.X + this.infoPopup.PopupWidth)
            {
                VisualStateManager.GoToState(this.infoPopup, "Destra", true);
            }
            this.infoPopup.Margin = new Thickness(popLoc.X, popLoc.Y - this.infoPopup.PopupHeight/2, 0.0, 0.0);
        }

        private void ResizeHtmlHostControl()
        {
            if (!String.IsNullOrEmpty(this.htmlControlHostId))
            {
                HtmlElement htmlElement = HtmlPage.Document.GetElementById(this.htmlControlHostId);
                if (htmlElement != null)
                {
                    htmlElement.SetStyleAttribute("height",
                                                  this.MeasureOverride(new Size(double.PositiveInfinity,
                                                                                double.PositiveInfinity)).Height + "px");
                }
            }
        }

        private void UISchedaDetail_SchedaDetailResize(object sender, EventArgs e)
        {
            this.ResizeHtmlHostControl();
        }

        private void autoRefreshTimer_Tick(object sender, EventArgs e)
        {
            this.LoadData();
        }
    }
}
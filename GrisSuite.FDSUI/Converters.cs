﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using GrisSuite.FDS.UI.BO;


namespace GrisSuite.FDS.UI
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is bool && (bool) value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is Visibility && ((Visibility) value) == Visibility.Visible;
        }
    }


    public class IsRemoteToBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is bool && (bool) value
                       ? Application.Current.Resources["RemoteBrush"]
                       : Application.Current.Resources["LocalBrush"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is SolidColorBrush && ((SolidColorBrush) value) == Application.Current.Resources["RemoteBrush"];
        }
    }

    public class IsRemoteToLocazioneConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is bool && (bool) value
                       ? "Remoto"
                       : "Locale";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is SolidColorBrush && ((SolidColorBrush) value) == Application.Current.Resources["RemoteBrush"];
        }
    }

    public class StateEnumToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is StateEnum)
            {
                switch ((StateEnum) value)
                {
                    case StateEnum.Error:
                        return Application.Current.Resources["ErrorBrush"];
                    case StateEnum.Warning:
                        return Application.Current.Resources["WarningBrush"];
                    case StateEnum.Ok:
                        return Application.Current.Resources["OkBrush"];
                }
            }
            return Application.Current.Resources["UnknownBrush"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Brush)
            {
                if (((Brush) value) == (Brush) Application.Current.Resources["ErrorBrush"])
                    return StateEnum.Error;

                if (((Brush) value) == (Brush) Application.Current.Resources["WarningBrush"])
                    return StateEnum.Warning;

                if (((Brush) value) == (Brush) Application.Current.Resources["OkBrush"])
                    return StateEnum.Ok;
            }
            return StateEnum.Unknown;
        }
    }
}
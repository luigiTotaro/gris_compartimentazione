﻿using System;
using System.Windows;
using GrisSuite.FDS.UI.BO;

namespace GrisSuite.FDS.UI
{
    public partial class UISchedaDetail
    {
        public event EventHandler SchedaDetailResize;

        private SchedaData scheda;

        public UISchedaDetail()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null && this.DataContext is SchedaData)
            {
                scheda = this.DataContext as SchedaData;
                scheda.PropertyChanged += scheda_PropertyChanged;

                expanderE1.Visibility = (scheda.AttributiE1 != null)
                                            ? Visibility.Visible
                                            : Visibility.Collapsed;
            }
        }

        void scheda_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsHighlighted")
                    VisualStateManager.GoToState(this, (scheda.IsHighlighted ? "MouseOver" : "Normal"), true);
        }

        private void UserCotrol_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (scheda != null)
                scheda.IsHighlighted = true;
        }

        private void UserControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (scheda != null) 
                scheda.IsHighlighted = false;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            SchedaData sc = this.DataContext as SchedaData;
            if (sc != null)
            {
                scheda.IsHighlighted = false;
                sc.IsObserved = !sc.IsObserved;
            }

            OnSchedaDetailResized(new EventArgs());
        }

        internal void OnSchedaDetailResized(EventArgs e)
        {
            EventHandler handler = SchedaDetailResize;

            if (handler != null)
            {
                SchedaDetailResize(this, e);
            }
        }

        private void expanderE1_Expanded(object sender, RoutedEventArgs e)
        {
            OnSchedaDetailResized(new EventArgs());
        }

        private void expanderE1_Collapsed(object sender, RoutedEventArgs e)
        {
            OnSchedaDetailResized(new EventArgs());
        }
    }
}

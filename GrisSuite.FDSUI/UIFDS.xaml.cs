﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GrisSuite.FDS.UI
{
    public partial class UIFDS : UserControl
    {
        public bool IsRemote
        {
            get { return (bool) GetValue(IsRemoteProperty); }
            set { SetValue(IsRemoteProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRemote.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRemoteProperty =
            DependencyProperty.Register("IsRemote", typeof (bool), typeof (UIFDS), new PropertyMetadata(false));

        public delegate void SchedaClickHandler(object sender, RoutedEventArgs e);

        public event SchedaClickHandler SchedaClick;

        public delegate void SchedaMouseHandler(object sender, MouseEventArgs e);

        public event SchedaMouseHandler SchedaMouseMove;
        public event SchedaMouseHandler SchedaMouseLeave;
        public event SchedaMouseHandler SchedaMouseEnter;

        public string Title
        {
            get { return (string) GetValue(TitleProperty); }
            set
            {
                TitleBlock.Text = value;
                SetValue(TitleProperty, value);
            }
        }

        public string LastUpdate
        {
            get { return (string) GetValue(LastUpdateProperty); }
            set
            {
                LastUpdateBlock.Text = value;
                SetValue(LastUpdateProperty, value);
            }
        }

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof (string),
                                                                                              typeof (UIFDS),
                                                                                              new PropertyMetadata(
                                                                                                  string.Empty));

        public static readonly DependencyProperty LastUpdateProperty = DependencyProperty.Register("LastUpdate",
                                                                                                   typeof (string),
                                                                                                   typeof (UIFDS),
                                                                                                   new PropertyMetadata(
                                                                                                       string.Empty));

        public UIFDS()
        {
            InitializeComponent();
        }

        private void UISchedaSmall_MouseEnter(object sender, MouseEventArgs e)
        {
            if (SchedaMouseEnter != null)
                SchedaMouseEnter(sender, e);
        }

        private void UISchedaSmall_MouseMove(object sender, MouseEventArgs e)
        {
            if (SchedaMouseMove != null)
                SchedaMouseMove(sender, e);
        }

        private void UISchedaSmall_MouseLeave(object sender, MouseEventArgs e)
        {
            if (SchedaMouseLeave != null)
                SchedaMouseLeave(sender, e);
        }

        private void UISchedaSmall_Click(object sender, RoutedEventArgs e)
        {
            if (SchedaClick != null)
                SchedaClick(sender, e);
        }
    }
}
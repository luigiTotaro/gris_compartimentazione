﻿
using System;
using System.Text;
using System.Security;
using System.Security.Cryptography;

class App
{
    static void Main(string[] argv)
    {
        Console.WriteLine("Program for generate keys validationKey and descriptioKey for Web.Config crypt, example:");
        Console.WriteLine("<machineKey\n validationKey=\"xxx\"  decryptionKey=\"yyy\" validation=\"SHA1\" decryption=\"AES\"/>");
        Console.WriteLine("The recommended key lengths are as follows: ");
        Console.WriteLine("For SHA1, set the validationKey to 64 bytes (128 hexadecimal characters)."); 
        Console.WriteLine("For AES, set the decryptionKey to 32 bytes (64 hexadecimal characters). ");
        Console.WriteLine("For 3DES, set the decryptionKey to 24 bytes (48 hexadecimal characters). ");

        int lenV = 128;
        if (argv.Length > 0)
            lenV = int.Parse(argv[0]);

        int lenD = 64;
        if (argv.Length > 1)
            lenD = int.Parse(argv[1]);

        Console.WriteLine("");

        Console.WriteLine("Syntax: keygen <lenValudationKey (default:128)> <lenDecryptionKey (default:64)> ");

        Console.WriteLine("");

        Console.WriteLine("validationKey:\n" + Keygen(lenV) + "\n");
        Console.WriteLine("decryptionKey:\n" + Keygen(lenD) + "\n");
    Console.ReadLine();

    }

    static string Keygen(int len)
    {
        byte[] buff = new byte[len / 2];
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetBytes(buff);
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < buff.Length; i++)
            sb.Append(string.Format("{0:X2}", buff[i]));
        return sb.ToString();

    }
}





﻿CREATE FUNCTION [dbo].[GetOnlyDate](@Date as Datetime)
--Ritorna solo la parte data, azzera l'ora
-- @Date	: prima data
RETURNS DATETIME AS
BEGIN
	RETURN CONVERT(DATETIME, CONVERT(VARCHAR(4),DATEPART(yyyy, @Date)) + '.' + CONVERT(VARCHAR(4),DATEPART(m, @Date)) + '.' + CONVERT(VARCHAR(4),DATEPART(d, @Date)),102)
END



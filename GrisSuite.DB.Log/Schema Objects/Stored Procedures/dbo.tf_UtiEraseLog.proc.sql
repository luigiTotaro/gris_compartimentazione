﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 17/11/2006
-- Description:	Elimina tutte le righe di log fino ad una certa data
-- =============================================
CREATE PROCEDURE [dbo].[tf_UtiEraseLog](@OlderThan datetime)
AS
BEGIN

BEGIN TRY
	BEGIN TRANSACTION
	DELETE FROM log_streams
	WHERE LogID In  (SELECT LogID FROM log_msg
						WHERE (Created < @OlderThan))

	DELETE FROM log_msg
	WHERE     (Created < @OlderThan)
	COMMIT 
	PRINT 'OK'
END TRY
BEGIN CATCH
	ROLLBACK 
	PRINT 'Error'
END CATCH

END



CREATE PROCEDURE [dbo].[tf_InsLog_Msg]
@HostSender VARCHAR (64), @Message IMAGE, @MessageType VARCHAR (255), @MAC VARCHAR (16) = NULL, @HostReceiver VARCHAR (64) = NULL
AS
SET NOCOUNT ON;
	DECLARE @NewId UNIQUEIDENTIFIER

	SET @NewId = NEWID();
	
	INSERT INTO log_msg	(LogID, Message, MessageType, HostSender, MAC, HostReceiver)
	VALUES (@NewId, @Message,@MessageType,@HostSender, @MAC, @HostReceiver)

	SELECT @NewId as LogID



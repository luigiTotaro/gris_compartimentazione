﻿CREATE PROCEDURE [dbo].[tf_UtiInvalidateMsgLogAnalyzed]
(@hoursToInvalidate int = 0)
AS
	declare @invalidateFrom datetime
	declare @invalidateTo datetime
	declare @records int

	set @invalidateto = (select max(created) from log_msg where analyzed = 1)
	set @invalidatefrom = DATEADD(hh,(@hoursToInvalidate * -1),@invalidateto)

	update log_msg
	set analyzed = 0
	where created >= @invalidatefrom
	and created <= @invalidateto
	and analyzed = 1

	Select @records = @@ROWCOUNT

	Delete from TelefinCentralPro.dbo.cache_log_messages
	where created >= @invalidatefrom
	and created <= @invalidateto

	RETURN @records;
﻿CREATE PROCEDURE [dbo].[tf_UtiLogAnalyzed] (@From DateTime)
AS 
SELECT GETDATE() as Adesso, 
	 
	 SUM(AnalizzatiOK) AS AnalizzatiOK,
	 SUM(AnalizzatiK0) AS AnalizzatiK0,
	 SUM(DaAnalizzare) AS DaAnalizzare,
	 MAX(OraUltimoOK) AS OraUltimoOK,
	 MAX(OraUltimoKO) AS OraUltimoKO,
	 MAX(OraUltimoDaAnalizzare) AS OraUltimoDaAnalizzare
FROM 
(
select count(*) as AnalizzatiOK, 0 as AnalizzatiK0, 0 as DaAnalizzare, MAX(Created) as OraUltimoOK, NULL AS OraUltimoKO, NULL as OraUltimoDaAnalizzare
from log_msg 
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 1) AND (Message IS NOT NULL)
union
select 0 as AnalizzatiOK, count(*) as AnalizzatiK0, 0 as DaAnalizzare,NULL as OraUltimoOK, MAX(Created) AS OraUltimoKO, NULL as OraUltimoDaAnalizzare
from log_msg  
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 2) AND (Message IS NOT NULL)
union
select 0 as AnalizzatiOK, 0 as AnalizzatiK0, count(*) as DaAnalizzare,NULL as OraUltimoOK, NULL AS OraUltimoKO, MAX(Created) as OraUltimoDaAnalizzare
from log_msg 
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 0) AND (Message IS NOT NULL)) AS A

RETURN 0;
CREATE PROCEDURE [dbo].[tf_UtiGetLastContact]
@HostSender VARCHAR (64)
AS
SELECT TOP 1 LogID, HostSender, Created, Message, MessageType, MAC, HostReceiver
FROM         log_msg
WHERE     (HostSender = @HostSender)
ORDER BY Created DESC



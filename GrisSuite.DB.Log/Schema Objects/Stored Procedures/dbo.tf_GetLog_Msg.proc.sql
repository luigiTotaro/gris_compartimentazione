CREATE PROCEDURE [dbo].[tf_GetLog_Msg]
@LogId UNIQUEIDENTIFIER=null, @DateFrom DATETIME=null, @DateTo DATETIME=null, @MessageTypes VARCHAR (MAX)=null, @HostSender VARCHAR (64)=null, @MinMessageSize INT=0, @Top INT=1000, @MAC VARCHAR (16)=null, @HostReceiver VARCHAR (64)=null
AS
SET NOCOUNT ON

	DECLARE	@DateFromN DateTime
	DECLARE	@DateToN DateTime

	SET @DateFromN = dbo.GetOnlyDate(ISNULL(@DateFrom, CONVERT(datetime,'1900-01-01',102)))
	SET @DateToN = dbo.GetOnlyDate(ISNULL(@DateTo, CONVERT(datetime,'9999-01-01',102)))
	SET @HostSender = ISNULL(@HostSender,'')
	SET @MessageTypes = ISNULL(@MessageTypes,'')
	SET @MAC = ISNULL(@MAC,'')
	SET @HostReceiver = ISNULL(@HostReceiver,'')

	SELECT TOP(@Top) LogID, HostSender, Created, Message, MessageType, MAC, HostReceiver
	FROM log_msg
	WHERE 
	(@LogID IS NULL OR  LogID = @LogID)
	AND Created BETWEEN @DateFromN AND @DateToN
	AND (@HostSender = '' OR HostSender = @HostSender)
	AND (@HostReceiver = '' OR IsNull(HostReceiver,'') = @HostReceiver)
	AND (@MAC = '' OR IsNull(MAC,'') = @MAC)
	AND (@MessageTypes = '' OR CHARINDEX(MessageType,@MessageTypes) > 0)
	AND (ISNULL(DATALENGTH(Message),0) >= @MinMessageSize)
	ORDER BY  Created DESC



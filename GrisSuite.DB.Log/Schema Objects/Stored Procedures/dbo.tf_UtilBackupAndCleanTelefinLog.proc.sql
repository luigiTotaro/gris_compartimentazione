﻿-- Esegue il backup del database e poi pulisce la tabella di msg_log_history 
-- @FileNameBase = Path e prefisso del file di backup da creare, Es.: e:\TelefinLogBackup\TelefinLog 
-- La procedura completa il @FileNameBase con la data minore e maggione dei dati di log contenuti 
-- nella tabella history e con l'estensione .bak 
CREATE PROCEDURE tf_UtilBackupAndCleanTelefinLog
	@FileNameBase VARCHAR(255)
	AS

	DECLARE @FileName VARCHAR(MAX)
	DECLARE @MinDate DateTime
	DECLARE @MaxDate DateTime
	DECLARE @Result INT

	IF EXISTS(SELECT TOP (1) LogId FROM log_msg_history)
	BEGIN
		IF OBJECT_ID('tempdb..##TelefinLogMutex') IS NOT NULL 
		BEGIN
			BEGIN TRY
				CREATE TABLE ##TelefinLogMutex (id int);

				-- trova la data minima e la massima
				SELECT @MinDate = MIN(Created), @MaxDate = Max(Created)
					FROM log_msg_history
					
				-- crea il nome del file di backup
				SET @FileName = @FileNameBase 
							+ REPLACE(REPLACE(REPLACE(CONVERT(varchar(20),@MinDate, 120),' ',''),'-',''),':','')
							+ 'to' 
							+ REPLACE(REPLACE(REPLACE(CONVERT(varchar(20),@MaxDate, 120),' ',''),'-',''),':','')
							+ '.bak'

				DECLARE @res INT
				

				-- fai backup
				BACKUP DATABASE TelefinLog TO DISK = @FileName  WITH FORMAT;

				-- tronca la tabella di storico
				TRUNCATE TABLE log_msg_history;

				-- ricostruzione indici
				DBCC DBREINDEX (log_msg)
				DBCC DBREINDEX (log_msg_history)
				
			END TRY
			BEGIN CATCH
				IF OBJECT_ID('tempdb..##TelefinLogMutex') IS NOT NULL DROP TABLE ##TelefinLogMutex
				RAISERROR('Errore in fase di backup e pulizia del database',16,1) 
			END CATCH		
			IF OBJECT_ID('tempdb..##TelefinLogMutex') IS NOT NULL DROP TABLE ##TelefinLogMutex
		END
		ELSE
		BEGIN
			RAISERROR('Backup e pulizia del database già in corso.',16,1) 
		END
	END
	ELSE
	BEGIN
		RAISERROR('History vuota pulizia non necessaria.',16,1) 
	END

RETURN
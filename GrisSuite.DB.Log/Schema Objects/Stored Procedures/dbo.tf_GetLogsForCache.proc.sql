CREATE PROCEDURE [dbo].[tf_GetLogsForCache]

AS
SELECT TOP (1500) LogID, HostSender, Created, Message, Analyzed, MessageType, MAC, HostReceiver
	FROM log_msg
	WHERE (Analyzed = 0) 
		AND (Message IS NOT NULL)
		AND ((MessageType = 'GrisSuite.Data.dsDeviceStatus')
		   OR (MessageType = 'GrisSuite.Data.dsConfig')
		   OR (MessageType = 'Telefin.dsDeviceStatus') 
    	   OR (MessageType = 'Telefin.dsConfig'))
	ORDER BY Created
RETURN 0;
﻿CREATE PROCEDURE [dbo].[uti_LogAnalyzed] (@From DateTime)
AS 
SELECT GETDATE() as Ora, 
	 SUM(AnalizzatiOK) AS AnalizzatiOK,
	 SUM(AnalizzatiK0) AS AnalizzatiK0,
	 SUM(DaAnalizzare) AS DaAnalizzare
FROM 
(
select count(*) as AnalizzatiOK, 0 as AnalizzatiK0, 0 as DaAnalizzare
from log_msg 
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 1) AND (Message IS NOT NULL)
union
select 0 as AnalizzatiOK, count(*) as AnalizzatiK0, 0 as DaAnalizzare
from log_msg  
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 2) AND (Message IS NOT NULL)
union
select 0 as AnalizzatiOK, 0 as AnalizzatiK0, count(*) as DaAnalizzare
from log_msg 
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 0) AND (Message IS NOT NULL)) AS A
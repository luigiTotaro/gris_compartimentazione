﻿CREATE PROCEDURE tf_UpdMoveToLogHistory (@OlderThan datetime)
AS
	IF OBJECT_ID('tempdb..##TelefinLogMutex') IS NULL 
		BEGIN 	
			begin tran
				INSERT INTO log_msg_history (LogID, HostSender, Created, Message, MessageType, Analyzed, MAC, HostReceiver)
				SELECT LogID, HostSender, Created, Message, MessageType, Analyzed, MAC, HostReceiver
				FROM log_msg
				WHERE Created <= @OlderThan 
				
				DELETE 
				FROM log_msg
				WHERE Created <= @OlderThan 
			commit tran
		END 
	ELSE
		BEGIN
			RAISERROR('Backup e pulizia del database in corso.',16,1)
		END
RETURN
﻿CREATE PROCEDURE [dbo].[tf_InsLog_Streams] 
(
	@LogID uniqueidentifier,
	@DevID bigint,
	@StrID int,
	@Name varchar(64),
	@Visible tinyint,
	@Data image,
	@DateTime datetime,
	@SetLevel int,
	@Description text
)
AS
	SET NOCOUNT ON;
	INSERT INTO log_streams
			   (LogID
			   ,DevID
			   ,StrID
			   ,[Name]
			   ,Visible
			   ,Data
			   ,[DateTime]
			   ,SetLevel
			   ,Description)
		 VALUES
			   (@LogID,
				@DevID,
				@StrID,
				@Name,
				@Visible,
				@Data,
				@DateTime,
				@SetLevel,
				@Description)



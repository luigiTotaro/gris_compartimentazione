﻿ALTER TABLE [dbo].[log_msg_history]
    ADD CONSTRAINT [PK_log_msg_history] PRIMARY KEY NONCLUSTERED ([LogID] ASC) WITH (FILLFACTOR = 70, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


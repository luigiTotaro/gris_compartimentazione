﻿CREATE TABLE [dbo].[log_streams]
(
[LogID] [uniqueidentifier] NOT NULL,
[DevID] [bigint] NOT NULL,
[StrID] [int] NOT NULL,
[Name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[Visible] [tinyint] NULL,
[Data] [image] NULL,
[DateTime] [datetime] NOT NULL,
[SetLevel] [int] NOT NULL,
[Description] [text] COLLATE Latin1_General_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]



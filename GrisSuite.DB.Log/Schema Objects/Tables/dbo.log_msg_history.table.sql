﻿CREATE TABLE [dbo].[log_msg_history] (
    [LogID]        UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [HostSender]   VARCHAR (64)     NOT NULL,
    [Created]      DATETIME         NOT NULL,
    [Message]      IMAGE            NULL,
    [MessageType]  VARCHAR (255)    NULL,
    [Analyzed]     TINYINT          NOT NULL,
    [MAC]          VARCHAR (16)     NULL,
    [HostReceiver] VARCHAR (64)     NULL
);


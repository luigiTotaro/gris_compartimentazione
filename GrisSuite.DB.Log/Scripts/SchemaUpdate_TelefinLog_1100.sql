/*
This script was created by Visual Studio on 17/12/2008 at 16.33.
Run this script on crisdev.TelefinLog2.dbo to make it the same as GrisSuite.LogDB.
Please back up your target database before running this script.
*/
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[GetOnlyDate]'
GO
CREATE FUNCTION [dbo].[GetOnlyDate](@Date as Datetime)
--Ritorna solo la parte data, azzera l'ora
-- @Date	: prima data
RETURNS DATETIME AS
BEGIN
	RETURN CONVERT(DATETIME, CONVERT(VARCHAR(4),DATEPART(yyyy, @Date)) + '.' + CONVERT(VARCHAR(4),DATEPART(m, @Date)) + '.' + CONVERT(VARCHAR(4),DATEPART(d, @Date)),102)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_UtiInvalidateMsgLogAnalyzed]'
GO
CREATE PROCEDURE [dbo].[tf_UtiInvalidateMsgLogAnalyzed]
(@hoursToInvalidate int = 0)
AS
	declare @invalidateFrom datetime
	declare @invalidateTo datetime
	declare @records int

	set @invalidateto = (select max(created) from log_msg where analyzed = 1)
	set @invalidatefrom = DATEADD(hh,(@hoursToInvalidate * -1),@invalidateto)

	update log_msg
	set analyzed = 0
	where created >= @invalidatefrom
	and created <= @invalidateto
	and analyzed = 1

	Select @records = @@ROWCOUNT

	Delete from TelefinCentralPro.dbo.cache_log_messages
	where created >= @invalidatefrom
	and created <= @invalidateto

	RETURN @records;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_UtiLogAnalyzed]'
GO
CREATE PROCEDURE [dbo].[tf_UtiLogAnalyzed] (@From DateTime)
AS 
SELECT GETDATE() as Adesso, 
	 
	 SUM(AnalizzatiOK) AS AnalizzatiOK,
	 SUM(AnalizzatiK0) AS AnalizzatiK0,
	 SUM(DaAnalizzare) AS DaAnalizzare,
	 MAX(OraUltimoOK) AS OraUltimoOK,
	 MAX(OraUltimoKO) AS OraUltimoKO,
	 MAX(OraUltimoDaAnalizzare) AS OraUltimoDaAnalizzare
FROM 
(
select count(*) as AnalizzatiOK, 0 as AnalizzatiK0, 0 as DaAnalizzare, MAX(Created) as OraUltimoOK, NULL AS OraUltimoKO, NULL as OraUltimoDaAnalizzare
from log_msg 
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 1) AND (Message IS NOT NULL)
union
select 0 as AnalizzatiOK, count(*) as AnalizzatiK0, 0 as DaAnalizzare,NULL as OraUltimoOK, MAX(Created) AS OraUltimoKO, NULL as OraUltimoDaAnalizzare
from log_msg  
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 2) AND (Message IS NOT NULL)
union
select 0 as AnalizzatiOK, 0 as AnalizzatiK0, count(*) as DaAnalizzare,NULL as OraUltimoOK, NULL AS OraUltimoKO, MAX(Created) as OraUltimoDaAnalizzare
from log_msg 
where (Created >= @From) AND (MessageType like '%dsDeviceStatus') AND (Analyzed = 0) AND (Message IS NOT NULL)) AS A

RETURN 0;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[tf_GetLogsForCache]'
GO
ALTER PROCEDURE [dbo].[tf_GetLogsForCache]
AS
	SELECT TOP (1500) LogID, HostSender, Created, Message, Analyzed, MessageType
	FROM log_msg
	WHERE (Analyzed = 0) 
		AND (Message IS NOT NULL)
		AND ((MessageType = 'GrisSuite.Data.dsDeviceStatus')
		   OR (MessageType = 'GrisSuite.Data.dsConfig')
		   OR (MessageType = 'Telefin.dsDeviceStatus') 
    	   OR (MessageType = 'Telefin.dsConfig'))
	ORDER BY Created
RETURN 0;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded.'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed.'
GO
DROP TABLE #tmpErrors
GO

/*
This script was created by Visual Studio on 03/11/2008 at 12.34.
Run this script on crisdev.TelefinLog.dbo to make it the same as GrisSuite.LogDB.
Please back up your target database before running this script.
*/
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[log_msg]'
GO
ALTER TABLE [dbo].[log_msg] DROP CONSTRAINT [PK_log]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[log_msg]'
GO
ALTER TABLE [dbo].[log_msg] DROP CONSTRAINT [DF_log_msg_Analyzed]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[log_streams]'
GO
ALTER TABLE [dbo].[log_streams] DROP CONSTRAINT [PK_log_streams]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_log_msg] from [dbo].[log_msg]'
GO
DROP INDEX [IX_log_msg] ON [dbo].[log_msg]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_CLUST_Created] from [dbo].[log_msg]'
GO
DROP INDEX [IX_CLUST_Created] ON [dbo].[log_msg]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[log_msg]'
GO
ALTER TABLE [dbo].[log_msg] ALTER COLUMN [Analyzed] [tinyint] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [idx_clust_Created_Desc] on [dbo].[log_msg]'
GO
CREATE CLUSTERED INDEX [idx_clust_Created_Desc] ON [dbo].[log_msg] ([Created] DESC) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_log] on [dbo].[log_msg]'
GO
ALTER TABLE [dbo].[log_msg] ADD CONSTRAINT [PK_log] PRIMARY KEY NONCLUSTERED  ([LogID]) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tf_GetLogsForCache]'
GO
CREATE PROCEDURE [dbo].[tf_GetLogsForCache]
AS
	SELECT TOP 100 LogID, HostSender, Created, Message, Analyzed, MessageType
	FROM log_msg
	WHERE (MessageType = 'Telefin.dsDeviceStatus') AND (Analyzed = 0) AND (Message IS NOT NULL)
	ORDER BY Created
RETURN 0;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [idx_clust_datetime_desc] on [dbo].[log_streams]'
GO
CREATE CLUSTERED INDEX [idx_clust_datetime_desc] ON [dbo].[log_streams] ([DateTime] DESC) WITH (FILLFACTOR=50) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_log_streams] on [dbo].[log_streams]'
GO
ALTER TABLE [dbo].[log_streams] ADD CONSTRAINT [PK_log_streams] PRIMARY KEY NONCLUSTERED  ([LogID], [DevID], [StrID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[log_msg]'
GO
ALTER TABLE [dbo].[log_msg] ADD CONSTRAINT [DF_log_msg_Analyzed] DEFAULT ((0)) FOR [Analyzed]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded.'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed.'
GO
DROP TABLE #tmpErrors
GO

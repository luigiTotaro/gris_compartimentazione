﻿CREATE PROCEDURE [dbo].[tf_UtiStatoRicezioneMessaggi]
AS 
BEGIN
	SELECT 
		CASE WHEN DATEDIFF(second, MAX(Created), GETDATE()) < 5 AND DATEDIFF(second, MIN(Created), GETDATE()) < 900 THEN 'OK'
			 WHEN DATEDIFF(second, MAX(Created), GETDATE()) >= 5 AND DATEDIFF(second, MIN(Created), GETDATE()) >= 900 THEN 'Problemi di centralizzazione messaggi e nel calcolo della cache'
			 WHEN DATEDIFF(second, MAX(Created), GETDATE()) >= 5 THEN 'Problemi nella ricezione dei messaggi'
			 ELSE 'Problemi nel calcolo della cache'
		END as Stato,
		GETDATE() as Adesso,  
		MAX(created) as UltimoMessaggio, 
		MIN(created) as PrimoMessaggio, 
		DATEDIFF(second, MAX(Created), GETDATE()) as DiffInSecondiUltimo,
		DATEDIFF(second, MIN(Created), GETDATE()) as DiffInSecondiPrimo,
		count(*) as NumeroMessaggi
	FROM log_msg
END
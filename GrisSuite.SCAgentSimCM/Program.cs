using System;
using System.Collections.Generic;
using System.Text;
using Telefin;

namespace GrisSuite.SCAgentSimCM
{
    class Program
    {
        static void Main(string[] args)
        {
            string configFilePath = "";
            if (args.Length > 0)
                configFilePath = args[0];

            if (System.IO.File.Exists(configFilePath))
            {
                try
                {
                    TestDS ds = new TestDS();
                    ds.ReadXml(configFilePath);
                    Simulator.Start(ds);
                    Console.WriteLine("Simulation in progress...");
                    System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error on start Simulation:" + ex.Message);
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("File not found:" + configFilePath);
                Console.ReadLine();
            }
        }
    }
}

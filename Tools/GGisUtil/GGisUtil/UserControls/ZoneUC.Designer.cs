﻿namespace GGisUtil.UserControls
{
    partial class ZoneUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAddSubZones = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtObjectStatusId = new System.Windows.Forms.TextBox();
            this.bindingSourceZones = new System.Windows.Forms.BindingSource(this.components);
            this.myData = new GGisUtil.MyData();
            this.label8 = new System.Windows.Forms.Label();
            this.txtZonId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboMapLayers = new System.Windows.Forms.ComboBox();
            this.mapLayersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.numCaptionFromScale = new System.Windows.Forms.NumericUpDown();
            this.numOffsetY = new System.Windows.Forms.NumericUpDown();
            this.numOffsetX = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboClassificationValues = new System.Windows.Forms.ComboBox();
            this.classificationValuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtKeepPoints = new System.Windows.Forms.NumericUpDown();
            this.btnBuildZonesMapPoints = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.subZoneCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NSegment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subZoneIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subzonesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cboZones = new System.Windows.Forms.ComboBox();
            this.zonesTableAdapter = new GGisUtil.MyDataTableAdapters.ZonesTableAdapter();
            this.subzonesTableAdapter = new GGisUtil.MyDataTableAdapters.ZonesSubzonesTableAdapter();
            this.classificationValuesTableAdapter = new GGisUtil.MyDataTableAdapters.ClassificationValuesTableAdapter();
            this.mapLayersTableAdapter = new GGisUtil.MyDataTableAdapters.MapLayerListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapLayersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCaptionFromScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classificationValuesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeepPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subzonesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddSubZones
            // 
            this.btnAddSubZones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddSubZones.Location = new System.Drawing.Point(165, 469);
            this.btnAddSubZones.Name = "btnAddSubZones";
            this.btnAddSubZones.Size = new System.Drawing.Size(109, 23);
            this.btnAddSubZones.TabIndex = 65;
            this.btnAddSubZones.Text = "Associa &Tratte";
            this.btnAddSubZones.UseVisualStyleBackColor = true;
            this.btnAddSubZones.Click += new System.EventHandler(this.btnAddSubZones_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(509, 473);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 66;
            this.label9.Text = "Mantieni un &punto ogni:";
            // 
            // txtObjectStatusId
            // 
            this.txtObjectStatusId.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceZones, "ObjectStatusId", true));
            this.txtObjectStatusId.Location = new System.Drawing.Point(87, 41);
            this.txtObjectStatusId.Name = "txtObjectStatusId";
            this.txtObjectStatusId.ReadOnly = true;
            this.txtObjectStatusId.Size = new System.Drawing.Size(322, 20);
            this.txtObjectStatusId.TabIndex = 49;
            // 
            // bindingSourceZones
            // 
            this.bindingSourceZones.DataMember = "Zones";
            this.bindingSourceZones.DataSource = this.myData;
            // 
            // myData
            // 
            this.myData.DataSetName = "MyData";
            this.myData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "ObjectStatusId";
            // 
            // txtZonId
            // 
            this.txtZonId.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceZones, "ZonID", true));
            this.txtZonId.Location = new System.Drawing.Point(463, 41);
            this.txtZonId.Name = "txtZonId";
            this.txtZonId.ReadOnly = true;
            this.txtZonId.Size = new System.Drawing.Size(176, 20);
            this.txtZonId.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(414, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 50;
            this.label7.Text = "ZonId";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 58;
            this.label6.Text = "Lay&er";
            // 
            // cboMapLayers
            // 
            this.cboMapLayers.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSourceZones, "MapLayerId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cboMapLayers.DataSource = this.mapLayersBindingSource;
            this.cboMapLayers.DisplayMember = "Description";
            this.cboMapLayers.FormattingEnabled = true;
            this.cboMapLayers.Location = new System.Drawing.Point(89, 93);
            this.cboMapLayers.Name = "cboMapLayers";
            this.cboMapLayers.Size = new System.Drawing.Size(320, 21);
            this.cboMapLayers.TabIndex = 59;
            this.cboMapLayers.ValueMember = "MapLayerId";
            // 
            // mapLayersBindingSource
            // 
            this.mapLayersBindingSource.DataMember = "MapLayerList";
            this.mapLayersBindingSource.DataSource = this.myData;
            // 
            // numCaptionFromScale
            // 
            this.numCaptionFromScale.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceZones, "CaptionFromScale", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numCaptionFromScale.Location = new System.Drawing.Point(583, 95);
            this.numCaptionFromScale.Name = "numCaptionFromScale";
            this.numCaptionFromScale.Size = new System.Drawing.Size(56, 20);
            this.numCaptionFromScale.TabIndex = 61;
            // 
            // numOffsetY
            // 
            this.numOffsetY.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceZones, "MapOffsetY", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numOffsetY.Location = new System.Drawing.Point(584, 67);
            this.numOffsetY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numOffsetY.Name = "numOffsetY";
            this.numOffsetY.Size = new System.Drawing.Size(56, 20);
            this.numOffsetY.TabIndex = 57;
            // 
            // numOffsetX
            // 
            this.numOffsetX.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceZones, "MapOffsetX", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numOffsetX.Location = new System.Drawing.Point(463, 67);
            this.numOffsetX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numOffsetX.Name = "numOffsetX";
            this.numOffsetX.Size = new System.Drawing.Size(56, 20);
            this.numOffsetX.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(484, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 60;
            this.label5.Text = "&CaptionFromScale";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(536, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 56;
            this.label4.Text = "Offset&Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(415, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "Offset&X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "&Metallo";
            // 
            // cboClassificationValues
            // 
            this.cboClassificationValues.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSourceZones, "ClassificationValueId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cboClassificationValues.DataSource = this.classificationValuesBindingSource;
            this.cboClassificationValues.DisplayMember = "ValueName";
            this.cboClassificationValues.FormattingEnabled = true;
            this.cboClassificationValues.Location = new System.Drawing.Point(89, 67);
            this.cboClassificationValues.Name = "cboClassificationValues";
            this.cboClassificationValues.Size = new System.Drawing.Size(320, 21);
            this.cboClassificationValues.TabIndex = 53;
            this.cboClassificationValues.ValueMember = "ClassificationValueId";
            this.cboClassificationValues.SelectedIndexChanged += new System.EventHandler(this.cboClassificationValues_SelectedIndexChanged);
            // 
            // classificationValuesBindingSource
            // 
            this.classificationValuesBindingSource.DataMember = "ClassificationValues";
            this.classificationValuesBindingSource.DataSource = this.myData;
            // 
            // txtKeepPoints
            // 
            this.txtKeepPoints.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeepPoints.Location = new System.Drawing.Point(633, 469);
            this.txtKeepPoints.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtKeepPoints.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtKeepPoints.Name = "txtKeepPoints";
            this.txtKeepPoints.Size = new System.Drawing.Size(59, 20);
            this.txtKeepPoints.TabIndex = 67;
            this.txtKeepPoints.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btnBuildZonesMapPoints
            // 
            this.btnBuildZonesMapPoints.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuildZonesMapPoints.Location = new System.Drawing.Point(698, 468);
            this.btnBuildZonesMapPoints.Name = "btnBuildZonesMapPoints";
            this.btnBuildZonesMapPoints.Size = new System.Drawing.Size(97, 23);
            this.btnBuildZonesMapPoints.TabIndex = 68;
            this.btnBuildZonesMapPoints.Text = "Crea Linea &GIS";
            this.btnBuildZonesMapPoints.UseVisualStyleBackColor = true;
            this.btnBuildZonesMapPoints.Click += new System.EventHandler(this.btnBuildZonesMapPoints_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(3, 468);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 63;
            this.btnRefresh.Text = "&Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(84, 468);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 64;
            this.btnSave.Text = "&Salva";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.subZoneCodeDataGridViewTextBoxColumn,
            this.NSegment,
            this.nameDataGridViewTextBoxColumn,
            this.subZoneIdDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.subzonesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 120);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 341);
            this.dataGridView1.TabIndex = 62;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // subZoneCodeDataGridViewTextBoxColumn
            // 
            this.subZoneCodeDataGridViewTextBoxColumn.DataPropertyName = "SubZoneCode";
            this.subZoneCodeDataGridViewTextBoxColumn.HeaderText = "Codice Tratta";
            this.subZoneCodeDataGridViewTextBoxColumn.Name = "subZoneCodeDataGridViewTextBoxColumn";
            // 
            // NSegment
            // 
            this.NSegment.DataPropertyName = "NSegment";
            this.NSegment.HeaderText = "Segmento";
            this.NSegment.Name = "NSegment";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Tratta";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // subZoneIdDataGridViewTextBoxColumn
            // 
            this.subZoneIdDataGridViewTextBoxColumn.DataPropertyName = "SubZoneId";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.subZoneIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.subZoneIdDataGridViewTextBoxColumn.HeaderText = "SubZoneId";
            this.subZoneIdDataGridViewTextBoxColumn.Name = "subZoneIdDataGridViewTextBoxColumn";
            this.subZoneIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // subzonesBindingSource
            // 
            this.subzonesBindingSource.DataMember = "ZonesSubzones";
            this.subzonesBindingSource.DataSource = this.myData;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "&Linea";
            // 
            // cboZones
            // 
            this.cboZones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboZones.DataSource = this.bindingSourceZones;
            this.cboZones.DisplayMember = "Name";
            this.cboZones.FormattingEnabled = true;
            this.cboZones.Location = new System.Drawing.Point(87, 8);
            this.cboZones.Name = "cboZones";
            this.cboZones.Size = new System.Drawing.Size(684, 21);
            this.cboZones.TabIndex = 47;
            this.cboZones.ValueMember = "ZonID";
            this.cboZones.SelectedIndexChanged += new System.EventHandler(this.cboZones_SelectedIndexChanged);
            // 
            // zonesTableAdapter
            // 
            this.zonesTableAdapter.ClearBeforeFill = true;
            // 
            // subzonesTableAdapter
            // 
            this.subzonesTableAdapter.ClearBeforeFill = true;
            // 
            // classificationValuesTableAdapter
            // 
            this.classificationValuesTableAdapter.ClearBeforeFill = true;
            // 
            // mapLayersTableAdapter
            // 
            this.mapLayersTableAdapter.ClearBeforeFill = true;
            // 
            // ZoneUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnAddSubZones);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtObjectStatusId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtZonId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboMapLayers);
            this.Controls.Add(this.numCaptionFromScale);
            this.Controls.Add(this.numOffsetY);
            this.Controls.Add(this.numOffsetX);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboClassificationValues);
            this.Controls.Add(this.txtKeepPoints);
            this.Controls.Add(this.btnBuildZonesMapPoints);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboZones);
            this.Name = "ZoneUC";
            this.Size = new System.Drawing.Size(800, 500);
            this.Load += new System.EventHandler(this.ZoneUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapLayersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCaptionFromScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classificationValuesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeepPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subzonesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddSubZones;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtObjectStatusId;
        private System.Windows.Forms.BindingSource bindingSourceZones;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtZonId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboMapLayers;
        private System.Windows.Forms.BindingSource mapLayersBindingSource;
        private System.Windows.Forms.NumericUpDown numCaptionFromScale;
        private System.Windows.Forms.NumericUpDown numOffsetY;
        private System.Windows.Forms.NumericUpDown numOffsetX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboClassificationValues;
        private System.Windows.Forms.BindingSource classificationValuesBindingSource;
        private System.Windows.Forms.NumericUpDown txtKeepPoints;
        private System.Windows.Forms.Button btnBuildZonesMapPoints;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource subzonesBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboZones;
        private MyData myData;
        private GGisUtil.MyDataTableAdapters.ZonesTableAdapter zonesTableAdapter;
        private GGisUtil.MyDataTableAdapters.ZonesSubzonesTableAdapter subzonesTableAdapter;
        private GGisUtil.MyDataTableAdapters.ClassificationValuesTableAdapter classificationValuesTableAdapter;
        private GGisUtil.MyDataTableAdapters.MapLayerListTableAdapter mapLayersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn subZoneCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NSegment;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subZoneIdDataGridViewTextBoxColumn;


    }
}

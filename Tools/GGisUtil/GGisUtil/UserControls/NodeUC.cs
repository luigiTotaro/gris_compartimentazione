﻿using System;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

namespace GGisUtil.UserControls
{
    public partial class NodeUC : UserControl
    {
        public NodeUC()
        {
            this.InitializeComponent();
        }

        private void NodeUC_Load(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void LoadData()
        {
            int savedRowIndex = 0;
            if (this.dataGridView1.FirstDisplayedCell != null)
            {
                savedRowIndex = this.dataGridView1.FirstDisplayedScrollingRowIndex;
            }

            this.classificationValuesTableAdapter.Fill(this.myData.ClassificationValues);
            this.mapLayersTableAdapter.Fill(this.myData.MapLayerList);
            this.nodesTableAdapter.Fill(this.myData.Nodes);

            if (savedRowIndex > 0)
            {
                this.dataGridView1.FirstDisplayedScrollingRowIndex = savedRowIndex;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Validate())
                {
                    this.bindingNodes.EndEdit();
                    this.nodesTableAdapter.Update(this.myData.Nodes);

                    // E' possibile che l'aggiornamento della stazione legga i dati geografici dalla tabella di RFI, quindi occorre
                    // ricaricare completamente la tabella dopo il salvataggio corretto
                    this.LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Errore: " + ex.Message, "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(this, "Errore nella validazione:" + e.Exception.Message, "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "MapPoints")
            {
                string validValue;

                if (this.ValidateMapPoint(e.FormattedValue.ToString(), out validValue))
                {
                    MyData.NodesRow nodeRow = (MyData.NodesRow)((DataRowView)this.dataGridView1.Rows[e.RowIndex].DataBoundItem).Row;

                    if (!e.FormattedValue.ToString().Equals(nodeRow.MapPoints))
                    {
                        nodeRow.MapPoints = validValue;
                    }
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].ErrorText = "Il formato del MapPoints deve essere del tipo \"point latitudine.decimali longitudine.decimali\", ad esempio \"point 40.12345578 15.87654321";
                    e.Cancel = true;                    
                }
            }
        }

        private bool ValidateMapPoint(string value, out string validValue)
        {
            validValue = null;

            // point  841.13506653  14.18857099
            if (!String.IsNullOrEmpty(value))
            {
                value = value.ToLowerInvariant().Trim().Replace("     ", " ").Replace("    ", " ").Replace("   ", " ").Replace("  ", " ").Replace("  ", " ");

                string[] pointComponents = value.Split(' ');

                if (pointComponents.Length == 3)
                {
                    string point = pointComponents[0].Trim();
                    string latitudeString = pointComponents[1].Trim().Replace(",", ".");
                    string longitudeString = pointComponents[2].Trim().Replace(",", ".");

                    double latitude;
                    double longitude;

                    if ((point.Equals("point")) && (double.TryParse(latitudeString, NumberStyles.Number,CultureInfo.InvariantCulture, out latitude)) && (double.TryParse(longitudeString, NumberStyles.Number,CultureInfo.InvariantCulture, out longitude)))
                    {
                        if (((latitude >= -90) && (latitude <= 90)) && ((longitude >= -180) && (longitude <= 180)))
                        {
                            validValue = String.Format("{0}  {1}  {2}", point, latitude.ToString("0.########").Replace(",", "."), longitude.ToString("0.########").Replace(",", "."));

                            return true;
                        }
                    }
                }

                return false;
            }

            return true;
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            // Clear the row error in case the user presses ESC.   
            this.dataGridView1.Rows[e.RowIndex].ErrorText = String.Empty;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GGisUtil.UserControls
{
    public partial class MapLayerUC : UserControl
    {
        public MapLayerUC()
        {
            InitializeComponent();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Validate())
                {
                    mapLayersBindingSource.EndEdit();
                    this.mapLayersTableAdapter.Update(this.myData.MapLayers);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Errore :" + ex.Message, "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }


        private void LoadData()
        {
            this.classificationValuesTableAdapter.Fill(this.myData.ClassificationValues);
            this.mapLayersTableAdapter.Fill(this.myData.MapLayers);

        }

        private void MapLayerUC_Load(object sender, EventArgs e)
        {
            LoadData();

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(this, "Errore nella valitazione :" + e.Exception.Message, "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}

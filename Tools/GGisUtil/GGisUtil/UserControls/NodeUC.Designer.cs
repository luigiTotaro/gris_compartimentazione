﻿namespace GGisUtil.UserControls
{
    partial class NodeUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.classificationValuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.myData = new GGisUtil.MyData();
            this.mapLayersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNodes = new System.Windows.Forms.BindingSource(this.components);
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.nodesTableAdapter = new GGisUtil.MyDataTableAdapters.NodesTableAdapter();
            this.mapLayersTableAdapter = new GGisUtil.MyDataTableAdapters.MapLayerListTableAdapter();
            this.classificationValuesTableAdapter = new GGisUtil.MyDataTableAdapters.ClassificationValuesTableAdapter();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameRFI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classificationValueIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.MetalloRFI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MapPoints = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mapOffsetXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mapOffsetYDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.captionFromScaleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mapLayerIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.objectStatusIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DecodedNodId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NodID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HasNoDevice = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classificationValuesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapLayersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNodes)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.NameRFI,
            this.classificationValueIdDataGridViewTextBoxColumn,
            this.MetalloRFI,
            this.MapPoints,
            this.mapOffsetXDataGridViewTextBoxColumn,
            this.mapOffsetYDataGridViewTextBoxColumn,
            this.captionFromScaleDataGridViewTextBoxColumn,
            this.mapLayerIdDataGridViewTextBoxColumn,
            this.objectStatusIdDataGridViewTextBoxColumn,
            this.DecodedNodId,
            this.NodID,
            this.HasNoDevice});
            this.dataGridView1.DataSource = this.bindingNodes;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(794, 456);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // classificationValuesBindingSource
            // 
            this.classificationValuesBindingSource.DataMember = "ClassificationValues";
            this.classificationValuesBindingSource.DataSource = this.myData;
            // 
            // myData
            // 
            this.myData.DataSetName = "MyData";
            this.myData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mapLayersBindingSource
            // 
            this.mapLayersBindingSource.DataMember = "MapLayerList";
            this.mapLayersBindingSource.DataSource = this.myData;
            // 
            // bindingNodes
            // 
            this.bindingNodes.AllowNew = false;
            this.bindingNodes.DataMember = "Nodes";
            this.bindingNodes.DataSource = this.myData;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(3, 468);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 65;
            this.btnRefresh.Text = "&Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(84, 468);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 66;
            this.btnSave.Text = "&Salva";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // nodesTableAdapter
            // 
            this.nodesTableAdapter.ClearBeforeFill = true;
            // 
            // mapLayersTableAdapter
            // 
            this.mapLayersTableAdapter.ClearBeforeFill = true;
            // 
            // classificationValuesTableAdapter
            // 
            this.classificationValuesTableAdapter.ClearBeforeFill = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.MinimumWidth = 50;
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 180;
            // 
            // NameRFI
            // 
            this.NameRFI.DataPropertyName = "NameRFI";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.NameRFI.DefaultCellStyle = dataGridViewCellStyle1;
            this.NameRFI.HeaderText = "NameRFI";
            this.NameRFI.MinimumWidth = 50;
            this.NameRFI.Name = "NameRFI";
            this.NameRFI.ReadOnly = true;
            this.NameRFI.Width = 180;
            // 
            // classificationValueIdDataGridViewTextBoxColumn
            // 
            this.classificationValueIdDataGridViewTextBoxColumn.DataPropertyName = "ClassificationValueId";
            this.classificationValueIdDataGridViewTextBoxColumn.DataSource = this.classificationValuesBindingSource;
            this.classificationValueIdDataGridViewTextBoxColumn.DisplayMember = "ValueName";
            this.classificationValueIdDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.classificationValueIdDataGridViewTextBoxColumn.HeaderText = "Metallo";
            this.classificationValueIdDataGridViewTextBoxColumn.MinimumWidth = 30;
            this.classificationValueIdDataGridViewTextBoxColumn.Name = "classificationValueIdDataGridViewTextBoxColumn";
            this.classificationValueIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.classificationValueIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.classificationValueIdDataGridViewTextBoxColumn.ValueMember = "ClassificationValueId";
            this.classificationValueIdDataGridViewTextBoxColumn.Width = 150;
            // 
            // MetalloRFI
            // 
            this.MetalloRFI.DataPropertyName = "MetalloRFI";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MetalloRFI.DefaultCellStyle = dataGridViewCellStyle2;
            this.MetalloRFI.HeaderText = "MetalloRFI";
            this.MetalloRFI.Name = "MetalloRFI";
            this.MetalloRFI.ReadOnly = true;
            this.MetalloRFI.Width = 150;
            // 
            // MapPoints
            // 
            this.MapPoints.DataPropertyName = "MapPoints";
            this.MapPoints.HeaderText = "MapPoints";
            this.MapPoints.MinimumWidth = 30;
            this.MapPoints.Name = "MapPoints";
            this.MapPoints.Width = 150;
            // 
            // mapOffsetXDataGridViewTextBoxColumn
            // 
            this.mapOffsetXDataGridViewTextBoxColumn.DataPropertyName = "MapOffsetX";
            this.mapOffsetXDataGridViewTextBoxColumn.HeaderText = "Offset X";
            this.mapOffsetXDataGridViewTextBoxColumn.MaxInputLength = 5;
            this.mapOffsetXDataGridViewTextBoxColumn.Name = "mapOffsetXDataGridViewTextBoxColumn";
            this.mapOffsetXDataGridViewTextBoxColumn.Width = 40;
            // 
            // mapOffsetYDataGridViewTextBoxColumn
            // 
            this.mapOffsetYDataGridViewTextBoxColumn.DataPropertyName = "MapOffsetY";
            this.mapOffsetYDataGridViewTextBoxColumn.HeaderText = "Offset Y";
            this.mapOffsetYDataGridViewTextBoxColumn.MaxInputLength = 5;
            this.mapOffsetYDataGridViewTextBoxColumn.Name = "mapOffsetYDataGridViewTextBoxColumn";
            this.mapOffsetYDataGridViewTextBoxColumn.Width = 40;
            // 
            // captionFromScaleDataGridViewTextBoxColumn
            // 
            this.captionFromScaleDataGridViewTextBoxColumn.DataPropertyName = "CaptionFromScale";
            this.captionFromScaleDataGridViewTextBoxColumn.HeaderText = "Caption From Scale";
            this.captionFromScaleDataGridViewTextBoxColumn.MaxInputLength = 5;
            this.captionFromScaleDataGridViewTextBoxColumn.Name = "captionFromScaleDataGridViewTextBoxColumn";
            this.captionFromScaleDataGridViewTextBoxColumn.Width = 90;
            // 
            // mapLayerIdDataGridViewTextBoxColumn
            // 
            this.mapLayerIdDataGridViewTextBoxColumn.DataPropertyName = "MapLayerId";
            this.mapLayerIdDataGridViewTextBoxColumn.DataSource = this.mapLayersBindingSource;
            this.mapLayerIdDataGridViewTextBoxColumn.DisplayMember = "Description";
            this.mapLayerIdDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mapLayerIdDataGridViewTextBoxColumn.HeaderText = "Layer";
            this.mapLayerIdDataGridViewTextBoxColumn.MinimumWidth = 30;
            this.mapLayerIdDataGridViewTextBoxColumn.Name = "mapLayerIdDataGridViewTextBoxColumn";
            this.mapLayerIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.mapLayerIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.mapLayerIdDataGridViewTextBoxColumn.ValueMember = "MapLayerId";
            this.mapLayerIdDataGridViewTextBoxColumn.Width = 150;
            // 
            // objectStatusIdDataGridViewTextBoxColumn
            // 
            this.objectStatusIdDataGridViewTextBoxColumn.DataPropertyName = "ObjectStatusId";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.objectStatusIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.objectStatusIdDataGridViewTextBoxColumn.HeaderText = "ObjectStatusId";
            this.objectStatusIdDataGridViewTextBoxColumn.Name = "objectStatusIdDataGridViewTextBoxColumn";
            this.objectStatusIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // DecodedNodId
            // 
            this.DecodedNodId.DataPropertyName = "DecodedNodId";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DecodedNodId.DefaultCellStyle = dataGridViewCellStyle4;
            this.DecodedNodId.HeaderText = "DecodedNodId";
            this.DecodedNodId.Name = "DecodedNodId";
            this.DecodedNodId.ReadOnly = true;
            // 
            // NodID
            // 
            this.NodID.DataPropertyName = "NodID";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.NodID.DefaultCellStyle = dataGridViewCellStyle5;
            this.NodID.HeaderText = "NodID";
            this.NodID.Name = "NodID";
            this.NodID.ReadOnly = true;
            // 
            // HasNoDevice
            // 
            this.HasNoDevice.DataPropertyName = "HasNoDevice";
            this.HasNoDevice.HeaderText = "NoDevice";
            this.HasNoDevice.MinimumWidth = 80;
            this.HasNoDevice.Name = "HasNoDevice";
            this.HasNoDevice.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.HasNoDevice.Width = 80;
            // 
            // NodeUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dataGridView1);
            this.Name = "NodeUC";
            this.Size = new System.Drawing.Size(800, 500);
            this.Load += new System.EventHandler(this.NodeUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classificationValuesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapLayersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNodes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingNodes;
        private MyData myData;
        private GGisUtil.MyDataTableAdapters.NodesTableAdapter nodesTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource mapLayersBindingSource;
        private GGisUtil.MyDataTableAdapters.MapLayerListTableAdapter mapLayersTableAdapter;
        private System.Windows.Forms.BindingSource classificationValuesBindingSource;
        private GGisUtil.MyDataTableAdapters.ClassificationValuesTableAdapter classificationValuesTableAdapter;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameRFI;
        private System.Windows.Forms.DataGridViewComboBoxColumn classificationValueIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MetalloRFI;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapPoints;
        private System.Windows.Forms.DataGridViewTextBoxColumn mapOffsetXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mapOffsetYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn captionFromScaleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn mapLayerIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn objectStatusIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DecodedNodId;
        private System.Windows.Forms.DataGridViewTextBoxColumn NodID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn HasNoDevice;

    }
}

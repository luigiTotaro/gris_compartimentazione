﻿namespace GGisUtil.UserControls
{
    partial class MapLayerUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.mapLayersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.myData = new GGisUtil.MyData();
            this.classificationValuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mapLayersTableAdapter = new GGisUtil.MyDataTableAdapters.MapLayersTableAdapter();
            this.classificationValuesTableAdapter = new GGisUtil.MyDataTableAdapters.ClassificationValuesTableAdapter();
            this.layerSequenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shapefileUriDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visibleFromScaleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visibleToScaleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.symbolSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CaptionFromScale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isSensitiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.showElementsSymbolsDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.showElementsTooltipsDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.showElementsStatusDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mapLayerIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapLayersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classificationValuesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(3, 468);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 67;
            this.btnRefresh.Text = "&Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(84, 468);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 68;
            this.btnSave.Text = "&Salva";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.layerSequenceDataGridViewTextBoxColumn,
            this.shapefileUriDataGridViewTextBoxColumn,
            this.visibleFromScaleDataGridViewTextBoxColumn,
            this.visibleToScaleDataGridViewTextBoxColumn,
            this.symbolSizeDataGridViewTextBoxColumn,
            this.CaptionFromScale,
            this.isSensitiveDataGridViewCheckBoxColumn,
            this.showElementsSymbolsDataGridViewCheckBoxColumn,
            this.showElementsTooltipsDataGridViewCheckBoxColumn,
            this.showElementsStatusDataGridViewCheckBoxColumn,
            this.mapLayerIdDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.mapLayersBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 457);
            this.dataGridView1.TabIndex = 69;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // mapLayersBindingSource
            // 
            this.mapLayersBindingSource.DataMember = "MapLayers";
            this.mapLayersBindingSource.DataSource = this.myData;
            // 
            // myData
            // 
            this.myData.DataSetName = "MyData";
            this.myData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // classificationValuesBindingSource
            // 
            this.classificationValuesBindingSource.DataMember = "ClassificationValues";
            this.classificationValuesBindingSource.DataSource = this.myData;
            // 
            // mapLayersTableAdapter
            // 
            this.mapLayersTableAdapter.ClearBeforeFill = true;
            // 
            // classificationValuesTableAdapter
            // 
            this.classificationValuesTableAdapter.ClearBeforeFill = true;
            // 
            // layerSequenceDataGridViewTextBoxColumn
            // 
            this.layerSequenceDataGridViewTextBoxColumn.DataPropertyName = "LayerSequence";
            this.layerSequenceDataGridViewTextBoxColumn.HeaderText = "Layer Sequence";
            this.layerSequenceDataGridViewTextBoxColumn.MaxInputLength = 4;
            this.layerSequenceDataGridViewTextBoxColumn.Name = "layerSequenceDataGridViewTextBoxColumn";
            // 
            // shapefileUriDataGridViewTextBoxColumn
            // 
            this.shapefileUriDataGridViewTextBoxColumn.DataPropertyName = "ShapefileUri";
            this.shapefileUriDataGridViewTextBoxColumn.HeaderText = "Shapefile Uri";
            this.shapefileUriDataGridViewTextBoxColumn.MaxInputLength = 50;
            this.shapefileUriDataGridViewTextBoxColumn.Name = "shapefileUriDataGridViewTextBoxColumn";
            // 
            // visibleFromScaleDataGridViewTextBoxColumn
            // 
            this.visibleFromScaleDataGridViewTextBoxColumn.DataPropertyName = "VisibleFromScale";
            this.visibleFromScaleDataGridViewTextBoxColumn.HeaderText = "Visible From Scale";
            this.visibleFromScaleDataGridViewTextBoxColumn.MaxInputLength = 5;
            this.visibleFromScaleDataGridViewTextBoxColumn.Name = "visibleFromScaleDataGridViewTextBoxColumn";
            // 
            // visibleToScaleDataGridViewTextBoxColumn
            // 
            this.visibleToScaleDataGridViewTextBoxColumn.DataPropertyName = "VisibleToScale";
            this.visibleToScaleDataGridViewTextBoxColumn.HeaderText = "Visible To Scale";
            this.visibleToScaleDataGridViewTextBoxColumn.MaxInputLength = 5;
            this.visibleToScaleDataGridViewTextBoxColumn.Name = "visibleToScaleDataGridViewTextBoxColumn";
            // 
            // symbolSizeDataGridViewTextBoxColumn
            // 
            this.symbolSizeDataGridViewTextBoxColumn.DataPropertyName = "SymbolSize";
            this.symbolSizeDataGridViewTextBoxColumn.HeaderText = "Symbol Size";
            this.symbolSizeDataGridViewTextBoxColumn.MaxInputLength = 5;
            this.symbolSizeDataGridViewTextBoxColumn.Name = "symbolSizeDataGridViewTextBoxColumn";
            // 
            // CaptionFromScale
            // 
            this.CaptionFromScale.DataPropertyName = "CaptionFromScale";
            this.CaptionFromScale.HeaderText = "Caption From Scale";
            this.CaptionFromScale.MaxInputLength = 5;
            this.CaptionFromScale.Name = "CaptionFromScale";
            // 
            // isSensitiveDataGridViewCheckBoxColumn
            // 
            this.isSensitiveDataGridViewCheckBoxColumn.DataPropertyName = "IsSensitive";
            this.isSensitiveDataGridViewCheckBoxColumn.HeaderText = "Is Sensitive";
            this.isSensitiveDataGridViewCheckBoxColumn.Name = "isSensitiveDataGridViewCheckBoxColumn";
            // 
            // showElementsSymbolsDataGridViewCheckBoxColumn
            // 
            this.showElementsSymbolsDataGridViewCheckBoxColumn.DataPropertyName = "ShowElementsSymbols";
            this.showElementsSymbolsDataGridViewCheckBoxColumn.HeaderText = "Show Elements Symbols";
            this.showElementsSymbolsDataGridViewCheckBoxColumn.Name = "showElementsSymbolsDataGridViewCheckBoxColumn";
            // 
            // showElementsTooltipsDataGridViewCheckBoxColumn
            // 
            this.showElementsTooltipsDataGridViewCheckBoxColumn.DataPropertyName = "ShowElementsTooltips";
            this.showElementsTooltipsDataGridViewCheckBoxColumn.HeaderText = "Show Elements Tooltips";
            this.showElementsTooltipsDataGridViewCheckBoxColumn.Name = "showElementsTooltipsDataGridViewCheckBoxColumn";
            // 
            // showElementsStatusDataGridViewCheckBoxColumn
            // 
            this.showElementsStatusDataGridViewCheckBoxColumn.DataPropertyName = "ShowElementsStatus";
            this.showElementsStatusDataGridViewCheckBoxColumn.HeaderText = "Show Elements Status";
            this.showElementsStatusDataGridViewCheckBoxColumn.Name = "showElementsStatusDataGridViewCheckBoxColumn";
            // 
            // mapLayerIdDataGridViewTextBoxColumn
            // 
            this.mapLayerIdDataGridViewTextBoxColumn.DataPropertyName = "MapLayerId";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mapLayerIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.mapLayerIdDataGridViewTextBoxColumn.HeaderText = "MapLayerId";
            this.mapLayerIdDataGridViewTextBoxColumn.MaxInputLength = 4;
            this.mapLayerIdDataGridViewTextBoxColumn.Name = "mapLayerIdDataGridViewTextBoxColumn";
            this.mapLayerIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // MapLayerUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.Name = "MapLayerUC";
            this.Size = new System.Drawing.Size(800, 500);
            this.Load += new System.EventHandler(this.MapLayerUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapLayersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classificationValuesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource classificationValuesBindingSource;
        private MyData myData;
        private System.Windows.Forms.BindingSource mapLayersBindingSource;
        private GGisUtil.MyDataTableAdapters.MapLayersTableAdapter mapLayersTableAdapter;
        private GGisUtil.MyDataTableAdapters.ClassificationValuesTableAdapter classificationValuesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn layerSequenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shapefileUriDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn visibleFromScaleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn visibleToScaleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn symbolSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CaptionFromScale;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isSensitiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showElementsSymbolsDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showElementsTooltipsDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showElementsStatusDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mapLayerIdDataGridViewTextBoxColumn;
    }
}

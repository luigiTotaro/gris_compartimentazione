﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace GGisUtil.UserControls
{
    public partial class ZoneUC : UserControl
    {
        public ZoneUC()
        {
            InitializeComponent();
        }

        private void cboZones_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboZones.SelectedValue != null)
            {
                this.subzonesTableAdapter.Fill(this.myData.ZonesSubzones, (long) cboZones.SelectedValue);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            long savedZonId = 0;
            if (cboZones.SelectedValue != null)
            {
                savedZonId = (long) cboZones.SelectedValue;
            }
            this.classificationValuesTableAdapter.Fill(this.myData.ClassificationValues);
            this.mapLayersTableAdapter.Fill(this.myData.MapLayerList);
            this.zonesTableAdapter.Fill(this.myData.Zones);
            if (savedZonId != 0)
            {
                cboZones.SelectedValue = savedZonId;
            }
        }

        private void btnBuildZonesMapPoints_Click(object sender, EventArgs e)
        {
            if (cboZones.SelectedValue != null)
            {
                Util u = new Util();
                MyData.ZonesRow r = (MyData.ZonesRow) ((DataRowView) bindingSourceZones.Current).Row;
                string s = u.BuildZonesMapPoints((long) cboZones.SelectedValue, (int) txtKeepPoints.Value, true, r.ObjectStatusId);
                MessageBox.Show(this, "Linea GIS Creata", "GIS", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Validate())
                {
                    bindingSourceZones.EndEdit();
                    this.zonesTableAdapter.Update(this.myData.Zones);
                    subzonesBindingSource.EndEdit();
                    this.subzonesTableAdapter.Update(this.myData.ZonesSubzones);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Errore :" + ex.Message, "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboClassificationValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboClassificationValues.SelectedValue != null)
            {
                MyData.ZonesRow r = (MyData.ZonesRow) ((DataRowView) bindingSourceZones.Current).Row;
                int mapLayerId = 0;
                if (!r.IsMapLayerIdNull())
                {
                    mapLayerId = r.MapLayerId;
                }
            }
        }

        private void btnAddSubZones_Click(object sender, EventArgs e)
        {
            if (cboZones.SelectedValue == null)
            {
                return;
            }

            try
            {
                long zonId = (long) cboZones.SelectedValue;
                AddSubZonesForm a = new AddSubZonesForm();

                List<ZoneSegment> subZoneCodeList = new List<ZoneSegment>();
                foreach (MyData.ZonesSubzonesRow r in this.myData.ZonesSubzones.Rows)
                {
                    subZoneCodeList.Add(new ZoneSegment {SubZoneCode = r.SubZoneCode, NSegment = r.NSegment, Name = r.Name});
                }

                a.SubZoneCodeList = subZoneCodeList;

                if (a.ShowDialog(this) == DialogResult.OK && a.SubZoneCodeList.Count > 0)
                {
                    this.subzonesTableAdapter.DeleteByZonId(zonId);
                    this.subzonesTableAdapter.Fill(this.myData.ZonesSubzones, zonId);

                    foreach (var item in a.SubZoneCodeList)
                    {
                        int? subZoneId = this.subzonesTableAdapter.GetSubZonIdFromCode(item.SubZoneCode);
                        if (subZoneId != null)
                        {
                            MyData.ZonesSubzonesRow r = this.myData.ZonesSubzones.NewZonesSubzonesRow();
                            r.ZonId = zonId;
                            r.SubZoneId = (int) subZoneId;
                            r.NSegment = item.NSegment;
                            this.myData.ZonesSubzones.AddZonesSubzonesRow(r);
                        }
                        this.subzonesTableAdapter.Update(this.myData.ZonesSubzones);
                    }
                }
                this.myData.ZonesSubzones.Clear();
                this.subzonesTableAdapter.Fill(this.myData.ZonesSubzones, zonId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore:" + ex.Message);
            }
        }

        private void ZoneUC_Load(object sender, EventArgs e)
        {
            this.classificationValuesTableAdapter.Fill(this.myData.ClassificationValues);
            this.mapLayersTableAdapter.Fill(this.myData.MapLayerList);
            this.zonesTableAdapter.Fill(this.myData.Zones);
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(this, "Errore nella valitazione :" + e.Exception.Message, "Salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
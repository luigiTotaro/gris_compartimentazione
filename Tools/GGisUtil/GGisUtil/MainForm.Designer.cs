﻿namespace GGisUtil
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabNode = new System.Windows.Forms.TabPage();
            this.nodeUC1 = new GGisUtil.UserControls.NodeUC();
            this.tabZone = new System.Windows.Forms.TabPage();
            this.zoneUC1 = new GGisUtil.UserControls.ZoneUC();
            this.tabMapLayers = new System.Windows.Forms.TabPage();
            this.mapLayerUC1 = new GGisUtil.UserControls.MapLayerUC();
            this.tabMain.SuspendLayout();
            this.tabNode.SuspendLayout();
            this.tabZone.SuspendLayout();
            this.tabMapLayers.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabNode);
            this.tabMain.Controls.Add(this.tabZone);
            this.tabMain.Controls.Add(this.tabMapLayers);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(865, 539);
            this.tabMain.TabIndex = 23;
            // 
            // tabNode
            // 
            this.tabNode.Controls.Add(this.nodeUC1);
            this.tabNode.Location = new System.Drawing.Point(4, 22);
            this.tabNode.Name = "tabNode";
            this.tabNode.Padding = new System.Windows.Forms.Padding(3);
            this.tabNode.Size = new System.Drawing.Size(857, 513);
            this.tabNode.TabIndex = 0;
            this.tabNode.Text = "Stazioni";
            this.tabNode.UseVisualStyleBackColor = true;
            // 
            // nodeUC1
            // 
            this.nodeUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nodeUC1.Location = new System.Drawing.Point(3, 3);
            this.nodeUC1.Name = "nodeUC1";
            this.nodeUC1.Size = new System.Drawing.Size(851, 507);
            this.nodeUC1.TabIndex = 0;
            // 
            // tabZone
            // 
            this.tabZone.Controls.Add(this.zoneUC1);
            this.tabZone.Location = new System.Drawing.Point(4, 22);
            this.tabZone.Name = "tabZone";
            this.tabZone.Padding = new System.Windows.Forms.Padding(3);
            this.tabZone.Size = new System.Drawing.Size(857, 513);
            this.tabZone.TabIndex = 1;
            this.tabZone.Text = "Linee";
            this.tabZone.UseVisualStyleBackColor = true;
            // 
            // zoneUC1
            // 
            this.zoneUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zoneUC1.Location = new System.Drawing.Point(3, 3);
            this.zoneUC1.Name = "zoneUC1";
            this.zoneUC1.Size = new System.Drawing.Size(851, 507);
            this.zoneUC1.TabIndex = 0;
            // 
            // tabMapLayers
            // 
            this.tabMapLayers.Controls.Add(this.mapLayerUC1);
            this.tabMapLayers.Location = new System.Drawing.Point(4, 22);
            this.tabMapLayers.Name = "tabMapLayers";
            this.tabMapLayers.Padding = new System.Windows.Forms.Padding(3);
            this.tabMapLayers.Size = new System.Drawing.Size(857, 513);
            this.tabMapLayers.TabIndex = 2;
            this.tabMapLayers.Text = "Layers";
            this.tabMapLayers.UseVisualStyleBackColor = true;
            // 
            // mapLayerUC1
            // 
            this.mapLayerUC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapLayerUC1.Location = new System.Drawing.Point(3, 3);
            this.mapLayerUC1.Name = "mapLayerUC1";
            this.mapLayerUC1.Size = new System.Drawing.Size(851, 507);
            this.mapLayerUC1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 539);
            this.Controls.Add(this.tabMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(680, 400);
            this.Name = "MainForm";
            this.Text = "Gestione Info Mappa";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabMain.ResumeLayout(false);
            this.tabNode.ResumeLayout(false);
            this.tabZone.ResumeLayout(false);
            this.tabMapLayers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabZone;
        private System.Windows.Forms.TabPage tabNode;
        private GGisUtil.UserControls.ZoneUC zoneUC1;
        private GGisUtil.UserControls.NodeUC nodeUC1;
        private System.Windows.Forms.TabPage tabMapLayers;
        private GGisUtil.UserControls.MapLayerUC mapLayerUC1;
    }
}
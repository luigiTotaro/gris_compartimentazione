﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
//using GGisUtil.SubZonesTableAdapters;
using GGisUtil.MyDataTableAdapters;
using System.Windows.Forms;


namespace GGisUtil
{
    class Util
    {

        internal SubZoneFirstLast GetNextSubZoneFirstLast(List<SubZoneFirstLast> szList, string station, bool remove)
        {
            CultureInfo Invc = CultureInfo.InvariantCulture;

            SubZoneFirstLast retItem = null;
            foreach (var item in szList)
            {
                if (string.Compare(item.FirstStation, station, true, Invc) == 0)
                {
                    retItem = item;
                    break;
                }
            }
            if (retItem != null && remove)
                szList.Remove(retItem);

            return retItem;
        }

        internal void AddToStationList(Dictionary<string, int> stationDictionary, string station)
        {
            if (stationDictionary.ContainsKey(station))
                stationDictionary[station] = stationDictionary[station] + 1;
            else
                stationDictionary.Add(station, 1);

        }

        internal string BuildZonesMapPoints(long zonId, int keepPoints, bool updateDB, Guid objectStatusId)
        {
            string polylinePrefix = "polyline ";
            ZonesSubzonesTableAdapter ta = new ZonesSubzonesTableAdapter();
            MyData.ZonesSubzonesDataTable sz = ta.GetData(zonId);


            short currentNSegment = -1;
            List<MapSegment> segments = new List<MapSegment>();
            List<string> macroSegments = new List<string>();

            foreach (MyData.ZonesSubzonesRow subzone in sz.Rows)
            {
                if (currentNSegment == -1) currentNSegment = subzone.NSegment;

                if (currentNSegment != subzone.NSegment)
                {
                    macroSegments.Add(SumSegments(segments));
                    segments = new List<MapSegment>();
                }
                
                if (subzone.MapPoints.StartsWith(polylinePrefix))
                {
                    string smapPoints = SimplifyMapPoints(subzone.MapPoints, keepPoints);
                    segments.Add(ParseMapPoints(smapPoints));
                }
            }

            if (segments.Count > 0)
            {
                macroSegments.Add(SumSegments(segments));
            }

            string zoneMapPointsString = string.Empty;
            foreach (string item in macroSegments)
            {
                zoneMapPointsString += item + "|";
            }
            if (zoneMapPointsString != string.Empty)
                zoneMapPointsString =  string.Format("polyline {0}", zoneMapPointsString.Substring(0,zoneMapPointsString.Length-1));

            if (updateDB)
            {
                ObjectMapPointsTableAdapter taMapPoints = new ObjectMapPointsTableAdapter();
                MyData.ObjectMapPointsDataTable tbMapPoints = taMapPoints.GetDataByZonId(zonId);
                if (tbMapPoints.Rows.Count > 0)
                {
                    MyData.ObjectMapPointsRow rowMapPoints = (MyData.ObjectMapPointsRow)tbMapPoints.Rows[0];

                    rowMapPoints.MapPoints = zoneMapPointsString;

                    taMapPoints.Update(tbMapPoints);
                }
                else
                {
                    MyData.ObjectMapPointsRow rowMapPoints = tbMapPoints.NewObjectMapPointsRow();
                    rowMapPoints.ObjectStatusId = objectStatusId;
                    rowMapPoints.MapPoints = zoneMapPointsString;
                    tbMapPoints.Rows.Add(rowMapPoints);


                    taMapPoints.Update(tbMapPoints);
 
                }
            }

            return zoneMapPointsString;

        }

        private string SumSegments(List<MapSegment> segments)
        {
            MapSegment zoneMapPoints = new MapSegment();
            zoneMapPoints = AddMapSegment(zoneMapPoints, segments[0]);
            segments.RemoveAt(0);

            MapSegment nextSegment = GetNextSegment(zoneMapPoints, segments);
            while (nextSegment != null)
            {
                zoneMapPoints = AddMapSegment(zoneMapPoints, nextSegment);
                nextSegment = GetNextSegment(zoneMapPoints, segments);
            }
            return MapPointsToString(zoneMapPoints, false);
        }

        private MapSegment GetNextSegment(MapSegment line, List<MapSegment> segments)
        {
            MapPoint a = line[0];
            MapPoint b = line[line.Count - 1];

            double minDistance = 100;
            MapSegment minSegment = null;

            foreach (var segment in segments)
            {
                MapPoint a1 = segment[0];
                MapPoint b1 = segment[segment.Count - 1];
                double[] distances = new double[] { a.GetDistance(a1), a.GetDistance(b1), b.GetDistance(a1), b.GetDistance(b1) };
                double min = distances.Min();

                if (min < minDistance)
                {
                    minDistance = min;
                    minSegment = segment;
                }
            }
            if (minSegment != null)
                segments.Remove(minSegment);

            return minSegment;
        }

        private MapSegment AddMapSegment(MapSegment line, MapSegment segment)
        {
            MapSegment lineAndSegment = new MapSegment();
            if (line.Count == 0)
            {
                if (segment.Count > 0)
                {
                    line.AddRange(segment);
                    lineAndSegment = line;
                }
            }
            else if (segment.Count > 0)
            {
                MapPoint a = line[0];
                MapPoint b = line[line.Count - 1];

                MapPoint a1 = segment[0];
                MapPoint b1 = segment[segment.Count - 1];

                double[] distances = new double[] { a.GetDistance(a1), a.GetDistance(b1), b.GetDistance(a1), b.GetDistance(b1) };
                double min = distances.Min();

                if (distances[0] == min) // a <-> a1
                {
                    segment.Reverse();
                    segment.AddRange(line);
                    lineAndSegment = segment;
                }
                else if (distances[1] == min) // a <-> b1
                {
                    segment.AddRange(line);
                    lineAndSegment = segment;
                }
                else if (distances[2] == min) // b <-> a1
                {
                    line.AddRange(segment);
                    lineAndSegment = line;
                }
                else if (distances[3] == min) // b <-> b1
                {
                    segment.Reverse();
                    line.AddRange(segment);
                    lineAndSegment = line;
                }
            }
            else if (segment.Count == 0)
            {
                lineAndSegment = line;
            }

            return lineAndSegment;
        }
       
        private string SimplifyMapPoints(string mapPoints, int keepPoints)
        {
            if (mapPoints.StartsWith("polyline"))
            {

                string pts = mapPoints.Substring("polyline".Length + 1);
                string[] parts = pts.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                List<string> newParts = new List<string>(parts.Length);

                foreach (string part in parts)
                {
                    string[] aPts = part.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<string> newPoints = new List<string>((aPts.Length / keepPoints) + 2);
                    newPoints.Add(aPts[0]);
                    for (int i = 1; i < aPts.Length - 1; i++)
                    {
                        if (i % keepPoints == 0)
                        {
                            newPoints.Add(aPts[i]);
                        }
                    }
                    newPoints.Add(aPts[aPts.Length - 1]);
                    string[] aNewPts = newPoints.ToArray();
                    newParts.Add(string.Join(",", aNewPts));
                }

                string[] aNewParts = newParts.ToArray();
                return "polyline " + string.Join("|", aNewParts);
                //return "polyline " + string.Join(",", aNewParts);
            }
            else
            {
                return mapPoints;
            }
        }

        private string MapPointsToString(List<MapPoint> mapPoints, bool withPrefix)
        {
            if (mapPoints.Count == 0)
            {
                return string.Empty;
            }
            else if (mapPoints.Count == 1)
            {
                string format = withPrefix ? "point {0} {1}" : "{0} {1}";
                return string.Format(CultureInfo.InvariantCulture, format, mapPoints[0].Y, mapPoints[0].X);
            }
            else
            {
                string mapPointsString = string.Empty;
                foreach (var mapPoint in mapPoints)
                {
                    mapPointsString += string.Format(CultureInfo.InvariantCulture, "{0} {1},", mapPoint.Y, mapPoint.X);
                }

                if (mapPointsString.Length > 0)
                {
                    string format = withPrefix ? "polyline {0}" : "{0}";
                    mapPointsString = string.Format(CultureInfo.InvariantCulture, format, mapPointsString.Substring(0, mapPointsString.Length - 1));
                }

                return mapPointsString;
            }
        }

        private MapSegment ParseMapPoints(string mapPoints)
        {
             MapSegment points = new MapSegment();
            if (mapPoints.StartsWith("polyline"))
            {

                string pts = mapPoints.Substring("polyline".Length + 1);
                string[] parts = pts.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                List<string> newParts = new List<string>(parts.Length);

                if (parts.Length == 1)
                {
                    string[] aPts = parts[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    points.Add(ParsePoint(aPts[0]));
                    for (int i = 1; i < aPts.Length - 1; i++)
                    {
                        points.Add(ParsePoint(aPts[i]));
                    }
                    points.Add(ParsePoint(aPts[aPts.Length - 1]));
                }
                else
                {
                    throw new Exception("Polyline parse not implemented");
                }
                return points;
            }
            else if (mapPoints.StartsWith("point"))
            {
                points.Add(ParsePoint(mapPoints));
            }
            else
            {
                throw new Exception("MapPoints format not supported");
            }

            return points;
        }

        private MapPoint ParsePoint(string point)
        {
            string pts = point.Trim();
            if (pts.StartsWith("point"))
            {
                pts = pts.Substring("point".Length + 1);
            }

            string[] pt = pts.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (pt.Length > 1)
            {
                double y = double.Parse(pt[0], CultureInfo.InvariantCulture);
                double x = double.Parse(pt[1], CultureInfo.InvariantCulture);

                return new MapPoint(x, y);
            }

            return new MapPoint(0, 0);
        }

    }




    class SubZoneFirstLast
    {
        public long SubZoneId { get; set; }
        public string FirstStation { get; set; }
        public string LastStation { get; set; }
    }

    class MapPoint
    {
        public MapPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }


        public double GetDistance(MapPoint p)
        {
            return Math.Sqrt( Math.Pow(p.X - this.X,2) + Math.Pow(p.Y - this.Y,2));

        }

    }

    class MapSegment : List<MapPoint>
    {


    }
}

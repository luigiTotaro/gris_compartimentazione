﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GGisUtil
{
    public partial class AddSubZonesForm : Form
    {
        public List<ZoneSegment> SubZoneCodeList { get; set; }

        public AddSubZonesForm()
        {
            InitializeComponent();
        }

        private void AddSubZonesForm_Load(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            
            string s = txtSubZonesList.Text.Replace("\n",string.Empty).Trim();
            string[] a = s.Split(new char[]{'\r'},StringSplitOptions.RemoveEmptyEntries);

            SubZoneCodeList.Clear();
            for (int i = 0; i < a.Length; i++)
			{
                string[] parts = a[i].Split(new char[] {'\t'},StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length > 0)
                {
                    ZoneSegment z = new ZoneSegment();
                    z.SubZoneCode = parts[0];

                    if (parts.Length > 1)
                    {
                        short n = 0;
                        if (short.TryParse(parts[1],out n))
                            z.NSegment = n;
                    }

                    if (SubZoneCodeList.Find(x => x.SubZoneCode == z.SubZoneCode) == null)
                    {
                        SubZoneCodeList.Add(z);
                    }

                   
                }
			}
        }

        private void AddSubZonesForm_Shown(object sender, EventArgs e)
        {
            if (SubZoneCodeList != null)
            {
                string s = string.Empty;
                foreach (var item in SubZoneCodeList)
                {
                    s += item.SubZoneCode + "\t" + item.NSegment.ToString() + "\t" + item.Name.Replace("\t", " ").Replace("\r", "").Replace("\n", "").ToString() + "\r\n";
                }
                txtSubZonesList.Text = s;
            }
        }
    }

    public class ZoneSegment
    {
        public string SubZoneCode { get; set; }
        public short NSegment { get; set; }
        public string Name { get; set; }
       
    }
}

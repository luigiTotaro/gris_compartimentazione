using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace stlcver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {           
            //System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
            //this.Icon = (System.Drawing.Icon)resources.GetObject("centralstlc.ico");
            
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Telefin\\STLC1000");
            lblVersion.Text = (string)regKey.GetValue("Version"); 
            return;
        }
    }
}
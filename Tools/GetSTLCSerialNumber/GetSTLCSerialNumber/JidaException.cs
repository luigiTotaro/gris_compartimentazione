﻿using System;

namespace GetSTLCSerialNumber {
    public class JidaException : Exception {
        public JidaException() { }
        public JidaException(string message) : base(message) { }
        public JidaException(string message, Exception inner) : base(message, inner) { }
    }

    public class JidaI2CReadRegisterException : JidaException {
        public JidaI2CReadRegisterException() : base("Impossibile leggere il registro") { }
        public JidaI2CReadRegisterException(int iReg, byte byAddr) : base(String.Format("Impossibile leggere il registro 0x{0:X4} dall'indirizzo 0x{1:X2}", iReg, byAddr)) { }
        public JidaI2CReadRegisterException(string message) : base(message) { }
        public JidaI2CReadRegisterException(string message, Exception inner) : base(message, inner) { }
    }

    public class JidaDllInitializeException : JidaException {
        public JidaDllInitializeException() : base("JidaDllInitialize Failed") { }
        public JidaDllInitializeException(string message) : base(message) { }
        public JidaDllInitializeException(string message, Exception inner) : base(message, inner) { }
    }

    public class JidaBoardOpenException : JidaException {
        public JidaBoardOpenException() : base("JidaBoardOpen Failed") { }
        public JidaBoardOpenException(string message) : base(message) { }
        public JidaBoardOpenException(string message, Exception inner) : base(message, inner) { }
    }
}

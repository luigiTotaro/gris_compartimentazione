﻿using System;
using System.Runtime.InteropServices;

namespace GetSTLCSerialNumber
{
    public class JidaPlatform
    {
        private const string JIDA_BOARD_CLASS_CPU = "CPU";
        private const byte DEVICE_I2C_UNKNOW_ADDRESS = 0xAC;
        private const uint JIDA_TYPE_UNOKNOWN = 0;

        [DllImport("jida.dll")]
        public static extern int JidaDllInitialize();

        [DllImport("jida.dll")]
        public static extern int JidaBoardOpen(string pszClass, uint dwNum, uint dwFlags, ref uint phJida);

        [DllImport("jida.dll")]
        public static extern int JidaBoardClose(uint hJida);

        [DllImport("jida.dll")]
        public static extern int JidaI2CReadRegister(uint hJida, uint dwType, byte bAddr, short wReg, ref Byte pDataByte);

        [DllImport("jida.dll")]
        public static extern int JidaDllUninitialize();

        public static int GetServerID()
        {
            //1 byte per l'anno
            //1 byte per la settimana
            //2 byte per il progressivo
            byte bytemp = 0;
            uint hJida = 0;
            int dllInitialize = 0;
            int dwTemp = 0;
            int serverID;

            try
            {
                dllInitialize = JidaDllInitialize();
                if (dllInitialize != 0)
                {
                    if (JidaBoardOpen(JIDA_BOARD_CLASS_CPU, 0, 0, ref hJida) != 0)
                    {
                        int iReg;
                        for (iReg = 0; iReg < sizeof (int); iReg++)
                        {
                            if (JidaI2CReadRegister(hJida, JIDA_TYPE_UNOKNOWN, (DEVICE_I2C_UNKNOW_ADDRESS & 0xFE), (short) iReg, ref bytemp) == 0)
                            {
                                throw new JidaI2CReadRegisterException(iReg, DEVICE_I2C_UNKNOW_ADDRESS);
                            }
                            dwTemp |= (bytemp << (24 - 8*iReg));
                        }

                        serverID = dwTemp;
                    }
                    else
                    {
                        throw new JidaBoardOpenException();
                    }
                }
                else
                {
                    throw new JidaDllInitializeException();
                }
            }
            finally
            {
                if (hJida != 0)
                {
                    JidaBoardClose(hJida);
                }

                if (dllInitialize != 0)
                {
                    JidaDllUninitialize();
                }
            }

            return serverID;
        }

        public static string SerialNumberToString(int srvID)
        {
            byte byAnno = (byte) (srvID >> 24);
            byte byWeek = (byte) (srvID >> 16);
            ushort wProg = (ushort) srvID;

            return String.Format("{0:00}.{1:00}.{2:000}", byAnno, byWeek, wProg);
        }
    }
}
﻿using System;

namespace GetSTLCSerialNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length >= 1)
            {
                if (args[0].Equals("/b", StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        int serverID = GetServerID();
                        Environment.ExitCode = serverID;
                        Console.WriteLine("Setting %errorlevel% environment variable with value {0}", serverID);
                    }
                    catch (JidaException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            else
            {
                try
                {
                    int serverID = GetServerID();
                    Environment.ExitCode = serverID;
                    Console.WriteLine("Serial number {0} ({1})", JidaPlatform.SerialNumberToString(serverID), serverID);
                }
                catch (JidaException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static int GetServerID()
        {
#if (DEBUG)
            return 134938631;
#else
            return JidaPlatform.GetServerID();
#endif
        }
    }
}
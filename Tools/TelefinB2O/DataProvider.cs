﻿using System;
using System.Linq;
using System.Diagnostics;
using TelefinStreamReader.DeviceStreams;
using System.IO;
using System.Collections.Generic;

namespace TelefinStreamReader
{
    class DataProvider
    {
        public byte[][] GetDeviceBinaryStatus(long devId)
        {
            var tEnt = new GrisEntities();
            var strm = (from st in tEnt.streams
                        where st.DevID == devId //&& st.StrID == 4
                        select st).ToArray();

            byte[][] bDevice = new byte[6][];

            foreach (var st in strm)
            {
                switch (st.StrID)
                {
                    case 1: bDevice[0] = st.Data; break;
                    case 2: bDevice[1] = st.Data; break;
                    case 3: bDevice[2] = st.Data; break;
                    case 4: bDevice[3] = st.Data; break;
                    case 5: bDevice[4] = st.Data; break;
                    case 6: bDevice[5] = st.Data; break;
                }
            }

            return bDevice;

        }

        public void SaveFDS(FdsDevice fds)
        {
            TextWriter outfile = new StreamWriter("FdsDevice.xml", false);
            XmlDumper.Write(fds, 10, outfile);
        }

    }
}

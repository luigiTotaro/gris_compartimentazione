using System;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
    public partial class FDS : ObjectWithState
	{
		public System.String GeneralStatus { get; set; }
		public CPUMeasures CPUMeasures { get; set; }
		public System.String PowerStatus { get; set; }
		public System.String InterfaceStatus { get; set; }
		public System.String E1Counter { get; set; }
		public System.String GeneralInfo { get; set; }

		public FDS() : base()
		{
			this.CPUMeasures = new CPUMeasures(this);
		}
	}

	public partial class CPUMeasures : ObjectWithState
	{
		public Measure Local { get; set; }
		public Measure Remote { get; set; }

		public CPUMeasures(ObjectWithState root) : base(root)
		{
			if ( !(root is FDS) )
			{
				throw new ArgumentException("Root must of type FDS", "root");
			}		
			this.Local = new Measure(root);
			this.Remote = new Measure(root);
		}
	}

	public partial class Measure : ObjectWithState
	{
		public ValueWithState<System.Decimal> Power24 { get; set; }
		public ValueWithState<System.Decimal> Power12V { get; set; }
		public ValueWithState<System.Decimal> Power5V { get; set; }
		public ValueWithState<System.Decimal> PowerM12V { get; set; }
		public ValueWithState<System.Decimal?> FiberVolt { get; set; }
		public ValueWithState<System.Decimal> Temperature { get; set; }

		public Measure(ObjectWithState root) : base(root)
		{
			if ( !(root is FDS) )
			{
				throw new ArgumentException("Root must of type FDS", "root");
			}		
			this.Power24 = new ValueWithState<System.Decimal>
			{
				Label = "Tensione +24V",
				FormatString = "{0:+####0.00;-####0.00} V"
			};
			this.Power12V = new ValueWithState<System.Decimal>
			{
				Label = "Tensione +12V",
				FormatString = "{0:+####0.00;-####0.00} V"
			};
			this.Power5V = new ValueWithState<System.Decimal>
			{
				Label = "Tensione +5V",
				FormatString = "{0:+####0.00;-####0.00} V"
			};
			this.PowerM12V = new ValueWithState<System.Decimal>
			{
				Label = "Tensione -12V",
				FormatString = "{0:+####0.00;-####0.00} V"
			};
			this.FiberVolt = new ValueWithState<System.Decimal?>
			{
				Label = "Livello segnale fibra",
				FormatString = "{0:+####0.00;-####0.00} V"
			};
			this.Temperature = new ValueWithState<System.Decimal>
			{
				Label = "Temperatura",
				FormatString = "{0:+####0.0;-####0.0} c"
			};
		}
	}

}	

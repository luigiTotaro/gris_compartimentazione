using System;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct FDSStruct
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
		internal Byte[] GeneralStatus;
		internal CPUMeasuresStruct CPUMeasures;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
		internal Byte[] PowerStatus;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
		internal Byte[] InterfaceStatus;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
		internal Byte[] E1Counter;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
		internal Byte[] GeneralInfo;
	}
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct CPUMeasuresStruct
	{
		internal MeasureStruct Local;
		internal MeasureStruct Remote;
	}
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct MeasureStruct
	{
		internal Int16 Power24;
		internal Int16 Power12V;
		internal Int16 Power5V;
		internal Int16 PowerM12V;
		internal Int16 FiberVolt;
		internal Int16 Temperature;
	}
}	

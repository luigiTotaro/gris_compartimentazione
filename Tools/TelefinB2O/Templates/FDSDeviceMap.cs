using System;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
        public partial class FDSMapB2O : IDeviceMapB2O
	    {
	        public FDS GetDevice(byte[][] data)
	        {
	            FDS FDSObj = new FDS();
	
	// Creazione mappers
		var CPUMeasuresMap = new CPUMeasuresMapB2O(FDSObj);
	// Esecuzione mappers
	FDSObj.CPUMeasures = CPUMeasuresMap.GetStream(data[0]);
	            return  FDSObj;
	        }
	
	        #region IDeviceMapB2O Members
	
	        public IObjectWithState GetStream(byte[][] data)
	        {
	            return GetDevice(data);
	        }
	
	        #endregion
	    }
	    public partial class CPUMeasuresMapB2O : IDeviceStreamMapB2O
	    {
	        private readonly FDS device;
	
	        public CPUMeasuresMapB2O (FDS device)
	        {
	            this.device = device;
	        }
	
	        #region IDeviceStreamMapB2O<CPUMeasuresMapB2O> Members
	
	        public CPUMeasures GetStream(byte[] data)
	        {
	            CPUMeasures stream = new CPUMeasures(device);
	            CPUMeasuresStruct bstruct = BinaryHelper< CPUMeasuresStruct >.RawStringBytesDeserialize(data);
	
				SetMeasure(bstruct.Local,stream.Local);
	SetMeasure(bstruct.Remote,stream.Remote);
	
	            return stream;
	        }
	
	        #endregion
	
	        #region IDeviceStreamMapB2O Members
	
	        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
	        {
	            return (IObjectWithState)GetStream(data);
	        }
	
	        #endregion
			
			        private void SetMeasure(MeasureStruct bstruct, Measure stream)
	        {
				stream.Power24.Value = bstruct.Power24 * 0.0001m; 
	stream.Power12V.Value = bstruct.Power12V * 0.0001m; 
	stream.Power5V.Value = bstruct.Power5V * 0.0001m; 
	stream.PowerM12V.Value = bstruct.PowerM12V * 0.0001m; 
	stream.FiberVolt.Value = bstruct.FiberVolt * 0.0001m; 
	stream.Temperature.Value = bstruct.Temperature; 
	        }
	    }
	
}	

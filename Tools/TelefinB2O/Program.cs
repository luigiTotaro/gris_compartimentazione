﻿using System;
using System.Linq;
using System.Diagnostics;
using TelefinStreamReader.DeviceStreams;
using System.IO;
using System.Collections.Generic;

[assembly: CLSCompliant(true)]
namespace TelefinStreamReader
{
    class Program
    {

        static void Main(string[] args)
        {
            DataProvider dp = new DataProvider();

            DateTime T0 = DateTime.Now;

            byte[][] bDevice = dp.GetDeviceBinaryStatus(2542917103583239);

            DateTime T1 = DateTime.Now;

            var fds = new FdsDeviceMapB2O().GetDevice(bDevice);

            DateTime T2 = DateTime.Now;
            
            fds.RefreshState();

            DateTime T3 = DateTime.Now;

            dp.SaveFDS(fds);

            DateTime T4 = DateTime.Now;

            Console.WriteLine("Caricamento da DB:\t\t{0}", T1 - T0);
            Console.WriteLine("Deserializzazione streams:\t{0}", T2 - T1);
            Console.WriteLine("Refresh state:\t\t\t{0}", T3 - T2);
            Console.WriteLine("Serializzazione oggetto:\t{0}", T4 - T3);
            Console.WriteLine("Totale:\t\t\t\t{0}", T4 - T0);

            Console.ReadLine();

        }
    }
}
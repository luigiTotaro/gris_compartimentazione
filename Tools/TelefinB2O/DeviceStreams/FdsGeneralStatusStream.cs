﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
    #region Data Struct
    // Struttura del buffer richiesta stato generale--------------------------------
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsGeneralStatusStruct
    {
        [BinaryFormatAttribute(Endian = EndianEnum.BigEndian)]
        internal Int16 LocalFiberTension;
        internal FdsSystemStatusStruct LocalSystemStatus;
        [BinaryFormatAttribute(Endian = EndianEnum.BigEndian)]
        internal Int16 RemoteFiberTension;
        internal FdsSystemStatusStruct RemoteSystemStatus;
    };

    // Struttura dello stato di una interfaccia Riepilogativo-----------------------
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsCardStatusStruct
    {
        internal Byte CardCode;
        internal Byte CardStatus;
    };

    // Struttura dello stato di una unità-------------------------------------------
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsSystemStatusStruct
    {
        internal UInt16 Warnings;
        internal UInt16 StatusFlags;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        internal FdsCardStatusStruct[] Devices;
    }

    #endregion

    #region Data Stream

    //Root Stream
    public partial class FdsGeneralStatusStream : ObjectWithState
    {
        public ValueWithState<decimal> LocalFiberTension { get; private set; }
        public FdsSystemStatusStream LocalSystemStatus { get; private set; }
        public ValueWithState<decimal> RemoteFiberTension { get; private set; }
        public FdsSystemStatusStream RemoteSystemStatus { get; private set; }

        public FdsGeneralStatusStream()
        {
            LocalSystemStatus = new FdsSystemStatusStream();
            LocalFiberTension = new ValueWithState<decimal>
                                    {
                                        Label = "Tensione fibra locale",
                                                 FormatString = "{0:+####0.00;-####0.00} V",
                                        StateMessageRules = new[]
                                        {
                                            new StateMessageRule<decimal>
                                                { 
                                                Rule = (x => (true)),
                                                Message = new StateMessage { Message = "Misura Ricevuta", State = StateEnum.Ok }                      
                                            }
                                        }
                                    };

            RemoteSystemStatus = new FdsSystemStatusStream();
            RemoteFiberTension = new ValueWithState<decimal>
                                    {
                                        Label = "Tensione fibra remota",
                                        FormatString = "{0:+####0.00;-####0.00} V",
                                        StateMessageRules = new[]
                                        {
                                            new StateMessageRule<decimal>
                                                { 
                                                Rule = (x => (true)),
                                                Message = new StateMessage { Message = "Misura Ricevuta", State = StateEnum.Ok }                      
                                            }
                                        }
                                    };
        }

    }

    public partial class FdsSystemStatusStream : ObjectWithState
    {
        public FdsWarningsBits Warnings { get; private set; }
        public FdsStatusBits StatusFlags { get; private set; }
        public List<FdsCardStatusStream> Devices { get; private set; }

        public FdsSystemStatusStream()
        {
            this.Devices = new List<FdsCardStatusStream>();
            this.Warnings = new FdsWarningsBits();
            this.StatusFlags = new FdsStatusBits();
        }
    }

    public partial class FdsCardStatusStream : ObjectWithState
    {
        public ValueWithState<FdsInterfaceCardTypes> CardCode { get; private set; }
        public FdsCardStatusBits CardStatus { get; private set; }

        public FdsCardStatusStream()
        {
            this.CardCode = new ValueWithState<FdsInterfaceCardTypes>
                                {
                                    Label = "Codice Scheda",
                                    FormatFunction = (x => string.Format("0x{0:X2}", (byte) x.Value))
                                };
            this.CardCode.ValueChanged += CardCode_ValueChanged;
            
        }

        void CardCode_ValueChanged(ValueWithState<FdsInterfaceCardTypes> sender, EventArgs args)
        {
            switch (this.CardCode.Value)
            {
                case FdsInterfaceCardTypes.FD20000_ConsoleDTS:
                case FdsInterfaceCardTypes.FD39200_ADSE:
                case FdsInterfaceCardTypes.FD40X00_Telecomando:
                case FdsInterfaceCardTypes.FD60X00_BCA:
                    this.CardStatus = new FdsCardStatusBits { Label = "Stato Scheda" };
                    break;
                case FdsInterfaceCardTypes.SlotEmpty:
                    CardStatus = null;
                    break;
            }
        }
    }

    public partial class FdsWarningsBits : ValueWithState<long>
    {
        public ValueWithState<bool> LinkPCME1 { get; private set; }
        public ValueWithState<bool> LivelloSegnaleFibra { get; private set; }
        public ValueWithState<CorrettoIncrociatoEnum> ConnessionFibre { get; private set; }
        public ValueWithState<bool> StatoRtc { get; private set; }
        public ValueWithState<bool> IntegritaConfigurazione { get; private set; }
        public ValueWithState<bool> TensioneV5 { get; private set; }
        public ValueWithState<bool> TensioneV24 { get; private set; }
        public ValueWithState<bool> TensioneV12 { get; private set; }
        public ValueWithState<bool> TensioneMV12 { get; private set; }
        public ValueWithState<bool> TemperaturaSistema { get; private set; }
        public ValueWithState<bool> SistemaAlimentazione { get; private set; }
        public ValueWithState<bool> ConfigurazioneInterfacce { get; private set; }
        public ValueWithState<bool> ComunicazioneRemota { get; private set; }

        public FdsWarningsBits()
        {
            FormatString = "0x{0:X4}";
            LinkPCME1 = new ValueWithState<bool> { Label = "Link PCM (E1)", ErrorValue = true };
            LivelloSegnaleFibra = new ValueWithState<bool> { Label = "Livello Segnale Fibra", ErrorValue = true };
            ConnessionFibre = new ValueWithState<CorrettoIncrociatoEnum> { Label = "Connession Fibre", ErrorValue = CorrettoIncrociatoEnum.Incrociato };
            StatoRtc = new ValueWithState<bool> { Label = "Stato RTC", ErrorValue = true };
            IntegritaConfigurazione = new ValueWithState<bool> { Label = "Integrita Configurazione", ErrorValue = true };
            TensioneV5 = new ValueWithState<bool> { Label = "Tensione (+5v)", ErrorValue = true };
            TensioneV24 = new ValueWithState<bool> { Label = "Tensione (+24v)", ErrorValue = true };
            TensioneV12 = new ValueWithState<bool> { Label = "Tensione (+12v)", ErrorValue = true };
            TensioneMV12 = new ValueWithState<bool> { Label = "Tensione (-12v)", ErrorValue = true };
            TemperaturaSistema = new ValueWithState<bool> { Label = "Temperatura Sistema", ErrorValue = true };
            SistemaAlimentazione = new ValueWithState<bool> { Label = "Sistema Alimentazione", ErrorValue = true };
            ConfigurazioneInterfacce = new ValueWithState<bool> { Label = "Configurazione Interfacce", ErrorValue = true };
            ComunicazioneRemota = new ValueWithState<bool> { Label = "Comunicazione Remota", ErrorValue = true };

        }
    }

    public partial class FdsCardStatusBits : ValueWithState<long>
    {
        public ValueWithState<bool> NonRilevataLocale { get; private set; }
        public ValueWithState<bool> NonInstallataCorrettamente { get; private set; }
        public ValueWithState<bool> NonCorrispondenteAllaConfigurazione { get; private set; }
        public ValueWithState<bool> NonRilevataRemoto { get; private set; }
        public ValueWithState<bool> DiversaDaIntallazioneRemota { get; private set; }
        public ValueWithState<bool> DisattivaPerErroreInterno { get; private set; }
        public ValueWithState<bool> AbilitataCollegamentoEseguito { get; private set; }
        public ValueWithState<bool> Anomalia { get; private set; }

        public FdsCardStatusBits()
        {
            FormatString = "0x{0:X2}";
            NonRilevataLocale = new ValueWithState<bool> { Label = "Scheda non rilevata sul sistema locale" };
            NonInstallataCorrettamente = new ValueWithState<bool> { Label = "Scheda non installata mediante procedura di configurazione" };
            NonCorrispondenteAllaConfigurazione = new ValueWithState<bool> { Label = "Scheda non corrispondente alla configurazione" };
            NonRilevataRemoto = new ValueWithState<bool> { Label = "Scheda non rilevata dal sistema remoto" };
            DiversaDaIntallazioneRemota = new ValueWithState<bool> { Label = "Scheda diversa da quella installata su sistema remoto" };
            DisattivaPerErroreInterno = new ValueWithState<bool> { Label = "Scheda disattiva per errore interno" };
            AbilitataCollegamentoEseguito = new ValueWithState<bool> { Label = "Scheda abilitata e collegamento eseguito" };
            Anomalia = new ValueWithState<bool> { Label = "Scheda in anomalia" };
        }
    }

    public partial class FdsStatusBits : ValueWithState<long>
    {
        public ValueWithState<IndefinitoAcquisitoEnum> StatoInterfacce { get; private set; }
        public ValueWithState<VuotoNonVuotoEnum> FileEventiSistema { get; private set; }
        public ValueWithState<PrincipaleSecondariaEnum> GerarchiaCPU { get; private set; }
        public ValueWithState<bool> ControlloInterfacceAbilitato { get; private set; }
        public ValueWithState<MasterSlaveEnum> TipoClock { get; private set; }
        public ValueWithState<OtticaG703Enum> TipoInterfacciaCollegamento { get; private set; }

        public FdsStatusBits()
        {
            FormatString = "0x{0:X4}";
            StatoInterfacce = new ValueWithState<IndefinitoAcquisitoEnum> { Label = "Stato Interfacce"};
            FileEventiSistema = new ValueWithState<VuotoNonVuotoEnum> { Label = "File Eventi di Sistema"};
            GerarchiaCPU = new ValueWithState<PrincipaleSecondariaEnum> { Label = "Gerarchia CPU"};
            ControlloInterfacceAbilitato = new ValueWithState<bool> { Label = "Controllo Interfacce Abilitato"};
            TipoClock = new ValueWithState<MasterSlaveEnum> { Label = "Tipo Clock"};
            TipoInterfacciaCollegamento = new ValueWithState<OtticaG703Enum> {Label = "Tipo Interfaccia Collegamento"};
        }
    }

    #endregion

    #region Stream MapB2O

    public partial class FdsGeneralStatusStreamMapB2O : IDeviceStreamMapB2O
    {
        #region IDeviceStreamMapB2O<FdsCPUsMeasureStream> Members

        public FdsGeneralStatusStream GetStream(byte[] data)
        {
            FdsGeneralStatusStruct bs = BinaryHelper<FdsGeneralStatusStruct>.RawStringBytesDeserialize(data);
            FdsGeneralStatusStream stream = new FdsGeneralStatusStream();

            stream.LocalFiberTension.Value = bs.LocalFiberTension*0.001m;
            stream.RemoteFiberTension.Value = bs.RemoteFiberTension*0.001m;
            SetStatusBits(bs.LocalSystemStatus.StatusFlags, stream.LocalSystemStatus.StatusFlags);
            SetWaringBits(bs.LocalSystemStatus.Warnings, stream.LocalSystemStatus.Warnings);
            SetDevices(bs.LocalSystemStatus.Devices, stream.LocalSystemStatus.Devices);

            stream.RemoteFiberTension.Value = bs.RemoteFiberTension * 0.001m;
            stream.RemoteFiberTension.Value = bs.RemoteFiberTension * 0.001m;
            SetStatusBits(bs.RemoteSystemStatus.StatusFlags, stream.RemoteSystemStatus.StatusFlags);
            SetWaringBits(bs.RemoteSystemStatus.Warnings, stream.RemoteSystemStatus.Warnings);
            SetDevices(bs.RemoteSystemStatus.Devices, stream.RemoteSystemStatus.Devices);

            return stream;

        }

        #endregion

        private void SetDevices(IEnumerable<FdsCardStatusStruct> cards, ICollection<FdsCardStatusStream> streamCards)
        {
            foreach (var card in cards)
            {
                FdsCardStatusStream s = new FdsCardStatusStream();
                s.CardCode.Value = (FdsInterfaceCardTypes)Enum.ToObject(typeof(FdsInterfaceCardTypes),card.CardCode) ;
                SetCardStatusBits(card.CardStatus, s.CardStatus);
                streamCards.Add(s);
            }
        }

        private void SetWaringBits(ushort warningValue, FdsWarningsBits warningBits)
        {
            warningBits.LinkPCME1.Value = warningValue.BitIsTrue(0);
            warningBits.LivelloSegnaleFibra.Value = warningValue.BitIsTrue(1);
            warningBits.ConnessionFibre.Value = warningValue.BitIsTrue(2) ? CorrettoIncrociatoEnum.Incrociato : CorrettoIncrociatoEnum.Corretto;
            warningBits.StatoRtc.Value = warningValue.BitIsTrue(3);
            warningBits.IntegritaConfigurazione.Value = warningValue.BitIsTrue(4);
            warningBits.TensioneV5.Value = warningValue.BitIsTrue(5);
            warningBits.TensioneV24.Value = warningValue.BitIsTrue(6);
            warningBits.TensioneV12.Value = warningValue.BitIsTrue(7);
            warningBits.TensioneMV12.Value = warningValue.BitIsTrue(8);
            warningBits.TemperaturaSistema.Value = warningValue.BitIsTrue(9);
            warningBits.SistemaAlimentazione.Value = warningValue.BitIsTrue(10);
            warningBits.ConfigurazioneInterfacce.Value = warningValue.BitIsTrue(11);
            warningBits.ComunicazioneRemota.Value = warningValue.BitIsTrue(12);
        }

        private void SetCardStatusBits(ushort statusValue, FdsCardStatusBits statusBits)
        {
            if (statusBits == null)
                return;

            statusBits.NonRilevataLocale.Value = statusValue.BitIsTrue(0);
            statusBits.NonInstallataCorrettamente.Value = statusValue.BitIsTrue(1);
            statusBits.NonCorrispondenteAllaConfigurazione.Value = statusValue.BitIsTrue(2);
            statusBits.NonRilevataRemoto.Value = statusValue.BitIsTrue(3);
            statusBits.DiversaDaIntallazioneRemota.Value = statusValue.BitIsTrue(4);
            statusBits.DisattivaPerErroreInterno.Value = statusValue.BitIsTrue(5);
            statusBits.AbilitataCollegamentoEseguito.Value = statusValue.BitIsTrue(6);
            statusBits.Anomalia.Value = statusValue.BitIsTrue(7);

        }

        private void SetStatusBits(ushort statusValue, FdsStatusBits statusBits)
        {
            statusBits.StatoInterfacce.Value = statusValue.BitIsTrue(0) ? IndefinitoAcquisitoEnum.Acquisito : IndefinitoAcquisitoEnum.Indefinito;
            statusBits.FileEventiSistema.Value = statusValue.BitIsTrue(1) ? VuotoNonVuotoEnum.Vuoto : VuotoNonVuotoEnum.NonVuoto; 
            statusBits.GerarchiaCPU.Value = statusValue.BitIsTrue(2) ? PrincipaleSecondariaEnum.Principale : PrincipaleSecondariaEnum.Secondaria;
            statusBits.ControlloInterfacceAbilitato.Value = statusValue.BitIsTrue(3); 
            statusBits.TipoClock.Value = statusValue.BitIsTrue(4) ? MasterSlaveEnum.Master : MasterSlaveEnum.Slave;
            statusBits.TipoInterfacciaCollegamento.Value = statusValue.BitIsTrue(5) ? OtticaG703Enum.Ottica : OtticaG703Enum.G703;
        }

        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion
}

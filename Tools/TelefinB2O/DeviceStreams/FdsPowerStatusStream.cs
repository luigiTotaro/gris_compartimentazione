﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
    #region Data Struct

    // Root Struct
    // Struttura stato sistema di alimentazione-------------------------------------
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FdsPowerStatusStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
        public FdsPowerStatusPartStruct[] Local;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
        public FdsPowerStatusPartStruct[] Remote;    
    }

    // Struttura stato generica scheda di alimentazione-----------------------------
    [StructLayout(LayoutKind.Explicit, Pack = 1)]
    public struct FdsPowerStatusPartStruct
    {
        [FieldOffset(0)]
        public Int16 CardType;
        [FieldOffset(2)]
        public FdsPowerStatusInputStruct Input;
        [FieldOffset(2)]
        public FdsPowerStatusOutputStruct Output;
    }

    // Struttura stato scheda alimentatore------------------------------------------
    // Ho dovuto marcare le variabili con Endian opposto perchè essendo le strutture FdsPowerStatusInputStruct e FdsPowerStatusOutputStruct 
    // riferite  agli stessi byte la funzione SwapEndian ruotava 2 volte i byte.    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FdsPowerStatusOutputStruct
    {
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        public Int16 Status;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        public Int16 BusTension;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        public Int16 IntTension;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        public Int16 Current;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        public Int16 Temperature;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        public Int16 Filler;
    }

    // Struttura stato scheda controllo alimentazione------------------------------- 
    // Ho dovuto marcare le variabili con Endian opposto perchè essendo le strutture FdsPowerStatusInputStruct e FdsPowerStatusOutputStruct 
    // riferite  agli stessi byte la funzione SwapEndian ruotava 2 volte i byte.
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FdsPowerStatusInputStruct
    {
        [BinaryFormat(Endian = EndianEnum.LittleEndian)]
        public Int16 Status;
        [BinaryFormat(Endian = EndianEnum.LittleEndian)]
        public Int16 BusTension;
        [BinaryFormat(Endian = EndianEnum.LittleEndian)]
        public Int16 In1Tension;
        [BinaryFormat(Endian = EndianEnum.LittleEndian)]
        public Int16 In2Tension;
        [BinaryFormat(Endian = EndianEnum.LittleEndian)]
        public Int16 Current;
        [BinaryFormat(Endian = EndianEnum.LittleEndian)]
        public Int16 Timeout;
    }

    #endregion

    #region Data Stream

    public partial class FdsPowerStatusStream : ObjectWithState
    {
        public FdsPowerStatusStream()
        {
            Local = new List<FdsPowerStatusPartStream>();
            Remote = new List<FdsPowerStatusPartStream>();
        }

        public List<FdsPowerStatusPartStream> Local;
        public List<FdsPowerStatusPartStream> Remote;
    }

    public partial class FdsPowerStatusPartStream: ObjectWithState
    {
        public int Index;
        public ValueWithState<FdsPowerCardTypes> CardType {get; set;}
    }

    public partial class FdsPowerStatusInputStream : FdsPowerStatusPartStream
    {
        public FdsPowerStatusInputBits Status { get; set; }
        public ValueWithState<decimal?> BusTension {get; set;}
        public ValueWithState<decimal?> In1Tension { get; set; }
        public ValueWithState<decimal?> In2Tension { get; set; }
        public ValueWithState<decimal?> Current { get; set; }
        public ValueWithState<decimal?> Timeout { get; set; }

        public FdsPowerStatusInputStream()
        {
            this.CardType = new ValueWithState<FdsPowerCardTypes>
            {
                FormatFunction = (x => string.Format("0x{0:X4}", (int) x.Value)),
                StateMessageRules = new[]
                                    {
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message = "Scheda alimentazione mancante", State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.SlotEmpty)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message = "Errore nel rilevamento scheda", State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.Unknown_NotReadable)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message = "Scheda ingresso alimentazione (FD13000)", State = StateEnum.Ok},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD13000)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message ="Tipo scheda errato (FD11000)", State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD11000_V5)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage {Message ="Tipo scheda errato (FD11100)",State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD11100_V12)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message =new StateMessage {Message ="Tipo scheda errato (FD11200)", State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD11200_M12)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage {Message ="Tipo scheda non riconosciuto",State = StateEnum.Warning},
                                                Rule = (x => x.Value == FdsPowerCardTypes.Unknown)
                                            },

                                    },
                Label = "Tipo scheda"
            };

            this.BusTension = new ValueWithState<decimal?> { Label = "Tensione Bus", FormatString = "{0:+####0.00;-####0.00} V" };
            this.Current = new ValueWithState<decimal?> { Label = "Corrente", FormatString = "{0:+####0.00;-####0.00} A" };
            this.In1Tension = new ValueWithState<decimal?> { Label = "Tensione Ingresso 1", FormatString = "{0:+####0.00;-####0.00} V" };
            this.In2Tension = new ValueWithState<decimal?> { Label = "Tensione Ingresso 2", FormatString = "{0:+####0.00;-####0.00} V" };
            this.Timeout = new ValueWithState<decimal?> { Label = "Timeout" };
            this.Status = new FdsPowerStatusInputBits();

            this.CardType.ValueChanged += CardType_ValueChanged;
        }

        void CardType_ValueChanged(ValueWithState<FdsPowerCardTypes> sender, EventArgs args)
        {
            this.CardType.RefreshState();
            if (this.CardType.State != StateEnum.Ok)
            {
                this.BusTension.Value = null;
                this.Current.Value = null;
                this.In1Tension.Value = null;
                this.In2Tension.Value = null;
                this.Timeout.Value = null;
                this.Status = null;
            }
            else if (this.Status == null)
            {
                this.Status = new FdsPowerStatusInputBits();
            }
        }

    }

    public partial class FdsPowerStatusOutputStream : FdsPowerStatusPartStream
    {
        public FdsPowerStatusOutputBits Status { get; set; }
        public ValueWithState<decimal?> BusTension { get; set; }
        public ValueWithState<decimal?> IntTension { get; set; }
        public ValueWithState<decimal?> Current { get; set; }
        public ValueWithState<decimal?> Temperature { get; set; }

        public FdsPowerStatusOutputStream()
        {
            this.CardType = new ValueWithState<FdsPowerCardTypes>
            {
                StateMessageRules = new[]
                                    {
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message = "Scheda alimentazione mancante", State = StateEnum.Ok},
                                                Rule = (x => x.Value == FdsPowerCardTypes.SlotEmpty)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message = "Errore nel rilevamento scheda", State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.Unknown_NotReadable)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message = "Tipo scheda errato (FD13000)", State = StateEnum.Error},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD13000)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage { Message ="Scheda alimentazione +5V (FD11000)", State = StateEnum.Ok},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD11000_V5)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage {Message ="Scheda alimentazione +12V  (FD11100)",State = StateEnum.Ok},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD11100_V12)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message =new StateMessage {Message ="Scheda alimentazione -12V  (FD11200)", State = StateEnum.Ok},
                                                Rule = (x => x.Value == FdsPowerCardTypes.FD11200_M12)
                                            },
                                        new StateMessageRule<FdsPowerCardTypes>
                                            {
                                                Message = new StateMessage {Message ="Tipo scheda non riconosciuto",State = StateEnum.Warning},
                                                Rule = (x => x.Value == FdsPowerCardTypes.Unknown)
                                            },

                                    },
                Label = "Tipo scheda"
            };

            this.BusTension = new ValueWithState<decimal?> { Label = "Tensione Bus", FormatString = "{0:+####0.00;-####0.00} V" };
            this.Current = new ValueWithState<decimal?> { Label = "Corrente", FormatString = "{0:+####0.00;-####0.00} A" };
            this.IntTension = new ValueWithState<decimal?> { Label = "Tensione Interna", FormatString = "{0:+####0.00;-####0.00} V" };
            this.Temperature = new ValueWithState<decimal?> { Label = "Temperatura", FormatString = "{0:+####0.0;-####0.0} °C" };
            this.Status = new FdsPowerStatusOutputBits();

            this.CardType.ValueChanged += CardType_ValueChanged;
        }

        void CardType_ValueChanged(ValueWithState<FdsPowerCardTypes> sender, EventArgs args)
        {
            this.CardType.RefreshState();
            if (this.CardType.State != StateEnum.Ok )
            {
                this.BusTension.Value = null;
                this.Current.Value = null;
                this.IntTension.Value = null;
                this.Temperature.Value = null;
                this.Status = null;
            }
            else if (this.Status == null)
            {
                this.Status = new FdsPowerStatusOutputBits();
            }
        }
    }

    public partial class FdsPowerStatusInputBits : ValueWithState<long>
    {
        public ValueWithState<bool> Configurazione { get; private set; }
        public ValueWithState<bool> TensioneBusBassa { get; private set; }
        public ValueWithState<bool> TensioneBusElevata { get; private set; }
        public ValueWithState<bool> TensioneIngresso1Bassa { get; private set; }
        public ValueWithState<bool> TensioneIngresso2Bassa { get; private set; }
        public ValueWithState<bool> CorrenteErogataBassa { get; private set; }
        public ValueWithState<bool> CorrenteErogataAlta { get; private set; }

        public FdsPowerStatusInputBits()
        {
            FormatString = "0x{0:X4}";
            Configurazione = new ValueWithState<bool> { Label = "Configurazione", WarningValue = true};
            TensioneBusBassa = new ValueWithState<bool> { Label = "Tensione Bus Bassa", WarningValue = true };
            TensioneBusElevata = new ValueWithState<bool> {Label = "Tensione Bus Elevata", WarningValue = true };
            TensioneIngresso1Bassa = new ValueWithState<bool> {Label = "Tensione Ingresso 1 Bassa ", WarningValue = true };
            TensioneIngresso2Bassa = new ValueWithState<bool> {  Label = "Tensione Ingresso 2 Bassa", WarningValue = true };
            CorrenteErogataBassa = new ValueWithState<bool> { Label = "Corrente Erogata Bassa ", WarningValue = true };
            CorrenteErogataAlta = new ValueWithState<bool> { Label = "Corrente Erogata Alta", WarningValue = true };
        }
    }

    public partial class FdsPowerStatusOutputBits : ValueWithState<long>
    {

        public ValueWithState<bool> AlimentatoreInFunzione { get; private set; }
        public ValueWithState<bool> TensioneFuoriRange { get; private set; }
        public ValueWithState<bool> ElevatoAssortimentoCorrente { get; private set; }
        public ValueWithState<bool> TemperaturaElevata { get; private set; }

        public FdsPowerStatusOutputBits()
        {
            FormatString = "0x{0:X4}";
            AlimentatoreInFunzione = new ValueWithState<bool> { Label = "Alimentatore in funzione", ErrorValue = false};
            TensioneFuoriRange = new ValueWithState<bool> {  Label = "Tensione fuori range", ErrorValue = true};
            ElevatoAssortimentoCorrente = new ValueWithState<bool> {Label = "Elevato assorbimento di corrente", ErrorValue = true};
            TemperaturaElevata = new ValueWithState<bool> {  Label = "TemperaturaElevata", WarningValue = true};
        }

        
    }

    #endregion

    #region Stream MapB2O

    public partial class FdsPowerStatusStreamMapB2O : IDeviceStreamMapB2O
    {

        #region IDeviceStreamMapB2O<FdsPowerStatusStream> Members

        public FdsPowerStatusStream GetStream(byte[] data)
        {
            FdsPowerStatusStream stream = new FdsPowerStatusStream();
            FdsPowerStatusStruct bs = BinaryHelper<FdsPowerStatusStruct>.RawStringBytesDeserialize(data);

            stream.Local.Add(GetFdsPowerStatusInputStream(bs.Local[0],15));
            stream.Local.Add(GetFdsPowerStatusOutput(bs.Local[1], 1));
            stream.Local.Add(GetFdsPowerStatusOutput(bs.Local[2], 2));
            stream.Local.Add(GetFdsPowerStatusOutput(bs.Local[3], 13));
            stream.Local.Add(GetFdsPowerStatusOutput(bs.Local[4], 14));
            stream.Local.Add(GetFdsPowerStatusOutput(bs.Local[5], 25));
            stream.Local.Add(GetFdsPowerStatusOutput(bs.Local[6], 26));

            stream.Remote.Add(GetFdsPowerStatusInputStream(bs.Remote[0], 15));
            stream.Remote.Add(GetFdsPowerStatusOutput(bs.Remote[1], 1));
            stream.Remote.Add(GetFdsPowerStatusOutput(bs.Remote[2], 2));
            stream.Remote.Add(GetFdsPowerStatusOutput(bs.Remote[3], 13));
            stream.Remote.Add(GetFdsPowerStatusOutput(bs.Remote[4], 14));
            stream.Remote.Add(GetFdsPowerStatusOutput(bs.Remote[5], 25));
            stream.Remote.Add(GetFdsPowerStatusOutput(bs.Remote[6], 26));                

            stream.RefreshState();

            return stream;
        }

        #endregion

        private static FdsPowerStatusOutputStream GetFdsPowerStatusOutput(FdsPowerStatusPartStruct bs, int index)
        {
            var pss = new FdsPowerStatusOutputStream
                          {
                              CardType = { Value = DecodCardType(bs.CardType) },
                              Index = index
                          };
            if (pss.CardType.State == StateEnum.Ok)
            {
                pss.Temperature.Value = bs.Output.Temperature*0.001m;
                pss.IntTension.Value = bs.Output.IntTension*0.001m;
                pss.Current.Value = bs.Output.Current*0.001m;
                pss.BusTension.Value = bs.Output.BusTension*0.001m;

                pss.Status.AlimentatoreInFunzione.Value = bs.Output.Status.BitIsTrue(0);
                pss.Status.TensioneFuoriRange.Value = bs.Output.Status.BitIsTrue(1);
                pss.Status.ElevatoAssortimentoCorrente.Value = bs.Output.Status.BitIsTrue(2);
                pss.Status.TemperaturaElevata.Value = bs.Output.Status.BitIsTrue(3);
                pss.Status.Value = bs.Output.Status;
            }

            pss.RefreshState();

            return pss;
        }

        private static FdsPowerCardTypes DecodCardType(int cardType)
        {
            if (cardType >= 0 && cardType <= 5 )
            {
                return (FdsPowerCardTypes) Enum.ToObject(typeof (FdsPowerCardTypes), cardType);
            }

            return FdsPowerCardTypes.Unknown;
        }

        private static FdsPowerStatusInputStream GetFdsPowerStatusInputStream(FdsPowerStatusPartStruct bs, int index)
        {
            var cps = new FdsPowerStatusInputStream
                          {
                              CardType = { Value = DecodCardType(bs.CardType) },
                              Index = index
                          };

            if (cps.State == StateEnum.Ok)
            {
                cps.Status.Value = bs.Input.Status;
                cps.Status.Configurazione.Value = bs.Input.Status.BitIsTrue(0);
                cps.Status.TensioneBusBassa.Value = bs.Input.Status.BitIsTrue(1);
                cps.Status.TensioneBusElevata.Value = bs.Input.Status.BitIsTrue(2);
                cps.Status.TensioneIngresso1Bassa.Value = bs.Input.Status.BitIsTrue(3);
                cps.Status.TensioneIngresso2Bassa.Value = bs.Input.Status.BitIsTrue(4);
                cps.Status.CorrenteErogataBassa.Value = bs.Input.Status.BitIsTrue(5);
                cps.Status.CorrenteErogataAlta.Value = bs.Input.Status.BitIsTrue(6);

                cps.Timeout.Value = bs.Input.Timeout*0.001m;
                cps.In2Tension.Value = bs.Input.In2Tension*0.001m;
                cps.In1Tension.Value = bs.Input.In1Tension*0.001m;
                cps.Current.Value = bs.Input.Current*0.001m;
                cps.BusTension.Value = bs.Input.BusTension*0.001m;
            }

            cps.RefreshState();

            return cps;
        }


        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion
}

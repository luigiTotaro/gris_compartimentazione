﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
    #region Data Struct

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsInterfacesStatusStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        internal FdsInterfaceCardStatusStruct[] LocalCards;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        internal FdsInterfaceCardStatusStruct[] RemoteCards;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsInterfaceCardStatusStruct
    {
        internal Byte CardCode;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 CardStatus;
    };

    #endregion

    #region Data Stream

    public partial class FdsInterfacesStatusStream
    {
        public FdsInterfacesStatusStream()
        {
            this.LocalCards = new List<FdsInterfaceCardStatusStream>();
            this.RemoteCards = new List<FdsInterfaceCardStatusStream>();
        }

        public List<FdsInterfaceCardStatusStream> LocalCards { get; set; }
        public List<FdsInterfaceCardStatusStream> RemoteCards { get; set; }
    }

    public partial class FdsInterfaceCardStatusStream
    {
        public ValueWithState<FdsInterfaceCardTypes> CardCode { get; private set; }
        public FdsInterfaceCardStatusBits CardStatus { get; private set; }

        public FdsInterfaceCardStatusStream()
        {
            this.CardCode = new ValueWithState<FdsInterfaceCardTypes> {FormatFunction = (x => string.Format("0x{0:X2}", (byte) x.Value))};
            this.CardCode.ValueChanged += CardCode_ValueChanged;
        }

        void CardCode_ValueChanged(ValueWithState<FdsInterfaceCardTypes> sender, EventArgs args)
        {
            switch (this.CardCode.Value)
            {
                case FdsInterfaceCardTypes.FD20000_ConsoleDTS:
                    CardStatus = new FdsInterfaceCardFD20000StatusBits();
                    break;
                case FdsInterfaceCardTypes.FD39200_ADSE:
                    CardStatus = new FdsInterfaceCardFD39200StatusBits();
                    break;
                case FdsInterfaceCardTypes.FD40X00_Telecomando:
                    CardStatus = new FdsInterfaceCardFD40X00StatusBits();
                    break;
                case FdsInterfaceCardTypes.FD60X00_BCA:
                    CardStatus = new FdsInterfaceCardFD60X00StatusBits();
                    break;
                case FdsInterfaceCardTypes.SlotEmpty:
                    CardStatus = null;
                    break;
            }
        }
    }

    public partial class FdsInterfaceCardStatusBits : ValueWithState<long>
    {
        public ValueWithState<bool> CardFailure { get; private set; }
        public ValueWithState<bool> CardWarning { get; private set; }
        public ValueWithState<bool> BusError { get; private set; }
        public ValueWithState<bool> V5Error { get; private set; }
        public ValueWithState<bool> V24Error { get; private set; }
        public ValueWithState<bool> LinkPCMNonAttivo { get; private set; }
        public ValueWithState<bool> F1On { get; private set; }
        public ValueWithState<bool> F2On { get; private set; }

        public FdsInterfaceCardStatusBits()
        {
            FormatString = "0x{0:X4}";

            CardFailure = new ValueWithState<bool> { Label = "Scheda in errore", ErrorValue = true };
            CardWarning = new ValueWithState<bool> { Label = "Scheda in allarme", ErrorValue = true };
            
            BusError = new ValueWithState<bool> { Label = "Bus Error", ErrorValue = true };
            V5Error = new ValueWithState<bool> { Label = "Errore +5V", ErrorValue = true };
            V24Error = new ValueWithState<bool> { Label = "Errore +24V", ErrorValue = true };
            LinkPCMNonAttivo = new ValueWithState<bool> { Label = "Link PCM non attivo", ErrorValue = true };
            F1On = new ValueWithState<bool> { Label = "F1 ON" };
            F2On = new ValueWithState<bool> { Label = "F2 ON" };
        }

    }

    public partial class FdsInterfaceCardFD60X00StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> V12Error { get; private set; }
        public ValueWithState<bool> MV12Error { get; private set; }
        public ValueWithState<bool> FrameError { get; private set; }

        public FdsInterfaceCardFD60X00StatusBits()
        {
            V12Error = new ValueWithState<bool> { Label = "Errore +12V", ErrorValue = true };
            MV12Error = new ValueWithState<bool> { Label = "Errore -12V", ErrorValue = true };
            FrameError = new ValueWithState<bool> { Label = "Errore frame", ErrorValue = true };
        }

    }

    public partial class FdsInterfaceCardFD40X00StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> V12Error { get; private set; }
        public ValueWithState<bool> MV12Error { get; private set; }
        public ValueWithState<bool> AICError { get; private set; }
        public ValueWithState<bool> FrameError { get; private set; }
        public ValueWithState<bool> XCall { get; private set; }
        public ValueWithState<bool> YCall { get; private set; }

        public FdsInterfaceCardFD40X00StatusBits()
        {
            V12Error = new ValueWithState<bool> { Label = "Errore +12V", ErrorValue = true };
            MV12Error = new ValueWithState<bool> { Label = "Errore -12V", ErrorValue = true };
            AICError = new ValueWithState<bool> { Label = "Errore AIC", ErrorValue = true };
            FrameError = new ValueWithState<bool> { Label = "Errore frame", ErrorValue = true };
            XCall = new ValueWithState<bool> { Label = "Errore X Call", ErrorValue = true };
            YCall = new ValueWithState<bool> { Label = "Errore Y Call", ErrorValue = true };
        }

    }

    public partial class FdsInterfaceCardFD39200StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> V12Error { get; private set; }
        public ValueWithState<bool> MV12Error { get; private set; }
        public ValueWithState<bool> AlimentatoreASDEAbilitato { get; private set; }
        public ValueWithState<bool> FrameError { get; private set; }
        public ValueWithState<RelazioneASDEEnum> RelazioneASDE { get; private set; }
        public ValueWithState<Byte> CorrenteASDE { get; private set; }


        public FdsInterfaceCardFD39200StatusBits()
        {
            V12Error = new ValueWithState<bool> { Label = "Errore +12V", ErrorValue = true };
            MV12Error = new ValueWithState<bool> { Label = "Errore -12V", ErrorValue = true };
            AlimentatoreASDEAbilitato = new ValueWithState<bool> { Label = "Alimentatore ASDE abilitato" };
            FrameError = new ValueWithState<bool> { Label = "Errore frame", ErrorValue = true };
            RelazioneASDE = new ValueWithState<RelazioneASDEEnum> { Label = "Errore X Call", ErrorValue = RelazioneASDEEnum.ApertaInAttesaRipristino, WarningValue = RelazioneASDEEnum.ApertaInRichiusura };
            CorrenteASDE = new ValueWithState<Byte>
                           {
                               Label = "Corrente di relazione",
                               StateMessageRules = new[]
                               {
                                   new StateMessageRule<byte>
                                       {
                                           Rule = (x => x.Value == 63),
                                           Message = new StateMessage { Message = "Corrente relazione ASDE oltre la soglia massima", State = StateEnum.Error } 
                                       },
                                   new StateMessageRule<byte>
                                       {
                                           Rule = (x => x.Value == 0),
                                           Message = new StateMessage { Message = "Corrente relazione ASDE nulla", State = StateEnum.Warning }
                                       },
                               }
                           };
        }
    }

    public partial class FdsInterfaceCardFD20000StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> LocalSyncError { get; private set; }
        public ValueWithState<bool> RemoteSyncError { get; private set; }

        public FdsInterfaceCardFD20000StatusBits()
        {
            LocalSyncError = new ValueWithState<bool> { Label = "Errore sincronizzazione locale", ErrorValue = true };
            RemoteSyncError = new ValueWithState<bool> { Label = "Errore sincronizzazione remota", ErrorValue = true };
        }
    }

    #endregion

    #region Stream MapB2O

    public partial class FdsInterfacesStatusStreamMapB2O : IDeviceStreamMapB2O
    {

        #region IDeviceStreamMapB2O<FdsInterfaceCardStatusStream> Members

        public FdsInterfacesStatusStream GetStream(byte[] data)
        {
            var bs = BinaryHelper<FdsInterfacesStatusStruct>.RawStringBytesDeserialize(data);
            var stream = new FdsInterfacesStatusStream();

            foreach (FdsInterfaceCardStatusStruct card in bs.LocalCards)
            {
                var cardStatus = new FdsInterfaceCardStatusStream();

                cardStatus.CardCode.Value = (FdsInterfaceCardTypes)Enum.ToObject(typeof(FdsInterfaceCardTypes), card.CardCode);

                SetCardStatus(card.CardStatus, cardStatus.CardStatus);

                stream.LocalCards.Add(cardStatus);
            }

            foreach (FdsInterfaceCardStatusStruct card in bs.RemoteCards)
            {
                var cardStatus = new FdsInterfaceCardStatusStream();

                cardStatus.CardCode.Value = (FdsInterfaceCardTypes)Enum.ToObject(typeof(FdsInterfaceCardTypes), card.CardCode);

                SetCardStatus(card.CardStatus, cardStatus.CardStatus);

                stream.RemoteCards.Add(cardStatus);
            }

            return stream;

        }

        #endregion

        private static void SetCardStatus(uint intCardStatus, FdsInterfaceCardStatusBits cardStatus)
        {
            if (cardStatus == null) return;


            cardStatus.CardFailure.Value = intCardStatus.BitIsTrue(27);
            cardStatus.CardWarning.Value = intCardStatus.BitIsTrue(28);
            
            
            cardStatus.BusError.Value = intCardStatus.BitIsTrue(23);
            cardStatus.V5Error.Value = intCardStatus.BitIsTrue(22);\\
            cardStatus.V24Error.Value = intCardStatus.BitIsTrue(21);
            cardStatus.LinkPCMNonAttivo.Value = intCardStatus.BitIsTrue(15);
            cardStatus.F1On.Value = intCardStatus.BitIsTrue(14);
            cardStatus.F2On.Value = intCardStatus.BitIsTrue(13);

            if (cardStatus is FdsInterfaceCardFD20000StatusBits)
            {
                var cs = (FdsInterfaceCardFD20000StatusBits)cardStatus;
                cs.LocalSyncError.Value = intCardStatus.BitIsTrue(20);
                cs.RemoteSyncError.Value = intCardStatus.BitIsTrue(19);
            }
            else if (cardStatus is FdsInterfaceCardFD39200StatusBits)
            {
                var cs = (FdsInterfaceCardFD39200StatusBits)cardStatus;
                cs.V12Error.Value = intCardStatus.BitIsTrue(20);
                cs.MV12Error.Value = intCardStatus.BitIsTrue(19);
                cs.AlimentatoreASDEAbilitato.Value = intCardStatus.BitIsTrue(12);
                cs.FrameError.Value = intCardStatus.BitIsTrue(10);  // collegamento tra periferiche
                if (!intCardStatus.BitIsTrue(18))
                {
                    cs.RelazioneASDE.Value = RelazioneASDEEnum.Attiva;
                }
                else
                {
                    cs.RelazioneASDE.Value = !intCardStatus.BitIsTrue(11)
                                                 ? RelazioneASDEEnum.ApertaInRichiusura
                                                 : RelazioneASDEEnum.ApertaInAttesaRipristino;
                }

                cs.CorrenteASDE.Value = (byte)intCardStatus.GetBitsValue(2, 7);
            }
            else if (cardStatus is FdsInterfaceCardFD40X00StatusBits)
            {
                var cs = (FdsInterfaceCardFD40X00StatusBits)cardStatus;
                cs.V12Error.Value = intCardStatus.BitIsTrue(20);
                cs.MV12Error.Value = intCardStatus.BitIsTrue(19);
                cs.FrameError.Value = intCardStatus.BitIsTrue(12);
                cs.AICError.Value = intCardStatus.BitIsTrue(18);
                cs.XCall.Value = intCardStatus.BitIsTrue(11);
                cs.YCall.Value = intCardStatus.BitIsTrue(10);
            }
            else if (cardStatus is FdsInterfaceCardFD60X00StatusBits)
            {
                var cs = (FdsInterfaceCardFD60X00StatusBits)cardStatus;
                cs.V12Error.Value = intCardStatus.BitIsTrue(20);
                cs.MV12Error.Value = intCardStatus.BitIsTrue(19);
                cs.FrameError.Value = intCardStatus.BitIsTrue(18);
            }
        }

        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion

}    

﻿namespace TelefinStreamReader.DeviceStreams
{
    public class FdsDevice : ObjectWithState
    {
        public FdsGeneralStatusStream GeneralStatus { get; set; }   // 1 = Stato generale
        public FdsCPUsMeasureStream CPUsMeasures { get; set; }      // 2 = Tensioni e temperature
        public FdsPowerStatusStream PowerStatus { get; set; }       // 3 = Configurazione e stato alimentazione
        public FdsInterfacesStatusStream InterfacesStatus { get; set; } // 4 = Configurazione e stato interfacce
        public FdsE1CountersStream E1Counters { get; set; }         // 5 = Contatori E1
        public FdsGeneralInfoStream GeneralInfo { get; set; }       // 6 = Intormazioni Generali
    }

    public class FdsDeviceMapB2O : IDeviceMapB2O
    {
        public FdsDevice GetDevice(byte[][] data)
        {
            FdsDevice fds = new FdsDevice();

            var GeneralStatusMapB2O = new FdsGeneralStatusStreamMapB2O();
            var CPUsMeasureMapB2O = new FdsCPUsMeasureStreamMapB2O(fds);
            var PowerStatusMapB2O = new FdsPowerStatusStreamMapB2O();
            var InterfacesStatusMapB2O = new FdsInterfacesStatusStreamMapB2O();
            var E1CountersMapB2O = new FdsE1CountersStreamMapB2O();
            var GeneralInfoMapB2O = new FdsGeneralInfoStreamMapB2O();

            fds.GeneralStatus = GeneralStatusMapB2O.GetStream(data[0]);
            fds.CPUsMeasures = CPUsMeasureMapB2O.GetStream(data[1]);
            fds.PowerStatus = PowerStatusMapB2O.GetStream(data[2]);
            fds.InterfacesStatus = InterfacesStatusMapB2O.GetStream(data[3]);
            fds.E1Counters = E1CountersMapB2O.GetStream(data[4]);
            fds.GeneralInfo = GeneralInfoMapB2O.GetStream(data[5]);

            return fds;
        }

        #region IDeviceMapB2O Members

        public IObjectWithState GetStream(byte[][] data)
        {
            return GetDevice(data);
        }

        #endregion
    }
}

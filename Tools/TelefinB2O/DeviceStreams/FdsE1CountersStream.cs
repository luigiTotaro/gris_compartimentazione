﻿using System;
using System.Runtime.InteropServices;

namespace TelefinStreamReader.DeviceStreams
{
    #region Data Struct

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsE1CountersStruct
    {
        internal FdsE1StatusStruct Local;
        internal FdsE1StatusStruct Remote;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsE1StatusStruct
    {
        internal UInt16 Alarms;
        internal UInt16 Sync;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 BERC;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 RCER;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 EFASC;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 BVEC;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 EBEC;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 C4ER;
    };

    #endregion

    #region Data Stream

    public partial class FdsE1CountersStream
    {
        internal FdsE1StatusStream Local { get; set; }
        public FdsE1StatusStream Remote { get; set; }

        public FdsE1CountersStream()
        {
            Local = new FdsE1StatusStream();
            Remote = new FdsE1StatusStream();
        }

    }

    public partial class FdsE1StatusStream
    {
        public FdsE1AllarmsBits Alarms { get; private set; }
        public FdsE1SyncBits Sync { get; private set; }
        public ValueWithState<long> BitErrorRateCounter { get; private set; }
        public ValueWithState<long> RAIContCRCErrorCounter { get; private set; }
        public ValueWithState<long> ErroredFrameAlignSignalCounter { get; private set; }
        public ValueWithState<long> BipolarViolationErrorCounter { get; private set; }
        public ValueWithState<long> EBitErrorCounter { get; private set; }
        public ValueWithState<long> CRC4ErrorCounter { get; private set; }

        public FdsE1StatusStream()
        {
            Alarms = new FdsE1AllarmsBits { Label = "Allarmi" };
            Sync = new FdsE1SyncBits { Label = "Sincronismi E1" };
            BitErrorRateCounter = new ValueWithState<long> { Label = "Bit Error Rate Counter" };
            RAIContCRCErrorCounter = new ValueWithState<long> { Label = "RAI Cont CRC Error Counter" };
            ErroredFrameAlignSignalCounter = new ValueWithState<long> { Label = "Errored Frame Align Signal Counter" };
            BipolarViolationErrorCounter = new ValueWithState<long> { Label = "Bipolar Violation Error Counter" };
            EBitErrorCounter = new ValueWithState<long> { Label = "EBit Error Counter" };
            CRC4ErrorCounter = new ValueWithState<long> { Label = "CRC-4 Error Counter" };
        }
    };


    public partial class FdsE1AllarmsBits : ValueWithState<long>
    {
        public ValueWithState<IndefinitoAcquisitoEnum> StatoE1 { get; private set; }
        public ValueWithState<bool> SyncFramePresente { get; private set; }
        public ValueWithState<bool> LinkE1Stabile { get; private set; }
        public ValueWithState<AcquiredLossEnum> SignalStatusIndicator { get; private set; }
        public ValueWithState<NormalAlarmEnum> AlarmIndicatorStatusSignal { get; private set; }
        public ValueWithState<NormalAlarmEnum> RemoteAlarmIndicatorStatusSignal { get; private set; }
        public ValueWithState<NormalAlarmEnum> RemoteAlarmIndicatorCrcError { get; private set; }

        public FdsE1AllarmsBits()
        {
            FormatString = "0x{0:X4}";
            StatoE1 = new ValueWithState<IndefinitoAcquisitoEnum>
                               {
                                   Label = "Stato E1",
                                   ErrorValue = IndefinitoAcquisitoEnum.Indefinito
                               };
            SyncFramePresente = new ValueWithState<bool>
                                        {
                                            Label = "Segnale Sincronizzazione FRAME Presente",
                                            ErrorValue = false
                                        };
            LinkE1Stabile = new ValueWithState<bool>
                                    {
                                        Label = "Link E1 stabile",
                                        ErrorValue = false
                                    };
            SignalStatusIndicator = new ValueWithState<AcquiredLossEnum>
            {
                Label = "Signal Status Indication",
                ErrorValue = AcquiredLossEnum.Loss
            };
            AlarmIndicatorStatusSignal = new ValueWithState<NormalAlarmEnum>
            {
                Label = "Alarm Indication Status Signal (AISS)",
                ErrorValue = NormalAlarmEnum.Alarm
            };
            RemoteAlarmIndicatorStatusSignal = new ValueWithState<NormalAlarmEnum>
            {
                Label = "Remote Alarm Ind. Status Signal (RAI)",
                ErrorValue = NormalAlarmEnum.Alarm
            };
            RemoteAlarmIndicatorCrcError = new ValueWithState<NormalAlarmEnum>
            {
                Label = "RAI and Continuous CRC Error Status",
                ErrorValue = NormalAlarmEnum.Alarm
            };
        }

    }

    public partial class FdsE1SyncBits : ValueWithState<long>
    {
        public ValueWithState<AcquiredLossEnum> ReceiveBasicFrameAligment { get; private set; }
        public ValueWithState<AcquiredLossEnum> ReceiveMultiframeAligment { get; private set; }
        public ValueWithState<AcquiredLossEnum> ReceiveCrc4Synchronization { get; private set; }
        public ValueWithState<byte> BitE1 { get; private set; }
        public ValueWithState<byte> BitE2 { get; private set; }
        public ValueWithState<CrcEnum> Crc4Internetworking { get; private set; }

        public FdsE1SyncBits()
        {
            FormatString = "0x{0:X4}";

            this.ReceiveBasicFrameAligment = new ValueWithState<AcquiredLossEnum>
            {
                Label = "Receive Basic Frame Aligment",
                ErrorValue = AcquiredLossEnum.Loss
            };
            this.ReceiveMultiframeAligment = new ValueWithState<AcquiredLossEnum>
            {
                Label = "Receive Multiframe Aligment",
                ErrorValue = AcquiredLossEnum.Loss
            };
            this.ReceiveCrc4Synchronization = new ValueWithState<AcquiredLossEnum>
            {
                Label = "CRC-4 Internetworking",
                ErrorValue = AcquiredLossEnum.Loss
            };
            this.BitE1 = new ValueWithState<byte>
            {
                Label = "Bit E1",
            };
            this.BitE2 = new ValueWithState<byte>
            {
                Label = "Bit E2",
            };
            this.Crc4Internetworking = new ValueWithState<CrcEnum>
            {
                Label = "CRC-4 Internetworking",
            };
        }

    }

    #endregion

    #region Stream Provider

    public partial class FdsE1CountersStreamMapB2O : IDeviceStreamMapB2O
    {

        #region IDeviceStreamMapB2O<FdsE1StatusStream> Members

        public FdsE1CountersStream GetStream(byte[] data)
        {
            var bs = BinaryHelper<FdsE1CountersStruct>.RawStringBytesDeserialize(data);

            var stream = new FdsE1CountersStream
                             {
                                 Remote =
                                     {
                                         BipolarViolationErrorCounter = { Value = bs.Remote.BVEC },
                                         Alarms = { Value = bs.Remote.Alarms },
                                         Sync = { Value = bs.Remote.Sync },
                                         BitErrorRateCounter = { Value = bs.Remote.BERC },
                                         RAIContCRCErrorCounter = { Value = bs.Remote.RCER },
                                         ErroredFrameAlignSignalCounter = { Value = bs.Remote.EFASC },
                                         EBitErrorCounter = { Value = bs.Remote.EBEC },
                                         CRC4ErrorCounter = { Value = bs.Remote.C4ER }
                                     },
                                 Local =
                                     {
                                         Sync = { Value = bs.Local.Sync },
                                         Alarms = { Value = bs.Local.Alarms },
                                         BitErrorRateCounter = { Value = bs.Local.BERC },
                                         RAIContCRCErrorCounter = { Value = bs.Local.RCER },
                                         ErroredFrameAlignSignalCounter = { Value = bs.Local.EFASC },
                                         BipolarViolationErrorCounter = { Value = bs.Local.BVEC },
                                         EBitErrorCounter = { Value = bs.Local.EBEC },
                                         CRC4ErrorCounter = { Value = bs.Local.C4ER }
                                     }
                             };

            SetAlarmsBits(bs.Local.Alarms, stream.Local.Alarms);
            SetSyncBits(bs.Local.Sync, stream.Local.Sync);

            SetAlarmsBits(bs.Remote.Alarms, stream.Remote.Alarms);
            SetSyncBits(bs.Remote.Sync, stream.Remote.Sync);

            return stream;

        }

        #endregion

        private void SetAlarmsBits(ushort alarmsValue, FdsE1AllarmsBits alarmsBits)
        {
            alarmsBits.StatoE1.Value = alarmsValue.BitIsTrue(15)
                                                            ? IndefinitoAcquisitoEnum.Indefinito
                                                            : IndefinitoAcquisitoEnum.Acquisito;


            alarmsBits.SyncFramePresente.Value = alarmsValue.BitIsTrue(9)
                                                                        ? false
                                                                        : true;

            alarmsBits.LinkE1Stabile.Value = alarmsValue.BitIsTrue(8)
                                                                        ? false
                                                                        : true;

            alarmsBits.SignalStatusIndicator.Value = alarmsValue.BitIsTrue(4)
                                                                        ? AcquiredLossEnum.Loss
                                                                        : AcquiredLossEnum.Acquired;

            alarmsBits.AlarmIndicatorStatusSignal.Value = alarmsValue.BitIsTrue(2)
                                                                        ? NormalAlarmEnum.Alarm
                                                                        : NormalAlarmEnum.Normal;

            alarmsBits.RemoteAlarmIndicatorStatusSignal.Value = alarmsValue.BitIsTrue(1)
                                                                        ? NormalAlarmEnum.Alarm
                                                                        : NormalAlarmEnum.Normal;

            alarmsBits.RemoteAlarmIndicatorCrcError.Value = alarmsValue.BitIsTrue(0)
                                                                        ? NormalAlarmEnum.Alarm
                                                                        : NormalAlarmEnum.Normal;

        }

        private void SetSyncBits(ushort syncValue, FdsE1SyncBits syncBits)
        {
            syncBits.ReceiveBasicFrameAligment.Value =syncValue.BitIsTrue(7)? AcquiredLossEnum.Loss: AcquiredLossEnum.Acquired;
            syncBits.ReceiveMultiframeAligment.Value =syncValue.BitIsTrue(6)? AcquiredLossEnum.Loss: AcquiredLossEnum.Acquired;
            syncBits.ReceiveCrc4Synchronization.Value =syncValue.BitIsTrue(5)? AcquiredLossEnum.Loss: AcquiredLossEnum.Acquired;
            syncBits.BitE1.Value =syncValue.BitIsTrue(4)? (byte)1: (byte)0;
            syncBits.BitE2.Value =syncValue.BitIsTrue(3)? (byte)1: (byte)0;
            syncBits.Crc4Internetworking.Value =syncValue.BitIsTrue(0)? CrcEnum.CrcCrc: CrcEnum.CrcNonCrc;
        }


        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion

}


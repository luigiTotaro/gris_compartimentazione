﻿namespace TelefinStreamReader.DeviceStreams
{
    #region Enumerati
    public enum IndefinitoAcquisitoEnum { Indefinito, Acquisito }
    public enum VuotoNonVuotoEnum { Vuoto, NonVuoto }
    public enum PrincipaleSecondariaEnum { Principale, Secondaria }
    public enum MasterSlaveEnum { Master, Slave }
    public enum OtticaG703Enum { Ottica, G703 }
    public enum CorrettoIncrociatoEnum { Corretto, Incrociato }
    public enum AcquiredLossEnum { Acquired, Loss }
    public enum NormalAlarmEnum { Normal, Alarm }
    public enum CrcEnum {CrcCrc,CrcNonCrc}
    public enum RelazioneASDEEnum {Attiva,ApertaInRichiusura,ApertaInAttesaRipristino}
    public enum CorrenteASDEEnum { Nulla, Normale, SopraSoglia }

    public enum FdsInterfaceCardTypes : byte
    {
        FD60X00_BCA = 99,
        FD40X00_Telecomando = 66,
        FD39200_ADSE = 33,
        FD20000_ConsoleDTS = 96,
        SlotEmpty = byte.MaxValue
    }

    public enum FdsPowerCardTypes
    {
        SlotEmpty = 0,
        Unknown_NotReadable = 1,
        FD13000 = 2,
        FD11000_V5 = 3,
        FD11100_V12 = 4,
        FD11200_M12 = 5,
        Unknown = int.MaxValue
    }

    //public enum PresenteAssenteEnum { Presente, Assente }
    //public enum StabileInstabileEnum { Stabile, Instabile }
    #endregion



}

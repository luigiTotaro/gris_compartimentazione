﻿
using CommandLine;
using CommandLine.Text;

namespace AbsPathRepl
{
	/// <summary>
	/// Classe wrapper per i parametri passati da riga di comando.
	/// </summary>
	class Options
	{
		/// <summary>
		/// [Opzionale] (--list o -l) Indica il percorso del file che contiene la lista dei file '.pro' in cui effettuare la sostituzione. Se non specificato, il programma esegue una scansione ricorsiva alla ricerca di tutti i files '.pro'. Se specificato il parametro --repl, questo parametro viene ignorato.
		/// <remarks>es. absrepl -l "c:\MyDir\files.conf"</remarks>
		/// </summary>
		[Option('l', "list", HelpText = "(Default: files.conf nella cartella corrente) Nome completo del file che contiene la lista dei file di configurazione in cui effettuare la sostituzione.")]
		public string ListFile { get; set; }

		/// <summary>
		/// [Opzionale] (--repl o -r) Indica il percorso del file singolo in cui si intende effettuare la sostituzione. Questo parametro ha la precedenza sul parametro --list. Se non specificato, il programma esegue una scansione ricorsiva alla ricerca di tutti i files '.pro'.
		/// <remarks>es. absrepl -r "c:\MyDir\script.sql"</remarks>
		/// </summary>
		[Option('r', "repl", HelpText = "(Default: null) Nome completo del file in cui effettuare la sostituzione.")]
		public string ReplFile { get; set; }

		/// <summary>
		/// [Opzionale] Indica il tipo di sostituzione da eseguire. 1) all -> tutti i tipi di segnaposto. 2) -> solo i segnaposto di tipo Windows. 3) -> solo i segnaposto di tipo Unix. Se non specificato è "all".
		/// <remarks>es. absrepl -f unix</remarks>
		/// </summary>
		[Option('f', "pathformat", DefaultValue = "all", HelpText = "Formato in cui va scritto il path da inserire nei file di configurazione: all, win, unix.")]
		public string PathFormat { get; set; }

		/// <summary>
		/// [Opzionale] Indica il percorso di base da sostituire al segnaposto. Se non specificato, inserisce il path della cartella corrente.
		/// <remarks>es. absrepl -p "c:\BaseDir\"</remarks>
		/// </summary>
		[Option('p', "appath", HelpText = "(Default: Cartella corrente) Path da inserire nei file di configurazione. Viene usata la cartella corrente quando non specificato.")]
		public string AppPath { get; set; }

		/// <summary>
		/// [Opzionale] Indica il nome della macchina. Se non specificato la stringa predefinita sarà stringa vuota.
		/// <remarks>es. absrepl -n stlc1000</remarks>
		/// </summary>
		[Option('n', "srvnameph", DefaultValue = "", HelpText = "Nome Server.")]
		public string ServerName { get; set; }

		/// <summary>
		/// [Opzionale] Indica se si vuole effettuare solo la sostituzione dei segnaposto o se si vuole anche sovrascrivere i file di configurazione originali. Se non specificato, i file di configurazione originali vengono sovrascritti.
		/// <remarks>es. absrepl -d</remarks>
		/// </summary>
		[Option('d', "dontreplorig", DefaultValue = false, HelpText = "Indica che non si vogliono sostituire i file di configurazione originali.")]
		public bool DoNotReplaceOriginal { get; set; }

		/// <summary>
		/// [Opzionale] Indica la stringa segnaposto per i path di tipo Windows. Se non specificato la stringa predefinita sarà "APP_PATH".
		/// <remarks>es. absrepl -w PERCORSO_APP_WIN</remarks>
		/// </summary>
		[Option('w', "winph", DefaultValue = "APP_PATH", HelpText = "Segnaposto per i percorsi con formato tipo Windows.")]
		public string WinPlaceholder { get; set; }

		/// <summary>
		/// [Opzionale] Indica la stringa segnaposto per i path di tipo Unix. Se non specificato la stringa predefinita sarà "APP_PATH.NIX".
		/// <remarks>es. absrepl -u PERCORSO_APP_UNIX</remarks>
		/// </summary>
		[Option('u', "unixph", DefaultValue = "APP_PATH.NIX", HelpText = "Segnaposto per i percorsi con formato tipo Unix.")]
		public string UnixPlaceholder { get; set; }

		/// <summary>
		/// [Opzionale] Indica la stringa segnaposto per il nome della macchina. Se non specificato la stringa predefinita sarà "SERVER_NAME".
		/// <remarks>es. absrepl -s NOME_SERVER</remarks>
		/// </summary>
		[Option('s', "srvnameph", DefaultValue = "SERVER_NAME", HelpText = "Segnaposto per il nome Server.")]
		public string ServerPlaceholder { get; set; }

		[ParserState]
		public IParserState LastParserState { get; set; }

		[HelpOption]
		public string GetUsage()
		{
			return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
		}
	}
}

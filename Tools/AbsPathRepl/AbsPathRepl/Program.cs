﻿
using System;

namespace AbsPathRepl
{
	/// <summary>
	/// Applicazione per eseguire il replace dei segnaposto inseriti nei file '.pro' al fine di rendere dinamici i path assoluti attualmente contenuti nei file di configurazione degli applicativi della Client GrisSuite.
	/// <remarks>Eseguire l'applicativo passando il parametro --help (-h) per visualizzare l'elenco dei parametri disponibili.</remarks>
	/// </summary>
	class Program
	{
		/// <summary>
		/// Procedura principale in cui vengono eseguiti: 1) Caricamento dei files '.pro' da file contenente la lista o attraverso ricerca. 2) Sostituzione dei segnaposto. 3) Sovrascrittura dei file di configurazione originali.
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			var options = new Options();
			if (CommandLine.Parser.Default.ParseArguments(args, options))
			{
				Console.WriteLine("List file: {0}, Replace file: {1}, Path Format: {2}, App Path: {3}, Server Name: {4}, Don't Replace Original: {5}.", options.ListFile, options.ReplFile, options.PathFormat, options.AppPath, options.ServerName, options.DoNotReplaceOriginal);

				try
				{
					var replacer = new ReplFiles(options.ListFile, options.ReplFile, options.WinPlaceholder, options.UnixPlaceholder, options.ServerPlaceholder);
					replacer.Load();
					replacer.ReplacePaths(options.AppPath, options.ServerName, CastPathFormat(options));
					if (!options.DoNotReplaceOriginal) replacer.ReplaceFiles();
				}
				catch (Exception exc)
				{
					Console.WriteLine("Errore durante l'esecuzione del programma: {0}.\n Stack trace: {1}.", exc.Message, exc.StackTrace);
				}
			}
			else
			{
				if (args.Length > 0 && args[0] != "--help" && args[0] != "-h")
				{
					Console.WriteLine("Uno o più parametri non corretti.");					
				}
			}
		}

		static PathFormat CastPathFormat(Options opt)
		{
			if (string.IsNullOrEmpty(opt.PathFormat)) return PathFormat.All; // opzione non fornita

			switch (opt.PathFormat.ToLower())
			{
				case "all":
					{
						return PathFormat.All;
					}
				case "win":
					{
						return PathFormat.Win;
					}
				case "unix":
					{
						return PathFormat.Unix;
					}
				default:
					{
						// se l'opzione è stata fornita ma non riconosciuta
						return PathFormat.None;
					}
			}
		}
	}
}

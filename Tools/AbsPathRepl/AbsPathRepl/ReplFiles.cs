﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AbsPathRepl
{
	/// <summary>
	/// Classe che contiene la logica principale dell'applicazione: caricamento della lista dei file da elaborare, elaborazione e sostituzione dei file.
	/// </summary>
	public class ReplFiles
	{
		private readonly string _listFilePath;
		private readonly string _fileToRepl;
		private readonly string _winPlaceHolder;
		private readonly string _unixPlaceHolder;
		private readonly string _servPlaceHolder;
		private readonly List<FileInfo> _replFiles;

		/// <summary>
		/// Costruttore della classe di replace.
		/// </summary>
		/// <param name="listFilePath">Path del file contenente la lista dei file '.pro'</param>
		/// <param name="fileToRepl">Path del file in cui eseguire la sostituzione</param>
		/// <param name="winPlaceHolder">Segnaposto per il formato Windows</param>
		/// <param name="unixPlaceHolder">Segnaposto per il formato Unix</param>
		///<param name="servPlaceHolder"> </param>
		///<remarks>
		///		Quando è stato indicato un valore per il parametro fileToRepl il valore del parametro listFilePath viene ignorato.
		/// </remarks>
		public ReplFiles(string listFilePath, string fileToRepl, string winPlaceHolder, string unixPlaceHolder, string servPlaceHolder)
		{
			_replFiles = new List<FileInfo>();
			_listFilePath = string.IsNullOrEmpty(listFilePath) ? "files.conf" : listFilePath;
			_fileToRepl = fileToRepl;
			_winPlaceHolder = winPlaceHolder;
			_unixPlaceHolder = unixPlaceHolder;
			_servPlaceHolder = servPlaceHolder;
		}

		/// <summary>
		/// Path del file contenente la lista dei file '.pro'.
		/// </summary>
		public string ListFilePath
		{
			get { return _listFilePath; }
		}

		/// <summary>
		/// Segnaposto per il formato Windows.
		/// </summary>
		public string WinPlaceHolder
		{
			get { return _winPlaceHolder; }
		}

		/// <summary>
		/// Segnaposto per il formato Unix.
		/// </summary>
		public string UnixPlaceHolder
		{
			get { return _unixPlaceHolder; }
		}

		public string FileToRepl
		{
			get { return _fileToRepl; }
		}

		/// <summary>
		/// Segnaposto per il nome server.
		/// </summary>
		public string ServPlaceHolder
		{
			get { return _servPlaceHolder; }
		}

		/// <summary>
		/// Carica i file '.pro' in cui eseguire la sostituzione dei segnaposto.
		/// </summary>
		public void Load()
		{
			FileInfo replFile;
			FileInfo replListFile;

			this._replFiles.Clear();

			if (!string.IsNullOrEmpty(this._fileToRepl) && (replFile = new FileInfo(this._fileToRepl)).Exists)
			{
				this._replFiles.Add(replFile);
				Console.WriteLine("File '{0}' caricato.", replFile.FullName);
				return;
			}

			if (!string.IsNullOrEmpty(this._listFilePath) && (replListFile = new FileInfo(this._listFilePath)).Exists)
			{
				Console.WriteLine("Lista dei file in caricamento da '{0}'.", this._listFilePath);
				Console.WriteLine("Elaborazione del file lista.");
				var sr = replListFile.OpenText();

				string relativePath;
				while ((relativePath = sr.ReadLine()) != null)
				{
					string absolutePath = Path.Combine(Environment.CurrentDirectory, relativePath);
					if (File.Exists(absolutePath))
					{
						this._replFiles.Add(new FileInfo(absolutePath));
						Console.WriteLine("File '{0}' caricato.", absolutePath);
					}
					else
					{
						Console.WriteLine("File '{0}' non trovato.", relativePath);
					}
				}
				return;
			}

			Console.WriteLine("File lista o file indicato non trovato, ricerca dei file '.pro'!");
			this._replFiles.AddRange(Directory.EnumerateFiles(Environment.CurrentDirectory, "*.pro", SearchOption.AllDirectories).Select(f => new FileInfo(f)));
			foreach (var rplFile in this._replFiles) Console.WriteLine("File '{0}' caricato.", rplFile.FullName);
		}

		/// <summary>
		/// Esegue la sostituzione dei segnaposto.
		/// </summary>
		/// <param name="path">Path sostituire ai segnaposto</param>
		/// <param name="serverName">Nome del server da usare nella sostituzione</param>
		/// <param name="nixPath">Formato del path da inserire</param>
		public void ReplacePaths(string path = null, string serverName = "", PathFormat nixPath = PathFormat.All)
		{
			if (nixPath == PathFormat.None)
			{
				Console.WriteLine("Tipo di formato path non riconosciuto, sostituzione non effettuata.");
				return;
			}

			if (string.IsNullOrEmpty(path)) path = Environment.CurrentDirectory;

			if (this._replFiles != null && this._replFiles.Count > 0 && Directory.Exists(path))
			{
				Console.WriteLine("{0} file da elaborare.", this._replFiles.Count);
				foreach (var file in this._replFiles)
				{
					switch (nixPath)
					{
						case PathFormat.All:
							{
								File.WriteAllText(file.FullName,
									File.ReadAllText(file.FullName)
										.Replace(string.Format("<{0}>", this._winPlaceHolder), path.TrimEnd('\\'))
										.Replace(string.Format("<{0}>", this._unixPlaceHolder), path.Replace("\\", "/").TrimEnd('/'))
										.Replace(string.Format("<{0}>", this._servPlaceHolder), serverName));
								break;
							}
						case PathFormat.Win:
							{
								File.WriteAllText(file.FullName, File.ReadAllText(file.FullName).Replace(string.Format("<{0}>", this._winPlaceHolder), path.TrimEnd('\\')));
								break;
							}
						case PathFormat.Unix:
							{
								File.WriteAllText(file.FullName, File.ReadAllText(file.FullName).Replace(string.Format("<{0}>", this._unixPlaceHolder), path.Replace("\\", "/").TrimEnd('/')));
								break;
							}
						case PathFormat.Serv:
							{
								File.WriteAllText(file.FullName, File.ReadAllText(file.FullName).Replace(string.Format("<{0}>", this._servPlaceHolder), serverName));
								break;
							}
					}
					Console.WriteLine("Sostituzione effettuata nel file '{0}'.", file.FullName);
				}
			}
			else
			{
				Console.WriteLine("File su cui effettuare la sostituzione non presenti.");
			}
		}

		/// <summary>
		/// Esegue la sovrascrittura dei file di configurazione originali.
		/// </summary>
		public void ReplaceFiles()
		{
			if (this._replFiles != null && this._replFiles.Count > 0)
			{
				Console.WriteLine("Replace dei file di configurazione in corso...");

				foreach (var file in this._replFiles)
				{
					if (file.FullName.EndsWith(".pro"))
					{
						string proFilename = file.FullName;
						string configFilename = file.FullName.Substring(0, file.FullName.Length - ".pro".Length);

						try
						{
							if (File.Exists(configFilename)) File.Delete(configFilename);

							File.Move(proFilename, configFilename);
							Console.WriteLine("Replace del file '{0}' effettuato.", configFilename);
						}
						catch (Exception exc)
						{
							Console.WriteLine("Errore durante la sostituzione del file di configurazione '{0}': {1}.", configFilename, exc.Message);
						}
					}
					else
					{
						Console.WriteLine("Il nome del file '{0}' non termina con '.pro'.", file.FullName);
					}
				}
			}
		}
	}
}

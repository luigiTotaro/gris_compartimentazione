using System;
using System.Windows.Forms;

namespace ConvertSN2Number {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void btnConvert_Click(object sender, EventArgs e) {
            if (mtxtSN.Text != "  ,  ,")
                lblNumber.Text = StringToSerialNumber(mtxtSN.Text);
        }

        private static string StringToSerialNumber(string SN) {
            try {

                string[] sn = SN.Replace(',', '.').Split('.');

                byte byAnno = byte.Parse(sn[0]);
                byte byWeek = byte.Parse(sn[1]);
                ushort wProg = ushort.Parse(sn[2]);

                int dwLetto = 0;
                dwLetto |= (byAnno << 24);
                dwLetto |= (byWeek << 16);
                dwLetto |= (wProg);

                return dwLetto.ToString();
            }
            catch (FormatException) {
                return "N/D";
            }
        }

        private static string SerialNumberToString(string SN) {
            int number;

            if (Int32.TryParse(SN, out number)) {
                byte byAnno = BitConverter.GetBytes(number)[3];
                byte byWeek = BitConverter.GetBytes(number)[2];
                ushort wProg = (ushort)((BitConverter.GetBytes(number)[1] << 8) + (BitConverter.GetBytes(number)[0]));

                return byAnno.ToString("00") + "." + byWeek.ToString("00") + "." + wProg.ToString("000");
            }

            return "N/D";
        }

        private void btnConvertNumberToSN_Click(object sender, EventArgs e) {
            this.lblSN.Text = SerialNumberToString(this.txtNumber.Text);
        }
    }
}
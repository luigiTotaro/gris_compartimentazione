
//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		AmpliConfig.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		24.05.2007
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace Telefin.AutoConfigurator.Class
{
    public class AmpliConfig
    {
        #region Internal Class / Struct

        public struct URANGE 
        {
            public UInt16 min;      
            public UInt16 MAX;         
        }
        public struct IRANGE
        {
            public Int16 min;
            public Int16 MAX;
        }

        public struct FunctionParams
        {
            public Byte []  ParametersVersion;        // 6 byte fissi (non usati in questo programma)
        }


        public struct ADS_SYS_CONFIG
        {


        }

        #endregion

        #region Token

//        private const int       TOK_BYTES_BUFFER_LEN            = 145;
        private const int       TOK_BYTES_BUFFER_LEN = 90;

        private const int       TOK_SW_VERSION_LENGTH           = 13; 
        private const int       TOK_CONFIG_VERSION_LENGTH       = 17;

        private const string    TOK_BUFFER_LEN_STR              = @" BufferLen: ";
        private const string    TOK_BUFFER_STR                  = @"    Buffer: ";

        private const string    TOK_ERROR_INPUT_BYTE_LEN        = @"Lunghezza buffer di ingresso errata";

        #endregion

        #region Private Members



        private char [] _cConfigVersion;        // 17 caratteri versione configurazione sistema 
        private Byte    _nSetErrorTimeout;
        private Byte    _nClearErrorTimeout;
        private Byte    _nSetEventTimeout;
        private Byte    _nClearEventTimeout;
        private URANGE  _oBatteryVoltageRange;
        private URANGE  _oInputVoltageRange;
        private URANGE  _oBatteryChargeCurrentRange;
        private URANGE  _oBatteryDischargeCurrentRange;
        private IRANGE  _oTransformerTemperatureRange;
        private IRANGE  _oTransistorTemperatureRange;

        private Byte []             _btSerialize;           // serialize buffer;

        #endregion

        #region Accessor

        public Byte[] SerializeBuffer   
        {
            get {return(_btSerialize);}
        }

        public char [] ConfigVersion    
        {
            get {return(_cConfigVersion);}
            set {_cConfigVersion = value;}
        }

        public Byte SetErrorTimeout 
        {
            get { return (_nSetErrorTimeout); }
            set { _nSetErrorTimeout = value; }
        }

        public Byte ClearErrorTimeout   
        {
            get { return (_nClearErrorTimeout); }
            set { _nClearErrorTimeout = value; }
        }

        public Byte SetEventTimeout 
        {
            get { return (_nSetEventTimeout); }
            set { _nSetEventTimeout = value; }
        }

        public Byte ClearEventTimeout   
        {
            get { return (_nClearEventTimeout); }
            set { _nClearEventTimeout = value; }
        }

        public URANGE BatteryVoltageRange   
        {
            get { return (_oBatteryVoltageRange); }
            set { _oBatteryVoltageRange = value; }
        }

        public URANGE InputVoltageRange
        {
            get { return (_oInputVoltageRange); }
            set { _oInputVoltageRange = value; }
        }

        public URANGE BatteryChargeCurrentRange
        {
            get { return (_oBatteryChargeCurrentRange ); }
            set { _oBatteryChargeCurrentRange = value; }
        }

        public URANGE BatteryDischargeCurrentRange
        {
            get { return (_oBatteryDischargeCurrentRange); }
            set { _oBatteryDischargeCurrentRange = value; }
        }

        public IRANGE TransformerTemperatureRange
        {
            get { return (_oTransformerTemperatureRange); }
            set { _oTransformerTemperatureRange = value; }
        }

        public IRANGE TransistorTemperatureRange
        {
            get { return (_oTransistorTemperatureRange); }
            set { _oTransistorTemperatureRange = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Load members from bytes stream
        /// </summary>
        /// <param name="btTmp">input bytes stream</param>
        public void LoadFromStream(byte[] btInput, int nLen)
        {
            try
            {
                int     nCount   = 0;
                int     nOffset  = 0;
                Byte [] btTmp    = new Byte[1024];

                // check input len
                //
                if (btInput == null || nLen != TOK_BYTES_BUFFER_LEN)
                    throw new Exception(TOK_ERROR_INPUT_BYTE_LEN);

                _cConfigVersion     = new char[TOK_CONFIG_VERSION_LENGTH];
               
                nOffset  = 0;
                for (nCount = 0; nCount < TOK_CONFIG_VERSION_LENGTH * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _cConfigVersion = Common.ConvertAlfa2CharArray(btTmp, TOK_CONFIG_VERSION_LENGTH);
                nOffset += TOK_CONFIG_VERSION_LENGTH * 2;

                for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nSetErrorTimeout = Common.ConvertAlfa2Byte(btTmp);
                nOffset += (sizeof(Byte) * 2);

                for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nClearErrorTimeout = Common.ConvertAlfa2Byte(btTmp);
                nOffset += (sizeof(Byte) * 2);

                for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nSetEventTimeout = Common.ConvertAlfa2Byte(btTmp);
                nOffset += (sizeof(Byte) * 2);

                for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nClearEventTimeout = Common.ConvertAlfa2Byte(btTmp);
                nOffset += (sizeof(Byte) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oBatteryVoltageRange.min = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oBatteryVoltageRange.MAX = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oInputVoltageRange.min = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oInputVoltageRange.MAX = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oBatteryChargeCurrentRange.min = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oBatteryChargeCurrentRange.MAX = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oBatteryDischargeCurrentRange.min = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oBatteryDischargeCurrentRange.MAX = Common.ConvertAlfa2UInt16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(Int16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oTransformerTemperatureRange.min = Common.ConvertAlfa2Int16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(Int16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oTransformerTemperatureRange.MAX = Common.ConvertAlfa2Int16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(Int16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oTransistorTemperatureRange.min = Common.ConvertAlfa2Int16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(Int16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _oTransistorTemperatureRange.MAX = Common.ConvertAlfa2Int16(btTmp);
                nOffset += (sizeof(UInt16) * 2);

            
                #if DEBUG
                    DumpConfig();
                #endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;            
            }
        }

        /// <summary>
        /// Process configuration and apply zone to test
        /// </summary>

        /// <summary>
        /// Serialize class to byte stream
        /// </summary>
        /// <returns></returns>
        public void Serialize2Stream()
        {
            try
            {
                int     nOffset     = 0;
                byte [] btSerialize = new Byte[256];
                byte [] btTmp;

                // clear buffer
                //
                for (int nIdx = 0; nIdx < 256; nIdx++)
                    btSerialize[nIdx] = 0;

                // config version
                //
                btTmp = Common.ConvertCharArray2Alfa(_cConfigVersion, _cConfigVersion.Length);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                
                // SetErrorTimeout
                //
                btTmp = Common.ConvertByte2Alfa(_nSetErrorTimeout);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // ClearErrorTimeout
                //
                btTmp = Common.ConvertByte2Alfa(_nClearErrorTimeout);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // SetEventTimeout
                //
                btTmp = Common.ConvertByte2Alfa(_nSetEventTimeout);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // ClearEventTimeout
                //
                btTmp = Common.ConvertByte2Alfa(_nClearEventTimeout);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // BatteryVoltageRange
                //
                //min
                btTmp = Common.ConvertUInt162Alfa(_oBatteryVoltageRange.min);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                //MAX
                btTmp = Common.ConvertUInt162Alfa(_oBatteryVoltageRange.MAX);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // InputVoltageRange
                //
                //min
                btTmp = Common.ConvertUInt162Alfa(_oInputVoltageRange.min);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                //MAX
                btTmp = Common.ConvertUInt162Alfa(_oInputVoltageRange.MAX);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // BatteryChargeCurrentRange
                //
                //min
                btTmp = Common.ConvertUInt162Alfa(_oBatteryChargeCurrentRange.min);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                //MAX
                btTmp = Common.ConvertUInt162Alfa(_oBatteryChargeCurrentRange.MAX);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;


                // BatteryDischargeCurrentRange
                //
                //min
                btTmp = Common.ConvertUInt162Alfa(_oBatteryDischargeCurrentRange.min);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                //MAX
                btTmp = Common.ConvertUInt162Alfa(_oBatteryDischargeCurrentRange.MAX);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // TransformerTemperature
                //
                //min
                btTmp = Common.ConvertInt162Alfa(_oTransformerTemperatureRange.min);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                //MAX
                btTmp = Common.ConvertInt162Alfa(_oTransformerTemperatureRange.MAX);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // TransistorTemperature
                //
                //min
                btTmp = Common.ConvertInt162Alfa(_oTransistorTemperatureRange.min);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                //MAX
                btTmp = Common.ConvertInt162Alfa(_oTransistorTemperatureRange.MAX);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                _btSerialize = new Byte[nOffset];
                
                for(int nIdx = 0; nIdx < nOffset; nIdx++)
                    _btSerialize[nIdx] = btSerialize[nIdx];

                #if DEBUG

                    Common.DumpLogMessage(" >>> Serialize Config Dump <<<");
                    Common.DumpLogMessage(TOK_BUFFER_LEN_STR + _btSerialize.Length.ToString());
                    Common.DumpLogMessage(TOK_BUFFER_STR     + Common.Byte2HexString(_btSerialize));

                #endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Dump config
        /// </summary>
        public void DumpConfig()
        {
            string sTmp = string.Empty;

            for (int nIdx = 0; nIdx < TOK_CONFIG_VERSION_LENGTH; nIdx++)
                sTmp += _cConfigVersion[nIdx];

            Common.DumpLogMessage("    >>> Ampli Config Dump <<<");
            Common.DumpLogMessage("                   ConfigVersion: " + sTmp);
            Common.DumpLogMessage("                 SetErrorTimeout: " + _nSetErrorTimeout.ToString("D"));
            Common.DumpLogMessage("               ClearErrorTimeout: " + _nClearErrorTimeout.ToString("D"));
            Common.DumpLogMessage("                 SetEventTimeout: " + _nSetEventTimeout.ToString("D"));
            Common.DumpLogMessage("               ClearEventTimeout: " + _nSetEventTimeout.ToString("D"));

            Common.DumpLogMessage("         BatteryVoltageRange.min: " + _oBatteryVoltageRange.min.ToString("D"));
            Common.DumpLogMessage("                            .MAX: " + _oBatteryVoltageRange.MAX.ToString("D"));

            Common.DumpLogMessage("           InputVoltageRange.min: " + _oInputVoltageRange.min.ToString("D"));
            Common.DumpLogMessage("                            .MAX: " + _oInputVoltageRange.MAX.ToString("D"));

            Common.DumpLogMessage("   BatteryChargeCurrentRange.min: " + _oBatteryChargeCurrentRange.min.ToString("D"));
            Common.DumpLogMessage("                            .MAX: " + _oBatteryChargeCurrentRange.MAX.ToString("D"));

            Common.DumpLogMessage("BatteryDischargeCurrentRange.min: " + _oBatteryDischargeCurrentRange.min.ToString("D"));
            Common.DumpLogMessage("                            .MAX: " + _oBatteryDischargeCurrentRange.MAX.ToString("D"));

            Common.DumpLogMessage(" TransformerTemperatureRange.min: " + _oTransformerTemperatureRange.min.ToString("D"));
            Common.DumpLogMessage("                            .MAX: " + _oTransformerTemperatureRange.MAX.ToString("D"));

            Common.DumpLogMessage("  TransistorTemperatureRange.min: " + _oTransistorTemperatureRange.min.ToString("D"));
            Common.DumpLogMessage("                            .MAX: " + _oTransistorTemperatureRange.MAX.ToString("D"));

        }

        /// <summary>
        /// Save configuration to file
        /// </summary>
        /// <param name="sFile">file name</param>
        public void Save2File(string sFile)
        {
            try
            {
                string sTmp = string.Empty;

                // dump configuration to file
                //
                using (TextWriter oWriter = new StreamWriter(sFile))
                {
                    oWriter.WriteLine("[Telefin DT04000 Config File]");
                    for (int nIdx = 0; nIdx < TOK_CONFIG_VERSION_LENGTH; nIdx++)
                        sTmp += _cConfigVersion[nIdx];
                    oWriter.WriteLine("Version="                + sTmp);
                    oWriter.WriteLine("");

                    oWriter.WriteLine("[Timeouts]");
                    sTmp = ((int)(_nSetErrorTimeout)).ToString("000");      oWriter.WriteLine("SetErrorTimeout=" + sTmp);
                    sTmp = ((int)(_nClearErrorTimeout)).ToString("000");    oWriter.WriteLine("ClearErrorTimeout=" + sTmp);
                    sTmp = ((int)(_nSetEventTimeout)).ToString("000");      oWriter.WriteLine("SetEventTimeout=" + sTmp);
                    sTmp = ((int)(_nClearEventTimeout)).ToString("000");    oWriter.WriteLine("ClearEventTimeout=" + sTmp);

                    oWriter.WriteLine("[Range]");
                    sTmp = ((int)(_oBatteryVoltageRange.min)).ToString("00000");    oWriter.WriteLine("BatteryVoltageRange_min=" + sTmp);
                    sTmp = ((int)(_oBatteryVoltageRange.MAX)).ToString("00000");    oWriter.WriteLine("BatteryVoltageRange_MAX=" + sTmp);

                    sTmp = ((int)(_oInputVoltageRange.min)).ToString("00000");      oWriter.WriteLine("InputVoltageRange_min=" + sTmp);
                    sTmp = ((int)(_oInputVoltageRange.MAX)).ToString("00000");      oWriter.WriteLine("InputVoltageRange_MAX=" + sTmp);

                    sTmp = ((int)(_oBatteryChargeCurrentRange.min)).ToString("00000"); oWriter.WriteLine("BatteryChargeCurrentRange_min=" + sTmp);
                    sTmp = ((int)(_oBatteryChargeCurrentRange.MAX)).ToString("00000"); oWriter.WriteLine("BatteryChargeCurrentRange_MAX=" + sTmp);

                    sTmp = ((int)(_oBatteryDischargeCurrentRange.min)).ToString("00000"); oWriter.WriteLine("BatteryDischargeCurrentRange_min=" + sTmp);
                    sTmp = ((int)(_oBatteryDischargeCurrentRange.MAX)).ToString("00000"); oWriter.WriteLine("BatteryDischargeCurrentRange_MAX=" + sTmp);

                    sTmp = ((int)(_oTransformerTemperatureRange.min)).ToString("00000"); oWriter.WriteLine("TransformerTemperatureRange_min=" + sTmp);
                    sTmp = ((int)(_oTransformerTemperatureRange.MAX)).ToString("00000"); oWriter.WriteLine("TransformerTemperatureRange_MAX=" + sTmp);

                    sTmp = ((int)(_oTransistorTemperatureRange.min)).ToString("00000"); oWriter.WriteLine("TransistorTemperatureRange_min=" + sTmp);
                    sTmp = ((int)(_oTransistorTemperatureRange.MAX)).ToString("00000"); oWriter.WriteLine("TransistorTemperatureRange_MAX=" + sTmp);
                }

            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Load configuration from file
        /// </summary>
        /// <param name="sFile">file name</param>
        public void LoadFromfile(string sFile)
        {
            try
            {
                string      sTmp = string.Empty;

                _cConfigVersion     = new char[TOK_CONFIG_VERSION_LENGTH];
                sTmp = Common.ReadKeyIniValue("Telefin DT04000 Config File", "Version", sFile);
                sTmp.CopyTo(0, _cConfigVersion, 0, TOK_CONFIG_VERSION_LENGTH);

//                for (int nCount = 0; nCount < TOK_CONFIG_VERSION_LENGTH; nCount++)
//                    _cConfigVersion[nCount] = sTmp[nCount];

                sTmp = Common.ReadKeyIniValue("Timeouts", "SetErrorTimeout", sFile);    _nSetErrorTimeout = Convert.ToByte(sTmp);
                sTmp = Common.ReadKeyIniValue("Timeouts", "ClearErrorTimeout", sFile);  _nClearErrorTimeout = Convert.ToByte(sTmp);
                sTmp = Common.ReadKeyIniValue("Timeouts", "SetEventTimeout", sFile);    _nSetEventTimeout = Convert.ToByte(sTmp);
                sTmp = Common.ReadKeyIniValue("Timeouts", "ClearEventTimeout", sFile);  _nClearEventTimeout = Convert.ToByte(sTmp);

                sTmp = Common.ReadKeyIniValue("Range", "BatteryVoltageRange_min", sFile); _oBatteryVoltageRange.min = Convert.ToUInt16(sTmp);
                sTmp = Common.ReadKeyIniValue("Range", "BatteryVoltageRange_MAX", sFile); _oBatteryVoltageRange.MAX = Convert.ToUInt16(sTmp);

                sTmp = Common.ReadKeyIniValue("Range", "InputVoltageRange_min", sFile); _oInputVoltageRange.min = Convert.ToUInt16(sTmp);
                sTmp = Common.ReadKeyIniValue("Range", "InputVoltageRange_MAX", sFile); _oInputVoltageRange.MAX = Convert.ToUInt16(sTmp);

                sTmp = Common.ReadKeyIniValue("Range", "BatteryChargeCurrentRange_min", sFile); _oBatteryChargeCurrentRange.min = Convert.ToUInt16(sTmp);
                sTmp = Common.ReadKeyIniValue("Range", "BatteryChargeCurrentRange_MAX", sFile); _oBatteryChargeCurrentRange.MAX = Convert.ToUInt16(sTmp);

                sTmp = Common.ReadKeyIniValue("Range", "BatteryDischargeCurrentRange_min", sFile); _oBatteryDischargeCurrentRange.min = Convert.ToUInt16(sTmp);
                sTmp = Common.ReadKeyIniValue("Range", "BatteryDischargeCurrentRange_MAX", sFile); _oBatteryDischargeCurrentRange.MAX = Convert.ToUInt16(sTmp);

                sTmp = Common.ReadKeyIniValue("Range", "TransformerTemperatureRange_min", sFile); _oTransformerTemperatureRange.min = Convert.ToInt16(sTmp);
                sTmp = Common.ReadKeyIniValue("Range", "TransformerTemperatureRange_MAX", sFile); _oTransformerTemperatureRange.MAX = Convert.ToInt16(sTmp);

                sTmp = Common.ReadKeyIniValue("Range", "TransistorTemperatureRange_min", sFile); _oTransistorTemperatureRange.min = Convert.ToInt16(sTmp);
                sTmp = Common.ReadKeyIniValue("Range", "TransistorTemperatureRange_MAX", sFile); _oTransistorTemperatureRange.MAX = Convert.ToInt16(sTmp);

            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }
        #endregion

        #region Private Members

        #endregion

        internal void ApplyDefault()
        {
            _nSetErrorTimeout   = 1; // (1 sec.)Durata minima dell'errore affinch� si attivi la segnalazione 
            _nClearErrorTimeout = 1; // (1 sec.)Durata minima dell'assenza di errore affinch� si disattivi la segnalazione 
            _nSetEventTimeout   = 1; // (1 sec.)Durata minima dell'evento affinch� si attivi la segnalazione 
            _nClearEventTimeout = 1; // (1 sec.)Durata minima dell'assenza di eevento affinch� si disattivi la segnalazione

            _oBatteryVoltageRange.min   = 24100; // (24100 mV) 
            _oBatteryVoltageRange.MAX   = 27800; // (27800 mV)

            _oInputVoltageRange.min     = 35000; // (35000 mV)
            _oInputVoltageRange.MAX     = 45000; // (45000 mV)

            _oBatteryChargeCurrentRange.min     = 0;    // (   0 mA)
            _oBatteryChargeCurrentRange.MAX     = 2000; // (2000 mA)

            _oBatteryDischargeCurrentRange.min  = 0;    // ( 0 �C)
            _oBatteryDischargeCurrentRange.MAX  = 200;  // (20 �C = 200 * 1/10 �C)

            _oTransformerTemperatureRange.min   = 0;    // ( 0 �C)
            _oTransformerTemperatureRange.MAX   = 650;  // (65 �C = 650 * 1/10 �C)

            _oTransistorTemperatureRange.min    = 0;    // ( 0 �C)
            _oTransistorTemperatureRange.MAX    = 650;  // (65 �C = 650 * 1/10 �C)

        }
    }
}

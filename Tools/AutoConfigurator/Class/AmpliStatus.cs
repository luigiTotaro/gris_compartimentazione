
//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		AmpliStatus.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		17.05.2007
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;


namespace Telefin.AutoConfigurator.Class
{
    public class AmpliStatus
    {
        #region Token

        private const int       TOK_BYTES_BUFFER_LEN        = 36;

        private const string    TOK_ERROR_INPUT_BYTE_LEN    = @"Lunghezza buffer di ingresso errata";

        #endregion

        #region Private Members

        private UInt16      _nStatusFlags;           // Flags di stato unsigned 
        private UInt16      _nMeasures;              // Flags stato misure 
        private UInt16      _nLinePower;             // Tensione di linea
        private UInt16      _nBatteryVoltage;
        private UInt16      _nInputVoltage;
        private UInt16      _nBatteryChargeCurrent;
        private UInt16      _nBatteryDischargeCurrent;
        private Int16       _nTransformerTemperature;
        private Int16       _nTransistorTemperature;
        bool                _bStatusAvailable;

        #endregion

        #region Accessor

	
        public UInt16 StatusFlags
        {
            get{return(_nStatusFlags);}
            set{_nStatusFlags = value;}
        }

        public UInt16 Measures
        {
            get {return (_nMeasures);}
            set {_nMeasures = value; }
        }

        public UInt16 LinePower         
        {
            get {return (_nLinePower);}
            set {_nLinePower = value;}
        }

        public UInt16 BatteryVoltage
        {
            get {return (_nBatteryVoltage);}
            set {_nBatteryVoltage = value;}
        }

        public UInt16 InputVoltage
        {
            get {return (_nInputVoltage);}
            set {_nInputVoltage = value;}
        }

        public UInt16 BatteryChargeCurrent
        {
            get {return (_nBatteryChargeCurrent);}
            set {_nBatteryChargeCurrent = value;}
        }

        public UInt16 BatteryDischargeCurrent
        {
            get {return (_nBatteryDischargeCurrent);}
            set {_nBatteryDischargeCurrent = value;}
        }

        public Int16 TransformerTemperature
        {
            get { return (_nTransformerTemperature); }
            set { _nTransformerTemperature = value; }
        }

        public Int16 TransistorTemperature
        {
            get { return (_nTransistorTemperature); }
            set { _nTransistorTemperature = value; }
        }

        public bool StatusAvailable
        {
            get { return (_bStatusAvailable); }
            set { _bStatusAvailable = value; }
        }
       

        #endregion

        #region Public Methods

        /// <summary>
        /// Load members from bytes stream
        /// </summary>
        /// <param name="btTmp">input bytes stream</param>
        public void LoadFromStream(byte[] btInput, int nLen)
        {
            try
            {
                int     nCount   = 0;
                int     nOffset  = 0;
                Byte [] btTmp    = new Byte[8];

                // check input len
                //
                if (btInput == null || nLen != TOK_BYTES_BUFFER_LEN)
                    throw new Exception(TOK_ERROR_INPUT_BYTE_LEN);


                nOffset  = 0;
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nStatusFlags = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nMeasures = Common.ConvertAlfa2UInt16(btTmp);                

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nLinePower = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nBatteryVoltage = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nInputVoltage = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nBatteryChargeCurrent = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nBatteryDischargeCurrent = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for (nCount = 0; nCount < sizeof(Int16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTransformerTemperature = Common.ConvertAlfa2Int16(btTmp);

                nOffset += (sizeof(Int16) * 2);
                for (nCount = 0; nCount < sizeof(Int16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTransistorTemperature = Common.ConvertAlfa2Int16(btTmp);

                _bStatusAvailable = true;


                #if DEBUG
//                    DumpStatus();
                #endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                
                StatusAvailable = false;

                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;            
            }
        }

        /// <summary>
        /// Dump status
        /// </summary>
        public void DumpStatus()
        {
            if (StatusAvailable)
            {
                Common.DumpLogMessage(" >>> Status Dump <<<");
                Common.DumpLogMessage("            StatusFlags: 0x" + _nStatusFlags.ToString("X4"));
                Common.DumpLogMessage("           MeasuresFlag: 0x" + _nMeasures.ToString("X4"));
                Common.DumpLogMessage("              LinePower: " + (_nLinePower / 1000.0).ToString("F") + " Volt");
                Common.DumpLogMessage("         BatteryVoltage: " + (_nBatteryVoltage / 1000.0).ToString("F") + " Volt");
                Common.DumpLogMessage("           InputVoltage: " + (_nInputVoltage / 1000.0).ToString("F") + " Volt");
                Common.DumpLogMessage("   BatteryChargeCurrent: " + _nBatteryChargeCurrent.ToString("D") + " mA");
                Common.DumpLogMessage("BatteryDischargeCurrent: " + _nBatteryDischargeCurrent.ToString("D") + " mA");
                Common.DumpLogMessage(" TransformerTemperature: " + (_nTransformerTemperature / 10.0).ToString("F") + " �C");
                Common.DumpLogMessage("  TransistorTemperature: " + (_nTransistorTemperature / 10.0).ToString("F") + " �C");
            }
            else
                Common.DumpLogMessage(" >>> Status Unavailable <<<");
        }

        #endregion

        #region Private Members

        #endregion
    }
}


//
// Telefin
//
// Progetto:	PZAutoConfigurator
// File:		Manager.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		31.05.2007
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Microsoft.Win32;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.ServiceProcess;

namespace Telefin.AutoConfigurator.Class
{
    class PzManager:ManagerBase
    {
        #region Token


        private const string    TOK_PZ_TYPPE                        = @"TT10210";
        private const string    TOK_XPATH_DEVICES_QUERY             = "./telefin/system/server/region/zone/node/device[@type=\"TT10210\"]/.";
        private const string    TOK_XPATH_PORT_QUERY                = "./telefin/port/item[@id=\"@PORT_NUMBER\"]/.";

        private const string    TOK_MSG_ITEM_NAME                   = @"Lettura configurazione Pannello Zone: ";
        private const string    TOK_MSG_INTERNAL_TESTS              = @"Test interni ";
        private const string    TOK_MSG_AUTO_TESTS                  = @"Auto test ";
        private const string    TOK_MSG_WRITE_CONFIG                = @"Scrittura configurazione ";
        private const string    TOK_MSG_WRITE_END_CONFIG            = @"Scrittura configurazione finale";
        private const string    TOK_MSG_START_SAVE_CONFIG           = @"Salvataggio configurazione dispositivo zone su file...";
        private const string    TOK_MSG_END_SAVE_CONFIG             = @"Salvataggio configurazione concluso";
        private const string    TOK_MSG_START_WRITE_CONFIG          = @"Scrittura configurazione dispositivo zone da file...";
        private const string    TOK_MSG_END_WRITE_CONFIG            = @"Scrittura configurazione concluso";

        private const string    TOK_CMD_ASK_SW_VERSION              = @"N";
        private const string    TOK_CMD_ASK_STATUS                  = @"D";
        private const string    TOK_CMD_ASK_CONFIG                  = @"A";
        private const string    TOK_CMD_WRITE_CONFIG                = @"R";
        private const string    TOK_CMD_SEND_AUTOSETUP              = @"S";
        private const string    TOK_CMD_START_TESTS                 = @"T";
        private const string    TOK_CMD_WRITE_EEPROM_CONFIG         = @"H";
        private const string    TOK_CMD_RESET_FLAG_CONFIG           = @"J";

        private const int       TOK_TIMEOUT_AUTO_TESTS_MINUTE       = 10;           
        private const int       TOK_TIMEOUT_AUTO_SETUP_MINUTE       = 10;           
        private const int       TOK_MS_WAIT                         = 2000;
        private const UInt16    TOK_ID_AUTOSETUP_RUNNING            = 0x0040;
        private const UInt16    TOK_ID_TEST_RUNNING                 = 0x0080;
        private const UInt16    TOK_ID_TEST_SUSPEND                 = 0x0100;



        private const string    TOK_ERROR_READ_SW_VERSION           = @"*** Errore in lettura versione software ***";
        private const string    TOK_ERROR_READ_STATUS               = @"*** Errore in lettura stato dispositivo ***";
        private const string    TOK_ERROR_READ_CONFIGURATION        = @"*** Errore in lettura configurazione dispositivo ***";
        private const string    TOK_ERROR_WRITE_AUTO_SETUP          = @"*** Errore in scrittura auto setup dispositivo ***";
        private const string    TOK_ERROR_TIMEOUT_CHECK_STATUS      = @"*** Errore di Timout su controllo stato dispositivo ***";
        private const string    TOK_ERROR_WRITE_START_TESTS         = @"*** Errore in scrittura start tests dispositivo ***";



        #endregion

        #region Enums

        private enum ManageState : int
        {
            None,
            Start,
            OpenComPort,
            CloseComPort,
            ReadSoftwareVersion,
            ReadStatus,
            ReadConfiguration,
            ProcessConfiguration,
            WriteConfiguration,
            SendAutoSetup,
            StartInternalTest,
            EndInternalTest,
            WriteEEPromConfiguration,
            ResetFlagConfigurazione,
            End,
            ErrorStatus
        }

        #endregion

        #region Private Members

        private List<PZItem>    _olPZItems          = null;
        private ManageState     _nState             = ManageState.None;
        private PZStatus        _oPZStatus          = null;
        private PZConfig        _oPZConfig          = null;

        #endregion

        #region Accessor
        #endregion

        #region Public Methods

        /// <summary>
        /// Load xml config file
        /// </summary>
        /// <returns>true = Ok false = error</returns>
        public override bool LoadConfig()
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_LOAD_CONFIG);

                RegistryKey oRegKey      = Registry.LocalMachine.OpenSubKey(TOK_KEY_REG, false);
                string      sXmlFileName = oRegKey != null ? oRegKey.GetValue(TOK_KEY_VALUE).ToString() : TOK_DEFAULT_XML;

                if (File.Exists(sXmlFileName) == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_XML_NOT_EXIST);
                    return(false);
                }

                XmlDocument oDoc = new XmlDocument();

                oDoc.Load(sXmlFileName);

                XmlNodeList oNodeList = oDoc.SelectNodes(TOK_XPATH_DEVICES_QUERY);

                if (oNodeList == null || oNodeList.Count == 0)
                {
                    Common.DumpLogMessage(TOK_ERROR_XML_DEVICES_NOT_PRESENT);
                    return (false);
                }

                XmlElement  oPortEle    = null;   
                XmlElement  oEle        = null;
                PZItem      oPZItem     = null;
                _olPZItems = new List<PZItem>();
                
                foreach (XmlNode oNode in oNodeList)
                {
                    oPZItem = new PZItem();

                    oEle = (XmlElement)oNode;

                    oPZItem.PZName       = oEle.Attributes["name"].Value.ToString();
                    oPZItem.SerialNumber = oEle.Attributes["SN"].Value.ToString();
                    oPZItem.IDSerialPort = XmlConvert.ToInt32(oEle.Attributes["port"].Value);
                    oPZItem.Address      = oEle.Attributes["addr"].Value.ToString();
                    oPZItem.NodeName     = oEle.ParentNode.Attributes["name"].Value.ToString();

                    oPortEle = (XmlElement)oDoc.SelectSingleNode(TOK_XPATH_PORT_QUERY.Replace("@PORT_NUMBER", oPZItem.IDSerialPort.ToString()));
                    if(oPortEle == null)
                    {
                        Common.DumpLogMessage(TOK_ERROR_XML_PORT_NOT_PRESENT.Replace("@PORT_NUMBER", oPZItem.IDSerialPort.ToString()));
                        return (false);
                    }

                    oPZItem.IDPort      = XmlConvert.ToInt32(oPortEle.Attributes["id"].Value);
                    oPZItem.PortName    = oPortEle.Attributes["name"].Value;
                    oPZItem.PortType    = oPortEle.Attributes["type"].Value;
                    oPZItem.ComID       = XmlConvert.ToInt32(oPortEle.Attributes["com"].Value);
                    oPZItem.ComBaud     = XmlConvert.ToInt32(oPortEle.Attributes["baud"].Value);
                    oPZItem.ComEcho     = XmlConvert.ToBoolean(oPortEle.Attributes["echo"].Value);
                    oPZItem.ComDataLen  = XmlConvert.ToInt32(oPortEle.Attributes["data"].Value);
                    oPZItem.ComDataStop = XmlConvert.ToInt32(oPortEle.Attributes["stop"].Value);
                    oPZItem.ComParity   = oPortEle.Attributes["parity"].Value;
                    oPZItem.ComTimeout  = XmlConvert.ToInt32(oPortEle.Attributes["timeout"].Value);

                    _olPZItems.Add(oPZItem);

                    StringBuilder oBuilder = new StringBuilder();
                    oBuilder.Append(TOK_MSG_ITEM_NAME);
                    oBuilder.Append(oPZItem.PZName);
                    oBuilder.Append(TOK_MSG_NODE_NAME);
                    oBuilder.Append(oPZItem.NodeName);

                    Common.DumpLogMessage(oBuilder.ToString());
                }

                return(true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());
                
                return(false);
            }
        }

        /// <summary>
        /// Main function
        /// </summary>
        public override void DoAutoConfig()
        {
            try
            {
                bool            bRun;
                StringBuilder   oBuilder;
                bool            bRet    = false;
                bool            bFirst  = false;

                _nState = ManageState.None;

                foreach (PZItem oPZItem in _olPZItems)
                {
                    bRun    = true;
                    bFirst  = true;
                    _nState = ManageState.None;

                    do
                    {
                        switch (_nState)
                        {
                            // init state
                            // 
                            case ManageState.None:
                                
                                oBuilder = new StringBuilder();
                                oBuilder.Append(TOK_MSG_START_AUTOCONF);
                                oBuilder.Append(oPZItem.PZName);
                                oBuilder.Append(TOK_MSG_NODE_NAME);
                                oBuilder.Append(oPZItem.NodeName);
                                Common.DumpLogMessage(oBuilder.ToString());

                                oPZItem.SerialWrap = new SerialWrapper();
                                oPZItem.SerialWrap.InitComPort(oPZItem.ComID, 
                                                               oPZItem.ComBaud,
                                                               oPZItem.ComParity, 
                                                               oPZItem.ComDataLen, 
                                                               oPZItem.ComDataStop, 
                                                               oPZItem.ComTimeout,
                                                               oPZItem.PortType == TOK_RS232_TYPE,
                                                               oPZItem.ComEcho);                               
                                _nState = ManageState.Start;
                               
                                break;

                            // start
                            //
							case ManageState.Start:
								{
									// Cristian 25-03-2008
									if ( !this.StopSTLCServices() )
									{
										Common.DumpLogMessage(TOK_ERROR_SERVICE_STOP_TEST);
										// return;
									}

									_nState = ManageState.OpenComPort;

									break;
								}
                            // open com port
                            //
                            case ManageState.OpenComPort:

                                oPZItem.SerialWrap.OpenComPort();

                                _nState = ManageState.ReadSoftwareVersion;

                                break;

                            // Read software version
                            //
                            case ManageState.ReadSoftwareVersion:

                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_ASK_SW_VERSION, oPZItem.Address))
                                {
                                    bool bIsEnd = false;
                                    if (oPZItem.SerialWrap.ReadCommandBuffer(oPZItem.Address, ref bIsEnd))
                                    {
                                        bRet = true;
                                        Common.DumpLogMessage(TOK_MSG_SW_VERSION + Common.Byte2String(oPZItem.SerialWrap.ReadBuffer, oPZItem.SerialWrap.ReadBufferLen));
                                    }
                                }

                                oPZItem.SerialWrap.CloseSentCommand();

                                if(bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.ReadStatus;

                                break;

                            // Read Status
                            //
                            case ManageState.ReadStatus:

                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oPZItem.Address))
                                {
                                    bool bIsEnd = false;

                                    do
                                    {
                                        if (oPZItem.SerialWrap.ReadCommandBuffer(oPZItem.Address, ref bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oPZItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_READ_STATUS);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                {
                                    _oPZStatus = new PZStatus();
                                    _oPZStatus.LoadFromStream(oPZItem.SerialWrap.ReadBuffer, oPZItem.SerialWrap.ReadBufferLen);
                                    _nState = ManageState.ReadConfiguration;
                                }

                                break;

                            // Read PZ config
                            //
                            case ManageState.ReadConfiguration:
                                
                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_ASK_CONFIG, oPZItem.Address))
                                {
                                    bool bIsEnd = false;

                                    do
                                    {
                                        if (oPZItem.SerialWrap.ReadCommandBuffer(oPZItem.Address, ref bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oPZItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_READ_CONFIGURATION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                {
                                    _oPZConfig = new PZConfig();
                                    _oPZConfig.LoadFromStream(oPZItem.SerialWrap.ReadBuffer, oPZItem.SerialWrap.ReadBufferLen);
                                    _nState = ManageState.ProcessConfiguration;
                                }

                                break;

                            // process configuration
                            //
                            case ManageState.ProcessConfiguration:

                                _oPZConfig.ApplyZone2Test();                                
                                _nState = ManageState.WriteConfiguration;
                                
                                break;

                            // write configuration
                            //
                            case ManageState.WriteConfiguration:

                                Common.DumpLogMessage(TOK_MSG_WRITE_CONFIG);

                                // create strem from object
                                //
                                _oPZConfig.Serialize2Stream();

                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oPZItem.Address))
                                {
                                    bool bIsEnd         = false;
                                    Byte [] btBlock     = null;
                                    int  nBlock         = 0;                                    
                                    int  nLen           = 0;
                                    int  nOffset        = 0;
                                    Byte [] btSerialize = _oPZConfig.SerializeBuffer;

                                    do
                                    {
                                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                                        nLen    = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                                        btBlock = new Byte[nLen];

                                        for(int nIdx = 0; nIdx < nLen; nIdx++)
                                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                                        bIsEnd = nLen == TOK_MAX_BLOCK_SIZE ? false : true;

                                        if (oPZItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oPZItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.SendAutoSetup;

                                break;

                            // send auto setup
                            //
                            case ManageState.SendAutoSetup:

                                Common.DumpLogMessage(TOK_MSG_AUTO_TESTS);

                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_SEND_AUTOSETUP, oPZItem.Address))
                                {
                                    oPZItem.SerialWrap.CloseSentCommand();
                                    
                                    // must wait to start autosetup
                                    //
                                    Thread.Sleep(300);

                                    UInt16 [] nFlags = new UInt16[3];

                                    nFlags[0] = TOK_ID_AUTOSETUP_RUNNING;
                                    nFlags[1] = TOK_ID_TEST_RUNNING;
                                    nFlags[2] = TOK_ID_TEST_SUSPEND;

                                    bRet = WaitStatusFlag(oPZItem, nFlags, TOK_TIMEOUT_AUTO_SETUP_MINUTE);
                                }
                                else
                                    oPZItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_WRITE_AUTO_SETUP);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.StartInternalTest;

                                break;

                            // send start internal test
                            //
                            case ManageState.StartInternalTest:

                                Common.DumpLogMessage(TOK_MSG_INTERNAL_TESTS);

                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_START_TESTS, oPZItem.Address))
                                {
                                    oPZItem.SerialWrap.CloseSentCommand();
                                    
                                    // must wait to start autosetup
                                    //
                                    Thread.Sleep(300);

                                    UInt16 [] nFlags = new UInt16[3];

                                    nFlags[0] = TOK_ID_AUTOSETUP_RUNNING;
                                    nFlags[1] = TOK_ID_TEST_RUNNING;
                                    nFlags[2] = TOK_ID_TEST_SUSPEND;

                                    bRet = WaitStatusFlag(oPZItem, nFlags, TOK_TIMEOUT_AUTO_SETUP_MINUTE);
                                }
                                else
                                    oPZItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_WRITE_START_TESTS);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.EndInternalTest;

                                break;

                            // end internal test
                            //
                            case ManageState.EndInternalTest:

                                if ((_oPZStatus.ZonesTestFail & _oPZConfig.ZonesToTest) == 0 && bFirst == false)
                                    _nState = ManageState.WriteEEPromConfiguration;
                                else
                                {
                                    // flag to force the first test
                                    //
                                    bFirst = false;

                                    // read zone tested
                                    //
                                    _oPZConfig.ZonesToTest = 0;
                                    for (int nIdx = 0; nIdx < _oPZStatus.ImpedenceValue.Length; nIdx++)
                                    {
                                        if (_oPZStatus.ImpedenceValue[nIdx] != 0 && _oPZStatus.ImpedenceValue[nIdx] != UInt32.MaxValue)
                                        {
                                            _oPZConfig.ZonesToTest          |= (UInt16) Math.Pow(2, nIdx);
                                            _oPZConfig.ResistanceRange[nIdx] = (UInt16) (0.15 * _oPZStatus.ImpedenceValue[nIdx]);                                            
                                        }
                                    }

                                    for (int nIdx = 0; nIdx < _oPZConfig.TensionsRange.Length; nIdx++)
                                        _oPZConfig.TensionsRange[nIdx] = (UInt16)(0.2 * _oPZStatus.TestTensionValue[nIdx]);

                                    #if DEBUG
                                        _oPZConfig.DumpConfig();
                                    #endif

                                    _nState = ManageState.WriteConfiguration;                                    
                                }

                                break;

                            // write configuration to EEProm
                            //
                            case ManageState.WriteEEPromConfiguration:

                                bRet = false;
                                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oPZItem.Address))
                                {
                                    bool bIsEnd         = false;
                                    Byte [] btBlock     = null;
                                    int  nBlock         = 0;                                    
                                    int  nLen           = 0;
                                    int  nOffset        = 0;
                                    Byte [] btSerialize = _oPZConfig.SerializeBuffer;

                                    do
                                    {
                                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                                        nLen    = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                                        btBlock = new Byte[nLen];

                                        for(int nIdx = 0; nIdx < nLen; nIdx++)
                                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                                        bIsEnd = nLen == TOK_MAX_BLOCK_SIZE ? false : true;

                                        if (oPZItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oPZItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.ResetFlagConfigurazione;

                                break;

                            // reset flag configuration changed
                            //
							case ManageState.ResetFlagConfigurazione:
								{
									bRet = oPZItem.SerialWrap.SendCommand(TOK_CMD_RESET_FLAG_CONFIG, oPZItem.Address);
									oPZItem.SerialWrap.CloseSentCommand();

									if ( bRet == false )
									{
										Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
										_nState = ManageState.ErrorStatus;
									}
									else
										_nState = ManageState.CloseComPort;

									Common.DumpLogMessage(TOK_MSG_WRITE_END_CONFIG);

									break;
								}
							case ManageState.CloseComPort:
								{
									oPZItem.SerialWrap.Close();

									_nState = ManageState.End;

									break;
								}
							case ManageState.End:
								{
									_oPZConfig.Save2File(string.Format("PZConfig_COM{0}_ADDRESS{1}.txt", oPZItem.ComID, oPZItem.Address));
									oBuilder = new StringBuilder();
									oBuilder.Append(TOK_MSG_END_AUTOCONF);
									oBuilder.Append(oPZItem.PZName);
									oBuilder.Append(TOK_MSG_NODE_NAME);
									oBuilder.Append(oPZItem.NodeName);
									Common.DumpLogMessage(oBuilder.ToString());
									bRun = false;
									_nState = ManageState.None;

									// Cristian 25-03-2008
									if ( !this.StartSTLCServices() )
									{
										Common.DumpLogMessage(TOK_ERROR_SERVICE_START_TEST);
									}

									break;
								}
                            // error status
                            //
							case ManageState.ErrorStatus:
								{
									_nState = ManageState.CloseComPort;
									break;
								}
                        }

                    } while (bRun == true);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Wait status flag
        /// </summary>
        /// <param name="nFlag">flag to check</param>
        /// <param name="nTimeOut">wait time out</param>
        /// <returns>bool </returns>
        private bool WaitStatusFlag(PZItem oPZItem, UInt16 [] nFlags, int nTimeOut)
        {
            try
            {
                bool        bWait       = true;
                DateTime    dtStart     = DateTime.Now;
                TimeSpan    tsDiff;
                bool        bError      = false;
                bool        bCheckFlag  = false;

                bError = false;

                do
                {
                    // send ask status command
                    //
                    if (oPZItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oPZItem.Address))
                    {
                        bool bIsEnd = false;

                        do
                        {
                            // read command buffer
                            //
                            if (oPZItem.SerialWrap.ReadCommandBuffer(oPZItem.Address, ref bIsEnd) == false)
                            {
                                bError = true;
                                break;
                            }

                        } while (bIsEnd == false);
                    }
                    else
                        bError = true;

                    oPZItem.SerialWrap.CloseSentCommand();

                    if (bError)
                        return (false);

                    if (_oPZStatus == null)
                        _oPZStatus = new PZStatus();

                    // load object from stream
                    //
                    _oPZStatus.LoadFromStream(oPZItem.SerialWrap.ReadBuffer, oPZItem.SerialWrap.ReadBufferLen);

                    // check flag status
                    //
                    bCheckFlag = false;
                    for(int nIdx = 0; nIdx < nFlags.Length; nIdx++)
                    {
                        if ((_oPZStatus.StatusFlags & nFlags[nIdx]) == nFlags[nIdx])
                        {
                            bCheckFlag = true;
                            break;
                        }
                    }

                    if (bCheckFlag)
                    {
                        Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);

                        System.Threading.Thread.Sleep(TOK_MS_WAIT);

                        tsDiff = DateTime.Now - dtStart;

                        if (tsDiff.Minutes >= nTimeOut)
                        {
                            Common.DumpLogMessage(TOK_ERROR_TIMEOUT_CHECK_STATUS);
                            bError = true;
                            return (false);
                        }
                    }
                    else
                    {
                        Common.DumpLogNewLine();
                        bWait = false;
                    }

                } while (bWait == true);

                return(true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }


        /// <summary>
        /// dump device config to file
        /// </summary>
        /// <param name="sAddress">Address device</param>
        /// <param name="sFile">Output file</param>
        /// <param name="sComSettings">Com settings</param>
        public override void DoReadConfig(string sAddress, string sFile, string sComSettings)
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_START_SAVE_CONFIG);

                string [] sComArg =  sComSettings.Split(',');

                PZItem      oPZItem     = new PZItem();
                PZConfig    oPZConfig   = new PZConfig();

                oPZItem.Address    = sAddress;
                oPZItem.SerialWrap = new SerialWrapper();
                oPZItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                               Convert.ToInt32(sComArg[1]),
                                               sComArg[2],
                                               Convert.ToInt32(sComArg[3]),
                                               Convert.ToInt32(sComArg[4]) - 1,
                                               1000,
                                               sComArg[5] == "T" ? true : false,
                                               sComArg[6] == "T" ? true : false);

                oPZItem.SerialWrap.OpenComPort();

                bool bRet = false;
                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_ASK_CONFIG, oPZItem.Address))
                {
                    bool bIsEnd = false;

                    do
                    {
                        if (oPZItem.SerialWrap.ReadCommandBuffer(oPZItem.Address, ref bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (bIsEnd == false);
                }

                oPZItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_CONFIGURATION);
                    oPZItem.SerialWrap.Close();
                    return;
                }
                else
                {
                    oPZConfig.LoadFromStream(oPZItem.SerialWrap.ReadBuffer, oPZItem.SerialWrap.ReadBufferLen);
                    oPZConfig.Save2File(sFile);
                }

                oPZItem.SerialWrap.Close();

                Common.DumpLogMessage(TOK_MSG_END_SAVE_CONFIG);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// write config from file to device
        /// </summary>
        /// <param name="sFile">Input file</param>
        /// <param name="sComSettings">Com settings</param>
        public override void DoWriteConfig(string sAddress, string sFile, string sComSettings)
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_START_WRITE_CONFIG);

                string [] sComArg =  sComSettings.Split(',');

                PZItem      oPZItem     = new PZItem();
                PZConfig    oPZConfig   = new PZConfig();

				if ( !File.Exists(sFile) ) sFile = Common.GetProgramPath() + Path.DirectorySeparatorChar + sFile;
                oPZConfig.LoadFromfile(sFile);
                oPZConfig.Serialize2Stream();

                oPZItem.Address = sAddress;
                oPZItem.SerialWrap = new SerialWrapper();
                oPZItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                               Convert.ToInt32(sComArg[1]),
                                               sComArg[2],
                                               Convert.ToInt32(sComArg[3]),
                                               Convert.ToInt32(sComArg[4]) - 1,
                                               1000,
                                               sComArg[5] == "T" ? true : false,
                                               sComArg[6] == "T" ? true : false);

                oPZItem.SerialWrap.OpenComPort();

                bool bRet = false;
                if (oPZItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oPZItem.Address))
                {
                    bool    bIsEnd      = false;
                    Byte[]  btBlock     = null;
                    int     nBlock      = 0;
                    int     nLen        = 0;
                    int     nOffset     = 0;
                    Byte[]  btSerialize = oPZConfig.SerializeBuffer;

                    do
                    {
                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                        nLen    = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                        btBlock = new Byte[nLen];

                        for (int nIdx = 0; nIdx < nLen; nIdx++)
                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                        bIsEnd = nLen == TOK_MAX_BLOCK_SIZE ? false : true;

                        if (oPZItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (bIsEnd == false);
                }

                oPZItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                    oPZItem.SerialWrap.Close();
                    return;
                }

                bRet = oPZItem.SerialWrap.SendCommand(TOK_CMD_RESET_FLAG_CONFIG, oPZItem.Address);
                oPZItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                    _nState = ManageState.ErrorStatus;
                }
                else
                    _nState = ManageState.CloseComPort;


                oPZItem.SerialWrap.Close();

                Common.DumpLogMessage(TOK_MSG_END_WRITE_CONFIG);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }


        #endregion

    }
}


//
// Telefin
//
// Progetto:	PZAutoConfigurator
// File:		PZStatus.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		17.05.2007
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;


namespace Telefin.AutoConfigurator.Class
{
    public class PZStatus
    {
        #region Token

        private const int       TOK_BYTES_BUFFER_LEN        = 144;
        private const int       TOK_NUMBER_OF_AMPLI         = 2;
        private const int       TOK_NUMBER_OF_ZONES         = 12;

        private const string    TOK_ERROR_INPUT_BYTE_LEN    = @"Lunghezza buffer di ingresso errata";

        #endregion

        #region Private Members

        private UInt16      _nStatusFlags;           // Flags di stato unsigned 
        private UInt16      _nZonesTested;           // Flags indicanti le zone testate 
        private UInt16      _nZonesTestFail;         // Flags indicanti le zone sulle quail il test � fallito 
        private UInt16      _nZonesTestWarning;
        private UInt16      _nZonesActive;
        private UInt16      _nInputsStatus;
        private UInt16      _nOutputsStatus;
        private Byte[]      _nMicroAmpli;             // NUMBER_OF_AMPLI=2 
        private UInt32[]    _nTestTensionValue;       // NUMBER_OF_AMPLI=2 
        private UInt32[]    _nImpedenceValue;         // NUMBER_OF_ZONES=12

        #endregion

        #region Accessor

        public UInt16 StatusFlags
        {
            get
            {
                return(_nStatusFlags);
            }
            set
            {
                _nStatusFlags = value;
            }
        }

        public UInt16 ZonesTested
        {
            get
            {
                return (_nZonesTested);
            }
            set
            {
                _nZonesTested = value;
            }
        }

        public UInt16 ZonesTestFail         
        {
            get
            {
                return (_nZonesTestFail);
            }
            set
            {
                _nZonesTestFail = value;
            }
        }
        
        public UInt16 ZonesTestWarning
        {
            get
            {
                return (_nZonesTestWarning);
            }
            set
            {
                _nZonesTestWarning = value;
            }
        }
        
        public UInt16 ZonesActive
        {
            get
            {
                return (_nZonesActive);
            }
            set
            {
                _nZonesActive = value;
            }
        }

        public UInt16 InputsStatus
        {
            get
            {
                return (_nInputsStatus);
            }
            set
            {
                _nInputsStatus = value;
            }
        }

        public UInt16 OutputsStatus
        {
            get
            {
                return (_nOutputsStatus);
            }
            set
            {
                _nOutputsStatus = value;
            }
        }

        public Byte[] MicroAmpli
        {
            get
            {
                return (_nMicroAmpli);
            }
            set
            {
                _nMicroAmpli = value;
            }
        }

        public UInt32[] TestTensionValue
        {
            get
            {
                return (_nTestTensionValue);
            }
            set
            {
                _nTestTensionValue = value;
            }
        }

        public UInt32[] ImpedenceValue
        {
            get
            {
                return (_nImpedenceValue);
            }
            set
            {
                _nImpedenceValue = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Load members from bytes stream
        /// </summary>
        /// <param name="btTmp">input bytes stream</param>
        public void LoadFromStream(byte[] btInput, int nLen)
        {
            try
            {
                int     nCount   = 0;
                int     nOffset  = 0;
                Byte [] btTmp    = new Byte[8];

                // check input len
                //
                if (btInput == null || nLen != TOK_BYTES_BUFFER_LEN)
                    throw new Exception(TOK_ERROR_INPUT_BYTE_LEN);

                _nMicroAmpli       = new Byte [TOK_NUMBER_OF_AMPLI];
                _nTestTensionValue = new UInt32[TOK_NUMBER_OF_AMPLI];
                _nImpedenceValue   = new UInt32[TOK_NUMBER_OF_ZONES];

                nOffset  = 0;
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nStatusFlags = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesTested = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesTestFail = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesTestWarning = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesActive = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nInputsStatus = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nOutputsStatus = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for(int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {                    
                    for(nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nMicroAmpli[nIdx] = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt32) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nTestTensionValue[nIdx] = Common.ConvertAlfa2UInt32(btTmp);

                    nOffset += (sizeof(UInt32) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt32) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nImpedenceValue[nIdx] = Common.ConvertAlfa2UInt32(btTmp);

                    nOffset += (sizeof(UInt32) * 2);
                }

                #if DEBUG
                    DumpStatus();
                #endif

            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;            
            }
        }

        /// <summary>
        /// Dump status
        /// </summary>
        public void DumpStatus()
        {
            Common.DumpLogMessage(" >>> Status Dump <<<");
            Common.DumpLogMessage("         StatusFlags: 0x" + _nStatusFlags.ToString("X4"));
            Common.DumpLogMessage("         ZonesTested: 0x" + _nZonesTested.ToString("X4"));
            Common.DumpLogMessage("       ZonesTestFail: 0x" + _nZonesTestFail.ToString("X4"));
            Common.DumpLogMessage("    ZonesTestWarning: 0x" + _nZonesTestWarning.ToString("X4"));
            Common.DumpLogMessage("         ZonesActive: 0x" + _nZonesActive.ToString("X4"));
            Common.DumpLogMessage("        InputsStatus: 0x" + _nInputsStatus.ToString("X4"));
            Common.DumpLogMessage("       OutputsStatus: 0x" + _nOutputsStatus.ToString("X4"));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                Common.DumpLogMessage(string.Format("       MicroAmpli[{0}]: 0x{1}", nIdx.ToString("X"), _nMicroAmpli[nIdx].ToString("X2")));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                Common.DumpLogMessage(string.Format(" TestTensionValue[{0}]: 0x{1}", nIdx.ToString("X"), _nTestTensionValue[nIdx].ToString("X4")));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                Common.DumpLogMessage(string.Format("   ImpedenceValue[{0}]: 0x{1}", nIdx.ToString("X"), _nImpedenceValue[nIdx].ToString("X4")));
        }

        #endregion

        #region Private Members

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Reflection;

namespace Telefin.AutoConfigurator.Class
{
    public class AmpliManager : ManagerBase
    {
        private const string TOK_PZ_TYPPE = @"DT04000";
        private const string TOK_XPATH_DEVICES_QUERY = "./telefin/system/server/region/zone/node/device[@type=\"DT04000\"]/.";
        private const string TOK_XPATH_PORT_QUERY = "./telefin/port/item[@id=\"@PORT_NUMBER\"]/.";

        private const string TOK_MSG_ITEM_NAME = @"Lettura configurazione Amplificatore: ";
        private const string TOK_MSG_WRITE_CONFIG = @"Scrittura configurazione ";
        private const string TOK_MSG_WRITE_END_CONFIG = @"Scrittura configurazione finale";
        private const string TOK_MSG_START_SAVE_CONFIG = @"Salvataggio configurazione Amplificatore su file...";
        private const string TOK_MSG_END_SAVE_CONFIG = @"Salvataggio configurazione concluso";
        private const string TOK_MSG_START_WRITE_CONFIG = @"Scrittura configurazione Amplificatore da file...";
        private const string TOK_MSG_END_WRITE_CONFIG = @"Scrittura configurazione concluso";

        private const string TOK_ERROR_READ_CONFIGURATION = @"*** Errore in lettura configurazione dispositivo ***";
        private const string TOK_ERROR_READ_SW_VERSION = @"*** Errore in lettura versione software ***";
        private const string TOK_ERROR_READ_STATUS = @"*** Errore in lettura stato dispositivo ***";

        private const string TOK_CMD_ASK_SW_VERSION = @"N";
        private const string TOK_CMD_ASK_STATUS = @"D";
        private const string TOK_CMD_ASK_CONFIG = @"A";
        private const string TOK_CMD_WRITE_CONFIG = @"R";
        private const string TOK_CMD_WRITE_EEPROM_CONFIG = @"H";
        private const string TOK_CMD_RESET_FLAG_CONFIG = @"J";

        #region Enums

        private enum ManageState : int
        {
            None,
            Start,
            OpenComPort,
            CloseComPort,
            ReadSoftwareVersion,
            ReadStatus,
            ReadConfiguration,
            ProcessConfiguration,
            WriteConfiguration,
            WriteEEPromConfiguration,
            ResetFlagConfigurazione,
            End,
            ErrorStatus
        }

        #endregion


        #region Private Members

        private ManageState _nState = ManageState.None;

        private List<AmpliItem> _olItems = null;
        private AmpliStatus _oStatus = null;
        private AmpliConfig _oConfig = null;

        #endregion



        /// <summary>
        /// Load xml config file
        /// </summary>
        /// <returns>true = Ok false = error</returns>
        public override bool LoadConfig()
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_LOAD_CONFIG);

                RegistryKey oRegKey = Registry.LocalMachine.OpenSubKey(TOK_KEY_REG, false);
                string sXmlFileName = oRegKey != null ? oRegKey.GetValue(TOK_KEY_VALUE).ToString() : TOK_DEFAULT_XML;

                if (File.Exists(sXmlFileName) == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_XML_NOT_EXIST);
                    return (false);
                }

                XmlDocument oDoc = new XmlDocument();

                oDoc.Load(sXmlFileName);

                XmlNodeList oNodeList = oDoc.SelectNodes(TOK_XPATH_DEVICES_QUERY);

                if (oNodeList == null || oNodeList.Count == 0)
                {
                    Common.DumpLogMessage(TOK_ERROR_XML_DEVICES_NOT_PRESENT);
                    return (false);
                }

                XmlElement oPortEle = null;
                XmlElement oEle = null;
                AmpliItem oItem = null;
                _olItems = new List<AmpliItem>();

                foreach (XmlNode oNode in oNodeList)
                {
                    oItem = new AmpliItem();

                    oEle = (XmlElement)oNode;

                    oItem.Name = oEle.Attributes["name"].Value.ToString();
                    oItem.SerialNumber = oEle.Attributes["SN"].Value.ToString();
                    oItem.IDSerialPort = XmlConvert.ToInt32(oEle.Attributes["port"].Value);
                    oItem.Address = oEle.Attributes["addr"].Value.ToString();
                    oItem.NodeName = oEle.ParentNode.Attributes["name"].Value.ToString();

                    oPortEle = (XmlElement)oDoc.SelectSingleNode(TOK_XPATH_PORT_QUERY.Replace("@PORT_NUMBER", oItem.IDSerialPort.ToString()));
                    if (oPortEle == null)
                    {
                        Common.DumpLogMessage(TOK_ERROR_XML_PORT_NOT_PRESENT.Replace("@PORT_NUMBER", oItem.IDSerialPort.ToString()));
                        return (false);
                    }

                    oItem.IDPort = XmlConvert.ToInt32(oPortEle.Attributes["id"].Value);
                    oItem.PortName = oPortEle.Attributes["name"].Value;
                    oItem.PortType = oPortEle.Attributes["type"].Value;
                    oItem.ComID = XmlConvert.ToInt32(oPortEle.Attributes["com"].Value);
                    oItem.ComBaud = XmlConvert.ToInt32(oPortEle.Attributes["baud"].Value);
                    oItem.ComEcho = XmlConvert.ToBoolean(oPortEle.Attributes["echo"].Value);
                    oItem.ComDataLen = XmlConvert.ToInt32(oPortEle.Attributes["data"].Value);
                    oItem.ComDataStop = XmlConvert.ToInt32(oPortEle.Attributes["stop"].Value);
                    oItem.ComParity = oPortEle.Attributes["parity"].Value;
                    oItem.ComTimeout = XmlConvert.ToInt32(oPortEle.Attributes["timeout"].Value);

                    _olItems.Add(oItem);

                    StringBuilder oBuilder = new StringBuilder();
                    oBuilder.Append(TOK_MSG_ITEM_NAME);
                    oBuilder.Append(oItem.Name);
                    oBuilder.Append(TOK_MSG_NODE_NAME);
                    oBuilder.Append(oItem.NodeName);

                    Common.DumpLogMessage(oBuilder.ToString());
                }

                return (true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif
                Common.DumpLogMessage(sbError.ToString());

                return (false);
            }
        }


        /// <summary>
        /// dump device config to file
        /// </summary>
        /// <param name="sAddress">Address device</param>
        /// <param name="sFile">Output file</param>
        /// <param name="sComSettings">Com settings</param>
        public override void DoReadConfig(string sAddress, string sFile, string sComSettings)
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_START_SAVE_CONFIG);

                string[] sComArg = sComSettings.Split(',');

                AmpliItem oItem = new AmpliItem();
                AmpliConfig oConfig = new AmpliConfig();

                oItem.Address = sAddress;
                oItem.SerialWrap = new SerialWrapper();
                oItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                               Convert.ToInt32(sComArg[1]),
                                               sComArg[2],
                                               Convert.ToInt32(sComArg[3]),
                                               Convert.ToInt32(sComArg[4]) - 1,
                                               1000,
                                               sComArg[5] == "T" ? true : false,
                                               sComArg[6] == "T" ? true : false);

                oItem.SerialWrap.OpenComPort();

                // Richiesta versione SW -----------
                bool bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_SW_VERSION, oItem.Address))
                {
                    bool bIsEnd = false;
                    if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd))
                    {
                        bRet = true;
                        Common.DumpLogMessage(TOK_MSG_SW_VERSION + Common.Byte2String(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen));
                    }
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION);
                    _nState = ManageState.ErrorStatus;
                }


                // Richiesta Stato -------------
                bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oItem.Address))
                {
                    bool bIsEnd = false;

                    do
                    {
                        if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (bIsEnd == false);
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_STATUS);
                    _nState = ManageState.ErrorStatus;
                }
                else
                {
                    _oStatus = new AmpliStatus();
                    _oStatus.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                    _oStatus.DumpStatus();
                }

                // Prelievo Configurazione ------------------
                bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_CONFIG, oItem.Address))
                {
                    bool bIsEnd = false;

                    do
                    {
                        if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (bIsEnd == false);
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_CONFIGURATION);
                    oItem.SerialWrap.Close();
                    return;
                }
                else
                {
                    oConfig.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                    oConfig.Save2File(sFile);
                }

                oItem.SerialWrap.Close();

                Common.DumpLogMessage(TOK_MSG_END_SAVE_CONFIG);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }


        /// <summary>
        /// Do Autoconfig
        /// </summary>
        public override void DoAutoConfig()
        {
            try
            {
                bool bRun;
                StringBuilder oBuilder;
                bool bRet = false;

                _nState = ManageState.None;

                foreach (AmpliItem oItem in _olItems)
                {
                    bRun = true;
                    _nState = ManageState.None;

                    do
                    {
                        switch (_nState)
                        {
                            // init state
                            // 
                            case ManageState.None:

                                oBuilder = new StringBuilder();
                                oBuilder.Append(TOK_MSG_START_AUTOCONF);
                                oBuilder.Append(oItem.Name);
                                oBuilder.Append(TOK_MSG_NODE_NAME);
                                oBuilder.Append(oItem.NodeName);
                                Common.DumpLogMessage(oBuilder.ToString());

                                oItem.SerialWrap = new SerialWrapper();
                                oItem.SerialWrap.InitComPort(oItem.ComID,
                                                               oItem.ComBaud,
                                                               oItem.ComParity,
                                                               oItem.ComDataLen,
                                                               oItem.ComDataStop,
                                                               oItem.ComTimeout,
                                                               oItem.PortType == TOK_RS232_TYPE,
                                                               oItem.ComEcho);
                                _nState = ManageState.Start;

                                break;

                            // start
                            //
                            case ManageState.Start:
                                {
                                    // Cristian 25-03-2008
                                    if (!this.StopSTLCServices())
                                    {
                                        Common.DumpLogMessage(TOK_ERROR_SERVICE_STOP_TEST);
                                        //return;
                                    }

                                    _nState = ManageState.OpenComPort;

                                    break;
                                }
                            // open com port
                            //
                            case ManageState.OpenComPort:

                                oItem.SerialWrap.OpenComPort();

                                _nState = ManageState.ReadSoftwareVersion;

                                break;

                            // Read software version
                            //
                            case ManageState.ReadSoftwareVersion:

                                bRet = false;
                                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_SW_VERSION, oItem.Address))
                                {
                                    bool bIsEnd = false;
                                    if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd))
                                    {
                                        bRet = true;
                                        Common.DumpLogMessage(TOK_MSG_SW_VERSION + Common.Byte2String(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen));
                                    }
                                }

                                oItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.ReadStatus;

                                break;

                            // Read Status
                            //
                            case ManageState.ReadStatus:

                                bRet = false;
                                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oItem.Address))
                                {
                                    bool bIsEnd = false;

                                    do
                                    {
                                        if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_READ_STATUS);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                {
                                    _oStatus = new AmpliStatus();
                                    _oStatus.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                                    _oStatus.DumpStatus();
                                    _nState = ManageState.ReadConfiguration;

                                }

                                break;

                            // Read Ampli config
                            //
                            case ManageState.ReadConfiguration:

                                bRet = false;
                                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_CONFIG, oItem.Address))
                                {
                                    bool bIsEnd = false;

                                    do
                                    {
                                        if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_READ_CONFIGURATION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                {
                                    _oConfig = new AmpliConfig();
                                    _oConfig.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                                    _nState = ManageState.ProcessConfiguration;
                                }

                                break;

                            // process configuration
                            //
                            case ManageState.ProcessConfiguration:

                                _oConfig.ApplyDefault();
                                _nState = ManageState.WriteConfiguration;

                                break;

                            // write configuration
                            //
                            case ManageState.WriteConfiguration:

                                Common.DumpLogMessage(TOK_MSG_WRITE_CONFIG);

                                // create strem from object
                                //
                                _oConfig.Serialize2Stream();

                                bRet = false;
                                if (oItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oItem.Address))
                                {
                                    bool bIsEnd = false;
                                    Byte[] btBlock = null;
                                    int nBlock = 0;
                                    int nLen = 0;
                                    int nOffset = 0;
                                    Byte[] btSerialize = _oConfig.SerializeBuffer;

                                    do
                                    {
                                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                                        nLen = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                                        btBlock = new Byte[nLen];

                                        for (int nIdx = 0; nIdx < nLen; nIdx++)
                                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                                        bIsEnd = nLen == TOK_MAX_BLOCK_SIZE ? false : true;

                                        if (oItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.WriteEEPromConfiguration;

                                break;


                            // write configuration to EEProm
                            //
                            case ManageState.WriteEEPromConfiguration:

                                bRet = false;
                                if (oItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oItem.Address))
                                {
                                    bool bIsEnd = false;
                                    Byte[] btBlock = null;
                                    int nBlock = 0;
                                    int nLen = 0;
                                    int nOffset = 0;
                                    Byte[] btSerialize = _oConfig.SerializeBuffer;

                                    do
                                    {
                                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                                        nLen = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                                        btBlock = new Byte[nLen];

                                        for (int nIdx = 0; nIdx < nLen; nIdx++)
                                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                                        bIsEnd = nLen == TOK_MAX_BLOCK_SIZE ? false : true;

                                        if (oItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                                        {
                                            bRet = false;
                                            break;
                                        }
                                        else
                                            bRet = true;
                                    } while (bIsEnd == false);
                                }

                                oItem.SerialWrap.CloseSentCommand();

                                if (bRet == false)
                                {
                                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                                    _nState = ManageState.ErrorStatus;
                                }
                                else
                                    _nState = ManageState.ResetFlagConfigurazione;

                                break;

                            // reset flag configuration changed
                            //
                            case ManageState.ResetFlagConfigurazione:
                                {
                                    bRet = oItem.SerialWrap.SendCommand(TOK_CMD_RESET_FLAG_CONFIG, oItem.Address);
                                    oItem.SerialWrap.CloseSentCommand();

                                    if (bRet == false)
                                    {
                                        Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                                        _nState = ManageState.ErrorStatus;
                                    }
                                    else
                                        _nState = ManageState.CloseComPort;

                                    Common.DumpLogMessage(TOK_MSG_WRITE_END_CONFIG);

                                    break;
                                }
                            case ManageState.CloseComPort:
                                {
                                    oItem.SerialWrap.Close();

                                    _nState = ManageState.End;

                                    break;
                                }
                            case ManageState.End:
                                {
                                    _oConfig.Save2File(string.Format("AmpliConfig_COM{0}_ADDRESS{1}.txt", oItem.ComID, oItem.Address));
                                    oBuilder = new StringBuilder();
                                    oBuilder.Append(TOK_MSG_END_AUTOCONF);
                                    oBuilder.Append(oItem.Name);
                                    oBuilder.Append(TOK_MSG_NODE_NAME);
                                    oBuilder.Append(oItem.NodeName);
                                    Common.DumpLogMessage(oBuilder.ToString());
                                    bRun = false;
                                    _nState = ManageState.None;

                                    // Cristian 25-03-2008
                                    if (!this.StartSTLCServices())
                                    {
                                        Common.DumpLogMessage(TOK_ERROR_SERVICE_START_TEST);
                                    }

                                    break;
                                }
                            // error status
                            //
                            case ManageState.ErrorStatus:
                                {
                                    _nState = ManageState.CloseComPort;
                                    break;
                                }
                        }

                    } while (bRun == true);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }


        /// <summary>
        /// write config from file to device
        /// </summary>
        /// <param name="sFile">Input file</param>
        /// <param name="sComSettings">Com settings</param>
        public override void DoWriteConfig(string sAddress, string sFile, string sComSettings)
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_START_WRITE_CONFIG);

                string[] sComArg = sComSettings.Split(',');

                AmpliItem oItem = new AmpliItem();
                AmpliConfig oConfig = new AmpliConfig();

                if (!File.Exists(sFile)) sFile = Common.GetProgramPath() + Path.DirectorySeparatorChar + sFile;
                oConfig.LoadFromfile(sFile);
                oConfig.Serialize2Stream();

                oItem.Address = sAddress;
                oItem.SerialWrap = new SerialWrapper();
                oItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                             Convert.ToInt32(sComArg[1]),
                                             sComArg[2],
                                             Convert.ToInt32(sComArg[3]),
                                             Convert.ToInt32(sComArg[4]) - 1,
                                             1000,
                                             sComArg[5] == "T" ? true : false,
                                             sComArg[6] == "T" ? true : false);

                oItem.SerialWrap.OpenComPort();

                bool bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oItem.Address))
                {
                    bool bIsEnd = false;
                    Byte[] btBlock = null;
                    int nBlock = 0;
                    int nLen = 0;
                    int nOffset = 0;
                    Byte[] btSerialize = oConfig.SerializeBuffer;

                    do
                    {
                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                        nLen = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                        btBlock = new Byte[nLen];

                        for (int nIdx = 0; nIdx < nLen; nIdx++)
                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                        bIsEnd = nLen == TOK_MAX_BLOCK_SIZE ? false : true;

                        if (oItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (bIsEnd == false);
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                    oItem.SerialWrap.Close();
                    return;
                }

                bRet = oItem.SerialWrap.SendCommand(TOK_CMD_RESET_FLAG_CONFIG, oItem.Address);
                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                    _nState = ManageState.ErrorStatus;
                }
                else
                    _nState = ManageState.CloseComPort;


                oItem.SerialWrap.Close();

                Common.DumpLogMessage(TOK_MSG_END_WRITE_CONFIG);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }
    }
}

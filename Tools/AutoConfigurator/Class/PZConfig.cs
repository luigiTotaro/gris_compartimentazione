
//
// Telefin
//
// Progetto:	PZAutoConfigurator
// File:		PZConfig.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		24.05.2007
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace Telefin.AutoConfigurator.Class
{
    public class PZConfig
    {
        #region Internal Class / Struct

        public struct Input 
        {
            public Byte     MicroId;      // ingresso microfonico 0-2 
            public SByte [] AmpliLevel;   // liv.segn.in ampli -20..+6 dB(127 disable)
            public UInt16   Zones;        // zone da attivare 
        }

        public struct Micro
        {
            public Byte     Priority;      // priorita` ingresso microf. 0..2 
            public Byte     Equalization;  // livello equalizzazione ingresso 0..255 
        }

        public struct FunctionParams
        {
            public Byte []  Params;        // 6 byte fissi (non usati in questo programma)
        }

        #endregion

        #region Token

        private const int       TOK_BYTES_BUFFER_LEN            = 872;

        private const int       TOK_NUMBER_OF_AMPLI             = 2; 
        private const int       TOK_NUMBER_OF_MICRO             = 3;
        private const int       TOK_NUMBER_OF_ZONES             = 12;
        private const int       TOK_NUMBER_OF_INPUTS            = 16; 
        private const int       TOK_NUMBER_OF_OUTPUTS           = 16;
        private const int       TOK_SW_VERSION_LENGTH           = 12; 
        private const int       TOK_PARAMETERS_VERSION_LENGTH   = 15;
        private const int       TOK_PARAMETERS_LENGTH           = 6;
        private const int       TOK_CONFIG_VERSION_LENGTH       = 15;
        private const int       TOK_MAX_PLC_PROG_BLOCKS         = 48;

        private const string    TOK_APPLY_ZONE2TEST             = @" >>> ApplyZone2Test <<< ";
        private const string    TOK_BUFFER_LEN_STR              = @" BufferLen: ";
        private const string    TOK_BUFFER_STR                  = @"    Buffer: ";

        private const string    TOK_ERROR_INPUT_BYTE_LEN        = @"Lunghezza buffer di ingresso errata";

        #endregion

        #region Private Members

        private char []             _cConfigVersion;        // versione configurazione sistema 
        private UInt16              _nStartNightTime;       // orario di inizio attenuazione notturna
        private UInt16              _nEndNightTime;         // orario di fine attenuazione notturna 
        private UInt16              _nZonesToDisabled;      // zone da disattivare durante il periodo di attenuazione
        private Byte []             _nAmpliAttenuation;     // Attenuazione in dB 0..20
        private UInt16              _nTestStartTime;        // orario di inizio effettuazione del test
        private UInt16              _nTestEndTime;          // orario di fine effettuazione del test
        private UInt16              _nTestPeriod;           // periodicit� del test
        private UInt16              _nZonesToTest;          // zone su cui effettuare il test
        private Byte []             _nTestLevel;            // liv. segnale test all�input amplificatore
        private UInt16 []           _nTensionsRange;        // Variaz. ammessa misura tensione amplif. 
        private UInt16 []           _nResistanceRange;      // Variaz. ammessa misura resistenza zone 
        private Byte                _nAmplifiersMode;       // Amplificatori installati e loro modo d'uso 
        private Micro []            _Micros;                // Config. ingressi microfonici/Aux
        private Input []            _Inputs;                // Configurazione ingressi 
        private FunctionParams []   _PrgBlocks;             // Blocchi programma PLC

        private Byte []             _btSerialize;           // serialize buffer;

        #endregion

        #region Accessor

        public Byte[] SerializeBuffer
        {
            get
            {
                return(_btSerialize);
            }
        }

        public char [] ConfigVersion
        {
            get
            {
                return(_cConfigVersion);
            }
            set
            {
                _cConfigVersion = value;
            }
        }

        public UInt16 StartNightTime
        {
            get
            {
                return(_nStartNightTime);
            }
            set
            {
                _nStartNightTime = value;
            }
        }

        public UInt16 EndNightTime
        {
            get
            {
                return(_nEndNightTime);
            }
            set
            {
                _nEndNightTime = value;
            }
        }

        public UInt16 ZonesToDisabled
        {
            get
            {
                return(_nZonesToDisabled);
            }
            set
            {
                _nZonesToDisabled = value;
            }
        }

        public Byte [] AmpliAttenuation
        {
            get
            {
                return(_nAmpliAttenuation);
            }
            set
            {
                _nAmpliAttenuation = value;
            }
        }

        public UInt16 TestStartTime
        {
            get
            {
                return(_nTestStartTime);
            }
            set
            {
                _nTestStartTime = value;
            }
        }

        public UInt16 TestEndTime
        {
            get
            {
                return(_nTestEndTime);
            }
            set
            {
                _nTestEndTime = value;
            }
        }

        public UInt16 TestPeriod
        {
            get
            {
                return(_nTestPeriod);
            }
            set
            {
                _nTestPeriod = value;
            }
        }

        public UInt16 ZonesToTest
        {
            get
            {
                return(_nZonesToTest);
            }
            set
            {
                _nZonesToTest = value;
            }
        }

        public Byte [] TestLevel
        {
            get
            {
                return(_nTestLevel);
            }
            set
            {
                _nTestLevel = value;
            }
        }

        public UInt16 [] TensionsRange
        {
            get
            {
                return(_nTensionsRange);
            }
            set
            {
                _nTensionsRange = value;
            }
        }

        public UInt16 [] ResistanceRange
        {
            get
            {
                return(_nResistanceRange);
            }
            set
            {
                _nResistanceRange = value;
            }
        }

        public Byte AmplifiersMode
        {
            get
            {
                return(_nAmplifiersMode);
            }
            set
            {
                _nAmplifiersMode = value;
            }
        }

        public Micro [] Micros
        {
            get
            {
                return(_Micros);
            }
            set
            {
                _Micros = value;
            }
        }

        public Input [] Inputs
        {
            get
            {
                return(_Inputs);
            }
            set
            {
                _Inputs = value;
            }
        }

        public FunctionParams [] PrgBlocks
        {
            get
            {
                return(_PrgBlocks);
            }
            set
            {
                _PrgBlocks = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Load members from bytes stream
        /// </summary>
        /// <param name="btTmp">input bytes stream</param>
        public void LoadFromStream(byte[] btInput, int nLen)
        {
            try
            {
                int     nCount   = 0;
                int     nOffset  = 0;
                Byte [] btTmp    = new Byte[1024];

                // check input len
                //
                if (btInput == null || nLen != TOK_BYTES_BUFFER_LEN)
                    throw new Exception(TOK_ERROR_INPUT_BYTE_LEN);

                _cConfigVersion     = new char[TOK_CONFIG_VERSION_LENGTH];
                _nAmpliAttenuation  = new Byte[TOK_NUMBER_OF_AMPLI];
                _nTestLevel         = new Byte[TOK_NUMBER_OF_AMPLI];
                _nTensionsRange     = new UInt16[TOK_NUMBER_OF_AMPLI];
                _nResistanceRange   = new UInt16[TOK_NUMBER_OF_ZONES];
                _Micros             = new Micro[TOK_NUMBER_OF_MICRO];
                _Inputs             = new Input[TOK_NUMBER_OF_INPUTS];
                _PrgBlocks          = new FunctionParams[TOK_MAX_PLC_PROG_BLOCKS];
               
                nOffset  = 0;
                for (nCount = 0; nCount < TOK_CONFIG_VERSION_LENGTH * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _cConfigVersion = Common.ConvertAlfa2CharArray(btTmp, TOK_CONFIG_VERSION_LENGTH);

                nOffset += TOK_CONFIG_VERSION_LENGTH * 2;
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nStartNightTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nEndNightTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for(nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesToDisabled = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nAmpliAttenuation[nIdx] = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);
                }

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTestStartTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTestEndTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTestPeriod = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesToTest = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nTestLevel[nIdx] = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nTensionsRange[nIdx] = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nResistanceRange[nIdx] = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }

                for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nAmplifiersMode = Common.ConvertAlfa2Byte(btTmp);

                nOffset += (sizeof(Byte) * 2);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                {                    
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Micros[nIdx].Priority = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);

                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Micros[nIdx].Equalization = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);

                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Inputs[nIdx].MicroId = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);

                    _Inputs[nIdx].AmpliLevel = new SByte[TOK_NUMBER_OF_AMPLI]; 

                    for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                    {
                        for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                            btTmp[nCount] = btInput[nOffset + nCount];

                        _Inputs[nIdx].AmpliLevel[nTmp] = Common.ConvertAlfa2SByte(btTmp);

                        nOffset += (sizeof(SByte) * 2);
                    }

                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Inputs[nIdx].Zones = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                {
                    _PrgBlocks[nIdx].Params = new Byte[TOK_PARAMETERS_LENGTH];

                    for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                    {
                        for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                            btTmp[nCount] = btInput[nOffset + nCount];

                        _PrgBlocks[nIdx].Params[nTmp] = Common.ConvertAlfa2Byte(btTmp);

                        nOffset += (sizeof(Byte) * 2);
                    }
                }
            
                #if DEBUG
                    DumpConfig();
                #endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;            
            }
        }

        /// <summary>
        /// Process configuration and apply zone to test
        /// </summary>
        public void ApplyZone2Test()
        {
            try
            {
                #if DEBUG
                    Common.DumpLogMessage(TOK_APPLY_ZONE2TEST);
                #endif

                // set zone to test
                //
                _nZonesToTest = 0;               
                for(int nCount = 0; nCount < TOK_NUMBER_OF_INPUTS; nCount++)
                    _nZonesToTest |= _Inputs[nCount].Zones;

                // set ampli level
                //
                _nTestLevel[0] = 128;
                _nTestLevel[1] = (_nAmplifiersMode >= 1) ? (byte)128 : (byte)0;
                     
             
                // set time 
                //
                _nTestStartTime = 0;
                _nTestEndTime   = 1439;
                _nTestPeriod    = 30;

                #if DEBUG                    
                    DumpConfig();
                #endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e; 
            }
        }

        /// <summary>
        /// Serialize class to byte stream
        /// </summary>
        /// <returns></returns>
        public void Serialize2Stream()
        {
            try
            {
                int     nOffset     = 0;
                byte [] btSerialize = new Byte[2048];
                byte [] btTmp;

                // clear buffer
                //
                for (int nIdx = 0; nIdx < 2048; nIdx++)
                    btSerialize[nIdx] = 0;

                // config version
                //
                btTmp = Common.ConvertCharArray2Alfa(_cConfigVersion, _cConfigVersion.Length);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;
                
                // start night time
                //
                btTmp = Common.ConvertUInt162Alfa(_nStartNightTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // end night time
                //
                btTmp = Common.ConvertUInt162Alfa(_nEndNightTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // zone to disable
                //
                btTmp = Common.ConvertUInt162Alfa(_nZonesToDisabled);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // ampli attenuation
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertByte2Alfa(_nAmpliAttenuation[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // test start time
                //
                btTmp = Common.ConvertUInt162Alfa(_nTestStartTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // test end time
                //
                btTmp = Common.ConvertUInt162Alfa(_nTestEndTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // test period
                //
                btTmp = Common.ConvertUInt162Alfa(_nTestPeriod);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // zone to test
                //
                btTmp = Common.ConvertUInt162Alfa(_nZonesToTest);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // test level
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertByte2Alfa(_nTestLevel[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                //  tension range
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertUInt162Alfa(_nTensionsRange[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // resistance range
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                {
                    btTmp = Common.ConvertUInt162Alfa(_nResistanceRange[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // amplifiers mode
                //
                btTmp = Common.ConvertByte2Alfa(_nAmplifiersMode);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // micros
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                {              
                    // priority
                    //
                    btTmp = Common.ConvertByte2Alfa(_Micros[nIdx].Priority);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
      
                    // equalization
                    //
                    btTmp = Common.ConvertByte2Alfa(_Micros[nIdx].Equalization);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // inputs
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                {
                    // microid
                    //
                    btTmp = Common.ConvertByte2Alfa(_Inputs[nIdx].MicroId);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;

                    // ampli level
                    //
                    for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                    {
                        btTmp = Common.ConvertSByte2Alfa(_Inputs[nIdx].AmpliLevel[nTmp]);
                        btTmp.CopyTo(btSerialize, nOffset);
                        nOffset += btTmp.Length;
                    }

                    // Zones
                    //
                    btTmp = Common.ConvertUInt162Alfa(_Inputs[nIdx].Zones);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // program block
                //
                for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                {
                    for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                    {
                        btTmp = Common.ConvertByte2Alfa(_PrgBlocks[nIdx].Params[nTmp]);
                        btTmp.CopyTo(btSerialize, nOffset);
                        nOffset += btTmp.Length;
                    }
                }

                _btSerialize = new Byte[nOffset];
                
                for(int nIdx = 0; nIdx < nOffset; nIdx++)
                    _btSerialize[nIdx] = btSerialize[nIdx];

                #if DEBUG

                    Common.DumpLogMessage(" >>> Serialize Config Dump <<<");
                    Common.DumpLogMessage(TOK_BUFFER_LEN_STR + _btSerialize.Length.ToString());
                    Common.DumpLogMessage(TOK_BUFFER_STR     + Common.Byte2HexString(_btSerialize));

                #endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Dump config
        /// </summary>
        public void DumpConfig()
        {
            string sTmp = string.Empty;

            for (int nIdx = 0; nIdx < TOK_CONFIG_VERSION_LENGTH; nIdx++)
                sTmp += _cConfigVersion[nIdx];

            Common.DumpLogMessage(" >>> Config Dump <<<");
            Common.DumpLogMessage("           ConfigVersion: " + sTmp);
            Common.DumpLogMessage("          StartNightTime: 0x" + _nStartNightTime.ToString("X4"));
            Common.DumpLogMessage("            EndNightTime: 0x" + _nEndNightTime.ToString("X4"));
            Common.DumpLogMessage("         ZonesToDisabled: 0x" + _nZonesToDisabled.ToString("X4"));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                Common.DumpLogMessage(string.Format("     AmpliAttenuation[{0}]: 0x{1}", nIdx.ToString("X"), _nAmpliAttenuation[nIdx].ToString("X2")));

            Common.DumpLogMessage("           TestStartTime: 0x" + _nTestStartTime.ToString("X4"));
            Common.DumpLogMessage("             TestEndTime: 0x" + _nTestEndTime.ToString("X4"));
            Common.DumpLogMessage("              TestPeriod: 0x" + _nTestPeriod.ToString("X4"));
            Common.DumpLogMessage("             ZonesToTest: 0x" + _nZonesToTest.ToString("X4"));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                Common.DumpLogMessage(string.Format("            TestLevel[{0}]: 0x{1}", nIdx.ToString("X"), _nTestLevel[nIdx].ToString("X2")));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                Common.DumpLogMessage(string.Format("        TensionsRange[{0}]: 0x{1}", nIdx.ToString("X"), _nTensionsRange[nIdx].ToString("X4")));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                Common.DumpLogMessage(string.Format("      ResistanceRange[{0}]: 0x{1}", nIdx.ToString("X"), _nResistanceRange[nIdx].ToString("X4")));

            Common.DumpLogMessage("          AmplifiersMode: 0x" + _nAmplifiersMode.ToString("X2"));

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
            {
                Common.DumpLogMessage(string.Format("      Micros[{0}].Priority: 0x{1}", nIdx.ToString("X"), _Micros[nIdx].Priority.ToString("X2")));
                Common.DumpLogMessage(string.Format("  Micros[{0}].Equalization: 0x{1}", nIdx.ToString("X"), _Micros[nIdx].Equalization.ToString("X2")));
            }

            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
            {
                Common.DumpLogMessage(string.Format("       Inputs[{0}].MicroId: 0x{1}", nIdx.ToString("X"), _Inputs[nIdx].MicroId.ToString("X2")));

                for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                    Common.DumpLogMessage(string.Format(" Inputs[{0}].AmpliLevel[{1}]: 0x{2}", nIdx.ToString("X"), nTmp.ToString("X"), _Inputs[nIdx].AmpliLevel[nTmp].ToString("X2")));

                Common.DumpLogMessage(string.Format("         Inputs[{0}].Zones: 0x{1}", nIdx.ToString("X"), _Inputs[nIdx].Zones.ToString("X4")));
            }

            for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
            {
                for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                    Common.DumpLogMessage(string.Format("  PrgBlocks[{0}].Params[{1}]: 0x{2}", nIdx.ToString("X"), nTmp.ToString("X"), _PrgBlocks[nIdx].Params[nTmp].ToString("X2")));
            }
        }

        /// <summary>
        /// Save configuration to file
        /// </summary>
        /// <param name="sFile">file name</param>
        public void Save2File(string sFile)
        {
            try
            {
                string sTmp = string.Empty;

                // dump configuration to file
                //
                using (TextWriter oWriter = new StreamWriter(sFile))
                {
                    oWriter.WriteLine("[Telefin PDS Config File]");
                    for (int nIdx = 0; nIdx < TOK_CONFIG_VERSION_LENGTH; nIdx++)
                        sTmp += _cConfigVersion[nIdx];
                    oWriter.WriteLine("Version="                + sTmp);
                    oWriter.WriteLine("");

                    oWriter.WriteLine("[General Config]");
                    sTmp = ((int)(_nStartNightTime / 60)).ToString("00") + "." + ((int)(_nStartNightTime % 60)).ToString("00");
                    oWriter.WriteLine("StartNightTime="         + sTmp);
                    sTmp = ((int)(_nEndNightTime / 60)).ToString("00") + "." + ((int)(_nEndNightTime % 60)).ToString("00");
                    oWriter.WriteLine("EndNightTime="           + sTmp);
                    oWriter.WriteLine("AttenuationAmpli1="      + ((int)_nAmpliAttenuation[0]).ToString());
                    oWriter.WriteLine("AttenuationAmpli2="      + ((int)_nAmpliAttenuation[1]).ToString());
                    sTmp = ((int)(_nTestStartTime / 60)).ToString("00") + "." + ((int)(_nTestStartTime % 60)).ToString("00");
                    oWriter.WriteLine("TestStartTime=" + sTmp);
                    sTmp = ((int)(_nTestEndTime / 60)).ToString("00") + "." + ((int)(_nTestEndTime % 60)).ToString("00");
                    oWriter.WriteLine("TestEndTime="            + sTmp);
                    oWriter.WriteLine("TestPeriod="             + _nTestPeriod.ToString());
                    oWriter.WriteLine("ZonesToTest="            + Common.GetBitPositionString(_nZonesToTest));
                    oWriter.WriteLine("ZonesToDisabled="        + Common.GetBitPositionString(_nZonesToDisabled));
                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                        oWriter.WriteLine("TestLevelAmpli" + (nIdx + 1).ToString() + "=" + ((int)_nTestLevel[nIdx]).ToString());
                    oWriter.WriteLine("AmplifiersMode="         + ((int)_nAmplifiersMode).ToString());
                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                        oWriter.WriteLine("TensionsRangeAmpli" + (nIdx + 1).ToString() + "=" + ((int)_nTensionsRange[nIdx]).ToString());
                    for(int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                        oWriter.WriteLine("ResistanceRange" + (nIdx + 1).ToString() + "=" + ((int)_nResistanceRange[nIdx]).ToString());
                    oWriter.WriteLine("");

                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                    {
                        oWriter.WriteLine("[MicroInputs"  + (nIdx + 1).ToString() + "]");
                        oWriter.WriteLine("Priority="     + ((int)_Micros[nIdx].Priority).ToString());
                        oWriter.WriteLine("Equalization=" + ((int)_Micros[nIdx].Equalization).ToString());
                        oWriter.WriteLine("");
                    }

                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                    {
                        oWriter.WriteLine("[Input"  + (nIdx + 1).ToString() + "]");
                        oWriter.WriteLine("MicroId=" + ((int)_Inputs[nIdx].MicroId).ToString());
                        for(int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                            oWriter.WriteLine("LevelAmpi" + (nTmp + 1).ToString() + "=" + ((int)_Inputs[nIdx].AmpliLevel[nTmp]).ToString());
                        oWriter.WriteLine("Zones=" + Common.GetBitPositionString((int)_Inputs[nIdx].Zones));
                        oWriter.WriteLine("");
                    }

                    for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                    {
                        oWriter.WriteLine("[PrgBlock" + (nIdx + 1).ToString() + "]");
                        for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                            oWriter.WriteLine("Param" + (nTmp + 1).ToString() + "=" + ((int)_PrgBlocks[nIdx].Params[nTmp]).ToString());
                        oWriter.WriteLine("");
                    }
                }

            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Load configuration from file
        /// </summary>
        /// <param name="sFile">file name</param>
        public void LoadFromfile(string sFile)
        {
            try
            {
                string      sTmp = string.Empty;
                string []   sArray;

                _cConfigVersion     = new char[TOK_CONFIG_VERSION_LENGTH];
                _nAmpliAttenuation  = new Byte[TOK_NUMBER_OF_AMPLI];
                _nTestLevel         = new Byte[TOK_NUMBER_OF_AMPLI];
                _nTensionsRange     = new UInt16[TOK_NUMBER_OF_AMPLI];
                _nResistanceRange   = new UInt16[TOK_NUMBER_OF_ZONES];
                _Micros             = new Micro[TOK_NUMBER_OF_MICRO];
                _Inputs             = new Input[TOK_NUMBER_OF_INPUTS];
                _PrgBlocks          = new FunctionParams[TOK_MAX_PLC_PROG_BLOCKS];
               
                sTmp = Common.ReadKeyIniValue("Telefin PDS Config File", "Version", sFile);
                for (int nCount = 0; nCount < TOK_CONFIG_VERSION_LENGTH; nCount++)
                    _cConfigVersion[nCount] = sTmp[nCount];

                sTmp   = Common.ReadKeyIniValue("General Config", "StartNightTime", sFile);
                sArray = sTmp.Split('.');
                _nStartNightTime = (UInt16)(Convert.ToUInt16(sArray[0]) * 60 + Convert.ToUInt16(sArray[1]));

                sTmp = Common.ReadKeyIniValue("General Config", "EndNightTime", sFile);
                sArray = sTmp.Split('.');
                _nEndNightTime = (UInt16)(Convert.ToUInt16(sArray[0]) * 60 + Convert.ToUInt16(sArray[1]));

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("General Config", "AttenuationAmpli" + (nIdx + 1).ToString(), sFile);
                    _nAmpliAttenuation[nIdx] = (Byte)Convert.ToUInt16(sTmp);
                }

                sTmp = Common.ReadKeyIniValue("General Config", "TestStartTime", sFile);
                sArray = sTmp.Split('.');
                _nTestStartTime = (UInt16)(Convert.ToUInt16(sArray[0]) * 60 + Convert.ToUInt16(sArray[1]));

                sTmp = Common.ReadKeyIniValue("General Config", "TestEndTime", sFile);
                sArray = sTmp.Split('.');
                _nTestEndTime = (UInt16)(Convert.ToUInt16(sArray[0]) * 60 + Convert.ToUInt16(sArray[1]));

                sTmp = Common.ReadKeyIniValue("General Config", "TestPeriod", sFile);
                _nTestPeriod = Convert.ToUInt16(sTmp);

                sTmp = Common.ReadKeyIniValue("General Config", "ZonesToTest", sFile);
                sArray = sTmp.Split(',');
                _nZonesToTest = Common.GetValueBitPositionString(sArray);

                sTmp = Common.ReadKeyIniValue("General Config", "ZonesToDisabled", sFile);
                sArray = sTmp.Split(',');
                _nZonesToDisabled = Common.GetValueBitPositionString(sArray);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("General Config", "TestLevelAmpli" + (nIdx + 1).ToString(), sFile);
                    _nTestLevel[nIdx] = (Byte)Convert.ToUInt16(sTmp);
                }

                sTmp = Common.ReadKeyIniValue("General Config", "AmplifiersMode", sFile);
                _nAmplifiersMode = (Byte)Convert.ToUInt16(sTmp);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("General Config", "TensionsRangeAmpli" + (nIdx + 1).ToString(), sFile);
                    _nTensionsRange[nIdx] = Convert.ToUInt16(sTmp);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("General Config", "ResistanceRange" + (nIdx + 1).ToString(), sFile);
                    _nResistanceRange[nIdx] = Convert.ToUInt16(sTmp);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("MicroInputs" + (nIdx + 1).ToString(), "Priority", sFile);
                    _Micros[nIdx].Priority = (Byte)Convert.ToUInt16(sTmp);

                    sTmp = Common.ReadKeyIniValue("MicroInputs" + (nIdx + 1).ToString(), "Equalization", sFile);
                    _Micros[nIdx].Equalization = (Byte)Convert.ToUInt16(sTmp);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("Input" + (nIdx + 1).ToString(), "MicroId", sFile);
                    _Inputs[nIdx].MicroId = (Byte)Convert.ToUInt16(sTmp);

                    _Inputs[nIdx].AmpliLevel = new SByte[TOK_NUMBER_OF_AMPLI]; 

                    for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                    {
                        sTmp = Common.ReadKeyIniValue("Input" + (nIdx + 1).ToString(), "LevelAmpi" + (nTmp + 1).ToString(), sFile);
                        _Inputs[nIdx].AmpliLevel[nTmp] = (SByte)Convert.ToInt16(sTmp);
                    }

                    sTmp = Common.ReadKeyIniValue("Input" + (nIdx + 1).ToString(), "Zones", sFile);
                    sArray = sTmp.Split(',');
                    _Inputs[nIdx].Zones = Common.GetValueBitPositionString(sArray);
                }

                for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                {
                    _PrgBlocks[nIdx].Params = new Byte[TOK_PARAMETERS_LENGTH];

                    for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                    {
                        sTmp = Common.ReadKeyIniValue("PrgBlock" + (nIdx + 1).ToString(), "Param" + (nTmp + 1).ToString(), sFile);
                        _PrgBlocks[nIdx].Params[nTmp] = (Byte)Convert.ToUInt16(sTmp);
                    }
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }
        #endregion

        #region Private Members

        #endregion
    }
}


//
// Telefin
//
// Progetto:	PZAutoConfigurator
// File:		PZItem.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		10.05.2007
//

using System;
using System.Collections.Generic;
using System.Text;

namespace Telefin.AutoConfigurator.Class
{
    /// <summary>
    /// Amplificatore
    /// </summary>
    class AmpliItem
    {
        #region Private Members

        private string          _sName          = string.Empty;
        private string          _sSerialNumber  = string.Empty;
        private int             _nIDSerialPort  = 0;
        private string          _sAddress       = string.Empty;
        private string          _sNodeName      = string.Empty;

        private int             _nIDPort        = 0;
        private string          _sPortName      = string.Empty;
        private string          _sPortType      = string.Empty;
        private int             _nComID         = 0;
        private int             _nComBaud       = 0;
        private bool            _bComEcho       = false;
        private int             _nComDataLen    = 0;
        private int             _nComDataStop   = 0;
        private string          _sComParity     = string.Empty;
        private int             _nComTimeout    = 0;

        private SerialWrapper   _oSerialWrap    = null;

        #endregion

        #region Accessor

        public string Name
        {
            get     { return _sName; }
            set     {_sName = value; }
        }

        public string SerialNumber
        {
            get {return _sSerialNumber;}
            set {_sSerialNumber = value;}
        }

        public int IDSerialPort
        {
            get {return _nIDSerialPort;}
            set {_nIDSerialPort = value;}
        }

        public string Address
        {
            get {return _sAddress;}
            set {_sAddress = value;}
        }

        public string NodeName
        {
            get {return _sNodeName;}
            set {_sNodeName = value;}
        }

        public int IDPort
        {
            get {return _nIDPort;}
            set {_nIDPort = value;}
        }

        public string PortName
        {
            get {return _sPortName;}
            set {_sPortName = value;}
        }

        public string PortType
        {
            get {return _sPortType;}
            set {_sPortType = value;}
        }

        public int ComID
        {
            get {return _nComID;}
            set {_nComID = value;}
        }

        public int ComBaud
        {
            get {return _nComBaud;}
            set {_nComBaud = value;}
        }

        public bool ComEcho
        {
            get {return _bComEcho;}
            set {_bComEcho = value;}
        }

        public int ComDataLen
        {
            get {return _nComDataLen;}
            set {_nComDataLen = value;}
        }

        public int ComDataStop
        {
            get {return _nComDataStop;}
            set {_nComDataStop = value;}
        }

        public string ComParity
        {
            get {return _sComParity;}
            set {_sComParity = value;}
        }

        public int ComTimeout
        {
            get {return _nComTimeout;}
            set {_nComTimeout = value;}
        }

        public SerialWrapper SerialWrap
        {
            get {return(_oSerialWrap);}
            set {_oSerialWrap = value;}
        }

        #endregion
    }
}

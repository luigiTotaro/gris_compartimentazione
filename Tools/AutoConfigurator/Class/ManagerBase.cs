using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.ServiceProcess;

namespace Telefin.AutoConfigurator.Class
{
    public abstract class ManagerBase
    {

        protected const int TOK_MAX_BLOCK_SIZE = 128;

        #region Metodi di gestione dei servizi
        protected const string TOK_KEY_REG = @"SYSTEM\CurrentControlSet\Services\SPVServerDaemon";
        protected const string TOK_KEY_VALUE = @"XMLSystem";

        protected const string TOK_DEFAULT_XML = @"C:\Program Files\Telefin\SupervisorServer\system.xml";

        protected const string TOK_MSG_WAIT_STEP = @".";
        protected const string TOK_ERROR_XML_NOT_EXIST = @"Il file xml di configurazione non � presente [Code=0]";
        protected const string TOK_ERROR_XML_DEVICES_NOT_PRESENT = @"Il file di configurazione non contiene nessuna definizione di pannelli zona [Code=1]";
        protected const string TOK_ERROR_XML_PORT_NOT_PRESENT = @"Il file di configurazione non contiene la definizione della porta numero @PORT_NUMBER [Code=2]";

        protected const string TOK_MSG_LOAD_CONFIG = @"Lettura configurazione sistema...";
        protected const string TOK_MSG_NODE_NAME = @" nodo: ";
        protected const string TOK_MSG_START_AUTOCONF = @"Inizio auto configurazione dispositivo: ";
        protected const string TOK_MSG_END_AUTOCONF = @"Fine auto configurazione dispositivo: ";
        protected const string TOK_MSG_SW_VERSION = @"Versione Software: ";

        protected const string TOK_RS232_TYPE = @"STLC1000_RS232";

        private const string TOK_STLCMANAGERSERVICE = @"STLCManagerService";
        private const string TOK_SPVSERVERDAEMON = @"SPVServerDaemon";
        private const string TOK_MSG_STARTING_MGR_SERVICE = @"Avvio del servizio ""STLC Manager Service"" in corso...";
        private const string TOK_MSG_STARTED_MGR_SERVICE = @"Servizio ""STLC Manager"" avviato !";
        private const string TOK_MSG_STOPPING_MGR_SERVICE = @"Arresto del servizio ""STLC Manager Service"" in corso...";
        private const string TOK_MSG_STOPPED_MGR_SERVICE = @"Servizio ""STLC Manager"" arrestato !";
        private const string TOK_MSG_STOPPING_SPV_SERVICE = @"Arresto del servizio ""Telefin Supervisor Server"" in corso...";
        private const string TOK_MSG_STOPPED_SPV_SERVICE = @"Servizio ""Telefin Supervisor Server"" arrestato !";

        protected const string TOK_ERROR_WRITE_CONFIGURATION = @"*** Errore in scrittura configurazione dispositivo ***";

        internal const string TOK_ERROR_SERVICE_START_TEST = @"*** Impossibile avviare il servizio 'STLC Manager Service'. ***";
        internal const string TOK_ERROR_SERVICE_STOP_TEST = @"*** Impossibile arrestare il servizio 'STLC Manager Service' o 'STLC Supervisor Server Service'. ***";

        private bool _bSTLCManagerServiceStopped = true;


        public bool StartSTLCServices()
        {
            if (_bSTLCManagerServiceStopped)
            {
                try
                {
                    using (ServiceController oServiceControl = new ServiceController(TOK_STLCMANAGERSERVICE))
                    {
                        if (oServiceControl != null)
                        {
                            if (oServiceControl.Status == ServiceControllerStatus.Stopped)
                            {
                                Common.DumpLogMessage(TOK_MSG_STARTING_MGR_SERVICE);

                                oServiceControl.Start();
                                _bSTLCManagerServiceStopped = false;

                                do
                                {
                                    Thread.Sleep(500);
                                    oServiceControl.Refresh();
                                    Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);
                                } while (oServiceControl.Status != ServiceControllerStatus.Running);

                                Common.DumpLogMessage(TOK_MSG_STARTED_MGR_SERVICE);
                                return true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        public bool StopSTLCServices()
        {
            try
            {
                using (ServiceController oServiceControl = new ServiceController(TOK_STLCMANAGERSERVICE))
                {
                    if (oServiceControl != null)
                    {
                        if (oServiceControl.Status == ServiceControllerStatus.Running)
                        {
                            Common.DumpLogMessage(TOK_MSG_STOPPING_MGR_SERVICE);

                            oServiceControl.Stop();
                            _bSTLCManagerServiceStopped = true;

                            do
                            {
                                Thread.Sleep(500);
                                oServiceControl.Refresh();
                                Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);
                            } while (oServiceControl.Status != ServiceControllerStatus.Stopped);

                            Common.DumpLogMessage(TOK_MSG_STOPPED_MGR_SERVICE);
                        }
                    }
                }

                using (ServiceController oServiceControl = new ServiceController(TOK_SPVSERVERDAEMON))
                {
                    if (oServiceControl != null)
                    {
                        if (oServiceControl.Status == ServiceControllerStatus.Running)
                        {
                            Common.DumpLogMessage(TOK_MSG_STOPPING_SPV_SERVICE);

                            oServiceControl.Stop();

                            do
                            {
                                Thread.Sleep(500);
                                oServiceControl.Refresh();
                                Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);
                            } while (oServiceControl.Status != ServiceControllerStatus.Stopped);

                            Common.DumpLogMessage(TOK_MSG_STOPPED_SPV_SERVICE);
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Dump program args
        /// </summary>
        public void DumpProgramArgs()
        {
            StringBuilder sbTemp = new StringBuilder();

            sbTemp.Append("usare: PZAutoConfigurator R File.cds 04 3,9600,N,8,1,F,T Y");
            sbTemp.Append("\r\n");
            sbTemp.Append("R                R       Legge configurazione da dispositivo e genera file ");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 W       Scrive configurazione da file in EEProm dispositivo");
            sbTemp.Append("\r\n");
            sbTemp.Append("FILE                     Nome del file contenente la configurazione");
            sbTemp.Append("\r\n");
            sbTemp.Append("04               Address Indirizzo del dispositivo ");
            sbTemp.Append("\r\n");
            sbTemp.Append("2,9600,N,8,1,T,T 2       porta seriale");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 9600    baud rate");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 N(E)(O) parit� N (Nessuna), E (Even), O (Odd)");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 8       data");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 1       stop bit 0 (Nessuno), 1 (1 stop bit), 1.5, 2");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 T       T=RS232, F=RS485/RS422");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 T       T=Echo on, F= Echo Off");
            sbTemp.Append("\r\n");
            sbTemp.Append("Y                Y       Y=riavvia servizi, N=non riavviare i servizi");
            sbTemp.Append("\r\n");

            Common.DumpLogMessage(sbTemp.ToString(), false);
        }


        /// <summary>
        /// Main function
        /// </summary>
        public abstract void DoAutoConfig();
        
        /// <summary>
        /// Load xml config file
        /// </summary>
        /// <returns>true = Ok false = error</returns>
        public abstract bool LoadConfig();

        /// <summary>
        /// dump device config to file
        /// </summary>
        /// <param name="sAddress">Address device</param>
        /// <param name="sFile">Output file</param>
        /// <param name="sComSettings">Com settings</param>
        public abstract void DoReadConfig(string sAddress, string sFile, string sComSettings);

        /// <summary>
        /// write config from file to device
        /// </summary>
        /// <param name="sFile">Input file</param>
        /// <param name="sComSettings">Com settings</param>
        public abstract void DoWriteConfig(string sAddress, string sFile, string sComSettings);




    }
}

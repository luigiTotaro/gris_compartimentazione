
//
// Telefin
//
// Progetto:	PZAutoConfigurator
// File:		SerailWrapper.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		31.05.2007
//

using System;
using System.IO;
using System.IO.Ports;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Reflection;

namespace Telefin.AutoConfigurator.Class
{
    class SerialWrapper
    {
        #region Token

        private const string TOK_ERROR_COM_OBJECT_NULL      = @"*** L'oggetto COM non � stato inizializzato ***";
        private const string TOK_ERROR_ECHO_COMPARE         = @"*** Errore in controllo buffer di echo ***";

        private const string TOK_SEND_COMMAND               = @"           SendCommand: ";
        private const string TOK_RET_COMMAND                = @"            RetCommand: ";
        private const string TOK_READ_COMMAND_BUFFER        = @"     ReadCommandBuffer: ";
        private const string TOK_SEND_ANSWER_READBLOCK      = @"  SendAnswer4ReadBlock: ";
        private const string TOK_SEND_BLOCK                 = @"             SendBlock: ";
        private const string TOK_READ_ANSWER_WRITEBLOCK     = @" ReadAnswer4WriteBlock: ";

        private const char   TOK_SOH                        = (char)0x01;
        private const char   TOK_STX                        = (char)0x02;
        private const char   TOK_ETX                        = (char)0x03;
        private const char   TOK_EOT                        = (char)0x04;
        private const char   TOK_ETB                        = (char)0x17;
        private const char   TOK_ACK                        = (char)0x06;
        private const char   TOK_NACK                       = (char)0x15;

        private const int    TOK_COM_TIMEOUT                = 4000;

        private const int    TOK_COMMAND_LEN                = 8;
        private const int    TOK_RET_COMMAND_LEN            = 7;
        private const int    TOK_DATA_BLOCK_PREFIX_LEN      = 5;

        private const int    TOK_ANSWER_DATA_BLOCK_LEN      = 7;

        private const int    TOK_MAX_RETRY                  = 3;

        private const char   TOK_ANSWER_ACK_CODE            = (char)'0';
        private const char   TOK_ANSWER_NACK_CODE           = (char)'5';

        #endregion

        #region Private Members

        private SerialPort  _oSerialPort    = null;
        private string      _sLastError     = string.Empty;
        private byte []     _btTmpBuffer    = new byte[512];
        private byte[]      _btReadBuffer   = new byte[2048];
        private int         _nReadBufferIdx = 0;
        private bool        _bEchoEnable     = false;
        private bool        _bIsCom232      = false;

        #endregion

        #region Accessor

        /// <summary>
        /// Has echo enable
        /// </summary>
        public bool EchoEnable
        {
            get
            {
                return (_bEchoEnable);
            }
        }

        /// <summary>
        /// is a 232 com type
        /// </summary>
        public bool IsCom232
        {
            get
            {
                return (_bIsCom232);
            }
        }

        /// <summary>
        /// Return last error string
        /// </summary>
        public string LastError
        {
            get
            {
                return (_sLastError);
            }
        }

        /// <summary>
        /// Port is open
        /// </summary>
        public bool IsOpen
        {
            get
            {
                return (_oSerialPort != null ? _oSerialPort.IsOpen : false);
            }
        }

        public byte[] ReadBuffer
        {
            get
            {
                return(_btReadBuffer);
            }
        }

        public int ReadBufferLen
        {
            get
            {
                return(_nReadBufferIdx);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Init com object
        /// </summary>
        /// <param name="nIdCom">ID com port(0,1,2,3,...)</param>
        /// <param name="nBaud">Baud rade</param>
        /// <param name="sParity">Parity (N,E,O,M,S)</param>
        /// <param name="nDataLen">Data len</param>
        /// <param name="nDataStop">Data stop bit</param>
        /// <param name="nTimeout">Timeout for read/write</param>
        public void InitComPort(int nIdCom, int nBaud, string sParity, int nDataLen, int nDataStop, int nTimeout, bool bIsCom232, bool bHasEcho)
        {
            try
            {
                Parity nParity = Parity.None;

                switch(sParity)
                {
                    case "N":
                        nParity = Parity.None;
                        break;
                    case "E":
                        nParity = Parity.Even;
                        break;
                    case "O":
                        nParity = Parity.Odd;
                        break;
                    case "M":
                        nParity = Parity.Mark;
                        break;
                    case "S":
                        nParity = Parity.Space;
                        break;
                }

                StopBits nStopBits = StopBits.None;

                switch (nDataStop)
                {
                    case 0:
                        nStopBits = StopBits.One;
                        break;

                    case 1:
                        nStopBits = StopBits.OnePointFive;
                        break;

                    case 2:
                        nStopBits = StopBits.Two;
                        break;
                }

                _oSerialPort = new SerialPort("COM" + nIdCom.ToString(), 
                                              nBaud, 
                                              nParity, 
                                              nDataLen,
                                              nStopBits);

                _oSerialPort.WriteTimeout   = TOK_COM_TIMEOUT; // nTimeout;
                _oSerialPort.ReadTimeout    = TOK_COM_TIMEOUT; // nTimeout;

                _oSerialPort.DiscardNull = false;

                // set handshake
                //
                _bEchoEnable = bHasEcho;
                _bIsCom232   = bIsCom232;
                _oSerialPort.Handshake = bIsCom232 ? Handshake.RequestToSend : Handshake.None;
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Open com port
        /// </summary>
        public void OpenComPort()
        {
            try
            {
                if(_oSerialPort == null)
                    throw new Exception(TOK_ERROR_COM_OBJECT_NULL);

                _oSerialPort.Open();

                _oSerialPort.DiscardInBuffer();
                _oSerialPort.DiscardOutBuffer();
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Close display port
        /// </summary>
        public void Close()
        {
            try
            {
                if (_oSerialPort == null)
                    throw new Exception(TOK_ERROR_COM_OBJECT_NULL);

                _oSerialPort.DiscardInBuffer();
                _oSerialPort.DiscardOutBuffer();
                
                //_oSerialPort.DtrEnable = false;
                //_oSerialPort.RtsEnable = false;

                _oSerialPort.Close();
                _oSerialPort.Dispose();
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Calculate CRC
        /// </summary>
        /// <param name="sBuffer">Input buffer</param>
        /// <param name="nLen">Len of input buffer</param>
        /// <returns>Crc string</returns>
        public byte [] CalculateCheckSum(byte [] btBuffer, int nLen)
        {
            try
            {
                int nCrc    = 0; // Valore di appoggio per il calcolo del checksum                
                int nCrcTmp = 0; // Valore di appoggio per il calcolo del checksum

                if (btBuffer != null && nLen > 0)
                {
                    // Calcolo il checksum CRC16 lungo tutta la lunghezza del buffer
                    //
                    for (int nCount = 0; nCount < nLen; nCount++)
                    {
                        nCrcTmp = (nCrc << 8);
                        nCrcTmp ^= btBuffer[nCount];
                        if ((nCrc & 0x8000) != 0) nCrcTmp ^= 0x8303;
                        if ((nCrc & 0x4000) != 0) nCrcTmp ^= 0x8183;
                        if ((nCrc & 0x2000) != 0) nCrcTmp ^= 0x80c3;
                        if ((nCrc & 0x1000) != 0) nCrcTmp ^= 0x8063;
                        if ((nCrc & 0x0800) != 0) nCrcTmp ^= 0x8033;
                        if ((nCrc & 0x0400) != 0) nCrcTmp ^= 0x801b;
                        if ((nCrc & 0x0200) != 0) nCrcTmp ^= 0x800f;
                        if ((nCrc & 0x0100) != 0) nCrcTmp ^= 0x8005;

                        nCrc = nCrcTmp;
                    }

                    byte [] cTmp = new byte[2];

                    cTmp[0] = (byte)(nCrc >> 8);
                    cTmp[1] = (byte)(nCrc);

                    return (cTmp);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }

            return(null);
        }

        /// <summary>
        /// Send command to serial port
        /// </summary>
        /// <param name="sCommnad">command to send</param>
        /// <param name="sAddress">address to send</param>
        public bool SendCommand(string sCommand, string sAddress)
        {
            try
            {
                if (_oSerialPort == null)
                    throw new Exception(TOK_ERROR_COM_OBJECT_NULL);

                byte [] btCmd = new byte[8];
                string sTmp   = string.Empty;

                // build command
                //
                btCmd[0] = (byte)TOK_SOH;
                btCmd[1] = (byte)sAddress[0];
                btCmd[2] = (byte)sAddress[1];
                btCmd[3] = (byte)TOK_STX;
                btCmd[4] = (byte)sCommand[0];
                btCmd[5] = (byte)TOK_ETX;

                byte[] cTmp = CalculateCheckSum(btCmd, 6);

                btCmd[6] = (byte)cTmp[0];
                btCmd[7] = (byte)cTmp[1];

                _oSerialPort.Write(btCmd, 0, TOK_COMMAND_LEN);

                if (_bEchoEnable)
                {
                    Thread.Sleep(1);
                    if (!DiscardEchoBuffer(btCmd, TOK_COMMAND_LEN))
                        throw new Exception(TOK_ERROR_ECHO_COMPARE);
                }

                #if DEBUG
                    Common.DumpLogMessage(TOK_SEND_COMMAND + Common.Byte2HexString(btCmd));
                #endif

                for(int nIdx = 0; nIdx < TOK_RET_COMMAND_LEN; nIdx++)
                    _btTmpBuffer[nIdx] = (byte)_oSerialPort.ReadByte();

                #if DEBUG
                    Common.DumpLogMessage(TOK_RET_COMMAND + Common.Byte2HexString(_btTmpBuffer, TOK_RET_COMMAND_LEN));
                #endif

                // clear read buffer
                //
                _btReadBuffer.Initialize();
                _nReadBufferIdx = 0;

                // recalculate check sum
                //
                cTmp = CalculateCheckSum(_btTmpBuffer, TOK_RET_COMMAND_LEN - 2);

                return (_btTmpBuffer[4] == TOK_ACK && 
                        cTmp[0]         == _btTmpBuffer[TOK_RET_COMMAND_LEN - 2] &&
                        cTmp[1]         == _btTmpBuffer[TOK_RET_COMMAND_LEN - 1] ? true : false);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Read command buffer
        /// </summary>
        /// <param name="sAddress">address to send answer</param>
        /// <param name="bIsEnd">flag for end buffer</param>
        /// <returns>true ok, false error</returns>
        public bool ReadCommandBuffer(string sAddress, ref bool bIsEnd)
        {
            try
            {
                int  nStep = 0;
                bool bRet  = false;

                do
                {
                    for (int nIdx = 0; nIdx < TOK_DATA_BLOCK_PREFIX_LEN; nIdx++)
                        _btTmpBuffer[nIdx] = (byte)_oSerialPort.ReadByte();

                    if (_btTmpBuffer[0] == TOK_STX)
                    {
                        string sTmp = string.Empty;
                        sTmp += (char)_btTmpBuffer[3];
                        sTmp += (char)_btTmpBuffer[4];
                        int nLen = int.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);

                        for (int nIdx = TOK_DATA_BLOCK_PREFIX_LEN; nIdx < TOK_DATA_BLOCK_PREFIX_LEN + nLen + 3; nIdx++)
                            _btTmpBuffer[nIdx] = (byte)_oSerialPort.ReadByte();

                        #if DEBUG
                            Common.DumpLogMessage(TOK_READ_COMMAND_BUFFER + Common.Byte2HexString(_btTmpBuffer, TOK_DATA_BLOCK_PREFIX_LEN + nLen + 3));
                        #endif

                        bIsEnd = _btTmpBuffer[TOK_DATA_BLOCK_PREFIX_LEN + nLen] == TOK_ETX ? true : false;

                        byte[] cTmp = CalculateCheckSum(_btTmpBuffer, TOK_DATA_BLOCK_PREFIX_LEN + nLen + 1);

                        bRet = cTmp[0] == _btTmpBuffer[TOK_DATA_BLOCK_PREFIX_LEN + nLen + 1] &&
                               cTmp[1] == _btTmpBuffer[TOK_DATA_BLOCK_PREFIX_LEN + nLen + 2] ? true : false;

                        // send ack - nack for recived block
                        //
                        SendAnswer4ReadBlock(sAddress, bRet);

                        // copy read buffer
                        //
                        if(bRet == true)
                        {
                            for(int nCount = 0; nCount < nLen; nCount++)
                                _btReadBuffer[_nReadBufferIdx + nCount] = _btTmpBuffer[nCount + TOK_DATA_BLOCK_PREFIX_LEN];

                            _nReadBufferIdx += nLen;
                        }
                    }

                    if(++nStep > TOK_MAX_RETRY)
                        break;

                }while(bRet == false);

                return(bRet);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Close sent command
        /// </summary>
        public void CloseSentCommand()
        {
            try
            {
                if (_oSerialPort == null)
                    throw new Exception(TOK_ERROR_COM_OBJECT_NULL);

                byte [] btCmd = new byte[3];

                btCmd[0] = (byte)TOK_EOT;
                btCmd[1] = (byte)TOK_EOT;
                btCmd[2] = (byte)TOK_EOT;

                _oSerialPort.Write(btCmd, 0, 3);

                if (_bEchoEnable)
                {
                    Thread.Sleep(1);
                    if (!DiscardEchoBuffer(btCmd, 3))
                        throw new Exception(TOK_ERROR_ECHO_COMPARE);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Send answer for read block
        /// </summary>
        /// <param name="sAddress">address to send</param>
        /// <param name="bOk">answer type (Ack, Nack)</param>
        /// <returns>tru ok, false error</returns>
        private bool SendAnswer4ReadBlock(string sAddress, bool bOk)
        {
            try
            {
                byte[] btCmd = new byte[7];
                string sTmp = string.Empty;

                // build answer
                //
                btCmd[0] = (byte)TOK_SOH;
                btCmd[1] = (byte)sAddress[0];
                btCmd[2] = (byte)sAddress[1];
                btCmd[3] = (byte)(bOk == true ? (byte)TOK_ANSWER_ACK_CODE : (byte)TOK_ANSWER_NACK_CODE);
                btCmd[4] = (byte)(bOk == true ? (byte)TOK_ACK             : (byte)TOK_NACK);

                byte[] cTmp = CalculateCheckSum(btCmd, 5);

                btCmd[5] = (byte)cTmp[0];
                btCmd[6] = (byte)cTmp[1];

                _oSerialPort.Write(btCmd, 0, TOK_ANSWER_DATA_BLOCK_LEN);

                if (_bEchoEnable)
                {
                    Thread.Sleep(1);
                    if (!DiscardEchoBuffer(btCmd, TOK_ANSWER_DATA_BLOCK_LEN))
                        throw new Exception(TOK_ERROR_ECHO_COMPARE);
                }

                #if DEBUG
                    Common.DumpLogMessage(TOK_SEND_ANSWER_READBLOCK + Common.Byte2HexString(btCmd, TOK_ANSWER_DATA_BLOCK_LEN));
                #endif

                return(true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Send buffer block
        /// </summary>
        /// <param name="btBuffer">buffer to send</param>
        /// <param name="nBlock">block number</param>
        /// <param name="bIsEnd">last block</param>
        public bool SendBufferBlock(Byte [] btBuffer, int nBlock, bool bIsEnd)
        {
            try
            {
                if (_oSerialPort == null)
                    throw new Exception(TOK_ERROR_COM_OBJECT_NULL);

                byte [] btAnswer    = new byte[TOK_ANSWER_DATA_BLOCK_LEN];
                byte [] btBlock     = new byte[8 + btBuffer.Length];
                string sBlock       = nBlock.ToString("X2");
                string sLen         = btBuffer.Length.ToString("X2");
                int    nCurIdx      = 0;

                // build block buffer
                //
                btBlock[0] = (byte)TOK_STX;
                btBlock[1] = (byte)sBlock[0];
                btBlock[2] = (byte)sBlock[1];
                btBlock[3] = (byte)sLen[0];
                btBlock[4] = (byte)sLen[1];

                nCurIdx = 5;
                for(int nIdx = 0; nIdx < btBuffer.Length; nIdx++)
                    btBlock[nCurIdx++] = btBuffer[nIdx];

                btBlock[nCurIdx++] = bIsEnd == true ? (Byte)TOK_ETX : (Byte)TOK_ETB;

                byte[] cTmp = CalculateCheckSum(btBlock, nCurIdx);

                btBlock[nCurIdx++] = (byte)cTmp[0];
                btBlock[nCurIdx++] = (byte)cTmp[1];

                for(int nIdx = 0; nIdx < btBlock.Length; nIdx++)
                    _oSerialPort.Write(btBlock, nIdx, 1);

                #if DEBUG
                    Common.DumpLogMessage(TOK_SEND_BLOCK + Common.Byte2HexString(btBlock));
                #endif

                if (_bEchoEnable)
                {
                    Thread.Sleep(1);
                    if (!DiscardEchoBuffer(btBlock, btBlock.Length))
                        throw new Exception(TOK_ERROR_ECHO_COMPARE);
                }


                for (int nIdx = 0; nIdx < TOK_ANSWER_DATA_BLOCK_LEN; nIdx++)
                    btAnswer[nIdx] = (byte)_oSerialPort.ReadByte();

                #if DEBUG
                    Common.DumpLogMessage(TOK_READ_ANSWER_WRITEBLOCK + Common.Byte2HexString(btAnswer, TOK_ANSWER_DATA_BLOCK_LEN));
                #endif

                cTmp = CalculateCheckSum(btAnswer, TOK_ANSWER_DATA_BLOCK_LEN - 2);

                return (btAnswer[4] == TOK_ACK &&
                        cTmp[0]     == btAnswer[TOK_ANSWER_DATA_BLOCK_LEN - 2] &&
                        cTmp[1]     == btAnswer[TOK_ANSWER_DATA_BLOCK_LEN - 1] ? true : false);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// Discard com echo buffer
        /// </summary>
        /// <param name="btCmp">byte to compare</param>
        /// <param name="nLen">len of compare buffer</param>
        /// <returns>true the echo was right, false error</returns>
        private bool DiscardEchoBuffer(Byte [] btCmp, int nLen)
        {
            try
            {
                Byte btTmp;
                Byte[] bufferEcho = new Byte[nLen];
                for (int nCount = 0; nCount < nLen; nCount++)
                    bufferEcho[nCount] = (byte)_oSerialPort.ReadByte();

                #if DEBUG
                    Common.DumpLogMessage(">Buffer Echo = " + Common.Byte2HexString(bufferEcho));
                #endif

                for (int nCount = 0; nCount < nLen; nCount++)
                {
//                    btTmp = (Byte)_oSerialPort.ReadByte();
                    btTmp = bufferEcho[nCount];
                    if (btCmp[nCount] != btTmp)
                        return(false);
                }

                return(true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        #endregion

    }
}

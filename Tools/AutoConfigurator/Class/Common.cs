
//
// Telefin
//
// Progetto:	PZAutoConfigurator
// File:		Common.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		24.05.2007
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Runtime.InteropServices;

namespace Telefin.AutoConfigurator.Class
{
    class Common
    {
        #region Token

        public const string     TOK_ERROR_PREFIX    = @"Errore: [";
        public const string     TOK_ERROR_SUFFIX    = @"] :";
        public const string     TOK_PGM_TITLE       = @"PZAutoConfigurator";
        public const string     TOK_PGM_LOG_FILE    = @"PZAutoConfigurator.log";
        public const int        TOK_MAX_LEN_TRACE   = 1024000;

        #endregion

        #region Private Members

        private static int [] _nBitValues = new int[]{1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536};

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Get program path
        /// </summary>
        /// <returns>program path</returns>
        public static string GetProgramPath()
        {
            return(System.Environment.CurrentDirectory);
        }

        /// <summary>
        ///  Dump message on log file
        /// </summary>
        /// <param name="sError">error message base</param>
        /// <param name="e">exception</param>
        public static void DumpLogMessage(string sError, Exception e)
        {
            DumpLogMessage(string.Format("Message: {0}, Exception Type: {1}, Exception Message: {2}, Exception Stack Trace: {3}", sError, e.ToString(), e.Message, e.StackTrace));
        }

        /// <summary>
        ///  Dump message on log file
        /// </summary>
        /// <param name="e">exception</param>
        public static void DumpLogMessage(Exception e)
        {
            DumpLogMessage(string.Format("Exception Type: {0}, Exception Message: {1}, Exception Stack Trace: {2}", e.ToString(), e.Message, e.StackTrace));
        }

        /// </summary>
        ///  Dump message on log file
        /// </summary>
        /// <param name="sError">error message base</param>
        public static void DumpLogMessage(string sMessage)
        {
            DumpLogMessage(sMessage, false);
        }

        /// </summary>
        ///  Dump message on log file
        /// </summary>
        /// <param name="sError">error message base</param>
        /// <param name="bDateTime">dump date time</param>
        public static void DumpLogMessage(string sMessage, bool bDateTime)
        {
            try
            {
                string sFileName = GetProgramPath() + @"\" + TOK_PGM_LOG_FILE;
                StringBuilder sSBLine = new StringBuilder();

                if (bDateTime)
                {
                    sSBLine.Append(DateTime.Now.ToString());
                    sSBLine.Append(" - ");
                    sSBLine.Append(sMessage);
                }
                else
                    sSBLine.Append(sMessage);

                using (StreamWriter SWrite = File.AppendText(sFileName))
                {
                    SWrite.WriteLine(sSBLine.ToString());
                    Console.WriteLine(sSBLine.ToString());
                }
            }
            catch (Exception)
            {
            }
        }

        /// </summary>
        ///  Dump inline message on log file
        /// </summary>
        /// <param name="sError">message to dump</param>
        public static void DumpLogInlineMessage(string sMessage)
		{
            try
            {
                string          sFileName = GetProgramPath() + @"\" + TOK_PGM_LOG_FILE;
                StringBuilder   sSBLine   = new StringBuilder();

                using (StreamWriter SWrite = File.AppendText(sFileName))
                {
                    SWrite.Write(sMessage);
                    Console.Write(sMessage);
                }
            }
            catch (Exception)
            {
            }
        }

        /// </summary>
        ///  Dump new line on log file
        /// </summary>
        public static void DumpLogNewLine()
		{
            try
            {
                string          sFileName = GetProgramPath() + @"\" + TOK_PGM_LOG_FILE;
                StringBuilder   sSBLine   = new StringBuilder();

                using (StreamWriter SWrite = File.AppendText(sFileName))
                {
                    SWrite.WriteLine("");
                    Console.WriteLine("");
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// dump byte to binary string 
        /// </summary>
        /// <param name="data">byte to dump</param>
        /// <returns>dump string</returns>
        public static string Byte2BinaryString(byte data)
        {
            int     num     = (int)((ushort)data);
            string  result  = "";
            int     pivot   = 128;

            while (true)
            {
                if (num >= pivot)
                {
                    result = String.Concat(result, "1");
                    num -= pivot;
                }
                else
                {
                    result = String.Concat(result, "0");
                }
                if (pivot == 1)
                {
                    break;
                }
                pivot = (pivot / 2);
            }

            return result;
        }

        /// <summary>
        /// dump byte to binary string 
        /// </summary>
        /// <param name="sData">string to dump</param>
        /// <returns>dump string</returns>
        public static string Byte2BinaryString(string  sData)
        {
            string sTmp = string.Empty;

            byte [] btTmp = ASCIIEncoding.ASCII.GetBytes(sData);

            for(int nCount = 0; nCount < btTmp.Length; nCount++)
                sTmp += Byte2BinaryString(btTmp[nCount]);

            return(sTmp);
        }

        /// <summary>
        /// dump byte to string (Hex)
        /// </summary>
        /// <param name="sData">string to dump</param>
        /// <returns>dump string</returns>
        public static string String2HexString(string  sData)
        {
            StringBuilder sbTmp = new StringBuilder();

            byte [] btTmp = ASCIIEncoding.ASCII.GetBytes(sData);

            for(int nCount = 0; nCount < btTmp.Length; nCount++)
            {
                sbTmp.Append(btTmp[nCount].ToString("X2"));
                sbTmp.Append(" ");
            }

            return (sbTmp.ToString());
        }

        /// <summary>
        /// dump byte to string (Hex)
        /// </summary>
        /// <param name="sData">string to dump</param>
        /// <returns>dump string</returns>
        public static string Byte2HexString(byte [] btTmp)
        {
            StringBuilder sbTmp = new StringBuilder();

            for (int nCount = 0; nCount < btTmp.Length; nCount++)
            {
                sbTmp.Append(btTmp[nCount].ToString("X2"));
                sbTmp.Append(" ");
            }

            return (sbTmp.ToString());
        }

        /// <summary>
        /// dump byte to string (Hex)
        /// </summary>
        /// <param name="sData">string to dump</param>
        /// <returns>dump string</returns>
        public static string Byte2HexString(byte[] btTmp, int nLen)
        {
            StringBuilder sbTmp = new StringBuilder();

            for (int nCount = 0; nCount < nLen; nCount++)
            {
                sbTmp.Append(btTmp[nCount].ToString("X2"));
                sbTmp.Append(" ");
            }

            return (sbTmp.ToString());
        }

        /// <summary>
        /// Convert byte to string
        /// </summary>
        /// <param name="btTmp">byte to convert</param>
        /// <param name="nLen">number of bytes to convert</param>
        /// <returns>string of input bytes</returns>
        public static string Byte2String(byte[] btTmp, int nLen)
        {
            ASCIIEncoding oConv = new ASCIIEncoding();
            return(oConv.GetString(btTmp, 0, nLen));
        }

        /// <summary>
        /// Convert alfa rappresentation to byte
        /// </summary>
        /// <param name="btTmp">input bytes</param>
        /// <returns>byte convert</returns>
        public static Byte ConvertAlfa2Byte(byte [] btTmp)
        {
            string sTmp = string.Empty;
            sTmp += (char)btTmp[0];
            sTmp += (char)btTmp[1];
            Byte btRet = Byte.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);

            return(btRet);
        }

        /// <summary>
        /// Convert alfa rappresentation to char array
        /// </summary>
        /// <param name="btTmp">input bytes</param>
        /// <returns>char array converted</returns>
        public static char[] ConvertAlfa2CharArray(byte[] btTmp, int nLen)
        {
            char [] cRet = new char[nLen];

            string sTmp = string.Empty;

            for (int nCount = 0, nIdx = 0; nCount < nLen * 2; nCount += 2)
            {
                sTmp  = string.Empty;
                sTmp += (char)btTmp[nCount];
                sTmp += (char)btTmp[nCount + 1];
             
                cRet[nIdx++] = (char)byte.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);
            }

            return (cRet);
        }

        /// <summary>
        /// Convert alfa rappresentation to signed byte
        /// </summary>
        /// <param name="btTmp">input bytes</param>
        /// <returns>byte convert</returns>
        public static SByte ConvertAlfa2SByte(byte [] btTmp)
        {
            string sTmp = string.Empty;
            sTmp += (char)btTmp[0];
            sTmp += (char)btTmp[1];
            SByte btRet = SByte.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);

            return(btRet);
        }

        /// <summary>
        /// Convert alfa rappresentation to unsigned int 16
        /// </summary>
        /// <param name="btTmp">input bytes</param>
        /// <returns>UInt16 convert</returns>
        public static UInt16 ConvertAlfa2UInt16(byte[] btTmp)
        {
            string sTmp = string.Empty;
            sTmp += (char)btTmp[0];
            sTmp += (char)btTmp[1];
            sTmp += (char)btTmp[2];
            sTmp += (char)btTmp[3];
            UInt16 nRet = UInt16.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (nRet);
        }

        /// <summary>
        /// Convert alfa rappresentation to int 16
        /// </summary>
        /// <param name="btTmp">input bytes</param>
        /// <returns>Int16 convert</returns>
        public static Int16 ConvertAlfa2Int16(byte[] btTmp)
        {
            string sTmp = string.Empty;
            sTmp += (char)btTmp[0];
            sTmp += (char)btTmp[1];
            sTmp += (char)btTmp[2];
            sTmp += (char)btTmp[3];
            Int16 nRet = Int16.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (nRet);
        }

        /// <summary>
        /// Convert alfa rappresentation to unsigned int 32
        /// </summary>
        /// <param name="btTmp">input bytes</param>
        /// <returns>UInt32 convert</returns>
        public static UInt32 ConvertAlfa2UInt32(byte[] btTmp)
        {
            string sTmp = string.Empty;
            sTmp += (char)btTmp[0];
            sTmp += (char)btTmp[1];
            sTmp += (char)btTmp[2];
            sTmp += (char)btTmp[3];
            sTmp += (char)btTmp[4];
            sTmp += (char)btTmp[5];
            sTmp += (char)btTmp[6];
            sTmp += (char)btTmp[7];
            UInt32 nRet = UInt32.Parse(sTmp, System.Globalization.NumberStyles.AllowHexSpecifier);

            return (nRet);
        }

        /// <summary>
        /// Convert Byte to Alfa
        /// </summary>
        /// <param name="btInput">input byte</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte [] ConvertByte2Alfa(Byte btInput)
        {
            Byte [] btRet = new Byte[2];
            string sTmp = btInput.ToString("X2");
            btRet[0] = (Byte)sTmp[0];
            btRet[1] = (Byte)sTmp[1];
            
            return (btRet);
        }

        /// <summary>
        /// Convert Signed Byte to Alfa
        /// </summary>
        /// <param name="btInput">input signed byte</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte[] ConvertSByte2Alfa(SByte btInput)
        {
            Byte [] btRet = new Byte[2];
            string sTmp = btInput.ToString("X2");
            btRet[0] = (Byte)sTmp[0];
            btRet[1] = (Byte)sTmp[1];

            return (btRet);
        }
        
        
        /// <summary>
        /// Convert unsigned int 16 to Alfa
        /// </summary>
        /// <param name="btInput">input value</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte[] ConvertUInt162Alfa(UInt16 btInput)
        {
            Byte[] btRet = new Byte[4];
            string sTmp = btInput.ToString("X4");
            btRet[0] = (Byte)sTmp[0];
            btRet[1] = (Byte)sTmp[1];
            btRet[2] = (Byte)sTmp[2];
            btRet[3] = (Byte)sTmp[3];

            return (btRet);
        }


        /// <summary>
        /// Convert int 16 to Alfa
        /// </summary>
        /// <param name="btInput">input value</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte[] ConvertInt162Alfa(Int16 btInput)
        {
            Byte[] btRet = new Byte[4];
            string sTmp = btInput.ToString("X4");
            btRet[0] = (Byte)sTmp[0];
            btRet[1] = (Byte)sTmp[1];
            btRet[2] = (Byte)sTmp[2];
            btRet[3] = (Byte)sTmp[3];

            return (btRet);
        }

        /// <summary>
        /// Convert unsigned int 32 to Alfa
        /// </summary>
        /// <param name="btInput">input value</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte[] ConvertUInt322Alfa(UInt32 btInput)
        {
            Byte[] btRet = new Byte[8];
            string sTmp = btInput.ToString("X8");
            btRet[0] = (Byte)sTmp[0];
            btRet[1] = (Byte)sTmp[1];
            btRet[2] = (Byte)sTmp[2];
            btRet[3] = (Byte)sTmp[3];
            btRet[4] = (Byte)sTmp[4];
            btRet[5] = (Byte)sTmp[5];
            btRet[6] = (Byte)sTmp[6];
            btRet[7] = (Byte)sTmp[7];

            return (btRet);
        }

        /// <summary>
        /// Convert char array to Alfa
        /// </summary>
        /// <param name="cTmp">input char array</param>
        /// <param name="nLen">len of input char array</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte[] ConvertCharArray2Alfa(char[] cTmp, int nLen)
        {
            Byte[] btRet = new Byte[nLen * 2];
            string sTmp  = string.Empty;

            for (int nCount = 0, nIdx = 0; nCount < nLen; nCount++)
            {
                sTmp = ((Byte)cTmp[nCount]).ToString("X2");
                btRet[nIdx++] = (Byte)sTmp[0];
                btRet[nIdx++] = (Byte)sTmp[1];
            }            

            return (btRet);
        }

        /// <summary>
        /// Convert string to Alfa
        /// </summary>
        /// <param name="cTmp">input string</param>
        /// <returns>Alfa rappresentation</returns>
        public static Byte[] ConvertString2Alfa(string sInput)
        {
            Byte[] btRet = new Byte[sInput.Length * 2];
            string sTmp  = string.Empty;

            for (int nCount = 0, nIdx = 0; nCount < sInput.Length; nCount++)
            {
                sTmp = ((Byte)sInput[nCount]).ToString("X2");
                btRet[nIdx++] = (Byte)sTmp[0];
                btRet[nIdx++] = (Byte)sTmp[1];
            }            

            return (btRet);
        }

        /// <summary>
        /// Get string of bit position
        /// </summary>
        /// <param name="nValue">Input value</param>
        /// <returns>string of bits</returns>
        public static string GetBitPositionString(int nValue)
        {
            if(nValue == 0)
                return("0");

            string sTmp = string.Empty;

            for(int nIdx = 0; nIdx < _nBitValues.Length; nIdx++)
            {
                if ((nValue & _nBitValues[nIdx]) == _nBitValues[nIdx])
                {
                    if(sTmp.Length > 0)
                        sTmp += ",";

                    sTmp += (nIdx + 1).ToString();
                }
            }       

            return(sTmp);
        }

        /// <summary>
        /// Get string of bit position
        /// </summary>
        /// <param name="nValue">Input value</param>
        /// <returns>string of bits</returns>
        public static UInt16 GetValueBitPositionString(string [] sArray)
        {
            UInt16 nValue = 0;

            if(sArray.Length == 1 && sArray[0] == "0")
                return(nValue);

            for (int nIdx = 0; nIdx < sArray.Length; nIdx++)
                nValue |= (UInt16)_nBitValues[Convert.ToInt16(sArray[nIdx]) - 1];

            return (nValue);
        }


        /// <summary>
        /// Read string value 
        /// </summary>
        /// <param name="Section">section name</param>
        /// <param name="Key">key name</param>
        /// <returns>key value</returns>
        public static string ReadKeyIniValue(string Section, string Key, string sFile)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, sFile);
            return temp.ToString();
        }

        #endregion

        #region PInvoke

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        
        #endregion

    }
}

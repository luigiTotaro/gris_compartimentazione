
//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		AutoConfigurator.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		17.05.2007
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using System.Windows.Forms;

using Telefin.AutoConfigurator.Class;

namespace Telefin.AutoConfigurator.Program
{
    class Program
    {
        #region Token

        private const string    TOK_READ        = @"R";
        private const string    TOK_WRITE       = @"W";

        #endregion

        #region Private Members

        private static ManagerBase  _oManager = null;

        #endregion

        #region Accessor

        public ManagerBase Manager
        {
            get
            {
                return(_oManager);
            }
            set
            {
                _oManager = value;
            }
        }

        #endregion

        // args: null autoconfigurazione      
        //       READ                       READ        Legge configurazione da dispositivo e genera file 
        //                                  WRITE       Scrive configurazione da file e scrive in EEProm dispositivo 
        //       FILE                                   Nome del file contenente la configurazione
        //       Addr                       00          indirizzo periferica
        //       2,9600,N,8,1,T,T           2           porta seriale
        //                                  9600        baud rate
        //                                  N(E)(O)     parit� N=nessuna, E Even, O Odd
        //                                  8           data
        //                                  1           stop bit 0=nessuno, 1 = 1 stop bit, 1.5, 2
        //                                  T           T=232, F=485/422
        //                                  T           T=Echo on, F= Echo Off
        //		gestione dei servizi		Y			Y=yes, N=no
        //      tipo periferica             A           A=Ampli, PZ=Pannello Zone
        //
		[STAThread]
        static void Main(string[] args)
        {
            try
            {
                Common.DumpLogNewLine();
                Common.DumpLogMessage(string.Format("*** AutoConfig Versione: {0}, Data: {1:dd-MM-yyyy HH:mm} ***", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DateTime.Now));

//                _oManager = new AmpliManager();

                // auto config
                //
                if (args.Length == 0)
                {
                    // load config
                    //
                    bool loadingOk = false;
                    _oManager = new PzManager();
                    loadingOk = _oManager.LoadConfig();
                    if (loadingOk)
                        _oManager.DoAutoConfig();

                    _oManager = new AmpliManager();
                    loadingOk = _oManager.LoadConfig();
                    if (loadingOk)
                        _oManager.DoAutoConfig();

                }
                else
                {
                    // Parametri lettura cfg Amplificatore:R ".\cfgAmpli_1.txt" 05 3,9600,N,8,1,F,T N A
                    // Parametri lettura cfg Pannello Zone:R ".\cfgPZ.txt" 04 3,9600,N,8,1,F,T N PZ
                    _oManager = new PzManager();
                    if (args.Length != 6)
    					_oManager.DumpProgramArgs();
    				else
    				{
    					if ( String.Compare(args[4], "Y", StringComparison.OrdinalIgnoreCase) == 0 && !_oManager.StopSTLCServices() )
    					{
    						Common.DumpLogMessage(PzManager.TOK_ERROR_SERVICE_STOP_TEST);
    						return;
    					}

                        if (String.Compare(args[5], "PZ", StringComparison.OrdinalIgnoreCase) == 0)
                        {

                            switch (args[0])
                            {
                                case Program.TOK_READ:

                                    _oManager.DoReadConfig(args[2], args[1], args[3]);
                                    break;

                                case Program.TOK_WRITE:
                                    _oManager.DoWriteConfig(args[2], args[1], args[3]);
                                    break;

                                default:
                                    _oManager.DumpProgramArgs();
                                    break;
                            }
                        }
                        else if (String.Compare(args[5], "A", StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            _oManager = new AmpliManager();
                            switch (args[0])
                            {
                                case Program.TOK_READ:

                                    _oManager.DoReadConfig(args[2], args[1], args[3]);
                                    break;

                                case Program.TOK_WRITE:
                                    _oManager.DoWriteConfig(args[2], args[1], args[3]);
                                    break;

                                default:
                                    _oManager.DumpProgramArgs();
                                    break;
                            }
                        }
                        else {
                            Common.DumpLogMessage(@"ERRORE PARAMETRI");
                            _oManager.DumpProgramArgs();
                        }

    					if ( String.Compare(args[4], "Y", StringComparison.OrdinalIgnoreCase) == 0 && !_oManager.StartSTLCServices() )
    					{
                            Common.DumpLogMessage(PzManager.TOK_ERROR_SERVICE_START_TEST);
                        }
    				}
					
                }
            }
            catch (Exception e)
            {
                Common.DumpLogMessage(e);

				if ( args.Length == 5 && String.Compare(args[4], "Y", StringComparison.OrdinalIgnoreCase) == 0 && !_oManager.StartSTLCServices() )
				{
					Common.DumpLogMessage(PzManager.TOK_ERROR_SERVICE_START_TEST);
				}
            }        
        }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using DeviceTypeEditor.Properties;

namespace DeviceTypeEditor
{
    public partial class MainForm : Form
    {
        private enum GridColumns
        {
            DeviceTypeID,
            SystemID,
            VendorID,
            DeviceTypeDescription,
            WSUrlPattern,
            Order,
            TechnologyID,
            Obsolete,
            DefaultName,
            PortType,
            SnmpCommunity,
            ImageName,
            GlobalOrder
        }

        private int defaultSystemID = -1;
        private int defaultTechnologyID = -1;
        private int defaultVendorID = -1;
        private string defaultPortType = string.Empty;

        public MainForm()
        {
            this.InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Text += string.Format(" - ver. {0}", Application.ProductVersion);

            using (SqlConnection cn = new SqlConnection(Settings.Default.TelefinConnectionString))
            {
                this.toolStripDBName.Text = "Server:" + cn.DataSource + " Database:" + cn.Database;
            }

            string[] portTypes = Settings.Default.PortTypes.Split(',');

            if (portTypes.Length > 0)
            {
                this.portTypeDataGridViewTextBoxColumn.Items.AddRange(portTypes);
                this.defaultPortType = portTypes[0];
            }

            this.OpenFromDB();
        }

        private void dgDeviceType_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.OpenFromDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on read DB data:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OpenFromDB()
        {
            this.technologyTableAdapter.Fill(this.deviceTypeDS.technology);
            this.vendorsTableAdapter.Fill(this.deviceTypeDS.vendors);
            this.systemsTableAdapter.Fill(this.deviceTypeDS.systems);
            this.device_typeTableAdapter.Fill(this.deviceTypeDS.device_type);

            if ((this.deviceTypeDS.technology != null) && (this.deviceTypeDS.technology.Rows.Count > 0))
            {
                this.defaultTechnologyID = (int) this.deviceTypeDS.technology.Rows[0]["TechnologyID"];
            }

            if ((this.deviceTypeDS.vendors != null) && (this.deviceTypeDS.vendors.Rows.Count > 0))
            {
                this.defaultVendorID = (int) this.deviceTypeDS.vendors.Rows[0]["VendorID"];
            }

            if ((this.deviceTypeDS.systems != null) && (this.deviceTypeDS.systems.Rows.Count > 0))
            {
                this.defaultSystemID = (int) this.deviceTypeDS.systems.Rows[0]["SystemID"];
            }

            this.toolStripStatusLabel1.Text = string.Format("{0} device types loaded", this.deviceTypeDS.device_type.Rows.Count);
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.device_typeTableAdapter.Update(this.deviceTypeDS.device_type);

                this.OpenFromDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on update DB:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgDeviceType_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[(int) GridColumns.Obsolete].Value = false;

            if (this.defaultSystemID > 0)
            {
                e.Row.Cells[(int) GridColumns.SystemID].Value = this.defaultSystemID;
            }

            if (this.defaultTechnologyID > 0)
            {
                e.Row.Cells[(int) GridColumns.TechnologyID].Value = this.defaultTechnologyID;
            }

            if (this.defaultVendorID > 0)
            {
                e.Row.Cells[(int) GridColumns.VendorID].Value = this.defaultVendorID;
            }

            if (!string.IsNullOrEmpty(this.defaultPortType))
            {
                e.Row.Cells[(int) GridColumns.PortType].Value = this.defaultPortType;
            }
        }

        private void dgDeviceType_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            string deviceType = (string) e.Row.Cells[(int) GridColumns.DeviceTypeID].Value;

            if (!string.IsNullOrEmpty(deviceType))
            {
                int? relatedDevicesCount = this.device_typeTableAdapter.CountRelatedDevices(deviceType);

                if ((relatedDevicesCount.HasValue) && (relatedDevicesCount.Value > 0))
                {
                    e.Cancel = true;
                    MessageBox.Show(this,
                                    string.Format("Unable to delete this device type. It is related to {0} device{1}.", relatedDevicesCount.Value,
                                                  (relatedDevicesCount.Value == 1 ? string.Empty : "s")), this.Text, MessageBoxButtons.OK,
                                                  MessageBoxIcon.Error);
                }
            }
        }
    }
}
﻿namespace DeviceTypeEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDBName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.dgDeviceType = new System.Windows.Forms.DataGridView();
            this.systemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deviceTypeDS = new DeviceTypeEditor.DeviceTypeDS();
            this.vendorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.technologyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.devicetypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.device_typeTableAdapter = new DeviceTypeEditor.DeviceTypeDSTableAdapters.device_typeTableAdapter();
            this.systemsTableAdapter = new DeviceTypeEditor.DeviceTypeDSTableAdapters.systemsTableAdapter();
            this.vendorsTableAdapter = new DeviceTypeEditor.DeviceTypeDSTableAdapters.vendorsTableAdapter();
            this.technologyTableAdapter = new DeviceTypeEditor.DeviceTypeDSTableAdapters.technologyTableAdapter();
            this.deviceTypeIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.systemIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.vendorIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.deviceTypeDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wSUrlPatternDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.technologyIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.obsoleteDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ExportToList = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.defaultNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.portTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.snmpCommunityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.globalOrderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeviceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.technologyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.devicetypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDBName,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 440);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDBName
            // 
            this.toolStripDBName.Name = "toolStripDBName";
            this.toolStripDBName.Size = new System.Drawing.Size(99, 17);
            this.toolStripDBName.Text = "toolStripDBName";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(4, 17);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(784, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Load data from DB";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save data to DB";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // dgDeviceType
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDeviceType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDeviceType.AutoGenerateColumns = false;
            this.dgDeviceType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgDeviceType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDeviceType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.deviceTypeIDDataGridViewTextBoxColumn,
            this.systemIDDataGridViewTextBoxColumn,
            this.vendorIDDataGridViewTextBoxColumn,
            this.deviceTypeDescriptionDataGridViewTextBoxColumn,
            this.wSUrlPatternDataGridViewTextBoxColumn,
            this.orderDataGridViewTextBoxColumn,
            this.technologyIDDataGridViewTextBoxColumn,
            this.obsoleteDataGridViewCheckBoxColumn,
            this.ExportToList,
            this.defaultNameDataGridViewTextBoxColumn,
            this.portTypeDataGridViewTextBoxColumn,
            this.snmpCommunityDataGridViewTextBoxColumn,
            this.imageNameDataGridViewTextBoxColumn,
            this.globalOrderDataGridViewTextBoxColumn});
            this.dgDeviceType.DataSource = this.devicetypeBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDeviceType.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgDeviceType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDeviceType.Location = new System.Drawing.Point(0, 25);
            this.dgDeviceType.MultiSelect = false;
            this.dgDeviceType.Name = "dgDeviceType";
            this.dgDeviceType.ShowCellErrors = false;
            this.dgDeviceType.Size = new System.Drawing.Size(784, 415);
            this.dgDeviceType.TabIndex = 8;
            this.dgDeviceType.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgDeviceType_DataError);
            this.dgDeviceType.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgDeviceType_DefaultValuesNeeded);
            this.dgDeviceType.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgDeviceType_UserDeletingRow);
            // 
            // systemsBindingSource
            // 
            this.systemsBindingSource.DataMember = "systems";
            this.systemsBindingSource.DataSource = this.deviceTypeDS;
            // 
            // deviceTypeDS
            // 
            this.deviceTypeDS.DataSetName = "DeviceTypeDS";
            this.deviceTypeDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vendorsBindingSource
            // 
            this.vendorsBindingSource.DataMember = "vendors";
            this.vendorsBindingSource.DataSource = this.deviceTypeDS;
            // 
            // technologyBindingSource
            // 
            this.technologyBindingSource.DataMember = "technology";
            this.technologyBindingSource.DataSource = this.deviceTypeDS;
            // 
            // devicetypeBindingSource
            // 
            this.devicetypeBindingSource.DataMember = "device_type";
            this.devicetypeBindingSource.DataSource = this.deviceTypeDS;
            // 
            // device_typeTableAdapter
            // 
            this.device_typeTableAdapter.ClearBeforeFill = true;
            // 
            // systemsTableAdapter
            // 
            this.systemsTableAdapter.ClearBeforeFill = true;
            // 
            // vendorsTableAdapter
            // 
            this.vendorsTableAdapter.ClearBeforeFill = true;
            // 
            // technologyTableAdapter
            // 
            this.technologyTableAdapter.ClearBeforeFill = true;
            // 
            // deviceTypeIDDataGridViewTextBoxColumn
            // 
            this.deviceTypeIDDataGridViewTextBoxColumn.DataPropertyName = "DeviceTypeID";
            this.deviceTypeIDDataGridViewTextBoxColumn.Frozen = true;
            this.deviceTypeIDDataGridViewTextBoxColumn.HeaderText = "DeviceTypeID";
            this.deviceTypeIDDataGridViewTextBoxColumn.Name = "deviceTypeIDDataGridViewTextBoxColumn";
            this.deviceTypeIDDataGridViewTextBoxColumn.Width = 101;
            // 
            // systemIDDataGridViewTextBoxColumn
            // 
            this.systemIDDataGridViewTextBoxColumn.DataPropertyName = "SystemID";
            this.systemIDDataGridViewTextBoxColumn.DataSource = this.systemsBindingSource;
            this.systemIDDataGridViewTextBoxColumn.DisplayMember = "SystemDescription";
            this.systemIDDataGridViewTextBoxColumn.HeaderText = "SystemID";
            this.systemIDDataGridViewTextBoxColumn.Name = "systemIDDataGridViewTextBoxColumn";
            this.systemIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.systemIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.systemIDDataGridViewTextBoxColumn.ValueMember = "SystemID";
            this.systemIDDataGridViewTextBoxColumn.Width = 77;
            // 
            // vendorIDDataGridViewTextBoxColumn
            // 
            this.vendorIDDataGridViewTextBoxColumn.DataPropertyName = "VendorID";
            this.vendorIDDataGridViewTextBoxColumn.DataSource = this.vendorsBindingSource;
            this.vendorIDDataGridViewTextBoxColumn.DisplayMember = "VendorName";
            this.vendorIDDataGridViewTextBoxColumn.HeaderText = "VendorID";
            this.vendorIDDataGridViewTextBoxColumn.Name = "vendorIDDataGridViewTextBoxColumn";
            this.vendorIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.vendorIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.vendorIDDataGridViewTextBoxColumn.ValueMember = "VendorID";
            this.vendorIDDataGridViewTextBoxColumn.Width = 77;
            // 
            // deviceTypeDescriptionDataGridViewTextBoxColumn
            // 
            this.deviceTypeDescriptionDataGridViewTextBoxColumn.DataPropertyName = "DeviceTypeDescription";
            this.deviceTypeDescriptionDataGridViewTextBoxColumn.HeaderText = "DeviceTypeDescription";
            this.deviceTypeDescriptionDataGridViewTextBoxColumn.Name = "deviceTypeDescriptionDataGridViewTextBoxColumn";
            this.deviceTypeDescriptionDataGridViewTextBoxColumn.Width = 143;
            // 
            // wSUrlPatternDataGridViewTextBoxColumn
            // 
            this.wSUrlPatternDataGridViewTextBoxColumn.DataPropertyName = "WSUrlPattern";
            this.wSUrlPatternDataGridViewTextBoxColumn.HeaderText = "WSUrlPattern";
            this.wSUrlPatternDataGridViewTextBoxColumn.Name = "wSUrlPatternDataGridViewTextBoxColumn";
            this.wSUrlPatternDataGridViewTextBoxColumn.Width = 97;
            // 
            // orderDataGridViewTextBoxColumn
            // 
            this.orderDataGridViewTextBoxColumn.DataPropertyName = "Order";
            this.orderDataGridViewTextBoxColumn.HeaderText = "Order";
            this.orderDataGridViewTextBoxColumn.Name = "orderDataGridViewTextBoxColumn";
            this.orderDataGridViewTextBoxColumn.Width = 58;
            // 
            // technologyIDDataGridViewTextBoxColumn
            // 
            this.technologyIDDataGridViewTextBoxColumn.DataPropertyName = "TechnologyID";
            this.technologyIDDataGridViewTextBoxColumn.DataSource = this.technologyBindingSource;
            this.technologyIDDataGridViewTextBoxColumn.DisplayMember = "TechnologyData";
            this.technologyIDDataGridViewTextBoxColumn.HeaderText = "TechnologyID";
            this.technologyIDDataGridViewTextBoxColumn.Name = "technologyIDDataGridViewTextBoxColumn";
            this.technologyIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.technologyIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.technologyIDDataGridViewTextBoxColumn.ValueMember = "TechnologyID";
            this.technologyIDDataGridViewTextBoxColumn.Width = 99;
            // 
            // obsoleteDataGridViewCheckBoxColumn
            // 
            this.obsoleteDataGridViewCheckBoxColumn.DataPropertyName = "Obsolete";
            this.obsoleteDataGridViewCheckBoxColumn.HeaderText = "Obsolete";
            this.obsoleteDataGridViewCheckBoxColumn.Name = "obsoleteDataGridViewCheckBoxColumn";
            this.obsoleteDataGridViewCheckBoxColumn.Width = 55;
            // 
            // ExportToList
            // 
            this.ExportToList.DataPropertyName = "ExportToList";
            this.ExportToList.HeaderText = "ExportToList";
            this.ExportToList.Name = "ExportToList";
            this.ExportToList.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ExportToList.Width = 91;
            // 
            // defaultNameDataGridViewTextBoxColumn
            // 
            this.defaultNameDataGridViewTextBoxColumn.DataPropertyName = "DefaultName";
            this.defaultNameDataGridViewTextBoxColumn.HeaderText = "DefaultName";
            this.defaultNameDataGridViewTextBoxColumn.Name = "defaultNameDataGridViewTextBoxColumn";
            this.defaultNameDataGridViewTextBoxColumn.Width = 94;
            // 
            // portTypeDataGridViewTextBoxColumn
            // 
            this.portTypeDataGridViewTextBoxColumn.DataPropertyName = "PortType";
            this.portTypeDataGridViewTextBoxColumn.HeaderText = "PortType";
            this.portTypeDataGridViewTextBoxColumn.Name = "portTypeDataGridViewTextBoxColumn";
            this.portTypeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.portTypeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.portTypeDataGridViewTextBoxColumn.Width = 75;
            // 
            // snmpCommunityDataGridViewTextBoxColumn
            // 
            this.snmpCommunityDataGridViewTextBoxColumn.DataPropertyName = "SnmpCommunity";
            this.snmpCommunityDataGridViewTextBoxColumn.HeaderText = "SnmpCommunity";
            this.snmpCommunityDataGridViewTextBoxColumn.Name = "snmpCommunityDataGridViewTextBoxColumn";
            this.snmpCommunityDataGridViewTextBoxColumn.Width = 110;
            // 
            // imageNameDataGridViewTextBoxColumn
            // 
            this.imageNameDataGridViewTextBoxColumn.DataPropertyName = "ImageName";
            this.imageNameDataGridViewTextBoxColumn.HeaderText = "ImageName";
            this.imageNameDataGridViewTextBoxColumn.Name = "imageNameDataGridViewTextBoxColumn";
            this.imageNameDataGridViewTextBoxColumn.Width = 89;
            // 
            // globalOrderDataGridViewTextBoxColumn
            // 
            this.globalOrderDataGridViewTextBoxColumn.DataPropertyName = "GlobalOrder";
            this.globalOrderDataGridViewTextBoxColumn.HeaderText = "GlobalOrder";
            this.globalOrderDataGridViewTextBoxColumn.Name = "globalOrderDataGridViewTextBoxColumn";
            this.globalOrderDataGridViewTextBoxColumn.Width = 88;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 462);
            this.Controls.Add(this.dgDeviceType);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "GRIS Device Type Editor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeviceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.technologyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.devicetypeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripDBName;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.DataGridView dgDeviceType;
        private DeviceTypeDS deviceTypeDS;
        private DeviceTypeDSTableAdapters.device_typeTableAdapter device_typeTableAdapter;
        private System.Windows.Forms.BindingSource devicetypeBindingSource;
        private System.Windows.Forms.BindingSource systemsBindingSource;
        private DeviceTypeDSTableAdapters.systemsTableAdapter systemsTableAdapter;
        private System.Windows.Forms.BindingSource vendorsBindingSource;
        private DeviceTypeDSTableAdapters.vendorsTableAdapter vendorsTableAdapter;
        private System.Windows.Forms.BindingSource technologyBindingSource;
        private DeviceTypeDSTableAdapters.technologyTableAdapter technologyTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn systemDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendorNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn technologyDataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deviceTypeIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn systemIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn vendorIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deviceTypeDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wSUrlPatternDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn technologyIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn obsoleteDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ExportToList;
        private System.Windows.Forms.DataGridViewTextBoxColumn defaultNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn portTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn snmpCommunityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imageNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn globalOrderDataGridViewTextBoxColumn;
    }
}


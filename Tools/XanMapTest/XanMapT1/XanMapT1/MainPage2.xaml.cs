﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Infragistics.Silverlight.Map;
using XanMapT1.DataServices;
using System.Collections.ObjectModel;

namespace XanMapT1
{
    public partial class MainPage2 : UserControl
    {
        private List<MyElementData> myStationData;

        public MainPage2()
        {
            InitializeComponent();

            myStationData = new List<MyElementData> 
            {
                new MyElementData { ElementName = "Roma", State = 0, GrisState = GrisState.Ok},
                new MyElementData { ElementName = "Genova", State = 1, GrisState = GrisState.Warning},
                new MyElementData { ElementName = "Milano", State = 2, GrisState = GrisState.Error},
                new MyElementData { ElementName = "Bologna", State = 4, GrisState = GrisState.MaintenanceMode},
                new MyElementData { ElementName = "Pisa", State = 0, GrisState = GrisState.Ok},
                new MyElementData { ElementName = "Palermo", State = 0, GrisState = GrisState.Ok},
                new MyElementData { ElementName = "Bari", State = 0, GrisState = GrisState.Ok},
            };

        }

        private void MapLayerStations_Imported(object sender, MapLayerImportEventArgs e)
        {
            if (e.Action == MapLayerImportAction.End)
            {
                MapLayer layer = (MapLayer)sender;
                layer.DataSource = myStationData;
                layer.DataBind();

                //foreach (var element in layer.Elements)
                //{
                //    object v = element.GetProperty("GrisState");
                //    GrisState gs = GrisState.Offline;
                //    if (v != null)
                //    {
                //        gs = (GrisState)v;
                //    }
                //    switch (gs)
                //    {
                //        case GrisState.Offline:
                //            element.Fill = new SolidColorBrush(Colors.LightGray);
                //            break;
                //        case GrisState.Error:
                //            element.Fill = new SolidColorBrush(Colors.Red);
                //            break;
                //        case GrisState.Ok:
                //            element.Fill = new SolidColorBrush(Colors.Green);
                //            break;
                //        case GrisState.Warning:
                //            element.Fill = new SolidColorBrush(Colors.Yellow);
                //            break;
                //        case GrisState.MaintenanceMode:
                //            element.Fill = new SolidColorBrush(Colors.Blue);
                //            break;
                //    }

                    
                //}
            }
        }



    }

}

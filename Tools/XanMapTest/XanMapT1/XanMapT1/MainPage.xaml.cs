﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Infragistics.Silverlight.Map;
using XanMapT1.DataServices;
using System.Collections.ObjectModel;
using GrisSuite.Web.Controls.SL;

namespace XanMapT1
{
    public partial class MainPage : UserControl
    {
        //private List<MyMapPoint> myStations;
        //private List<MyMapLine> myRailwais;
        private List<MyElementData> myRegionData;
        private List<MyElementData> myRailwaisData;
        private List<MyElementData> myStationData;
        //private List<MapElement> mapElementsToAdd = new List<MapElement>();

        //private MapElement currentElement = null;
        //private System.Windows.Threading.DispatcherTimer myTimer = new System.Windows.Threading.DispatcherTimer();
        //private System.Windows.Threading.DispatcherTimer timerShowMapElements = new System.Windows.Threading.DispatcherTimer();

        public MainPage()
        {
            InitializeComponent();

            //timerShowMapElements.Interval = new TimeSpan(0, 0, 0, 0, 2000);
            //timerShowMapElements.Tick += new EventHandler(timerShowMapElements_Tick);
            //timerShowMapElements.Start();

            //myTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); 
            //myTimer.Tick += new EventHandler(myTimer_Tick);

            myRegionData = new List<MyElementData> 
            {
                new MyElementData { ElementName = "Abruzzi", State=0},
                new MyElementData { ElementName = "Basilicata", State=1},
                new MyElementData { ElementName = "Calabria", State=2},
                new MyElementData { ElementName = "Campania", State=3},
                new MyElementData { ElementName = "Emilia Romagna", State=1},
                new MyElementData { ElementName = "Friuli Venezia Giulia", State=0},
                new MyElementData { ElementName = "Lazio", State=0},
                new MyElementData { ElementName = "Liguria", State=0},
                new MyElementData { ElementName = "Lombardia", State=0},
                new MyElementData { ElementName = "Molise", State=0},
                new MyElementData { ElementName = "Marche", State=0},
                new MyElementData { ElementName = "Piemonte", State=0},
                new MyElementData { ElementName = "Puglia", State=0},
                new MyElementData { ElementName = "Sicilia", State=0},
                new MyElementData { ElementName = "Sardegna", State=0},
                new MyElementData { ElementName = "Trentino Alto Adige", State=0},
                new MyElementData { ElementName = "Toscana", State=0},
                new MyElementData { ElementName = "Umbria", State=0},
                new MyElementData { ElementName = "Valle d'Aosta", State=0},
                new MyElementData { ElementName = "Veneto", State=0}
            };

            myRailwaisData= new List<MyElementData> 
            {
                new MyElementData { ElementName = "Milano-Bologna", State=0},
                new MyElementData { ElementName = "Milano-Genova", State=1},
                new MyElementData { ElementName = "Padova-Bologna", State=2},
                new MyElementData { ElementName = "Roma-Bologna", State=3},
                new MyElementData { ElementName = "Bologna-Pisa", State=4},
                new MyElementData { ElementName = "Roma-Pisa", State=0},
                new MyElementData { ElementName = "Roma-Bari", State=0},
                new MyElementData { ElementName = "Palermo-Roma", State=0},
                new MyElementData { ElementName = "Milano-Genova", State=0},
                new MyElementData { ElementName = "Verona-Venezia", State=0},
                new MyElementData { ElementName = "Mestre-Belluno", State=0},
                new MyElementData { ElementName = "Milano-Padova", State=0}

            };

            myStationData = new List<MyElementData> 
            {
                new MyElementData { ElementName = "Roma", State = 0, GrisState = GrisState.Ok},
                new MyElementData { ElementName = "Genova", State = 1, GrisState = GrisState.Warning},
                new MyElementData { ElementName = "Milano", State = 2, GrisState = GrisState.Error},
                //new MyElementData { ElementName = "Padova", State = 3, GrisState = GrisState.Offline},
                new MyElementData { ElementName = "Bologna", State = 4, GrisState = GrisState.MaintenanceMode},
                new MyElementData { ElementName = "Pisa", State = 0, GrisState = GrisState.Ok},
                new MyElementData { ElementName = "Palermo", State = 0, GrisState = GrisState.Ok},
                new MyElementData { ElementName = "Bari", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Vicenza", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Venezia", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Mestre", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Treviso", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Belluno", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Verona", State = 0, GrisState = GrisState.Ok},
                //new MyElementData { ElementName = "Rovigo", State = 0, GrisState = GrisState.Ok}
            };

            // Caricamento delle informazioni geografiche sulle linee e sulle stazioni non da layer ma
            // direttamente da DB.
            // Le prestazioni erano deludenti...
            //
            //MapServiceClient msc = new XanMapT1.DataServices.MapServiceClient();
            //msc.GetStationsCompleted += new EventHandler<XanMapT1.DataServices.GetStationsCompletedEventArgs>(msc_GetStationsCompleted);
            //msc.GetStationsAsync();

            //msc.GetRailwaysCompleted += new EventHandler<GetRailwaysCompletedEventArgs>(msc_GetRailwaysCompleted);
            //msc.GetRailwaysAsync();
        }

        //void msc_GetRailwaysCompleted(object sender, GetRailwaysCompletedEventArgs e)
        //{
        //    myRailwais = new List<MyMapLine>();
        //    ObservableCollection<Railway> railways = e.Result;
        //    foreach (var railway in railways)
        //    {
        //        MyMapLine myLine = new MyMapLine { Name = railway.RailwayName };
        //        myLine.Points = new List<MyMapPoint>();
        //        foreach (var rPoint in railway.RailwayPoints)
        //        {
        //            myLine.Points.Add(new MyMapPoint { Name = "", Latitude = rPoint.MapPoint.Lat, Longitude = rPoint.MapPoint.Long, Layer = 0, SevLevel = 0 });
        //        }    
        //        myRailwais.Add(myLine);
        //    }
        //    ShowMyMapLines(myRailwais);
        //}

        //void msc_GetStationsCompleted(object sender, XanMapT1.DataServices.GetStationsCompletedEventArgs e)
        //{
        //    myStations = new List<MyMapPoint>();
        //    ObservableCollection<Station> stations = e.Result;
        //    foreach (var station in stations)
        //    {
        //        myStations.Add(new MyMapPoint {  Name = station.StationName, Latitude = station.MapPoint.Lat, Longitude = station.MapPoint.Long, Layer = GetLayerFromRelevance(station.Relevance), SevLevel = station.SevLevel});
        //    }
        //    ShowMyMapPoints(myStations);
        //}

        private void MapLayerRegions_Imported(object sender, MapLayerImportEventArgs e)
        {
            if (e.Action == MapLayerImportAction.End)
            {
                MapLayer layer = (MapLayer)sender;
                layer.DataSource = myRegionData;
                layer.DataBind();
            }
        }

        private void MapLayerRailways_Imported(object sender, MapLayerImportEventArgs e)
        {
            if (e.Action == MapLayerImportAction.End)
            {
                MapLayer layer = (MapLayer)sender;
                layer.DataSource = myRailwaisData;
                layer.DataBind();
            }
        }


        private void MapLayerStations_Imported(object sender, MapLayerImportEventArgs e)
        {
            if (e.Action == MapLayerImportAction.End)
            {
                MapLayer layer = (MapLayer)sender;
                layer.DataSource = myStationData;
                layer.DataBind();

                foreach (var element in layer.Elements)
                {
                    object v = element.GetProperty("GrisState");
                    GrisState gs = GrisState.Offline;
                    if (v != null)
                    {
                        gs = (GrisState)v;
                    }
                    switch (gs)
                    {
                        case GrisState.Offline:
                            element.Fill = new SolidColorBrush(Colors.LightGray);
                            break;
                        case GrisState.Error:
                            element.Fill = new SolidColorBrush(Colors.Red);
                            break;
                        case GrisState.Ok:
                            element.Fill = new SolidColorBrush(Colors.Green);
                            break;
                        case GrisState.Warning:
                            element.Fill = new SolidColorBrush(Colors.Yellow);
                            break;
                        case GrisState.MaintenanceMode:
                            element.Fill = new SolidColorBrush(Colors.Blue);
                            break;
                    }

                    
                }
            }
        }


        //private void ShowMyMapPoints(List<MyMapPoint> myStations)
        //{
        //    int stationCounter = 0;

        //    int visibleLayer = GetVisibleLayer();

        //    stationCounter = 0;
        //    foreach (var myPoint in myStations)
        //    {

        //        if (visibleLayer < myPoint.Layer || myPoint.IsOnLayers[visibleLayer])
        //            continue;

        //        for (int i = myPoint.Layer; i < theMap.Layers.Count; i++)
        //        {

        //            Point origin = theMap.MapProjection.ProjectToMap(new Point(myPoint.Longitude, myPoint.Latitude));

        //            if (!theMap.WindowRect.Contains(origin))
        //                continue;

        //            stationCounter++;
        //            SymbolElement element = new SymbolElement() 
        //            { 
        //                SymbolOrigin = origin, 
        //                Name = myPoint.Name, 
        //                Caption = myPoint.Name, 
        //                SymbolType = MapSymbolType.Pyramid, 
        //                SymbolSize = 15, 
        //                Value = myPoint.Value 
        //            };
                    
        //            theMap.Layers[i].Elements.Add(element);

        //            // Test caricamento asincrono crea creava cambio della colorozione degli elementi
        //            // probabilmente perchè il LinearScale per determinare quale colore applicare
        //            // valuta il Massimo e il Minimo dei valori caricati, caricadoli progressivamente
        //            // la scala cambia.
        //            // 
        //            //element.SetProperty("AddToLayer", i);
        //            //lock (this.mapElementsToAdd)
        //            //{
        //            //    mapElementsToAdd.Add(element);
        //            //}

        //            myPoint.IsOnLayers[i] = true;
        //        }
        //    }
        //    LoadedText.Text = stationCounter.ToString();

        //}


        //private void ShowMyMapLines(List<MyMapLine> myRailwais)
        //{

        //    foreach (var myLine in myRailwais)
        //    {
        //        // polyline collection for end-points of line
        //        MapPolylineCollection lines = new MapPolylineCollection();

        //        List<Point> points = new List<Point>();
        //        foreach (var myPoint in myLine.Points)
        //        {
        //            points.Add(new Point(myPoint.Longitude, myPoint.Latitude));
        //        }

        //        // Convert Geodetic to Cartesian coordinates
        //        lines.Add(theMap.MapProjection.ProjectToMap(points));
        //        for (int i = myLine.Layer; i < theMap.Layers.Count; i++)
        //        {
        //            // Create path element and set points using polylines
        //            PathElement lineElement = new PathElement() { Polylines = lines, StrokeThickness = 2, Name = myLine.Name };
        //            theMap.Layers[i].Elements.Add(lineElement);

        //            Rect worldRect = lineElement.WorldRect;
        //            worldRect = lines.GetWorldRect();
        //            lineElement.WorldRect = worldRect;
        //        }
                
        //    }
        //}

        private void theMap_ElementClick(object sender, MapElementClickEventArgs e)
        {
            theMap.WindowFit(e.Element);

            //currentElement = e.Element;
            //myTimer.Start();
        }

        //void myTimer_Tick(object sender, EventArgs e)
        //{
        //    LoadedText.Text = currentElement.Name + " WindowsScale:" + theMap.WindowScale.ToString();
        //    myTimer.Stop();
        //}

        //void timerShowMapElements_Tick(object sender, EventArgs e)
        //{
        //    lock (this.mapElementsToAdd)
        //    {
        //        if (this.mapElementsToAdd != null && this.mapElementsToAdd.Count > 0)
        //        {
        //            int max = 20;
        //            int removed = 0;
        //            for (int i = this.mapElementsToAdd.Count - 1; i > 0; i--)
        //            {
        //                var item = this.mapElementsToAdd[i];
        //                int iLayer = (int)item.GetProperty("AddToLayer");

        //                theMap.Layers[iLayer].Elements.Add(item);

        //                System.Diagnostics.Debug.WriteLine("Display:" + item.Name + " Layer:" + iLayer);
        //                this.mapElementsToAdd.RemoveAt(i);
        //                if (++removed >= max)
        //                    return;
        //            }
        //        }
        //    }
        //}

        private void theMap_WindowRectChanged(object sender, MapWindowRectChangedEventArgs e)
        {
            LoadedText.Text = "ChangeRect:" + e.WindowScale.ToString();
            //if (this.myStations != null)
            //    ShowMyMapPoints(this.myStations);
        }

        //private int GetVisibleLayer()
        //{
        //    for (int i = 0; i < theMap.Layers.Count; i++)
        //    {
        //        if (theMap.WindowScale >= theMap.Layers[i].VisibleFromScale && theMap.WindowScale < theMap.Layers[i].VisibleToScale)
        //        {
        //            return i;
        //        }
        //    }
        //    return theMap.Layers.Count-1;
        //}

        //private int GetLayerFromRelevance(byte relevance)
        //{
        //    return relevance;
        //}

        //private void MapLayer_ElementPrerender(object sender, MapElementRenderEventArgs e)
        //{
            // Tentativi di recuperare lo stato associato al MapElement che sta per essere visualizzato
            // e di associarlo al ControlloCustumm contenuto nel ValueTemplate.
            // Il Recupero del valore funzionava il recupero del controllo custom no.
            // 20/11/2009
            //
            //MapElement me = e.Element;
            //GrisState gs = GrisState.Ok;

            //foreach (var item in me.Layer.Elements)
            //{
            //    if (item.Name == me.Name && item.HasCustomProperty("MyState")) 
            //    { 
            //        object p = item.GetProperty("MyState");
            //        if (p != null)
            //            gs = (GrisState)item.GetProperty("MyState");
            //        break;
            //    }
            //}

            //VisualStateObject vso = me.ActualValueTemplate.LoadContent() as VisualStateObject;

            ////VisualStateObject vso = FindVisualChild<VisualStateObject>(uie);
            //if (vso != null)
            //    vso.State = GrisState.Error;
        //}

        //private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        //{

        //    for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
        //    {

        //        DependencyObject child = VisualTreeHelper.GetChild(obj, i);

        //        if (child != null && child is childItem)
        //            return (childItem)child;
        //        else
        //        {
        //            childItem childOfChild = FindVisualChild<childItem>(child);
        //            if (childOfChild != null)
        //                return childOfChild;
        //        }
        //    }

        //    return null;
        //}


        //private void VisualStateObject_Click(object sender, EventArgs e)
        //{
        //    FrameworkElement s = (FrameworkElement)sender;

        //    MapElement me = (Infragistics.Silverlight.Map.MapElement)s.DataContext;
        //    GrisState gs = GrisState.Error;

        //    foreach (var item in me.Layer.Elements)
        //    {
        //        if (item.Name == me.Name)
        //        {
        //            gs = (GrisState)item.GetProperty("MyState");
        //            break;
        //        }
        //    }

        //    LoadedText.Text = "Click " + me.Name + " " + gs.ToString();
        //}

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Ports;

namespace SetBreak
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string portName = "";
                bool breakState = false;

                if (args.Length > 1)
                {
                    portName = args[0];
                    breakState = bool.Parse(args[1]);

                    SerialPort sp = new SerialPort(portName);
                    
                    sp.Open();
                    sp.BreakState = breakState;
                    
                    Console.WriteLine("Done SetBreak=: {0}", sp.BreakState);
                    Console.ReadLine();
                    sp.Close();

                }
                else
                {
                    Console.WriteLine("SetBreak, sintassi: SetBreak.Exe <PortName> <True|False>");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}",ex.Message);
            }

        }
    }
}

﻿using System;
using System.Globalization;
using System.IO;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Win32;

namespace ConvertSystemXmlToSnmp20
{
    internal class Program
    {
        #region Id supervisori

        private const int SERIAL_SUPERVISOR_ID = 0;
        private const int SNMP_SUPERVISOR_ID = 1;
        private const int MODBUS_SUPERVISOR_ID = 2;
        private const int TELNET_SUPERVISOR_ID = 3;

        #endregion

        #region Path e nome file di log

        private static readonly string logFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ConvertSystemXmlToSnmp20.txt");

        #endregion

        #region Campi XmlDocument con i tre file di configurazione - System.xml, StlcManagerConfig.xml e device_type_list.xml

        private static readonly XmlDocument docSystemXml = new XmlDocument();
        private static readonly XmlDocument docStlcManagerConfigXml = new XmlDocument();
        private static readonly XmlDocument docDeviceTypeListXml = new XmlDocument();

        #endregion

        private static void Main(string[] args)
        {
            string systemXmlFilename = GetSystemXmlPath();
            string stlcManagerXmlFilename = GetSTLCManagerXmlPath();
            string deviceTypeListFilename = GetDeviceTypeListPath();

            Console.WriteLine("Log di testo salvato su file '{0}'", logFilename);

            BackupFile(logFilename, true);

            #region Verifiche esistenza file di configurazione da modificare

            if (!CheckFileCanRead(systemXmlFilename))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare o aprire il file System.xml in: '{0}'",
                    systemXmlFilename));
                return;
            }

            if (!CheckFileCanRead(stlcManagerXmlFilename))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare o aprire il file STLCManagerConfig.xml in: '{0}'",
                    stlcManagerXmlFilename));
                return;
            }

            if (!CheckFileCanRead(deviceTypeListFilename))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Impossibile trovare o aprire il file device_type_list.xml in: '{0}'",
                    deviceTypeListFilename));
                return;
            }

            #endregion

            #region Backup dei file di configurazione

            if (!BackupFile(systemXmlFilename))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Impossibile creare copia di backup del file System.xml"));
                return;
            }

            if (!BackupFile(stlcManagerXmlFilename))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Impossibile creare copia di backup del file STLCManagerConfig.xml"));
                return;
            }

            if (!BackupFile(deviceTypeListFilename))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Impossibile creare copia di backup del file device_type_list.xml"));
                return;
            }

            #endregion

            #region Ricerca parametro /f che indica la modalità di scrittura forzata, con conversione dei tipi ad InfoStazioni

            bool convertToInfostazioni = false;

            foreach (string arg in args)
            {
                if (arg.Equals("/f", StringComparison.OrdinalIgnoreCase))
                {
                    convertToInfostazioni = true;
                    break;
                }
            }

            if (convertToInfostazioni)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Attivata conversione periferiche a tipo Infostazioni, saranno cambiati tipi periferiche e SNMP community"));
            }
            else
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Disattivata conversione periferiche a tipo Infostazioni, non saranno cambiati tipi periferiche e SNMP community"));
            }

            #endregion

            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                "--------------------------------------------------------------------------------"));

            #region Aggiornamento System.xml

            string snmpPort;
            string nodId;
            string srvId;
            XmlNode stationNode;

            string buildingIdString;
            string rackIdString;

            XmlNode node = GetSystemXmlNodeElement(systemXmlFilename, out snmpPort, out nodId, out srvId, out stationNode);

            #region Validazione del System.xml per porta SNMP, NodeId, ServerId e Station

            if (string.IsNullOrEmpty(snmpPort))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare la porta SNMP nel file di configurazione '{0}', nel percorso /telefin/port.", systemXmlFilename));
                return;
            }

            if (string.IsNullOrEmpty(nodId))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare il NodeId nel file di configurazione '{0}', nel percorso /telefin/system/server/region/zone/node.",
                    systemXmlFilename));
                return;
            }

            if (string.IsNullOrEmpty(srvId))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare il ServerId nel file di configurazione '{0}', nel percorso /telefin/system/server.", systemXmlFilename));
                return;
            }

            #endregion

            #region Recupera i dati della Stazione

            if (stationNode == null)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare i dati della stazione, relativi alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            if (stationNode.Attributes["id"] == null)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare l'id della stazione, relativo alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            if (string.IsNullOrEmpty(stationNode.Attributes["id"].Value))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare l'id della stazione, relativo alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            if (stationNode.Attributes["name"] == null)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare il nome della stazione, relativo alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            if (string.IsNullOrEmpty(stationNode.Attributes["name"].Value))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare il nome della stazione, relativo alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            string stationIdString = stationNode.Attributes["id"].Value.Trim();
            string stationNameString = stationNode.Attributes["name"].Value.Trim().Replace("Stazione ", string.Empty);

            #endregion

            #region Recupera i dati del Fabbricato (Building)

            bool fabbricatoViaggiatoriFound = false;
            int lastBuildingId = -1;

            XmlNode fabbricatoViaggiatori = null;

            foreach (XmlNode buildingNode in stationNode.ChildNodes)
            {
                if (buildingNode.Name.Equals("building"))
                {
                    if (buildingNode.Attributes["id"] != null)
                    {
                        if (!string.IsNullOrEmpty(buildingNode.Attributes["id"].Value))
                        {
                            int buildingId;
                            if (int.TryParse(buildingNode.Attributes["id"].Value, out buildingId))
                            {
                                if (buildingId > lastBuildingId)
                                {
                                    lastBuildingId = buildingId;
                                }
                            }
                        }
                    }

                    if (buildingNode.Attributes["name"] != null)
                    {
                        if (buildingNode.Attributes["name"].Value.Equals("Fabbricato Viaggiatori", StringComparison.OrdinalIgnoreCase))
                        {
                            fabbricatoViaggiatoriFound = true;
                            fabbricatoViaggiatori = buildingNode;
                            break;
                        }
                    }
                }
            }

            if (!fabbricatoViaggiatoriFound)
            {
                XmlNode fabbricatoViaggiatoriNode = docSystemXml.CreateNode("element", "building", string.Empty);

                // <building id="0" name="Fabbricato Viaggiatori" note="Nessuna" />

                XmlAttribute idAttribute = docSystemXml.CreateAttribute("id");
                idAttribute.Value = (lastBuildingId + 1).ToString();
                fabbricatoViaggiatoriNode.Attributes.Append(idAttribute);

                XmlAttribute nameAttribute = docSystemXml.CreateAttribute("name");
                nameAttribute.Value = "Fabbricato Viaggiatori";
                fabbricatoViaggiatoriNode.Attributes.Append(nameAttribute);

                XmlAttribute noteAttribute = docSystemXml.CreateAttribute("note");
                noteAttribute.Value = "Nessuna";
                fabbricatoViaggiatoriNode.Attributes.Append(noteAttribute);

                stationNode.AppendChild(fabbricatoViaggiatoriNode);

                try
                {
                    docSystemXml.Save(systemXmlFilename);
                }
                catch (XmlException ex)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}", systemXmlFilename,
                        ex.Message));
                }

                fabbricatoViaggiatori = fabbricatoViaggiatoriNode;
            }

            if ((fabbricatoViaggiatori.Attributes["id"] != null) && (!string.IsNullOrEmpty(fabbricatoViaggiatori.Attributes["id"].Value)))
            {
                buildingIdString = fabbricatoViaggiatori.Attributes["id"].Value.Trim();
            }
            else
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare l'id del 'Fabbricato Viaggiatori', relativo alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            #endregion

            #region Recupera i dati dell'Armadio (Location - Rack)

            bool armadioRackFound = false;
            int lastRackId = -1;

            XmlNode armadioRack = null;

            foreach (XmlNode rackNode in fabbricatoViaggiatori.ChildNodes)
            {
                if (rackNode.Name.Equals("location"))
                {
                    if (rackNode.Attributes["id"] != null)
                    {
                        if (!string.IsNullOrEmpty(rackNode.Attributes["id"].Value))
                        {
                            int rackId;
                            if (int.TryParse(rackNode.Attributes["id"].Value, out rackId))
                            {
                                if (rackId > lastRackId)
                                {
                                    lastRackId = rackId;
                                }
                            }
                        }
                    }

                    if (rackNode.Attributes["name"] != null)
                    {
                        if (rackNode.Attributes["name"].Value.Equals("Armadio Rack", StringComparison.OrdinalIgnoreCase))
                        {
                            armadioRackFound = true;
                            armadioRack = rackNode;
                            break;
                        }
                    }
                }
            }

            if (!armadioRackFound)
            {
                XmlNode armadioRackNode = docSystemXml.CreateNode("element", "location", string.Empty);

                // <location id="0" name="Armadio Rack" type="RACK24U" note="Nessuna" />

                XmlAttribute idAttribute = docSystemXml.CreateAttribute("id");
                idAttribute.Value = (lastRackId + 1).ToString();
                armadioRackNode.Attributes.Append(idAttribute);

                XmlAttribute nameAttribute = docSystemXml.CreateAttribute("name");
                nameAttribute.Value = "Armadio Rack";
                armadioRackNode.Attributes.Append(nameAttribute);

                XmlAttribute typeAttribute = docSystemXml.CreateAttribute("type");
                typeAttribute.Value = "RACK24U";
                armadioRackNode.Attributes.Append(typeAttribute);

                XmlAttribute noteAttribute = docSystemXml.CreateAttribute("note");
                noteAttribute.Value = "Nessuna";
                armadioRackNode.Attributes.Append(noteAttribute);

                fabbricatoViaggiatori.AppendChild(armadioRackNode);

                try
                {
                    docSystemXml.Save(systemXmlFilename);
                }
                catch (XmlException ex)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}", systemXmlFilename,
                        ex.Message));
                }

                armadioRack = armadioRackNode;
            }

            if ((armadioRack.Attributes["id"] != null) && (!string.IsNullOrEmpty(armadioRack.Attributes["id"].Value)))
            {
                rackIdString = armadioRack.Attributes["id"].Value.Trim();
            }
            else
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare l'id del 'Armadio Rack', relativo alla topografia, nel file '{0}'.", systemXmlFilename));
                return;
            }

            #endregion

            string stlcAddr = GetCurrentStlcIpAddressEth1();

            if (string.IsNullOrEmpty(stlcAddr))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile recuperare l'indirizzo IP della scheda di rete Intel dell'STLC (ETH1)."));
                return;
            }

            bool stlcDeviceFound = false;

            if (node != null)
            {
                int existingMaxDeviceId = -1;

                #region Ciclo su righe Device per modifiche dei tipi, community, supervisorId e ricerca della periferica STLC1000

                int modifiedDevices = 0;

                foreach (XmlNode device in node.ChildNodes)
                {
                    if (device.Name.Equals("device"))
                    {
                        #region Validazione attributo device

                        if ((device.Attributes["DevID"] == null) || (device.Attributes["DevID"].Value.Trim().Length == 0) ||
                            (device.Attributes["name"] == null) || (device.Attributes["name"].Value.Trim().Length == 0) ||
                            (device.Attributes["station"] == null) || (device.Attributes["station"].Value.Trim().Length == 0) ||
                            (device.Attributes["building"] == null) || (device.Attributes["building"].Value.Trim().Length == 0) ||
                            (device.Attributes["location"] == null) || (device.Attributes["location"].Value.Trim().Length == 0) ||
                            (device.Attributes["type"] == null) || (device.Attributes["type"].Value.Trim().Length == 0) ||
                            (device.Attributes["addr"] == null) || (device.Attributes["addr"].Value.Trim().Length == 0) ||
                            (device.Attributes["SN"] == null) || (device.Attributes["SN"].Value.Trim().Length == 0) ||
                            (device.Attributes["profile"] == null) || (device.Attributes["profile"].Value.Trim().Length == 0) ||
                            (device.Attributes["active"] == null) || (device.Attributes["active"].Value.Trim().Length == 0) ||
                            (device.Attributes["scheduled"] == null) || (device.Attributes["scheduled"].Value.Trim().Length == 0) ||
                            (device.Attributes["position"] == null) || (device.Attributes["position"].Value.Trim().Length == 0) ||
                            (device.Attributes["port"] == null) || (device.Attributes["port"].Value.Trim().Length == 0))
                        {
                            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                "Impossibile trovare l'attributo 'DevID', 'name', 'station', 'building', 'location', 'type', 'addr', 'SN', 'port', 'profile', 'active', 'scheduled' o 'position' sul nodo 'device', oppure almeno un attributo nullo nel file {0}. Il contenuto del nodo e':\r\n{1}",
                                systemXmlFilename, device.OuterXml));
                            return;
                        }

                        #endregion

                        #region Ricerca massimo DevID tra le periferiche esistenti

                        int currentDeviceId;

                        if (!int.TryParse(device.Attributes["DevID"].Value, out currentDeviceId))
                        {
                            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                "Saltata periferica '{0}' (DevID: {1}), perche' ha un DevID non numerico", device.Attributes["name"].Value,
                                device.Attributes["DevID"].Value));

                            continue;
                        }

                        if (currentDeviceId > existingMaxDeviceId)
                        {
                            existingMaxDeviceId = currentDeviceId;
                        }

                        #endregion

                        bool deviceUpdated = false;

                        #region Verifica tipo porta (dispositivo SNMP)

                        if (!device.Attributes["port"].Value.Trim().Equals(snmpPort, StringComparison.OrdinalIgnoreCase))
                        {
                            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                "Saltata periferica '{0}' (DevID: {1}), perche' non di tipo SNMP o TCP/IP", device.Attributes["name"].Value,
                                device.Attributes["DevID"].Value));
                            continue;
                        }

                        #endregion

                        StringBuilder deviceReport = new StringBuilder();

                        #region Modifica attributo 'type'

                        string originalDeviceType = device.Attributes["type"].Value;

                        device.Attributes["type"].Value = GetNewDeviceTypeFromCurrentType(convertToInfostazioni,
                            device.Attributes["type"].Value.Trim());

                        if (!originalDeviceType.Equals(device.Attributes["type"].Value))
                        {
                            if (deviceReport.Length > 0)
                            {
                                deviceReport.Append(", ");
                            }

                            deviceReport.AppendFormat("modificato tipo periferica da '{0}' a '{1}'", originalDeviceType,
                                device.Attributes["type"].Value);

                            deviceUpdated = true;
                        }

                        #endregion

                        #region Modifica o inserisce attributo 'snmp_community'

                        if (device.Attributes["snmp_community"] != null)
                        {
                            string originalCommunity = device.Attributes["snmp_community"].Value;

                            string newCommunity = GetNewCommunity(convertToInfostazioni, device.Attributes["type"].Value,
                                device.Attributes["snmp_community"].Value);
                            if (!string.IsNullOrEmpty(newCommunity))
                            {
                                device.Attributes["snmp_community"].Value = newCommunity;

                                if (!originalCommunity.Equals(device.Attributes["snmp_community"].Value))
                                {
                                    if (deviceReport.Length > 0)
                                    {
                                        deviceReport.Append(", ");
                                    }

                                    deviceReport.AppendFormat("modificata SNMP Community da '{0}' a '{1}'", originalCommunity,
                                        device.Attributes["snmp_community"].Value);

                                    deviceUpdated = true;
                                }
                            }
                        }
                        else
                        {
                            if (!device.Attributes["type"].Value.StartsWith("XXIP", StringComparison.OrdinalIgnoreCase))
                            {
                                XmlAttribute attribute = docSystemXml.CreateAttribute("snmp_community");
                                attribute.Value = "public";
                                device.Attributes.Append(attribute);

                                if (deviceReport.Length > 0)
                                {
                                    deviceReport.Append(", ");
                                }

                                deviceReport.AppendFormat("aggiunta SNMP Community '{0}'", attribute.Value);

                                deviceUpdated = true;
                            }
                        }

                        #endregion

                        #region Modifica o inserisce attributo 'supervisor_id'

                        if (device.Attributes["supervisor_id"] == null)
                        {
                            // Se la device è di rete e non ha l'attributo del supervisore indicato, lo aggiungiamo per il supervisore SNMP
                            // Diamo per scontato che si tratti di un system.xml molto vecchio
                            XmlAttribute attribute = docSystemXml.CreateAttribute("supervisor_id");
                            attribute.Value = SNMP_SUPERVISOR_ID.ToString(CultureInfo.InvariantCulture);
                            device.Attributes.Append(attribute);

                            if (deviceReport.Length > 0)
                            {
                                deviceReport.Append(", ");
                            }

                            deviceReport.AppendFormat("aggiunto attributo supervisor_id '{0}'", SNMP_SUPERVISOR_ID);

                            deviceUpdated = true;
                        }
                        else
                        {
                            // l'ID Supervisore deve essere aggiornato solamente se la periferica sta passando da monitoraggio seriale a quello SNMP
                            // In tutti gli altri casi, ad esempio per supervisore ModBus, non si interviene sul valore già presente
                            int supervisorId;
                            if (!int.TryParse(device.Attributes["supervisor_id"].Value, out supervisorId))
                            {
                                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                    "L'attributo 'supervisor_id' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                    systemXmlFilename, device.Attributes["supervisor_id"].Value));
                                return;
                            }

                            string originalSupervisorId = device.Attributes["supervisor_id"].Value;

                            if (supervisorId == SERIAL_SUPERVISOR_ID)
                            {
                                device.Attributes["supervisor_id"].Value = SNMP_SUPERVISOR_ID.ToString(CultureInfo.InvariantCulture);

                                if (deviceReport.Length > 0)
                                {
                                    deviceReport.Append(", ");
                                }

                                deviceReport.AppendFormat("modificato attributo 'supervisor_id' da '{0}' a '{1}'", originalSupervisorId,
                                    device.Attributes["supervisor_id"].Value);

                                deviceUpdated = true;
                            }
                            else
                            {
                                deviceReport.AppendFormat("attributo 'supervisor_id', con valore '{0}' ({1}) non modificato", originalSupervisorId,
                                    DecodeSupervisorID(supervisorId));
                            }
                        }

                        #endregion

                        #region Salvataggio e log

                        if (deviceUpdated)
                        {
                            modifiedDevices++;

                            try
                            {
                                docSystemXml.Save(systemXmlFilename);
                            }
                            catch (XmlException ex)
                            {
                                AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}",
                                    systemXmlFilename, ex.Message));
                            }

                            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                "Aggiornato nodo di tipo 'device', DevID '{0}', name '{1}':\r\n{2}.", device.Attributes["DevID"].Value,
                                device.Attributes["name"].Value, deviceReport));
                        }

                        #endregion

                        #region Ricerca esistenza periferica STLC1000 e log

                        if (device.Attributes["type"].Value.Equals("STLC1000"))
                        {
                            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                "Dispositivo STLC1000 gia' presente con Id: {0}. Niente da fare.", device.Attributes["DevID"].Value));
                            stlcDeviceFound = true;
                        }

                        #endregion
                    }
                }

                #endregion

                #region Log periferiche SNMP aggiornate

                if (modifiedDevices == 0)
                {
                    AppendStringToFile("Non sono stati trovati nodi 'device' da aggiornare.");
                }
                else if (modifiedDevices == 1)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Aggiornato {0} nodo 'device'.", modifiedDevices));
                }
                else
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Aggiornati {0} nodi 'device'.", modifiedDevices));
                }

                #endregion

                #region Il DeviceID dell'STLC1000 è uguale a 0 se non sono presenti altre periferiche, oppure all'ultimo DevID esistente + 1

                int newStlcDevId = existingMaxDeviceId + 1;

                if (!stlcDeviceFound)
                {
                    XmlNode stlcDeviceNode = docSystemXml.CreateNode("element", "device", string.Empty);

                    // <device DevID="104" name="STLC remoto test Bologna" station="11" building="0" location="1" position="1,1"
                    // addr="10.102.212.184" SN="00000000" port="2" type="STLC1000" profile="1" active="true" scheduled="true" snmp_community="public"
                    // supervisor_id="1" fake_local_data="true" />

                    XmlAttribute devIdAttribute = docSystemXml.CreateAttribute("DevID");
                    devIdAttribute.Value = newStlcDevId.ToString();
                    stlcDeviceNode.Attributes.Append(devIdAttribute);

                    XmlAttribute nameAttribute = docSystemXml.CreateAttribute("name");
                    nameAttribute.Value = string.Format("STLC1000 {0}", (string.IsNullOrEmpty(stationNameString) ? string.Empty : stationNameString));
                    stlcDeviceNode.Attributes.Append(nameAttribute);

                    XmlAttribute stationAttribute = docSystemXml.CreateAttribute("station");
                    stationAttribute.Value = stationIdString;
                    stlcDeviceNode.Attributes.Append(stationAttribute);

                    XmlAttribute buildingAttribute = docSystemXml.CreateAttribute("building");
                    buildingAttribute.Value = buildingIdString;
                    stlcDeviceNode.Attributes.Append(buildingAttribute);

                    XmlAttribute locationAttribute = docSystemXml.CreateAttribute("location");
                    locationAttribute.Value = rackIdString;
                    stlcDeviceNode.Attributes.Append(locationAttribute);

                    XmlAttribute positionAttribute = docSystemXml.CreateAttribute("position");
                    positionAttribute.Value = "1,1";
                    stlcDeviceNode.Attributes.Append(positionAttribute);

                    XmlAttribute addrAttribute = docSystemXml.CreateAttribute("addr");
                    addrAttribute.Value = stlcAddr;
                    stlcDeviceNode.Attributes.Append(addrAttribute);

                    XmlAttribute snAttribute = docSystemXml.CreateAttribute("SN");
                    snAttribute.Value = "00000000";
                    stlcDeviceNode.Attributes.Append(snAttribute);

                    XmlAttribute portAttribute = docSystemXml.CreateAttribute("port");
                    portAttribute.Value = snmpPort;
                    stlcDeviceNode.Attributes.Append(portAttribute);

                    XmlAttribute typeAttribute = docSystemXml.CreateAttribute("type");
                    typeAttribute.Value = "STLC1000";
                    stlcDeviceNode.Attributes.Append(typeAttribute);

                    XmlAttribute profileAttribute = docSystemXml.CreateAttribute("profile");
                    profileAttribute.Value = "1";
                    stlcDeviceNode.Attributes.Append(profileAttribute);

                    XmlAttribute activeAttribute = docSystemXml.CreateAttribute("active");
                    activeAttribute.Value = "true";
                    stlcDeviceNode.Attributes.Append(activeAttribute);

                    XmlAttribute scheduledAttribute = docSystemXml.CreateAttribute("scheduled");
                    scheduledAttribute.Value = "true";
                    stlcDeviceNode.Attributes.Append(scheduledAttribute);

                    XmlAttribute snmpCommunityAttribute = docSystemXml.CreateAttribute("snmp_community");
                    snmpCommunityAttribute.Value = "public";
                    stlcDeviceNode.Attributes.Append(snmpCommunityAttribute);

                    XmlAttribute supervisorIdAttribute = docSystemXml.CreateAttribute("supervisor_id");
                    supervisorIdAttribute.Value = SNMP_SUPERVISOR_ID.ToString();
                    stlcDeviceNode.Attributes.Append(supervisorIdAttribute);

                    node.AppendChild(stlcDeviceNode);

                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Aggiunta periferica STLC1000 - Device Id: {0}, StationId: {1}, BuildingId: {2}, LocationId: {3}, Address: {4}, nel file '{5}'",
                        newStlcDevId, stationIdString, buildingIdString, rackIdString, stlcAddr, systemXmlFilename));

                    try
                    {
                        docSystemXml.Save(systemXmlFilename);
                    }
                    catch (XmlException ex)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}",
                            systemXmlFilename, ex.Message));
                    }
                }

                #endregion
            }

            #endregion

            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                "--------------------------------------------------------------------------------"));

            #region Aggiornamento STLCManagerConfig.xml

            XmlNode serviceElement = GetStlcManagerConfigServiceElement(stlcManagerXmlFilename);

            if (serviceElement != null)
            {
                #region Esegue ricerca nodo per servizio da aggiungere ed elimina servizio obsoleto

                foreach (XmlNode item in serviceElement.ChildNodes)
                {
                    if (item.Name.Equals("item"))
                    {
                        if (item.Attributes["name"] != null)
                        {
                            if (item.Attributes["name"].Value.Equals("SNMPManagerService", StringComparison.OrdinalIgnoreCase))
                            {
                                serviceElement.RemoveChild(item);

                                try
                                {
                                    docStlcManagerConfigXml.Save(stlcManagerXmlFilename);
                                }
                                catch (XmlException ex)
                                {
                                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}",
                                        stlcManagerXmlFilename, ex.Message));
                                }

                                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                    "Disattivato servizio SNMPManagerService su STLC Manager, nel file '{0}'", stlcManagerXmlFilename));
                            }
                        }
                    }
                }

                #endregion

                #region Esegue stessa ricerca precedente, per contemplare il caso in cui siano presenti entrambi i servizi e il foreach sia stato modificato durante l'elaborazione

                foreach (XmlNode item in serviceElement.ChildNodes)
                {
                    if (item.Name.Equals("item"))
                    {
                        if (item.Attributes["name"] != null)
                        {
                            if (item.Attributes["name"].Value.Equals("SNMPManagerService", StringComparison.OrdinalIgnoreCase))
                            {
                                serviceElement.RemoveChild(item);

                                try
                                {
                                    docStlcManagerConfigXml.Save(stlcManagerXmlFilename);
                                }
                                catch (XmlException ex)
                                {
                                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}",
                                        stlcManagerXmlFilename, ex.Message));
                                }

                                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                                    "Disattivato servizio SNMPManagerService su STLC Manager, nel file '{0}'", stlcManagerXmlFilename));
                            }
                        }
                    }
                }

                #endregion
            }

            #endregion

            AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                "--------------------------------------------------------------------------------"));

            #region Aggiornamento device_type_list.xml

            XmlNode deviceTypeElement = GetDeviceTypeListDeviceTypeElement(deviceTypeListFilename);

            // 		<type name="INFSTAZ1 - Monitor InfoStazioni" default_name="Monitor" code="INFSTAZ1" port_type="TCP_Client" snmp_community="public"/>
            AddDeviceType(deviceTypeListFilename, deviceTypeElement, "INFSTAZ1", "INFSTAZ1 - Monitor InfoStazioni", "Monitor", "TCP_Client", "public");

            // 		<type name="Telefin STLC1000 - Concentratore Diagnostica" default_name="STLC1000" code="STLC1000" port_type="TCP_Client" snmp_community="public"/>
            AddDeviceType(deviceTypeListFilename, deviceTypeElement, "STLC1000", "Telefin STLC1000 - Concentratore Diagnostica", "STLC1000",
                "TCP_Client", "public");

            // 		<type name="WRKWIN10 - Postazione di lavoro Windows" default_name="Postazione di lavoro" code="WRKWIN10" port_type="TCP_Client" snmp_community="public"/>
            AddDeviceType(deviceTypeListFilename, deviceTypeElement, "WRKWIN10", "WRKWIN10 - Postazione di lavoro Windows", "Postazione di lavoro",
                "TCP_Client", "public");

            #endregion
        }

        #region AddDeviceType - Aggiunge un tipo periferica, se non presente, su device_type_list.xml

        private static void AddDeviceType(string deviceTypeListFilename,
            XmlNode deviceTypeElement,
            string deviceTypeCode,
            string deviceTypeName,
            string deviceTypeDefaultName,
            string deviceTypePort,
            string deviceTypeCommunity)
        {
            bool isDeviceTypeAvailable = false;

            if (deviceTypeElement != null)
            {
                foreach (XmlNode type in deviceTypeElement.ChildNodes)
                {
                    if (type.Name.Equals("type"))
                    {
                        if (type.Attributes != null && type.Attributes["code"] != null)
                        {
                            if (type.Attributes["code"].Value.Equals(deviceTypeCode, StringComparison.OrdinalIgnoreCase))
                            {
                                isDeviceTypeAvailable = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (!isDeviceTypeAvailable)
            {
                if (deviceTypeElement != null)
                {
                    XmlNode deviceTypeInfostazioniNode = docDeviceTypeListXml.CreateNode("element", "type", string.Empty);
                    XmlAttribute nameAttribute = docDeviceTypeListXml.CreateAttribute("name");
                    nameAttribute.Value = deviceTypeName;
                    if (deviceTypeInfostazioniNode.Attributes != null)
                    {
                        deviceTypeInfostazioniNode.Attributes.Append(nameAttribute);

                        XmlAttribute defaultNameAttribute = docDeviceTypeListXml.CreateAttribute("default_name");
                        defaultNameAttribute.Value = deviceTypeDefaultName;
                        deviceTypeInfostazioniNode.Attributes.Append(defaultNameAttribute);

                        XmlAttribute codeAttribute = docDeviceTypeListXml.CreateAttribute("code");
                        codeAttribute.Value = deviceTypeCode;
                        deviceTypeInfostazioniNode.Attributes.Append(codeAttribute);

                        XmlAttribute portTypeAttribute = docDeviceTypeListXml.CreateAttribute("port_type");
                        portTypeAttribute.Value = deviceTypePort;
                        deviceTypeInfostazioniNode.Attributes.Append(portTypeAttribute);

                        XmlAttribute communityAttribute = docDeviceTypeListXml.CreateAttribute("snmp_community");
                        communityAttribute.Value = deviceTypeCommunity;
                        deviceTypeInfostazioniNode.Attributes.Append(communityAttribute);
                    }

                    deviceTypeElement.AppendChild(deviceTypeInfostazioniNode);

                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Aggiunto tipo periferica '{0}' su lista periferiche, nel file '{1}'", deviceTypeCode, deviceTypeListFilename));

                    try
                    {
                        docDeviceTypeListXml.Save(deviceTypeListFilename);
                    }
                    catch (XmlException ex)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture, "Errore nel salvataggio del file: '{0}'.\r\n{1}",
                            deviceTypeListFilename, ex.Message));
                    }
                }
            }
            else
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Tipo periferica '{0}' gia' presente su lista periferiche, nel file '{1}'", deviceTypeCode, deviceTypeListFilename));
            }
        }

        #endregion

        #region GetNewDeviceTypeFromCurrentType - Converte tipo periferica da Teleindicatori a InfoStazioni

        private static string GetNewDeviceTypeFromCurrentType(bool convertToInfostazioni, string deviceType)
        {
            const string deviceInfostazioniType = "INFSTAZ1";

            if (convertToInfostazioni)
            {
                if (deviceType.Equals("AEL0000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("AET0000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SOCM000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SOLM000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SOTM000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SOXM200", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYCMH00", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYLB000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYLF000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYLM000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYLQ000", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYTM100", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                if (deviceType.Equals("SYTM200", StringComparison.OrdinalIgnoreCase))
                {
                    return deviceInfostazioniType;
                }

                return deviceType;
            }

            return deviceType;
        }

        #endregion

        #region GetNewCommunity - Converte community se tipo InfoStazioni

        private static string GetNewCommunity(bool convertToInfostazioni, string deviceType, string currentCommunity)
        {
            const string deviceInfostazioniType = "INFSTAZ1";

            if (convertToInfostazioni)
            {
                if (deviceType.StartsWith("XXIP", StringComparison.OrdinalIgnoreCase))
                {
                    return null;
                }

                if (deviceType.Equals(deviceInfostazioniType, StringComparison.OrdinalIgnoreCase))
                {
                    return "public";
                }

                return currentCommunity;
            }

            return currentCommunity;
        }

        #endregion

        #region GetSystemXmlNodeElement - Recupera e valida dati per System.xml

        private static XmlNode GetSystemXmlNodeElement(string systemXmlFilename,
            out string snmpPort,
            out string nodId,
            out string srvId,
            out XmlNode stationNode)
        {
            XmlNode root;
            snmpPort = null;
            nodId = null;
            srvId = null;
            stationNode = null;

            try
            {
                docSystemXml.Load(systemXmlFilename);
                root = docSystemXml.DocumentElement;
            }
            catch (XmlException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.\r\nEccezione: {1}", systemXmlFilename,
                    ex.Message));
                return null;
            }
            catch (ArgumentException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.\r\nEccezione: {1}", systemXmlFilename,
                    ex.Message));
                return null;
            }
            catch (IOException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.\r\nEccezione: {1}", systemXmlFilename,
                    ex.Message));
                return null;
            }
            catch (UnauthorizedAccessException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.\r\nEccezione: {1}", systemXmlFilename,
                    ex.Message));
                return null;
            }
            catch (NotSupportedException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.\r\nEccezione: {1}", systemXmlFilename,
                    ex.Message));
                return null;
            }
            catch (SecurityException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.\r\nEccezione: {1}", systemXmlFilename,
                    ex.Message));
                return null;
            }

            if (root == null)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la definizione XML della periferica.", systemXmlFilename));
                return null;
            }

            if (!root.Name.Equals("telefin", StringComparison.OrdinalIgnoreCase))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'telefin' come nodo radice del file di configurazione '{0}'.", systemXmlFilename));
                return null;
            }

            XmlNode node;
            try
            {
                node = root.SelectSingleNode("./system/server/region/zone/node");

                if (node == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'node' nel file di configurazione '{0}', nel percorso /system/server/region/zone/node.",
                        systemXmlFilename));
                    return null;
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'node' nel file di configurazione '{0}', nel percorso /system/server/region/zone/node.",
                    systemXmlFilename));
                return null;
            }

            XmlNode snmpPortNode;
            try
            {
                snmpPortNode = root.SelectSingleNode("./port");

                if (snmpPortNode == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'port' nel file di configurazione '{0}', nel percorso /telefin/port.", systemXmlFilename));
                    return null;
                }

                foreach (XmlNode portItem in snmpPortNode.ChildNodes)
                {
                    if (portItem.Attributes != null &&
                        ((portItem.Name.Equals("item", StringComparison.OrdinalIgnoreCase)) && (portItem.Attributes["name"] != null)))
                    {
                        if (portItem.Attributes["name"].Value.IndexOf("SNMP Manager Locale", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            if (portItem.Attributes["id"] != null)
                            {
                                snmpPort = portItem.Attributes["id"].Value;
                            }
                            break;
                        }

                        if (portItem.Attributes["name"].Value.IndexOf("Rete TCP/IP", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            if (portItem.Attributes["id"] != null)
                            {
                                snmpPort = portItem.Attributes["id"].Value;
                            }
                            break;
                        }
                    }
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'port' nel file di configurazione '{0}', nel percorso /telefin/port.", systemXmlFilename));
                return null;
            }

            if (string.IsNullOrEmpty(snmpPort))
            {
                return null;
            }

            try
            {
                XmlNode nodIdNode = root.SelectSingleNode("./system/server/region/zone/node");

                if (nodIdNode == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'node' nel file di configurazione '{0}', nel percorso /system/server/region/zone/node.",
                        systemXmlFilename));
                    return null;
                }

                XmlNode zoneIdNode = root.SelectSingleNode("./system/server/region/zone");

                if (zoneIdNode == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'zone' nel file di configurazione '{0}', nel percorso /system/server/region/zone.",
                        systemXmlFilename));
                    return null;
                }

                XmlNode regionIdNode = root.SelectSingleNode("./system/server/region");

                if (regionIdNode == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'region' nel file di configurazione '{0}', nel percorso /system/server/region.",
                        systemXmlFilename));
                    return null;
                }

                if (regionIdNode.Attributes != null &&
                    (zoneIdNode.Attributes != null &&
                     (nodIdNode.Attributes != null &&
                      ((nodIdNode.Attributes["NodID"] != null) && (!string.IsNullOrEmpty(nodIdNode.Attributes["NodID"].Value)) &&
                       (zoneIdNode.Attributes["ZonID"] != null) && (!string.IsNullOrEmpty(zoneIdNode.Attributes["ZonID"].Value)) &&
                       (regionIdNode.Attributes["RegID"] != null) && (!string.IsNullOrEmpty(regionIdNode.Attributes["RegID"].Value))))))
                {
                    try
                    {
                        nodId =
                            new NodeIdentifier(ushort.Parse(nodIdNode.Attributes["NodID"].Value), ushort.Parse(zoneIdNode.Attributes["ZonID"].Value),
                                ushort.Parse(regionIdNode.Attributes["RegID"].Value)).NodeId.ToString();
                    }
                    catch (ArgumentException)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile recuperare il NodID, ZonID o RegID come valore numerico intero nel file di configurazione '{0}'.",
                            systemXmlFilename));
                        return null;
                    }
                    catch (FormatException)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile recuperare il NodID, ZonID o RegID come valore numerico intero nel file di configurazione '{0}'.",
                            systemXmlFilename));
                        return null;
                    }
                    catch (OverflowException)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile recuperare il NodID, ZonID o RegID come valore numerico intero nel file di configurazione '{0}'.",
                            systemXmlFilename));
                        return null;
                    }
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'node', 'zone' o 'region' nel file di configurazione '{0}', nel percorso /system/server.",
                    systemXmlFilename));
                return null;
            }

            if (string.IsNullOrEmpty(nodId))
            {
                return null;
            }

            try
            {
                XmlNode srvIdNode = root.SelectSingleNode("./system/server");

                if (srvIdNode == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'server' nel file di configurazione '{0}', nel percorso /system/server.", systemXmlFilename));
                    return null;
                }

                if (srvIdNode.Attributes != null &&
                    ((srvIdNode.Attributes["SrvID"] != null) && (!string.IsNullOrEmpty(srvIdNode.Attributes["SrvID"].Value))))
                {
                    try
                    {
                        srvId = long.Parse(srvIdNode.Attributes["SrvID"].Value).ToString();
                    }
                    catch (ArgumentException)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile recuperare il SrvID come valore numerico intero nel file di configurazione '{0}'.", systemXmlFilename));
                        return null;
                    }
                    catch (FormatException)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile recuperare il SrvID come valore numerico intero nel file di configurazione '{0}'.", systemXmlFilename));
                        return null;
                    }
                    catch (OverflowException)
                    {
                        AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile recuperare il SrvID come valore numerico intero nel file di configurazione '{0}'.", systemXmlFilename));
                        return null;
                    }
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'server' nel file di configurazione '{0}', nel percorso /system/server.", systemXmlFilename));
                return null;
            }

            if (string.IsNullOrEmpty(srvId))
            {
                return null;
            }

            try
            {
                stationNode = root.SelectSingleNode("./topography/station");

                if (stationNode == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'station' nel file di configurazione '{0}', nel percorso /topography/station.",
                        systemXmlFilename));
                    return null;
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'station' nel file di configurazione '{0}', nel percorso /topography/station.", systemXmlFilename));
                return null;
            }

            return node;
        }

        #endregion

        #region GetStlcManagerConfigServiceElement - Recupera e valida dati per System.xml

        private static XmlNode GetStlcManagerConfigServiceElement(string stlcManagerXmlFilename)
        {
            XmlNode root;

            try
            {
                docStlcManagerConfigXml.Load(stlcManagerXmlFilename);
                root = docStlcManagerConfigXml.DocumentElement;
            }
            catch (XmlException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.\r\nEccezione: {1}",
                    stlcManagerXmlFilename, ex.Message));
                return null;
            }
            catch (ArgumentException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.\r\nEccezione: {1}",
                    stlcManagerXmlFilename, ex.Message));
                return null;
            }
            catch (IOException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.\r\nEccezione: {1}",
                    stlcManagerXmlFilename, ex.Message));
                return null;
            }
            catch (UnauthorizedAccessException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.\r\nEccezione: {1}",
                    stlcManagerXmlFilename, ex.Message));
                return null;
            }
            catch (NotSupportedException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.\r\nEccezione: {1}",
                    stlcManagerXmlFilename, ex.Message));
                return null;
            }
            catch (SecurityException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.\r\nEccezione: {1}",
                    stlcManagerXmlFilename, ex.Message));
                return null;
            }

            if (root == null)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la configurazione del servizio STLC Manager.", stlcManagerXmlFilename));
                return null;
            }

            if (!root.Name.Equals("telefin", StringComparison.OrdinalIgnoreCase))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'telefin' come nodo radice del file di configurazione '{0}'.", stlcManagerXmlFilename));
                return null;
            }

            XmlNode service;
            try
            {
                service = root.SelectSingleNode("./manager/service");

                if (service == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'service' nel file di configurazione '{0}', nel percorso /manager/service.",
                        stlcManagerXmlFilename));
                    return null;
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'service' nel file di configurazione '{0}', nel percorso /manager/service.",
                    stlcManagerXmlFilename));
                return null;
            }

            return service;
        }

        #endregion

        #region GetStlcManagerConfigServiceElement - Recupera e valida dati per System.xml

        private static XmlNode GetDeviceTypeListDeviceTypeElement(string docDeviceTypeListFilename)
        {
            XmlNode root;

            try
            {
                docDeviceTypeListXml.Load(docDeviceTypeListFilename);
                root = docDeviceTypeListXml.DocumentElement;
            }
            catch (XmlException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.\r\nEccezione: {1}", docDeviceTypeListFilename,
                    ex.Message));
                return null;
            }
            catch (ArgumentException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.\r\nEccezione: {1}", docDeviceTypeListFilename,
                    ex.Message));
                return null;
            }
            catch (IOException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.\r\nEccezione: {1}", docDeviceTypeListFilename,
                    ex.Message));
                return null;
            }
            catch (UnauthorizedAccessException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.\r\nEccezione: {1}", docDeviceTypeListFilename,
                    ex.Message));
                return null;
            }
            catch (NotSupportedException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.\r\nEccezione: {1}", docDeviceTypeListFilename,
                    ex.Message));
                return null;
            }
            catch (SecurityException ex)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.\r\nEccezione: {1}", docDeviceTypeListFilename,
                    ex.Message));
                return null;
            }

            if (root == null)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file '{0}', con la lista dei tipi periferica.", docDeviceTypeListFilename));
                return null;
            }

            if (!root.Name.Equals("telefin", StringComparison.OrdinalIgnoreCase))
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'Telefin' come nodo radice del file di configurazione '{0}'.", docDeviceTypeListFilename));
                return null;
            }

            XmlNode deviceType;
            try
            {
                deviceType = root.SelectSingleNode("./device_type");

                if (deviceType == null)
                {
                    AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile trovare l'elemento 'device_type' nel file di configurazione '{0}', nel percorso /device_type.",
                        docDeviceTypeListFilename));
                    return null;
                }
            }
            catch (XPathException)
            {
                AppendStringToFile(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'device_type' nel file di configurazione '{0}', nel percorso /device_type.",
                    docDeviceTypeListFilename));
                return null;
            }

            return deviceType;
        }

        #endregion

        #region CheckFileCanRead - Verifica se un file esiste su disco e se può essere aperto e letto

        /// <summary>
        ///     Verifica se un file esiste su disco e se può essere aperto e letto
        /// </summary>
        /// <param name="fileName">Nome del file completo</param>
        /// <returns>True se il file esiste e può essere correttamente letto. False altrimenti.</returns>
        public static bool CheckFileCanRead(string fileName)
        {
            bool returnValue;

            FileStream fs = null;
            try
            {
                if (!File.Exists(fileName))
                {
                    returnValue = false;
                }
                else
                {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    returnValue = fs.CanRead;
                }
            }
            catch (UnauthorizedAccessException)
            {
                returnValue = false;
            }
            catch (IOException)
            {
                returnValue = false;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }

            return returnValue;
        }

        #endregion

        #region GetRegistryKey - Recupera dati da Registry

        private static string GetRegistryKey(string keyName, string valueName, string defaultValue)
        {
            string keyValue = null;
            RegistryKey key = null;

            try
            {
                key = Registry.LocalMachine.OpenSubKey(keyName);
                if (key != null)
                {
                    keyValue = key.GetValue(valueName, defaultValue) as String;
                }
            }
            catch (ArgumentException)
            {
            }
            catch (SecurityException)
            {
            }
            catch (ObjectDisposedException)
            {
            }
            finally
            {
                if (key != null)
                {
                    key.Close();
                }
            }

            return keyValue;
        }

        #endregion

        #region GetSystemXmlPath - Recupera path del System.xml

        private static string GetSystemXmlPath()
        {
            string systemXmlPath1 = GetRegistryKey("SYSTEM\\CurrentControlSet\\Services\\StlcManagerService\\Paths", "XMLSystem", String.Empty);

            if (!String.IsNullOrEmpty(systemXmlPath1))
            {
                return systemXmlPath1;
            }

            string systemXmlPath2 = GetRegistryKey("SYSTEM\\CurrentControlSet\\Services\\StlcSPVService", "XMLSystem", String.Empty);

            if (!String.IsNullOrEmpty(systemXmlPath2))
            {
                return systemXmlPath2;
            }

            return "C:\\Program Files\\Telefin\\Config\\system.xml";
        }

        #endregion

        #region GetSTLCManagerXmlPath - Recupera path del STLCManagerConfig.xml

        private static string GetSTLCManagerXmlPath()
        {
            const string stlcManagerXmlDefaultPath = "C:\\Program Files\\Telefin\\STLCManager\\STLCManagerConfig.xml";

            string stlcManagerXmlPath = GetRegistryKey("SYSTEM\\CurrentControlSet\\Services\\STLCManagerService", "XMLConfig",
                stlcManagerXmlDefaultPath);

            if (string.IsNullOrEmpty(stlcManagerXmlPath))
            {
                stlcManagerXmlPath = stlcManagerXmlDefaultPath;
            }

            return stlcManagerXmlPath;
        }

        #endregion

        #region GetDeviceTypeListPath - Recupera path del device_type_list.xml

        private static string GetDeviceTypeListPath()
        {
            const string stlcManagerXmlDefaultPath = @"C:\Program Files\Telefin\SKC\xml\device_type_list.xml";

            return stlcManagerXmlDefaultPath;
        }

        #endregion

        #region BackupFile - Crea copia di backup di un file

        public static bool BackupFile(string oldFilename)
        {
            return BackupFile(oldFilename, false);
        }

        public static bool BackupFile(string oldFilename, bool removeOldFilename)
        {
            bool returnValue = false;

            if (File.Exists(oldFilename))
            {
                try
                {
                    FileInfo oldFileInfo = new FileInfo(oldFilename);
                    string oldFilenameFolder = Path.GetDirectoryName(oldFilename);

                    if (oldFilenameFolder != null)
                    {
                        string newFilename = Path.Combine(oldFilenameFolder,
                            string.Format(CultureInfo.InvariantCulture, "{0}_{1}",
                                DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture), Path.GetFileName(oldFilename)));

                        if (File.Exists(newFilename))
                        {
                            if ((File.GetAttributes(newFilename) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            {
                                File.SetAttributes(newFilename, FileAttributes.Normal);
                            }

                            File.Delete(newFilename);
                        }

                        oldFileInfo.CopyTo(newFilename);

                        if (removeOldFilename)
                        {
                            File.Delete(oldFilename);
                        }

                        returnValue = true;
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (IOException)
                {
                }
                catch (ObjectDisposedException)
                {
                }
                catch (ArgumentException)
                {
                }
                catch (SecurityException)
                {
                }
                catch (NotSupportedException)
                {
                }
            }

            return returnValue;
        }

        /// <summary>
        ///     Aggiunge una messaggio ad un file di testo su disco. In caso di errore nell'accesso al file, non fa nulla.
        /// </summary>
        /// <param name="text">Testo del messaggio</param>
        private static void AppendStringToFile(string text)
        {
            AppendStringToFile(text, true);
        }

        /// <summary>
        ///     Aggiunge una messaggio ad un file di testo su disco. In caso di errore nell'accesso al file, non fa nulla.
        /// </summary>
        /// <param name="text">Testo del messaggio</param>
        /// <param name="overwrite">Indica se sovrascrivere il file esistente</param>
        private static void AppendStringToFile(string text, bool overwrite)
        {
            if (!String.IsNullOrEmpty(logFilename))
            {
                try
                {
                    using (TextWriter fileWriter = new StreamWriter(logFilename, overwrite, Encoding.GetEncoding(1252)))
                    {
                        fileWriter.Write(text + Environment.NewLine);
                        fileWriter.Flush();
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (IOException)
                {
                }
                catch (ObjectDisposedException)
                {
                }
                catch (ArgumentException)
                {
                }
                catch (SecurityException)
                {
                }
            }
        }

        #endregion

        #region Recupera IP della prima scheda di rete dell'STLC (ETH1) - Intel

        private static string GetCurrentStlcIpAddressEth1()
        {
            string mac;

            return GetNetworkInterfaceIP("ETH1", out mac);
        }

        private static string GetNetworkInterfaceIP(string networkInterfaceName, out string physicalAddress)
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            if (nics.Length > 0)
            {
                foreach (NetworkInterface adapter in nics)
                {
                    if (string.Compare(adapter.Name, networkInterfaceName, true) == 0)
                    {
                        physicalAddress = adapter.GetPhysicalAddress().ToString();
                        IPInterfaceProperties properties = adapter.GetIPProperties();
                        if (properties.UnicastAddresses.Count > 0)
                        {
                            foreach (UnicastIPAddressInformation ipAddress in properties.UnicastAddresses)
                            {
                                if (ipAddress.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    return ipAddress.Address.ToString();
                                }
                            }
                        }
                    }
                }
            }

            physicalAddress = string.Empty;
            return string.Empty;
        }

        #endregion

        #region Decodifica testuale Id Supervisore numerico

        private static string DecodeSupervisorID(int supervisorId)
        {
            switch (supervisorId)
            {
                case SERIAL_SUPERVISOR_ID:
                    return "Seriale";
                case SNMP_SUPERVISOR_ID:
                    return "SNMP";
                case MODBUS_SUPERVISOR_ID:
                    return "ModBus";
                case TELNET_SUPERVISOR_ID:
                    return "Telnet";
                default:
                    return "N/D";
            }
        }

        #endregion
    }

    #region Struttura NodeIdentifier

    /// <summary>
    ///     Incapsula le logiche di codifica / decodifica di un Node Id da Database a System.xml e viceversa
    /// </summary>
    internal struct NodeIdentifier
    {
        /// <summary>
        ///     Node Id originale da System.xml
        /// </summary>
        public ushort OriginalNodeId;

        /// <summary>
        ///     Zone Id originale da System.xml
        /// </summary>
        public ushort OriginalZoneId;

        /// <summary>
        ///     Region Id originale da System.xml
        /// </summary>
        public ushort OriginalRegionId;

        /// <summary>
        ///     Node Id da database
        /// </summary>
        public long NodeId;

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="originalNodeId">Node Id originale da System.xml</param>
        /// <param name="originalZoneId">Zone Id originale da System.xml</param>
        /// <param name="originalRegionId">Region Id originale da System.xml</param>
        public NodeIdentifier(ushort originalNodeId, ushort originalZoneId, ushort originalRegionId)
        {
            this.NodeId = (long) (originalRegionId | ((ulong) originalZoneId << 16) | ((ulong) originalNodeId << 32));
            this.OriginalNodeId = originalNodeId;
            this.OriginalZoneId = originalZoneId;
            this.OriginalRegionId = originalRegionId;
        }
    }

    #endregion
}
//------------------------------------------------------------------------------
// <copyright file="IScriptManagerInternal.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
 
namespace System.Web.UI {
    using System;
    using System.Web.UI;

    internal interface IScriptManagerInternal {
        bool IsInAsyncPostBack {
            get;
        }

        void RegisterScriptControl<TScriptControl>(TScriptControl scriptControl)
            where TScriptControl : Control, IScriptControl;
        void RegisterScriptDescriptors(IScriptControl scriptControl);
    }
}

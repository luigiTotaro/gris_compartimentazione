//------------------------------------------------------------------------------
// <copyright file="MediaChapterCollection.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.Resources;

    [
    TypeConverter(typeof(EmptyStringExpandableObjectConverter))
    ]
    public class MediaChapterCollection : StateManagedCollection, IList<MediaChapter> {
        private static readonly Type[] _knownTypes = new Type[] { typeof(MediaChapter) };

        public MediaChapter this[int index] {
            get {
                return (MediaChapter)((IList)this)[index];
            }
        }

        public int Add(MediaChapter mediaChapter) {
            return ((IList)this).Add(mediaChapter);
        }

        public bool Contains(MediaChapter chapter) {
            return ((IList)this).Contains(chapter);
        }

        public void CopyTo(MediaChapter[] array, int index) {
            base.CopyTo(array, index);
        }

        protected override object CreateKnownType(int index) {
            if (index != 0) {
                throw new ArgumentOutOfRangeException("index", index, SilverlightWeb.MediaChapter_InvalidTypeIndex);
            }
            return new MediaChapter();
        }

        protected override Type[] GetKnownTypes() {
            return _knownTypes;
        }

        public int IndexOf(MediaChapter chapter) {
            return ((IList)this).IndexOf(chapter);
        }

        public void Insert(int index, MediaChapter mediaChapter) {
            ((IList)this).Insert(index, mediaChapter);
        }

        protected override void OnValidate(object value) {
            base.OnValidate(value);
            if (!(value is MediaChapter)) {
                throw new ArgumentException(SilverlightWeb.Silverlight_MediaChapterCollectionInvalidType);
            }
        }

        public void Remove(MediaChapter mediaChapter) {
            ((IList)this).Remove(mediaChapter);
        }

        public void RemoveAt(int index) {
            ((IList)this).RemoveAt(index);
        }

        protected override void SetDirtyObject(object o) {
            ((MediaChapter)o).SetDirty();
        }

        #region IList<MediaChapter> Members

        int IList<MediaChapter>.IndexOf(MediaChapter item) {
            return IndexOf(item);
        }

        void IList<MediaChapter>.Insert(int index, MediaChapter item) {
            Insert(index, item);
        }

        void IList<MediaChapter>.RemoveAt(int index) {
            RemoveAt(index);
        }

        MediaChapter IList<MediaChapter>.this[int index] {
            get {
                return (MediaChapter)((IList)this)[index];
            }
            set {
                ((IList)this)[index] = value;
            }
        }

        #endregion

        #region ICollection<MediaChapter> Members

        void ICollection<MediaChapter>.Add(MediaChapter item) {
            Add(item);
        }

        void ICollection<MediaChapter>.Clear() {
            Clear();
        }

        bool ICollection<MediaChapter>.Contains(MediaChapter item) {
            return Contains(item);
        }

        void ICollection<MediaChapter>.CopyTo(MediaChapter[] array, int arrayIndex) {
            CopyTo(array, arrayIndex);
        }

        int ICollection<MediaChapter>.Count {
            get {
                return Count;
            }
        }

        bool ICollection<MediaChapter>.IsReadOnly {
            get {
                return false;
            }
        }

        bool ICollection<MediaChapter>.Remove(MediaChapter item) {
            int count = Count;
            Remove(item);
            return count != Count;
        }

        #endregion

        #region IEnumerable<MediaChapter> Members

        IEnumerator<MediaChapter> IEnumerable<MediaChapter>.GetEnumerator() {
            foreach (MediaChapter chapter in this) {
                yield return chapter;
            }
        }

        #endregion
    }
}

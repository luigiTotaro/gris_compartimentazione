//------------------------------------------------------------------------------
// <copyright file="ScaleMode.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    public enum ScaleMode {
        None = 0,
        Zoom,
        Stretch
    }
}

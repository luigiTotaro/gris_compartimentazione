//------------------------------------------------------------------------------
// <copyright file="SilverlightPlugin.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.Resources;

    public class SilverlightPlugin {
        private AttributeCollection _attributes;
        private string _cssClass;
        private string _id;
        private HtmlAccess _htmlAccess;
        private int _maxFrameRate;
        private string _initParameters;
        private string _onError;
        private string _onLoad;
        private string _onResize;
        private string _onFullScreenChanged;
        private string _onSourceDownloadCompleted;
        private string _onSourceDownloadProgressChanged;
        private string _source;
        private string _splashScreenSource;
        private bool _autoUpgrade = true;
        private Version _minimumVersion;

        private const string _createObjectScriptStart = "Sys.UI.Silverlight.Control.createObject('";
        private const string _createObjectScriptMiddle = "', '";
        private const string _createObjectScriptEnd = "');";
        private const string _clientScriptStart = "\r\n<script type=\"text/javascript\">\r\n//<![CDATA[\r\n";
        private const string _clientScriptEnd = "//]]>\r\n</script>\r\n";
        internal const string SilverlightMimeType10 = "application/x-silverlight";
        internal const string SilverlightMimeType2 = "application/x-silverlight-2";

        internal static void AddParametersToRender(HtmlTextWriter writer,
            IDictionary<String, String> parameters,
            bool asAttributes) {

            Debug.Assert(writer != null, "HtmlTextWriter should not be null.");
            Debug.Assert(parameters != null, "Parameters should not be null.");

            foreach (KeyValuePair<String, String> entry in parameters) {
                if (!String.IsNullOrEmpty(entry.Key) && !String.IsNullOrEmpty(entry.Value)) {
                    if (asAttributes) {
                        writer.AddAttribute(entry.Key, entry.Value, true);
                    }
                    else {
                        writer.AddAttribute(HtmlTextWriterAttribute.Name, entry.Key, true);
                        writer.AddAttribute(HtmlTextWriterAttribute.Value, entry.Value, true);
                        writer.RenderBeginTag(HtmlTextWriterTag.Param);
                        writer.RenderEndTag();
                    }
                }
            }
        }

        private static void AppendCharAsUnicode(StringBuilder builder, char c) {
            builder.Append("\\u");
            builder.AppendFormat(CultureInfo.InvariantCulture, "{0:x4}", (int)c);
        }

        internal static string GetCreateObjectScript(string parentId, string html) {
            Debug.Assert(html != null, "HTML should never be null.");
            return _createObjectScriptStart + QuoteString(parentId) + _createObjectScriptMiddle +
                QuoteString(html) + _createObjectScriptEnd;
        }

        internal static string GetMimeType(Version version) {
            if ((version == null) || ((version.Major == 1) && (version.Minor == 0))) {
                return SilverlightMimeType10;
            }
            else {
                // 1.1 and up use silverlight 2.0
                return SilverlightMimeType2;
            }
        }

        internal static void RenderCreateObjectScript(Control control, HtmlTextWriter writer,
            string parentId, string html) {

            Debug.Assert(control != null, "Control should never be null.");
            Debug.Assert(html != null, "HTML should never be null.");
            Debug.Assert(!String.IsNullOrEmpty(parentId), "parentId should never be null or empty.");

            // render a <script> tag around the script, using the same logic Page/ClientScriptManager uses when
            // writing script blocks.
            writer.Write(_clientScriptStart);
            writer.WriteLineNoTabs(GetCreateObjectScript(parentId, html));
            writer.Write(_clientScriptEnd);
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity",
            Justification = "Method would be more complex if split into separate pieces.")]
        private static string QuoteString(string value) {
            // NOTE: Adapted from Microsoft.Web.Script.Serialization.JavaScriptString in Microsoft.Web.Extensions
            // Rather than use the public JavaScriptSerializer, this is more lightweight for perf.
            // It does not escape these characters: >"
            // Because for our purposes there's no reason to, and in fact >" will be common in the HTML we are encoding.
            // Since the html is more likely to contain double-quotes (") than single-quotes ('),
            // the string is inside single quotes.
            // Note: we DO encode "<", because this works around a Safari bug where it is interpreting the HTML
            // even though it is inside a javascript string. We dont need to encode ">" because just encoding "<"
            // prevents any of the string appearing to be the beginning of an HTML element.
            StringBuilder b = null;

            if (String.IsNullOrEmpty(value)) {
                return String.Empty;
            }

            int startIndex = 0;
            int count = 0;
            for (int i = 0; i < value.Length; i++) {
                char c = value[i];

                // Append the unhandled characters (that do not require special treament)
                // to the string builder when special characters are detected.
                if (c == '\r' || c == '\t' || c == '\'' || c == '\\' ||
                    c == '\n' || c == '\b' || c == '\f' || c == '<' || c < ' ') {
                    if (b == null) {
                        b = new StringBuilder(value.Length + 5);
                    }

                    if (count > 0) {
                        b.Append(value, startIndex, count);
                    }

                    startIndex = i + 1;
                    count = 0;
                }

                switch (c) {
                    case '<':
                        b.Append("\\u003c");
                        break;
                    case '\r':
                        b.Append("\\r");
                        break;
                    case '\t':
                        b.Append("\\t");
                        break;
                    case '\'':
                        b.Append("\\'");
                        break;
                    case '\\':
                        b.Append("\\\\");
                        break;
                    case '\n':
                        b.Append("\\n");
                        break;
                    case '\b':
                        b.Append("\\b");
                        break;
                    case '\f':
                        b.Append("\\f");
                        break;
                    default:
                        if (c < ' ') {
                            AppendCharAsUnicode(b, c);
                        }
                        else {
                            count++;
                        }
                        break;
                }
            }

            if (b == null) {
                return value;
            }

            if (count > 0) {
                b.Append(value, startIndex, count);
            }

            return b.ToString();
        }

        public SilverlightPlugin() {
        }

        public AttributeCollection Attributes {
            get {
                if (_attributes == null) {
                    _attributes = new AttributeCollection(new StateBag(true));
                }
                return _attributes;
            }
        }

        public virtual bool AutoUpgrade {
            get {
                return _autoUpgrade;
            }
            set {
                _autoUpgrade = value;
            }
        }

        public virtual Color Background {
            get; set;
        }
        
        public virtual string CssClass {
            get {
                return _cssClass ?? String.Empty;
            }
            set {
                _cssClass = value;
            }
        }

        public virtual bool EnableFrameRateCounter {
            get; set;
        }

        [SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", Justification = "Same as Silverlight Parameter Name.")]
        public virtual bool EnableGPUAcceleration {
            get; set;
        }

        public virtual bool EnableCacheVisualization {
            get; set;
        }

        public virtual HtmlAccess HtmlAccess {
            get {
                return _htmlAccess;
            }
            set {
                if ((value < HtmlAccess.SameDomain) || (value > HtmlAccess.Enabled)) {
                    throw new ArgumentOutOfRangeException("value");
                }
                _htmlAccess = value;
            }
        }

        public virtual bool EnableRedrawRegions {
            get; set;
        }

        public virtual string Height {
            get {
                return Style[HtmlTextWriterStyle.Height] ?? String.Empty;
            }
            set {
                Style[HtmlTextWriterStyle.Height] = value;
            }
        }

        public virtual string Id {
            get {
                return _id ?? String.Empty;
            }
            set {
                _id = value;
            }
        }

        public virtual string InitParameters {
            get {
                return _initParameters ?? String.Empty;
            }
            set {
                _initParameters = value;
            }
        }

        public virtual int MaxFrameRate {
            get {
                return _maxFrameRate;
            }
            set {
                if (value < 0) {
                    throw new ArgumentOutOfRangeException("value",
                        SilverlightWeb.Common_GreaterThanOrEqualToZero);
                }
                _maxFrameRate = value;
            }
        }

        public virtual string OnError {
            get {
                return _onError ?? String.Empty;
            }
            set {
                _onError = value;
            }
        }

        public virtual string OnFullScreenChanged {
            get {
                // NOTE: OnFullScreenChanged is the correct parameter name even though
                // the silverlight property for it is host.content.OnFullScreenChange (no 'd')
                return _onFullScreenChanged ?? String.Empty;
            }
            set {
                _onFullScreenChanged = value;
            }
        }

        public virtual string OnLoad {
            get {
                return _onLoad ?? String.Empty;
            }
            set {
                _onLoad = value;
            }
        }

        public virtual string OnResized {
            get {
                return _onResize ?? String.Empty;
            }
            set {
                _onResize = value;
            }
        }

        public virtual string OnSourceDownloadCompleted {
            get {
                return _onSourceDownloadCompleted ?? String.Empty;
            }
            set {
                _onSourceDownloadCompleted = value;
            }
        }
        
        public virtual string OnSourceDownloadProgressChanged {
            get {
                return _onSourceDownloadProgressChanged ?? String.Empty;
            }
            set {
                _onSourceDownloadProgressChanged = value;
            }
        }

        public virtual string Source {
            get {
                return _source ?? String.Empty;
            }
            set {
                _source = value;
            }
        }

        public virtual string SplashScreenSource {
            get {
                return _splashScreenSource ?? String.Empty;
            }
            set {
                _splashScreenSource = value;
            }
        }

        public virtual CssStyleCollection Style {
            get {
                return Attributes.CssStyle;
            }
        }

        public virtual Version MinimumVersion {
            get {
                if (_minimumVersion == null) {
                    _minimumVersion = new Version(1, 0);
                }
                return _minimumVersion;
            }
            set {
                _minimumVersion = value;
            }
        }

        public virtual string Width {
            get {
                return Style[HtmlTextWriterStyle.Width] ?? String.Empty;
            }
            set {
                Style[HtmlTextWriterStyle.Width] = value;
            }
        }

        public virtual bool Windowless {
            get; set;
        }

        protected virtual void AddAttributesToRender(HtmlTextWriter writer) {
            if (writer == null) {
                throw new ArgumentNullException("writer");
            }

            bool hasAttributes = (_attributes != null);

            if (!String.IsNullOrEmpty(Id) &&
                (!hasAttributes || (_attributes["id"] == null))) {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, Id, true);
            }

            if (!hasAttributes || _attributes["type"] == null) {
                writer.AddAttribute(HtmlTextWriterAttribute.Type, GetMimeType(MinimumVersion), true);
            }

            if (!String.IsNullOrEmpty(CssClass) &&
                (!hasAttributes || (_attributes["class"] == null))) {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass, true);
            }

            if (hasAttributes) {
                foreach (string key in _attributes.Keys) {
                    writer.AddAttribute(key, _attributes[key], true);
                }
            }
        }

        public virtual void CreateObject(Control control,
            HtmlTextWriter writer,
            string parentId) {

            if (control == null) {
                throw new ArgumentNullException("control");
            }
            if (control.Page == null) {
                throw new InvalidOperationException(SilverlightWeb.Common_PageCannotBeNull);
            }
            if (writer == null) {
                throw new ArgumentNullException("writer");
            }
            if (String.IsNullOrEmpty(parentId)) {
                throw new ArgumentNullException("parentId", SilverlightWeb.Common_NullOrEmpty);
            }

            StringBuilder sb = new StringBuilder(255);
            StringWriter sw = new StringWriter(sb, CultureInfo.InvariantCulture);
            Render(new HtmlTextWriter(sw));
            string html = sb.ToString();

            ScriptManager sm = ScriptManager.GetCurrent(control.Page);
            if (sm != null && sm.IsInAsyncPostBack) {
                // during async postbacks, the create object script must be registered as a startup script.
                string script = GetCreateObjectScript(parentId, html);

                ScriptManager.RegisterStartupScript(control,
                    typeof(SilverlightPlugin),
                    Guid.NewGuid().ToString(),
                    script,
                    true);
            }
            else {
                // during non-async requests, the script must be directly rendered into the writer so it is
                // adjacent to the placeholder it is created within, avoiding flicker.
                RenderCreateObjectScript(control, writer, parentId, html);
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Depends on other property values.")]
        protected virtual IDictionary<String, String> GetSilverlightParameters() {
            var parameters = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);

            if (!String.IsNullOrEmpty(SplashScreenSource)) {
                parameters.Add("SplashScreenSource", SplashScreenSource);
            }

            if (!String.IsNullOrEmpty(Source)) {
                parameters.Add("Source", Source);
            }

            Version version = MinimumVersion;
            if ((version != null) && ((version.Major > 1) || ((version.Major == 1) && (version.Minor > 0)))) {
                parameters.Add("MinRuntimeVersion", version.ToString());
            }

            if (!AutoUpgrade) {
                parameters.Add("AutoUpgrade", "false");
            }

            if (Background != Color.Empty) {
                parameters.Add("Background",
                    "#" + Background.ToArgb().ToString("X8", CultureInfo.InvariantCulture));
            }

            if (EnableFrameRateCounter) {
                parameters.Add("EnableFrameRateCounter", "True");
            }

            if (HtmlAccess != HtmlAccess.SameDomain) {
                parameters.Add("EnableHtmlAccess", (HtmlAccess == HtmlAccess.Enabled ? "True" : "False"));
            }

            if (EnableGPUAcceleration) {
                parameters.Add("EnableGPUAcceleration", "True");
            }

            if (EnableRedrawRegions) {
                parameters.Add("EnableRedrawRegions", "True");
            }

            if (EnableCacheVisualization) {
                parameters.Add("EnableCacheVisualization", "True");
            }

            if (!String.IsNullOrEmpty(InitParameters)) {
                parameters.Add("InitParams", InitParameters);
            }

            if (MaxFrameRate != 0) {
                parameters.Add("MaxFrameRate", MaxFrameRate.ToString(CultureInfo.InvariantCulture));
            }

            if (!String.IsNullOrEmpty(OnFullScreenChanged)) {
                parameters.Add("OnFullScreenChanged", OnFullScreenChanged);
            }

            if (!String.IsNullOrEmpty(OnError)) {
                parameters.Add("OnError", OnError);
            }

            if (!String.IsNullOrEmpty(OnLoad)) {
                parameters.Add("OnLoad", OnLoad);
            }

            if (!String.IsNullOrEmpty(OnSourceDownloadCompleted)) {
                parameters.Add("OnSourceDownloadCompleted", OnSourceDownloadCompleted);
            }

            if (!String.IsNullOrEmpty(OnSourceDownloadProgressChanged) ){
                parameters.Add("OnSourceDownloadProgressChanged", OnSourceDownloadProgressChanged);
            }

            if (!String.IsNullOrEmpty(OnResized)) {
                // NOTE: OnResize is the correct parameter name (does not end in 'd') even though
                // the param name for the full screen event is OnFullScreenChanged (with the 'd')
                parameters.Add("OnResize", OnResized);
            }

            if (Windowless) {
                parameters.Add("Windowless", "True");
            }

            return parameters;
        }

        protected virtual void Render(HtmlTextWriter writer) {
            if (writer == null) {
                throw new ArgumentNullException("writer");
            }

            AddAttributesToRender(writer);

            writer.RenderBeginTag(HtmlTextWriterTag.Object);

            // render parameters as param tags
            AddParametersToRender(writer, GetSilverlightParameters(), false);

            writer.RenderEndTag();
        }
    }
}
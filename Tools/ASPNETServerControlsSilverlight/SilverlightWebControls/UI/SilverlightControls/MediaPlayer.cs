//------------------------------------------------------------------------------
// <copyright file="MediaPlayer.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.Hosting;
    using System.Web.Resources;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Xml;
    using Microsoft.Web.Util;

    [
    Designer("System.Web.UI.Design.SilverlightControls.MediaPlayerDesigner, " + AssemblyRef.MicrosoftWebSilverlightDesign),
    ParseChildren(true),
    PersistChildren(false),
    ToolboxBitmap(typeof(EmbeddedResourceFinder), "System.Web.Resources.MediaPlayer.bmp"),
    ToolboxData("<{0}:MediaPlayer runat=\"server\" Width=\"320px\" Height=\"240px\"></{0}:MediaPlayer>"),
    ]
    public class MediaPlayer : Silverlight, IScriptControl {
        private const string _defaultSkinResourceName = "Classic.xaml";
        private const string _defaultScriptType = "Sys.UI.Silverlight.MediaPlayer";
        private static string _defaultSkinResourceUrl;
        private MediaChapterCollection _chapters;

        public MediaPlayer() : base() {
        }

        internal MediaPlayer(IScriptManagerInternal scriptManager) : base(scriptManager) {
        }

        [
        ResourceDescription("MediaPlayer_AutoLoad"),
        Category("Behavior"),
        DefaultValue(true),
        MergableProperty(true),
        Themeable(true)
        ]
        public virtual bool AutoLoad {
            get {
                object o = ViewState["AutoLoad"];
                return o == null ? true : (bool)o;
            }
            set {
                ViewState["AutoLoad"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_AutoPlay"),
        Category("Behavior"),
        DefaultValue(false),
        MergableProperty(true),
        Themeable(true)
        ]
        public virtual bool AutoPlay {
            get {
                object o = ViewState["AutoPlay"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["AutoPlay"] = value;
            }
        }


        [
        ResourceDescription("MediaPlayer_Chapters"),
        Category("Behavior"),
        Editor("System.Web.UI.Design.SilverlightControls.CollectionEditorBase, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        MergableProperty(false),
        Themeable(true)
        ]
        public virtual MediaChapterCollection Chapters {
            get {
                if (_chapters == null) {
                    _chapters = new MediaChapterCollection();
                    if (IsTrackingViewState) {
                        (_chapters as IStateManager).TrackViewState();
                    }
                }
                return _chapters;
            }
        }

        private string DefaultSkinResourceUrl {
            get {
                if (_defaultSkinResourceUrl == null) {
                    _defaultSkinResourceUrl = Page.ClientScript.GetWebResourceUrl(typeof(MediaPlayer), _defaultSkinResourceName);
                }
                return _defaultSkinResourceUrl;
            }
        }

        [
        ResourceDescription("MediaPlayer_EnableCaptions"),
        Category("Appearance"),
        DefaultValue(true),
        MergableProperty(true),
        Themeable(true)
        ]
        public virtual bool EnableCaptions {
            get {
                object o = ViewState["EnableCaptions"];
                return o == null ? true : (bool)o;
            }
            set {
                ViewState["EnableCaptions"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_MediaDefinition"),
        Category("Appearance"),
        DefaultValue(""),
        Themeable(true),
        Editor("System.Web.UI.Design.SilverlightControls.MediaDefinitionEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        UrlProperty("*.dat;*.xml")
        ]
        public virtual string MediaDefinition {
            get {
                return ((String)ViewState["MediaDefinition"]) ?? String.Empty;
            }
            set {
                ViewState["MediaDefinition"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_MediaSkinSource"),
        UrlProperty("*.xaml;*.xap"),
        Editor("System.Web.UI.Design.SilverlightControls.SourceEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        Category("Appearance"),
        DefaultValue(""),
        MergableProperty(true),
        Themeable(true)
        ]
        public virtual string MediaSkinSource {
            get {
                return base.Source;
            }
            set {
                base.Source = value;
            }
        }

        [
        Browsable(false),
        EditorBrowsable(EditorBrowsableState.Never)
        ]
        public override string Source {
            get {
                return MediaSkinSource;
            }
            set {
                MediaSkinSource = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_MediaSource"),
        Category("Appearance"),
        Editor("System.Web.UI.Design.SilverlightControls.MediaSourceEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        DefaultValue(""),
        UrlProperty("*.wmv;*.wma;*.mp3;*.asx"),
        Bindable(true),
        Themeable(true),
        ]
        public virtual string MediaSource {
            get {
                return ((String)ViewState["MediaSource"]) ?? String.Empty;
            }
            set {
                ViewState["MediaSource"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_Muted"),
        Category("Behavior"),
        DefaultValue(false),
        Themeable(true),
        ]
        public virtual bool Muted {
            get {
                object o = ViewState["Muted"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["Muted"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_PlaceholderSource"),
        Category("Appearance"),
        Editor("System.Web.UI.Design.SilverlightControls.ImageSourceEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        DefaultValue(""),
        UrlProperty("*.jpg;*.jpeg;*.png"),
        Bindable(true),
        Themeable(true),
        ]
        public virtual string PlaceholderSource {
            get {
                return ((String)ViewState["PlaceholderSource"]) ?? String.Empty;
            }
            set {
                ViewState["PlaceholderSource"] = value;
            }
        }

        [
        // Reuse Silverlight_ScaleMode
        ResourceDescription("Silverlight_ScaleMode"),
        Category("Appearance"),
        DefaultValue(ScaleMode.Zoom),
        Bindable(true),
        Themeable(true),
        ]
        public override ScaleMode ScaleMode {
            get {
                // overrides base to return Zoom as the default value
                object o = ViewState["ScaleMode"];
                return o == null ? ScaleMode.Zoom : (ScaleMode)o;
            }
            set {
                if (value < ScaleMode.None || value > ScaleMode.Stretch) {
                    throw new ArgumentOutOfRangeException("value");
                }
                ViewState["ScaleMode"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_Volume"),
        Category("Behavior"),
        Browsable(true),
        Themeable(true),
        DefaultValue(0.5D),
        Editor("System.Web.UI.Design.SilverlightControls.VolumeEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        ]
        public virtual double Volume {
            get {
                object o = ViewState["Volume"];
                return o == null ? 0.5D : (double)o;
            }
            set {
                if (value < 0 || value > 1) {
                    throw new ArgumentOutOfRangeException("value", value, SilverlightWeb.Common_GreaterThanOrEqualToZeroAndLessThanOrEqualToOne);
                }
                ViewState["Volume"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientChapterSelected"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientChapterSelected {
            get {
                return ((String)ViewState["OnClientChapterSelected"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientChapterSelected"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientChapterStarted"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientChapterStarted {
            get {
                return ((String)ViewState["OnClientChapterStarted"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientChapterStarted"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientCurrentStateChanged"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientCurrentStateChanged {
            get {
                return ((String)ViewState["OnClientCurrentStateChanged"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientCurrentStateChanged"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientMarkerReached"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientMarkerReached {
            get {
                return ((String)ViewState["OnClientMarkerReached"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientMarkerReached"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientMediaEnded"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientMediaEnded {
            get {
                return ((String)ViewState["OnClientMediaEnded"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientMediaEnded"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientMediaFailed"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientMediaFailed {
            get {
                return ((String)ViewState["OnClientMediaFailed"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientMediaFailed"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientMediaOpened"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientMediaOpened {
            get {
                return ((String)ViewState["OnClientMediaOpened"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientMediaOpened"] = value;
            }
        }

        [
        ResourceDescription("MediaPlayer_OnClientVolumeChanged"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnClientVolumeChanged {
            get {
                return ((String)ViewState["OnClientVolumeChanged"]) ?? String.Empty;
            }
            set {
                ViewState["OnClientVolumeChanged"] = value;
            }
        }

        protected override string DefaultScriptType {
            get {
                return _defaultScriptType;
            }
        }

        private void AppendChapterScript(StringBuilder sb, JavaScriptSerializer jss, string title,
            double position, string thumbnailSource) {
            sb.Append(jss.Serialize(title));
            sb.Append(",");
            sb.Append(jss.Serialize(position));
            sb.Append(",");
            string clientThumbnailSource = thumbnailSource;
            if (!String.IsNullOrEmpty(clientThumbnailSource)) {
                // silverlight requires urls not url encoded, but ResolveClientUrl url encodes,
                // so UrlDecode the result.
                clientThumbnailSource = HttpUtility.UrlDecode(ResolveClientUrl(clientThumbnailSource));
            }
            sb.Append(jss.Serialize(clientThumbnailSource));
        }

        private string GetMediaChapterScript(MediaChapterCollection chapters) {
            // returns a script used by the script descriptor to create client-side media chapter instances.
            // Sys.UI.Silverlight.MediaChapter._createChapters(chapterParams);
            // where chapterParams =
            // "chapterTitle1", position1, "thumbnailSource1", "chapterTitle2", position2, "thumbnailSource2", ... 
            if (chapters.Count != 0) {
                // make sure the chapters are written in chronological order
                MediaChapter[] sortedChapters = new MediaChapter[chapters.Count];
                chapters.CopyTo(sortedChapters, 0);
                Array.Sort<MediaChapter>(sortedChapters, new MediaChapterComparer());

                // initialize string builder to 128 to avoid certain reallocation, since
                // default size is 16.
                // 128 will avoid reallocations from 16,32,64 which are very likely to occur,
                // but doesn't allocate too much.
                StringBuilder sb = new StringBuilder(128);
                sb.Append("Sys.UI.Silverlight.MediaChapter._createChapters(");
                var first = true;
#pragma warning disable 618
                // serializer is marked obsolete in 3.5, but not in 3.5 SP1
                JavaScriptSerializer jss = new JavaScriptSerializer();
#pragma warning restore 618
                foreach (MediaChapter chapter in sortedChapters) {
                    if (!first) {
                        sb.Append(",");
                    }
                    first = false;
                    AppendChapterScript(sb, jss, chapter.Title, chapter.Position, chapter.ThumbnailSource);
                }
                sb.Append(")");
                return sb.ToString();
            }
            else {
                return null;
            }
        }

        private string GetMediaChapterScript(MediaDefinitionCache.MediaChapterDefinition[] chapters) {
            // returns a script used by the script descriptor to create client-side media chapter instances.
            // Sys.UI.Silverlight.MediaChapter._createChapters(chapterParams);
            // where chapterParams =
            // "chapterTitle1", position1, "thumbnailSource1", "chapterTitle2", position2, "thumbnailSource2", ... 
            if (chapters != null && chapters.Length > 0) {
                // MediaDefinition chapters are always sorted already

                // initialize string builder to 128 to avoid certain reallocation, since
                // default size is 16.
                // 128 will avoid reallocations from 16,32,64 which are very likely to occur,
                // but doesn't allocate too much.
                StringBuilder sb = new StringBuilder(128);
                sb.Append("Sys.UI.Silverlight.MediaChapter._createChapters(");
                var first = true;
#pragma warning disable 618
                // serializer is marked obsolete in 3.5, but not in 3.5 SP1
                JavaScriptSerializer jss = new JavaScriptSerializer();
#pragma warning restore 618
                foreach (MediaDefinitionCache.MediaChapterDefinition chapter in chapters) {
                    if (!first) {
                        sb.Append(",");
                    }
                    first = false;
                    AppendChapterScript(sb, jss, chapter.Title, chapter.Position, chapter.ThumbnailSource);
                }
                sb.Append(")");
                return sb.ToString();
            }
            else {
                return null;
            }
        }

        private MediaDefinitionCache GetMediaDefinition(string path) {
            path = ResolveUrl(path);

            VirtualPathProvider vpp = HostingEnvironment.VirtualPathProvider;
            if (!vpp.FileExists(path)) {
                throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
                    SilverlightWeb.MediaPlayer_InvalidMediaDefinitionPath, ID, path));
            }

            string cacheKey = null;
            string fileHash = null;
            MediaDefinitionCache def;
            if (Context != null) {
                //string definitionHash = vpp.GetFileHash(path);
                cacheKey = "MediaDefinition:" + path;
                cacheKey = cacheKey.ToUpper(CultureInfo.InvariantCulture);

                fileHash = vpp.GetFileHash(path, new String[] { path });

                def = Context.Cache[cacheKey] as MediaDefinitionCache;
                if (def != null && def.FileHash == fileHash) {
                    return def;
                }
            }

            VirtualFile file = vpp.GetFile(path);
            XmlDocument doc = new XmlDocument();
            using (Stream stream = file.Open()) {
                doc.Load(stream);
            }

            def = MediaDefinitionCache.ParseMediaDefinition(path, fileHash, doc);

            if (Context != null) {
                Context.Cache.Insert(cacheKey, def);
            }

            return def;
        }

        private MediaDefinitionCache.MediaItem GetMediaDefinitionItem(string path) {
            MediaDefinitionCache md = GetMediaDefinition(path);

            if (md.MediaItems != null && md.MediaItems.Length > 0) {
                return md.MediaItems[0];
            }
            else {
                return null;
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors() {
            ScriptDescriptor[] baseDescriptors = (ScriptDescriptor[]) base.GetScriptDescriptors();
            ScriptControlDescriptor scd = (ScriptControlDescriptor)baseDescriptors[0];

            // first process MediaDefinition if it exists
            string mediaSource = null;
            string placeholderSource = null;
            string chaptersScript = null;

            if (!String.IsNullOrEmpty(MediaDefinition)) {
                var item = GetMediaDefinitionItem(MediaDefinition);
                if (item != null) {
                    mediaSource = item.MediaSource;
                    placeholderSource = item.PlaceholderSource;
                    chaptersScript = GetMediaChapterScript(item.Chapters);
                }
            }

            // check for values not found in media definition and use them from the control instead
            if (String.IsNullOrEmpty(mediaSource) && !String.IsNullOrEmpty(MediaSource)) {
                mediaSource = MediaSource;
            }
            if (String.IsNullOrEmpty(placeholderSource) && !String.IsNullOrEmpty(PlaceholderSource)) {
                placeholderSource = PlaceholderSource;
            }
            if (String.IsNullOrEmpty(chaptersScript)) {
                chaptersScript = GetMediaChapterScript(Chapters);
            }

            // we need the urls to be resolved but not encoded, because it consumed with script not html
            // silverlight does not expect an encoded url. But ResolveClientUrl url encodes, so we UrlDecode.
            if (!String.IsNullOrEmpty(mediaSource)) {
                scd.AddProperty("mediaSource", HttpUtility.UrlDecode(ResolveClientUrl(mediaSource)));
            }
            if (!String.IsNullOrEmpty(placeholderSource)) {
                scd.AddProperty("placeholderSource", HttpUtility.UrlDecode(ResolveClientUrl(placeholderSource)));
            }
            if (!String.IsNullOrEmpty(chaptersScript)) {
                scd.AddScriptProperty("chapters", chaptersScript);
            }

            // End of values that might be in MediaDefinition

            if (String.IsNullOrEmpty(MediaSkinSource)) {
                // Use default skin if no skin set
                scd.AddProperty("source", DefaultSkinResourceUrl);
            }

            if (!EnableCaptions) {
                scd.AddProperty("enableCaptions", false);
            }

            if (!AutoLoad) {
                scd.AddProperty("autoLoad", false);
            }
            if (AutoPlay) {
                scd.AddProperty("autoPlay", true);
            }
            if (Muted) {
                scd.AddProperty("muted", true);
            }
            if (Volume != 0.5D) {
                scd.AddProperty("volume", Volume);
            }

            // Client events
            if (!String.IsNullOrEmpty(OnClientChapterSelected)) {
                scd.AddEvent("chapterSelected", OnClientChapterSelected);
            }
            if (!String.IsNullOrEmpty(OnClientChapterStarted)) {
                scd.AddEvent("chapterStarted", OnClientChapterStarted);
            }
            if (!String.IsNullOrEmpty(OnClientCurrentStateChanged)) {
                scd.AddEvent("currentStateChanged", OnClientCurrentStateChanged);
            }
            if (!String.IsNullOrEmpty(OnClientVolumeChanged)) {
                scd.AddEvent("volumeChanged", OnClientVolumeChanged);
            }
            if (!String.IsNullOrEmpty(OnClientMarkerReached)) {
                scd.AddEvent("markerReached", OnClientMarkerReached);
            }
            if (!String.IsNullOrEmpty(OnClientMediaEnded)) {
                scd.AddEvent("mediaEnded", OnClientMediaEnded);
            }
            if (!String.IsNullOrEmpty(OnClientMediaFailed)) {
                scd.AddEvent("mediaFailed", OnClientMediaFailed);
            }
            if (!String.IsNullOrEmpty(OnClientMediaOpened)) {
                scd.AddEvent("mediaOpened", OnClientMediaOpened);
            }

            return baseDescriptors;
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences() {
            List<ScriptReference> scripts = new List<ScriptReference>(base.GetScriptReferences());
            scripts.Add(new ScriptReference("SilverlightMedia.js", AssemblyRef.MicrosoftWebSilverlight.FullName));
            return scripts;
        }

        protected override void LoadViewState(object savedState) {
            if (savedState != null) {
                Pair pair = (Pair)savedState;
                base.LoadViewState(pair.First);
                ((IStateManager)Chapters).LoadViewState(pair.Second);
            }
            else {
                base.LoadViewState(null);
            }
        }

        protected override object SaveViewState() {
            object baseState = base.SaveViewState();
            object chapterState = ((IStateManager)Chapters).SaveViewState();

            if (baseState == null && chapterState == null) {
                return null;
            }
            return new Pair(baseState, chapterState);
        }

        protected override void TrackViewState() {
            base.TrackViewState();
            ((IStateManager)Chapters).TrackViewState();
        }

        private class MediaChapterComparer : IComparer<MediaChapter> {

            #region IComparer<MediaChapter> Members

            int IComparer<MediaChapter>.Compare(MediaChapter x, MediaChapter y) {
                if (x == y) {
                    return 0;
                }
                if (x == null) {
                    return -1;
                }
                if (y == null) {
                    return 1;
                }
                return x.Position.CompareTo(y.Position);
            }

            #endregion
        }
    }
}

//------------------------------------------------------------------------------
// <copyright file="MediaChapter.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing.Design;
    using System.Web.UI;
    using System.Web.Resources;
    using Microsoft.Web.Util;

    [
    ParseChildren(true, "Title")
    ]
    public class MediaChapter : IStateManager {
        private StateBag _viewState;

        public MediaChapter() {
            _viewState = new StateBag();
        }

        protected virtual bool IsTrackingViewState {
            get {
                return ((IStateManager)_viewState).IsTrackingViewState;
            }
        }

        [
        ResourceDescription("MediaChapter_Position"),
        Category("Behavior"),
        DefaultValue(0D)
        ]
        public double Position {
            get {
                object o = ViewState["Position"];
                if (o != null) {
                    return (Double)o;
                }
                return 0;
            }
            set {
                if (value < 0) {
                    throw new ArgumentOutOfRangeException("value", value, SilverlightWeb.Common_GreaterThanOrEqualToZero);
                }
                ViewState["Position"] = value;
            }
        }

        [
        ResourceDescription("MediaChapter_ThumbnailSource"),
        Category("Appearance"),
        DefaultValue(""),
        UrlProperty("*.jpg;*.jpeg;*.png"),
        Editor("System.Web.UI.Design.SilverlightControls.ImageSourceEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        ]
        public string ThumbnailSource {
            get {
                return ((String)ViewState["ThumbnailSource"]) ?? String.Empty;
            }
            set {
                ViewState["ThumbnailSource"] = value;
            }
        }

        [
        ResourceDescription("MediaChapter_Title"),
        Category("Appearance"),
        DefaultValue("")
        ]
        public string Title {
            get {
                return ((String)ViewState["Title"]) ?? String.Empty;
            }
            set {
                ViewState["Title"] = value;
            }
        }

        protected StateBag ViewState {
            get {
                return _viewState;
            }
        }

        protected virtual void LoadViewState(object state) {
            ((IStateManager)_viewState).LoadViewState(state);
        }

        protected virtual object SaveViewState() {
            return ((IStateManager)_viewState).SaveViewState();
        }

        public override string ToString() {
            return String.IsNullOrEmpty(Title) ? typeof(MediaChapter).Name : Title;
        }

        protected virtual void TrackViewState() {
            ((IStateManager)_viewState).TrackViewState();
        }

        protected internal virtual void SetDirty() {
            _viewState.SetDirty(true);
        }

        #region IStateManager Methods
        void IStateManager.LoadViewState(object state) {
            LoadViewState(state);
        }

        object IStateManager.SaveViewState() {
            return SaveViewState();
        }

        void IStateManager.TrackViewState() {
            TrackViewState();
        }

        bool IStateManager.IsTrackingViewState {
            get {
                return IsTrackingViewState;
            }
        }
        #endregion
    }
}

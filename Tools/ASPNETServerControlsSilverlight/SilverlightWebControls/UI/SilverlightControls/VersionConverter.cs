//------------------------------------------------------------------------------
// <copyright file="VersionConverter.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;

    // Provides StandardValues for the Silverlight.MinimumVersion property
    public class VersionConverter : TypeConverter {
        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context) {
            return new StandardValuesCollection(new String[] { "1.0", "2.0", "3.0" });
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) {
            // future silverlight versions are supported besides 1.0 and 2.0
            return false;
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context) {
            // versions 1.0-3.0 are returned as suggestions
            return true;
        }
    }
}

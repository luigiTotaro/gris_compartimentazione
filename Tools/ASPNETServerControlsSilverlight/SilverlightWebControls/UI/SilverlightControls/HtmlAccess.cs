//------------------------------------------------------------------------------
// <copyright file="HtmlAccess.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    public enum HtmlAccess {
        SameDomain = 0,
        Disabled,
        Enabled,
    }
}

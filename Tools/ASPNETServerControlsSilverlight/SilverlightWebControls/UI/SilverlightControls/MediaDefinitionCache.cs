﻿//------------------------------------------------------------------------------
// <copyright file="MediaDefinitionCache.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web;
    using System.Xml;

    internal class MediaDefinitionCache {
        public MediaItem[] MediaItems;
        public string FileHash;

        private static string CombinePaths(string basePath, string path) {
            // used for resolving relative paths in the media definition relative to the base path of the
            // media definition itself.
            if (!IsRelativeUrl(path)) {
                return path;
            }
            else {
                // strip off querystring if any before using VirtualPathUtility.Combine because it throws
                // with querystrings
                string query = null;
                string virtualPath = path;
                int queryIndex = path.IndexOf('?');
                if (queryIndex != -1) {
                    query = path.Substring(queryIndex + 1);
                    virtualPath = path.Substring(0, queryIndex);
                }
                path = VirtualPathUtility.Combine(basePath, virtualPath);
                // put querystring back on if any
                if (query != null) {
                    return path + "?" + query;
                }
                else {
                    return path;
                }
            }
        }

        private static bool IsRelativeUrl(string path) {
            // logic copied from asp.net resolveurl
            if (String.IsNullOrEmpty(path)) {
                return false;
            }

            if (path.IndexOf(':') != -1) {
                // url like http://... or file:// .. etc
                return false;
            }
            return !IsRooted(path);
        }

        private static bool IsRooted(string path) {
            // logic copied from asp.net resolveurl
            char first = path[0];
            if (first == '/' && first == '\\') {
                // urls starting with / or \ are "rooted"
                return true;
            }
            // doesnt start with / or \ so its relative
            return false;
        }

        private static int CompareMediaChapters(MediaChapterDefinition c1, MediaChapterDefinition c2) {
            // used for sorting the parsed chapters by Position into the cached MediaDefinition,
            // so this will only need to occur once
            return c1.Position.CompareTo(c2.Position);
        }

        public static MediaDefinitionCache ParseMediaDefinition(string basePath, string fileHash, XmlDocument document) {
            MediaDefinitionCache md = new MediaDefinitionCache();
            md.FileHash = fileHash;

            XmlElement root = document.DocumentElement;

            XmlNode node = root.SelectSingleNode("mediaItems");
            if (node != null) {
                md.MediaItems = ParseMediaItems(basePath, node);
            }
            return md;
        }

        private static MediaChapterDefinition ParseChapter(string basePath, XmlNode chapterNode) {
            MediaChapterDefinition mc = new MediaChapterDefinition();
                
            XmlAttribute attrib = chapterNode.Attributes["position"];
            if (attrib != null) {
                double position;
                if (Double.TryParse(attrib.Value,
                    NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowDecimalPoint,
                    CultureInfo.InvariantCulture, out position)) {

                    mc.Position = position;
                }
            }

            attrib = chapterNode.Attributes["thumbnailSource"];
            if (attrib != null) {
                mc.ThumbnailSource = CombinePaths(basePath, attrib.Value);
            }

            attrib = chapterNode.Attributes["title"];
            if (attrib != null) {
                mc.Title = attrib.Value;
            }

            return mc;
        }

        private static MediaChapterDefinition[] ParseChapters(string basePath, XmlNode chaptersRoot) {
            List<MediaChapterDefinition> chapters = new List<MediaChapterDefinition>();

            foreach (XmlNode node in chaptersRoot.SelectNodes("chapter")) {
                chapters.Add(ParseChapter(basePath, node));
            }

            chapters.Sort(CompareMediaChapters);
            return chapters.ToArray();
        }

        private static MediaItem[] ParseMediaItems(string basePath, XmlNode mediaItemsRoot) {
            List<MediaItem> mediaItems = new List<MediaItem>();

            foreach (XmlNode node in mediaItemsRoot.SelectNodes("mediaItem")) {
                mediaItems.Add(ParseMediaItem(basePath, node));
            }

            return mediaItems.ToArray();
        }

        private static MediaItem ParseMediaItem(string basePath, XmlNode mediaItemNode) {
            MediaItem mi = new MediaItem();

            XmlAttribute attrib = mediaItemNode.Attributes["mediaSource"];
            if (attrib != null) {
                mi.MediaSource = CombinePaths(basePath, attrib.Value);
            }
            attrib = mediaItemNode.Attributes["placeholderSource"];
            if (attrib != null) {
                mi.PlaceholderSource = CombinePaths(basePath, attrib.Value);
            }

            XmlNode node = mediaItemNode.SelectSingleNode("chapters");
            if (node != null) {
                mi.Chapters = ParseChapters(basePath, node);
            }

            return mi;
        }

        public class MediaItem {
            public MediaChapterDefinition[] Chapters;
            public string MediaSource;
            public string PlaceholderSource;
        }

        public class MediaChapterDefinition {
            public double Position;
            public string ThumbnailSource;
            public string Title;
        }
    }
}

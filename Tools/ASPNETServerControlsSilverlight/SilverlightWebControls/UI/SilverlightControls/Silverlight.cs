//------------------------------------------------------------------------------
// <copyright file="Silverlight.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.SilverlightControls {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.Resources;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Microsoft.Web.Util;

    [
    Designer("System.Web.UI.Design.SilverlightControls.SilverlightDesigner, " + AssemblyRef.MicrosoftWebSilverlightDesign),
    ParseChildren(true),
    PersistChildren(false),
    ToolboxBitmap(typeof(EmbeddedResourceFinder), "System.Web.Resources.Silverlight.bmp"),
    ToolboxData("<{0}:Silverlight runat=\"server\" Width=\"100px\" Height=\"100px\"></{0}:Silverlight>"),
    ]
    public class Silverlight : WebControl, IScriptControl {

        private static readonly Version _defaultSilverlightVersion = new Version(1, 0);
        private static readonly string _defaultSilverlightVersionString = _defaultSilverlightVersion.ToString();
        // pre attribute-encoded for perf
        private const string _silverlightInstallerUrl = "http://go2.microsoft.com/fwlink/?LinkID=141884&amp;v=";
        private const string _silverlightLogoUrl = "http://go2.microsoft.com/fwlink/?LinkID=108181";

        private IScriptManagerInternal _scriptManager;
        private IDictionary<String, String> _parameters;
        private ITemplate _pluginNotInstalledTemplate;
        private bool _versionCached;
        private Version _version;

        public Silverlight() : base(HtmlTextWriterTag.Object) {
        }

        internal Silverlight(IScriptManagerInternal scriptManager) : this() {
            _scriptManager = scriptManager;
        }

        private static void RenderDefaultTemplate(HtmlTextWriter writer, Version version) {
            // Renders default alternate content when a PluginNotInstalledTemplate is not provided.
            //<a href="http://go2.microsoft.com/fwlink/?LinkID=141884&v=[major.minor]">
            //<img style="border:0" src="http://go2.microsoft.com/fwlink/?LinkID=108181" alt="Get Silverlight"/>
            //</a>

            // <a>
            string versionParameter = (version == null ? _defaultSilverlightVersionString : version.ToString(2));
            writer.AddAttribute(HtmlTextWriterAttribute.Href, _silverlightInstallerUrl + versionParameter, false);
            writer.RenderBeginTag(HtmlTextWriterTag.A);

            // <img>
            writer.AddAttribute(HtmlTextWriterAttribute.Src, _silverlightLogoUrl, false);
            writer.AddAttribute(HtmlTextWriterAttribute.Alt, SilverlightWeb.Silverlight_GetSilverlight, true);
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0");
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();
            // </img>

            // </a>
            writer.RenderEndTag();
        }

        [
        ResourceDescription("Silverlight_AutoUpgrade"),
        Category("Behavior"),
        Browsable(true),
        DefaultValue(true),
        ]
        public virtual bool AutoUpgrade {
            get {
                object o = ViewState["AutoUpgrade"];
                return o == null ? true : (bool)o;
            }
            set {
                ViewState["AutoUpgrade"] = value;
            }
        }

        protected virtual string DefaultScriptType {
            get {
                return "Sys.UI.Silverlight.Control";
            }
        }

        [
        ResourceDescription("Silverlight_EnableCacheVisualization"),
        Category("Appearance"),
        Browsable(true),
        DefaultValue(false),
        ]
        public virtual bool EnableCacheVisualization {
            get {
                object o = ViewState["EnableCacheVisualization"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["EnableCacheVisualization"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_EnableFrameRateCounter"),
        Category("Appearance"),
        Browsable(true),
        DefaultValue(false),
        ]
        public virtual bool EnableFrameRateCounter {
            get {
                object o = ViewState["EnableFrameRateCounter"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["EnableFrameRateCounter"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_EnableGPUAcceleration"),
        Category("Behavior"),
        Browsable(true),
        DefaultValue(false),
        SuppressMessage("Microsoft.Naming", "CA1705:LongAcronymsShouldBePascalCased", Justification = "Same as Silverlight Parameter Name.")
        ]
        public virtual bool EnableGPUAcceleration {
            get {
                object o = ViewState["EnableGPUAcceleration"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["EnableGPUAcceleration"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_EnableRedrawRegions"),
        Category("Appearance"),
        Browsable(true),
        DefaultValue(false),
        ]
        public virtual bool EnableRedrawRegions {
            get {
                object o = ViewState["EnableRedrawRegions"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["EnableRedrawRegions"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_HtmlAccess"),
        Category("Behavior"),
        Browsable(true),
        DefaultValue(HtmlAccess.SameDomain),
        ]
        public virtual HtmlAccess HtmlAccess {
            get {
                object o = ViewState["HtmlAccess"];
                return o == null ? HtmlAccess.SameDomain : (HtmlAccess)o;
            }
            set {
                if ((value < HtmlAccess.SameDomain) || (value > HtmlAccess.Enabled)) {
                    throw new ArgumentOutOfRangeException("value");
                }
                ViewState["HtmlAccess"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_InitParameters"),
        Category("Behavior"),
        Browsable(true),
        DefaultValue(""),
        ]
        public virtual string InitParameters {
            get {
                return ((String)ViewState["InitParameters"]) ?? String.Empty;
            }
            set {
                ViewState["InitParameters"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_MaxFrameRate"),
        Category("Behavior"),
        Browsable(true),
        DefaultValue(0),
        ]
        public virtual int MaxFrameRate {
            get {
                object o = ViewState["MaxFrameRate"];
                return o == null ? 0 : (int)o;
            }
            set {
                if (value < 0) {
                    throw new ArgumentOutOfRangeException("value",
                        SilverlightWeb.Common_GreaterThanOrEqualToZero);
                }
                ViewState["MaxFrameRate"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_MinimumVersion"),
        Category("Behavior"),
        Browsable(true),
        DefaultValue("1.0"),
        TypeConverter(typeof(VersionConverter)),
        ]
        public virtual string MinimumVersion {
            get {
                return (String)ViewState["MinimumVersion"] ?? _defaultSilverlightVersionString;
            }
            set {
                ViewState["MinimumVersion"] = String.IsNullOrEmpty(value) ? null : new Version(value).ToString();
            }
        }

        [
        ResourceDescription("Silverlight_OnPluginError"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnPluginError {
            get {
                return ((String)ViewState["OnPluginError"]) ?? string.Empty;
            }
            set {
                ViewState["OnPluginError"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_OnPluginFullScreenChanged"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnPluginFullScreenChanged {
            get {
                return ((String)ViewState["OnPluginFullScreenChanged"]) ?? string.Empty;
            }
            set {
                ViewState["OnPluginFullScreenChanged"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_OnPluginLoaded"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnPluginLoaded {
            get {
                return ((String)ViewState["OnPluginLoaded"]) ?? string.Empty;
            }
            set {
                ViewState["OnPluginLoaded"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_OnPluginResized"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnPluginResized {
            get {
                return ((String)ViewState["OnPluginResized"]) ?? string.Empty;
            }
            set {
                ViewState["OnPluginResized"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_OnPluginSourceDownloadComplete"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnPluginSourceDownloadComplete {
            get {
                return ((String)ViewState["OnPluginSourceDownloadComplete"]) ?? string.Empty;
            }
            set {
                ViewState["OnPluginSourceDownloadComplete"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_OnPluginSourceDownloadProgressChanged"),
        Category("Behavior"),
        DefaultValue(""),
        Bindable(true)
        ]
        public virtual string OnPluginSourceDownloadProgressChanged {
            get {
                return ((String)ViewState["OnPluginSourceDownloadProgressChanged"]) ?? string.Empty;
            }
            set {
                ViewState["OnPluginSourceDownloadProgressChanged"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_PluginBackground"),
        Category("Appearance"),
        Browsable(true),
        DefaultValue(typeof(Color), ""),
        Themeable(true),
        ]
        public Color PluginBackground {
            get {
                object o = ViewState["PluginBackground"];
                return o == null ? Color.Empty : (Color)o;
            }
            set {
                ViewState["PluginBackground"] = value;
            }
        }

        [
        Browsable(false),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        ]
        public ITemplate PluginNotInstalledTemplate
        {
            get
            {
                return _pluginNotInstalledTemplate;
            }
            set
            {
                if (!DesignMode && (_pluginNotInstalledTemplate != null))
                {
                    throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
                        SilverlightWeb.Silverlight_CannotSetAlternateContentTemplate, ID));
                }
                _pluginNotInstalledTemplate = value;
                if (_pluginNotInstalledTemplate != null)
                {
                    CreateAlternateContent();
                }
            }
        }

        [
        ResourceDescription("Silverlight_ScaleMode"),
        Category("Appearance"),
        DefaultValue(ScaleMode.None),
        Bindable(true),
        Themeable(true),
        ]
        public virtual ScaleMode ScaleMode {
            get {
                object o = ViewState["ScaleMode"];
                return o == null ? ScaleMode.None : (ScaleMode)o;
            }
            set {
                if (value < ScaleMode.None || value > ScaleMode.Stretch) {
                    throw new ArgumentOutOfRangeException("value");
                }
                ViewState["ScaleMode"] = value;
            }
        }

        internal IScriptManagerInternal ScriptManager {
            get {
                if (_scriptManager == null) {
                    Page page = Page;
                    if (page == null) {
                        throw new InvalidOperationException(SilverlightWeb.Common_PageCannotBeNull);
                    }
                    ScriptManager sm = System.Web.UI.ScriptManager.GetCurrent(page);
                    if (sm == null) {
                        throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture, SilverlightWeb.Common_ScriptManagerRequired, ID));
                    }
                    _scriptManager = new ScriptManagerWrapper(sm);

                }
                return _scriptManager;
            }
        }

        [
        ResourceDescription("Silverlight_ScriptType"),
        Category("Behavior"),
        Browsable(true),
        Themeable(true),
        DefaultValue(""),
        ]
        public virtual string ScriptType {
            get {
                return ((String)ViewState["ScriptType"]) ?? String.Empty;
            }
            set {
                ViewState["ScriptType"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_Source"),
        Category("Appearance"),
        Editor("System.Web.UI.Design.SilverlightControls.SourceEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        DefaultValue(""),
        Bindable(true),
        Themeable(true),
        UrlProperty("*.xaml;*.xap"),
        ]
        public virtual string Source {
            get {
                return ((String)ViewState["Source"]) ?? String.Empty;
            }
            set {
                ViewState["Source"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_SplashScreenSource"),
        Category("Appearance"),
        Editor("System.Web.UI.Design.SilverlightControls.SplashSourceEditor, " + AssemblyRef.MicrosoftWebSilverlightDesign, typeof(UITypeEditor)),
        DefaultValue(""),
        Bindable(true),
        Themeable(true),
        UrlProperty("*.xaml"),
        ]
        public virtual string SplashScreenSource {
            get {
                return ((String)ViewState["SplashScreenSource"]) ?? String.Empty;
            }
            set {
                ViewState["SplashScreenSource"] = value;
            }
        }

        [
        ResourceDescription("Silverlight_Windowless"),
        Category("Appearance"),
        Browsable(true),
        DefaultValue(false),
        Themeable(true),
        ]
        public virtual bool Windowless {
            get {
                object o = ViewState["Windowless"];
                return o == null ? false : (bool)o;
            }
            set {
                ViewState["Windowless"] = value;
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer) {
            if (writer == null) {
                throw new ArgumentNullException("writer");
            }

            if (Attributes["type"] == null) {
                writer.AddAttribute(HtmlTextWriterAttribute.Type, SilverlightPlugin.GetMimeType(GetVersion()));
            }

            if (String.IsNullOrEmpty(ID)) {
                // not added by base if no ID but we require it
                writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            }

            base.AddAttributesToRender(writer);

            // if parameters need to be rendered as child <param> elements, this collection is used
            // from RenderContents. If they need to be rendered as attributes, they are used immediately
            // from here. GetSilverlightParameters virtual method is executed now even when they are rendered
            // later so that the order of this call is consistent regardless of how the parameters are rendered.
            _parameters = GetSilverlightParameters();

            if (TagKey != HtmlTextWriterTag.Object) {
                // render silverlight parameters as attributes
                SilverlightPlugin.AddParametersToRender(writer, _parameters, true);
            }
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:DisposeObjectsBeforeLosingScope", Justification = "container is added to the control tree, it can't be disposed.")]
        private void CreateAlternateContent() {
            // The controls inside the template are instantiated into
            // a dummy container to ensure that they all do lifecycle catchup
            // at the same time (i.e. Init1, Init2, Load1, Load2) as opposed to
            // one after another (i.e. Init1, Load1, Init2, Load2).
            Debug.Assert(_pluginNotInstalledTemplate != null, "CreateAlternateContent should not be called when there is no template.");
            Control container = new Control();
            _pluginNotInstalledTemplate.InstantiateIn(container);
            Controls.Add(container);
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Depends on other property values.")]
        protected virtual IDictionary<String, String> GetSilverlightParameters() {
            // returns a dictionary of key/values that will be child <param> elements to the <object> element,
            // or attributes on the <embed> tag. May be overridden in a derived control.

            var parameters = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);

            // MinRuntimeVersion applies whenever version is > 1.0
            Version version = GetVersion();
            if ((version != null) && (version.Major > 1) || ((version.Major == 1) && (version.Minor > 0))) {
                parameters.Add("MinRuntimeVersion", version.ToString());
            }

            if (!AutoUpgrade) {
                parameters.Add("AutoUpgrade", "false");
            }

            // for the rest, only apply parameters that are set to non-default values

            if (PluginBackground != Color.Empty) {
                // Silverlight requires the Background to be specified in #AARRGGBB hex format.
                // ToArgb() converts the color to a number, and ToString(X8) converts it to this
                // 8 digit hexidecimal number. Finally, we prepend "#".
                parameters.Add("Background",
                    "#" + PluginBackground.ToArgb().ToString("X8", CultureInfo.InvariantCulture));
            }

            if (EnableFrameRateCounter) {
                parameters.Add("EnableFrameRateCounter", "True");
            }

            if (HtmlAccess != HtmlAccess.SameDomain) {
                parameters.Add("EnableHtmlAccess", (HtmlAccess == HtmlAccess.Enabled ? "True" : "False"));
            }

            if (EnableGPUAcceleration) {
                parameters.Add("EnableGPUAcceleration", "True");
            }

            if (EnableRedrawRegions) {
                parameters.Add("EnableRedrawRegions", "True");
            }

            if (EnableCacheVisualization) {
                parameters.Add("EnableCacheVisualization", "True");
            }

            if (!String.IsNullOrEmpty(InitParameters)) {
                parameters.Add("InitParams", InitParameters);
            }

            if (MaxFrameRate != 0) {
                parameters.Add("MaxFrameRate", MaxFrameRate.ToString(CultureInfo.InvariantCulture));
            }

            if (Windowless) {
                parameters.Add("Windowless", "True");
            }

            return parameters;
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification="Same as IScriptControl interface, depends on other properties.")]
        protected virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors() {
            string scriptType = ScriptType;
            if (String.IsNullOrEmpty(scriptType)) {
                scriptType = DefaultScriptType;

                if (String.IsNullOrEmpty(scriptType)) {
                    throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
                        SilverlightWeb.Silverlight_ScriptTypeRequired,
                        ID));
                }
            }

            ScriptControlDescriptor scd = new ScriptControlDescriptor(scriptType, ClientID + "_parent");

            string source = Source;
            if (!String.IsNullOrEmpty(source)) {
                scd.AddProperty("source", ResolveClientUrl(source));
            }

            source = SplashScreenSource;
            if (!String.IsNullOrEmpty(source)) {
                scd.AddProperty("splashScreenSource", ResolveClientUrl(source));
            }

            if (!String.IsNullOrEmpty(OnPluginLoaded)) {
                scd.AddEvent("pluginLoaded", OnPluginLoaded);
            }

            if (!String.IsNullOrEmpty(OnPluginError)) {
                scd.AddEvent("pluginError", OnPluginError);
            }

            if (ScaleMode != ScaleMode.None) {
                scd.AddProperty("scaleMode", ScaleMode);
            }

            // client events
            if (!String.IsNullOrEmpty(OnPluginSourceDownloadComplete)) {
                scd.AddEvent("pluginSourceDownloadComplete", OnPluginSourceDownloadComplete);
            }

            if (!String.IsNullOrEmpty(OnPluginSourceDownloadProgressChanged)) {
                scd.AddEvent("pluginSourceDownloadProgressChanged", OnPluginSourceDownloadProgressChanged);
            }

            if (!String.IsNullOrEmpty(OnPluginResized)) {
                scd.AddEvent("pluginResized", OnPluginResized);
            }

            if (!String.IsNullOrEmpty(OnPluginFullScreenChanged)) {
                scd.AddEvent("pluginFullScreenChanged", OnPluginFullScreenChanged);
            }

            return new ScriptDescriptor[] { scd };
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Same as IScriptControl interface.")]
        protected virtual IEnumerable<ScriptReference> GetScriptReferences() {
            return new ScriptReference[] {
                new ScriptReference("SilverlightControl.js", AssemblyRef.MicrosoftWebSilverlight.FullName)
            };
        }

        private Version GetVersion() {
            if (!_versionCached) {
                string versionString = MinimumVersion;
                // _version is cached for performance. VersionString property is virtual,
                // but the property isn't expected to change during the rendering process.
                _version = (versionString == null ? null : new Version(versionString));
                _versionCached = true;
            }
            return _version;
        }

        protected override void OnPreRender(EventArgs e) {
            ScriptManager.RegisterScriptControl(this);
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer) {
            if (writer == null) {
                throw new ArgumentNullException("writer");
            }

            // We must write script to render the object tag to work around IE's "Click To Activate"
            // We must also avoid inlining the object tag to avoid plugin installation helpers in FF and Safari.
            // If the user does not have the correct version installed, they will get a silverlight installation
            // prompt, which will have the same id, width, height, style, and cssClass as the object would have.

            // the control is rendered to a string using a StringWriter because it must be passed as a javascript
            // parameter to the Plugin.create method, where it is dynamically inserted into the innerHTML of the
            // placeholder span that is rendered in its place.

            if (DesignMode) {
                base.Render(writer);
            }
            else {
                // capture base rendering to string using a StringWriter.
                // Initialize StringBuilder to 255 as this is likely to be large enough to fit the typical
                // rendering. Default SB size is 16 which is definitely not large enough.
                StringBuilder sb = new StringBuilder(255);
                using (StringWriter stringWriter = new StringWriter(sb, CultureInfo.InvariantCulture)) {
                    using (HtmlTextWriter xamlWriter = new HtmlTextWriter(stringWriter)) {
                        base.Render(xamlWriter);
                    }
                }

                string controlHtml = sb.ToString();

                // create a placeholder where the object will be inserted
                // <span id="id_parent"></span>
                string placeholderId = ClientID + "_parent";
                writer.AddAttribute(HtmlTextWriterAttribute.Id, placeholderId, true);
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.RenderEndTag();

                if (ScriptManager.IsInAsyncPostBack) {
                    // in async postback we must register script because rendering inline script does not
                    // work in browsers when it is injected via innerHTML.
                    string script = SilverlightPlugin.GetCreateObjectScript(placeholderId, controlHtml);
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, typeof(Silverlight), UniqueID, script, true);
                }
                else {
                    // in non-async postbacks we render it directly so it will execute inline with the
                    // placeholder. Using startup script here could result in a flickering UI.
                    SilverlightPlugin.RenderCreateObjectScript(this, writer, placeholderId, controlHtml);
                }

                // note: important to keep this call after the RegisterStartupScript above,
                // because create() must occur before the ajax component is created.
                // Normally on regular requests this order does not matter, because they are
                // created later on during app.init, but components are created immediately
                // during async update processing.
                ScriptManager.RegisterScriptDescriptors(this);
            }
        }

        protected override void RenderContents(HtmlTextWriter writer) {
            if (writer == null) {
                throw new ArgumentNullException("writer");
            }

            if (TagKey == HtmlTextWriterTag.Object) {
                // render parameters as param tags
                SilverlightPlugin.AddParametersToRender(writer, _parameters, false);
            }

            base.RenderContents(writer);

            if (Controls.Count == 0) {
                RenderDefaultTemplate(writer, GetVersion());
            }
        }

        #region IScriptControl Members
        IEnumerable<ScriptDescriptor> IScriptControl.GetScriptDescriptors() {
            return GetScriptDescriptors();
        }

        IEnumerable<ScriptReference> IScriptControl.GetScriptReferences() {
            return GetScriptReferences();
        }
        #endregion
    }
}

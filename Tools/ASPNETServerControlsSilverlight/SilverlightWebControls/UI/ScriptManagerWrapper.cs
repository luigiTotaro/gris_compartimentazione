//------------------------------------------------------------------------------
// <copyright file="ScriptManagerWrapper.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
 
namespace System.Web.UI {
    using System;
    using System.Diagnostics;
    using System.Web.UI;

    internal class ScriptManagerWrapper : IScriptManagerInternal {
        private ScriptManager _scriptManager;

        public ScriptManagerWrapper(ScriptManager scriptManager) {
            Debug.Assert(scriptManager != null);
            _scriptManager = scriptManager;
        }

        bool IScriptManagerInternal.IsInAsyncPostBack {
            get {
                return _scriptManager.IsInAsyncPostBack;
            }
        }

        void IScriptManagerInternal.RegisterScriptControl<TScriptControl>(TScriptControl scriptControl) {
            _scriptManager.RegisterScriptControl(scriptControl);
        }

        void IScriptManagerInternal.RegisterScriptDescriptors(IScriptControl scriptControl) {
            _scriptManager.RegisterScriptDescriptors(scriptControl);
        }
    }
}

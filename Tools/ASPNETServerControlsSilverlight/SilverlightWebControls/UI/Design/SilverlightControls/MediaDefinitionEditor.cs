//------------------------------------------------------------------------------
// <copyright file="MediaDefinitionEditor.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace System.Web.UI.Design.SilverlightControls {
    using System.Diagnostics.CodeAnalysis;
    using System.Web.Resources.Design;
    using System.Web.UI.Design;

    public class MediaDefinitionEditor : UrlEditor {

        protected override string Filter {
            get {
                return SilverlightWebDesign.Silverlight_MediaDefinitionFilter;
                //Xml Files(*.wml)|*.xml|All Files(*.*)|*.*
            }
        }
    }
}

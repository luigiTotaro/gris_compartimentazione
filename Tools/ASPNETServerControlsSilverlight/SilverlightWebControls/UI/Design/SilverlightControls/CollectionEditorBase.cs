//------------------------------------------------------------------------------
// <copyright file="CollectionEditorBase.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
 
namespace System.Web.UI.Design.SilverlightControls {
    using System;
    using System.ComponentModel.Design;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows.Forms;

    public class CollectionEditorBase : CollectionEditor {
        public CollectionEditorBase(Type type)
            : base(type) {
        }

        protected override CollectionForm CreateCollectionForm() {
            CollectionForm form = base.CreateCollectionForm();

            // Locate the PropertyGrid somewhere in the control tree and enable its help display
            UpdatePropertyGridSettings(form);

            return form;
        }

        private bool UpdatePropertyGridSettings(Control control) {
            PropertyGrid grid = control as PropertyGrid;
            if (grid != null) {
                grid.HelpVisible = true;
                return true;
            }
            foreach (Control child in control.Controls) {
                if (UpdatePropertyGridSettings(child)) {
                    return true;
                }
            }
            return false;
        }
    }
}

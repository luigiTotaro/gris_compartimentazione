//------------------------------------------------------------------------------
// <copyright file="SilverlightUrlEditor.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.Design.SilverlightControls {
    using System.Diagnostics.CodeAnalysis;
    using System.Web.UI.Design;
    using System.Web.Resources.Design;

    public class SourceEditor : UrlEditor {

        protected override string Filter {
            get {
                return SilverlightWebDesign.Silverlight_SourceFilter;
            }
        }
    }
}

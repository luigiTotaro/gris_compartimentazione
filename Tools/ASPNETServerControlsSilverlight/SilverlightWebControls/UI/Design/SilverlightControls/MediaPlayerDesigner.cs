//------------------------------------------------------------------------------
// <copyright file="MediaPlayerDesigner.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.Design.SilverlightControls {
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing.Design;
    using System.IO;
    using System.Security;
    using System.Web.Resources;
    using System.Web.Resources.Design;
    using System.Web.UI.Design;
    using System.Web.UI.SilverlightControls;
    using System.Windows.Forms;
    using Microsoft.Win32;

    public class MediaPlayerDesigner : SilverlightDesigner {
        private const string _classicSkinPreviewResourceName = "System.Web.Resources.Design.Classic.xaml.jpg";

        public override DesignerActionListCollection ActionLists {
            get {
                var actionLists = new DesignerActionListCollection();
                actionLists.Add(new MediaPlayerActionList(this));
                return actionLists;
            }
        }

        protected new MediaPlayer Control {
            get {
                return (MediaPlayer) base.Control;
            }
        }

        public override void Initialize(IComponent component) {
            MediaPlayerDesigner.VerifyInitializeArgument(component, typeof(MediaPlayer));
            base.Initialize(component);
        }

        private class MediaPlayerActionList : DesignerActionList {
            MediaPlayerDesigner _designer;
            private static string _mediaSkinsLocationRegKey =
                "SOFTWARE\\Microsoft\\.NETFramework\\v2.0.50727\\AssemblyFoldersEx\\Silverlight 2.0 ASP.NET";
            private static string _mediaSkinsLocation;

            public MediaPlayerActionList(MediaPlayerDesigner designer)
                : base(designer.Component) {
                _designer = designer;
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")]
            private static string MediaPlayerSkinsPath {
                get {
                    if (_mediaSkinsLocation == null) {
                        try {
                            var key = Registry.LocalMachine.OpenSubKey(_mediaSkinsLocationRegKey);
                            if (key != null) {
                                _mediaSkinsLocation = (String)key.GetValue(null);
                            }
                        }
                        catch(SecurityException) {
                            // the path will be set to empty, providing no default path for media skin imports
                        }

                        if (_mediaSkinsLocation == null) {
                            _mediaSkinsLocation = String.Empty;
                        }
                        else {
                            _mediaSkinsLocation = Path.Combine(_mediaSkinsLocation, "MediaPlayerSkins");
                            try {
                                if (!Directory.Exists(_mediaSkinsLocation)) {
                                    _mediaSkinsLocation = String.Empty;
                                }
                            }
                            catch (IOException) {
                                _mediaSkinsLocation = String.Empty;
                            }
                        }
                    }

                    return _mediaSkinsLocation;
                }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")]
            public bool AutoPlay {
                get {
                    return _designer.Control.AutoPlay;
                }
                set {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_designer.Control)["AutoPlay"];
                    pd.SetValue(_designer.Control, value);
                }
            }

            [
            Editor(typeof(SourceEditor), typeof(UITypeEditor)),
            SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")
            ]
            public string MediaSkinSource {
                get {
                    return _designer.Control.MediaSkinSource;
                }
                set {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_designer.Control)["MediaSkinSource"];
                    pd.SetValue(_designer.Control, value);
                }
            }

            [
            Editor(typeof(MediaSourceEditor), typeof(UITypeEditor)),
            SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")
            ]
            public string MediaSource {
                get {
                    return _designer.Control.MediaSource;
                }
                set {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_designer.Control)["MediaSource"];
                    pd.SetValue(_designer.Control, value);
                }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")]
            public bool Muted {
                get {
                    return _designer.Control.Muted;
                }
                set {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_designer.Control)["Muted"];
                    pd.SetValue(_designer.Control, value);
                }
            }

            [
            Editor(typeof(ImageSourceEditor), typeof(UITypeEditor)),
            SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")
            ]
            public string PlaceholderSource {
                get {
                    return _designer.Control.PlaceholderSource;
                }
                set {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_designer.Control)["PlaceholderSource"];
                    pd.SetValue(_designer.Control, value);
                }
            }

            [
            Editor(typeof(VolumeEditor), typeof(UITypeEditor)),
            SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer.")
            ]
            public double Volume {
                get {
                    return _designer.Control.Volume;
                }
                set {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_designer.Control)["Volume"];
                    pd.SetValue(_designer.Control, value);
                }
            }

            public override DesignerActionItemCollection GetSortedActionItems() {
                var items = new DesignerActionItemCollection();
                
                items.Add(new DesignerActionPropertyItem("MediaSkinSource", SilverlightWebDesign.Silverlight_MediaSkin));
                items.Add(new DesignerActionMethodItem(this, "ImportMediaSkin", SilverlightWebDesign.Silverlight_ImportMediaSkin));
                
                items.Add(new DesignerActionPropertyItem("MediaSource", SilverlightWebDesign.Silverlight_MediaSource));
                items.Add(new DesignerActionPropertyItem("PlaceholderSource", SilverlightWebDesign.Silverlight_PlaceholderSource));
                items.Add(new DesignerActionPropertyItem("Volume", SilverlightWebDesign.Silverlight_Volume));
                items.Add(new DesignerActionPropertyItem("Muted", SilverlightWebDesign.Silverlight_Muted));
                items.Add(new DesignerActionPropertyItem("AutoPlay", SilverlightWebDesign.Silverlight_AutoPlay));
                return items;
            }

            [
            SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by the designer."),
            ]
            public void ImportMediaSkin() {
                string fileName;
                using (OpenFileDialog openDialog = new OpenFileDialog()) {
                    openDialog.InitialDirectory = MediaPlayerActionList.MediaPlayerSkinsPath;
                    openDialog.Filter = SilverlightWebDesign.Silverlight_SourceFilter;
                    openDialog.Title = SilverlightWebDesign.Silverlight_ImportMediaSkinTitle;
                    openDialog.Multiselect = false;

                    if (openDialog.ShowDialog() != DialogResult.OK) {
                        // user cancelled
                        return;
                    }

                    fileName = openDialog.FileName;
                }

                // be careful of nulls at every step
                ISite site = _designer.Component.Site;
                if (site == null) {
                    return;
                }
                var app = (IWebApplication)site.GetService(typeof(IWebApplication));
                if (app == null) {
                    return;
                }

                if (_designer.RootDesigner == null) {
                    return;
                }

                // get the url to the current page
                IProjectItem item = app.GetProjectItemFromUrl(_designer.RootDesigner.DocumentUrl);
                if (item == null) {
                    return;
                }
                // get the folder item the page is in
                IFolderProjectItem folderItem = (IFolderProjectItem)item.Parent;

                if (folderItem == null) {
                    return;
                }

                // read all the file bytes 
                byte[] fileBytes;
                try {
                    fileBytes = File.ReadAllBytes(fileName);
                }
                catch(IOException) {
                    return;
                }
                catch (SecurityException) {
                    return;
                }
                catch (UnauthorizedAccessException) {
                    return;
                }
                // create a new document
                IProjectItem newSkin = (IProjectItem)folderItem.AddDocument(Path.GetFileName(fileName), fileBytes);
                
                // set the media skin source to the new item if it was created successfully
                if (newSkin != null) {
                    MediaSkinSource = newSkin.AppRelativeUrl;
                }
            }
        }
    }
}

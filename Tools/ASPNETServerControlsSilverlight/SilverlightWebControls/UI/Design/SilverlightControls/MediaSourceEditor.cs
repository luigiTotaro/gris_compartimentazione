//------------------------------------------------------------------------------
// <copyright file="MediaSourceEditor.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.Design.SilverlightControls {
    using System.Diagnostics.CodeAnalysis;
    using System.Web.Resources.Design;
    using System.Web.UI.Design;

    public class MediaSourceEditor : UrlEditor {

        protected override string Filter {
            get {
                return SilverlightWebDesign.Silverlight_MediaFilter;
                //Media Files(*.wmv;*.wma;*.mp3;*.asx)|*.wmv;*.wma;*.mp3;*.asx|All Files(*.*)|*.*
            }
        }
    }
}

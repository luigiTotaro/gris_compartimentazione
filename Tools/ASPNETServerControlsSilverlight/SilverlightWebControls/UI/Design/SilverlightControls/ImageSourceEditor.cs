//------------------------------------------------------------------------------
// <copyright file="ImageSourceEditor.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.Design.SilverlightControls {
    using System.Diagnostics.CodeAnalysis;
    using System.Web.Resources.Design;
    using System.Web.UI.Design;

    public class ImageSourceEditor : UrlEditor {

        protected override string Filter {
            get {
                return SilverlightWebDesign.Silverlight_ImageFilter;
                // Image Files(*.jpg;*.jpeg;*.png)|*.jpg;*.jpeg;*.png|All Files(*.*)|*.*|
            }
        }
    }
}

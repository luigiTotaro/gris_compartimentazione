//------------------------------------------------------------------------------
// <copyright file="SilverlightDesigner.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.Design.SilverlightControls {
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.SilverlightControls;
    using System.Web.Resources.Design;

    public class SilverlightDesigner : System.Web.UI.Design.ControlDesigner {
        private Silverlight _silverlightControl;

        internal static void VerifyInitializeArgument(IComponent component, Type expectedType) {
            if (!expectedType.IsInstanceOfType(component)) {
                throw new ArgumentException(
                    String.Format(CultureInfo.CurrentCulture, SilverlightWebDesign.Common_ArgumentMustBeOfType,
                        expectedType.FullName), "component");
            }
        }

        protected Silverlight Control {
            get {
                return _silverlightControl;
            }
        }

        public override string GetDesignTimeHtml() {
            string imageUrl = Control.Page.ClientScript.GetWebResourceUrl(typeof(SilverlightDesigner),
                "System.Web.Resources.Design.Xaml.png");
            return GetImagePreview(imageUrl);
        }

        private string GetImagePreview(string previewImage) {
            string html = "<img src=\"";
            html += HttpUtility.HtmlAttributeEncode(previewImage);
            html += "\"";

            // Different browsers display the object at different default sizes when
            // no width or height is specified. IE defaults to width=200px. Firefox defaults
            // to 0px. The best we can do at design time is show what IE would show.
            // If no height is specified leave it off so the image scales to fit within the default.

            Unit width = Control.Width == Unit.Empty ? Unit.Pixel(200) : Control.Width;
            html += " width=\"" + width.ToString(CultureInfo.InvariantCulture) + "\"";

            if (Control.Height != Unit.Empty) {
                html += " height=\"" + Control.Height.ToString(CultureInfo.InvariantCulture) + "\"";
            }

            html += "/>";
            return html;
        }

        public override void Initialize(IComponent component) {
            SilverlightDesigner.VerifyInitializeArgument(component, typeof(Silverlight));
            _silverlightControl = (Silverlight) component;
            base.Initialize(component);
        }
    }
}

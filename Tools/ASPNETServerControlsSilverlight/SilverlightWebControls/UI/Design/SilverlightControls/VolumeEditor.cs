//------------------------------------------------------------------------------
// <copyright file="VolumeEditor.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace System.Web.UI.Design.SilverlightControls {
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Globalization;
    using System.Security;
    using System.Security.Permissions;
    using System.Web.Resources.Design;
    using System.Windows.Forms;
    using System.Windows.Forms.Design;

    public class VolumeEditor : UITypeEditor {
        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) {
            return UITypeEditorEditStyle.DropDown;
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value) {
            if (provider == null) {
                throw new ArgumentNullException("provider");
            }
            if (value == null) {
                throw new ArgumentNullException("provider");
            }
            if (!(value is Double)) {
                throw new ArgumentException(
                    String.Format(CultureInfo.CurrentCulture, SilverlightWebDesign.Common_ArgumentMustBeOfType,
                        typeof(Double).FullName), "value");
            }

            using (TrackBar tb = new TrackBar()) {
                tb.BackColor = SystemColors.Window;
                tb.Maximum = 100;
                tb.Minimum = 0;
                tb.SmallChange = 1;
                tb.LargeChange = 10;
                tb.TickFrequency = 10;
                tb.TickStyle = TickStyle.BottomRight;
                tb.Orientation = System.Windows.Forms.Orientation.Horizontal;

                double volume = (Double)value * 100;
                tb.Value = (int)Math.Min(100, Math.Max(0, volume));

                IWindowsFormsEditorService formsService =
                    (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                formsService.DropDownControl(tb);

                return (Double)tb.Value / 100;
            }
        }
    }
}

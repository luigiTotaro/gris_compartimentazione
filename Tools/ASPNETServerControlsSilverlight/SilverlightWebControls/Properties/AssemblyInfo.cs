//------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Web.UI;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Microsoft.Web.Silverlight")]
[assembly: AssemblyDescription("ASP.NET Controls for Silverlight")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft Corporation")]
[assembly: AssemblyProduct("Microsoft (R) Silverlight")]
[assembly: AssemblyCopyright("(c) Microsoft Corporation. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//      Major Version
//      Minor Version
//      Build Number
//      Revision
[assembly: AssemblyVersion("3.0.0.0")]
[assembly: AssemblyFileVersion("3.0.40210.0")]

[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]

[assembly: NeutralResourcesLanguage("en", UltimateResourceFallbackLocation.MainAssembly)]

// Generic design time preview for xaml
[assembly: WebResource("System.Web.Resources.Design.Xaml.png", "image/png")]

[assembly: WebResource("SilverlightControl.js", "application/x-javascript")]
[assembly: WebResource("SilverlightMedia.js", "application/x-javascript")]

// Default MediaPlayer skin
[assembly: WebResource("Classic.xaml", "text/xml")]

// Script resources
[assembly: ScriptResource("SilverlightControl.js", "System.Web.Resources.ScriptLibrary.Control.Res", "Sys.UI.Silverlight.ControlRes")]
[assembly: ScriptResource("SilverlightMedia.js", "System.Web.Resources.ScriptLibrary.Media.Res", "Sys.UI.Silverlight.MediaRes")]

// Default tag prefix for designer
[assembly: TagPrefix("System.Web.UI.SilverlightControls", "asp")]

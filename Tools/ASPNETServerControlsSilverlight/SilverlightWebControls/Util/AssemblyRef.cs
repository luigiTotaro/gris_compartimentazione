//------------------------------------------------------------------------------
// <copyright file="AssemblyRef.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
namespace Microsoft.Web.Util {
    using System;
    using System.Reflection;

    internal static class AssemblyRef {
        // PERF: Cache reference to Microsoft.Web.Silverlight assembly
        public static readonly Assembly MicrosoftWebSilverlight = typeof(AssemblyRef).Assembly;
        public const string MicrosoftWebSilverlightDesign = "System.Web.Silverlight";
    }
}

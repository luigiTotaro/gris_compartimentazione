﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GrisServicesTester
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEndopointUrl = new System.Windows.Forms.Label();
            this.txtEndpointUrl = new System.Windows.Forms.TextBox();
            this.btnGetLocationData = new System.Windows.Forms.Button();
            this.lblLocationId = new System.Windows.Forms.Label();
            this.txtLocationId = new System.Windows.Forms.TextBox();
            this.lstDevices = new System.Windows.Forms.ListBox();
            this.lblSharedSecret = new System.Windows.Forms.Label();
            this.txtSharedSecret = new System.Windows.Forms.TextBox();
            this.rtbLocality = new System.Windows.Forms.RichTextBox();
            this.lstStreamFields = new System.Windows.Forms.ListBox();
            this.cboSystems = new System.Windows.Forms.ComboBox();
            this.btnGetLocationDataDetail = new System.Windows.Forms.Button();
            this.txtRegionId = new System.Windows.Forms.TextBox();
            this.lblRegionId = new System.Windows.Forms.Label();
            this.buttonGetSeverity = new System.Windows.Forms.Button();
            this.listSeverity = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblEndopointUrl
            // 
            this.lblEndopointUrl.AutoSize = true;
            this.lblEndopointUrl.Location = new System.Drawing.Point(12, 15);
            this.lblEndopointUrl.Name = "lblEndopointUrl";
            this.lblEndopointUrl.Size = new System.Drawing.Size(63, 13);
            this.lblEndopointUrl.TabIndex = 0;
            this.lblEndopointUrl.Text = "Url Servizio:";
            // 
            // txtEndpointUrl
            // 
            this.txtEndpointUrl.Location = new System.Drawing.Point(113, 12);
            this.txtEndpointUrl.Name = "txtEndpointUrl";
            this.txtEndpointUrl.Size = new System.Drawing.Size(724, 20);
            this.txtEndpointUrl.TabIndex = 1;
            // 
            // btnGetLocationData
            // 
            this.btnGetLocationData.Location = new System.Drawing.Point(229, 61);
            this.btnGetLocationData.Name = "btnGetLocationData";
            this.btnGetLocationData.Size = new System.Drawing.Size(152, 23);
            this.btnGetLocationData.TabIndex = 7;
            this.btnGetLocationData.Text = "Ottieni dati località";
            this.btnGetLocationData.UseVisualStyleBackColor = true;
            this.btnGetLocationData.Click += new System.EventHandler(this.btnGetLocationData_Click);
            // 
            // lblLocationId
            // 
            this.lblLocationId.AutoSize = true;
            this.lblLocationId.Location = new System.Drawing.Point(12, 67);
            this.lblLocationId.Name = "lblLocationId";
            this.lblLocationId.Size = new System.Drawing.Size(59, 13);
            this.lblLocationId.TabIndex = 4;
            this.lblLocationId.Text = "Id Località:";
            // 
            // txtLocationId
            // 
            this.txtLocationId.Location = new System.Drawing.Point(113, 64);
            this.txtLocationId.Name = "txtLocationId";
            this.txtLocationId.Size = new System.Drawing.Size(100, 20);
            this.txtLocationId.TabIndex = 5;
            // 
            // lstDevices
            // 
            this.lstDevices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDevices.FormattingEnabled = true;
            this.lstDevices.HorizontalScrollbar = true;
            this.lstDevices.Location = new System.Drawing.Point(15, 230);
            this.lstDevices.Name = "lstDevices";
            this.lstDevices.Size = new System.Drawing.Size(826, 186);
            this.lstDevices.TabIndex = 9;
            this.lstDevices.SelectedIndexChanged += new System.EventHandler(this.lstDevices_SelectedIndexChanged);
            // 
            // lblSharedSecret
            // 
            this.lblSharedSecret.AutoSize = true;
            this.lblSharedSecret.Location = new System.Drawing.Point(12, 41);
            this.lblSharedSecret.Name = "lblSharedSecret";
            this.lblSharedSecret.Size = new System.Drawing.Size(95, 13);
            this.lblSharedSecret.TabIndex = 2;
            this.lblSharedSecret.Text = "Gettone sicurezza:";
            // 
            // txtSharedSecret
            // 
            this.txtSharedSecret.Location = new System.Drawing.Point(113, 38);
            this.txtSharedSecret.Name = "txtSharedSecret";
            this.txtSharedSecret.Size = new System.Drawing.Size(725, 20);
            this.txtSharedSecret.TabIndex = 3;
            // 
            // rtbLocality
            // 
            this.rtbLocality.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbLocality.Location = new System.Drawing.Point(15, 120);
            this.rtbLocality.Name = "rtbLocality";
            this.rtbLocality.ReadOnly = true;
            this.rtbLocality.Size = new System.Drawing.Size(826, 101);
            this.rtbLocality.TabIndex = 8;
            this.rtbLocality.Text = "";
            this.rtbLocality.WordWrap = false;
            // 
            // lstStreamFields
            // 
            this.lstStreamFields.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstStreamFields.FormattingEnabled = true;
            this.lstStreamFields.HorizontalScrollbar = true;
            this.lstStreamFields.Location = new System.Drawing.Point(15, 430);
            this.lstStreamFields.Name = "lstStreamFields";
            this.lstStreamFields.Size = new System.Drawing.Size(826, 199);
            this.lstStreamFields.TabIndex = 10;
            // 
            // cboSystems
            // 
            this.cboSystems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSystems.FormattingEnabled = true;
            this.cboSystems.Location = new System.Drawing.Point(387, 63);
            this.cboSystems.MaxDropDownItems = 15;
            this.cboSystems.Name = "cboSystems";
            this.cboSystems.Size = new System.Drawing.Size(297, 21);
            this.cboSystems.TabIndex = 6;
            this.cboSystems.SelectedIndexChanged += new System.EventHandler(this.cboSystems_SelectedIndexChanged);
            // 
            // btnGetLocationDataDetail
            // 
            this.btnGetLocationDataDetail.Location = new System.Drawing.Point(689, 61);
            this.btnGetLocationDataDetail.Name = "btnGetLocationDataDetail";
            this.btnGetLocationDataDetail.Size = new System.Drawing.Size(152, 23);
            this.btnGetLocationDataDetail.TabIndex = 11;
            this.btnGetLocationDataDetail.Text = "Ottieni dettaglio sistemi";
            this.btnGetLocationDataDetail.UseVisualStyleBackColor = true;
            this.btnGetLocationDataDetail.Click += new System.EventHandler(this.btnGetLocationDataDerail_Click);
            // 
            // txtRegionId
            // 
            this.txtRegionId.Location = new System.Drawing.Point(113, 90);
            this.txtRegionId.Name = "txtRegionId";
            this.txtRegionId.Size = new System.Drawing.Size(100, 20);
            this.txtRegionId.TabIndex = 12;
            // 
            // lblRegionId
            // 
            this.lblRegionId.AutoSize = true;
            this.lblRegionId.Location = new System.Drawing.Point(12, 93);
            this.lblRegionId.Name = "lblRegionId";
            this.lblRegionId.Size = new System.Drawing.Size(92, 13);
            this.lblRegionId.TabIndex = 13;
            this.lblRegionId.Text = "Id Compartimento:";
            // 
            // buttonGetSeverity
            // 
            this.buttonGetSeverity.Location = new System.Drawing.Point(229, 90);
            this.buttonGetSeverity.Name = "buttonGetSeverity";
            this.buttonGetSeverity.Size = new System.Drawing.Size(152, 23);
            this.buttonGetSeverity.TabIndex = 14;
            this.buttonGetSeverity.Text = "Ottieni dati Severità";
            this.buttonGetSeverity.UseVisualStyleBackColor = true;
            this.buttonGetSeverity.Click += new System.EventHandler(this.btnGetSeverity_Click);
            // 
            // listSeverity
            // 
            this.listSeverity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listSeverity.FormattingEnabled = true;
            this.listSeverity.HorizontalScrollbar = true;
            this.listSeverity.Location = new System.Drawing.Point(864, 120);
            this.listSeverity.Name = "listSeverity";
            this.listSeverity.Size = new System.Drawing.Size(330, 511);
            this.listSeverity.TabIndex = 15;
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(1228, 641);
            this.Controls.Add(this.listSeverity);
            this.Controls.Add(this.buttonGetSeverity);
            this.Controls.Add(this.lblRegionId);
            this.Controls.Add(this.txtRegionId);
            this.Controls.Add(this.btnGetLocationDataDetail);
            this.Controls.Add(this.cboSystems);
            this.Controls.Add(this.lstStreamFields);
            this.Controls.Add(this.rtbLocality);
            this.Controls.Add(this.txtSharedSecret);
            this.Controls.Add(this.lblSharedSecret);
            this.Controls.Add(this.lstDevices);
            this.Controls.Add(this.txtLocationId);
            this.Controls.Add(this.lblLocationId);
            this.Controls.Add(this.btnGetLocationData);
            this.Controls.Add(this.txtEndpointUrl);
            this.Controls.Add(this.lblEndopointUrl);
            this.MinimumSize = new System.Drawing.Size(746, 600);
            this.Name = "MainForm";
            this.Text = "Gris Services Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblEndopointUrl;
        private TextBox txtEndpointUrl;
        private Button btnGetLocationData;
        private Label lblLocationId;
        private TextBox txtLocationId;
        private ListBox lstDevices;
        private Label lblSharedSecret;
        private TextBox txtSharedSecret;
        private RichTextBox rtbLocality;
        private ListBox lstStreamFields;
        private ComboBox cboSystems;
        private Button btnGetLocationDataDetail;
        private TextBox txtRegionId;
        private Label lblRegionId;
        private Button buttonGetSeverity;
        private ListBox listSeverity;
    }
}


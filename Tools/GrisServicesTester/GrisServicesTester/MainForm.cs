﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows.Forms;
using GrisServicesTester.GrisServiceReference;
using GrisServicesTester.Properties;

namespace GrisServicesTester
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            this.InitializeComponent();

#if (DEBUG)
            if (String.IsNullOrEmpty(Settings.Default.EndpointUrl))
            {
                Settings.Default.EndpointUrl = "http://localhost:1167/GrisData/GrisService.svc";
            }

            if (String.IsNullOrEmpty(Settings.Default.ClientNodeId))
            {
                Settings.Default.ClientNodeId = "3027";
            }

            Settings.Default.Save();
#endif
            if (String.IsNullOrEmpty(Settings.Default.SecurityToken))
            {
                Settings.Default.SecurityToken = "CE900392-6931-4173-AD8D-EA9A120635C9";
                Settings.Default.Save();
            }

            this.txtEndpointUrl.Text    = Settings.Default.EndpointUrl;
            this.txtLocationId.Text     = Settings.Default.ClientNodeId;
            this.txtSharedSecret.Text   = Settings.Default.SecurityToken;
            this.txtRegionId.Text       = Settings.Default.RegionId;

        }

        private void btnGetLocationDataDerail_Click(object sender, EventArgs e)
        {
            this.GetDatiLocalitaSistemi(true);
        }

        private void btnGetLocationData_Click(object sender, EventArgs e)
        {
            this.GetDatiLocalita();

            //this.cboSystems.Items.Clear ();
            //this.GetDatiLocalitaSistemi        (false);
        }

        private void btnGetSeverity_Click(object sender, EventArgs e)
        {
            this.GetDatiSeverity();
        }


        private void GetDatiSeverity()
        {
            GrisServiceClient grisServiceClientFromConfig = null;
            GrisServiceClient grisServiceClient = null;

            try
            {
                // Usiamo i dati da configurazione, ma andiamo in override della URL dell'endpoint
                // Inutile se non configurabile a runtime, si può usare direttamente da config, senza creare un oggetto doppio
                grisServiceClientFromConfig = new GrisServiceClient();
                ServiceEndpoint ep = grisServiceClientFromConfig.Endpoint;
                ep.Address = new EndpointAddress(this.txtEndpointUrl.Text);
                grisServiceClient = new GrisServiceClient("basicHttpBinding_IGrisService", ep.Address);

                // Usato come shared-secret di sicurezza
                // Nel caso sia sbagliato, non c'è feedback dal server, solo non si ottengono dati validi
                string sharedSecret = this.txtSharedSecret.Text.Trim();

                //         Severity datiSeverity = grisServiceClient.OttieniDatiSeverity(sharedSecret, localitaId, sistemaId);

                short RegionId;
                if (Int16.TryParse(this.txtRegionId.Text, out RegionId))
                {
                    this.GetDatiSeverity(grisServiceClient, sharedSecret, RegionId);

                    this.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetDatiLocalita " + ex.Message);
            }
       }

        private void GetDatiSeverity(GrisServiceClient grisServiceClient, string sharedSecret, short regionId)
        {

            CompartimentoSeverita[] severity = grisServiceClient.OttieniSeverita(sharedSecret, (int)regionId);

            this.listSeverity.Items.Clear();

            int numLoc = severity.Length;
            for (int n = 0; n<numLoc; n++) 
                this.listSeverity.Items.Add(String.Format("Id Località: {0} - Severità: {1}\r\n",
                    severity[n].IdLocalita.ToString(), severity[n].Severita.ToString()));

            
        }

        private void GetDatiLocalita()
        {
            GrisServiceClient grisServiceClientFromConfig   = null;
            GrisServiceClient grisServiceClient             = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                // Usiamo i dati da configurazione, ma andiamo in override della URL dell'endpoint
                // Inutile se non configurabile a runtime, si può usare direttamente da config, senza creare un oggetto doppio
                grisServiceClientFromConfig = new GrisServiceClient     ();
                ServiceEndpoint ep          = grisServiceClientFromConfig.Endpoint;
                ep.Address                  = new EndpointAddress       (this.txtEndpointUrl.Text);
                grisServiceClient           = new GrisServiceClient     ("basicHttpBinding_IGrisService", ep.Address);

                // Usato come shared-secret di sicurezza
                // Nel caso sia sbagliato, non c'è feedback dal server, solo non si ottengono dati validi
                string sharedSecret         = this.txtSharedSecret.Text.Trim();

                this.rtbLocality.Clear          ();
                this.lstDevices.Items.Clear     ();
                this.lstStreamFields.Items.Clear();
                this.cboSystems.Items.Clear     ();

                short localitaId;
                if (Int16.TryParse(this.txtLocationId.Text, out localitaId))
                {
                    this.GetDatiLocalita(grisServiceClient, sharedSecret, localitaId);

                    this.SaveSettings   ();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetDatiLocalita " + ex.Message);
            }
            finally
            {
                if ((grisServiceClientFromConfig != null)   && (grisServiceClientFromConfig.State   == CommunicationState.Opened))
                {
                    grisServiceClientFromConfig.Close();
                }

                if ((grisServiceClient != null)             && (grisServiceClient.State             == CommunicationState.Opened))
                {
                    grisServiceClient.Close();
                }

                this.Cursor = Cursors.Default;
            }
        }

        private void GetDatiLocalitaSistemi(bool reloadSystems)
        {
            GrisServiceClient grisServiceClientFromConfig = null;
            GrisServiceClient grisServiceClient = null;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                // Usiamo i dati da configurazione, ma andiamo in override della URL dell'endpoint
                // Inutile se non configurabile a runtime, si può usare direttamente da config, senza creare un oggetto doppio
                grisServiceClientFromConfig = new GrisServiceClient();
                ServiceEndpoint ep = grisServiceClientFromConfig.Endpoint;
                ep.Address = new EndpointAddress(this.txtEndpointUrl.Text);
                grisServiceClient = new GrisServiceClient("basicHttpBinding_IGrisService", ep.Address);

                // Usato come shared-secret di sicurezza
                // Nel caso sia sbagliato, non c'è feedback dal server, solo non si ottengono dati validi
                string sharedSecret = this.txtSharedSecret.Text.Trim();

                this.rtbLocality.Clear();
                this.lstDevices.Items.Clear();
                this.lstStreamFields.Items.Clear();

                if (reloadSystems)
                {
                    this.cboSystems.Items.Clear();
                }

                short localitaId;
                if (Int16.TryParse(this.txtLocationId.Text, out localitaId))
                {
                    if (reloadSystems)
                    {
                        // Recupero lista dei sistemi per la località selezionata
                        List<Sistema> sistemi = new List<Sistema>(grisServiceClient.OttieniListaSistemi(sharedSecret, localitaId) ?? new Sistema[]{});

                        // Aggiungiamo il filtro che permette di ottenere tutte le periferiche per tutti i sistemi
                        sistemi.Insert(0, new Sistema {IdSistema = 0, NomeSistema = "Tutti i sistemi"});
                        foreach (Sistema sistema in sistemi)
                        {
                            this.cboSystems.Items.Add(new SistemaWrapper(sistema));
                        }
                        // Selezioniamo il filtro generico
                        this.cboSystems.SelectedIndex = 0;
                    }
                    else
                    {
                        if (this.cboSystems.SelectedItem == null)
                        {
                            this.GetDatiLocalitaByIdAndSistema(grisServiceClient, sharedSecret, localitaId, 0);
                        }
                        else
                            this.GetDatiLocalitaByIdAndSistema(grisServiceClient, sharedSecret, localitaId, ((SistemaWrapper) this.cboSystems.SelectedItem).Sistema.IdSistema);
                    }

                    this.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetDatiLocalitaSistemi " + ex.Message + " " + ex.InnerException);
            }
            finally
            {
                if ((grisServiceClientFromConfig != null) && (grisServiceClientFromConfig.State == CommunicationState.Opened))
                {
                    grisServiceClientFromConfig.Close();
                }

                if ((grisServiceClient != null) && (grisServiceClient.State == CommunicationState.Opened))
                {
                    grisServiceClient.Close();
                }

                this.Cursor = Cursors.Default;
            }
        }

        private void SaveSettings()
        {
            Settings.Default.EndpointUrl = this.txtEndpointUrl.Text;
            Settings.Default.ClientNodeId = this.txtLocationId.Text;
            Settings.Default.SecurityToken = this.txtSharedSecret.Text;
            Settings.Default.RegionId = this.txtRegionId.Text;
            Settings.Default.Save();
        }

        private void GetDatiLocalita(GrisServiceClient grisServiceClient, string sharedSecret, short localitaId)
        {
            Localita datiLocalita = grisServiceClient.OttieniDatiLocalita(sharedSecret, localitaId);

            if ((datiLocalita != null) && (datiLocalita.Locazione != null))
            {
                // Recupero dati topografici della località
                this.rtbLocality.AppendText(String.Format("Id Compartimento: {0} - Nome Compartimento: {1}\r\n",
                    datiLocalita.Locazione.IdCompartimento, datiLocalita.Locazione.NomeCompartimento));
                this.rtbLocality.AppendText(String.Format("Id Linea: {0} - Nome Linea: {1}\r\n", datiLocalita.Locazione.IdLinea,
                    datiLocalita.Locazione.NomeLinea));
                this.rtbLocality.AppendText(String.Format("Id Località: {0} - Nome Località: {1}\r\n", datiLocalita.Locazione.IdLocalita,
                    datiLocalita.Locazione.NomeLocalita));
                this.rtbLocality.AppendText(String.Format("Severità: {0}\r\n", datiLocalita.Severita.ToString()));
            }
        }

        private void GetDatiLocalitaByIdAndSistema(GrisServiceClient grisServiceClient, string sharedSecret, short localitaId, int sistemaId)
        {
            LocalitaSistemi datiLocalita = grisServiceClient.OttieniDatiLocalitaSistemi(sharedSecret, localitaId, sistemaId);

            if ((datiLocalita != null) && (datiLocalita.Locazione != null))
            {
                // Recupero dati topografici della località
                this.rtbLocality.AppendText(String.Format("Id Compartimento: {0} - Nome Compartimento: {1}\r\n",
                    datiLocalita.Locazione.IdCompartimento, datiLocalita.Locazione.NomeCompartimento));
                this.rtbLocality.AppendText(String.Format("Id Linea: {0} - Nome Linea: {1}\r\n", datiLocalita.Locazione.IdLinea,
                    datiLocalita.Locazione.NomeLinea));
                this.rtbLocality.AppendText(String.Format("Id Località: {0} - Nome Località: {1}\r\n", datiLocalita.Locazione.IdLocalita,
                    datiLocalita.Locazione.NomeLocalita));
                this.rtbLocality.AppendText(String.Format("Severità: {0}\r\n",  datiLocalita.Severita.ToString()));

                if (datiLocalita.Sistemi.Length > 0)
                {
                    // Recupero dati dei sistemi associati alla località
                    foreach (Sistema sistema in datiLocalita.Sistemi)
                    {
                        // Recupero dati delle periferiche associate alla località
                        foreach (Periferica periferica in sistema.Periferiche)
                        {
                            PerifericaWrapper perifericaWrapper = new PerifericaWrapper(periferica);
                            this.lstDevices.Items.Add(perifericaWrapper);
                        }
                    }
                }
            }
        }

        private void GetDatiPeriferica()
        {
            if (this.lstDevices.SelectedItem != null)
            {
                this.lstStreamFields.Items.Clear();

                short localitaId;
                long perifericaId = ((PerifericaWrapper) this.lstDevices.SelectedItem).Periferica.IdPeriferica;

                if ((Int16.TryParse(this.txtLocationId.Text, out localitaId)) && (perifericaId > 0))
                {
                    GrisServiceClient grisServiceClientFromConfig = null;
                    GrisServiceClient grisServiceClient = null;

                    try
                    {
                        this.Cursor = Cursors.WaitCursor;

                        // Usiamo i dati da configurazione, ma andiamo in override della URL dell'endpoint
                        // Inutile se non configurabile a runtime, si può usare direttamente da config, senza creare un oggetto doppio
                        grisServiceClientFromConfig = new GrisServiceClient();
                        ServiceEndpoint ep = grisServiceClientFromConfig.Endpoint;
                        ep.Address = new EndpointAddress(this.txtEndpointUrl.Text);
                        grisServiceClient = new GrisServiceClient("basicHttpBinding_IGrisService", ep.Address);

                        // Usato come shared-secret di sicurezza
                        // Nel caso sia sbagliato, non c'è feedback dal server, solo non si ottengono dati validi
                        string sharedSecret = this.txtSharedSecret.Text.Trim();

                        DettaglioPeriferica dettaglioPeriferica = grisServiceClient.OttieniDettaglioPeriferica(sharedSecret, localitaId, perifericaId);

                        if ((dettaglioPeriferica != null) && (dettaglioPeriferica.Periferica != null) && (dettaglioPeriferica.Stati != null))
                        {
                            if (dettaglioPeriferica.Stati.Length > 0)
                            {
                                // Recupero dati delle periferiche associate alla località
                                foreach (StatoPeriferica statoPeriferica in dettaglioPeriferica.Stati)
                                {
                                    this.lstStreamFields.Items.Add(
                                        String.Format(
                                            "Id Periferica: {0} - Id Stato: {1} - Nome Stato: {2} - Severità Stato: {3} ({11}) - Data Aggiornamento {4} - Id Stato Dettaglio: {5} - Id Stato Sotto Dettaglio: {6} - Stato Dettaglio Nome: {7} - Stato Dettaglio Severità: {8} - Stato Dettaglio Valore: {9} - Stato Dettaglio Descrizione: {10}",
                                            statoPeriferica.IdPeriferica.ToString(), statoPeriferica.IdStato.ToString(), statoPeriferica.StatoNome,
                                            statoPeriferica.StatoSeverita.ToString(),
                                            statoPeriferica.DataAggiornamento.ToString("dd/MM/yyyy HH.mm.ss"),
                                            statoPeriferica.IdStatoDettaglio.ToString(), statoPeriferica.IdStatoSottoDettaglio.ToString(),
                                            statoPeriferica.StatoDettaglioNome, statoPeriferica.StatoDettaglioSeverita.ToString(),
                                            statoPeriferica.StatoDettaglioValore, statoPeriferica.StatoDettaglioDescrizione,
                                            Enum.GetName(typeof (TipiSeverita), statoPeriferica.StatoSeverita)));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("GetDatiPeriferica " + ex.Message);
                    }
                    finally
                    {
                        if ((grisServiceClientFromConfig != null) && (grisServiceClientFromConfig.State == CommunicationState.Opened))
                        {
                            grisServiceClientFromConfig.Close();
                        }

                        if ((grisServiceClient != null) && (grisServiceClient.State == CommunicationState.Opened))
                        {
                            grisServiceClient.Close();
                        }

                        this.Cursor = Cursors.Default;
                    }
                }
            }
        }

        private void lstDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GetDatiPeriferica();
        }

        private void cboSystems_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GetDatiLocalitaSistemi(false);
        }

        private void lstSeverity_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GetDatiSeverity();
        }


    }

    public enum TipiSeverita
    {
        Informativo = -255,
        Ok = 0,
        AllarmeLieve = 1,
        AllarmeGrave = 2,
        DaConfigurare = 9,
        Sconosciuto = 255
    }

    public class PerifericaWrapper
    {
        public Periferica Periferica { get; private set; }

        public PerifericaWrapper(Periferica periferica)
        {
            this.Periferica = periferica;
        }

        public override string ToString()
        {
            return
                string.Format(
                    "Id Periferica: {0} - Nome: {1} - Tipo: {2} - Numero di serie: {3} - Indirizzo: {4} - Severità: {5} ({6}) - Descrizione severità: {7} - Descrizione severità completa: {8} - Server: {9} ({10}) - Sistema: {11} ({12})",
                    this.Periferica.IdPeriferica.ToString(), this.Periferica.NomePeriferica, this.Periferica.Tipo, this.Periferica.NumeroDiSerie,
                    this.Periferica.Indirizzo, this.Periferica.Severita.ToString(), Enum.GetName(typeof (TipiSeverita), this.Periferica.Severita),
                    this.Periferica.SeveritaDescrizione, this.Periferica.SeveritaDescrizioneCompleta, this.Periferica.Server, this.Periferica.ServerIP,
                    this.Periferica.NomeSistema, this.Periferica.IdSistema.ToString());
        }
    }

    public class SistemaWrapper
    {
        public Sistema Sistema { get; private set; }

        public SistemaWrapper(Sistema sistema)
        {
            this.Sistema = sistema;
        }

        public override string ToString()
        {
            return this.Sistema.NomeSistema;
        }
    }
}
﻿namespace LogParserHelper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnGenerateReport = new System.Windows.Forms.Button();
            this.rtbResults = new System.Windows.Forms.RichTextBox();
            this.txtSources = new System.Windows.Forms.TextBox();
            this.chkTestMode = new System.Windows.Forms.CheckBox();
            this.dtpDateFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpDateTo = new System.Windows.Forms.DateTimePicker();
            this.lblDateFrom = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblSources = new System.Windows.Forms.Label();
            this.btnClipboardCopy = new System.Windows.Forms.Button();
            this.chkShowRowsWithNoErrors = new System.Windows.Forms.CheckBox();
            this.chkGroupByWSMethods = new System.Windows.Forms.CheckBox();
            this.grpReportRange = new System.Windows.Forms.GroupBox();
            this.rdoByNone = new System.Windows.Forms.RadioButton();
            this.rdoByHour = new System.Windows.Forms.RadioButton();
            this.rdoByDay = new System.Windows.Forms.RadioButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.grpReportRange.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGenerateReport
            // 
            this.btnGenerateReport.Location = new System.Drawing.Point(255, 106);
            this.btnGenerateReport.Name = "btnGenerateReport";
            this.btnGenerateReport.Size = new System.Drawing.Size(107, 23);
            this.btnGenerateReport.TabIndex = 10;
            this.btnGenerateReport.Text = "Crea report";
            this.toolTip.SetToolTip(this.btnGenerateReport, "Esegue le query sulla sorgente indicata");
            this.btnGenerateReport.UseVisualStyleBackColor = true;
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            // 
            // rtbResults
            // 
            this.rtbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbResults.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbResults.Location = new System.Drawing.Point(12, 135);
            this.rtbResults.Name = "rtbResults";
            this.rtbResults.ReadOnly = true;
            this.rtbResults.Size = new System.Drawing.Size(560, 252);
            this.rtbResults.TabIndex = 12;
            this.rtbResults.Text = "";
            this.rtbResults.WordWrap = false;
            // 
            // txtSources
            // 
            this.txtSources.Location = new System.Drawing.Point(91, 38);
            this.txtSources.Name = "txtSources";
            this.txtSources.Size = new System.Drawing.Size(392, 20);
            this.txtSources.TabIndex = 5;
            this.txtSources.Text = "\\\\RFIAPPGRIW01PRO\\GrisSuite, \\\\RFIAPPGRIW02PRO\\GrisSuite";
            this.toolTip.SetToolTip(this.txtSources, "Sorgente dei dati su cui eseguire la query, separando sorgenti multiple con virgo" +
                    "le");
            // 
            // chkTestMode
            // 
            this.chkTestMode.AutoSize = true;
            this.chkTestMode.Location = new System.Drawing.Point(15, 110);
            this.chkTestMode.Name = "chkTestMode";
            this.chkTestMode.Size = new System.Drawing.Size(177, 17);
            this.chkTestMode.TabIndex = 8;
            this.chkTestMode.Text = "Modalità test (mostra solo query)";
            this.toolTip.SetToolTip(this.chkTestMode, "Visualizza le query che saranno eseguite via LogParser nella casella dei risultat" +
                    "i");
            this.chkTestMode.UseVisualStyleBackColor = true;
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateFrom.Location = new System.Drawing.Point(91, 12);
            this.dtpDateFrom.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtpDateFrom.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Size = new System.Drawing.Size(108, 20);
            this.dtpDateFrom.TabIndex = 1;
            this.toolTip.SetToolTip(this.dtpDateFrom, "Data da cui iniziare la ricerca negli eventi (sempre dalla mezzanotte)");
            this.dtpDateFrom.ValueChanged += new System.EventHandler(this.dtpDateFrom_ValueChanged);
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "dd/MM/yyyy";
            this.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateTo.Location = new System.Drawing.Point(375, 12);
            this.dtpDateTo.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtpDateTo.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.Size = new System.Drawing.Size(108, 20);
            this.dtpDateTo.TabIndex = 3;
            this.toolTip.SetToolTip(this.dtpDateTo, "Data fino a cui eseguire la ricerca negli eventi (fino alla mezzanotte del giorno" +
                    " indicato)");
            this.dtpDateTo.ValueChanged += new System.EventHandler(this.dtpDateTo_ValueChanged);
            // 
            // lblDateFrom
            // 
            this.lblDateFrom.AutoSize = true;
            this.lblDateFrom.Location = new System.Drawing.Point(12, 15);
            this.lblDateFrom.Name = "lblDateFrom";
            this.lblDateFrom.Size = new System.Drawing.Size(69, 13);
            this.lblDateFrom.TabIndex = 0;
            this.lblDateFrom.Text = "Da (0.00.00):";
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Location = new System.Drawing.Point(301, 15);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(68, 13);
            this.lblDateTo.TabIndex = 2;
            this.lblDateTo.Text = "A (23.59.59):";
            // 
            // lblSources
            // 
            this.lblSources.AutoSize = true;
            this.lblSources.Location = new System.Drawing.Point(12, 41);
            this.lblSources.Name = "lblSources";
            this.lblSources.Size = new System.Drawing.Size(73, 13);
            this.lblSources.TabIndex = 4;
            this.lblSources.Text = "Sorgente dati:";
            // 
            // btnClipboardCopy
            // 
            this.btnClipboardCopy.Location = new System.Drawing.Point(368, 106);
            this.btnClipboardCopy.Name = "btnClipboardCopy";
            this.btnClipboardCopy.Size = new System.Drawing.Size(115, 23);
            this.btnClipboardCopy.TabIndex = 11;
            this.btnClipboardCopy.Text = "Copia negli appunti";
            this.toolTip.SetToolTip(this.btnClipboardCopy, "Copia i risultati correnti nella clipboard di Windows, separati da tab");
            this.btnClipboardCopy.UseVisualStyleBackColor = true;
            this.btnClipboardCopy.Click += new System.EventHandler(this.btnClipboardCopy_Click);
            // 
            // chkShowRowsWithNoErrors
            // 
            this.chkShowRowsWithNoErrors.AutoSize = true;
            this.chkShowRowsWithNoErrors.Location = new System.Drawing.Point(15, 64);
            this.chkShowRowsWithNoErrors.Name = "chkShowRowsWithNoErrors";
            this.chkShowRowsWithNoErrors.Size = new System.Drawing.Size(215, 17);
            this.chkShowRowsWithNoErrors.TabIndex = 6;
            this.chkShowRowsWithNoErrors.Text = "Escludi righe con conteggio errori a zero";
            this.toolTip.SetToolTip(this.chkShowRowsWithNoErrors, "Non saranno presenti nel report le righe che non contengono errori (la colonna ch" +
                    "e indica il conteggio degli errori è configurata nel file .query)");
            this.chkShowRowsWithNoErrors.UseVisualStyleBackColor = true;
            // 
            // chkGroupByWSMethods
            // 
            this.chkGroupByWSMethods.AutoSize = true;
            this.chkGroupByWSMethods.Checked = true;
            this.chkGroupByWSMethods.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGroupByWSMethods.Location = new System.Drawing.Point(15, 87);
            this.chkGroupByWSMethods.Name = "chkGroupByWSMethods";
            this.chkGroupByWSMethods.Size = new System.Drawing.Size(169, 17);
            this.chkGroupByWSMethods.TabIndex = 7;
            this.chkGroupByWSMethods.Text = "Raggruppa per metodi del WS";
            this.toolTip.SetToolTip(this.chkGroupByWSMethods, "L\'elenco degli errori sarà diviso per nomi dei metodi del web service CollectorWS" +
                    "");
            this.chkGroupByWSMethods.UseVisualStyleBackColor = true;
            this.chkGroupByWSMethods.CheckedChanged += new System.EventHandler(this.chkGroupByWSMethods_CheckedChanged);
            // 
            // grpReportRange
            // 
            this.grpReportRange.Controls.Add(this.rdoByNone);
            this.grpReportRange.Controls.Add(this.rdoByHour);
            this.grpReportRange.Controls.Add(this.rdoByDay);
            this.grpReportRange.Location = new System.Drawing.Point(255, 60);
            this.grpReportRange.Name = "grpReportRange";
            this.grpReportRange.Size = new System.Drawing.Size(228, 40);
            this.grpReportRange.TabIndex = 9;
            this.grpReportRange.TabStop = false;
            this.grpReportRange.Text = "Range orario";
            this.toolTip.SetToolTip(this.grpReportRange, "Indica il filtro temporale secondo cui saranno differenziati i risultati");
            // 
            // rdoByNone
            // 
            this.rdoByNone.AutoSize = true;
            this.rdoByNone.Location = new System.Drawing.Point(159, 17);
            this.rdoByNone.Name = "rdoByNone";
            this.rdoByNone.Size = new System.Drawing.Size(67, 17);
            this.rdoByNone.TabIndex = 2;
            this.rdoByNone.Text = "Nessuno";
            this.toolTip.SetToolTip(this.rdoByNone, "Il report conterrà un\'aggregazione unica, non differenziata per data");
            this.rdoByNone.UseVisualStyleBackColor = true;
            this.rdoByNone.CheckedChanged += new System.EventHandler(this.rdoByNone_CheckedChanged);
            // 
            // rdoByHour
            // 
            this.rdoByHour.AutoSize = true;
            this.rdoByHour.Location = new System.Drawing.Point(94, 17);
            this.rdoByHour.Name = "rdoByHour";
            this.rdoByHour.Size = new System.Drawing.Size(59, 17);
            this.rdoByHour.TabIndex = 1;
            this.rdoByHour.Text = "Per ore";
            this.toolTip.SetToolTip(this.rdoByHour, "Il report sarà diviso per singola ora");
            this.rdoByHour.UseVisualStyleBackColor = true;
            this.rdoByHour.CheckedChanged += new System.EventHandler(this.rdoByHour_CheckedChanged);
            // 
            // rdoByDay
            // 
            this.rdoByDay.AutoSize = true;
            this.rdoByDay.Checked = true;
            this.rdoByDay.Location = new System.Drawing.Point(6, 17);
            this.rdoByDay.Name = "rdoByDay";
            this.rdoByDay.Size = new System.Drawing.Size(82, 17);
            this.rdoByDay.TabIndex = 0;
            this.rdoByDay.TabStop = true;
            this.rdoByDay.Text = "Per giornata";
            this.toolTip.SetToolTip(this.rdoByDay, "Il report sarà diviso per singola giornata (dalle 0.00 alle 23.59)");
            this.rdoByDay.UseVisualStyleBackColor = true;
            this.rdoByDay.CheckedChanged += new System.EventHandler(this.rdoByDay_CheckedChanged);
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 15000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.IsBalloon = true;
            this.toolTip.ReshowDelay = 100;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripProgressBar});
            this.statusStrip.Location = new System.Drawing.Point(0, 390);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(584, 22);
            this.statusStrip.TabIndex = 10;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.AutoSize = false;
            this.toolStripStatusLabel.Margin = new System.Windows.Forms.Padding(12, 3, 0, 2);
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(200, 17);
            this.toolStripStatusLabel.Text = "Queries da eseguire: 0";
            this.toolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.AutoSize = false;
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(270, 16);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 412);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.grpReportRange);
            this.Controls.Add(this.chkGroupByWSMethods);
            this.Controls.Add(this.chkShowRowsWithNoErrors);
            this.Controls.Add(this.btnClipboardCopy);
            this.Controls.Add(this.lblDateTo);
            this.Controls.Add(this.lblSources);
            this.Controls.Add(this.lblDateFrom);
            this.Controls.Add(this.dtpDateTo);
            this.Controls.Add(this.dtpDateFrom);
            this.Controls.Add(this.chkTestMode);
            this.Controls.Add(this.txtSources);
            this.Controls.Add(this.rtbResults);
            this.Controls.Add(this.btnGenerateReport);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 300);
            this.Name = "MainForm";
            this.Text = "Log Parser Helper";
            this.grpReportRange.ResumeLayout(false);
            this.grpReportRange.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerateReport;
        private System.Windows.Forms.RichTextBox rtbResults;
        private System.Windows.Forms.TextBox txtSources;
        private System.Windows.Forms.CheckBox chkTestMode;
        private System.Windows.Forms.DateTimePicker dtpDateFrom;
        private System.Windows.Forms.DateTimePicker dtpDateTo;
        private System.Windows.Forms.Label lblDateFrom;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblSources;
        private System.Windows.Forms.Button btnClipboardCopy;
        private System.Windows.Forms.CheckBox chkShowRowsWithNoErrors;
        private System.Windows.Forms.CheckBox chkGroupByWSMethods;
        private System.Windows.Forms.GroupBox grpReportRange;
        private System.Windows.Forms.RadioButton rdoByHour;
        private System.Windows.Forms.RadioButton rdoByDay;
        private System.Windows.Forms.RadioButton rdoByNone;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using LogQuery = MSUtil.LogQueryClassClass;
using EventLogInputFormat = MSUtil.COMEventLogInputContextClassClass;
using LogRecordSet = MSUtil.ILogRecordset;

namespace LogParserHelper
{
    public partial class MainForm : Form
    {
        delegate void AppenResultCallback(string text);

        const string CREA_REPORT = "Crea report";
        const string ANNULLA_REPORT = "Annulla report";

        private readonly Dictionary<string, string> webServiceMethods = new Dictionary<string, string>();
        private readonly Dictionary<string, string> exceptionTypes = new Dictionary<string, string>();
        private readonly Dictionary<string, string> sqlExceptionCategories = new Dictionary<string, string>();
        private readonly Dictionary<string, string> codeExceptionCategories = new Dictionary<string, string>();

        private readonly string mainQuery;
        private readonly int recordCounterColumn;

        private int executedQueryCounter;
        private object locker = new object();

        public MainForm()
        {
            this.InitializeComponent();

            this.btnGenerateReport.Text = CREA_REPORT;

            string queryConfigFilePath;

            if (Assembly.GetEntryAssembly() != null)
            {
                queryConfigFilePath = Assembly.GetEntryAssembly().Location + ".query";
            }
            else
            {
                queryConfigFilePath = Assembly.GetExecutingAssembly().Location + ".query";
            }

            XDocument queryConfig;

            if (File.Exists(queryConfigFilePath))
            {
                try
                {
                    queryConfig = XDocument.Load(queryConfigFilePath);

                    this.mainQuery = queryConfig.Descendants("MainQuery").FirstOrDefault().Value;

                    var webServiceMethodParseData = queryConfig.Descendants("WebServiceMethods").FirstOrDefault().Descendants("ParseData");
                    foreach (var parseData in webServiceMethodParseData)
                    {
                        if ((parseData.Attribute("filter") != null) && (parseData.Attribute("value") != null))
                        {
                            this.webServiceMethods.Add(parseData.Attribute("filter").Value, parseData.Attribute("value").Value);
                        }
                    }

                    var exceptionTypeParseData = queryConfig.Descendants("ExceptionTypes").FirstOrDefault().Descendants("ParseData");
                    foreach (var parseData in exceptionTypeParseData)
                    {
                        if ((parseData.Attribute("filter") != null) && (parseData.Attribute("value") != null))
                        {
                            this.exceptionTypes.Add(parseData.Attribute("filter").Value, parseData.Attribute("value").Value);
                        }
                    }

                    var sqlExceptionCategorieParseData = queryConfig.Descendants("SqlExceptionCategories").FirstOrDefault().Descendants("ParseData");
                    foreach (var parseData in sqlExceptionCategorieParseData)
                    {
                        if ((parseData.Attribute("filter") != null) && (parseData.Attribute("value") != null))
                        {
                            this.sqlExceptionCategories.Add(parseData.Attribute("filter").Value, parseData.Attribute("value").Value);
                        }
                    }

                    var codeExceptionCategorieParseData = queryConfig.Descendants("CodeExceptionCategories").FirstOrDefault().Descendants("ParseData");
                    foreach (var parseData in codeExceptionCategorieParseData)
                    {
                        if ((parseData.Attribute("filter") != null) && (parseData.Attribute("value") != null))
                        {
                            this.codeExceptionCategories.Add(parseData.Attribute("filter").Value, parseData.Attribute("value").Value);
                        }
                    }

                    this.recordCounterColumn = int.Parse(queryConfig.Descendants("RecordCounterColumn").FirstOrDefault().Value);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Error on configuration query file '{0}' load.\r\n{1}", queryConfigFilePath, ex.Message));
                    this.DisableButtons();
                }
            }

            this.dtpDateFrom.Value = DateTime.Now.AddDays(-2);
            this.dtpDateTo.Value = DateTime.Now;

            this.UpdateStatusQueriesData();
        }

        private void DisableButtons()
        {
            this.btnGenerateReport.Enabled = false;
            this.btnClipboardCopy.Enabled = false;
        }

        private void CreateAndRunQueries()
        {
            string startingQuery = this.mainQuery;
            startingQuery = startingQuery.Replace("@Sources@", this.txtSources.Text.Trim());

            DateTime dateFrom = new DateTime(this.dtpDateFrom.Value.Year, this.dtpDateFrom.Value.Month, this.dtpDateFrom.Value.Day, 0, 0, 0);
            DateTime dateTo = new DateTime(this.dtpDateTo.Value.Year, this.dtpDateTo.Value.Month, this.dtpDateTo.Value.Day, 23, 59, 59, 999);

            Dictionary<string, string> webServiceMethodsCopy = new Dictionary<string, string>();

            if (this.chkGroupByWSMethods.Checked)
            {
                foreach (var webServiceMethod in this.webServiceMethods)
                {
                    webServiceMethodsCopy.Add(webServiceMethod.Key, webServiceMethod.Value);
                }
            }
            else
            {
                webServiceMethodsCopy.Add("%%", "N/D");
            }

            lock (this.locker)
            {
                this.executedQueryCounter = 0;
            }

            string headers = string.Empty;
            StringBuilder report = new StringBuilder();

            if (this.rdoByDay.Checked)
            {
                DateTime dateFromPartial = dateFrom;
                DateTime dateToPartial = dateFromPartial.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

                string startingInnerQuery;

                while (dateToPartial <= dateTo)
                {
                    if (this.backgroundWorker.CancellationPending)
                    {
                        break;
                    }

                    startingInnerQuery = startingQuery.Replace("@DateFrom@", dateFromPartial.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));
                    startingInnerQuery = startingInnerQuery.Replace("@DateTo@", dateToPartial.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));

                    headers = this.CreateInnerQuery(startingInnerQuery, webServiceMethodsCopy, report, headers);

                    dateFromPartial = dateFromPartial.AddHours(24);
                    dateToPartial = dateToPartial.AddHours(24);
                }
            }
            else if (this.rdoByHour.Checked)
            {
                DateTime dateFromPartial = dateFrom;
                DateTime dateToPartial = dateFromPartial.AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

                string startingInnerQuery;

                while (dateToPartial <= dateTo)
                {
                    if (this.backgroundWorker.CancellationPending)
                    {
                        break;
                    }

                    startingInnerQuery = startingQuery.Replace("@DateFrom@", dateFromPartial.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));
                    startingInnerQuery = startingInnerQuery.Replace("@DateTo@", dateToPartial.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));

                    headers = this.CreateInnerQuery(startingInnerQuery, webServiceMethodsCopy, report, headers);

                    dateFromPartial = dateFromPartial.AddHours(1);
                    dateToPartial = dateToPartial.AddHours(1);
                }
            }
            else if (this.rdoByNone.Checked)
            {
                startingQuery = startingQuery.Replace("@DateFrom@", dateFrom.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));
                startingQuery = startingQuery.Replace("@DateTo@", dateTo.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));

                headers = this.CreateInnerQuery(startingQuery, webServiceMethodsCopy, report, headers);
            }
            else
            {
                startingQuery = startingQuery.Replace("@DateFrom@", dateFrom.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));
                startingQuery = startingQuery.Replace("@DateTo@", dateTo.ToString(@"yyyy-MM-dd HH\:mm\:ss.fff"));

                headers = this.CreateInnerQuery(startingQuery, webServiceMethodsCopy, report, headers);
            }

            if (!string.IsNullOrEmpty(headers))
            {
                this.AppendResult(headers + Environment.NewLine);
            }

            if (report.Length > 0)
            {
                this.AppendResult(report.ToString());
            }
        }

        private string CreateInnerQuery(string startingQuery, Dictionary<string, string> webServiceMethodsCopy, StringBuilder report, string headers)
        {
            if (this.backgroundWorker.CancellationPending)
            {
                return string.Empty;
            }

            foreach (var webServiceMethod in webServiceMethodsCopy)
            {
                if (this.backgroundWorker.CancellationPending)
                {
                    break;
                }

                string webServiceMethodQuery = startingQuery;
                webServiceMethodQuery = webServiceMethodQuery.Replace("@WebServiceMethodName@", webServiceMethod.Value);
                webServiceMethodQuery = webServiceMethodQuery.Replace("@WebServiceMethodValue@", webServiceMethod.Key);

                foreach (var exceptionType in this.exceptionTypes)
                {
                    if (this.backgroundWorker.CancellationPending)
                    {
                        break;
                    }

                    string exceptionTypeQuery = webServiceMethodQuery;
                    exceptionTypeQuery = exceptionTypeQuery.Replace("@ExceptionTypeName@", exceptionType.Value);
                    exceptionTypeQuery = exceptionTypeQuery.Replace("@ExceptionTypeValue@", exceptionType.Key);

                    if (exceptionType.Key.Contains("SQL"))
                    {
                        foreach (var sqlExceptionCategory in this.sqlExceptionCategories)
                        {
                            if (this.backgroundWorker.CancellationPending)
                            {
                                break;
                            }

                            string sqlExceptionCategoryQuery = exceptionTypeQuery;
                            sqlExceptionCategoryQuery += " AND Strings LIKE '@ExceptionCategoryValue@'";
                            sqlExceptionCategoryQuery = sqlExceptionCategoryQuery.Replace("@ExceptionCategoryName@", sqlExceptionCategory.Value);
                            sqlExceptionCategoryQuery = sqlExceptionCategoryQuery.Replace("@ExceptionCategoryValue@", sqlExceptionCategory.Key);

                            report.Append(this.RunQuery(sqlExceptionCategoryQuery, this.chkTestMode.Checked, out headers));
                        }

                        if (this.backgroundWorker.CancellationPending)
                        {
                            break;
                        }

                        string sqlExceptionCategoryOtherQuery = exceptionTypeQuery;
                        foreach (var sqlExceptionCategory in this.sqlExceptionCategories)
                        {
                            sqlExceptionCategoryOtherQuery += string.Format(" AND Strings NOT LIKE '{0}'", sqlExceptionCategory.Key);
                        }
                        sqlExceptionCategoryOtherQuery = sqlExceptionCategoryOtherQuery.Replace("@ExceptionCategoryName@", "Non categorizzato");
                        report.Append(this.RunQuery(sqlExceptionCategoryOtherQuery, this.chkTestMode.Checked, out headers));
                    }
                    else
                    {
                        foreach (var codeExceptionCategory in this.codeExceptionCategories)
                        {
                            if (this.backgroundWorker.CancellationPending)
                            {
                                break;
                            }

                            string codeExceptionCategoryQuery = exceptionTypeQuery;
                            codeExceptionCategoryQuery += " AND Strings LIKE '@ExceptionCategoryValue@'";
                            codeExceptionCategoryQuery = codeExceptionCategoryQuery.Replace("@ExceptionCategoryName@", codeExceptionCategory.Value);
                            codeExceptionCategoryQuery = codeExceptionCategoryQuery.Replace("@ExceptionCategoryValue@", codeExceptionCategory.Key);

                            report.Append(this.RunQuery(codeExceptionCategoryQuery, this.chkTestMode.Checked, out headers));
                        }

                        if (this.backgroundWorker.CancellationPending)
                        {
                            break;
                        }

                        string codeExceptionCategoryOtherQuery = exceptionTypeQuery;
                        foreach (var codeExceptionCategory in this.codeExceptionCategories)
                        {
                            codeExceptionCategoryOtherQuery += string.Format(" AND Strings NOT LIKE '{0}'", codeExceptionCategory.Key);
                        }
                        codeExceptionCategoryOtherQuery = codeExceptionCategoryOtherQuery.Replace("@ExceptionCategoryName@", "Non categorizzato");
                        report.Append(this.RunQuery(codeExceptionCategoryOtherQuery, this.chkTestMode.Checked, out headers));
                    }
                }
            }

            return headers;
        }

        private string RunQuery(string query, bool testMode, out string headers)
        {
            lock (this.locker)
            {
                this.executedQueryCounter++;
            }

            StringBuilder returnData = new StringBuilder();
            headers = string.Empty;

            if (testMode)
            {
                returnData.AppendLine(query);
            }
            else
            {
                // Instantiate the LogQuery object
                LogQuery oLogQuery = new LogQuery();

                // Instantiate the Event Log Input Format object
                // Set its "direction" parameter to "BW"
                EventLogInputFormat oEVTInputFormat = new EventLogInputFormat {direction = "BW"};

                // Execute the query
                LogRecordSet oRecordSet = oLogQuery.Execute(query, oEVTInputFormat);

                // Browse the recordset
                for (; !oRecordSet.atEnd(); oRecordSet.moveNext())
                {
                    if (this.chkShowRowsWithNoErrors.Checked)
                    {
                        if (this.recordCounterColumn < oRecordSet.getColumnCount())
                        {
                            int recordCounterColumnValue;
                            if (int.TryParse(oRecordSet.getRecord().getValue(this.recordCounterColumn).ToString(), out recordCounterColumnValue))
                            {
                                if (recordCounterColumnValue > 0)
                                {
                                    returnData.AppendLine(oRecordSet.getRecord().toNativeString("\t"));
                                }
                            }
                        }
                        else
                        {
                            returnData.AppendLine(oRecordSet.getRecord().toNativeString("\t"));
                        }
                    }
                    else
                    {
                        returnData.AppendLine(oRecordSet.getRecord().toNativeString("\t"));
                    }
                }

                if (headers.Length == 0)
                {
                    for (int columnCounter = 0; columnCounter < oRecordSet.getColumnCount(); columnCounter++)
                    {
                        headers += oRecordSet.getColumnName(columnCounter) + "\t";
                    }
                }

                // Close the recordset
                oRecordSet.close();
            }

            this.backgroundWorker.ReportProgress((this.executedQueryCounter*100)/this.CalculateQueriesNumber());

            return returnData.ToString();
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            if (this.btnGenerateReport.Text == CREA_REPORT)
            {
                this.rtbResults.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.btnGenerateReport.Text = ANNULLA_REPORT;
                this.toolStripProgressBar.Value = 0;
                this.backgroundWorker.RunWorkerAsync();
            }
            else
            {
                this.backgroundWorker.CancelAsync();
            }
        }

        private void btnClipboardCopy_Click(object sender, EventArgs e)
        {
            if (this.rtbResults.TextLength > 0)
            {
                Clipboard.SetText(this.rtbResults.Text);
            }
        }

        private void UpdateStatusQueriesData()
        {
            this.toolStripStatusLabel.Text = string.Format("Queries da eseguire: {0}", this.CalculateQueriesNumber());
        }

        private int CalculateQueriesNumber()
        {
            int timeRangeCounter = 0;
            int webServiceMethodsCount = this.webServiceMethods.Count;
            //int exceptionTypesCount = this.exceptionTypes.Count;
            int sqlExceptionCategoriesCount = this.sqlExceptionCategories.Count + 1 /* Query per i "non categorizzati" che corrisponde ai NOT IN */;
            int codeExceptionCategoriesCount = this.codeExceptionCategories.Count + 1 /* Query per i "non categorizzati" che corrisponde ai NOT IN */;

            DateTime dateFrom = new DateTime(this.dtpDateFrom.Value.Year, this.dtpDateFrom.Value.Month, this.dtpDateFrom.Value.Day, 0, 0, 0);
            DateTime dateTo = new DateTime(this.dtpDateTo.Value.Year, this.dtpDateTo.Value.Month, this.dtpDateTo.Value.Day, 23, 59, 59, 999);

            if (!this.chkGroupByWSMethods.Checked)
            {
                webServiceMethodsCount = 1;
            }

            if (this.rdoByDay.Checked)
            {
                DateTime dateFromPartial = dateFrom;
                DateTime dateToPartial = dateFromPartial.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

                while (dateToPartial <= dateTo)
                {
                    timeRangeCounter++;

                    dateFromPartial = dateFromPartial.AddHours(24);
                    dateToPartial = dateToPartial.AddHours(24);
                }
            }
            else if (this.rdoByHour.Checked)
            {
                DateTime dateFromPartial = dateFrom;
                DateTime dateToPartial = dateFromPartial.AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

                while (dateToPartial <= dateTo)
                {
                    timeRangeCounter++;

                    dateFromPartial = dateFromPartial.AddHours(1);
                    dateToPartial = dateToPartial.AddHours(1);
                }
            }
            else if (this.rdoByNone.Checked)
            {
                timeRangeCounter++;
            }
            else
            {
                timeRangeCounter++;
            }

            return timeRangeCounter*webServiceMethodsCount*(sqlExceptionCategoriesCount + codeExceptionCategoriesCount);
        }

        private void dtpDateFrom_ValueChanged(object sender, EventArgs e)
        {
            this.UpdateStatusQueriesData();
        }

        private void dtpDateTo_ValueChanged(object sender, EventArgs e)
        {
            this.UpdateStatusQueriesData();
        }

        private void chkGroupByWSMethods_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateStatusQueriesData();
        }

        private void rdoByDay_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateStatusQueriesData();
        }

        private void rdoByHour_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateStatusQueriesData();
        }

        private void rdoByNone_CheckedChanged(object sender, EventArgs e)
        {
            this.UpdateStatusQueriesData();
        }

        private void backgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            this.CreateAndRunQueries();
        }

        private void backgroundWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            this.toolStripProgressBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.toolStripProgressBar.Value = 100;

            this.Cursor = Cursors.Default;

            this.btnGenerateReport.Text = CREA_REPORT;
        }

        private void AppendResult(string text)
        {
            if (this.rtbResults.InvokeRequired)
            {
                AppenResultCallback d = this.AppendResult;
                this.Invoke(d, new object[] {text});
            }
            else
            {
                this.rtbResults.AppendText(text);
                this.rtbResults.ScrollToCaret();
            }
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace STLCImageSplash
{
	public partial class Splash : Form
	{
		private readonly Properties.Settings _settings;
		private readonly string _batchFilePath;
		
		public Splash ()
		{
			InitializeComponent();
			this.pssTasks.Step = 1;

			this._settings = Properties.Settings.Default;

			string workingDir = ( this._settings.WorkingDir.Length == 0 )
									? Directory.GetCurrentDirectory()
									: this._settings.WorkingDir;

			string batchFileName = ( this._settings.BatchFileName.Length == 0 )
									? "Apply.STLCImage.cmd"
									: this._settings.BatchFileName;
									
			this._batchFilePath = Path.Combine(workingDir, batchFileName);
			
			this.lblVersion.Text = ( this._settings.ImageVersion.Length == 0 )
			                       	? ""
									: string.Format("Versione: {0} ({1})", this._settings.ImageVersion, this.GetFirstWIMFile(workingDir));
		}

		private void btnClose_Click ( object sender, EventArgs e )
		{
			Application.Exit();
		}

		private void btnApply_Click ( object sender, EventArgs e )
		{
			if ( File.Exists(this._batchFilePath) )
			{
				ProcessStartInfo procInfo = new ProcessStartInfo(this._batchFilePath)
				                            {
				                            	CreateNoWindow = true,
				                            	UseShellExecute = false,
				                            	ErrorDialog = false,
				                            	RedirectStandardOutput = true
				                            };

				try 
				{
					Process process = Process.Start(procInfo);

					if ( process != null )
					{
						using ( BackgroundWorker worker = new BackgroundWorker() )
						{
							StreamReader sr = process.StandardOutput;

							worker.WorkerSupportsCancellation = false;
							worker.WorkerReportsProgress = true;
							worker.DoWork += worker_DoWork;
							worker.ProgressChanged += worker_ProgressChanged;
							worker.RunWorkerCompleted += worker_RunWorkerCompleted;
							worker.RunWorkerAsync(sr);

							this.btnApply.Enabled = false;
							this.btnClose.Enabled = false;

							this.tmrProgress.Start();
						}
					}
					else
					{
						MessageBox.Show(this, "Impossibile avviare il batch per l'applicazione dell'immagine");
					}
				} 
				catch (Exception ex) 
				{
					MessageBox.Show(ex.Message); 
				} 
			}
			else
			{
				MessageBox.Show(this, "Impossibile trovare il file di batch per l'applicazione dell'immagine.");
			}
		}

		private void worker_DoWork ( object sender, DoWorkEventArgs e )
		{
			string outputMessages = "";
			BackgroundWorker worker = (BackgroundWorker)sender;
			StreamReader sr = (StreamReader)e.Argument;

			using ( sr )
			{
				while ( !sr.EndOfStream )
				{
					string message = string.Format("{0}{1}", sr.ReadLine(), Environment.NewLine);
					worker.ReportProgress(1, message);
					outputMessages += message;
				}

				sr.Close();				
			}

			e.Result = outputMessages;
		}

		private void worker_ProgressChanged ( object sender, ProgressChangedEventArgs e )
		{
			// questa implementazione del timer non permette il reset del conteggio
			this.tmrProgress.Stop();

			string messageLine = e.UserState.ToString();
			this.txtDisplay.AppendText(messageLine);
			this.txtDisplay.ScrollToCaret();
			
			// 10 - 20 - 5 - 65
			if ( messageLine.IndexOf("STEP", StringComparison.InvariantCultureIgnoreCase) != -1 )
			{
				short stepNum;
				string step = messageLine.Substring(messageLine.IndexOf("STEP", StringComparison.InvariantCultureIgnoreCase), 7);
				step = step.Remove(0, 4).Trim(' ');

				if ( short.TryParse(step, out stepNum) )
				{
					switch ( stepNum )
					{
						case 1:
							{
								this.pssTasks.Value = 400;
								break;
							}
						case 2:
							{
								this.pssTasks.Step = 2;
								this.pssTasks.Value = 1000;
								break;
							}
						//case 3:
						//    {
						//        this.pssTasks.Value = 350;
						//        break;
						//    }
						//case 4:
						//    {
						//        this.pssTasks.Value = 1000;
						//        break;
						//    }
					}
				}
			}
			else
			{
				this.pssTasks.PerformStep();
			}

			this.tmrProgress.Start();
		}

		private void worker_RunWorkerCompleted ( object sender, RunWorkerCompletedEventArgs e )
		{
			this.txtDisplay.Text = e.Result.ToString();
			this.btnApply.Enabled = true;
			this.btnClose.Enabled = true;

			string message = "Procedura di ripristino dell'Immagine completata. Arrestare il Sistema ora?";

			if ( MessageBox.Show(this, message, "", MessageBoxButtons.YesNo) == DialogResult.Yes )
			{
				Process.Start("Shutdown", "/s /t 0");
			}
		}

		private void tmrProgress_Tick ( object sender, EventArgs e )
		{
			this.pssTasks.PerformStep();
		}

		private string GetFirstWIMFile ( string dirPath )
		{
			string wimDirPath = dirPath + @"\WIM";
			if ( Directory.Exists(wimDirPath) && Directory.GetFiles(wimDirPath, "*.wim").Length > 0 )
			{
				DirectoryInfo dir = new DirectoryInfo(wimDirPath);
				return dir.GetFiles("*.wim")[0].Name;
			}
			
			return "";
		}
	}
}

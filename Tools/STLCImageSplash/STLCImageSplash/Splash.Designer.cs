﻿namespace STLCImageSplash
{
	partial class Splash
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose ( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.components = new System.ComponentModel.Container();
			this.btnApply = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.txtDisplay = new System.Windows.Forms.TextBox();
			this.pssTasks = new System.Windows.Forms.ProgressBar();
			this.tmrProgress = new System.Windows.Forms.Timer(this.components);
			this.lblVersion = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnApply
			// 
			this.btnApply.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.btnApply.BackColor = System.Drawing.Color.DimGray;
			this.btnApply.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnApply.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
			this.btnApply.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(( (int)( ( (byte)( 64 ) ) ) ), ( (int)( ( (byte)( 64 ) ) ) ), ( (int)( ( (byte)( 64 ) ) ) ));
			this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnApply.ForeColor = System.Drawing.Color.Orange;
			this.btnApply.Location = new System.Drawing.Point(30, 235);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new System.Drawing.Size(493, 24);
			this.btnApply.TabIndex = 0;
			this.btnApply.Text = "Applica Immagine";
			this.btnApply.UseVisualStyleBackColor = false;
			this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.btnClose.BackColor = System.Drawing.Color.DimGray;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
			this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(( (int)( ( (byte)( 64 ) ) ) ), ( (int)( ( (byte)( 64 ) ) ) ), ( (int)( ( (byte)( 64 ) ) ) ));
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.Orange;
			this.btnClose.Location = new System.Drawing.Point(30, 265);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(493, 24);
			this.btnClose.TabIndex = 1;
			this.btnClose.Text = "Esci";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// txtDisplay
			// 
			this.txtDisplay.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
						| System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.txtDisplay.Location = new System.Drawing.Point(30, 329);
			this.txtDisplay.Multiline = true;
			this.txtDisplay.Name = "txtDisplay";
			this.txtDisplay.ReadOnly = true;
			this.txtDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtDisplay.Size = new System.Drawing.Size(493, 355);
			this.txtDisplay.TabIndex = 2;
			// 
			// pssTasks
			// 
			this.pssTasks.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.pssTasks.Location = new System.Drawing.Point(30, 295);
			this.pssTasks.Maximum = 1000;
			this.pssTasks.Name = "pssTasks";
			this.pssTasks.Size = new System.Drawing.Size(493, 25);
			this.pssTasks.Step = 1;
			this.pssTasks.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.pssTasks.TabIndex = 3;
			// 
			// tmrProgress
			// 
			this.tmrProgress.Interval = 7000;
			this.tmrProgress.Tick += new System.EventHandler(this.tmrProgress_Tick);
			// 
			// lblVersion
			// 
			this.lblVersion.AutoSize = true;
			this.lblVersion.BackColor = System.Drawing.Color.Transparent;
			this.lblVersion.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ));
			this.lblVersion.ForeColor = System.Drawing.Color.White;
			this.lblVersion.Location = new System.Drawing.Point(44, 77);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(0, 16);
			this.lblVersion.TabIndex = 4;
			// 
			// Splash
			// 
			this.AcceptButton = this.btnApply;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::STLCImageSplash.Properties.Resources.WINPE;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(1121, 708);
			this.Controls.Add(this.lblVersion);
			this.Controls.Add(this.pssTasks);
			this.Controls.Add(this.txtDisplay);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnApply);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Splash";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Splash";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TextBox txtDisplay;
		private System.Windows.Forms.ProgressBar pssTasks;
		private System.Windows.Forms.Timer tmrProgress;
		private System.Windows.Forms.Label lblVersion;
	}
}
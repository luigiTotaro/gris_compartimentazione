﻿using System.Globalization;

namespace TestDeviceStatus
{
    /// <summary>
    ///     Contiene i dati di stato generali della periferica
    /// </summary>
    public class DeviceStatusData
    {
        /// <summary>
        ///     ID della periferica
        /// </summary>
        public long DeviceID { get; private set; }

        /// <summary>
        ///     Nome della periferica
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        ///     Numero di serie della periferica
        /// </summary>
        public string SerialNumber { get; private set; }

        /// <summary>
        ///     Indirizzo IP della periferica
        /// </summary>
        public string IP { get; private set; }

        /// <summary>
        ///     Indirizzo IP della periferica, come codificato su database
        /// </summary>
        public string EncodedIP { get; private set; }

        /// <summary>
        ///     Tipo della periferica
        /// </summary>
        public string DeviceType { get; private set; }

        /// <summary>
        ///     Nome della periferica che esegue il monitoraggio della periferica corrente
        /// </summary>
        public string MonitoringDeviceID { get; private set; }

        /// <summary>
        ///     ID della stazione a cui è associata la periferica
        /// </summary>
        public long NodeID { get; private set; }

        /// <summary>
        ///     Nome della stazione a cui è associata la periferica
        /// </summary>
        public string NodeName { get; private set; }

        /// <summary>
        ///     Severità attuale della periferica
        /// </summary>
        public Severity SevLevel { get; private set; }

        /// <summary>
        ///     Descrizione dello stato attuale della periferica
        /// </summary>
        public string StatusDescription { get; private set; }

        /// <summary>
        ///     Indica se la periferica è offline, quindi non raggiungibile in rete
        /// </summary>
        public bool IsOffline { get; private set; }

        /// <summary>
        ///     Dati della stazione, decodificati dall'ID su database
        /// </summary>
        public NodeIdentifier NodeData { get; private set; }

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="deviceID">ID della periferica</param>
        /// <param name="deviceName">Nome della periferica</param>
        /// <param name="serialNumber">Numero di serie della periferica</param>
        /// <param name="ip">Indirizzo IP della periferica</param>
        /// <param name="encodedIP">Indirizzo IP della periferica, come codificato su database</param>
        /// <param name="deviceType">Tipo della periferica</param>
        /// <param name="monitoringDeviceID">Nome della periferica che esegue il monitoraggio della periferica corrente</param>
        /// <param name="nodeID">ID della stazione a cui è associata la periferica</param>
        /// <param name="nodeName">Nome della stazione a cui è associata la periferica</param>
        /// <param name="sevLevel">Severità attuale della periferica</param>
        /// <param name="statusDescription">Descrizione dello stato attuale della periferica</param>
        /// <param name="isOffline">Indica se la periferica è offline, quindi non raggiungibile in rete</param>
        public DeviceStatusData(long deviceID,
            string deviceName,
            string serialNumber,
            string ip,
            string encodedIP,
            string deviceType,
            string monitoringDeviceID,
            long nodeID,
            string nodeName,
            Severity sevLevel,
            string statusDescription,
            bool isOffline)
        {
            this.DeviceID = deviceID;
            this.DeviceName = deviceName;
            this.SerialNumber = serialNumber;
            this.IP = ip;
            this.EncodedIP = encodedIP;
            this.DeviceType = deviceType;
            this.MonitoringDeviceID = monitoringDeviceID;
            this.NodeID = nodeID;
            this.NodeName = nodeName;
            this.SevLevel = sevLevel;
            this.StatusDescription = statusDescription;
            this.IsOffline = isOffline;
            this.NodeData = new NodeIdentifier(nodeID);
        }

        /// <summary>
        ///     Produce una rappresentazione completa dei dati generali della periferica
        /// </summary>
        /// <returns>Rappresentazione testuale dei dati generali della periferica</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture,
                "Id periferica: {0}, Nome periferica: {1}, Numero di serie: {2}, IP: {3}, IP codificato per database: {4}, Tipo periferica: {5}, Nome della periferica che esegue il monitoraggio della periferica corrente: {6}, Id della stazione da database: {7}, Nome della stazione: {8}, {9}, Severità attuale della periferica: {10}, Descrizione dello stato attuale della periferica: {11}, Periferica offline: {12}",
                this.DeviceID, this.DeviceName, this.SerialNumber, this.IP, this.EncodedIP, this.DeviceType,
                this.MonitoringDeviceID != null ? this.MonitoringDeviceID : "N/D", this.NodeID, this.NodeName, this.NodeData, this.SevLevel,
                this.StatusDescription, this.IsOffline ? "Sì" : "No");
        }
    }
}
﻿using System.Globalization;

namespace TestDeviceStatus
{
    /// <summary>
    /// Incapsula le logiche di codifica / decodifica di un Node Id da Database a System.xml e viceversa 
    /// </summary>
    public struct NodeIdentifier
    {
        /// <summary>
        /// Node Id originale da System.xml
        /// </summary>
        public ushort OriginalNodeId;

        /// <summary>
        /// Zone Id originale da System.xml
        /// </summary>
        public ushort OriginalZoneId;

        /// <summary>
        /// Region Id originale da System.xml
        /// </summary>
        public ushort OriginalRegionId;

        /// <summary>
        /// Node Id da database
        /// </summary>
        public long NodeId;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="nodeIdFromDatabase">Node Id da database</param>
        public NodeIdentifier(long nodeIdFromDatabase)
        {
            this.OriginalNodeId = (ushort)((((ulong)nodeIdFromDatabase) & 0xFFFF00000000) >> 32);
            this.OriginalZoneId = (ushort)((((ulong)nodeIdFromDatabase) & 0xFFFF0000) >> 16);
            this.OriginalRegionId = (ushort)((((ulong)nodeIdFromDatabase) & 0xFFFF));
            this.NodeId = nodeIdFromDatabase;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="originalNodeId">Node Id originale da System.xml</param>
        /// <param name="originalZoneId">Zone Id originale da System.xml</param>
        /// <param name="originalRegionId">Region Id originale da System.xml</param>
        public NodeIdentifier(ushort originalNodeId, ushort originalZoneId, ushort originalRegionId)
        {
            this.NodeId = (long)(originalRegionId | ((ulong)originalZoneId << 16) | ((ulong)originalNodeId << 32));
            this.OriginalNodeId = originalNodeId;
            this.OriginalZoneId = originalZoneId;
            this.OriginalRegionId = originalRegionId;
        }

        /// <summary>
        /// Produce rappresentazione stringa dei dati della stazione
        /// </summary>
        /// <returns>Rappresentazione stringa della stazione</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Id della stazione da configurazione: {0}, Id linea: {1}, Id compartimento: {2}", this.OriginalNodeId, this.OriginalZoneId, this.OriginalRegionId);
        }
    }
}

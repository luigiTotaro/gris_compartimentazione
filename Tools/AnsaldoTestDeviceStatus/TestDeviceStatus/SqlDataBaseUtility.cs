﻿using System;
using System.Data;
using System.Data.SqlClient;
using TestDeviceStatus.Properties;

namespace TestDeviceStatus
{
    public class SqlDataBaseUtility
    {
        /// <summary>
        ///     Recupera i dati generali di stato della periferica
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione a SQL Server</param>
        /// <param name="deviceIP">Indirizzo IP della periferica</param>
        /// <returns>Oggetto che contiene i dati della periferica richiesta, nullo se la periferica non esiste</returns>
        public static DeviceStatusData GetDeviceDataByIP(string dbConnectionString, string deviceIP)
        {
            DeviceStatusData deviceStatus = null;

            if (!String.IsNullOrEmpty(deviceIP))
            {
                SqlConnection dbConnectionGetData = null;
                SqlCommand cmdGetData = new SqlCommand();
                SqlDataReader drGetData = null;

                try
                {
                    try
                    {
                        dbConnectionGetData = new SqlConnection(dbConnectionString);
                    }
                    catch (ArgumentException ex)
                    {
                        // TODO: Log "La stringa di connessione al database non è valida. " + ex.Message
                        return null;
                    }

                    cmdGetData.CommandType = CommandType.StoredProcedure;
                    cmdGetData.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                    cmdGetData.CommandText = "tf_GetDeviceDataByIP";
                    cmdGetData.Parameters.Add(new SqlParameter("@IP", SqlDbType.VarChar, 15, ParameterDirection.Input, false, 0, 0, null,
                        DataRowVersion.Current, deviceIP));

                    cmdGetData.Connection = dbConnectionGetData;

                    dbConnectionGetData.Open();

                    drGetData = cmdGetData.ExecuteReader();

                    if (drGetData.Read())
                    {
                        int colDevID = drGetData.GetOrdinal("DevID");
                        int colDeviceName = drGetData.GetOrdinal("DeviceName");
                        int colSN = drGetData.GetOrdinal("SN");
                        int colAddr = drGetData.GetOrdinal("Addr");
                        int colIP = drGetData.GetOrdinal("IP");
                        int colType = drGetData.GetOrdinal("Type");
                        int colMonitoringDeviceID = drGetData.GetOrdinal("MonitoringDeviceID");
                        int colNodID = drGetData.GetOrdinal("NodID");
                        int colNodeName = drGetData.GetOrdinal("NodeName");
                        int colSevLevel = drGetData.GetOrdinal("SevLevel");
                        int colDescription = drGetData.GetOrdinal("Description");
                        int colOffline = drGetData.GetOrdinal("Offline");

                        do
                        {
                            if ((!drGetData.IsDBNull(colDevID)) && (!drGetData.IsDBNull(colAddr)) && (!drGetData.IsDBNull(colIP)))
                            {
                                deviceStatus = new DeviceStatusData(drGetData.GetSqlInt64(colDevID).Value, drGetData.GetSqlString(colDeviceName).Value,
                                    drGetData.GetSqlString(colSN).Value, drGetData.GetSqlString(colIP).Value, drGetData.GetSqlString(colAddr).Value,
                                    drGetData.GetSqlString(colType).Value,
                                    drGetData.IsDBNull(colMonitoringDeviceID) ? null : drGetData.GetSqlString(colMonitoringDeviceID).Value,
                                    drGetData.GetSqlInt64(colNodID).Value, drGetData.GetSqlString(colNodeName).Value,
                                    (Severity) drGetData.GetSqlInt32(colSevLevel).Value, drGetData.GetSqlString(colDescription).Value,
                                    (drGetData.GetSqlByte(colOffline).Value != 0));
                            }
                        } while (drGetData.Read());
                    }
                }
                catch (SqlException ex)
                {
                    // TODO: Log "Errore durante l'accesso alla base dati. " + ex.Message
                }
                catch (IndexOutOfRangeException ex)
                {
                    // TODO: Log "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando GetDeviceDataByIP. " + ex.Message
                }
                finally
                {
                    if (drGetData != null && !drGetData.IsClosed)
                    {
                        drGetData.Close();
                    }

                    if (dbConnectionGetData != null)
                    {
                        dbConnectionGetData.Close();
                    }

                    cmdGetData.Dispose();
                }
            }

            return deviceStatus;
        }
    }
}
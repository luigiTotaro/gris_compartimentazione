﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestDeviceStatus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            string dbConnectionString = this.txtConnectionString.Text.Trim();
            string deviceIP = this.txtDeviceIP.Text.Trim();

            if ((!String.IsNullOrEmpty(deviceIP)) && (!String.IsNullOrEmpty(dbConnectionString)))
            {
                DeviceStatusData deviceStatusData = SqlDataBaseUtility.GetDeviceDataByIP(dbConnectionString, deviceIP);

                this.txtOutput.Clear();

                if (deviceStatusData != null)
                {
                    this.txtOutput.AppendText(deviceStatusData.ToString().Replace(", ", "\r\n"));
                }
                else
                {
                    this.txtOutput.AppendText(String.Format("Impossibile trovare i dati di stato della periferica con IP: {0}\r\n", deviceIP));
                }
            }
        }
    }
}

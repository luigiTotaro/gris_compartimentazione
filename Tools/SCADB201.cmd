@echo off

echo Rimuovi GrisSuite.DB.Client.Setup 1.4
msiexec.exe /x {CA892D40-73F3-4E31-A291-A05BED93D327} /passive 

echo Rimuovi GrisSuite.DB.Client.Setup 2.0
msiexec.exe /x {0F1565DE-4665-4226-AA5B-7FD56EB65E90} /passive 

echo Rimuovi GrisSuite.DB.Client.Setup 2.0.1
msiexec.exe /x {10911D75-4C65-4F89-9BDD-436AF76E4719} /passive 

echo Rimuovi GrisSuite.SCAgentSetup 1.4
msiexec.exe /x {8D09E54E-0563-4BAB-B535-254E570B2FE0} /passive 

echo Rimuovi GrisSuite.SCAgentSetup 2.0
msiexec.exe /x {3A6FC14C-5744-40AC-8819-962083B6CCA5} /passive 

echo Rimuovi GrisSuite.SCAgentSetup 2.0.1
msiexec.exe /x {C48DA172-0CB9-4D69-8C17-CC44014BDFBC} /passive 

echo Install GrisSuite.DB.Client.Setup 2.0.1
msiexec.exe /i "GrisSuite.DB.Client.Setup.msi" /passive

echo Install GrisSuite.SCAgentSetup 2.0.1
msiexec.exe /i "GrisSuite.SCAgentSetup.msi" /passive


echo STOP Supervisor service
sc stop SPVServerDaemon
> "%Temp%.\sleep.vbs" ECHO WScript.Sleep 1000
CSCRIPT //NoLogo "%Temp%.\sleep.vbs"
DEL "%Temp%.\sleep.vbs"

echo START Supervisor service
sc start SPVServerDaemon
> "%Temp%.\sleep.vbs" ECHO WScript.Sleep 10000
CSCRIPT //NoLogo "%Temp%.\sleep.vbs"
DEL "%Temp%.\sleep.vbs"

echo Test SCAgent service
"c:\Program Files\Telefin\SCAgentService\SCAgentService.exe" /t
> "%Temp%.\sleep.vbs" ECHO WScript.Sleep 3000
CSCRIPT //NoLogo "%Temp%.\sleep.vbs"
DEL "%Temp%.\sleep.vbs"

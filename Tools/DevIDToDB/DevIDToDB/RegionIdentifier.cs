﻿namespace DevIDToDB {
    /// <summary>
    /// Incapsula le logiche di codifica / decodifica di uno Region Id da Database a System.xml e viceversa 
    /// </summary>
    internal struct RegionIdentifier {
        /// <summary>
        /// Region Id originale da System.xml
        /// </summary>
        public ushort OriginalRegionId;

        /// <summary>
        /// Region Id da database
        /// </summary>
        public long RegionId;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="regionIdFromDatabase">Region Id da database</param>
        public RegionIdentifier(long regionIdFromDatabase) {
            this.OriginalRegionId = (ushort) ((((ulong) regionIdFromDatabase) & 0xFFFF));
            this.RegionId = regionIdFromDatabase;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="originalRegionId">Region Id originale da System.xml</param>
        public RegionIdentifier(ushort originalRegionId) {
            this.RegionId = (long) (originalRegionId | ((ulong) 0xFFFFFFFF0000));
            this.OriginalRegionId = originalRegionId;
        }
    }
}
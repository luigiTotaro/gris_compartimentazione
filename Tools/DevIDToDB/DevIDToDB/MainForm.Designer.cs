﻿namespace DevIDToDB {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblDevID = new System.Windows.Forms.Label();
            this.txtDevID = new System.Windows.Forms.TextBox();
            this.btnEncode = new System.Windows.Forms.Button();
            this.lblRegionIDXML = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDeviceIDXML = new System.Windows.Forms.TextBox();
            this.txtNodeIDXML = new System.Windows.Forms.TextBox();
            this.txtZoneIDXML = new System.Windows.Forms.TextBox();
            this.txtRegionIDXML = new System.Windows.Forms.TextBox();
            this.lblDeviceIDXML = new System.Windows.Forms.Label();
            this.lblNodeIDXML = new System.Windows.Forms.Label();
            this.lblZoneIDXML = new System.Windows.Forms.Label();
            this.lblRegID = new System.Windows.Forms.Label();
            this.txtRegID = new System.Windows.Forms.TextBox();
            this.lblZonID = new System.Windows.Forms.Label();
            this.txtZonID = new System.Windows.Forms.TextBox();
            this.lblNodID = new System.Windows.Forms.Label();
            this.txtNodID = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtDBDeviceID = new System.Windows.Forms.TextBox();
            this.txtDBRegionID = new System.Windows.Forms.TextBox();
            this.txtDBNodeID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDBZoneID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeviceIDSystemXML = new System.Windows.Forms.TextBox();
            this.txtNodeIDSystemXML = new System.Windows.Forms.TextBox();
            this.txtZoneIDSystemXML = new System.Windows.Forms.TextBox();
            this.txtRegionIDSystemXML = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDevID
            // 
            this.lblDevID.AutoSize = true;
            this.lblDevID.Location = new System.Drawing.Point(11, 22);
            this.lblDevID.Name = "lblDevID";
            this.lblDevID.Size = new System.Drawing.Size(76, 13);
            this.lblDevID.TabIndex = 0;
            this.lblDevID.Text = "DB Device ID:";
            // 
            // txtDevID
            // 
            this.txtDevID.Location = new System.Drawing.Point(93, 19);
            this.txtDevID.Name = "txtDevID";
            this.txtDevID.Size = new System.Drawing.Size(185, 20);
            this.txtDevID.TabIndex = 1;
            // 
            // btnEncode
            // 
            this.btnEncode.Location = new System.Drawing.Point(20, 123);
            this.btnEncode.Name = "btnEncode";
            this.btnEncode.Size = new System.Drawing.Size(264, 23);
            this.btnEncode.TabIndex = 8;
            this.btnEncode.Text = "Encode";
            this.btnEncode.UseVisualStyleBackColor = true;
            this.btnEncode.Click += new System.EventHandler(this.btnEncode_Click);
            // 
            // lblRegionIDXML
            // 
            this.lblRegionIDXML.AutoSize = true;
            this.lblRegionIDXML.Location = new System.Drawing.Point(12, 22);
            this.lblRegionIDXML.Name = "lblRegionIDXML";
            this.lblRegionIDXML.Size = new System.Drawing.Size(58, 13);
            this.lblRegionIDXML.TabIndex = 0;
            this.lblRegionIDXML.Text = "Region ID:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDeviceIDXML);
            this.groupBox1.Controls.Add(this.txtNodeIDXML);
            this.groupBox1.Controls.Add(this.txtZoneIDXML);
            this.groupBox1.Controls.Add(this.txtRegionIDXML);
            this.groupBox1.Controls.Add(this.lblDeviceIDXML);
            this.groupBox1.Controls.Add(this.lblNodeIDXML);
            this.groupBox1.Controls.Add(this.lblZoneIDXML);
            this.groupBox1.Controls.Add(this.lblRegionIDXML);
            this.groupBox1.Location = new System.Drawing.Point(11, 152);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 128);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sytem.xml";
            // 
            // txtDeviceIDXML
            // 
            this.txtDeviceIDXML.Location = new System.Drawing.Point(79, 97);
            this.txtDeviceIDXML.Name = "txtDeviceIDXML";
            this.txtDeviceIDXML.ReadOnly = true;
            this.txtDeviceIDXML.Size = new System.Drawing.Size(182, 20);
            this.txtDeviceIDXML.TabIndex = 7;
            // 
            // txtNodeIDXML
            // 
            this.txtNodeIDXML.Location = new System.Drawing.Point(79, 71);
            this.txtNodeIDXML.Name = "txtNodeIDXML";
            this.txtNodeIDXML.ReadOnly = true;
            this.txtNodeIDXML.Size = new System.Drawing.Size(182, 20);
            this.txtNodeIDXML.TabIndex = 5;
            // 
            // txtZoneIDXML
            // 
            this.txtZoneIDXML.Location = new System.Drawing.Point(79, 45);
            this.txtZoneIDXML.Name = "txtZoneIDXML";
            this.txtZoneIDXML.ReadOnly = true;
            this.txtZoneIDXML.Size = new System.Drawing.Size(182, 20);
            this.txtZoneIDXML.TabIndex = 3;
            // 
            // txtRegionIDXML
            // 
            this.txtRegionIDXML.Location = new System.Drawing.Point(79, 19);
            this.txtRegionIDXML.Name = "txtRegionIDXML";
            this.txtRegionIDXML.ReadOnly = true;
            this.txtRegionIDXML.Size = new System.Drawing.Size(182, 20);
            this.txtRegionIDXML.TabIndex = 1;
            // 
            // lblDeviceIDXML
            // 
            this.lblDeviceIDXML.AutoSize = true;
            this.lblDeviceIDXML.Location = new System.Drawing.Point(12, 100);
            this.lblDeviceIDXML.Name = "lblDeviceIDXML";
            this.lblDeviceIDXML.Size = new System.Drawing.Size(58, 13);
            this.lblDeviceIDXML.TabIndex = 6;
            this.lblDeviceIDXML.Text = "Device ID:";
            // 
            // lblNodeIDXML
            // 
            this.lblNodeIDXML.AutoSize = true;
            this.lblNodeIDXML.Location = new System.Drawing.Point(12, 74);
            this.lblNodeIDXML.Name = "lblNodeIDXML";
            this.lblNodeIDXML.Size = new System.Drawing.Size(50, 13);
            this.lblNodeIDXML.TabIndex = 4;
            this.lblNodeIDXML.Text = "Node ID:";
            // 
            // lblZoneIDXML
            // 
            this.lblZoneIDXML.AutoSize = true;
            this.lblZoneIDXML.Location = new System.Drawing.Point(12, 48);
            this.lblZoneIDXML.Name = "lblZoneIDXML";
            this.lblZoneIDXML.Size = new System.Drawing.Size(49, 13);
            this.lblZoneIDXML.TabIndex = 2;
            this.lblZoneIDXML.Text = "Zone ID:";
            // 
            // lblRegID
            // 
            this.lblRegID.AutoSize = true;
            this.lblRegID.Location = new System.Drawing.Point(11, 48);
            this.lblRegID.Name = "lblRegID";
            this.lblRegID.Size = new System.Drawing.Size(76, 13);
            this.lblRegID.TabIndex = 2;
            this.lblRegID.Text = "DB Region ID:";
            // 
            // txtRegID
            // 
            this.txtRegID.Location = new System.Drawing.Point(93, 45);
            this.txtRegID.Name = "txtRegID";
            this.txtRegID.Size = new System.Drawing.Size(185, 20);
            this.txtRegID.TabIndex = 3;
            // 
            // lblZonID
            // 
            this.lblZonID.AutoSize = true;
            this.lblZonID.Location = new System.Drawing.Point(11, 74);
            this.lblZonID.Name = "lblZonID";
            this.lblZonID.Size = new System.Drawing.Size(67, 13);
            this.lblZonID.TabIndex = 4;
            this.lblZonID.Text = "DB Zone ID:";
            // 
            // txtZonID
            // 
            this.txtZonID.Location = new System.Drawing.Point(93, 71);
            this.txtZonID.Name = "txtZonID";
            this.txtZonID.Size = new System.Drawing.Size(185, 20);
            this.txtZonID.TabIndex = 5;
            // 
            // lblNodID
            // 
            this.lblNodID.AutoSize = true;
            this.lblNodID.Location = new System.Drawing.Point(11, 100);
            this.lblNodID.Name = "lblNodID";
            this.lblNodID.Size = new System.Drawing.Size(68, 13);
            this.lblNodID.TabIndex = 6;
            this.lblNodID.Text = "DB Node ID:";
            // 
            // txtNodID
            // 
            this.txtNodID.Location = new System.Drawing.Point(93, 97);
            this.txtNodID.Name = "txtNodID";
            this.txtNodID.Size = new System.Drawing.Size(185, 20);
            this.txtNodID.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDevID);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.lblDevID);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.lblRegID);
            this.groupBox2.Controls.Add(this.txtNodID);
            this.groupBox2.Controls.Add(this.lblZonID);
            this.groupBox2.Controls.Add(this.txtZonID);
            this.groupBox2.Controls.Add(this.lblNodID);
            this.groupBox2.Controls.Add(this.txtRegID);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(293, 291);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Da ID del DataBase a ID di System.xml";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 123);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(264, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Decode";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnDecode_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtDeviceIDSystemXML);
            this.groupBox3.Controls.Add(this.txtNodeIDSystemXML);
            this.groupBox3.Controls.Add(this.txtZoneIDSystemXML);
            this.groupBox3.Controls.Add(this.btnEncode);
            this.groupBox3.Controls.Add(this.txtRegionIDSystemXML);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(311, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(299, 291);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Da ID di System.xml a ID DataBase";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtDBDeviceID);
            this.groupBox4.Controls.Add(this.txtDBRegionID);
            this.groupBox4.Controls.Add(this.txtDBNodeID);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtDBZoneID);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(7, 153);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(277, 127);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "DataBase";
            // 
            // txtDBDeviceID
            // 
            this.txtDBDeviceID.Location = new System.Drawing.Point(77, 96);
            this.txtDBDeviceID.Name = "txtDBDeviceID";
            this.txtDBDeviceID.ReadOnly = true;
            this.txtDBDeviceID.Size = new System.Drawing.Size(182, 20);
            this.txtDBDeviceID.TabIndex = 7;
            // 
            // txtDBRegionID
            // 
            this.txtDBRegionID.Location = new System.Drawing.Point(77, 18);
            this.txtDBRegionID.Name = "txtDBRegionID";
            this.txtDBRegionID.ReadOnly = true;
            this.txtDBRegionID.Size = new System.Drawing.Size(182, 20);
            this.txtDBRegionID.TabIndex = 1;
            // 
            // txtDBNodeID
            // 
            this.txtDBNodeID.Location = new System.Drawing.Point(77, 70);
            this.txtDBNodeID.Name = "txtDBNodeID";
            this.txtDBNodeID.ReadOnly = true;
            this.txtDBNodeID.Size = new System.Drawing.Size(182, 20);
            this.txtDBNodeID.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Region ID:";
            // 
            // txtDBZoneID
            // 
            this.txtDBZoneID.Location = new System.Drawing.Point(77, 44);
            this.txtDBZoneID.Name = "txtDBZoneID";
            this.txtDBZoneID.ReadOnly = true;
            this.txtDBZoneID.Size = new System.Drawing.Size(182, 20);
            this.txtDBZoneID.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Zone ID:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Node ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Device ID:";
            // 
            // txtDeviceIDSystemXML
            // 
            this.txtDeviceIDSystemXML.Location = new System.Drawing.Point(95, 97);
            this.txtDeviceIDSystemXML.Name = "txtDeviceIDSystemXML";
            this.txtDeviceIDSystemXML.Size = new System.Drawing.Size(189, 20);
            this.txtDeviceIDSystemXML.TabIndex = 7;
            // 
            // txtNodeIDSystemXML
            // 
            this.txtNodeIDSystemXML.Location = new System.Drawing.Point(95, 71);
            this.txtNodeIDSystemXML.Name = "txtNodeIDSystemXML";
            this.txtNodeIDSystemXML.Size = new System.Drawing.Size(189, 20);
            this.txtNodeIDSystemXML.TabIndex = 5;
            // 
            // txtZoneIDSystemXML
            // 
            this.txtZoneIDSystemXML.Location = new System.Drawing.Point(95, 45);
            this.txtZoneIDSystemXML.Name = "txtZoneIDSystemXML";
            this.txtZoneIDSystemXML.Size = new System.Drawing.Size(189, 20);
            this.txtZoneIDSystemXML.TabIndex = 3;
            // 
            // txtRegionIDSystemXML
            // 
            this.txtRegionIDSystemXML.Location = new System.Drawing.Point(95, 19);
            this.txtRegionIDSystemXML.Name = "txtRegionIDSystemXML";
            this.txtRegionIDSystemXML.Size = new System.Drawing.Size(189, 20);
            this.txtRegionIDSystemXML.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Region ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Zone ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Node ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Device ID:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 312);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "System.xml <-> DataBase";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDevID;
        private System.Windows.Forms.TextBox txtDevID;
        private System.Windows.Forms.Button btnEncode;
        private System.Windows.Forms.Label lblRegionIDXML;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRegID;
        private System.Windows.Forms.TextBox txtRegID;
        private System.Windows.Forms.Label lblZonID;
        private System.Windows.Forms.TextBox txtZonID;
        private System.Windows.Forms.Label lblNodID;
        private System.Windows.Forms.TextBox txtNodID;
        private System.Windows.Forms.TextBox txtZoneIDXML;
        private System.Windows.Forms.TextBox txtRegionIDXML;
        private System.Windows.Forms.Label lblZoneIDXML;
        private System.Windows.Forms.TextBox txtNodeIDXML;
        private System.Windows.Forms.Label lblNodeIDXML;
        private System.Windows.Forms.TextBox txtDeviceIDXML;
        private System.Windows.Forms.Label lblDeviceIDXML;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtDeviceIDSystemXML;
        private System.Windows.Forms.TextBox txtNodeIDSystemXML;
        private System.Windows.Forms.TextBox txtZoneIDSystemXML;
        private System.Windows.Forms.TextBox txtRegionIDSystemXML;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtDBDeviceID;
        private System.Windows.Forms.TextBox txtDBRegionID;
        private System.Windows.Forms.TextBox txtDBNodeID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDBZoneID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}


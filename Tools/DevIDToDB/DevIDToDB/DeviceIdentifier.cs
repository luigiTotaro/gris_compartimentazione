﻿namespace DevIDToDB {
    /// <summary>
    /// Incapsula le logiche di codifica / decodifica di un Device Id da Database a System.xml e viceversa 
    /// </summary>
    internal struct DeviceIdentifier {
        /// <summary>
        /// Device Id originale da System.xml
        /// </summary>
        public ushort OriginalDeviceId;

        /// <summary>
        /// Node Id originale da System.xml
        /// </summary>
        public ushort OriginalNodeId;

        /// <summary>
        /// Zone Id originale da System.xml
        /// </summary>
        public ushort OriginalZoneId;

        /// <summary>
        /// Region Id originale da System.xml
        /// </summary>
        public ushort OriginalRegionId;

        /// <summary>
        /// Device Id da database
        /// </summary>
        public long DeviceId;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="deviceIdFromDatabase">Device Id da database</param>
        public DeviceIdentifier(long deviceIdFromDatabase) {
            this.OriginalDeviceId = (ushort) ((((ulong) deviceIdFromDatabase) & 0xFFFF000000000000) >> 48);
            this.OriginalNodeId = (ushort) ((((ulong) deviceIdFromDatabase) & 0xFFFF00000000) >> 32);
            this.OriginalZoneId = (ushort) ((((ulong) deviceIdFromDatabase) & 0xFFFF0000) >> 16);
            this.OriginalRegionId = (ushort) ((((ulong) deviceIdFromDatabase) & 0xFFFF));
            this.DeviceId = deviceIdFromDatabase;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="originalDeviceId">Device Id originale da System.xml</param>
        /// <param name="originalNodeId">Node Id originale da System.xml</param>
        /// <param name="originalZoneId">Zone Id originale da System.xml</param>
        /// <param name="originalRegionId">Region Id originale da System.xml</param>
        public DeviceIdentifier(ushort originalDeviceId, ushort originalNodeId, ushort originalZoneId, ushort originalRegionId) {
            this.DeviceId =
                (long) (originalRegionId | ((ulong) originalZoneId << 16) | ((ulong) originalNodeId << 32) | ((ulong) originalDeviceId << 48));
            this.OriginalDeviceId = originalDeviceId;
            this.OriginalNodeId = originalNodeId;
            this.OriginalZoneId = originalZoneId;
            this.OriginalRegionId = originalRegionId;
        }
    }
}
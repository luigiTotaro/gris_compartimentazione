﻿using System;
using System.Windows.Forms;

namespace DevIDToDB {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void btnDecode_Click(object sender, EventArgs e) {
            string valueString;

            if (this.txtNodID.Text.Trim().Length > 0) {
                valueString = this.txtNodID.Text.Trim();

                long value;

                if (long.TryParse(valueString, out value)) {
                    NodeIdentifier nodeIdentifier = new NodeIdentifier(value);

                    this.txtDeviceIDXML.Text = string.Empty;
                    this.txtNodeIDXML.Text = nodeIdentifier.OriginalNodeId.ToString();
                    this.txtZoneIDXML.Text = nodeIdentifier.OriginalZoneId.ToString();
                    this.txtRegionIDXML.Text = nodeIdentifier.OriginalRegionId.ToString();
                }
                else {
                    MessageBox.Show(valueString + " non è un valore intero a 64 bit valido");
                    return;
                }
            }

            if (this.txtZonID.Text.Trim().Length > 0) {
                valueString = this.txtZonID.Text.Trim();

                long value;

                if (long.TryParse(valueString, out value)) {
                    ZoneIdentifier zoneIdentifier = new ZoneIdentifier(value);

                    this.txtDeviceIDXML.Text = string.Empty;
                    this.txtNodeIDXML.Text = string.Empty;
                    this.txtZoneIDXML.Text = zoneIdentifier.OriginalZoneId.ToString();
                    this.txtRegionIDXML.Text = zoneIdentifier.OriginalRegionId.ToString();
                }
                else {
                    MessageBox.Show(valueString + " non è un valore intero a 64 bit valido");
                    return;
                }
            }

            if (this.txtRegID.Text.Trim().Length > 0) {
                valueString = this.txtRegID.Text.Trim();

                long value;

                if (long.TryParse(valueString, out value)) {
                    RegionIdentifier regionIdentifier = new RegionIdentifier(value);

                    this.txtDeviceIDXML.Text = string.Empty;
                    this.txtNodeIDXML.Text = string.Empty;
                    this.txtZoneIDXML.Text = string.Empty;
                    this.txtRegionIDXML.Text = regionIdentifier.OriginalRegionId.ToString();
                }
                else {
                    MessageBox.Show(valueString + " non è un valore intero a 64 bit valido");
                    return;
                }
            }

            if (this.txtDevID.Text.Trim().Length > 0) {
                valueString = this.txtDevID.Text.Trim();

                long value;

                if (long.TryParse(valueString, out value)) {
                    DeviceIdentifier deviceIdentifier = new DeviceIdentifier(value);

                    this.txtDeviceIDXML.Text = deviceIdentifier.OriginalDeviceId.ToString();
                    this.txtNodeIDXML.Text = deviceIdentifier.OriginalNodeId.ToString();
                    this.txtZoneIDXML.Text = deviceIdentifier.OriginalZoneId.ToString();
                    this.txtRegionIDXML.Text = deviceIdentifier.OriginalRegionId.ToString();
                }
                else {
                    MessageBox.Show(valueString + " non è un valore intero a 64 bit valido");
                    return;
                }
            }
        }

        private void btnEncode_Click(object sender, EventArgs e) {
            this.txtDBRegionID.Text = string.Empty;
            this.txtDBZoneID.Text = string.Empty;
            this.txtDBNodeID.Text = string.Empty;
            this.txtDBDeviceID.Text = string.Empty;

            if (this.txtRegionIDSystemXML.Text.Trim().Length > 0) {
                ushort regionId;
                if (ushort.TryParse(this.txtRegionIDSystemXML.Text, out regionId)) {
                    this.txtDBRegionID.Text = (new RegionIdentifier(regionId)).RegionId.ToString();
                }
                else {
                    MessageBox.Show(this.txtRegionIDSystemXML.Text + " non è un valore intero valido");
                    return;
                }

                if (this.txtZoneIDSystemXML.Text.Trim().Length > 0) {
                    ushort zoneId;
                    if (ushort.TryParse(this.txtZoneIDSystemXML.Text, out zoneId)) {
                        this.txtDBZoneID.Text = (new ZoneIdentifier(zoneId, regionId)).ZoneId.ToString();
                    }
                    else {
                        MessageBox.Show(this.txtZoneIDSystemXML.Text + " non è un valore intero valido");
                        return;
                    }

                    if (this.txtNodeIDSystemXML.Text.Trim().Length > 0) {
                        ushort nodeId;
                        if (ushort.TryParse(this.txtNodeIDSystemXML.Text, out nodeId)) {
                            this.txtDBNodeID.Text = (new NodeIdentifier(nodeId, zoneId, regionId)).NodeId.ToString();
                        }
                        else {
                            MessageBox.Show(this.txtNodeIDSystemXML.Text + " non è un valore intero valido");
                            return;
                        }

                        if (this.txtDeviceIDSystemXML.Text.Trim().Length > 0) {
                            ushort deviceId;
                            if (ushort.TryParse(this.txtDeviceIDSystemXML.Text, out deviceId)) {
                                this.txtDBDeviceID.Text = (new DeviceIdentifier(deviceId, nodeId, zoneId, regionId)).DeviceId.ToString();
                            }
                            else {
                                MessageBox.Show(this.txtDeviceIDSystemXML.Text + " non è un valore intero valido");
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
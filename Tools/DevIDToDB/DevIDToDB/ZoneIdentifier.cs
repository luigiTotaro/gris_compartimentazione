﻿namespace DevIDToDB {
    /// <summary>
    /// Incapsula le logiche di codifica / decodifica di uno Zone Id da Database a System.xml e viceversa 
    /// </summary>
    internal struct ZoneIdentifier {
        /// <summary>
        /// Zone Id originale da System.xml
        /// </summary>
        public ushort OriginalZoneId;

        /// <summary>
        /// Region Id originale da System.xml
        /// </summary>
        public ushort OriginalRegionId;

        /// <summary>
        /// Zone Id da database
        /// </summary>
        public long ZoneId;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="zoneIdFromDatabase">Zone Id da database</param>
        public ZoneIdentifier(long zoneIdFromDatabase) {
            this.OriginalZoneId = (ushort) ((((ulong) zoneIdFromDatabase) & 0xFFFF0000) >> 16);
            this.OriginalRegionId = (ushort) ((((ulong) zoneIdFromDatabase) & 0xFFFF));
            this.ZoneId = zoneIdFromDatabase;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="originalZoneId">Zone Id originale da System.xml</param>
        /// <param name="originalRegionId">Region Id originale da System.xml</param>
        public ZoneIdentifier(ushort originalZoneId, ushort originalRegionId) {
            this.ZoneId = (long) (originalRegionId | ((ulong) originalZoneId << 16));
            this.OriginalZoneId = originalZoneId;
            this.OriginalRegionId = originalRegionId;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Xml;
using System.Xml.Serialization;
using SyxmlUtil.Properties;

namespace SyxmlUtil
{
    public interface IExternalDataSource
    {

        string Name { get; }
        string FileExtension { get; }
        string ParametersLabel { get; }
        string[] ParametersValues { get; }

        void FillDataTable(string file, string parameter, bool withHeaderRow, ref DataTable table);
    }

    public class ExcelDataSource : IExternalDataSource
    {
        readonly string connectionTemplate = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR={1}\"";

        ExternalDataSource eds;

        public ExcelDataSource()
        {
            eds = Settings.Default.ExternalDataSources.GetDataSourceByFileExtension(".xls");
        }

        #region IExternalDataSource Members

        public string Name
        {
            get { return eds.Name; }
        }

        public string FileExtension
        {
            get { return eds.FileExtension; }
        }

        public string ParametersLabel
        {
            get { return eds.ParametersLabel; }
        }

        public string[] ParametersValues
        {
            get { return eds.ParametersValues.Split('|'); }
        }

        public void FillDataTable(string file, string parameter, bool withHeaderRow, ref DataTable table)
        {
            table.Clear();
            table.Columns.Clear();

            if (!File.Exists(file))
            {
                throw new Exception(string.Format("File excel: '{0}' not found.", file));
            }

            try
            {
                string conn = string.Format(connectionTemplate, file, (withHeaderRow ? "YES" : "NO") );
                OleDbDataAdapter oleAdapter = new OleDbDataAdapter();
                oleAdapter.SelectCommand = new OleDbCommand(string.Format(@"SELECT * FROM [{0}$]", parameter), new OleDbConnection(conn));
                oleAdapter.FillSchema(table, SchemaType.Source);
                oleAdapter.Fill(table);
            }
            catch (Exception ex)
            {
                //throw new Exception("Error reading excel file {0}", ex);
                throw new Exception(ex.Message);
            }

        }

        #endregion
    }

    public class CsvDataSource : IExternalDataSource
    {

        ExternalDataSource eds;

        public CsvDataSource()
        {
            eds = Settings.Default.ExternalDataSources.GetDataSourceByFileExtension(".csv");
        }

        #region IExternalDataSource Members

        public string Name
        {
            get { return eds.Name; }
        }

        public string FileExtension
        {
            get { return eds.FileExtension; }
        }

        public string ParametersLabel
        {
            get { return eds.ParametersLabel; }
        }

        public string[] ParametersValues
        {
            get { return eds.ParametersValues.Split('|'); }
        }

        public void FillDataTable(string file, string parameter, bool withHeaderRow, ref DataTable table)
        {
            if (!File.Exists(file))
            {
                throw new Exception(string.Format("File CSV: '{0}' not found.", file));
            }

            try
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    String line;
                    string[] separators;

                    table.Clear();
                    table.Columns.Clear();

                    if (parameter == "{tab}")
                        separators = new string[] { "\t" };
                    else
                        separators = new string[] { parameter };


                    line = sr.ReadLine();

                    if (line != null)
                    {
                        DataColumn[] cols = new DataColumn[line.Split(separators, StringSplitOptions.None).Length];
                        if (withHeaderRow)
                        {
                            string[] aLine = line.Split(separators, StringSplitOptions.None);
                            for (int i = 0; i < cols.Length; i++)
                                cols[i] = new DataColumn(aLine[i], typeof(string));
                            table.Columns.AddRange(cols);

                            line = sr.ReadLine();
                            if (line == null)
                                return;
                        }
                        else
                        {
                            for (int i = 0; i < cols.Length; i++)
                                cols[i] = new DataColumn("Column" + i.ToString(), typeof(string));
                            table.Columns.AddRange(cols);
                        }

                        
                        do
                        {
                            string[] aLine = line.Split(separators, table.Columns.Count, StringSplitOptions.None);
                            for (int i = 0; i < cols.Length; i++)
                                if (aLine[i].Length > 1 && aLine[i].StartsWith("\"") && aLine[i].EndsWith("\""))
                                {
                                    aLine[i] = aLine[i].Substring(1, aLine[i].Length - 2);
                                    aLine[i] = aLine[i].Replace("\"\"", "\"");
                                }

                            table.Rows.Add(aLine);
                            

                        } while ((line = sr.ReadLine()) != null);
                    }
                    else
                    {
                        throw new Exception(string.Format("File CSV: '{0}' empty.", file));
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Error reading CSV file", ex);
                throw new Exception(ex.Message);
            }
        }


        #endregion
    }

    public class ExternalDataSources
    {
        [XmlElement(ElementName = "dataSource")]
        public List<ExternalDataSource> DataSources { get; set; }


        public ExternalDataSource GetDataSourceByFileExtension(string fileExtension)
        {
            if (this.DataSources != null && this.DataSources.Count > 0)
            {
                ExternalDataSource eds = this.DataSources.Find(x => string.Compare(x.FileExtension,fileExtension,true) == 0);
                if (eds == null)
                    eds = this.DataSources[0];
                return eds;
            }
            else
            {
                return null;
            }
        }

    }

    public class ExternalDataSource
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "fileExtension")]
        public string FileExtension { get; set; }
        [XmlAttribute(AttributeName = "parametersLabel")]
        public string ParametersLabel { get; set; }
        [XmlAttribute(AttributeName = "parametersValues")]
        public string ParametersValues { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string TypeName { get; set; }
    }
    
}

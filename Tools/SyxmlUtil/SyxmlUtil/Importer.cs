﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Data;
using System.Data.OleDb;
using SyxmlUtil;
using SyxmlUtil.Properties;

namespace SyxmlUtil
{
    public class Importer
    {
        public string SystemXmlFile { get; set; }
        public string ExternalFile { get; set; }
        public string ExternalParameter { get; set; }
        public bool WithHeaderRow { get; set; }
        public string CurrentMap { get; set; }
        public string LastMessage { get; set; }

        public TextWriter Output { get; set; }

        public Importer() : this(null) 
        { }

        public Importer(TextWriter output)
        {
            if (output == null)
                this.Output = output;
            else
                this.Output = new StringWriter();

            this.SystemXmlFile = "System.Xml";
            this.ExternalFile = string.Empty;
            this.ExternalParameter = string.Empty;
            this.WithHeaderRow = false;
            if (Settings.Default.SystemMaps != null && Settings.Default.SystemMaps.Maps.Count > 0)
                this.CurrentMap = Settings.Default.SystemMaps.Maps[0].Name;
            else
                this.CurrentMap = string.Empty;

            this.LastMessage = string.Empty;
        }

        public bool Import()
        {
            return Import(this.SystemXmlFile, this.ExternalFile, this.ExternalParameter, this.WithHeaderRow, this.CurrentMap);
        }

        public bool Import(string fileSystemXml, string externalFile, string externalParameter, bool withHeaderRow, string mapName)
        {
            DataTable devices = new DataTable();
            XmlDocument telefin = new XmlDocument();

            this.LastMessage = string.Empty;

            if (!string.IsNullOrEmpty(fileSystemXml))
                this.SystemXmlFile = fileSystemXml;
            if (!string.IsNullOrEmpty(externalFile))
                this.ExternalFile = externalFile;
            if (!string.IsNullOrEmpty(externalParameter))
                this.ExternalParameter = externalParameter;
            if (!string.IsNullOrEmpty(mapName))
                this.CurrentMap = mapName;
            
            this.WithHeaderRow = withHeaderRow;

            if (CheckSystemXml(this.SystemXmlFile, ref telefin) && FillExteralDataTable(this.ExternalFile, this.ExternalParameter,this.WithHeaderRow, ref devices))
            {
                if (!ImportDevices(devices, telefin, this.CurrentMap))
                    return false;

                if (!SaveSystemXml(telefin, fileSystemXml))
                    return false;

                return true;
            }
            return false;
        }
 
        public DataTable GetExternalData(string externalFile, string externalParameter, bool withHeaderRow)
        {
            DataTable dt = new DataTable();
            FillExteralDataTable(externalFile, externalParameter,withHeaderRow, ref dt);
            return dt;
        }      

        public string FileTypes
        {
            get 
            {
                string fileTypes = string.Empty;
                foreach (ExternalDataSource eds in Settings.Default.ExternalDataSources.DataSources)
                {
                    fileTypes += "|" + eds.Name + "|*" + eds.FileExtension ;  
                }
                return fileTypes.StartsWith("|") ? fileTypes.Substring(1) : fileTypes;
            }
        }

        public IExternalDataSource GetDataSource(string fileExtension)
        {
            return CreateDataSourceFromFileExtension(fileExtension);
        }

        public string[] Maps
        {
            get 
            {
                if (Settings.Default.SystemMaps != null
                     && Settings.Default.SystemMaps.Maps != null
                        && Settings.Default.SystemMaps.Maps.Count > 0)
                {
                    string[] sm = new string[Settings.Default.SystemMaps.Maps.Count];
                    for (int i = 0; i < Settings.Default.SystemMaps.Maps.Count; i++)
                    {
                        sm[i] = Settings.Default.SystemMaps.Maps[i].Name;
                    }
                    return sm;
                }
                else
                {
                    return new string[] { };
                }
            }
        }

        bool SaveSystemXml(XmlDocument telefin, string filename)
        {
            try
            {
                TextWriter tw = new StreamWriter(filename, false, System.Text.Encoding.ASCII);
                telefin.Save(tw);
                tw.Close();
                return true;
            }
            catch (Exception ex)
            {
                WriteTrace("Errore in fase di salvataggio del file System.xml:{0}", ex.Message);
                return false;
            }
        }

        bool CheckSystemXml(string file, ref XmlDocument telefin)
        {
            if (!File.Exists(file))
            {
                WriteTrace("File System.xml: '{0}' non trovato.", file);
                return false;
            }

            try
            {
                telefin.Load(file);

                XmlNode topography = telefin.SelectSingleNode("//topography");
                if (topography == null) throw new Exception("Node 'topography' not found");
                XmlNode station = topography.SelectSingleNode("//station");
                if (station == null) throw new Exception("Node 'station' not found");
                XmlNode building = station.SelectSingleNode("//building");
                if (building == null) throw new Exception("Node 'building' not found");
                XmlNode system = telefin.SelectSingleNode("//system");
                if (system == null) throw new Exception("Node 'system' not found");
                XmlNode server = system.SelectSingleNode("//server");
                if (server == null) throw new Exception("Node 'server' not found");
                XmlNode region = server.SelectSingleNode("//region");
                if (region == null) throw new Exception("Node 'region' not found");
                XmlNode zone = region.SelectSingleNode("//zone");
                if (zone == null) throw new Exception("Node 'zone' not found");
                XmlNode node = zone.SelectSingleNode("//node");
                if (node == null) throw new Exception("Node 'node' not found");
                XmlNode stationId = station.SelectSingleNode("@id");
                if (stationId == null) throw new Exception("Attribute 'station.id' not found");
                XmlNode buildingId = building.SelectSingleNode("@id");
                if (buildingId == null) throw new Exception("Attribute 'buildin.id' not found");

            }
            catch (Exception ex)
            {
                WriteTrace("Error reading System.xml: '{0}'", ex.Message);
                return false;
            }

            return true;
        }

        bool FillExteralDataTable(string file, string parameters,bool withHeaderRow, ref DataTable table)
        {
            try
            {
                string fileExtension = Path.GetExtension(file);

                IExternalDataSource es = this.CreateDataSourceFromFileExtension(fileExtension);
                es.FillDataTable(file, parameters,withHeaderRow, ref table);
                return true;
            }
            catch (Exception ex)
            {
                WriteTrace("Error on read external data:{0}", ex.Message);
                return false;
            }
        }

        IExternalDataSource CreateDataSourceFromFileExtension(string fileExtension)
        {
            IExternalDataSource edsObject = null;
            ExternalDataSource eds = Settings.Default.ExternalDataSources.GetDataSourceByFileExtension(fileExtension);
            if (eds == null)
            {
                WriteTrace("ExternalDataSources not defined");
                return null;
            }
            else if (Type.GetType(eds.TypeName) == null)
            {
                WriteTrace("Type of ExternalDataSources not defined");
                return null;
            }
            else
            {

                edsObject = (IExternalDataSource)Activator.CreateInstance(Type.GetType(eds.TypeName));
            }
            return edsObject;
        }


        bool ImportDevices(DataTable devices, XmlDocument telefin, string mapName)
        {
            try
            {
                SystemMaps maps = Settings.Default.SystemMaps;
                SystemMap map = null;
                XmlElement device = null;
                XmlElement location = null;
                int countOk = 0;
                int countError = 0;
                int countSkipped = 0;


                // trova la mappa
                if (mapName != string.Empty)
                {
                    map = maps.Maps.Find(x => (string.Compare(x.Name, mapName, true) == 0));
                }

                if (map == null)
                {
                    WriteTrace("Map '{0}' Not Found!", mapName);
                    map = maps.Maps[0];
                }

                XmlNode station = telefin.SelectSingleNode("//topography/station");
                XmlNode building = station.SelectSingleNode("//building");
                XmlNode node = telefin.SelectSingleNode("//system/server/region/zone/node");

                Dictionary<string, string> locationContext = new Dictionary<string, string>();
                locationContext.Add("{identity}", GetMaxLocationId(building).ToString());

                Dictionary<string, string> deviceContext = new Dictionary<string, string>();
                deviceContext.Add("{identity}", GetMaxDeviceId(node).ToString());
                deviceContext.Add("{station}", station.Attributes["id"].Value);
                deviceContext.Add("{building}", building.Attributes["id"].Value);
                deviceContext.Add("{location}", string.Empty);

                for (int i = 0; i < devices.Rows.Count; i++)
                {
                    try
                    {
                        DataRow row = devices.Rows[i];

                        location = CreateLocation(telefin);
                        ImportRow(row, location, map.LocationMapList, locationContext);

                        deviceContext["{location}"] = locationContext["{identity}"];
                        device = CreateDevice(telefin);
                        ImportRow(row, device, map.DeviceMapList, deviceContext);

                        if (location.Attributes["name"].Value.Trim().Length == 0 || location.Attributes["type"].Value.Trim().Length == 0)
                        {
                            WriteTrace("Skipped row {0} reason: '{1}'", i, "location without name or type");
                            countSkipped++;
                            continue;
                        }

                        if (device.Attributes["name"].Value.Trim().Length == 0
                            || device.Attributes["addr"].Value.Trim().Length == 0
                            || device.Attributes["type"].Value.Trim().Length == 0)
                        {
                            WriteTrace("Skipped row {0} reason: '{1}'", i, "device without name, addr or type");
                            countSkipped++;
                            continue;
                        }

                        if (ExistLocation(building,location.Attributes["name"].Value))
                        {
                            WriteTrace("Skipped row {0} reason: location '{1}' already exist", i, location.Attributes["name"].Value);
                            countSkipped++;
                            continue;
                        }

                        if (ExistDevice(node, device.Attributes["name"].Value))
                        {
                            WriteTrace("Skipped row {0} reason: device '{1}' already exist", i, device.Attributes["name"].Value);
                            countSkipped++;
                            continue;
                        }

                        building.AppendChild(location);
                        node.AppendChild(device);
                        countOk++;
                    }
                    catch (Exception ex)
                    {
                        WriteTrace("Error on import row {0} errors: '{1}'", i, ex.Message);
                        countError++;
                    }

                }
                WriteTrace("Import completed. \n \n Imported rows: {0} \n Skipped rows:{1} \n Errors rows:{2}", countOk, countSkipped, countError);
                return true;
                
            }
            catch (Exception ex)
            {
                WriteTrace("Error on import: '{0}'", ex.Message);
                return false;
            }
        }

        void ImportRow(DataRow row, XmlElement element, List<ColumnAttribute> mapList, Dictionary<String, String> context)
        {
            foreach (ColumnAttribute ca in mapList)
            {
                if (!string.IsNullOrEmpty(ca.Value))
                {
                    if (element.Attributes[ca.AttributeName] == null)
                        throw new Exception(string.Format("Attribute not found {0}", ca.AttributeName));

                    if (ca.Value == "{identity}")
                    {
                        int identity = int.Parse(context["{identity}"]);
                        element.Attributes[ca.AttributeName].Value = (++identity).ToString();
                        context["{identity}"] = identity.ToString();
                    }
                    else if (ca.Value.StartsWith("{") && ca.Value.EndsWith("}"))
                    {
                        element.Attributes[ca.AttributeName].Value = context[ca.Value];
                    }
                    else
                    {
                        element.Attributes[ca.AttributeName].Value = ca.Value.Trim();
                    }
                }
                else if (!string.IsNullOrEmpty(ca.AttributeBuilderString))
                {
                    string s = ca.AttributeBuilderString;
                    for (int i = 0; i < row.Table.Columns.Count; i++)
                    {
                        string v = string.Empty;
                        if (row[i] != null && row[i] != DBNull.Value)
                            v = row[i].ToString();

                        s = s.Replace("{" + i.ToString() + "}", v); 
                    }
                    element.Attributes[ca.AttributeName].Value = s;
                }
                else
                {
                    string colValue = row[ca.ColumnIndex].ToString().Trim();


                    if (ca.ValueMapList.Count > 0 && colValue.Length > 0)
                    {
                        ValueMap vm = ca.ValueMapList.Find(p => string.Compare(p.Source.Trim(), colValue.Trim(), true) == 0);

                        if (vm != null)
                            colValue = vm.Destination;
                        else
                            throw new Exception(string.Format("Decod not found for attribute '{0}' value '{1}'", ca.AttributeName, colValue));
                    }

                    element.Attributes[ca.AttributeName].Value = colValue;
                }
            }

        }

        bool ExistLocation(XmlNode building, string keyValue)
        {

            XmlNodeList nl = building.SelectNodes(string.Format("//location[@name=\"{0}\"]", keyValue));

            return (nl != null && nl.Count > 0);
        }

        bool ExistDevice(XmlNode node, string keyValue)
        {
            XmlNodeList nl = node.SelectNodes(string.Format("//device[@name=\"{0}\"]", keyValue));

            return (nl != null && nl.Count > 0);
        }

        private int GetMaxLocationId(XmlNode building)
        {
            try
            {
                int max = -1;

                XmlNodeList nl = building.SelectNodes("location");

                foreach (XmlNode n in nl)
                {
                    int id = 0;
                    if (int.TryParse(n.Attributes["id"].Value, out id))
                        max = (id > max ? id : max);
                }

                return max;
            }
            catch (Exception ex)
            {
                throw new Exception("Error on GetMaxLocationId", ex);
            }
        }

        private int GetMaxDeviceId(XmlNode node)
        {
            try
            {
                int max = -1;

                XmlNodeList nl = node.SelectNodes("device");

                foreach (XmlNode n in nl)
                {
                    int id = 0;
                    if (int.TryParse(n.Attributes["DevID"].Value, out id))
                        max = (id > max ? id : max);
                }

                return max;
            }
            catch (Exception ex)
            {
                throw new Exception("Error on GetMaxDeviceId", ex);
            }
        }

        private XmlElement CreateDevice(XmlDocument telefin)
        {
            //<device DevID="0" name="FAS01AT1700L" station="0" building="0" location="0" position="1,1" addr="10.102.222.1" 
            // SN="00000000" port="2" type="AEL0000" profile="1" active="true" scheduled="false" snmp_community="public" /> 

            XmlElement e = telefin.CreateElement("device");

            e.Attributes.Append(telefin.CreateAttribute("DevID"));
            e.Attributes.Append(telefin.CreateAttribute("name"));
            e.Attributes.Append(telefin.CreateAttribute("station"));
            e.Attributes.Append(telefin.CreateAttribute("building"));
            e.Attributes.Append(telefin.CreateAttribute("location"));
            e.Attributes.Append(telefin.CreateAttribute("position"));
            e.Attributes.Append(telefin.CreateAttribute("SN"));
            e.Attributes.Append(telefin.CreateAttribute("addr"));
            e.Attributes.Append(telefin.CreateAttribute("port"));
            e.Attributes.Append(telefin.CreateAttribute("type"));
            e.Attributes.Append(telefin.CreateAttribute("profile"));
            e.Attributes.Append(telefin.CreateAttribute("active"));
            e.Attributes.Append(telefin.CreateAttribute("scheduled"));
            e.Attributes.Append(telefin.CreateAttribute("snmp_community"));
            e.Attributes.Append(telefin.CreateAttribute("supervisor_id"));

            return e;
        }

        private XmlElement CreateLocation(XmlDocument telefin)
        {
            XmlElement e = telefin.CreateElement("location");

            e.Attributes.Append(telefin.CreateAttribute("id"));
            e.Attributes.Append(telefin.CreateAttribute("name"));
            e.Attributes.Append(telefin.CreateAttribute("type"));
            e.Attributes.Append(telefin.CreateAttribute("note"));

            return e;
        }

        void WriteTrace(string message, params object[] pars)
        {
            this.LastMessage = string.Format(message, pars);
            Output.WriteLine(this.LastMessage);
        }
    }
}

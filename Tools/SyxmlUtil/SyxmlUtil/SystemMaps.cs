﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SyxmlUtil
{
    public class SystemMaps
    {
        [XmlElement(ElementName = "map")]
        public List<SystemMap> Maps { get; set; }
    }

    public class SystemMap
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "deviceMap")]
        public List<ColumnAttribute> DeviceMapList { get; set; }
        [XmlElement(ElementName = "locationMap")]
        public List<ColumnAttribute> LocationMapList { get; set; }
    }

    public class ColumnAttribute
    {
        [XmlAttribute(AttributeName = "attributeName")]
        public string AttributeName { get; set; }

        [XmlAttribute(AttributeName = "columnIndex")]
        public int ColumnIndex { get; set; }

        [XmlAttribute(AttributeName = "attributeBuilderString")]
        public string AttributeBuilderString { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        
        [XmlElement(ElementName = "valueMap")]
        public List<ValueMap> ValueMapList { get; set; }

    }

    public class ValueMap
    {
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "destination")]
        public string Destination { get; set; }
    }

}

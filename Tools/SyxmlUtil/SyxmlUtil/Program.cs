﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Data;
using System.Data.OleDb;
using SyxmlUtil.Properties;

namespace SyxmlUtil
{
    class Program
    {
        static readonly string connectionTemplate = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=YES\"";

        static void Main(string[] args)
        {
            string fileSystemXml = string.Empty;
            string excelFile = string.Empty;
            string excelSheet = Settings.Default.SheetDefaultName;
            string mapName = string.Empty;

            DataTable devices = new DataTable();
            XmlDocument telefin = null;

            foreach (string arg in args)
            {
                if (arg.StartsWith("/fx:",StringComparison.OrdinalIgnoreCase))
                {
                    fileSystemXml =  arg.Substring(4);
                }
                else if (arg.StartsWith("/ef:", StringComparison.OrdinalIgnoreCase))
                {
                    excelFile = arg.Substring(4);
                }
                else if (arg.StartsWith("/es:", StringComparison.OrdinalIgnoreCase))
                {
                    excelSheet = arg.Substring(4);
                }
                else if (arg.StartsWith("/m:", StringComparison.OrdinalIgnoreCase))
                {
                    mapName = arg.Substring(3);
                }
            }

            if (CheckSystemXml(fileSystemXml, ref telefin) && CheckFileExcel(excelFile, excelSheet, ref devices))
            {
                ImportDevices(devices, telefin, mapName);
                SaveSystemXml(telefin, fileSystemXml);
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("Utility per Telefin System.xml, sintassi:");
                Console.WriteLine("");
                Console.WriteLine("SyxmlUtil /fx:<file> /ef:<file> [/es:<sheet>] [/m:<mappa>]");
                Console.WriteLine("");
                Console.WriteLine("          fx:<system.xml da modificare>");
                Console.WriteLine("          ef:<nomefile excel da importare>");
                Console.WriteLine("          es:<nome del foglio di excel>");
                Console.WriteLine("          m:<nome della mappa da utilizzare>");
                Console.WriteLine("");
                Console.WriteLine("Esempio: SyxmlUtil /fx:system.xml /ef:listadevice.xls /es:sheet1 /m:mappa1");
            }
        }

        static void SaveSystemXml(XmlDocument telefin, string filename)
        {
            try
            {
                TextWriter tw = new StreamWriter(filename, false, System.Text.Encoding.ASCII);
                telefin.Save(tw);
                tw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on saving System.xml:{0}", ex.Message);
            }
        }

        static bool CheckSystemXml(string file, ref XmlDocument telefin)
        {
            if (!File.Exists(file))
            {
                Console.WriteLine("File System.xml: '{0}' non trovato.", file);
                return false;
            }

            try
            {
                telefin.Load(file);

                XmlNode topography = telefin.SelectSingleNode("//topography");
                if (topography == null) throw new Exception("Node 'topography' not found");
                XmlNode station = topography.SelectSingleNode("//station");
                if (station == null) throw new Exception("Node 'station' not found");
                XmlNode building = station.SelectSingleNode("//building");
                if (building == null) throw new Exception("Node 'building' not found");
                XmlNode system = telefin.SelectSingleNode("//system");
                if (system == null) throw new Exception("Node 'system' not found");
                XmlNode server = system.SelectSingleNode("//server");
                if (server == null) throw new Exception("Node 'server' not found");
                XmlNode region = server.SelectSingleNode("//region");
                if (region == null) throw new Exception("Node 'region' not found");
                XmlNode zone = region.SelectSingleNode("//zone");
                if (zone == null) throw new Exception("Node 'zone' not found");
                XmlNode node = zone.SelectSingleNode("//node");
                if (node == null) throw new Exception("Node 'node' not found");
                XmlNode stationId = station.SelectSingleNode("@id");
                if (stationId == null) throw new Exception("Attribute 'station.id' not found");
                XmlNode buildingId = building.SelectSingleNode("@id");
                if (buildingId == null) throw new Exception("Attribute 'buildin.id' not found");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading System.xml: '{0}'", ex.Message);
                return false;
            }

            return true;
        }

        static bool CheckFileExcel(string file, string sheet, ref DataTable table)
        {

            if (!File.Exists(file))
            {
                Console.WriteLine("File excel: '{0}' non trovato.", file);
                return false;
            }

            try
            {
                string conn = string.Format(connectionTemplate, file);
                OleDbDataAdapter oleAdapter = new OleDbDataAdapter();
                oleAdapter.SelectCommand = new OleDbCommand(string.Format(@"SELECT * FROM [{0}$]", sheet), new OleDbConnection(conn));
                oleAdapter.FillSchema(table, SchemaType.Source);
                oleAdapter.Fill(table);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading excel file: '{0}'", ex.Message);
                return false;
            }

            return true;
        }

        static void ImportDevices(DataTable devices, XmlDocument telefin, string mapName)
        {
            try 
            {
                SystemMaps maps = Settings.Default.SystemMaps;
                SystemMap map = null;
                XmlNode device = null;
                XmlNode location = null;
                int countOk = 0;
                int countError = 0;
                int countSkipped = 0;


                // trova la mappa
                if (mapName == string.Empty)
                    map = maps.Maps[0];
                else
                    map = maps.Maps.Find(x => x.Name == mapName);

                XmlNode station = telefin.SelectSingleNode("//topography/station");
                XmlNode building = station.SelectSingleNode("//building");
                XmlNode node = telefin.SelectSingleNode("//system/server/region/zone/node");

                Dictionary<string, string> locationContext = new Dictionary<string, string>();
                locationContext.Add("{identity}",GetMaxLocationId(building).ToString());

                Dictionary<string, string> deviceContext = new Dictionary<string, string>();
                deviceContext.Add("{identity}", GetMaxDeviceId(node).ToString());
                deviceContext.Add("{station}", station.Attribute("id").Value);
                deviceContext.Add("{building}", building.Attribute("id").Value);
                deviceContext.Add("{location}", string.Empty);

                for (int i = 0; i < devices.Rows.Count; i++)
                {
                    try
                    {
                        DataRow row = devices.Rows[i];

                        
                        location = CreateLocation(telefin);
                        ImportRow(row, location, map.LocationMapList, locationContext);

                        deviceContext["{location}"] = locationContext["{identity}"];
                        device = CreateDevice(telefin);
                        ImportRow(row, device, map.DeviceMapList, deviceContext);

                        if (location.Attribute("name").Value.Trim().Length == 0 || location.Attribute("type").Value.Trim().Length == 0)
                        {
                            Console.WriteLine("Skipped row {0} reason: '{1}'", i, "location without name or type");
                            countSkipped++;
                            continue;
                        }

                        if (device.Attribute("name").Value.Trim().Length == 0
                            || device.Attribute("addr").Value.Trim().Length == 0
                            || device.Attribute("type").Value.Trim().Length == 0)
                        {
                            Console.WriteLine("Skipped row {0} reason: '{1}'", i, "device without name, addr or type");
                            countSkipped++;
                            continue;
                        }

                        building.Add(location);
                        node.Add(device);
                        countOk++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error on import row {0} errors: '{1}'", i, ex.Message);
                        countError++;
                    }

                }
                Console.WriteLine("Import completed imported rows: {0} skipped:{1} errors:{2}", countOk,countSkipped, countError);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on import: '{0}'", ex.Message);
            }
         }

        static void ImportRow(DataRow row, XElement element, List<ColumnAttribute> mapList, Dictionary<String,String> context )
        {
            foreach (ColumnAttribute ca in mapList)
            {
                if (!string.IsNullOrEmpty(ca.Value))
                {
                    if (ca.Value == "{identity}")
                    {
                        int identity = int.Parse(context["{identity}"]);
                        element.Attribute(ca.AttributeName).Value = (++identity).ToString();
                        context["{identity}"] = identity.ToString();
                    }
                    else if (ca.Value.StartsWith("{") && ca.Value.EndsWith("}"))
                    {
                        element.Attribute(ca.AttributeName).Value = context[ca.Value];
                    }
                    else
                    {
                        element.Attribute(ca.AttributeName).Value = ca.Value.Trim();
                    }
                }
                else
                {
                    string colValue = row[ca.ColumnIndex].ToString().Trim();


                    if (ca.ValueMapList.Count > 0 && colValue.Length > 0 )
                    {
                        ValueMap vm = ca.ValueMapList.Find(p => string.Compare(p.Source.Trim(), colValue.Trim(), true) == 0);

                        if (vm != null)
                            colValue = vm.Destination;
                        else
                            throw new Exception(string.Format("Decod not found for attribute '{0}' value '{1}'", ca.AttributeName, colValue));
                    }

                    element.Attribute(ca.AttributeName).Value = colValue;
                }
            }

        }

        static int GetMaxLocationId(XmlNode building)
        {
            try
            {
                int max = 0;

                XmlNodeList nl = building.SelectNodes("location");

                foreach (XmlNode n in nl)
                {
                    int id = 0;
                    if (int.TryParse(n.Attributes["id"].Value,out id))
                        max = (id > max ? id : max);
                }

                return max;
            }
            catch (Exception ex)
            {
                throw new Exception("Error on GetMaxLocationId", ex);
            }
        }

        static int GetMaxDeviceId(XmlNode node)
        {
            try
            {
                int max = 0;

                XmlNodeList nl = node.SelectNodes("device");

                foreach (XmlNode n in nl)
                {
                    int id = 0;
                    if (int.TryParse(n.Attributes["id"].Value, out id))
                        max = (id > max ? id : max);
                }

                return max;
            }
            catch (Exception ex)
            {
                throw new Exception("Error on GetMaxDeviceId", ex);
            }
        }

        private XmlElement CreateDevice(XmlDocument telefin)
        {
            //<device DevID="0" name="FAS01AT1700L" station="0" building="0" location="0" position="1,1" addr="10.102.222.1" 
            // SN="00000000" port="2" type="AEL0000" profile="1" active="true" scheduled="false" snmp_community="public" /> 

            XmlElement e = telefin.CreateElement("device");

            e.Attributes.Append(telefin.CreateAttribute("DevID"));
            e.Attributes.Append(telefin.CreateAttribute("name"));
            e.Attributes.Append(telefin.CreateAttribute("station"));
            e.Attributes.Append(telefin.CreateAttribute("building"));
            e.Attributes.Append(telefin.CreateAttribute("location"));
            e.Attributes.Append(telefin.CreateAttribute("position"));
            e.Attributes.Append(telefin.CreateAttribute("SN"));
            e.Attributes.Append(telefin.CreateAttribute("addr"));
            e.Attributes.Append(telefin.CreateAttribute("port"));
            e.Attributes.Append(telefin.CreateAttribute("type"));
            e.Attributes.Append(telefin.CreateAttribute("active"));
            e.Attributes.Append(telefin.CreateAttribute("scheduled"));
            e.Attributes.Append(telefin.CreateAttribute("snmp_community"));

            return e;
        }

        private XmlElement CreateLocation(XmlDocument telefin)
        {
            XmlElement e = telefin.CreateElement("location");

            e.Attributes.Append(telefin.CreateAttribute("id"));
            e.Attributes.Append(telefin.CreateAttribute("name"));
            e.Attributes.Append(telefin.CreateAttribute("type"));
            e.Attributes.Append(telefin.CreateAttribute("note"));

            return e;
        }


    }
}

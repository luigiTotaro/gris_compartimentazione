﻿namespace SyxmUtil.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSystemXml = new System.Windows.Forms.TextBox();
            this.btnSystemXml = new System.Windows.Forms.Button();
            this.btnExternalFile = new System.Windows.Forms.Button();
            this.txtExternalFile = new System.Windows.Forms.TextBox();
            this.lblExternalFile = new System.Windows.Forms.Label();
            this.lblParameter = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboMappa = new System.Windows.Forms.ComboBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnImporta = new System.Windows.Forms.Button();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.grdExcel = new System.Windows.Forms.DataGridView();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.ofdSystemXml = new System.Windows.Forms.OpenFileDialog();
            this.ofdExternalFile = new System.Windows.Forms.OpenFileDialog();
            this.cboExternalParameter = new System.Windows.Forms.ComboBox();
            this.chkWithHeaderRow = new System.Windows.Forms.CheckBox();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdExcel)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "System.Xml path";
            // 
            // txtSystemXml
            // 
            this.txtSystemXml.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSystemXml.Location = new System.Drawing.Point(105, 9);
            this.txtSystemXml.Name = "txtSystemXml";
            this.txtSystemXml.ReadOnly = true;
            this.txtSystemXml.Size = new System.Drawing.Size(532, 20);
            this.txtSystemXml.TabIndex = 1;
            // 
            // btnSystemXml
            // 
            this.btnSystemXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSystemXml.Location = new System.Drawing.Point(643, 7);
            this.btnSystemXml.Name = "btnSystemXml";
            this.btnSystemXml.Size = new System.Drawing.Size(26, 23);
            this.btnSystemXml.TabIndex = 2;
            this.btnSystemXml.Text = "...";
            this.btnSystemXml.UseVisualStyleBackColor = true;
            this.btnSystemXml.Click += new System.EventHandler(this.btnSystemXml_Click);
            // 
            // btnExternalFile
            // 
            this.btnExternalFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExternalFile.Location = new System.Drawing.Point(643, 36);
            this.btnExternalFile.Name = "btnExternalFile";
            this.btnExternalFile.Size = new System.Drawing.Size(26, 23);
            this.btnExternalFile.TabIndex = 5;
            this.btnExternalFile.Text = "...";
            this.btnExternalFile.UseVisualStyleBackColor = true;
            this.btnExternalFile.Click += new System.EventHandler(this.btnExternalFile_Click);
            // 
            // txtExternalFile
            // 
            this.txtExternalFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExternalFile.Location = new System.Drawing.Point(105, 36);
            this.txtExternalFile.Name = "txtExternalFile";
            this.txtExternalFile.ReadOnly = true;
            this.txtExternalFile.Size = new System.Drawing.Size(532, 20);
            this.txtExternalFile.TabIndex = 4;
            // 
            // lblExternalFile
            // 
            this.lblExternalFile.AutoSize = true;
            this.lblExternalFile.Location = new System.Drawing.Point(12, 40);
            this.lblExternalFile.Name = "lblExternalFile";
            this.lblExternalFile.Size = new System.Drawing.Size(61, 13);
            this.lblExternalFile.TabIndex = 3;
            this.lblExternalFile.Text = "File esterno";
            // 
            // lblParameter
            // 
            this.lblParameter.AutoSize = true;
            this.lblParameter.Location = new System.Drawing.Point(12, 66);
            this.lblParameter.Name = "lblParameter";
            this.lblParameter.Size = new System.Drawing.Size(51, 13);
            this.lblParameter.TabIndex = 6;
            this.lblParameter.Text = "Parametri";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Mappa";
            // 
            // cboMappa
            // 
            this.cboMappa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboMappa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMappa.FormattingEnabled = true;
            this.cboMappa.Location = new System.Drawing.Point(105, 113);
            this.cboMappa.Name = "cboMappa";
            this.cboMappa.Size = new System.Drawing.Size(532, 21);
            this.cboMappa.TabIndex = 9;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreview.Location = new System.Drawing.Point(687, 7);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 10;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnImporta
            // 
            this.btnImporta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImporta.Location = new System.Drawing.Point(687, 33);
            this.btnImporta.Name = "btnImporta";
            this.btnImporta.Size = new System.Drawing.Size(75, 101);
            this.btnImporta.TabIndex = 11;
            this.btnImporta.Text = "Importa";
            this.btnImporta.UseVisualStyleBackColor = true;
            this.btnImporta.Click += new System.EventHandler(this.btnImporta_Click);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(15, 140);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.grdExcel);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.txtOutput);
            this.splitContainer.Size = new System.Drawing.Size(747, 313);
            this.splitContainer.SplitterDistance = 217;
            this.splitContainer.TabIndex = 14;
            // 
            // grdExcel
            // 
            this.grdExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdExcel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdExcel.Location = new System.Drawing.Point(0, 0);
            this.grdExcel.Name = "grdExcel";
            this.grdExcel.Size = new System.Drawing.Size(747, 217);
            this.grdExcel.TabIndex = 13;
            // 
            // txtOutput
            // 
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(0, 0);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(747, 92);
            this.txtOutput.TabIndex = 15;
            // 
            // ofdSystemXml
            // 
            this.ofdSystemXml.FileName = "System.xml";
            this.ofdSystemXml.Filter = "File Xml|*.xml|Tutti i file|*.*";
            // 
            // ofdExternalFile
            // 
            this.ofdExternalFile.Filter = "File CSV|*.csv|File Excel|*.xls|Tutti i file|*.*";
            // 
            // cboExternalParameter
            // 
            this.cboExternalParameter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboExternalParameter.FormattingEnabled = true;
            this.cboExternalParameter.Location = new System.Drawing.Point(105, 63);
            this.cboExternalParameter.Name = "cboExternalParameter";
            this.cboExternalParameter.Size = new System.Drawing.Size(532, 21);
            this.cboExternalParameter.TabIndex = 15;
            // 
            // chkWithHeaderRow
            // 
            this.chkWithHeaderRow.AutoSize = true;
            this.chkWithHeaderRow.Location = new System.Drawing.Point(105, 90);
            this.chkWithHeaderRow.Name = "chkWithHeaderRow";
            this.chkWithHeaderRow.Size = new System.Drawing.Size(248, 17);
            this.chkWithHeaderRow.TabIndex = 16;
            this.chkWithHeaderRow.Text = "La prima riga contiene le intestazioni di colonna";
            this.chkWithHeaderRow.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 460);
            this.Controls.Add(this.chkWithHeaderRow);
            this.Controls.Add(this.cboExternalParameter);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.btnImporta);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.cboMappa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblParameter);
            this.Controls.Add(this.btnExternalFile);
            this.Controls.Add(this.txtExternalFile);
            this.Controls.Add(this.lblExternalFile);
            this.Controls.Add(this.btnSystemXml);
            this.Controls.Add(this.txtSystemXml);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Importatore periferiche in System.xml";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdExcel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSystemXml;
        private System.Windows.Forms.Button btnSystemXml;
        private System.Windows.Forms.Button btnExternalFile;
        private System.Windows.Forms.TextBox txtExternalFile;
        private System.Windows.Forms.Label lblExternalFile;
        private System.Windows.Forms.Label lblParameter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboMappa;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnImporta;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.DataGridView grdExcel;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.OpenFileDialog ofdSystemXml;
        private System.Windows.Forms.OpenFileDialog ofdExternalFile;
        private System.Windows.Forms.ComboBox cboExternalParameter;
        private System.Windows.Forms.CheckBox chkWithHeaderRow;
    }
}


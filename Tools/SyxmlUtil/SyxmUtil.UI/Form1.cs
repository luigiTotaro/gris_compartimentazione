﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SyxmlUtil;
using System.IO;

namespace SyxmUtil.UI
{
    public partial class Form1 : Form
    {
        Importer importer;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSystemXml_Click(object sender, EventArgs e)
        {
            ofdSystemXml.FileName = txtSystemXml.Text;
            if (ofdSystemXml.ShowDialog(this) == DialogResult.OK)
                txtSystemXml.Text = ofdSystemXml.FileName;
        }

        private void btnExternalFile_Click(object sender, EventArgs e)
        {
            ofdExternalFile.Filter = importer.FileTypes; 
            ofdExternalFile.FileName = txtExternalFile.Text;

            if (ofdExternalFile.ShowDialog(this) == DialogResult.OK)
            {
                txtExternalFile.Text = ofdExternalFile.FileName;

                IExternalDataSource eds = importer.GetDataSource(Path.GetExtension(txtExternalFile.Text));
                lblParameter.Text = eds.ParametersLabel;
                cboExternalParameter.DataSource = eds.ParametersValues;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            importer =  new Importer();
            //Load maps
            cboMappa.DataSource = importer.Maps;
            splitContainer.Panel1Collapsed = true;

        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            importer.Output = new StringWriter();
            grdExcel.DataSource = null;
            grdExcel.DataSource = importer.GetExternalData(txtExternalFile.Text, cboExternalParameter.Text, chkWithHeaderRow.Checked);
            txtOutput.Text = importer.Output.ToString();
            if (string.IsNullOrEmpty(txtOutput.Text))
            {
                splitContainer.Panel2Collapsed = true;
            }
            else
            {
                splitContainer.Panel1Collapsed = true;
            }
            this.Cursor = Cursors.Default;
        }

        private void BindFormToImporter()
        {
            importer.CurrentMap = cboMappa.Text;
            importer.ExternalFile = txtExternalFile.Text;
            importer.ExternalParameter = cboExternalParameter.Text;
            importer.SystemXmlFile = txtSystemXml.Text;
            importer.WithHeaderRow = chkWithHeaderRow.Checked;
        }


        private void BindImporterToForm()
        {
            cboMappa.SelectedText = importer.CurrentMap;
            txtExternalFile.Text = importer.ExternalFile;
            cboExternalParameter.SelectedText = importer.ExternalParameter;
            txtSystemXml.Text = importer.SystemXmlFile;
            chkWithHeaderRow.Checked = importer.WithHeaderRow;
        }

        private void btnImporta_Click(object sender, EventArgs e)
        {
            BindFormToImporter();
            this.Cursor = Cursors.WaitCursor;
            importer.Output = new StringWriter();
            bool ok = importer.Import();
            
            txtOutput.Text = importer.Output.ToString();
            txtOutput.SelectionStart = txtOutput.Text.Length;
            txtOutput.ScrollToCaret(); 
            splitContainer.Panel1Collapsed = true;
            this.Cursor = Cursors.Default;

            MessageBox.Show(this, importer.LastMessage, "Importer", MessageBoxButtons.OK, (ok ? MessageBoxIcon.Information : MessageBoxIcon.Error));
        }

    }
}

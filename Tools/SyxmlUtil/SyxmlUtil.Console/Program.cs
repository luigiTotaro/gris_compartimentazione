﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Data;
using System.Data.OleDb;
using SyxmlUtil.Properties;

namespace SyxmlUtil
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileSystemXml = string.Empty;
            string excelFile = string.Empty;
            string excelSheet = string.Empty;
            string mapName = string.Empty;
            bool withHeaderRow = false;

            foreach (string arg in args)
            {
                if (arg.StartsWith("/fx:",StringComparison.OrdinalIgnoreCase))
                {
                    fileSystemXml =  arg.Substring(4);
                }
                else if (arg.StartsWith("/ef:", StringComparison.OrdinalIgnoreCase))
                {
                    excelFile = arg.Substring(4);
                }
                else if (arg.StartsWith("/ep:", StringComparison.OrdinalIgnoreCase))
                {
                    excelSheet = arg.Substring(4);
                }
                else if (arg.StartsWith("/hr:", StringComparison.OrdinalIgnoreCase))
                {
                    withHeaderRow = (string.Compare(arg.Substring(4), "YES", true) == 0);
                }
                else if (arg.StartsWith("/m:", StringComparison.OrdinalIgnoreCase))
                {
                    mapName = arg.Substring(3);
                }
            }

            SyxmlUtil.Importer importer = new Importer(Console.Out);

            if (!importer.Import(fileSystemXml, excelFile, excelSheet,withHeaderRow, mapName))
            {
                Console.WriteLine("");
                Console.WriteLine("Utility per Telefin System.xml, sintassi:");
                Console.WriteLine("");
                Console.WriteLine("SyxmlUtil /fx:<file> /ef:<file> [/ep:<sheet>] [/hr:YES|NO] [/m:<mappa>]");
                Console.WriteLine("");
                Console.WriteLine("          fx:<system.xml da modificare>");
                Console.WriteLine("          ef:<nomefile da importare>");
                Console.WriteLine("          ep:<per file excel:nome del foglio di excel>");
                Console.WriteLine("             <per file csv:separtore da ultizzare>");
                Console.WriteLine("          hr:<YES|NO> Se yes la prima riga contiene le intestazioni");
                Console.WriteLine("          m:<nome della mappa da utilizzare>");
                Console.WriteLine("");
                Console.WriteLine("Esempio: SyxmlUtil /fx:system.xml /ex:listadevice.xls /ep:sheet1 /hr:YES /m:mappa1");
            }
        }
    }
}

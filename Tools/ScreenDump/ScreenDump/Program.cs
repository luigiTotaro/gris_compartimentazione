﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Web.Services.Protocols;

namespace ScreenDump
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length != 2) {
                Console.WriteLine("Syntax: ScreenDump.exe <vedor> <address>");
                Console.WriteLine("Example: ScreenDump.exe Solari http://10.20.30.40:8081");
                return;
            }

            CallWS(args[0], args[1]);
        }


        public static void CallWS(string vendor, string uri)
        {
            if (!string.IsNullOrEmpty(vendor) && !string.IsNullOrEmpty(uri))
            {
                object result = MonitorWSMethodCall(vendor, uri, "GetScreenDump", null);

                if (result != null)
                {
                    string xmlScreenDump = result.ToString();

                    Console.WriteLine(xmlScreenDump);

                    XmlDocument xmlDoc = new XmlDocument();

                    try
                    {
                        xmlDoc.LoadXml(xmlScreenDump);
                    }
                    catch (XmlException)
                    {
                        HandleException("Errore: il documento xml trasmesso non è valido.");
                        return;
                    }

                    XmlNode rootNode = xmlDoc.GetElementsByTagName("ScreenDump")[0];
                    if (rootNode == null)
                    {
                            rootNode = xmlDoc.GetElementsByTagName("mstns:ScreenDump")[0];
                            if (rootNode == null)
                            {
                                HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'ScreenDump', la specifiche non sono state rispettate.");
                                return;
                            }
                    }

                    XmlNode formatoNode = xmlDoc.GetElementsByTagName("Formato")[0];
                    if (formatoNode == null)
                    {
                        HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'Formato' che specifica il formato di codifica dell'immagine di screen dump.");
                        return;
                    }

                    XmlNode datiNode = xmlDoc.GetElementsByTagName("Dati")[0];
                    if (datiNode == null)
                    {
                        HandleException("Errore: l'xml restituito dal web service non contiene il nodo 'Dati' che contiene l'immagine di screen dump.");
                        return;
                    }

                    if (datiNode.InnerXml.Trim().Length == 0)
                    {
                        HandleException("Errore: l'xml restituito dal web service non contiene informazioni nel nodo 'Dati' che contiene l'immagine di screen dump.");
                        return;
                    }

                    string b64ImgString = datiNode.InnerText.Replace("\n", "");

                    //_context.Response.ContentType = GetContentMimeType(formatoNode.InnerText);
                    //_context.Response.BinaryWrite(Convert.FromBase64String(b64ImgString));
                    //_context.Response.Flush();
                    //_context.Response.End();

                    Console.WriteLine("Tutto ok " + formatoNode.InnerText + " size : " + Convert.FromBase64String(b64ImgString).LongLength);
                }
                else
                {
                    HandleException("Errore: nessuna immagine restituita dal web service.");
                }
            }
            else
            {
                HandleException("Errore: lo screen dump handler non ha ricevuto i parametri corretti per il rendering dell' immagine.");
            }
        }


        private static object MonitorWSMethodCall(string monitorType, string wsUri, string methodName, object[] parameters)
        {
            SoapHttpClientProtocol wsProxy = new SolariMonitorWS.ProxyDispositivoInfostazioni();
            switch (monitorType)
            {
                case "Sysco":
                    {
                        wsProxy = new SyscoMonitorWS.Ws();
                        break;
                    }
                case "Aesys":
                    {
                        wsProxy = new AesysMonitorWS.ICMonitor();
                        break;
                    }
                case "Solari":
                    {
                        wsProxy = new SolariMonitorWS.ProxyDispositivoInfostazioni();
                        break;
                    }
                case "Fida":
                    {
                        wsProxy = new SolariMonitorWS.ProxyDispositivoInfostazioni();
                        break;
                    }
                default:
                    {
                        wsProxy = new FakeMonitorWS.Service();
                        break;
                    }
            }


            wsProxy.Url = wsUri;
            

            MethodInfo wsMethod = null;
            try
            {
                wsMethod = wsProxy.GetType().GetMethod(methodName);
            }
            catch (AmbiguousMatchException)
            {
                HandleException("Errore di reflection durante la risoluzione del metodo del proxy al web service. Il nome potrebbe essere ambiguo.");
            }

            if (wsMethod != null)
            {
                try
                {
                    return wsMethod.Invoke(wsProxy, parameters);
                }
                catch (TargetInvocationException ex)
                {
                    Console.WriteLine(ex.ToString());
                    HandleException("Errore: eccezione sollevata nel metodo del web service.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    HandleException("Errore generico durante l'invocazione del metodo del web service.");
                }
            }

            return null;
        }

        private static string GetContentMimeType(string format)
        {
            switch (format.ToUpper())
            {
                case "JPG":
                    {
                        return "image/jpg";
                    }
                case "JPEG":
                    {
                        return "image/jpeg";
                    }
                case "GIF":
                    {
                        return "image/gif";
                    }
                case "PNG":
                    {
                        return "image/png";
                    }
                case "BMP":
                    {
                        return "image/bmp";
                    }
            }

            return null;
        }

        private static void HandleException(string errorMessage)
        {
            System.Console.WriteLine(errorMessage);
        }



    }
}

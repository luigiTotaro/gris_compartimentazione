﻿//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using System.Xml.Linq;

// See the ReadMe.html for additional information
public class XmlDumper
{

    private List<string> _excludedMembers;
    public List<string> ExcludedMembers
    {
        get { return _excludedMembers; }
        set{_excludedMembers = value;}
    }


    public static void Write(object element)
    {
        Write(element, 0);
    }

    public static void Write(object element, int depth)
    {
        Write(element, depth, Console.Out, new List<string>());
    }

    public static void Write(object element, int depth, TextWriter log, List<string> excludedMembers)
    {
        XmlDumper dumper = new XmlDumper(depth);

        if (excludedMembers != null)
            dumper.ExcludedMembers = excludedMembers;
        else
            dumper.ExcludedMembers = new List<string>();

        XElement xe = dumper.GetXElement(element,null, null);
        xe.Save(log);
    }

    int level;
    readonly int depth;

    private XmlDumper(int depth)
    {
        this.depth = depth;
    }

    private bool IsSimpleType(object o)
    {
        return IsSimpleType(o.GetType());
    }

    private static bool IsSimpleType(Type t)
    {
        return ((t.IsValueType && (t.IsPrimitive || t.IsEnum)) || t == typeof(string) || t == typeof(decimal) || t.BaseType == typeof(MulticastDelegate));
    }


    private XElement GetXElement( object element, string name, XElement parent)
    {
        XElement xReturn;

        if (element == null)
            return null;

        if (string.IsNullOrEmpty(name))
            name = element.GetType().Name;

        if (IsSimpleType(element) && parent != null)
        {
            xReturn = parent;
        }
        else
        {
            xReturn = new XElement(name);
        }

        level++;

        if (IsSimpleType(element))
        {
            XAttribute xa = new XAttribute(name, element);
            xReturn.Add(xa);
        }
        else
        {
            IEnumerable enumerableElement = element as IEnumerable;
            if (enumerableElement != null)
            {
                int index = 0;
                foreach (object value in enumerableElement)
                {
                    string nameValue = name + "_" + index++;
                    if (level < depth) GetXElement(value, nameValue, xReturn);
                }
                if (index == 0)
                    xReturn = null;
            }
            else
            {

                MemberInfo[] members = element.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance );

                foreach (MemberInfo m in members)
                {
                    FieldInfo f = m as FieldInfo;
                    PropertyInfo p = m as PropertyInfo;

                    if ((f != null || p != null) && !this.ExcludedMembers.Contains(m.Name))
                    {

                        object value = f != null ? f.GetValue(element) : p.GetValue(element, null);

                        if (level < depth) GetXElement(value, m.Name, xReturn);

                    }
                }
            }

            if (parent != null && xReturn != null) parent.Add(xReturn);
        }

        level--;
        return xReturn;
    }


}

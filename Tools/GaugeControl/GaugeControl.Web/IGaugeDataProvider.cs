﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GaugeControl.Web
{
	interface IGaugeDataProvider
	{
		double ReferenceValue { get; }
		double ActualValue { get; }
		double SuperiorLimitValue { get; }
		double InferiorLimitValue { get; }
		double SuperiorYellowRedValue { get; }
		double InferiorYellowRedValue { get; }
		double SuperiorYellowGreenValue { get; }
		double InferiorYellowGreenValue { get; }

		double InferiorRedZoneHeight { get; }
		double InferiorYellowZoneHeight { get; }
		double InferiorGreenZoneHeight { get; }
		double SuperiorGreenZoneHeight { get; }
		double SuperiorYellowZoneHeight { get; }
		double SuperiorRedZoneHeight { get; }
		double IndicatorHeight { get; }
	}
}
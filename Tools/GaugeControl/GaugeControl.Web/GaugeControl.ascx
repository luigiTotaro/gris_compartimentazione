﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GaugeControl.ascx.cs" Inherits="GaugeControl.Web.GaugeControl" %>
<div id="divGauge" class="gauge" runat="server" style="width: 40px; height: 130px; background-image: url(IMG/interfaccia/gauge_bg.gif);">
	<asp:updatepanel id="updGauge" runat="server" updatemode="Conditional">
		<contenttemplate>
			<div class="Update">
				<div id="divUpRed" class="band upRed" runat="server"><div style="height: 100%;"></div></div>
				<div id="divUpYellow" class="band upYellow" runat="server"><div style="height: 100%;"></div></div>
				<div id="divUpGreen" class="band upGreen" runat="server"><div style="height: 100%;"></div></div>
				<div id="divDownGreen" class="band downGreen" runat="server"><div style="height: 100%;"></div></div>
				<div id="divDownYellow" class="band downYellow" runat="server"><div style="height: 100%;"></div></div>
				<div id="divDownRed" class="band downRed" runat="server"><div style="height: 100%;"></div></div>
				<img id="imgArrow" src="IMG/Interfaccia/gauge_arrow.gif" alt="actual value" runat="server" />			
			</div>
		</contenttemplate>
	</asp:updatepanel>
</div>		

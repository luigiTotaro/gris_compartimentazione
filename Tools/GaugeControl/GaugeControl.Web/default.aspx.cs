﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GaugeControl.Web
{
	public partial class _default : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			this.UpdateGauge();
		}

		protected void btnUpdate_Click ( object sender, EventArgs e )
		{
			this.UpdateGauge();
		}

		private void UpdateGauge ()
		{
			this.GaugeControl1.ReferenceFormattedValue = this.txtRif.Text;
			this.GaugeControl1.ActualFormattedValue = this.txtValue.Text;
			this.GaugeControl1.SuperiorLimitPerc = this.txtFSPos.Text;
			this.GaugeControl1.SuperiorYellowRedOffsetPerc = this.txtGRPos.Text;
			this.GaugeControl1.SuperiorYellowGreenOffsetPerc = this.txtVGPos.Text;
			this.GaugeControl1.InferiorYellowGreenOffsetPerc = this.txtVGNeg.Text;
			this.GaugeControl1.InferiorYellowRedOffsetPerc = this.txtGRNeg.Text;
			this.GaugeControl1.InferiorLimitPerc = this.txtFSNeg.Text;
			this.GaugeControl1.DataBind();

			this.txtRifVal.Text = this.GaugeControl1.ReferenceValue.ToString("0.##");
            this.txtValueVal.Text = this.GaugeControl1.ActualValue.ToString("0.##");
            this.txtFSPosVal.Text = this.GaugeControl1.SuperiorLimitValue.ToString("0.##");
            this.txtGRPosVal.Text = this.GaugeControl1.SuperiorYellowRedValue.ToString("0.##");
            this.txtVGPosVal.Text = this.GaugeControl1.SuperiorYellowGreenValue.ToString("0.##");
            this.txtVGNegVal.Text = this.GaugeControl1.InferiorYellowGreenValue.ToString("0.##");
            this.txtGRNegVal.Text = this.GaugeControl1.InferiorYellowRedValue.ToString("0.##");
            this.txtFSNegVal.Text = this.GaugeControl1.InferiorLimitValue.ToString("0.##");	
			
			this.lblError.Text = this.GaugeControl1.InitializationExceptionMessage;		
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GaugeControl.Web
{
	public partial class GaugeControl : System.Web.UI.UserControl
	{
		private const int PIXEL_HEIGHT = 100;
		private IGaugeDataProvider _dataProvider = null;

		# region Public Properties
		public string ReferenceFormattedValue
		{
			get { return this.ViewState["ReferenceFormattedValue"].ToString(); }
			set { this.ViewState["ReferenceFormattedValue"] = value; }
		}

		public string ActualFormattedValue
		{
			get { return this.ViewState["ActualFormattedValue"].ToString(); }
			set { this.ViewState["ActualFormattedValue"] = value; }
		}

		public string SuperiorLimitPerc
		{
			get { return this.ViewState["SuperiorLimitPerc"].ToString(); }
			set { this.ViewState["SuperiorLimitPerc"] = value; }
		}

		public string SuperiorYellowRedOffsetPerc
		{
			get { return this.ViewState["SuperiorYellowRedOffsetPerc"].ToString(); }
			set { this.ViewState["SuperiorYellowRedOffsetPerc"] = value; }
		}

		public string SuperiorYellowGreenOffsetPerc
		{
			get { return this.ViewState["SuperiorYellowGreenOffsetPerc"].ToString(); }
			set { this.ViewState["SuperiorYellowGreenOffsetPerc"] = value; }
		}

		public string InferiorYellowGreenOffsetPerc
		{
			get { return this.ViewState["InferiorYellowGreenOffsetPerc"].ToString(); }
			set { this.ViewState["InferiorYellowGreenOffsetPerc"] = value; }
		}

		public string InferiorYellowRedOffsetPerc
		{
			get { return this.ViewState["InferiorYellowRedOffsetPerc"].ToString(); }
			set { this.ViewState["InferiorYellowRedOffsetPerc"] = value; }
		}

		public string InferiorLimitPerc
		{
			get { return this.ViewState["InferiorLimitPerc"].ToString(); }
			set { this.ViewState["InferiorLimitPerc"] = value; }
		}

		public double ReferenceValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.ReferenceValue : 0D; }
		}

		public double ActualValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.ActualValue : 0D; }
		}

		public double SuperiorLimitValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.SuperiorLimitValue : 0D; }
		}

		public double InferiorLimitValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.InferiorLimitValue : 0D; }
		}

		public double SuperiorYellowRedValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.SuperiorYellowRedValue : 0D; }
		}

		public double InferiorYellowRedValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.InferiorYellowRedValue : 0D; }
		}

		public double SuperiorYellowGreenValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.SuperiorYellowGreenValue : 0D; }
		}

		public double InferiorYellowGreenValue
		{
			get { return ( this._dataProvider != null ) ? this._dataProvider.InferiorYellowGreenValue : 0D; }
		}

		public string InitializationExceptionMessage { get; private set; }

		# endregion

		protected void Page_Load ( object sender, EventArgs e )
		{
			//this.updGauge.Attributes["class"] = "Update";
		}

		public new void DataBind ()
		{
			this.InitializeData();
			this.UpdateGauge();			
		}

		private void InitializeData ()
		{
			try
			{
				this._dataProvider = new GaugeDataProvider(this.ReferenceFormattedValue, this.ActualFormattedValue, this.SuperiorLimitPerc,
												this.SuperiorYellowRedOffsetPerc, this.SuperiorYellowGreenOffsetPerc,
												this.InferiorLimitPerc, this.InferiorYellowRedOffsetPerc, this.InferiorYellowGreenOffsetPerc, PIXEL_HEIGHT);
			}
			catch ( GaugeDataProviderInitializationException exc )
			{
				this.InitializationExceptionMessage = exc.Message;
			}
		}

		private void UpdateGauge ()
		{
			if ( this._dataProvider != null && string.IsNullOrEmpty(this.InitializationExceptionMessage) )
			{
				this.divGauge.Style[HtmlTextWriterStyle.BackgroundImage] = "url(IMG/interfaccia/gauge_bg.gif)";

				this.divUpRed.Visible = true;
				this.divUpYellow.Visible = true;
				this.divUpGreen.Visible = true;
				this.divDownGreen.Visible = true;
				this.divDownYellow.Visible = true;
				this.divDownRed.Visible = true;
				this.imgArrow.Visible = true;

				this.divUpRed.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.SuperiorRedZoneHeight));
				this.divUpYellow.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.SuperiorYellowZoneHeight));

                if (this._dataProvider.SuperiorGreenZoneHeight <= 0)
                {
                    this.divUpGreen.Visible = false;
                }
                else
                {
                    this.divUpGreen.Style.Add(HtmlTextWriterStyle.Height,
                                              this._dataProvider.InferiorGreenZoneHeight <= 0
                                                  ? this.FormatPixels(this._dataProvider.SuperiorGreenZoneHeight)
                                                  : this.FormatPixels(this._dataProvider.SuperiorGreenZoneHeight - 1));
                }

                if (this._dataProvider.InferiorGreenZoneHeight <= 0)
                {
                    this.divDownGreen.Visible = false;
                }
                else
                {
                    this.divDownGreen.Style.Add(HtmlTextWriterStyle.Height,
                                                this._dataProvider.SuperiorGreenZoneHeight <= 0
                                                    ? this.FormatPixels(this._dataProvider.InferiorGreenZoneHeight)
                                                    : this.FormatPixels(this._dataProvider.InferiorGreenZoneHeight - 1));
                }

				this.divDownYellow.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.InferiorYellowZoneHeight));
				this.divDownRed.Style.Add(HtmlTextWriterStyle.Height, this.FormatPixels(this._dataProvider.InferiorRedZoneHeight));
                this.imgArrow.Style.Add(HtmlTextWriterStyle.Top, this.FormatPixels(PIXEL_HEIGHT - this._dataProvider.IndicatorHeight - 7/* metà altezza dell' immagine */));
			}
			else
			{
				this.divGauge.Style[HtmlTextWriterStyle.BackgroundImage] = "url(IMG/interfaccia/gauge_d_bg.gif)";

				this.divUpRed.Visible = false;
				this.divUpYellow.Visible = false;
				this.divUpGreen.Visible = false;
				this.divDownGreen.Visible = false;
				this.divDownYellow.Visible = false;
				this.divDownRed.Visible = false;
				this.imgArrow.Visible = false;
			}
		}

		private string FormatPixels ( double value )
		{
			return string.Format("{0}px", value);
		}
	}
}
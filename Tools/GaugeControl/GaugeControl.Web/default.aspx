﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="GaugeControl.Web._default" %>

<%@ Register src="GaugeControl.ascx" tagname="GaugeControl" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
	<link href="Style/gauge.css" rel="stylesheet" type="text/css">
</head>
<body>
	<form id="form1" runat="server">
	<asp:scriptmanager id="sm" runat="server"></asp:scriptmanager>
	<div>
		<table cellpadding="0" cellspacing="0" style="width: 100%;">
			<tr>
				<td style="text-align: center; vertical-align: middle;">
					<uc1:gaugecontrol id="GaugeControl1" runat="server" />
				</td>
				<td>
					<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Valore di Riferimento</td>
					<td><asp:textbox id="txtRif" runat="server" text="5 Volt"></asp:textbox></td>
					<td><asp:textbox id="txtRifVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>Valore da rappresentare</td>
					<td><asp:textbox id="txtValue" runat="server" text="3 Volt"></asp:textbox></td>
					<td><asp:textbox id="txtValueVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>% per fondo scala positivo</td>
					<td><asp:textbox id="txtFSPos" runat="server" text="100%"></asp:textbox></td>
					<td><asp:textbox id="txtFSPosVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>% per soglia Giallo-Rosso positiva</td>
					<td><asp:textbox id="txtGRPos" runat="server" text="30%"></asp:textbox></td>
					<td><asp:textbox id="txtGRPosVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>% per soglia Verde-Giallo positiva</td>
					<td><asp:textbox id="txtVGPos" runat="server" text="10%"></asp:textbox></td>
					<td><asp:textbox id="txtVGPosVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>% per soglia Verde-Giallo Negativa</td>
					<td><asp:textbox id="txtVGNeg" runat="server" text="10%"></asp:textbox></td>
					<td><asp:textbox id="txtVGNegVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>% per soglia Giallo-Rosso negativa</td>
					<td><asp:textbox id="txtGRNeg" runat="server" text="30%"></asp:textbox></td>
					<td><asp:textbox id="txtGRNegVal" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>% per fondo scala negativo</td>
					<td><asp:textbox id="txtFSNeg" runat="server" text="100%"></asp:textbox></td>
					<td><asp:textbox id="txtFSNegVal" runat="server"></asp:textbox></td>
				</tr>
			</table>
				</td>
			</tr>
			<tr>
				<td colspan="2"><asp:button id="btnUpdate" runat="server" text="Ricarica" 
						onclick="btnUpdate_Click" /><asp:label id="lblError" runat="server" forecolor="Red" enableviewstate="false"></asp:label></td>
			</tr>
		</table>		
	</div>
	</form>
</body>
</html>

﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Text;
using System.Xml;
using Microsoft.Win32;
using Properties;

static class Utils
{
	public const string PATH_STLC_VERSION_KEY = @"SOFTWARE\Telefin\STLC1000";

    public static string GetRegistryKey(string keyName, string valueName, string defaultValue)
    {
        string keyValue = null;
        RegistryKey key = null;

        try
        {
            key = Registry.LocalMachine.OpenSubKey(keyName);
            if (key != null)
            {
                keyValue = key.GetValue(valueName, defaultValue) as string;
            }
        }
        catch (SecurityException)
        {
            keyValue = defaultValue;
        }
        finally
        {
            if (key != null)
            {
                key.Close();
            }
        }

        return (string.IsNullOrEmpty(keyValue) ? defaultValue : keyValue);
    }

	public static string GetDBParam ( string dbConnectionString, string param, string defaultValue )
	{
		SqlConnection dbConnection = null;
		SqlCommand cmd = new SqlCommand();
		SqlDataReader drDevices = null;
		string dbVersion;

		try
		{
			dbConnection = new SqlConnection(dbConnectionString);

			cmd.CommandType = CommandType.Text;
			cmd.CommandTimeout = 60;
			cmd.CommandText = string.Format("SELECT ParameterValue FROM stlc_parameters WHERE (ParameterName LIKE '{0}')", param);

			cmd.Connection = dbConnection;

			dbConnection.Open();

			drDevices = cmd.ExecuteReader();

			if ( drDevices.Read() )
			{
				int colParameterValue = drDevices.GetOrdinal("ParameterValue");

				do
				{
					dbVersion = drDevices.IsDBNull(colParameterValue) ? defaultValue : drDevices.GetSqlString(colParameterValue).Value;
				} while ( drDevices.Read() );
			}
			else
			{
				dbVersion = defaultValue;
			}
		}
		finally
		{
			if ( drDevices != null && !drDevices.IsClosed )
			{
				drDevices.Close();
			}

			if ( dbConnection != null )
			{
				dbConnection.Close();
			}

			cmd.Dispose();
		}

		return ( string.IsNullOrEmpty(dbVersion) ? defaultValue : dbVersion );
	}

    public static string GetMainVersionValue(Version dbVersion, Version regVersion, Version manifestVersion)
    {
        if ((dbVersion == null) || (regVersion == null) || (manifestVersion == null))
        {
            return "-";
        }

        if ((dbVersion.Equals(regVersion)) && (dbVersion.Equals(manifestVersion)))
        {
            return dbVersion.ToString();
        }

        return "-";
    }

    public static void caricoDataset(DataSet ds, string sourcefile)
    {
        FileStream input = new FileStream(sourcefile, FileMode.Open);
        XmlTextReader reader = new XmlTextReader(input);
        ds.ReadXmlSchema(reader);
        reader.Close();
        input.Close();
        new XmlDataDocument(ds).Load(sourcefile);
    }

    public static void ReadConfigData(bool verboseMode)
    {
        string percorso = Settings.Default.Percorso;
        FileInfo info = new FileInfo(percorso);
        DataSet mainDataset = new DataSet();

        Version manifestVersion = null;
        Version regVersion = null;
        Version dbVersion = null;

        bool canProceedVerifica = false;

		try
		{
			string pathSTLCVersionKey = Settings.Default.PathSTLCVersionKey;
			if (string.IsNullOrEmpty(pathSTLCVersionKey))
			{
				pathSTLCVersionKey = PATH_STLC_VERSION_KEY;
			}

			regVersion = new Version(GetRegistryKey(pathSTLCVersionKey, "Version", "-"));
		}
		catch
		{
			Console.WriteLine("Errore nella lettura della versione dal file di registro di Windows.");
		}

        if (info.Exists)
        {
            try
            {
                caricoDataset(mainDataset, percorso);
				string manifestVers = mainDataset.Tables["stlc1000"].Rows[0].ItemArray[1].ToString();
				if ( !string.IsNullOrEmpty(manifestVers) )
				{
					if ( manifestVers.StartsWith("x", true, System.Threading.Thread.CurrentThread.CurrentCulture) )
					{
						if ( regVersion != null ) manifestVers = string.Format("{0}.{1}.{2}", regVersion.Major, regVersion.Minor, manifestVers.Substring("x.x.".Length));
					}

					try
					{
						manifestVersion = new Version(manifestVers);
					}
					catch ( ArgumentException )
					{
						Console.WriteLine("La versione del manifest STLC1000 non è valida. Versione letta: {0}.", manifestVers);
					}
					catch ( FormatException )
					{
						Console.WriteLine("La versione del manifest STLC1000 non è valida. Versione letta: {0}.", manifestVers);
					}
					catch ( OverflowException )
					{
						Console.WriteLine("La versione del manifest STLC1000 non è valida. Versione letta: {0}.", manifestVers);
					}
				}
				else
				{
					Console.WriteLine("Il formato della stringa di versione del manifest è scorretto.");
				}
                canProceedVerifica = true;
            }
            catch
            {
                Console.WriteLine("Errore nel caricamento del file manifest.");
            }
        }
        else
        {
            Console.WriteLine("File manifest non presente.");
        }

        ConnectionStringSettings localDatabaseConnectionString;

        try
        {
            localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
        	string stlcDbVersion = "";
            try
            {
            	stlcDbVersion = GetDBParam(localDatabaseConnectionString.ConnectionString, Settings.Default.STLCVersionDbParameterName, "-");
				dbVersion = new Version(stlcDbVersion);
            }
            catch (ArgumentException)
            {
				Console.WriteLine("La versione del parametro STLCVersion recuperata da database non è valida. Versione letta: {0}.",
											  stlcDbVersion);
            }
            catch (SqlException)
            {
                Console.WriteLine("Errore nella lettura della versione dal database SQL locale.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Errore nella lettura della versione dal database SQL locale.");
            }
        }
        catch (ConfigurationErrorsException)
        {
            Console.WriteLine("Errore nella lettura della stringa di connessione al database SQL locale.");
        }
        catch (Exception)
        {
            Console.WriteLine("Errore nella lettura della versione dal database SQL locale.");
        }

        if (verboseMode)
        {
            string version = GetMainVersionValue(dbVersion, regVersion, manifestVersion);

            if (version.Length > 1)
            {
                Console.WriteLine("Sistema STLC1000 ver. {0}", version);
            }
            else
            {
                Console.WriteLine("Sistema STLC1000 ver. {0} (reg: {1} * DB: {2} * manifest: {3})", version, regVersion, dbVersion, manifestVersion);
            }

            if (canProceedVerifica)
            {
                VerificaSistema(mainDataset);
            }
        }
        else
        {
            Console.WriteLine(GetMainVersionValue(dbVersion, regVersion, manifestVersion));
        }
    }

    private static string chkVerEXEConsole(string fileName)
    {
        string fileVersion = "";
        try
        {
            if (File.Exists(fileName))
            {
                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(fileName);
                fileVersion = string.Format("{0}.{1}.{2}.{3}", myFileVersionInfo.FileMajorPart, myFileVersionInfo.FileMinorPart,
                                            myFileVersionInfo.FileBuildPart, myFileVersionInfo.FilePrivatePart); // myFileVersionInfo.FileVersion;
            }
        }
        catch (Exception exception)
        {
            Console.WriteLine("Exception: " + exception.Message);
        }
        return fileVersion;
    }

    private static string chkVerTXTConsole(string fileName)
    {
        string str2 = "";
        bool flag = true;
        try
        {
            if (File.Exists(fileName))
            {
                StreamReader reader = new StreamReader(fileName);
                string str = reader.ReadLine();
                while (flag & (str != null))
                {
                    if (str.Contains("value=\""))
                    {
                        flag = false;
                        str2 = str.Substring(str.IndexOf("value=\"") + 7);
                        str2 = str2.Substring(0, str2.IndexOf("\""));
                    }
                    else
                    {
                        str = reader.ReadLine();
                    }
                }

                reader.Close();
            }
        }
        catch (Exception exception)
        {
            Console.WriteLine("Exception: " + exception.Message);
        }
        return str2;
    }

    private static void VerificaSistema(DataSet mainDataset)
    {
        bool allModulesOk = true;
        StringBuilder modules = new StringBuilder();
		ConnectionStringSettings localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];

        for (int i = 0; i < mainDataset.Tables["modulo"].Rows.Count; i++)
        {
			string sourceName = mainDataset.Tables["modulo"].Rows[i].ItemArray[0].ToString();
            string versioneRif = mainDataset.Tables["modulo"].Rows[i].ItemArray[1].ToString();
            string estensione = mainDataset.Tables["modulo"].Rows[i].ItemArray[2].ToString().ToUpper();
            string descrizione = mainDataset.Tables["modulo"].Rows[i].ItemArray[3].ToString();
			string versioneSource = "";

            if (estensione == "EXE")
            {
				versioneSource = chkVerEXEConsole(sourceName);
            }
            else if (estensione == "XML")
            {
				versioneSource = chkVerTXTConsole(sourceName);
            }
			else if ( estensione == "DBPARAM" )
			{
				versioneSource = Utils.GetDBParam(localDatabaseConnectionString.ConnectionString, sourceName, "-");
			}

            try
            {
				if ( !string.IsNullOrEmpty(versioneSource) )
                {
					Version fileVersion = new Version(versioneSource);
                    Version refVersion = new Version(versioneRif);

                    if (fileVersion == refVersion)
                    {
                        // Verde
                    }
                    else if (fileVersion > refVersion)
                    {
                        // Blu
						allModulesOk = false;

						modules.AppendFormat("    - {0} <versione successiva> v. {1} (richiesta v. {2}) \r\n", descrizione, versioneSource, versioneRif);
                    }
                    else
                    {
                        // Rosso
                        allModulesOk = false;

						modules.AppendFormat("    - {0} v. {1}\r\n", descrizione, versioneSource);
                    }
                }
                else
                {
                    // Rosso, non presente
                    allModulesOk = false;

                    modules.AppendFormat("    - {0} <non presente>\r\n", descrizione);
                }
            }
            catch
            {
                // Rosso, versione non corretta
                allModulesOk = false;

                modules.AppendFormat("    - {0} <formato versione non valido>\r\n", descrizione);
            }
        }

        if (allModulesOk)
        {
            Console.WriteLine("\r\nModuli software: corretti!");
        }
        else
        {
            Console.WriteLine("\r\nModuli software: alcuni moduli sono obsoleti, non presenti o in versione successiva a quella prevista!");
            Console.Write(modules.ToString());
        }
    }
}
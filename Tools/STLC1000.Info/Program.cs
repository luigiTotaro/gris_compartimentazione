using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace STLC1000INFO
{
    internal static class Program
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FreeConsole();

        private enum RunningMode
        {
            WindowsForms,
            ConsoleVersion,
            ConsoleVerbose
        } ;

        [STAThread]
        private static void Main(string[] args)
        {
            RunningMode mode = RunningMode.WindowsForms;

            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "/ver":
                        mode = RunningMode.ConsoleVersion;
                        break;
                    case "/info":
                        mode = RunningMode.ConsoleVerbose;
                        break;
                }
            }

            switch (mode)
            {
                case RunningMode.WindowsForms:
                    FreeConsole();
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1());
                    break;
                case RunningMode.ConsoleVersion:
                    Utils.ReadConfigData(false);
                    break;
                case RunningMode.ConsoleVerbose:
                    Utils.ReadConfigData(true);
                    break;
            }
        }
    }
}
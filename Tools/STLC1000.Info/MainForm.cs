using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Properties;

namespace STLC1000INFO
{
    public class Form1 : Form
    {
        private Button btEsci;
        private Button btVerifica;
        private IContainer components;
        private Label lblVersione;
        private Label lblEx;
        private DataGridView dataGridView1;
        private DataGridViewImageColumn img;
        private DataGridViewTextBoxColumn modulo;
        private DataGridViewTextBoxColumn versione;
        private DataGridViewTextBoxColumn Commento;
        private Label lblVersioneValue;
        private GroupBox grpVersioni;
        private Label lblRegValue;
        private Label lblReg;
        private Label lblManifestValue;
        private Label lblDbValue;
        private Label lblManifest;
        private Label lblDb;
        private readonly DataSet mainDataset = new DataSet();

        private Version regVersion;
        private Version dbVersion;
        private Version manifestVersion;

        public Form1()
        {
            this.InitializeComponent();
        }

        private void btEsci_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btVerifica_Click(object sender, EventArgs e)
        {
            this.VerificaSistema();
        }

        private string chkVerEXE(string fileName)
        {
            string fileVersion = "";
            try
            {
                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(fileName);
                fileVersion = string.Format("{0}.{1}.{2}.{3}", myFileVersionInfo.FileMajorPart, myFileVersionInfo.FileMinorPart,
                                            myFileVersionInfo.FileBuildPart, myFileVersionInfo.FilePrivatePart); // myFileVersionInfo.FileVersion;
            }
            catch (Exception exception)
            {
                this.lblEx.Text = "Exception: " + exception.Message;
            }
            return fileVersion;
        }

        private string chkVerTXT(string fileName)
        {
            string str2 = "";
            bool flag = true;
            try
            {
                StreamReader reader = new StreamReader(fileName);
                string str = reader.ReadLine();
                while (flag & (str != null))
                {
                    if (str.Contains("value=\""))
                    {
                        flag = false;
                        str2 = str.Substring(str.IndexOf("value=\"") + 7);
                        str2 = str2.Substring(0, str2.IndexOf("\""));
                    }
                    else
                    {
                        str = reader.ReadLine();
                    }
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                this.lblEx.Text = "Exception: " + exception.Message;
            }
            return str2;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Init();
        }

        private void Init()
        {
            this.lblEx.Text = "";

			try
			{
				string pathSTLCVersionKey = Settings.Default.PathSTLCVersionKey;
				if (string.IsNullOrEmpty(pathSTLCVersionKey))
				{
					pathSTLCVersionKey = Utils.PATH_STLC_VERSION_KEY;
				}

				this.lblRegValue.Text = Utils.GetRegistryKey(pathSTLCVersionKey, "Version", "-");

				if ( this.lblRegValue.Text.Equals("-") )
				{
					// Il valore recuperato da database corrisponde al defaut, quindi il parametro non ha un valore valido, oppure non � presente su database
				}
				else
				{
					try
					{
						this.regVersion = new Version(this.lblRegValue.Text);
					}
					catch ( ArgumentException )
					{
						this.lblEx.Text = string.Format("La versione dell'STLC1000 recuperata da registro non � valida. Versione letta: {0}.",
														this.lblRegValue.Text);
					}
					catch ( FormatException )
					{
						this.lblEx.Text = string.Format("La versione dell'STLC1000 recuperata da registro non � valida. Versione letta: {0}.",
														this.lblRegValue.Text);
					}
					catch ( OverflowException )
					{
						this.lblEx.Text = string.Format("La versione dell'STLC1000 recuperata da registro non � valida. Versione letta: {0}.",
														this.lblRegValue.Text);
					}
				}
			}
			catch
			{
				this.lblEx.Text = "Errore nella lettura della versione dal file di registro di Windows.";
			}

			string percorso = Settings.Default.Percorso;
			FileInfo info = new FileInfo(percorso);
			if ( info.Exists )
            {
				try
				{
					Utils.caricoDataset(this.mainDataset, percorso);
					string manifestVers = this.mainDataset.Tables["stlc1000"].Rows[0].ItemArray[1].ToString();

					if ( !string.IsNullOrEmpty(manifestVers) )
					{
						if ( manifestVers.StartsWith("x", true, System.Threading.Thread.CurrentThread.CurrentCulture) )
						{
							manifestVers = string.Format("{0}.{1}.{2}", this.regVersion.Major, this.regVersion.Minor, manifestVers.Substring("x.x.".Length));
						}

						try
						{
							this.manifestVersion = new Version(manifestVers);
						}
						catch ( ArgumentException )
						{
							this.lblEx.Text = string.Format("La versione del manifest STLC1000 non � valida. Versione letta: {0}.",
															manifestVers);
						}
						catch ( FormatException )
						{
							this.lblEx.Text = string.Format("La versione del manifest STLC1000 non � valida. Versione letta: {0}.",
															manifestVers);
						}
						catch ( OverflowException )
						{
							this.lblEx.Text = string.Format("La versione del manifest STLC1000 non � valida. Versione letta: {0}.",
															manifestVers);
						}

						this.btVerifica.Enabled = true;
						this.lblManifestValue.Text = this.manifestVersion.ToString();
					}
					else
					{
						this.lblEx.Text = "Il formato della stringa di versione del manifest � scorretto.";
					}
				}
				catch
				{
					this.lblEx.Text = "Errore nel caricamento del file manifest.";
				}
			}
            else
            {
                this.lblEx.Text = "File manifest non presente.";
            }

            try
            {
                ConnectionStringSettings localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];

                try
                {
                    lblDbValue.Text = Utils.GetDBParam(localDatabaseConnectionString.ConnectionString, Settings.Default.STLCVersionDbParameterName, "-");

                    if (this.lblDbValue.Text.Equals("-"))
                    {
                        // Il valore recuperato da database corrisponde al defaut, quindi il parametro non ha un valore valido, oppure non � presente su database
                    }
                    else
                    {
                        try
                        {
                            this.dbVersion = new Version(this.lblDbValue.Text);
                        }
                        catch (ArgumentException)
                        {
                            this.lblEx.Text =
                                string.Format("La versione del parametro STLCVersion recuperata da database non � valida. Versione letta: {0}.",
                                              this.lblDbValue.Text);
                        }
                        catch (FormatException)
                        {
                            this.lblEx.Text =
                                string.Format("La versione del parametro STLCVersion recuperata da database non � valida. Versione letta: {0}.",
                                              this.lblDbValue.Text);
                        }
                        catch (OverflowException)
                        {
                            this.lblEx.Text =
                                string.Format("La versione del parametro STLCVersion recuperata da database non � valida. Versione letta: {0}.",
                                              this.lblDbValue.Text);
                        }
                    }
                }
                catch (ArgumentException)
                {
					this.lblEx.Text = "Errore nella lettura della versione dal database SQL locale.";
                }
                catch (SqlException)
                {
                    this.lblEx.Text = "Errore nella lettura della versione dal database SQL locale.";
                }
                catch (IndexOutOfRangeException)
                {
                    this.lblEx.Text = "Errore nella lettura della versione dal database SQL locale.";
                }
            }
            catch (ConfigurationErrorsException)
            {
                this.lblEx.Text = "Errore nella lettura della stringa di connessione al database SQL locale.";
            }
            catch (Exception)
            {
                this.lblEx.Text = "Errore nella lettura della versione dal database SQL locale.";
            }

            this.lblVersioneValue.Text = Utils.GetMainVersionValue(this.dbVersion, this.regVersion, this.manifestVersion);
        }

        #region InitializeComponent

        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
            ComponentResourceManager resources = new ComponentResourceManager(typeof (Form1));
            this.lblVersione = new Label();
            this.lblEx = new Label();
            this.btVerifica = new Button();
            this.btEsci = new Button();
            this.dataGridView1 = new DataGridView();
            this.img = new DataGridViewImageColumn();
            this.modulo = new DataGridViewTextBoxColumn();
            this.versione = new DataGridViewTextBoxColumn();
            this.Commento = new DataGridViewTextBoxColumn();
            this.lblVersioneValue = new Label();
            this.grpVersioni = new GroupBox();
            this.lblManifestValue = new Label();
            this.lblDbValue = new Label();
            this.lblRegValue = new Label();
            this.lblManifest = new Label();
            this.lblDb = new Label();
            this.lblReg = new Label();
            ((ISupportInitialize) (this.dataGridView1)).BeginInit();
            this.grpVersioni.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblVersione
            // 
            this.lblVersione.AutoSize = true;
            this.lblVersione.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblVersione.Location = new Point(12, 28);
            this.lblVersione.Name = "lblVersione";
            this.lblVersione.Size = new Size(65, 16);
            this.lblVersione.TabIndex = 1;
            this.lblVersione.Text = "Versione:";
            // 
            // lblEx
            // 
            this.lblEx.Anchor = (((AnchorStyles.Bottom | AnchorStyles.Left)));
            this.lblEx.AutoSize = true;
            this.lblEx.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblEx.ForeColor = Color.Red;
            this.lblEx.Location = new Point(12, 458);
            this.lblEx.Name = "lblEx";
            this.lblEx.Size = new Size(45, 16);
            this.lblEx.TabIndex = 3;
            this.lblEx.Text = "label3";
            // 
            // btVerifica
            // 
            this.btVerifica.Anchor = (((AnchorStyles.Top | AnchorStyles.Right)));
            this.btVerifica.Enabled = false;
            this.btVerifica.FlatStyle = FlatStyle.Flat;
            this.btVerifica.Font = new Font("Microsoft Sans Serif", 9F, FontStyle.Bold, GraphicsUnit.Point, ((0)));
            this.btVerifica.ForeColor = Color.White;
            this.btVerifica.Location = new Point(609, 20);
            this.btVerifica.Name = "btVerifica";
            this.btVerifica.Size = new Size(121, 31);
            this.btVerifica.TabIndex = 4;
            this.btVerifica.Text = "Verifica moduli";
            this.btVerifica.UseVisualStyleBackColor = true;
            this.btVerifica.Click += this.btVerifica_Click;
            // 
            // btEsci
            // 
            this.btEsci.Anchor = (((AnchorStyles.Bottom | AnchorStyles.Right)));
            this.btEsci.FlatStyle = FlatStyle.Flat;
            this.btEsci.Font = new Font("Microsoft Sans Serif", 9F, FontStyle.Bold, GraphicsUnit.Point, ((0)));
            this.btEsci.ForeColor = Color.White;
            this.btEsci.Location = new Point(609, 451);
            this.btEsci.Name = "btEsci";
            this.btEsci.Size = new Size(121, 29);
            this.btEsci.TabIndex = 5;
            this.btEsci.Text = "Esci";
            this.btEsci.UseVisualStyleBackColor = true;
            this.btEsci.Click += this.btEsci_Click;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle1.ForeColor = Color.White;
            dataGridViewCellStyle1.SelectionBackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle1.SelectionForeColor = Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Anchor = (((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            this.dataGridView1.BorderStyle = BorderStyle.Fixed3D;
            this.dataGridView1.CausesValidation = false;
            this.dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.None;
            this.dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            dataGridViewCellStyle2.ForeColor = Color.White;
            dataGridViewCellStyle2.SelectionBackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle2.SelectionForeColor = Color.White;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new DataGridViewColumn[] {this.img, this.modulo, this.versione, this.Commento});
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            dataGridViewCellStyle3.ForeColor = Color.White;
            dataGridViewCellStyle3.SelectionBackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle3.SelectionForeColor = Color.White;
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.GridColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            this.dataGridView1.Location = new Point(12, 73);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle4.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            dataGridViewCellStyle4.ForeColor = Color.White;
            dataGridViewCellStyle4.SelectionBackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle4.SelectionForeColor = Color.White;
            dataGridViewCellStyle4.WrapMode = DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle5.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle5.ForeColor = Color.White;
            dataGridViewCellStyle5.SelectionBackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            dataGridViewCellStyle5.SelectionForeColor = Color.White;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.RowTemplate.DefaultCellStyle.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            this.dataGridView1.RowTemplate.DefaultCellStyle.ForeColor = Color.White;
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.White;
            this.dataGridView1.Size = new Size(718, 372);
            this.dataGridView1.TabIndex = 7;
            // 
            // img
            // 
            this.img.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.img.HeaderText = "img";
            this.img.MinimumWidth = 20;
            this.img.Name = "img";
            this.img.ReadOnly = true;
            this.img.Width = 20;
            // 
            // modulo
            // 
            this.modulo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.modulo.HeaderText = "modulo";
            this.modulo.MinimumWidth = 150;
            this.modulo.Name = "modulo";
            this.modulo.ReadOnly = true;
            this.modulo.Width = 150;
            // 
            // versione
            // 
            this.versione.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.versione.HeaderText = "versione";
            this.versione.MinimumWidth = 100;
            this.versione.Name = "versione";
            this.versione.ReadOnly = true;
            // 
            // Commento
            // 
            this.Commento.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.Commento.HeaderText = "Commento";
            this.Commento.MinimumWidth = 200;
            this.Commento.Name = "Commento";
            this.Commento.ReadOnly = true;
            this.Commento.Width = 200;
            // 
            // lblVersioneValue
            // 
            this.lblVersioneValue.AutoSize = true;
            this.lblVersioneValue.Font = new Font("Microsoft Sans Serif", 11.25F, FontStyle.Bold, GraphicsUnit.Point, ((0)));
            this.lblVersioneValue.Location = new Point(83, 26);
            this.lblVersioneValue.Name = "lblVersioneValue";
            this.lblVersioneValue.Size = new Size(14, 18);
            this.lblVersioneValue.TabIndex = 8;
            this.lblVersioneValue.Text = "-";
            // 
            // grpVersioni
            // 
            this.grpVersioni.Controls.Add(this.lblManifestValue);
            this.grpVersioni.Controls.Add(this.lblDbValue);
            this.grpVersioni.Controls.Add(this.lblRegValue);
            this.grpVersioni.Controls.Add(this.lblManifest);
            this.grpVersioni.Controls.Add(this.lblDb);
            this.grpVersioni.Controls.Add(this.lblReg);
            this.grpVersioni.FlatStyle = FlatStyle.Flat;
            this.grpVersioni.Location = new Point(167, 12);
            this.grpVersioni.Name = "grpVersioni";
            this.grpVersioni.Size = new Size(357, 44);
            this.grpVersioni.TabIndex = 9;
            this.grpVersioni.TabStop = false;
            // 
            // lblManifestValue
            // 
            this.lblManifestValue.AutoSize = true;
            this.lblManifestValue.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblManifestValue.Location = new Point(291, 16);
            this.lblManifestValue.Name = "lblManifestValue";
            this.lblManifestValue.Size = new Size(12, 16);
            this.lblManifestValue.TabIndex = 1;
            this.lblManifestValue.Text = "-";
            // 
            // lblDbValue
            // 
            this.lblDbValue.AutoSize = true;
            this.lblDbValue.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblDbValue.Location = new Point(148, 16);
            this.lblDbValue.Name = "lblDbValue";
            this.lblDbValue.Size = new Size(12, 16);
            this.lblDbValue.TabIndex = 1;
            this.lblDbValue.Text = "-";
            // 
            // lblRegValue
            // 
            this.lblRegValue.AutoSize = true;
            this.lblRegValue.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblRegValue.Location = new Point(43, 16);
            this.lblRegValue.Name = "lblRegValue";
            this.lblRegValue.Size = new Size(12, 16);
            this.lblRegValue.TabIndex = 1;
            this.lblRegValue.Text = "-";
            // 
            // lblManifest
            // 
            this.lblManifest.AutoSize = true;
            this.lblManifest.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblManifest.Location = new Point(224, 16);
            this.lblManifest.Name = "lblManifest";
            this.lblManifest.Size = new Size(61, 16);
            this.lblManifest.TabIndex = 0;
            this.lblManifest.Text = "manifest:";
            // 
            // lblDb
            // 
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblDb.Location = new Point(115, 16);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new Size(27, 16);
            this.lblDb.TabIndex = 0;
            this.lblDb.Text = "db:";
            // 
            // lblReg
            // 
            this.lblReg.AutoSize = true;
            this.lblReg.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            this.lblReg.Location = new Point(6, 16);
            this.lblReg.Name = "lblReg";
            this.lblReg.Size = new Size(31, 16);
            this.lblReg.TabIndex = 0;
            this.lblReg.Text = "reg:";
            // 
            // Form1
            // 
            this.BackColor = Color.FromArgb(((((74)))), ((((74)))), ((((74)))));
            this.ClientSize = new Size(742, 492);
            this.Controls.Add(this.grpVersioni);
            this.Controls.Add(this.lblVersioneValue);
            this.Controls.Add(this.btEsci);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblEx);
            this.Controls.Add(this.btVerifica);
            this.Controls.Add(this.lblVersione);
            this.ForeColor = Color.White;
            this.Icon = ((Icon) (resources.GetObject("$this.Icon")));
            this.MinimumSize = new Size(690, 210);
            this.Name = "Form1";
            this.Text = "STLC1000 Info V2";
            this.Load += this.Form1_Load;
            ((ISupportInitialize) (this.dataGridView1)).EndInit();
            this.grpVersioni.ResumeLayout(false);
            this.grpVersioni.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private void VerificaSistema()
        {
            this.dataGridView1.Rows.Clear();

            Color versionColor = Color.White;
            ConnectionStringSettings localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];

            for (int i = 0; i < this.mainDataset.Tables["modulo"].Rows.Count; i++)
            {
                string sourceName = this.mainDataset.Tables["modulo"].Rows[i].ItemArray[0].ToString();
                string versioneRif = this.mainDataset.Tables["modulo"].Rows[i].ItemArray[1].ToString();
                string estensione = this.mainDataset.Tables["modulo"].Rows[i].ItemArray[2].ToString().ToUpper();
                string descrizione = this.mainDataset.Tables["modulo"].Rows[i].ItemArray[3].ToString();
                string versioneSource = "";

                if (estensione == "EXE")
                {
                    versioneSource = this.chkVerEXE(sourceName);
                }
                else if (estensione == "XML")
                {
                    versioneSource = this.chkVerTXT(sourceName);
                }
                else if (estensione == "DBPARAM")
                {
                    versioneSource = Utils.GetDBParam(localDatabaseConnectionString.ConnectionString, sourceName, "-");
                }

                try
                {
                    if (!string.IsNullOrEmpty(versioneSource))
                    {
                        Version requestedVersion = new Version(versioneSource);
                        Version referenceVersion = new Version(versioneRif);

                        if (requestedVersion == referenceVersion)
                        {
                            this.dataGridView1.Rows.Add(new object[] {Resources.picOK, descrizione, versioneRif, "Versione corretta"});

                            if ((versionColor == Color.White) || (versionColor == Color.LimeGreen))
                            {
                                versionColor = Color.LimeGreen;
                            }
                        }
                        else if (requestedVersion > referenceVersion)
                        {
                            this.dataGridView1.Rows.Add(new object[]
                                                            {
                                                                Resources.picNewer, descrizione, versioneSource,
                                                                "Versione successiva a quella prevista! versione richiesta: " + versioneRif
                                                            });

                            if ((versionColor == Color.White) || (versionColor == Color.LimeGreen))
                            {
                                versionColor = Color.LimeGreen;
                            }
                        }
                        else
                        {
                            this.dataGridView1.Rows.Add(new object[]
                                                            {
                                                                Resources.picKO, descrizione, versioneSource,
                                                                "Versione del modulo incorretta! versione richiesta: " + versioneRif
                                                            });

                            versionColor = Color.Red;
                        }
                    }
                    else
                    {
                        this.dataGridView1.Rows.Add(new object[]
                                                        {
                                                            Resources.picKO, descrizione, "", "Errore nella lettura della versione o modulo non presente"
                                                        });
                        versionColor = Color.Red;
                    }
                }
                catch
                {
                    this.dataGridView1.Rows.Add(new object[]
                                                    {
                                                        Resources.picKO, descrizione, versioneSource,
                                                        "Formato della versione non corretto per il file. Versione letta: " + versioneSource
                                                    });

                    versionColor = Color.Red;
                }
            }

            this.lblVersioneValue.ForeColor = versionColor;
        }
    }
}

//------------------------------------------------------------------------------
// Telefin XML View 1.0
//------------------------------------------------------------------------------
// LIBRERIA DI VISUALIZZAZIONE DATI XML (XMLView.cpp)
// Libreria di funzioni per la visualizzazione delle informazioni contenute
// nelle strutturate ricavate da file in formato XML.
// Questa libreria e' stata pensata come strumento per la libreria XMLParser 1.0
//
// Questa libreria supporta i file in formato standard per la creazione di
// strutture generiche ad albero. Supporta inoltre specifici elementi definiti
// da Telefin.
//
// Versione:	0.04 (22.01.2004 -> 29.01.2004)
// Revisione:	nessuna
//
// Copyright: 2003 Telefin s.r.l.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede XMLView.h,XMLParser.cpp,XMLParser.h,ComCtrls.hpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [22.01.2004]:
// - Prima versione prototipo della libreria.
// 0.01 [23.01.2004]:
// - Aggiunto il supporto per tutti i root pointer nella funzione XMLRead.
// 0.02 [26.01.2004]:
// - Aggiunto il supporto dei codici delle entit� XML nella funzione XMLTreeHint
// - Ultimata la funzione XMLTreeView
// - Aggiunto il supporto per l'algoritmo Quicksort (variante "median of three")
//   nella funzione XMLTreeView in fase di ordinamento delle sub-entit� XML.
// - Aggiunta la funzione di comparazione della posizione XMLCmpEntity.
// 0.03 [27.01.2004]:
// - Sistemata la visualizzazione dei nodi per le intestazioni XML nella
//   funzione XMLTreeView.
// 0.04 [29.01.2004]:
// - Elimata la funzione XMLCmpEntity (spostata nella libreria XMLParser).
//------------------------------------------------------------------------------
#pragma hdrstop
#include "XMLView.h"

#pragma package(smart_init)

//==============================================================================
/// Funzione per la creazione di nodi XML.
/// Genera un nodo di tipo TTreeNode per contenere informazioni su un'entit� XML
/// all'interno di un albero di tipo TTreeView che conterr� l'intera struttura
/// della sorgente XML ottenuta tramite libreria XMLParser 1.0.
///
/// \date [22.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
void * XMLCreateNode(TTreeView * pTree,TTreeNode * ParentNode,void * pItem,AnsiString Name,int IconIndex)
{
  TTreeNode * NewNode;

  NewNode                 = pTree->Items->AddChild(ParentNode,Name);
  NewNode->ImageIndex     = IconIndex;
  NewNode->SelectedIndex  = IconIndex;
  NewNode->Data           = pItem;

  return (void *)NewNode;
}

//==============================================================================
/// Funzione per la visualizzazione di nodi XML.
/// Genera un nodo di tipo TTreeNode utilizzando la funzione XMLCreateNode
/// e imposta le sue propriet� in base all�entit� XML <xENTITY> passata.
///
/// \date [26.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLViewEntity(XML_ENTITY * xENTITY,TTreeView * pTree,TTreeNode * ParentNode)
{
  int                     ret_code          ;
  int                     icon              ;
  AnsiString              name              ;
  void                  * tag               ;
  TTreeNode             * node              ;
  XML_ELEMENT           * xHEADER     = NULL;
  XML_ELEMENT           * xELEMENT    = NULL;
  XML_ATTRIBUTE         * xATTRIB     = NULL;
  XML_TEXT              * xTEXT       = NULL;
  XML_COMMENT           * xCOMMENT    = NULL;
  XML_CDATA_SECTION     * xCDATA      = NULL;
  XML_PROC_INSTRUCTION  * xINSTR      = NULL;

  if (xENTITY) // Controllo entit�
  {
    if (xENTITY->structure) // Controllo struttura
    {
      name = "<XML_ENTITY>";
      icon = xENTITY->type; // Icona = codice entit�
      tag = XMLCreateNode(pTree,ParentNode,xENTITY->structure,name,icon);
      node = (TTreeNode*) tag;

      switch (xENTITY->type) // Vari casi del titpo entit�
      {
        case XML_ENTITY_HEADER:
          xHEADER = (XML_ELEMENT*) xENTITY->structure;
          xHEADER->tag = tag;
          name = AnsiString("<?xml>");
        break;
        case XML_ENTITY_ELEMENT:
          xELEMENT = (XML_ELEMENT*) xENTITY->structure;
          xELEMENT->tag = tag;
          name = AnsiString("<")+xELEMENT->name+AnsiString(">");
        break;
        case XML_ENTITY_ATTRIBUTE:
          xATTRIB = (XML_ATTRIBUTE*) xENTITY->structure;
          xATTRIB->tag = tag;
          name = xATTRIB->name+AnsiString("=\"")+xATTRIB->value+AnsiString("\"");
        break;
        case XML_ENTITY_TEXT:
          xTEXT = (XML_TEXT*) xENTITY->structure;
          xTEXT->tag = tag;
          name = AnsiString("\"")+xTEXT->value+AnsiString("\"");
        break;
        case XML_ENTITY_COMMENT:
          xCOMMENT = (XML_COMMENT*) xENTITY->structure;
          xCOMMENT->tag = tag;
          name = xCOMMENT->value;
        break;
        case XML_ENTITY_CDATA_SECTION:
          xCDATA = (XML_CDATA_SECTION*) xENTITY->structure;
          xCDATA->tag = tag;
          name = xCDATA->value;
        break;
        case XML_ENTITY_PROC_INSTRUCTION:
          xINSTR = (XML_PROC_INSTRUCTION*) xENTITY->structure;
          xINSTR->tag = tag;
          name = xINSTR->value;
        break;
        default:
          name = "<UNKNOWN_ENTITY>";
      }
      node->Text = name; // Aggiorno il testo del nodo con quello specifico
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ENTITY;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_ENTITY;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per la visualizzazione dati XML.
/// Visualizza i dati contenuti in una struttura di tipo XML costruita con la
/// libreria XMLParser 1.0, utilizzando l'oggetto TTreeView che permette di
/// sfogliare le entit� mantenendo la loro organizzazione strutturale
/// multilivello.
///
/// \date [27.01.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
int XMLTreeView(XML_SOURCE * source,TTreeView * tree)
{
  int ret_code;
  int level;
  bool finished                       = false;
  bool next_found                     = false;
  XML_ELEMENT           * xFAKE       = NULL;
  XML_ELEMENT           * xHEADER     = NULL;
  XML_ELEMENT           * xELEMENT    = NULL;
  XML_ELEMENT           * xCHILD      = NULL;
  XML_ATTRIBUTE         * xATTRIB     = NULL;
  XML_TEXT              * xTEXT       = NULL;
  XML_COMMENT           * xCOMMENT    = NULL;
  XML_CDATA_SECTION     * xCDATA      = NULL;
  XML_PROC_INSTRUCTION  * xINSTR      = NULL;
  XML_ENTITY            * xENTITY     = NULL;
  XML_ENTITY            * xlENTITY    = NULL; // Lista di sub-entit� per elemento
  int                   * cENTITY     = NULL;
  int                     entity_count;       // Numero di sub-entit� per elemento

  if (source) // Controllo la sorgente
  {
    // Alloco la memoria per una struttura entit� XML di appoggio
    xENTITY = (XML_ENTITY*) malloc(sizeof(XML_ENTITY));

    if (source->root_header)
    {
      xHEADER = source->root_header;

      while(xHEADER)
      {
        // Carico i dati dell'entit� Header XML
        xENTITY->type = XML_ENTITY_HEADER;
        xENTITY->pos = 0;
        xENTITY->structure = (void *) xHEADER;
        // Disegno il nodo dell'intestazione
        XMLViewEntity(xENTITY,tree,NULL);
        // Disegno eventuali attributi
        xATTRIB = xHEADER->attrib;
        while (xATTRIB)
        {
          // Carico i dati dell'entit� attributo dell'Header XML
          xENTITY->type = XML_ENTITY_ATTRIBUTE;
          xENTITY->pos = 0;
          xENTITY->structure = (void *) xATTRIB;
          // Disegno il nodo dell'attributo Header XML
          XMLViewEntity(xENTITY,tree,(TTreeNode*) xHEADER->tag);
          xATTRIB = xATTRIB->next;
        }
        // Passo al successivo (se esiste)
        xHEADER = xHEADER->next;
      }
    }

    if (source->root_element) // Controllo l'elemento root
    {
      level = 0; // Comincio dal livello 0

      // Creo un elemento fasullo come root virtuale dell'albero
      xFAKE                   = XMLCreateElement(source->name,NULL,NULL);
      xELEMENT                = xFAKE;
      xELEMENT->child         = source->root_element;
      xELEMENT->comment       = source->root_comment;
      xELEMENT->text          = source->root_text;
      xELEMENT->cdata         = source->root_cdata;
      xELEMENT->instruction   = source->root_instruction;
      xELEMENT->next          = NULL;

      // Numero massimo di livelli in una struttura XML: 1024
      cENTITY = (int *) malloc(sizeof(int)*1024);
      // Inizializzazioni
      cENTITY[0] = 0; // Salvo il contatore per il livello
      // Numero massimo di sub entit� per elemento: 1024*100 = 102400
      xlENTITY = (XML_ENTITY *) malloc(sizeof(XML_ENTITY*)*1024*100); // Alloco la memoria per le sub-entit�

      while (!finished)
      {
        entity_count = cENTITY[level]; // Recupero il contatore di entit� per il livello dell'albero XML

        /* Disegno il nodo dell'elemento */
        //xELEMENT->tag = XMLCreateNode(tree,pNode,(void *) xELEMENT,AnsiString("<")+xELEMENT->name+AnsiString(">"),1);
        //if (level >= 0) xELEMENT->tag = XMLCreateNode(tree,pNode,(void *) xELEMENT,AnsiString("<") + AnsiString(xELEMENT->name) + ">"+" ["+AnsiString(xELEMENT->pos)+"]"+" "+AnsiString((int)xELEMENT->parent)+":"+AnsiString((int)xELEMENT->prev)+":"+AnsiString((int)xELEMENT)+":"+AnsiString((int)xELEMENT->next)+":"+AnsiString((int)xELEMENT->child),1);

        // Cerco eventuali eventuali attributi (e li disegno subito)
        xATTRIB = xELEMENT->attrib;
        while (xATTRIB)
        {
          // Carico i dati dell'entit� attributo XML
          xENTITY->type = XML_ENTITY_ATTRIBUTE;
          xENTITY->pos = 0;
          xENTITY->structure = (void *) xATTRIB;
          // Visualizzo subito il nodo dell'entit� XML
          XMLViewEntity(xENTITY,tree,(TTreeNode*) xELEMENT->tag);
          // Passo all'attributo successivo (se esiste)
          xATTRIB = xATTRIB->next;
        }

        // Cerco eventuale testo
        xTEXT = xELEMENT->text;
        while (xTEXT)
        {
          xlENTITY[entity_count].pos = xTEXT->pos;
          xlENTITY[entity_count].type = XML_ENTITY_TEXT;
          xlENTITY[entity_count].structure = (void *) xTEXT;
          entity_count++;
          // Passo al testo successivo (se esiste)
          xTEXT = xTEXT->next;
        }

        // Cerco eventuali commenti
        xCOMMENT = xELEMENT->comment;
        while (xCOMMENT)
        {
          xlENTITY[entity_count].pos = xCOMMENT->pos;
          xlENTITY[entity_count].type = XML_ENTITY_COMMENT;
          xlENTITY[entity_count].structure = (void *) xCOMMENT;
          entity_count++;
          // Passo al testo successivo (se esiste)
          xCOMMENT = xCOMMENT->next;
        }

        // Cerco eventuali sezioni CDATA
        xCDATA = xELEMENT->cdata ;
        while (xCDATA)
        {
          xlENTITY[entity_count].pos = xCDATA->pos;
          xlENTITY[entity_count].type = XML_ENTITY_CDATA_SECTION;
          xlENTITY[entity_count].structure = (void *) xCDATA;
          entity_count++;
          // Passo alla sezione dati successiva (se esiste)
          xCDATA = xCDATA->next;
        }

        // Cerco eventuali istruzioni
        xINSTR = xELEMENT->instruction;
        while (xINSTR)
        {
          xlENTITY[entity_count].pos = xINSTR->pos;
          xlENTITY[entity_count].type = XML_ENTITY_PROC_INSTRUCTION;
          xlENTITY[entity_count].structure = (void *) xINSTR;
          entity_count++;
          // Passo all'istruzione successiva (se esiste)
          xINSTR = xINSTR->next;
        }

        // Cerco gli elementi XML figli
        xCHILD = xELEMENT->child;
        while (xCHILD)
        {
          xlENTITY[entity_count].pos = xCHILD->pos;
          xlENTITY[entity_count].type = XML_ENTITY_ELEMENT;
          xlENTITY[entity_count].structure = (void *) xCHILD;
          entity_count++;
          // Passo all'elemento successivo (se esiste)
          xCHILD = xCHILD->next;
        }

        cENTITY[level] = entity_count; // Salvo il nuovo contatore per il livello

        // Algoritmo Bubble Sort (Per l'ordinamento delle entit� XML)
        /*
        for (int i=0; i<entity_count-1; i++)
        {
          for (int j=0; j<entity_count-1; j++)
          {
            if (xlENTITY[j].pos > xlENTITY[j+1].pos)
            {
              xENTITY->pos            = xlENTITY[j].pos         ;
              xENTITY->type           = xlENTITY[j].type        ;
              xENTITY->structure      = xlENTITY[j].structure   ;

              xlENTITY[j].pos         = xlENTITY[j+1].pos       ;
              xlENTITY[j].type        = xlENTITY[j+1].type      ;
              xlENTITY[j].structure   = xlENTITY[j+1].structure ;

              xlENTITY[j+1].pos       = xENTITY->pos            ;
              xlENTITY[j+1].type      = xENTITY->type           ;
              xlENTITY[j+1].structure = xENTITY->structure      ;
            }
          }
        }
        //*/

        // Alboritmo Median Of Three (per l'ordinamento delle sub-entit� XML)
        void * base = (void *) &xlENTITY[0];
        size_t nelem = entity_count;
        size_t width = sizeof(XML_ENTITY);
        qsort(base,nelem,width,&XMLCmpEntity);

        // Ciclo di visualizzazione delle entit� XML appena ordinate
        for (int e=0; e<entity_count; e++)
        {
          // Visualizzo il nodo dell'entit� XML e-esima
          XMLViewEntity(&xlENTITY[e],tree,(TTreeNode*) xELEMENT->tag);
        }

        // Cerco l'elemento successivo da trattare
        if (xELEMENT->child) // Comincio dagli elementi figli
        {
          xELEMENT = xELEMENT->child;
          level++; // Vado al livello successivo
          cENTITY[level] = 0; // Azzero il contatore di entit� per il nuovo livello
        }
        else // Altrimenti...
        {
          next_found = false;

          while(!next_found)
          {
            if (xELEMENT->next) // ... cerco tra gli elementi fratelli ...
            {
              cENTITY[level] = 0;
              xELEMENT = xELEMENT->next; // Passo all'elemento successivo
              next_found = true; // Asserisco il flag di elemento trovato
            }
            else // ... ed eventualmente ...
            {
              if (xELEMENT->parent) // ... torno all'elemento genitore ...
              {
                // Passo al livello superiore
                level--;
                cENTITY[level] = 0;
                xELEMENT = xELEMENT->parent;
              }
              else // ... oppure mi fermo
              {
                xELEMENT = NULL;
                next_found = true; // Asserisco il flag (ma solo per uscire)
              }
            }
          }
        }
        finished = (xELEMENT)? false:true;
      }
      free(xFAKE);
      free(cENTITY);
      free(xlENTITY);
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ROOT;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_SOURCE;
  }

  free(xENTITY);

  return ret_code;
}

//==============================================================================
/// Funzione per la gestione dell'hint.
/// Gestisce l'hint da visualizzare sul TreeView <sender> che rappresenta la
/// struttura XML visualizzata con la funzione XMLTreeView, in base al tipo e
/// alle informazioni relative all'entit� XML identificata dal TreeNode <node>.
///
/// \date [26.01.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall XMLTreeHint(TObject * sender,TTreeNode * node)
{
  TTreeView * tree;
  AnsiString hint;
  XML_ELEMENT           * xELEMENT  ;
  XML_ATTRIBUTE         * xATTRIB   ;
  XML_TEXT              * xTEXT     ;
  XML_COMMENT           * xCOMMENT  ;
  XML_CDATA_SECTION     * xCDATA    ;
  XML_PROC_INSTRUCTION  * xINSTR    ;

  if (sender)
  {
    tree = (TTreeView *) sender;

    switch (node->ImageIndex)
    {
      case XML_ENTITY_HEADER:
        xELEMENT = (XML_ELEMENT*) node->Data;
        hint = AnsiString("Intestazione XML\nNome: ")+xELEMENT->name;
      break;
      case XML_ENTITY_ELEMENT:
        xELEMENT = (XML_ELEMENT*) node->Data;
        hint = AnsiString("Elemento XML\nNome: ")+xELEMENT->name;
      break;
      case XML_ENTITY_ATTRIBUTE:
        xATTRIB = (XML_ATTRIBUTE*) node->Data;
        hint = AnsiString("Attributo XML\nNome: ")+xATTRIB->name+"\nValore: "+xATTRIB->value;
      break;
      case XML_ENTITY_TEXT:
        xTEXT = (XML_TEXT*) node->Data;
        hint = AnsiString("Testo XML\nValore: ")+xTEXT->value;
      break;
      case XML_ENTITY_COMMENT:
        xCOMMENT = (XML_COMMENT*) node->Data;
        hint = AnsiString("Commento XML\nValore: ")+xCOMMENT->value;
      break;
      case XML_ENTITY_CDATA_SECTION:
        xCDATA = (XML_CDATA_SECTION*) node->Data;
        hint = AnsiString("Sezione dati XML\nValore: ")+xCDATA->value;
      break;
      case XML_ENTITY_PROC_INSTRUCTION:
        xINSTR = (XML_PROC_INSTRUCTION*) node->Data;
        hint = AnsiString("Istruzione XML\nValore: ")+xINSTR->value;
      break;
      default:
        hint = "Entit� sconosciuta";
    }

    tree->Hint = hint;
    tree->ShowHint = true;
  }
}


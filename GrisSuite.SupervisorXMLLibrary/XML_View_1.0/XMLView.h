//------------------------------------------------------------------------------
// Telefin XML View 1.0
//------------------------------------------------------------------------------
// HEADER LIBRERIA DI VISUALIZZAZIONE DATI XML (XMLView.h)
//
// Versione:	0.04 (22.01.2004 -> 29.01.2004)
// Revisione:	nessuna
//
// Copyright: 2003 Telefin s.r.l.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede XMLView.cpp,XMLParser.cpp,XMLParser.h,ComCtrls.hpp
//------------------------------------------------------------------------------
// Version history: vedere XMLView.cpp
//------------------------------------------------------------------------------

#ifndef XMLViewH
#define XMLViewH

#include <ComCtrls.hpp>
#include "XMLParser.h"

void              * XMLCreateNode(TTreeView * pTree,TTreeNode * ParentNode,void * pItem,AnsiString Name,int IconIndex);
int                 XMLViewEntity(XML_ENTITY * xENTITY,TTreeView * pTree,TTreeNode * ParentNode);
int                 XMLTreeView(XML_SOURCE * source,TTreeView * tree);
void  __fastcall    XMLTreeHint(TObject * sender,TTreeNode * node);
#endif

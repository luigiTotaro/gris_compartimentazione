//==============================================================================
// Telefin XML Port Module 1.0
//------------------------------------------------------------------------------
// Header Modulo di implementazione porta XML (XMLPort.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.01 (16.01.2007 -> 16.01.2007)
//
// Copyright: 2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland C++ Builder 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede XMLPort.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [16.01.2007]:
// - Prima versione prototipo della libreria.
// 0.02 [17.01.2007]:
// - Aggiunto il campo LastRequestBuffer nel tipo XML_PORT.
// - Aggiunto il campo LastResponseBuffer nel tipo XML_PORT.
// - Modificata XMLNewPort().
// - Modificata XMLOpenPort().
// - Aggiunta XMLLoadPortFile().
// - Aggiunta XMLUnLoadPortFile().
// - Aggiunta XMLExecutePortRequest().
// - Aggiunta XMLGetPortRequestType().
// - Aggiunta XMLGetPortRequestPath().
//==============================================================================
#ifndef XMLPortH
#define XMLPortH

//==============================================================================
// Definizione valori
//------------------------------------------------------------------------------
#define XML_PORT_VALIDITY_CHECK_CODE  					2790945014
#define XML_PORT_STATUS_VALIDITY_CHECK_CODE			2790945251

#define XML_PORT_REQUEST_BUFFER_MAX_LENGTH			1500
#define XML_PORT_RESPONSE_BUFFER_MAX_LENGTH			1500

#define XML_PORT_REQUEST_TYPE_NOT_ACCESSIBLE		456
#define XML_PORT_REQUEST_TYPE_EMPTY							453
#define XML_PORT_REQUEST_TYPE_NOT_XML						452
#define XML_PORT_REQUEST_TYPE_UNKNOWN						457
#define XML_PORT_REQUEST_TYPE_XML_GET_TEXT			455
#define XML_PORT_REQUEST_TYPE_XML_GET_ATTRIBUTE 451

//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define XML_PORT_NO_ERROR                  	    0
#define XML_PORT_FUNCTION_EXCEPTION        	    279001
#define XML_PORT_INVALID_STRUCTURE					    279002
#define XML_PORT_SOURCE_DELETE_FAILURE			    279003
#define XML_PORT_STRUCTURE_DELETE_FAILURE		    279004
#define XML_PORT_FILE_OPEN_FAILURE						  279005
#define XML_PORT_ALREADY_OPENED								  279006
#define XML_PORT_ALREADY_CLOSED								  279007
#define XML_PORT_NOT_OPENED										  279008
#define XML_PORT_INVALID_SOURCE								  279009
#define XML_PORT_BUFFER_EXCEEDS_MAX_LENGTH		  279010
#define XML_PORT_REQUEST_INVALID_OR_NOT_PRESENT	279011
#define XML_PORT_ELEMENT_NOT_RESOLVED						279012
#define XML_PORT_TEXT_NOT_PRESENT								279013
#define XML_PORT_NULL_VALUE											279014
#define XML_PORT_ATTRIBUTE_NOT_PRESENT					279015

//==============================================================================
/// Struttura dati per lo stato della porta XML.
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _XML_PORT_STATUS
{
	unsigned __int32      VCC     ; // Validity Check Code
	bool								  Opened	; // Flag di porta aperta
}
XML_PORT_STATUS;

//==============================================================================
/// Struttura dati per i parametri della porta XML.
///
/// \date [18.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _XML_PORT
{
	unsigned __int32      VCC     ; // Validity Check Code
	void								* Source	; // Struttura sorgente file XML
	XML_PORT_STATUS       Status	; // Struttura stato della porta
	unsigned char					LastRequestBuffer	[XML_PORT_REQUEST_BUFFER_MAX_LENGTH]		; // Buffer dell'ultima richiesta
	unsigned char					LastResponseBuffer[XML_PORT_RESPONSE_BUFFER_MAX_LENGTH]	; // Buffer dell'ultima risposta
	unsigned int					LastRequestLen	;
	unsigned int					LastResponseLen	;
}
XML_PORT;

// Funzione per allocare una nuova struttura parametri porta XML
XML_PORT	* XMLNewPort						( char * filename );
// Funzione per disallocare una struttura parametri porta XML
int 				XMLDeletePort					( XML_PORT * port );
// Funzione per verificare la validita' di una struttura porta XML
bool 				XMLValidPort					( XML_PORT * port );
// Funzione per aprire una porta XML
int					XMLOpenPort						( XML_PORT * port );
// Funzione per chiudere una porta XML
int 				XMLClosePort					( XML_PORT * port );
// Funzione per verificare se una porta XML e' aperta
bool 				XMLPortIsOpened				( XML_PORT * port );
// Funzione per scrivere su una porta XML
int 				XMLWritePort					( XML_PORT * port , char * buffer, unsigned long len, int * written );
// Funzione per leggere da una porta XML
int 				XMLReadPort						( XML_PORT * port , char * buffer, unsigned long len, int * read );
// Funzione per caricare un file 'porta' XML
int					XMLLoadPortFile				( XML_PORT * port );
// Funzione per rilasciare un file 'porta' XML
int					XMLUnLoadPortFile			( XML_PORT * port );
// Funzione per eseguire una richiesta su una porta XML
int					XMLExecutePortRequest	( XML_PORT * port );
// Funzione per recuperare il tipo di richiesta su una porta XML
int					XMLGetPortRequestType	( XML_PORT * port );
// Funzione per recuperare il percorso della richiesta su una porta XML
char *			XMLGetPortRequestPath	( XML_PORT * port );
// Funzione per eseguire la richiesta XML GET TEXT
int					XMLExecuteGetText			( XML_PORT * port, char * path );
// Funzione per eseguire la richiesta XML GET ATTRIBUTE
int					XMLExecuteGetAttribute( XML_PORT * port, char * path );

//------------------------------------------------------------------------------
#endif

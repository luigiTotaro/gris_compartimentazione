//==============================================================================
// Telefin XML Port Module 1.0
//------------------------------------------------------------------------------
// Modulo di implementazione porta XML (XMLPort.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.01 (16.01.2007 -> 16.01.2007)
//
// Copyright: 2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland C++ Builder 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede XMLPort.h, XMLInterface.h
//------------------------------------------------------------------------------
// Version history: vedere in XMLPort.h
//==============================================================================
#pragma hdrstop
//------------------------------------------------------------------------------
#include "XMLPort.h"
#include "XMLInterface.h"
//------------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
/// Funzione per allocare una nuova struttura parametri porta XML
///
/// \date [18.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
XML_PORT	* XMLNewPort( char * filename )
{
	XML_PORT * port = NULL; // Ptr a struttura parametri porta

	try
	{
		if ( filename != NULL ) {
			// --- Alloco la porta XML ---
			port = (XML_PORT*)malloc( sizeof(XML_PORT) );
			if ( port != NULL ) {
				try
				{
					// --- Applico il codice di validita' ---
					port->VCC = XML_PORT_VALIDITY_CHECK_CODE;
					port->Status.VCC = XML_PORT_STATUS_VALIDITY_CHECK_CODE;
					port->Status.Opened = false;
					// --- Inizializzo i buffer ---
					port->LastRequestLen = 0;
					port->LastResponseLen = 0;
					memset( &port->LastResponseBuffer[0], 0, sizeof(unsigned char) * XML_PORT_RESPONSE_BUFFER_MAX_LENGTH );
					memset( &port->LastRequestBuffer[0]	, 0, sizeof(unsigned char) * XML_PORT_REQUEST_BUFFER_MAX_LENGTH	 );
				}
				catch(...)
				{
					port = NULL;
				}
				// ---
				if ( port != NULL ) {
					// --- Alloco una nuova sorgente XML e la inserisco ---
					port->Source = (void*) XMLCreate( filename );
				}
			}
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		port = NULL;
	}

	return port;
}

//==============================================================================
/// Funzione per disallocare una struttura parametri porta XML
///
/// \date [26.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLDeletePort( XML_PORT * port )
{
	int ret_code = XML_PORT_NO_ERROR;
	int fun_code = 0;
	XML_SOURCE * source = NULL;

	try
	{
		if ( XMLValidPort( port ) ) {
			try
			{
				source = (XML_SOURCE*)port->Source;
				if( source != NULL )
				{
					// --- Cancello la sorgente e la sua struttura XML ---
					XMLFree( source );
				}
			}
			catch(...)
			{
				ret_code = XML_PORT_SOURCE_DELETE_FAILURE;
			}
			try
			{
				memset( port, 0, sizeof(XML_PORT) );
				free( port );
			}
			catch(...)
			{
				ret_code = XML_PORT_STRUCTURE_DELETE_FAILURE;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una struttura porta XML
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLValidPort( XML_PORT * port )
{
	bool valid = false;

	try
	{
		if ( port != NULL ) {
			if ( port->VCC == XML_PORT_VALIDITY_CHECK_CODE ) {
				valid = true;
			}
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		valid = false;
	}

	return valid;
}

//==============================================================================
/// Funzione per aprire una porta XML
///
/// \date [17.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	XMLOpenPort( XML_PORT * port )
{
	int ret_code = XML_PORT_NO_ERROR;
	int fun_code = XML_PORT_NO_ERROR;
	XML_SOURCE * source = NULL;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( port->Status.Opened ) {
				ret_code = XML_PORT_ALREADY_OPENED;
			}
			else
			{
				source = (XML_SOURCE*) port->Source;
				if ( source != NULL ) {
				  // --- Faccio un tentativo di apertura del file ---
				  fun_code = XMLOpen( source, NULL, 'r' );
				  if ( fun_code == XML_PORT_NO_ERROR ) {
				  	// --- Tentativo riuscito ---
				  	port->Status.Opened = true;
				  	// --- Chiudo il file XML ---
						XMLClose( source );
				  }
				  else
					{
				  	// --- Tentativo di apertura del file non riouscito ---
				  	port->Status.Opened = false;
				  	ret_code = XML_PORT_FILE_OPEN_FAILURE;
					}
				}
				else
				{
					ret_code = XML_PORT_INVALID_SOURCE;
				}
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	ret_code = XML_PORT_NO_ERROR;

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere una porta XML
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLClosePort( XML_PORT * port )
{
	int ret_code = XML_PORT_NO_ERROR;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( port->Status.Opened ) {
				port->Status.Opened = false;
			}
			else
			{
				ret_code = XML_PORT_ALREADY_CLOSED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se una porta e' aperta
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLPortIsOpened( XML_PORT * port )
{
	bool opened = false;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( port->Status.VCC == XML_PORT_STATUS_VALIDITY_CHECK_CODE ) {
				if ( port->Status.Opened == true ) {
					opened = true;
				}
			}
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		opened = false;
	}

	return opened;
}

//==============================================================================
/// Funzione per scrivere su una porta XML
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLWritePort( XML_PORT * port , char * buffer, unsigned long len, int * written )
{
	int ret_code = XML_PORT_NO_ERROR;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( XMLPortIsOpened( port ) ) {
				// --- Controllo troncamento richiesta ---
				if (len <= XML_PORT_RESPONSE_BUFFER_MAX_LENGTH) {
					memcpy( port->LastRequestBuffer, buffer, sizeof(char)* len );
					port->LastRequestLen = len;
				}
				else
				{
					ret_code = XML_PORT_BUFFER_EXCEEDS_MAX_LENGTH;
				}
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
	  else
		{
	  	ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per leggere da una porta XML
///
/// \date [01.02.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int XMLReadPort( XML_PORT * port , char * buffer, unsigned long len, int * read )
{
	int 								ret_code 			= XML_PORT_NO_ERROR;
	unsigned __int16 		reslen				= 0;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( XMLPortIsOpened( port ) ) {
				if ( port->LastRequestLen > 0 ) {
					XMLLoadPortFile( port );
					XMLExecutePortRequest( port );
					XMLUnLoadPortFile( port );
				}
				if ( port->LastResponseLen > 0 ) {
					strcpy( &buffer[0], "OK:" );
					memcpy( &buffer[3], &reslen, sizeof(unsigned __int16) );
					reslen = port->LastResponseLen;
					memcpy( &buffer[5], &reslen, sizeof(unsigned __int16) );
					memcpy( &buffer[7], port->LastResponseBuffer, sizeof( char ) * port->LastResponseLen );
					*read = ( port->LastResponseLen + 7 );
					// --- Azzero il buffer dell'ultima risposta sulla porta ---
					memset( port->LastResponseBuffer, 0, sizeof( char ) * XML_PORT_RESPONSE_BUFFER_MAX_LENGTH );
					port->LastResponseLen = 0;
				}
				else
				{
					*read = 0;
				}
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare un file 'porta' XML
///
/// \date [18.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	XMLLoadPortFile( XML_PORT * port )
{
	int 					ret_code 	= XML_PORT_NO_ERROR;
	XML_SOURCE 	* source 		= NULL;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( XMLPortIsOpened( port ) ) {
				source = (XML_SOURCE*)port->Source;
				if ( source != NULL ) {
					ret_code = XMLOpen( source, NULL, 'r' );
					if ( ret_code == XML_PORT_NO_ERROR ) {
						// --- Leggo il file XML ---
						ret_code = XMLRead( source, NULL );
					}
				}
				else
				{
					ret_code = XML_PORT_INVALID_SOURCE;
				}
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per rilasciare un file 'porta' XML
///
/// \date [26.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	XMLUnLoadPortFile( XML_PORT * port )
{
	int 					ret_code 	= XML_PORT_NO_ERROR;
	XML_SOURCE 	* source 		= NULL;

	try
	{
		if ( XMLValidPort( port ) ) {
			if ( XMLPortIsOpened( port ) ) {
				source = (XML_SOURCE*)port->Source;
				if ( source != NULL ) {
					// --- Cancello la struttura XML dalla memoria ---
					ret_code = XMLClear( source );
					// --- Chiudo il file XML ---
					ret_code = XMLClose( source );
				}
				else
				{
					ret_code = XML_PORT_INVALID_SOURCE;
				}
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eseguire una richiesta su una porta XML
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	XMLExecutePortRequest( XML_PORT * port )
{
	int 		ret_code 			= XML_PORT_NO_ERROR;
	int 		request_type 	= XML_PORT_REQUEST_TYPE_UNKNOWN;
	char	* path					= NULL;

	try
	{
		// --- Verifico la validita' della porta XML ---
		if ( XMLValidPort( port ) ) {
			// --- Verifico se la porta XML e' aperta ---
			if ( XMLPortIsOpened( port ) )
			{
				request_type = XMLGetPortRequestType( port );
				switch ( request_type ){
					case XML_PORT_REQUEST_TYPE_XML_GET_TEXT:
						path = XMLGetPortRequestPath( port );
						if ( path != NULL ) {
							ret_code = XMLExecuteGetText( port, path );
							free( path );
						}
					break;
					case XML_PORT_REQUEST_TYPE_XML_GET_ATTRIBUTE:
						path = XMLGetPortRequestPath( port );
						if ( path != NULL ) {
							ret_code = XMLExecuteGetAttribute( port, path );
							free( path );
						}
					break;
				  default:
				  	ret_code = XML_PORT_REQUEST_INVALID_OR_NOT_PRESENT;
				}
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per recuperare il tipo di richiesta su una porta XML
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	XMLGetPortRequestType( XML_PORT * port )
{
	int 		request_type 		= XML_PORT_REQUEST_TYPE_NOT_ACCESSIBLE;
	char 	* request_family 	= NULL;
	char 	* request_string 	= NULL;

	try
	{
		// --- Verifico la validita' della porta ---
		if ( XMLValidPort( port ) ) {
			// --- Verifico la validita' della richiesta ---
			if ( port->LastRequestLen > 0 ) {
				// --- Verifico che la richiesta sia della famiglia XML ---
				request_family = XMLGetToken( port->LastRequestBuffer, " ", 0 ); // -MALLOC
				if ( strcmpi( request_family, "XML" ) == 0 ) {
					// --- Verifico il tipo di richiesta ---
					request_string = XMLGetToken( port->LastRequestBuffer, " FROM ", 0 ); // -MALLOC
					if ( strcmpi( request_string, "XML GET TEXT" ) == 0 ) {
						request_type = XML_PORT_REQUEST_TYPE_XML_GET_TEXT;
					}
					else if ( strcmpi( request_string, "XML GET ATTRIBUTE" ) == 0 ) {
						request_type = XML_PORT_REQUEST_TYPE_XML_GET_ATTRIBUTE;
					}
					else {
						request_type = XML_PORT_REQUEST_TYPE_UNKNOWN;
					}
				}
				else
				{
					request_type = XML_PORT_REQUEST_TYPE_NOT_XML;
				}
      }
			else
			{
				request_type = XML_PORT_REQUEST_TYPE_EMPTY;
			}
		}
		else
		{
			request_type = XML_PORT_REQUEST_TYPE_NOT_ACCESSIBLE;
		}
	}
	catch(...)
	{
		// --- Eccezione generale ---
		request_type = XML_PORT_REQUEST_TYPE_NOT_ACCESSIBLE;
	}

	// --- Elimino le stringhe di appoggio ---
	if ( request_string != NULL ) free( request_string ); // -FREE
	if ( request_family != NULL ) free( request_family ); // -FREE

	return request_type;
}

//==============================================================================
/// Funzione per recuperare il percorso della richiesta su una porta XML
///
/// \date [18.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLGetPortRequestPath( XML_PORT * port )
{
	int 		request_type = XML_PORT_REQUEST_TYPE_NOT_ACCESSIBLE;
	char 	* request_path = NULL;

	try
	{
		// --- Verifico la validita' della porta ---
		if ( XMLValidPort( port ) ) {
			// --- Verifico la validita' della richiesta ---
			if ( port->LastRequestLen > 0 ) {
				// --- Estraggo il percorso XML della richiesta sulla porta ---
				request_path = XMLGetToken( port->LastRequestBuffer, " FROM ", 1 ); // -MALLOC
			}
		}
	}
	catch(...)
	{
		// --- Eccezione generale ---
		request_path = NULL;
	}

	return request_path;
}

//==============================================================================
/// Funzione per eseguire la richiesta XML GET TEXT
///
/// \date [01.02.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int	XMLExecuteGetText( XML_PORT * port, char * path )
{
	int 					ret_code 	= XML_PORT_NO_ERROR;
	XML_SOURCE 	* source 		= NULL;
	XML_ELEMENT	* element		= NULL;
	XML_TEXT		* text			= NULL;
	char				* value			= NULL;

	try
	{
		// --- Verifico la validita' della porta XML ---
		if ( XMLValidPort( port ) ) {
			// --- Verifico se la porta XML e' aperta ---
			if ( XMLPortIsOpened( port ) )
			{
				source = (XML_SOURCE*)port->Source;
				element = XMLResolveElement( source->root_element, path, NULL );
				// ---  ---
				if ( element != NULL ) {
					text = (XML_TEXT*)element->text;
					if ( text != NULL ) {
						value = text->value;
						if ( value != NULL ) {
							memcpy( port->LastResponseBuffer, value, sizeof(char) * text->length );
							port->LastResponseLen = strlen( value ) + 1;
						}
						else
						{
							/*
							memset( port->LastResponseBuffer, 0, sizeof(char) * XML_PORT_RESPONSE_BUFFER_MAX_LENGTH );
							port->LastResponseLen = 0;
							*/
							strcpy( port->LastResponseBuffer, "Informazione non disponibile" );
							port->LastResponseLen = strlen( "Informazione non disponibile" ) + 1;
							ret_code = XML_PORT_NULL_VALUE;
						}
					}
					else
					{
						strcpy( port->LastResponseBuffer, "Testo non trovato" );
						port->LastResponseLen = strlen( "Testo non trovato" ) + 1;
						ret_code = XML_PORT_TEXT_NOT_PRESENT;
					}
				}
				else
				{
					strcpy( port->LastResponseBuffer, "Elemento non trovato" );
					port->LastResponseLen = strlen( "Elemento non trovato" ) + 1;
					ret_code = XML_PORT_ELEMENT_NOT_RESOLVED;
				}
				// --- Azzero il buffer della richiesta sulla porta ---
				memset( port->LastRequestBuffer, 0, sizeof(char) * XML_PORT_REQUEST_BUFFER_MAX_LENGTH );
				port->LastRequestLen = 0;
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- Eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eseguire la richiesta XML GET ATTRIBUTE
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	XMLExecuteGetAttribute( XML_PORT * port, char * path )
{
	int ret_code = XML_PORT_NO_ERROR;
	XML_SOURCE 	* source 		= NULL;
	XML_ELEMENT	* element		= NULL;
	char				* attrib		= NULL;
	char				* value			= NULL;

	try
	{
		// --- Verifico la validita' della porta XML ---
		if ( XMLValidPort( port ) ) {
			// --- Verifico se la porta XML e' aperta ---
			if ( XMLPortIsOpened( port ) )
			{
				source = (XML_SOURCE*)port->Source;
				element = XMLResolveElement( source->root_element, path, NULL );
				// ---  ---
				if ( element != NULL ) {
					attrib = XMLGetToken( path, "." ,1 ); // -MALLOC
					if ( attrib != NULL ) {
						value = XMLGetValue( element, attrib );
						if ( value != NULL ) {
							memcpy( port->LastResponseBuffer, value, sizeof(char) * (strlen(value)+1) );
							port->LastResponseLen = (strlen(value)+1);
						}
						else
						{
							memset( port->LastResponseBuffer, 0, sizeof(char) * XML_PORT_RESPONSE_BUFFER_MAX_LENGTH );
							port->LastResponseLen = 0;
							ret_code = XML_PORT_NULL_VALUE;
						}
						free( attrib ); // -FREE
					}
					else
					{
						ret_code = XML_PORT_ATTRIBUTE_NOT_PRESENT;
					}
				}
				else
				{
					ret_code = XML_PORT_ELEMENT_NOT_RESOLVED;
				}
				// --- Azzero il buffer della richiesta sulla porta ---
				memset( port->LastRequestBuffer, 0, sizeof(char) * XML_PORT_REQUEST_BUFFER_MAX_LENGTH );
				port->LastRequestLen = 0;
			}
			else
			{
				ret_code = XML_PORT_NOT_OPENED;
			}
		}
		else
		{
			ret_code = XML_PORT_INVALID_STRUCTURE;
		}
	}
	catch(...)
	{
		// --- eccezione generale ---
		ret_code = XML_PORT_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
// Telefin XML Parser 1.0
//------------------------------------------------------------------------------
// HEADER LIBRERIA ACCESSO A FILE FORMATO XML (XMLParser.h)
//
// Versione:	1.19 (04.12.2003 -> 22.08.2012)
//
// Copyright:	2003-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Riccardo Venturini (riccardo.venturini@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:		richiede XMLParser.cpp,stdio.h,string.h,stdlib.h,alloc.h,process.h
//------------------------------------------------------------------------------
// Version history:
// 0.00 [04.12.2003]:
// - Prima versione prototipo della libreria.
// 0.01 [23.12.2003]:
// - Aggiunta dichiarazione struttura XML_TEXT
// - Aggiunta dichiarazione struttura XML_CDATA_SECTION
// - Aggiunta dichiarazione struttura XML_PROC_INSTRUCTION
// - Aggiunto campo <file> nella struttura XML_SOURCE
// 0.02 [08.01.2004]:
// - Aggiunti campi <prev> e <next> nelle strutture XML
// - Aggiornata la funzione XMLCreateElement
// - Aggiunta la funzione XMLGets
// - Aggiunta la funzione XMLChop
// - Aggiornata la funzione XMLRead
// - Aggiunta la funzione XMLPuts
// 0.03 [09.01.2004]:
// - Aggiunta la funzione XMLTrim
// - Modificata la funzione XMLGets
// 0.04 [13.01.2004]:
// - Modificata la funzione XMLGets per migliorare la gestione dell'allocazione
//   della memoria
// 0.05 [14.01.2004]:
// - Modificata la funzione XMLCreateElement introducendo il campo <prev>
// - Aggiunta la funzione XMLCreateAttribtue
// - Aggiunta la funzione XMLExtract
// 0.06 [15.01.2004]:
// - Modificata la funzione XMLExtract
// - Modificata la funzione XMLCreateAttribute
// - Aggiunta la funzione XMLElementLink
// - Aggiunta la funzione XMLElementUpLink
// - Aggiunta la funzione XMLAttributeLink
// - Aggiunta la funzione XMLAttributeUpLink
// - Aggiornata la struttura XML_ELEMENT con l'aggiunta del campo <tag>
// - Modificata la funzione XMLCreateElement con l'inizializzazione di <tag>
// 0.07 [16.01.2004]:
// - Aggiunto il campo <tag> in tutte le strutture di oggetti XML
// - Aggiunto il campo <pos> in tutte le strutture di oggetti XML
// 0.08 [19.01.2004]:
// - Migliorata la funzione XMLRead con l'aggiunta di lettura di sezioni dati,
//   testi, commenti e istruzioni.
// 0.09 [20.01.2004]:
// - Aggiunta la funzione XMLTokCmp.
// - Aggiunta la funzione XMLFilled.
// - Modificata la funzione XMLRead per gestire i casi particolari dei tag CData
//   e Comments.
// - Aggiunto il campo <header> nella struttura XML_SOURCE.
// - Aggiunti campi puntatori alle entit� XML root nella struttura XML_SOURCE.
// - Aggiunto flag di <well_formed> nella struttura XML_SOURCE.
// 0.10 [21.01.2004]:
// - Aggiunto il campo <text_counter> nella struttura XML_ELEMENT.
// - Aggiunto il controllo dei contatori di sub-entit� XML nella funzione
//   XMLRead.
// - Aggiunto controllo dei campi <pos> e migliorata la gestione degli attributi
//   con valore implicito.
// - Aggiunte funzioni di cancellazione della memoria alla fine della funzione
//   XMLRead.
// - Aggiunto supporto per i NON-TAG finali in un file XML nella funzione
//   XMLRead.
// 0.11 [22.04.2004]:
// - Migliorato supporto per i tag speciali.
// 0.12 [23.01.2004]:
// - Aggiunta la gestione dei root pointer anche per tutti i tag e i testi XML
//   nella funzione XMLRead.
// - Aggiunto supporto per elementi con nome di 1 carattere nella funzione
//   XMLExtract.
// - Aggiunto il campo <version> nella struttura XML_SOURCE.
// - Aggiunta l'estrazione dei campi <version>,<veriosn_major>,<version_minor>,
//   <encoding> e <stand_alone> nella funzione XMLRead.
// - Definiti i codici di tipo di entit� XML.
// - Dichiarata la struttura XML_ENTITY.
// - Il campo <pos> per i tag Header XML � sempre fissato a 0.
// 0.13 [26.01.2004]:
// - Eliminata l'inclusione del form di debug.
// - Eliminate tutte le chiamate verso oggetti del form di debug.
// - Eliminate tutte le funzioni di debug.
// 0.14 [27.01.2004]:
// - Modificata la funzione XMLOpen per supportare due modalit� di apertura
//   delle sorgenti XML.
// - Aggiunta la funzione XMLCreate per generare nuove strutture di sorgente XML
// - Aggiunta la funzione XMLWrite per la scrittura di file XML (da finire).
// 0.15 [28.01.2004]:
// - Aggiunta la funzione XMLGenerateTag per creare righe di testo XML che
//   contengano le informazioni di un elemento XML.
// 0.16 [29.01.2004]:
// - Modificata la funzione XMLGenerateTag con supporto sia per elementi con
//   chiusura sia elementi senza.
// - Aggiunta la fuznione XMLGenerateClosureTag per creare chiusure di elementi.
// - Aggiunta la funzione XMLNoChildElement per controllare se un elemento ha
//   sub-entit� XML.
// - Aggiunta la funzione XMLGetTag per creare righe di testo del tag da una
//   struttura di entit� XML.
// - Incorporata la funzione XMLCmpEntity precedentemente implementata nella
//   libreria XMLView 0.03.
// - Aggiunti i campi <prev> e <next> nella struttura XML_ENTITY.
// - Aggiunto il campo <first_entity> nella struttura XML_SOURCE.
// - Aggiunto il campo <level> nella struttura XML_ENTITY.
// - Aggiunto il controllo del campo <first_entity> nella funzione XMLCreate.
// - Aggiunta la funzione XMLCreateEntity per la creazione di nuove entit�.
// - Aggiunto la creazione di una lista di entit� XML linkata dal campo
//   <first_entity> della struttura <source> e il supporto per il campo <level>
//   delle strutture XML_ENTITY nella funzione XMLRead.
// - Ultimata la funzione XMLWrite con il supporto della lista di entit� XML.
// 0.17 [30.01.2004]:
// - Aggiunte le funzioni XMLFreeElement, XMLFreeAttribute, XMLFreeText,
//   XMLFreeComment, XMLFreeCData, XMLFreeInstruction, XMLFreeElementClosure,
//   XMLFreeEntity e XMLFree per la cancellazione dalla memoria delle strutture
//   generate con XMLParser.
// - Aggiornata la fuznione XMLClose.
// - Aggiunta la funzione XMLDelete per eliminare una sorgente XML.
// 0.18 [09.02.2004]:
// - Aggiunta definizione di errore inclusione e di allocazione.
// - Aggiunte funzioni per la copia di entit� XML.
// 0.19 [10.02.2004]:
// - Aggiunto controllo esistenza stringhe nelle funzioni di copia entit� XML.
// 0.20 [26.02.2004]:
// - Modificato nella funzione XMLCreateEntity il controllo del parametro
//   <source>.
// 0.21 [19.03.2004]:
// - Aggiunto un nuovo errore: XML_ERROR_INVALID_NULLTERMSTRING.
// 0.22 [25.03.2004]:
// - Aggiunto il campo <temp> nella definizione della struttura XML_ENTITY.
// - Aggiunta l'inizializzaz. del campo <temp> nella funzione XMLCreateEntity.
// 0.23 [02.04.2004]:
// - Corretta la funzione XMLGets con l'aggiunta del controllo sulla sorgente.
// 1.00 [13.08.2004]:
// - Modificata la funzione XMLCreate.
// - Corretta la disallocazione nella funzione XMLFreeElement.
// - Corretta la disallocazione nella funzione XMLFreeAttribute.
// - Corretta la disallocazione nella funzione XMLFreeComment.
// - Modificata la funzione XMLFreeText.
// - Modificata la funzione XMLFreeCData.
// - Modificata la funzione XMLFreeInstruction.
// - Modificata la funzione XMLFreeElementClosure.
// - Modificata la funzione XMLFreeEntity.
// - Modificata la funzione XMLFree.
// - Modificata la funzione XMLDelete.
// - Corretta l'allocazione nella funzione XMLCreateEntity.
// - Corretta l'allocazione nella funzione XMLCreateElement.
// - Corretta l'allocazione nella funzione XMLCreateAttribute.
// - Corretta l'allocazione nella funzione XMLCreateText.
// - Corretta l'allocazione nella funzione XMLCreateComment.
// - Corretta l'allocazione nella funzione XMLCreateCData.
// - Corretta l'allocazione nella funzione XMLCreateInstruction.
// 1.01 [30.08.2004]:
// - Corretta l'allocazione dei campi <name> nella funzione XMLRead.
// 1.02 [21.01.2005]:
// - Modificata la funzione XMLFreeElement aggiungendo un try-catch.
// 1.03 [04.03.2005]:
// - Aggiunta la funzione XMLStrCpy.
// 1.04 [11.04.2005]:
// - Ottimizzazione del codice.
// 1.05 [30.08.2005]:
// - Modificata la funzione XMLFreeAttribute.
// 1.06 [01.09.2005]:
// - Modificata la funzione XMLCopyComment.
// - Modificata la funzione XMLExtract.
// 1.07 [06.09.2005]:
// - Modificata la funzione XMLCreate.
// - Modificata la funzione XMLFreeElement.
// 1.08 [07.09.2005]:
// - Modificata la funzione XMLFreeAttribute.
// - Aggiunta la funzione XMLCheck.
// 1.09 [28.06.2006]:
// - Corretta la funzione XMLGenerateHeader.
// 1.10 [20.07.2006]:
// - Aggiunta la funzione XMLSetDefaultPath.
// - Modificata la funzione XMLOpen.
// - Aggiunta XMLStrCat().
// 1.11 [21.07.2006]:
// - Aggiunta la funzione XMLGetDefaultPath.
// 1.12 [18.10.2006]:
// - Corretta XMLGenerateHeader().
// 1.13 [07.12.2006]:
// - Corretta la funzione XMLGenerateHeader().
// 1.14 [29.01.2007]:
// - Modificata la funzione XMLFree().
// - Aggiunta la funzione XMLClear().
// 1.15 [05.03.2007]:
// - Corretta lettura degli attributi con stringhe nulle in XMLExtract().
// - Migliorata la gestione dei file non conformi XML in XMLRead().
// 1.16 [07.03.2007]:
// - Corretto memory leak nella funzione XMLWrite().
// 1.17 [21.08.2007]:
// - Corretta codifica delle entita' TEXT nella funzione XMLGetTag().
// 1.18 [26.09.2007]:
// - Corretta la codifica delle chiusure elementi XML in XMLGetTag().
//==============================================================================

#ifndef XMLParserH
#define XMLParserH

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <process.h>

#define XML_SOURCE_TYPE                     __int8
#define XML_ENCODING                        __int8
#define XML_MAX_CHAR                        1024
#define XML_MAX_LINE                        4096
#define XML_MAX_LEVEL                       4096

#define XML_TAG_HEADER                      8
#define XML_TAG_ELEMENT                     1
#define XML_TAG_ELEMENT_START               2
#define XML_TAG_ELEMENT_STOP                3
#define XML_TAG_CDATA_SECTION               4
#define XML_TAG_COMMENT                     5
#define XML_TAG_PROC_INSTRUCTION            6
#define XML_NONTAG_TEXT                     9
#define XML_NONTAG_UNKNOWN                  10

#define XML_E_XTR_IDLE                      0
#define XML_E_XTR_TAG_SEARCH                1
#define XML_E_XTR_NAME_SEARCH               10
#define XML_E_XTR_NAME_SAVE                 11
#define XML_E_XTR_EOA_SEARCH                20
#define XML_E_XTR_END_SEARCH                20
#define XML_E_XTR_ATTRIB_NAME_SAVE          31
#define XML_E_XTR_ATTRIB_EQUAL_SEARCH       32
#define XML_E_XTR_ATTRIB_VALUE_SEARCH       33
#define XML_E_XTR_ATTRIB_VALUE_SAVE         34

#define XML_ERROR_BAD_CLOSURE               3
#define XML_ERROR_EXPECTING_CLOSURE         4
#define XML_ERROR_TOO_CLOSURES              5

#define XML_NO_ERROR                        0
#define XML_ERROR_INVALID_FILE              1
#define XML_ERROR_INVALID_SOURCE            -5
#define XML_ERROR_INVALID_STREAM            -6
#define XML_ERROR_INVALID_BUFFER            -7
#define XML_ERROR_INVALID_ROOT              -8
#define XML_ERROR_INVALID_ENTITY            -9
#define XML_ERROR_INVALID_HEADER            50
#define XML_ERROR_INVALID_ELEMENT           51
#define XML_ERROR_INVALID_ATTRIBUTE         52
#define XML_ERROR_INVALID_TEXT              53
#define XML_ERROR_INVALID_COMMENT           54
#define XML_ERROR_INVALID_CDATA_SECTION     55
#define XML_ERROR_INVALID_PROC_INSTRUCTION  56
#define XML_ERROR_INVALID_ELEMENT_CLOSURE   57
#define XML_ERROR_INVALID_INCLUSION         58
#define XML_ERROR_INVALID_PATH_FILE_NAME    59
#define XML_ERROR_INVALID_NULLTERMSTRING    66
#define XML_ERROR_FUNCTION_EXCEPTION		67
#define XML_ERROR_INVALID_ALLOCATION        100
#define XML_CRITICAL_ERROR                  99

#define XML_ENTITY_HEADER                   0
#define XML_ENTITY_ELEMENT                  1
#define XML_ENTITY_ATTRIBUTE                2
#define XML_ENTITY_TEXT                     3
#define XML_ENTITY_COMMENT                  4
#define XML_ENTITY_CDATA_SECTION            5
#define XML_ENTITY_PROC_INSTRUCTION         6
#define XML_ENTITY_ELEMENT_CLOSURE          7

//==============================================================================
/// Struttura di definizione dell'attributo XML.
/// Questa struttura definisce le propriet� di un attributo inteso come entit�
/// secondo le specifiche XML.
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct XML_ATTRIBUTE{
  int                     pos               ; ///< Posizione dell'oggetto XML
  char                  * name              ; ///< Nome dell'attributo XML
  char                  * type              ; ///< Tipo di attributo XML
  char                  * value             ; ///< Valore dell'attributo XML
  void                  * parent            ; ///< Puntatore dell'elemento XML parent
  int                     length            ; ///< Lunghezza del valore
  XML_ATTRIBUTE         * prev              ; ///< Puntatore a precedente
  XML_ATTRIBUTE         * next              ; ///< Puntatore a sucessivo
  void                  * tag               ; ///< Puntatore di servizio
} XML_ATTRIBUTE;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di definizione del commento XML.
/// Questa struttura definisce le propriet� di un commento inteso come entit�
/// secondo le specifiche XML.
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct XML_COMMENT{
  int                     pos               ; ///< Posizione dell'oggetto XML
  char                  * value             ; ///< Stringa del commento XML
  void                  * parent            ; ///< Puntatore all'elemento XML parent
  int                     length            ; ///< Lunghezza della stringa commento
  XML_COMMENT           * prev              ; ///< Puntatore a precedente
  XML_COMMENT           * next              ; ///< Puntatore a sucessivo
  void                  * tag               ; ///< Puntatore di servizio
} XML_COMMENT;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di definizione del testo XML.
/// Questa struttura definisce le propriet� di un testo inteso come entit�
/// secondo le specifiche XML.
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct XML_TEXT{
  int                     pos               ; ///< Posizione dell'oggetto XML
  char                  * value             ; ///< Stringa del testo XML
  void                  * parent            ; ///< Puntatore all'elemento XML parent
  int                     length            ; ///< Lunghezza della stringa testo
  XML_TEXT              * prev              ; ///< Puntatore a precedente
  XML_TEXT              * next              ; ///< Puntatore a sucessivo
  void                  * tag               ; ///< Puntatore di servizio
} XML_TEXT;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di definizione di sezione CDATA XML.
/// Questa struttura definisce le propriet� di una sezione CDATA intesa come
/// entit� secondo le specifiche XML.
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct XML_CDATA_SECTION{
  int                     pos               ; ///< Posizione dell'oggetto XML
  char                  * value             ; ///< Valore della sezione CDATA XML
  void                  * parent            ; ///< Puntatore all'elemento XML parent
  int                     length            ; ///< Lunghezza della sezione CDATA XML
  XML_CDATA_SECTION     * prev              ; ///< Puntatore a precedente
  XML_CDATA_SECTION     * next              ; ///< Puntatore a sucessivo
  void                  * tag               ; ///< Puntatore di servizio
} XML_CDATA_SECTION;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di definizione di una istruzione XML.
/// Questa struttura definisce le propriet� di una istruzione intesa come
/// entit� secondo le specifiche XML.
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct XML_PROC_INSTRUCTION{
  int                     pos               ; ///< Posizione dell'oggetto XML
  char                  * value             ; ///< Stringa dell'istruzione XML
  int                     length            ; ///< Lunghezza dell'istruzione
  void                  * parent            ; ///< Puntatore all'elemento XML parent
  XML_PROC_INSTRUCTION  * prev              ; ///< Puntatore a precedente
  XML_PROC_INSTRUCTION  * next              ; ///< Puntatore a sucessivo
  void                  * tag               ; ///< Puntatore di servizio
} XML_PROC_INSTRUCTION;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di definizione elementi XML.
/// Definisce i membri e i tipi che costituiscono un elemento secondo di file
/// in formato XML.
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct XML_ELEMENT{
  int                     pos               ; ///< Posizione dell'oggetto XML
  char                  * name              ; ///< Nome dell'elemento XML
  XML_ATTRIBUTE         * attrib            ; ///< Puntatore alla lista di attributi XML
  XML_CDATA_SECTION     * cdata             ; ///< Puntatore alla lista di sezioni CDATA XML
  XML_TEXT              * text              ; ///< Puntatore al testo XML
  XML_COMMENT           * comment           ; ///< Puntatore alla lista di commenti XML
  XML_PROC_INSTRUCTION  * instruction       ; ///< Puntatore alla lista di istruzioni XML
  XML_ELEMENT           * child             ; ///< Puntatore alla lista di elementi XML child
  XML_ELEMENT           * parent            ; ///< Puntatore all'elemento XML parent
  XML_ELEMENT           * prev              ; ///< Puntatore a precedente
  XML_ELEMENT           * next              ; ///< Puntatore a sucessivo
  int                     attrib_count      ; ///< Numero di attributi XML
  int                     text_count        ; ///< Numero di sezioni di testo XML
  int                     cdata_count       ; ///< Numero di sezioni CDATA XML
  int                     comment_count     ; ///< Numero di commenti XML
  int                     instruction_count ; ///< Numero di istruzioni XML
  int                     child_count       ; ///< Numero di elementi XML child
  void                  * tag               ; ///< Puntatore di servizio
} XML_ELEMENT;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di entit� XML.
/// Definisce il tipo di un'entit� XML e contiene il puntatore alla struttura
/// specifica che raccoglie tutte le sue informazioni.
/// Il Flag di entit� temporanea <temp> indica che la struttura pu� essere
/// cancellata perch� non fa parte di un albero XML ma � stata generata da
/// funzione.
///
/// \date [25.03.2004]
/// \author Enrico Alborali.
/// \version 0.05
//------------------------------------------------------------------------------
typedef struct XML_ENTITY{
  int                 pos       ; ///< Posizione dell'entit� XML
  int                 level     ; ///< Livello dell'albero XML
  int                 type      ; ///< Tipo di entit� XML
  bool                temp      ; ///< Flag di entit� temporanea
  void              * structure ; ///< Puntatore alla struttura dell'entit� XML
  XML_ENTITY        * prev      ; ///< Puntatore alla precedente
  XML_ENTITY        * next      ; ///< Puntatore alla successiva
} XML_ENTITY;
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura di definizione sorgente XML.
/// Definisce i membri e i tipi che costituiscono un file sorgente in formato
/// XML.
/// \date [29.01.2004]
/// \author Enrico Alborali.
/// \version 0.04
//------------------------------------------------------------------------------
typedef struct XML_SOURCE{
  char                  * name              ; ///< Nome della sorgente XML
  FILE                  * file              ; ///< Punatatore al file XML
  XML_SOURCE_TYPE         type              ; ///< Tipo di sorgente XML
  bool                    well_formed       ; ///< Flag Well-Formed
  XML_ELEMENT           * root_header       ; ///< Puntatore all'header root
  XML_ELEMENT           * root_element      ; ///< Puntatore all'elemento root
  XML_TEXT              * root_text         ; ///< Puntatore al testo root
  XML_COMMENT           * root_comment      ; ///< Puntatore al commento root
  XML_CDATA_SECTION     * root_cdata        ; ///< Puntatore al cdata root
  XML_PROC_INSTRUCTION  * root_instruction  ; ///< Puntatore all'istruzione
  XML_ENTITY            * first_entity      ; ///< Puntatore alla prima entit�
  char *                  version           ; ///< Puntatore a versione XML
  int                     version_major     ; ///< Versione XML (major)
  int                     version_minor     ; ///< Versione XML (minor)
  bool                    stand_alone       ; ///< Flag di XML Stand Alone
  char *                  encoding          ; ///< Tipo di Encoding XML
} XML_SOURCE;
//------------------------------------------------------------------------------

int                     XMLOpen               (XML_SOURCE * source, XML_ELEMENT * root, char mode);
int                     XMLClose              (XML_SOURCE * source);
char                  * XMLGets               (XML_SOURCE * source, int size);
int                     XMLPuts               (XML_SOURCE * source, char * buffer);
char                  * XMLChop               (char * line);
char                  * XMLTrim               (char * line, int size);
bool                    XMLTokCmp             (char * line, char * token, int offset);
bool                    XMLFilled             (char * line, char c);
int                     XMLRead               (XML_SOURCE * source, XML_ELEMENT * root);
bool                    XMLNoChildElement     (XML_ELEMENT * element);
char                  * XMLGenerateHeader     (XML_SOURCE * source);
char                  * XMLGenerateTag        (XML_ELEMENT * element);
char                  * XMLGenerateClosureTag (XML_ELEMENT * element);
char                  * XMLGetTag             (XML_ENTITY * entity);
int                     XMLWrite              (XML_SOURCE * source);
int                     XMLFreeElement        (XML_ELEMENT * xELEMENT);
int                     XMLFreeAttribute      (XML_ATTRIBUTE * xATTRIB);
int                     XMLFreeText           (XML_TEXT * xTEXT);
int                     XMLFreeComment        (XML_COMMENT * xCOMMENT);
int                     XMLFreeCData          (XML_CDATA_SECTION * xCDATA);
int                     XMLFreeInstruction    (XML_PROC_INSTRUCTION * xINSTR);
int                     XMLFreeElementClosure (char * xCLOSURE);
int                     XMLFreeEntity         (XML_ENTITY * xENTITY);
int                     XMLFree               (XML_SOURCE * source);
int                     XMLDelete             (XML_SOURCE * source);
// Funzione per cancellare la struttura XML dalla memoria mantenendo source
int											XMLClear							(XML_SOURCE * source );
XML_SOURCE            * XMLCreate             (char * name);
XML_ENTITY            * XMLCreateEntity       (XML_SOURCE * source,XML_ENTITY * prev,void * structure,int type,int pos);
XML_ELEMENT           * XMLCreateElement      (char * name, XML_ELEMENT * parent, XML_ELEMENT * prev);
XML_ATTRIBUTE         * XMLCreateAttribute    (char * name, XML_ELEMENT * parent, XML_ATTRIBUTE * prev);
XML_TEXT              * XMLCreateText         (XML_ELEMENT * parent, XML_TEXT * prev);
XML_COMMENT           * XMLCreateComment      (XML_ELEMENT * parent, XML_COMMENT * prev);
XML_CDATA_SECTION     * XMLCreateCData        (XML_ELEMENT * parent, XML_CDATA_SECTION * prev);
XML_PROC_INSTRUCTION  * XMLCreateInstruction  (XML_ELEMENT * parent, XML_PROC_INSTRUCTION * prev);
XML_SOURCE            * XMLCopy               (XML_SOURCE           * source  );
XML_ENTITY            * XMLCopyEntity         (XML_ENTITY           * entity  );
XML_ELEMENT           * XMLCopyElement        (XML_ELEMENT          * element );
XML_ATTRIBUTE         * XMLCopyAttribute      (XML_ATTRIBUTE        * attrib  );
XML_TEXT              * XMLCopyText           (XML_TEXT             * text    );
XML_COMMENT           * XMLCopyComment        (XML_COMMENT          * comment );
XML_CDATA_SECTION     * XMLCopyCData          (XML_CDATA_SECTION    * cdata   );
XML_PROC_INSTRUCTION  * XMLCopyInstruction    (XML_PROC_INSTRUCTION * instr   );
int                     XMLExtract            (XML_ELEMENT * xmle, char * line);
int                     XMLElementLink        (XML_ELEMENT * first, XML_ELEMENT * second);
int                     XMLElementUpLink      (XML_ELEMENT * parent, XML_ELEMENT * child);
int                     XMLAttributeLink      (XML_ATTRIBUTE * first, XML_ATTRIBUTE * second);
int                     XMLAttributeUpLink    (XML_ELEMENT * element, XML_ATTRIBUTE * attrib);
int                     XMLTextLink           (XML_TEXT * first, XML_TEXT * second);
int                     XMLTextUpLink         (XML_ELEMENT * element, XML_TEXT * object);
int                     XMLCommentLink        (XML_COMMENT * first, XML_COMMENT * second);
int                     XMLCommentUpLink      (XML_ELEMENT * element, XML_COMMENT * object);
int                     XMLCDataLink          (XML_CDATA_SECTION * first, XML_CDATA_SECTION * second);
int                     XMLCDataUpLink        (XML_ELEMENT * element, XML_CDATA_SECTION * object);
int                     XMLInstructionLink    (XML_PROC_INSTRUCTION * first, XML_PROC_INSTRUCTION * second);
int                     XMLInstructionUpLink  (XML_ELEMENT * element, XML_PROC_INSTRUCTION * object);
int                     XMLCmpEntity          (const void * xE1, const void * xE2);
// Funzione per copiare stringhe
char                  * XMLStrCpy             ( char * dest, char * src );
// Funzione per concatenare stringhe
char                  * XMLStrCat             ( char * dest, char * src );
// Funzione per settare il percorso di default per i file XML
int                     XMLSetDefaultPath     ( char * path );
// Funzione per ottenere il ptr al percorso di default per i file XML
char                  * XMLGetDefaultPath     ( void );
// Funzione per controllare l'integrit� di una struttura XML
int                     XMLCheck              ( XML_SOURCE * source );

#endif

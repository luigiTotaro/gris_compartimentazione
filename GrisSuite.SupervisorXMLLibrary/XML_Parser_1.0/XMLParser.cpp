//==============================================================================
// Telefin XML Parser 1.0
//------------------------------------------------------------------------------
// LIBRERIA ACCESSO A FILE FORMATO XML (XMLParser.cpp)
// Libreria di funzioni per l'accesso alle informazioni strutturate contenute
// all'interno di file in formato XML.
// Questa libreria supporta i file in formato standard per la creazione di
// strutture generiche ad albero. Supporta inoltre specifici elementi definiti
// da Telefin.
//
// Versione:	1.19 (04.12.2003 -> 22.08.2012)
//
// Copyright:	2003-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Riccardo Venturini (riccardo.venturini@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:		richiede XMLParser.h,math.h
//------------------------------------------------------------------------------
// Version history: vedere XMLParser.h
//==============================================================================
#pragma hdrstop
#include "math.h"
#include "XMLParser.h"

//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#pragma package(smart_init)
#endif

static char * XMLSourceDefaultPath = NULL;

//==============================================================================
/// Funzione per l'apertura di sorgenti XML.
/// Apre una sorgente XML specificata dal puntatore <source> generando una
/// struttuura di elementi XML a partire dall'elemento puntato da <root>.
/// Specificando la modalit� di apertura � possibile definire se la sorgente
/// deve essere aperta per la lettura o per la scrittura. Passare uno dei
/// seguenti valori nel campo <mode>:
/// 'r' : apertura in lettura
/// 'w' : apertura in scrittura
///
/// \date [20.07.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLOpen( XML_SOURCE * source, XML_ELEMENT * root, char mode )
{
	int     ret_code    = 0   ;
	FILE  * source_file       ;
	char    open_mode[4]      ;
	char  * full_path   = NULL;

	open_mode[0] = 'r';
	open_mode[1] = 't';
	open_mode[2] = '\0';
	if ( mode == 'w' )
	{
		open_mode[0] = 'w';
		open_mode[1] = '\0';
	}
	else if ( mode == 'W' )
	{
		open_mode[0] = 'w';
		open_mode[1] = '+';
		open_mode[2] = '\0';
	}

	if ( source != NULL )
	{
		if ( XMLSourceDefaultPath )
		{
			full_path = XMLStrCat( NULL, XMLSourceDefaultPath );
			full_path = XMLStrCat( full_path, source->name );
		}
		else
		{
			full_path = XMLStrCpy( NULL, source->name );
		}
		if ( full_path != NULL )
		{
			if ( ( source_file = fopen( full_path, open_mode ) ) == NULL )
			{
				ret_code      = XML_ERROR_INVALID_FILE;
				source->file  = NULL;
			}
			else
			{
				source->file = source_file;
			}
			free ( full_path );
		}
		else
		{
			source->file  = NULL;
			ret_code = XML_ERROR_INVALID_PATH_FILE_NAME;
		}
	}
	else
	{
		ret_code = XML_ERROR_INVALID_SOURCE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per la chiusura di sorgenti XML.
/// Chiude una sorgente XML specificata dal puntatore <source>.
/// La struttura degli elementi di questa sorgente eventualmente aperti in
/// precedenza non viene eliminata.
/// \date [30.12.2003]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLClose( XML_SOURCE * source )
{
	int ret_code = 0;

	if ( source->file == NULL )
	{
		ret_code = XML_ERROR_INVALID_FILE;
	}
	else
	{
		fclose(source->file);
		source->file = NULL;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per la creazione di sorgenti XML.
/// Crea la struttura di una nuova sorgente XML impostando il nome fornito dal
/// campo <name> e tutte le altre informazioni di default.
///
/// \date [06.09.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
XML_SOURCE * XMLCreate( char * name )
{
	XML_SOURCE  * source  = NULL;
	char        * version = NULL;

	// Alloco la memoria per contenere la nuova struttura sorgente XML
	source = (XML_SOURCE*) malloc( sizeof(XML_SOURCE) );

	if ( source != NULL ) // Controllo se la struttura � stata allocata con successo
	{
		source->name              = XMLStrCpy( NULL, name );
		source->file              = NULL    ;
		source->type              = 'f'     ;
		source->well_formed       = false   ;
		source->root_header       = NULL    ;
		source->root_element      = NULL    ;
		source->root_text         = NULL    ;
		source->root_comment      = NULL    ;
		source->root_cdata        = NULL    ;
		source->root_instruction  = NULL    ;
		source->first_entity      = NULL    ;
		source->version           = NULL    ;
		source->version_major     = 1       ;
		source->version_minor     = 0       ;
		source->stand_alone       = false   ;
		source->encoding          = NULL    ;
	}

	return source;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di elementi XML.
/// Data una struttura <xELEMENT> di un elemento XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Elimina inoltre la lista di eventuali strutture XML_ATTRIBUTE linkate.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [06.09.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int XMLFreeElement( XML_ELEMENT * xELEMENT )
{
	int             ret_code      = 0 ; // Codice di ritorno
	XML_ATTRIBUTE * xATTRIB           ; // Ptr di appoggio per strutture attributo XML
	XML_ATTRIBUTE * xATTRIB_NEXT      ; // Ptr di appoggio per strutture attributo XML

	if ( xELEMENT != NULL ) // Controllo la validit� del ptr a elemento XML
	{
		if ( xELEMENT->attrib != NULL ) // Controllo se ci sono degli attributi
		{
			xATTRIB = xELEMENT->attrib;
			while( xATTRIB != NULL ) // Ciclo sugli attributi dell'elemento
			{
				try
				{
					xATTRIB_NEXT = xATTRIB->next;
				}
				catch(...)
				{

				}
				// Cancello l'attributo
				XMLFreeAttribute( xATTRIB );
				xATTRIB = xATTRIB_NEXT;
			}
		}
		// Cancello l'elemento
		if ( xELEMENT->name != NULL )
		{
			try
			{
				free( xELEMENT->name );
			}
			catch(...)
			{
			}
		}
		free( xELEMENT );
		xELEMENT = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_ELEMENT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di attributi XML.
/// Data una struttura <xATTRIB> di un'attributo XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [07.09.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int XMLFreeAttribute( XML_ATTRIBUTE * xATTRIB )
{
	int     ret_code  = 0   ; // Codice di ritorno

	if ( xATTRIB != NULL ) // Controllo la validit� del ptr ad attributo XML
	{
		// --- Cancello il nome dell'attributo ---
		if ( xATTRIB->name != NULL )
		{
			try
			{
				free( (void*)xATTRIB->name );
			}
			catch(...)
			{
				ret_code = XML_CRITICAL_ERROR;
			}
		}
		// --- Cancello il tipo di attributo ---
		if ( xATTRIB->type != NULL )
		{
			try
			{
				free( xATTRIB->type );
			}
			catch(...)
			{
				ret_code = XML_CRITICAL_ERROR;
			}
		}
		// --- Cancello il valore dell'attributo ---
		if( xATTRIB->value != NULL )
		{
			try
			{
				free( xATTRIB->value );
			}
			catch(...)
			{
				ret_code = XML_CRITICAL_ERROR;
			}
		}
		// --- Cancello l'attributo ---
		try
		{
			memset( xATTRIB, 0, sizeof(XML_ATTRIBUTE) );
			free( xATTRIB );
		}
		catch(...)
		{
			ret_code = XML_CRITICAL_ERROR;
		}
		xATTRIB = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_ATTRIBUTE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di testi XML.
/// Data una struttura <xTEXT> di un testo XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLFreeText( XML_TEXT * xTEXT )
{
	int ret_code = 0; // Codice di ritorno

	if ( xTEXT != NULL ) // Controllo la validit� del ptr a testo XML
	{
		// Cancello il testo XML
		if ( xTEXT->value != NULL ) free( xTEXT->value );
		memset( xTEXT, 0, sizeof(XML_TEXT) );
		free( xTEXT );
		xTEXT = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_TEXT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di commenti XML.
/// Data una struttura <xCOMMENT> di un commento XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLFreeComment( XML_COMMENT * xCOMMENT )
{
	int ret_code = 0; // Codice di ritorno

	if ( xCOMMENT != NULL )
	{
		// Cancello il testo XML
		if ( xCOMMENT->value != NULL ) free( xCOMMENT->value );
		free( xCOMMENT );
		xCOMMENT = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_COMMENT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di sezioni dati XML.
/// Data una struttura <xCDATA> di una sez. dati XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLFreeCData( XML_CDATA_SECTION * xCDATA )
{
	int ret_code = 0; // Codice di ritorno

	if ( xCDATA != NULL ) // Controllo la validit� del ptr a sezione dati XML
	{
		// Cancello il testo XML
		if ( xCDATA->value != NULL ) free( xCDATA->value );
		free( xCDATA );
		xCDATA = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_CDATA_SECTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di istruzioni XML.
/// Data una struttura <xINSTR> di un'istruzione XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLFreeInstruction( XML_PROC_INSTRUCTION * xINSTR )
{
	int ret_code = 0; // Codice di ritorno

	if ( xINSTR != NULL ) // Controllo la validit� del ptr a istruzione XML
	{
		// Cancello il testo XML
		if ( xINSTR->value != NULL ) free( xINSTR->value );
		free( xINSTR );
		xINSTR = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_PROC_INSTRUCTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di chiusure elementi XML.
/// Data una struttura <xCLOSURE> di una chiusura XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLFreeElementClosure( char * xCLOSURE )
{
	int ret_code = 0; // Codice di ritorno

	if ( xCLOSURE != NULL ) // Controllo la validit� del ptr a chiusura elemento XML
	{
		// Cancello il testo XML
		free( xCLOSURE );
		xCLOSURE = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_ELEMENT_CLOSURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di entit� XML.
/// Data una struttura <xENTITY> di un'entit� XML effettua la cancellazione
/// della memoria occupata dalla stessa.
/// Elimina inoltre la memoria occupata dalla struttura <structure> che �
/// eventualmente dall'entit� attraverso la specifica funsione di cancellazione.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLFreeEntity( XML_ENTITY * xENTITY )
{
	int ret_code = 0; // Codice di ritorno

	if ( xENTITY != NULL ) // Verifico la validit� del ptr a entit� XML
	{
		switch ( xENTITY->type )
		{
			case XML_ENTITY_HEADER:
				ret_code = XMLFreeElement( (XML_ELEMENT*) xENTITY->structure );
			break;
			case XML_ENTITY_ELEMENT:
				ret_code = XMLFreeElement( (XML_ELEMENT*) xENTITY->structure );
			break;
			case XML_ENTITY_ATTRIBUTE:
				ret_code = XMLFreeAttribute( (XML_ATTRIBUTE*) xENTITY->structure );
			break;
			case XML_ENTITY_TEXT:
				ret_code = XMLFreeText( (XML_TEXT*) xENTITY->structure );
			break;
			case XML_ENTITY_COMMENT:
				ret_code = XMLFreeComment( (XML_COMMENT*) xENTITY->structure );
			break;
			case XML_ENTITY_CDATA_SECTION:
				ret_code = XMLFreeCData( (XML_CDATA_SECTION*) xENTITY->structure );
			break;
			case XML_ENTITY_PROC_INSTRUCTION:
				ret_code = XMLFreeInstruction( (XML_PROC_INSTRUCTION*) xENTITY->structure );
			break;
			case XML_ENTITY_ELEMENT_CLOSURE:
				ret_code = XMLFreeElementClosure( (char*) xENTITY->structure );
			break;
			default:
				ret_code = XML_ERROR_INVALID_ENTITY;
		}
		// Cancello l'entit�
		free( xENTITY );
		xENTITY = NULL;
	}
	else
	{
		ret_code = XML_ERROR_INVALID_ENTITY;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione (dalla memoria) di sorgenti XML.
/// Data una struttura <source> di una sorgente XML effettua la cancellazione
/// della memoria anche per tutte le sottostrutture linkate.
/// Restituisce un codice diverso da 0 in caso di errore.
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int XMLFree( XML_SOURCE * source )
{
	int ret_code                      = 0   ; // Codice di ritorno
	void                  * xNEXT     = NULL; // Ptr di appoggio
	XML_ELEMENT           * xHEADER   = NULL; // Ptr per intestazioni XML
	XML_ENTITY            * xENTITY   = NULL; // Ptr per entit� XML

	if ( source != NULL ) // Controllo il ptr a sorgente
	{
		// --- Cancello la struttura XML ---
		ret_code = XMLClear( source );
		// --- Cancello la sorgente ---
		try
		{
			if ( source->file      != NULL ) XMLClose( source );
			if ( source->name      != NULL ) free( source->name );
			memset( source, 0, sizeof( XML_SOURCE ) );
			free( source );
		}
		catch(...)
		{
			ret_code = XML_ERROR_FUNCTION_EXCEPTION;
		}
	}
	else
	{
		ret_code = XML_ERROR_INVALID_SOURCE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'eliminazione di sorgenti XML.
/// Data una struttura <source> di una sorgente XML effettua la cancellazione
/// fisica della stessa. Mantiene in memoria al struttura della sorgente, che
/// pu� essere utilizzata successivamente oppure eliminata con XMLFree.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLDelete( XML_SOURCE * source )
{
	int ret_code = 0; // Codice di ritorno

	if ( source != NULL )
	{
		if ( source->file != NULL )
		{
			ret_code = XMLClose( source );
		}
		else
		{
			ret_code = XML_ERROR_INVALID_FILE;
		}
		if ( source->name != NULL )
		{
			ret_code = _unlink( source->name );
			source->file = NULL;
		}
		else
		{
			ret_code = XML_ERROR_INVALID_FILE;
		}
	}
	else
	{
		ret_code = XML_ERROR_INVALID_SOURCE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per cancellare la struttura XML dalla memoria mantenendo source
/// Simile a XMLFree ma lascia intetta source e i suoi parametri.
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	XMLClear( XML_SOURCE * source )
{
	int 					ret_code 	= XML_NO_ERROR;
	void        * xNEXT     = NULL; // Ptr di appoggio
	XML_ELEMENT * xHEADER   = NULL; // Ptr per intestazioni XML
	XML_ENTITY  * xENTITY   = NULL; // Ptr per entit� XML

	try
	{
		if ( source != NULL )
		{
			// --- Controllo la lista header ---
			if ( source->root_header != NULL )
			{
				xHEADER = source->root_header; // Prendo il primo header
				while( xHEADER != NULL ) // Ciclo sulle intestazioni
				{
					xNEXT = (void*) xHEADER->next;
					ret_code = XMLFreeElement( xHEADER );
					xHEADER = (XML_ELEMENT*) xNEXT; // Vado all'header successivo (se esiste)
				}
			}
			source->root_header = NULL;
			// --- Controllo la lista entita' ---
			if ( source->first_entity != NULL )
			{
				xENTITY = source->first_entity;
				while( xENTITY != NULL ) // Ciclo sulle entita'
				{
					xNEXT = (void*) xENTITY->next;
					ret_code = XMLFreeEntity( xENTITY );
					xENTITY = (XML_ENTITY*) xNEXT; // Vado all'entita' successiva
				}
			}
			source->first_entity = NULL;
			if ( source->version   != NULL ) free( source->version ); source->version = NULL;
			if ( source->encoding  != NULL ) free( source->encoding ); source->encoding = NULL;
		}
		else
		{
			ret_code = XML_ERROR_INVALID_SOURCE;
		}
	}
	catch(...)
	{
		ret_code = XML_ERROR_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per la lettura da sorgenti XML.
/// Legge gli elementi di una sorgente XML specificata dal puntatore <source>.
/// Genera anche una lista di entit� XML utilizzabili per una lettura
/// sequenziale della struttura XML globale.
/// Questa lista non comprende le entit� di tipo Header e Attribute ma include
/// le entit� Element_Closure che non contengono dati ma sono utili per la
/// ricostruzione della struttura originale della sorgente XML.
///
/// \date [05.03.2007]
/// \author Enrico Alborali, Riccardo Venturini
/// \version 0.08
//------------------------------------------------------------------------------
int XMLRead( XML_SOURCE * source, XML_ELEMENT * root )
{
	int           ret_code = 0          ;
	int           LineSize              ; // Lunghezza della singola linea
	int           TextSize              ; // Lunghezza del testo XML (totale)
	char        * XMLText               ; // Testo XML
	int           TextOffset            ; // Offset della scrittura nel testo XML
	int           TagCount              ; // Numero di Tag presenti nel testo XML
	void       ** TagList               ; // Lista dei Tag XML
	int         * TagType               ; // Lista dei tipi di Tag (vedi define)
	int         * TagUsed               ; // Lista dei flag di Tag usato
	char        * TagLine               ; // Singola linea di Tag XML
	bool          TagStart              ; // Flag di inizio Tag XML
	int           TagSize               ; // Dimensione del Tag XML
	int           TagOffset             ; // Offset del Tag XML
	int           TagMax                ;
	char        * LineList[XML_MAX_LINE]; // Lista delle righe del file di esportazione
	char        * Line                  ; // Riga del file di esportazione
	int           LineNum               ; // Numero di righe lette
//	char       ** LineToken             ; // Token della riga del file di esportazione
//	int           LineTokenNum          ; // Numero di token della riga del file di esportazione
//	char       ** TokenFormato          ; // Token dell'header dei campi
//	int           TokenFormatoNum       ; // Numero di token dell'header dei campi
	bool          TagCData              ;
	bool          TagComment            ;

	LineNum = 0; // Azzero il contatore delle righe
	TextSize = 0; // Azzero la lunghezza del testo XML (totale)

	Line = (char *) 1;

	// Lettura delle singole righe di testo XML
	while ( Line != NULL )
	{
		Line = XMLGets( source, XML_MAX_CHAR );
		char * TrimmedLine = XMLTrim( Line, XML_MAX_CHAR );
		free( Line );
		Line = TrimmedLine;
		TrimmedLine = NULL;
		if ( Line != NULL )
		{
			LineSize = strlen( Line );  // Calcolo la lunghezza della singola linea
			TextSize += LineSize;     // e aggiungo il valore alla lunghezza del testo
			LineList[LineNum] = Line;
			LineNum++; // Incremento il contatore delle righe
		}
		else
		{
			free( Line );
		}
	}

	// Tutte le linee XML vengono raggruppate in un unico array di caratteri
	XMLText = (char *) malloc( TextSize+1 ); // Alloco la memoria per il testo
	TextOffset = 0;

	for ( int l = 0; l < LineNum; l++ )
	{
		Line = LineList[l];
		LineSize = strlen( Line ); // Recupero la lunghezza della riga
		for ( int c = 0; c < LineSize; c++ )
		{
			XMLText[TextOffset + c] = Line[c];
		}
		TextOffset += LineSize;
		free( Line ); // Libero la memoria della riga l-esima
	}
	XMLText[TextSize] = '\0';

	// Conto il numero di Tag presenti nel testo XML
	TagCount = 0; // Resetto il contatore Tag
	for ( int c = 0; c < TextSize; c++ )
	{
		if ( XMLText[c] == '<' ) TagCount++;
	}

	// Recupero la lista dei Tag
	TextOffset = 0;
	TagList = (void **) malloc(sizeof(void *)*((TagCount*2)+1));
	TagType = (int *) malloc(sizeof(int)*((TagCount*2)+1));
	TagUsed = (int *) malloc(sizeof(int)*((TagCount*2)+1));

	for ( int i = 0; i < (TagCount*2)+1; i++ )
	{
		TagList[i] = NULL;
		TagType[i] = 0;
		TagUsed[i] = 0;
	}

	for ( int t = 0; t < (TagCount*2)+1; t++ )
	{
		TagStart = false;
		TagSize = 0;
		TagMax = t;
		for ( int c = 0; c < (TextSize - TextOffset); c++ )
		{
			if (TagStart)
			{
				// Il Tag XML � gi� iniziato
				TagSize++;

        if (XMLText[TextOffset+c] == '>')
        {
					// Verifica dei casi particolari
          if (TagCData)
          {
						if (XMLTokCmp(XMLText,"]]>",(TextOffset+c-2)))
            {
			  // Ho trovato la fine del Tag CData XML
              TagStart = false;
            }
          }
          else
          {
            if (TagComment)
            {
              if (XMLTokCmp(XMLText,"-->",(TextOffset+c-2)))
              {
                // Ho trovato la fine del Tag Commento XML
                TagStart = false;
              }
            }
            else
            {
              // Ho trovato la fine del Tag Generico XML
              TagStart = false;
            }
          }

          if (!TagStart) // Se ho trovato la fine del Tag
          {
            TagLine = (char *) malloc(TagSize+1);
            for (int i=0; i<TagSize; i++)
            {
              TagLine[i] = XMLText[TextOffset+TagOffset+i];
            }
            TagLine[TagSize] = '\0';
            TagList[t] = (void *) TagLine;
						TagUsed[t] = 0;
            TagType[t] = 0;
            /*
						if (TagLine[1]!='/' && TagLine[1]!='?' && TagLine[1]!='!' && TagLine[TagSize-2]!='-') TagType[t] = XML_TAG_ELEMENT_START;
            if (TagLine[1]=='/' && TagLine[1]!='?' && TagLine[1]!='!' && TagLine[TagSize-2]!='-') TagType[t] = XML_TAG_ELEMENT_STOP;
            if (TagLine[1]!='/' && TagLine[1]!='?' && TagLine[1]!='!' && TagLine[TagSize-2]=='/') TagType[t] = XML_TAG_ELEMENT;
            //*/
            if (TagLine[1]!='/' && TagLine[1]!='!' && TagLine[TagSize-2]!='-') TagType[t] = XML_TAG_ELEMENT_START;
            if (TagLine[1]=='/' && TagLine[1]!='!' && TagLine[TagSize-2]!='-') TagType[t] = XML_TAG_ELEMENT_STOP;
            if (TagLine[1]!='/' && TagLine[1]!='!' && TagLine[TagSize-2]=='/') TagType[t] = XML_TAG_ELEMENT;
            if (TagLine[1]!='/' && TagLine[1]=='!' && TagLine[2]=='-' && TagLine[3]=='-') TagType[t] = XML_TAG_COMMENT;
            if (TagLine[1]!='/' && TagLine[1]=='?' && TagLine[2]=='x' && TagLine[3]=='m' && TagLine[4]=='l' && TagLine[TagSize-2]=='?') TagType[t] = XML_TAG_HEADER;
            if (TagLine[1]!='/' && TagLine[1]=='?' && !(TagLine[2]=='x' && TagLine[3]=='m' && TagLine[4]=='l') && TagLine[TagSize-2]=='?') TagType[t] = XML_TAG_PROC_INSTRUCTION;
            if (TagLine[1]=='!' && TagLine[2]=='[' && TagLine[3]=='C' && TagLine[4]=='D' && TagLine[5]=='A'&& TagLine[6]=='T'&& TagLine[7]=='A'&& TagLine[8]=='[' && TagLine[TagSize-3]==']' && TagLine[TagSize-2]==']') TagType[t] = XML_TAG_CDATA_SECTION;
            //if (TagLine[1]!='/' && TagLine[1]=='!' && TagLine[2]=='-' && TagLine[3]=='-'&& TagLine[TagSize-3]=='-' && TagLine[TagSize-2]=='-') TagType[t] = XML_TAG_COMMENT;
            TextOffset += TagOffset+TagSize; // Aggiorno l'offset
            c = (TextSize - TextOffset);
          }
        }
      }
      else
      {
        // Cerco l'inizio del Tag XML
        if (XMLText[TextOffset+c] == '<')
        {
          // Salvo un eventuale Testo NON-Tag
          if (c)
          {
            TagLine = (char *) malloc(c+1);
            for (int i=0; i<c; i++)
            {
              TagLine[i] = XMLText[TextOffset+i];
            }
            TagLine[c] = '\0';
            // Controllo che non sia una serie di spazi
						if (XMLFilled(TagLine,' '))
			{
              free(TagLine); // Libero la memori perch� scarto la riga
						}
            else
            {
              TagList[t] = (void *) TagLine;
              TagUsed[t] = 0;
              TagType[t] = XML_NONTAG_TEXT;
              t++;
            }
          }

          // Verifica dei casi particolari (Commenti e CData)
          if (XMLTokCmp(XMLText,"<![CDATA[",TextOffset+c))
          {
            TagCData = true;
          }
          else
          {
            TagCData = false;
            if (XMLTokCmp(XMLText,"<!--",TextOffset+c))
            {
              TagComment = true;
            }
            else
            {
              TagComment = false;
            }
          }

          // Ho trovato l'inizio del Tag XML
          TagStart = true;
          TagOffset = c;
		  TagSize++;
				}
        else
        {
					if (TextOffset+c == (TextSize-1))
          {
            // Salvo un eventuale Testo NON-Tag
            if (c)
            {
              TagLine = (char *) malloc(c+1);
              for (int i=0; i<c; i++)
              {
                TagLine[i] = XMLText[TextOffset+i];
              }
              TagLine[c] = '\0';
              // Controllo che non sia una serie di spazi
              if (XMLFilled(TagLine,' '))
              {
                free(TagLine); // Libero la memori perch� scarto la riga
              }
              else
              {
                TagList[t] = (void *) TagLine;
                TagUsed[t] = 0;
                TagType[t] = XML_NONTAG_TEXT;
                t++;
              }
              TextOffset += c;
            }
          }
        }
      }
    }
  }
	// --- BUGFIX by Ric 05.03.2007 ---
	if ( TagMax < 1 )
	{
			free(XMLText      );
			free(TagList      );
			free(TagType      );
			free(TagUsed      );
			source->well_formed = false;
			return XML_NO_ERROR;
	}
	// ---
	// Ricerca degli elementi XML

  // Dichiarazione dei puntatori a strutture delle entit� XML
  XML_TEXT              * xTEXT     = NULL;
  XML_COMMENT           * xCOMMENT  = NULL;
  XML_CDATA_SECTION     * xCDATA    = NULL;
  XML_PROC_INSTRUCTION  * xINSTR    = NULL;
  XML_ELEMENT           * xELEMENT  = NULL;
  XML_ELEMENT           * xHEADER   = NULL;
  XML_ELEMENT           * xCURRENT  = NULL; // Elemento corrente
  XML_ELEMENT           * xPARENT   = NULL; // Elemento parente
  XML_ATTRIBUTE         * xATTRIB   = NULL;
  XML_ENTITY            * xENTITY   = NULL;
  void                  * xPTR      = NULL;

  // Dichiarazione e allocazione variabili per la generazione dell'albero XML
  XML_ELEMENT           ** xlHEADER  = (XML_ELEMENT**)          malloc(sizeof(XML_ELEMENT*)*TagMax);
  XML_ELEMENT           ** xlELEMENT = (XML_ELEMENT**)          malloc(sizeof(XML_ELEMENT*)*TagMax);
  XML_COMMENT           ** xlCOMMENT = (XML_COMMENT**)          malloc(sizeof(XML_COMMENT*)*TagMax);
  XML_TEXT              ** xlTEXT    = (XML_TEXT**)             malloc(sizeof(XML_TEXT*)*TagMax);
  XML_CDATA_SECTION     ** xlCDATA   = (XML_CDATA_SECTION**)    malloc(sizeof(XML_CDATA_SECTION*)*TagMax);
  XML_PROC_INSTRUCTION  ** xlINSTR   = (XML_PROC_INSTRUCTION**) malloc(sizeof(XML_PROC_INSTRUCTION*)*TagMax);

  int                    * PosTable     = (int *)               malloc(sizeof(int)*TagMax);
  void                  ** ElementTable = (void **)             malloc(sizeof(void *)*TagMax);
  int                      CurrentLev   = 0;
  char                   * name         = NULL; // Stringa di appoggio per nomi

  /* Inizializzazione dei root pointer della sorgente XML */
  source->root_header       = NULL;
  source->root_comment      = NULL;
  source->root_element      = NULL;
  source->root_text         = NULL;
  source->root_cdata        = NULL;
  source->root_instruction  = NULL;

  int Type;

  PosTable[0] = 0;
  ElementTable[0] = NULL;

  // Inizializzo le liste del livello 0
  xlHEADER  [0] = NULL;
  xlELEMENT [0] = NULL;
  xlCOMMENT [0] = NULL;
  xlTEXT    [0] = NULL;
  xlCDATA   [0] = NULL;
  xlINSTR   [0] = NULL;

  for (int t=0; t<TagMax; t++)
  {
    Type = TagType[t]; // Recupero il tipo
    Line = (char *) TagList[t]; // Recupero la riga di tag
    // Controllo il tipo
    switch (Type)
    {
      case XML_TAG_HEADER:
        xPARENT = xCURRENT; // Recupero il puntatore all'elemento parent
        name = (char*)malloc(sizeof(char)*(strlen("XML_HEADER")+1));
        strcpy(name,"XML_HEADER");
        xHEADER = XMLCreateElement(name,xPARENT,NULL);
        xHEADER->pos = 0; // Scrivo la posizione (sempre 0 per gli Header)
        ret_code = XMLExtract (xHEADER,Line); // Estraggo gli attributi dell'elemento XML
        if (source->root_header == NULL) source->root_header = xHEADER; // Collego l'intestazione root
        XMLElementLink(xlHEADER[0],xHEADER);
        xlHEADER[0] = xHEADER; // Assegno il puntatore all'ultimo elemento del livello
        /* Controllo gli attributi dell'header */
        xATTRIB = xHEADER->attrib; // Prendo il primo (se c'�)
        while(xATTRIB)
        {
          if (strcmpi("standalone",xATTRIB->name)==0)
          {
            source->stand_alone = (strcmpi("yes",xATTRIB->value)==0)?true:false;
          }
          if (strcmpi("version",xATTRIB->name)==0)
          {
            source->version = (char *) malloc(sizeof(char *)*(xATTRIB->length+1));
            strcpy(source->version,xATTRIB->value);
            /* Estraggo i due campi della versione */
            bool finish = false;
            bool minor = false;
            int vlen = 0; // Lunghezza valore
            int vcnt = 0; // Contatore caratteri valore
            int vmin = 0;
            int vmaj = 0;
            int iapp = 0;
            char capp = '\0';
            while(!finish)
            {
              capp = xATTRIB->value[vcnt];
              iapp = (int) capp;
              iapp = iapp-48;

              if (capp=='.')
              {
                for (int v=(vlen-1); v>=0; v--)
                {
                  capp = xATTRIB->value[v];
                  iapp = (int) capp;
				  iapp = iapp-48;
				  vmaj += (int)(iapp*pow((double)10,(double)(vlen-v-1)));
                }
                minor = true; //Passo al campo meno significativo
                vlen = 0;
              }
              else
              {
                if (minor)
				{
					vmin += (int)(iapp*pow((double)10,(double)(xATTRIB->length-1-vcnt)));
				}
                vlen++;
              }
              vcnt++;
              finish = (vcnt == xATTRIB->length)?true:false;
            }
            source->version_major = vmaj;
            source->version_minor = vmin;
          }
          if (strcmpi("encoding",xATTRIB->name)==0)
          {
            source->encoding = (char *) malloc(sizeof(char *)*(xATTRIB->length+1));
            strcpy(source->encoding,xATTRIB->value);
          }
          xATTRIB = xATTRIB->next; // Prendo l'attributo successivo (se c'�)
        }

        free(Line); // Per questo Tag non mi serve la riga XML
      break;

      case XML_TAG_ELEMENT_START :
        xPARENT = (CurrentLev)? xlELEMENT[CurrentLev-1]:NULL; // Recupero il puntatore all'elemento parent
        name = (char*)malloc(sizeof(char)*(strlen("XML_ELEMENT_START")+1));
        strcpy(name,"XML_ELEMENT_START");
        xELEMENT = XMLCreateElement(name,xPARENT,NULL);
        xELEMENT->pos = PosTable[CurrentLev]; // Scrivo la posizione
        ret_code = XMLExtract (xELEMENT,Line); // Estraggo gli attributi dell'elemento XML
        XMLElementLink(xlELEMENT[CurrentLev],xELEMENT);
        if (xlELEMENT[CurrentLev] == NULL)
        {
          XMLElementUpLink(xPARENT,xELEMENT);
          if (CurrentLev == 0)
          {
            root = xELEMENT;
            source->root_element = xELEMENT;
          }
        }
        if (xPARENT) xPARENT->child_count++;
        xlELEMENT[CurrentLev] = xELEMENT;
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)xELEMENT,XML_ENTITY_ELEMENT,xELEMENT->pos);
        xENTITY->level = CurrentLev;

        xCURRENT = xELEMENT;
        PosTable[CurrentLev]++; // Aggiorno la posizione per il prossimo elemento
        CurrentLev++; // Passo al livello successivo
        PosTable[CurrentLev] = 0; // Resetto il nuovo contatore di posizione
        ElementTable[CurrentLev] = (void *) xELEMENT->name; // Copio il puntatore al nome
        // Inizializzo i puntatori alle strutture delle entit� XML
        xlCOMMENT [CurrentLev] = NULL;
        xlTEXT    [CurrentLev] = NULL;
        xlCDATA   [CurrentLev] = NULL;
        xlINSTR   [CurrentLev] = NULL;
        xlELEMENT [CurrentLev] = NULL;
        free(Line); // Per questo Tag non mi serve la riga XML
      break;

      case XML_TAG_ELEMENT :
        xPARENT = (CurrentLev)? xlELEMENT[CurrentLev-1]:NULL; // Recupero il puntatore all'elemento parent
        name = (char*)malloc(sizeof(char)*(strlen("XML_ELEMENT")+1));
        strcpy(name,"XML_ELEMENT");
        xELEMENT = XMLCreateElement(name,xPARENT,NULL);
        xELEMENT->pos = PosTable[CurrentLev]; // Scrivo la posizione
        ret_code = XMLExtract (xELEMENT,Line); // Estraggo gli attributi dell'elemento XML
        XMLElementLink(xlELEMENT[CurrentLev],xELEMENT);
        if (xlELEMENT[CurrentLev] == NULL) XMLElementUpLink(xPARENT,xELEMENT);
        if (xPARENT) xPARENT->child_count++;
        xlELEMENT[CurrentLev] = xELEMENT;
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)xELEMENT,XML_ENTITY_ELEMENT,xELEMENT->pos);
        xENTITY->level = CurrentLev;

        PosTable[CurrentLev]++; // Aggiorno la posizione per il prossimo elemento
        free(Line); // Per questo Tag non mi serve la riga XML
      break;

      case XML_TAG_ELEMENT_STOP :
        if (CurrentLev>0)
        {
          CurrentLev--;
          if (xlELEMENT[CurrentLev] != NULL)
          {
            if (!XMLTokCmp(Line,xlELEMENT[CurrentLev]->name,2))
            {
              ret_code = XML_ERROR_BAD_CLOSURE;
            }
          }
        }
        else
        {
          ret_code = XML_ERROR_TOO_CLOSURES;
        }
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)Line,XML_ENTITY_ELEMENT_CLOSURE,-1);
        xENTITY->level = CurrentLev;

        xCURRENT  = xlELEMENT[CurrentLev];
        xPARENT   = xlELEMENT[CurrentLev]->parent;
        //free(Line); // Per questo Tag non mi serve la riga XML
      break;

      case XML_TAG_COMMENT :
        xPARENT = (CurrentLev)? xlELEMENT[CurrentLev-1]:NULL; // Recupero il puntatore all'elemento parent
        xPTR = (void *) xCOMMENT; // Salvo il puntatore precedente
        xCOMMENT = XMLCreateComment(xPARENT,(XML_COMMENT *)xPTR);
        xCOMMENT->pos = PosTable[CurrentLev]; // Copio la posizione
        xCOMMENT->value = Line;
        xCOMMENT->length = strlen(Line);
        XMLCommentLink(xlCOMMENT[CurrentLev],xCOMMENT);
        if (xlCOMMENT[CurrentLev] == NULL)
        {
          XMLCommentUpLink(xPARENT,xCOMMENT);
          if (CurrentLev == 0)
          {
            source->root_comment = xCOMMENT;
          }
        }
        if (xPARENT) xPARENT->comment_count++;
        xlCOMMENT[CurrentLev] = xCOMMENT;
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)xCOMMENT,XML_ENTITY_COMMENT,xCOMMENT->pos);
        xENTITY->level = CurrentLev;
        PosTable[CurrentLev]++; // Aggiorno la posizione per il prossimo elemento
      break;

      case XML_NONTAG_TEXT :
        xPARENT = (CurrentLev)? xlELEMENT[CurrentLev-1]:NULL; // Recupero il puntatore all'elemento parent
        xPTR = (void *) xTEXT; // Salvo il puntatore precedente
        xTEXT = XMLCreateText(xPARENT,(XML_TEXT *)xPTR);
        xTEXT->pos = PosTable[CurrentLev]; // Copio la posizione
        xTEXT->value = Line;
        xTEXT->length = strlen(Line);
        XMLTextLink(xlTEXT[CurrentLev],xTEXT);
        if (xlTEXT[CurrentLev] == NULL)
        {
          XMLTextUpLink(xPARENT,xTEXT);
          if (CurrentLev == 0)
          {
            source->root_text = xTEXT;
          }
        }
        if (xPARENT) xPARENT->text_count++;
        xlTEXT[CurrentLev] = xTEXT;
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)xTEXT,XML_ENTITY_TEXT,xTEXT->pos);
        xENTITY->level = CurrentLev;
        PosTable[CurrentLev]++; // Aggiorno la posizione per il prossimo elemento
      break;

      case XML_TAG_CDATA_SECTION :
        xPTR = (void *) xCDATA; // Salvo il puntatore precedente
        xPARENT = (CurrentLev)? xlELEMENT[CurrentLev-1]:NULL; // Recupero il puntatore all'elemento parent
        xCDATA = XMLCreateCData(xPARENT,(XML_CDATA_SECTION *)xPTR);
        xCDATA->pos = PosTable[CurrentLev]; // Copio la posizione
        xCDATA->value = Line;
        xCDATA->length = strlen(Line);
        XMLCDataLink(xlCDATA[CurrentLev],xCDATA);
        if (xlCDATA[CurrentLev] == NULL)
        {
          XMLCDataUpLink(xPARENT,xCDATA);
          if (CurrentLev == 0)
          {
            source->root_cdata = xCDATA;
          }
        }
        if (xPARENT) xPARENT->cdata_count++;
        xlCDATA[CurrentLev] = xCDATA;
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)xCDATA,XML_ENTITY_CDATA_SECTION,xCDATA->pos);
        xENTITY->level = CurrentLev;
        PosTable[CurrentLev]++; // Aggiorno la posizione per il prossimo elemento
      break;

      case XML_TAG_PROC_INSTRUCTION :
        xPTR = (void *) xINSTR; // Salvo il puntatore precedente
        xPARENT = (CurrentLev)? xlELEMENT[CurrentLev-1]:NULL; // Recupero il puntatore all'elemento parent
        xINSTR = XMLCreateInstruction(xPARENT,(XML_PROC_INSTRUCTION *)xPTR);
        xINSTR->pos = PosTable[CurrentLev]; // Copio la posizione
        xINSTR->value = Line;
        xINSTR->length = strlen(Line);
        XMLInstructionLink(xlINSTR[CurrentLev],xINSTR);
        if (xlINSTR[CurrentLev] == NULL)
        {
          XMLInstructionUpLink(xPARENT,xINSTR);
          if (CurrentLev == 0)
          {
            source->root_instruction = xINSTR;
          }
        }

        if (xPARENT) xPARENT->instruction_count++;
        xlINSTR[CurrentLev] = xINSTR;
        xENTITY = XMLCreateEntity(source,xENTITY,(void*)xINSTR,XML_ENTITY_PROC_INSTRUCTION,xINSTR->pos);
        xENTITY->level = CurrentLev;
        PosTable[CurrentLev]++; // Aggiorno la posizione per il prossimo elemento
      break;
    }
  }

  if(CurrentLev>0) ret_code = XML_ERROR_EXPECTING_CLOSURE;

  // Libero la memoria occupata da strutture di appoggio
  free(XMLText      );
  free(TagList      );
  free(TagType      );
  free(TagUsed      );

  free(xlHEADER     );
  free(xlELEMENT    );
  free(xlCOMMENT    );
  free(xlTEXT       );
  free(xlCDATA      );
  free(xlINSTR      );
  free(PosTable     );
  free(ElementTable );

  source->well_formed = (ret_code)? false:true;

  return ret_code;
}

//==============================================================================
/// Funzione il controllo di entit� child XML.
/// Verifica se un elemento XML presenta delle sub-entit� XML. Restituisce
/// <true> in caso sia presente almeno una sub-entit� XML, altrimenti
/// restituisce <false>.
///
/// \date [29.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLNoChildElement(XML_ELEMENT * element)
{
  bool no_child = false;

  if (element) // Controllo l'elemento
  {
    if (element->comment == NULL
    && element->cdata == NULL
    && element->text == NULL
    && element->instruction == NULL
    && element->child == NULL)
    {
      no_child = true;
    }
  }

  return no_child;
}

//==============================================================================
/// Funzione per generare una riga di testo header XML.
/// A partire dalla struttura allocata in memoria contenuta all'interno di
/// <source> di una sorgente XML si ricostruisce una riga di testo in formato
/// XML rappresentante il tag dell'intestazione della sorgente.
///
/// \date [07.12.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
char * XMLGenerateHeader( XML_SOURCE * source )
{
	char            line[XML_MAX_CHAR];
	char            chap[XML_MAX_CHAR];
	char          * header            ;
	XML_ELEMENT   * xHEADER           ;
	XML_ATTRIBUTE * xATTRIB           ;
	int             offset            ;

	if ( source != NULL )
	{
		if ( source->root_header != NULL )
		{
			xHEADER = source->root_header;

			while( xHEADER != NULL )
			{
				strcpy( line, "<?xml" );
				offset = 5;

				// Disegno eventuali attributi
				xATTRIB = xHEADER->attrib;
				while ( xATTRIB != NULL )
				{
					line[offset] = ' '; // metto prima uno spazio
					offset++; // Avanzo l'offset
					for (size_t c=0; c<strlen(xATTRIB->name); c++)
					{
						line[offset] = xATTRIB->name[c];
						offset++;
					}
					line[offset] = '='; // metto l'uguale
					offset++; // Avanzo l'offset
					line[offset] = '\"'; // metto le virgolette iniziali
					offset++;
					for ( int c = 0; c < xATTRIB->length; c++ )
					{
						line[offset] = xATTRIB->value[c];
						offset++;
					}
					line[offset] = '\"'; // metto le virgolette finali
					offset++; // Avanzo l'offset
					//xATTRIB->tag = XMLCreateNode(tree,(TTreeNode*) xHEADER->tag,(void *) xATTRIB,xATTRIB->name+AnsiString("=\"")+xATTRIB->value+AnsiString("\""),2);
					xATTRIB = xATTRIB->next;
				}
				line[offset] = '?'; // metto il primo carattere di fine
				offset++; // Avanzo l'offset
				line[offset] = '>'; // metto il secondo carattere di fine
				offset++; // Avanzo l'offset
				line[offset] = '\n'; // Vado a capo
				offset++;
				// --- Passo all'header successivo ---
				xHEADER = xHEADER->next;
			}
			line[offset] = '\0'; // Metto il NULL terminatore
			offset++;
		}
		else
		{
			strcpy( line, "<?xml" );
			offset = 5;
			if ( source->version )
			{
				strcpy( chap, " encoding=\"" );
				for ( int c = 0; c < 11; c++ )
				{
					line[offset+c] = chap[c];
				}
				offset += 11;
				for ( size_t c = 0; c < strlen( source->version ); c++ )
				{
					line[offset+c] = source->version[c];
				}
				offset+=strlen( source->version );
				line[offset] = '\"'; // metto le virgolette finali
				offset++; // Avanzo l'offset
			}
			if ( source->encoding )
			{
				strcpy( chap, " encoding=\"" );
				for ( int c = 0; c < 11; c++ )
				{
					line[offset+c] = chap[c];
				}
				offset += 11;
				for ( size_t c = 0; c < strlen( source->encoding ); c++ )
				{
					line[offset+c] = source->encoding[c];
				}
				offset += strlen( source->encoding );
				line[offset] = '\"'; // metto le virgolette finali
				offset++; // Avanzo l'offset
			}
			if ( source->stand_alone )
			{
				strcpy( chap, " standalone=\"yes\"" );
				for ( int c = 0; c < 17; c++ )
				{
					line[offset+c] = chap[c];
				}
				offset += 17;
			}

			line[offset] = '?'; // metto il primo carattere di fine
			offset++; // Avanzo l'offset
			line[offset] = '>'; // metto il secondo carattere di fine
			offset++; // Avanzo l'offset
			line[offset] = '\n'; // Vado a capo
			offset++;
			line[offset] = '\0'; // Metto il NULL terminatore
			offset++;
		}

		header = (char*) malloc( sizeof(char) * offset );
		for ( int c = 0; c < offset; c++ )
		{
			header[c] = line[c];
		}
	}
	else
	{
		header = NULL;
	}

	return header;
}

//==============================================================================
/// Funzione per generare una riga di testo elementi XML.
/// A partire dalla struttura allocata in memoria contenuta all'interno di
/// <element> di un elemento XML si ricostruisce una riga di testo in formato
/// XML rappresentante tale entit�.
///
/// \date [29.01.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLGenerateTag( XML_ELEMENT * element )
{
	char line[XML_MAX_CHAR];
	int offset = 0;
	XML_ATTRIBUTE * attrib;
	char * newline;

	if (element)
	{
		line[offset] = '<'; // Apertura tag
		offset++; // Avanzo l'offset

		// Copio il nome dell'elemento
		for ( size_t c = 0; c < strlen( element->name ); c++ )
		{
			line[offset+c] = element->name[c];
		}
		offset += strlen( element->name );

		attrib = element->attrib;

		while( attrib )
		{
			line[offset] = ' '; // Aggiungo uno spazio
			offset++; // Avanzo l'offset
			// Copio il nome dell'elemento
			for ( size_t c = 0; c < strlen( attrib->name ); c++ )
			{
				line[offset+c] = attrib->name[c];
			}
			offset += strlen( attrib->name );
			line[offset] = '='; // Aggiungo l'uguale
			offset++; // Avanzo l'offset
			line[offset] = '\"'; // Aggiungo uno spazio
			offset++; // Avanzo l'offset
			// Copio il valore dell'elemento
			for ( size_t c = 0; c < strlen( attrib->value ); c++ )
			{
				line[offset+c] = attrib->value[c];
			}
			offset += strlen( attrib->value );
			line[offset] = '\"'; // Aggiungo uno spazio
			offset++; // Avanzo l'offset

			attrib = attrib->next; // Vado all'attributo successivo (se esiste)
		}
		if ( XMLNoChildElement( element ) )
		{
			line[offset] = '/'; // Tag singolo
			offset++; // Avanzo l'offset
		}
		line[offset] = '>'; // Chiusura tag
		offset++; // Avanzo l'offset

		if ( XMLNoChildElement( element ) )
		{
			line[offset] = '\n'; // Vado a capo
			offset++; // Avanzo l'offset
		}

		//line[offset] = '\n'; // Vado a capo  - Tolto A CAPO
		//offset++; // Avanzo l'offset
		line[offset] = '\0'; // Aggiungo il terminatore di stringa
		offset++;

		newline = (char *) malloc( sizeof(char) * offset );
		for ( int c = 0; c < offset; c++ )
		{
			newline[c] = line[c];
		}
	}
	else
	{
		newline = NULL;
	}

	return newline;
}

//==============================================================================
/// Funzione per generare una riga di testo per chiusura elementi XML.
/// A partire dalla struttura allocata in memoria contenuta all'interno di
/// <element> di un elemento XML si ricostruisce una riga di testo in formato
/// XML rappresentante la chiusura tale entit�.
///
/// \date [29.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLGenerateClosureTag( XML_ELEMENT * element )
{
	char chapp[XML_MAX_CHAR];
	char * newline;

	if ( element )
	{
		chapp[0] = '<';
		chapp[1] = '/';
		for ( size_t c = 0; c < strlen( element->name ); c++ )
		{
			chapp[2+c] = element->name[c];
		}
		chapp[2+strlen(element->name)] = '>';
		chapp[3+strlen(element->name)] = '\n';
		chapp[4+strlen(element->name)] = '\0';

		newline = (char*) malloc( sizeof(char) * (strlen(element->name)+5) );

		for ( size_t c = 0; c < (strlen(element->name)+5); c++ )
		{
			newline[c] = chapp[c];
		}
	}
	else
	{
		newline = NULL;
	}

	return newline;
}

//==============================================================================
/// Funzione per generare una riga di tag per entit� XML.
/// A partire dalla struttura allocata in memoria contenuta all'interno di
/// <entity> di un entit� XML si ricostruisce una riga di testo in formato
/// XML rappresentante il tag tale entit�.
/// Restituisce NULL se la struttura non � valida.
///
/// \date [26.09.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
char * XMLGetTag( XML_ENTITY * entity )
{
	char                  * newtag              ;
	char                  * ptr                 ;
	int                     level               ;
	char                  * xCLOSURE    = NULL  ;
	XML_ENTITY            * xENTITY     = entity;
	XML_ELEMENT           * xHEADER     = NULL  ;
	XML_ELEMENT           * xELEMENT    = NULL  ;
	XML_ATTRIBUTE         * xATTRIB     = NULL  ;
	XML_TEXT              * xTEXT       = NULL  ;
	XML_COMMENT           * xCOMMENT    = NULL  ;
	XML_CDATA_SECTION     * xCDATA      = NULL  ;
	XML_PROC_INSTRUCTION  * xINSTR      = NULL  ;
	bool					tab			= false	;

	if ( entity != NULL ) // Controllo l'entit�
	{
		level = xENTITY->level;
		switch ( entity->type )
		{
			case XML_ENTITY_HEADER:
				xHEADER = (XML_ELEMENT*) xENTITY->structure;
				newtag = NULL;
			break;
			case XML_ENTITY_ELEMENT:
				xELEMENT = (XML_ELEMENT*) xENTITY->structure;
				newtag = XMLGenerateTag( xELEMENT );
				if ( level )
				{
					ptr = (char*) malloc(sizeof(char)*(strlen(newtag)+1+level));
					for (int t=0; t<level; t++)
					{
						ptr[t] = '\t';
					}
					for ( size_t c = 0; c < (strlen(newtag)+1); c++ )
					{
						ptr[level+c] = newtag[c];
					}
					free( newtag );
					newtag = ptr;
					ptr = NULL;
				}
				// --- Se non ha testi inclusi e ha almeno un figlio ---
				if ( xELEMENT->text == NULL && xELEMENT->child != NULL )
				{
					ptr = (char*) malloc( sizeof(char) * (strlen(newtag)+1+1) ); // -MALLOC
					// --- Copio il vecchio ---
					memcpy( &ptr[0], &newtag[0], sizeof(char)*(strlen(newtag)) );
					// --- Vado a capo ---
					ptr[strlen(newtag)]		= '\n';
					ptr[strlen(newtag)+1]	= '\0';
					// --- Sostituisco il vecchio con il nuovo ---
					free( newtag ); // -FREE
					newtag = ptr;
					ptr = NULL;
				}
			break;
			case XML_ENTITY_ELEMENT_CLOSURE:
				xCLOSURE = (char*) xENTITY->structure;
				ptr = (char*) malloc( sizeof(char)*(strlen(xCLOSURE)+2+level) );
				tab = true;
                // --- Controllo se prima c'era un testo ---
				if ( xENTITY->prev != NULL ) {
					if ( xENTITY->prev->type == XML_ENTITY_TEXT )
					{
						tab = false;
					}
				}
				// --- Con tabulazione ---
				if ( tab )
				{
					for ( int t = 0; t < level; t++ )
					{
						ptr[t] = '\t';  
					}
					for ( size_t c = 0; c < (strlen(xCLOSURE)); c++ )
					{
						ptr[level+c] = xCLOSURE[c];
					}
					ptr[level+strlen(xCLOSURE)] 	= '\n';
					ptr[level+strlen(xCLOSURE)+1]	= '\0';
				}
				// --- Senza tabulazione ---
				else
				{
					for ( size_t c = 0; c < (strlen(xCLOSURE)); c++ )
					{
						ptr[c] = xCLOSURE[c];
					}
					ptr[strlen(xCLOSURE)]	= '\n';
					ptr[strlen(xCLOSURE)+1] = '\0';
				}
				newtag = ptr;
				ptr = NULL;
			break;
			case XML_ENTITY_ATTRIBUTE:
				xATTRIB = (XML_ATTRIBUTE*) xENTITY->structure;
				newtag = NULL;
			break;
			case XML_ENTITY_TEXT:
				xTEXT = (XML_TEXT*) xENTITY->structure;
				newtag = (char*) malloc(sizeof(char)*(xTEXT->length+2+level));
				for ( int t = 0; t < level; t++ )
				{
					// newtag[t] = '\t';   Tolta TABULAZIONE
				}
				for ( size_t c = 0; c < (strlen(xTEXT->value)); c++ )
				{
					//newtag[level+c] = xTEXT->value[c];
					newtag[c] = xTEXT->value[c];
				}
				// newtag[level+strlen(xTEXT->value)] = '\n'; Tolto A CAPO
				//newtag[level+strlen(xTEXT->value)+1] = '\0';
				newtag[strlen(xTEXT->value)] = '\0';
			break;
			case XML_ENTITY_COMMENT:
				xCOMMENT = (XML_COMMENT*) xENTITY->structure;
				newtag = (char*) malloc(sizeof(char)*(xCOMMENT->length+2+level));
				for ( int t = 0; t < level; t++ )
				{
					newtag[t] = '\t';
				}
				for ( size_t c = 0; c < (strlen(xCOMMENT->value)); c++ )
				{
					newtag[level+c] = xCOMMENT->value[c];
				}
				newtag[level+strlen(xCOMMENT->value)] = '\n';
				newtag[level+strlen(xCOMMENT->value)+1] = '\0';
			break;
			case XML_ENTITY_CDATA_SECTION:
				xCDATA = (XML_CDATA_SECTION*) xENTITY->structure;
				newtag = (char*) malloc(sizeof(char)*(xCDATA->length+2+level));
				for ( int t = 0; t < level; t++ )
				{
					newtag[t] = '\t';
				}
				for ( size_t c = 0; c < (strlen(xCDATA->value)); c++ )
				{
					newtag[level+c] = xCDATA->value[c];
				}
				newtag[level+strlen(xCDATA->value)] = '\n';
				newtag[level+strlen(xCDATA->value)+1] = '\0';
			break;
			case XML_ENTITY_PROC_INSTRUCTION:
				xINSTR = (XML_PROC_INSTRUCTION*) xENTITY->structure;
				newtag = (char*) malloc(sizeof(char)*(xINSTR->length+2+level));
				for ( int t = 0; t < level; t++ )
				{
					newtag[t] = '\t';
				}
				for ( size_t c = 0; c < (strlen(xINSTR->value)); c++ )
				{
					newtag[level+c] = xINSTR->value[c];
				}
				newtag[level+strlen(xINSTR->value)] = '\n';
				newtag[level+strlen(xINSTR->value)+1] = '\0';
			break;
			default:
				newtag = NULL;
		}
	}
	else
	{
		newtag = NULL;
	}

	return newtag;
}

//==============================================================================
/// Funzione per scrivere file XML.
/// A partire dalla struttura allocata in memoria contenuta all'interno di
/// <source> si ricostruisce un file in formato XML rappresentante tale
/// struttura.
///
/// \date [07.03.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int XMLWrite( XML_SOURCE * source )
{
	int                     ret_code    = 0         ;
	XML_ENTITY            * xENTITY     = NULL      ;
	char                  * newline     = NULL      ;

	if ( source != NULL ) // Controllo la sorgente
	{
		// Genero la riga per l'header XML
		newline = XMLGenerateHeader(source);
		// Scrivo la riga di Header XML
		XMLPuts(source,newline);
		//* BUGFIX 20070307 by Enrico - Memory Leak
		if ( newline != NULL ) free( newline );
		//*/

    // Controllo se esiste una lista di entit� XML nella sorgente
    if (source->first_entity)
    {
      xENTITY = source->first_entity; // Prendo la prima entit�

      while(xENTITY)
      {
		newline = XMLGetTag(xENTITY);
		XMLPuts(source,newline);
        free(newline);

        xENTITY = xENTITY->next; // Passo all'entit� successiva (se esiste)
      }
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ROOT;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_SOURCE;
  }
  return ret_code;
}

//==============================================================================
/// Funzione per la creazione di entit� XML.
/// Alloca in memoria una struttura entit� XML con nome legata alla struttura
/// <structure>, al tipo <type> e alla posizione <pos> passati.
/// Di defalut imposta il flag di entit� temporanea con valore falso (ci
/// pensareanno eventuali funzioni a livello superiore a gestire questo flag).
/// Se la sorgente <source> non contiene nessuna entit� viene linkata come
/// <first_entity>. Se <prev> � passato con valore non NULL la struttura
/// precedente viene linkata alla nuova.
/// Restituisce NULL se la struttura <structure> passata non � valida.
/// Se la sorgente <source> passata � diversa da NULL va a controllare se il suo
/// campo <first_entity> � vuoto e in questo caso lo aggiorna facendolo puntare
/// alla nuova entit� creata.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
XML_ENTITY * XMLCreateEntity(XML_SOURCE * source,XML_ENTITY * prev,void * structure,int type,int pos)
{
	XML_ENTITY * xENTITY = NULL; // Ptr a entit� XML

	if ( structure != NULL ) // Controllo struttura
	{
		xENTITY = (XML_ENTITY*) malloc( sizeof(XML_ENTITY) ); // Alloco la memoria
		if ( xENTITY != NULL ) // Controllo se l'allocazione � andata a buon fine
		{
			if ( prev != NULL ) // Controllo se esiste un precedente
			{
				prev->next = xENTITY; // Linko la nuova entit�
			}
			if ( source != NULL && source->first_entity == NULL ) // Controllo se � stato fornito il <source> e se devo aggironare il primo entity
			{
				source->first_entity = xENTITY; // Linko come prima entit�
			}
			xENTITY->pos        = pos;
			xENTITY->type       = type;
			xENTITY->temp       = false;
			xENTITY->structure  = structure;
			xENTITY->prev       = prev;
			xENTITY->next       = NULL;
		}
	}
	else
	{
		xENTITY = NULL;
	}
	return xENTITY;
}

//==============================================================================
/// Funzione per la creazione di elementi XML.
/// Alloca in memoria una struttura elemento XML con nome <name> e elemento
/// parent <parent> ed elemento precedente <prev>.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
XML_ELEMENT * XMLCreateElement(char * name, XML_ELEMENT * parent, XML_ELEMENT * prev)
{
	XML_ELEMENT * xml_element = NULL; // Ptr a elemento XML

	xml_element = (XML_ELEMENT*) malloc( sizeof(XML_ELEMENT) );

    xml_element->pos                = -1;
    xml_element->name               = name;
    xml_element->attrib             = NULL;
    xml_element->cdata              = NULL;
    xml_element->text               = NULL;
    xml_element->comment            = NULL;
    xml_element->instruction        = NULL;
    xml_element->child              = NULL;
    xml_element->parent             = parent;
    xml_element->attrib_count       = 0;
    xml_element->cdata_count        = 0;
    xml_element->comment_count      = 0;
    xml_element->instruction_count  = 0;
    xml_element->child_count        = 0;
    xml_element->text_count         = 0;
    xml_element->prev               = prev;
    xml_element->next               = NULL;
    xml_element->tag                = NULL;

    return xml_element;
}

//==============================================================================
/// Funzione per la creazione di attributi XML.
/// Alloca in memoria una struttura attributo XML con nome <name> e elemento
/// parent <parent> ed elemento precedente <prev>.
/// NOTA: Il puntatore alla stringa del nome dell'attributo <name> deve essere
///       un puntatore ad un blocco di memoria allocato con malloc() altrimenti
///       se fosse una costante in fase di cancellazione dell'attributo avrei
///       un errore perch� tnterei di liberare un blocco di memoria non valido.
///       Se si una la funzione XMLNewAttrib() viene automaticamente fatta una
///       copia della stringa <name>.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
XML_ATTRIBUTE * XMLCreateAttribute( char * name, XML_ELEMENT * parent, XML_ATTRIBUTE * prev )
{
  XML_ATTRIBUTE * xml_a = NULL; // Ptr ad attributo XML

  xml_a         = (XML_ATTRIBUTE*)malloc( sizeof(XML_ATTRIBUTE) ); // -MALLOC

  xml_a->pos    = -1;
  xml_a->name   = name;
  xml_a->type   = NULL;
  xml_a->value  = NULL;
  xml_a->parent = parent;
  xml_a->length = 0;
  xml_a->prev   = prev;
  xml_a->next   = NULL;
  xml_a->tag    = NULL;

  return xml_a;
}

//==============================================================================
/// Funzione per la creazione di testo XML.
/// Alloca in memoria una struttura attributo XML con nome <name> e elemento
/// parent <parent> e il testo precedente <prev>.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_TEXT * XMLCreateText( XML_ELEMENT * parent, XML_TEXT * prev )
{
  XML_TEXT * xml_t = NULL; // Ptr a testo XML

  xml_t = (XML_TEXT*)malloc( sizeof(XML_TEXT) );

  xml_t->pos        = -1;
  xml_t->value      = NULL;
  xml_t->parent     = parent;
  xml_t->length     = 0;
  xml_t->prev       = prev;
  xml_t->next       = NULL;
  xml_t->tag        = NULL;

  return xml_t;
}

//==============================================================================
/// Funzione per la creazione di commenti XML.
/// Alloca in memoria una struttura attributo XML con nome <name> e elemento
/// parent <parent> e il commento precedente <prev>.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_COMMENT * XMLCreateComment( XML_ELEMENT * parent, XML_COMMENT * prev )
{
  XML_COMMENT * xml_c = NULL; // Ptr a commento XML

  xml_c = (XML_COMMENT*)malloc( sizeof(XML_COMMENT) );

  xml_c->pos          = -1;
  xml_c->value        = NULL;
  xml_c->parent       = parent;
  xml_c->length       = 0;
  xml_c->prev         = prev;
  xml_c->next         = NULL;
  xml_c->tag          = NULL;

  return xml_c;
}

//==============================================================================
/// Funzione per la creazione di sezioni dati XML.
/// Alloca in memoria una struttura attributo XML con nome <name> e elemento
/// parent <parent> e la sezione dati precedente <prev>.
///
/// \date [13.08.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_CDATA_SECTION * XMLCreateCData( XML_ELEMENT * parent, XML_CDATA_SECTION * prev )
{
  XML_CDATA_SECTION * xml_d = NULL; // Ptr a sezione dati XML

  xml_d = (XML_CDATA_SECTION*)malloc( sizeof(XML_CDATA_SECTION) );

  xml_d->pos                = -1;
  xml_d->value              = NULL;
  xml_d->parent             = parent;
  xml_d->length             = 0;
  xml_d->prev               = prev;
  xml_d->next               = NULL;
  xml_d->tag                = NULL;

  return xml_d;
}

//==============================================================================
/// Funzione per la creazione di istruzioni XML.
/// Alloca in memoria una struttura attributo XML con nome <name> e elemento
/// parent <parent> e l'istruzione precedente <prev>.
///
/// \date [13.08.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
XML_PROC_INSTRUCTION * XMLCreateInstruction( XML_ELEMENT * parent, XML_PROC_INSTRUCTION * prev )
{
  XML_PROC_INSTRUCTION * xml_i = NULL; // Ptr a istruzione XML

  xml_i = (XML_PROC_INSTRUCTION*)malloc( sizeof(XML_PROC_INSTRUCTION) );

  xml_i->pos                    = -1;
  xml_i->value                  = NULL;
  xml_i->parent                 = parent;
  xml_i->length                 = 0;
  xml_i->prev                   = prev;
  xml_i->next                   = NULL;
  xml_i->tag                    = NULL;

  return xml_i;
}

//==============================================================================
/// Funzione per la copia di sorgenti XML.
/// Rialloca la memoria necessaria per contentere la struttura XML <source> e
/// ne copia il contenuto.
/// La lista delle entit� puntata da <first_entity> non viene copiata.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [09.02.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
XML_SOURCE * XMLCopy( XML_SOURCE * source )
{
	XML_SOURCE            * out     = NULL;
    XML_ELEMENT           * element = NULL;
    XML_ELEMENT           * curr    = NULL;
    XML_ELEMENT           * prev    = NULL;
    XML_ELEMENT           * par     = NULL;
    XML_COMMENT           * comment = NULL;
    XML_TEXT              * text    = NULL;
	XML_CDATA_SECTION     * cdata   = NULL;
	XML_PROC_INSTRUCTION  * instr   = NULL;

	if ( source ) // Controllo la sorgente originale
	{
		out = (XML_SOURCE *) malloc( sizeof(XML_SOURCE) ); // Alloco per la copia
		if (out) // Controllo l'allocazione
		{
			void * ptr = memcpy( out, source, sizeof(XML_SOURCE) ); // Copia source
			if ( ptr ) // Controllo la copia
			{
				out->file = NULL; // Annullo il puntatore al file sorgente XML
				out->first_entity = NULL; // Non copio la lista entit�
				// --- Copio tutti i sottocampi char pointer ---
				if ( source->name )
				{
					out->name = (char *) malloc( sizeof(char) * (strlen(source->name)+1) );
					strcpy( out->name, source->name );
				}
				if ( source->version )
				{
					out->version = (char *) malloc( sizeof(char) * (strlen(source->version)+1) );
					strcpy( out->version, source->version );
				}
				if (source->encoding)
				{
				    out->encoding = (char *) malloc( sizeof(char) * (strlen(source->encoding)+1) );
					strcpy( out->encoding, source->encoding );
				}
				// --- Copio eventuali header (come struttura sono element) ---
				out->root_header = NULL;
				element = source->root_header;
				while ( element )
				{
					curr = XMLCopyElement( element ); // Copio l'Header
					if ( prev ) prev->next = curr;
					if ( out->root_header == NULL ) out->root_header = curr;
				    curr->parent = NULL;
				    curr->prev = prev;
				    curr->next = NULL;
				    prev = curr;
					element = element->next; // Vado all'header successivo (se esiste)
		    	}
		    	// --- Copio gli elementi ---
		    	curr = NULL;
				prev = NULL;
				par  = NULL;
				out->root_element = NULL;
				element = source->root_element;
				while ( element )
				{
					curr = XMLCopyElement( element ); // Copio l'elemento
					if ( prev ) prev->next = curr;
					if ( out->root_element == NULL ) out->root_element = curr;
					curr->parent = par;
					curr->prev = prev;
					curr->next = NULL;
					prev = curr;
					// --- Cerco l'elemento successivo (se esiste) ---
					if ( element->child )
					{
						par   = curr;
						prev  = NULL;
						element = element->child;
					}
					else
					{
						bool finish = false;
						while ( !finish )
						{
							if ( element->next )
							{
								prev = curr;
								element = element->next;
								finish = true;
							}
							else
							{
								if ( element->parent )
								{
									curr = curr->parent;
									par  = curr->parent;
									element = element->parent;
								}
								else
								{
									finish = true; // La scansione della struttura � finita
								}
							}
						}
					}
				}
				// --- Copio i testi ---
				XML_TEXT * xt_curr  = NULL;
				XML_TEXT * xt_prev  = NULL;
				out->root_text      = NULL;
				text                = source->root_text;
				while ( text )
				{
					xt_curr = XMLCopyText( text ); // Copio il testo
					xt_curr->next   = NULL;
					xt_curr->prev   = xt_prev;
					xt_curr->parent = NULL;
					if ( xt_prev ) xt_prev->next = xt_curr;
					if ( out->root_text == NULL ) out->root_text = xt_curr;
					xt_prev         = xt_curr;
					text = text->next; // Passo al testo successivo
				}
				// --- Copio i commenti ---
				XML_COMMENT * xc_curr  = NULL;
				XML_COMMENT * xc_prev  = NULL;
				out->root_comment      = NULL;
				comment                = source->root_comment;
				while ( comment )
				{
					xc_curr = XMLCopyComment( comment ); // Copio il commento
					xc_curr->next   = NULL;
					xc_curr->prev   = xc_prev;
					xc_curr->parent = NULL;
					if ( xc_prev ) xc_prev->next = xc_curr;
					if ( out->root_comment == NULL ) out->root_comment = xc_curr;
				    xc_prev         = xc_curr;
				    comment = comment->next; // Passo al commento successivo
				}
				// --- Copio le sezioni dati ---
				XML_CDATA_SECTION * xd_curr = NULL;
				XML_CDATA_SECTION * xd_prev = NULL;
				out->root_cdata             = NULL;
				cdata                       = source->root_cdata;
				while ( cdata )
				{
					xd_curr = XMLCopyCData( cdata ); // Copio la sezione
					xd_curr->next   = NULL;
					xd_curr->prev   = xd_prev;
					xd_curr->parent = NULL;
					if ( xd_prev ) xd_prev->next = xd_curr;
					if ( out->root_cdata == NULL ) out->root_cdata = xd_curr;
				    xd_prev         = xd_curr;
				    cdata = cdata->next; // Passo alla sezione dati successiva
		    	}
		    	// --- Copio le istruzioni ---
				XML_PROC_INSTRUCTION * xi_curr  = NULL;
				XML_PROC_INSTRUCTION * xi_prev  = NULL;
				out->root_instruction           = NULL;
				instr                           = source->root_instruction;
				while ( instr )
				{
					xi_curr = XMLCopyInstruction( instr ); // Copio l'istruzione successiva
					xi_curr->next   = NULL;
					xi_curr->prev   = xi_prev;
					xi_curr->parent = out;
					if ( xi_prev ) xi_prev->next = xi_curr;
					if ( out->root_instruction == NULL ) out->root_instruction = xi_curr;
					xi_prev         = xi_curr;
				    instr = instr->next; // Passo all'istruzione successiva
		    	}
		    }
		    else
		    {
				free( out );
		    	out = NULL;
		    }
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la copia di entit� XML.
/// Rialloca la memoria necessaria per contentere l'entit� XML <entity> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [09.02.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ENTITY * XMLCopyEntity( XML_ENTITY * entity )
{
	XML_ENTITY * out = NULL;

	if ( entity )
	{
		out = (XML_ENTITY *) malloc( sizeof(XML_ENTITY) ); // Alloco per la copia
		if ( out ) // Controllo l'allocazione
		{
			void * ptr = memcpy( out, entity, sizeof(XML_ENTITY) ); // Copia entity
    	    if ( ptr ) // Controllo la copia
    	    {
				switch ( entity->type ) // Copio anche la struttura dati specifica
				{
					case XML_ENTITY_HEADER:
						out->structure = (void*) XMLCopyElement( (XML_ELEMENT*)entity->structure );
					break;
					case XML_ENTITY_ELEMENT:
						out->structure = (void*) XMLCopyElement( (XML_ELEMENT*)entity->structure );
					break;
					case XML_ENTITY_ELEMENT_CLOSURE:
						out->structure = malloc( sizeof(char) * (strlen((char*)entity->structure)+1) );
						strcpy( (char*)out->structure, (char*)entity->structure );
					break;
					case XML_ENTITY_ATTRIBUTE:
						out->structure = (void*) XMLCopyAttribute( (XML_ATTRIBUTE*)entity->structure );
					break;
					case XML_ENTITY_TEXT:
						out->structure = (void*) XMLCopyText( (XML_TEXT*)entity->structure );
					break;
					case XML_ENTITY_COMMENT:
						out->structure = (void*) XMLCopyComment( (XML_COMMENT*)entity->structure );
					break;
					case XML_ENTITY_CDATA_SECTION:
						out->structure = (void*) XMLCopyCData( (XML_CDATA_SECTION*)entity->structure );
					break;
					case XML_ENTITY_PROC_INSTRUCTION:
						out->structure = (void*) XMLCopyInstruction( (XML_PROC_INSTRUCTION*)entity->structure );
					break;
					default:
						free( out );
						out = NULL;
    	    	}
    	    }
    	    else
    	    {
    	    	free( out );
		    	out = NULL;
		    }
    	}
    }

    return out;
}

//==============================================================================
/// Funzione per la copia di elementi XML.
/// Rialloca la memoria necessaria per contentere l'elemento XML <element> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [01.08.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_ELEMENT * XMLCopyElement( XML_ELEMENT * element )
{
	XML_ELEMENT           * out     = NULL;
	XML_ATTRIBUTE         * attrib  = NULL;
	XML_COMMENT           * comment = NULL;
	XML_TEXT              * text    = NULL;
	XML_CDATA_SECTION     * cdata   = NULL;
	XML_PROC_INSTRUCTION  * instr   = NULL;

	if ( element != NULL )
	{
		out = (XML_ELEMENT *) malloc( sizeof(XML_ELEMENT) ); // Alloco per la copia
		if ( out != NULL ) // Controllo l'allocazione
		{
			memcpy( out, element, sizeof(XML_ELEMENT) ); // Copia element
			// --- Copio tutti i sottocampi char pointer ---
			out->name  = (char *) malloc( sizeof(char)*( strlen( element->name ) + 1 ) );
			strcpy( out->name, element->name );
			// --- Copio gli attributi ---
			XML_ATTRIBUTE * xa_curr = NULL;
			XML_ATTRIBUTE * xa_prev = NULL;
			out->attrib   = NULL;
			attrib        = element->attrib;
			while ( attrib != NULL )
			{
				xa_curr = XMLCopyAttribute( attrib ); // Copio l'attributo
				xa_curr->next   = NULL;
				xa_curr->prev   = xa_prev;
				xa_curr->parent = out;
				if ( xa_prev != NULL ) xa_prev->next = xa_curr;
				if ( out->attrib == NULL ) out->attrib = xa_curr;
				xa_prev         = xa_curr;
				attrib = attrib->next; // Passo all'attributo successivo
			}
			// --- Copio i testi ---
			XML_TEXT * xt_curr  = NULL;
			XML_TEXT * xt_prev  = NULL;
			out->text           = NULL;
			text                = element->text;
			while ( text != NULL )
			{
				xt_curr = XMLCopyText(text); // Copio il testo
				xt_curr->next   = NULL;
				xt_curr->prev   = xt_prev;
				xt_curr->parent = out;
				if ( xt_prev ) xt_prev->next = xt_curr;
				if (out->text == NULL) out->text = xt_curr;
				xt_prev         = xt_curr;
				text = text->next; // Passo al testo successivo
			}
			// --- Copio i commenti ---
			XML_COMMENT * xc_curr  = NULL;
			XML_COMMENT * xc_prev  = NULL;
			out->comment           = NULL;
			comment                = element->comment;
			while ( comment != NULL )
			{
				xc_curr = XMLCopyComment( comment ); // Copio il commento
				xc_curr->next   = NULL;
				xc_curr->prev   = xc_prev;
				xc_curr->parent = out;
				if ( xc_prev ) xc_prev->next = xc_curr;
				if ( out->comment == NULL ) out->comment = xc_curr;
				xc_prev         = xc_curr;
				comment = comment->next; // Passo al commento successivo
			}
			// --- Copio le sezioni dati ---
			XML_CDATA_SECTION * xd_curr = NULL;
			XML_CDATA_SECTION * xd_prev = NULL;
			out->cdata                  = NULL;
			cdata                       = element->cdata;
			while ( cdata != NULL )
			{
				xd_curr = XMLCopyCData( cdata ); // Copio la sezione
				xd_curr->next   = NULL;
				xd_curr->prev   = xd_prev;
				xd_curr->parent = out;
				if ( xd_prev ) xd_prev->next = xd_curr;
				if ( out->cdata == NULL ) out->cdata = xd_curr;
				xd_prev         = xd_curr;
				cdata = cdata->next; // Passo alla sezione dati successiva
			}
			// --- Copio le istruzioni ---
			XML_PROC_INSTRUCTION * xi_curr  = NULL;
			XML_PROC_INSTRUCTION * xi_prev  = NULL;
			out->instruction                = NULL;
			instr                           = element->instruction;
			while ( instr != NULL )
			{
				xi_curr = XMLCopyInstruction( instr ); // Copio l'istruzione successiva
				xi_curr->next   = NULL;
				xi_curr->prev   = xi_prev;
				xi_curr->parent = out;
				if ( xi_prev ) xi_prev->next = xi_curr;
				if ( out->instruction == NULL ) out->instruction = xi_curr;
				xi_prev         = xi_curr;
				instr = instr->next; // Passo all'istruzione successiva
			}
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la copia di attributi XML.
/// Rialloca la memoria necessaria per contentere l'elemento XML <attrib> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [10.02.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_ATTRIBUTE * XMLCopyAttribute( XML_ATTRIBUTE * attrib )
{
	XML_ATTRIBUTE * out = NULL;

	if ( attrib )
	{
		out = (XML_ATTRIBUTE *) malloc(sizeof(XML_ATTRIBUTE)); // Alloco per la copia
		if ( out ) // Controllo l'allocazione
		{
			void * ptr = memcpy(out,attrib,sizeof(XML_ATTRIBUTE)); // Copia attributo
			if ( ptr ) // Controllo la copia
			{
				// --- Copio tutti i sottocampi char pointer ---
				if ( attrib->name )
				{
					out->name = (char *) malloc( sizeof(char)*(strlen(attrib->name )+1) );
					strcpy( out->name, attrib->name );
				}
				if ( attrib->type )
				{
					out->type = (char *) malloc( sizeof(char)*(strlen(attrib->type )+1) );
					strcpy( out->type, attrib->type );
				}
				if ( attrib->value )
				{
					out->value  = (char *) malloc( sizeof(char)*(strlen(attrib->value)+1) );
					strcpy( out->value, attrib->value );
				}
			}
			else
			{
				free( out );
				out = NULL;
			}
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la copia di testi XML.
/// Rialloca la memoria necessaria per contentere l'elemento XML <text> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [10.02.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_TEXT * XMLCopyText( XML_TEXT * text )
{
	XML_TEXT * out = NULL;

	if ( text ) // Controllo il testo
	{
		out = (XML_TEXT *) malloc( sizeof(XML_TEXT) ); // Alloco per la copia
		if ( out ) // Controllo l'allocazione
		{
			void * ptr = memcpy( out, text, sizeof(XML_TEXT) ); // Copia text
			if ( ptr ) // Controllo la copia
			{
		    	// --- Copio tutti i sottocampi char pointer ---
		    	if ( text->value )
		    	{
		    		out->value  = (char *) malloc( sizeof(char)*(strlen(text->value)+1) );
		    		strcpy( out->value, text->value );
		    	}
		    }
		    else
		    {
		    	free( out );
		    	out = NULL;
			}
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la copia di commenti XML.
/// Rialloca la memoria necessaria per contentere l'elemento XML <comment> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [01.09.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
XML_COMMENT * XMLCopyComment( XML_COMMENT * comment )
{
	XML_COMMENT * out = NULL;

	if ( comment != NULL ) // Controllo il commento
	{
		out = (XML_COMMENT *) malloc( sizeof(XML_COMMENT) ); // Alloco per la copia
		if ( out != NULL ) // Controllo l'allocazione
		{
			memcpy( out, comment, sizeof(XML_COMMENT) ); // Copia commenti
			// --- Copio tutti i sottocampi char pointer ---
			if ( comment->value != NULL )
			{
				out->value  = (char *) malloc( sizeof(char) * ( strlen( comment->value ) + 1 ) ); // -MALLOC
				strcpy( out->value , comment->value );
			}
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la copia di sezioni dati XML.
/// Rialloca la memoria necessaria per contentere l'elemento XML <cdata> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [10.02.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_CDATA_SECTION * XMLCopyCData( XML_CDATA_SECTION * cdata )
{
	XML_CDATA_SECTION * out = NULL;

	if ( cdata ) // Controllo la sezione
	{
		out = (XML_CDATA_SECTION *) malloc( sizeof(XML_CDATA_SECTION) ); // Alloco per la copia
		if ( out ) // Controllo l'allocazione
		{
			void * ptr = memcpy( out, cdata, sizeof(XML_CDATA_SECTION) ); // Copia la sezione
			if ( ptr ) // Controllo la copia
			{
				// --- Copio tutti i sottocampi char pointer ---
				if ( cdata->value )
				{
					out->value  = (char *) malloc( sizeof(char)*(strlen(cdata->value)+1) );
					strcpy( out->value, cdata->value );
				}
			}
			else
			{
				free( out );
				out = NULL;
			}
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la copia di istruzioni XML.
/// Rialloca la memoria necessaria per contentere l'elemento XML <instr> e ne
/// copia il contenuto.
/// Restituisce il puntatore alla nuova struttura oppure NULL in caso di errore.
///
/// \date [10.02.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_PROC_INSTRUCTION * XMLCopyInstruction( XML_PROC_INSTRUCTION * instr )
{
	XML_PROC_INSTRUCTION * out = NULL;

	if ( instr ) // Controllo l'istruzione
	{
		out = (XML_PROC_INSTRUCTION *) malloc(sizeof(XML_PROC_INSTRUCTION) ); // Alloco per la copia
		if ( out ) // Controllo l'allocazione
		{
			void * ptr = memcpy( out, instr, sizeof(XML_PROC_INSTRUCTION) ); // Copia l'istruzione
			if ( ptr ) // Controllo la copia
			{
				// --- Copio tutti i sottocampi char pointer ---
				if ( instr->value )
				{
					out->value  = (char *) malloc( sizeof(char)*(strlen(instr->value)+1) );
					strcpy( out->value, instr->value );
				}
			}
			else
			{
				free( out );
				out = NULL;
			}
		}
	}

	return out;
}

//==============================================================================
/// Funzione per la lettura di linee XML.
/// Legge da una sorgente XML <source> una singola linea di testo che viene
/// scritta in un array di caratteri <buffer> appositamente allocato secondo
/// la lunghezza effettiva della linea <size>.
/// Restituisce il puntatore alla n.t. stringa di caratteri della riga letta.
/// In caso di errore restituisce NULL.
///
/// \date [02.04.2004]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
char * XMLGets( XML_SOURCE * source, int size )
{
	char * Line     = NULL; // Riga letta da restituire
	char * a_Line         ; // Riga di appoggio

	if ( source ) // Controllo se ho specificato una sorgente XML
	{
		size = (size>XML_MAX_CHAR) ? XML_MAX_CHAR:size; // Ridefinisco la dimensione se non valida
		if ( source->file ) // Controllo se c'� un handler di file per la sorgente
		{
			Line = (char *) malloc( size+1 ); // Alloco la memoria
			a_Line = Line; // Copio il contenuto del puntatore
			a_Line = fgets( Line, size, source->file ); // Leggo una riga
			if ( a_Line == NULL )
			{
				// Se la riga � nulla
				free( Line ); // Libero la memoria
				Line = NULL;
			}
		}
	}

	return Line;
}

//==============================================================================
/// Funzione per la scrittura di linee XML.
/// Scrive su una sorgente XML <source> uno stream di caratteri che viene letto
/// da un array di caratteri <buffer> precedentemente allocato.
///
/// \date [22.01.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLPuts ( XML_SOURCE * source, char * buffer )
{
	int ret_code;
	FILE * stream;

	if ( source )
	{
		stream = source->file;
		if ( stream )
		{
			if ( buffer )
			{
				ret_code = fputs( buffer, stream );
			}
			else
			{
				ret_code = XML_ERROR_INVALID_BUFFER;
			}
		}
		else
		{
			ret_code = XML_ERROR_INVALID_STREAM;
		}
	}
	else
	{
		ret_code = XML_ERROR_INVALID_SOURCE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per il chopping di linee XML.
/// Elabora una singola linea di testo XML eliminando gli spazi finali e tutti
/// i caratteri di controllo.
///
/// \date [08.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLChop( char * line )
{
	int len;
	int EOLCount = 0;
	int CharCount = 0;
	char * a_Line;

	if ( line != NULL )
	{
		len = strlen( line );

		for ( int i = 0; i < (len); i++ )
		{
			if ( line[i] == '\n' ) EOLCount++;
		}
	}

	if ( EOLCount )
	{
		a_Line = (char *) malloc( len-EOLCount+1 );

		for ( int i = 0; i < (len); i++ )
		{
			if ( line[i] != '\n' )
			{
				a_Line[CharCount] = line[i];
				CharCount++;
			}
		}

		a_Line[CharCount] = NULL;

		free( line );
		line = a_Line;
	}

	return line;
}

//==============================================================================
/// Funzione per il trimming di linee XML.
/// Elabora una singola linea di testo XML eliminando tutti i caratteri di
/// controllo.
///
/// \date [09.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLTrim( char * line, int size )
{
	char  * cpAPP; // Array di caratteri di appoggio
	char    cp;
	int     out_size;
	char  * out;

	out_size = 0;

	if ( line == NULL ) return NULL;

	cpAPP = (char *) malloc( size + 1 );
	for ( int c = 0; c < size; c++ )
	{
		cp = line[c]; // recupero il puntatore al carattere c-esimo della riga
		if ( cp == '\0' )
		{
		  c=size;
		}
		if ( cp == '\n' || cp == '\r' || cp == '\t' )
		{

		}
		else
		{
			cpAPP[out_size] = cp;
		    out_size++;
		}
	}
	//free (line);
	out = (char *) malloc( out_size );
	memset( out, 0, out_size );
	for ( int c = 0; c < out_size; c++ )
	{
		out[c] = cpAPP[c];
	}
	free ( cpAPP );

	return out;
}

//==============================================================================
/// Funzione per la comparazione di un token.
/// Esegue la comparazione di un token fornito nel campo <token> con una
/// determinata porzione di una riga XML <line> partendo dalla posizione
/// <offset>.
///
/// \date [20.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLTokCmp( char * line, char * token, int offset )
{
	bool ret_code = true;
	int token_len;
	int line_len;

	if (line == NULL || token == NULL) // Le stringhe non sono valide
	{
		ret_code = false;
	}

	token_len = strlen( token );
	line_len = strlen( line );

	if ( line_len >= (token_len + offset) )
	{
		for ( int c = 0; c < token_len; c++ )
		{
			if ( line[offset+c] != token[c] )
			{
				ret_code = false;
			}
		}
	}
	else
	{
		ret_code = false;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare il contenuto di una riga.
/// Esegue la comparazione di ogni carattere di una riga con un carattere dato.
/// Restituisce true se tutti i caratteri corrispondono altrimenti restituisce
/// false.
///
/// \date [20.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLFilled( char * line, char c )
{
	bool ret_code = true;
	int line_len;

	if ( line == NULL ) // Verifico se la riga � valida
	{
		ret_code = false;
	}
	else
	{
		line_len = strlen(line);
        for (int i=0; i<line_len; i++)
        {
			if (line[i] != c)
			{
				ret_code = false;
			}
		}
	}

	return ret_code;
}

//==============================================================================
/// Funzione per l'estrazione di elementi XML da una riga.
/// Elabora una singola linea di testo XML eliminando tutti i caratteri di
/// controllo.
///
/// \date [05.03.2007]
/// \author Enrico Alborali, Riccardo Venturini
/// \version 0.06
//------------------------------------------------------------------------------
int XMLExtract( XML_ELEMENT * xmle, char * line )
{
	int             ret_code;
	int             line_len;
	int             tag_name_offset;
	int             tag_name_len;
//	int             attrib_len;
	int             attrib_name_offset;
	int             attrib_name_len;
	int             attrib_value_offset;
	int             attrib_value_len;
	int             msf_state;
	char            line_char;
	char            line_app[XML_MAX_CHAR*100];
	XML_ATTRIBUTE * xmla = NULL;
	XML_ATTRIBUTE * xmlapp = NULL;
	// Lista Flag
//	bool           tag_name_found;
//	bool           tag_name_ended;
//	bool           tag_property_found;
//	bool           tag_property_ended;

	// Inizializazione Flag
	msf_state = XML_E_XTR_TAG_SEARCH;

	ret_code = 0;
	line_len = strlen( line ); // Recupero la lunghezza della riga

	for ( int c = 0; c < line_len; c++ )
	{
		line_char = line[c]; // recupero il carattere c-esimo della riga

		switch ( msf_state )
		{
			case XML_E_XTR_TAG_SEARCH :
				if ( line_char == '<' )
				{
					msf_state = XML_E_XTR_NAME_SEARCH;
				}
			break;
			case XML_E_XTR_NAME_SEARCH :
				if ( line_char != ' ' )
				{
					msf_state = XML_E_XTR_NAME_SAVE;
					tag_name_len = 1;
					tag_name_offset = c;
					line_app[0] = line_char;
				}
			break;
			case XML_E_XTR_NAME_SAVE :
			    if ( line_char != ' ' && line_char != '>' && line_char != '/' )
			    {
			    	line_app[c-tag_name_offset] = line_char;
			    	tag_name_len = c-tag_name_offset+1;
			    }
			    else
			    {
			    	char * name = (char *) malloc (tag_name_len+1);
			    	for ( int i = 0; i < tag_name_len; i++ )
			    	{
			    		name[i] = line_app[i]; // copio l'i-esimo carattere del nome tag
			    	}
			    	name[tag_name_len] = '\0';
			    	//* BUGFIX 20050901-01
			    	// Cancello la memoria occupata dal nome temporaneo dell'elemento XML
			    	// che � stata assegnata nella funzione XMLCreateElement.
			    	if ( xmle->name != NULL )
			    	{
			    		try
			    		{
			    			free( xmle->name ); // -FREE
			    		}
			    		catch(...)
			    		{
			    			/* TODO -oEnrico -cError : Gestione dell'errore critico */
			    		}
			    	}
			    	//*/
			    	xmle->name = &name[0];
			    	if (line_char == ' ') msf_state = XML_E_XTR_EOA_SEARCH;
			    	if (line_char == '>') msf_state = XML_E_XTR_IDLE;
			    	if (line_char == '/') msf_state = XML_E_XTR_END_SEARCH;
				}
		    break;
		    case XML_E_XTR_EOA_SEARCH :
		    	if ( line_char == '/' && line_char == '?' ) // Chiusura Tag
		    	{
		    		msf_state = XML_E_XTR_END_SEARCH;
		    	}
		    	if ( line_char == '>' ) // Fine Tag
		        {
		        	msf_state = XML_E_XTR_IDLE;
		        }
		        if ( line_char != ' ' && line_char != '/' && line_char != '>' && line_char != '?' ) // Attrib
		        {
		        	attrib_name_offset = c;
		        	line_app[0] = line_char;
		        	msf_state = XML_E_XTR_ATTRIB_NAME_SAVE;
		    	}
		    break;
		    case XML_E_XTR_ATTRIB_NAME_SAVE :
		    	if ( line_char != ' ' && line_char != '=' && line_char != '>' )
		    	{
		    		line_app[c-attrib_name_offset] = line_char;
		    		attrib_name_len = c-attrib_name_offset+1;
		    	}
		    	else
		    	{
		    		char * name = (char *) malloc (sizeof(char)*(attrib_name_len+1));
		    		for ( int i = 0; i < attrib_name_len; i++ )
		    		{
		    			name[i] = line_app[i]; // copio l'i-esimo carattere del nome attrib
		    		}
		    		name[attrib_name_len] = '\0';
		    		xmlapp = xmla;
		    		xmla = XMLCreateAttribute(name, xmle, NULL);
		    		if ( xmle->attrib == NULL ) // Se � il primo attributo
		    		{
		    			xmle->attrib = xmla; // Linko il primo attributo
		    		}
		    		else
		    		{
		    			XMLAttributeLink( xmlapp, xmla );
		    		}
		    		attrib_value_offset = c;
		    		xmle->attrib_count++; // Incremento il contatore di attributi

		    		if ( line_char == ' ' )
		    		{
		    			msf_state = XML_E_XTR_ATTRIB_EQUAL_SEARCH;
		    		}
		    		if ( line_char == '=' )
		    		{
		    			msf_state = XML_E_XTR_ATTRIB_VALUE_SEARCH;
		    		}
		    		if ( line_char == '>' )
		    	    {
		    	    	// In questo caso l'attributo non ha un valore esplicito e viene
		    	    	// considerato un bool vero quindi viene assegnato un valore "true".
		    	    	xmla->value = (char *) malloc(sizeof(char)*5);
		    			xmla->value[0] = 't';
		    	    	xmla->value[1] = 'r';
		    	    	xmla->value[2] = 'u';
		    	    	xmla->value[3] = 'e';
		    	    	xmla->value[4] = '\0';
		    	    	xmla->length = 4;
		    	    	xmla->pos = xmle->attrib_count-1;
		    	    	msf_state = XML_E_XTR_IDLE;
		    	    }
		    	}
		    break;
		    case XML_E_XTR_ATTRIB_EQUAL_SEARCH :
		    	if ( line_char == '=' ) // Uguale
		    	{
		    		msf_state = XML_E_XTR_ATTRIB_VALUE_SEARCH;
		    	}
		    	if ( line_char != ' ' && line_char != '=' )
		    	{
		    		// In questo caso l'attributo non ha un valore esplicito e viene
		    		// considerato un bool vero quindi viene assegnato un valore "true".
		    		xmla->value = (char *) malloc( sizeof(char)*5 );
		    		xmla->value[0] = 't';
		    		xmla->value[1] = 'r';
		    		xmla->value[2] = 'u';
		    		xmla->value[3] = 'e';
		    		xmla->value[4] = '\0';
		    		xmla->length = 4;
		    		xmla->pos = xmle->attrib_count-1;
		    		attrib_name_offset = c;
		    		msf_state = XML_E_XTR_ATTRIB_NAME_SAVE;
		    	}
		    break;
		    case XML_E_XTR_ATTRIB_VALUE_SEARCH :
		    	if ( line_char == '\"' ) // Inizio valore
		    	{
		    		// --- BUGFIX by Ric 05.03.2007 ---
		    		attrib_value_len = 0;
		    		// ---
		    		attrib_value_offset = c;
		    		msf_state = XML_E_XTR_ATTRIB_VALUE_SAVE;
		    	}
		    break;
		    case XML_E_XTR_ATTRIB_VALUE_SAVE :
		    	if ( line_char == '\"' ) // Fine vaore
		    	{
		    		char * value = (char *) malloc( attrib_value_len + 1 );
		    		for ( int i = 0; i < attrib_value_len; i++ )
		    		{
		    			value[i] = line_app[i]; // copio l'i-esimo car. del valore attrib
		    		}
		    		value[attrib_value_len] = '\0';
		    		xmla->value 	= value;
		    		xmla->length	= attrib_value_len;
		    		xmla->pos		= xmle->attrib_count-1;
		    		msf_state 		= XML_E_XTR_EOA_SEARCH;
		    	}
		    	else
		    	{
		    		line_app[c-attrib_value_offset-1] = line_char;
		    		attrib_value_len = c-attrib_value_offset;
		    	}
		    break;

			default: msf_state = XML_E_XTR_IDLE;
		}
	}

	ret_code = msf_state;

	return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di elementi XML fratelli.
/// Imposta i campi <next> e <prev> rispettivamente dell'elemento <first> e
/// <second>.
///
/// \date [15.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLElementLink( XML_ELEMENT * first, XML_ELEMENT * second )
{
	int ret_code = 0;

	if ( first )	first->next = second;
	if ( second )	second->prev = first;

    return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di elementi XML padre e figlio.
/// Imposta i campi <element> e <parent> rispettivamente dell'elemento <parent>
/// e <child>.
///
/// \date [15.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLElementUpLink( XML_ELEMENT * parent, XML_ELEMENT * child )
{
	int ret_code = 0;

	if ( parent ) parent->child = child;
	if ( child ) child->parent = parent;

	return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di attributi XML fratelli.
/// Imposta i campi <next> e <prev> rispettivamente dell'elemento <first> e
/// <second>.
///
/// \date [15.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLAttributeLink( XML_ATTRIBUTE * first, XML_ATTRIBUTE * second )
{
	int ret_code = 0;

	if ( first ) first->next = second;
	if ( second ) second->prev = first;

	return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di attributi XML all'elemento XML padre.
/// Imposta i campi <attribute> e <parent> rispettivamente dell'elemento
/// <element> e <attrib>.
///
/// \date [15.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLAttributeUpLink( XML_ELEMENT * element, XML_ATTRIBUTE * attrib )
{
	int ret_code = 0;
	XML_ELEMENT * xmle;

	xmle = (XML_ELEMENT *) attrib->parent; // Mi salvo l'attuale puntatore al <parent> dell'attr.

	if (xmle == NULL)
	{

	}
	else
	{
		ret_code = 1; // Il puntatore <parent> della struttura <attrib> non era NULL
	}

	if ( attrib ) attrib->parent = element;
	if ( element )element->attrib = attrib;

	return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di testi XML fratelli.
/// Imposta i campi <next> e <prev> rispettivamente dell'elemento <first> e
/// <second>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLTextLink( XML_TEXT * first, XML_TEXT * second )
{
	int ret_code = 0;

	if ( first )	first->next = second;
	if ( second )	second->prev = first;

	return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di testi XML all'elemento XML padre.
/// Imposta i campi <text> e <parent> rispettivamente dell'elemento
/// <element> e <object>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLTextUpLink(XML_ELEMENT * element, XML_TEXT * object)
{
  int ret_code = 0;

  if (object) object->parent = element;
  if (element) element->text = object;

  return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di commenti XML fratelli.
/// Imposta i campi <next> e <prev> rispettivamente dell'elemento <first> e
/// <second>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLCommentLink(XML_COMMENT * first, XML_COMMENT * second)
{
  int ret_code = 0;

  if (first) first->next = second;
  if (second) second->prev = first;

  return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di commenti XML all'elemento XML padre.
/// Imposta i campi <comment> e <parent> rispettivamente dell'elemento
/// <element> e <object>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLCommentUpLink(XML_ELEMENT * element, XML_COMMENT * object)
{
  int ret_code = 0;

  if (object) object->parent = element;
  if (element) element->comment = object;

  return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di sezioni dati XML fratelli.
/// Imposta i campi <next> e <prev> rispettivamente dell'elemento <first> e
/// <second>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLCDataLink(XML_CDATA_SECTION * first, XML_CDATA_SECTION * second)
{
  int ret_code = 0;

  if (first) first->next = second;
  if (second) second->prev = first;

  return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di sezioni dati XML all'elemento XML padre.
/// Imposta i campi <cdata> e <parent> rispettivamente dell'elemento
/// <element> e <object>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLCDataUpLink(XML_ELEMENT * element, XML_CDATA_SECTION * object)
{
  int ret_code = 0;

  if (object) object->parent = element;
  if (element) element->cdata = object;

  return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di istruzioni XML fratelli.
/// Imposta i campi <next> e <prev> rispettivamente dell'elemento <first> e
/// <second>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLInstructionLink(XML_PROC_INSTRUCTION * first, XML_PROC_INSTRUCTION * second)
{
  int ret_code = 0;

  if (first) first->next = second;
  if (second) second->prev = first;

  return ret_code;
}

//==============================================================================
/// Funzione per il collegamento di istruzioni XML all'elemento XML padre.
/// Imposta i campi <instruction> e <parent> rispettivamente dell'elemento
/// <element> e <object>.
///
/// \date [16.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLInstructionUpLink(XML_ELEMENT * element, XML_PROC_INSTRUCTION * object)
{
  int ret_code = 0;

  if (object) object->parent = element;
  if (element) element->instruction = object;

  return ret_code;
}

//==============================================================================
/// Funzione di comparazione posizioni entit� XML.
/// Esegue la comparazione delle posizioni di due entit� XML fornite come
/// parametri e restituisce valore -1 in caso di ordine crescente, +1 in caso di
/// ordine decrescente, 0 in caso di posizione uguale oppure 666 se una o
/// entrambe le strutture fornite non sono valide.
///
/// \date [29.01.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLCmpEntity(const void * xE1, const void * xE2)
{
  int ret_code;
  XML_ENTITY * xEE1;
  XML_ENTITY * xEE2;

  if (xE1 && xE2)
  {
    xEE1 = (XML_ENTITY *) xE1;
    xEE2 = (XML_ENTITY *) xE2;
    if (xEE1->pos <  xEE2->pos) ret_code = -1;
    if (xEE1->pos == xEE2->pos) ret_code = 0;
    if (xEE1->pos >  xEE2->pos) ret_code = 1;
  }
  else
  {
    ret_code = 666;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per copiare stringhe
/// Effettua la copia della stringa fornita nel parametro <src> restituendo il
/// puntatore alla nuova stringa allocata e copiata.
/// Se viene fornito il parametro <dest> diverso da NULL viene cancellata la
/// memoria allocata (serve per cancellare la stringa prima di aggiornarla).
/// Restituisce NULL se la stringa sorgente <src> non � valida.
/// NOTA: lasciare il campo <dest> a NULL se il ptr alla stringa di destinazione
///       punta ad una locazione di memoria che non deve essere cancellata!
///
/// ESEMPIO:
/// /* alla fine str_old = "ciao" */
/// str_old = (char*)malloc( sizeof( char ) * 5 );
/// strcpy( str_old, "oaic" );
/// str_new = (char*)malloc( sizeof( char ) * 5 );
/// strcpy( str_new, "ciao" );
/// str_old = XMLStrCpy( str_old, str_new );
///
/// \date [04.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLStrCpy( char * dest, char * src )
{
  char  * res     = NULL; // Nuova stringa
  int     src_len = 0   ; // Lunghezza stringa originale

  // --- Controllo che la stringa di destinazione non sia usata altrimenti la cancello ---
  if ( dest != NULL ) free( dest );
  // --- Copio la stringa <src> in una nuova locazione di memoria ---
  if ( src != NULL )
  {
    src_len = strlen( src );
    res = (char*)malloc( sizeof(char) * ( src_len + 1 ) );
    strcpy( res, src );
  }
  else
  {
    res = NULL;
  }

  return res;
}

//==============================================================================
/// Funzione per concatenare stringhe
///
/// \date [20.07.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLStrCat( char * dest, char * src )
{
  char  * res       = NULL; // Nuova stringa
  int     src_len   = 0   ; // Lunghezza stringa originale
  int     dest_len  = 0   ;

  try
  {
    // --- Controllo che la stringa di destinazione non sia usata altrimenti la cancello ---
    if ( dest != NULL )
    {
      dest_len = strlen( dest );
    }
    // --- Copio la stringa <src> in una nuova locazione di memoria ---
    if ( src != NULL )
    {
      src_len = strlen( src );
      res = (char*)malloc( sizeof(char) * ( dest_len + src_len + 1 ) );
      if ( dest_len > 0 )
      {
        strcpy( &res[0], dest );
        free( dest ); // --- Cancello la stringa precedente ---
      }
      if ( src_len > 0 )
      {
        strcpy( &res[dest_len], src );
      }
    }
    else
    {
      res = dest;
    }
  }
  catch(...)
  {
    res = NULL;
  }

  return res;
}

//==============================================================================
/// Funzione per ottenere il ptr al percorso di default per i file XML
///
/// \date [21.07.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLGetDefaultPath( void )
{
  char * path = NULL;

  path = XMLSourceDefaultPath;

  return path;
}

//==============================================================================
/// Funzione per settare il percordo di default per i file XML
///
/// \date [20.07.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLSetDefaultPath( char * path )
{
  int ret_code = XML_NO_ERROR; // Codice di ritorno

  try
  {
    if ( path != NULL )
    {
      XMLSourceDefaultPath = XMLStrCpy( XMLSourceDefaultPath, path );
    }
    else
    {
      if ( XMLSourceDefaultPath != NULL ) free( XMLSourceDefaultPath );
    }
  }
  catch(...)
  {
    ret_code = XML_CRITICAL_ERROR;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per controllare l'integrit� di una struttura XML
///
/// \date [07.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLCheck( XML_SOURCE * source )
{
  int             ret_code  = XML_NO_ERROR; // Codice di ritorno
  XML_ENTITY    * entity    = NULL;
  XML_ATTRIBUTE * attrib    = NULL;
  char          * value     = NULL;

  if ( source != NULL )
  {
    // --- Controllo la lista delle entit� XML ---
    entity = source->first_entity;
    while ( entity != NULL )
    {
      switch ( entity->type )
      {
        case XML_ENTITY_ATTRIBUTE:
          attrib = (XML_ATTRIBUTE*)entity->structure;
          if ( attrib != NULL )
          {
            try
            {
              value = attrib->name;
            }
            catch(...)
            {
              ret_code = XML_CRITICAL_ERROR;
            }
          }
          else
          {
            ret_code = XML_ERROR_INVALID_ATTRIBUTE;
          }
        break;
      }
      entity = entity->next;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_SOURCE;
  }

  return ret_code;
}


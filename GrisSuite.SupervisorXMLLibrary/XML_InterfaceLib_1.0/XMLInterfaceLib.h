//==============================================================================
// Telefin XML Interface Library 1.0
//------------------------------------------------------------------------------
// Header Libreria di interfacciamento XML (XMLInterfaceLib.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.05 (09.09.2004 -> 21.08.2012)
//
// Copyright: 	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:      	richiede XMLInterfaceLib.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [09.09.2004]:
// - Prima versione della libreria.
// 0.02 [10.09.2004]:
// - Eliminata la funzione XMLAddIntFunction.
// - Modificata la funzione XMLInterfaceLibInit.
// 0.03 [06.04.2005]:
// - Modificata la funzione XMLAddLibFunction.
// - Eliminata la funzione XMLIL_CRC16x5_3434.
// 0.04 [29.08.2005]:
// - Aggiunta la funzione XMLInterfaceLibClear.
//==============================================================================

#ifndef XMLInterfaceLibH
#define XMLInterfaceLibH

#define XML_INTLIB_MAX_FUNCTIONS                  1000

#define XML_INTLIB_ERROR_FUNCTIONS_ARRAY_FULL        12000
#define XML_INTLIB_ERROR_FUNCTION_ID_ALREADY_EXISTS  12001

//==============================================================================
/// Struttura di riferimento ad una funzione.
///
/// \date [09.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _XML_INT_FUNCTION
{
  int                     id                ; ///< Cod. numerico della funzione
  void                  * ptf               ; ///< Puntatore alla funzione
  char                  * input             ; ///< Definizione input
  char                  * output            ; ///< Definizione output
} XML_INT_FUNCTION;
//------------------------------------------------------------------------------

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
extern XML_INT_FUNCTION   XMLIntFunctions[XML_INTLIB_MAX_FUNCTIONS]; // Lista di strutture funzione
extern int                XMLIntFunctionsCount; // Contatore delle funzioni in lista

//==============================================================================
// Dichiarazione funzioni
//------------------------------------------------------------------------------

/// Funzione per inizializzare la libreria
void  XMLInterfaceLibInit ( void );
/// Funzione per chiudere la libreria
void XMLInterfaceLibClear ( void );
/// Funzione per aggiungere una funzione alla lista della libreria
void  XMLAddLibFunction   ( int ID, void * ptf, char * output, char * input );

//------------------------------------------------------------------------------
#endif

//==============================================================================
// Telefin XML Interface Library 1.0
//------------------------------------------------------------------------------
// Libreria di interfacciamento XML (XMLInterfaceLib.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.05 (09.09.2004 -> 21.08.2012)
//
// Copyright: 	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:      	richiede XMLInterfaceLib.h,XMLInterface.h,cnv_lib.h
//------------------------------------------------------------------------------
// Version history: vedere XMLInterfaceLib.h
//==============================================================================

#pragma hdrstop

//==============================================================================
// Inclusioni
//------------------------------------------------------------------------------
#include "XMLInterfaceLib.h"
#include <stdlib.h>
#include <string.h>

//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#pragma package(smart_init)
#endif

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
XML_INT_FUNCTION   XMLIntFunctions[XML_INTLIB_MAX_FUNCTIONS]; // Lista di strutture funzione
int                XMLIntFunctionsCount;                      // Contatore delle funzioni in lista

//==============================================================================
// Implementazione funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per inizializzare la libreria
///
/// Non restituisce nessun valore.
///
/// \date [10.09.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void XMLInterfaceLibInit( void )
{
  XMLIntFunctionsCount = 0;
}

//==============================================================================
/// Funzione per chiudere la libreria
///
/// Non restituisce nessun valore.
///
/// \date [29.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void XMLInterfaceLibClear( void )
{
  XML_INT_FUNCTION * function = NULL; // Ptr di appoggio pe rstrutture funzioni XML
  // --- Ciclo sulla lista delle funzioni XML ---
  for ( int f = 0; f < XMLIntFunctionsCount; f++ )
  {
    function = &XMLIntFunctions[f];
    if ( function != NULL )
    {
      if ( function->input != NULL )
      {
        try
        {
          free( function->input );
        }
        catch(...)
        {
          function->input = NULL;
        }
      }
      if ( function->output != NULL )
      {
        try
        {
          free( function->output );
        }
        catch(...)
        {
          function->output = NULL;
        }
      }
    }
  }
  // --- Cancello la memoria occupata dalla lista delle funzioni XML ---
  memset( &XMLIntFunctions[0], 0, sizeof(XML_INT_FUNCTION) * XML_INTLIB_MAX_FUNCTIONS );
  // --- Azzero il numero di funzioni XML ---
  XMLIntFunctionsCount = 0;
}

//==============================================================================
/// Funzione per aggiungere una funzione alla lista della libreria
///
/// NOTA: Questa funzione deve essere usata solo per l'inizializzazione della
///       libreria.
///
/// Non restituisce nessun valore.
///
/// \date [06.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void XMLAddLibFunction( int ID, void * ptf, char * output, char * input )
{
  XML_INT_FUNCTION * function = &XMLIntFunctions[XMLIntFunctionsCount];
  /* Copio i parametri della funzione XML nella lista della libreria */
  function->id      = ID;
  function->ptf     = ptf;
  function->input   = (char*)malloc( sizeof(char) * ( strlen( input )  + 1 ) );
  function->output  = (char*)malloc( sizeof(char) * ( strlen( output ) + 1 ) );
  strcpy( function->input , input  );
  strcpy( function->output, output );
  /* Aggiorno il contatore globale delle funzioni */
  XMLIntFunctionsCount++;
}


//==============================================================================
// Telefin XML Interface 1.0
//------------------------------------------------------------------------------
// Header Modulo di interfacciamento strutture XML (XMLInterface.h)
// Progetto:  Telefin Supervisor Server 1.0
//
// Versione:  	1.17 (05.02.2004 -> 21.08.2012)
//
// Copyright: 	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:      	richiede XMLInterface.cpp, XMLParser.h
//------------------------------------------------------------------------------
// Version history:
// 0.00 [04.02.2004]:
// - Prima versione prototipo della libreria.
// 0.01 [09.02.2004]:
// - Inserita funzione per l'inclusione di sottostrutture XML.
// 0.02 [10.02.2004]:
// - Aggiunto controllo del livello nella funzione XMLInclude.
// - Corretta gestione dei parent e dei child nella funzione XMLInclude.
// - Aggiunta la funzione di processamento inclusioni XMLProcInc.
// 0.03 [11.02.2004]:
// - Modificata la funzione XMLGetValue.
// - Aggiunta la funzione XMLGetFirst.
// - Aggiunta la funzione XMLGetName.
// - Modificata la funzione XMLProcInc.
// 0.04 [12.02.2004]:
// - Corretta gestione di campi <prev> e <next> nella funzione XMLInclude.
// - Modificata la funzione XMLGetValue.
// 0.05 [13.02.2004]:
// - Aggiunta la funzione XMLGetArrayDim.
// - Aggiunta la funzione XMLGetArrayPos.
// - Aggiunto parametro <caller> nella chiamata della funzione XMLResolvePath.
// - Aggiunta la funzione XMLGetID.
// 0.06 [26.02.2004]:
// - Modifcata la funzione XMLGetNext con l'opzione -1 sul parametro <id>.
// - Corretto il funzionamento per la ricerca di sub-elementi in XMLResolvePath.
// 0.07 [09.03.2004]:
// - Aggiunta la funzione XMLGetEntity per cercare entit� XML in una sorgente.
// - Aggiunto il parametro <source> alla funzione XMLInclude per gestire
//   le entit� XML.
// 0.08 [10.03.2004]:
// - Modificata la funzione XMLInclude.
// - Modificata la funzione XMLProcInc.
// 0.09 [12.03.2004]:
// - Sostituita la funzione XMLInclude con una nuova che gestisce sia l'albero
//   degli elementi XML, sia la lista di entit� XML.
// - Aggiunta la struttura di definizione di funzione XML_INT_FUNCTION.
// - Aggiunta la struttura di definizione di parametro XML_INT_PARAM.
// - Aggiunta la funzione XMLGetParamsCount per ottenere il numero di parametri.
// 0.10 [15.03.2004]:
// - Aggiunto controllo intermedio dell'elemento corrente in XMLResolvePath.
// - Aggiunta la funzione XMLAddIntFunction.
// - Aggiunta la funzione XMLGetIntFunction.
// - Aggiunta la funzione XMLExecIntFunction.
// - Aggiunta la funzione XMLCreateParams.
// - Aggiunta la funzione XMLSetParam.
// - Aggiunta la funzione XMLExtractParams.
// 0.11 [16.03.2004]:
// - Aggiunta la funzione XMLGetTypeCode.
// - Aggiunta la funzione XMLASCII2Type.
// - Modificata la funzione XMLExecIntFunction.
// - Aggiunta la funzione XMLType2ASCII.
// 0.12 [17.03.2004]:
// - Modificata la funzione XMLSetParam.
// - Modificata la funzione XMLASCII2Type.
// 0.13 [18.03.2004]:
// - Aggiunta la funzione XMLStrCompare.
// - Modificata la funzione XMLResolvePath migliorando la gestione del caso di
//   elementi di tipo funzione di interfaccia.
// 0.14 [19.03.2004]:
// - Corretta la ricostruzione della posizione nella funzione XMLGetArrayPos.
// - Aggiunta la funzione XMLPackParams.
// - Aggiunta la funzione XMLSetValue.
// - Migliorata la gestione delle funzioni di interfaccia in XMLResolvePath.
// - Aggiornata la funzione XMLType2ASCII.
// - Aggiornata la funzione XMLASCII2Type.
// 0.15 [22.03.2004]:
// - Modificata la funzione XMLGetValue per risolvere i riferimenti.
// - Corretta la funzione XMLPackParams per funzionare con parametri multipli.
// - Corretta la gestione dei valori nella funzione XMLExtractParams.
// 0.16 [23.03.2004]:
// - Aggiunta definizione t_float.
// - Aggiunte due nuove implementazioni di XMLGetValue per le entit� XML.
// 0.17 [24.03.2004]:
// - Aggiunte due implementazioni (implicta ed esplicita) della funzione
//   XMLExtractValue.
// - Modificate due implementazioni della funzione XMLGetValue (per elementi e
//   entit�).
// - Modificata la gestione della raccolta parametri nella funzione
//   XMLResolvePath.
// 0.18 [25.03.2004]:
// - Modificata la gestione delle risoluzioni molti-livello nella funzione
//   XMLGetValue (implementazione per gli elementi XML).
// - Aggiunta la gestione della disallocazione per i risultati di funzioni di
//   interfaccia nella funzione XMLGetValue (implementazione per gli elementi).
// 0.19 [26.03.2004]:
// - Aggiunto caso t_float nella funzione XMLGetTypeCode.
// - Migliorata l'implementazione implicita di XMLGetValue (per elementi).
// - Aggiunta un'implementazione esplicita di XMLGetValue (per elementi e
//   percorsi).
// - Modificata l'implementazione di XMLGetValue (per sorgenti e percorsi).
// - Aggiunte definizioni di tipi esadecimali (con segno e senza segno).
// - Aggiunti casi esadecimali della funzione XMLGetTypeCode.
// - Aggiunti nuovi casi nella funzione XMLASCII2Type.
// 0.20 [29.03.2004]:
// - Aggiunto il caso dei t_float nella funzione XMLType2ASCII.
// - Corretta gestione di path nell'implementazione per elementi di XMLGetValue.
// 0.21 [09.04.2004]:
// - Aggiunta la funzione XMLGetLastAttrib.
// - Aggiunta la funzione XMLAddAttrib.
// - Aggiunta la funzione XMLNewAttrib.
// - Corretta la copia del valore nella funzione XMLExtractValue.
// 0.22 [16.04.2004]:
// - Aggiunta la funzione XMLGetChildCount.
// - Corretta l'implementazione implitica per entit� della funzione XMLGetValue.
// - Aggiunta la definizione di XML_INT_ERROR_INVALID_NAME.
// 0.23 [19.04.2004]:
// - Corretto il ciclo di ricerca elementi nella funzione XMLGetChildCount.
// 0.24 [12.05.2004]:
// - Aggiunta la funzione XMLGetValueInt.
// 0.25 [17.05.2004]:
// - Aggiunta la funzione XMLChildFunction.
// - Aggiunta la definizione del tipo XML_CHILD_FUNCTION_PTR.
// 0.26 [25.05.2004]:
// - Aggiunta la funzione XMLBitSizeOf.
// - Aggiunta la funzione XMLByteSizeOf.
// 0.27 [01.06.2004]:
// - Modificata la funzione XMLBitSizeOf.
// 0.28 [14.06.2004]:
// - Aggiunto il caso t_u_hex_8 nella funzione XMLType2ASCII.
// 0.29 [15.06.2004]:
// - Aggiunta la funzione XMLType2BIN.
// 0.30 [17.06.2004]:
// - Aggiunta la funzione XMLGetValueBool.
// - Corretta la funzione XMLGetValueInt.
// - Modificate le define dei tipi di dato.
// - Modificata la funzione XMLBitSyzeOf secondo le nuove define.
// - Aggiunta la funzione XMLCharSizeOf.
// 0.31 [18.06.2004]:
// - Modificata la funzione XMLGetTypeCode.
// 0.32 [22.06.2004]:
// - Modificata la funzione XMLAddAttrib.
// - Aggiunta la funzione XMLAddChild.
// - Aggiunti alcuni codici di errore.
// - Aggiunta la funzione XMLAddEntity.
// - Corretta la funzione XMLProcInc.
// 0.33 [23.06.2004]:
// - Corretta la funzione XMLExtractParams.
// 0.34 [02.07.2004]:
// - Corretta la funzione XMLResolvePath.
// - Corretta la funzione XMLExtractParams.
// 0.35 [30.08.2004]:
// - Corretta l'allocazione dei campi <name> nella funzione XMLResolvePath.
// 0.36 [09.09.2004]:
// - Aggiunta la funzione XMLInterfaceInit.
// - Inclusa la libreria XMLInterfaceLib 1.0.
// - Spostata la variabile globale XMLIntFunctions nella libreria
//   XMLInterfaceLib.
// - Spostata la variabile globale XMLIntFunctionsCount nella libreria
//   XMLInterfaceLib.
// - Spostata la funzione XMLAddIntFunction nella libreria XMLInterfaceLib.
// - Eliminato il codice di errore XML_INT_ERROR_FUNCTIONS_ARRAY_FULL.
// - Eliminato il codice di errore XML_INT_ERROR_FUNCTION_ID_ALREADY_EXISTS.
// 0.37 [03.02.2005]:
// - Aggiunto il codice di errore XML_INT_ERROR_INVALID_PARAM.
// - Aggiunto il codice di errore XML_INT_ERROR_FREE_FAILURE.
// - Aggiunto il codice di errore XML_INT_ERROR_STOP_PARAM_FOUND.
// - Reimplementata la funzione XMLGetToken.
// - Modificata la funzione XMLCreateParams.
// - Aggiunta la funzione XMLDeleteParams.
// - Modificata la funzione XMLSetParam.
// - Modificata la funzione XMLExtractParams.
// 0.38 [10.02.2005]:
// - Modificata la funzione XMLResolvePath.
// 0.39 [25.02.2005]:
// - Modificata la funzione XMLExtractParams.
// 0.40 [03.03.2005]:
// - Aggiunta la funzione XMLCopyValue.
// - Modificata la funzione XMLResolvePath.
// 0.41 [04.03.2005]:
// - Aggiunta la funzione XMLGetTypeString.
// - Aggiunta la funzione XMLBuildElement.
// - Modificata la funzione XMLResolvePath.
// 0.42 [09.03.2005]:
// - Modificata la funzione XMLType2ASCII.
// 0.43 [10.03.2005]:
// - Modificata la funzione XMLResolvePath.
// - Modificata la funzione XMLExtractParams.
// - Modificata la funzione XMLGetValue.
// - Modificata la funzione XMLGetValue.
// 0.44 [31.03.2005]:
// - Modificata la funzione XMLResolvePath.
// - Modificata la funzione XMLExtractParams.
// - Modificata la funzione XMLExecIntFunction.
// - Modificata la funzione XMLGetValue( XML_ELEMENT * element, char * name ).
// - Modificata la funzione XMLGetValue( XML_ELEMENT * element, char * path, char* name ).
// - Aggiunta la funzione XMLCalcValueLen.
// - Aggiunta la funzione XMLIsHexType.
// - Aggiunta la funzione XMLIsIntType.
// - Aggiunta la funzione XMLResolveRoot.
// - Aggiunta la funzione XMLResolveParent.
// - Aggiunta la funzione XMLResolveCaller.
// - Aggiunta la funzione XMLResolveFunction.
// - Aggiunta la funzione XMLResolveElement.
// - Aggiunta la funzione XMLResolvePathRAW.
// 0.45 [01.04.2005]:
// - Modificata la funzione XMLGetValue.
// - Modificata la funzione XMLGetID.
// - Aggiunta la funzione XMLIsValueRef.
// 0.46 [06.04.2005]:
// - Modificata la funzione XMLProcInc.
// - Modificata la funzione XMLNewAttrib.
// 0.47 [11.04.2005]:
// - Modificata la funzione XMLGetNext.
// 0.48 [10.06.2005]:
// - Modificata la funzione XMLASCII2Type.
// - Modificata la funzione XMLType2ASCII.
// 0.49 [20.06.2005]:
// - Aggiunto il tipo 't_time'.
// - Modificata la funzione XMLType2ASCII.
// - Modificata la funzione XMLASCII2Type.
// 0.50 [30.06.2005]:
// - Modificata la funzione XMLType2ASCII.
// 0.51 [14.07.2005]:
// - Modificata la funzione XMLBuildElement.
// - Modificata la funzione XMLSetValue.
// 0.52 [30.08.2005]:
// - Corretta la funzione XMLGetID.
// 0.53 [01.09.2005]:
// - Modificata la funzione XMLASCII2Type.
// - Modificata la funzione XMLStrCompare.
// 0.54 [06.09.2005]:
// - Aggiunta la funzione XMLInclude_NEW.
// - Modificata la funzione XMLProcInc.
// 0.55 [07.09.2005]:
// - Rinominata la funzione XMLInclude_NEW in XMLInclude sostituendo la prec.
// - Modificata la funzione XMLInclude.
// - Modificata la funzione XMLProcInc.
// 0.56 [08.09.2005]:
// - Modificata la funzione XMLGetValue( XML_ELEMENT * , char * ).
// 1.00 [27.12.2005]:
// - Aggiunta la funzione XMLInitTrash.
// - Aggiunta la funzione XMLAddToTrash.
// - Aggiunta la funzione XMLEmptyTrash.
// - Aggiunta la funzione XMLClearTrash.
// 1.01 [10.01.2006]:
// - Modificata la funzione XMLInterfaceInit.
// - Aggiunta la funzione XMLInterfaceClear.
// 1.02 [19.01.2006]:
// - Modificata la funzione XMLBuildElement.
// - Modificata la funzione XMLResolveElement.
// - Modificata la funzione XMLResolvePath.
// - Modificata la funzione XMLDeleteParams.
// - Modificata la funzione XMLExtractParams.
// - Modificata la funzione XMLGetValue.
// - Aggiunta la funzione XMLLockTrash.
// - Aggiunta la funzione XMLUnlockTrash.
// 1.03 [24.01.2006]:
// - Modificata la funzione XMLGetLastAttrib.
// 1.04 [08.03.2006]:
// - Modificata la funzione XMLType2ASCII.
// 1.05 [09.03.2006]:
// - Aggiunta la funzione XMLAddToString.
// 1.06 [29.03.2006]:
// - Modificata la funzione XMLIsHexType.
// - Modificata la funzione XMLIsIntType.
// - Aggiunta la funzione XMLHex2DecIntType.
// 1.07 [24.04.2006]:
// - Modificata la funzione XMLASCII2Type.
// 1.08 [20.06.2006]:
// - Modificata la funzione XMLType2ASCII.
// 1.09 [21.06.2006]:
// - Modificata la funzione XMLDeleteParams.
// 1.10 [28.06.2006]:
// - Aggiunta la funzione XMLRemoveAttrib.
// - Aggiunta la funzione XMLRemoveEntity.
// 1.11 [29.06.2006]:
// - Modificata la funzione XMLGetToken.
// - Modificata la funzione XMLResolvePathRAW.
// - Modificata la funzione XMLExtractParams.
// - Modificata la funzione XMLInclude.
// - Modificata la funzione XMLAddToString.
// - Aggiunta la funzione XMLAddElement.
// - Aggiunta la funzione XMLRemoveElement.
// 1.12 [01.07.2006]:
// - Modificata XMLExtractParams().
// - Corretta XMLRemoveElement().
// 1.13 [13.12.2006]:
// - Aggiunta XMLGetValueUInt32().
// 1.14 [31.07.2007]:
// - Corretta XMLProcInc() - Tolto il free della sorgente XML.
// 1.15 [07.12.2007]:
// - Corretta la funzione XMLGetAttrib().
// 1.16 [08.02.2008]:
// - Introdotta nuova gestione delle eccezioni nelle seguenti funzioni:
//		- XMLInitTrash
//		- XMLLockTrash
//		- XMLAddToTrash
//		- XMLEmptyTrash
//		- XMLUnlockTrash
//		- XMLClearTrash
//	- Modificata la funzione XMLExtractParams().
//==============================================================================

#ifndef XMLInterfaceH
#define XMLInterfaceH

#include "XMLParser.h"
#include "XMLInterfaceLib.h"

#define XML_INT_STOP_PARAM                        666

//==============================================================================
// Macro dei codici di errore
//------------------------------------------------------------------------------
#define XML_INT_NO_ERROR                          0
#define XML_INT_ERROR_INVALID_FUNCTION            3331
#define XML_INT_ERROR_INVALID_INPUT               3332
#define XML_INT_ERROR_INVALID_NAME                3333
#define XML_INT_ERROR_INVALID_ELEMENT             3334
#define XML_INT_ERROR_INVALID_CHILD               3335
#define XML_INT_ERROR_INVALID_ENTITY              3336
#define XML_INT_ERROR_INVALID_PARAM               3337
#define XML_INT_ERROR_FREE_FAILURE                3338
#define XML_INT_ERROR_STOP_PARAM_FOUND            3339
#define XML_INT_CRITICAL_ERROR                    3340
#define XML_INT_ERROR_NO_ELEMENT_FOUND            3341
#define XML_INT_ERROR_INVALID_POINTER             3342
#define XML_INT_ERROR_TRASH_FULL                  3343
#define XML_INT_ERROR_TRASH_EMPTY                 3344
#define XML_INT_ERROR_INVALID_CRITICAL_SECTION    3345
#define XML_INT_ERROR_NOT_PARENT_ELEMENT          3346
#define XML_INT_ERROR_INVALID_PARENT              3347
#define XML_INT_ERROR_ELEMENT_OWNS_CHILDS         3348
#define XML_INT_ERROR_ELEMENT_OWNS_ATTRIBS        3349

//==============================================================================
// Regola definizione tipi di dato: t_S_TYP_b     XXXYYYZZZ
//==============================================================================
// S     : segno (u=unsigned, s=signed)
// TYP   : tipologia del dato (int=intero, bin=binario, hex=esadecimale, ...)
// b     : bit di informazione
// XXX   : ID della tipologia del dato (TYP); 009=tipologie particolari
// YYY   : Numero di cifre in formato ASCII; 000=non specificato
// ZZZ   : Numero di bit in formato binario; 000=non specificato
//==============================================================================
#define t_u_int_8     1003008
#define t_u_int_16    1005016
#define t_u_int_32    1010032
#define t_u_int_64    1020064
#define t_u_int_128   1000128
#define t_s_int_8    -1003008
#define t_s_int_16   -1005016
#define t_s_int_32   -1010032
#define t_s_int_64   -1020064
#define t_s_int_128  -1000128
#define t_u_bin_1     2001001
#define t_u_bin_2     2002002
#define t_u_bin_3     2003003
#define t_u_bin_4     2004004
#define t_u_bin_5     2005005
#define t_u_bin_6     2006006
#define t_u_bin_7     2007007
#define t_u_bin_8     2008008
#define t_u_bin_16    2016016
#define t_u_bin_32    2032032
#define t_u_bin_64    2064064
#define t_u_bin_128   2128128
#define t_u_hex_8     3002008
#define t_u_hex_16    3004016
#define t_u_hex_32    3008032
#define t_u_hex_64    3016064
#define t_u_hex_128   3032128
#define t_s_hex_8    -3002008
#define t_s_hex_16   -3004016
#define t_s_hex_32   -3008032
#define t_s_hex_64   -3016064
#define t_s_hex_128  -3032128
#define t_float      -4000000
#define t_string      9000000
#define t_bool        10000000
#define t_time        8008032
#define t_datetime    8017032
//------------------------------------------------------------------------------

//==============================================================================
/// Definizione del tipo funzione per elementi XML child.
///
/// \date [17.05.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef int(* XML_CHILD_FUNCTION_PTR)(void*,XML_ELEMENT*);

//==============================================================================
/// Struttura di riferimento ad un parametro.
///
/// \date [18.06.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct {
  int                     id                ; ///< Cod. numerico del parametro
  int                     type              ; ///< Tipo di parametro
  bool                    ASCII             ; ///< Flag formato ASCII (altrimenti binario)
  void                  * value             ; ///< Valore del parametro
} XML_INT_PARAM;
//------------------------------------------------------------------------------

//==============================================================================
/// Definizione tipo puntatore a funzione.
/// \date [15.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef int(* XML_INT_FUNCTION_PTR)(void*,void*);
//------------------------------------------------------------------------------

//==============================================================================
// Dichiarazione funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo
void                XMLInterfaceInit( void );
// Funzione per chiudere il modulo
void                XMLInterfaceClear( void );
//
int                 XMLGetTypeCode      ( char * type_string );
// Funzione per generare una stringa di tipo valore
char              * XMLGetTypeString    ( int type_code );
// Funzione per convertire stringhe in valori
void *				XMLASCII2Type       ( char * string, int type);
// Funzione per convertire stringhe in valori. Caso elementi multipli (len>0)
void *				XMLASCII2Type		( char * string, int type, int len );
// Funzione per convertire stringhe in valori. Caso elemento indicizzato.
void *				XMLIndexedASCII2Type( char * string, int type, int index );
// Funzione per costruire un elemento XML con alcuni parametri
XML_ELEMENT       * XMLBuildElement     ( char * name, int id, int type, int len, bool ascii, char * value, bool temp );
// Funzione per convertire valori in stringhe
char              * XMLType2ASCII       ( void * value, int type );
// Funzione per convertire valori in stringhe (caso elementi multipli, len>1)
char * 				XMLType2ASCII( void * value, int type, int len );
//
char              * XMLType2BIN         (void * value, int type);
XML_INT_FUNCTION  * XMLGetIntFunction   (int              id      );
int                 XMLExecIntFunction  (XML_INT_FUNCTION * function, XML_INT_PARAM * output, XML_INT_PARAM * input);

int                 XMLGetArrayDim( char         * str                            );
int                 XMLGetArrayPos( char         * str    ,int           num      );
char              * XMLGetToken   ( char         * str    ,char        * sep      , int           num   );
int                 XMLStrCompare ( char * str, char * search);
// Funzione per calcolare la lunghezza di un valore
int                 XMLCalcValueLen( char * value, int type );
// Funzione per verificare se il tipo � esadecimale
bool                XMLIsHexType( int code );
// Funzione per verificare se il tipo � intero
bool                XMLIsIntType( int code );
// Funzione per verificare se il valore in realt� � una reference
bool                XMLIsValueRef( char * value );
// Funzione per risolvere l'elemento XML 'ROOT'
XML_ELEMENT       * XMLResolveRoot( XML_ELEMENT * element );
// Funzione per risolvere l'elemento XML 'PARENT'
XML_ELEMENT       * XMLResolveParent( XML_ELEMENT * element );
// Funzione per risolvere l'elemento XML 'CALLER'
XML_ELEMENT       * XMLResolveCaller( XML_ELEMENT * element, XML_ELEMENT * caller );
// Funzione per risolvere un elemento XML da un percorso
XML_ELEMENT       * XMLResolveElement( XML_ELEMENT * element, char * path, XML_ELEMENT * caller );
// Funzione per risolvere entit� XML attravrso un percorso
XML_ENTITY        * XMLResolvePath( XML_ELEMENT * element, char * path, XML_ELEMENT * caller );
//
XML_ENTITY        * XMLResolvePathRAW( XML_ELEMENT * element, char * path, XML_ELEMENT * caller, bool far_params );
//
int                 XMLGetParamsCount ( char          * params                                                        );
// Funzione per allocare una struttura parmetri interni XML
XML_INT_PARAM     * XMLCreateParams   ( int             count                                                         );
// Funzione per cancellare una struttura parametri interni XML
int                 XMLDeleteParams   ( XML_INT_PARAM * params                                                        );
// Funzione per impostare i campi di un parametro
int                 XMLSetParam       ( XML_INT_PARAM * output        , int     id          , int type, void * value  );
//
char              * XMLPackParams     ( XML_INT_PARAM * params        , char  * def_string                            );
//
XML_INT_PARAM     * XMLExtractParams  ( char          * params_string , char  * def_string  , XML_ELEMENT * element   );
//
char              * XMLGetValue       (XML_ELEMENT  * element,char      * name     );
//
char              * XMLGetValue       (XML_ELEMENT  * element                      );
//
char              * XMLGetValue       (XML_ELEMENT  * element,char      * path     );
//
char              * XMLGetValue       (XML_SOURCE   * source ,char      * path     );
//
char              * XMLExtractValue   (XML_ELEMENT  * element,char      * name     );
//
char              * XMLExtractValue   (XML_ELEMENT  * element                      );
//
char              * XMLGetValue       (XML_ENTITY   * entity                       );
//
char              * XMLGetValue       (XML_ENTITY   * entity, char      * name     );
// Funzione per ottenere valori interi (32 bit senza segno) di attributi XML.
unsigned __int32 		XMLGetValueUInt32 ( XML_ELEMENT * element, char * name 				 );
// Funzione per ottenere valori interi (16 bit senza segno) di attributi XML.
int                 XMLGetValueInt    (XML_ELEMENT  * element,char      * name     );
//
bool                XMLGetValueBool   (XML_ELEMENT  * element,char      * name     );
//
void              * XMLGetName        (XML_SOURCE   * source ,char      * x_path   );
//
char              * XMLGetName        (XML_ELEMENT  * element                      );
//
void              * XMLGetID          (XML_SOURCE   * source ,char      * x_path   );
//
int                 XMLGetID          (XML_ELEMENT  * element                      );
//
XML_ELEMENT       * XMLGetFirst       (XML_SOURCE   * source ,char      * name     );
// Funzione per ottenere elementi con un nome e un id
XML_ELEMENT       * XMLGetNext        (XML_ELEMENT  * element,char      * name     , int           id    );
//
XML_ELEMENT       * XMLGetChild       (XML_ELEMENT  * element,char      * name     );
//
XML_ELEMENT       * XMLGetElement     (XML_SOURCE   * source ,char      * x_path   );
//
XML_ELEMENT       * XMLGetArray       (XML_SOURCE   * source ,char      * x_path   );
//
XML_ATTRIBUTE     * XMLGetAttrib      (XML_ELEMENT  * element,char      * name     );
//
XML_ATTRIBUTE     * XMLGetAttrib      (XML_ELEMENT  * element,int         id       );
//
XML_ATTRIBUTE     * XMLGetAttrib      (XML_SOURCE   * source ,char      * x_path   );
//
XML_ATTRIBUTE     * XMLGetAttrib      (XML_SOURCE   * source ,char      * x_path   );
// Funzione per cercare strutture entit� XML
XML_ENTITY    * XMLGetEntity      ( XML_ENTITY    * first     , void        * structure );
// Funzione per copiare il valore di un attributo
char          * XMLCopyValue      ( XML_ELEMENT   * element   , char        * name      );
// Funzione per includere sottostrutture XML.
int             XMLInclude        ( XML_ENTITY    * inc_point , XML_ENTITY  * inc_root  );
// Funzione per il processamento delle inclusioni XML.
int             XMLProcInc        ( XML_SOURCE    * source    );
// Funzione per settare il valori di un attributo.
int             XMLSetValue       ( XML_ATTRIBUTE * attrib    , char    * value    );
XML_ATTRIBUTE * XMLGetLastAttrib  ( XML_ELEMENT   * element   );
//
XML_ATTRIBUTE * XMLNewAttrib      ( XML_ELEMENT   * element   , char    * name     ,char * value);
//
int             XMLAddAttrib      ( XML_ELEMENT   * element   , XML_ATTRIBUTE * attrib  );
// Funzione per rimuovere un attributo da un elemento XML
int             XMLRemoveAttrib   ( XML_ELEMENT   * element   , XML_ATTRIBUTE * attrib  );
// Funzione per aggiungere elementi child
int             XMLAddChild       ( XML_ELEMENT   * element   , XML_ELEMENT   * child   );
// Funzione per aggiungere un elemento XML
int             XMLAddElement     ( XML_ELEMENT   * parent    , XML_ELEMENT   * element );
// Funzione per rimuovere un elemento XML
int             XMLRemoveElement  ( XML_ELEMENT   * element   );
// Funzione per aggiungere entit� XML
int             XMLAddEntity      ( XML_ENTITY    * old_entity, XML_ENTITY * new_entity );
// Funzione per rimuovere un entita' XML (dalla lista delle entita')
int             XMLRemoveEntity   ( XML_ENTITY    * entity    );
//
int             XMLGetChildCount  ( XML_ELEMENT   * element   , bool            sublevel);
//
int             XMLChildFunction  ( XML_ELEMENT   * root      , void * output, int size, void * function);
//
int             XMLBitSizeOf      ( int             type      );
//
int             XMLByteSizeOf     ( int             type      );
//
int             XMLCharSizeOf     ( int             type      );
// Funzione per inizializzare il cestino XML
int             XMLInitTrash      ( void );
// Funzione per bloccare l'accesso al cestino XML per gli altri thread
int             XMLLockTrash      ( char * label );
// Funzione per aggiungere un puntatore di memoria al cestino XML
int             XMLAddToTrash     ( void * memptr );
// Funzione per svuotare il cestino XML
int             XMLEmptyTrash     ( void );
// Funzione per sbloccare l'accesso al cestino XML per gli altri thread
int             XMLUnlockTrash    ( void );
// Funzione per chiudere il cestino XML
int             XMLClearTrash     ( void );
// Funzione per aggiungere un pezzo di stringa
char          * XMLAddToString    ( char * old_str, char * tok_str );
// Funzione per convertire un tipo esadecimale nel rispettivo decimale
int             XMLHex2DecIntType ( int hex_type );


#endif

//==============================================================================
// Telefin XML Interface 1.0
//------------------------------------------------------------------------------
// Modulo di interfacciamento strutture XML (XMLInterface.cpp)
// Progetto:  	Telefin Supervisor Server 1.0
//
// Versione:  	1.17 (05.02.2004 -> 21.08.2012)
//
// Copyright: 	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:      	richiede XMLInterface.h,XMLInterfaceLib.h,cnv_lib.h
//------------------------------------------------------------------------------
// Version history: vedere XMLInterface.h
//==============================================================================

#pragma hdrstop
#include "XMLInterface.h"
#include "cnv_lib.h"
#include "SYSLock.h"
#include <math.h>
#include <windows.h>
//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#pragma package(smart_init)
#endif

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
#define XML_TRASH_MAX_ELEMENTS 1024
//static  _CRITICAL_SECTION  XMLTrashCriticalSection;
static  SYS_LOCK		  XMLTrashLock;
static  void *            XMLTrash[XML_TRASH_MAX_ELEMENTS];
static  int               XMLTrashCount;

//==============================================================================
// Implementazione funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per inizializzare il modulo
///
/// Non restituisce nessun valore
///
/// \date [10.01.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void XMLInterfaceInit( void )
{
  // --- Carico la libreria di interfacciamento XML ---
  XMLInterfaceLibInit();
  // --- Inizializzo il cestino XML ---
  XMLInitTrash();
}

//==============================================================================
/// Funzione per chiudere il modulo
/// Non restituisce alcun valore
/// \date [10.01.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void XMLInterfaceClear( void )
{
  // --- Chiudo il cestino XML ---
  XMLEmptyTrash( );
  XMLClearTrash( );
  // --- Chiudo la libreria di interfacciamento XML ---
  XMLInterfaceLibClear( );
}

//==============================================================================
/// Funzione per determinare il codice di tipo.
/// Data una stringa di tipo viene determinato il codice numerico corrispondente
/// al tipo.
/// In caso di errore o di tipo non riconosciuto la funzione restituisce 0.
///
/// \date [18.06.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
int XMLGetTypeCode( char * type_string )
{
  int type = 0;

  if ( type_string ) // Verifico la validit� della stringa del tipo
  {
    if ( strcmpi( type_string, "t_u_int_8" )   == 0 ) type = t_u_int_8   ;
    if ( strcmpi( type_string, "t_u_int_16" )  == 0 ) type = t_u_int_16  ;
    if ( strcmpi( type_string, "t_u_int_32" )  == 0 ) type = t_u_int_32  ;
    if ( strcmpi( type_string, "t_u_int_64" )  == 0 ) type = t_u_int_64  ;
    if ( strcmpi( type_string, "t_u_int_128" ) == 0 ) type = t_u_int_128 ;
    if ( strcmpi( type_string, "t_s_int_8" )   == 0 ) type = t_s_int_8   ;
    if ( strcmpi( type_string, "t_s_int_16" )  == 0 ) type = t_s_int_16  ;
    if ( strcmpi( type_string, "t_s_int_32" )  == 0 ) type = t_s_int_32  ;
    if ( strcmpi( type_string, "t_s_int_64" )  == 0 ) type = t_s_int_64  ;
    if ( strcmpi( type_string, "t_s_int_128" ) == 0 ) type = t_s_int_128 ;
    if ( strcmpi( type_string, "t_u_bin_1" )   == 0 ) type = t_u_bin_1   ;
    if ( strcmpi( type_string, "t_u_bin_2" )   == 0 ) type = t_u_bin_2   ;
    if ( strcmpi( type_string, "t_u_bin_3" )   == 0 ) type = t_u_bin_3   ;
    if ( strcmpi( type_string, "t_u_bin_4" )   == 0 ) type = t_u_bin_4   ;
    if ( strcmpi( type_string, "t_u_bin_5" )   == 0 ) type = t_u_bin_5   ;
    if ( strcmpi( type_string, "t_u_bin_6" )   == 0 ) type = t_u_bin_6   ;
    if ( strcmpi( type_string, "t_u_bin_7" )   == 0 ) type = t_u_bin_7   ;
    if ( strcmpi( type_string, "t_u_bin_8" )   == 0 ) type = t_u_bin_8   ;
    if ( strcmpi( type_string, "t_u_bin_16" )  == 0 ) type = t_u_bin_16  ;
    if ( strcmpi( type_string, "t_u_bin_32" )  == 0 ) type = t_u_bin_32  ;
    if ( strcmpi( type_string, "t_u_bin_64" )  == 0 ) type = t_u_bin_64  ;
    if ( strcmpi( type_string, "t_u_bin_128" ) == 0 ) type = t_u_bin_128 ;
    if ( strcmpi( type_string, "t_u_hex_8" )   == 0 ) type = t_u_hex_8   ;
    if ( strcmpi( type_string, "t_u_hex_16" )  == 0 ) type = t_u_hex_16  ;
    if ( strcmpi( type_string, "t_u_hex_32" )  == 0 ) type = t_u_hex_32  ;
    if ( strcmpi( type_string, "t_u_hex_64" )  == 0 ) type = t_u_hex_64  ;
    if ( strcmpi( type_string, "t_u_hex_128" ) == 0 ) type = t_u_hex_128 ;
    if ( strcmpi( type_string, "t_s_hex_8" )   == 0 ) type = t_s_hex_8   ;
	if ( strcmpi( type_string, "t_s_hex_16" )  == 0 ) type = t_s_hex_16  ;
    if ( strcmpi( type_string, "t_s_hex_32" )  == 0 ) type = t_s_hex_32  ;
    if ( strcmpi( type_string, "t_s_hex_64" )  == 0 ) type = t_s_hex_64  ;
    if ( strcmpi( type_string, "t_s_hex_128" ) == 0 ) type = t_s_hex_128 ;
    if ( strcmpi( type_string, "t_string" )    == 0 ) type = t_string    ;
    if ( strcmpi( type_string, "t_bool" )      == 0 ) type = t_bool      ;
    if ( strcmpi( type_string, "t_float" )     == 0 ) type = t_float     ;
  }

  return type;
}

//==============================================================================
/// Funzione per generare una stringa di tipo valore
/// Dato un codice di tipo viene determinata la stringa corrispondente al tipo.
/// In caso di tipo non riconosciuto la funzione restituisce "t_string".
///
/// \date [04.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLGetTypeString( int type_code )
{
  char * type_string = NULL;

  switch( type_code ) // Verifico la validit� della stringa del tipo
  {
    case t_u_int_8    : type_string = XMLStrCpy( NULL, "t_u_int_8"    ) ; break;
    case t_u_int_16   : type_string = XMLStrCpy( NULL, "t_u_int_16"   ) ; break;
    case t_u_int_32   : type_string = XMLStrCpy( NULL, "t_u_int_32"   ) ; break;
    case t_u_int_64   : type_string = XMLStrCpy( NULL, "t_u_int_64"   ) ; break;
    case t_u_int_128  : type_string = XMLStrCpy( NULL, "t_u_int_128"  ) ; break;
    case t_s_int_8    : type_string = XMLStrCpy( NULL, "t_s_int_8"    ) ; break;
    case t_s_int_16   : type_string = XMLStrCpy( NULL, "t_s_int_16"   ) ; break;
    case t_s_int_32   : type_string = XMLStrCpy( NULL, "t_s_int_32"   ) ; break;
    case t_s_int_64   : type_string = XMLStrCpy( NULL, "t_s_int_64"   ) ; break;
    case t_s_int_128  : type_string = XMLStrCpy( NULL, "t_s_int_128"  ) ; break;
    case t_u_bin_1    : type_string = XMLStrCpy( NULL, "t_u_bin_1"    ) ; break;
    case t_u_bin_2    : type_string = XMLStrCpy( NULL, "t_u_bin_2"    ) ; break;
    case t_u_bin_3    : type_string = XMLStrCpy( NULL, "t_u_bin_3"    ) ; break;
    case t_u_bin_4    : type_string = XMLStrCpy( NULL, "t_u_bin_4"    ) ; break;
    case t_u_bin_5    : type_string = XMLStrCpy( NULL, "t_u_bin_5"    ) ; break;
    case t_u_bin_6    : type_string = XMLStrCpy( NULL, "t_u_bin_6"    ) ; break;
    case t_u_bin_7    : type_string = XMLStrCpy( NULL, "t_u_bin_7"    ) ; break;
    case t_u_bin_8    : type_string = XMLStrCpy( NULL, "t_u_bin_8"    ) ; break;
    case t_u_bin_16   : type_string = XMLStrCpy( NULL, "t_u_bin_16"   ) ; break;
    case t_u_bin_32   : type_string = XMLStrCpy( NULL, "t_u_bin_32"   ) ; break;
    case t_u_bin_64   : type_string = XMLStrCpy( NULL, "t_u_bin_64"   ) ; break;
    case t_u_bin_128  : type_string = XMLStrCpy( NULL, "t_u_bin_128"  ) ; break;
    case t_u_hex_8    : type_string = XMLStrCpy( NULL, "t_u_hex_8"    ) ; break;
    case t_u_hex_16   : type_string = XMLStrCpy( NULL, "t_u_hex_16"   ) ; break;
    case t_u_hex_32   : type_string = XMLStrCpy( NULL, "t_u_hex_32"   ) ; break;
    case t_u_hex_64   : type_string = XMLStrCpy( NULL, "t_u_hex_64"   ) ; break;
    case t_u_hex_128  : type_string = XMLStrCpy( NULL, "t_u_hex_128"  ) ; break;
    case t_s_hex_8    : type_string = XMLStrCpy( NULL, "t_s_hex_8"    ) ; break;
	case t_s_hex_16   : type_string = XMLStrCpy( NULL, "t_s_hex_16"   ) ; break;
    case t_s_hex_32   : type_string = XMLStrCpy( NULL, "t_s_hex_32"   ) ; break;
    case t_s_hex_64   : type_string = XMLStrCpy( NULL, "t_s_hex_64"   ) ; break;
    case t_s_hex_128  : type_string = XMLStrCpy( NULL, "t_s_hex_128"  ) ; break;
    case t_string     : type_string = XMLStrCpy( NULL, "t_string"     ) ; break;
    case t_bool       : type_string = XMLStrCpy( NULL, "t_bool"       ) ; break;
    case t_float      : type_string = XMLStrCpy( NULL, "t_float"      ) ; break;
    default           : type_string = XMLStrCpy( NULL, "t_string"     ) ;
  }

  return type_string;
}

//==============================================================================
/// Funzione per convertire stringhe in valori.
/// Data una stringa di caratteri viene convertita in un valore secondo il tipo
/// specificato dal codice <type>. Viene restituito il puntatore alla variabile.
/// In caso di errore viene restituito NULL.
///
/// \date [24.04.2006]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
void * XMLASCII2Type( char * string, int type )
{
              void  * value         = NULL  ;
              bool  * value_bool            ;
              int     string_len            ;
  unsigned  __int8  * value_uint8           ;
  unsigned  __int16 * value_uint16          ;
  unsigned  __int32 * value_uint32  = NULL  ;
  unsigned  __int64 * value_uint64  = NULL  ;
			__int8  * value_int8            ;
			__int16	* value_int16			;
  //			__int32 * value_int32			;
              float * value_float           ;
              long  * value_time            ;

  if (string)
  {
    string_len = strlen(string);
    if (string_len && type!=0)
    {
      switch (type)
      {
        case t_u_int_8:
          value_uint8 = (unsigned __int8 *) malloc(sizeof(unsigned __int8));
          *value_uint8 = cnv_CharPToUInt8( string );
          value = (void*) value_uint8;
        break;
        case t_u_int_16:
          value_uint16 = (unsigned __int16 *) malloc(sizeof(unsigned __int16));
          *value_uint16 = cnv_CharPToUInt16( string );
          value = (void*) value_uint16;
        break;
        case t_u_int_32:
          value_uint32 = (unsigned __int32 *) malloc(sizeof(unsigned __int32));
          *value_uint32 = cnv_CharPToUInt32( string );
          value = (void*) value_uint32;
        break;
        case t_u_int_64:
          value_uint64 = (unsigned __int64 *) malloc(sizeof(unsigned __int64));
          *value_uint64 = cnv_CharPToUInt64( string );
          value = (void*) value_uint64;
        break;
        case t_s_int_8:
          value_int8 = (__int8 *) malloc(sizeof(__int8));
          *value_int8 = cnv_CharPToInt8( string );
          value = (void*) value_int8;
		break;
        case t_s_int_16:
          value = (__int16 *) malloc(sizeof(__int16));
        break;
        case t_s_int_32:
          value = (__int32 *) malloc(sizeof(__int32));
        break;
        case t_s_int_64:
          value = (__int64 *) malloc(sizeof(__int64));
        break;
        case t_u_hex_8:
          value_uint8 = (unsigned __int8 *) malloc(sizeof(unsigned __int8));
          *value_uint8 = cnv_CharPToUHex8( string );
          value = (void*) value_uint8;
        break;
        case t_u_hex_16:
          value_uint16 = (unsigned __int16 *) malloc(sizeof(unsigned __int16));
          *value_uint16 = cnv_CharPToUHex16( string );
          value = (void*) value_uint16;
        break;
        case t_u_hex_32:
          value_uint32 = (unsigned __int32 *) malloc(sizeof(unsigned __int32));
          *value_uint32 = cnv_CharPToUHex32( string );
          value = (void*) value_uint32;
		break;
		case t_s_hex_8:
		  value_int8 = (__int8 *) malloc(sizeof(__int8));
		  *value_int8 = cnv_CharPToSHex8( string );
		  value = (void*) value_int8;
		break;
		case t_s_hex_16:
		  value_int16 = (__int16 *) malloc(sizeof(__int16));
		  *value_int16 = cnv_CharPToSHex16( string );
		  value = (void*) value_int16;
		break;
		/*
		case t_s_hex_32:
		  value_uint32 = (unsigned __int32 *) malloc(sizeof(signed __int32));
		  *value_uint32 = cnv_CharPToSHex32( string );
		  value = (void*) value_uint32;
		break;
		*/
		case t_string:
          value = (char *) malloc(sizeof(char)*(string_len+1));
          strcpy((char*)value, string );
        break;
        case t_bool:
          value_bool = (bool *) malloc(sizeof(bool));
          *value_bool = cnv_CharPToBool( string );
          value = (void*) value_bool;
        break;
        case t_float:
          value_float = (float *) malloc(sizeof(float));
          *value_float = cnv_CharPToFloat( string );
          value = (void*) value_float;
        break;
        case t_time:
          value_time = (long*) malloc(sizeof(float));
          *value_time = cnv_CharPToTime( string );
          value = (void*) value_time;
        break;
        default: ;
      }
    }
  }

  return value;
}

//==============================================================================
/// Funzione per convertire stringhe in valori. Caso elementi multipli (len>0)
/// Data una stringa di caratteri viene convertita in un valore secondo il tipo
/// specificato dal codice <type>. Viene restituito il puntatore alla variabile.
/// In caso di errore viene restituito NULL.
///
/// \date [25.01.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
void * XMLASCII2Type( char * string, int type, int len )
{
	unsigned char *	output				=	NULL;
	void *			single_output		= 	NULL;
	int				single_size			=	0	;
	int				single_len			=	0	;
	char			single_in				[17];

	if (len <= 1) {
		output = (unsigned char *) XMLASCII2Type( string, type );
	}
	else
	{
		// Trovo la lunghezza in byte e caratteri del singolo elemento
		single_size = XMLByteSizeOf( type );
		single_len = XMLCharSizeOf( type );
		// Alloco gi� tutto l'output completo
		output = (unsigned char*)malloc(sizeof(unsigned char)*(single_size*len)); // -MALLOC
		memset((void*)output, 0, sizeof(unsigned char)*(single_size*len));

		memset((void*)&single_in[0], 0, sizeof(char)*(17));

		for (int i = 0; i < len; i++) {
			// Preparo l'i-esimo elemento
			memcpy((void*)&single_in[0], (void*)&string[(i*single_len)], (size_t)single_len);
			// Converto l'i-esimo elemento
			single_output = XMLASCII2Type( single_in, type ); // -MALLOC
			// Copio l'i-esimo output in quello completo
			memcpy((void*)&output[(i*single_size)], single_output, (size_t)single_size);
			// Elimino l'i-esimo output singolo
			free(single_output); // -FREE
		}

	}

	return (void*)output;
}

//==============================================================================
/// Funzione per convertire stringhe in valori. Caso elemento indicizzato.
/// Data una stringa di caratteri viene convertita in un valore secondo il tipo
/// specificato dal codice <type>. Viene restituito il puntatore alla variabile.
/// In caso di errore viene restituito NULL.
///
/// \date [25.01.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
void * XMLIndexedASCII2Type( char * string, int type, int index )
{
	unsigned char *	output				=	NULL;
	void *			single_output		= 	NULL;
	int				single_size			=	0	;
	int				single_len			=	0	;
	char			single_in				[17];

	/*
	if (index == 0) {
		output = (unsigned char *) XMLASCII2Type( string, type );
	}
	else
	*/
	{
		// Trovo la lunghezza in byte e caratteri del singolo elemento
		single_size = XMLByteSizeOf( type );
		single_len = XMLCharSizeOf( type );
		// Alloco gi� tutto l'output completo
		output = (unsigned char*)malloc(sizeof(unsigned char)*(single_size)); // -MALLOC
		memset((void*)output, 0, sizeof(unsigned char)*(single_size));

		memset((void*)&single_in[0], 0, sizeof(char)*(17));
		try
		{
			// Preparo l'i-esimo elemento
			memcpy((void*)&single_in[0], (void*)&string[(index*single_len)], (size_t)single_len);
			// Converto l'i-esimo elemento
			single_output = XMLASCII2Type( single_in, type ); // -MALLOC
			// Copio l'i-esimo output in quello completo
			memcpy((void*)&output[0], single_output, (size_t)single_size);
			// Elimino l'i-esimo output singolo
			free(single_output); // -FREE
		}
		catch(...)
		{
			memset((void*)output, 0, sizeof(unsigned char)*(single_size));
		}
	}

	return (void*)output;
}

//==============================================================================
/// Funzione per costruire un elemento XML con alcuni parametri
///
/// \date [19.01.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
XML_ELEMENT * XMLBuildElement( char * name, int id, int type, int len, bool ascii, char * value, bool temp )
{
  XML_ELEMENT   * element       = NULL; // Ptr alla struttura elemento XML
  XML_ATTRIBUTE * attrib        = NULL; // Ptr alla struttura attributo XML
  char          * attrib_string = NULL; // Stringa per il nome dell'attributo XML
  char          * name_string   = NULL; // Stringa per il nome dell'elemento XML
  char          * id_string     = NULL; // Stringa per l'ID dell'elemento XML
  char          * type_string   = NULL; // Stringa per il tipo di elemento XML
  char          * len_string    = NULL; // Stringa per la lunghezza del valore
  char          * ascii_string  = NULL; // Stringa per il flag di formato ASCII
  char          * value_string  = NULL; // Stringa del valore dell'elemento XML
  char          * temp_string   = NULL; // Stringa per il flag di elemento temporaneo

  if ( name != NULL ) // Controllo che il ptr al nome sia valido
  {
    name_string = XMLStrCpy( NULL, name ); // Copio il nome dell'elemento -MALLOC
    if ( temp ) XMLAddToTrash( (void*)name_string ); // -ADDTRASH
    element     = XMLCreateElement( name_string, NULL, NULL ); // Creo l'elemento XML -MALLOC
    if ( element != NULL ) // Contollo che la creazione sia andata a buon fine
    {
      if ( temp ) XMLAddToTrash( (void*)element ); // -ADDTRASH
      element->pos  = 0;
      // --- Creo l'attributo ID ---
      attrib_string = XMLStrCpy( NULL, "id" ); // Copio il nome dell'attributo -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib_string ); // -ADDTRASH
      id            = ( id < 0 ) ? 0 : ( id > 255 ) ? 255 : id;
      id_string     = XMLType2ASCII( (void*)&id, t_u_int_8 ); // Converto in formato ASCII -MALLOC
      attrib        = XMLCreateAttribute( attrib_string, element, NULL ); // -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib ); // -ADDTRASH
      XMLSetValue   ( attrib, id_string ); // Copio(!) il valore dell'attributo
      if ( temp ) XMLAddToTrash( (void*)attrib->value ); // -ADDTRASH
      XMLAddAttrib  ( element, attrib ); // Aggiuno l'attributo all'elemento
      if ( id_string != NULL )
      {
        try
        {
          free( id_string ); // -FREE
        }
        catch(...)
        {
        }
      }
      // --- Creo l'attributo type ---
      attrib_string = XMLStrCpy( NULL, "type" ); // Copio il nome dell'attributo -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib_string ); // -ADDTRASH
      type_string   = XMLGetTypeString( type ); // Converto in formato ASCII -MALLOC
      attrib        = XMLCreateAttribute( attrib_string, element, NULL ); // -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib ); // -ADDTRASH
      XMLSetValue   ( attrib, type_string ); // Imposto il valore dell'attributo
      if ( temp ) XMLAddToTrash( (void*)attrib->value ); // -ADDTRASH
      XMLAddAttrib  ( element, attrib ); // Aggiuno l'attributo all'elemento
      if ( type_string != NULL )
      {
        try
        {
          free( type_string ); // -FREE
        }
        catch(...)
        {
        }
      }
      // --- Creo l'attributo len ---
      attrib_string = XMLStrCpy( NULL, "len" ); // Copio il nome dell'attributo -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib_string ); // -ADDTRASH
      len           = ( len < 0 ) ? 0 : ( len > 65535 ) ? 65535 : len;
      len_string    = XMLType2ASCII( (void*)&len, t_u_int_16 ); // Converto in formato ASCII -MALLOC
      attrib        = XMLCreateAttribute( attrib_string, element, NULL ); // -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib ); // -ADDTRASH
      XMLSetValue   ( attrib, len_string ); // Imposto il valore dell'attributo
      if ( temp ) XMLAddToTrash( (void*)attrib->value ); // -ADDTRASH
      XMLAddAttrib  ( element, attrib ); // Aggiuno l'attributo all'elemento
      if ( len_string != NULL )
      {
        try
        {
          free( len_string ); // -FREE
        }
        catch(...)
        {
        }
      }
      // --- Creo l'attributo ASCII ---
      attrib_string = XMLStrCpy( NULL, "ascii" ); // Copio il nome dell'attributo -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib_string ); // -ADDTRASH
      ascii_string  = XMLType2ASCII( (void*)&ascii, t_bool ); // Converto in formato ASCII -MALLOC
      attrib        = XMLCreateAttribute( attrib_string, element, NULL ); // -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib ); // -ADDTRASH
      XMLSetValue   ( attrib, ascii_string ); // Imposto il valore dell'attributo
      if ( temp ) XMLAddToTrash( (void*)attrib->value ); // -ADDTRASH
      XMLAddAttrib  ( element, attrib ); // Aggiuno l'attributo all'elemento
      if ( ascii_string != NULL )
      {
        try
        {
          free( ascii_string ); // -FREE
        }
        catch(...)
        {
        }
      }
      // --- Creo l'attributo value ---
      attrib_string = XMLStrCpy( NULL, "value" ); // Copio il nome dell'attributo -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib_string ); // -ADDTRASH
      value_string  = XMLStrCpy( NULL, value ); // Converto in formato ASCII -MALLOC
      attrib        = XMLCreateAttribute( attrib_string, element, NULL ); // -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib ); // -ADDTRASH
      XMLSetValue   ( attrib, value_string ); // Imposto il valore dell'attributo
      if ( temp ) XMLAddToTrash( (void*)attrib->value ); // -ADDTRASH
      XMLAddAttrib  ( element, attrib ); // Aggiuno l'attributo all'elemento
      if ( value_string != NULL )
      {
        try
        {
          free( value_string ); // -FREE
        }
        catch(...)
        {
        }
      }
      // --- Creo l'attributo temp ---
      attrib_string = XMLStrCpy( NULL, "temp" ); // Copio il nome dell'attributo -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib_string ); // -ADDTRASH
      temp_string   = XMLType2ASCII( (void*)&temp, t_bool ); // Converto in formato ASCII -MALLOC
      attrib        = XMLCreateAttribute( attrib_string, element, NULL ); // -MALLOC
      if ( temp ) XMLAddToTrash( (void*)attrib ); // -ADDTRASH
      XMLSetValue   ( attrib, temp_string ); // Imposto il valore dell'attributo
      if ( temp ) XMLAddToTrash( (void*)attrib->value ); // -ADDTRASH
      XMLAddAttrib  ( element, attrib ); // Aggiuno l'attributo all'elemento
      if ( temp_string != NULL )
      {
        try
        {
          free( temp_string ); // -FREE
        }
        catch(...)
        {
        }
      }
    }
  }

  return element;
}

//==============================================================================
/// Funzione per convertire valori in stringhe.
/// Dato un valore puntato da <value> di tipo <type> viene convertito in una
/// stringa di caratteri.
/// In caso di errore viene restituito NULL.
///
/// \date [26.09.2006]
/// \author Enrico Alborali
/// \version 0.12
//------------------------------------------------------------------------------
char * XMLType2ASCII( void * value, int type )
{
  char              * output        = NULL;
  bool                value_bool          ;
  unsigned __int8     value_uint8         ;
  unsigned __int16    value_uint16        ;
  unsigned __int32    value_uint32        ;
  unsigned __int64    value_uint64        ;
  float               value_float         ;
  long                value_time          ;
  int                 string_len          ;

  if ( value != NULL )
  {
    if ( type!=0 )
    {
      switch ( type )
      {
        case t_u_int_8:
          value_uint8 = *( (unsigned __int8*)value );
          output = cnv_UInt8ToCharP( value_uint8 );
        break;
        case t_u_int_16:
          value_uint16 = *( (unsigned __int16*)value );
          output = cnv_UInt16ToCharP( value_uint16 );
        break;
        case t_u_int_32:
          value_uint32 = *( (unsigned __int32*)value );
          output = cnv_UInt32ToCharP( value_uint32 );
        break;
        case t_u_int_64:
          value_uint64 = *( (unsigned __int64*)value );
          output = cnv_UInt64ToCharP( value_uint64 );
        break;
        /*
        case t_u_int_128:
        break;
        */
        case t_u_hex_8:
          value_uint8 = *( (unsigned __int8*)value );
          output = cnv_UHex8ToCharP( value_uint8 );
        break;
        case t_u_hex_16:
          value_uint16 = *( (unsigned __int16*)value );
          output = cnv_UHex16ToCharP( value_uint16 );
        break;
        case t_u_hex_32:
          value_uint32 = *( (unsigned __int32*)value );
          output = cnv_UHex32ToCharP( value_uint32 );
        break;
        /*
        case t_u_hex_64:
        break;
        case t_u_hex_128:
        break;
        case t_s_int_8:
        break;
        case t_s_int_16:
        break;
        case t_s_int_32:
        break;
        case t_s_int_64:
        break;
        case t_s_int_128:
        break;
        //*/
        case t_string:
          try
          {
            string_len = strlen( (char*)value );
          }
          catch(...)
          {
            string_len = 0;
          }
          if ( string_len > 0 )
          {
            try
            {
              output = (char*)malloc(sizeof(char)*(string_len+1));
            }
            catch(...)
            {
              output = NULL;
            }
            if ( output != NULL )
            {
              try
              {
                memcpy( output, value, sizeof(char)*(string_len) );
                output[string_len] = NULL;
              }
              catch(...)
              {
                output = NULL;
              }
            }
          }
        break;
        case t_bool:
          value_bool = *((bool*)value);
          output = cnv_BoolToCharP( value_bool );
        break;
        case t_float:
          value_float = *((float*)value);
          output = cnv_FloatToCharP( value_float );
        break;
        case t_time:
          value_time = *((long*)value);
          output = cnv_TimeToCharP( value_time );
        break;
        case t_datetime:
          value_time = *((long*)value);
          output = cnv_DateTimeToCharP( value_time );
        break;
      }
    }
  }

  return output;
}

//==============================================================================
/// Funzione per convertire valori in stringhe (caso elementi multipli, len>1)
/// Dato un valore puntato da <value> di tipo <type> viene convertito in una
/// stringa di caratteri.
/// In caso di errore viene restituito NULL.
///
/// \date [26.09.2006]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
char * XMLType2ASCII( void * value, int type, int len )
{
	char *			output        		= 	NULL;
	char *			single_output		= 	NULL;
	int				single_size			=	0	;
	int				single_len			=	0	;
	char			single_in				[8]	;
	unsigned char * value_in			=	NULL;
	int				single_output_len	=	0	;

	if (len <= 1) {
		output = XMLType2ASCII( value, type );
	}
	else
	{
		value_in = (unsigned char*)value;
		// Trovo la lunghezza in byte del singolo elemento
		single_size = XMLByteSizeOf( type );
		single_len = XMLCharSizeOf( type );
		// Alloco gi� tutto l'output completo
		output = (char*)malloc(sizeof(char)*((single_len*len)+1)); // -MALLOC
		memset((void*)output, 0, sizeof(char)*((single_len*len)+1));
		memset((void*)output, '0', sizeof(char)*(single_len*len));
		memset((void*)&single_in[0], 0, sizeof(char)*(8));
		for (int i = 0; i < len; i++) {
			// Preparo l'i-esimo elemento
			memcpy((void*)&single_in[0], (void*)&value_in[(i*single_size)], (size_t)single_size);
			// Converto l'i-esimo elemento
			single_output = XMLType2ASCII( single_in, type ); // -MALLOC
			single_output_len = strlen(single_output);
			// Copio l'i-esimo output in quello completo
			memcpy((void*)&output[(i*single_len)+(single_len-single_output_len)], (void*)&single_output[0], (size_t)single_len);
			// Elimino l'i-esimo output singolo
			free(single_output); // -FREE
		}
	}

	return output;
}

//==============================================================================
/// Funzione per convertire valori in dati binari.
/// Dato un valore puntato da <value> di tipo <type> viene convertito in uno o
/// pi� byte di dati binari.
/// In caso di errore viene restituito NULL.
///
/// \date [15.06.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLType2BIN(void * value, int type)
{
  char              * output = NULL;
  bool                value_bool;
  unsigned __int8     value_uint8;
  unsigned __int16    value_uint16;
  float               value_float;

  if (value)
  {
    if (type!=0)
    {
      switch (type)
      {
        case t_u_int_8:
          value_uint8 = *((unsigned __int8*)value);
          output = cnv_UInt8ToCharP(value_uint8);
        break;
        case t_u_int_16:
          value_uint16 = *((unsigned __int16*)value);
          output = cnv_UInt16ToCharP(value_uint16);
        break;
        case t_u_hex_8:
          value_uint8 = *((unsigned __int8*)value);
          output = cnv_UHex8ToCharP(value_uint8);
        break;
        case t_bool:
          value_bool = *((bool*)value);
          output = cnv_BoolToCharP(value_bool);
        break;
        case t_float:
          value_float = *((float*)value);
          output = cnv_FloatToCharP(value_float);
        break;
      }
    }
  }

  return output;
}

//==============================================================================
/// Funzione per estrarre la posizione per un Array.
/// Cerca la <num>-esima numesima definizione di array all'interno della stringa
/// <str> e, se esiste, ritorna la posizione definita.
/// Nel caso particolare in cui non viene specificata la posizione ovvero quando
/// � presente la definizione "[]" la funzione ritorna -1.
/// Nel caso particolare in cui la posizione � implicita e dipende dalla
/// posizione dell'elemento stesso, che � inserito in un array, ovvero quando
/// � presente la definizione "[*]" la funzione ritorna -2.
/// Se la definizione non viene trovata la funzione restituisce -3.
/// Se <str> passata come parametro non � valida la funzione restituisce -4.
///
/// \date [19.02.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
int XMLGetArrayPos(char * str, int num)
{
  char    str_c             ; // Un carattere della stringa
  int     str_len           ; // La lunghezza della stringa
  int     counter       = 0 ; // Contatore delle dimensioni
  bool    start_search      ; // Flag di ricerca inizio o fine
  char    pos_c             ; // Un carattere della definizione della posizione
  int     pos               ; // Valore della posizione definita per l'array
  bool    pos_found = false ; // Flag di definizione posizione trovata
  int     pos_offset    = 0 ; // Offset della definizione della posizione corrente
  int     pos_len       = 0 ; // Lunghezza della definizione della posizione corrente

  if (str)
  {
    num = (num)? num:0; // Controllo che num non sia negativo
    str_len = strlen(str);
    if (str_len) // Se la stringa � lunga almeno un carattere
    {
      start_search = true;
      for (int c=0; c<str_len; c++) // Scansione della stringa
      {
        str_c = str[c]; // Prendo il c-esimo carattere della stringa
        if (start_search)
        {
          if (str_c == '[') // Trovato l'inizio della definizione array
          {
            start_search = false;
            pos_offset = c+1; // Salvo l'offset della 'posizione' corrente
            pos_len = 0; // Resetto la lunghezza della 'posizione' corrente
          }
        }
        else
        {
          if (str_c == ']') // Trovata la fine della definizione array
          {
            start_search = true;
            if (counter == num) // La definizione � quella che cercavo!
            {
              pos_found = true;
              if (pos_len) // Se la posizione � definita da almeno un carattere
              {
                if (str[pos_offset]=='*') // definita implicitamente
                {
                  pos = -2;
                }
                else // definita esplicitamente
                {
                  pos = 0;

                  for (int p=0; p<pos_len; p++)
                  {
                    pos_c = str[pos_offset+p];
                    pos *= 10;
                    pos += pos_c - 48;
                  }
                }
              }
              else // posizione indefinita (tutto l'array)
              {
                pos = -1;
              }
            }
            counter++; // Ho trovato una definizione di array
          }
          else
          {
            pos_len++; // Incremento la lunghezza della 'posizione' corrente
          }
        }
      }
      if (!pos_found)
      {
        pos = -3;
      }
    }
    else
    {
      pos = -4;
    }
  }
  else
  {
    pos = -4;
  }
  return pos;
}

//==============================================================================
/// Funzione per contare la dimensione di un Array.
/// Conta le dimensione di un array che vengono definite in una stringa, ovvero
/// quante "[x]" vengono trovate al suo interno.
/// Restituisce la dimensione, oppure 0 se non c'� un array.
///
/// \date [13.02.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLGetArrayDim(char * str)
{
  char    str_c             ; // Un carattere della stringa
  int     str_len           ; // La lunghezza della stringa
  int     counter       = 0 ; // Contatore delle dimensioni
  bool    start_search      ; // Flag di ricerca inizio o fine

  if (str)
  {
    str_len = strlen(str);
    if (str_len) // Se la stringa � lunga almeno un carattere
    {
      start_search = true;
      for (int c=0; c<str_len; c++)
      {
        str_c = str[c]; // Prendo il c-esimo carattere della stringa
        if (start_search)
        {
          if (str_c == '[')
          {
            start_search = false;
          }
        }
        else
        {
          if (str_c == ']')
          {
            start_search = true;
            counter++; // Ho trovato una definizione di array
          }
        }
      }
    }
  }
  return counter;
}

//==============================================================================
/// Funzione per confrontare stringhe.
/// Cerca la stringa <search> all'interno della stringa <str>.
/// Se le stringhe sono identiche restituisce 0.
/// Se la stringa � contenuta restituisce la posizione in <str> dove 1 � la
/// prima posizione.
/// Se la stringa non � contenuta restituisce -1.
/// In caso di errori restituisce -2.
///
/// \date [01.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLStrCompare( char * str, char * search )
{
  int   ret_code    = 0;
  int   str_len     = 0;
  int   search_len  = 0;
  char  ic          = 0;
  char  jc          = 0;
  bool  found       = false;

  if ( str != NULL && search != NULL )
  {
    str_len     = strlen( str );
    search_len  = strlen( search );
    if ( str_len && search_len )
    {
      ret_code = -1;
      if ( search_len == str_len ) // Se sono lunche uguali
      {
        ret_code = 0;
        for ( int i = 0; i < str_len; i++ ) // Ciclo sul carattere di partenza
        {
          ic = str[i];
          jc = search[i];
          if ( ic != jc )
          {
            ret_code = -1;
          }
        }
      }
      if ( search_len < str_len ) // Se hanno lunghezza diversa
      {
        for ( int i = 0; i < ( str_len - search_len - 1 ); i++ ) // Ciclo sul carattere di partenza
        {
          found = true;
          for ( int j = 0; j < search_len; j++ ) // Ciclo sui caratteri della stringa da cercare
          {
            ic = str[i+j];
            jc = search[j];
            if ( jc != ic )
            {
              found = false;
            }
          }
          if ( found ) // Se ho trovato la stringa
          {
            ret_code = i+1; // Assegno la posizione come ret_code
          }
        }
      }
    }
    else
    {
      ret_code = -2;
    }
  }
  else
  {
    ret_code = -2;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per estrarre token.
/// Estrae il <num>-esimo token dalla string <str> secondo la stringa di
/// separazione <sep>, dove con num=0 si intende il primo token.
/// Se viene fornito un numero di token negativo viene impostato a 0 (primo).
/// Restituisce il puntatore alla stringa del token ottenuto.
/// In caso di token non trovato o di errore restituisce NULL.
///
/// \date [20.09.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
char * XMLGetToken( char * str, char * sep, int num )
{
  char  * token         = NULL  ; // Token
  int     str_len       = 0     ; // Lunghezza della stringa sorgente
  int     sep_len       = 0     ; // Lunghezza del separatore
  char    str_c         = 0     ; // Un carattere della stringa...
  char    sep_c         = 0     ; // Un carattere del separatore...
  char    cur_c         = 0     ;
  bool    sep_found     = false ; // Flag di separatore trovato
  int     token_len     = 0     ; // Lunghezza del token corrente
  int     token_count   = 0     ; // Contatore dei token trovati
  char  * token_temp    = NULL  ; // Puntatore a token temporaneo

  if ( str != NULL && sep != NULL ) // Controllo stringa e separatore
  {
    num = ( num < 0 ) ? 0 : num; // Controllo che l'indice del token sia >= 0
    try
    {
      str_len = strlen( str ); // Calcolo la lunghezza della stringa sorgente
      sep_len = strlen( sep ); // Calcolo la lunghezza del separatore
    }
    catch(...)
    {
      str_len = 0;
      sep_len = 0;
    }
    if ( str_len > 0 && sep_len > 0 ) // Controllo che le lunghezze siano valide
    {
      token_temp = (char*)malloc(sizeof(char)*str_len); // Alloco il token temporaneo
      // --- Ciclo di scansione dei caratteri della stringa sorgente ---
      for ( int i = 0; i <= str_len; i++ )
      {
        str_c = str[i]; // Prendo l'i-esimo carattere della stringa sorgente
        // --- Ricerco il separatore ---
        if ( sep_len <= ( str_len - i ) ) // Se posso ancora trovare un separatore
        {
          sep_found = true; // Assumo a priori di aver trovato il separatore
          // --- Ciclo di ricerca del separatore ---
          for ( int s = 0; s < sep_len; s++ )
          {
            sep_c = sep[s];
            cur_c = str[i+s];
            if ( sep_c != cur_c )
            {
              sep_found = false; // Il separatore non corrisponde
              break; // Esco dal ciclo for di ricerca separatore
            }
          }
        }
        else
        {
          if ( i == str_len )
          {
            sep_found = true; // Sono alla fine della stringa (come un separatore)
            sep_len   = 1; // La lunghezza del separatore � reimpostata a 1
          }
          else
          {
            sep_found = false; // Separatore non trovato
          }
        }
        if ( sep_found == true ) // Se ho trovato il separatore
        {
          if ( token_count == num ) // Se � il token che stavo cercando
          {
            if ( token_len > 0 ) // Se la lunghezza del token temporanea � valida
            {
              token = (char*)malloc(sizeof(char)*( token_len + 1 )); // Alloco il token
              if ( token != NULL ) // Se l'allocazione � andata a buon fine
              {
                for ( int c=0; c<token_len; c++ )
                {
                  token[c] = token_temp[c];
                }
                //memcpy( &token[0], token_temp, sizeof(char)*token_len ); // Copio il token
                token[token_len] = NULL; // Metto il terminatore di stringa alla fine del token
              }
            }
            break; // Fine del ciclo for di scansione caratteri della stringa sorgente
          }
          else // Non � il token che volevo perci� vado avanti
          {
            i = i + ( sep_len - 1 ); // salto i caratteri del separatore
            token_len = 0; // Resetto la dimensione del token temporaneo
            token_count++; // Incremento il contatore dei token trovati
          }
        }
        else
        {
          token_temp[token_len] = str_c; // Copio l'i-esimo carattere della stringa sorgente nel token temporaneo
          token_len++; // Incremento la dimensione del token temporaneo
        }
      }
      free ( token_temp ); // Libero la memoria del token temporaneo
    }
  }

  return token;
}

//==============================================================================
/// Funzione per calcolare la lunghezza di un valore
/// La lunghezza � calcolata in base al valore fornito come parametro secondo
/// la definizione del tipo e dal fatto che sia in formato ASCII o meno.
/// In caso di errore restituisce -1.
/// NOTA: Usare solo con il valore <value> in formato ASCII con terminatore di
///       stringa '\0'.
///
/// \date [13.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLCalcValueLen( char * value, int type )
{
  int len       = -1;
  int value_len = 0;
  int data_len  = 0;

  if ( value != NULL )
  {
    value_len = strlen( value );
    if ( XMLIsIntType( type ) )
    {
      len = 1; // Se � un intero in formato ASCII non pu� essere > 1
    }
    else
    {
      if ( type == t_string )
      {
        len       = value_len;
      }
      else
      {
		data_len  = XMLCharSizeOf( type );
		len       = (int)floor((float)(value_len/data_len));
	  }
    }
  }

  return len;
}

//==============================================================================
/// Funzione per verificare se il tipo � esadecimale
///
/// \date [29.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
bool XMLIsHexType( int code )
{
  bool  is_hex  = false;
  int   div1m   = 0;

  div1m = (int)( code / 1000000L );

  if ( div1m == 3 || div1m == -3 )
  {
    is_hex = true;
  }

  return is_hex;
}

//==============================================================================
/// Funzione per verificare se il tipo � intero
///
/// \date [29.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
bool XMLIsIntType( int code )
{
  bool is_int = false;
  int   div1m   = 0;

  div1m = (int)( code / 1000000L );

  if ( div1m == 1 )
  {
    is_int = true;
  }

  return is_int;
}

//==============================================================================
/// Funzione per verificare se il valore in realt� � una reference
///
/// \date [01.04.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLIsValueRef( char * value )
{
  bool is_ref = false;

  if ( value != NULL )
  {
    if ( value[0] == '@' )
    {
      is_ref = true;
    }
  }

  return is_ref;
}

//==============================================================================
/// Funzione per risolvere l'elemento XML 'ROOT'
///
/// \date [31.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * XMLResolveRoot( XML_ELEMENT * element )
{
  XML_ELEMENT * current = NULL;
  XML_ELEMENT * parent  = NULL;

  if ( element != NULL )
  {
    current = element; // Parto dall'elemento iniziale
    while( ( parent = XMLResolveParent( current ) ) != current ) // Cerco l'elemento di root...
    {
      current = parent; // ...attraverso i parent
    }
  }

  return current;
}

//==============================================================================
/// Funzione per risolvere l'elemento XML 'PARENT'
///
/// Restituisce il puntatore alla struttura dell'elemento XML trovato.
/// In caso di parent insesistente restituisce l'elemento di partenza.
/// In caso di errore restituisce NULL.
///
/// \date [31.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * XMLResolveParent( XML_ELEMENT * element )
{
  XML_ELEMENT * current = NULL;

  if ( element != NULL )
  {
    if ( element->parent != NULL )
    {
      current = element->parent; // Passo all'elemento parent
    }
    else
    {
      current = element; // Restituisco l'elemento di partenza
    }
  }

  return current;
}

//==============================================================================
/// Funzione per risolvere l'elemento XML 'CALLER'
///
/// \date [31.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * XMLResolveCaller( XML_ELEMENT * element, XML_ELEMENT * caller )
{
  XML_ELEMENT * current = NULL;

  if ( element != NULL )
  {
    if ( caller != NULL )
    {
      current = caller; // Restituisco l'elemento caller
    }
    else
    {
      current = element; // Restituisco l'elemento di partenza
    }
  }

  return current;
}

//==============================================================================
/// Funzione per risolvere l'elemento XML risultato di una funzione interna
///
/// \date [31.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * XMLResolveFunction( int function_id, XML_ELEMENT * element )
{
  XML_ELEMENT       * result        = NULL  ; // Ptr all'elemento risultato da restituire
  XML_INT_FUNCTION  * function      = NULL  ; // Ptr alla funzione interna da eseguire
  int                 fun_code              ; // Codice di ritorno della funzione
  XML_ATTRIBUTE     * params_attrib = NULL  ; // Ptr alla struttura attributo def. parametri funzione
  char              * params_string = NULL  ; // Stringa di def. paramettri funzione
  XML_INT_PARAM     * input         = NULL  ; // Ptr alla struttura dei parametri di input
  int                 input_count   = 0     ; // Numero dei parametri di input
  XML_INT_PARAM     * output        = NULL  ; // Ptr alla struttura del risultato
  int                 output_count  = 0     ; // Numero di elementi nel risultato
  char              * element_value = NULL  ; // Valore dell'elemento
  char              * element_name  = NULL  ; // Stringa del nome elemento
  bool                element_temp  = false ; // Flag di elemento temporaneo
  int                 element_id    = 0     ; // ID dell'elemento
  int                 element_type  = 0     ; // Tipo del valore dell'elemento
  int                 element_len   = 0     ; // Lunghezza del valore dell'elemento
  bool                element_ascii = false ; // Flag di valore in formato ASCII

  function = XMLGetIntFunction( function_id ); // Recupero il ptr alla funzione interna
  if ( function != NULL ) // Controllo la funzione di interfaccia ottenuta
  {
    if ( element != NULL ) // Se c'� un elemento chiamante
    {
      // --- Preparazione dei parametri ---
      params_attrib = XMLGetAttrib( element, "params" ); // Recupero l'attributo di definizione dei parametri
      if ( params_attrib != NULL ) // Se c'� un attributo dei parametri
      {
        params_string = params_attrib->value; // Recupero la stringa dei parametri
      }
      input_count   = XMLGetParamsCount ( function->input );
      input         = XMLExtractParams  ( params_string, function->input, element ); // MALLOC
      output_count  = XMLGetParamsCount ( function->output );
      output        = XMLCreateParams   ( output_count ); // MALLOC
      // --- Esecuzione della funzione ---
      fun_code      = XMLExecIntFunction( function, output, input ); // Eseguo la funzione interna
      if ( fun_code == XML_INT_NO_ERROR )
      {
        /* TODO 1 -oEnrico -cXML : Discriminare il caso di esecuzione senza errori o con errori */
      }
      else
      {

      }
      // --- Formattazione dell'output della funzione ---
      if ( output != NULL ) // Se la funzione ha prodotto dei risultati
      {
        element_value = XMLPackParams( output, function->output ); // MALLOC
        element_name  = "result";
        element_id    = function_id;
        element_type  = output->type;
        element_ascii = output->ASCII;
        element_len   = XMLCalcValueLen( element_value, element_type );
        element_temp  = true;
        // --- Buildo l'elemento XML di risultato funzione ---
        result = XMLBuildElement( element_name, element_id, element_type, element_len, element_ascii, element_value, element_temp );
      }
      else
      {
        result = NULL;
      }
      // --- Eliminazione dei parametri allocati ---
      XMLDeleteParams( input );
      XMLDeleteParams( output );
      if ( element_value != NULL )  free( element_value );
    }
  }

  return result;
}

//==============================================================================
/// Funzione per risolvere un elemento XML da un percorso
/// Restituisce il puntatore alla struttura dell'elemento XML risolto dal
/// percorso <path> passato come parametro.
/// Se il percorso fornito non � valido viene restituito il ptr dell'elemento
/// XML di partenza <element>.
/// In caso di errore restituisce NULL.
/// NOTA: In caso di elementi ricavati come risultato di funzioni interne verr�
///       allocata una struttura di elemento XML temporanea (marchiata dallo
///       specifico attributo temp="true") che dovr� essere eliminata dalla
///       funzione esterna che richiama questa funzione.
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
XML_ELEMENT * XMLResolveElement( XML_ELEMENT * element, char * path, XML_ELEMENT * caller )
{
  XML_ELEMENT   * curr            = NULL  ; // Ptr all'elemento XML corrente
  XML_ELEMENT   * prev            = NULL  ; // Ptr all'elemento XML precedente
  char          * safe_path       = NULL  ; // Stringa del percorso senza attributi
	char          * token           = NULL  ; // Token estratto dal percorso
	char					* token_element		= NULL	; // Token (solo nome elemento)
  int             token_count     = 0     ; // Contatore dei token del percorso
  int             function_id     = 0     ; // ID di una funzione interna
  int             array_dim       = 0     ; // Dimensione di un array di elementi XML
  int             array_pos       = 0     ; // Posizione dell'array degli elementi XML

  if ( element != NULL )
  {
    curr = element; // Parto dell'elemento XML iniziale
    if ( path != NULL ) // Se ho fornito un valido percorso da risolvere
    {
      safe_path = XMLGetToken( path, ".", 0 ); // Tolgo eventuali attributi -MALLOC
			token = XMLGetToken( safe_path, ":", token_count ); // Prendo il primo token -MALLOC
      while( token != NULL ) // Ciclo di scansione dei token
      {
        prev = curr; // Salvo il ptr all'elemento XML precedente
        // --- Caso SPECIALE ---
        if ( token[0] == '_' )
        {
          // --- Caso speciale PARENT ---
          if ( strcmpi( token, "_parent" ) == 0 )
          {
            curr = XMLResolveParent( prev );
          }
          // --- Caso speciale ROOT ---
          else if ( strcmpi( token, "_root" ) == 0 )
          {
            curr = XMLResolveRoot( prev );
          }
          // --- Caso speciale CALLER ---
          else if ( strcmpi( token, "_caller" ) == 0 )
          {
            curr = XMLResolveCaller( prev, caller );
          }
          // --- Caso speciale FUNCTION ---
          else if ( XMLStrCompare( token, "_function" ) > 0 )
          {
            function_id = XMLGetArrayPos( token, 0 );
            curr = XMLResolveFunction( function_id, prev ); // -MALLOC
          }
        }
        // --- Caso NORMALE ---
        else
        {
          array_dim = XMLGetArrayDim( token ); // Recupero la dimensione di una eventuale array
          if ( array_dim > 0 ) // Se � definito un array (mono/multi-dimensionale)
          {
            // --- Ciclo di scansione delle definizioni di array ---
            for ( int a = 0; a < array_dim; a++ )
            {
							array_pos = XMLGetArrayPos( token, a );
							if ( array_pos >= 0 ) // Se la posizione � definita esplicitamente
							{
								token_element = XMLGetToken( token, "[", 0 ); // Prendo solo il nome -MALLOC
								curr = XMLGetNext( curr->child, token_element, array_pos );
								if ( token_element != NULL ) {
									free( token_element ); // -FREE
								}
              }
              else
              {
                if ( array_pos == - 2 ) // Caso posizione implicita [*]
                {
                  //// Devo andare a estrarre la posizione dal caller (?)
                }
                if ( array_pos == -1 ) // Caso posizione indefinita []
                {
                  //// Alloco tutto l'array
                }
              }
            }
          }
          else // Non � definito un array
					{
						// --- Prendo il primo elemento 'figlio' con nome 'tot_ele' ---
						curr = XMLGetNext( curr->child, token, -1 );
					}
        }
        // --- Elimino un eventuale elemento XML risultato di una funzione ---
        if ( prev != NULL && XMLGetValueBool( prev, "temp" ) == true )
        {
          XMLFreeElement( prev );
          prev = NULL;
        }
        // --- Fine del passo ---
        if ( token != NULL ) free( token ); // -FREE
        token_count++;
        token = ( curr != NULL )?XMLGetToken( safe_path, ":", token_count ):NULL; // Prendo il prossimo token
      }
      // --- Cancellazione della memoria allocata ---
      if ( safe_path != NULL ) free( safe_path ); // -FREE

    }
  }

  return curr;
}

//==============================================================================
/// Funzione per risolvere entit� XML attravrso un percorso
/// All'interno della struttura XML, partendo dalla posizione <element> esegue
/// la ricerca dell'entit� secondo il percorso <path> fornito.
/// La struttura restituita � un'entit� XML temporanea che racchiude al suo
/// interno il ptr ad una struttura di un elemento XML o un attributo XML.
/// Restituisce il puntatore all'entit� trovata (se siste) altrimenti NULL.
/// NOTA: Il risultato � temporaneo quindi se non pi� utilizzato dovr� essere
///       cancellato dalla memoria.
///
/// \date [19.01.2006]
/// \author Enrico Alborali
/// \version 1.05
//------------------------------------------------------------------------------
XML_ENTITY * XMLResolvePath( XML_ELEMENT * element, char * path, XML_ELEMENT * caller )
{
  XML_ENTITY    * entity          = NULL  ; // Puntatore alla struttura entit� XML
  XML_ELEMENT   * result_element  = NULL  ; // Elemento risultato da una risoluzione
  char          * attrib_name     = NULL  ; // Stringa del nome di un eventuale attributo
  XML_ATTRIBUTE * attrib          = NULL  ; // Ptr all'attributo cercato

  if ( element != NULL ) // Controllo l'elemento di partenza
  {
    result_element = XMLResolveElement( element, path, caller ); // Risolvo l'elemento attraverso il percorso
    if ( result_element != NULL )
    {
      attrib_name = XMLGetToken( path, ".", 1 ); // Prendo ci� che sta dopo un eventuale '.' -MALLOC
      if ( attrib_name != NULL )
      {
        attrib = XMLGetAttrib( result_element, attrib_name ); // Cerco l'attributo
        if ( attrib != NULL )
        {
          // --- Buildo l'entit� XML (attributo) da restituire ---
          entity = XMLCreateEntity( NULL, NULL, attrib, XML_ENTITY_ATTRIBUTE, 0 ); // -MALLOC
        }
        free ( attrib_name ); // -FREE
      }
      else
      {
        // --- Buildo l'entit� XML (elemento) da restituire ---
        entity = XMLCreateEntity( NULL, NULL, result_element, XML_ENTITY_ELEMENT, 0 ); // -MALLOC
      }
      if ( entity != NULL )
      {
        entity->temp = true; // Setto il flag di entit� temporanea
      }
    }
  }

  return entity;
}

//==============================================================================
/// Funzione per risolvere path XML (versione RAW).
/// All'interno della struttura XML, partendo dalla posizione <element> esegue
/// la ricerca dell'entit� secondo il <path> fornito.
/// La struttura restituita � un'entit� XML temporanea che racchiude al suo
/// interno una struttura di un elemento XML temporaneo. L'elemento a sua volta
/// racchiude i parametri <id>, <type>, <len>, <ascii>, <value>, <temp>, dove
/// <value> � il valore dell'entit� ricercata. Il campo <name> dell'elemento XML
/// � anch'esso ricavato dalla ricerca.
/// Se viene specificato il parametro <far_params> a 'true' allora l'elemento
/// XML restituito avr� i parametri (escluso value) ricavati dall'ultima entit�
/// letta durante la ricerca, altrimenti saranno ricavati dall'elemento di
/// partenza <element>.
/// Il risultato � temporaneo quindi se non pi� utilizzato dovr� essere
/// cancellato dalla memoria.
/// Restituisce il puntatore all'entit� trovata (se siste) altrimenti NULL.
///
/// \date [29.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_ENTITY * XMLResolvePathRAW( XML_ELEMENT * element, char * path, XML_ELEMENT * caller, bool far_params )
{
  XML_ELEMENT   * current         = NULL  ; // Ptr all'elemento XML corrente
  XML_ELEMENT   * previous        = NULL  ; // Ptr all'elemento XML precedente
  XML_ENTITY    * entity          = NULL  ;
  char          * token           = NULL  ;
  char          * tok_ele         = NULL  ;
  char          * tok_att         = NULL  ;
  XML_ELEMENT   * element_ptr     = NULL  ; // Ptr alla struttura elemento XML da restituire
  char          * element_name    = NULL  ; // Nome dell'elemento XML da restituire
  int             element_id      = 0     ; // ID dell'elemento XML da restituire
  int             element_type    = 0     ; // Tipo dell'elemento XML da restituire
  int             element_len     = 0     ; // Lunghezza dell'elemento XML da restituire
  bool            element_ascii   = false ; // Flag formato ASCII dell'elemento XML da restituire
  char          * element_value   = NULL  ; // Ptr al valore dell'elemento XML da restituire
  bool            element_temp    = false ; // Flag di temporaneo per l'elemento XML da restituire
  XML_ATTRIBUTE * attrib          = NULL  ; // Ptr all'attributo cercato
  int             token_count     = 0     ;
  int             array_dim               ;
  XML_ELEMENT   * result_element  = NULL  ; // Elemento risultato di una funzione
  XML_ATTRIBUTE * attrib_temp     = NULL  ;
  char          * value_temp      = NULL  ;

  /* TODO 2 -oEnrico -cMemory : Controllare tutte le allocazioni e disallocazione */
  /* TODO 4 -oEnrico -cXML : Verificare il funzionamento della ricerca dell'elemento */
  if ( element != NULL ) // Controllo l'elemento di partenza
  {
    // --- Imposto il punto di partenza ---
    current   = element; // Parto dall'elemento corrente
    previous  = NULL;
    // --- Raccolgo i parametri dell'elemento ---
    ///if ( far_params == false || far_params == true )
    if ( far_params == false )
    {
      element_name  = XMLGetName( element );
      element_id    = XMLGetValueInt( element, "id" );
      element_type  = XMLGetTypeCode( XMLGetValue( element, "type" ) );
      attrib_temp   = XMLGetAttrib( element, "len" );
      value_temp    = attrib_temp->value;
      if ( value_temp[0] == '@' )
      {
        element_len   = 0; // Dimensione indefinita
      }
      else
      {
        element_len   = XMLGetValueInt( element, "len" );
      }
      element_ascii = XMLGetValueBool( element, "ascii" );
    }
    else
    {
      element_name  = "result";
      element_id    = 0;
      element_type  = t_string;
      element_len   = 0; // Dimensione indefinita
      element_ascii = true;
    }
    element_temp  = false;
    // --- Inizio a risolvere il percorso ---
    if ( path != NULL ) // Controllo che il percorso sia valido
    {
      token = XMLGetToken( path, ":", token_count ); // Prendo il primo token
      while( token != NULL ) // Ciclo di scansione dei token
      {
        previous = current; // Salvo il ptr all'elemento XML precedente
        if ( tok_ele != NULL ) free( tok_ele ); // Libero la memoria allocata per il token elemento
        if ( tok_att != NULL ) free( tok_att ); // Libero la memoria allocata per il token attributi
        tok_ele = XMLGetToken( token, ".", 0 ); // Scarto l'eventuale attributo
        tok_att = XMLGetToken( token, ".", 1 ); // Salvo l'eventuale attributo
        // --- Caso SPECIALE ---
        if ( tok_ele[0] == '_' )
        {
          // --- Caso speciale PARENT ---
          if ( strcmpi( tok_ele, "_parent" ) == 0 )
          {
            current = current->parent; // Passo all'elemento parent
          }
          // --- Caso speciale ROOT ---
          else if ( strcmpi( tok_ele, "_root" ) == 0 )
          {
            while( current->parent != NULL ) // Cerco l'elemento di root...
            {
              current = current->parent; // ...attraverso i parent
            }
          }
          // --- Caso speciale CALLER ---
          else if ( strcmpi( tok_ele, "_caller" ) == 0 )
          {
            if ( caller != NULL ) // Controllo se c'� un elemento chimante
            {
              current = caller; // Passo al chiamante
            }
          }
          // --- Caso speciale FUNCTION ---
          else if ( XMLStrCompare( tok_ele, "_function" ) > 0 )
          {
            int                 function_id   = XMLGetArrayPos( tok_ele, 0 )    ;
            XML_INT_FUNCTION  * function      = XMLGetIntFunction( function_id );
            XML_INT_PARAM     * output                                          ;
            XML_INT_PARAM     * input                                           ;
            XML_ATTRIBUTE     * params_attrib                                   ;
            char              * params_string = NULL                            ;
            int                 output_count                                    ;
            int                 input_count                                     ;
            char              * output_string = NULL                            ;
            if ( function != NULL ) // Controllo la funzione di interfaccia ottenuta
            {
              // --- Preparazione dei parametri ed esecuzione della funzione ---
              if ( element != NULL ) // Se c'� un elemento chiamante
              {
                params_attrib = XMLGetAttrib( element, "params" );
                if ( params_attrib != NULL ) // Se c'� un attributo dei parametri
                {
                  params_string = params_attrib->value;
                }
              }
              input_count   = XMLGetParamsCount ( function->input );
              input         = XMLExtractParams  ( params_string, function->input, element );
              output_count  = XMLGetParamsCount ( function->output );
              output        = XMLCreateParams   ( output_count );
              XMLExecIntFunction( function, output, input ); // Eseguo la funzione interna
              // --- Formattazione dell'output della funzione ---
              if ( output != NULL ) // Se la funzione ha prodotto dei risultati
              {
                //* BUGFIX [BUG:SP30XMIN02]
                output_string = XMLPackParams( output, function->output );
                if ( far_params == true ) // Se devo recuperare i parametri 'far'
                {
                  element_name  = XMLGetName( current );
                  element_id    = XMLGetValueInt( current, "id" );
                  element_type  = XMLGetTypeCode( XMLGetValue( current, "type" ) );
                  element_len   = XMLGetValueInt( current, "len" );
                  element_len   = ( element_len < 1 ) ? 1 : element_len;
                  element_ascii = XMLGetValueBool( current, "ascii" );
                } // ...altrimenti mantengo tutti i parametri dell'elemento XML di partenza
                element_temp  = true;
                // --- Buildo l'elemento XML di risultato funzione ---
                result_element = XMLBuildElement( element_name, element_id, element_type, element_len, element_ascii, output_string, element_temp );
                /* TODO 1 -oEnrico -cMemory : Dovr� cancellare l'elemento XML di risultato */
                current = result_element; // Setto come elemento corrente quello di risultato
                free( input );
                free( output );
                // BUGFIX END */
              }
              else
              {
                current = NULL;
                free( input );
                free( output );
              }
            }
          }
        }
        // --- Caso NORMALE ---
        else
        {
          array_dim = XMLGetArrayDim( tok_ele ); // Recupero la dimensione di una eventuale array
          if ( array_dim > 0 ) // Se � definito un array (mono/multi-dimensionale)
          {
            int array_pos;
            for ( int a=0; a<array_dim; a++ ) // Ciclo di scansione delle definizioni di array
            {
              array_pos = XMLGetArrayPos( tok_ele, a );
              if ( array_pos >= 0 ) // Se la posizione � definita esplicitamente
              {
                XML_ELEMENT * array_element = XMLGetNext( current->child, NULL, array_pos );
                if ( array_element != NULL )
                {
                  current = array_element;
                }
                else
                {
                  current = NULL;
                }
              }
              else
              {
                if ( array_pos == -2 ) // Caso posizione implicita [*]
                {
                  //// Devo andare a estrarre la posizione dal caller (?)
                }
                if ( array_pos == -1 ) // Caso posizione indefinita []
                {
                  //// Alloco tutto l'array
                }
              }
            }
          }
          else // Non � definito un array
          {
            // --- Prendo il primo elemento 'figlio' con nome 'tot_ele' ---
            current = XMLGetNext( current->child, tok_ele, -1 );
          }
          if ( far_params == true ) // Se devo recuperare i parametri 'far'
          {
            element_name  = XMLGetName( current );
            element_id    = XMLGetValueInt( current, "id" );
            element_type  = XMLGetTypeCode( XMLGetValue( current, "type" ) );
            element_len   = XMLGetValueInt( current, "len" );
            element_len   = ( element_len < 1 ) ? 1 : element_len;
            element_ascii = XMLGetValueBool( current, "ascii" );
          }
        }
        // --- Elimino un eventuale elemento XML risultato di una funzione ---
        if ( previous != NULL && XMLGetValueBool( previous, "temp" ) == true )
        {
          XMLFreeElement( previous );
          previous = NULL;
        }
        token_count++;
        token = ( current != NULL )?XMLGetToken( path, ":", token_count ):NULL; // Prendo il prossimo token
      }
      if ( current != NULL ) // Se ho risolto con successo il path (almeno fino all'elemento)
      {
        // --- Se ho risolto un attributo ---
        if ( tok_att != NULL ) // Se devo prendere un singolo attributo
        {
          attrib = XMLGetAttrib( current, tok_att );
          if ( attrib != NULL )
          {
            element_value = attrib->value;
            if ( far_params == true ) // Se � abilitata la ricerca 'far'
            {
              element_name  = attrib->name;
              element_id    = 0;
              element_type  = XMLGetTypeCode( attrib->type );
              element_type  = ( element_type == 0 ) ? t_string : element_type;
              element_len   = attrib->length;
              element_len   = ( element_len < 1 ) ? 1 : element_len;
              element_ascii = true;
            }
          }
        }
        // --- Se ho risolto un elemento ---
        else
        {
          element_value = XMLGetValue( current );
        }
        // --- Buildo l'elemento XML da restituire ---
        element_ptr = XMLBuildElement( element_name, element_id, element_type, element_len, element_ascii, element_value, true );
        // --- Buildo l'entit� XML da restituire ---
        entity = XMLCreateEntity( NULL, NULL, element_ptr, XML_ENTITY_ELEMENT, 0 );
        ///entity->temp = true; // Setto il flag che identifica l'entit� come temporanea
        // --- Se ho ottenuto un risultato temporaneo di una funzione ---
        if ( XMLGetValueBool( current, "temp" ) == true )
        {
          XMLFreeElement( current ); // Cancello l'elemento XML temporaneo
        }
      }
      else
      {
        entity = NULL;
      }
      if ( tok_ele != NULL ) free( tok_ele ); // Libero la memoria allocata per il token elemento
      if ( tok_att != NULL ) free( tok_att ); // Libero la memoria allocata per il token attributi
    }
  }
  return entity;
}


//==============================================================================
/// Funzione per ottenere il numero di parametri.
/// Data una stringa (char pointer) che contenga la definizione dei parametri,
/// ovvero sottostringhe separate da virgole (doppia virgola = virgola), conta
/// il numero di questi e lo restituisce.
/// In caso di errore restituisce un numero non positivo.
///
/// \date [12.02.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLGetParamsCount(char * params)
{
  int ret_code    =   0 ;
  int last_comma  =  -1 ;
  int params_len;
  char paramsc;

  if (params)
  {
    params_len = strlen(params);
    for (int c=0; c<params_len; c++)
    {
      paramsc = params[c];
      if (paramsc == ',')
      {
        if (c-last_comma > 1)
        {
          ret_code++; // Ho trovato il termine di un parametro
        }
        last_comma = c;
      }
    }
    if (params[params_len-1] != ',') ret_code++; // C'� un parametro finale
  }
  return ret_code;
}

//==============================================================================
/// Funzione per generare una lista di parametri.
///
/// \date [03.02.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_INT_PARAM * XMLCreateParams( int count )
{
  XML_INT_PARAM * params = NULL;
  XML_INT_PARAM * last   = NULL;

  if ( count > 0 ) // Controllo se il numero di parametri � maggiore di 0
  {
    params    = ( XML_INT_PARAM * ) malloc ( sizeof(XML_INT_PARAM)*(count+1) );
    last      = params + count;
    last->id  = XML_INT_STOP_PARAM; // Metto lo stop in fondo
  }

  return params;
}

//==============================================================================
/// Funzione per cancellare una struttura parametri interni XML
/// Elimina sia la struttura intera dei parametri, sia i valori di ogni singolo
/// parametro.
///
/// \date [21.06.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLDeleteParams( XML_INT_PARAM * params )
{
  int             ret_code  = XML_INT_NO_ERROR;
  XML_INT_PARAM   param                       ;
  int             count     = 0               ;
  bool            finished  = false           ;
  void          * value     = NULL            ;

  if ( params != NULL )
  {
    // --- Ciclo di cancellazione valori dei parametri ---
    while ( !finished )
    {
      param = params[count]; // Prendo il count-esimo parametro
      if ( param.id != XML_INT_STOP_PARAM ) // Se non � un 'parametro di stop'
      {
        value = param.value;
        if ( value != NULL )
        {
          try
          {
            free ( value ); // -FREE
          }
          catch (...)
          {
            ret_code = XML_INT_ERROR_FREE_FAILURE;
          }
          param.value = NULL;
        }
        count++; // Incremento il contatore dei parametri
      }
      else
      {
        finished = true; // Finisco il ciclo while
      }
    }
    try
    {
      free( params ); // -FREE
    }
    catch(...)
    {
      ret_code = XML_INT_ERROR_FREE_FAILURE;
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_PARAM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per impostare i campi di un parametro.
///
/// \date [03.02.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLSetParam( XML_INT_PARAM * output, int id, int type, void * value )
{
  int ret_code = XML_INT_NO_ERROR; // Codice di ritorno

  if ( output != NULL ) // Controllo se l'output � valido
  {
    if ( output->id != XML_INT_STOP_PARAM )
    {
      output->id    = id    ;
      output->type  = type  ;
      output->value = value ;
    }
    else
    {
      ret_code = XML_INT_ERROR_STOP_PARAM_FOUND;
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_PARAM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per estrarre i parametri.
///
/// \date [08.02.2008]
/// \author Enrico Alborali, Mario
/// \version 0.14
//------------------------------------------------------------------------------
XML_INT_PARAM * XMLExtractParams( char * params_string, char * def_string, XML_ELEMENT * element )
{
  XML_INT_PARAM   * params        = NULL              ; // Lista di strutture parametri
  int               params_count  = 0                 ; // Numero di parametri in params_string
  char            * value_string                      ; // Puntatore a stringa valore
  char            * value                             ; // Puntatore al valore del parametro
  void            * value_temp    = NULL              ; // Puntatore temporaneo ad un valore
  int               value_size    = 0                 ; // Dimensione del valore del parametro
  int               value_csize   = 0                 ; // Dimensione del valore del parametro (in caratteri)
  int               value_len     = 0                 ; // Lunghezza dell'array
  bool              value_ascii   = false             ; // Flag di valori in formato ASCII
  char            * type_string                       ; // Puntatore a stringa di tipo
  int               type                              ; // Codice numerico di tipo
  XML_ENTITY      * entity        = NULL              ; // Ptr a entit� XML di appoggio
  XML_ELEMENT     * entity_ele    = NULL              ; // Ptr a elemento XML
  XML_ATTRIBUTE   * entity_att    = NULL              ; // Ptr a attributo XML
  char            * value_path    = NULL              ; // Ptr a percorso del valore
  char            * token_string  = NULL              ; // Ptr a stringa token
  char *			value_temp_string	= NULL			; // Stringa temporanea

  if ( params_string != NULL ) // Controllo la stringa dei parametri
  {
	params_count = XMLGetParamsCount( params_string );

	if ( params_count > 0 ) // Se ho almeno un parametro
	{
	  // --- Blocco il cestino XML ---
	  XMLLockTrash("XMLExtractParams");

      params = XMLCreateParams( params_count ); // Alloco i parametri -MALLOC
      for ( int p = 0; p < params_count; p++ ) // Ciclo sui parametri
      {
        value_string = XMLGetToken( params_string, ",", p ); // Estraggo il p-esimo valore -MALLOC
        if ( value_string == NULL )
        {
        }
        else
        {
          token_string = value_string; // Mi salvo il ptr al token
          // --- Propriet� di default del valore esplicito ---
          value_ascii = false;
          value_len   = 1;
          // --- Ciclo di recupero valore implicito ---
          while( XMLIsValueRef( value_string ) ) // Ciclo per risolvere i valori impliciti
          {
            value_path = &value_string[1]; // Recupero il percorso (non considero la chiocciola!)
            entity = XMLResolvePath( element, value_path, element );
            if ( entity != NULL ) // Se l'entit� � valida
            {
              XMLAddToTrash( (void*)entity ); // -ADDTRASH
              switch ( entity->type )
              {
                case XML_ENTITY_ELEMENT:
                  entity_ele = (XML_ELEMENT*)entity->structure;
                  if ( entity_ele != NULL )
                  {
                    value_ascii   = XMLGetValueBool ( entity_ele, "ascii" );
                    value_len     = XMLGetValueInt  ( entity_ele, "len"   );
                    value_string  = XMLGetValue     ( entity_ele ); // Risolvo il nuovo valore
                  }
                  //* DEBUG
                  if ( value_string == NULL )
                  {
                    break;
                  }
                  //*/
                break;
                case XML_ENTITY_ATTRIBUTE:
                  entity_att = (XML_ATTRIBUTE*)entity->structure;
                  if ( entity_att != NULL )
                  {
                    value_ascii   = true;
                    value_len     = entity_att->length;
                    value_string  = entity_att->value;
                  }
                break;
              }
            }
            /* TODO 1 -oEnrico -cMemory : Forse dovrei cancellare <entity> dalla memoria dopo averla utilizzata? */
          }
          type = t_string; // Di default il tipo � una stringa (se non � definito in def_string)
          if ( def_string != NULL ) // Se c'� una definizione dei parametri
          {
            type_string = XMLGetToken( def_string, ",", p ); // Prendo la p-esima stringa di definizione di tipo
            if ( type_string != NULL ) // Se la stringa di definizione esiste
            {
              type = XMLGetTypeCode( type_string ); // Estraggo il codice del tipo di parametro
              // BUGFIX #20060701-001
              if ( type == t_string ) // --- Un astringa e' per definizione ASCII ---
              {
                value_ascii = true;
              }
              free( type_string ); // Libero la memoria occupata dalla stringa di definizione tipo
            }
          }
          //* BUGFIX [BUG:SP30XMIN01]
          value_size  = XMLByteSizeOf( type ); // Recupero la dimensione in byte
          value_csize = XMLCharSizeOf( type ); // Recupero la dimensione in caratteri ASCII
          if ( value_string != NULL ) // Se ho trovato il valore
          {
            if ( value_ascii ) // Caso formato ASCII
            {
              value = XMLStrCpy( NULL, value_string );
            }
            else // Caso formato binario
            {
				value = (char*)malloc(sizeof(char)*( ( value_size * value_len ) ) );
				value_temp_string = (char*)malloc(sizeof(char)*(value_csize+1)); // -MALLOC
				memset((void*)value_temp_string,0,sizeof(char)*(value_csize+1));
				for ( int l = 0; l < value_len; l++ ) // Ciclo sull'array dei valori
				{
					if (value_len>1) memcpy( (void*)value_temp_string,(void*)&value_string[ l * value_csize ],sizeof(char)*(value_csize) );
					else strcpy( value_temp_string,&value_string[ l * value_csize ]);
					value_temp = XMLASCII2Type( value_temp_string, type ); // Converto il valore da stringa ASCII al tipo specifico
					memcpy( &value[ l * value_size ], value_temp, value_size ); // Copio l'elemento l-esimo dell'array
					free( value_temp ); // Libero la memoria temporaneo per l'elemento dell'array
				}
				free( value_temp_string ); // -FREE
			}
          }
          else // Se non � stato trovato il valore
          {
            value = NULL;
          }
          XMLSetParam( &params[p], p, type, value ); // Imposto il valori del p-esimo parametro
          params[p].ASCII = value_ascii;
          // BUGFIX END */
          free( token_string ); // -FREE
        }
      }

      // --- Svuoto il cestino XML ---
      //XMLEmptyTrash( );

      // --- Sblocco il cestino XML ---
      XMLUnlockTrash( );
    }
  }

  return params;
}

//==============================================================================
/// Funzione per impacchettare i parametri.
/// Calcola il numero di parametri da impacchettare secondo la definizione
/// <def_string> ed estrae i valori in formato stringa di caratteri dalla lista
/// di strutture <params>.
/// Restituisce la stringa di caratteri che contiene tutti i valori separati da
/// virgole.
/// In caso di errore (<params> non valido o lista troppo corta rispetto a
/// quanto specificato in <def_string>) restituisce NULL.
///
/// \date [22.03.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLPackParams(XML_INT_PARAM * params, char * def_string)
{
  char          * output        = NULL  ; // Stringa dei parametri impacchettati
  int             output_len    = 0     ; // Lunghezza della stringa di output
  char          * app           = NULL  ; // Stringa di appoggio
  int             app_len       = 0     ; // Lunghezza della stringa di appoggio
  char          * value         = NULL  ; // Stringa di appoggio per un singolo parametro
  int             value_len     = 0     ; // Lunghezza del singolo parametro
  int             params_count  = 0     ; // Numero di parametri
  XML_INT_PARAM * current       = NULL  ; // Puntatore di appoggio per parametri

  if (params && def_string) // Se c'� una stringa di definizione dei parametri
  {
    params_count = XMLGetParamsCount(def_string);
    for (int i=0; i<params_count; i++)
    {
      current = &params[i]; // Recupero il puntatore dell'i-esimo parametro
      if (current)
      {
        value = XMLType2ASCII(current->value,current->type);
        if (value) // Se ho ricevuto un valore in stringa valido
        {
          value_len = strlen(value); // Calcolo la lunghezza del nuovo parametro
          app_len = output_len + value_len; // Ricalcolo la nuova lunghezza della stringa di appoggio
          if (output_len) app_len++;
          app = (char*) malloc(sizeof(char)*(app_len + 1)); // Alloco la memoria della stringa di appoggio
          if (output_len)
          {
            for (int c=0; c<output_len; c++) // Copio la stringa di output intermedia precedente
            {
              app[c] = output[c];
            }
            free(output); // Cancella la precedente stringa di output intermedia
            app[output_len] = ','; // Aggiungo il nuovo separatore
            output_len++;
          }
          for (int c=0; c<value_len; c++) // Copio il nuovo parametro
          {
            app[output_len+c] = value[c];
          }
          free(value); // Libero la memoria occupata da value
          app[app_len] = NULL; // Aggiungo il terminatore di stringa
        }
        else // Aggiungo un "NULL"
        {
          value     = "NULL";
          value_len = 4; // Lunghezza del "NULL" (fissa)
          app_len   = output_len + value_len; // Ricalcolo la nuova lunghezza della stringa di appoggio
          if (output_len) app_len++;
          app = (char*) malloc(sizeof(char)*(app_len + 1)); // Alloco la memoria della stringa di appoggio
          if (output_len)
          {
            for (int c=0; c<output_len; c++) // Copio la stringa di output intermedia precedente
            {
              app[c] = output[c];
            }
            free(output); // Cancella la precedente stringa di output intermedia
            app[output_len] = ','; // Aggiungo il nuovo separatore
            output_len++;
          }
          for (int c=0; c<value_len; c++) // Copio il nuovo parametro
          {
            app[output_len+c] = value[c];
          }
          app[app_len] = NULL; // Aggiungo il terminatore di stringa
        }
        output = app;
        output_len = app_len;
      }
      else
      {
        if (output) free(output);
        output = NULL;
        i = params_count; // Forzo la fine del ciclo
      }
    }
  }

  return output;
}

//==============================================================================
/// Funzione per ottenere funzioni di interfaccia.
///
/// \date [15.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
XML_INT_FUNCTION * XMLGetIntFunction(int id)
{
  XML_INT_FUNCTION * function = NULL;

  for (int f=0; f<XMLIntFunctionsCount; f++)
  {
    if (XMLIntFunctions[f].id == id)
    {
      function = &XMLIntFunctions[f];
    }
  }

  return function;
}

//==============================================================================
/// Funzione per eseguire funzioni di interfaccia.
///
/// \date [31.03.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLExecIntFunction( XML_INT_FUNCTION * function, XML_INT_PARAM * output, XML_INT_PARAM * input )
{
  int                   ret_code  = XML_INT_NO_ERROR  ; // Codice di ritorno
  XML_INT_FUNCTION_PTR  pfunction                     ; // Puntatore alla funzione di interfaccia

  if ( function != NULL ) // Controllo il puntatore a struttura funzione interfaccia
  {
    pfunction = (XML_INT_FUNCTION_PTR) function->ptf;
    if ( pfunction != NULL ) // Controllo il puntatore alla funzione da eseguire
    {
      if ( input != NULL )// Controllo il puntatore alla struttura dei paramentri di ingresso
      {
        ret_code = pfunction( (void*)output, (void*)input );
      }
      else
      {
        ret_code = XML_INT_ERROR_INVALID_INPUT;
      }
    }
    else
    {
      ret_code = XML_INT_ERROR_INVALID_FUNCTION;
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_FUNCTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere valori da entit� XML.
/// In base al tipo di entit� XML <entity> ricerca un attributo con nome <name>.
/// Ritorna il puntatore di una stringa che contiene il valore (non una copia).
/// <name> pu� essere omesso (con valore NULL) e di default viene cercato
/// l'attributo con nome "name".
/// In caso di errore restituisce NULL.
/// Nota: Per ottenere una copia usare XMLExtractValue.
///
/// \date [24.03.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLGetValue(XML_ENTITY * entity, char * name)
{
  XML_ELEMENT   * element = NULL; // Puntatore di appoggio per elementi XML
  XML_ATTRIBUTE * attrib  = NULL; // Puntatore di appoggio per attributi XML
  char          * attrib_value  ; // Puntatore alla stringa valore dell'attributo
  char          * value   = NULL; // Puntatore per il valore in uscita
  int             value_len     ; // Lunghezza del valore in uscita

  if (entity) // Controllo se l'entit� � valida
  {
    if (entity->type == XML_ENTITY_ELEMENT) // Caso elemento XML
    {
      element = (XML_ELEMENT*) entity->structure;
      if (element) // Controllo se l'elemento � valido
      {
        attrib = XMLGetAttrib(element,name); // Recupero il puntatore (non � una copia!)
      }
    }
    if (entity->type == XML_ENTITY_ATTRIBUTE) // Caso attributo XML
    {
      attrib = (XML_ATTRIBUTE*) entity->structure;
    }
    if (attrib) // Controllo se l'attributo � stato trovato
    {
      attrib_value = attrib->value;
      if (attrib_value) // Controllo se c'� un valore
      {
        value_len = strlen(attrib_value);
        if (value_len) // Se il valore � di almeno un carattere
        {
          value = attrib->value; // Prendo solo il puntatore (non � una copia!)
        }
      }
    }
  }
  return value;
}

//==============================================================================
/// Funzione per ottenere valori da entit� XML.
/// Come precedente con <name> implicitamente a NULL.
///
/// \date [16.04.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLGetValue(XML_ENTITY * entity)
{
  char * value;

  value = XMLGetValue(entity,NULL);

  return value;
}

//==============================================================================
/// Funzione per ottenere nomi XML.
/// Ritorna il puntatore al nome di un elemento XML <element>.
/// In caso di errore restituisce NULL.
///
/// \date [11.02.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLGetName(XML_ELEMENT * element)
{
  char * name = NULL;

  if ( element != NULL ) // Controllo se <element> punta a una struttura
  {
    name = element->name; // Estraggo il puntatore campo <name>
  }

  return name;
}

//==============================================================================
/// Funzione per estrarre nomi XML.
/// Ritorna il puntatore alla copia di un nome di un elemento XML <element>.
/// Questa funzione alloca una nuova stringa ed effettua una copia del contenuto
/// di quella originale (se trovata).
/// In caso di errore restituisce NULL.
///
/// \date [27.02.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLExtractName(XML_ELEMENT * element)
{
  char  * name      = NULL; // Puntatore alla cpia del nome trovato
  int     name_len  = 0   ; // Lunghezza del nome originale

  if (element) // Controllo se <element> punta a una struttura
  {
    if (element->name) // Controllo il puntatore campo <name>
    {
      name_len = strlen(element->name);
    }
    if (name_len) // Se la stringa � valida
    {

    }

  }

  return name;
}

//==============================================================================
/// Funzione per ottenere attributi XML.
///
/// \date [05.02.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
XML_ATTRIBUTE * XMLGetAttrib (XML_ELEMENT * element, int id)
{
  XML_ATTRIBUTE * out     = NULL;
  XML_ATTRIBUTE * attrib  = NULL;
  int             counter = 0;

  if (element) // Controllo se l'elemento � valido
  {
    attrib  = element->attrib;
    while(attrib)
    {
      if (counter == id)
      {
        out     = attrib;
        attrib  = NULL;
      }
      else
      {
        attrib  = attrib->next; // Passo all'attributo successivo (se esiste)
        counter++; // Incremento il contatore
      }
    }
  }

  return out;
}

//==============================================================================
/// Funzione per ottenere attributi XML.
/// A partire dalla struttura elemento XML <element>, di cui viene passato il
/// puntatore, vado alla ricerca dell'attributo <name> (se esplicito) altrimenti
/// (se valore NULL) vado alla ricerca per default dell'attributo <value>.
/// In caso di errore (<element> non valido) o di attributo non trovato viene
/// restituito NULL, altrimenti viene restituito il puntatore all'attributo.
/// Nota: L'attributo non viene copiato in una nuova struttura!
///
/// \date [06.12.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
XML_ATTRIBUTE * XMLGetAttrib (XML_ELEMENT * element, char * name)
{
	XML_ATTRIBUTE * out     = NULL; // Puntatore per l'attributo XML risultante
	XML_ATTRIBUTE * attrib  = NULL; // Puntatore di appoggio per attributi XML

	if ( element != NULL ) // Controllo se l'elemento � valido
	{
		attrib  = element->attrib;
		// --- Ciclo sugli attributi dell'elemento ---
		while( attrib != NULL )
		{
			// --- Se ho specificato un particolare attributo ---
			if ( name != NULL )
			{
				if ( strcmpi( name, attrib->name ) == 0 )
				{
					out     = attrib;
					attrib  = NULL;
				}
				else
				{
					attrib  = attrib->next; // Passo all'attributo successivo (se esiste)
				}

			}
			// --- Altrimenti di default ottengo l'attributo value ---
			else
			{
				if ( strcmpi( "value", attrib->name ) == 0 )
				{
					out     = attrib;
					attrib  = NULL;
				}
				else
				{
					attrib  = attrib->next; // Passo all'attributo successivo (se esiste)
				}
			}
		}
	}
	return out;
}

//==============================================================================
/// Funzione per ottenere valori XML.
/// In base alla struttura puntata da <element> fornisce (se esiste) il valore
/// (una copia) dell'attributo dal nome <name> (se esiste).
/// In caso di errore o di mancanza di informazioni restutuisce NULL.
///
/// \date [19.01.2006]
/// \author Enrico Alborali
/// \version 0.10
//------------------------------------------------------------------------------
char * XMLGetValue( XML_ELEMENT * element, char * name )
{
  XML_ATTRIBUTE * attrib              ;
  char          * value         = NULL;
  char          * value_string  = NULL;
  int             value_len     = 0   ;
  char          * path          = NULL;
  XML_ENTITY    * curr_ent            ; // Puntatore di appoggio per entit�
  XML_ELEMENT   * curr_ele            ; // Puntatore di appoggio per elementi
  XML_ATTRIBUTE * curr_att            ; // Puntatore di appoggio per attributi
  char          * temp_string   = NULL; // Ptr a stringa temporanea

  // --- Blocco il cestino XML ---
  //XMLLockTrash();

  if ( element != NULL )
  {
    attrib = XMLGetAttrib( element, name );
    curr_ele = element; // L'elemento corrente � quello che ho passato
    if ( attrib != NULL )
    {
      if ( attrib->value != NULL )
      {
        value_string = attrib->value; // Recupero il puntatore al valore in stringa
        value_len = strlen( value_string );
        if ( value_len > 0 )
        {
          // --- Ciclo di risoluzione dei riferimenti ---
          while( XMLIsValueRef( value_string ) ) // Ciclo finch� c'� un riferimento
          {
            // --- Alloco la stringa per il percorso ---
            path = (char*) malloc( sizeof(char) * value_len ); // -MALLOC
            // --- Copio il percorso dalla stringa valore ---
            memcpy( &path[0], &value_string[1], sizeof(char) * value_len );
            // --- Risolvo il nuovo percorso ottenendo un'entit� temporanea ---
            curr_ent = XMLResolvePath( curr_ele, path, element );
            if ( curr_ent != NULL )
            {
              if ( curr_ent->type == XML_ENTITY_ELEMENT )
              {
                curr_ele = (XML_ELEMENT*)curr_ent->structure;
                if ( curr_ele != NULL )
                {
                  temp_string = XMLGetValue( curr_ele );
                }
              }
              else if ( curr_ent->type == XML_ENTITY_ATTRIBUTE )
              {
                curr_att = (XML_ATTRIBUTE*)curr_ent->structure;
                if ( curr_att != NULL )
                {
                  curr_ele = (XML_ELEMENT*) curr_att->parent;
                  temp_string = curr_att->value;
                }
                else
                {
                  /* TODO -oEnrico -cError : Gestione dell'errore */
                  curr_ele = NULL;
                  temp_string = NULL;
                }
              }
            }
            // --- Ricavo la stringa valore ---
            if ( temp_string != NULL )
            {
              value_len = strlen( temp_string );
              // --- Copio la stringa temporanea del valore ---
              value_string = XMLStrCpy( NULL, temp_string ); // -MALLOC -FREE
              XMLAddToTrash( (void*)value_string ); // -ADDTRASH
            }
            // --- Gestione della disallocazione ---
            if ( curr_ent != NULL )
            {
              if ( curr_ent->temp ) // Caso di risultato di fuznione
              {
                XMLAddToTrash( (void*)curr_ent ); // -ADDTRASH
                //XMLFreeEntity( curr_ent ); // Cancello entit� e sottostrutture
              }
              else // Caso di dato diretto
              {
                free( curr_ent ); // Cancello solo l'entit�
              }
            }
            // --- Cancello la memoria occupata dal percorso ---
            free( path ); // -FREE
          }
        }
        // --- Restituisco l'ultimo valore ---
        if ( value_string != NULL )
        {
          value = value_string;
        }
      }
    }
  }

  // --- Svuoto il cestino XML ---
  //XMLEmptyTrash( );

  // --- Sblocco il cestino XML ---
  //XMLUnlockTrash( );

  return value;
}

//==============================================================================
/// Funzione per ottenere valori XML.
/// Come la precedente ma il nome dell'attributo � implicitamente "value".
/// Restituisce una copia del valore dell'attributo.
/// In caso di errore o di mancanza di informazioni restutuisce NULL.
///
/// \date [26.03.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
char * XMLGetValue(XML_ELEMENT * element)
{
  char  * out     = NULL;
  char  * name          ;

  if (element)
  {
    name = "value";
    out = XMLGetValue(element,name);
  }

  return out;
}

//==============================================================================
/// Funzione per ottenere copie di valori XML.
/// Dato l'elemento XML <element> recupera il puntatore della stringa del valore
/// con nome <name> cercato (con <name> a NULL ottengo il valore dell'attributo
/// "value" se esiste).
/// Successivamente il valore viene copiato e restituito.
/// In caso di errore restituisce NULL.
///
/// \date [09.04.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLExtractValue(XML_ELEMENT * element,char * name)
{
  char  * out           ; // Puntatore all'uscita di GetValue
  int     out_len       ; // Lunghezza di out
  char  * result  = NULL; // Puntatore della copia del valore

  try
  {
    out = XMLGetValue(element,name);
  }
  catch (...)
  {
  }
  if (out)
  {
    out_len = strlen(out);
    if (out_len)
    {
      result = (char*) malloc(sizeof(char)*(out_len+1));
      for (int i=0; i < out_len; i++)
      {
        result[i] = out[i];
      }
      result[out_len] = '\0';
    }
  }

  return result;
}

//==============================================================================
/// Funzione per ottenere copie di valori XML.
/// Come il precedente ma viene implicitamente cercato il valore dell'attributo
/// "value".
///
/// \date [24.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLExtractValue(XML_ELEMENT * element)
{
  char * result;

  result = XMLExtractValue(element,"value");

  return result;
}

//==============================================================================
/// Funzione per ottenere valori XML.
/// Partendo dall'elemento <element> passato viene fatta la risoluzione del
/// percorso <path>.
/// Restituisce la copia del valore richiesto.
/// In caso di errore restituisce NULL.
///
/// \date [31.03.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
char * XMLGetValue( XML_ELEMENT * element, char * path, char* name )
{
  XML_ENTITY      * res_ent   = NULL;
  char            * out       = NULL;

  if (element) // Controllo l'elemento
  {
    if (path) // Guardo se un percorso � stato specificato
    {
      ///res_ent = XMLResolvePath( element, path, element, false ); // Risolvo il percorso
      res_ent = XMLResolvePath( element, path, element ); // Risolvo il percorso
      if (res_ent) // Se � stato risolto con successo
      {
        out = XMLGetValue(res_ent); // Recupero il valore
        if (res_ent->temp) // Se � il risultato di un afunzione di interfaccia
        {
          XMLFreeEntity(res_ent); // Libero entit� e sotto-strutture XML
        }
        else // Se � un risultato diretto
        {
          free(res_ent); // Libero solo l'entit�
        }
      }
    }
    else
    {
      out = XMLGetValue(element); // Prendo il valore dell'elemento passato
    }
  }

  return out;
}

//==============================================================================
/// Funzione per ottenere valori XML.
/// Simile alla precedente ma non � specificato un elemento di partenza.
/// Questo sar� determinato dal <root_element> della sorgente XML <source>
/// passata.
///
/// \date [26.03.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
char * XMLGetValue(XML_SOURCE * source, char * path)
{
  char        * out     = NULL;
  XML_ELEMENT * element = NULL;

  if (source) // Controllo la sorgente
  {
    element = source->root_element; // Prendo il <root_element>
    if (element) // Se c'� un <root_element>
		{
      out = XMLGetValue(element, path, NULL); // Recupero il valore
    }
  }

  return out;
}

//==============================================================================
/// Funzione per ottenere valori interi (32 bit senza segno) di attributi XML.
///
/// \date [13.12.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
unsigned __int32 XMLGetValueUInt32( XML_ELEMENT * element, char * name )
{
	unsigned __int32 value_uint32 = 0     ; // Valore intero
	char  * value     = NULL  ; // Valore char ptr

	if ( element != NULL )
	{
		value = XMLExtractValue( element, name );
		if ( value != NULL )
		{
			try
			{
				value_uint32 = cnv_CharPToUInt32( value );
				free( value );
			}
			catch(...)
			{
				value_uint32 = 0;
			}
		}
	}

	return value_uint32;
}

//==============================================================================
/// Funzione per ottenere valori interi di attributi XML.
/// Dato l'elemento XML <element> recupera il valore intero dell'attributo
/// con nome <name> cercato (con <name> a NULL ottengo il valore dell'attributo
/// "value" se esiste).
/// In caso di errore restituisce 0.
///
/// \date [17.06.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLGetValueInt( XML_ELEMENT * element, char * name )
{
	int     value_int = 0     ; // Valore intero
	char  * value     = NULL  ; // Valore char ptr

	if ( element != NULL )
	{
		value = XMLExtractValue( element, name );
		if ( value != NULL )
		{
			try
			{
				value_int = cnv_CharPToUInt16( value );
				free( value );
			}
			catch(...)
			{
				value_int = 0;
			}
		}
	}

	return value_int;
}

//==============================================================================
/// Funzione per ottenere valori booleani di attributi XML.
/// Dato l'elemento XML <element> recupera il valore booleano dell'attributo
/// con nome <name> cercato (con <name> a NULL ottengo il valore dell'attributo
/// "value" se esiste).
/// Restituisce il valore dell'attributo.
///
/// \date [17.06.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
bool XMLGetValueBool(XML_ELEMENT * element, char * name)
{
  bool    value_bool  = false ; // Valore booleano
  char  * value       = NULL  ; // Valore char ptr

  if (element) // Controllo l'elemento
  {
    value = XMLExtractValue(element,name);
    if (value) // Se ottengo un valore valido
    {
      if (strcmpi("true",value) == 0) value_bool = true;
      free(value); // Libero la memoria allocata
    }
  }

  return value_bool;
}

//==============================================================================
/// Funzione per ottenere id XML.
/// A partire dall'elemento XML <element> ritorna, se disponibile, il valore
/// del suo attributo denominato da "id".
/// In caso di errore o di mancato ritrovamento restituisce -1.
/// NOTA: L'attributo deve identificare un valore intero senza segno a 16 bit.
///
/// \date [30.08.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLGetID( XML_ELEMENT * element )
{
  char *  id_cp = NULL;
  int     id    = -1;

  if ( element != NULL )
  {
    id_cp = XMLGetValue( element, "id" );
    if ( id_cp != NULL ) // Se l'ha trovato
    {
      id = (int) cnv_CharPToUInt16( id_cp );
    }
  }

  return id;
}

//==============================================================================
/// Funzione per ottenere elementi XML.
/// A partire dalla lista di entit� contenuta all'interno della sorgente XML
/// <source> viene cercato il primo elemento con nome <name> e restituito.
/// In caso di errore o di mancato ritrovamento restituisce NULL.
///
/// \date [11.02.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * XMLGetFirst(XML_SOURCE * source, char * name)
{
  XML_ENTITY  * entity    = NULL;
  XML_ELEMENT * element   = NULL;
  XML_ELEMENT * out       = NULL;
  char        * ele_name  = NULL;

  if (source && name) // Controllo gli input
  {
    if (source->first_entity) // Controllo se esiste la lista di entit�
    {
      entity = source->first_entity; // Prendo la prima entit�
      while(entity)
      {
        if (entity->type == XML_ENTITY_ELEMENT)
        {
          element   = (XML_ELEMENT*) entity->structure;
          ele_name  = XMLGetName(element);
          if (strcmpi(ele_name,name)==0) // Se ho trovato l'elemento
          {
            out = element; // L'elemento l'ho trovato
            entity = NULL; // Esco dal ciclo di scansione
          }
        }
        if (entity) entity = entity->next; // Vado all'entit� successiva
      }
    }
  }

  return out;
}

//==============================================================================
/// Funzione per ottenere elementi con un nome e un id.
/// A partire dall'elemento XML <element> viene cercato, tra i suoi 'fratelli',
/// ricerca il primo elemento (se specificato con nome <name>) e con l'<id>
/// corrispondente e, se trovato, ne viene restituito il suo puntatore.
/// Se l'id = -1 viene ignorato e viene restituito il primo elemento con nome
/// <name> (se specificato) oppure il primo elemento con qualsiasi nome.
/// In caso di errore o di mancato ritrovamento restituisce NULL.
///
/// \date [29.01.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
XML_ELEMENT * XMLGetNext( XML_ELEMENT * element, char * name, int id )
{
	XML_ELEMENT * next 				= NULL;
	XML_ELEMENT * current						;
	int           current_id				;
	int           counter_id				;

	try
	{
	  if ( element != NULL ) // Controllo l'elemento di partenza
	  {
	  	current = element; // Parto da 'element'
	  	counter_id = 0; // Resetto il contatore-ID
	  	while ( current != NULL )
	  	{
	  		current_id = XMLGetID( current );
	  		// --- Se l'elemento non ha un attributo ID (uso il contatore-ID) ---
	  		if ( current_id == -1 )
	  		{
	  			current_id = counter_id;
	  		}
				// --- Se l'elemento deve avere un nome particolare ---
				if ( name != NULL )
				{
					if ( strcmpi( name, current->name ) == 0 ) // � proprio l'elemento che cercavo!
					{
						if ( current_id == id || id == -1 ) // � proprio l'id che cercavo (oppure non mi interessa l'id)!
						{
							next = current;
							current = NULL; // Cos� esco dal ciclo
						}
						else
						{
							current = current->next; // Vado a quello successivo (se esiste)
							// --- Incremento il contatore-ID ---
							counter_id++;
						}
					}
					else
					{
						current = current->next; // Vado a quello successivo (se esiste)
					}
				}
				// --- Va bene qualsiasi elemento basta che abbia l'id giusto ---
				else
				{
					if ( current_id == id || id == -1 ) // � proprio l'id che cercavo (oppure non mi interessa l'id)!
					{
						next = current;
						current = NULL; // Cos� esco dal ciclo
					}
					else
					{
						current = current->next; // Vado a quello successivo (se esiste)
						// --- Incremento il contatore-ID ---
						counter_id++;
					}
				}
			}
		}
	}
	catch(...)
	{
		next = NULL;
	}

	return next;
}

//==============================================================================
/// Funzione per ottenere un elemento child con un nome specifico.
/// A partire dall'elemento XML <element> viene cercato, tra i suoi 'figli',
/// ricerca il primo elemento (se specificato con nome <name>) corrispondente e,
/// se trovato, ne viene restituito il suo puntatore.
/// In caso di errore o di mancato ritrovamento restituisce NULL.
///
/// \date [06.09.2012]
/// \author Enrico Alborali - Paolo Colli
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * XMLGetChild( XML_ELEMENT * element, char * name )
{
	XML_ELEMENT * current = NULL;

	try
	{
		if ( element != NULL ) // Controllo l'elemento di partenza
		{
			current = element->child; // Parto dal primo 'child'
	  		while ( current != NULL )
	  		{
				// --- Se l'elemento deve avere un nome particolare ---
				if ( name != NULL )
				{
					if ( strcmpi( name, current->name ) == 0 ) // � proprio l'elemento che cercavo!
					{
						break;
					}
					else
					{
						current = current->next; // Vado a quello successivo (se esiste)
					}
				}
				// --- Va bene qualsiasi elemento ---
				else
				{
					break;
				}
			}
		}
	}
	catch(...)
	{
		current = NULL;
	}

	return current;
}

//==============================================================================
/// Funzione per cercare strutture entit� XML.
/// Richiede come parametri la prima entit� <first> XML dove partire con la
/// ricerca e il puntatore <structure> alla struttura dell'entit� XML da
/// cercare. Restituisce il puntatore all'entit� XML associata alla struttura.
/// In caso di errore (parametri non validi o struttura non trovata) restituisce
/// NULL.
///
/// \date [09.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
XML_ENTITY * XMLGetEntity(XML_ENTITY * first,void * structure)
{
  XML_ENTITY * entity = NULL; // Puntatore a entit� trovata
  XML_ENTITY * current;       // Puntatore a entit� di appoggio

  if (first) // Controllo se punta ad una sorgente
  {
    current = first; // Prendo la prima entit� XML da dove partire
    if (structure) // Controllo se punta ad un astruttura di entit�
    {
      while (current) // Scorro tutta la lista delle entit�
      {
        if (current->structure == structure) // Se ho trovato l'entit� XML
        {
          entity = current; // Salvo il puntatore a questa entit� XML
          current = NULL;   // Per uscire dal ciclo while
        }
        else
        {
          current = current->next; // Prendo l'entit� successiva
        }
      }
    }
  }

  return entity;
}

//==============================================================================
/// Funzione per copiare il valore di un attributo
///
/// \date [03.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * XMLCopyValue( XML_ELEMENT * element, char * name )
{
  char * value      = NULL; // Ptr al valore originale
  char * value_copy = NULL; // Ptr alla copia del valore
  int    value_len  = 0   ; // Lunghezza del valore da copiare

  if ( element != NULL )
  {
    value = XMLGetValue( element, name );
    if ( value != NULL )
    {
      value_len = strlen( value );
      if ( value_len > 0 )
      {
        value_copy = (char*)malloc( sizeof(char) * ( value_len + 1 ) ); // Alloco il nuovo valore
        if ( value_copy != NULL ) // Se l'allocazione � andata a buon fine
        {
          strcpy( value_copy, value ); // Copio il valore
        }
      }
    }
  }

  return value_copy;
}

//==============================================================================
/// Funzione per includere sottostrutture XML.
/// Sostituisce l'entit� <inc_point> che � il punto di inclusione con un albero
/// la cui root � definita dall'entit� <inc_root> ovvero la root di inclusione.
///
/// NOTE: Il parametro <inc_point> passato come perametro devono essere del tipo
/// XML_ENTITY_ELEMENT altrimenti viene restituito XML_ERROR_INVALID_ENTITY.
/// Vengono copiati gli elementi XML dall'albero sorgente verso il punto di
/// inclusione e viene aggiornata la lista delle entit� XML.
/// La struttura copiata partir� dalla prima entit� che contiene un elemento XML
/// e poi seguiranno tutte le rimanenti entit�.
/// La struttura da includere puntata da <inc_root> viene opportunamente
/// modificata in modod che la si possa eliminare in un secondo momento senza
/// problemi.
///
/// In caso di successo restituisce il valore 0.
///
/// \date [29.06.2006]
/// \author Enrico Alborali
/// \version 1.06
//------------------------------------------------------------------------------
int XMLInclude( XML_ENTITY * inc_point, XML_ENTITY * inc_root )
{
  int             ret_code      = XML_INT_NO_ERROR; // Codice di ritorno
  XML_ENTITY    * inc_head      = NULL; // Ptr alla prima entit� XML da includere
  XML_ENTITY    * inc_foot      = NULL; // Ptr all'ultima entit� XML da includere
  XML_ENTITY    * entity        = NULL; // Puntatore di appoggio per le nuove entit�
  XML_ELEMENT   * inc_point_ele = NULL;
  XML_ELEMENT   * inc_head_ele  = NULL;
  XML_ENTITY    * attrib_entity = NULL;

  if ( inc_point != NULL ) // Controllo l'inclusion point
  {
    if ( inc_point->type == XML_ENTITY_ELEMENT ) // Se l'iclusion point � un elemento
    {
      // --- Recupero l'elemento XML dell'inc_point ---
      inc_point_ele = (XML_ELEMENT*) inc_point->structure;
      if ( inc_root != NULL ) // Controllo l'inclusion root
      {
        // --- Recupero la prima entit� da includere ---
        entity = inc_root;
        // --- Cerco l'inc_head, ovvero la prima entit� tipo elemento ---
        while ( entity != NULL )
        {
          // --- Verifico di aver trovato un elemento XML ---
          if ( entity->type == XML_ENTITY_ELEMENT )
          {
            inc_head = entity; // Salvo l'inc_head...
            break; // ... ed esco dal ciclo while
          }
          // --- Altrimenti vado avanti ---
          else
          {
            entity = entity->next;
          }
        }
        // --- Verifico di aver trovato l'inc_head ---
        if ( inc_head != NULL )
        {

          // --- Recupero l'element XML dell'inc_head ---
          inc_head_ele = (XML_ELEMENT*) inc_head->structure;
          // --- Ciclo sulle entit� XML cercando l'inc_foot ---
          while ( entity != NULL )
          {
            inc_foot = entity; // Prendo come ultima entit� quella corrente
            entity->level += inc_point->level; // Aumento il livello dell'entit� da includere
            entity = entity->next;
          }
          // --- (1) Copio alcuni parametri da inc_point_ele a inc_head_ele ---
          inc_head_ele->prev   = inc_point_ele->prev   ;
          inc_head_ele->next   = inc_point_ele->next   ;
          inc_head_ele->parent = inc_point_ele->parent ;
          inc_head_ele->pos    = inc_point_ele->pos    ;
          // --- (1.1) Collego il prev di inc_head_ele ---
          if ( inc_head_ele->prev != NULL )
          {
            inc_head_ele->prev->next = inc_head_ele;
          }
          // --- (1.2) Collego il next di inc_head_ele ---
          if ( inc_head_ele->next != NULL )
          {
            inc_head_ele->next->prev = inc_head_ele;
          }
          // --- (1.3) Collego il parent di inc_head_ele ---
          if ( inc_head_ele->parent != NULL )
          {
            if ( inc_point_ele->parent->child == inc_point_ele )
            {
              inc_head_ele->parent->child = inc_head_ele;
            }
          }
          // --- (2) Collego l'inc_head_ele a inc_point ---
          inc_point->structure = (void*)inc_head_ele;
          //* BUGFIX: 20050907-01
          // Devo cancellare gli attributi dell'elemento originariamente linkato
          // dall'inc_point altrimenti non vengono eliminati correttamente
          // --- (2.1) Cancello tutti gli attributi che seguono l'inc_point ---
          entity = inc_point->next;
          while ( entity != NULL )
          {
            if ( entity->type == XML_ENTITY_ATTRIBUTE )
            {
              attrib_entity = entity;
              entity = entity->next;
              XMLFreeEntity( attrib_entity );
            }
            else
            {
              break;
            }
          }
          // --- (2.2) Collego l'inc_point alla prima entit� non attributo ---
          inc_point->next = entity;
          if ( entity != NULL )
          {
            entity->prev = inc_point;
          }
          // --- (2.3) Taglio il colleg. con gli attributi di inc_point_ele ---
          inc_point_ele->attrib = NULL;
          //*/
          // --- (3) Collego l'inc_point_ele a inc_head ---
          inc_head->structure = (void*)inc_point_ele;
          // --- (4) Collego inc_point ad un'eventuale lista di entit� XML ---
          if ( inc_head->next != NULL )
          {
            // --- (4.1) Collego l'inc_foot al resto della struttura XML ---
            if ( inc_foot != NULL )
            {
              inc_foot->next = inc_point->next;
              if ( inc_point->next != NULL )
              {
                inc_point->next->prev = inc_foot;
              }
            }
            // --- (4.2) Collego il next dell'inc_point ---
            inc_point->next = inc_head->next;
            inc_point->next->prev = inc_head;
          }
          // --- (5) Taglio il collegamento con le entit� XML successive ---
          inc_head->next = NULL;
        }
        else
        {
          ret_code = XML_INT_ERROR_NO_ELEMENT_FOUND;
        }
      }
      else
      {
        ret_code = XML_ERROR_INVALID_INCLUSION;
      }
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ENTITY;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per il processamento delle inclusioni XML.
/// Scansiona la struttura estratta dalla sorgente XML <source> verificando se
/// vi sono presenti elementi di inclusione (<include/>).
/// Se ne trova effettua tutte le inclusioni andando a cercare gli elementi di
/// root per l'inclusione specificati dall'attributo <name> degli elementi.
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int XMLProcInc( XML_SOURCE * source )
{
  int           ret_code    = 0   ;
  XML_SOURCE  * inc_source  = NULL;
  XML_ENTITY  * entity      = NULL;
  XML_ENTITY  * next        = NULL;
  XML_ELEMENT * element     = NULL;
  XML_ELEMENT * inc_element = NULL;
  char        * inc_file    = NULL;
  char        * inc_name    = NULL;

  if ( source != NULL ) // Controllo se la sorgente � valida
  {
    entity = source->first_entity; // Prendo il puntatore alla prima entit�

    while( entity != NULL ) // Controllo se l'entit� � valida
    {
      next = entity->next; // Salvo il puntattore alla prossima entit�
      if ( entity->type == XML_ENTITY_ELEMENT ) // Se l'entit� � un elemento XML
      {
        element = (XML_ELEMENT*) entity->structure;
        if ( strcmpi( XMLGetName( element ), "include" ) == 0 ) // Se � un'elemento di inclusione
        {
          inc_file = XMLGetValue( element, "src" ); // Recupero il file da includere
          inc_name = XMLGetValue( element, "name" ); // Recupero il root da includere
          if ( inc_file != NULL ) // Se esiste un file XML da includere
          {
            inc_source = XMLCreate( inc_file );
            XMLOpen( inc_source, NULL, 'r' );
            XMLRead( inc_source, NULL );
            if( inc_source != NULL ) // Se la sorgente di inclusione � valido
            {
              inc_element = XMLGetFirst( inc_source, inc_name );
              if ( inc_element != NULL )
              {
                ret_code = XMLInclude( entity, XMLGetEntity( inc_source->first_entity, (void*)inc_element ) );
                next = NULL; // Fine del ciclo
              }
            }
            else // Se la sorgente di inclusione non � valida
            {
              ret_code = XML_ERROR_INVALID_INCLUSION;
            }
            XMLClose( inc_source ); // Chiudo la sorgente di inclusione...
            XMLFree( inc_source ); // ...ed elimino la memoria allocata
		  }
        }
      }
      entity = next; // Vado all'entit� successiva (salvata precedentemente)
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_SOURCE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per settare il valori di un attributo.
/// Dato un puntatore ad attributo <attrib> setta il suo valore secondo quanto
/// contenuto in <value>.
/// In caso di errore restituisce un valore non nullo.
/// NOTA: <value> deve essere una stringa NULL-terminata. Il contenuto di
///       di <value> viene copiato all'interno della struttura <attrib> perci�
///       se allocato dinamicamente dovr� essere eliminato dopo l'esecuzione
///       della funzione.
///       Se l'attributo avesse gi� il campo <value> settato viene cancellato
///       prima di essere aggiornato.
///
/// \date [14.07.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int XMLSetValue( XML_ATTRIBUTE * attrib, char * value )
{
  int ret_code    = 0 ; // Codice di ritorno
  int value_len   = 0 ; // Lunghezza del valore

  if ( attrib != NULL )
  {
    if ( value != NULL )
    {
      // --- Elimino un eventuale valore precedente ---
      try
      {
        if ( attrib->value != NULL )
        {
          free( attrib->value ); // -FREE
          attrib->value = NULL;
        }
        // --- Copio il nuovo valore ---
        value_len       = strlen( value );
        attrib->value   = (char*) malloc( sizeof(char) * ( value_len + 1 ) ); // -MALLOC
        attrib->length  = value_len;
        strcpy( attrib->value, value );
      }
      catch(...)
      {
        ret_code = XML_INT_CRITICAL_ERROR;
        /* TODO 2 -oEnrico -cError : Gestire l'errore critico */
      }
    }
    else
    {
      ret_code = XML_ERROR_INVALID_NULLTERMSTRING;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_ATTRIBUTE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere l'ultimo attributo di un elemento.
/// Dato un elemento <element> valido, restituisce il puntatore all'ultimo suo
/// attributo.
/// In caso di errore (elemento non valido o assenza di attributi) la funzione
/// restituisce NULL.
///
/// \date [24.01.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_ATTRIBUTE * XMLGetLastAttrib(XML_ELEMENT * element)
{
  XML_ATTRIBUTE * current = NULL; // Puntatore all'attributo corrente
  XML_ATTRIBUTE * attrib  = NULL; // Puntatore all'attributo da restituire

  if ( element != NULL ) // Controllo se l'elemento � valido
  {
    try
    {
      current = element->attrib;
      while ( current != NULL ) // Ciclo fino all'ultimo
      {
        attrib = current; // Prendo come buono l'attributo corrente
        current = current->next; // Passo a quello successivo (se esiste)
      }
    }
    catch(...)
    {
      attrib = NULL;
    }
  }

  return attrib;
}

//==============================================================================
/// Funzione per aggiungere attributi.
/// Dato un attributo <attrib> valido viene inserito all'ultimo posto della
/// lista di attributi dell'elemento <element> fornito.
/// In caso di errore restituisce un codice non nullo.
///
/// \date [22.06.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
int XMLAddAttrib(XML_ELEMENT * element, XML_ATTRIBUTE * attrib)
{
  int               ret_code  = 0   ; // Codice di ritorno
  XML_ATTRIBUTE   * last      = NULL; // L'ultimo attributo delle'elemento

  if (element) // Controllo l'elemento
  {
    if (attrib) // Controllo l'attributo
    {
      last = XMLGetLastAttrib(element);
      if (last) // Se c'� gi� un atrributo
      {
        XMLAttributeLink(last,attrib); // Linko l'attributo ad un eventuale precedente
      }
      else
      {
        XMLAttributeUpLink(element,attrib); // Linko l'attributo all'elemento
      }
      attrib->pos = element->attrib_count;
      element->attrib_count++;
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ATTRIBUTE;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per rimuovere un attributo da un elemento XML
///
/// \date [28.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLRemoveAttrib( XML_ELEMENT * element, XML_ATTRIBUTE * attrib )
{
  int               ret_code    = 0   ; // Codice di ritorno
  XML_ATTRIBUTE   * attrib_prev = NULL;
  XML_ATTRIBUTE   * attrib_next = NULL;
  XML_ATTRIBUTE   * attrib_curr = NULL;

  if ( element != NULL ) // Controllo l'elemento
  {
    if ( attrib != NULL ) // Controllo l'attributo
    {
      if ( attrib->parent == element )
      {
        // --- Recupero i collegamenti con eventuali attributi vicini ---
        attrib_prev = attrib->prev;
        attrib_next = attrib->next;
        // --- Collego tra loro eventuali attributi vicini ---
        if ( attrib_prev != NULL )
        {
          attrib_prev->next = attrib_next;
        }
        if ( attrib_next != NULL )
        {
          attrib_next->prev = attrib_prev;
        }
        // --- Aggiorno un eventuale nuovo primo attributo ---
        if ( element->attrib == attrib )
        {
          element->attrib = attrib_next;
        }
        // --- Scollego l'attributo dai vicini e dall'elemento ---
        attrib->prev = NULL;
        attrib->next = NULL;
        attrib->parent = NULL;
        // --- Aggiorno il numero degli attributi ---
        element->attrib_count--;
        // --- Aggiorno il capo pos per eventuali attributi successivi ---
        attrib_curr = attrib_next;
        while( attrib_curr != NULL )
        {
          attrib_curr->pos--;
          attrib_curr = attrib_curr->next;
        }
      }
      else
      {
        ret_code = XML_INT_ERROR_NOT_PARENT_ELEMENT;
      }
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ATTRIBUTE;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare nuovi attributi.
/// Dato un elemento <element> valido viene inserito all'ultimo posto della sua
/// lista un nuovo attributo con nome e valore specificati rispettivamente dai
/// parametri <name> e <value>.
/// NOTA: Il valore puntato da <name> viene copiato prima di essere inserito
///       nella nuova struttura dell'attributo XML. Questo perch� potrebbe
///       essere fornita una costante e in fase di cancellazione dell'attributo
///       andrei a liberare un blocco di memoria non valido.
///       Inoltre anche il valore puntato da <value> viene copiato dalla
///       funzione qui utilizzata XMLSetValue.
///
/// \date [06.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
XML_ATTRIBUTE * XMLNewAttrib( XML_ELEMENT * element, char * name, char * value )
{
  XML_ATTRIBUTE   * attrib    = NULL  ; // Puntatore all'attributo aggiunto
  XML_ATTRIBUTE   * last      = NULL  ; // L'ultimo attributo delle'elemento
  char            * namecopy  = NULL  ; // Ptr alla stringa del nome (copia)
  int               namelen   = 0     ; // Lunghezza del nome attributo

  if ( element != NULL && name != NULL && value != NULL )
  {
    namelen   = strlen( name ); // Calcolo la lunghezza del nome
    namecopy  = (char*) malloc( sizeof(char) * ( namelen + 1 ) ); // -MALLOC
    if ( namecopy != NULL )
    {
      memcpy( namecopy, name, namelen + 1 ); // Faccio la copia del nome
      last    = XMLGetLastAttrib( element ); // Recupero un eventuale unltimo attributo
      attrib  = XMLCreateAttribute( namecopy, element, last ); // Creo il nuovo attributo
      XMLSetValue( attrib, value ); // Ne setto il valore
    }
  }

  return attrib;
}

//==============================================================================
/// Funzione per aggiungere elementi child.
/// Dato un elemento XML <element> viene aggiunto l'elemento <child> come primo
/// child se non ne contiene altri, altrimenti lo inserisce all'ultimo posto.
/// Nota: questa funzione non aggiunge le entit� (XML_ENTITY).
/// In caso di errore la funzione restituisce una valore non nullo.
///
/// \date [22.06.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLAddChild(XML_ELEMENT * element , XML_ELEMENT * child)
{
  int           ret_code    = 0   ; // Codice di ritorno
  XML_ELEMENT * current     = NULL; // Ptr di appoggio

  if (element) // Controllo la validit� del ptr a elemento
  {
    if (child) // Controllo la validit� del ptr a child
    {
      if (element->child_count > 0) // Se c'� gi� almeno un elemento child
      {
        current = element->child;
				while (current->next) //Finch� il child � valido
        {
          current = current->next; // Vado al child successivo
        }
        child->prev = current;
        current->next = child;
      }
      else // Non c'� nessun child
      {
        element->child  = child; // Assegno questo come primo child
        child->prev = NULL;
      }
      child->next   = NULL;
      child->parent = element;
      child->pos    = element->child_count;
      element->child_count++;
    }
    else
    {
      ret_code = XML_INT_ERROR_INVALID_CHILD;
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere un elemento XML
///
/// \date [29.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLAddElement( XML_ELEMENT * parent, XML_ELEMENT * element )
{
  int           ret_code    = XML_INT_NO_ERROR; // Codice di ritorno
  XML_ELEMENT * current     = NULL; // Ptr di appoggio

  if ( element != NULL ) // Controllo la validit� del ptr a elemento
  {
    if ( parent != NULL ) // Controllo la validit� del ptr a parent
    {
      if ( parent->child_count > 0 ) // Se c'� gi� almeno un elemento child
      {
        current = parent->child;
        while ( current->next ) //Finch� il child � valido
        {
          current = current->next; // Vado al child successivo
        }
        element->prev = current;
        current->next = element;
      }
      else // Non c'� nessun child
      {
        parent->child  = element; // Assegno questo come primo child
        element->prev = NULL;
      }
      element->next   = NULL;
      element->parent = parent;
      element->pos    = parent->child_count;
      parent->child_count++;
    }
    else
    {
      ret_code = XML_INT_ERROR_INVALID_PARENT;
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per rimuovere un elemento XML
/// NOTA: Prima di rimuovere un elemento rimuovere tutti i suoi eventuali
/// elementi child.
///
/// \date [01.07.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLRemoveElement( XML_ELEMENT * element )
{
  int           ret_code      = XML_INT_NO_ERROR; // Codice di ritorno
  XML_ELEMENT * element_prev  = NULL;
  XML_ELEMENT * element_next  = NULL;
  XML_ELEMENT * parent        = NULL;
  XML_ELEMENT * current       = NULL; // Ptr di appoggio

  if ( element != NULL ) // Controllo la validit� del ptr a elemento
  {
    element_prev = element->prev;
    element_next = element->next;
    parent = element->parent;
    if ( element->child == NULL )
    {
      // --- Collego tra loro eventuali elementi vicini ---
      if ( element_prev != NULL )
      {
        element_prev->next = element_next;
      }
      if ( element_next != NULL )
      {
        element_next->prev = element_prev;
      }
      // --- Scollego l'elemento ---
      element->prev   = NULL;
      element->next   = NULL;
      element->parent = NULL;
      // --- Aggiorno un eventuale parent ---
      if ( parent != NULL )
      {
        // --- Eventualmente riassegno il primo child nel parent ---
        if ( parent->child == element )
        {
          parent->child = element_next;
        }
        // --- Aggiorno il numero degli child del parent ---
        parent->child_count--;
      }
      // --- Aggiorno il campo pos per eventuali elementi successivi ---
      current = element_next;
      while( current != NULL )
      {
        current->pos--;
        current = current->next;
      }
    }
    else
    {
      ret_code = XML_INT_ERROR_ELEMENT_OWNS_CHILDS;
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere entit� XML
/// Inserisce l'entit� puntata da <new_entity> subito dopo l'entit� puntata da
/// <old_entity> ricostruendo i puntatori anche di un eventuale entit�
/// successiva a <old_entity>.
/// La funzione ricostruisce i campi <pos> delle eventuali entit� successive che
/// si trovano sullo stesso livello di <new_entiy>.
/// In caso di errore la funzione restituisce una valore non nullo.
///
/// \date [22.06.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLAddEntity(XML_ENTITY * old_entity, XML_ENTITY * new_entity)
{
  int           ret_code    = 0   ; // Codice di ritorno
  XML_ENTITY  * next_entity = NULL; // Ptr a entit� successiva a <old_entity>
  int           new_level   = -1  ; // Livello della nuova entit�
  int           next_level  = -1  ; // Livello dell'entit� successiva
  XML_ENTITY  * current     = NULL; // Ptr di appoggio per entit� XML

  if (old_entity && new_entity) // Verifico la validit� dei ptr alle antit� XML
  {
    next_entity = old_entity->next; // Salvo il ptr all'entit� successiva (se esiste)

    new_level        = new_entity->level;
    old_entity->next = new_entity;
    new_entity->prev = old_entity;
    new_entity->next = next_entity;
    if (next_entity) // Se c'� un'entit� successiva
    {
      next_entity->prev = new_entity;
      next_level        = next_entity->level;
      current = next_entity; // Parto dall'entit� successiva
      while (current) // Ciclo sulle entit� XML successive per sistemare i campi <pos>
      {
        next_level = current->level;
        if (next_level == new_level) // Se sono sullo stesso livello
        {
          current->pos++;
          current = current->next;
        }
        else // SOno fuori dal livello della nuova identit�
        {
          current = NULL; // Fine del ciclo
        }
      }
    }
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_ENTITY;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per rimuovere un entita' XML (dalla lista delle entita')
///
/// \date [28.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLRemoveEntity( XML_ENTITY * entity )
{
  int           ret_code    = XML_INT_NO_ERROR; // Codice di ritorno
  XML_ENTITY  * prev_entity = NULL;
  XML_ENTITY  * next_entity = NULL;

  if ( entity != NULL )
  {
    // --- Recupero i link alle entita' vicine ---
    prev_entity = entity->prev;
    next_entity = entity->next;
    // --- Sistemo i link delle entita' vicine ---
    if ( prev_entity != NULL )
    {
      prev_entity->next = next_entity;
    }
    if ( next_entity != NULL )
    {
      next_entity->prev = prev_entity;
    }
    // --- Scollego l'entita' da quelle vicine ---
    entity->prev = NULL;
    entity->next = NULL;
  }
  else
  {
    ret_code = XML_INT_ERROR_INVALID_ENTITY;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per contare i sotto-elementi.
/// Dato un elemento <element> valido vengono contati i suoi elementi child.
/// Se il parametro <sublevel> � vero vengono contati anche tutti i child dei
/// sottoelementi ricorsivamente.
/// Restituisce il numero di child torvati.
/// Nota: il conteggio viene effettuatoutilizzando i campi <child_count> e non
/// con il conteggio effettivo delle liste di elementi XML.
///
/// \date [19.04.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
int XMLGetChildCount(XML_ELEMENT * element, bool sublevel)
{
  int             count   = 0 ;
  XML_ELEMENT   * current     ;
  bool            searching   ;

  if (element) // Controllo l'elemento
  {
    count = element->child_count; // Conto semplicemente i child di <element>
    if (sublevel) // CASO SUBLEVEL: conto tutti i child di tutti i sottolivelli
    {
      current   = element->child;
      while (current) // Ciclo sugli elementi
      {
        count += current->child_count;

        if (current->child)
        {
          current = current->child;
        }
        else
        {
          if (current->next)
          {
            current = current->next;
          }
          else
          {
            searching = true;
            while (searching) // Torno al livello superiore utile
            {
              if (current->parent == element)
              {
                current = NULL; // Esco dal ciclo normalmente
                searching = false;
              }
              else
              {
                current = current->parent;
                if (current->next)
                {
                  current = current->next;
                  searching = false;
                }
              }
            }
          }
        }
      }
    }
  }

  return count;
}

//==============================================================================
/// Funzione per eseguire una funzione su rami XML.
/// Esegue la funzione puntata da <function> su tutti i child dell'elemento XML
/// <root> su tutti i livelli sottostanti. I risultati di esecuzione della
/// funzione vengono inseriti in una lista precedentemente allocata <output>.
/// In caso di errore restituisce un valore non nullo.
/// Nota: <function> deve essere del tipo:
/// int function(void * result, XML_ELEMENT * element)
///
/// \date [17.05.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int XMLChildFunction(XML_ELEMENT * root, void * output, int size, void * function)
{
  int             ret_code  = 0   ; // Codice di ritorno
  int             count     = 0   ;
  XML_ELEMENT   * current         ;
  bool            searching       ;
  XML_CHILD_FUNCTION_PTR      ptf ;
  unsigned char * outlist         ;

  if (root) // Controllo l'elemento di root
  {
    if (function) // Controllo il puntatore a funzione
    {
      ptf = (XML_CHILD_FUNCTION_PTR) function;
      if (output && size>=1)
      {
        outlist = (unsigned char *) output;
        current   = root->child; // Prendo il primo child
        while (current) // Ciclo sugli elementi child
        {
          ret_code = ptf((void*)&outlist[size*count],current); // Eseguo la funzione
          count++;
          if (current->child)
          {
            current = current->child;
          }
          else
          {
            if (current->next)
            {
              current = current->next;
            }
            else
            {
              searching = true;
              while (searching) // Torno al livello superiore utile
              {
                if (current->parent == root)
                {
                  current = NULL; // Esco dal ciclo normalmente
                  searching = false;
                }
                else
                {
                  current = current->parent;
                  if (current->next)
                  {
                    current = current->next;
                    searching = false;
                  }
                }
              }
            }
          }
        }
      }
      else
      {

      }
    }
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere la dimensione in bit di un tipo
/// Restituisce la dimensione in bit per il tipo di dato <type> passato.
/// Se la dimensione non � specificata restituisce 0.
///
/// \date [12.10.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
int XMLBitSizeOf(int type)
{
  int size  = 0; // Dimensione in bit
  int mod1K = 0; // Modulo 1000

  type = abs(type);
  mod1K = type % 1000   ; // Calcolo il mod 1K

  size = mod1K;

  return size;
}

//==============================================================================
/// Funzione per ottenere la dimensione in byte di un tipo
///
/// \date [13.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLByteSizeOf( int type )
{
  int     size      = 0; // Dimensione in byte
  int     bit_size  = 0; // Dimensione in bit
  double  div8      = 0; // Diviso 8

  if ( type == t_string )
  {
    size = 1;
  }
  else
  {
    bit_size = XMLBitSizeOf(type);
    div8 = bit_size / 8;
    size = (int) ceil(div8);
  }

  return size;
}

//==============================================================================
/// Funzione per ottenere la dimensione in caratteri ASCII di un tipo.
/// Restituisce il numero di caratteri ASCII necessari per descrivere il tipo di
/// dato <type> passato.
/// Se la dimensione non � specificata restituisce 0.
///
/// \date [13.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int XMLCharSizeOf( int type )
{
  int size  = 0; // Dimensione in caratteri ASCII
  int mod1M = 0; // Modulo 1000000

  if ( type == t_string )
  {
    size = 1;
  }
  else
  {
	if (type<0) type = -type;
	mod1M = type % 1000000; // Calcolo il mod 1M
	size = (int) floor((float)(mod1M/1000));
  }

  return size;
}

//==============================================================================
/// Funzione per inizializzare il cestino XML
///
/// \date [08.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int XMLInitTrash( void )
{
  int ret_code = XML_INT_NO_ERROR;

  // --- Inizializzo la lista dei puntatori ---
  XMLTrashCount = 0;
  memset( XMLTrash, 0, sizeof(void*)*XML_TRASH_MAX_ELEMENTS );

  //InitializeCriticalSection( &XMLTrashCriticalSection );
  SYSInitLock(&XMLTrashLock,"XMLTrashLock");

  return ret_code;
}

//==============================================================================
/// Funzione per bloccare l'accesso al cestino XML per gli altri thread
/// \date [08.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int XMLLockTrash( char * label )
{
  int ret_code = XML_INT_NO_ERROR;

  try
  {
	// --- Entro nella sezione critica (e la blocco) ---
	//EnterCriticalSection( &XMLTrashCriticalSection );
	SYSLock( &XMLTrashLock, label);
  }
  catch(...)
  {
	ret_code = XML_INT_ERROR_INVALID_CRITICAL_SECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere un puntatore di memoria al cestino XML
///
/// \date [08.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int XMLAddToTrash( void * memptr )
{
  int ret_code = XML_INT_NO_ERROR;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &XMLTrashCriticalSection );
  SYSLock( &XMLTrashLock, "XMLAddToTrash");

  if ( memptr != NULL )
  {
	if ( XMLTrashCount < XML_TRASH_MAX_ELEMENTS )
	{
	  // --- Salvo il puntatore ---
	  XMLTrash[XMLTrashCount] = memptr;
	  // --- Aggiorno il contatore ---
	  XMLTrashCount++;
	}
	else
	{
	  ret_code = XML_INT_ERROR_TRASH_FULL;
	}
  }
  else
  {
	ret_code = XML_INT_ERROR_INVALID_POINTER;
  }

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &XMLTrashCriticalSection );
  SYSUnLock( &XMLTrashLock );

  return ret_code;
}

//==============================================================================
/// Funzione per svuotare il cestino XML
///
/// \date [27.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLEmptyTrash( void )
{
  int     ret_code  = XML_INT_NO_ERROR;
  void  * memptr    = NULL;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &XMLTrashCriticalSection );
  SYSLock( &XMLTrashLock, "XMLEmptyTrash");

  if ( XMLTrashCount > 0 )
  {
	// --- Ciclo di cancellazione puntatori ---
	for ( int t = 0; t < XMLTrashCount; t++ )
	{
	  memptr = XMLTrash[t];
	  if ( memptr != NULL )
	  {
		try
		{
		  free( memptr );
		}
		catch(...)
		{
		  /* TODO 1 -oEnrico -cError : Gestione dell'errore critico */
		}
	  }
	}
	// --- Inizializzo la lista dei puntatori ---
	XMLTrashCount = 0;
	memset( XMLTrash, 0, sizeof(void*)*XML_TRASH_MAX_ELEMENTS );
  }
  else
  {
	ret_code = XML_INT_ERROR_TRASH_EMPTY;
  }

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &XMLTrashCriticalSection );
  SYSUnLock( &XMLTrashLock );

  return ret_code;
}

//==============================================================================
/// Funzione per sbloccare l'accesso al cestino XML per gli altri thread
/// \date [19.01.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLUnlockTrash( void )
{
  int ret_code = XML_INT_NO_ERROR;

  try
  {
	// --- Esco nella sezione critica (e la sblocco) ---
	//LeaveCriticalSection( &XMLTrashCriticalSection );
	SYSUnLock( &XMLTrashLock );
  }
  catch(...)
  {
	ret_code = XML_INT_ERROR_INVALID_CRITICAL_SECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il cestino XML
///
/// \date [27.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLClearTrash( void )
{
  int ret_code = XML_INT_NO_ERROR;

  //DeleteCriticalSection( &XMLTrashCriticalSection );
  SYSClearLock(&XMLTrashLock);

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere un pezzo di stringa
///
/// \date [29.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * XMLAddToString( char * old_str, char * tok_str )
{
  char  * new_str   = NULL; // ptr alla nuova stringa
  int     old_len   = 0   ; // Lunghezza precedente della stringa
  int     tok_len   = 0   ; // Lunghezza del nuovo pezzo di stringa

  // --- Ricavo la lunghezza precedente della stringa ---
  if ( old_str != NULL )
  {
    try
    {
      old_len = strlen( old_str );
    }
    catch(...)
    {
      old_len = 0;
      old_str = NULL;
    }
  }
  else
  {
    old_len = 0;
  }
  // --- Ricavo la lunghezza
  if ( tok_str != NULL )
  {
    try
    {
      tok_len = strlen( tok_str );
    }
    catch(...)
    {
      tok_len = 0;
      tok_str = NULL;
    }
  }
  else
  {
    tok_len = 0;
  }
  // --- Caso nuovo pezzo di stringa ---
  if ( tok_len > 0 )
  {
    // --- Caso stringa precedente ---
    if ( old_len > 0 )
    {
      new_str = (char*)realloc((void*)old_str,sizeof(char)*(old_len+tok_len+1)); // -MALLOC
      if ( new_str != NULL )
      {
        try
        {
          strcpy( &new_str[old_len], tok_str );
        }
        catch(...)
        {
          new_str = NULL;
        }
      }
    }
    // --- Caso nessuna stringa precedente ---
    else
    {
      new_str = (char*)malloc( sizeof(char)*(tok_len+1) ); // -MALLOC
      if ( new_str != NULL )
      {
        try
        {
          strcpy( new_str, tok_str );
        }
        catch(...)
        {
          new_str = NULL;
        }
      }
    }
  }
  // --- Caso nuovo pezzo di stringa non valido ---
  else
  {
    new_str = old_str;
  }

  return new_str;
}

//==============================================================================
/// Funzione per convertire un tipo esadecimale nel rispettivo decimale
///
/// \date [29.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int XMLHex2DecIntType ( int hex_type )
{
  int dec_type = 0;

  switch ( hex_type )
  {
    case t_u_hex_8:
      dec_type = t_u_int_8;
    break;
    case t_u_hex_16:
      dec_type = t_u_int_16;
    break;
    case t_u_hex_32:
      dec_type = t_u_int_32;
    break;
    case t_u_hex_64:
      dec_type = t_u_int_64;
    break;
    case t_u_hex_128:
      dec_type = t_u_int_128;
    break;
    case t_s_hex_8:
      dec_type = t_s_int_8;
    break;
	case t_s_hex_16:
      dec_type = t_s_int_16;
    break;
    case t_s_hex_32:
      dec_type = t_s_int_32;
    break;
    case t_s_hex_64:
      dec_type = t_s_int_64;
    break;
    case t_s_hex_128:
      dec_type = t_s_int_128;
    break;
    default:
      dec_type = 0;
  }

  return dec_type;
}


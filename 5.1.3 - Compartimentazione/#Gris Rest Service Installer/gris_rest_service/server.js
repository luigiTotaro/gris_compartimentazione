'use strict';
var express = require('express');
var app = express();
const config = require('./config.js');
const endpoints = require('./classes/endpoints.js');
const databaseCentral = require('./classes/database/db_connection_central.js');
const databaseEvents = require('./classes/database/db_connection_events.js');
const logger = require('./classes/utils/logger.js');
const http = require('http');
const https = require('https');
var fs = require('fs');

if (config.connectToDB)
{
    logger.log ("info","Try connection to DBs");
    databaseCentral.connect((err) => {

        if (err) logger.log ("error","Error connecting to Central DB: " + err);
        databaseEvents.connect((err) => {
            if (err) logger.log ("error","Error connecting to Events DB: " + err);
        
            start();
        
        });
    
    });
}
else
{
    logger.log ("info","Override connection to DBs");
    start();
}

function start()
{
    
    var options = {
        //key: fs.readFileSync( './cert/localhost.key' ),
        //cert: fs.readFileSync( './cert/localhost.cert' ),
        //key: fs.readFileSync( './cert/gristest_rfi_it_dec.key' ),
        //cert: fs.readFileSync( './cert/gristest_rfi_it.crt' ),
        key: fs.readFileSync( './cert/grisconcentratore_rfi_it_dec.key' ),
        cert: fs.readFileSync( './cert/grisconcentratore_rfi_it.crt' ),
        requestCert: false,
        rejectUnauthorized: false
    };

    
    // versione http
    /*
    var httpServer = http.createServer(app);
    httpServer.listen(config.http.port, '0.0.0.0', function() {
        logger.log("info", `STARTED ON ${config.http.port}`);
    });
    */
    
    // versione https
    var httpsServer = https.createServer( options, app );
    httpsServer.listen(config.http.port, '0.0.0.0', function() {
        logger.log("info", `STARTED ON ${config.http.port}`);
    });

    
    /*
    // vecchia versione
    app.listen(config.http.port, '0.0.0.0', function() {
        logger.log("info", `STARTED ON ${config.http.port}`);
    });
    */

    
    endpoints.init(app);
}





/**
 * modulo di configurazione contiene i parametri di inizializzazione di tutta l'applicazione.
 * Questo modulo deve accentrare tutte le opzioni di configurazione
 * @module config

 * @param {json} http - opzioni per la connessione in ascolto
 * @param {int} http.port - porta sulla quale è in ascolto il servizio REST
 * @param {json} database - opzioni per la connessione verso un database mssql
 * @param {boolean} database.start_local_sql_browser - true/false, indica se far partire il servizio SQL Browser perima di connettersi al db
 * @param {string} database.user - nome utente
 * @param {string} database.password - password
 * @param {string} database.server - indirizzo ip del server
 * @param {string} database.database - nome del database
 * @param {int} database.pool.max - numero massimo di connessioni verso il pool
 * @param {int} database.pool.min - numero minimo di connessioni verso il pool
 * @param {int} database.pool.idleTimeoutMillis - tempo di inattività prima di chiudere una connessione
 * @param {int} database.options.encrypt - crypt della connessione (true se Windows Azure)
 * @param {json} logfile - opzioni per il logging su file secondo le modalità della libreria winston {@link https://github.com/winstonjs/winston}
 * @param {string} logfile.file - nome del file (oppure nome con percorso completo)
 * @param {int} logfile.maxsize - massima dimensione del file di log prima di effettuare una rotazione

 */


module.exports = {

    queue: {
        max_processing_document: 10,
        document_age_limit: 25000
    },

	connectToDB: true,

	testMode: true,

	showErrorMessage: true,

    http: {
        port: 8916
    },

    database_central: {
		//SVILUPPO
		
		start_local_sql_browser: false,
		user: 'telefinDev',
		password: 'telefinDev',
	    //server: '192.170.5.21',
	    server: '10.211.55.6',

		database: 'TelefinCentralPRO',

	    options: {
	        encrypt: false 
	    },
	    
		
		// PRODUZIONE
		/*
		start_local_sql_browser: true,
	    user: 'sa',
	    password: 'StLc!000',
	    server: '127.0.0.1',
	    database: 'telefin',

	    options: {
	        encrypt: false,
			instanceName: 'SQLEXPRESS',
			trustedConnection: true,
			localAddress: '127.0.0.1'
	    },
	    */

	    pool: {
	        max: 10,
	        min: 5,
	        idleTimeoutMillis: 10000
	    }

    },
	
    database_events: {
		//SVILUPPO
		
		start_local_sql_browser: false,
		user: 'telefinDev',
		password: 'telefinDev',
	    //server: '192.170.5.21',
	    server: '10.211.55.6',

		database: 'TelefinEvents',

	    options: {
	        encrypt: false 
	    },
	    
		
		// PRODUZIONE
		/*
		start_local_sql_browser: true,
	    user: 'sa',
	    password: 'StLc!000',
	    server: '127.0.0.1',
	    database: 'telefin',

	    options: {
	        encrypt: false,
			instanceName: 'SQLEXPRESS',
			trustedConnection: true,
			localAddress: '127.0.0.1'
	    },
	    */

	    pool: {
	        max: 10,
	        min: 5,
	        idleTimeoutMillis: 10000
	    }

	},
		
    logfile: {
		file: 'log.txt',
		level: 'info', //debug, info, warn, error
		maxsize: 100*1024*5,
		maxfiles: 1
	}


}
"use strict";
const db_conn = require('./db_connection_central.js');
const logger = require('../utils/logger.js'); 
const config = require('../../config.js');
const fs = require('fs');
const path = require('path');

module.exports = {
	


	/** 
 	* @function 'configFull' - Aggiorna i dati sulle tabelle interessate dal messaggio configFull
 	* @param {callback} on_complete - function(err, result)
 	*/	
	configFull: function(oggetto, on_complete) {

		let arrayQuery= new Array();
		let arrayCancellazioni= new Array(); //le cancellazioni dovranno essere fatte al contgrario di come inserite, per questioni di intergità referenziale

		//aggiornare le righe di server
		if (oggetto.servers)
		{
			for (var i=0;i<oggetto.servers.length;i++)
			{
				let server=oggetto.servers[i];
				//ci sono dei dati binari, devo preparare il dato da inviare
				let clientKey=null;
				if (server.ClientKey!=null)
				{
					clientKey="0x";
					for (var t=0;t<server.ClientKey.data.length;t++)
					{
						clientKey+=server.ClientKey.data[t].toString(16);
					}
				}
				let clientValidationSign=null;
				if (server.ClientValidationSign!=null)
				{
					clientValidationSign="0x";
					for (var t=0;t<server.ClientValidationSign.data.length;t++)
					{
						clientValidationSign+=server.ClientValidationSign.data[t].toString(16);
					}
				}

				//controllo Date, aggiungendo gli apici
				if (server.LastUpdate!=null)
				{
					server.LastUpdate = `'${server.LastUpdate}'`; 
				}				
				if (server.ClientDateValidationRequested!=null)
				{
					server.ClientDateValidationRequested = `'${server.ClientDateValidationRequested}'`; 
				}	
				if (server.ClientDateValidationObtained!=null)
				{
					server.ClientDateValidationObtained = `'${server.ClientDateValidationObtained}'`; 
				}	
				
				
				//controlli campi xml
				let SupervisorSystemXML=server.SupervisorSystemXML==null?'NULL':server.SupervisorSystemXML.replace(/'/g, "''");
				let ClientSupervisorSystemXMLValidated=server.ClientSupervisorSystemXMLValidated==null?'NULL':server.ClientSupervisorSystemXMLValidated.replace(/'/g, "''");
				
				
				let sql= `UPDATE servers set Name='${server.Name}', 
							Host='${server.Host}', 
							FullHostName='${server.FullHostName}', 
							IP='${server.IP}', 
							LastUpdate=${server.LastUpdate}, 
							LastMessageType='${server.LastMessageType}', 
							ClientSupervisorSystemXMLValidated='${ClientSupervisorSystemXMLValidated}', 
							SupervisorSystemXML='${SupervisorSystemXML}',
							ClientValidationSign=${clientValidationSign}, 
							ClientDateValidationRequested=${server.ClientDateValidationRequested}, 
							ClientDateValidationObtained=${server.ClientDateValidationObtained}, 
							ClientKey=${clientKey},  
							NodID='${server.NodID}', 
							ServerVersion='${server.ServerVersion}', 
							MAC='${server.MAC}', 
							IsDeleted=${server.Removed} 
							WHERE SrvID=${server.SrvID}
							IF @@ROWCOUNT = 0 
							INSERT INTO servers (SrvID,Name,Host,FullHostName,IP,LastUpdate,LastMessageType,SupervisorSystemXML,ClientSupervisorSystemXMLValidated,ClientValidationSign,ClientDateValidationRequested,ClientDateValidationObtained,ClientKey,NodID,ServerVersion,MAC,IsDeleted) 
							VALUES (${server.SrvID},'${server.Name}','${server.Host}','${server.FullHostName}','${server.IP}',${server.LastUpdate},'${server.LastMessageType}','${SupervisorSystemXML}','${ClientSupervisorSystemXMLValidated}',${clientValidationSign},${server.ClientDateValidationRequested},${server.ClientDateValidationObtained},${clientKey},'${server.NodID}','${server.ServerVersion}','${server.MAC}',${server.Removed})`;
				arrayQuery.push(sql);
				//console.log(`Inserita query per tabella servers con SrvID: ${server.SrvID}`);
			}
		}
		
		//aggiornare le righe di port
		if (oggetto.port)
		{
			for (var i=0;i<oggetto.port.length;i++)
			{
				let port=oggetto.port[i];
				
				//è un inserimento o una cancellazione?
				let sql="";
				if (port.grisAskRemoval==true)
				{
					sql= `DELETE FROM port WHERE PortID='${port.PortID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					sql= `UPDATE port set PortXMLID=${port.PortXMLID}, 
								PortName='${port.PortName}', 
								PortType='${port.PortType}', 
								Parameters='${port.Parameters}', 
								Status='${port.Status}', 
								Removed=${port.Removed}, 
								SrvID=${port.SrvID}
								WHERE PortID='${port.PortID}'
								IF @@ROWCOUNT = 0 
								INSERT INTO port (PortID, PortXMLID, PortName, PortType, Parameters, Status, Removed, SrvID) 
								VALUES ('${port.PortID}',${port.PortXMLID},'${port.PortName}','${port.PortType}','${port.Parameters}','${port.Status}',${port.Removed},${port.SrvID})`;
					arrayQuery.push(sql);
				}
				//console.log(`Inserita query per tabella port con PortID: ${port.PortID}`);
				//console.log(sql);
			}
		}

		//aggiornare le righe di station
		if (oggetto.station)
		{
			for (var i=0;i<oggetto.station.length;i++)
			{
				let station=oggetto.station[i];
				
				let sql="";
				if (station.grisAskRemoval==true)
				{
					sql= `DELETE FROM station WHERE StationID='${station.StationID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					sql= `UPDATE station set StationXMLID=${station.StationXMLID}, 
								StationName='${station.StationName}', 
								Removed=${station.Removed} 
								WHERE StationID='${station.StationID}'
								IF @@ROWCOUNT = 0 
								INSERT INTO station (StationID, StationXMLID, StationName, Removed) 
								VALUES ('${station.StationID}', ${station.StationXMLID}, '${station.StationName}', ${station.Removed})`;
					arrayQuery.push(sql);
				}
				//console.log(`Inserita query per tabella station con StationID: ${station.StationID}`);
				//console.log(sql);
			}
		}

		//aggiornare le righe di building
		if (oggetto.building)
		{
			for (var i=0;i<oggetto.building.length;i++)
			{
				let building=oggetto.building[i];
				
				let sql="";
				if (building.grisAskRemoval==true)
				{
					sql= `DELETE FROM building WHERE BuildingID='${building.BuildingID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					sql= `UPDATE building set BuildingXMLID=${building.BuildingXMLID}, 
								StationID='${building.StationID}', 
								BuildingName='${building.BuildingName}', 
								BuildingDescription='${building.BuildingDescription}', 
								Removed=${building.Removed} 
								WHERE BuildingID='${building.BuildingID}'
								IF @@ROWCOUNT = 0 
								INSERT INTO building (BuildingID, BuildingXMLID, StationID, BuildingName, BuildingDescription, Removed) 
								VALUES ('${building.BuildingID}', ${building.BuildingXMLID}, '${building.StationID}', '${building.BuildingName}', '${building.BuildingDescription}', ${building.Removed})`;
					arrayQuery.push(sql);
				}
				//console.log(`Inserita query per tabella building con BuildingID: ${building.BuildingID}`);
				//console.log(sql);
			}
		}


		//aggiornare le righe di rack
		if (oggetto.rack)
		{
			for (var i=0;i<oggetto.rack.length;i++)
			{
				let rack=oggetto.rack[i];
				
				let sql="";
				if (rack.grisAskRemoval==true)
				{
					sql= `DELETE FROM rack WHERE RackID='${rack.RackID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					sql= `UPDATE rack set RackXMLID=${rack.RackXMLID}, 
								BuildingID='${rack.BuildingID}', 
								RackName='${rack.RackName}', 
								RackType='${rack.RackType}', 
								RackDescription='${rack.RackDescription}', 
								Removed=${rack.Removed} 
								WHERE RackID='${rack.RackID}'
								IF @@ROWCOUNT = 0 
								INSERT INTO rack (RackID, RackXMLID, BuildingID, RackName, RackType, RackDescription, Removed) 
								VALUES ('${rack.RackID}', ${rack.RackXMLID}, '${rack.BuildingID}', '${rack.RackName}', '${rack.RackType}', '${rack.RackDescription}', ${rack.Removed})`;
					arrayQuery.push(sql);
				}
				//console.log(`Inserita query per tabella rack con RackID: ${rack.RackID}`);
				//console.log(sql);
			}
		}

		//aggiornare le righe di device
		if (oggetto.devices)
		{
			for (var i=0;i<oggetto.devices.length;i++)
			{
				let device=oggetto.devices[i];
				
				let sql="";
				if (device.grisAskRemoval==true)
				{
					//per device devo fare cancellazioni più specifiche....

					//rimuovo il device
					sql= `DELETE FROM devices WHERE DevID='${device.DevID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
					
					//rimuovo (l'unico?) device_status associati a quel device
					sql= `DELETE FROM device_status WHERE DevID='${device.DevID}'`;						
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
					
					//rimuovo tutti gli streams associati a quel device
					sql= `DELETE FROM streams WHERE DevID='${device.DevID}'`;		
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
	
					//rimuovo tutti gli stream field associati a quel device
					sql= `DELETE FROM stream_fields WHERE DevID='${device.DevID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime

				}
				else
				{
					sql= `UPDATE devices set NodID=${device.NodID}, 
								SrvID=${device.SrvID}, 
								Name='${this.checkString(device.Name)}', 
								Type='${this.checkDiscoveredType(device.Type, device.DiscoveredType)}', 
								SN='${this.checkString(device.SN)}', 
								Addr='${device.Addr}', 
								PortId='${device.PortId}',
								ProfileID=${device.ProfileID},
								Active=${device.Active},
								Scheduled=${device.Scheduled},
								RackID='${device.RackID}',
								RackPositionRow=${device.RackPositionRow},
								RackPositionCol=${device.RackPositionCol},
								DefinitionVersion='${device.DefinitionVersion}',
								ProtocolDefinitionVersion='${device.ProtocolDefinitionVersion}' 
								WHERE DevID=${device.DevID}
								IF @@ROWCOUNT = 0 
								INSERT INTO devices (DevID, NodID, SrvID, Name, Type, SN, Addr, PortId, ProfileID, Active, Scheduled, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion) 
								VALUES (${device.DevID}, ${device.NodID}, ${device.SrvID}, '${this.checkString(device.Name)}', '${this.checkDiscoveredType(device.Type, device.DiscoveredType)}', '${this.checkString(device.SN)}', '${device.Addr}', '${device.PortId}', ${device.ProfileID}, ${device.Active}, ${device.Scheduled}, '${device.RackID}', ${device.RackPositionRow}, ${device.RackPositionCol}, '${device.DefinitionVersion}', '${device.ProtocolDefinitionVersion}')`;
					arrayQuery.push(sql);
				}
				//console.log(`Inserita query per tabella devices con DevID: ${device.DevID}`);
				//console.log(sql);
			}
		}

		//aggiornare la riga di server con le informazioni addizionali
		if (oggetto.msgInfo)
		{
			let sql= `UPDATE servers set IP='${oggetto.msgInfo.externalIP}', LastUpdate=getdate(), LastMessageType='${oggetto.msgInfo.msgType}' WHERE SrvID=${oggetto.msgInfo.SrvId}`;
			arrayQuery.push(sql);
			//console.log(`Inserita query di informazioni addizionali per la tabella servers con SrvID: ${oggetto.msgInfo.SrvId}`);
			//console.log(sql);
			
		}		
		else
		{
			on_complete("ERROR - No msgInfo Section in message", null);
			return;
		}
		
		//aggiungo le cancellazioni
		for (let i=0;i<arrayCancellazioni.length;i++) arrayQuery.push(arrayCancellazioni[i]);
		logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
		this.writeTestFile(arrayQuery,"Config");
		this.processaQuery(arrayQuery,on_complete);
		
	},


		
	/** 
 	* @function 'devicestatusFull' - Aggiorna i dati sulle tabelle interessate dal messaggio devicestatusFull
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 devicestatusFull: function(oggetto, on_complete) {

		let arrayQuery= new Array();
		let arrayCancellazioni= new Array(); //le cancellazioni dovranno essere fatte al contgrario di come inserite, per questioni di intergità referenziale

		//aggiornare le righe di device_status
		if (oggetto.device_status)
		{
			for (var i=0;i<oggetto.device_status.length;i++)
			{
				let device_status=oggetto.device_status[i];
				
				let sql="";
				if (device_status.grisAskRemoval==true)
				{
					//per device_status devo fare cancellazioni più specifiche....
					
					//rimuovo (l'unico?) device_status associati a quel device_status
					sql= `DELETE FROM device_status WHERE DevID='${device_status.DevID}'`;						
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
					
					//rimuovo tutti gli streams associati a quel device_status
					sql= `DELETE FROM streams WHERE DevID='${device_status.DevID}'`;		
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
	
					//rimuovo tutti gli stream field associati a quel device_status
					sql= `DELETE FROM stream_fields WHERE DevID='${device_status.DevID}'`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					sql= `UPDATE device_status set SevLevel=${device_status.SevLevel}, 
								Description='${this.checkString(device_status.Description)}', 
								Offline=${device_status.Offline}, 
								ShouldSendNotificationByEmail=${device_status.ShouldSendNotificationByEmail} 
								WHERE DevID=${device_status.DevID}
								IF @@ROWCOUNT = 0 
								INSERT INTO device_status (DevID, SevLevel, Description, Offline, ShouldSendNotificationByEmail) 
								VALUES (${device_status.DevID}, ${device_status.SevLevel}, '${this.checkString(device_status.Description)}', ${device_status.Offline}, ${device_status.ShouldSendNotificationByEmail})`;
					arrayQuery.push(sql);
				}
				//console.log(`Inserita query per tabella device_status con DevID: ${device_status.DevID}`);
			}
		}

		//aggiornare le righe di streams
		if (oggetto.streams)
		{
			for (var i=0;i<oggetto.streams.length;i++)
			{
				let stream=oggetto.streams[i];

				let sql="";
				if (stream.grisAskRemoval==true)
				{
					//per stream devo fare cancellazioni più specifiche....
					
					//rimuovo tutti gli streams associati a quel stream
					sql= `DELETE FROM streams WHERE DevID=${stream.DevID} AND StrID=${stream.StrID}`;		
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
	
					//rimuovo tutti gli stream field associati a quel stream
					sql= `DELETE FROM stream_fields WHERE DevID=${stream.DevID} AND StrID=${stream.StrID}`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					//ci sono dei dati binari, devo preparare il dato da inviare
					let data=null;
					if (stream.Data!=null)
					{
						data="0x";
						for (var t=0;t<stream.Data.data.length;t++)
						{
							data+=stream.Data.data[t].toString(16);
						}
					}

					//controllo Date, aggiungendo gli apici
					if (stream.DateTime!=null)
					{
						stream.DateTime = `'${stream.DateTime}'`; 
					}

					sql= `UPDATE streams set Name='${this.checkString(stream.Name)}', 
						Visible=${stream.Visible}, 
						Data=${data}, 
						DateTime=${stream.DateTime}, 
						SevLevel=${stream.SevLevel}, 
						Description='${this.checkString(stream.Description)}', 
						Processed=${stream.Processed} 
						WHERE DevID=${stream.DevID} AND StrID=${stream.StrID} 
						IF @@ROWCOUNT = 0 
						INSERT INTO streams (DevID, StrID, Name, Visible, Data, DateTime, SevLevel, Description, Processed) 
						VALUES (${stream.DevID}, ${stream.StrID}, '${this.checkString(stream.Name)}', ${stream.Visible}, ${data}, ${stream.DateTime}, ${stream.SevLevel}, '${this.checkString(stream.Description)}', ${stream.Processed})`;
					arrayQuery.push(sql);
				}	
				//console.log(`Inserita query per tabella device_status con DevID: ${device_status.DevID}`);
			}
		}
		
		//aggiornare le righe di stream fields
		if (oggetto.stream_fields)
		{
			for (var i=0;i<oggetto.stream_fields.length;i++)
			{
				let stream_field=oggetto.stream_fields[i];

				let sql="";
				if (stream_field.grisAskRemoval==true)
				{	
					//rimuovo tutti gli stream field associati a quel stream
					sql= `DELETE FROM stream_fields WHERE DevID=${stream_field.DevID} AND StrID=${stream_field.StrID} AND FieldID=${stream_field.FieldID} AND ArrayID=${stream_field.ArrayID}`;				
					arrayCancellazioni.unshift(sql); //lo metto all'inizio, in modo che man mano shiftino in basso. le prime devono essere eseguite per ultime
				}
				else
				{
					//controllo ReferenceId, aggiungendo gli apici
					if (stream_field.ReferenceID!=null)
					{
						stream_field.ReferenceID = `'${stream_field.ReferenceID}'`; 
					}
					
					sql= `UPDATE stream_fields set Name='${this.checkString(stream_field.Name,64)}', 
						Visible=${stream_field.Visible}, 
						Value='${this.checkString(stream_field.Value)}', 
						Description='${this.checkString(stream_field.Description)}', 
						SevLevel=${stream_field.SevLevel}, 
						ReferenceID=${stream_field.ReferenceID}, 
						ShouldSendNotificationByEmail=${stream_field.ShouldSendNotificationByEmail} 
						WHERE DevID=${stream_field.DevID} AND StrID=${stream_field.StrID} AND FieldID=${stream_field.FieldID} AND ArrayID=${stream_field.ArrayID} 
						IF @@ROWCOUNT = 0 
						INSERT INTO stream_fields (DevID, StrID, FieldID, ArrayID, Name, Visible, Value, Description, SevLevel, ReferenceID, ShouldSendNotificationByEmail) 
						VALUES (${stream_field.DevID}, ${stream_field.StrID}, ${stream_field.FieldID}, ${stream_field.ArrayID}, '${this.checkString(stream_field.Name,64)}', ${stream_field.Visible}, '${this.checkString(stream_field.Value)}', '${this.checkString(stream_field.Description)}', ${stream_field.SevLevel}, ${stream_field.ReferenceID}, ${stream_field.ShouldSendNotificationByEmail})`;
					arrayQuery.push(sql);
				}	
				//console.log(`Inserita query per tabella device_status con DevID: ${device_status.DevID}`);
			}
		}
		

		//aggiornare la riga di server con le informazioni addizionali
		if (oggetto.msgInfo)
		{
			let sql= `UPDATE servers set IP='${oggetto.msgInfo.externalIP}', LastUpdate=getdate(), LastMessageType='${oggetto.msgInfo.msgType}' WHERE SrvID=${oggetto.msgInfo.SrvId}`;
			arrayQuery.push(sql);
			//console.log(`Inserita query di informazioni addizionali per la tabella servers con SrvID: ${oggetto.msgInfo.SrvId}`);
			//console.log(sql);
			
		}		
		else
		{
			on_complete("ERROR - No msgInfo Section in message", null);
			return;
		}

		//aggiungo le cancellazioni
		for (let i=0;i<arrayCancellazioni.length;i++) arrayQuery.push(arrayCancellazioni[i]);
		logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
		this.writeTestFile(arrayQuery,"Device_status");
		this.processaQuery(arrayQuery,on_complete);
		
	},

	/** 
 	* @function 'acks' - Aggiorna i dati sulle tabelle interessate dal messaggio acks
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 acks: function(oggetto, on_complete) {

		let arrayQuery= new Array();
		let sql="";

		//aggiornare le righe di acks
		if (oggetto.acks)
		{
			//devo cancellare tutti gli ack relativi a tutti i device di quel server
			if (oggetto.msgInfo)
			{
				let SrvId = oggetto.msgInfo.SrvId;
				sql= `DELETE FROM device_ack WHERE (DevID IN (SELECT DevID FROM devices WHERE SrvID = ${SrvId}))`;				
				arrayQuery.push(sql);
			}	
			else
			{
				on_complete("ERROR - No msgInfo Section in message", null);
				return;
			}
				
			for (var i=0;i<oggetto.acks.length;i++)
			{
				let ack=oggetto.acks[i];

				//controllo Date, aggiungendo gli apici
				if (ack.AckDate!=null)
				{
					ack.AckDate = `'${ack.AckDate}'`; 
				}	

				sql= `INSERT INTO device_ack (DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username) 
							VALUES ('${ack.DeviceAckID}', ${ack.DevID}, ${ack.StrID}, ${ack.FieldID}, ${ack.AckDate}, ${ack.AckDurationMinutes}, ${ack.SupervisorID}, '${ack.Username}')`;
				arrayQuery.push(sql);
				//console.log(`Inserita query per tabella device_ack con DeviceAckID: ${ack.DeviceAckID}`);
			}
		}

		//aggiornare la riga di server con le informazioni addizionali
		if (oggetto.msgInfo)
		{
			let sql= `UPDATE servers set IP='${oggetto.msgInfo.externalIP}', LastUpdate=getdate(), LastMessageType='${oggetto.msgInfo.msgType}' WHERE SrvID=${oggetto.msgInfo.SrvId}`;
			arrayQuery.push(sql);
			//console.log(`Inserita query di informazioni addizionali per la tabella servers con SrvID: ${oggetto.msgInfo.SrvId}`);
			//console.log(sql);
			
		}		
		else
		{
			on_complete("ERROR - No msgInfo Section in message", null);
			return;
		}

		logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
		this.writeTestFile(arrayQuery,"Acks");
		this.processaQuery(arrayQuery,on_complete);
		
	},

		/** 
 	* @function 'stlc_param' - Aggiorna i dati sulle tabelle interessate dal messaggio stlc_param
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 stlc_param: function(oggetto, on_complete) {

		if (oggetto.msgInfo)
		{
			//primo, cancello dal db tutte le righe di stlc_param che sono riferibili a quel SrvID
			let sql= `DELETE FROM stlc_parameters WHERE SrvID = ${oggetto.msgInfo.SrvId}`;
			db_conn.delete(sql, (err, result) => {
						
				if(err) {
					//logger.log("info", "ERRORE per: " + array[0].substring(0, 34));
					logger.log("info", "ERRORE per: " + sql);
					on_complete(err.message, null);
				} else {
					//procediamo agli inserimenti
					let arrayQuery= new Array();

					//aggiornare le righe di device_status
					if (oggetto.stlc_param)
					{
						for (var i=0;i<oggetto.stlc_param.length;i++)
						{
							let stlc_param=oggetto.stlc_param[i];
							
							let sql= `INSERT INTO stlc_parameters (SrvID, ParameterName, ParameterValue, ParameterDescription) 
										VALUES (${oggetto.msgInfo.SrvId}, '${this.checkString(stlc_param.ParameterName,64)}', '${this.checkString(stlc_param.ParameterValue,256)}', '${this.checkString(stlc_param.ParameterDescription,1024)}')`;
							arrayQuery.push(sql);
							//console.log(`Inserita query per tabella device_status con DevID: ${device_status.DevID}`);
						}
					}

					//aggiornare la riga di server con le informazioni addizionali
					if (oggetto.msgInfo)
					{
						let sql= `UPDATE servers set IP='${oggetto.msgInfo.externalIP}', LastUpdate=getdate(), LastMessageType='${oggetto.msgInfo.msgType}' WHERE SrvID=${oggetto.msgInfo.SrvId}`;
						arrayQuery.push(sql);
						//console.log(`Inserita query di informazioni addizionali per la tabella servers con SrvID: ${oggetto.msgInfo.SrvId}`);
						//console.log(sql);
						
					}		

					logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
					this.writeTestFile(arrayQuery,"Stlc_params");
					this.processaQuery(arrayQuery,on_complete);

				}
			})

			
		}
		else on_complete("ERROR - No MsgInfo Section in message", null);	

		
	},

		/** 
 	* @function 'is_alive' - Aggiorna i dati sulle tabelle interessate dal messaggio isAlive
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 is_alive: function(oggetto, on_complete) {

		if (oggetto.msgType)
		{
			let arrayQuery= new Array();
			let sql= `UPDATE servers set IP='${oggetto.externalIP}', LastUpdate=getdate(), LastMessageType='${oggetto.msgType}' WHERE SrvID=${oggetto.SrvId}`;
			arrayQuery.push(sql);

			logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
			this.writeTestFile(arrayQuery,"Is_Alive");
			this.processaQuery(arrayQuery,on_complete);

		}
		else on_complete("ERROR - No MsgInfo Section in message", null);	

	},


	/** 
 	* @function 'update_server' - Aggiorna i dati sulla tabella server
 	* @param {callback} on_complete - function(err, result)
 	*/	
	 update_server: function(oggetto, on_complete) {

		if (oggetto.msgInfo)
		{
			let arrayQuery= new Array();
			let sql= `UPDATE servers set IP='${oggetto.msgInfo.externalIP}', LastUpdate=getdate(), LastMessageType='${oggetto.msgInfo.msgType}' WHERE SrvID=${oggetto.msgInfo.SrvId}`;
			arrayQuery.push(sql);
			//console.log(`Inserita query di informazioni addizionali per la tabella servers con SrvID: ${oggetto.msgInfo.SrvId}`);
			//console.log(sql);
			logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
			this.processaQuery(arrayQuery,on_complete);			
		}	
		else
		{
			on_complete("ERROR - No msgInfo Section in message", null);
			return;
		}

	},

	/** 
 	* @function 'processaQuery' - Funzione ricorsiva che processa un array di query
 	* @param {callback} on_complete - function(err, result)
 	*/	
	processaQuery: function(array,on_complete) {

		if (!config.connectToDB)
		{
			logger.log("info", "Query non eseguite. Nessuna connessione ai DBs");
			on_complete(null, null);
			return;
		}
		
		//ci sono altri elementi da processare?
		if (array.length==0)
		{
			logger.log("info", "Query Terminate con successo");
			on_complete(null, null);
		}
		else
		{
			let sql=array[0];
			//console.log(array.length);
			//console.log("Query da eseguire: " + sql);
			//sql=""; //per debug

			db_conn.update(sql, (err, result) => {
						
				if(err) {
					//logger.log("info", "ERRORE per: " + array[0].substring(0, 34));
					let errorMessage="QUERY ERROR for " +  array[0] + " - Error: " + err.message;
					logger.log("error", "ERRORE per: " + array[0]);
					logger.log("error", err.message);
					on_complete(errorMessage, null);
				} else {
					//console.log("Query OK per: " + array[0].substring(0, 34));
					//tolgo l'elemento
					array.splice(0, 1);
					this.processaQuery(array,on_complete);
				}
			})
		}        	
	},

	/** 
 	* @function 'checkString' - Funzione che corregge eventuali apici presenti in una stringa destinata al db ed eventulamente la tronca a una dimensione massima
 	* @param {string} text - stringa da processare
 	* @param {int} limit - numero massimo di caratteri da restituire
 	*/		
	checkString: function(text,limit) {

		if (text==null) return null;
		var ret= text.replace(/'/g, "''");
		if (limit!=null) ret = ret.substring(0,limit);
		return ret;
	},


	/** 
 	* @function 'checkDiscoveredType' - Funzione che corregge eventuali apici presenti in una stringa destinata al db ed eventulamente la tronca a una dimensione massima, dopo aver scelto tra type e discoveredType
 	* @param {string} text - stringa da processare
 	* @param {string} text - stringa da processare
 	* @param {int} limit - numero massimo di caratteri da restituire
 	*/		
	 checkDiscoveredType: function(textType, textDiscovered, limit) {

		var text;
		if (textDiscovered!=null)
		{
			text=textDiscovered;
		}
		else
		{
			text=textType;
		}
		if (text==null) return null;
		var ret= text.replace(/'/g, "''");
		if (limit!=null) ret = ret.substring(0,limit);
		return ret;
	},	

	writeTestFile: function(array, topic) {

		if (config.testMode)
		{
			let _msg = null;

			_msg = JSON.stringify(array, null, 2);

			let x = new Date();
			let y = x.getFullYear().toString();
			let m = (x.getMonth() + 1).toString();
			let d = x.getDate().toString();
			let h = x.getHours().toString();
			let mi = x.getMinutes().toString();
			let s = x.getSeconds().toString();
			let ms = x.getMilliseconds().toString();

			(d.length == 1) && (d = '0' + d);
			(m.length == 1) && (m = '0' + m);
			(h.length == 1) && (h = '0' + h);
			(mi.length == 1) && (mi = '0' + mi);
			(s.length == 1) && (s = '0' + s);
			(ms.length == 1) && (ms = '000' + ms);
			(ms.length == 2) && (ms = '00' + ms);
			(ms.length == 3) && (ms = '0' + ms);

			var dateformat = y + m + d + "_" + h + mi + s + ms;
			
			let root_folder = path.join(__dirname, '..', '..', "testFolder");
			let file_name = path.join(root_folder, dateformat + '_' + topic + '.json');
	
			if(!fs.existsSync(root_folder)) {
				fs.mkdirSync(root_folder);
			}
	
			if(fs.existsSync(file_name)) {
				fs.unlinkSync(file_name);
			}
	
			fs.writeFileSync(file_name, _msg);
		}		
	
	},

	writeDataReceived: function(type, received, array) {

		if (config.testMode)
		{
			let _msg = null;

			
			let _array = JSON.stringify(array, null, 2);
			_msg = "Stringa ricevuta (campo data del json):\n" + received;
			_msg += "\n\nBuffer ricostruito:\n" + _array;
			

			let x = new Date();
			let y = x.getFullYear().toString();
			let m = (x.getMonth() + 1).toString();
			let d = x.getDate().toString();
			let h = x.getHours().toString();
			let mi = x.getMinutes().toString();
			let s = x.getSeconds().toString();
			let ms = x.getMilliseconds().toString();

			(d.length == 1) && (d = '0' + d);
			(m.length == 1) && (m = '0' + m);
			(h.length == 1) && (h = '0' + h);
			(mi.length == 1) && (mi = '0' + mi);
			(s.length == 1) && (s = '0' + s);
			(ms.length == 1) && (ms = '000' + ms);
			(ms.length == 2) && (ms = '00' + ms);
			(ms.length == 3) && (ms = '0' + ms);

			var dateformat = y + m + d + "_" + h + mi + s + ms;
			
			let root_folder = path.join(__dirname, '..', '..', "testFolder");
			let file_name = path.join(root_folder, dateformat + '_messaggioRicevuto_'+type+'.json');
	
			if(!fs.existsSync(root_folder)) {
				fs.mkdirSync(root_folder);
			}
	
			if(fs.existsSync(file_name)) {
				fs.unlinkSync(file_name);
			}
	
			fs.writeFileSync(file_name, _msg);
		}		
	
	}

}
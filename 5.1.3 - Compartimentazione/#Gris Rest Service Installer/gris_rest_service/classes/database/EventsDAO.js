"use strict";
const db_conn = require('./db_connection_events.js');
const logger = require('../utils/logger.js'); 
const config = require('../../config.js');
const fs = require('fs');
const path = require('path');

module.exports = {
	


	/** 
 	* @function 'events' - Aggiorna i dati sulle tabelle interessate dal messaggio events
 	* @param {callback} on_complete - function(err, result)
 	*/	
	events: function(oggetto, on_complete) {

		let arrayQuery= new Array();

		//inserire le righe di events
		if (oggetto.events)
		{
			for (var i=0;i<oggetto.events.length;i++)
			{
				let event=oggetto.events[i];
				//ci sono dei dati binari, devo preparare il dato da inviare
				let EventData=null;
				if (event.EventData!=null)
				{
					EventData="0x";
					for (var t=0;t<event.EventData.data.length;t++)
					{
						EventData+=event.EventData.data[t].toString(16);
					}
				}

				//controllo Date, aggiungendo gli apici
				if (event.Created!=null)
				{
					event.Created = `'${event.Created}'`; 
				}				
				if (event.Requested!=null)
				{
					event.Requested = `'${event.Requested}'`; 
				}
				
				//toBeDeleted
				let toBeDeleted=0;
				if (event.ToBeDeleted==true) toBeDeleted=1;
				
				
				let sql= `INSERT INTO events (EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory, Centralized) 
							VALUES ('${event.EventID}',${event.DevID},${EventData},${event.Created},${event.Requested},${toBeDeleted},${event.EventCategory},getdate())`;
				arrayQuery.push(sql);
				//console.log(`Inserita query per tabella event con EventID: ${event.EventID}`);
			}
		}
		

		//aggiornare la riga di server con le informazioni addizionali
		//l'aggiornamento deve essere fatto sul db centrale

		logger.log("info", `Query pronte per l'esecuzione: ${arrayQuery.length}`);
		this.writeTestFile(arrayQuery,"Events");
		this.processaQuery(arrayQuery,on_complete);
		
	},

	/** 
 	* @function 'processaQuery' - Funzione ricorsiva che processa un array di query
 	* @param {callback} on_complete - function(err, result)
 	*/	
	processaQuery: function(array,on_complete) {

		if (!config.connectToDB)
		{
			logger.log("info", "Query non eseguite. Nessuna connessione ai DBs");
			on_complete(null, null);
			return;
		}
		
		//ci sono altri elementi da processare?
		if (array.length==0)
		{
			logger.log("info", "Query Terminate con successo");
			on_complete(null, null);
		}
		else
		{
			let sql=array[0];
			//console.log("Query da eseguire: " + sql);
			//sql=""; //per debug

			db_conn.update(sql, (err, result) => {
						
				if(err) {
					let errorMessage="QUERY ERROR for " +  array[0] + " - Error: " + err.message;
					logger.log("error", "ERRORE per: " + array[0]);
					logger.log("error", err.message);
					on_complete(errorMessage, null);
				} else {
					//console.log("Query OK per: " + array[0].substring(0, 34));
					//tolgo l'elemento
					array.splice(0, 1);
					this.processaQuery(array,on_complete);
				}
			})
		}        	
	},

	/** 
 	* @function 'checkString' - Funzione che corregge eventuali apici presenti in una stringa destinata al db ed eventulamente la tronca a una dimensione massima
 	* @param {string} text - stringa da processare
 	* @param {int} limit - numero massimo di caratteri da restituire
 	*/			
	checkString: function(text,limit) {

		if (text==null) return null;
		var ret= text.replace(/'/g, "''");
		if (limit!=null) ret = ret.substring(0,limit);
		return ret;
	},

	writeTestFile: function(array, topic) {

		if (config.testMode)
		{
			let _msg = null;

			_msg = JSON.stringify(array, null, 2);

			let x = new Date();
			let y = x.getFullYear().toString();
			let m = (x.getMonth() + 1).toString();
			let d = x.getDate().toString();
			let h = x.getHours().toString();
			let mi = x.getMinutes().toString();
			let s = x.getSeconds().toString();
			let ms = x.getMilliseconds().toString();

			(d.length == 1) && (d = '0' + d);
			(m.length == 1) && (m = '0' + m);
			(h.length == 1) && (h = '0' + h);
			(mi.length == 1) && (mi = '0' + mi);
			(s.length == 1) && (s = '0' + s);
			(ms.length == 1) && (ms = '000' + ms);
			(ms.length == 2) && (ms = '00' + ms);
			(ms.length == 3) && (ms = '0' + ms);

			var dateformat = y + m + d + "_" + h + mi + s + ms;
			
			let root_folder = path.join(__dirname, '..', '..', "testFolder");
			let file_name = path.join(root_folder, dateformat + '_' + topic + '.json');
	
			if(!fs.existsSync(root_folder)) {
				fs.mkdirSync(root_folder);
			}
	
			if(fs.existsSync(file_name)) {
				fs.unlinkSync(file_name);
			}
	
			fs.writeFileSync(file_name, _msg);
		}		
	
	}	


}
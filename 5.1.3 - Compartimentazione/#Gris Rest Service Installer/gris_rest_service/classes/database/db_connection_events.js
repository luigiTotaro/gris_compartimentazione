const config = require('../../config.js');
const sql = require('mssql');
//const sql = require('mssql/msnodesqlv8');
const child = require('child_process');

/**
 * Modulo per la gestione della connessione al database. instanzia un pool di connessioni ed espone le primitive
 * per le operazioni verso il database
 * @module classes/database
 */
module.exports = (() => {

	var pool1 = null;
	
	var _connect = (on_complete) => {
		
		var _create_pool = (on_complete) => {
			pool1 = new sql.ConnectionPool(config.database_events, err => { 
				on_complete(err);
			});
		}
		
		if(process.platform === 'win32' && config.database_events.start_local_sql_browser === true) {
			child.exec('net start "SQL Server Browser"', (error, stdout, stderr) => {
				_create_pool(on_complete);
			});
		} else {
			_create_pool(on_complete);
		}

	};

	/** 
 	* @function 'select' - esecuzione (semplice) di una query verso il db mssql
 	* @param {string} sql - select
 	* @param {callback} on_complete - function(err, result)
 	*/
	var _select = (sql, on_complete) => {

		var request = pool1.request();

	    request.query(sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result.recordset)
        	}

	    });
 
	}


	/** 
 	* @function 'insert' - esecuzione di una insert (semplice, senza transazione) verso il db mssql
 	* @param {string} insert_sql - insert
 	* @param {callback} on_complete - function(err, result)
 	*/
	var _insert = (insert_sql, on_complete) => {

		var request = pool1.request();

	    request.query(insert_sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result);
        	}

	    });

	}



	/** 
 	* @function 'delete' - esecuzione di una query di cancellazione (semplice senza transazione)
 	* @param {string} delete_sql - delete
 	* @param {callback} on_complete - function(err, result)
 	*/
 	
	var _delete = (delete_sql, on_complete) => {

		var request = pool1.request();

	    request.query(delete_sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result);
        	}

	    });

	}
	


	/** 
 	* @function 'update' - esecuzione di una query di aggiornamento (semplice senza transazione)
 	* @param {string} update_sql - update
 	* @param {callback} on_complete - function(err, result)
 	*/
	var _update = (update_sql, on_complete) => {

		var request = pool1.request();

	    request.query(update_sql, (err, result) => {
	        
        	if(err) {
        		on_complete(err, null);
        	} else {
        		on_complete(null, result);
        	}

	    });

	}


	return {
		connect:_connect,
		select:_select,
		insert:_insert,
		delete:_delete,
		update:_update
	}


})();
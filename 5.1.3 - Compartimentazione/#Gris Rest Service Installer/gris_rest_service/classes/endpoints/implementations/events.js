'use strict';
const iEndpoint = require('../iEndpoint.js');
const _ = require('underscore');
//var zlib = require('zlib');
const logger = require('../../utils/logger.js'); 
const EventsDAO = require('../../database/EventsDAO'); 
const CentralDAO = require('../../database/CentralDAO'); 
const config = require('../../../config.js');

class GetEvents extends iEndpoint {

    constructor(express_instance) {
        super(express_instance);
    }


    attach_endpoint(express_instance) {

        express_instance.post('/events/', function(request, response) {
           
            if(!request.body.data) {
                response.status(500).json({
                    state : -3,
                    message : config.showErrorMessage==true ? 'parametro "data" obbligatorio': ''
                });
                return;
            }

            let data = request.body.data;
            data = decodeURIComponent(data);
            //let originalData = new Buffer(data, 'binary');
            //let uncompressed = zlib.gunzipSync(originalData);
            //uncompressed = uncompressed.toString();   
            let message;
            try {
                message = JSON.parse(data); 
            }
            catch(err) {
                response.status(500).json({
                    state : -1,
                    message : config.showErrorMessage==true ? 'errore durante il parsing del messaggio': ''
                });
                return;
            }

            logger.log("info", "[events] ricevuto");
            EventsDAO.events(message, (err) => {
                if (err!=null)
                {
                    response.status(500).json({
                        state : -2,
                        message : config.showErrorMessage==true ? err: ''
                    });
                }
                else
                {
                    CentralDAO.update_server(message, (err,res) => {
                        if (err!=null)
                        {
                            response.status(500).json({
                                state : -2,
                                message : config.showErrorMessage==true ? err: ''
                            });
                        }
                        else response.status(200).json({
                            state : 0,
                            message : config.showErrorMessage==true ? 'comando [events] ricevuto e processato': ''
                        });
                    })

                }
            
            })
    
        });
        
        
    }

}

module.exports = GetEvents;
'use strict';
const iEndpoint = require('../iEndpoint.js');
const _ = require('underscore');
var zlib = require('zlib');
const logger = require('../../utils/logger.js'); 
const CentralDAO = require('../../database/CentralDAO'); 
const config = require('../../../config.js');

class GetConfigFull extends iEndpoint {

    constructor(express_instance) {
        super(express_instance);
    }


    attach_endpoint(express_instance) {

        express_instance.post('/config_full/', function(request, response) {
           
            if(!request.body.data) {
                response.status(500).json({
                    state : -3,
                    message : config.showErrorMessage==true ? 'parametro "data" obbligatorio': ''
                });
                return;
            }

            let data = request.body.data;
            //let data = decodeURIComponent(data);

            let uncompressed;
            try {
                let originalData = new Buffer(data, 'binary');
                CentralDAO.writeDataReceived("config_full", data, originalData);
                uncompressed = zlib.gunzipSync(originalData);
                uncompressed = uncompressed.toString();   
            }
            catch(err) {
                response.status(500).json({
                    state : -4,
                    message : config.showErrorMessage==true ? 'errore durante la decompressione del messaggio': ''
                });
                return;
            }


            let message;
            try {
                message = JSON.parse(uncompressed); 
            }
            catch(err) {
                response.status(500).json({
                    state : -1,
                    message : config.showErrorMessage==true ? 'errore durante il parsing del messaggio': ''
                });
                return;
            }

            logger.log("info", "[config_full] ricevuto");
            CentralDAO.configFull(message, (err,res) => {
                if (err!=null)
                {
                    response.status(500).json({
                        state : -2,
                        message : config.showErrorMessage==true ? err: ''
                    });
                }
                else response.status(200).json({
                    state : 0,
                    message : config.showErrorMessage==true ? 'comando [config_full] ricevuto e processato': ''
                });
            })
    
        });
        
        
    }

}

module.exports = GetConfigFull;
'use strict';
const iEndpoint = require('../iEndpoint.js');
const _ = require('underscore');
//var zlib = require('zlib');
const logger = require('../../utils/logger.js'); 
const CentralDAO = require('../../database/CentralDAO'); 

class GetTest extends iEndpoint {

    constructor(express_instance) {
        super(express_instance);
    }


    attach_endpoint(express_instance) {

        express_instance.get('/test/', function(request, response) {
           
			response.status(200).json('OK');
            
    
            });
        
        
    }

}

module.exports = GetTest;
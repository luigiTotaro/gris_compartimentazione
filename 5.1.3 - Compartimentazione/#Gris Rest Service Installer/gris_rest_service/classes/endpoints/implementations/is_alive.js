'use strict';
const iEndpoint = require('../iEndpoint.js');
const _ = require('underscore');
const logger = require('../../utils/logger.js'); 
const CentralDAO = require('../../database/CentralDAO'); 
const config = require('../../../config.js');

class GetIsAlive extends iEndpoint {

    constructor(express_instance) {
        super(express_instance);
    }


    attach_endpoint(express_instance) {

        express_instance.post('/is_alive/', function(request, response) {
           
            if(!request.body.data) {
                response.status(500).json({
                    state : -3,
                    message : config.showErrorMessage==true ? 'parametro "data" obbligatorio': ''
                });
                return;
            }

            let data = request.body.data;
            data = decodeURIComponent(data);
            //let originalData = new Buffer(data, 'binary');
            //let uncompressed = zlib.gunzipSync(originalData);
            //uncompressed = uncompressed.toString();   
            let message;
            try {
                message = JSON.parse(data); 
            }
            catch(err) {
                response.status(500).json({
                    state : -1,
                    message : config.showErrorMessage==true ? 'errore durante il parsing del messaggio': ''
                });
                return;
            }

            logger.log("info", "[is_alive] ricevuto");
            CentralDAO.is_alive(message, (err,res) => {
                if (err!=null)
                {
                    response.status(500).json({
                        state : -2,
                        message : config.showErrorMessage==true ? err: ''
                    });
                }
                else response.status(200).json({
                    state : 0,
                    message : config.showErrorMessage==true ? 'comando [is_alive] ricevuto e processato': ''
                });
            })
    
            });
        
        
    }

}

module.exports = GetIsAlive;
/**
 * Interfaccia per la gestione delle classi che implementano gli endpoint esposti. Tutte le 
 * classi che implementano questa interfaccia sono responsabili della della gestione di un endpoint
 * @interface iEndpoint
 */

'use strict';

class iEndpoint {

    constructor(express_instance) {
        this.attach_endpoint(express_instance);
    }

    attach_endpoint(express_instance) {
        throw 'abstract method';
    }


}


module.exports = iEndpoint;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.UpdateServices.Administration;

namespace WSUSCertSrv
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*===================================================*");
            Console.WriteLine("* WSUS Certificate generation tool v1.0             *");
            Console.WriteLine("*===================================================*");
            Console.WriteLine("Press any key to start generation...");
            Console.ReadLine();
            try
            {
                IUpdateServer udpServer = AdminProxy.GetUpdateServer();
                IUpdateServerConfiguration conf = udpServer.GetConfiguration();

                conf.SetSigningCertificate();
                conf.Save();
                Console.WriteLine("Certificate successfully generated!");
                Console.WriteLine("Open MMC, select certsrv add-in and look in WSUS Container to see the certificate.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore durante la generazione del certificato! " + ex.Message);
            }
            Console.WriteLine("Press any key to close...");
            Console.ReadLine();
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using GrisSuite.UpdateAgent;

namespace WCUService
{
    partial class WCUService : ServiceBase
    {

        public WCUService()
        {
            InitializeComponent();
        }

        protected override void OnCustomCommand(int command)
        {
            switch (command)
            {
                case 150:
                    //Force Update
                    ClientUpdateAgent _updateAgent = new ClientUpdateAgent();
                    _updateAgent.Update();
                    _updateAgent = null;
                    break;
                default:
                    break;
            }
        }        
    }
}

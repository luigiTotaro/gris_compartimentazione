using System.ServiceProcess;
using System;

namespace WCUService
{
    partial class WCUService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        // The main entry point for the process
        [MTAThread(), System.Diagnostics.DebuggerNonUserCode()]
        public static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun = null;

            // More than one NT Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
            //
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new WCUService() };

            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // WCUService
            // 
            this.CanPauseAndContinue = true;
            this.CanStop = false;
            this.ServiceName = "WCUService";

        }

        #endregion
    }
}

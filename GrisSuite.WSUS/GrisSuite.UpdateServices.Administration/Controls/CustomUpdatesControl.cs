using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class CustomUpdatesControl : IFormViewControl
    {

        private FormView _view;

        public delegate void UpdateContentsEventHandler();
        public static event UpdateContentsEventHandler UpdateContents;        

        public static void RefreshContents()
        {
            if (UpdateContents != null) UpdateContents();
        }

        public CustomUpdatesControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            dgvUpdates.AutoGenerateColumns = false;
            cmbApproval.Items.Add("Unapproved");
            cmbApproval.Items.Add("Approved");
            cmbApproval.Items.Add("Declined");
            cmbApproval.Items.Add("Any Except Declined");
            cmbApproval.SelectedIndex = 1;

            cmbStatus.Items.Add("Failed or Needed");
            cmbStatus.Items.Add("Installed/Not Applicable or No Status");
            cmbStatus.Items.Add("Failed");
            cmbStatus.Items.Add("Needed");
            cmbStatus.Items.Add("Installed/Not Applicable");
            cmbStatus.Items.Add("No Status");
            cmbStatus.Items.Add("Any");
            cmbStatus.SelectedIndex=4;
        }

        public void Initialize(Microsoft.ManagementConsole.FormView view)
        {
            _view = view;
            _view.SelectionData.ActionsPaneItems.Clear();
        }

        public void RefreshData(UpdateCollection updateCollection)
        {
            dgvUpdates.DataSource = updateCollection;
            dgvUpdates.Refresh();
        }

        private void dgvUpdates_CellContentClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            updateDetailControl1.Init((IUpdate)(dgvUpdates.SelectedRows[0].DataBoundItem));
        }

        private void dgvUpdates_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvUpdates.SelectedRows.Count>0)
                updateDetailControl1.Init((IUpdate)(dgvUpdates.SelectedRows[0].DataBoundItem));
        }

        private void cmdRefresh_Click(object sender, EventArgs e)
        {
            UpdateCollection updateCollection = Application.Current.Server.GetUpdates(GetUpdateScope());
            RefreshData(updateCollection);
        }

        public UpdateScope GetUpdateScope()
        {                     
            UpdateScope _searchScope = new UpdateScope();
            switch (cmbApproval.SelectedIndex)	        
            {
                case 0:
                    _searchScope.ApprovedStates=ApprovedStates.NotApproved;
                    break;
                case 1:
                    _searchScope.ApprovedStates=ApprovedStates.LatestRevisionApproved;
                    break;
                case 2:
                    _searchScope.ApprovedStates=ApprovedStates.Declined;
                    break;
                case 3:
                    _searchScope.ApprovedStates=ApprovedStates.LatestRevisionApproved | ApprovedStates.NotApproved | ApprovedStates.HasStaleUpdateApprovals;
                    break;
		        default:
                    break;
	        }             
            switch (cmbStatus.SelectedIndex)
	        {
                case 0: //Failed or Needed
                    _searchScope.IncludedInstallationStates=UpdateInstallationStates.Failed | UpdateInstallationStates.NotInstalled;
                    break;
                case 1: //Installed/Not Applicable or No Status
                    _searchScope.IncludedInstallationStates=UpdateInstallationStates.Installed | UpdateInstallationStates.NotApplicable | UpdateInstallationStates.Unknown;
                    break;
                case 2: //Failed
                    _searchScope.IncludedInstallationStates=UpdateInstallationStates.Failed;
                    break;
                case 3: //Needed
                    _searchScope.IncludedInstallationStates= UpdateInstallationStates.NotInstalled;
                    break;
                case 4: //Installed/Not Applicable
                    _searchScope.IncludedInstallationStates=UpdateInstallationStates.Installed | UpdateInstallationStates.NotApplicable;
                    break;
                case 5: //No Status
                    _searchScope.IncludedInstallationStates = UpdateInstallationStates.Unknown;
                    break;
                case 6: //Any
                    _searchScope.IncludedInstallationStates = UpdateInstallationStates.All;
                    break;
		        default:
                    break;
	        }
            return _searchScope;            
        }
    }
}
using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class ServerControl : IFormViewControl
    {

        private FormView _view;

        public ServerControl()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void Initialize(Microsoft.ManagementConsole.FormView view)
        {
            _view = view;
            _view.SelectionData.ActionsPaneItems.Clear();
        }


    }
}
using Microsoft.UpdateServices.Administration;
using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class UpdateDetailControl
    {

        private IUpdate _update;

        public UpdateDetailControl()
        {
            InitializeComponent();
        }

        public void Init(IUpdate update)
        {
            _update = update;
            lblTitle.Text = update.Title;
            lblSubTitle.Text = update.Description;
            lblCreationDate.Text = update.CreationDate.ToShortDateString();
            
            btnDelete.Enabled = !_update.IsApproved;
            btnDecline.Enabled = !_update.IsDeclined;
            btnApprove.Enabled = !_update.IsApproved;
        }

        private void btnApprove_Click(object sender, System.EventArgs e)
        {
            TreeSelectionDialog _dialog = new TreeSelectionDialog();
            if (_dialog.ShowComputerGroupsDialog(Application.Current.Server.GetComputerTargetGroups()) == DialogResult.OK)
            {
                foreach (IComputerTargetGroup _group in _dialog.ApprovedGroups)
                {
                    _update.Approve(UpdateApprovalAction.Install, _group);
                }
                btnDelete.Enabled = false;
                MessageBox.Show("Update approvato!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnDecline_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Si � certi di voler annullare l'update selezionato?", "Conferma", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                _update.Decline();
                btnDecline.Enabled = false;
                btnDelete.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Si � certi di voler cancellare l'update selezionato?", "Conferma", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Application.Current.Server.DeleteUpdate(_update.Id.UpdateId);
            }
        }
    }
}
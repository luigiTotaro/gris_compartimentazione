using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class ComputerGroupControl : System.Windows.Forms.UserControl
    {

        //UserControl overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.dgvComputers = new System.Windows.Forms.DataGridView();
            this.dgvComputerUpdates = new System.Windows.Forms.DataGridView();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIPAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOSDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastReportedStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComputers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComputerUpdates)).BeginInit();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvComputers
            // 
            this.dgvComputers.BackgroundColor = System.Drawing.Color.White;
            this.dgvComputers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvComputers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComputers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colName,
            this.colIPAddress,
            this.colOSDescription,
            this.colLastReportedStatus});
            this.dgvComputers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComputers.Location = new System.Drawing.Point(0, 0);
            this.dgvComputers.Name = "dgvComputers";
            this.dgvComputers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvComputers.Size = new System.Drawing.Size(527, 154);
            this.dgvComputers.TabIndex = 0;
            this.dgvComputers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvComputers_CellClick);
            this.dgvComputers.SelectionChanged += new System.EventHandler(this.dgvComputers_SelectionChanged);
            // 
            // dgvComputerUpdates
            // 
            this.dgvComputerUpdates.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvComputerUpdates.BackgroundColor = System.Drawing.Color.White;
            this.dgvComputerUpdates.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvComputerUpdates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComputerUpdates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvComputerUpdates.Location = new System.Drawing.Point(0, 0);
            this.dgvComputerUpdates.Name = "dgvComputerUpdates";
            this.dgvComputerUpdates.ReadOnly = true;
            this.dgvComputerUpdates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvComputerUpdates.Size = new System.Drawing.Size(527, 151);
            this.dgvComputerUpdates.TabIndex = 1;
            this.dgvComputerUpdates.SelectionChanged += new System.EventHandler(this.dgvComputerUpdates_SelectionChanged);
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Name = "SplitContainer1";
            this.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.dgvComputers);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.dgvComputerUpdates);
            this.SplitContainer1.Size = new System.Drawing.Size(527, 309);
            this.SplitContainer1.SplitterDistance = 154;
            this.SplitContainer1.TabIndex = 2;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "FullDomainName";
            this.colName.HeaderText = "Nome";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 150;
            // 
            // colIPAddress
            // 
            this.colIPAddress.DataPropertyName = "IPAddress";
            this.colIPAddress.HeaderText = "IP Address";
            this.colIPAddress.Name = "colIPAddress";
            this.colIPAddress.ReadOnly = true;
            // 
            // colOSDescription
            // 
            this.colOSDescription.DataPropertyName = "OSDescription";
            this.colOSDescription.HeaderText = "Sistema Operativo";
            this.colOSDescription.Name = "colOSDescription";
            this.colOSDescription.ReadOnly = true;
            this.colOSDescription.Width = 150;
            // 
            // colLastReportedStatus
            // 
            this.colLastReportedStatus.DataPropertyName = "LastReportedStatus";
            this.colLastReportedStatus.HeaderText = "Ultimo Contatto";
            this.colLastReportedStatus.Name = "colLastReportedStatus";
            this.colLastReportedStatus.ReadOnly = true;
            this.colLastReportedStatus.Width = 120;
            // 
            // ComputerGroupControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SplitContainer1);
            this.Name = "ComputerGroupControl";
            this.Size = new System.Drawing.Size(527, 309);
            ((System.ComponentModel.ISupportInitialize)(this.dgvComputers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComputerUpdates)).EndInit();
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            this.SplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal System.Windows.Forms.DataGridView dgvComputers;
        private System.Windows.Forms.DataGridView dgvComputerUpdates;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        private DataGridViewTextBoxColumn colName;
        private DataGridViewTextBoxColumn colIPAddress;
        private DataGridViewTextBoxColumn colOSDescription;
        private DataGridViewTextBoxColumn colLastReportedStatus;

    }
}
using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class ServerControl : System.Windows.Forms.UserControl
    {

        //UserControl overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            //
            //ReportViewer1
            //
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.ShowToolBar = false;
            this.ReportViewer1.Size = new System.Drawing.Size(586, 421);
            this.ReportViewer1.TabIndex = 0;
            //
            //ServerControl
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF((float)(6.0), (float)(13.0));
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ReportViewer1);
            this.Name = "ServerControl";
            this.Size = new System.Drawing.Size(586, 421);
            this.ResumeLayout(false);

        }
        internal Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;

    }
}
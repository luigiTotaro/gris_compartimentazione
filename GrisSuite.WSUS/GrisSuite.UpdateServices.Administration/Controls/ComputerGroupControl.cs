using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class ComputerGroupControl
    {

        public delegate void UpdateContentsEventHandler();
        public static event UpdateContentsEventHandler UpdateContents;
        public static void RefreshContents()
        {
            if (UpdateContents != null) UpdateContents();
        }

        public ComputerGroupControl()
        {
            InitializeComponent();
            dgvComputers.AutoGenerateColumns = false;
            this.Dock = DockStyle.Fill;
        }

        public void RefreshData(ComputerTargetCollection computerTargetCollection)
        {
            dgvComputers.DataSource = computerTargetCollection;
            dgvComputers.Refresh();
        }

        private void dgvComputers_SelectionChanged(object sender, System.EventArgs e)
        {
            SelectComputer();           
        }

        private void dgvComputers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectComputer();
        }

        private void SelectComputer()
        {
            if (dgvComputers.SelectedRows.Count > 0)
            {
                IComputerTarget _computer = (IComputerTarget)(dgvComputers.SelectedRows[0].DataBoundItem);
                dgvComputerUpdates.DataSource = CreateComputerUpdates(_computer);
                IUpdateSummary _updateSummary = _computer.GetUpdateInstallationSummary();
                System.Collections.ObjectModel.ReadOnlyCollection<IInventoryItem> _inventory = _computer.GetInventory();
            }
        }

        private void dgvComputerUpdates_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvComputerUpdates.SelectedRows.Count > 0)
            {
                //IUpdateInstallationInfo _installationInfo = (IUpdateInstallationInfo)(dgvComputerUpdates.SelectedRows[0].DataBoundItem);
            }
        }

        private List<ComputerUpdate> CreateComputerUpdates(IComputerTarget computer)
        { 
            List<ComputerUpdate> _computerUpdates = new List<ComputerUpdate>();
            UpdateInstallationInfoCollection _installs = computer.GetUpdateInstallationInfoPerUpdate();
            foreach (IUpdateInstallationInfo _install in _installs)
	        {
                IUpdate _update = _install.GetUpdate();
                ComputerUpdate _computerUpdate = new ComputerUpdate();                
                _computerUpdate.Title = _update.Title;
                _computerUpdate.InstallationState = _install.UpdateInstallationState.ToString();
                _computerUpdate.UpdateApprovalAction = _install.UpdateApprovalAction.ToString();
                //...
                _computerUpdates.Add(_computerUpdate);
	        }
            return _computerUpdates;
        }
        
        /// <summary>
        /// Forza l'update di un client
        /// </summary>
        /// <remarks>Esiste ancora il problema con l'IP del Client Remoto
        /// Username=update
        /// Password=update</remarks>
        public void ForceUpdateSelected()
        {
            if (dgvComputers.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Si vuole forzare l'update del client selezionato?", "Informazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    try
                    {
                        string _ipAddress = ((IComputerTarget)(dgvComputers.SelectedRows[0].DataBoundItem)).IPAddress.ToString();

                        NetworkCredentialDialog _dialog = new NetworkCredentialDialog(_ipAddress);
                        if (_dialog.ShowDialog() == DialogResult.OK)
                        {                            
                            WCUService.ClientService _clientService = new WCUService.ClientService();
                            _clientService.Url = "http://" + _ipAddress + "/WCUS/ClientService.asmx";
                            _clientService.Credentials = new System.Net.NetworkCredential(_dialog.UserName, _dialog.Password);
                            _clientService.ForceUpdate();
                        }
                        else
                        {
                            MessageBox.Show("Update annullato", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

        }

        private class ComputerUpdate
        {
            private string _title;
            public string Title
            {
                get { return _title; }
                set { _title = value; }
            }

            private string _installationState;

            public string InstallationState
            {
                get { return _installationState; }
                set { _installationState = value; }
            }

            private string _updateApprovalAction;

            public string UpdateApprovalAction
            {
                get { return _updateApprovalAction; }
                set { _updateApprovalAction = value; }
            }
	

        }

    }
}
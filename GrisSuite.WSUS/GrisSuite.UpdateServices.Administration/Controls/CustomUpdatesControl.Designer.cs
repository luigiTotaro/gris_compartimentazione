using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class CustomUpdatesControl : System.Windows.Forms.UserControl
    {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.dgvUpdates = new System.Windows.Forms.DataGridView();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCreationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUpdateType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPublicationState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsApproved = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cmbApproval = new System.Windows.Forms.ToolStripComboBox();
            this.ToolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cmbStatus = new System.Windows.Forms.ToolStripComboBox();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmdRefresh = new System.Windows.Forms.ToolStripButton();
            this.updateDetailControl1 = new GrisSuite.UpdateServices.Administration.UpdateDetailControl();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdates)).BeginInit();
            this.ToolStrip1.SuspendLayout();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvUpdates
            // 
            this.dgvUpdates.BackgroundColor = System.Drawing.Color.White;
            this.dgvUpdates.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvUpdates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdates.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTitle,
            this.colDescription,
            this.colCreationDate,
            this.colUpdateType,
            this.colPublicationState,
            this.colIsApproved,
            this.colState});
            this.dgvUpdates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUpdates.Location = new System.Drawing.Point(0, 0);
            this.dgvUpdates.Name = "dgvUpdates";
            this.dgvUpdates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUpdates.Size = new System.Drawing.Size(615, 238);
            this.dgvUpdates.TabIndex = 0;
            this.dgvUpdates.SelectionChanged += new System.EventHandler(this.dgvUpdates_SelectionChanged);
            this.dgvUpdates.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdates_CellContentClick);
            // 
            // colTitle
            // 
            this.colTitle.DataPropertyName = "Title";
            this.colTitle.HeaderText = "Titolo";
            this.colTitle.Name = "colTitle";
            this.colTitle.Width = 150;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            this.colDescription.HeaderText = "Descrizione";
            this.colDescription.Name = "colDescription";
            this.colDescription.Width = 150;
            // 
            // colCreationDate
            // 
            this.colCreationDate.DataPropertyName = "CreationDate";
            this.colCreationDate.HeaderText = "Data creazione";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.Width = 120;
            // 
            // colUpdateType
            // 
            this.colUpdateType.DataPropertyName = "UpdateType";
            this.colUpdateType.HeaderText = "Tipo aggiornamento";
            this.colUpdateType.Name = "colUpdateType";
            this.colUpdateType.Width = 130;
            // 
            // colPublicationState
            // 
            this.colPublicationState.DataPropertyName = "PublicationState";
            this.colPublicationState.HeaderText = "Stato di pubblicazione";
            this.colPublicationState.Name = "colPublicationState";
            this.colPublicationState.Width = 150;
            // 
            // colIsApproved
            // 
            this.colIsApproved.DataPropertyName = "IsApproved";
            this.colIsApproved.HeaderText = "Approvato";
            this.colIsApproved.Name = "colIsApproved";
            // 
            // colState
            // 
            this.colState.DataPropertyName = "State";
            this.colState.HeaderText = "Stato";
            this.colState.Name = "colState";
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.AutoSize = false;
            this.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripLabel1,
            this.cmbApproval,
            this.ToolStripLabel2,
            this.cmbStatus,
            this.cmdRefresh});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ToolStrip1.Size = new System.Drawing.Size(615, 28);
            this.ToolStrip1.TabIndex = 2;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // ToolStripLabel1
            // 
            this.ToolStripLabel1.Name = "ToolStripLabel1";
            this.ToolStripLabel1.Size = new System.Drawing.Size(58, 25);
            this.ToolStripLabel1.Text = "Approval:";
            // 
            // cmbApproval
            // 
            this.cmbApproval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbApproval.Name = "cmbApproval";
            this.cmbApproval.Size = new System.Drawing.Size(130, 28);
            // 
            // ToolStripLabel2
            // 
            this.ToolStripLabel2.Name = "ToolStripLabel2";
            this.ToolStripLabel2.Size = new System.Drawing.Size(42, 25);
            this.ToolStripLabel2.Text = "Status:";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(180, 28);
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 28);
            this.SplitContainer1.Name = "SplitContainer1";
            this.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.dgvUpdates);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.updateDetailControl1);
            this.SplitContainer1.Size = new System.Drawing.Size(615, 416);
            this.SplitContainer1.SplitterDistance = 238;
            this.SplitContainer1.TabIndex = 3;
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Image = global::GrisSuite.UpdateServices.Administration.Properties.Resources.RefreshDocViewHS;
            this.cmdRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(66, 25);
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // updateDetailControl1
            // 
            this.updateDetailControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateDetailControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateDetailControl1.Location = new System.Drawing.Point(0, 0);
            this.updateDetailControl1.Name = "updateDetailControl1";
            this.updateDetailControl1.Size = new System.Drawing.Size(615, 174);
            this.updateDetailControl1.TabIndex = 0;
            // 
            // CustomUpdatesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SplitContainer1);
            this.Controls.Add(this.ToolStrip1);
            this.Name = "CustomUpdatesControl";
            this.Size = new System.Drawing.Size(615, 444);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdates)).EndInit();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            this.SplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        internal System.Windows.Forms.DataGridView dgvUpdates;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel1;
        internal System.Windows.Forms.ToolStripComboBox cmbApproval;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel2;
        internal System.Windows.Forms.ToolStripComboBox cmbStatus;
        internal System.Windows.Forms.ToolStripButton cmdRefresh;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        private DataGridViewTextBoxColumn colTitle;
        private DataGridViewTextBoxColumn colDescription;
        private DataGridViewTextBoxColumn colCreationDate;
        private DataGridViewTextBoxColumn colUpdateType;
        private DataGridViewTextBoxColumn colPublicationState;
        private DataGridViewTextBoxColumn colIsApproved;
        private DataGridViewTextBoxColumn colState;
        private UpdateDetailControl updateDetailControl1;

    }
}
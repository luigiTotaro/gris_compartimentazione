using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class ServerScopeNode : ScopeNode
    {

        private IUpdateServer _server;

        public ServerScopeNode(IUpdateServer server)
        {
            _server = server;

            this.DisplayName = _server.Name;
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(ServerControl));
            viewComputers.DisplayName = _server.Name;
            viewComputers.ViewType = typeof(ServerFormView);
            this.ImageIndex = 0;
            this.SelectedImageIndex = 0;

            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;

        }

        public string ServerConnection
        {
            get
            {
                if (_server.IsConnectionSecureForApiRemoting)
                {
                    return "https://" + _server.Name + ":" + _server.PortNumber;
                }
                else
                {
                    return "http://" + _server.Name + ":" + _server.PortNumber;
                }
            }
        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
        }

    }
}
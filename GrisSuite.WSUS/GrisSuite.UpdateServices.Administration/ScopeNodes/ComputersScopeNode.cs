using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class ComputersScopeNode : ScopeNode
    {

        public ComputersScopeNode()
        {

            this.DisplayName = "Computers";
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(ServerControl));
            viewComputers.DisplayName = "Computers";
            viewComputers.ViewType = typeof(ServerFormView);

            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;
            this.ImageIndex = 2;
            this.SelectedImageIndex = 2;

        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
            //ProcessWatcher.Refresh()
        }
    }
}
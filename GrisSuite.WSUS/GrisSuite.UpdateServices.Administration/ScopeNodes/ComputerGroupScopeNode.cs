using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;
using GrisSuite.UpdateServices.Administration.Dialogs;

namespace GrisSuite.UpdateServices.Administration
{
    public class ComputerGroupScopeNode : ScopeNode
    {

        private IComputerTargetGroup _group;

        //TODO: INSTANT C# TODO TASK: Insert the following converted event handler wireups at the end of the 'InitializeComponent' method for forms, 'Page_Init' for web pages, or into a constructor for other classes:

        public void ComputersScopeNode()
        {
            this.ActionsActivated += ComputerGroupScopeNode_ActionsActivated;
        }

        public IComputerTargetGroup Group
        {
            get
            {
                return _group;
            }
        }

        public ComputerGroupScopeNode(IComputerTargetGroup group)
        {
            _group = group;

            this.DisplayName = _group.Name;
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(ComputerGroupControl));
            viewComputers.DisplayName = _group.Name;
            viewComputers.ViewType = typeof(ComputerGroupFormView);

            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;
            this.ImageIndex = 2;
            this.SelectedImageIndex = 2;

            this.ActionsPaneItems.Add(new Action("Reports", "Reports", -1, "UpdateReports"));
        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
            //ProcessWatcher.Refresh()
        }

        private void ComputerGroupScopeNode_ActionsActivated(object sender, System.EventArgs e)
        {
            ComputerGroupControl.RefreshContents();
        }

        protected override void OnAction(Microsoft.ManagementConsole.Action action, Microsoft.ManagementConsole.AsyncStatus status)
        {
            switch (action.Tag.ToString())
            {            
                case "UpdateReports":
                    ReportDialog _reportDialog = new ReportDialog();
                    _reportDialog.ShowComputerReport();
                    break;
            }
        }
    }
}
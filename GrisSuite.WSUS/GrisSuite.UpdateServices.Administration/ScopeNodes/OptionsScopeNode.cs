using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class OptionsScopeNode : ScopeNode
    {

        private IUpdateServer _server;

        public OptionsScopeNode(IUpdateServer server)
        {
            _server = server;

            this.DisplayName = "Options";
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(ServerControl));
            viewComputers.DisplayName = "Options";
            viewComputers.ViewType = typeof(ServerFormView);

            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;

        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
            //ProcessWatcher.Refresh()
        }

    }
}
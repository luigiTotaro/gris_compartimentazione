using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class CustomWSUSScopeNode : ScopeNode
    {

        public CustomWSUSScopeNode()
        {
            this.DisplayName = "Custom Update Services";
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(CustomWSUSControl));
            viewComputers.DisplayName = "Custom Update Services";
            viewComputers.ViewType = typeof(CustomWSUSFormView);
            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;
            this.ImageIndex = 0;
            this.SelectedImageIndex = 0;

            this.ActionsPaneItems.Add(new Action("Connect to Server...", "Connect to Server...", -1, "ConnectToServer"));
            //_view.SelectionData.ActionsPaneItems.Add(New Action("New", "New upgrade", -1, "NewUpgrade"))
        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
        }

        protected override void OnAction(Microsoft.ManagementConsole.Action action, Microsoft.ManagementConsole.AsyncStatus status)
        {
            switch (action.Tag.ToString())
            {
                case "ConnectToServer":
                    using (ConnectToServerDialog _dialog = new ConnectToServerDialog())
                    {
                        _dialog.ShowDialog();
                        if (_dialog.DialogResult == System.Windows.Forms.DialogResult.OK)
                        {
                            Connect(_dialog.ConnectionString);
                        }
                    }
                    break;
            }
        }

        public void Connect(string connectionString)
        {
            bool _useSSL = false;
            try
            {
                if (connectionString.StartsWith("https://"))
                {
                    _useSSL = true;
                    connectionString = connectionString.Substring(8);
                }
                else
                {
                    connectionString = connectionString.Substring(7);
                }
                string[] _params = connectionString.Split(':');

                IUpdateServer _server = AdminProxy.GetUpdateServer(_params[0], _useSSL, int.Parse(_params[1]));
                Application.Current.Server = _server;
                ServerScopeNode _serverScopeNode = new ServerScopeNode(_server);                
                this.Children.Add(_serverScopeNode);
                _serverScopeNode.Children.Add(new CustomUpdatesScopeNode(_server));
                //Creo il nodo "Computers"
                ComputersScopeNode _computersScopeNode = new ComputersScopeNode();
                _serverScopeNode.Children.Add(_computersScopeNode);
                //Carico ricorsivamente i gruppi di computers
                ComputerTargetGroupCollection _groups = Application.Current.Server.GetComputerTargetGroups();
                LoadTreeGroups(_computersScopeNode, _groups);

                //_serverScopeNode.Children.Add(new ReportsScopeNode(_server));
                //_serverScopeNode.Children.Add(new OptionsScopeNode(_server));
                this.SnapIn.IsModified = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadTreeGroups(ScopeNode parent, ComputerTargetGroupCollection groups)
        {
            foreach (IComputerTargetGroup group in groups)
            {
                ComputerGroupScopeNode _node = new ComputerGroupScopeNode(group);
                if (parent is ComputersScopeNode)
                {
                    if (group.Name == "All Computers")
                    {
                        parent.Children.Add(_node);
                    }
                    else
                    {
                        _node = null;
                    }
                }
                else
                {
                    parent.Children.Add(_node);
                }
                if (_node != null)
                {
                    LoadTreeGroups(_node, group.GetChildTargetGroups());
                }
            }
        }

    }
}
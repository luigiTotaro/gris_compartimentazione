using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class ReportsScopeNode : ScopeNode
    {

        private IUpdateServer _server;

        public ReportsScopeNode(IUpdateServer server)
        {
            _server = server;

            this.DisplayName = "Reports";
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(ServerControl));
            viewComputers.DisplayName = "Reports";
            viewComputers.ViewType = typeof(ServerFormView);

            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;

        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
            //ProcessWatcher.Refresh()
        }

    }
}
using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using Microsoft.UpdateServices.Administration;
using GrisSuite.UpdateServices.Administration.Dialogs;

namespace GrisSuite.UpdateServices.Administration
{
    public class CustomUpdatesScopeNode : ScopeNode
    {

        private IUpdateServer _server;

        public CustomUpdatesScopeNode(IUpdateServer server)
            : base()
        {
            _server = server;

            this.DisplayName = "Custom Updates";
            this.EnabledStandardVerbs = StandardVerbs.Refresh;

            FormViewDescription viewComputers = new FormViewDescription(typeof(CustomUpdatesControl));
            viewComputers.DisplayName = "Custom Updates";
            viewComputers.ViewType = typeof(CustomUpdatesFormView);
            viewComputers.ControlType = typeof(CustomUpdatesControl);
            this.ViewDescriptions.Add(viewComputers);
            this.ViewDescriptions.DefaultIndex = 0;
            this.ImageIndex = 1;
            this.SelectedImageIndex = 1;

            this.ActionsPaneItems.Add(new Action("New Update", "New Update", -1, "NewUpdate"));
            this.ActionsPaneItems.Add(new Action("Reports", "Reports", -1, "UpdateReports"));
        }

        protected override void OnRefresh(AsyncStatus status)
        {
            base.OnRefresh(status);
            CustomUpdatesControl.RefreshContents();
        }

        protected override void OnAction(Microsoft.ManagementConsole.Action action, Microsoft.ManagementConsole.AsyncStatus status)
        {
            switch (action.Tag.ToString())
            {
                case "NewUpdate":
                    NewUpdateWizard _dialog = new NewUpdateWizard();
                    if (_dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        CustomUpdatesControl.RefreshContents();
                    }
                    break;
                case "UpdateReports":
                    ReportDialog _reportDialog = new ReportDialog();
                    _reportDialog.ShowUpdateReport();
                    break;
            }
        }

    }
}
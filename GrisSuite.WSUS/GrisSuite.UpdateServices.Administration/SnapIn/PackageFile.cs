namespace GrisSuite.UpdateServices.Administration
{
    public enum PackageFile : int
    {
        Exe = 0,
        ExeWrappedMsi = 1,
        Msi = 2,
        Msp = 3
    }
}
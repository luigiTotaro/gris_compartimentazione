using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Text;

namespace GrisSuite.UpdateServices.Administration
{
    [SnapInSettings("{4D0CD90B-BF74-42a8-8F3B-DA346B718C26}", DisplayName = "WSUS Custom Packages Administration", Description = "MMC 3.0 Administration Snap-In for publishing and managing third party packages with WSUS 3.0")]
    public class WSUSCustomSnapIn : SnapIn
    {

        public WSUSCustomSnapIn()
        {
            //Aggiungo le immagini da utilizzare nella treeview
            this.SmallImages.Add(Properties.Resources.CustomWSUS);
            this.SmallImages.Add(Properties.Resources.CustomUpdates);
            this.SmallImages.Add(Properties.Resources.Computers);
            this.RootNode = new CustomWSUSScopeNode();
        }

        protected override void OnLoadCustomData(Microsoft.ManagementConsole.AsyncStatus status, byte[] persistenceData)
        {
            if (!(string.IsNullOrEmpty(Encoding.Unicode.GetString(persistenceData))))
            {
                ((CustomWSUSScopeNode)this.RootNode).Connect(Encoding.Unicode.GetString(persistenceData));
            }
        }

        protected override byte[] OnSaveCustomData(Microsoft.ManagementConsole.SyncStatus status)
        {
            return Encoding.Unicode.GetBytes(((ServerScopeNode)(this.RootNode.Children[0])).ServerConnection);
        }

    }
}
using System;
using System.Diagnostics;
using Microsoft.UpdateServices.Administration;
using System.IO;

namespace GrisSuite.UpdateServices.Administration
{
    public class Application
    {

        private static GrisSuite.UpdateServices.Administration.Application _current = new GrisSuite.UpdateServices.Administration.Application();
        public const string Name = "Gris Update Services";

        private Application()
        {

        }

        public static GrisSuite.UpdateServices.Administration.Application Current
        {
            get
            {
                return _current;
            }
        }

        private IUpdateServer _server;
        public IUpdateServer Server
        {
            get
            {
                return _server;
            }
            set
            {
                _server = value;
            }
        }

        /// <summary>
        /// Creazione nuovo update e sua pubblicazione
        /// </summary>
        /// <param name="file"></param>
        /// <param name="packagePath"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="rule"></param>
        /// <param name="packageType"></param>
        /// <param name="packageUpdateClassification"></param>
        /// <param name="vendorName"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool CreateNewUpdate(PackageFile file, string packagePath, string title, string description, string rule, PackageType packageType, PackageUpdateClassification packageUpdateClassification, string vendorName)
        {
            try
            {
                SoftwareDistributionPackage sdp = new SoftwareDistributionPackage();
                sdp.Title = title;
                sdp.Description = description;
                sdp.IsInstallable = rule;
                sdp.PackageType = packageType;
                sdp.PackageUpdateClassification = packageUpdateClassification;
                sdp.SecurityRating = SecurityRating.Moderate;
                sdp.VendorName = vendorName;

                switch (file)
                {
                    case PackageFile.Msi:
                        sdp.PopulatePackageFromWindowsInstaller(packagePath);
                        break;
                    case PackageFile.Msp:
                        sdp.PopulatePackageFromWindowsInstallerPatch(packagePath);
                        break;
                    case PackageFile.Exe:
                        sdp.PopulatePackageFromExe(packagePath);
                        break;
                    case PackageFile.ExeWrappedMsi:
                        //sdp.PopulatePackageFromExeWrappedMsi()
                        break;
                }

                //if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SoftwareDistributionPackages")))
                //    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SoftwareDistributionPackages"));

                //string sdpFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SoftwareDistributionPackages\\" + title + sdp.PackageId.ToString());
                string sdpFilePath = Environment.GetEnvironmentVariable("TEMP") + "\\" + title + sdp.PackageId.ToString();

                sdp.Save(sdpFilePath);

                IPublisher publisher = _server.GetPublisher(sdpFilePath);
                FileInfo fi = new FileInfo(packagePath);
                
                publisher.PublishPackage(fi.Directory.ToString(), null);
                return true;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Application.Name, ex.Message, EventLogEntryType.Error);
                throw ex;
                return false;
            }
        }
    }
}
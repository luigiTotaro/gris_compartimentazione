using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration.Dialogs
{
    public partial class ReportDialog : Form
    {
        ComputerTargetGroupCollection _groupFilter;
        UpdateInstallationStates _statesFilter;

        public ReportDialog()
        {
            InitializeComponent();
            _statesFilter = UpdateInstallationStates.All;            
        }

        public DialogResult ShowComputerReport()
        {
            this.Text = "Computer Report";
            cmbReportType.SelectedIndex = 0;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "GrisSuite.UpdateServices.Administration.Reports.ComputerStatus.rdlc";
            lnkUpdateStatus.Text = ParseStates();
            lnkComputers.Text = ParseComputers();
            return this.ShowDialog();
        }

        private string ParseComputers()
        {
            string _result = string.Empty;
            if (_groupFilter != null)
            {
                foreach (IComputerTargetGroup _group in _groupFilter)
                {
                    _result = _result + _group.Name + ", ";
                }
                if (_result.Length > 2)
                    return _result.Substring(0, _result.Length - 2);
                else
                    return "Not set";
            }
            else
                return "Not set";
        }

        private string ParseStates()
        {
            return _statesFilter.ToString();
        }

        public DialogResult ShowUpdateReport()
        {
            this.Text = "Update Report";
            cmbReportType.SelectedIndex = 0;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "GrisSuite.UpdateServices.Administration.Reports.UpdateStatus.rdlc";
            return this.ShowDialog();
        }

        private void cmdReportFilter_Click(object sender, EventArgs e)
        {
            pnlFilter.Visible = !pnlFilter.Visible;
        }

        private void cmdRunReport_Click(object sender, EventArgs e)
        {
            SetFilter();
            this.reportViewer1.RefreshReport();
        }

        private void SetFilter()
        {
            LoadUpdateStatus();
            reportViewer1.LocalReport.DataSources["UpdateInfo_Update"].Value = UpdateInfo.Update.DefaultView;
        }

        private void lnkComputers_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TreeSelectionDialog _dialog = new TreeSelectionDialog();
            if (_dialog.ShowComputerGroupsDialog(Application.Current.Server.GetComputerTargetGroups()) == DialogResult.OK)
            {
                _groupFilter = _dialog.ApprovedGroups;
                lnkComputers.Text = ParseComputers();
            }
        }

        private void lnkUpdateStatus_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TreeSelectionDialog _dialog = new TreeSelectionDialog();
            if (_dialog.ShowUpdateTypesDialog(_statesFilter) == DialogResult.OK)
            {
                _statesFilter = _dialog.InstallationStates;
                lnkUpdateStatus.Text = ParseStates();
            }
        }

        private void LoadUpdateStatus()
        { 
            ComputerTargetCollection _computers;
            UpdateInstallationInfoCollection _installStatus;
            ComputerTargetScope _searchScope = new ComputerTargetScope();
            _searchScope.IncludeDownstreamComputerTargets = true;
            //_searchScope.ComputerTargetGroups = _groupFilter;
            _searchScope.IncludedInstallationStates = _statesFilter;
            _computers = Application.Current.Server.GetComputerTargets(_searchScope);            

            foreach (IComputerTarget _computer in _computers)
            {                
                _installStatus=_computer.GetUpdateInstallationInfoPerUpdate();
                foreach (IUpdateInstallationInfo _update in _installStatus)
                {
                    Data.UpdateInfo.UpdateRow _row = UpdateInfo.Update.NewUpdateRow();
                    _row.FullDomainName=_computer.FullDomainName;                   
                    _row.LastReportedStatusTime=_computer.LastReportedStatusTime;
                    _row.IPAddress=_computer.IPAddress.ToString();
                    _row.Model = _computer.Model;
                    _row.OperatingSystem=_computer.OSDescription;
                    _row.ServicePack = String.Format("{0}.{1}", _computer.OSInfo.Version.ServicePackMajor, _computer.OSInfo.Version.ServicePackMinor);
                    _row.Language = _computer.OSInfo.DefaultUILanguage;
                    IUpdate _updateInfo = _update.GetUpdate();

                    _row.UpdateTitle = _updateInfo.Title;
                    _row.UpdateCreationDate = _updateInfo.CreationDate;
                    _row.UpdateDescription = _updateInfo.Description;
                    _row.UpdateId = _updateInfo.Id.UpdateId;
                    _row.UpdateInstallationState=_update.UpdateInstallationState.ToString();
                    UpdateInfo.Update.Rows.Add(_row);
                }
            }
        }
    }
}
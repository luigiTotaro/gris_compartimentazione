using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class NetworkCredentialDialog
    {
        public NetworkCredentialDialog(string ipAddress)
        {
            this.InitializeComponent();
            this.Text = this.Text + " - " + ipAddress;
        }

        private void OK_Button_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void Cancel_Button_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        public string UserName
        {
            get
            {
                return txtUserName.Text;
            }
        }

        public string Password
        {
            get
            {
                return txtPassword.Text;
            }
        }

    }
}
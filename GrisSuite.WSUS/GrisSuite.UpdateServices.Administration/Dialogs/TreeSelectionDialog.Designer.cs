using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class TreeSelectionDialog : System.Windows.Forms.Form
    {

        //Form overrides dispose to clean up the component list.
        internal TreeSelectionDialog()
        {
            InitializeComponent();
        }
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.trvGroups = new System.Windows.Forms.TreeView();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.Panel1.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // trvGroups
            // 
            this.trvGroups.CheckBoxes = true;
            this.trvGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trvGroups.Location = new System.Drawing.Point(3, 3);
            this.trvGroups.Name = "trvGroups";
            this.trvGroups.Size = new System.Drawing.Size(278, 364);
            this.trvGroups.TabIndex = 10;
            this.trvGroups.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.trvGroups_AfterCheck);
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.trvGroups);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Padding = new System.Windows.Forms.Padding(3);
            this.Panel1.Size = new System.Drawing.Size(284, 370);
            this.Panel1.TabIndex = 11;
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.btnOk);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel2.Location = new System.Drawing.Point(0, 370);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(284, 42);
            this.Panel2.TabIndex = 12;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(197, 7);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "O&k";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // TreeSelectionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 412);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Panel2);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TreeSelectionDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ApproveUpdateDialog";
            this.Panel1.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.TreeView trvGroups;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Button btnOk;
    }
}
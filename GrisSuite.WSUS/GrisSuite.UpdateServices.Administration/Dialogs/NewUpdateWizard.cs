using System;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class NewUpdateWizard
    {

        private string _rule = string.Empty;
        private PackageFile _packageFile;

        private void btnBrowse_Click(object sender, System.EventArgs e)
        {
            dlgOpenFile.Filter = "Microsoft Installer|*.msi|Microsoft Patch|*.msp|File Eseguibile|*.exe";
            if (dlgOpenFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtPackagePath.Text = dlgOpenFile.FileName;
            }
        }

        private void btnRuleBuilder_Click(object sender, System.EventArgs e)
        {
            //TODO! open rule builder
            _rule = "<bar:WindowsVersion Comparison=\"GreaterThanOrEqualTo\" MajorVersion=\"5\" MinorVersion=\"1\" />";
        }

        private void WizardControl1_FinishButtonClick(object sender, System.EventArgs e)
        {
            _rule = "<bar:WindowsVersion Comparison=\"GreaterThanOrEqualTo\" MajorVersion=\"5\" MinorVersion=\"1\" />";
            if (rbtMsi.Checked)
                _packageFile = PackageFile.Msi;
            else if (rbtExe.Checked)
                _packageFile = PackageFile.Exe;
            try
            {
                if (Application.Current.CreateNewUpdate(_packageFile, txtPackagePath.Text, txtTitle.Text, txtDescription.Text, _rule, PackageType.Application, PackageUpdateClassification.Tools, txtVendorName.Text))
                {
                    MessageBox.Show("Update publishing successful!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Update publishing error! {0}",ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void WizardControl1_CancelButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void WizardControl1_NextButtonClick(WizardBase.WizardControl sender, WizardBase.WizardNextButtonClickEventArgs args)
        {
            switch (WizardControl1.CurrentStepIndex)
            {
                case 2:
                    if (string.IsNullOrEmpty(txtPackagePath.Text))
                    {
                        errProvider.SetError(txtPackagePath, "Specificare un percorso.");
                        args.Cancel = true;
                    }
                    else 
                    {
                        errProvider.SetError(txtPackagePath, String.Empty);
                    }
                    break;
                case 3:
                    if (string.IsNullOrEmpty(txtTitle.Text))
                    {
                        errProvider.SetError(txtTitle, "Specificare un titolo.");
                        args.Cancel = true;
                    }
                    else
                    {
                        errProvider.SetError(txtPackagePath, String.Empty);
                    }
                    if (string.IsNullOrEmpty(txtDescription.Text))
                    {
                        errProvider.SetError(txtDescription, "Specificare una descrizione.");
                        args.Cancel = true;
                    }
                    else
                    {
                        errProvider.SetError(txtPackagePath, String.Empty);
                    }
                    if (string.IsNullOrEmpty(txtVendorName.Text))
                    {
                        errProvider.SetError(txtVendorName, "Specificare un produttore.");
                        args.Cancel = true;
                    }
                    else
                    {
                        errProvider.SetError(txtPackagePath, String.Empty);
                    }
                    break;
            }
        }
    }
}
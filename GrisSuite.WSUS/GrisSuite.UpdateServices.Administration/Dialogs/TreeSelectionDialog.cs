using System;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;
using System.Collections.Generic;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class TreeSelectionDialog
    {
        private enum TreeMode
        { 
            Computers=0,
            Updates=1
        }
        private ComputerTargetGroupCollection _approvedGroups = new ComputerTargetGroupCollection();
        TreeMode _mode;

        public DialogResult ShowComputerGroupsDialog(ComputerTargetGroupCollection groups)
        {
            _mode = TreeMode.Computers;
            LoadTreeGroups(null, groups);
            return this.ShowDialog();
        }

        public DialogResult ShowUpdateTypesDialog(UpdateInstallationStates states)
        {
            _mode = TreeMode.Updates;
            _installationStates = states;
            LoadUpdateTypes();
            return this.ShowDialog();
        }

        private void LoadUpdateTypes()
        {
            TreeNode _rootNode = new TreeNode("All");
            _rootNode.Tag = UpdateInstallationStates.All;
            trvGroups.Nodes.Add(_rootNode);
            _rootNode.Checked =((_installationStates & UpdateInstallationStates.All) == UpdateInstallationStates.All);            
            
            TreeNode _node;
            _node = _rootNode.Nodes.Add("Installed/Not Applicable");
            _node.Tag = UpdateInstallationStates.Installed | UpdateInstallationStates.NotApplicable;
            _node.Checked = ((_installationStates & UpdateInstallationStates.Installed) == UpdateInstallationStates.Installed) ||
                            ((_installationStates & UpdateInstallationStates.NotApplicable) == UpdateInstallationStates.NotApplicable);
            _node = _rootNode.Nodes.Add("Needed");
            _node.Tag = UpdateInstallationStates.NotInstalled;
            _node.Checked = ((_installationStates & UpdateInstallationStates.NotInstalled) == UpdateInstallationStates.NotInstalled);            
            _node = _rootNode.Nodes.Add("Failed");
            _node.Tag = UpdateInstallationStates.Failed;
            _node.Checked = ((_installationStates & UpdateInstallationStates.Failed) == UpdateInstallationStates.Failed);            
            _node = _rootNode.Nodes.Add("No Status");
            _node.Tag = UpdateInstallationStates.Unknown;
            _node.Checked = ((_installationStates & UpdateInstallationStates.Unknown) == UpdateInstallationStates.Unknown);            
        }

        private void LoadTreeGroups(TreeNode parent, ComputerTargetGroupCollection groups)
        {            
            foreach (IComputerTargetGroup _group in groups)
            {
                TreeNode _node = new TreeNode(_group.Name);
                _node.Tag = _group.Id;
                if (parent == null)
                {
                    if (_group.Name == "All Computers")
                    {
                        trvGroups.Nodes.Add(_node);
                    }
                    else
                    {
                        _node = null;
                    }
                }
                else
                {
                    parent.Nodes.Add(_node);
                }
                if (_node != null)
                {
                    LoadTreeGroups(_node, _group.GetChildTargetGroups());
                }
            }
        }

        private void TreeSelectionDialog_Shown(object sender, System.EventArgs e)
        {
            trvGroups.ExpandAll();
        }

        public ComputerTargetGroupCollection ApprovedGroups
        {
            get
            {
                return _approvedGroups;
            }
        }

        UpdateInstallationStates _installationStates;
        public UpdateInstallationStates InstallationStates
        {
            get
            {
                return _installationStates;
            }
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            if (_mode == TreeMode.Updates)
                ScanSelectedUpdates();
            else
                ScanSelectedGroups(trvGroups.Nodes);
            this.DialogResult = DialogResult.OK;
        }

        private void ScanSelectedUpdates()
        {
            _installationStates = 0;
            foreach (TreeNode _node in trvGroups.Nodes[0].Nodes)
            {                
                if (_node.Checked)
                {
                    _installationStates = _installationStates | (UpdateInstallationStates)_node.Tag;
                }        
            }
        }

        private void ScanSelectedGroups(TreeNodeCollection nodes)
        {
            foreach (TreeNode _node in nodes)
            {
                if (_node.Checked)
                {
                    _approvedGroups.Add(GrisSuite.UpdateServices.Administration.Application.Current.Server.GetComputerTargetGroup((Guid)_node.Tag));
                }
                ScanSelectedGroups(_node.Nodes);
            }
        }

        private void trvGroups_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {                    
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
            }

        }

        // Updates all child tree nodes recursively.
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }
    }
}
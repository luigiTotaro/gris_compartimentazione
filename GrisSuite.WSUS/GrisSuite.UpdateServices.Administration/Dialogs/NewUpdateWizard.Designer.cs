using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class NewUpdateWizard : System.Windows.Forms.Form
    {

        //Form overrides dispose to clean up the component list.
        internal NewUpdateWizard()
        {
            InitializeComponent();
        }
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewUpdateWizard));
            this.WizardControl1 = new WizardBase.WizardControl();
            this.StartStep1 = new WizardBase.StartStep();
            this.IntermediateStep4 = new WizardBase.IntermediateStep();
            this.rbtExe = new System.Windows.Forms.RadioButton();
            this.rbtMsp = new System.Windows.Forms.RadioButton();
            this.rbtMsi = new System.Windows.Forms.RadioButton();
            this.IntermediateStep1 = new WizardBase.IntermediateStep();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtPackagePath = new System.Windows.Forms.TextBox();
            this.IntermediateStep2 = new WizardBase.IntermediateStep();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.FinishStep1 = new WizardBase.FinishStep();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.errProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.IntermediateStep4.SuspendLayout();
            this.IntermediateStep1.SuspendLayout();
            this.IntermediateStep2.SuspendLayout();
            this.FinishStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // WizardControl1
            // 
            this.WizardControl1.BackButtonEnabled = true;
            this.WizardControl1.BackButtonVisible = true;
            this.WizardControl1.CancelButtonEnabled = true;
            this.WizardControl1.CancelButtonVisible = true;
            this.WizardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WizardControl1.HelpButtonEnabled = true;
            this.WizardControl1.HelpButtonVisible = true;
            this.WizardControl1.Location = new System.Drawing.Point(0, 0);
            this.WizardControl1.Name = "WizardControl1";
            this.WizardControl1.NextButtonEnabled = true;
            this.WizardControl1.NextButtonVisible = true;
            this.WizardControl1.Size = new System.Drawing.Size(506, 358);
            this.WizardControl1.WizardSteps.Add(this.StartStep1);
            this.WizardControl1.WizardSteps.Add(this.IntermediateStep4);
            this.WizardControl1.WizardSteps.Add(this.IntermediateStep1);
            this.WizardControl1.WizardSteps.Add(this.IntermediateStep2);
            this.WizardControl1.WizardSteps.Add(this.FinishStep1);
            this.WizardControl1.FinishButtonClick += new System.EventHandler(this.WizardControl1_FinishButtonClick);
            this.WizardControl1.NextButtonClick += new WizardBase.WizardNextButtonClickEventHandler(this.WizardControl1_NextButtonClick);
            this.WizardControl1.CancelButtonClick += new System.EventHandler(this.WizardControl1_CancelButtonClick);
            // 
            // StartStep1
            // 
            this.StartStep1.BindingImage = ((System.Drawing.Image)(resources.GetObject("StartStep1.BindingImage")));
            this.StartStep1.Icon = ((System.Drawing.Image)(resources.GetObject("StartStep1.Icon")));
            this.StartStep1.Name = "StartStep1";
            this.StartStep1.Subtitle = "Con questo wizard � possibile pubblicare un nuovo pacchetto di updates/installazi" +
                "one da distribuire tra i computer gestiti tramite WSUS.";
            this.StartStep1.SubtitleFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartStep1.Title = "Benvenuti nel wizard di nuova pubblicazione update";
            this.StartStep1.TitleFont = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            // 
            // IntermediateStep4
            // 
            this.IntermediateStep4.BindingImage = ((System.Drawing.Image)(resources.GetObject("IntermediateStep4.BindingImage")));
            this.IntermediateStep4.Controls.Add(this.rbtExe);
            this.IntermediateStep4.Controls.Add(this.rbtMsp);
            this.IntermediateStep4.Controls.Add(this.rbtMsi);
            this.IntermediateStep4.Name = "IntermediateStep4";
            this.IntermediateStep4.Subtitle = "Selezionare tra msi, msp e file eseguibile.";
            this.IntermediateStep4.SubtitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.IntermediateStep4.Title = "Selezionare il tipo di pacchetto da distribuire";
            this.IntermediateStep4.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            // 
            // rbtExe
            // 
            this.rbtExe.AutoSize = true;
            this.rbtExe.Location = new System.Drawing.Point(12, 141);
            this.rbtExe.Name = "rbtExe";
            this.rbtExe.Size = new System.Drawing.Size(123, 17);
            this.rbtExe.TabIndex = 2;
            this.rbtExe.Text = "Executable file (.exe)";
            this.rbtExe.UseVisualStyleBackColor = true;
            // 
            // rbtMsp
            // 
            this.rbtMsp.AutoSize = true;
            this.rbtMsp.Location = new System.Drawing.Point(12, 118);
            this.rbtMsp.Name = "rbtMsp";
            this.rbtMsp.Size = new System.Drawing.Size(169, 17);
            this.rbtMsp.TabIndex = 1;
            this.rbtMsp.Text = "Microsoft Installer Patch (.msp)";
            this.rbtMsp.UseVisualStyleBackColor = true;
            // 
            // rbtMsi
            // 
            this.rbtMsi.AutoSize = true;
            this.rbtMsi.Checked = true;
            this.rbtMsi.Location = new System.Drawing.Point(12, 95);
            this.rbtMsi.Name = "rbtMsi";
            this.rbtMsi.Size = new System.Drawing.Size(134, 17);
            this.rbtMsi.TabIndex = 0;
            this.rbtMsi.TabStop = true;
            this.rbtMsi.Text = "Microsoft Installer (.msi)";
            this.rbtMsi.UseVisualStyleBackColor = true;
            // 
            // IntermediateStep1
            // 
            this.IntermediateStep1.BindingImage = ((System.Drawing.Image)(resources.GetObject("IntermediateStep1.BindingImage")));
            this.IntermediateStep1.Controls.Add(this.Label1);
            this.IntermediateStep1.Controls.Add(this.btnBrowse);
            this.IntermediateStep1.Controls.Add(this.txtPackagePath);
            this.IntermediateStep1.Name = "IntermediateStep1";
            this.IntermediateStep1.Subtitle = "Il percorso del pacchetto contiene fisicamente il pacchetto da distribuire.";
            this.IntermediateStep1.SubtitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.IntermediateStep1.Title = "Selezionare il percorso del pacchetto";
            this.IntermediateStep1.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(13, 76);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(77, 13);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "Package path:";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(385, 93);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtPackagePath
            // 
            this.txtPackagePath.Location = new System.Drawing.Point(12, 95);
            this.txtPackagePath.Name = "txtPackagePath";
            this.txtPackagePath.Size = new System.Drawing.Size(341, 20);
            this.txtPackagePath.TabIndex = 0;
            // 
            // IntermediateStep2
            // 
            this.IntermediateStep2.BindingImage = ((System.Drawing.Image)(resources.GetObject("IntermediateStep2.BindingImage")));
            this.IntermediateStep2.Controls.Add(this.txtVendorName);
            this.IntermediateStep2.Controls.Add(this.Label4);
            this.IntermediateStep2.Controls.Add(this.Label3);
            this.IntermediateStep2.Controls.Add(this.Label2);
            this.IntermediateStep2.Controls.Add(this.txtDescription);
            this.IntermediateStep2.Controls.Add(this.txtTitle);
            this.IntermediateStep2.Name = "IntermediateStep2";
            this.IntermediateStep2.Subtitle = "Inserire i dati distintivi del pacchetto.";
            this.IntermediateStep2.SubtitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.IntermediateStep2.Title = "Identificazione del Pacchetto";
            this.IntermediateStep2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            // 
            // txtVendorName
            // 
            this.txtVendorName.Location = new System.Drawing.Point(12, 175);
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(457, 20);
            this.txtVendorName.TabIndex = 5;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(12, 159);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(59, 13);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "Produttore:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(12, 119);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(65, 13);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "Descrizione:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(12, 79);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(36, 13);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Titolo:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(12, 135);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(457, 20);
            this.txtDescription.TabIndex = 1;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(12, 95);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(457, 20);
            this.txtTitle.TabIndex = 0;
            // 
            // FinishStep1
            // 
            this.FinishStep1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FinishStep1.BackgroundImage")));
            this.FinishStep1.Controls.Add(this.label6);
            this.FinishStep1.Controls.Add(this.label5);
            this.FinishStep1.Name = "FinishStep1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Premere Finish per pubblicare il pacchetto.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(278, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "Creazione pacchetto completata.";
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.FileName = "OpenFileDialog1";
            // 
            // errProvider
            // 
            this.errProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errProvider.ContainerControl = this;
            // 
            // NewUpdateWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 358);
            this.Controls.Add(this.WizardControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewUpdateWizard";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New Update Wizard";
            this.IntermediateStep4.ResumeLayout(false);
            this.IntermediateStep4.PerformLayout();
            this.IntermediateStep1.ResumeLayout(false);
            this.IntermediateStep1.PerformLayout();
            this.IntermediateStep2.ResumeLayout(false);
            this.IntermediateStep2.PerformLayout();
            this.FinishStep1.ResumeLayout(false);
            this.FinishStep1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errProvider)).EndInit();
            this.ResumeLayout(false);

        }
        internal WizardBase.WizardControl WizardControl1;
        internal WizardBase.StartStep StartStep1;
        internal WizardBase.IntermediateStep IntermediateStep1;
        internal WizardBase.FinishStep FinishStep1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnBrowse;
        internal System.Windows.Forms.TextBox txtPackagePath;
        internal System.Windows.Forms.OpenFileDialog dlgOpenFile;
        internal WizardBase.IntermediateStep IntermediateStep2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.TextBox txtTitle;
        internal WizardBase.IntermediateStep IntermediateStep4;
        internal System.Windows.Forms.RadioButton rbtExe;
        internal System.Windows.Forms.RadioButton rbtMsp;
        internal System.Windows.Forms.RadioButton rbtMsi;
        internal System.Windows.Forms.TextBox txtVendorName;
        internal System.Windows.Forms.Label Label4;
        private Label label6;
        private Label label5;
        private ErrorProvider errProvider;

    }
}
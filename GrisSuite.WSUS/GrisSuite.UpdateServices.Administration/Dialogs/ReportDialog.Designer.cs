namespace GrisSuite.UpdateServices.Administration.Dialogs
{
    partial class ReportDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportDialog));
            this.UpdateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.UpdateInfo = new GrisSuite.UpdateServices.Administration.Data.UpdateInfo();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.lnkUpdateStatus = new System.Windows.Forms.LinkLabel();
            this.lnkComputers = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbReportType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmdReportFilter = new System.Windows.Forms.ToolStripButton();
            this.cmdRunReport = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateInfo)).BeginInit();
            this.pnlFilter.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // UpdateBindingSource
            // 
            this.UpdateBindingSource.DataMember = "Update";
            this.UpdateBindingSource.DataSource = this.UpdateInfo;
            // 
            // UpdateInfo
            // 
            this.UpdateInfo.DataSetName = "UpdateInfo";
            this.UpdateInfo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pnlFilter
            // 
            this.pnlFilter.Controls.Add(this.lnkUpdateStatus);
            this.pnlFilter.Controls.Add(this.lnkComputers);
            this.pnlFilter.Controls.Add(this.label3);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Controls.Add(this.cmbReportType);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 25);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(558, 100);
            this.pnlFilter.TabIndex = 0;
            // 
            // lnkUpdateStatus
            // 
            this.lnkUpdateStatus.AutoSize = true;
            this.lnkUpdateStatus.Location = new System.Drawing.Point(156, 61);
            this.lnkUpdateStatus.Name = "lnkUpdateStatus";
            this.lnkUpdateStatus.Size = new System.Drawing.Size(61, 13);
            this.lnkUpdateStatus.TabIndex = 5;
            this.lnkUpdateStatus.TabStop = true;
            this.lnkUpdateStatus.Text = "All Updates";
            this.lnkUpdateStatus.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkUpdateStatus_LinkClicked);
            // 
            // lnkComputers
            // 
            this.lnkComputers.AutoSize = true;
            this.lnkComputers.Location = new System.Drawing.Point(156, 44);
            this.lnkComputers.Name = "lnkComputers";
            this.lnkComputers.Size = new System.Drawing.Size(55, 13);
            this.lnkComputers.TabIndex = 4;
            this.lnkComputers.TabStop = true;
            this.lnkComputers.Text = "All Groups";
            this.lnkComputers.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkComputers_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Includi update con stato:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Computer dei gruppi:";
            // 
            // cmbReportType
            // 
            this.cmbReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReportType.FormattingEnabled = true;
            this.cmbReportType.Items.AddRange(new object[] {
            "Update Summary"});
            this.cmbReportType.Location = new System.Drawing.Point(159, 12);
            this.cmbReportType.Name = "cmbReportType";
            this.cmbReportType.Size = new System.Drawing.Size(222, 21);
            this.cmbReportType.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo report:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.reportViewer1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 125);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(558, 266);
            this.panel2.TabIndex = 1;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource2.Name = "UpdateInfo_Update";
            reportDataSource2.Value = this.UpdateBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "GrisSuite.UpdateServices.Administration.Reports.UpdateStatus.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(558, 266);
            this.reportViewer1.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdReportFilter,
            this.cmdRunReport});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(558, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmdReportFilter
            // 
            this.cmdReportFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdReportFilter.Image = ((System.Drawing.Image)(resources.GetObject("cmdReportFilter.Image")));
            this.cmdReportFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdReportFilter.Name = "cmdReportFilter";
            this.cmdReportFilter.Size = new System.Drawing.Size(76, 22);
            this.cmdReportFilter.Text = "Filtro Report";
            this.cmdReportFilter.Click += new System.EventHandler(this.cmdReportFilter_Click);
            // 
            // cmdRunReport
            // 
            this.cmdRunReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdRunReport.Image = ((System.Drawing.Image)(resources.GetObject("cmdRunReport.Image")));
            this.cmdRunReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRunReport.Name = "cmdRunReport";
            this.cmdRunReport.Size = new System.Drawing.Size(78, 22);
            this.cmdRunReport.Text = "Avvia Report";
            this.cmdRunReport.Click += new System.EventHandler(this.cmdRunReport_Click);
            // 
            // ReportDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 391);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlFilter);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ReportDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ReportDialog";
            ((System.ComponentModel.ISupportInitialize)(this.UpdateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateInfo)).EndInit();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.LinkLabel lnkUpdateStatus;
        private System.Windows.Forms.LinkLabel lnkComputers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbReportType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton cmdReportFilter;
        private System.Windows.Forms.ToolStripButton cmdRunReport;
        private System.Windows.Forms.BindingSource UpdateBindingSource;
        private GrisSuite.UpdateServices.Administration.Data.UpdateInfo UpdateInfo;
    }
}
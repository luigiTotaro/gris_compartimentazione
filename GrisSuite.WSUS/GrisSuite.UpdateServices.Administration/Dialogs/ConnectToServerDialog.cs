using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public partial class ConnectToServerDialog
    {

        public bool UseSSL
        {
            get
            {
                return chkUseSSL.Checked;
            }
        }

        public string ServerName
        {
            get
            {
                return txtServerName.Text;
            }
        }

        public int PortNumber
        {
            get
            {
                return int.Parse(cmbPortNumber.Text);
            }
        }

        public string ConnectionString
        {
            get
            {
                if (chkUseSSL.Checked)
                {
                    return "https://" + txtServerName.Text + ":" + cmbPortNumber.Text;
                }
                else
                {
                    return "http://" + txtServerName.Text + ":" + cmbPortNumber.Text;
                }
            }
        }

        private void OK_Button_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void Cancel_Button_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void ConnectToServerDialog_Shown(object sender, System.EventArgs e)
        {
            cmbPortNumber.SelectedIndex = 0;
            txtServerName.Focus();
        }

       
    }
}
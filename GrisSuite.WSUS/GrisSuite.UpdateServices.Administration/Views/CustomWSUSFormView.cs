using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public class CustomWSUSFormView : FormView
    {

        private Control _customWSUSControl;

        public CustomWSUSFormView()
            : base()
        {
        }

        protected override void OnInitialize(AsyncStatus status)
        {

            base.OnInitialize(status);
            _customWSUSControl = (CustomWSUSControl)this.Control;
            //control.RefreshProcessList();

            //ProcessWatcher.Update += new EventHandler(ProcessWatcher_Update);
        }

        //void ProcessWatcher_Update(object sender, EventArgs e)
        //{
        //   control.RefreshProcessList();
        //}

    }
}
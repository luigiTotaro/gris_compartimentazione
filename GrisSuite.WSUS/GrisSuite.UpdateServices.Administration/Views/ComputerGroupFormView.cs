using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class ComputerGroupFormView : FormView
    {

        private ComputerGroupControl _computerGroupControl;

        public ComputerGroupFormView():base()
        {
        }

        protected override void OnInitialize(AsyncStatus status)
        {
            base.OnInitialize(status);
            _computerGroupControl = (ComputerGroupControl)this.Control;
            //Creo lo handler per l'aggiornamento
            ComputerGroupControl.UpdateContents += UpdateContentsHandler;
            this.ActionsPaneItems.Add(new Action("Force Update", "Force Update", -1, "ForceUpdate"));
            Refresh();
        }

        private void UpdateContentsHandler()
        {
            Refresh();
        }

        protected void Refresh()
        {
            _computerGroupControl.RefreshData(((ComputerGroupScopeNode)this.ScopeNode).Group.GetComputerTargets());
        }

        protected override void OnAction(Microsoft.ManagementConsole.Action action, Microsoft.ManagementConsole.AsyncStatus status)
        {
            switch (action.Tag.ToString())
            {
                case "ForceUpdate":
                    _computerGroupControl.ForceUpdateSelected();
                    break;
            }
        }
    }
}
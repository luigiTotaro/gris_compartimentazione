using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;

namespace GrisSuite.UpdateServices.Administration
{
    public class ServerFormView : FormView
    {

        private Control _customWSUSControl;

        public ServerFormView()
            : base()
        {
        }

        protected override void OnInitialize(AsyncStatus status)
        {
            base.OnInitialize(status);
            _customWSUSControl = (ServerControl)this.Control;
        }

    }
}
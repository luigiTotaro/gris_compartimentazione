using System;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.ManagementConsole;
using System.Windows.Forms;
using Microsoft.UpdateServices.Administration;

namespace GrisSuite.UpdateServices.Administration
{
    public class CustomUpdatesFormView : FormView
    {

        private CustomUpdatesControl _customUpdatesControl;

        public CustomUpdatesFormView()
            : base()
        {
        }

        protected override void OnInitialize(AsyncStatus status)
        {
            base.OnInitialize(status);
            _customUpdatesControl = (CustomUpdatesControl)this.Control;
            Refresh();
            //Creo l'handler per l'aggiornamento
            CustomUpdatesControl.UpdateContents += UpdateContentsHandler;
        }

        private void UpdateContentsHandler()
        {
            Refresh();
        }

        protected void Refresh()
        {                
            UpdateCollection updateCollection = Application.Current.Server.GetUpdates(_customUpdatesControl.GetUpdateScope());
            //UpdateCollection updateCollection = Application.Current.Server.GetUpdates((ApprovedStates.Any, DateTime.MinValue, DateTime.MaxValue,null, null);
            _customUpdatesControl.RefreshData(updateCollection);
        }

    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using WUApiLib;

namespace GrisSuite.UpdateAgent
{
	public class ClientUpdateAgent
	{

		private UpdateSession _session;

		public ClientUpdateAgent()
		{
			_session = new UpdateSession();
		}

		public void Update()
		{            
			try
			{
				//Creo l'oggetto per ricercare gli updates
                UpdateSearcher _updateSearcher = (UpdateSearcher)_session.CreateUpdateSearcher();
				//Lancio ricerca
				ISearchResult _searchResult = _updateSearcher.Search("IsAssigned=1 and IsHidden=0 and IsInstalled=0 and Type='Software'");

				if (_searchResult.Updates.Count > 0)
				{
					//Se ho trovato almeno un update creo la lista degli update da scaricare
					UpdateCollection _updatesToDownload = new UpdateCollection();
					//Creo l'oggetto per lo scaricamento
					UpdateDownloader _downloader = _session.CreateUpdateDownloader();
					//Imposto gli update da scaricare
					foreach (IUpdate _update in _searchResult.Updates)
					{
						_updatesToDownload.Add(_update);
					}
					_downloader.Updates = _updatesToDownload;

					//Scarico gli updates
					IDownloadResult _downloadResult = _downloader.Download();
					if (_downloadResult.ResultCode == OperationResultCode.orcSucceeded)
					{
						//Se il downolad va a buon fine, creo lista degli update da installare
						UpdateCollection _updatesToInstall = new UpdateCollection();
						//Creo l'oggeto per l'installazione
                        UpdateInstaller _installer = (UpdateInstaller)_session.CreateUpdateInstaller();
						//Imposto gli update da installare
						foreach (IUpdate _update in _searchResult.Updates)
						{
							if (_update.IsDownloaded)
							{
								_updatesToInstall.Add(_update);
							}
						}
						_installer.ForceQuiet = true;
						_installer.Updates = _updatesToInstall;
						//Installo gli updates
						IInstallationResult _installResult = _installer.Install();
						if (_installResult.ResultCode == OperationResultCode.orcSucceeded)
						{
							EventLog.WriteEntry("WCUS", "Updates Install success!", EventLogEntryType.Information);
						}
						else
						{
							EventLog.WriteEntry("WCUS", "Updates Install Failed!", EventLogEntryType.Error);
						}
					}
					else
					{
						EventLog.WriteEntry("WCUS", "Updates Download Failed!", EventLogEntryType.Error);
					}
				}
				else
				{
					EventLog.WriteEntry("WCUS", "No updates to install found!", EventLogEntryType.Warning);
				}
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry("WCUS", ex.Message, EventLogEntryType.Error);
			}
		}
	}
}
using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;
using System.ServiceProcess;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ClientService : System.Web.Services.WebService
{
    ServiceController _sc;

    public ClientService () 
    {
        try
        {
            _sc = new ServiceController("WCUService");
        }
        catch (Exception)
        {

            throw;
        }        
    }

    [WebMethod]
    public void ForceUpdate() {
        if (_sc.Status == ServiceControllerStatus.Stopped)
        {
            _sc.Start();
            while (_sc.Status == ServiceControllerStatus.Stopped)
            {
                System.Threading.Thread.Sleep(1000);
                _sc.Refresh();
            }
        }
        //Esegue il comando 150 (Force Update)
        _sc.ExecuteCommand(150);

        //Scrivo un file nella directory "C:\Temp\Updates\" per far partire
        //il controllo degli updates
        //File.AppendAllText(@"C:\Temp\Updates\ForceUpdate.upd", "Dummy file");
    }
    
}

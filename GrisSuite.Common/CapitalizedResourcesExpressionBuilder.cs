﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Compilation;
using System.Web.UI;
using System.CodeDom;

namespace GrisSuite.Common
{
	class CapitalizedResourcesExpressionBuilder : ResourceExpressionBuilder
	{
		public override System.CodeDom.CodeExpression GetCodeExpression ( BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context )
		{
			CodeExpression baseExp = base.GetCodeExpression(entry, parsedData, context);
			CodeMethodInvokeExpression toStrExp = new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(typeof(Convert)), "ToString", baseExp);
			CodeMethodInvokeExpression exp = new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(typeof(Utility)), "CapitalizeFirstLetter", toStrExp);

			return exp;
		}
	}
}

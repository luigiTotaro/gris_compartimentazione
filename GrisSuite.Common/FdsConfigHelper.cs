﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite.Common
{
    public enum TipoFDS
    {
        Unknown,
        NormaleLocale,
        NormaleRemoto,
        RiservaLocale,
        RiservaRemoto,
        LightNormaleLocale,
        LightNormaleRemoto,
        LightRiservaLocale,
        LightRiservaRemoto
    }

    public static class FdsConfigHelper
    {
        public static bool IsFDSLight(TipoFDS fdsTipo)
        {
            switch (fdsTipo)
            {
                case TipoFDS.NormaleLocale:
                case TipoFDS.NormaleRemoto:
                case TipoFDS.RiservaLocale:
                case TipoFDS.RiservaRemoto:
                    return false;
                case TipoFDS.LightNormaleLocale:
                case TipoFDS.LightNormaleRemoto:
                case TipoFDS.LightRiservaLocale:
                case TipoFDS.LightRiservaRemoto:
                    return true;
            }

            return false;
        }

        public static int GetSlotCount(TipoFDS fdsTipo)
        {
            if (IsFDSLight(fdsTipo))
            {
                return 24;
            }

            return 36;
        }

        public static int[] GetInterfacesIndexes(TipoFDS fdsTipo)
        {
            // Indici a base 1
            if (IsFDSLight(fdsTipo))
            {
                // La struttura in base dati resta di 24 oggetti, ma le interfacce su FDS Light sono 14
                // indichiamo come 0 quelle inesistenti, che non saranno aggiunte ad interfaccia
                return new[] { 6, 7, 8, 9, 10, 11, 12, 0, 18, 19, 20, 21, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            }

            return new[] { 5, 6, 7, 8, 9, 10, 11, 12, 17, 18, 19, 20, 21, 22, 23, 24, 29, 30, 31, 32, 33, 34, 35, 36 };
        }

        public static int GetFD80000Position(TipoFDS fdsTipo)
        {
            // Indici a base 0
            if (IsFDSLight(fdsTipo))
            {
                return 4;
            }

            return 15;
        }

        public static int GetFD90000Position(TipoFDS fdsTipo, bool isRiserva)
        {
            // Indici a base 0
            if (IsFDSLight(fdsTipo))
            {
                return (isRiserva ? 16 : 14);
            }

            return (isRiserva ? 27 : 3);            
        }

        public static int GetSchedeFibraPosition(TipoFDS fdsTipo, bool isRiserva)
        {
            // Indici a base 0
            // Schede 91000, 92000
            if (IsFDSLight(fdsTipo))
            {
                return (isRiserva ? 15 : 13);
            }

            return (isRiserva ? 26 : 2);
        }

        public static int GetFD13000Position(TipoFDS fdsTipo)
        {
            // Indici a base 1
            if (IsFDSLight(fdsTipo))
            {
                return 13;
            }

            return 15;
        }

        public static Dictionary<int, int> GetSchedeAlimentazionePosition(TipoFDS fdsTipo)
        {
            // Indici a base 1
            // Schede 11000, 11100, 11200
            Dictionary<int, int> indiciPosizioni = new Dictionary<int, int>();

            if (IsFDSLight(fdsTipo))
            {
                indiciPosizioni.Add(1, 1);
                indiciPosizioni.Add(2, 2);
                indiciPosizioni.Add(3, 3);
                indiciPosizioni.Add(4, 4);
            }
            else
            {
                indiciPosizioni.Add(1, 1);
                indiciPosizioni.Add(2, 2);
                indiciPosizioni.Add(3, 13);
                indiciPosizioni.Add(4, 14);
                indiciPosizioni.Add(5, 25);
                indiciPosizioni.Add(6, 26);
            }

            return indiciPosizioni;
        }
    }
}
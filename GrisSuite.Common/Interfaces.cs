﻿
namespace GrisSuite.Common
{
	public interface IEntityWithSeverity
	{
		int SeverityValue { get; set; }
		string SeverityDescription { get; set; }
	}
}

﻿namespace GrisSuite.Common
{
	public enum Status
	{
		None = -1,
		Ok = 0,
		Warning = 1,
		Error = 2,
		Offline = 3,
		Maintenance = 9,
		Unknown = 255,
		Info = -255
	}

	public enum AlertsPresence
	{
		Present,
		NotPresent,
		Error,
		CacheEngineError
	}

	public enum ScagentActionEnum
	{
		SendConfigToCentralComplete = 128,
		SendConfigToCentralOnlyChanges,
		SendDeviceStatusToCentralComplete,
		SendDeviceStatusToCentralOnlyChanges,
		SendDeviceStatusAndConfigToCentral
	}

	public enum SystemValidationEnum : short
	{
		RequestCorrectlyReceived,
		ServerNotFound,
		SqlError
	}

	public enum GrisEventCategory : short
	{
		DeficeLevel1Events = 1,             //Eventi di periferica livello 1
		DeficeLevel2Events = 2,             //Eventi di periferica livello 2
		DeficeLevel3Events = 3,             //Eventi di periferica livello 3
		DeficeLevel4Events = 4,             //Eventi di periferica livello 4    
		SystemEvents = 5,                   //Eventi di Sistema
		ComunicationEvents = 6,             //Eventi relativi alle porte di comunicazione
		DatabaseEvents = 7                  //Eventi relativi al database
	}

	// Next Free Count  4027

	//    Categoria 5 = Eventi di Sistema
	public enum GrisSystemEvents : int
	{
		// Generici 
		OkInfo = 4010,                      //Sistema di diagnostica in stato di funzionamento corretto 0
		Warning = 4011,                     //Sistema di diagnostica in stato di attenzione 1
		Error = 4012,                       //Sistema di diagnostica in stato di errore 2
		InitializationStateWarning = 4013,  //Sistema di diagnostica in fase di inizializzazione 1
		ClosingStateWarning = 4014,         //Sistema di diagnostica in fase di chiusura 1
		SchedulingStartedInfo = 4015,       //Schedulazione del sistema di diagnostica avviata 0
		SchedulingStoppedWarning = 4016,    //Schedulazione del sistema di diagnostica arrestata 1
		ErrorOnCostructor = 4027,           //Errore nel costruttore 
		DBSemaphoreLockWarning = 2028,      //Attenzione problema in fase di lock di un semaforo
		DBSemaphoreUnlockWarning = 2029,    //Attenzione problema in fase di unlock di un semaforoù
		DBSemaphoreUnlockForcedWarning = 2031,  //Attenzione unlock forzato per "timeout" del blocco
		ReenterOnTimerWarning = 2030        //Rientro nell'evento di un timer prima che l'iterazione precedente sia conclusa
	}

	//Categoria 6 = Eventi relativi alle porte di comunicazione
	public enum GrisComunicationEvents : int
	{
		PortOkInfo = 4020,              //Porta di comunicazione in stato di funzionamento corretto 0
		PortWarning = 4021,             //Porta di comunicazione in stato di funzionamento anomalo 1
		PortError = 4022,               //Porta di comunicazione in stato di errore 2
		PortOpenedOkInfo = 4023,        //Porta aperta correttamente 0
		PortClosedOkInfo = 4024,        //Porta chiusa correttamente 0
		PortOpeningError = 4025,        //Errore nell'apertura della porta 2
		PortClosingWarning = 4026       //Errore nella chiusura della porta 1
	}

	//Categoria 7 = Eventi relativi al database
	public enum GrisDatabaseEvents : int
	{
		OpenConnctionInfo = 4000,       //Connessione al database riuscita 0
		OpenConnctionError = 4001,      //Connessione al database fallita 2
		CloseConnectionInfo = 4002,     //Disconnessione dal database riuscita  0
		CloseConnectionError = 4003,    //Disconnessione dal database fallita  2
		QueryInfo = 4004,               //Query sul database riuscita 0
		QueryError = 4005               //Query sul database fallita 2
	}

	public struct Systems
	{
		public const int DiffusioneSonora = 1;
		public const int InformazioneVisiva = 2;
		public const int SistemaIlluminazione = 3;
		public const int SistemaTelefonia = 4;
		public const int Rete = 5;
		public const int SistemaErogazioneEnergia = 6;
		public const int SistemaFDS = 8;
		public const int Diagnostica = 10;
		public const int Facilities = 11;
		public const int MonitoraggioVPNVerde = 12;
		public const int WebRadio = 13;
        public const int Orologi = 14;
        public const int Ascensori = 15;
		public const int AltrePeriferiche = 99;
	}

	public enum OperationScheduleType : short
	{
		Sms = 1,
		Mail = 2,
		StreamFieldMail = 3
	}

	// Mappa con tabella resource_types del DB
	public enum ResourceType : short
	{
		Reports = 1,
		Filtri = 2
	}

	// Mappa con tabella resource_categories del DB
	public enum ResourceCategory
	{
		Unknown = -1,
		REP_GEN = 1,
		REP_SRV = 2,
		REP_STO = 3,
		FIL_GEN = 4
	}

	// Mappa con tabella resources del DB
	public enum DatabaseResource
	{
		Unknown = -1,
		DevicesByStatusAndSystem = 1,
		FieldsByZoneSevLevelType = 2,
		Counters = 3,
		CountersDeviceVendor = 4,
		STLCOffline = 5,
		STLCList = 6,
		HistoryDeviceSevLevel = 7,
		HistoryStreamFieldsValue = 8,
		DeviceFilters = 9
	}

	// Mappa con valori nella tabella servers_licenses.IsLicensed e relative logiche SCAgent
	public enum ServerLicenseStatus : byte
	{
		NotLicensed = 0,
		Licensed = 1,
		NoLicenseRequired = 0xFF
	}

	/// <summary>
	/// Tipi di segnalazione parking - Indetica a SCAgent
	/// </summary>
	public enum ParkingKind
	{
		/// <summary>
		/// Sconosciuto
		/// </summary>
		Unknown = 0,
		/// <summary>
		/// Server con configurazione, con problemi su database o simili
		/// </summary>
		ConfiguredServer = 1,
		/// <summary>
		/// Server privo di System.xml
		/// </summary>
		MissingConfigurationServer = 2,
		/// <summary>
		/// Server che è stato parcheggiato, mantenuto per storico, ma che adesso centralizza
		/// </summary>
		PreviouslyParkedServer = 3
	}
}

namespace GrisSuite.Data
{

	public enum LogDBLevel
	{
		Off,
		MessageEnvelope,
		MessageComplete,
		MessageEnvelopeAndStream,
		MessageCompleteAndStream
	}
}
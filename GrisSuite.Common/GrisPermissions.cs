﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using GrisSuite.Data.Properties;
using GrisSuite.Data.Properties.PermissionsTableAdapters;

namespace GrisSuite.Common
{
	public static class GrisPermissions
	{
	    private const int GroupListCapacity = 200;

		# region Permessi

		public static PermissionGroupsCache GroupsCache
		{
			get { return PermissionGroupsCache.Instance; }
		}

		// Restituisce l'elenco dei compartimenti per cui l'utente ha visibilità almeno di livello 1
		public static long[] VisibleRegions()
		{
			return VisibleRegions(PermissionLevel.Level1);
		}

		public static long[] VisibleRegions(PermissionLevel level)
		{
			long regid = 281474976645120;
			List<long> regs = new List<long>(15);
			for (int reg = 1; reg <= 15; reg++)
			{
				if (IsInRole(reg, level))
				{
					regs.Add(regid + reg);
				}
			}

			return regs.ToArray();
		}

		public static bool IsInRole(long regId)
		{
			return IsInRole(Utility.GetRegCode(regId));
		}

		public static bool IsInRole(RegCodes regCode)
		{
			return IsInRole(regCode, PermissionLevel.Level1);
		}

		public static bool IsInRole(long regId, PermissionLevel level)
		{
			return IsInRole(Utility.GetRegCode(regId), level);
		}

		public static bool IsInRole(RegCodes regCode, PermissionLevel level)
		{
			IList<string> groups = GetGroupsByRegCodeAndLevel(regCode, level);
		    return IsInGrisGroups(groups);
		}

        private static bool IsInRole(IList<string> windowsGroups)
        {
            IPrincipal principal = HttpContext.Current.User;

            if (principal == null || windowsGroups.Count <= 0) return false;

            foreach (var groupOrUser in windowsGroups)
            {
                if (principal.IsInRole(groupOrUser) 
                    || principal.Identity.Name.Equals(groupOrUser,StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

		public static bool IsInRoleWithoutGlobalGroups(long regId)
		{
			return IsInRoleWithoutGlobalGroups(Utility.GetRegCode(regId));
		}

		public static bool IsInRoleWithoutGlobalGroups(RegCodes regCode)
		{
			return IsInRoleWithoutGlobalGroups(regCode, PermissionLevel.Level1);
		}

		public static bool IsInRoleWithoutGlobalGroups(long regId, PermissionLevel level)
		{
			return IsInRoleWithoutGlobalGroups(Utility.GetRegCode(regId), level);
		}

		public static bool IsInRoleWithoutGlobalGroups(RegCodes regCode, PermissionLevel level)
		{
            return IsInGrisGroups(GetGroupsByRegCodeAndLevelWithoutGlobalGroups(regCode, level));
		}

		public static IList<string> GetWindowsGroups(params string[] grisGroups)
		{
			return GetWindowsGroups(false, grisGroups);
		}

		public static IList<string> GetWindowsGroups(bool bypassCache, params string[] grisGroups)
		{
			List<Permissions.PermissionGroupsRow> groupsList = new List<Permissions.PermissionGroupsRow>();

			if (bypassCache)
			{
				string groupsString = string.Format("|{0}|", string.Join("|", grisGroups));

				PermissionGroupsTableAdapter permTa = new PermissionGroupsTableAdapter();
				Permissions.PermissionGroupsDataTable groupsTbl = permTa.GetGroupsByGroupNames(groupsString);

				foreach (Permissions.PermissionGroupsRow groupsRow in groupsTbl.Rows)
				{
					groupsList.Add(groupsRow);
				}
			}
			else
			{
				foreach (Permissions.PermissionGroupsRow groupsRow in GroupsCache.Get().Rows)
				{
					if (Array.IndexOf(grisGroups, groupsRow.GroupName) > -1)
					{
						groupsList.Add(groupsRow);
					}
				}
			}

            List<string> winGroups = new List<string>(GroupListCapacity);
			foreach (Permissions.PermissionGroupsRow groupsRow in groupsList)
			{
				foreach (string winGroup in groupsRow.WindowsGroups.Split(','))
				{
				    String g = winGroup.Trim();
                    if (!winGroups.Contains(g))
					{
                        winGroups.Add(g);
					}
				}
                foreach (string winUser in groupsRow.WindowsUsers.Split(','))
                {
                    String u = winUser.Trim();
                    if (!winGroups.Contains(u))
                    {
                        winGroups.Add(u);
                    }
                }

			}

			return winGroups;
		}

		public static IList<string> GetWindowsGroups(IEnumerable<string> grisGroups)
		{
			List<string> tmp = new List<string>(grisGroups);
			return GetWindowsGroups(tmp.ToArray());
		}

		public static IList<string> GetGroupsByRegCodeAndLevel(RegCodes regCode, PermissionLevel level)
		{
			return GetGroupsByRegCodeAndLevel(false, regCode, level);
		}

		public static IList<string> GetGroupsByRegCodeAndLevel(bool bypassCache, RegCodes regCode, PermissionLevel level)
		{
			List<Permissions.PermissionGroupsRow> groupsList = new List<Permissions.PermissionGroupsRow>();

			if (bypassCache)
			{
				PermissionGroupsTableAdapter permTa = new PermissionGroupsTableAdapter();
				Permissions.PermissionGroupsDataTable groupsTbl = permTa.GetGroupsByRegCodeAndLevel((byte) regCode, (byte) level);

				foreach (Permissions.PermissionGroupsRow groupsRow in groupsTbl.Rows)
				{
					groupsList.Add(groupsRow);
				}
			}
			else
			{
				foreach (Permissions.PermissionGroupsRow groupsRow in GroupsCache.Get().Rows)
				{
					//(RegCode IS NULL OR RegCode = @RegCode)
					//AND ([Level] >= @Level)
					//AND (IsBuiltIn = 1)
					if ((groupsRow.RegCode == 0 || (RegCodes) groupsRow.RegCode == regCode) && (PermissionLevel) groupsRow.Level >= level && groupsRow.IsBuiltIn)
					{
						groupsList.Add(groupsRow);
					}
				}
			}

            List<string> groups = new List<string>(GroupListCapacity);
			foreach (Permissions.PermissionGroupsRow permissionGroupsRow in groupsList)
			{
				groups.Add(permissionGroupsRow.GroupName);
			}

			return groups;
		}

		public static IList<string> GetGroupsByRegCodeAndLevelWithoutGlobalGroups(RegCodes regCode, PermissionLevel level)
		{
			return GetGroupsByRegCodeAndLevelWithoutGlobalGroups(false, regCode, level);
		}

		public static IList<string> GetGroupsByRegCodeAndLevelWithoutGlobalGroups(bool bypassCache, RegCodes regCode, PermissionLevel level)
		{
			List<Permissions.PermissionGroupsRow> groupsList = new List<Permissions.PermissionGroupsRow>();

			if (bypassCache)
			{
				PermissionGroupsTableAdapter permTa = new PermissionGroupsTableAdapter();
				Permissions.PermissionGroupsDataTable groupsTbl = permTa.GetGroupsByRegCodeAndLevel((byte) regCode, (byte) level);

				foreach (Permissions.PermissionGroupsRow groupsRow in groupsTbl.Rows)
				{
					if (groupsRow.RegCode != 0)
					{
						groupsList.Add(groupsRow);
					}
				}
			}
			else
			{
				foreach (Permissions.PermissionGroupsRow groupsRow in GroupsCache.Get().Rows)
				{
					//(RegCode IS NULL OR RegCode = @RegCode)
					//AND ([Level] >= @Level)
					//AND (IsBuiltIn = 1)
					if ((groupsRow.RegCode == 0 || (RegCodes) groupsRow.RegCode == regCode) && (PermissionLevel) groupsRow.Level >= level && groupsRow.IsBuiltIn)
					{
						if (groupsRow.RegCode != 0)
						{
							groupsList.Add(groupsRow);
						}
					}
				}
			}

            List<string> groups = new List<string>(GroupListCapacity);
			foreach (Permissions.PermissionGroupsRow permissionGroupsRow in groupsList)
			{
				groups.Add(permissionGroupsRow.GroupName);
			}

			return groups;
		}

		public static bool IsUserAtLeastInLevel(PermissionLevel level)
		{
			bool isInLevel = false;
			for (long reg = 1; reg <= 15; reg++)
			{
				if (IsInRole(reg, level))
				{
					isInLevel = true;
					break;
				}
			}

			return isInLevel;
		}

		public static bool IsUserAtLeastInLevelForAllRegions(PermissionLevel level)
		{
		    IList<string> groups = new List<string> {"groupAdmin", "groupGrisOperators"};

			switch (level)
			{
				case PermissionLevel.Level1:
				{
					groups.Add("groupAll_1");
					groups.Add("groupAll_2");
					groups.Add("groupAll_3");
					break;
				}
				case PermissionLevel.Level2:
				{
					groups.Add("groupAll_2");
					groups.Add("groupAll_3");
					break;
				}
				case PermissionLevel.Level3:
				{
					groups.Add("groupAll_3");
					break;
				}
				default:
				{
					groups = new List<string>(0);
					break;
				}
			}

		    return IsInGrisGroups(groups);
		}

		public static bool IsUserInConfigValidationGroup()
		{
            return IsInGrisGroups("groupConfigValidation");
		}

		public static bool IsUserInGrisOperatorsGroup()
		{
            return IsInGrisGroups("groupGrisOperators");
		}

		public static bool IsUserInSTLCOperatorsGroup()
		{
            return IsInGrisGroups("groupSTLCOperators");
		}

		public static bool IsUserInRulesOperatorsGroup()
		{
            return IsInGrisGroups("groupRulesOperators");
		}

		public static bool IsUserInAlertOperatorsGroup()
		{
            return IsInGrisGroups("groupAlertOperators");
		}

		public static bool IsUserInAlertUsersGroup()
		{
            return IsInGrisGroups("groupAlertUsers");
		}

		public static bool IsUserInAdminGroup()
		{
            return IsInGrisGroups("groupAdmin");
		}

		public static bool IsUserInVPNVerdeItaliaGroup()
		{
            return IsInGrisGroups("groupVPNVerdeItalia");
		}

		public static bool IsUserInWebRadioItaliaGroup()
		{
            return IsInGrisGroups("groupWebRadioItalia");
		}

		public static bool IsUserInLocalAdminGroup()
		{
            return IsInGrisGroups("groupAdmin1", "groupAdmin2", "groupAdmin3", "groupAdmin4", "groupAdmin5", "groupAdmin6",
				"groupAdmin7", "groupAdmin8", "groupAdmin9", "groupAdmin10", "groupAdmin11", "groupAdmin12", "groupAdmin13", "groupAdmin14", "groupAdmin15");
		}

		public static bool IsUserInLocalVPNVerdeCompGroup()
		{
            return IsInGrisGroups("groupVPNVerdeComp1", "groupVPNVerdeComp2", "groupVPNVerdeComp3", "groupVPNVerdeComp4",
				"groupVPNVerdeComp5", "groupVPNVerdeComp6", "groupVPNVerdeComp7", "groupVPNVerdeComp8", "groupVPNVerdeComp9", "groupVPNVerdeComp10",
				"groupVPNVerdeComp11", "groupVPNVerdeComp12", "groupVPNVerdeComp13", "groupVPNVerdeComp14", "groupVPNVerdeComp15");

		}

		public static bool IsUserInLocalWebRadioCompGroup()
		{
            return IsInGrisGroups("groupWebRadioComp1", "groupWebRadioComp2", "groupWebRadioComp3", "groupWebRadioComp4",
				"groupWebRadioComp5", "groupWebRadioComp6", "groupWebRadioComp7", "groupWebRadioComp8", "groupWebRadioComp9", "groupWebRadioComp10",
				"groupWebRadioComp11", "groupWebRadioComp12", "groupWebRadioComp13", "groupWebRadioComp14", "groupWebRadioComp15");

		}

		public static bool IsUserInGroupingOperatorsGroup()
		{
            return IsInGrisGroups("groupGroupingOperators1", "groupGroupingOperators2", "groupGroupingOperators3",
				"groupGroupingOperators4", "groupGroupingOperators5", "groupGroupingOperators6", "groupGroupingOperators7", "groupGroupingOperators8",
				"groupGroupingOperators9", "groupGroupingOperators10", "groupGroupingOperators11", "groupGroupingOperators12", "groupGroupingOperators13",
				"groupGroupingOperators14", "groupGroupingOperators15");

		}

        public static bool IsUserInGroupingCommandControl(int systemID)
        {
            string group = "groupCommandControlSystem_" + systemID.ToString();

            return IsInGrisGroups(group);

        }

		public static bool IsUserInLocalAdminGroup(RegCodes regId)
		{
			return IsUserInLocalGroup("groupAdmin", regId);
		}

		public static bool IsUserInLocalDeviceGroupingOperatorsGroup(RegCodes regId)
		{
			return IsUserInLocalGroup("groupGroupingOperators", regId);
		}

		public static bool IsUserInLocalVPNVerdeCompGroup(RegCodes regId)
		{
			return IsUserInLocalGroup("groupVPNVerdeComp", regId);
		}

		public static bool IsUserInLocalWebRadioCompGroup(RegCodes regId)
		{
			return IsUserInLocalGroup("groupWebRadioComp", regId);
		}

		public static bool IsUserInLocalGroup(string groupName, RegCodes regId)
		{
            return IsInGrisGroups(string.Format("{0}{1}", groupName, (int) regId));
		}

		public static bool CanUserAccessGris()
		{
			return (!IsUserInNoGrisGroup() && (IsUserAtLeastInLevel(PermissionLevel.Level1) || IsUserInConfigValidationGroup()));
		}

		public static bool CanUserViewAlerts()
		{
			return (CanUserEditAlerts() || IsUserInAlertUsersGroup());
		}

		public static bool CanUserEditAlerts()
		{
			return (IsUserInAdminGroup() || IsUserInAlertOperatorsGroup());
		}

		public static bool IsUserInGrisClientLicenseAdminGroup()
		{
            return IsInGrisGroups("groupGrisClientLicenseAdmin");
		}

        public static Boolean IsInGrisGroups(IEnumerable<string> grisGroups)
        {
            List<string> tmp = new List<string>(grisGroups);
            return IsInGrisGroups(tmp.ToArray());
        }

        private static bool IsInGrisGroups(params string[] grisGroups)
	    {
	        return IsInRole(GetWindowsGroups(grisGroups));
	    }

	    public static bool IsUserInNoGrisGroup()
	    {
	        return IsInGrisGroups("groupNoGris");
	    }

	    public static bool IsUserInGrisClientLicenseUsersGroup()
		{
            return IsInGrisGroups("groupGrisClientLicenseUsers");
		}


        public static IList<string> GetGroupsByResource(int resourceId)
        {
            List<Permissions.PermissionGroupsRow> groupsList = new List<Permissions.PermissionGroupsRow>();

            PermissionGroupsTableAdapter permTa = new PermissionGroupsTableAdapter();
            Permissions.PermissionGroupsDataTable groupsTbl = permTa.GetGroupsByResource(resourceId);

            foreach (Permissions.PermissionGroupsRow groupsRow in groupsTbl.Rows)
            {
                groupsList.Add(groupsRow);
            }

            
            List<string> groups = new List<string>(GroupListCapacity);
            foreach (Permissions.PermissionGroupsRow permissionGroupsRow in groupsList)
            {
                groups.Add(permissionGroupsRow.GroupName);
            }

            return groups;
        }

	    public static bool CanAccessResource(int resourceId)
	    {
            return IsInGrisGroups(GetGroupsByResource(resourceId));
	    }


		# endregion
	}

	public sealed class PermissionGroupsCache
	{
		private readonly object _sysnc = new object();
		private Permissions.PermissionGroupsDataTable _cachedGroupsTable;

		private static readonly PermissionGroupsCache _instance = new PermissionGroupsCache();

		public static PermissionGroupsCache Instance
		{
			get { return _instance; }
		}

		public Permissions.PermissionGroupsDataTable Get()
		{
			lock (_sysnc)
			{
			    if (_cachedGroupsTable != null) return (Permissions.PermissionGroupsDataTable) _cachedGroupsTable.Copy();
			    
                PermissionGroupsTableAdapter ta = new PermissionGroupsTableAdapter();
			    _cachedGroupsTable = ta.GetData();

			    return (Permissions.PermissionGroupsDataTable) _cachedGroupsTable.Copy();
			}
		}

		public void Reset()
		{
			lock (_sysnc)
			{
				_cachedGroupsTable = null;
			}
		}
	}

	public enum PermissionLevel : byte
	{
		NoLevel,
		Level1,
		Level2,
		Level3,
		AllLevels = 99
	}

	public enum RegCodes : byte
	{
		Torino = 1,
		Milano,
		Verona,
		Venezia,
		Trieste,
		Genova,
		Bologna,
		Firenze,
		Ancona,
		Roma,
		Napoli,
		Bari,
		ReggioCalabria,
		Palermo,
		Cagliari
	}

	public enum DatabaseRegCodes : long
	{
		Torino = 281474976645121,
		Milano,
		Verona,
		Venezia,
		Trieste,
		Genova,
		Bologna,
		Firenze,
		Ancona,
		Roma,
		Napoli,
		Bari,
		Reggio_Calabria,
		Palermo,
		Cagliari
	}
}
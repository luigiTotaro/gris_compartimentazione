﻿using System;
using System.Globalization;

namespace CentralWS.Database.Entities
{
    [Serializable]
    public class GrisVendor
    {
        public int VendorID { get; set; }

        public string VendorName { get; set; }

        public string VendorDescription { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "VendorID: {0}, VendorName: {1}, VendorDescription: {2}", this.VendorID.ToString(),
                this.VendorName, this.VendorDescription);
        }
    }

    [Serializable]
    public class GrisSystem
    {
        public int SystemID { get; set; }

        public string SystemDescription { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "SystemID: {0}, SystemDescription: {1}", this.SystemID.ToString(),
                this.SystemDescription);
        }
    }

    [Serializable]
    public class GrisDeviceType
    {
        public string DeviceTypeID { get; set; }

        public int SystemID { get; set; }

        public int VendorID { get; set; }

        public string DeviceTypeDescription { get; set; }

        public string WSUrlPattern { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "DeviceTypeID: {0}, SystemID: {1}, VendorID: {2}, DeviceTypeDescription: {3}, WSUrlPattern: {4}", this.DeviceTypeID.ToString(),
                this.SystemID.ToString(), this.VendorID.ToString(), this.DeviceTypeDescription, this.WSUrlPattern);
        }
    }

    [Serializable]
    public class GrisRegion
    {
        public long RegID { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "RegID: {0}, Name: {1}", this.RegID.ToString(), this.Name);
        }
    }

    [Serializable]
    public class GrisZone
    {
        public long ZonID { get; set; }

        public long RegID { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "ZonID: {0}, RegID: {1}, Name: {2}", this.ZonID.ToString(), this.RegID.ToString(),
                this.Name);
        }
    }

    [Serializable]
    public class GrisNode
    {
        public long NodID { get; set; }

        public long ZonID { get; set; }

        public string Name { get; set; }

        public int Meters { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "NodID: {0}, ZonID: {1}, Name: {2}, Meters: {3}", this.NodID.ToString(),
                this.ZonID.ToString(), this.Name, this.Meters.ToString());
        }
    }
}
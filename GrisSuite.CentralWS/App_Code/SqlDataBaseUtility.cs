﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using CentralWS.Database.Entities;

public static class SqlDataBaseUtility
{
    private static string DbConnectionString { get; set; }

    private static int SqlCommandTimeoutSeconds { get; set; }

    static SqlDataBaseUtility()
    {
        DbConnectionString = GetDbConnectionString();
        SqlCommandTimeoutSeconds = GetSqlCommandTimeoutSeconds();
    }

    private static string GetDbConnectionString()
    {
        ConnectionStringSettings localDatabaseConnectionString =
            ConfigurationManager.ConnectionStrings["GrisSuite.Data.Properties.Settings.TelefinConnectionString"];

        if (localDatabaseConnectionString == null)
        {
            return null;
        }

        SqlConnection dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);

        return dbConnection.ConnectionString;
    }

    private static int GetSqlCommandTimeoutSeconds()
    {
        int sqlCommandTimeoutSeconds;
        if (!int.TryParse(ConfigurationManager.AppSettings["SqlCommandTimeoutSeconds"], out sqlCommandTimeoutSeconds))
        {
            sqlCommandTimeoutSeconds = 120;
        }

        return sqlCommandTimeoutSeconds;
    }

    public static string GetCollectionStringRepresentation<T>(List<T> collection)
    {
        if ((collection != null) && (collection.Any()))
        {
            StringBuilder representation = new StringBuilder();

            foreach (object o in collection)
            {
                representation.AppendFormat("{0}\r\n", o.ToString());
            }

            return representation.ToString();
        }

        return String.Empty;
    }

    public static List<GrisDeviceType> GetDeviceTypeListFromDatabase()
    {
        List<GrisDeviceType> deviceTypes = new List<GrisDeviceType>();

        if (DbConnectionString != null)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                dbConnection = new SqlConnection(DbConnectionString);

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT DeviceTypeID, SystemID, VendorID, DeviceTypeDescription, WSUrlPattern FROM device_type ORDER BY DeviceTypeID";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colDeviceTypeID = dr.GetOrdinal("DeviceTypeID");
                    int colSystemID = dr.GetOrdinal("SystemID");
                    int colVendorID = dr.GetOrdinal("VendorID");
                    int colDeviceTypeDescription = dr.GetOrdinal("DeviceTypeDescription");
                    int colWSUrlPattern = dr.GetOrdinal("WSUrlPattern");

                    do
                    {
                        string deviceTypeID = dr.GetSqlString(colDeviceTypeID).Value;
                        int systemID = dr.GetSqlInt32(colSystemID).Value;
                        int vendorID = dr.GetSqlInt32(colVendorID).Value;
                        string deviceTypeDescription = dr.IsDBNull(colDeviceTypeDescription) ? null : dr.GetSqlString(colDeviceTypeDescription).Value;
                        string wsUrlPattern = dr.IsDBNull(colWSUrlPattern) ? null : dr.GetSqlString(colWSUrlPattern).Value;

                        deviceTypes.Add(new GrisDeviceType
                        {
                            DeviceTypeID = deviceTypeID,
                            SystemID = systemID,
                            VendorID = vendorID,
                            DeviceTypeDescription = deviceTypeDescription,
                            WSUrlPattern = wsUrlPattern
                        });
                    } while (dr.Read());
                }
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        return deviceTypes;
    }

    public static List<GrisVendor> GetVendorListFromDatabase()
    {
        List<GrisVendor> vendors = new List<GrisVendor>();

        if (DbConnectionString != null)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                dbConnection = new SqlConnection(DbConnectionString);

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT VendorID, VendorName, VendorDescription FROM vendors ORDER BY VendorID";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colVendorID = dr.GetOrdinal("VendorID");
                    int colVendorName = dr.GetOrdinal("VendorName");
                    int colVendorDescription = dr.GetOrdinal("VendorDescription");

                    do
                    {
                        int vendorID = dr.GetSqlInt32(colVendorID).Value;
                        string vendorName = dr.GetSqlString(colVendorName).Value;
                        string vendorDescription = dr.IsDBNull(colVendorDescription) ? null : dr.GetSqlString(colVendorDescription).Value;

                        vendors.Add(new GrisVendor {VendorID = vendorID, VendorName = vendorName, VendorDescription = vendorDescription});
                    } while (dr.Read());
                }
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        return vendors;
    }

    public static List<GrisSystem> GetSystemListFromDatabase()
    {
        List<GrisSystem> systems = new List<GrisSystem>();

        if (DbConnectionString != null)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                dbConnection = new SqlConnection(DbConnectionString);

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT SystemID, SystemDescription FROM systems ORDER BY SystemID";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colSystemID = dr.GetOrdinal("SystemID");
                    int colSystemDescription = dr.GetOrdinal("SystemDescription");

                    do
                    {
                        int systemID = dr.GetSqlInt32(colSystemID).Value;
                        string systemDescription = dr.GetSqlString(colSystemDescription).Value;

                        systems.Add(new GrisSystem {SystemID = systemID, SystemDescription = systemDescription});
                    } while (dr.Read());
                }
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        return systems;
    }

    public static List<GrisRegion> GetRegionListFromDatabase()
    {
        List<GrisRegion> regions = new List<GrisRegion>();

        if (DbConnectionString != null)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                dbConnection = new SqlConnection(DbConnectionString);

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT RegID, Name FROM regions ORDER BY RegID";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colRegID = dr.GetOrdinal("RegID");
                    int colName = dr.GetOrdinal("Name");

                    do
                    {
                        long regionID = dr.GetSqlInt64(colRegID).Value;
                        string name = dr.GetSqlString(colName).Value;

                        regions.Add(new GrisRegion {RegID = regionID, Name = name});
                    } while (dr.Read());
                }
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        return regions;
    }

    public static List<GrisZone> GetZoneListFromDatabase()
    {
        List<GrisZone> zones = new List<GrisZone>();

        if (DbConnectionString != null)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                dbConnection = new SqlConnection(DbConnectionString);

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "SELECT ZonID, RegID, Name FROM zones ORDER BY ZonID";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colZonID = dr.GetOrdinal("ZonID");
                    int colRegID = dr.GetOrdinal("RegID");
                    int colName = dr.GetOrdinal("Name");

                    do
                    {
                        long zoneID = dr.GetSqlInt64(colZonID).Value;
                        long regionID = dr.GetSqlInt64(colRegID).Value;
                        string name = dr.GetSqlString(colName).Value;

                        zones.Add(new GrisZone {ZonID = zoneID, RegID = regionID, Name = name});
                    } while (dr.Read());
                }
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        return zones;
    }

    public static List<GrisNode> GetNodeListFromDatabase()
    {
        List<GrisNode> nodes = new List<GrisNode>();

        if (DbConnectionString != null)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                dbConnection = new SqlConnection(DbConnectionString);

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT NodID, ZonID, Name, ISNULL(Meters, 0) AS Meters FROM nodes ORDER BY NodID";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNodID = dr.GetOrdinal("NodID");
                    int colZonID = dr.GetOrdinal("ZonID");
                    int colName = dr.GetOrdinal("Name");
                    int colMeters = dr.GetOrdinal("Meters");

                    do
                    {
                        long nodeID = dr.GetSqlInt64(colNodID).Value;
                        long zoneID = dr.GetSqlInt64(colZonID).Value;
                        string name = dr.GetSqlString(colName).Value;
                        int meters = dr.GetSqlInt32(colMeters).Value;

                        nodes.Add(new GrisNode {NodID = nodeID, ZonID = zoneID, Name = name, Meters = meters});
                    } while (dr.Read());
                }
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        return nodes;
    }
}
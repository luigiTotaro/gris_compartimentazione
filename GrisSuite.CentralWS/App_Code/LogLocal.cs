using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Data.SqlClient;
using GrisSuite.Common;

/// <summary>
/// Summary description for LogLocal
/// </summary>
internal class LogLocal
{
    private string _logSource = "GrisSuite.CentralWS"; //AppDomain.CurrentDomain.FriendlyName;
    private string _userHostAddress = string.Empty; 
    internal LogLocal()
    {}

    internal LogLocal(string userHostAddress)
    {
        _userHostAddress = userHostAddress;
    }

    internal string UserHostAddress
    {
        get 
        {
            return _userHostAddress;
        }
        set 
        {
            _userHostAddress = value;
        }
    }

    internal  void LogSqlErrors(SqlException exception, string procedureName)
    {
        LogSqlErrors(exception, procedureName, EventLogEntryType.Error, (int)GrisDatabaseEvents.QueryInfo);
    }

    internal void LogSqlErrors(SqlException exception, string procedureName, EventLogEntryType logEntryType, int eventId)
    {
        string error = "Error SQL on {0} from Host: {1} ErrorNumber {2} Error {3}";
        EventLog.WriteEntry(_logSource, string.Format(error, procedureName, this.UserHostAddress, exception.Number, exception.ToString()), logEntryType, eventId, (short)GrisEventCategory.DatabaseEvents);
        //foreach (SqlError sqlErr in exception.Errors)
        //{
        //    EventLog.WriteEntry(_logSource, string.Format(error, procedureName, this.Context.Request.UserHostAddress, sqlErr.Number, sqlErr.ToString()), EventLogEntryType.Warning);
        //}
    }

    internal void LogErrors(Exception exception, string procedureName)
    {
        if (exception is SqlException)
        {
            LogSqlErrors((SqlException)exception, procedureName, EventLogEntryType.Error, (int)GrisDatabaseEvents.QueryInfo);
        }
        else
        {
            LogErrors(exception, procedureName, EventLogEntryType.Error, (int)GrisSystemEvents.Error, GrisEventCategory.SystemEvents);
        }
    }


    internal  void LogErrors(Exception exception, string procedureName, EventLogEntryType logEntryType, int eventId, GrisEventCategory category)
    {
        string error = "Error on {0} from Host: {1} Error {2}";
        EventLog.WriteEntry(_logSource, string.Format(error, procedureName, this.UserHostAddress, exception.ToString()), logEntryType, eventId, (short)category);
    }
}


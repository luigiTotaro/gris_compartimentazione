using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services;
using System.Xml;
using CentralWS.Database.Entities;
using GrisSuite;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.dsCentralServersTableAdapters;

[WebService(Namespace = "http://Telefin.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Central : WebService
{
    private readonly LogLocal _log;

    public Central()
    {
        this._log = new LogLocal(this.Context.Request.UserHostAddress);
    }

    [WebMethod]
    public XmlDocument GetRegionList()
    {
        XmlDocument xDoc = new XmlDocument();

        try
        {
            ImpExpRegionList ierl = new ImpExpRegionList();

            string sRegionList;
            using (MemoryStream ms = new MemoryStream())
            {
                ierl.ExportToStream(ms);
                ms.Position = 0;
                TextReader tr = new StreamReader(ms, Encoding.GetEncoding("iso-8859-1"));
                sRegionList = tr.ReadToEnd();

                tr.Close();
            }

            xDoc.LoadXml(sRegionList);
            XmlDeclaration xmldecl = xDoc.CreateXmlDeclaration("1.0", "iso-8859-1", "yes");
            xDoc.InsertBefore(xmldecl, xDoc.DocumentElement);
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetRegionList");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetRegionList");
            throw new SCException(ex.Message);
        }

        return xDoc;
    }

    [WebMethod]
    public short SendValidationRequest(int serverID, DateTime clientDate)
    {
        try
        {
            serversTableAdapter ta = new serversTableAdapter();
            dsCentralServers.serversDataTable servers = ta.GetDataByID(serverID);

            if (servers.Count > 0)
            {
                ta.UpdateServerValidationRequest(serverID, clientDate, DateTime.Now);
                return (short) SystemValidationEnum.RequestCorrectlyReceived;
            }

            return (short) SystemValidationEnum.ServerNotFound;
        }
        catch (SqlException ex)
        {
            this._log.LogSqlErrors(ex, "SendValidationRequest");
            return (short) SystemValidationEnum.SqlError;
        }
    }

    [WebMethod]
    public DateTime GetTime()
    {
        return DateTime.Now;
    }

    [WebMethod]
    public XmlDocument GetDeviceTypeList()
    {
        XmlDocument xDoc = new XmlDocument();

        try
        {
            ImpExpDeviceTypeList iedpl = new ImpExpDeviceTypeList();

            string deviceTypeListString;
            using (MemoryStream ms = new MemoryStream())
            {
                iedpl.ExportToStream(ms);
                ms.Position = 0;
                TextReader tr = new StreamReader(ms, Encoding.GetEncoding("iso-8859-1"));
                deviceTypeListString = tr.ReadToEnd();
                tr.Close();
            }

            xDoc.LoadXml(deviceTypeListString);
            XmlDeclaration xmldecl = xDoc.CreateXmlDeclaration("1.0", "iso-8859-1", "yes");
            xDoc.InsertBefore(xmldecl, xDoc.DocumentElement);
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetDeviceTypeList");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetDeviceTypeList");
            throw new SCException(ex.Message);
        }

        return xDoc;
    }

    #region Metodi per STLCManager.NET (regioni, linee, stazioni, tipi periferica, sistemi e produttori)

    [WebMethod]
    public byte[] GetAllDeviceTypes()
    {
        byte[] serializedData;

        try
        {
            List<GrisDeviceType> deviceTypes = SqlDataBaseUtility.GetDeviceTypeListFromDatabase();

            StringBuilder serializedString = new StringBuilder();

            foreach (GrisDeviceType deviceType in deviceTypes)
            {
                serializedString.Append(String.Format("{0}�{1}�{2}�{3}�{4}\n", deviceType.DeviceTypeID.ToString(), deviceType.SystemID.ToString(),
                    deviceType.VendorID.ToString(), deviceType.DeviceTypeDescription.Replace("�", String.Empty),
                    (deviceType.WSUrlPattern == null) ? String.Empty : deviceType.WSUrlPattern.Replace("�", String.Empty)));
            }

            serializedData = CompressedBinaryConverter.ToByteArray(serializedString.ToString());
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllDeviceTypes");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllDeviceTypes");
            throw new SCException(ex.Message);
        }

        return serializedData;
    }

    [WebMethod]
    public string GetAllDeviceTypesChecksum()
    {
        try
        {
            List<GrisDeviceType> deviceTypes = SqlDataBaseUtility.GetDeviceTypeListFromDatabase();
            string deviceTypesRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(deviceTypes);

            if (!String.IsNullOrEmpty(deviceTypesRepresentation))
            {
                string checksum;

                using (SHA256 sha = new SHA256Managed())
                {
                    checksum =
                        BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(deviceTypesRepresentation)))
                            .Replace("-", string.Empty);
                }

                string indexes = deviceTypes.Select(deviceType => deviceType.DeviceTypeID).Aggregate((indexList, id) => indexList + "','" + id);

                return String.Format("{0};'{1}'", checksum, indexes);
            }

            return String.Empty;
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllDeviceTypesChecksum");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllDeviceTypesChecksum");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public byte[] GetAllSystems()
    {
        byte[] serializedData;

        try
        {
            List<GrisSystem> systems = SqlDataBaseUtility.GetSystemListFromDatabase();

            StringBuilder serializedString = new StringBuilder();

            foreach (GrisSystem system in systems)
            {
                serializedString.Append(String.Format("{0}�{1}\n", system.SystemID.ToString(), system.SystemDescription.Replace("�", String.Empty)));
            }

            serializedData = CompressedBinaryConverter.ToByteArray(serializedString.ToString());
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllSystems");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllSystems");
            throw new SCException(ex.Message);
        }

        return serializedData;
    }

    [WebMethod]
    public string GetAllSystemsChecksum()
    {
        try
        {
            List<GrisSystem> systems = SqlDataBaseUtility.GetSystemListFromDatabase();
            string systemsRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(systems);

            if (!String.IsNullOrEmpty(systemsRepresentation))
            {
                string checksum;

                using (SHA256 sha = new SHA256Managed())
                {
                    checksum = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(systemsRepresentation)))
                        .Replace("-", string.Empty);
                }

                string indexes = systems.Select(system => system.SystemID.ToString()).Aggregate((indexList, id) => indexList + "," + id);

                return String.Format("{0};{1}", checksum, indexes);
            }

            return String.Empty;
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllSystemsChecksum");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllSystemsChecksum");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public byte[] GetAllVendors()
    {
        byte[] serializedData;

        try
        {
            List<GrisVendor> vendors = SqlDataBaseUtility.GetVendorListFromDatabase();

            StringBuilder serializedString = new StringBuilder();

            foreach (GrisVendor vendor in vendors)
            {
                serializedString.Append(String.Format("{0}�{1}�{2}\n", vendor.VendorID.ToString(), vendor.VendorName.Replace("�", String.Empty),
                    (vendor.VendorDescription == null) ? String.Empty : vendor.VendorDescription.Replace("�", String.Empty)));
            }

            serializedData = CompressedBinaryConverter.ToByteArray(serializedString.ToString());
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllVendors");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllVendors");
            throw new SCException(ex.Message);
        }

        return serializedData;
    }

    [WebMethod]
    public string GetAllVendorsChecksum()
    {
        try
        {
            List<GrisVendor> vendors = SqlDataBaseUtility.GetVendorListFromDatabase();
            string vendorsRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(vendors);

            if (!String.IsNullOrEmpty(vendorsRepresentation))
            {
                string checksum;

                using (SHA256 sha = new SHA256Managed())
                {
                    checksum = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(vendorsRepresentation)))
                        .Replace("-", string.Empty);
                }

                string indexes = vendors.Select(vendor => vendor.VendorID.ToString()).Aggregate((indexList, id) => indexList + "," + id);

                return String.Format("{0};{1}", checksum, indexes);
            }

            return String.Empty;
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllVendorsChecksum");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllVendorsChecksum");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public byte[] GetAllRegions()
    {
        byte[] serializedData;

        try
        {
            List<GrisRegion> regions = SqlDataBaseUtility.GetRegionListFromDatabase();

            StringBuilder serializedString = new StringBuilder();

            foreach (GrisRegion region in regions)
            {
                serializedString.Append(String.Format("{0}�{1}\n", region.RegID.ToString(), region.Name.Replace("�", String.Empty)));
            }

            serializedData = CompressedBinaryConverter.ToByteArray(serializedString.ToString());
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllRegions");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllRegions");
            throw new SCException(ex.Message);
        }

        return serializedData;
    }

    [WebMethod]
    public string GetAllRegionsChecksum()
    {
        try
        {
            List<GrisRegion> regions = SqlDataBaseUtility.GetRegionListFromDatabase();
            string regionsRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(regions);

            if (!String.IsNullOrEmpty(regionsRepresentation))
            {
                string checksum;

                using (SHA256 sha = new SHA256Managed())
                {
                    checksum = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(regionsRepresentation)))
                        .Replace("-", string.Empty);
                }

                string indexes = regions.Select(region => region.RegID.ToString()).Aggregate((indexList, id) => indexList + "," + id);

                return String.Format("{0};{1}", checksum, indexes);
            }

            return String.Empty;
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllRegionsChecksum");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllRegionsChecksum");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public byte[] GetAllZones()
    {
        byte[] serializedData;

        try
        {
            List<GrisZone> zones = SqlDataBaseUtility.GetZoneListFromDatabase();

            StringBuilder serializedString = new StringBuilder();

            foreach (GrisZone zone in zones)
            {
                serializedString.Append(String.Format("{0}�{1}�{2}\n", zone.ZonID.ToString(), zone.RegID.ToString(),
                    zone.Name.Replace("�", String.Empty)));
            }

            serializedData = CompressedBinaryConverter.ToByteArray(serializedString.ToString());
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllZones");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllZones");
            throw new SCException(ex.Message);
        }

        return serializedData;
    }

    [WebMethod]
    public string GetAllZonesChecksum()
    {
        try
        {
            List<GrisZone> zones = SqlDataBaseUtility.GetZoneListFromDatabase();
            string zonesRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(zones);

            if (!String.IsNullOrEmpty(zonesRepresentation))
            {
                string checksum;

                using (SHA256 sha = new SHA256Managed())
                {
                    checksum = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(zonesRepresentation)))
                        .Replace("-", string.Empty);
                }

                string indexes = zones.Select(zone => zone.ZonID.ToString()).Aggregate((indexList, id) => indexList + "," + id);

                return String.Format("{0};{1}", checksum, indexes);
            }

            return String.Empty;
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllZonesChecksum");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllZonesChecksum");
            throw new SCException(ex.Message);
        }
    }

    [WebMethod]
    public byte[] GetAllNodes()
    {
        byte[] serializedData;

        try
        {
            List<GrisNode> nodes = SqlDataBaseUtility.GetNodeListFromDatabase();

            StringBuilder serializedString = new StringBuilder();

            foreach (GrisNode node in nodes)
            {
                serializedString.Append(String.Format("{0}�{1}�{2}�{3}\n", node.NodID.ToString(), node.ZonID.ToString(),
                    node.Name.Replace("�", String.Empty), node.Meters.ToString()));
            }

            serializedData = CompressedBinaryConverter.ToByteArray(serializedString.ToString());
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllNodes");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllNodes");
            throw new SCException(ex.Message);
        }

        return serializedData;
    }

    [WebMethod]
    public string GetAllNodesChecksum()
    {
        try
        {
            List<GrisNode> nodes = SqlDataBaseUtility.GetNodeListFromDatabase();
            string nodesRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(nodes);

            if (!String.IsNullOrEmpty(nodesRepresentation))
            {
                string checksum;

                using (SHA256 sha = new SHA256Managed())
                {
                    checksum = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(nodesRepresentation)))
                        .Replace("-", string.Empty);
                }

                string indexes = nodes.Select(node => node.NodID.ToString()).Aggregate((indexList, id) => indexList + "," + id);

                return String.Format("{0};{1}", checksum, indexes);
            }

            return String.Empty;
        }
        catch (SqlException sqlex)
        {
            this._log.LogSqlErrors(sqlex, "GetAllNodesChecksum");
            throw new SCException(sqlex.Message);
        }
        catch (Exception ex)
        {
            this._log.LogErrors(ex, "GetAllNodesChecksum");
            throw new SCException(ex.Message);
        }
    }

    #endregion
}
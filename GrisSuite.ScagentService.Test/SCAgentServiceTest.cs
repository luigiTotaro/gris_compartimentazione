﻿using GrisSuite.SCAgentService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.IO;
namespace GrisSuite.ScagentService.Test
{
    
    
    /// <summary>
    ///This is a test class for SCAgentServiceTest and is intended
    ///to contain all SCAgentServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SCAgentServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SCAgentService Constructor
        ///</summary>
        [TestMethod()]
        public void SCAgentServiceConstructorTest()
        {
            GrisSuite.SCAgentService.SCAgentService target = new GrisSuite.SCAgentService.SCAgentService();
            
            Encoding e = Encoding.Default;

            string configPath = testContextInstance.TestDeploymentDir + @"\SCAgentService.exe.config";

            if (File.Exists(configPath))
            {
                e = GetFileEncoding(configPath);
                Assert.IsTrue(e == Encoding.Default);
            }
            else
            {
                Assert.Inconclusive("File not found:" + configPath);
            }
            
        }




        public static Encoding GetFileEncoding(string FileName)

        // Return the Encoding of a text file.  Return Encoding.Default if no Unicode
        // BOM (byte order mark) is found.
        {
            Encoding Result = null;

            FileInfo FI = new FileInfo(FileName);

            FileStream FS = null;

            try
            {
                FS = FI.OpenRead();

                Encoding[] UnicodeEncodings = { Encoding.BigEndianUnicode, Encoding.Unicode, Encoding.UTF8 };

                for (int i = 0; Result == null && i < UnicodeEncodings.Length; i++)
                {
                    FS.Position = 0;

                    byte[] Preamble = UnicodeEncodings[i].GetPreamble();

                    bool PreamblesAreEqual = true;

                    for (int j = 0; PreamblesAreEqual && j < Preamble.Length; j++)
                    {
                        PreamblesAreEqual = Preamble[j] == FS.ReadByte();
                    }

                    if (PreamblesAreEqual)
                    {
                        Result = UnicodeEncodings[i];
                    }
                }
            }
            catch (System.IO.IOException)
            {
            }
            finally
            {
                if (FS != null)
                {
                    FS.Close();
                }
            }

            if (Result == null)
            {
                Result = Encoding.Default;
            }

            return Result;
        }

    }
}

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("SPV_ServerMainForm_1.0\SPVServerMainForm.cpp", fServerMain);
USEFORM("SPV_ApplicativeViewForm_1.0\SPVApplicativeViewForm.cpp", fApplicativeView);
USEFORM("SPV_ComProtViewForm_1.0\SPVComProtViewForm.cpp", fComProtView);
USEFORM("SPV_DBViewForm_1.0\SPVDBViewForm.cpp", fDatabaseView);
USEFORM("SPV_PortViewForm_1.0\SPVPortViewForm.cpp", fPortView);
USEFORM("SPV_SystemViewForm_1.0\SPVSystemViewForm.cpp", fSystemView);
USEFORM("SPV_PortMonitorForm_1.0\SPVPortMonitorForm.cpp", fPortMonitor);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		Application->Title = "Supervisor Server 1.0";
		Application->CreateForm(__classid(TfServerMain), &fServerMain);
		Application->CreateForm(__classid(TfApplicativeView), &fApplicativeView);
		Application->CreateForm(__classid(TfComProtView), &fComProtView);
		Application->CreateForm(__classid(TfDatabaseView), &fDatabaseView);
		Application->CreateForm(__classid(TfPortView), &fPortView);
		Application->CreateForm(__classid(TfSystemView), &fSystemView);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------

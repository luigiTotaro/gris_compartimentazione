//==============================================================================
// Telefin Supervisor Server Main Module 1.0
//------------------------------------------------------------------------------
// Modulo principale del server (SPVServerMain.cpp)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	0.19 (21.09.2004 -> 17.12.2007)
//
// Copyright:	2004-2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland C++ Builder 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:        richiede SPVServerMain.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVServerMain.h
//==============================================================================
#pragma hdrstop
#include "SPVServerMain.h"
#include "SPVApplicative.h"
#include "SPVSystem.h"
#include "SPVScheduler.h"
#include "SPVConfig.h"
#include "UtilityLibrary.h"
#include "LogLibrary.h"
#pragma package(smart_init)

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
SPV_SERVERMAIN_STATUS   SPVServerMainStatus;
SPV_EVENTLOG_MANAGER  * SPVServerMainEventLog;
SPV_EVENTLOG_MANAGER  * SPVServerMainDetailedEventLog;

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per inizializzare il modulo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVServerMainInit( void * memo )
{
  int ret_code = SPV_SERVER_MAIN_NO_ERROR;

  SPVEventLogInit();

  // --- Gestione degli EventLog Manager ---
  if ( memo != NULL ) // Se � stato assegnato un oggetto TMemo per l'EventLog
  {
	// --- Creo un nuovo EventLog manager ---
	SPVServerMainEventLog = SPVNewEventLog( SPV_EVENTLOG_TYPE_MEMO, SPV_EVENTLOG_TEMPLATE_NONE, memo );
	if ( SPVServerMainEventLog != NULL )
	{
	  // --- Apro l'EventLog Manager creato ---
	  ret_code = SPVOpenEventLog( SPVServerMainEventLog->ID );
	}
  }
  else
  {
	SPVServerMainEventLog = NULL;
  }

  // --- Inizializzo lo stato generale del server ---
  memset( &SPVServerMainStatus, 0, sizeof(SPV_SERVERMAIN_STATUS) );

  //ULCreateLogFile();
  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVServerMainClear( void )
{
  int ret_code = SPV_SERVER_MAIN_NO_ERROR;
  //ULDestroyLogFile();

  // --- Chiudo e cancello l'EventLog Manager ---
  if ( SPVServerMainEventLog != NULL )
  {
	if ( SPVServerMainEventLog->Opened )
	{
	  ret_code = SPVCloseEventLog( SPVServerMainEventLog->ID );
	}
	ret_code = SPVDeleteEventLog( SPVServerMainEventLog->ID );
	SPVServerMainEventLog = NULL;
  }
  else
  {
	ret_code = SPV_SERVER_MAIN_INVALID_EVENTLOG;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per caricare il server
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [26.08.2014]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.02
//------------------------------------------------------------------------------
int SPVServerMainLoad( void )
{
	int						ret_code        = SPV_SERVER_MAIN_NO_ERROR;
	SPV_DEVICE *			device          = NULL; // Ptr a struttura periferica
	XML_ELEMENT *			e_comprot       = NULL; // Elemento XML dati protocollo
	SPV_COMPROT_TYPE *		comprot         = NULL; // Ptr a struttura protocollo
	XML_INT_FUNCTION *		function        = NULL; // Ptr a struttura funzione
	XML_INT_FUNCTION_PTR *		ptf             = NULL; // Ptr a funzione
	XML_ELEMENT *			telefin_def     = NULL; // Elemento XML dati Telefin
	XML_ELEMENT *			topography_def	= NULL; // Elemento XML dati topografici
	XML_ELEMENT *			system_def      = NULL; // Elemento XML dati di sistema
	XML_ELEMENT *			port_def        = NULL; // Elemento XML dati delle porte
	char *			    	xml_system_file = NULL;

        bool logEnabled 				= SYSSetKeyDebugLogEnabled();

        if (logEnabled == true) {
		LLCreateLogFile();
        }

        if (logEnabled == true) LLAddLogFile( "SPVServerMainLoad..." );

	// --- Inizializzazione della lista dei lock ---
	SYSInitLockList(100);

      	if (logEnabled == true) LLAddLogFile( "SYSInitLockList ESEGUITO" );

	// --- Inizializzo il modulo Interfaccia XML ---
	XMLInterfaceInit();

        if (logEnabled == true) LLAddLogFile( "XMLInterfaceInit ESEGUITO" );

	// --- Inizializzazione Modulo di configurazione ---
	SPVConfigInit( );

        if (logEnabled == true) LLAddLogFile( "SPVConfigInit ESEGUITO" );

	// --- Inizializzazione della lista dei thread ---
	SYSInitThreadList(1000);

        if (logEnabled == true) LLAddLogFile( "SYSInitThreadList ESEGUITO" );

	// --- Inizializzazione Modulo Applicativo ---
	SPVApplicativeInit( );

        if (logEnabled == true) LLAddLogFile( "SPVApplicativeInit ESEGUITO" );

	// --- Inizializzazione Modulo di sistema ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento configurazione server..." );
	}

        if (logEnabled == true) LLAddLogFile( "Caricamento configurazione server..." );

	SPVSystemInit( );

	// --- Caricamento configurazione generale ---
	ret_code = SPVLoadConfig( );
	if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Configurazione caricata correttamente" );
		}

                if (logEnabled == true) LLAddLogFile( "Configurazione caricata correttamente" );
	}
	else
	{
		char * error = XMLType2ASCII( &ret_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante il caricamento della configurazione. Codice errore: ", error ) );
		}

                if (logEnabled == true) LLAddLogFile( strcat( "Errore durante il caricamento della configurazione. Codice errore: ", error ) );

		free( error );
		ret_code = SPV_SERVER_MAIN_NO_ERROR;


	}

	// --- Inizializzazione Sistema ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento definizione XML..." );
	}

        if (logEnabled == true) LLAddLogFile( "Caricamento definizione XML..." );

	//*
	if (logEnabled == true) {
		//LLCreateLogFile();
		LLAddLogFile( "1: Log recupero path system.xml abilitato" );
	}
	//*/
	xml_system_file = SPVConfigInfo.Path.XMLSystemPath;
	if (logEnabled == true) LLAddLogFile( xml_system_file );
	if ( xml_system_file != NULL ){
		if (logEnabled == true) LLAddLogFile( "3: File system.xml recuperato da config.xml o registro" );
		SPVSystemSource = XMLCreate( xml_system_file ); // -MALLOC
	}
	else{
		if (logEnabled == true) LLAddLogFile( "File system.xml recuperato da default define" );
		xml_system_file = SPVGetPath( SPV_CFG_XML_SYSTEM_DEFAULT );
		SPVSystemSource = XMLCreate( xml_system_file ); // -MALLOC
	}
	ret_code = SPVLoadSystemSource( SPVSystemSource ); // Creo una nuova struttura per il sistema
	if ( ret_code == SPV_SERVER_MAIN_NO_ERROR ) // Se non ci sono stati errori
	{
		if (logEnabled == true) LLAddLogFile( "Caricato il file system.xml" );
		telefin_def = SPVSystemSource->root_element; // Recupero l'elemento root del sistema
		if ( telefin_def != NULL ) // Se l'elemento XML principale � valido
		{
			// --- Inizializzazione porte ---
			port_def = XMLGetNext( telefin_def->child, "port", -1 ); // Recupero l'elemento delle porte
			if ( port_def != NULL ) // Se l'elemento XML delle porte � valido
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento porte di comunicazione..." );
				}

                                if (logEnabled == true) LLAddLogFile( "Caricamento porte di comunicazione..." );

				ret_code = SPVLoadPorts( port_def ); // Carico le porte di comunicazione
				if ( ret_code == SPV_SERVER_MAIN_NO_ERROR ) // Se non ci sono stati errori
				{
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricate porte" );
					}

                                        if (logEnabled == true) LLAddLogFile( "Caricate porte" );
				}
				else
				{
					char * error = XMLType2ASCII( &ret_code, t_u_int_32 ); // -MALLOC
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Porte non caricate. Codice errore: ", error ) );
					}

                                        if (logEnabled == true) LLAddLogFile( strcat( "Porte non caricate. Codice errore: ", error ) );

					free( error ); // -FREE
				}
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, "Apertura porte..." );
				}
				ret_code = SPVOpenPorts( ); // Apro le porte di comunicazione
				if ( ret_code == SPV_SERVER_MAIN_NO_ERROR ) // Se non ci sono stati errori
				{
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, "Porte aperte" );
					}
                                        
                                        if (logEnabled == true) LLAddLogFile( "Porte aperte" );
				}
				else
				{
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, "Porte non aperte" );
					}

                                        if (logEnabled == true) LLAddLogFile( "Porte non aperte" );
				}
			}
			else
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, "Definizione porte non trovata" );
				}

                                if (logEnabled == true) LLAddLogFile( "Definizione porte non trovata" );
			}

			// --- Inizializzazione periferiche ---
			topography_def 	= XMLGetNext( telefin_def->child, "topography", -1 ); // Recupero l'elemento topografia
			system_def 		= XMLGetNext( telefin_def->child, "system", -1 ); // Recupero l'elemento di sistema
			if ( system_def != NULL ) // Se l'elemento XML del sistema � valido
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID,"Caricamento sistema..." );
				}

                                if (logEnabled == true) LLAddLogFile( "Caricamento sistema..." );

				bool multi_language = SPVConfigIsMultiLanguage();
				ret_code = SPVLoadSystem( &SPVSystem, system_def, topography_def, multi_language ); // Carico il sistema

				// --- Caso connessione database non disponibile ---
				if ( ret_code == SPV_SYSTEM_DB_OPEN_FAILURE )
				{
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, "Connessione al database fallita." );
					}

                                        if (logEnabled == true) LLAddLogFile( "Connessione al database fallita." );

					ret_code = SPV_SERVER_MAIN_NO_ERROR;
				}
				if ( ret_code == SPV_SERVER_MAIN_NO_ERROR || ret_code == SPV_SYSTEM_DB_OPEN_FAILURE )
				{
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, "Sistema caricato." );
					}

                                        if (logEnabled == true) LLAddLogFile( "Sistema caricato." );

					// --- Ciclo di inclusione file XML ---
					while ( ret_code == SPV_SERVER_MAIN_NO_ERROR && XMLGetFirst( SPVSystemSource, "include" ) != NULL )
					{
						if ( SPVServerMainEventLog != NULL )
						{
							SPVPrintEventLog( SPVServerMainEventLog->ID, "Inclusione di sorgente XML..." );
						}

                                                if (logEnabled == true) LLAddLogFile( "Inclusione di sorgente XML..." );

						ret_code = XMLProcInc( SPVSystemSource ); // Processo l'inclusione del file XML
					}
					if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
					{
						if ( SPVServerMainEventLog != NULL )
						{
							SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento definizione dati periferiche..." );
						}

                                                if (logEnabled == true) LLAddLogFile( "Caricamento definizione dati periferiche..." );

						ret_code = SPVLoadSystemStreams( &SPVSystem ); // Carico gli stream del sistema
						if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
						{
							if ( SPVServerMainEventLog != NULL )
							{
								SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento definizione dati completata." );
								SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento protocollo di comunicazione..." );
							}

                                                        if (logEnabled == true) LLAddLogFile( "Caricamento definizione dati completata." );
                                                        if (logEnabled == true) LLAddLogFile( "Caricamento protocollo di comunicazione..." );

							ret_code = SPVLoadSystemComProt( &SPVSystem ); // Carico i protocolli del sistema
							if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
							{
								if ( SPVServerMainEventLog != NULL )
								{
									SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento protocollo di comunicazione riuscito." );
									SPVPrintEventLog( SPVServerMainEventLog->ID, "Assegnazione porte..." );
								}

                                                                if (logEnabled == true) LLAddLogFile( "Caricamento protocollo di comunicazione riuscito." );
                                                                if (logEnabled == true) LLAddLogFile( "Assegnazione porte..." );

								ret_code = SPVAssignSystemPorts( &SPVSystem ); // Assegno le porte del sistema alle periferiche
								if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
								{
									if ( SPVServerMainEventLog != NULL )
									{
										SPVPrintEventLog( SPVServerMainEventLog->ID, "Assegnazione porte riuscita." );
										SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento procedure..." );
									}

                                                                        if (logEnabled == true) LLAddLogFile( "Assegnazione porte riuscita." );
                                                                        if (logEnabled == true) LLAddLogFile( "Caricamento procedure..." );

									ret_code = SPVLoadSystemProcedures( &SPVSystem ); // Carico le procedure del sistema
									if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
									{
										if ( SPVServerMainEventLog != NULL )
										{
											SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento procedure completato." );
										}

                                                                                if (logEnabled == true) LLAddLogFile( "Caricamento procedure completato." );
										/* TEMPORANEO
										SPV_DEVICE *device = &SPVSystem.ServerList[0].RegionList[0].ZoneList[0].NodeList[0].DeviceList[0];
										TfPortMonitor * monitor  = new TfPortMonitor(NULL);
										SPV_EVENTLOG_MANAGER * event_log = SPVNewEventLog(SPV_EVENTLOG_TYPE_MEMO,SPV_EVENTLOG_TEMPLATE_COMPORT,monitor->mLog);
										SPVOpenEventLog(event_log->ID);
										((SYS_PORT*)device->SupCfg.Port)->Monitor = monitor;
										((SYS_PORT*)device->SupCfg.Port)->EventLog = event_log;
										//*/
									}
									else
									{
										if ( SPVServerMainEventLog != NULL )
										{
											SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento procedure fallito." );
										}

                                                                                if (logEnabled == true) LLAddLogFile( "Caricamento procedure fallito." );
									}
								}
								else
								{
									if ( SPVServerMainEventLog != NULL )
									{
										SPVPrintEventLog( SPVServerMainEventLog->ID, "Assegnazione porte fallita." );
									}

                                                                        if (logEnabled == true) LLAddLogFile( "Assegnazione porte fallita." );
								}
							}
							else
							{
								if ( SPVServerMainEventLog != NULL )
								{
									SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento protocollo di comunicazione fallito." );
								}

                                                                if (logEnabled == true) LLAddLogFile( "Caricamento protocollo di comunicazione fallito." );
							}
						}
						else
						{
							if ( SPVServerMainEventLog != NULL )
							{
								SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento definizione dati fallita." );
							}

                                                        if (logEnabled == true) LLAddLogFile( "Caricamento definizione dati fallita." );
						}
					}
					else
					{
						if ( SPVServerMainEventLog != NULL )
						{
							SPVPrintEventLog( SPVServerMainEventLog->ID, "Inclusione di sorgenti XML fallita." );
						}

                                                if (logEnabled == true) LLAddLogFile( "Inclusione di sorgenti XML fallita." );
					}
				}
				else
				{
					if ( SPVServerMainEventLog != NULL )
					{
						SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Caricamento sistema fallito. Codice errore: ", XMLType2ASCII( &ret_code, t_u_int_32 ) ) );
					}

                                        if (logEnabled == true) LLAddLogFile( strcat( "Caricamento sistema fallito. Codice errore: ", XMLType2ASCII( &ret_code, t_u_int_32 ) ) );
				}
			}

			/*//SPVSTD-3//
			// --- Invocazione della Stored Procedure che procede alla pulizia dei campi marcati come removed ---
			if ( SPVServerMainEventLog != NULL )
			{
				SPVPrintEventLog( SPVServerMainEventLog->ID, "Esecuzione pulizia record non utilizzati ..." );
			}
			ret_code = SPVDeleteUnusedRows( &SPVSystem );
			if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, "Pulizia effettuata" );
				}
			}
			else
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Pulizia non effettuata. Codice errore: ",XMLType2ASCII( &ret_code, t_u_int_32 ) ) );
				}
			}
			//*/

			// --- Inizializzazione schedulatore ---
			if ( SPVServerMainEventLog != NULL )
			{
				SPVPrintEventLog( SPVServerMainEventLog->ID, "Caricamento schedulatore..." );
			}

                        if (logEnabled == true) LLAddLogFile( "Caricamento schedulatore." );


			ret_code = SPVSchedulerInit( SPVSystem.Def );
			if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, "Schedulatore caricato" );
				}
                                
                                if (logEnabled == true) LLAddLogFile( "Schedulatore caricato" );
			}
			else
			{
				if ( SPVServerMainEventLog != NULL )
				{
					SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Schedulatore non caricato. Codice errore: ",XMLType2ASCII( &ret_code, t_u_int_32 ) ) );
				}

                                if (logEnabled == true) LLAddLogFile( strcat( "Schedulatore non caricato. Codice errore: ",XMLType2ASCII( &ret_code, t_u_int_32 ) ) );
			}
		}
	}
	else{
		if (logEnabled == true) LLAddLogFile( "File system.xml non caricato" );

                if (logEnabled == true) LLAddLogFile( "File system.xml non caricato" );
	}

	//if (logEnabled == true) LLDestroyLogFile( );

        SPVProcedureCheckInit( ); // SPERIMENTALE FABIANO         

	return ret_code;
}

//==============================================================================
/// Funzione per scaricare il server
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [24.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.11
//------------------------------------------------------------------------------
int SPVServerMainUnload( void )
{
	int     ret_code = SPV_SERVER_MAIN_NO_ERROR; // Codice di ritorno
	int     fun_code = SPV_SERVER_MAIN_NO_ERROR; // Codice di errore funzione
	char  * str_code = NULL;

	// --- Chiudo il modulo di schedulazione ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Chiusura schedulatore..." );
	}
	fun_code = SPVSchedulerClear( );

	if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Schedulatore chiuso" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante la chiusura dello schedulatore. Codice errore: ", str_code ) );
		}
		if ( str_code != NULL ) free( str_code );
	}

	// --- Chiusura e cancellazione Thread delle procedure
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Terminazione thread delle procedure..." );
	}
	fun_code = SPVClearProcedureThreads( &SPVSystem );
	if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Thread delle Procedure eliminati" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante l\'eliminazione dei thread delle procedure. Codice errore: ", str_code ) );
		}
    	if ( str_code != NULL ) free( str_code );
    }

    // --- Chiudo le procedure di sistema ---
    if ( SPVServerMainEventLog != NULL )
    {
    	SPVPrintEventLog( SPVServerMainEventLog->ID, "Chiusura procedure..." );
    }
    fun_code = SPVFreeSystemProcedures( &SPVSystem );
    if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
    {
    	if ( SPVServerMainEventLog != NULL )
    	{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Procedure chiuse" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante la chiusura delle procedure. Codice errore: ", str_code ) );
		}
		if ( str_code != NULL ) free( str_code );
	}

	// --- Chiudo i protocolli di comunicazione ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Chiusura protocolli di comunicazione..." );
	}
	fun_code = SPVFreeSystemComProt( &SPVSystem );
	if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Protocolli di comunicazione chiusi" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante la chiusura dei protocolli di comunicazioni. Codice errore: ", str_code ) );
		}
		if ( str_code != NULL ) free( str_code );
	}

	// --- Chiudo i dati delle periferiche ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Chiusura dati periferiche..." );
	}
	fun_code = SPVFreeSystemStreams( &SPVSystem );
	if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Dati periferiche chiusi" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante la chiusura dei dati periferiche. Codice errore: ", str_code ) );
		}
		if ( str_code != NULL ) free( str_code );
	}
	//*/
	//*
	// --- Chiudo la struttura del sistema ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Chiusura struttura sistema..." );
	}
	fun_code = SPVFreeSystem( &SPVSystem );
	if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Struttura sistema chiusa" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante la chiusura della struttura sistema. Codice errore: ", str_code ) );
		}
		if ( str_code != NULL ) free( str_code );
	}
	//*/

	//*
	// --- Questa cosa dovrei farla in SPVComProt ---
	for ( int p = 0; p < SPVPortCount; p++ )
	{
		fun_code = SYSClosePort( SPVPortList[p].Port );
	}
	//*/

	ret_code = SPVFreePorts( );

	//*/ --- Cancello la struttura XML di definizione del sistema ---
	if ( SPVServerMainEventLog != NULL )
	{
		SPVPrintEventLog( SPVServerMainEventLog->ID, "Chiusura definizione sistema..." );
	}
	fun_code = SPVFreeSystemSource( SPVSystemSource );
	if ( fun_code == SPV_SERVER_MAIN_NO_ERROR )
	{
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, "Definizione chiusa" );
		}
	}
	else
	{
		str_code = XMLType2ASCII( &fun_code, t_u_int_32 );
		if ( SPVServerMainEventLog != NULL )
		{
			SPVPrintEventLog( SPVServerMainEventLog->ID, strcat( "Errore durante la chiusura della definizione. Codice errore: ", str_code ) );
		}
		if ( str_code != NULL ) free( str_code );
	}
	//*/

	// --- Chiudo il modulo di sistema ---
	SPVSystemClear( );

	// --- Chiudo il modulo applicativo ---
	fun_code = SPVApplicativeClear( );

	// --- Cancello la lista dei thread ---
	SYSClearThreadList();

	// --- Cancello la lista dei lock ---
	SYSClearLockList();

	// --- Chiudo il modulo di Configurazione ---
	SPVConfigClear( );

	// --- Chiudo il modulo di interfacciamento XML ---
	XMLInterfaceClear( );

	return ret_code;
}

//==============================================================================
/// Funzione per avviare il server
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [17.12.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
int SPVServerMainStart( void )
{
  int ret_code = SPV_SERVER_MAIN_NO_ERROR;

  ret_code = SPVStartScheduler( );

  if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
  {
	if ( SPVServerMainEventLog != NULL )
	{
	  SPVPrintEventLog( SPVServerMainEventLog->ID, "Schedulatore del server avviato." );
	}

	SPVServerMainStatus.scheduling = true;
  }
  else
      SPVPrintEventLog( SPVServerMainEventLog->ID, "Schedulatore del server NON avviato." );

  return ret_code;
}

//==============================================================================
/// Funzione per arrestare il server
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [08.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVServerMainStop( void )
{
  int ret_code = SPV_SERVER_MAIN_NO_ERROR;

  ret_code = SPVStopScheduler( );

  if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
  {
	if ( SPVServerMainEventLog != NULL )
	{
	  SPVPrintEventLog( SPVServerMainEventLog->ID, "Schedulatore del server arrestato." );
	}
	SPVServerMainStatus.scheduling = false;
  }

  return ret_code;
}



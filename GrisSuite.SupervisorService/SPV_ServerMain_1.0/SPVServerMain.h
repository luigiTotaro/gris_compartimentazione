//==============================================================================
// Telefin Supervisor Server Main Module 1.0
//------------------------------------------------------------------------------
// Header Modulo principale del server (SPVServerMain.h)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	0.19 (21.09.2004 -> 17.12.2007)
//
// Copyright:	2004-2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland C++ Builder 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVServerMain.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [21.09.2004]:
// - Prima versione del modulo.
// 0.02 [14.01.2005]:
// - Aggiunti alcuni codici di errore.
// - Modificata la funzione SPVStopServer.
// 0.03 [26.01.2005]:
// - Modificata la funzione SPVStartServer.
// 0.04 [01.02.2005]:
// - Modificata la funzione SPVLoadServer.
// 0.05 [26.05.2005]:
// - Modificata la funzione SPVClearServer.
// 0.06 [27.06.2005]:
// - Modificata la funzione SPVStartServer.
// - Modificata la funzione SPVStopServer.
// 0.07 [07.07.2005]:
// - Modificata la funzione SPVServerMainInit.
// - Modificata la funzione SPVLoadServer.
// - Modificata la funzione SPVStartServer.
// - Modificata la funzione SPVStopServer.
// - Aggiunta la definizione del tipo SPV_SERVERMAIN_STATUS.
// 0.08 [29.08.2005]:
// - Modificata la funzione SPVClearServer.
// - Rinominata la funzione SPVClearServer in SPVUnloadServer.
// - Aggiunta la funzione SPVServerMainClear.
// 0.09 [30.08.2005]:
// - Modificata la funzione SPVLoadServer.
// - Modificata la funzione SPVUnloadServer.
// 0.10 [31.08.2005]:
// - Modificata la funzione SPVUnloadServer.
// 0.11 [06.09.2005]:
// - Modificata la funzione SPVLoadServer.
// - Modificata la funzione SPVUnloadServer.
// 0.12 [15.09.2005]:
// - Modificata la funzione SPVLoadServer.
// 0.13 [19.12.2005]:
// - Modificata la funzione SPVServerMainInit.
// - Modificata la funzione SPVServerMainClear.
// - Modificata la funzione SPVLoadServer.
// - Modificata la funzione SPVUnloadServer.
// - Modificata la funzione SPVStartServer.
// - Modificata la funzione SPVStopServer.
// 0.14 [20.07.2006]:
// - Modificata la funzione SPVLoadServer.
// 0.15 [31.07.2007]:
// - Corretta la funzione SPVUnloadServer().
// 0.16 [08.10.2007]
//- Cambiato nome di SPVLoadServer in SPVServerMainLoad
//- Cambiato nome di SPVUnloadServer in SPVServerMainUnload
//- Cambiato nome di SPVStartServer in SPVServerMainStart
//- Cambiato nome di SPVStopServer in SPVServerMainStop
// 0.17 [25.10.2007]
//- Modificata funzione SPVServerMainLoad per eliminare i memory leak
// 0.18 [30.11.2007]
//- Modificata funzione SPVServerMainLoad
// 0.19 [17.12.2007]
//- Modificata funzione SPVServerMainStart
// 0.20 [xx.xx.2008]
//- Modificata funzione SPVServerMainLoad
//==============================================================================
#ifndef SPVServerMainH
#define SPVServerMainH
#include "SPVEventLog.h"

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_SERVER_MAIN_NO_ERROR          0
#define SPV_SERVER_MAIN_INVALID_EVENTLOG  27001

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------



//==============================================================================
/// Struttura dati per lo stato del server di supervisione
///
/// \date [07.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_SERVERMAIN_STATUS
{
  int   status          ;
  bool  ready           ;
  bool  scheduling      ;
  bool  config_ok       ;
  bool  system_ok       ;
  bool  db_ok           ;
  bool  ports_ok        ;
  int   last_error_code ;
}
SPV_SERVERMAIN_STATUS;

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

/// Funzione per inizializzare il modulo
int SPVServerMainInit( void * memo );
/// Funzione per chiudere il modulo
int SPVServerMainClear( void );
/// Funzione per caricare il server
int SPVServerMainLoad( void );
/// Funzione per scaricare il server
int SPVServerMainUnload( void );
/// Funzione per avviare il server
int SPVServerMainStart( void );
/// Funzione per arrestare il server
int SPVServerMainStop( void );

//==============================================================================
// Variabili globali esterne
//------------------------------------------------------------------------------
extern SPV_SERVERMAIN_STATUS    SPVServerMainStatus   ;
extern SPV_EVENTLOG_MANAGER   * SPVServerMainEventLog ;

#endif

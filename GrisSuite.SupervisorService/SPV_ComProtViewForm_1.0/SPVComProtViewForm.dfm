object fComProtView: TfComProtView
  Left = 363
  Top = 319
  Width = 640
  Height = 200
  Caption = 'Finestra Protocolli di Comunicazione'
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  DesignSize = (
    632
    166)
  PixelsPerInch = 96
  TextHeight = 13
  object gbStatus: TGroupBox
    Left = 4
    Top = 4
    Width = 623
    Height = 140
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Elenco dei comandi in esecuzione'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      623
      140)
    object imgStatus: TImage
      Left = 10
      Top = 16
      Width = 16
      Height = 16
      Transparent = True
    end
    object lComProtNum: TLabel
      Left = 440
      Top = 16
      Width = 175
      Height = 16
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = 'Numero comandi attivi: n/d'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = cl3DDkShadow
      Font.Height = -11
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lStatus: TLabel
      Left = 30
      Top = 16
      Width = 82
      Height = 18
      Caption = 'Comandi attivi'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object pBluLineStatus: TPanel
      Left = 2
      Top = 39
      Width = 619
      Height = 3
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      Color = clSkyBlue
      TabOrder = 0
    end
    object sgCommands: TStringGrid
      Left = 2
      Top = 42
      Width = 619
      Height = 96
      TabStop = False
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsNone
      Color = cl3DLight
      ColCount = 9
      DefaultRowHeight = 17
      FixedCols = 0
      RowCount = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridLineWidth = 0
      Options = [goFixedVertLine, goFixedHorzLine, goDrawFocusSelected, goColSizing, goRowSelect]
      ParentFont = False
      PopupMenu = popComProt
      ScrollBars = ssVertical
      TabOrder = 1
      ColWidths = (
        102
        76
        120
        64
        64
        64
        64
        64
        64)
    end
  end
  object sbCommands: TStatusBar
    Left = 0
    Top = 147
    Width = 632
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object popComProt: TPopupMenu
    OnPopup = popComProtPopup
    Left = 126
    Top = 42
    object miKill: TMenuItem
      Caption = 'Annulla Comando'
      OnClick = miKillClick
    end
  end
  object tComProtView: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tComProtViewTimer
    Left = 160
    Top = 42
  end
end

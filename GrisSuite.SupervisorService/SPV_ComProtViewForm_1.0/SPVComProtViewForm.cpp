//==============================================================================
// Telefin Supervisor Communication Protocol View Form 1.0
//------------------------------------------------------------------------------
// Form dei protocolli di comunicazione (SPVComProtViewForm.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.12 (30.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVComProtViewForm.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVComProtViewForm.h
//==============================================================================

#include <vcl.h>
#pragma hdrstop

#include "SPVComProtViewForm.h"
#include "SPVComProt.h"
#include "SPVServerGUI.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
TfComProtView * fComProtView;

//==============================================================================
/// Metodo costruttore della classe TfComProtView
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
__fastcall TfComProtView::TfComProtView( TComponent * Owner ):TForm( Owner )
{
  this->gbStatus->Caption = AnsiString( "Elenco degli ultimi ") + AnsiString( SPV_COMPROT_MAX_THREAD ) + AnsiString(" comandi di comunicazione");

  // --- Dimensioni form ---
  this->Left    = 320;
  this->Top     = 600;
  this->Width   = 640;
  this->Height  = 200;
  // --- Colori form ---
  this->Color   = (TColor)0x00D8E9EC;

  // --- Dimensioni gbStatus ---
  this->gbStatus->Left        = 4;
  this->gbStatus->Top         = 4;
  this->gbStatus->Width       = 623;
  this->gbStatus->Height      = 140;
  // --- Colori gbStatus ---
  this->gbStatus->Color       = (TColor)0x00D8E9EC;
  // --- Font gbStatus ---
  this->gbStatus->Font->Color = (TColor)0x00C56A31;
  this->gbStatus->Font->Name  = "MS Sans Serif";
  this->gbStatus->Font->Size  = 8;

  // --- Dimensioni lStatus ---
  this->lStatus->Left         = 30;
  this->lStatus->Top          = 16;
  this->lStatus->Width        = 143;
  this->lStatus->Height       = 18;
  // --- Colori lStatus ---
  this->lStatus->Color        = (TColor)0x00D8E9EC;
  // --- Font lStatus ---
  this->lStatus->Font->Color  = (TColor)0x00000000;
  this->lStatus->Font->Name   = "Trebuchet MS";
  this->lStatus->Font->Size   = 8;

  // --- Dimensioni lCOMNum ---
  this->lComProtNum->Left          = 365;
  this->lComProtNum->Top           = 16;
  this->lComProtNum->Width         = 250;
  this->lComProtNum->Height        = 16;
  // --- Colori lCOMNum ---
  this->lComProtNum->Color         = (TColor)0x00D8E9EC;
  // --- Font lCOMNum ---
  this->lComProtNum->Font->Color   = (TColor)0x00888888;
  this->lComProtNum->Font->Name    = "Trebuchet MS";
  this->lComProtNum->Font->Size    = 8;

  // --- Dimensioni pBluLineStatus ---
  this->pBluLineStatus->Left   = 2;
  this->pBluLineStatus->Top    = 39;
  this->pBluLineStatus->Width  = 619;
  this->pBluLineStatus->Height = 3;
  // --- Colori pBluLineStatus ---
  this->pBluLineStatus->Color  = (TColor)0x00C56A31;

  // --- Dimensioni sgCommands ---
  this->sgCommands->Left   = 2;
  this->sgCommands->Top    = 42;
  this->sgCommands->Width  = 619;
  this->sgCommands->Height = 96;
  // --- Colori sgCommands ---
  this->sgCommands->Color       = (TColor)0x00CCFFFF;
  // --- Font sgCommands ---
  this->sgCommands->Font->Color = (TColor)0x00000000;
  this->sgCommands->Font->Name  = "MS Sans Serif";
  this->sgCommands->Font->Size  = 8;

  // --- Colori sbCommands ---
  this->sbCommands->Color       = (TColor)0x00D8E9EC;
  // --- Font sbCommands ---
  this->sbCommands->Font->Color = (TColor)0x00000000;
  this->sbCommands->Font->Name  = "MS Sans Serif";
  this->sbCommands->Font->Size  = 8;



  // --- Inizializzo la tabella stato comandi ---
  this->ClearAllCommandsStatus();
}

//==============================================================================
/// Metodo che cattura l'evento Resize del form
///
/// \date [14.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfComProtView::FormResize( TObject * Sender )
{
  // --- Ridimensionamento automatico della tabella comandi ---
  SPVTableAutoSize( (void*)this->sgCommands );

  this->ViewAllCommandsStatus();
}

//==============================================================================
/// Metodo per visualizzare lo stato di tutti i comandi di protocollo
///
/// \date [22.03.2006]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int __fastcall TfComProtView::ClearAllCommandsStatus( void )
{
  int ret_code = SPV_COMPROTVIEW_NO_ERROR;

  if ( this->sgCommands != NULL )
  {
    // --- Inizializzo la tabella dei comandi ---
    this->sgCommands->ColCount    = 9;
    this->sgCommands->RowCount    = 2;
    this->sgCommands->Cells[0][0] = "ID";
    this->sgCommands->Cells[1][0] = "Periferica";
    this->sgCommands->Cells[2][0] = "Porta";
    this->sgCommands->Cells[3][0] = "Comando";
    this->sgCommands->Cells[4][0] = "Stato";
    this->sgCommands->Cells[5][0] = "Step";
    this->sgCommands->Cells[6][0] = AnsiString( "Stato Step" );
    this->sgCommands->Cells[7][0] = AnsiString( "Inizio Step" );
    this->sgCommands->Cells[8][0] = AnsiString( "Fine Step" );
    this->sgCommands->Cells[0][1] = "<Nessun comando>";
    this->sgCommands->Cells[1][1] = "            ";
    this->sgCommands->Cells[2][1] = "            ";
    this->sgCommands->Cells[3][1] = "                      ";
    this->sgCommands->Cells[4][1] = "                ";
    this->sgCommands->Cells[5][1] = "                      ";
    this->sgCommands->Cells[6][1] = "                      ";
    this->sgCommands->Cells[7][1] = "                      ";
    this->sgCommands->Cells[8][0] = "                      ";
  }
  else
  {
    ret_code = SPV_COMPROTVIEW_INVALID_STRING_GRID;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di un comando di protocollo
///
/// \date [22.03.2006]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int __fastcall TfComProtView::ViewCommandStatus( int pos )
{
  int                   ret_code        = SPV_COMPROTVIEW_NO_ERROR;
  SPV_COMPROT_THREAD  * command_thread  = NULL;
  char                * char_value      = NULL;
  SPV_DEVICE          * p_device        = NULL;
  SYS_PORT            * p_port          = NULL;

  AnsiString            name            = "n/d";
  AnsiString            id              = "n/d";
  AnsiString            device          = "n/d";
  AnsiString            port            = "n/d";
  AnsiString            type            = "n/d";
  AnsiString            step            = "n/d";
  AnsiString            status          = "n/d";
  AnsiString            last_code       = "n/d";
  AnsiString            step_status     = "n/d";
  AnsiString            step_start      = "n/d";
  AnsiString            step_finish     = "n/d";

  try
  {
    if ( pos >= 0 && pos < SPV_COMPROT_MAX_THREAD )
    {
      // --- Entro nella sezione critica ---
      SPVEnterThreadData();

      command_thread = &SPVComProtThreadList[pos];
      if ( command_thread != NULL )
      {
        if ( command_thread->ID > 0 )
        {
          // --- Preparo le stringhe dello stato da inserire in tabella ---
          name    = AnsiString( command_thread->Type->CommandList[command_thread->Command].Name  )
                  + AnsiString( " (" ) + AnsiString( command_thread->Command ) + AnsiString( ")" );
          id      = AnsiString( command_thread->ID          );
          type    = AnsiString( command_thread->Command     );
          last_code = AnsiString( command_thread->FinishCode );
          step    = AnsiString( command_thread->Type->CommandList[command_thread->Command].StepList[command_thread->Step].Name )
                  + AnsiString( " (" ) + AnsiString( command_thread->Step ) + AnsiString( ")" );

          p_device = command_thread->Device;
          if ( p_device != NULL )
          {
            device  = AnsiString( p_device->Name )
                    + AnsiString( " (" ) + AnsiString( p_device->ID  ) + AnsiString( ")" );
          }
          p_port = command_thread->Port;
          if ( p_port != NULL )
          {
            port    = AnsiString( p_port->Name  )
                    + AnsiString( " (" ) + AnsiString( p_port->ID ) + AnsiString( ")" );
          }


          if ( command_thread->StepStart > 0 )
          {
            char_value = XMLType2ASCII( &command_thread->StepStart, t_datetime ); // -MALLOC
            if ( char_value != NULL )
            {
              step_start = AnsiString( char_value );
              free( char_value ); // -FREE
            }
          }
          if ( command_thread->StepFinish > 0 )
          {
            char_value = XMLType2ASCII( &command_thread->StepFinish, t_datetime ); // -MALLOC
            if ( char_value != NULL )
            {
              step_finish = AnsiString( char_value );
              free( char_value ); // -FREE
            }
          }

          if ( command_thread->Active )
          {
            if ( command_thread->Type->CommandList[command_thread->Command].StepList[command_thread->Step].Type == 0 )
            {
              status = AnsiString( "In trasmissione" );
            }
            else
            {
              status = AnsiString( "In ricezione" );
            }
          }
          else if ( command_thread->Waiting )
          {
            status = AnsiString( "In attesa" );
          }
          else
          {
            if ( command_thread->FinishCode == SPV_COMPROT_THREAD_OK )
            {
              status = AnsiString( "Riuscito" );
            }
            else if ( command_thread->FinishCode == SPV_COMPROT_THREAD_FAILURE )
            {
              status  = AnsiString( "Fallito" );
              if ( command_thread->FailureStep >= 0 )
              {
                status  = status + AnsiString( " - " )
                        + AnsiString( command_thread->FailureCode ) + AnsiString( "@" )
                        + AnsiString( command_thread->Type->CommandList[command_thread->Command].StepList[command_thread->FailureStep].Name )
                        + AnsiString( " (" ) + AnsiString( command_thread->FailureStep ) + AnsiString( ")" );
              }
            }
            else if ( command_thread->FinishCode == SPV_COMPROT_THREAD_KILLED )
            {
              status = AnsiString( "Annullato" );
            }
            else if ( command_thread->FinishCode == SPV_COMPROT_THREAD_ABORT )
            {
              status = AnsiString( "Abortito" );
            }
            else
            {
              status = AnsiString( "Pronto" );
            }
          }
          status = AnsiString( command_thread->CmdRetryCount + 1 ) + AnsiString( ": " ) + status; 

          switch ( command_thread->StepRetCode )
          {
            case SPV_COMPROT_STEP_UNKNOWN:
              step_status = AnsiString( "Sconosciuto" );
            break;
            case SPV_COMPROT_STEP_IDLE:
              step_status = AnsiString( "Fermo" );
            break;
            case SPV_COMPROT_STEP_START:
              step_status = AnsiString( "Avviato" );
            break;
            case SPV_COMPROT_STEP_FAILURE:
              step_status = AnsiString( "Fallito" );
            break;
            case SPV_COMPROT_STEP_READ_FIFO_START:
              step_status = AnsiString( "Lettura FIFO avviata" );
            break;
            case SPV_COMPROT_STEP_READ_FIFO_TIMEOUT:
              step_status = AnsiString( "Timeout lettura FIFO" );
            break;
            case SPV_COMPROT_STEP_READ_FIFO_FAILURE:
              step_status = AnsiString( "Lettura FIFO fallita" );
            break;
            case SPV_COMPROT_STEP_WRITE_FIFO_START:
              step_status = AnsiString( "Scrittura FIFO avviata" );
            break;
            case SPV_COMPROT_STEP_WRITE_FIFO_TIMEOUT:
              step_status = AnsiString( "Timeout scrittura FIFO" );
            break;
            case SPV_COMPROT_STEP_WRITE_FIFO_FAILURE:
              step_status = AnsiString( "Scrittura FIFO fallita" );
            break;
            case SPV_COMPROT_STEP_WRITE_FIFO_FULL:
              step_status = AnsiString( "FIFO piena" );
            break;
            case SPV_COMPROT_STEP_READ_FIFO_EMPTY:
              step_status = AnsiString( "FIFO vuota" );
            break;
            case SPV_COMPROT_STEP_FRAME_NOT_FOUND:
              step_status = AnsiString( "Frame non riconosciuto" );
            break;
            case SPV_COMPROT_STEP_FRAME_CORRUPTED:
              step_status = AnsiString( "Frame corrotto" );
            break;
            case SPV_COMPROT_STEP_FIELD_CORRUPTED:
              step_status = AnsiString( "Campo del frame corrotto" );
            break;
            case SPV_COMPROT_STEP_HEADER_CHECK_FAILURE:
              step_status = AnsiString( "Controllo header fallito" );
            break;
            case SPV_COMPROT_STEP_TRAILER_CHECK_FAILURE:
              step_status = AnsiString( "Controllo trailer fallito" );
            break;
            case SPV_COMPROT_STEP_SUCCESS:
              step_status = AnsiString( "Concluso" );
            break;
            case SPV_COMPROT_STEP_WRITE_FIFO_SUCCESS:
              step_status = AnsiString( "Scrittura FIFO riuscita" );
            break;
            case SPV_COMPROT_STEP_FRAME_BUILD_START:
              step_status = AnsiString( "Costruzione frame avviata" );
            break;
            case SPV_COMPROT_STEP_FRAME_BUILD_SUCCESS:
              step_status = AnsiString( "Costruzione frame riuscita" );
            break;
            case SPV_COMPROT_STEP_FRAME_BUILD_FAILURE:
              step_status = AnsiString( "Costruzione frame fallita" );
            break;
            case SPV_COMPROT_STEP_READ_FIFO_SUCCESS:
              step_status = AnsiString( "Lettura FIFO riuscita" );
            break;
            case SPV_COMPROT_STEP_TIMEOUT:
              step_status = AnsiString( "Timeout" );
            break;
            case SPV_COMPROT_STEP_FRAME_SEARCH_START:
              step_status = AnsiString( "Ricerca frame avviata" );
            break;
            case SPV_COMPROT_STEP_FRAME_FOUND:
              step_status = AnsiString( "Frame riconosciuto" );
            break;
            case SPV_COMPROT_STEP_FRAME_EXTRACT_START:
              step_status = AnsiString( "Estrazione frame avviata" );
            break;
            case SPV_COMPROT_STEP_FRAME_EXTRACT_SUCCESS:
              step_status = AnsiString( "Frame estratto" );
            break;
            case SPV_COMPROT_STEP_FRAME_EXTRACT_FAILURE:
              step_status = AnsiString( "Estrazione frame fallita" );
            break;
            /*
            case :
              step_status = AnsiString( "" );
            break;
            */
          }
          step_status = AnsiString( command_thread->Retry + 1 ) +  AnsiString( ": " ) + step_status;

          if ( this->sgCommands != NULL )
          {
            // --- Controllo se c'� la riga sulla tabella ---
            if ( ( pos + 2 ) > this->sgCommands->RowCount )
            {
              this->sgCommands->RowCount = pos + 2 ;
            }
            // --- Inserisco in tabella le stringhe di stato ---
            this->sgCommands->Cells[0][pos+1] = id          ;
            this->sgCommands->Cells[1][pos+1] = device      ;
            this->sgCommands->Cells[2][pos+1] = port        ;
            this->sgCommands->Cells[3][pos+1] = name        ;
            this->sgCommands->Cells[4][pos+1] = status      ;
            this->sgCommands->Cells[5][pos+1] = step        ;
            this->sgCommands->Cells[6][pos+1] = step_status ;
            this->sgCommands->Cells[7][pos+1] = step_start  ;
            this->sgCommands->Cells[8][pos+1] = step_finish ;
            Application->ProcessMessages();
          }
        }
      }
      else
      {
        ret_code = SPV_COMPROTVIEW_INVALID_THREAD;
      }

      // --- Esco dalla sezione critica ---
      SPVLeaveThreadData( );
    }
    else
    {
      ret_code = SPV_COMPROTVIEW_INVALID_COMMAND_ID;
    }
  }
  catch (...)
  {
    // --- Esco dalla sezione critica ---
    SPVLeaveThreadData( );
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di tutti i comandi di protocollo
///
/// \date [22.03.2006]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int __fastcall TfComProtView::ViewAllCommandsStatus( void )
{
  int ret_code = SPV_COMPROTVIEW_NO_ERROR;
  AnsiString com_prot_num_str = AnsiString( "pronti/attivi/riusciti/falliti/totali: n/d" );

  if ( this->sgCommands != NULL ) // Controllo il ptr all'oggetto StringGrid
  {
    // --- Entro nella sezione critica ---
    SPVEnterThreadData();

    this->lComProtNum->Caption = AnsiString( "pronti/attivi/riusciti/falliti/totali: 0/0/0/0/" ) + AnsiString( SPVComProtThreadCount );

    this->sgCommands->Cells[8][0] = AnsiString( "Fine Step" );

    if ( SPVComProtThreadCount > 0 )
    {
      this->lStatus->Caption = "Presenti comandi";
      SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
      for ( int c = 0; c < SPV_COMPROT_MAX_THREAD; c++ )
      {
        ViewCommandStatus( c );
      }
    }
    else
    {
      this->lStatus->Caption = "Nessun comando";
      SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_IDLE );
      this->ClearAllCommandsStatus();
    }
    // --- Esco dalla sezione critica ---
    SPVLeaveThreadData( );
  }
  else
  {
    ret_code = SPV_COMPROTVIEW_INVALID_STRING_GRID;
  }

  return ret_code;
}

//==============================================================================
///
/// \date [13.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfComProtView::miKillClick(TObject *Sender)
{
  SPV_COMPROT_THREAD * thread = NULL;
  int                  thread_pos = -1;
  int                   ret_code = 0;

  // --- Entro nella sezione critica ---
  SPVEnterThreadData();

  thread_pos  = this->sgCommands->Row-1;
  thread      = &SPVComProtThreadList[thread_pos];
  ret_code    = SPVKillCommand( thread );

  if ( ret_code == 0 )
  {

  }

  // --- Esco dalla sezione critica ---
  SPVLeaveThreadData( );
}
//---------------------------------------------------------------------------

//==============================================================================
///
/// \date [14.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfComProtView::popComProtPopup(TObject *Sender)
{
  SPV_COMPROT_THREAD * thread = NULL;
  int                  thread_pos = -1;
  int                   ret_code = 0;

  // --- Entro nella sezione critica ---
  SPVEnterThreadData();

  try
  {
    if ( SPVComProtThreadCount > 0 )
    {
      thread_pos  = this->sgCommands->Row-1;
      thread      = &SPVComProtThreadList[thread_pos];
      if ( thread != NULL )
      {
        if ( thread->Active || ( !thread->Active && thread->StepStart == 0 ) )
        {
          this->miKill->Enabled = true;
        }
        else
        {
          this->miKill->Enabled = false;
        }
      }
      else
      {
        this->miKill->Enabled = false;
      }
    }
    else
    {
      this->miKill->Enabled = false;
    }
  }
  catch (...)
  {
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveThreadData( );
}
//---------------------------------------------------------------------------

void __fastcall TfComProtView::tComProtViewTimer(TObject *Sender)
{
  //
  this->ViewAllCommandsStatus();
}
//---------------------------------------------------------------------------

void __fastcall TfComProtView::FormShow(TObject *Sender)
{
  this->tComProtView->Enabled = true;  
}
//---------------------------------------------------------------------------

void __fastcall TfComProtView::FormHide(TObject *Sender)
{
  this->tComProtView->Enabled = false;  
}
//---------------------------------------------------------------------------


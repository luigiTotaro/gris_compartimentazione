//==============================================================================
// Telefin Supervisor Communication Protocol View Form 1.0
//------------------------------------------------------------------------------
// Header form dei protocolli di comunicazione (SPVComProtViewForm.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.12 (30.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVComProtViewForm.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [30.09.2004]:
// - Prima versione prototipo del modulo.
// 0.02 [24.05.2005]:
// - Modificato il metodo costruttore TfComProtView::TfComProtView.
// 0.03 [14.06.2005]:
// - Modificato il metodo costruttore TfComProtView::TfComProtView.
// - Aggiunto il metodo TfComProtView::FormResize.
// - Aggiunto il metodo TfComProtView::ClearAllCommandsStatus.
// - Aggiunto il metodo TfComProtView::ViewCommandStatus.
// - Aggiunto il metodo TfComProtView::ViewAllCommandsStatus.
// 0.04 [15.06.2005]:
// - Modificato il metodo TfComProtView::ClearAllCommandsStatus.
// 0.05 [28.06.2005]:
// - Modificato il metodo TfComProtView::ViewCommandStatus.
// 0.06 [05.07.2005]:
// - Modificato il metodo TfComProtView::ViewCommandStatus.
// 0.07 [06.07.2005]:
// - Modificato il metodo TfComProtView::ViewCommandStatus.
// 0.08 [13.07.2005]:
// - Modificato il metodo TfComProtView::ClearAllCommandsStatus.
// - Aggiunto il metodo TfComProtView::miKillClick.
// 0.09 [14.07.2005]:
// - Modificato il metodo TfComProtView::ClearAllCommandsStatus.
// - Modificato il metodo TfComProtView::ViewCommandStatus.
// - Modificato il metodo TfComProtView::ViewAllCommandsStatus.
// - Aggiunto il metodo TfComProtView::popComProtPopup.
// 0.10 [21.07.2005]:
// - Modificato il metodo costruttore TfComProtView::TfComProtView.
// 0.11 [22.09.2005]:
// - Modificato il metodo costruttore TfComProtView::TfComProtView.
// - Modificato il metodo TfComProtView::ClearAllCommandsStatus.
// 0.12 [19.12.2005]:
// - Modificato il metodo costruttore TfComProtView::TfComProtView.
// - Modificato il metodo TfComProtView::ViewAllCommandsStatus.
//==============================================================================

#ifndef SPVComProtViewFormH
#define SPVComProtViewFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_COMPROTVIEW_NO_ERROR            0
#define SPV_COMPROTVIEW_INVALID_STRING_GRID 34001
#define SPV_COMPROTVIEW_INVALID_COMMAND_ID  34002
#define SPV_COMPROTVIEW_INVALID_THREAD      34003

//==============================================================================
// Classi
//------------------------------------------------------------------------------
class TfComProtView : public TForm
{
__published:	// IDE-managed Components
  TGroupBox   * gbStatus;
  TImage      * imgStatus;
  TLabel      * lStatus;
  TLabel *lComProtNum;
  TPanel      * pBluLineStatus;
  TStringGrid * sgCommands;
  TStatusBar *sbCommands;
  TPopupMenu *popComProt;
  TMenuItem *miKill;
  TTimer *tComProtView;
  /// Metodo che cattura l'evento Resize del form
  void __fastcall FormResize            ( TObject * Sender );
  void __fastcall miKillClick(TObject *Sender);
  void __fastcall popComProtPopup(TObject *Sender);
  void __fastcall tComProtViewTimer(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormHide(TObject *Sender);
private:	// User declarations
public:		// User declarations
      __fastcall TfComProtView          ( TComponent * Owner );
  /// Metodo per visualizzare lo stato di tutti i comandi di protocollo
  int __fastcall ClearAllCommandsStatus ( void );
  /// Metodo per visualizzare lo stato di un comando di protocollo
  int __fastcall ViewCommandStatus      ( int pos );
  /// Metodo per visualizzare lo stato di tutti i comandi di protocollo
  int __fastcall ViewAllCommandsStatus  ( void );
};
//---------------------------------------------------------------------------
extern PACKAGE TfComProtView *fComProtView;
//---------------------------------------------------------------------------
#endif

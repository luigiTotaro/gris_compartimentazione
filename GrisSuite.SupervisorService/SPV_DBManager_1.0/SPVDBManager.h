//==============================================================================
// Telefin Supervisor DB Manager Module 1.0
//------------------------------------------------------------------------------
// Header Modulo Manager DataBase (SPVDBManager.h)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	0.26 (28.03.2007 -> 21.12.2007)
//
// Copyright:	2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note: richiede SPVDBManager.h, SPVDBInterface.h
//------------------------------------------------------------------------------
// Version history:
// 0.01 [28.03.2007]:
// - Prima versione del modulo
// 0.02 [29.03.2007]:
// - Aggiunta SPVDBMLog().
// - Aggiunta SPVDBMStartConnCheck().
// - Aggiunta SPVDBMFinishConnCheck().
// 0.03 [02.04.2007]:
// - Modifiche minori.
// 0.04 [03.04.2007]:
// - Aggiunta SPVDBMLockConn().
// - Aggiunta SPVDBMReleaseConn().
// 0.05 [05.04.2007]:
// - Aggiunta SPVDBMSaveStreamFields().
// 0.06 [10.04.2007]:
// - Modificata SPVDBMSaveStreamField().
// 0.07 [13.04.2007]:
// - Modificata SPVDBMLinkReference().
// 0.08 [14.04.2007]:
// - Modificata SPVDBMDeleteReference().
// - Modificata SPVDBMUnLinkReference().
// - Modificata SPVDBMUpdateReference().
// 0.09 [18.04.2007]:
// - Modificata SPVDBMCheckForReference().
// - Modificata SPVDBMClearReference().
// 0.10 [19.04.2007]:
// - Modificata SPVDBMCheckForStream().
// - Modificata SPVDBMInsertStream().
// - Modificata SPVDBMUpdateStream().
// - Modificata SPVDBMSaveStream().
// 0.11 [20.04.2007]:
// - Modificata SPVDBMInsertReference().
// - Modificata SPVDBMSaveReference().
// 0.12 [26.04.2007]:
// - Aggiunta SPVDBMInsertStation().
// 0.13 [27.04.2007]:
// - Modificata SPVDBMInsertStation().
// 0.14 [02.05.2007]:
// - Modificata SPVDBMSaveStation().
// - Aggiunta SPVDBMSaveBuilding().
// - Aggiunta SPVDBMSaveRack().
// 0.15 [03.05.2007]:
// - Modificata SPVDBMInsertBuilding().
// - Modificata SPVDBMInsertRack().
// 0.16 [04.05.2007]:
// - Modificata SPVDBMCheckForStation().
// - Modificata SPVDBMCheckForBuilding().
// - Modificata SPVDBMCheckForRack().
// - Modificata SPVDBMUpdateStation().
// - Modificata SPVDBMUpdateBuilding().
// - Modificata SPVDBMUpdateRack().
// 0.17 [05.05.2007]:
// - Aggiunta SPVDBMRemoveAllStations().
// - Aggiunta SPVDBMDeleteRemovedStations().
// - Aggiunta SPVDBMRemoveAllBuildings().
// - Aggiunta SPVDBMDeleteRemovedBuildings().
// - Aggiunta SPVDBMRemoveAllRacks().
// - Aggiunta SPVDBMDeleteRemovedRacks().
// 0.18 [07.05.2007]:
// - Aggiunta SPVDBMCheckForPort().
// - Aggiunta SPVDBMRemoveAllPorts().
// 0.19 [08.05.2007]:
// - Modificata SPVDBMSavePort().
// - Modificata SPVDBMDeleteRemovedPorts().
// 0.20 [09.05.2007]:
// - Aggiunta SPVDBMCheckForDeviceStatus().
// - Modificata SPVDBMInsertPort().
// - Aggiunta SPVDBMInsertDeviceStatus().
// - Modificata SPVDBMUpdatePort().
// - Aggiunta SPVDBMUpdateDeviceStatus().
// - Aggiunta SPVDBMSaveDeviceStatus().
// 0.21 [12.10.2007]:
// - Aggiunta SPVDBMDeleteAllStreams().
// - Aggiunta SPVDBMCheckStreamsConformity().
// - Aggiunta SPVDBMCleanAllStreams().
// 0.22 [23.10.2007]:
// - Aggiunta la funzione interna _DeleteRemovedPorts()
// - Aggiunta la funzione interna _CheckForPort()
// - Aggiunta la funzione interna _RemoveAllPorts()
// - Aggiunta la funzione interna _DeleteRemovedRacks
// - Aggiunta la funzione interna _RemoveAllRacks
// - Aggiunta la funzione interna _DeleteRemovedBuildings
// - Aggiunta la funzione interna _RemoveAllBuildings
// - Aggiunta la funzione interna _DeleteRemovedStations
// - Aggiunta la funzione interna _RemoveAllStations

// - Modificata la funzione di interfaccia SPVDBMRemoveAllStations
// - Modificata la funzione di interfaccia SPVDBMSaveStation
// - Modificata la funzione di interfaccia SPVDBMDeleteRemovedStations
// - Modificata la funzione di interfaccia SPVDBMRemoveAllBuildings
// - Modificata la funzione di interfaccia SPVDBMSaveBuilding
// - Modificata la funzione di interfaccia SPVDBMDeleteRemovedBuildings
// - Modificata la funzione di interfaccia SPVDBMRemoveAllRacks
// - Modificata la funzione di interfaccia SPVDBMSaveRack
// - Modificata la funzione di interfaccia SPVDBMDeleteRemovedRacks
// - Modificata la funzione di interfaccia SPVDBMRemoveAllPorts
// - Modificata la funzione di interfaccia SPVDBMCheckForPort
// - Modificata la funzione di interfaccia SPVDBMSavePort
// - Modificata la funzione di interfaccia SPVDBMDeleteRemovedPorts
// - Modificata la funzione di interfaccia SPVDBMSaveStreamFields()
// - Modificata la funzione di interfaccia SPVDBMSaveStream()
// - Modificata la funzione di interfaccia SPVDBMSaveReference()
// - Modificata la funzione di interfaccia SPVDBMClearReference()
// - Modificata la funzione di interfaccia SPVDBMCleanAllStreams()
// - Modificata la funzione di interfaccia SPVDBMSaveDeviceStatus()

// - Modificata la funzione SPVDBMStartConnCheck() in _StartConnCheck() e resa interna
// - Modificata la funzione SPVDBMFinishConnCheck() in _FinishConnCheck() e resa interna
// - Modificata la funzione SPVDBMLog() in _Log() e resa interna
// - Modificata la funzione SPVDBMLockConn() in _LockConn() e resa interna
// - Modificata la funzione SPVDBMReleaseConn() in _ReleaseConn() e resa interna

// - Modificata funzione SPVDBMLinkReference() in _LinkReference() e resa interna
// - Modificata funzione SPVDBMInsertReference() in _InsertReference() e resa interna
// - Modificata funzione SPVDBMInsertStation() in _InsertStation() e resa interna
// - Modificata funzione SPVDBMCheckForStation() in _CheckForStation() e resa interna
// - Modificata funzione SPVDBMInsertBuilding() in _InsertBuilding() e resa interna
// - Modificata funzione SPVDBMUpdateBuilding() in _UpdateBuilding() e resa interna
// - Modificata funzione SPVDBMCheckForBuilding() in _CheckForBuilding() e resa interna
// - Modificata funzione SPVDBMInsertRack() in _InsertRack() e resa interna
// - Modificata funzione SPVDBMUpdateRack() in _UpdateRack() e resa interna
// - Modificata funzione SPVDBMCheckForRack() in _CheckForRack() e resa interna
// - Modificata funzione SPVDBMInsertPort() in _InsertPort() e resa interna
// - Modificata funzione SPVDBMUpdatePort() in _UpdatePort() e resa interna
// - Modificata funzione SPVDBMInsertDeviceStatus() in _InsertDeviceStatus() e resa interna
// - Modificata funzione SPVDBMCheckForDeviceStatus() in _CheckForDeviceStatus() e resa interna
// - Modificata funzione SPVDBMSaveStreamField() in _SaveStreamField() e resa interna
// - Modificata funzione SPVDBMInsertStream() in _InsertStream() e resa interna
// - Modificata funzione SPVDBMUpdateStream() in _UpdateStream() e resa interna
// - Modificata funzione SPVDBMCheckForStream() in _CheckForStream() e resa interna
// - Modificata funzione SPVDBMDeleteReference() in _DeleteReference() e resa interna
// - Modificata funzione SPVDBMUnLinkReference() in _UnLinkReference() e resa interna
// - Modificata funzione SPVDBMUpdateDeviceStatus() in _UpdateDeviceStatus() e resa interna
// - Modificata funzione SPVDBMDeleteAllStreams() in _DeleteAllStreams() e resa interna
// - Modificata funzione SPVDBMCheckStreamsConformity() in _CheckStreamsConformity() e resa interna
// - Modificata funzione SPVDBMCheckForReference() in _CheckForReference() e resa interna
// - Eliminata (commentata) la funzione SPVDBMUpdateReference()
// 0.23 [10.12.2007]:
// - Aggiunta la funzione di interfaccia SPVDBMTruncateProceduresTable
// - Aggiunta la funzione interna _TruncateProceduresTable
// - Aggiunta la funzione di interfaccia SPVDBMSaveProcedures
// - Aggiunta la funzione interna _SaveProcedures
// - Aggiunta la funzione interna _SQL_INSERT_Procedures_01
// - Aggiunta la funzione di interfaccia SPVDBMUpdateProcedure
// - Aggiunta la funzione interna _UpdateProcedure
// - Aggiunta la funzione interna _SQL_UPDATE_Procedure_01
// - Aggiunta la funzione di interfaccia SPVDBMDeleteUnusedRows
// - Aggiunta la funzione interna _DeleteUnusedRows
// - Aggiunta la funzione interna _SQL_EXEC_DelAllUnusedRows_01
// - Aggiunta la funzione di interfaccia SPVDBMSaveDeviceSystems
// - Aggiunta la funzione interna _SaveSaveDeviceSystems
// - Aggiunta la funzione interna _SQL_SELECT_systems_01
// - Aggiunta la funzione interna _SQL_UPDATE_systems_01
// - Aggiunta la funzione interna _SQL_INSERT_systems_01
// - Aggiunta la funzione di interfaccia SPVDBMSaveDeviceVemdors
// - Aggiunta la funzione interna _SaveSaveDeviceVendors
// - Aggiunta la funzione interna _SQL_SELECT_vendors_01
// - Aggiunta la funzione interna _SQL_UPDATE_vendors_01
// - Aggiunta la funzione interna _SQL_INSERT_vendors_01
// - Aggiunta la funzione di interfaccia SPVDBMSaveDeviceTypes
// - Aggiunta la funzione interna _SaveSaveDeviceTypes
// - Aggiunta la funzione interna _SQL_SELECT_device_type_01
// - Aggiunta la funzione interna _SQL_UPDATE_device_type_01
// - Aggiunta la funzione interna _SQL_INSERT_device_type_01
// 0.24 [19.12.2007]:
// - Modificata la funzione _CheckStreamsConformity;
// - Aggiunta la funzione _CheckStreamFieldsConformity;
// - Aggiunta la funzione _SQL_SELECT_fields_01
// - Aggiunta la funzione _SQL_SELECT_fields_02
// 0.25 [20.12.2007]:
// - Corrette le chiamate a _Log nel modulo.
// - Modificata SPVDBMCleanAllStreams.
// - Modificata _CheckStreamsConformity.
// 0.26 [21.12.2007]:
// - Corretta la funzione SPVDBMClearReference.
// - Modificata la funzione SPVDBMCleanAllStreams: tolto il controllo conformita'.
// 0.27 [xx.xx.2008]:
// - Aggiunta la funzione di interfaccia SPVDBMLoadProcExeCounts
// - Aggiunta la funzione interna _LoadProcExeCounts
// - Aggiunta la funzione interna _SQL_SELECT_procedures_01
// - Aggiunta la funzione di interfaccia SPVDBMSaveProcExeCounts
// - Aggiunta la funzione interna _SaveProcExeCounts
// - Aggiunta la funzione interna _SQL_UPDATE_Procedure_02
// - Modificata la funzione SPVDBMSaveReference
// - Aggiunta la funzione di interfaccia SPVDBMDeleteStream
// - Aggiunta la funzione interna _DeleteStream
// - Aggiunta la funzione interna _SQL_DELETE_Stream_01
// - Aggiunta la funzione di interfaccia SPVDBMGetStream
// - Aggiunta la funzione interna _GetStream
// - Aggiunta la funzione interna _SQL_SELECT_streams_03
// - Aggiunta la funzione di interfaccia SPVDBMSigneRemovedAllDevices
// - Aggiunta la funzione interna _SigneRemoved
// - Aggiunta la funzione di interfaccia SPVDBMSaveDevice
// - Aggiunta la funzione interna _SaveDevice
// - Aggiunta la funzione interna _SQL_SELECT_devices_01
// - Aggiunta la funzione interna _SQL_UPDATE_devices_01
// - Aggiunta la funzione interna _SQL_INSERT_devices_01
// - Aggiunta la funzione di interfaccia SPVDBMSigneRemovedAllNodes
// - Aggiunta la funzione di interfaccia SPVDBMSaveNode
// - Aggiunta la funzione interna _SaveNode
// - Aggiunta la funzione interna _SQL_SELECT_nodes_01
// - Aggiunta la funzione interna _SQL_UPDATE_nodes_01
// - Aggiunta la funzione interna _SQL_INSERT_nodes_01
// - Aggiunta la funzione di interfaccia SPVDBMSigneRemovedAllZones
// - Aggiunta la funzione di interfaccia SPVDBMSaveZone
// - Aggiunta la funzione interna _SaveZone
// - Aggiunta la funzione interna _SQL_SELECT_zones_01
// - Aggiunta la funzione interna _SQL_UPDATE_zones_01
// - Aggiunta la funzione interna _SQL_INSERT_zones_01
// - Aggiunta la funzione di interfaccia SPVDBMSigneRemovedAllRegions
// - Aggiunta la funzione di interfaccia SPVDBMSaveRegion
// - Aggiunta la funzione interna _SaveRegion
// - Aggiunta la funzione interna _SQL_SELECT_regions_01
// - Aggiunta la funzione interna _SQL_UPDATE_regions_01
// - Aggiunta la funzione interna _SQL_INSERT_regions_01
// - Aggiunta la funzione di interfaccia SPVDBMSaveServer
// - Aggiunta la funzione interna _SaveServer
// - Aggiunta la funzione interna _SQL_SELECT_servers_01
// - Aggiunta la funzione interna _SQL_UPDATE_servers_01
// - Aggiunta la funzione interna _SQL_INSERT_servers_01
// - Introdotta nuova gestione delle eccezioni nelle seguenti funzioni:
//		- _LockConn;
//		- _ReleaseConn;
//==============================================================================


#ifndef SPVDBManagerH
#define SPVDBManagerH

//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define SPV_DBM_NO_ERROR					  	0
#define SPV_DBM_GENERAL_ERROR					3500
#define SPV_DBM_INVALID_FIELD_CAPACITY	      	3501
#define SPV_DBM_INVALID_FIELD					3502
#define SPV_DBM_INVALID_DEVICE					3503
#define SPV_DBM_INVALID_STREAM_ID				3504
#define SPV_DBM_INVALID_DB_CONNECTION			3505
#define SPV_DBM_LOG_FUNCTION_NOT_SET			3506
#define SPV_DBM_DB_OPEN_FAILURE					3507
#define SPV_DBM_DB_CLOSE_FAILURE              	3508
#define SPV_DBM_LOG_FUNCTION_EXCEPTION			3509
#define SPV_DBM_REFERENCE_NOT_FOUND				3510
#define SPV_DBM_FUNCTION_EXCEPTION				3511
#define SPV_DBM_INVALID_CONVERTED_FIELD_VALUE	3512
#define SPV_DBM_INVALID_REFERENCE_ID			3513
#define SPV_DBM_INVALID_STREAM					3514
#define SPV_DBM_DB_QUERY_FAILURE				3516
#define SPV_DBM_REFERENCE_FOUND					3517
#define SPV_DBM_STREAM_FOUND					3518
#define	SPV_DBM_STREAM_NOT_FOUND				3519
#define SPV_DBM_INVALID_STATION					3520
#define SPV_DBM_STATION_FOUND					3521
#define SPV_DBM_STATION_NOT_FOUND				3522
#define SPV_DBM_BUILDING_FOUND					3523
#define SPV_DBM_BUILDING_NOT_FOUND				3524
#define SPV_DBM_RACK_FOUND						3525
#define SPV_DBM_RACK_NOT_FOUND					3526
#define SPV_DBM_INVALID_BUILDING				3527
#define SPV_DBM_INVALID_RACK					3528
#define SPV_DBM_INVALID_COMPROT_PORT			3529
#define SPV_DBM_INVALID_SYS_PORT				3530
#define SPV_DBM_PORT_NOT_FOUND					3531
#define SPV_DBM_PORT_FOUND						3532
#define SPV_DBM_DEVICE_STATUS_FOUND				3533
#define SPV_DBM_DEVICE_STATUS_NOT_FOUND			3534
#define SPV_DBM_NULL_DBI_FIELD					3535
#define SPV_DBM_STREAM_CONFORMITY_NOT_VERIFIED	3536
#define SPV_DBM_FIELD_CONFORMITY_NOT_VERIFIED	3537
#define SPV_DBM_PROC_TABLE_EXCEEDS_MAX 			3538
#define SPV_DBM_DB_EMPTY_RESULT        			3539
#define SPV_DBM_INVALID_PROCEDURE				3540
#define SPV_DBM_INVALID_EVENT_LIST				3541

#define SPV_DBM_FUNCTION_NOT_IMPLEMENTED		3599

// --- Codici eventi DB ---
#define SPV_DBM_EVENT_DB_CONNECT_SUCCESS     	4000
#define SPV_DBM_EVENT_DB_CONNECT_FAILURE     	4001
#define SPV_DBM_EVENT_DB_DISCONNECT_SUCCESS  	4002
#define SPV_DBM_EVENT_DB_DISCONNECT_FAILURE  	4003
#define SPV_DBM_EVENT_DB_QUERY_SUCCESS       	4004
#define SPV_DBM_EVENT_DB_QUERY_FAILURE       	4005
#define SPV_DBM_EVENT_DB_NOT_CONNECTED			4006
// --- Descrizione eventi DB ---
#define SPV_DBM_EVENT_DB_CONNECT_SUCCESS_DES     "Connessione al database riuscita"
#define SPV_DBM_EVENT_DB_CONNECT_FAILURE_DES     "Connessione al database fallita"
#define SPV_DBM_EVENT_DB_DISCONNECT_SUCCESS_DES  "Disconnessione dal database riuscita"
#define SPV_DBM_EVENT_DB_DISCONNECT_FAILURE_DES  "Disconnessione dal database fallita"
#define SPV_DBM_EVENT_DB_QUERY_SUCCESS_DES       "Query sul database riuscita"
#define SPV_DBM_EVENT_DB_QUERY_FAILURE_DES       "Query sul database fallita"

//==============================================================================
// Definizione dei tipi
//------------------------------------------------------------------------------
typedef int(* SPV_DBM_LOG_FUNCTION_PTR)( void *, int, int, char *, char * );

//==============================================================================
// Prototipi funzioni
//------------------------------------------------------------------------------

// Funzione per aggiornare un valore di riferimento di un campo
//int		SPVDBMUpdateReference		( void * pdevice, void * pfield, int arrayid, char * refid );

// Funzioni interfaccia
// Funzione per impostare una funzione di logging
void	SPVDBMSetLogFunction	( void * ptf );
// Funzione per rimuovere tutti gli oggetti topografici stazione
int		SPVDBMRemoveAllStations		( void );
// Funzione per salvare un oggetto topografico stazione
int		SPVDBMSaveStation			( void * pstation, char ** uid );
// Funzione per eliminare le stazioni rimosse
int		SPVDBMDeleteRemovedStations	( void );
// Funzione per rimuovere tutti gli oggetti topografici edificio
int		SPVDBMRemoveAllBuildings	( void );
// Funzione per salvare un oggetto topografico edificio
int		SPVDBMSaveBuilding			( void * pbuilding, char ** uid );
// Funzione per eliminare gli edifici rimossi
int		SPVDBMDeleteRemovedBuildings( void );
// Funzione per rimuovere tutti gli oggetti topografici armadio
int		SPVDBMRemoveAllRacks		( void );
// Funzione per salvare un oggetto topografico armadio
int		SPVDBMSaveRack				( void * prack, char ** uid );
// Funzione per eliminare gli armadi rimossi
int		SPVDBMDeleteRemovedRacks	( void );
// Funzione per rimuovere tutte le porte di comunicazione
int		SPVDBMRemoveAllPorts		( void );
// Funzione per salvare una porta di comunicazione
int		SPVDBMSavePort				( void * pport, char * SrvID, char ** uid );
// Funzione per verificare se esiste una porta
int 	SPVDBMCheckForPort			( void * pport, char ** uid );
// Funzione per eliminare le porte di comunicazione rimosse
int		SPVDBMDeleteRemovedPorts	( void );
// Funzione per salvare lo stato di una periferica
int		SPVDBMSaveDeviceStatus		( void * pdevice, bool multi_language );
// Funzione per salvare uno stream di periferica
int		SPVDBMSaveStream			( void * pdevice, void * pstream );
// Funzione per ottenere la severita' di uno stream di periferica
int		SPVDBMGetStreamSeverity	 	( void * pdevice, void * pstream );
// Funzione per salvare nel DataBase il valore di tutti i campi di stream di periferica
int		SPVDBMSaveStreamFields		( void * pdevice, void * pstream );
// Funzione per salvare un valore di riferimento di un campo
int		SPVDBMSaveReference			( void * pdevice, int strid, void * pfield, int arrayid, char * dstrid, char * dfieldid, char * darrayid );
// Funzione per eliminare un valore di riferimento di un campo
int		SPVDBMClearReference		( void * pdevice, int strid, void * pfield, int arrayid	);
// Funzione per ripulire la tabella degli stream da quelli non pi� validi
int		SPVDBMCleanAllStreams		( void * pdevice, bool multi_language);
// Funzione per salvare nel DB un blocco di eventi
int		SPVDBMSaveEvents			( void * pdevice, void * pevents, int event_count );
// Funzione esterna per svuotare completamente la tabella 'procedures'
int 	SPVDBMTruncateProceduresTable	( void );
// Funzione per salvare i valori di tutte le procedure
int 	SPVDBMSaveProcedures		( void * pproc_list, int proc_count );
// Funzione esterna per aggiornare le informazioni di una procedura applicativa
int 	SPVDBMUpdateProcedure		( void * pprocedure );
// Funzione per chiamare la Stored Proedure di pulizia delle righe non pi� usate
int		SPVDBMDeleteUnusedRows		( void );
// Funzione per salvare nel database i dati della tabella Device_Type
int 	SPVDBMSaveDeviceTypes		( void * pdevice_type_list, int device_type_count );
// Funzione per salvare nel database i dati della tabella Systems
int  	SPVDBMSaveDeviceSystems		( void * pdevice_system_list, int device_system_count );
// Funzione per salvare nel database i dati della tabella Vendors
int  	SPVDBMSaveDeviceVendors		( void * pdevice_vendor_list, int device_vendor_count );
// Funzione esterna per caricare dal DB la lista degli ExeCount
int 	SPVDBMLoadProcExeCounts		( void * pcount_list, void * pcount_num );
// Funzione esterna per salvare nel DB la lista degli ExeCount
int 	SPVDBMSaveProcExeCounts		( void * pcount_list, int count_num );
// Funzione esterna per caricare dal DB la lista degli ACK
int 	SPVDBMLoadDeviceAcks		( void * pack_list, void * pack_num );
// Funzione esterna per eliminare uno stream dal DB
int 	SPVDBMDeleteStream			( void * pdevice, int StrID );
// Funzione esterna per caricare dal DataBase il buffer di uno stream di periferica
int 	SPVDBMGetStream 			( void * pdevice, int streamID, void * pstream_buffer );
// Funzione esterna per marchiare come rimosse tutte le periferiche
int 	SPVDBMSigneRemovedAllDevices( void );
// Funzione esterna per salvare nel DB una periferica
int 	SPVDBMSaveDevice			( void * pdevice );
// Funzione esterna per aggiornare nel DB una periferica
int 	SPVDBMUpdateDevice			( void * pdevice );
// Funzione esterna per marchiare come rimossi tutti i nodi
int 	SPVDBMSigneRemovedAllNodes	( void );
// Funzione esterna per salvare nel DB un nodo
int 	SPVDBMSaveNode				( void * pnode );
// Funzione esterna per marchiare come rimosse tutte le zone
int 	SPVDBMSigneRemovedAllZones	( void );
// Funzione esterna per salvare nel DB una zona
int 	SPVDBMSaveZone				( void * pzone );
// Funzione esterna per marchiare come rimosse tutte le regioni
int 	SPVDBMSigneRemovedAllRegions( void );
// Funzione esterna per salvare nel DB una regione
int 	SPVDBMSaveRegion			( void * pregion );
// Funzione esterna per marchiare come rimosse tutti server
int		SPVDBMSignRemovedAllServers	( void );
// Funzione esterna per salvare nel DB un server
int 	SPVDBMSaveServer			( void * pserver );

#endif

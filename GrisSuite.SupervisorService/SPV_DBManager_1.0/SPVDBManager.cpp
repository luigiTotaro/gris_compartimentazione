//==========================================================================
// Telefin Supervisor DB Manager Module 1.0
//------------------------------------------------------------------------------
// Modulo Manager DataBase (SPVDBManager.cpp)
// Progetto:	Telefin Supervisor Server 1.4
//
// Revisione:	0.27 (28.03.2007 -> 01.03.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note: richiede SPVDBManager.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVDBManager.h
//==============================================================================

#pragma hdrstop
#include "SPVDBManager.h"
#include "SPVDBInterface.h"
#include "SPVSystem.h"
#include "SPVApplicative.h"
#include "SPVTopography.h"
#include "SPVComProt.h"
#include "SYSKernel.h"
#include "cnv_lib.h"
#include "UtilityLibrary.h"
#include "LogLibrary.h"

#pragma package(smart_init)

static  SPV_DBM_LOG_FUNCTION_PTR	SPVDBMLogFunction	= NULL;

//==============================================================================
/// Funzione per impostare una funzione di logging
///
/// \date [28.03.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVDBMSetLogFunction( void * ptf )
{
	// --- Salvo il puntatore a funzione (static) ---
	SPVDBMLogFunction = (SPV_DBM_LOG_FUNCTION_PTR)ptf;
}

//==============================================================================
/// Funzione per stampare una riga di log tramite la funzione impostata
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	_Log( void * conn, int sevlevel, int eventid, char * des, char * note )
{
	int ret_code = SPV_DBM_NO_ERROR	;

	// --- Controllo che la funzione di log sia stata impostata ---
	if ( SPVDBMLogFunction != NULL )
	{
		try
		{
			//LLAddLogFile( des );

			// --- Eseguo la funzione di log ---
			ret_code = SPVDBMLogFunction( conn, sevlevel, eventid, des, note );
		}
		catch(...)
		{
			ret_code = SPV_DBM_LOG_FUNCTION_EXCEPTION;
		}
	}
	else
	{
		ret_code = SPV_DBM_LOG_FUNCTION_NOT_SET;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per fare i controlli iniziali sulla connessione DB
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int _StartConnCheck( void * conn )
{
	int ret_code = SPV_DBM_NO_ERROR	;
	SPV_DBI_CONN * db_conn = (SPV_DBI_CONN *)conn;

	try
	{
		if ( db_conn != NULL ) // Controllo se il ptr a connssione � valido
		{
			if ( !db_conn->Active ) // Controllo che la connessione sia aperta
			{
				ret_code = SPVDBOpenConn( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					///_Log( db_conn, 0, SPV_DBM_EVENT_DB_CONNECT_SUCCESS, SPV_DBM_EVENT_DB_CONNECT_SUCCESS_DES, "_StartConnCheck" );
				}
				else
				{
					_Log( db_conn, 2, SPV_DBM_EVENT_DB_CONNECT_FAILURE, db_conn->LastError, "_StartConnCheck" );
					ret_code = SPV_DBM_DB_OPEN_FAILURE;
				}
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DB_CONNECTION;
		}
	}
	catch(...)
	{
		_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione generale", "_StartConnCheck" );
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per fare i controlli finali sulla connessione DB
///
/// \date [22.10.2007]
/// \author Enrico Alborali, mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int _FinishConnCheck( void * conn )
{
	int ret_code = SPV_DBM_NO_ERROR	;
	SPV_DBI_CONN * db_conn = (SPV_DBI_CONN *)conn;

	try
	{
		if ( db_conn != NULL ) // Controllo se il ptr a connssione � valido
		{
			// --- Controllo che la connessione non sia persistente ---
			if ( !db_conn->Persistent )
			{
				if ( db_conn->Active ) // Controllo che la connessione sia aper
				{
					ret_code = SPVDBCloseConn( db_conn );
					if ( ret_code != SPV_DBM_NO_ERROR  )
					{
						_Log( db_conn, 2, SPV_DBM_EVENT_DB_DISCONNECT_FAILURE, db_conn->LastError, "_FinishConnCheck" );
						ret_code = SPV_DBM_DB_CLOSE_FAILURE;
					}
					/*else
					{
						_Log( db_conn, 0, SPV_DBM_EVENT_DB_DISCONNECT_SUCCESS, SPV_DBM_EVENT_DB_DISCONNECT_SUCCESS_DES, "_FinishConnCheck" );
					} */
				}
				else
				{
					_Log( db_conn, 1, SPV_DBM_EVENT_DB_NOT_CONNECTED, "Trovata connessione non attiva", "_FinishConnCheck" );
				}
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DB_CONNECTION;
		}
	}
	catch(...)
	{
		_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione generale", "_FinishConnCheck" );
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per bloccare l'accesso alla connssione DB
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
void _LockConn( void * conn, char * label )
{
	SPV_DBI_CONN * db_conn = (SPV_DBI_CONN *)conn;

	if ( db_conn != NULL )
	{
		SYSLock( &db_conn->Lock, label );
	}
}

//==============================================================================
/// Funzione per rilasciare l'accesso alla connessione DB
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
void _ReleaseConn( void * conn )
{
	SPV_DBI_CONN * db_conn = (SPV_DBI_CONN *)conn;

	if ( db_conn != NULL ) {
		//LeaveCriticalSection( &db_conn->CriticalSection ); // Esco dalla sezione critica
		SYSUnLock(&db_conn->Lock);
	}
}

//==============================================================================
// DELETE FROM stream_fields WHERE DevID AND StrID AND FieldID AND ArrayID >=
//------------------------------------------------------------------------------
int _SQL_DELETE_stream_fields_01( char * query, int query_len, char * DevID, char * StrID, char * FieldID, char * ArrayID )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "ArrayID >= \'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// DELETE FROM building WHERE Removed=1
//------------------------------------------------------------------------------
int _SQL_DELETE_building_01( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM building WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// DELETE FROM poer WHERE Removed=1
//------------------------------------------------------------------------------
int _SQL_DELETE_port_01( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM port WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// DELETE FROM rack WHERE Removed=1
//------------------------------------------------------------------------------
int _SQL_DELETE_rack_01( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM rack WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// DELETE FROM station WHERE Removed=1
//------------------------------------------------------------------------------
int _SQL_DELETE_station_01( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM station WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO building OUTPUT BuildingID
//------------------------------------------------------------------------------
int _SQL_INSERT_building_01( char * query, int query_len, char * xmlid, char * name, char * description, char * stationid )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO building" );
	query_len = SPVDBAddToQuery( query, query_len, " (BuildingXMLID,BuildingName,BuildingDescription,StationID,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "OUTPUT inserted.BuildingID VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, xmlid );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, name );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, description );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, stationid );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO device_status
//------------------------------------------------------------------------------
int _SQL_INSERT_device_status_01( char * query, int query_len, char * DevID, char * SevLevel, char * Desc, char * Offline )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO device_status" );
	query_len = SPVDBAddToQuery( query, query_len, " (DevID,SevLevel,Description,Offline) " );
	query_len = SPVDBAddToQuery( query, query_len, "VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Desc );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Offline );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO port OUTPUT PortID
//------------------------------------------------------------------------------
int _SQL_INSERT_port_01( char * query, int query_len, char * xmlid, char * name, char * type, char * params, char * status, char * SrvID )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO port" );
	query_len = SPVDBAddToQuery( query, query_len, " (SrvID,PortXMLID,PortName,PortType,Parameters,Status,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "OUTPUT inserted.PortID VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, xmlid );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, name );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, type );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, params );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, status );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO rack OUTPUT RackID
//------------------------------------------------------------------------------
int _SQL_INSERT_rack_01( char * query, int query_len, char * xmlid, char * name, char * type, char * description, char * buildingid )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO rack" );
	query_len = SPVDBAddToQuery( query, query_len, " (RackXMLID,RackName,RackType,RackDescription,BuildingID,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "OUTPUT inserted.RackID VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, xmlid );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, name );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, type );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, description );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, buildingid );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// DELETE FROM reference WHERE ReferenceID =
//------------------------------------------------------------------------------
int _SQL_DELETE_reference_01( char * query, int query_len, char * RefID )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM reference WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "ReferenceID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RefID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO reference OUTPUT ReferenceID
//------------------------------------------------------------------------------
int _SQL_INSERT_reference_01( char * query, int query_len, char * Value, char * Visible, char * DeltaDevID, char * DeltaStrID, char * DeltaFieldID, char * DeltaArrayID )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO reference" );
	query_len = SPVDBAddToQuery( query, query_len, " (Value,Visible,DeltaDevID,DeltaStrID,DeltaFieldID,DeltaArrayID) " );
	query_len = SPVDBAddToQuery( query, query_len, "OUTPUT inserted.ReferenceID VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, Value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DeltaDevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DeltaStrID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DeltaFieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DeltaArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO station OUTPUT StationID
//------------------------------------------------------------------------------
int _SQL_INSERT_station_01( char * query, int query_len, char * xmlid, char * name )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO station" );
	query_len = SPVDBAddToQuery( query, query_len, " (StationXMLID,StationName,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "OUTPUT inserted.StationID VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, xmlid );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, name );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO stream_fields (*) VALUES
//------------------------------------------------------------------------------
int _SQL_INSERT_stream_fields_01( char * query, int query_len, char * DevID, char * StrID, char * FieldID, char * ArrayID, char * Name, char * SevLevel, char * Value, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO stream_fields (" );
	query_len = SPVDBAddToQuery( query, query_len, "DevID,StrID,FieldID,ArrayID,Name,SevLevel,Value,Description,Visible" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO streams
//------------------------------------------------------------------------------
int _SQL_INSERT_streams_01( char * query, int query_len, char * DevID, char * StrID, char * Name, char * Data, int Data_len, char * DateTime, char * SevLevel, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO streams (" );
	query_len = SPVDBAddToQuery( query, query_len, "DevID,StrID,Name,Data,DateTime,SevLevel,Description,Visible" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Data, Data_len );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DateTime );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO streams (con gestione dei blob)
//------------------------------------------------------------------------------
int _SQL_INSERT_streams_02( char * query, int query_len, char * DevID, char * StrID, char * Name, char * Data, int Data_len, char * DateTime, char * SevLevel, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO streams (" );
	query_len = SPVDBAddToQuery( query, query_len, "DevID,StrID,Name,Data,DateTime,SevLevel,Description,Visible" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, ":blob" );
	query_len = SPVDBAddToQuery( query, query_len, ",\'" );
	query_len = SPVDBAddToQuery( query, query_len, DateTime );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO streams (con gestione dei blob)
//------------------------------------------------------------------------------
int _SQL_INSERT_streams_03( char * query, int query_len, char * DevID, char * StrID, char * Name, char * Data, int Data_len, char * SevLevel, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO streams (" );
	query_len = SPVDBAddToQuery( query, query_len, "DevID,StrID,Name,Data,DateTime,SevLevel,Description,Visible" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, ":blob" );
	query_len = SPVDBAddToQuery( query, query_len, ",GETDATE(),\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}

//==============================================================================
// SELECT BuildingID FROM building WHERE BuildingXMLID AND StationID
//------------------------------------------------------------------------------
int _SQL_SELECT_building_01( char * query, int query_len, char * BuildingXMLID, char * StationID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT BuildingID FROM building WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "BuildingXMLID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, BuildingXMLID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StationID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StationID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT DevID FROM device_status WHERE DevID
//------------------------------------------------------------------------------
int _SQL_SELECT_device_status_01( char * query, int query_len, char * DevID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT DevID FROM device_status WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT PortID FROM port WHERE PortXMLID
//------------------------------------------------------------------------------
int _SQL_SELECT_port_01( char * query, int query_len, char * PortXMLID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT PortID FROM port WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "PortXMLID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortXMLID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT RackID FROM rack WHERE RackXMLID AND BuildingID
//------------------------------------------------------------------------------
int _SQL_SELECT_rack_01( char * query, int query_len, char * RackXMLID, char * BuildingID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT RackID FROM rack WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "RackXMLID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackXMLID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "BuildingID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, BuildingID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT StationID FROM station WHERE StationXMLID
//------------------------------------------------------------------------------
int _SQL_SELECT_station_01( char * query, int query_len, char * StationXMLID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT StationID FROM station WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "StationXMLID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StationXMLID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT ReferenceID FROM stream_fields WHERE DevID AND StrID AND FieldID
//------------------------------------------------------------------------------
int _SQL_SELECT_stream_fields_01( char * query, int query_len, char * DevID, char * StrID, char * FieldID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT ReferenceID FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT ReferenceID FROM stream_fields WHERE DevID AND StrID AND FieldID AND ArrayID >=
//------------------------------------------------------------------------------
int _SQL_SELECT_stream_fields_02( char * query, int query_len, char * DevID, char * StrID, char * FieldID, char * ArrayID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT ReferenceID FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "ArrayID >= \'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT ReferenceID FROM stream_fields WHERE DevID AND StrID AND FieldID AND ArrayID
//------------------------------------------------------------------------------
int _SQL_SELECT_stream_fields_03( char * query, int query_len, char * DevID, char * StrID, char * FieldID, char * ArrayID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT ReferenceID FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "ArrayID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// SELECT StrID FROM streams WHERE DevID AND StrID
//------------------------------------------------------------------------------
int _SQL_SELECT_streams_01( char * query, int query_len, char * DevID, char * StrID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT StrID FROM streams WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

/*
 *
 */
int _SQL_SELECT_streams_04(char * query, int query_len, char * DevID, char * StrID){
	query_len = SPVDBAddToQuery( query, query_len, "SELECT SevLevel FROM streams WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE building SET * WHERE BuildingID
//------------------------------------------------------------------------------
int _SQL_UPDATE_building_01( char * query, int query_len, char * BuildingID, char * BuildingName, char * BuildingDescription )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE building SET " );
	query_len = SPVDBAddToQuery( query, query_len, "BuildingName" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, BuildingName );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "BuildingDescription" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, BuildingDescription );
	query_len = SPVDBAddToQuery( query, query_len, "\',Removed=\'0\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "BuildingID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, BuildingID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE building SET Removed=1
//------------------------------------------------------------------------------
int _SQL_UPDATE_building_02( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE building SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// UPDATE device_status SET *
//------------------------------------------------------------------------------
int _SQL_UPDATE_device_status_01( char * query, int query_len, char * DevID, char * SevLevel, char * Desc, char * Offline )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE device_status SET " );
	query_len = SPVDBAddToQuery( query, query_len, "SevLevel" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Description" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Desc );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Offline" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Offline );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE DevID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE port SET * WHERE PortID
//------------------------------------------------------------------------------
int _SQL_UPDATE_port_01( char * query, int query_len, char * PortID, char * PortName, char * PortType, char * PortParams, char * PortStatus, char * SrvID )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE port SET " );
	query_len = SPVDBAddToQuery( query, query_len, "SrvID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "PortName" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortName );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "PortType" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortType );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Parameters" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortParams );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Status" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortStatus );
	query_len = SPVDBAddToQuery( query, query_len, "\',Removed=\'0\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "PortID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE port SET Removed=1
//------------------------------------------------------------------------------
int _SQL_UPDATE_port_02( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE port SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// UPDATE rack SET * WHERE RackID
//------------------------------------------------------------------------------
int _SQL_UPDATE_rack_01( char * query, int query_len, char * RackID, char * RackName, char * RackType, char * RackDescription )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE rack SET " );
	query_len = SPVDBAddToQuery( query, query_len, "RackName" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackName );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "RackType" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackType );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "RackDescription" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackDescription );
	query_len = SPVDBAddToQuery( query, query_len, "\',Removed=\'0\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "RackID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE rack SET Removed=1
//------------------------------------------------------------------------------
int _SQL_UPDATE_rack_02( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE rack SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// UPDATE station SET * WHERE StationID
//------------------------------------------------------------------------------
int _SQL_UPDATE_station_01( char * query, int query_len, char * StationID, char * StationName )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE station SET " );
	query_len = SPVDBAddToQuery( query, query_len, "StationName" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StationName );
	query_len = SPVDBAddToQuery( query, query_len, "\',Removed=\'0\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "StationID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StationID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE station SET Removed=1
//------------------------------------------------------------------------------
int _SQL_UPDATE_station_02( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE station SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Removed=\'1\'" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// UPDATE stream_fields SET * WHERE DevID AND StrID AND FieldID AND ArrayID
//------------------------------------------------------------------------------
int _SQL_UPDATE_stream_fields_01( char * query, int query_len, char * DevID, char * StrID, char * FieldID, char * ArrayID, char * Name, char * SevLevel, char * Value, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE stream_fields SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Name" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "SevLevel" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Value" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Description" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Visible" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "ArrayID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE stream_fields SET ReferenceID WHERE DevID AND StrID AND FieldID
//------------------------------------------------------------------------------
int _SQL_UPDATE_stream_fields_02( char * query, int query_len, char * DevID, char * StrID, char * FieldID )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE stream_fields SET " );
	query_len = SPVDBAddToQuery( query, query_len, "ReferenceID=NULL" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE stream_fields SET ReferenceID WHERE DevID AND StrID AND FieldID AND ArrayID
//------------------------------------------------------------------------------
int _SQL_UPDATE_stream_fields_03( char * query, int query_len, char * RefID, char * DevID, char * StrID, char * FieldID, char * ArrayID )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE stream_fields SET " );
	query_len = SPVDBAddToQuery( query, query_len, "ReferenceID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RefID );
	query_len = SPVDBAddToQuery( query, query_len, "\' WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "ArrayID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE stream_fields SET RefID=NULL WHERE DevID AND StrID AND FieldID AND ArrayID
//------------------------------------------------------------------------------
int _SQL_UPDATE_stream_fields_04( char * query, int query_len, char * DevID, char * StrID, char * FieldID, char * ArrayID )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE stream_fields SET " );
	query_len = SPVDBAddToQuery( query, query_len, "ReferenceID=NULL" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "FieldID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "ArrayID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ArrayID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE streams SET
//------------------------------------------------------------------------------
int _SQL_UPDATE_streams_01( char * query, int query_len, char * DevID, char * StrID, char * Name, char * Data, int Data_len, char * DateTime, char * SevLevel, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE streams SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Name=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Data=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Data, Data_len );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "DateTime=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DateTime );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "SevLevel=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Description=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Visible=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE streams SET (con gestione dei blob)
//------------------------------------------------------------------------------
int _SQL_UPDATE_streams_02( char * query, int query_len, char * DevID, char * StrID, char * Name, char * Data, int Data_len, char * DateTime, char * SevLevel, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE streams SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Name=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Data=" );
	query_len = SPVDBAddToQuery( query, query_len, ":blob" );
	query_len = SPVDBAddToQuery( query, query_len, "," );
	query_len = SPVDBAddToQuery( query, query_len, "DateTime=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DateTime );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "SevLevel=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Description=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Visible=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE streams SET (con gestione dei blob)
//------------------------------------------------------------------------------
int _SQL_UPDATE_streams_03( char * query, int query_len, char * DevID, char * StrID, char * Name, char * Data, int Data_len, char * SevLevel, char * Description, char * Visible )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE streams SET " );
	query_len = SPVDBAddToQuery( query, query_len, "Name=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Data=" );
	query_len = SPVDBAddToQuery( query, query_len, ":blob" );
	query_len = SPVDBAddToQuery( query, query_len, "," );
	query_len = SPVDBAddToQuery( query, query_len, "DateTime=GETDATE()," );
	query_len = SPVDBAddToQuery( query, query_len, "SevLevel=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SevLevel );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Description=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Description );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );
	query_len = SPVDBAddToQuery( query, query_len, "Visible=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Visible );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// DELETE di tutti gli stream, stream_fields e reference di un dato dispositivo
//------------------------------------------------------------------------------
int _SQL_DELETE_all_streams_by_device_01( char * query, int query_len, char * DevID )
{
	// Viene creata una tabella di appoggio per la tabella "reference" contente
	// i valori originali e il nuovo campo NumRiga autoincrementale ---
	query_len = SPVDBAddToQuery( query, query_len, "CREATE TABLE #tmp_reference ( RefID uniqueidentifier NOT NULL, ) ON [PRIMARY]\n" );

	// Riempio la tabella temporanea con i dati della mia tabella di partenza
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO #tmp_reference SELECT ReferenceID FROM stream_fields WHERE ReferenceID IS NOT NULL AND " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	// Procedo alla cancellazione degli stream_fields relativi al dispositivo
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	// Procedo alla cancellazione degli streams relativi al dispositivo
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM streams WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	// Procedo alla cancellazione dei reference relativi al dispositivo
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM reference WHERE ReferenceID IN (SELECT * FROM #tmp_reference)\n" );

	// Rimuovo la tabella di appoggio
	query_len = SPVDBAddToQuery( query, query_len, "DROP TABLE #tmp_reference\n" );

	return query_len;
}

//==============================================================================
// SELECT StrID, Name e Visible FROM streams WHERE DevID
//------------------------------------------------------------------------------
int _SQL_SELECT_streams_02( char * query, int query_len, char * DevID)
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT StrID, Name, Visible FROM streams WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	return query_len;
}

//==============================================================================
// SELECT Data FROM streams WHERE DevID AND StrID
//------------------------------------------------------------------------------
int _SQL_SELECT_streams_03( char * query, int query_len, char * DevID, char * StrID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT Data FROM streams WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO procedures
//------------------------------------------------------------------------------
int _SQL_INSERT_Procedures_01( char * query, int query_len, char * DevID, char * ProID, char * ProcedureName, char * ExeCount )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO procedures (" );
	query_len = SPVDBAddToQuery( query, query_len, "DevID,ProID,Name,ExeCount,LastExecution,InProgress" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES (" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProcedureName );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ExeCount );
	query_len = SPVDBAddToQuery( query, query_len, "\',NULL,\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}

//==============================================================================
// UPDATE procedures
//------------------------------------------------------------------------------
int _SQL_UPDATE_Procedure_01( char * query, int query_len, char * DevID_value, char * ProID_value, char * ExeCount_value, char * LastExecution_value, char * InProgress_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE procedures SET " );
	query_len = SPVDBAddToQuery( query, query_len, "ExeCount=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ExeCount_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',LastExecution=CONVERT(datetime, \'" );
	query_len = SPVDBAddToQuery( query, query_len, LastExecution_value );
	query_len = SPVDBAddToQuery( query, query_len, "\', 121),InProgress=\'" );
	query_len = SPVDBAddToQuery( query, query_len, InProgress_value );
	query_len = SPVDBAddToQuery( query, query_len, "\' WHERE DevID = " );
	query_len = SPVDBAddToQuery( query, query_len, DevID_value );
	query_len = SPVDBAddToQuery( query, query_len, " AND ProID = " );
	query_len = SPVDBAddToQuery( query, query_len, ProID_value );

	return query_len;
}

//==============================================================================
// UPDATE procedures
//------------------------------------------------------------------------------
int _SQL_UPDATE_Procedure_02( char * query, int query_len, char * DevID_value, char * ProID_value, char * ExeCount_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE procedures SET " );
	query_len = SPVDBAddToQuery( query, query_len, "ExeCount=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ExeCount_value );
	query_len = SPVDBAddToQuery( query, query_len, "\' WHERE DevID = " );
	query_len = SPVDBAddToQuery( query, query_len, DevID_value );
	query_len = SPVDBAddToQuery( query, query_len, " AND ProID = " );
	query_len = SPVDBAddToQuery( query, query_len, ProID_value );

	return query_len;
}

//==============================================================================
// EXEC DeleteUnusedRows
//------------------------------------------------------------------------------
int _SQL_EXEC_DeleteUnusedRows_01( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "EXEC util_DelUnusedRows" );

	return query_len;
}

//==============================================================================
// SELECT SystemID FROM systems WHERE SystemID
//------------------------------------------------------------------------------
int _SQL_SELECT_systems_01( char * query, int query_len, char * SystemID_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT SystemID FROM systems WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "SystemID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE systems
//------------------------------------------------------------------------------
int _SQL_UPDATE_systems_01( char * query, int query_len, char * SystemID_value, char * SystemDesc_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE systems SET " );
	query_len = SPVDBAddToQuery( query, query_len, "SystemDescription=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemDesc_value );
	query_len = SPVDBAddToQuery( query, query_len, "\' WHERE SystemID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO systems
//------------------------------------------------------------------------------
int _SQL_INSERT_systems_01( char * query, int query_len, char * SystemID_value, char * SystemDesc_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO systems (" );
	query_len = SPVDBAddToQuery( query, query_len, "SystemID,SystemDescription" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES (" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemDesc_value );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}


//==============================================================================
// SELECT VendorID FROM vendors WHERE VendorID
//------------------------------------------------------------------------------
int _SQL_SELECT_vendors_01( char * query, int query_len, char * VendorID_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT VendorID FROM vendors WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "VendorID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE vendors
//------------------------------------------------------------------------------
int _SQL_UPDATE_vendors_01( char * query, int query_len, char * VendorID_value, char * VendorName_value, char * VendorDesc_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE vendors SET " );
	query_len = SPVDBAddToQuery( query, query_len, "VendorName=\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorName_value );
	query_len = SPVDBAddToQuery( query, query_len, "\', VendorDescription=\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorDesc_value );
	query_len = SPVDBAddToQuery( query, query_len, "\' WHERE VendorID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO vendors
//------------------------------------------------------------------------------
int _SQL_INSERT_vendors_01( char * query, int query_len, char * VendorID_value, char * VendorName_value, char * VendorDesc_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO vendors (" );
	query_len = SPVDBAddToQuery( query, query_len, "VendorID,VendorName,VendorDescription" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES (" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorName_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorDesc_value );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}

//==============================================================================
// SELECT DeviceTypeID FROM device_type WHERE DeviceTypeID
//------------------------------------------------------------------------------
int _SQL_SELECT_device_type_01( char * query, int query_len, char * DevTypeID_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT DeviceTypeID FROM device_type WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DeviceTypeID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevTypeID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE device_type
//------------------------------------------------------------------------------
int _SQL_UPDATE_device_type_01( char * query, int query_len, char * DevTypeID_value, char * DevTypeDesc_value, char * SystemID_value, char * VendorID_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE device_type SET " );
	query_len = SPVDBAddToQuery( query, query_len, "DeviceTypeDescription=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevTypeDesc_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',SystemID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',VendorID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\' WHERE DeviceTypeID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevTypeID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// INSERT * INTO device_type
//------------------------------------------------------------------------------
int _SQL_INSERT_device_type_01( char * query, int query_len, char * DevTypeID_value, char * DevTypeDesc_value, char * SystemID_value, char * VendorID_value )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO device_type (" );
	query_len = SPVDBAddToQuery( query, query_len, "DeviceTypeID,DeviceTypeDescription,SystemID,VendorID" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES (" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevTypeID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevTypeDesc_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SystemID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, VendorID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\')\n" );

	return query_len;
}

//==============================================================================
// SELECT StrID, Name e Visible FROM streams WHERE DevID, StrID
//------------------------------------------------------------------------------
int _SQL_SELECT_fields_01( char * query, int query_len, char * DevID, char * StrID)
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT FieldID FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\' AND StrID =\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	return query_len;
}

//==============================================================================
// SELECT StrID, Name e Visible FROM streams WHERE DevID, StrID, FieldID
//------------------------------------------------------------------------------
int _SQL_SELECT_fields_02( char * query, int query_len, char * DevID, char * StrID, char * FieldID)
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT FieldID FROM stream_fields WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\' AND StrID =\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\' AND FieldID =\'" );
	query_len = SPVDBAddToQuery( query, query_len, FieldID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	return query_len;
}

//==============================================================================
// SELECT DevID, ProdID, ExeCount FROM procedures ORDER BY DevID,ProID
//------------------------------------------------------------------------------
int _SQL_SELECT_procedures_01( char * query, int query_len)
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT DevID,ProID,ExeCount FROM procedures ORDER BY DevID,ProID" );
	return query_len;
}

//==============================================================================
// DELETE FROM device_ack WHERE "Expired"
//------------------------------------------------------------------------------
int _SQL_DELETE_device_ack_01( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM device_ack WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "(DATEADD(minute, AckDurationMinutes, AckDate) <= GETDATE()) " );
	query_len = SPVDBAddToQuery( query, query_len, "AND (SupervisorID = \'0\')" );
	query_len = SPVDBAddToQuery( query, query_len, "\n" );

	return query_len;
}

//==============================================================================
// SELECT DevID, StrID, FieldID FROM device_ack
//------------------------------------------------------------------------------
int _SQL_SELECT_device_ack_01( char * query, int query_len)
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT DevID,StrID,FieldID FROM device_ack WHERE SupervisorID = \'0\'" );
	return query_len;
}

//==============================================================================
// DELETE FROM stream WHERE DevID AND StrID
//------------------------------------------------------------------------------
int _SQL_DELETE_Stream_01( char * query, int query_len, char * DevID, char * StrID )
{
	query_len = SPVDBAddToQuery( query, query_len, "DELETE FROM streams WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, " AND " );
	query_len = SPVDBAddToQuery( query, query_len, "StrID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, StrID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );
	return query_len;
}

//==============================================================================
// SELECT Type,DefinitionVersion FROM devices WHERE DevID
//------------------------------------------------------------------------------
int _SQL_SELECT_devices_01( char * query, int query_len, char * DevID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT Type, DefinitionVersion FROM devices WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "DevID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE devices WHERE DevID
//------------------------------------------------------------------------------
int _SQL_UPDATE_devices_01( char * query, int query_len, char * DevID,
							char * NodID_64_value,
							char * SrvID_32_value,
							char * Name_value,
							char * Type_value,
							char * SN_value,
							char * PortID_value,
							char * Addr_value,
							char * ProfileID_value,
							char * Active_value,
							char * Scheduled_value,
							char * RackFID_value,
							char * RackCol_value,
							char * RackRow_value,
							char * DefinitionVersion_value,
							char * ProtocolVersion_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE devices SET " );

	query_len = SPVDBAddToQuery( query, query_len, "NodID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, NodID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "SrvID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Name" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Type" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Type_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "SN" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SN_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "PortID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Addr" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Addr_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "ProfileID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProfileID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Active" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Active_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Scheduled" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Scheduled_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Removed" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "RackID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackFID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "RackPositionCol" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackCol_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "RackPositionRow" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackRow_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "DefinitionVersion" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DefinitionVersion_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "ProtocolDefinitionVersion" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProtocolVersion_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE DevID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE devices WHERE DevID (aggiorna solo la colonna removed)
//------------------------------------------------------------------------------
int _SQL_UPDATE_devices_02( char * query, int query_len, char * DevID )
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE devices SET " );

	query_len = SPVDBAddToQuery( query, query_len, "Removed" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE DevID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE devices WHERE DevID
//------------------------------------------------------------------------------
int _SQL_UPDATE_devices_03( char * query, int query_len, char * DevID,
							char * PortID_value,
							char * DefinitionVersion_value,
							char * ProtocolVersion_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE devices SET " );

	query_len = SPVDBAddToQuery( query, query_len, "PortID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "DefinitionVersion" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DefinitionVersion_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "ProtocolDefinitionVersion" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProtocolVersion_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE DevID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// INSERT INTO devices
//------------------------------------------------------------------------------
int _SQL_INSERT_devices_01( char * query, int query_len,
							char * DevID,
							char * NodID_64_value,
							char * SrvID_32_value,
							char * Name_value,
							char * Type_value,
							char * SN_value,
							char * PortID_value,
							char * Addr_value,
							char * ProfileID_value,
							char * Active_value,
							char * Scheduled_value,
							char * RackFID_value,
							char * RackCol_value,
							char * RackRow_value,
							char * DefinitionVersion_value,
							char * ProtocolVersion_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO devices" );
	query_len = SPVDBAddToQuery( query, query_len, " (DevID,NodID,SrvID,Name,Type,SN,PortID,Addr,ProfileID,Active,Scheduled,Removed,RackID,RackPositionCol,RackPositionRow,DefinitionVersion,ProtocolDefinitionVersion) " );
	query_len = SPVDBAddToQuery( query, query_len, "VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, NodID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Type_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, SN_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, PortID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Addr_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProfileID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Active_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Scheduled_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackFID_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackCol_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, RackRow_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, DefinitionVersion_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ProtocolVersion_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// SELECT NodID FROM nodes WHERE NodID
//------------------------------------------------------------------------------
int _SQL_SELECT_nodes_01( char * query, int query_len, char * NodID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT NodID FROM nodes WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "NodID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, NodID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE nodes WHERE NodID
//------------------------------------------------------------------------------
int _SQL_UPDATE_nodes_01( char * query, int query_len,
						  char * NodID_64_value, char * ZonID_64_value, char * Name_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE nodes SET " );

	query_len = SPVDBAddToQuery( query, query_len, "ZonID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ZonID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Name" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Removed" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE NodID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, NodID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}


//==============================================================================
// INSERT INTO nodes
//------------------------------------------------------------------------------
int _SQL_INSERT_nodes_01( char * query, int query_len,
						  char * NodID_64_value, char * ZonID_64_value, char * Name_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO nodes" );
	query_len = SPVDBAddToQuery( query, query_len, " (NodID,ZonID,Name,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, NodID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, ZonID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}

//==============================================================================
// SELECT ZonID FROM zones WHERE ZonID
//------------------------------------------------------------------------------
int _SQL_SELECT_zones_01( char * query, int query_len, char * ZonID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT ZonID FROM nodes WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "ZonID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ZonID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE zones WHERE ZonID
//------------------------------------------------------------------------------
int _SQL_UPDATE_zones_01( char * query, int query_len,
						  char * ZonID_64_value, char * RegID_64_value, char * Name_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE zones SET " );

	query_len = SPVDBAddToQuery( query, query_len, "RegID" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RegID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Name" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Removed" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE ZonID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, ZonID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}


//==============================================================================
// INSERT INTO zones
//------------------------------------------------------------------------------
int _SQL_INSERT_zones_01( char * query, int query_len,
						  char * ZonID_64_value, char * RegID_64_value, char * Name_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO zones" );
	query_len = SPVDBAddToQuery( query, query_len, " (ZonID,RegID,Name,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ZonID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, RegID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}


//==============================================================================
// SELECT RegID FROM regions WHERE RegID
//------------------------------------------------------------------------------
int _SQL_SELECT_regions_01( char * query, int query_len, char * RegID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT RegID FROM regions WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "RegID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RegID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE regions WHERE RegID
//------------------------------------------------------------------------------
int _SQL_UPDATE_regions_01( char * query, int query_len,
							char * RegID_64_value, char * Name_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE regions SET " );

	query_len = SPVDBAddToQuery( query, query_len, "Name" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Removed" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE RegID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, RegID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}


//==============================================================================
// INSERT INTO regions
//------------------------------------------------------------------------------
int _SQL_INSERT_regions_01( char * query, int query_len,
							char * RegID_64_value, char * Name_value)
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO regions" );
	query_len = SPVDBAddToQuery( query, query_len, " (RegID,Name,Removed) " );
	query_len = SPVDBAddToQuery( query, query_len, "VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, RegID_64_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, ")\n" );

	return query_len;
}


//==============================================================================
// SELECT SrvID FROM servers WHERE SrvID
//------------------------------------------------------------------------------
int _SQL_SELECT_servers_01( char * query, int query_len, char * SrvID )
{
	query_len = SPVDBAddToQuery( query, query_len, "SELECT SrvID FROM servers WHERE " );
	query_len = SPVDBAddToQuery( query, query_len, "SrvID=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}

//==============================================================================
// UPDATE servers WHERE SrvID
//------------------------------------------------------------------------------
int _SQL_UPDATE_servers_01( char * query, int query_len,
							char * SrvID_32_value, char * NodID_64_value,
							char * Name_value,
							char * Host_value, char * xml_text)
{
	query_len = SPVDBAddToQuery( query, query_len, "UPDATE servers SET " );

	if (strcmpi(NodID_64_value,"0")!=0) {
		query_len = SPVDBAddToQuery( query, query_len, "NodID" );
		query_len = SPVDBAddToQuery( query, query_len, "=\'" );
		query_len = SPVDBAddToQuery( query, query_len, NodID_64_value );
		query_len = SPVDBAddToQuery( query, query_len, "\'," );
	}

	query_len = SPVDBAddToQuery( query, query_len, "Name" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Host" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, Host_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

	query_len = SPVDBAddToQuery( query, query_len, "Removed" );
	query_len = SPVDBAddToQuery( query, query_len, "=\'0\'," );

	query_len = SPVDBAddToQuery( query, query_len, "SupervisorSystemXML = CONVERT(xml,'" );
	query_len = SPVDBAddToQuery( query, query_len, xml_text );
	query_len = SPVDBAddToQuery( query, query_len, "')" );

	query_len = SPVDBAddToQuery( query, query_len, " WHERE SrvID " );
	query_len = SPVDBAddToQuery( query, query_len, "=\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value );
	query_len = SPVDBAddToQuery( query, query_len, "\'\n" );

	return query_len;
}


//==============================================================================
// INSERT INTO servers
//------------------------------------------------------------------------------
int _SQL_INSERT_servers_01( char * query, int query_len,
							char * SrvID_32_value, char * NodID_64_value, char * Name_value,
							char * Host_value, char * xml_text)
{
//	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO servers" );
//	query_len = SPVDBAddToQuery( query, query_len, " (SrvID,Name,Host,SupervisorSystemXML) " );
//	query_len = SPVDBAddToQuery( query, query_len, "VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value );

	if (strcmpi(NodID_64_value,"0")==0) {
		query_len = SPVDBAddToQuery( query, query_len, "\',NULL,\'" );
	}
	else
	{
		query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
		query_len = SPVDBAddToQuery( query, query_len, NodID_64_value );
		query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	}

	query_len = SPVDBAddToQuery( query, query_len, Name_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, Host_value );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, "0" );
	query_len = SPVDBAddToQuery( query, query_len, "\',"      );
	query_len = SPVDBAddToQuery( query, query_len, "CONVERT(xml,'" );
	query_len = SPVDBAddToQuery( query, query_len, xml_text );
	query_len = SPVDBAddToQuery( query, query_len, "') \n" );
//	query_len = SPVDBAddToQuery( query, query_len, "')" );

	return query_len;
}

//==============================================================================
// INSERT INTO events (*) VALUES
//------------------------------------------------------------------------------
int _SQL_INSERT_events_01( char * query, int query_len, char * DevID, char * EventCategory, char * EventData, char * Created )
{
	query_len = SPVDBAddToQuery( query, query_len, "INSERT INTO events (" );
	query_len = SPVDBAddToQuery( query, query_len, "DevID,EventCategory,EventData,Created" );
	query_len = SPVDBAddToQuery( query, query_len, ") VALUES(" );
	query_len = SPVDBAddToQuery( query, query_len, "\'" );
	query_len = SPVDBAddToQuery( query, query_len, DevID );
	query_len = SPVDBAddToQuery( query, query_len, "\',\'" );
	query_len = SPVDBAddToQuery( query, query_len, EventCategory );
	query_len = SPVDBAddToQuery( query, query_len, "\'," );

//	CONVERT ( data_type [ ( length ) ] , expression [ , style ] )

	query_len = SPVDBAddToQuery( query, query_len, "CONVERT(varbinary(max),\'" );
	query_len = SPVDBAddToQuery( query, query_len, EventData );
	query_len = SPVDBAddToQuery( query, query_len, "\')," );

	query_len = SPVDBAddToQuery( query, query_len, "CONVERT(datetime, \'" );
	query_len = SPVDBAddToQuery( query, query_len, Created );
	query_len = SPVDBAddToQuery( query, query_len, "\')" );

    query_len = SPVDBAddToQuery( query, query_len, ");\n" );

	return query_len;
}

//==============================================================================
// TRANSACTION Begin
//------------------------------------------------------------------------------
int _SQL_TRANSACTION_begin( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "set xact_abort on\n\n" );
	query_len = SPVDBAddToQuery( query, query_len, "begin tran\n\n" );

	return query_len;
}

//==============================================================================
// TRANSACTION End
//------------------------------------------------------------------------------
int _SQL_TRANSACTION_end( char * query, int query_len )
{
	query_len = SPVDBAddToQuery( query, query_len, "commit tran\n\n" );

	return query_len;
}

//==============================================================================
/// Funzione per verificare se esiste una stazione
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_CheckForStation( SPV_DBI_CONN * db_conn, void * pstation, char ** uid )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_TPG_Station *		station			    = (SPV_TPG_Station*)pstation	;
	SPV_DBI_FIELD *			field				= NULL							;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int16		StationXMLID		= 0								;
	char *					StationXMLID_value	= NULL							;

	if ( station != NULL )
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero valori ---
				try
				{
					StationXMLID	= (unsigned __int16)station->GetXMLID();
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckForStation" );
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						StationXMLID_value  = XMLType2ASCII( (void*)&StationXMLID, t_u_int_16 ); 	// -MALLOC
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckForStation" );
					}
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Preparo la query per eliminare il reference ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						query_len = _SQL_SELECT_station_01( query, query_len, StationXMLID_value );
					}
					catch(...)
					{
						_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_CheckForStation" );
					}
					// --- Lancio la query per inserzione di una nuova riga ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							field = SPVDBGetField( db_conn, 0, 0 );
							// --- Se la riga � gi� presente nella tabella del DB ---
							if ( field != NULL )
							{
								// --- Se voglio estrarre l'ID in una stringa ---
								if ( uid != NULL )
								{
									ret_code = SPV_DBM_STATION_FOUND;
									//* DEBUG */ _Log( db_conn, 2, ret_code, "FOUND", "_CheckForStation" );
									try
									{
										if ( field->Data != NULL)
										{
											// --- Estraggo l'ID appena trovato ---
											*uid = ULStrCpy( NULL, field->Data );
										}
										else
										{
											ret_code = SPV_DBM_STATION_NOT_FOUND;
											//* DEBUG */ _Log( db_conn, 2, ret_code, "NOT FOUND", "_CheckForStation" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione durante l\'estrazione di StationID", "_CheckForStation" );
									}
								}
							}
							else
							{
								// --- Stream non trovato ---
								ret_code = SPV_DBM_STATION_NOT_FOUND;
							}
						}
						else
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckForStation" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckForStation" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_CheckForStation" );
				}
				// --- Cancello i valori ---
				try
				{
					if ( StationXMLID_value != NULL ) free( StationXMLID_value );	// -FREE
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckForStation" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckForStation" );
			}
			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckForStation" );
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_STATION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se esiste un edificio
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_CheckForBuilding( SPV_DBI_CONN * db_conn, void * pbuilding, char ** uid )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_TPG_Building *		building			= (SPV_TPG_Building*)pbuilding	;
	SPV_TPG_Station *		station				= NULL							;
	SPV_DBI_FIELD *			field				= NULL							;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int16		BuildingXMLID		= 0								;
	char *					BuildingXMLID_value	= NULL							;
	char *					StationRAWID_value	= NULL							; // Raw
	char *					StationFID_value	= NULL							; // Formattato

	if ( building != NULL )
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero valori ---
				try
				{
					BuildingXMLID	= (unsigned __int16)building->GetXMLID();
					station = (SPV_TPG_Station *)building->GetParent();
					if ( station != NULL )
					{
						StationRAWID_value = station->GetID();
					}
					else
					{
						StationRAWID_value = NULL;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckForBuilding" );
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						BuildingXMLID_value	= XMLType2ASCII( (void*)&BuildingXMLID, t_u_int_16 ); 	// -MALLOC
						StationFID_value 	= SPVDBGetUniqueID( StationRAWID_value );				// -MALLOC
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckForBuilding" );
					}
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Preparo la query per eliminare il reference ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						query_len = _SQL_SELECT_building_01( query, query_len, BuildingXMLID_value, StationFID_value );
					}
					catch(...)
					{
						_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_CheckForBuilding" );
					}
					// --- Lancio la query per inserzione di una nuova riga ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							field = SPVDBGetField( db_conn, 0, 0 );
							// --- Se la riga � gi� presente nella tabella del DB ---
							if ( field != NULL )
							{
								ret_code = SPV_DBM_BUILDING_FOUND;
								// --- Se voglio estrarre l'ID in una stringa ---
								if ( uid != NULL )
								{
									try
									{
										if ( field->Data != NULL)
										{
											// --- Estraggo l'ID appena trovato ---
											*uid = ULStrCpy( NULL, field->Data );
										}
										else
										{
											ret_code = SPV_DBM_BUILDING_NOT_FOUND;
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione durante l\'estrazione di BuildingID", "_CheckForBuilding" );
									}
								}
							}
							else
							{
								// --- Stream non trovato ---
								ret_code = SPV_DBM_BUILDING_NOT_FOUND;
							}
						}
						else
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckForBuilding" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckForBuilding" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_CheckForBuilding" );
				}
				// --- Cancello i valori ---
				try
				{
					if ( BuildingXMLID_value != NULL ) 	free( BuildingXMLID_value );	// -FREE
					if ( StationFID_value != NULL )		free( StationFID_value );		// -FREE
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckForBuilding" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckForBuilding" );
			}
			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckForBuilding" );
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_BUILDING;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se esiste un armadio
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_CheckForRack( SPV_DBI_CONN * db_conn, void * prack, char ** uid )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_TPG_Rack *			rack				= (SPV_TPG_Rack*)prack			;
	SPV_TPG_Building *		building			= NULL;
	SPV_DBI_FIELD *			field				= NULL							;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int16		RackXMLID			= 0								;
	char *					RackXMLID_value		= NULL							;
	char *					BuildingRAWID_value	= NULL							;
	char *					BuildingFID_value	= NULL							;

	if ( rack != NULL )
	{
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero valori ---
				try
				{
					RackXMLID	= (unsigned __int16)rack->GetXMLID();
					building = (SPV_TPG_Building*)rack->GetParent();
					if ( building != NULL ) {
						BuildingRAWID_value = building->GetID();
					}
					else
					{
						BuildingRAWID_value = NULL;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckForRack" );
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						RackXMLID_value  	= XMLType2ASCII( (void*)&RackXMLID, t_u_int_16 ); 	// -MALLOC
						BuildingFID_value	= SPVDBGetUniqueID( BuildingRAWID_value );			// -MALLOC
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckForRack" );
					}
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Preparo la query ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						query_len = _SQL_SELECT_rack_01( query, query_len, RackXMLID_value, BuildingFID_value );
					}
					catch(...)
					{
						_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_CheckForRack" );
					}
					// --- Lancio la query per inserzione di una nuova riga ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							field = SPVDBGetField( db_conn, 0, 0 );
							// --- Se la riga � gi� presente nella tabella del DB ---
							if ( field != NULL )
							{
								ret_code = SPV_DBM_RACK_FOUND;
								//* DEBUG */ _Log( db_conn, 2, ret_code, "FOUND", "_CheckForRack" );
								// --- Se voglio estrarre l'ID in una stringa ---
								if ( uid != NULL )
								{
									try
									{
										if ( field->Data != NULL)
										{
											// --- Estraggo l'ID appena trovato ---
											*uid = ULStrCpy( NULL, field->Data );
										}
										else
										{
											ret_code = SPV_DBM_RACK_NOT_FOUND;
											//* DEBUG */ _Log( db_conn, 2, ret_code, "NOT FOUND", "_CheckForRack" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione durante l\'estrazione di RackID", "_CheckForRack" );
									}
								}
							}
							else
							{
								// --- Stream non trovato ---
								ret_code = SPV_DBM_RACK_NOT_FOUND;
							}
						}
						else
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckForRack" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckForRack" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_CheckForRack" );
				}
				// --- Cancello i valori ---
				try
				{
					if ( RackXMLID_value != NULL )		free( RackXMLID_value );	// -FREE
					if ( BuildingFID_value != NULL )	free( BuildingFID_value );	// -FREE
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckForRack" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckForRack" );
			}
			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckForRack" );
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_RACK;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se esiste una porta
///
/// \date [18.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int _CheckForPort( SPV_DBI_CONN * db_conn, void * pport, char ** uid, bool check )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_COMPROT_PORT * 		cpport				= (SPV_COMPROT_PORT*)pport		;
	SYS_PORT *				port				= NULL							;
	SPV_DBI_FIELD *			field				= NULL							;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int16		PortXMLID			= 0								;
	char *					PortXMLID_value		= NULL							;

	if ( cpport != NULL )
	{
		port = cpport->Port;
		if ( port != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			if (check) ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						PortXMLID	= (unsigned __int16)port->ID;
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckForPort" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							PortXMLID_value  	= XMLType2ASCII( (void*)&PortXMLID, t_u_int_16 ); 	// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckForPort" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_SELECT_port_01( query, query_len, PortXMLID_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_CheckForPort" );
						}
						// --- Lancio la query per inserzione di una nuova riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 );
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									ret_code = SPV_DBM_PORT_FOUND;
									//* DEBUG */ _Log( db_conn, 2, ret_code, "PORT FOUND", "_CheckForPort" );
									// --- Se voglio estrarre l'ID in una stringa ---
									if ( uid != NULL )
									{
										try
										{
											if ( field->Data != NULL)
											{
												// --- Estraggo l'ID appena trovato ---
												*uid = ULStrCpy( NULL, field->Data );
											}
											else
											{
												ret_code = SPV_DBM_PORT_NOT_FOUND;
												//* DEBUG */ _Log( db_conn, 2, ret_code, "PORT NOT FOUND", "_CheckForPort" );
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione durante l\'estrazione di PortID", "_CheckForPort" );
										}
									}
								}
								else
								{
									// --- Porta non trovata ---
									ret_code = SPV_DBM_PORT_NOT_FOUND;
								}
							}
							else
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckForPort" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckForPort" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_CheckForPort" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( PortXMLID_value != NULL )	free( PortXMLID_value );	// -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckForPort" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckForPort" );
				}
				// --- Controlli finali sulla connessione DB ---
				if (check) _FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckForPort" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_SYS_PORT;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_COMPROT_PORT;
	}

	return ret_code;
}

int _CheckForPort( SPV_DBI_CONN * db_conn, void * pport, char ** uid )
{
	return _CheckForPort( db_conn, pport, uid, true );
}

//==============================================================================
/// Funzione per verificare se eiste uno stato periferica
///
/// \date [18.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	_CheckForDeviceStatus( SPV_DBI_CONN * db_conn, void * pdevice )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DBI_FIELD*			field			= NULL							;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64        = 0                           	;
	char                  * DevID_64_value  = NULL                        	;

	try
	{
		if ( SPVValidDevice( device ) ) {
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						DevID_64        = (unsigned __int64)device->DevID;
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckForDeviceStatus" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckForDeviceStatus" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_SELECT_device_status_01( query, query_len, DevID_64_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_CheckForDeviceStatus" );
						}
						// --- Lancio la query ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 );
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									// --- DeviceStatus trovato ---
									ret_code = SPV_DBM_DEVICE_STATUS_FOUND;
								}
								else
								{
									// --- DeviceStatus non trovato ---
									ret_code = SPV_DBM_DEVICE_STATUS_NOT_FOUND;
								}
							}
							else
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckForDeviceStatus" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckForDeviceStatus" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_CheckForDeviceStatus" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( DevID_64_value != NULL )	free( DevID_64_value 	);	// -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckForDeviceStatus" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_CheckForDeviceStatus" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckForDeviceStatus" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_CheckForDeviceStatus" );
	}

	return	ret_code;
}

//==============================================================================
/// Funzione per verificare se esiste uno stream di periferica
///
/// \date [17.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_CheckForStream(SPV_DBI_CONN * db_conn, void * pdevice, void * pstream )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM*			stream			= (SPV_DEV_STREAM*)pstream		;
	SPV_DBI_FIELD*			field			= NULL							;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64        = 0                           	;
	unsigned __int8         StrID_8         = 0                           	;
	char                  * DevID_64_value  = NULL                        	;
	char                  * StrID_value     = NULL                        	;

	if ( SPVValidDevice( device ) ) {
		if ( SPVValidDevStream( stream ) ) {
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Recupero valori ---
						try
						{
							DevID_64        = (unsigned __int64)device->DevID;
							StrID_8         = (unsigned __int8)stream->ID;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckForStream" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							//* DEBUG */ _Log( db_conn, 2, 2034, "DEBUG", "_CheckForStream" );
							try
							{
								DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
								StrID_value     = XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); 		// -MALLOC
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckForStream" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								// --- Preparo la query per eliminare il reference ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_SELECT_streams_01( query, query_len, DevID_64_value, StrID_value );
							}
							catch(...)
							{
								_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_CheckForStream" );
							}
							// --- Lancio la query per inserzione di una nuova riga ---
							try
							{
								ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
								if ( ret_code == SPV_DBM_NO_ERROR )
								{
									field = SPVDBGetField( db_conn, 0, 0 );
									// --- Se la riga � gi� presente nella tabella del DB ---
									if ( field != NULL )
									{
										// --- Stream trovato ---
										ret_code = SPV_DBM_STREAM_FOUND;
									}
									else
									{
										// --- Stream non trovato ---
										ret_code = SPV_DBM_STREAM_NOT_FOUND;
									}
								}
								else
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckForStream" );
									ret_code = SPV_DBM_DB_QUERY_FAILURE;
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckForStream" );
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_CheckForStream" );
						}
						// --- Cancello i valori ---
						try
						{
							if ( DevID_64_value != NULL )	free( DevID_64_value 	);	// -FREE
							if ( StrID_value != NULL )		free( StrID_value 		);	// -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckForStream" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckForStream" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckForStream" );
				}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_STREAM;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

/*
 *
 */
int	_GetStreamSeverity(SPV_DBI_CONN * db_conn, void * pdevice, void * pstream )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM*			stream			= (SPV_DEV_STREAM*)pstream		;
	SPV_DBI_FIELD*			field			= NULL							;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64        = 0                           	;
	unsigned __int8         StrID_8         = 0                           	;
	char                  * DevID_64_value  = NULL                        	;
	char                  * StrID_value     = NULL                        	;
	int						SevLevel		= 0							;

	if ( SPVValidDevice( device ) ) {
		if ( SPVValidDevStream( stream ) ) {
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR ) {
					try {
						// --- Recupero valori ---
						try
						{
							DevID_64        = (unsigned __int64)device->DevID;
							StrID_8         = (unsigned __int8)stream->ID;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_GetStreamSeverity" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
								StrID_value     = XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); 		// -MALLOC
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_GetStreamSeverity" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								// --- Preparo la query per eliminare il reference ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_SELECT_streams_04( query, query_len, DevID_64_value, StrID_value );
							}
							catch(...)
							{
								_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_GetStreamSeverity" );
							}
							// --- Lancio la query per inserzione di una nuova riga ---
							try
							{
								ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
								if ( ret_code == SPV_DBM_NO_ERROR )
								{
									field = SPVDBGetField( db_conn, 0, 0 );
									// --- Se la riga � gi� presente nella tabella del DB ---
									if ( field != NULL )
									{
										try{
											// Converto il dato ottenuto da DB
											SevLevel = cnv_CharPToInt16(field->Data);
											// --- Stream trovato ---
											ret_code = SPV_DBM_STREAM_FOUND;
											//*DEBUG*/_Log( db_conn, 0, 222, field->Data, "_GetStreamSeverity" );
										}
										catch(...){
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di conversione e salvataggio dati", "_GetStreamSeverity" );
										}
									}
									else
									{
										// --- Stream non trovato ---
										ret_code = SPV_DBM_STREAM_NOT_FOUND;
									}
								}
								else
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_GetStreamSeverity" );
									ret_code = SPV_DBM_DB_QUERY_FAILURE;
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_GetStreamSeverity" );
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_GetStreamSeverity" );
						}
						// --- Cancello i valori ---
						try
						{
							if ( DevID_64_value != NULL )	free( DevID_64_value 	);	// -FREE
							if ( StrID_value != NULL )		free( StrID_value 		);	// -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_GetStreamSeverity" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione generale", "_GetStreamSeverity" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_GetStreamSeverity" );
				}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_STREAM;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return SevLevel;
}

//==============================================================================
/// Funzione per rimuovere tutti gli oggetti topografici stazione
///
/// \date [22.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_RemoveAllStations( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{

			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_UPDATE_station_02( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_RemoveAllStations" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_RemoveAllStations" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_RemoveAllStations" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per inserire una nuova stazione
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_InsertStation( SPV_DBI_CONN * db_conn, void * pstation, char ** uid )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_TPG_Station *		station				= (SPV_TPG_Station*)pstation	;
	SPV_DBI_FIELD *			field				= NULL							;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int16		StationXMLID		= 0								;
	char *					StationXMLID_value	= NULL							;
	char *					Name_value			= NULL							;

	try
	{
		if ( station != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						StationXMLID	= (unsigned __int16)station->XMLID;
						Name_value		= station->GetName();
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertStation" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							StationXMLID_value 	= XMLType2ASCII( (void*)&StationXMLID, t_u_int_16 );	// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertStation" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_INSERT_station_01( query, query_len, StationXMLID_value, Name_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_InsertStation" );
						}
						// --- Lancio la query per inserzione di una nuova riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertStation" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
							else
							{
								// --- Recupero StationID ---
								if ( uid != NULL ) {
									try	{
										field = SPVDBGetField( db_conn, 0 , 0 );
										if ( field != NULL ) {
											if ( field->Data != NULL ) {
												// --- Copio il valore ---
												*uid = ULStrCpy( NULL, field->Data );
											}
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione in fase di recupero StationID", "_InsertStation" );
									}
								}
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_InsertStation" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertStation" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( StationXMLID_value != NULL ) free( StationXMLID_value ); // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertStation" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_InsertStation" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_InsertStation" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_STATION;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_InsertStation" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare le stazioni rimosse
///
/// \date [22.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_DeleteRemovedStations( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_DELETE_station_01( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_DeleteRemovedStations" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteRemovedStations" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_DeleteRemovedStations" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per rimuovere tutti gli oggetti topografici edificio
///
/// \date [22.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_RemoveAllBuildings( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{

			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_UPDATE_building_02( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_RemoveAllBuildings" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_RemoveAllBuildings" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_RemoveAllBuildings" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per inserire un nuovo edificio
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_InsertBuilding( SPV_DBI_CONN *	db_conn, void * pbuilding, char ** uid )
{
	int 				ret_code 		            = SPV_DBM_NO_ERROR				;
	SPV_TPG_Building *	building			        = (SPV_TPG_Building*)pbuilding	;
	SPV_DBI_FIELD *		field						= NULL							;
	char                query                       [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 query_len                   = 0                           	; // Lunghezza stringa query
	SPV_TPG_Station *	station					    = NULL						    ;
	unsigned __int16    BuildingXMLID			    = 0							    ;
	char *				BuildingXMLID_value		    = NULL						    ;
	char *				BuildingName_value			= NULL						    ;
	char *				BuildingDescription_value	= NULL						    ;
	char *		 		StationRAWUID_value 	    = NULL						    ; // Unique ID RAW
	char *		 		StationFUID_value		    = NULL						    ; // Unique ID formattato

	try
	{
		if ( building != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						BuildingXMLID				= (unsigned __int16)building->XMLID;
						BuildingName_value			= building->GetName();
						BuildingDescription_value	= building->GetNote();
						station 					= (SPV_TPG_Station*)building->GetParent();
						if ( station != NULL ) {
							StationRAWUID_value = station->GetID();
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertBuilding" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							BuildingXMLID_value = XMLType2ASCII( (void*)&BuildingXMLID, t_u_int_16 );	// -MALLOC
							StationFUID_value	= SPVDBGetUniqueID( StationRAWUID_value );				// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertBuilding" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_INSERT_building_01( query, query_len, BuildingXMLID_value, BuildingName_value, BuildingDescription_value,StationFUID_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_InsertBuilding" );
						}
						// --- Lancio la query per inserzione di una nuova riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertBuilding" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
							else
							{
								// --- Recupero BuildingID ---
								if ( uid != NULL ) {
									try	{
										field = SPVDBGetField( db_conn, 0 , 0 );
										if ( field != NULL ) {
											if ( field->Data != NULL ) {
												// --- Copio il valore ---
												*uid = ULStrCpy( NULL, field->Data );
											}
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione in fase di recupero BuildingID", "_InsertBuilding" );
									}
								}
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_InsertBuilding" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertBuilding" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( BuildingXMLID_value != NULL ) 	free( BuildingXMLID_value ); 	// -FREE
						if ( StationFUID_value != NULL ) 	free( StationFUID_value ); 		// -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertBuilding" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_InsertBuilding" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_InsertBuilding" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_BUILDING;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_InsertBuilding" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare gli edifici rimossi
///
/// \date [22.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_DeleteRemovedBuildings( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_DELETE_building_01( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_DeleteRemovedBuildings" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteRemovedBuildings" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_DeleteRemovedBuildings" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per inserire un nuovo armadio
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_InsertRack( SPV_DBI_CONN * db_conn, void * prack, char ** uid )
{
	int 				ret_code 		    	= SPV_DBM_NO_ERROR			;
	SPV_TPG_Rack *		rack					= (SPV_TPG_Rack*)prack		;
	SPV_DBI_FIELD *		field					= NULL						;
	char            	query               	[SPV_DBI_QUERY_MAX_LENGTH]  ; // Stringa query
	int             	query_len           	= 0							; // Lunghezza stringa query
	SPV_TPG_Building *	building				= NULL						;
	unsigned __int16    RackXMLID				= 0							;
	char *				RackXMLID_value			= NULL						;
	char *				RackName_value			= NULL						;
	char *				RackType_value			= NULL						;
	char *				RackDescription_value	= NULL						;
	char *		 		BuildingRAWUID_value 	= NULL						; // Unique ID RAW
	char *		 		BuildingFUID_value		= NULL						; // Unique ID formattato

	try
	{
		if ( rack != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						RackXMLID				= (unsigned __int16)rack->XMLID;
						RackName_value 			= rack->GetName();
						RackType_value			= rack->GetType();
						RackDescription_value	= rack->GetNote();
						building = (SPV_TPG_Building*)rack->GetParent();
						if ( building != NULL ) {
							BuildingRAWUID_value = building->GetID();
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertRack" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							RackXMLID_value		= XMLType2ASCII( (void*)&RackXMLID, t_u_int_16 );	// -MALLOC
							BuildingFUID_value	= SPVDBGetUniqueID( BuildingRAWUID_value );			// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertRack" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_INSERT_rack_01( query, query_len, RackXMLID_value, RackName_value, RackType_value, RackDescription_value, BuildingFUID_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_InsertRack" );
						}
						// --- Lancio la query per inserzione di una nuova riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertRack" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
							else
							{
								// --- Recupero RackID ---
								if ( uid != NULL ) {
									try	{
										field = SPVDBGetField( db_conn, 0 , 0 );
										if ( field != NULL ) {
											if ( field->Data != NULL ) {
												// --- Copio il valore ---
												*uid = ULStrCpy( NULL, field->Data );
											}
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione in fase di recupero RackID", "_InsertRack" );
									}
								}
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_InsertRack" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertRack" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( RackXMLID_value    != NULL ) free( RackXMLID_value ); 		// -FREE
						if ( BuildingFUID_value != NULL ) free( BuildingFUID_value ); 	// -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertRack" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_InsertRack" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_InsertRack" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_RACK;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_InsertRack" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare gli armadi rimossi
///
/// \date [22.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_RemoveAllRacks( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{

			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_UPDATE_rack_02( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_RemoveAllRacks" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_RemoveAllRacks" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_RemoveAllRacks" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per eliminare gli armadi rimossi
///
/// \date [22.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_DeleteRemovedRacks( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_DELETE_rack_01( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_DeleteRemovedRacks" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteRemovedRacks" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_DeleteRemovedRacks" );
	}

	return ret_code;
}


//==============================================================================
///  Funzione per rimuovere tutte le porte di comunicazione
///
/// \date [18.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_RemoveAllPorts( SPV_DBI_CONN * db_conn )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	char               	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                	query_len       = 0                           	; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_UPDATE_port_02( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_RemoveAllPorts" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_RemoveAllPorts" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_RemoveAllPorts" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per inserire una nuova porta
///
/// \date [18.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_InsertPort( SPV_DBI_CONN * db_conn, void * pport, char * SrvID, char ** uid )
{
	int 				ret_code 		    = SPV_DBM_NO_ERROR			;
	SPV_COMPROT_PORT * 	cpport				= (SPV_COMPROT_PORT*)pport	;
	SYS_PORT *			port				= NULL						;
	SPV_DBI_FIELD *		field				= NULL						;
	char            	query               [SPV_DBI_QUERY_MAX_LENGTH]  ; // Stringa query
	int             	query_len           = 0							; // Lunghezza stringa query
	unsigned __int16	PortXMLID			= 0							;
	char *				PortXMLID_value		= NULL						;
	char *				PortName_value		= NULL						;
	char *				PortType_value		= NULL						;
	char *				PortParams_value	= NULL						;
	char *				PortStatus_value	= NULL						;

	try
	{
		if ( cpport != NULL )
		{
			port = cpport->Port;
			if ( port != NULL )
			{
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Recupero valori ---
						try
						{
							PortXMLID				= (unsigned __int16)port->ID;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertPort" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								PortXMLID_value		= XMLType2ASCII( (void*)&PortXMLID, t_u_int_16 );	// -MALLOC
								PortName_value		= port->Name;
								PortType_value		= SPVGetPortTypeName( port->Type ); 				// -MALLOC
								PortParams_value	= ULStrCpy( NULL, "n/d" );							// -MALLOC
								if ( port->Active ) {
									PortStatus_value	= ULStrCpy( NULL, "Porta attiva" ); // -MALLOC
								}
								else{
									PortStatus_value	= ULStrCpy( NULL, "Porta non attiva" ); // -MALLOC
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertPort" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								// --- Preparo la query ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_INSERT_port_01( query, query_len, PortXMLID_value, PortName_value, PortType_value, PortParams_value, PortStatus_value, SrvID );
							}
							catch(...)
							{
								_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_InsertPort" );
							}
							// --- Lancio la query per inserzione di una nuova riga ---
							try
							{
								ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
								if ( ret_code != SPV_DBM_NO_ERROR )
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertPort" );
									ret_code = SPV_DBM_DB_QUERY_FAILURE;
								}
								else
								{
									// --- Recupero PortID ---
									if ( uid != NULL ) {
										try	{
											field = SPVDBGetField( db_conn, 0 , 0 );
											if ( field != NULL ) {
												if ( field->Data != NULL ) {
													// --- Copio il valore ---
													*uid = ULStrCpy( NULL, field->Data );
												}
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di recupero PortID", "_InsertPort" );
										}
									}
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_InsertPort" );
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertPort" );
						}
						// --- Cancello i valori ---
						try
						{
							if ( PortXMLID_value != NULL )	free( PortXMLID_value );	// -FREE
							if ( PortType_value != NULL )	free( PortType_value );		// -FREE
							if ( PortParams_value != NULL )	free( PortParams_value );	// -FREE
							if ( PortStatus_value != NULL )	free( PortStatus_value );	// -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertPort" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_InsertPort" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_InsertPort" );
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_SYS_PORT;
			}
		}
		else{
			ret_code = SPV_DBM_INVALID_COMPROT_PORT;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_InsertPort" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per inserire un nuovo stato periferica
///
/// \date [26.10.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int	_InsertDeviceStatus( SPV_DBI_CONN * db_conn, void * pdevice, bool multi_language )
{
	int 				ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_DEVICE *		device 			    = (SPV_DEVICE*)pdevice			;
	char                query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int64    DevID_64            = 0                           	;
	unsigned __int8     SevLevel_8        	= 0								;
	unsigned __int8     Offline_8         	= 0   							;
	char *				DevID_64_value      = NULL                        	;
	char              * SevLevel_value    	= NULL							;
	char              * Description_value 	= NULL							;
	char              * Offline_value     	= NULL							;

	try
	{
		if ( SPVValidDevice( device ) ) {
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						DevID_64 	= device->DevID;
						SevLevel_8 	= (unsigned __int8)device->SupStatus.Level;
						Offline_8 	= (unsigned __int8)device->SupStatus.Offline;
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertDeviceStatus" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							DevID_64_value    = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
							SevLevel_value    = XMLType2ASCII( (void*)&SevLevel_8, t_u_int_8 ); // -MALLOC
							Offline_value     = XMLType2ASCII( (void*)&Offline_8, t_u_int_8 ); 	// -MALLOC
							if ( device->SupStatus.Description != NULL ) {
								Description_value = ULStrCpy( NULL, device->SupStatus.Description ); // -MALLOC
							}
							else{
								if (multi_language) {
									Description_value = ULStrCpy( NULL, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_UNKNOWN ); // -MALLOC
								}
								else {
									Description_value = ULStrCpy( NULL, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_UNKNOWN ); // -MALLOC
								}
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertDeviceStatus" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_INSERT_device_status_01( query, query_len, DevID_64_value, SevLevel_value, Description_value, Offline_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_InsertDeviceStatus" );
						}
						// --- Lancio la query ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertDeviceStatus" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_InsertDeviceStatus" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertDeviceStatus" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( Description_value 	!= NULL )	free( Description_value	); // -FREE
						if ( DevID_64_value 	!= NULL ) 	free( DevID_64_value 	); // -FREE
						if ( SevLevel_value 	!= NULL )	free( SevLevel_value 	); // -FREE
						if ( Offline_value 		!= NULL )	free( Offline_value  	); // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertDeviceStatus" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_InsertDeviceStatus" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_InsertDeviceStatus" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_InsertDeviceStatus" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per inserire un nuovo stream di periferica
///
/// \date [09.07.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	_InsertStream( SPV_DBI_CONN * db_conn, void * pdevice, void * pstream )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_DEVICE *			device 			    = (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM *		stream			    = (SPV_DEV_STREAM*)pstream		;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64            = 0                           	;
	unsigned __int8         StrID_8             = 0                           	;
	char *					DevID_64_value      = NULL                        	;
	char *					StrID_value         = NULL                        	;
	char *					Name_value		    = NULL                          ;
	char *					Data_value		    = NULL                          ;
	int						Data_len			= 0								;
	//char *					DateTime_value	    = NULL                          ;
	char *					SevLevel_value	    = NULL                          ;
	char *					Description_value	= NULL                          ;
	char *					Visible_value		= NULL							;

	try
	{
		if ( SPVValidDevice( device ) ) {
			if ( SPVValidDevStream( stream ) ) {
					// --- Controlli iniziali sulla connessione DB ---
					ret_code = _StartConnCheck( db_conn );
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Recupero valori ---
							try
							{
								DevID_64        = (unsigned __int64)device->DevID;
								StrID_8         = (unsigned __int8)stream->ID;
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertStream" );
							}
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								//* DEBUG */ _Log( db_conn, 2, 2034, "DEBUG", "_CheckForStream" );
								try
								{
									DevID_64_value  	= XMLType2ASCII( (void*)&DevID_64, t_u_int_64 );	// -MALLOC
									StrID_value     	= XMLType2ASCII( (void*)&StrID_8, t_u_int_8 );		// -MALLOC
									Name_value		    = stream->Name;
									Data_value			= (char*)(void*)stream->Buffer.Ptr;
									Data_len			= stream->Buffer.Len;
									Description_value	= stream->Description;
									SevLevel_value  	= XMLType2ASCII( &stream->SevLevel, t_u_int_8 );	// -MALLOC
									//DateTime_value		= SPVDBGetTime( 0 ); // -MALLOC
									if ( stream->Visible ) {
										Visible_value = XMLStrCpy( NULL, "1" ); // -MALLOC
									}
									else{
										Visible_value = XMLStrCpy( NULL, "0" ); // -MALLOC
									}
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertStream" );
								}
							}
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								try
								{
									// --- Preparo la query per inserire lo stream ---
									query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
									query_len = _SQL_INSERT_streams_03( query, query_len, DevID_64_value, StrID_value, Name_value, NULL, 0, SevLevel_value, Description_value, Visible_value );
								}
								catch(...)
								{
									_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_InsertStream" );
								}
								// --- Lancio la query per inserzione di una nuova riga ---
								try
								{
									ret_code = SPVDBQuery( db_conn, query, query_len, Data_value, Data_len );
									if ( ret_code != SPV_DBM_NO_ERROR )
									{
										_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertStream" );
										ret_code = SPV_DBM_DB_QUERY_FAILURE;
									}
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_InsertStream" );
								}
							}
							else
							{
								_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertStream" );
							}
							// --- Cancello i valori ---
							try
							{
								if ( DevID_64_value != NULL )	free( DevID_64_value	); // -FREE
								if ( StrID_value 	!= NULL )	free( StrID_value 		); // -FREE
								if ( SevLevel_value != NULL )	free( SevLevel_value	); // -FREE
								//if ( DateTime_value != NULL )	free( DateTime_value	); // -FREE
								if ( Visible_value 	!= NULL )	free( Visible_value		); // -FREE
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertStream" );
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_InsertStream" );
						}
						// --- Controlli finali sulla connessione DB ---
						_FinishConnCheck( db_conn );
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_InsertStream" );
					}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_STREAM;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_InsertStream" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare una stazione esistente
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_UpdateStation( SPV_DBI_CONN * db_conn, void * pstation, char * uid )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_TPG_Station *		station				= (SPV_TPG_Station*)pstation	;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	char *					StationID_value		= NULL							;
	char *					StationName_value	= NULL							;

	try
	{
		if ( station != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						StationName_value		= station->GetName();
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_UpdateStation" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							StationID_value 	= SPVDBGetUniqueID( uid ); // -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UpdateStation" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_UPDATE_station_01( query, query_len, StationID_value, StationName_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_UpdateStation" );
						}
						// --- Lancio la query per update riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UpdateStation" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_UpdateStation" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UpdateStation" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( StationID_value != NULL ) free( StationID_value ); // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_UpdateStation" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_UpdateStation" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UpdateStation" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_STATION;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdateStation" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare un edificio esistente
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_UpdateBuilding( SPV_DBI_CONN * db_conn, void * pbuilding, char * uid )
{
	int 					ret_code 		            = SPV_DBM_NO_ERROR				;
	SPV_TPG_Building *		building			        = (SPV_TPG_Building*)pbuilding	;
	char                	query                       [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len                   = 0                           	; // Lunghezza stringa query
	char *					BuildingID_value	        = NULL							;
	char *					BuildingName_value	        = NULL							;
	char *					BuildingDescription_value	= NULL							;

	try
	{
		if ( building != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						BuildingName_value			= building->GetName();
						BuildingDescription_value	= building->GetNote();
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_UpdateBuilding" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							BuildingID_value 	= SPVDBGetUniqueID( uid ); // -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UpdateBuilding" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_UPDATE_building_01( query, query_len, BuildingID_value, BuildingName_value, BuildingDescription_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_UpdateBuilding" );
						}
						// --- Lancio la query per update riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UpdateBuilding" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_UpdateBuilding" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UpdateBuilding" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( BuildingID_value != NULL ) free( BuildingID_value ); // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_UpdateBuilding" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_UpdateBuilding" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UpdateBuilding" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_BUILDING;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdateBuilding" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare un armadio esistente
///
/// \date [22.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_UpdateRack( SPV_DBI_CONN *	db_conn, void * prack, char * uid )
{
	int 					ret_code	            = SPV_DBM_NO_ERROR				;
	SPV_TPG_Rack *			rack			        = (SPV_TPG_Rack*)prack			;
	char                	query                   [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len               = 0                           	; // Lunghezza stringa query
	char *					RackID_value	        = NULL							;
	char *					RackName_value	        = NULL							;
	char *					RackType_value			= NULL							;
	char *					RackDescription_value	= NULL							;

	try
	{
		if ( rack != NULL )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						RackName_value			= rack->GetName();
						RackType_value			= rack->GetType();
						RackDescription_value	= rack->GetNote();
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_UpdateRack" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							RackID_value 	= SPVDBGetUniqueID( uid ); // -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UpdateRack" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_UPDATE_rack_01( query, query_len, RackID_value, RackName_value, RackType_value, RackDescription_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_UpdateRack" );
						}
						// --- Lancio la query per update riga ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UpdateRack" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_UpdateRack" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UpdateRack" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( RackID_value != NULL ) free( RackID_value ); // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_UpdateRack" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_UpdateRack" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UpdateRack" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_RACK;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdateRack" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare una porta esistente
///
/// \date [18.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	_UpdatePort( SPV_DBI_CONN * db_conn, void * pport, char * SrvID, char * uid )
{
	int 				ret_code            = SPV_DBM_NO_ERROR			;
	SPV_COMPROT_PORT * 	cpport				= (SPV_COMPROT_PORT*)pport	;
	SYS_PORT *			port				= NULL						;
	char            	query               [SPV_DBI_QUERY_MAX_LENGTH]  ; // Stringa query
	int             	query_len           = 0							; // Lunghezza stringa query
	char *				PortRAWID_value		= NULL						;
	char *				PortFID_value		= NULL						;
	char *				PortName_value		= NULL						;
	char *				PortType_value		= NULL						;
	char *				PortParams_value	= NULL						;
	char *				PortStatus_value	= NULL						;

	try
	{
		if ( cpport != NULL )
		{
			port = cpport->Port;
			if ( port != NULL )
			{
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Recupero valori ---
						try
						{
							PortRAWID_value				= uid;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_UpdatePort" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								PortFID_value 		= SPVDBGetUniqueID( PortRAWID_value ); 	// -MALLOC
								PortName_value		= port->Name;
								PortType_value		= SPVGetPortTypeName( port->Type ); // -MALLOC
								PortParams_value	= ULStrCpy( NULL, "n/d" );			// -MALLOC
								if ( port->Active ) {
									PortStatus_value	= ULStrCpy( NULL, "Porta attiva" ); // -MALLOC
								}
								else{
									PortStatus_value	= ULStrCpy( NULL, "Porta non attiva" ); // -MALLOC
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UpdatePort" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								// --- Preparo la query ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_UPDATE_port_01( query, query_len, PortFID_value, PortName_value, PortType_value, PortParams_value, PortStatus_value, SrvID );
							}
							catch(...)
							{
								_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_UpdatePort" );
							}
							// --- Lancio la query per update riga ---
							try
							{
								ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
								if ( ret_code != SPV_DBM_NO_ERROR )
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UpdatePort" );
									ret_code = SPV_DBM_DB_QUERY_FAILURE;
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_UpdatePort" );
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UpdatePort" );
						}
						// --- Cancello i valori ---
						try
						{
							if ( PortFID_value != NULL )	free( PortFID_value );	// -FREE
							if ( PortType_value != NULL )	free( PortType_value );		// -FREE
							if ( PortParams_value != NULL )	free( PortParams_value );	// -FREE
							if ( PortStatus_value != NULL )	free( PortStatus_value );	// -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_UpdatePort" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_UpdatePort" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UpdatePort" );
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_SYS_PORT;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_COMPROT_PORT;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdatePort" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare uno stato periferica
///
/// \date [26.10.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int	_UpdateDeviceStatus(SPV_DBI_CONN * db_conn, void * pdevice, bool multi_language )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_DEVICE * 			device 			    = (SPV_DEVICE*)pdevice			;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64            = 0                           	;
	unsigned __int8         SevLevel_8        	= 0								;
	unsigned __int8         Offline_8         	= 0   							;
	char *			      	DevID_64_value      = NULL                        	;
	char *					SevLevel_value    	= NULL							;
	char *					Description_value 	= NULL							;
	char *					Offline_value     	= NULL							;

	try
	{
		if ( SPVValidDevice( device ) ) {
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Recupero valori ---
					try
					{
						DevID_64 	= device->DevID;
						SevLevel_8 	= (unsigned __int8)device->SupStatus.Level;
						Offline_8 	= (unsigned __int8)device->SupStatus.Offline;
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "SPVDBMUpdateDeviceStatus" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							DevID_64_value    = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
							SevLevel_value    = XMLType2ASCII( (void*)&SevLevel_8, t_u_int_8 ); // -MALLOC
							Offline_value     = XMLType2ASCII( (void*)&Offline_8, t_u_int_8 ); 	// -MALLOC
							if ( device->SupStatus.Description != NULL ) {
								Description_value = ULStrCpy( NULL, device->SupStatus.Description ); // -MALLOC
							}
							else{
								if (multi_language) {
									Description_value = ULStrCpy( NULL, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_UNKNOWN ); // -MALLOC
								}
								else {
									Description_value = ULStrCpy( NULL, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_UNKNOWN ); // -MALLOC
								}
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "SPVDBMUpdateDeviceStatus" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							// --- Preparo la query ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_UPDATE_device_status_01( query, query_len, DevID_64_value, SevLevel_value, Description_value, Offline_value );
						}
						catch(...)
						{
							_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "SPVDBMUpdateDeviceStatus" );
						}
						// --- Lancio la query ---
						try
						{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "SPVDBMUpdateDeviceStatus" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "SPVDBMUpdateDeviceStatus" );
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "SPVDBMUpdateDeviceStatus" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( Description_value 	!= NULL )	free( Description_value	); // -FREE
						if ( DevID_64_value 	!= NULL ) 	free( DevID_64_value 	); // -FREE
						if ( SevLevel_value 	!= NULL )	free( SevLevel_value 	); // -FREE
						if ( Offline_value 		!= NULL )	free( Offline_value  	); // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "SPVDBMUpdateDeviceStatus" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "SPVDBMUpdateDeviceStatus" );
				}
				// --- Controlli finali sulla connessione DB ---
				_FinishConnCheck( db_conn );
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "SPVDBMUpdateDeviceStatus" );
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...){
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "SPVDBMUpdateDeviceStatus" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare le porte di comunicazione rimosse
///
/// \date [20.12.2007]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	_DeleteRemovedPorts( SPV_DBI_CONN * db_conn )
{
	int    		ret_code 		= SPV_DBM_NO_ERROR				;
	char       	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int        	query_len       = 0                           	; // Lunghezza stringa query

	// --- Controlli iniziali sulla connessione DB ---
	ret_code = _StartConnCheck( db_conn );

	if ( ret_code == SPV_DBM_NO_ERROR )
	{
		try
		{
			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_DELETE_port_01( query, query_len );
			// --- Lancio la query riga ---
			ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			// --- Se la query non � andata a buon fine ---
			if ( ret_code != SPV_DBM_NO_ERROR )
			{
				_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, query, "_DeleteRemovedPorts" );
				ret_code = SPV_DBM_DB_QUERY_FAILURE;
			}
		}
		catch(...)
		{
			ret_code = SPV_DBM_FUNCTION_EXCEPTION;
			_Log( db_conn, 2, ret_code, "Eccezione generale", "_DeleteRemovedPorts" );
		}
		// --- Controlli finali sulla connessione DB ---
		_FinishConnCheck( db_conn );
	}
	else
	{
		_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteRemovedPorts" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per aggiornare uno stream di periferica esistente
///
/// \date [09.07.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	_UpdateStream(	SPV_DBI_CONN * db_conn, void * pdevice, void * pstream )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			    = (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM*			stream			    = (SPV_DEV_STREAM*)pstream		;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len           = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64            = 0                           	;
	unsigned __int8         StrID_8             = 0                           	;
	char                  * DevID_64_value      = NULL                        	;
	char                  * StrID_value         = NULL                        	;
	char*					Name_value		    = NULL                          ;
	char*					Data_value		    = NULL                          ;
	int						Data_len			= 0								;
	//char*					DateTime_value	    = NULL                          ;
	char*					SevLevel_value	    = NULL                          ;
	char*					Description_value	= NULL                          ;
	char*					Visible_value		= NULL							;

	try
	{
		if ( SPVValidDevice( device ) ) {
			if ( SPVValidDevStream( stream ) ) {
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Recupero valori ---
						try
						{
							DevID_64        = (unsigned __int64)device->DevID;
							StrID_8         = (unsigned __int8)stream->ID;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_UpdateStream" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								DevID_64_value  	= XMLType2ASCII( (void*)&DevID_64, t_u_int_64 );	// -MALLOC
								StrID_value     	= XMLType2ASCII( (void*)&StrID_8, t_u_int_8 );		// -MALLOC
								Name_value		    = stream->Name;
								Data_value			= (char*)(void*)stream->Buffer.Ptr;
								Data_len			= stream->Buffer.Len;
								Description_value	= stream->Description;
								SevLevel_value  	= XMLType2ASCII( &stream->SevLevel, t_u_int_8 );	// -MALLOC
								//DateTime_value		= SPVDBGetTime( 0 ); // -MALLOC
								if ( stream->Visible ) {
									Visible_value = XMLStrCpy( NULL, "1" ); // -MALLOC
								}
								else{
									Visible_value = XMLStrCpy( NULL, "0" ); // -MALLOC
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UpdateStream" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								// --- Preparo la query per modificare lo stream ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_UPDATE_streams_03( query, query_len, DevID_64_value, StrID_value, Name_value, NULL, 0, SevLevel_value, Description_value, Visible_value );
							}
							catch(...)
							{
								_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_UpdateStream" );
							}
							// --- Lancio la query per modificare lo stream ---
							try
							{
								ret_code = SPVDBQuery( db_conn, query, query_len, Data_value, Data_len );
								if ( ret_code != SPV_DBM_NO_ERROR )
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UpdateStream" );
									ret_code = SPV_DBM_DB_QUERY_FAILURE;
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_UpdateStream" );
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UpdateStream" );
						}
						// --- Cancello i valori ---
						try
						{
							if ( DevID_64_value != NULL )	free( DevID_64_value	); // -FREE
							if ( StrID_value 	!= NULL )	free( StrID_value 		); // -FREE
							if ( SevLevel_value != NULL )	free( SevLevel_value	); // -FREE
							//if ( DateTime_value != NULL )	free( DateTime_value	); // -FREE
							if ( Visible_value 	!= NULL )	free( Visible_value		); // -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_UpdateStream" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione generale - L2", "_UpdateStream" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UpdateStream" );
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_STREAM;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...){
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdateStream" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DataBase il valore di un campo di stream di periferica
///
/// \date [20.12.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int _SaveStreamField( SPV_DBI_CONN * db_conn, void * pdevice, int strid, void * pfield )
{
	int						ret_code			= SPV_APPLIC_FUNCTION_SUCCESS 	; // Codice di ritorno
	SPV_DEVICE 			  * device				= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM_FIELD  * field				= (SPV_DEV_STREAM_FIELD*)pfield	;
	int						field_capacity		= 0                           	;
	int						row_num				= 0								;
	SPV_DBI_FIELD         * table_field			= NULL                          ; // Ptr a struttura field
	char                  * field_value			= NULL                          ;
	char                    query				[SPV_DBI_QUERY_MAX_LENGTH]    	; // Stringa query
	int                     query_len			= 0                             ; // Lunghezza stringa query

	unsigned __int64		DevID_64			= 0                             ;
	unsigned __int8			StrID_8				= 0                             ;
	unsigned __int16		FieldID_16			= 0                             ;
	unsigned __int16		ArrayID_16			= 0                             ;
	unsigned __int8			SevLevel_8			= 0                             ;
	unsigned __int8			Visible_8			= 0                             ;
	unsigned __int16		FieldCapacity_16	= 0								;

	char                  * DevID_64_value      = NULL                         	;
	char                  * StrID_value         = NULL                         	;
	char                  * FieldID_value       = NULL                         	;
	char                  * ArrayID_value       = NULL                         	;
	char				  * FieldCapacity_value	= NULL							;
	char                  * Name_value          = NULL                         	;
	char                  * SevLevel_value      = NULL                         	;
	char                  * Description_value   = NULL                         	;
	char                  * Visible_value       = NULL                         	;
	char				  * RefID_value			= NULL							;

	if ( SPVValidDevice( device ) ) // Controllo device
	{
		if ( SPVValidDevField( field ) ) // Controllo field
		{
			if ( strid > 0 )
			{
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
    				// --- Recupero i parametri del field ---
    				try
    				{
    					field_capacity  = field->Capacity;
    				}
    				catch(...)
    				{
    					field_capacity = 0;
						_Log( db_conn, 2, SPV_DBM_INVALID_FIELD_CAPACITY, "Eccezione durante il recupero della capacit� del campo", "_SaveStreamField" );
    				}
    				// --- Se la capacita' del campo e' valida ---
    				if ( field_capacity > 0 )
					{
						// --- Recupero valori ---
						try
						{
							DevID_64        	= (unsigned __int64)device->DevID;
							StrID_8         	= (unsigned __int8)strid;
							FieldID_16      	= (unsigned __int16)field->ID;
							FieldCapacity_16	= (unsigned __int16)field_capacity;
							Visible_8       	= (unsigned __int8)field->Visible;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_SaveStreamField" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								DevID_64_value  	= XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
								StrID_value     	= XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); 		// -MALLOC
								FieldID_value   	= XMLType2ASCII( (void*)&FieldID_16, t_u_int_16 ); 	// -MALLOC
								FieldCapacity_value = XMLType2ASCII( (void*)&FieldCapacity_16, t_u_int_16 ); 	// -MALLOC
								Visible_value   	= XMLType2ASCII( (void*)&Visible_8, t_u_int_8 ); 	// -MALLOC
								Name_value			= field->Name;
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveStreamField" );
							}
						}
						try
						{
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								// --- Preparo la query per ottenere le righe del field ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_SELECT_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value );
								// --- Lancio la query ---
								ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
								//* DEBUG */ _Log( db_conn, 1, 1111, query, "_SaveStreamField" );
								// --- Se la query � andata a buon fine senza errori ---
								if ( ret_code == SPV_DBM_NO_ERROR )
								{
									//* DEBUG */ _Log( db_conn, 1, 1121, "DEBUG", "_SaveStreamField" );
									// --- Ottengo il numero di righe dal risultato della query ---
									row_num = SPVDBGetRowNum( db_conn );
									// --- Caso UPDATE ---
									if ( field_capacity <= row_num )
									{
										//* DEBUG */ _Log( db_conn, 1, 1131, "DEBUG", "_SaveStreamField" );
										// --- Ho delle righe orfane da eliminare ---
										if ( field_capacity != row_num )
										{
											//* DEBUG */ _Log( db_conn, 1, 1132, "DEBUG", "_SaveStreamField" );
											// --- Ciclo di preparazione query DELETE reference field orfani ---
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											for ( int r = 0; r < row_num; r++ )
											{
												RefID_value = NULL;
												table_field = SPVDBGetField( db_conn, r , 0 );
												if ( table_field != NULL ) RefID_value = SPVDBGetUniqueID( table_field->Data ); // -MALLOC
												if ( RefID_value != NULL )
												{
													// --- Aggiungo la query per cancellare ogni singolo reference ---
													query_len = _SQL_DELETE_reference_01( query, query_len, RefID_value );
												}
												else
												{

												}
												try
												{
													if (RefID_value != NULL) free(RefID_value);
												}
												catch(...)
												{
													/* LOG */
												}
											}
											// --- Lancio la query ---
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											//* DEBUG */ _Log( db_conn, 1, 1113, query, "_SaveStreamField" );
											if ( ret_code != SPV_DBM_NO_ERROR )	{
												_Log( db_conn, 2, ret_code, query, "_SaveStreamField" );
											}
											// --- Cancellazione delle righe orfane ---
											// --- Preparo la query per cancellare solo le righe orfane ---
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											query_len = _SQL_DELETE_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value, FieldCapacity_value );
											// --- Lancio la query ---
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											//* DEBUG */ _Log( db_conn, 2, 1400, query, "_SaveStreamField" );
											if ( ret_code != SPV_DBM_NO_ERROR ) {
												_Log( db_conn, 2, ret_code, query, "_SaveStreamField" );
											}
											// --- Preparo la query per togliere il link ai reference cancellati ---
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											query_len = _SQL_UPDATE_stream_fields_02( query, query_len, DevID_64_value, StrID_value, FieldID_value );
											// --- Lancio la query ---
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR ) {
												_Log( db_conn, 2, ret_code, query, "_SaveStreamField" );
											}
										}
										if ( ret_code == SPV_DBM_NO_ERROR )
										{
											//* DEBUG */ _Log( db_conn, 1, 1133, "DEBUG", "_SaveStreamField" );
											// --- Ciclo di preparazione query di UPDATE ---
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											for ( int c = 0; c < field_capacity; c++ )
											{
												//* DEBUG */ _Log( db_conn, 1, 1150 + c, "DEBUG", "_SaveStreamField" );
												// --- Preperazione valori specifici ---
												try
												{
													ArrayID_16      = (unsigned __int16)c;
													SevLevel_8      = (unsigned __int8)field->SevLevel[c];
												}
												catch(...)
												{
													_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante l recupero dei valori specifici", "_SaveStreamField" );
												}
												// --- Coversione in formato ASCII ---
												try
												{
													ArrayID_value     = XMLType2ASCII( (void*)&ArrayID_16, t_u_int_16 );	// -MALLOC
													SevLevel_value    = XMLType2ASCII( (void*)&SevLevel_8, t_u_int_8 );		// -MALLOC
													Description_value = field->Descr[c];
												}
												catch(...)
												{
													_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la conversione ASCII dei valori specifici", "_SaveStreamField" );
												}
												if ( ret_code == SPV_DBM_NO_ERROR )
												{
													try
													{
														// --- Converto il campo in un formato stampabile ---
														field_value = SPVPrintableFieldConv( field, c ); // -MALLOC
														//* DEBUG */ _Log( db_conn, 1, 1160 + c, ArrayID_value, Visible_value );
														if ( field_value != NULL )
														{
															// --- Aggiungo la query per fare l'UPDATE di ogni singola riga ---
															query_len = _SQL_UPDATE_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value, ArrayID_value,
																Name_value, SevLevel_value, field_value, Description_value, Visible_value );
														}
														else
														{
															// --- Aggiungo la query per fare l'UPDATE di ogni singola riga ---
															query_len = _SQL_UPDATE_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value, ArrayID_value,
																Name_value, "255", "n/d", "Valore non ricevuto", Visible_value );
														}
														//* DEBUG */ _Log( db_conn, 2, 1134, query, "_SaveStreamField" );
													}
													catch(...)
													{
														_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la preparazione della query", "_SaveStreamField" );
													}
												}
												else
												{

												}
												// --- Cancello i valori specifici ---
												try
												{
													if ( ArrayID_value 	!= NULL )	free( ArrayID_value		);
													if ( SevLevel_value != NULL )	free( SevLevel_value	);
													if ( field_value 	!= NULL )	free( field_value		);
												}
												catch(...)
												{
													_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione dei valori specifici", "_SaveStreamField" );
												}
											}
											// --- Lancio la query di UPDATE ---
											//* DEBUG */ _Log( db_conn, 2, 2222, query, "_SaveStreamField" );
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											//* DEBUG */ _Log( db_conn, 1, 1115, query, "_SaveStreamField" );
											if ( ret_code != SPV_DBM_NO_ERROR )	{
												_Log( db_conn, 2, ret_code, query, "_SaveStreamField" );
												/// Link REFERENCE ?
											}
										}
									}
									// --- Caso INSERT ---
									else
									{
										//* DEBUG */ _Log( db_conn, 1, 1122, "DEBUG", "_SaveStreamField" );
										// --- Se esistono alcune righe (da eliminare) riguardo al campo ---
										if ( row_num > 0 )
										{
											//* DEBUG */ _Log( db_conn, 2, 1401, "ROW > 0", "_SaveStreamField" );
											// --- Ciclo di preparazione query DELETE reference field orfani ---
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											for ( int r = 0; r < row_num; r++ )
											{
												RefID_value = NULL;
												table_field = SPVDBGetField( db_conn, r , 0 );
												if ( table_field != NULL ) RefID_value = SPVDBGetUniqueID( table_field->Data ); // -MALLOC
												if ( RefID_value != NULL )
												{
													// --- Aggiungo la query per cancellare ogni singolo reference ---
													query_len = _SQL_DELETE_reference_01( query, query_len, RefID_value );
												}
												else
												{

												}
												try
												{
													if ( RefID_value != NULL) free( RefID_value );
												}
												catch(...)
												{

												}
											}
											// --- Lancio la query ---
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											//* DEBUG */ _Log( db_conn, 1, 1113, query, "_SaveStreamField" );
											if ( ret_code != SPV_DBM_NO_ERROR )	{
												_Log( db_conn, 2, ret_code, query, "_SaveStreamField" );
											}
											// --- Preparo la query per cancellare solo le righe orfane ---
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											query_len = _SQL_DELETE_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value, "0" );
											// --- Lancio la query ---
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											//* DEBUG */ _Log( db_conn, 2, 1405, query, "_SaveStreamField" );
											if ( ret_code != SPV_DBM_NO_ERROR ) {
												_Log( db_conn, 2, ret_code, query, "_SaveStreamField" );
											}
										}
										else
										{
											//* DEBUG */ _Log( db_conn, 2, 1402, "ROW == 0", "_SaveStreamField" );
										}
										//* DEBUG */ _Log( db_conn, 1, 1123, "DEBUG", "_SaveStreamField" );
										// --- Ciclo di preparazione query di INSERT ---
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										for ( int c = 0; c < field_capacity; c++ )
										{
											// --- Preperazione valori specifici ---
											try
											{
												ArrayID_16      = (unsigned __int16)c;
												SevLevel_8      = (unsigned __int8)field->SevLevel[c];
											}
											catch(...)
											{
												_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante l recupero dei valori specifici", "_SaveStreamField" );
											}
											// --- Coversione in formato ASCII ---
											try
											{
												ArrayID_value     = XMLType2ASCII( (void*)&ArrayID_16, t_u_int_16 );	// -MALLOC
												SevLevel_value    = XMLType2ASCII( (void*)&SevLevel_8, t_u_int_8 );		// -MALLOC
												Description_value = field->Descr[c];
											}
											catch(...)
											{
												_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la conversione ASCII dei valori specifici", "_SaveStreamField" );
											}
											if ( ret_code == SPV_DBM_NO_ERROR )
											{
												//* DEBUG */ _Log( db_conn, 1, 1300 + c, query, "_SaveStreamField" );
												try
												{
													// --- Converto il campo in un formato stampabile ---
													field_value = SPVPrintableFieldConv( field, c ); // -MALLOC
													if ( field_value != NULL )
													{
														// --- Aggiungo la query per fare l'INSERT di ogni singola riga ---
														query_len = _SQL_INSERT_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value, ArrayID_value,
															Name_value, SevLevel_value, field_value, Description_value, Visible_value );
													}
													else
													{
														// --- Aggiungo la query per fare l'INSERT di ogni singola riga ---
														query_len = _SQL_INSERT_stream_fields_01( query, query_len, DevID_64_value, StrID_value, FieldID_value, ArrayID_value,
															Name_value, "255", "n/d", "Valore non ricevuto", Visible_value );
													}
												}
												catch(...)
												{
													_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di creazione query INSERT", "_SaveStreamField" );
												}
											}
											else
											{

											}
											// --- Cancello i valori specifici ---
											try
											{
												if ( ArrayID_value 	!= NULL )	free( ArrayID_value		); // -FREE
												if ( SevLevel_value != NULL )	free( SevLevel_value	); // -FREE
												if ( field_value 	!= NULL )	free( field_value		); // -FREE
											}
											catch(...)
											{
												_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione dei valori specifici", "_SaveStreamField" );
											}
										}
										// --- Lancio la query di INSERT ---
										ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
										//* DEBUG */ _Log( db_conn, 1, 1116, query, "_SaveStreamField" );
										if ( ret_code != SPV_DBM_NO_ERROR )	{
											/// Link REFERENCE ?
										}
									}
								}
								else
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveStreamField" );
									ret_code = SPV_APPLIC_SELECT_QUERY_FAILURE;
								}
							}
							else
							{
								_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveStreamField" );
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione generale", "_SaveStreamField" );
						}
						// --- Cancello i valori comuni ---
						try
						{
							if ( DevID_64_value			!= NULL	)	free( DevID_64_value 		);	// -FREE
							if ( StrID_value			!= NULL )	free( StrID_value 			);	// -FREE
							if ( FieldID_value			!= NULL )	free( FieldID_value 		);	// -FREE
							if ( FieldCapacity_value	!= NULL )	free( FieldCapacity_value 	);	// -FREE
							if ( Visible_value			!= NULL )	free( Visible_value 		);	// -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori comuni", "_SaveStreamField" );
						}
						// --- Controlli finali sulla connessione DB ---
						ret_code = _FinishConnCheck( db_conn );
					}
					else
					{
						ret_code = SPV_DBM_INVALID_FIELD_CAPACITY;
					}
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_STREAM_ID;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_FIELD;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per verificare su esiste il valore di riferimento di un campo
///
/// \date [20.12.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
int	_CheckForReference(SPV_DBI_CONN * db_conn, void * pdevice, int strid, void * pfield, int arrayid, char ** refid )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE * 			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM_FIELD *	field 			= (SPV_DEV_STREAM_FIELD*)pfield	;
	int                 	field_capacity  = 0                           	;
	SPV_DBI_FIELD * 		table_field     = NULL                        	; // Ptr a struttura field
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query

	unsigned __int64        DevID_64        = 0                           	;
	unsigned __int8         StrID_8         = 0                           	;
	unsigned __int16        FieldID_16      = 0                           	;
	unsigned __int16        ArrayID_16      = 0                           	;

	char * 					DevID_64_value  = NULL                        	;
	char * 					StrID_value     = NULL                        	;
	char *                 	FieldID_value   = NULL                        	;
	char *                  ArrayID_value   = NULL                        	;

	#define _LOCAL_DATA_START   "=\'"
	#define _LOCAL_DATA_STOP    "\'"

	if ( SPVValidDevice( device ) )
	{
		if ( SPVValidDevField( field ) )
		{
			if ( strid > 0 )
			{
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Recupero i parametri del field ---
					try
					{
						field_capacity  = field->Capacity;
					}
					catch(...)
					{
						field_capacity = 0;
						_Log( db_conn, 2, SPV_DBM_INVALID_FIELD_CAPACITY, "Eccezione durante il recupero della capacit� del campo", "SPVCheckForReference" );
					}
					// --- Se la capacita' del campo e' valida ---
					if ( field_capacity > 0 )
					{
						// --- Recupero valori ---
						try
						{
							DevID_64        = (unsigned __int64)device->DevID;
							StrID_8         = (unsigned __int8)strid;
							FieldID_16      = (unsigned __int16)field->ID;
							ArrayID_16      = (unsigned __int16)arrayid;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "SPVDBMCheckForReference" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							try
							{
								DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 		// -MALLOC
								StrID_value     = XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); 			// -MALLOC
								FieldID_value   = XMLType2ASCII( (void*)&FieldID_16, t_u_int_16 ); 	// -MALLOC
								ArrayID_value 	= XMLType2ASCII( (void*)&ArrayID_16, t_u_int_16 ); 	// -MALLOC
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "SPVDBMCheckForReference" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							// --- Preparo la query per vedere se i field sono gi� presenti nel database ---
							query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
							query_len = _SQL_SELECT_stream_fields_03( query, query_len, DevID_64_value, StrID_value, FieldID_value, ArrayID_value );
							// --- Lancio la query ---
							ret_code = SPVDBQuery( db_conn, query, 0, NULL, 0 );
							// --- Se la query non � andata a buon fine senza errori (la riga potrebbe non esistere) ---
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								table_field = SPVDBGetField( db_conn, 0, 0 );
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( table_field != NULL )
								{
									//* DEBUG */ _Log( db_conn, 2, 2001, "DEBUG 1", "SPVDBMCheckForReference" );
									// --- Se voglio estrarre il RefID in una stringa ---
									if ( refid != NULL )
									{
										try
										{
											if ( table_field->Data != NULL)
											{
												//* DEBUG */ _Log( db_conn, 2, 4004, table_field->Data, "SPVDBMCheckForReference" );
												if ( strcmpi( table_field->Data, "0" ) == 0 ) {
													ret_code = SPV_DBM_REFERENCE_NOT_FOUND;
												}
												else
												{
													// --- Estraggo il ReferenceID appena trovato ---
													*refid = ULStrCpy( NULL, table_field->Data );
													ret_code = SPV_DBM_REFERENCE_FOUND;
												}
											}
											else
											{
												//* DEBUG */ _Log( db_conn, 2, 2003, "DEBUG 3", "SPVDBMCheckForReference" );
												ret_code = SPV_DBM_REFERENCE_NOT_FOUND;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione durante l\'estrazione del ReferenceID", "SPVDBMCheckForReference" );
										}
									}
								}
								else
								{
									//* DEBUG */ _Log( db_conn, 2, 2004, "DEBUG 4", "SPVDBMCheckForReference" );
									ret_code = SPV_DBM_REFERENCE_NOT_FOUND;
								}
							}
							else
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "SPVDBMCheckForReference" );
								ret_code = SPV_APPLIC_SELECT_QUERY_FAILURE;
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "SPVDBMCheckForReference" );
						}
						// --- Cancello i valori ---
						try
						{
							free( DevID_64_value ); // -FREE
							free( StrID_value );    // -FREE
							free( FieldID_value );  // -FREE
							free( ArrayID_value );  // -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "SPVDBMCheckForReference" );
						}
						// --- Controlli finali sulla connessione DB ---
						_FinishConnCheck( db_conn );
					}
					else
					{
						ret_code = SPV_DBM_INVALID_FIELD_CAPACITY;
					}
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_STREAM_ID;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_FIELD;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per inserire un nuovo valore di riferimento di un campo
///
/// \date [20.12.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int	_InsertReference( SPV_DBI_CONN * db_conn, void * pdevice, void * pfield, int arrayid, char * dstrid, char * dfieldid, char * darrayid, char ** refid )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE 			  * device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM_FIELD  * field 			= (SPV_DEV_STREAM_FIELD*)pfield	;
	int                 	field_capacity  = 0                           	;
	SPV_DBI_FIELD     	  * table_field     = NULL                        	; // Ptr a struttura field
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int64		DevID_64		= 0								;
	unsigned __int8         Visible_8       = 0                             ;
	char*					DevID_value		= NULL							;
	char                  * Visible_value   = NULL                          ;
	char                  * field_value     = NULL                          ;

	if ( SPVValidDevice( device ) )
	{
		if ( SPVValidDevField( field ) )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
    			// --- Recupero i parametri del field ---
    			try
    			{
    				field_capacity  = field->Capacity;
    			}
    			catch(...)
    			{
    				field_capacity = 0;
    				_Log( db_conn, 2, SPV_DBM_INVALID_FIELD_CAPACITY, "Eccezione durante il recupero della capacit� del campo", "_InsertReference" );
    			}
    			// --- Se la capacita' del campo e' valida ---
    			if ( field_capacity > 0 )
				{
					// --- Recupero valori ---
					try
					{
						DevID_64        = (unsigned __int64)device->DevID;
						Visible_8       = (unsigned __int8)field->Visible;
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_InsertReference" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						try
						{
							DevID_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
							Visible_value   = XMLType2ASCII( (void*)&Visible_8, t_u_int_8 ); 	// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_InsertReference" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						// --- Converto il campo in un formato stampabile ---
						field_value = SPVPrintableFieldConv( field, arrayid ); // -MALLOC
						if ( field_value == NULL )
						{
							ret_code = SPV_DBM_INVALID_CONVERTED_FIELD_VALUE;
							_Log( db_conn, 2, ret_code, "Valore del campo convertito non valido", "_InsertReference" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						// --- Preparo la query per inserire nuove reference ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						query_len = _SQL_INSERT_reference_01( query, query_len, field_value, Visible_value, DevID_value, dstrid, dfieldid, darrayid );
						//* DEBUG */ _Log( db_conn, 2, 2011, query, "_InsertReference" );
						// --- Lancio la query per inserzione di una nuova riga ---
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						///ret_code = SPVDBWriteRow( db_conn, "reference", _LOCAL_COLUMNS, query, query_len );
						// --- Se la query non � andata a buon fine senza errori (la riga potrebbe non esistere) ---
						if ( ret_code != SPV_DBM_NO_ERROR )
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_InsertReference" );
							ret_code = SPV_APPLIC_SELECT_QUERY_FAILURE;
						}
						else
						{
							table_field = SPVDBGetField( db_conn, 0, 0 );
							// --- Se la riga � gi� presente nella tabella del DB ---
							if ( table_field != NULL )
							{
								// --- Se voglio estrarre il RefID in una stringa ---
								if ( refid != NULL )
								{
									try
									{
										if ( table_field->Data != NULL)
										{
											//* DEBUG */ _Log( db_conn, 2, 2014, table_field->Data, "_InsertReference" );
											if ( strcmpi( table_field->Data, "0" ) == 0 ) {
												ret_code = SPV_DBM_REFERENCE_NOT_FOUND;
											}
											else
											{
												// --- Estraggo il ReferenceID appena trovato ---
												*refid = ULStrCpy( NULL, table_field->Data );
											}
										}
										else
										{
											//* DEBUG */ _Log( db_conn, 2, 2015, "DEBUG 3", "_InsertReference" );
											ret_code = SPV_DBM_REFERENCE_NOT_FOUND;
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione durante l\'estrazione del ReferenceID", "_InsertReference" );
									}
								}
							}
							else
							{
								//* DEBUG */ _Log( db_conn, 2, 2004, "DEBUG 4", "_InsertReference" );
								ret_code = SPV_DBM_REFERENCE_NOT_FOUND;
							}
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_InsertReference" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( DevID_value != NULL )      free( DevID_value ); // -FREE
						if ( Visible_value != NULL ) 	free( Visible_value );	// -FREE
						if ( field_value != NULL )		free( field_value ); 	// -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_InsertReference" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					ret_code = SPV_DBM_INVALID_FIELD_CAPACITY;
				}
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_FIELD;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per collegare un valore di riferimento ad un campo
///
/// \date [20.12.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	_LinkReference( SPV_DBI_CONN * db_conn, void * pdevice, int strid, void * pfield, int arrayid, char * refid )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE 			  * device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM_FIELD  * field 			= (SPV_DEV_STREAM_FIELD*)pfield	;
	int                 	field_capacity  = 0                           	;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query

	unsigned __int64        DevID_64        = 0                           	;
	unsigned __int8         StrID_8         = 0                           	;
	unsigned __int16        FieldID_16      = 0                           	;
	unsigned __int16        ArrayID_16      = 0                           	;

	char                  * DevID_64_value  = NULL                        	;
	char                  * StrID_value     = NULL                        	;
	char                  * FieldID_value   = NULL                        	;
	char                  * ArrayID_value   = NULL                        	;
	char				  * RefID_value		= NULL;

	if ( SPVValidDevice( device ) )
	{
		if ( SPVValidDevField( field ) )
		{
			if ( strid > 0 )
			{
				if ( refid != NULL )
				{
					// --- Controlli iniziali sulla connessione DB ---
					ret_code = _StartConnCheck( db_conn );
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
    					// --- Recupero i parametri del field ---
    					try
    					{
    						field_capacity  = field->Capacity;
    					}
    					catch(...)
    					{
    						field_capacity = 0;
							_Log( db_conn, 2, SPV_DBM_INVALID_FIELD_CAPACITY, "Eccezione durante il recupero della capacit� del campo", "_LinkReference" );
    					}
    					// --- Se la capacita' del campo e' valida ---
    					if ( field_capacity > 0 )
						{
							// --- Recupero valori ---
							try
							{
								DevID_64        = (unsigned __int64)device->DevID;
								StrID_8         = (unsigned __int8)strid;
								FieldID_16      = (unsigned __int16)field->ID;
								ArrayID_16      = (unsigned __int16)arrayid;
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_LinkReference" );
							}
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								try
								{
									DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
									StrID_value     = XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); 		// -MALLOC
									FieldID_value   = XMLType2ASCII( (void*)&FieldID_16, t_u_int_16 ); 	// -MALLOC
									ArrayID_value 	= XMLType2ASCII( (void*)&ArrayID_16, t_u_int_16 ); 	// -MALLOC
									RefID_value		= SPVDBGetUniqueID( refid ); // -MALLOC
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_LinkReference" );
								}
							}
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								// --- Preparo la query per linkare il reference ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_UPDATE_stream_fields_03( query, query_len, RefID_value, DevID_64_value, StrID_value, FieldID_value, ArrayID_value );
								// --- Lancio la query ---
								ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
								// --- Se la query non � andata a buon fine senza errori (la riga potrebbe non esistere) ---
								if ( ret_code != SPV_DBM_NO_ERROR )
								{
									_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_LinkReference" );
									ret_code = SPV_APPLIC_SELECT_QUERY_FAILURE;
								}
							}
							else
							{
								_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_LinkReference" );
							}
							// --- Cancello i valori ---
							try
							{
								free( DevID_64_value ); // -FREE
								free( StrID_value );    // -FREE
								free( FieldID_value );  // -FREE
								free( ArrayID_value );  // -FREE
								if ( RefID_value != NULL ) free( RefID_value ); // -FREE
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_LinkReference" );
							}
							// --- Controlli finali sulla connessione DB ---
							_FinishConnCheck( db_conn );
						}
						else
						{
							ret_code = SPV_DBM_INVALID_FIELD_CAPACITY;
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_LinkReference" );
					}
				}
				else
				{
					ret_code = SPV_DBM_INVALID_REFERENCE_ID;
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_STREAM_ID;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_FIELD;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per cancellare un valore di riferimento di un campo
///
/// \date [17.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	_DeleteReference(SPV_DBI_CONN * db_conn, void * pdevice, char * refid )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			= (SPV_DEVICE*)pdevice			;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	char*					RefID_value		= NULL;

	if ( SPVValidDevice( device ) ) {
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			// --- Recupero valori ---
			//* DEBUG */ _Log( db_conn, 2, 2008, "DEBUG 8", "_DeleteReference" );
			try
			{
				RefID_value		= SPVDBGetUniqueID( refid ); // -MALLOC
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante la conversione a uniqueid", "_DeleteReference" );
			}
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				// --- Preparo la query per eliminare il reference ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
				query_len = _SQL_DELETE_reference_01( query, query_len, RefID_value );
				// --- Lancio la query per inserzione di una nuova riga ---
				ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
				// --- Se la query non � andata a buon fine senza errori (la riga potrebbe non esistere) ---
				if ( ret_code != SPV_DBM_NO_ERROR )
				{
					_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, refid, "_DeleteReference" );
					ret_code = SPV_DBM_DB_QUERY_FAILURE;
				}
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_DeleteReference" );
			}
			// --- Cancello i valori ---
			try
			{
				if ( RefID_value != NULL )		free( RefID_value ); 	// -FREE
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_DeleteReference" );
			}
			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteReference" );
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
// Funzione per scollegare un valore di riferimento da un campo
///
/// \date [14.04.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	_UnLinkReference(SPV_DBI_CONN * db_conn, void * pdevice, int strid, void * pfield, int arrayid )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM_FIELD *  field 			= (SPV_DEV_STREAM_FIELD*)pfield	;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query

	unsigned __int64        DevID_64        = 0                           	;
	unsigned __int8         StrID_8         = 0                           	;
	unsigned __int16        FieldID_16      = 0                           	;
	unsigned __int16        ArrayID_16      = 0                           	;

	char *                  DevID_64_value  = NULL                        	;
	char *                  StrID_value     = NULL                        	;
	char *                  FieldID_value   = NULL                        	;
	char *                  ArrayID_value   = NULL                        	;

	if ( SPVValidDevice( device ) ) {
		if ( strid > 0 ) {
			if ( SPVValidDevField( field ) ) {
				// --- Controlli iniziali sulla connessione DB ---
				ret_code = _StartConnCheck( db_conn );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Recupero valori ---
					try
					{
						DevID_64        = (unsigned __int64)device->DevID;
						StrID_8         = (unsigned __int8)strid;
						FieldID_16      = (unsigned __int16)field->ID;
						ArrayID_16      = (unsigned __int16)arrayid;
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_UnLinkReference" );
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						//* DEBUG */ _Log( db_conn, 2, 2034, "DEBUG", "_UnLinkReference" );
						try
						{
							DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
							StrID_value     = XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); 		// -MALLOC
							FieldID_value   = XMLType2ASCII( (void*)&FieldID_16, t_u_int_16 ); 	// -MALLOC
							ArrayID_value 	= XMLType2ASCII( (void*)&ArrayID_16, t_u_int_16 ); 	// -MALLOC
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UnLinkReference" );
						}
					}
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						// --- Preparo la query per eliminare il reference ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						query_len = _SQL_UPDATE_stream_fields_04( query, query_len, DevID_64_value, StrID_value, FieldID_value, ArrayID_value );
						// --- Lancio la query per inserzione di una nuova riga ---
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						// --- Se la query non � andata a buon fine senza errori (la riga potrebbe non esistere) ---
						if ( ret_code != SPV_DBM_NO_ERROR )
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UnLinkReference" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					else
					{
						_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UnLinkReference" );
					}
					// --- Cancello i valori ---
					try
					{
						if ( DevID_64_value != NULL )	free( DevID_64_value 	);	// -FREE
						if ( StrID_value != NULL )		free( StrID_value 		);	// -FREE
						if ( FieldID_value != NULL )	free( FieldID_value 	);  // -FREE
						if ( ArrayID_value != NULL )	free( ArrayID_value 	);  // -FREE
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_UnLinkReference" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UnLinkReference" );
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_FIELD;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_STREAM_ID;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare un valore di riferimento di un campo
///
/// \date [14.04.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
/*int	SPVDBMUpdateReference( void * pdevice, void * pfield, int arrayid, char * refid )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE 			  * device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM_FIELD  * field 			= (SPV_DEV_STREAM_FIELD*)pfield	;
	int                 	field_capacity  = 0                           	;
	SPV_DBI_CONN      	  * db_conn         = NULL                        	; // Ptr a struttura connessione
	SPV_DBI_FIELD     	  * table_field     = NULL                        	; // Ptr a struttura field
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int8         Visible_8       = 0                             ;
	char                  * Visible_value   = NULL                          ;
	char                  * field_value     = NULL                          ;
	char*					RefID_value		= NULL							;

	#define _LOCAL_DATA_START   "=\'"
	#define _LOCAL_DATA_STOP    "\'"

	if ( SPVValidDevice( device ) )
	{
		if ( SPVValidDevField( field ) )
		{
			if ( refid != NULL )
			{
				// --- Recupero i parametri del field ---
				try
				{
					field_capacity  = field->Capacity;
				}
				catch(...)
				{
					field_capacity = 0;
					_Log( NULL, 2, SPV_DBM_INVALID_FIELD_CAPACITY, "Eccezione durante il recupero della capacit� del campo", "SPVDBMUpdateReference" );
				}
			    // --- Se la capacita' del campo e' valida ---
				if ( field_capacity > 0 )
			    {
					db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn; // Recupero la struttura connessione DB
					// --- Controlli iniziali sulla connessione DB ---
					ret_code = _StartConnCheck( db_conn );
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						_LockConn( db_conn );
						// --- Recupero valori ---
						try
						{
							Visible_8       = (unsigned __int8)field->Visible;
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "SPVDBMUpdateReference" );
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
			    			try
							{
								Visible_value   = XMLType2ASCII( (void*)&Visible_8, t_u_int_8 ); 	// -MALLOC
								RefID_value		= SPVDBGetUniqueID( refid ); // -MALLOC
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "SPVDBMUpdateReference" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							// --- Converto il campo in un formato stampabile ---
							field_value = SPVPrintableFieldConv( field, arrayid ); // -MALLOC
							if ( field_value == NULL )
							{
								ret_code = SPV_DBM_INVALID_CONVERTED_FIELD_VALUE;
								_Log( db_conn, 2, ret_code, "Valore del campo convertito non valido", "SPVDBMUpdateReference" );
							}
						}
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							// --- Preparo la query per aggiornare il reference ---
							query_len = SPVDBAddToQuery( query, query_len, "UPDATE reference SET " );
							query_len = SPVDBAddToQuery( query, query_len, "Value" );
							query_len = SPVDBAddToQuery( query, query_len, "\'" );
							query_len = SPVDBAddToQuery( query, query_len, field_value );
							query_len = SPVDBAddToQuery( query, query_len, "\'" );
							query_len = SPVDBAddToQuery( query, query_len, "," );
							query_len = SPVDBAddToQuery( query, query_len, "Visible" );
							query_len = SPVDBAddToQuery( query, query_len, "\'" );
							query_len = SPVDBAddToQuery( query, query_len, Visible_value );
							query_len = SPVDBAddToQuery( query, query_len, "\'" );
							query_len = SPVDBAddToQuery( query, query_len, " WHERE " );
							query_len = SPVDBAddToQuery( query, query_len, "ReferenceID" );
							query_len = SPVDBAddToQuery( query, query_len, "\'=" );
							query_len = SPVDBAddToQuery( query, query_len, RefID_value );
							query_len = SPVDBAddToQuery( query, query_len, "\'\n" );
							// --- Lancio la query ---
							ret_code = SPVDBQuery( db_conn, query, 0, NULL, 0 );
							// --- Se la query non � andata a buon fine senza errori (la riga potrebbe non esistere) ---
							if ( ret_code != SPV_DBM_NO_ERROR )
							{
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "SPVDBMUpdateReference" );
								ret_code = SPV_APPLIC_SELECT_QUERY_FAILURE;
							}
						}
						else
						{
							_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "SPVDBMUpdateReference" );
						}
						// --- Cancello i valori ---
						try
						{
							if ( Visible_value != NULL ) 	free( Visible_value );  // -FREE
							if ( field_value != NULL ) 		free( field_value );	// -FREE
							if ( RefID_value != NULL ) 		free( RefID_value ); // -FREE
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "SPVDBMUpdateReference" );
						}
						_ReleaseConn( db_conn );
						// --- Controlli finali sulla connessione DB ---
						ret_code = _FinishConnCheck( db_conn );
					}
					else
					{
					  // --- Errore ---
					}
				}
				else
				{
					ret_code = SPV_DBM_INVALID_FIELD_CAPACITY;
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_REFERENCE_ID;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_FIELD;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}
*/

//==============================================================================
/// Funzione per eliminare tutti gli stream ed i relativi stream_fields e
/// reference associati al dispositivo
///
/// \date [09.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_DeleteAllStreams(SPV_DBI_CONN * db_conn, void * pdevice )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE* 			device 			= (SPV_DEVICE*)pdevice			;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64        = 0                           	;
	char                  * DevID_64_value  = NULL                        	;

	try
	{
		if ( SPVValidDevice( device ) )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione durante i controlli iniziali", "_DeleteAllStreams" );
	}

	if ( ret_code == SPV_DBM_NO_ERROR )
	{
		try
		{
			// --- Recupero valori ---
			try
			{
				DevID_64        = (unsigned __int64)device->DevID;
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_DeleteAllStreams" );
			}
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_DeleteAllStreams" );
				}
			}
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					// --- Preparo la query ---
					query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
					query_len = _SQL_DELETE_all_streams_by_device_01( query, query_len, DevID_64_value );
				}
				catch(...)
				{
					_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione in fase di preparazione della query", "_DeleteAllStreams" );
				}
				// --- Lancio la query ---
				try
				{
					ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
					if ( ret_code != SPV_DBM_NO_ERROR )
					{
						_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_DeleteAllStreams" );
						ret_code = SPV_DBM_DB_QUERY_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_DeleteAllStreams" );
				}
			}
			else
			{
				_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_DeleteAllStreams" );
			}
			// --- Cancello i valori ---
			try
			{
				if ( DevID_64_value != NULL )	free( DevID_64_value 	);	// -FREE
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_DeleteAllStreams" );
			}
		}
		catch(...)
		{
			ret_code = SPV_DBM_FUNCTION_EXCEPTION;
			_Log( db_conn, 2, ret_code, "Eccezione generale", "_DeleteAllStreams" );
		}

		// --- Controlli finali sulla connessione DB ---
		_FinishConnCheck( db_conn );
	}
	else
	{
		_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteAllStreams" );
	}

	return	ret_code;
}


//==============================================================================
/// Funzione per verificare la conformit� dei field di uno stream di una periferica
/// I field di uno stream di una data periferica presenti su db sono conformi se:
/// 1) il numero di record presenti sulla tabella "stream_fields" deve coincidere
///    col numero dei field caricati dalla configurazione per quella periferica;
/// 2) il numero dei record con stesso FieldID deve corrispondere al valore indicato
///	   nell'attributo "capacity" dello stesso field presente nella configurazione.
///
///  ATTENZIONE: non gestisce la connessione in quanto viene richiamata unicamente
///				 da _CheckStreamsConformity()
///
/// \date [20.12.2007]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	_CheckStreamFieldsConformity(SPV_DBI_CONN * db_conn, char * DevID_64_value, SPV_DEV_STREAM * pstream, bool * pconformity )
{
	int					ret_code			= SPV_DBM_NO_ERROR;
	SPV_DEV_STREAM * 	stream				= (SPV_DEV_STREAM*)pstream;
	char                query				[SPV_DBI_QUERY_MAX_LENGTH];		// Stringa query
	int                 query_len			= 0; 							// Lunghezza stringa query
	unsigned __int32	tempID_32			= 0;
	char *				StrID_32_value		= NULL;
	char *				FieldID_32_value	= NULL;
	int					num_field			= 0;
	int					row_num_total		= 0;
	int					row_num				= 0;
	int					f					= 0;
	int					capacity			= 0;
	SPV_DBI_FIELD *		table_field			= NULL; 						// Ptr a struttura field
	bool				conformity			= true;
	unsigned __int32	app_ui32			= 0;
	char *				app_ui32_value		= NULL;
	char *				app_msg				= NULL;

	try
	{
		try
		{
			// --- Recupero StrID
			tempID_32 = (unsigned __int32)stream->ID;
			StrID_32_value  = XMLType2ASCII( (void*)&tempID_32, t_u_int_32 ); 	// -MALLOC
			// --- Preparo la query ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = _SQL_SELECT_fields_01( query, query_len, DevID_64_value, StrID_32_value  );
		}
		catch(...)
		{
			ret_code = SPV_DBM_FUNCTION_EXCEPTION;
			_Log( db_conn, 2, ret_code, "Eccezione in fase di preparazione della query _SQL_SELECT_fields_01", "_CheckStreamFieldsConformity" );
		}

		if ( ret_code == SPV_DBM_NO_ERROR )
		{

			// --- Lancio la query ---
			try
			{
				ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Ottengo il numero di righe dal risultato della query ---
					row_num_total = SPVDBGetRowNum( db_conn );

					num_field = 0;
					for (f = 0; f < stream->FieldNum; f++)
					{
						if (&stream->FieldList[f] != NULL)
						{
							capacity = stream->FieldList[f].Capacity;
							// --- incremento il numero dei field caricati nella configurazione
							num_field += capacity;

							// --- verifico che il numero dei record con stesso FieldID corrisponde
							// --- al valore indicato nell'attributo "capacity" dello stesso field
							try
							{
								// --- Recupero FieldID
								tempID_32 = (unsigned __int32)stream->FieldList[f].ID;
								FieldID_32_value = XMLType2ASCII( (void*)&tempID_32, t_u_int_32 ); 	// -MALLOC
								// --- Preparo la query ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								query_len = _SQL_SELECT_fields_02( query, query_len, DevID_64_value, StrID_32_value, FieldID_32_value );
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione in fase di preparazione della query _SQL_SELECT_fields_02", "_CheckStreamFieldsConformity" );
							}

							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								// --- Lancio la query ---
								try
								{
									ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
									if ( ret_code == SPV_DBM_NO_ERROR )
									{
										// --- Ottengo il numero di righe dal risultato della query ---
										row_num = SPVDBGetRowNum( db_conn );
										if (row_num != capacity)
										{
											conformity = false;
											//* DEBUG
											app_ui32 = (unsigned __int32)row_num;
											app_ui32_value  = XMLType2ASCII( (void*)&app_ui32, t_u_int_32 ); 	// -MALLOC
											app_msg = ULAddText( app_msg, "DEBUG: row num (" );
											app_msg = ULAddText( app_msg, app_ui32_value );
											app_msg = ULAddText( app_msg, ") != capacity(" );
											ULFreeAndNullString(&app_ui32_value);
											app_ui32 = (unsigned __int32)capacity;
											app_ui32_value  = XMLType2ASCII( (void*)&app_ui32, t_u_int_32 ); 	// -MALLOC
											app_msg = ULAddText( app_msg, app_ui32_value );
											app_msg = ULAddText( app_msg, ")" );
											ULFreeAndNullString(&app_ui32_value);
											_Log( db_conn, 2, 335, app_msg, "_CheckStreamFieldsConformity" );
											ULFreeAndNullString(&app_msg);
											//*/
											break;
										}
									}
									else
									{
										conformity = false;
										_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckStreamFieldsConformity" );
										ret_code = SPV_DBM_DB_QUERY_FAILURE;
									}

								}
								catch(...)
								{
									conformity = false;
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_fields_02", "_CheckStreamFieldsConformity" );
								}

							}
							else
							{
								conformity = false;
								_Log( db_conn, 2, 335, "DEBUG non conforme per eccezione 2", "_CheckStreamFieldsConformity" );
							}

							// --- Cancello FieldID_32_value ---
							try
							{
								if ( FieldID_32_value != NULL )	free( FieldID_32_value );	// -FREE
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione di FieldID_32_value", "_CheckStreamFieldsConformity" );
							}

						}
						else
						{
							conformity = false;
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di accesso alla capacit� dello stream", "_CheckStreamFieldsConformity" );
							break;
						}
					} // --- end for

					// --- verificho se il numero dei record coincide col numero dei fields
					if (num_field != row_num_total)
					{
						conformity = false;
						//* DEBUG
						app_ui32 = (unsigned __int32)num_field;
						app_ui32_value  = XMLType2ASCII( (void*)&app_ui32, t_u_int_32 ); 	// -MALLOC
						app_msg = ULAddText( app_msg, "DEBUG: num_field (" );
						app_msg = ULAddText( app_msg, app_ui32_value );
						app_msg = ULAddText( app_msg, ") != row_num_total(" );
						ULFreeAndNullString(&app_ui32_value);
						app_ui32 = (unsigned __int32)row_num_total;
						app_ui32_value  = XMLType2ASCII( (void*)&app_ui32, t_u_int_32 ); 	// -MALLOC
						app_msg = ULAddText( app_msg, app_ui32_value );
						app_msg = ULAddText( app_msg, ")" );
						ULFreeAndNullString(&app_ui32_value);
						_Log( db_conn, 2, 335, app_msg, "_CheckStreamFieldsConformity" );
						ULFreeAndNullString(&app_msg);
						//*/
					}
				}
				else
				{
					conformity = false;
					_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckStreamFieldsConformity" );
					ret_code = SPV_DBM_DB_QUERY_FAILURE;
				}
			}
			catch(...)
			{
				conformity = false;
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_fields_01", "_CheckStreamFieldsConformity" );
			}
		}
		else
		{
			conformity = false;
			_Log( db_conn, 2, 335, "DEBUG non conforme per eccezione 1", "_CheckStreamFieldsConformity" );
		}

		// --- Cancello StrID_32_value ---
		try
		{
			if ( StrID_32_value != NULL )	free( StrID_32_value );	// -FREE
		}
		catch(...)
		{
			ret_code = SPV_DBM_FUNCTION_EXCEPTION;
			_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione di StrID_32_value", "_CheckStreamFieldsConformity" );
		}

	}
	catch(...)
	{
		conformity = false;
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckStreamFieldsConformity" );
	}

	*pconformity = conformity;

	return	ret_code;
}


//==============================================================================
/// Funzione per verificare la conformit� degli stream di una periferica
/// Gli stream di una data periferica presenti su db sono conformi se:
/// 1) l'Id di ogni stream � contenuto nella lista degli stream caricati
///    dalla configurazione per quella periferica;
/// 2) per ogni stream, a parit� di Id, il nome ed il flag visible devono
///	   coincidere con quelli degli stream caricati dalla configurazione
///	   per quella periferica;
/// 3) per ogni stream, a parit� di Id, il numero di record presenti sulla
///    tabella "stream_fields" deve coincidere col numero dei field caricati
///    dalla configurazione per quella periferica verificando che il numero
///	   dei record con stesso FieldID corrisponda al valore indicato
///	   nell'attributo "capacity" dello stesso field presente nella configurazione.
///
/// \date [19.12.2007]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	_CheckStreamsConformity(SPV_DBI_CONN * db_conn, void * pdevice, bool * pconformity, bool *pfconformity )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE * 			device 			= (SPV_DEVICE*)pdevice			;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]  	; // Stringa query
	int                 	query_len       = 0                           	; // Lunghezza stringa query
	unsigned __int64        DevID_64        = 0                           	;
	char *                  DevID_64_value  = NULL                        	;
	int						row_num			= 0								;
	SPV_DBI_FIELD *    	    table_field     = NULL                        	; // Ptr a struttura field
	int						stream_ID		= 0								;
	SPV_DEV_STREAM *        stream      	= NULL							; // Ptr ad una struttura stream
	bool					stream_Visible  = true							;
	bool					conformity		= true							; // Flag conformita'
	bool					field_conformity	= true						; // Flag conformita' field

	try
	{
		if ( SPVValidDevice( device ) )
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione durante i controlli iniziali", "_CheckStreamsConformity" );
	}

	if ( ret_code == SPV_DBM_NO_ERROR )
	{
		try
		{
			// --- Recupero valori ---
			try
			{
				DevID_64 = (unsigned __int64)device->DevID;
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori", "_CheckStreamsConformity" );
			}

			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				try
				{
					DevID_64_value  = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); 	// -MALLOC
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_CheckStreamsConformity" );
				}
			}

			if ( ret_code == SPV_DBM_NO_ERROR )
			{

				try
				{
					// --- Preparo la query ---
					query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
					query_len = _SQL_SELECT_streams_02( query, query_len, DevID_64_value );
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di preparazione della query", "_CheckStreamsConformity" );
				}
			}

			if ( ret_code == SPV_DBM_NO_ERROR )
			{

				// --- Lancio la query ---
				try
				{
					ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						// --- Ottengo il numero di righe dal risultato della query ---
						row_num = SPVDBGetRowNum( db_conn );

						// --- per ogni record ottenuto verifico la conformit�
						for ( int r = 0; r < row_num; r++ )
						{
							// --- controllo che l'Id dello stream sia presente nella configurazione in memoria
							table_field = SPVDBGetField( db_conn, r , 0 );
							if ( table_field != NULL )
							{
								if ( table_field->Data != NULL )
								{
									try
									{
										// --- Recupero il ptr alla struttura stream ---
										stream_ID = cnv_CharPToInt32( table_field->Data );
										stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, stream_ID );
									}
									catch(...)
									{
										stream = NULL;
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione in fase di recupero del puntatore allo stream", "_CheckStreamsConformity" );
										break;
									}

									if (stream != NULL)
									{
										// --- controllo che il nome dello stream sia uguale a parita di Id nella configurazione in memoria
										table_field = SPVDBGetField( db_conn, r , 1 );
										if ( table_field != NULL )
										{
											if ( table_field->Data != NULL )
											{
												if ( strcmpi( table_field->Data, stream->Name ) != 0 )
												{
													// --- il Nome non coincide
													conformity = false;
													break;
												}
											}
											else
											{
												conformity = false;
												break;
											}
										}
										else
										{
											conformity = false;
											ret_code = SPV_DBM_NULL_DBI_FIELD;
											_Log( db_conn, 2, ret_code, "Field ""Name"" del dataset nullo", "_CheckStreamsConformity" );
											break;
										}

										// --- controllo che il flag Visible dello stream sia uguale a parita di Id nella configurazione in memoria
										table_field = SPVDBGetField( db_conn, r , 2 );
										if ( table_field != NULL )
										{
											if ( table_field->Data != NULL )
											{
												try
												{
													// --- Recupero il ptr alla struttura stream ---
													stream_Visible = cnv_CharPToBool( table_field->Data );
													if (stream_Visible != stream->Visible)
													{
														// --- il flag Visible non coincide
														conformity = false;
														break;
													}
												}
												catch(...)
												{
													conformity = false;
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di di confronto del flag ""Visible""", "_CheckStreamsConformity" );
													break;
												}
											}
											else
											{
												conformity = false;
												break;
											}
										}
										else
										{
											conformity = false;
											ret_code = SPV_DBM_NULL_DBI_FIELD;
											_Log( db_conn, 2, ret_code, "Field ""Visible"" del dataset nullo", "_CheckStreamsConformity" );
											break;
										}
										// --- Verifico la conformit� a livello dei fields dello stream
										ret_code = _CheckStreamFieldsConformity(db_conn,DevID_64_value,stream,&field_conformity);
										if ( ret_code == SPV_DBM_NO_ERROR )
										{
											if (!field_conformity)
											{
												conformity = false;
												break;
											}
										}
										else
										{
											// --- fallimento della funzione di comfronto a livello di field
											conformity = false;
											break;
										}
									}
									else
									{
										// --- l'id dello stream non � contenuto nella configurazione in memoria
										conformity = false;
										break;
									}
								}
								else
								{
									conformity = false;
									break;
								}

							}
							else
							{
								conformity = false;
								ret_code = SPV_DBM_NULL_DBI_FIELD;
								_Log( db_conn, 2, ret_code, "Field ""StrID"" del dataset nullo", "_CheckStreamsConformity" );
								break;
							}
						} // --- end for
					}
					else
					{
						conformity = false;
						_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_CheckStreamsConformity" );
						ret_code = SPV_DBM_DB_QUERY_FAILURE;
					}
				}
				catch(...)
				{
					conformity = false;
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_CheckStreamsConformity" );
				}
			}
			else
			{
				conformity = false;
			}

			// --- Cancello i valori ---
			try
			{
				if ( DevID_64_value != NULL )	free( DevID_64_value 	);	// -FREE
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante cancellazione valori", "_CheckStreamsConformity" );
			}
		}
		catch(...)
		{
			conformity = false;
			ret_code = SPV_DBM_FUNCTION_EXCEPTION;
			_Log( db_conn, 2, ret_code, "Eccezione generale", "_CheckStreamsConformity" );
		}

		// --- Controlli finali sulla connessione DB ---
		_FinishConnCheck( db_conn );
	}
	else
	{
		conformity = false;
		_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_CheckStreamsConformity" );
	}

	*pconformity = conformity;
	*pfconformity = field_conformity;

	return	ret_code;
}

char * _StringUnString(char * value)
{
	int string_len = 0;
	char * new_value = NULL;
	int new_string_len = 0;
	char byte_string[3];
	char single_byte = 0;

	string_len = strlen(value);

	if (string_len > 0) {
		new_string_len = (string_len/2);
		new_value = (char*)malloc(sizeof(char)*(new_string_len+1));
		memset((void*)new_value,0,sizeof(char)*(new_string_len+1));
		for (int i = 0; i < new_string_len; i++) {
			byte_string[0] = value[(i*2)+0];
			byte_string[1] = value[(i*2)+1];
			byte_string[2] = 0;

			single_byte = cnv_CharPToUHex8( &byte_string[0] );
			new_value[i] = single_byte;
		}
	}

	return new_value;
}

//==============================================================================
/// Funzione per salvare nel DataBase un blocco di eventi
///
/// \date [16.12.2010]
/// \author Enrico Alborali
/// \version 1.02
//------------------------------------------------------------------------------
int _SaveEvents( SPV_DBI_CONN * db_conn, void * pdevice, SPV_DEVICE_EVENT * event_list, int event_count )
{
	int						ret_code			= SPV_APPLIC_FUNCTION_SUCCESS 	; // Codice di ritorno
	SPV_DEVICE 			  * device				= (SPV_DEVICE*)pdevice			;
	SPV_DEVICE_EVENT		* event 			= NULL;
	//char                    query				[SPV_DBI_QUERY_MAX_LENGTH]    	; // Stringa query
	char *					query_dinamic		= NULL;
	int                     query_len			= 0                             ; // Lunghezza stringa query
	int						query_dinamic_len	= 0;
	int						event_block			= 200;
	int						event_block_count	= 0;
	int						event_block_offset	= 0;
	int						left_event_count    = 0;

	unsigned __int64		DevID_64			= 0                             ;
	unsigned __int32		EventCategory_32	= 0								;

	char                  * DevID_64_value      = NULL                         	;
	char *					EventCategory_value = NULL	;
	char *					DateTime_value		= NULL	;
	char *					EventData_value		= NULL	;


	if ( SPVValidDevice( device ) ) // Controllo device
	{
		if ( event_list != NULL && event_count > 0 ) // Controllo lista eventi
		{
			// --- Controlli iniziali sulla connessione DB ---
			ret_code = _StartConnCheck( db_conn );
			if ( ret_code == SPV_DBM_NO_ERROR )
			{
				// --- Recupero valori comuni ---
				try
				{
					DevID_64        	= (unsigned __int64)device->DevID;
					EventCategory_32	= (unsigned __int32)event_list[0].Category;
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante il recupero valori comuni", "_SaveEvents" );
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						DevID_64_value  		= XMLType2ASCII( (void*)&DevID_64, t_u_int_64 );			// -MALLOC
						EventCategory_value     = XMLType2ASCII( (void*)&EventCategory_32, t_u_int_32 );	// -MALLOC
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori comuni", "_SaveEvents" );
					}
				}
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					/* DEBUG
					char * debug_event_count = cnv_UInt32ToCharP( event_count );
					_Log( db_conn, 1, 333, debug_event_count, "_SaveEvents" );
					if (debug_event_count) free(debug_event_count);
					//*/
					left_event_count = event_count;
					while(left_event_count>0)
					{
                        // Divido in query per il salvataggio di blocchi di eventi
						if (left_event_count < event_block) {
							event_block_count = left_event_count;
						}
						else
						{
							event_block_count = event_block;
						}
						left_event_count -= event_block_count;

						query_dinamic = (char*)malloc(sizeof(char)*event_count*500);
						// --- Preparo la query per inserire i singoli eventi ---
						query_dinamic_len = SPVDBClearQuery( query_dinamic, event_count*500 );
						//query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						// --- Inizio la transazione ---
						query_dinamic_len = _SQL_TRANSACTION_begin( query_dinamic, query_dinamic_len );

						for ( int e = 0; e < event_block_count; e++ )
						{
							try
							{
								event = &event_list[event_block_offset+e];
							}
							catch(...)
							{
								_Log( db_conn, 2, event_block_offset+e, "Debug: impossibile accedere all'evento e-esimo.", "_SaveEvents" );
							}
							try
							{
								DateTime_value = SPVDBGetTime( event->DateTime ); // -MALLOC
								EventData_value = &event->Data[0];
								/*
								// Decodifico il campo dati
								unstring_EventData_value = _StringUnString(EventData_value); // -MALLOC
								// Salvo l'evento
								if (unstring_EventData_value != null) {
									query_len = _SQL_INSERT_events_01( query, query_len, DevID_64_value, EventCategory_value, unstring_EventData_value, DateTime_value );
									free(unstring_EventData_value); // -FREE
								}
								*/
								query_dinamic_len = _SQL_INSERT_events_01( query_dinamic, query_dinamic_len, DevID_64_value, EventCategory_value, EventData_value, DateTime_value );
								if (DevID_64_value == NULL)
									_Log( db_conn, 2, 220, "DevID_64_value NULL", "_SaveEvents" );
								if (EventCategory_value == NULL)
									_Log( db_conn, 2, 221, "EventCategory_value NULL", "_SaveEvents" );
								if (EventData_value == NULL)
									_Log( db_conn, 2, 222, "EventData_value NULL", "_SaveEvents" );
								if (DateTime_value == NULL)
									_Log( db_conn, 2, 223, "DateTime_value NULL", "_SaveEvents" );
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la creazione della query per evento e-esimo.", "_SaveEvents" );
							}
							if ( DateTime_value	!= NULL )	free( DateTime_value );	// -FREE
							DateTime_value = NULL;
						}
						// --- Finisco la transazione ---
						query_dinamic_len = _SQL_TRANSACTION_end( query_dinamic, query_dinamic_len );
						if ( ret_code == SPV_DBM_NO_ERROR )
						{
							// --- Lancio la query ---
							ret_code = SPVDBQuery( db_conn, query_dinamic, query_dinamic_len, NULL, 0 );
							if ( ret_code != SPV_DBM_NO_ERROR ) {
								_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveEvents" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
							else
							{
								/* DEBUG
								debug_event_count = cnv_UInt32ToCharP( event_block_count );
								_Log( db_conn, 1, 332, debug_event_count, "_SaveEvents" );
								if (debug_event_count) free(debug_event_count);
								//*/
							}
							///_Log( db_conn, 1, ret_code, "Debug: Esito query salvataggio eventi", "_SaveEvents" );
						}
						free(query_dinamic);

						event_block_offset = event_block_count;
					}
					// --- Cancello i valori comuni ---
					try
					{
						if ( DevID_64_value			!= NULL	)	free( DevID_64_value 		);	// -FREE
						if ( EventCategory_value	!= NULL )	free( EventCategory_value	);	// -FREE
					}
					catch(...)
					{
						_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante cancellazione valori comuni", "_SaveEvents" );
					}
					// --- Controlli finali sulla connessione DB ---
					_FinishConnCheck( db_conn );
				}
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_EVENT_LIST;
		}
	}
	else
	{
		ret_code = SPV_DBM_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per rimuovere tutti gli oggetti topografici stazione
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMRemoveAllStations( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMRemoveAllStations" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _RemoveAllStations( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'eliminazione delle stazioni rimosse", "SPVDBMRemoveAllStations" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione per salvare un oggetto topografico stazione
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int	SPVDBMSaveStation( void * pstation, char ** uid )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	int						fun_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN*			db_conn         = NULL                        	; // Ptr a struttura connessione

	if ( pstation != NULL )
	{
		// Recupero la struttura connessione DB
		db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
		// --- Blocco la connessione ---
		_LockConn( db_conn, "SPVDBMSaveStation" );

		// --- Verifico l'esistenza della stazione ---
		fun_code = _CheckForStation( db_conn, pstation, uid );
		// --- Stazione trovata ---
		if ( fun_code == SPV_DBM_STATION_FOUND )
		{
			try
			{
				// --- Aggiorno la stazione ---
				ret_code = _UpdateStation( db_conn, pstation, *uid );
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante l\'aggiornamento della stazione", "SPVDBMSaveStation" );
			}
		}
		// --- Stazione non trovata ---
		else
		{
			// --- Salvataggio stazione ---
			try
			{
				// --- Inserisci una nuova stazione ---
				ret_code = _InsertStation( db_conn, pstation, uid );
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante l\'inserimento della stazione", "SPVDBMSaveStation" );
			}
		}

		// --- Rilascio la connessione ---
		_ReleaseConn( db_conn );
	}
	else
	{
		ret_code = SPV_DBM_INVALID_STATION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare le stazioni rimosse
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMDeleteRemovedStations( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMDeleteRemovedStations" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _DeleteRemovedStations( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'eliminazione delle stazioni rimosse", "SPVDBMDeleteRemovedStations" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per rimuovere tutti gli oggetti topografici edificio
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMRemoveAllBuildings( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMRemoveAllBuildings" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _RemoveAllBuildings( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'eliminazione degli edifici rimossi", "SPVDBMRemoveAllBuildings" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare un oggetto topografico edificio
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int	SPVDBMSaveBuilding( void * pbuilding, char ** uid )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	int						fun_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	if ( pbuilding != NULL )
	{
		// Recupero la struttura connessione DB
		db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
		// --- Blocco la connessione ---
		_LockConn( db_conn, "SPVDBMSaveBuilding" );

		// --- Verifico l'esistenza dell'edificio ---
		fun_code = _CheckForBuilding( db_conn, pbuilding, uid );
		// --- Edificio trovato ---
		if ( fun_code == SPV_DBM_BUILDING_FOUND )
		{
			try
			{
				// --- Aggiorno l'edificio ---
				ret_code = _UpdateBuilding( db_conn, pbuilding, *uid );
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante l\'aggiornamento dell\'edificio", "SPVDBMSaveBuilding" );
			}
		}
		// --- Edificio non trovato ---
		else
		{
			// --- Salvataggio edificio ---
			try
			{
				// --- Inserisci un nuovo edificio ---
				ret_code = _InsertBuilding( db_conn, pbuilding, uid );
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante l\'inserimento dell\'edificio", "SPVDBMSaveBuilding" );
			}
		}

		// --- Rilascio la connessione ---
		_ReleaseConn( db_conn );
	}
	else
	{
		ret_code = SPV_DBM_INVALID_BUILDING;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare gli edifici rimossi
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMDeleteRemovedBuildings( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMDeleteRemovedBuildings" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _DeleteRemovedBuildings( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'eliminazione degli edifici rimossi", "SPVDBMDeleteRemovedBuildings" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per rimuovere tutti gli oggetti topografici armadio
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMRemoveAllRacks( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMRemoveAllRacks" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _RemoveAllRacks( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'eliminazione degli armadi", "SPVDBMRemoveAllRacks" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare un oggetto topografico armadio
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMSaveRack( void * prack, char ** uid )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	int						fun_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN*			db_conn         = NULL                        	; // Ptr a struttura connessione


	if ( prack != NULL )
	{
		// Recupero la struttura connessione DB
		db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
		// --- Blocco la connessione ---
		_LockConn( db_conn, "SPVDBMSaveRack" );

		// --- Verifico l'esistenza dell'armadio ---
		fun_code = _CheckForRack( db_conn, prack, uid );
		// --- Armadio trovato ---
		if ( fun_code == SPV_DBM_RACK_FOUND )
		{
			try
			{
				// --- Aggiorno l'armadio ---
				ret_code = _UpdateRack( db_conn, prack, *uid );
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante l\'aggiornamento dell\'armadio", "SPVDBMSaveRack" );
			}
		}
		// --- Armadio non trovato ---
		else
		{
			// --- Salvataggio armadio ---
			try
			{
				// --- Inserisci un nuovo armadio ---
				ret_code = _InsertRack( db_conn, prack, uid );
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante l\'inserimento dell\'armadio", "SPVDBMSaveRack" );
			}
		}

		// --- Rilascio la connessione ---
		_ReleaseConn( db_conn );
	}
	else
	{
		ret_code = SPV_DBM_INVALID_RACK;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per eliminare gli armadi rimossi
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SPVDBMDeleteRemovedRacks( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMDeleteRemovedRacks" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _DeleteRemovedRacks( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'eliminazione degli armadi rimossi", "SPVDBMDeleteRemovedRacks" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per rimuovere tutte le porte di comunicazione
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDBMRemoveAllPorts( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMRemoveAllPorts" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _RemoveAllPorts( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante la rimozione di tutte le porte", "SPVDBMRemoveAllPorts" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione per salvare una porta di comunicazione
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int	SPVDBMSavePort( void * pport, char * SrvID, char ** uid )
{
	int					ret_code 		= SPV_DBM_NO_ERROR			;
	int					fun_code 		= SPV_DBM_NO_ERROR			;
	SPV_COMPROT_PORT * 	cpport	 		= (SPV_COMPROT_PORT*)pport	;
	SYS_PORT *			port	 		= NULL						;
	SPV_DBI_CONN *		db_conn         = NULL                     	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSavePort" );

	// --- Inizializzazione UID ---
	try
	{
		if ( uid != NULL )
		{
			*uid = NULL;
		}
	}
	catch(...)
	{
		_Log( db_conn, 2, ret_code, "Eccezione durante l\'inizializzazione uid", "SPVDBMSavePort" );
	}

	try
	{
		if ( cpport != NULL )
		{
			port = cpport->Port;
			if ( port != NULL )
			{
				// --- Verifico l'esistenza della porta ---
				fun_code = _CheckForPort( db_conn, pport, uid );
				// --- Porta trovata ---
				if ( fun_code == SPV_DBM_PORT_FOUND )
				{
					try
					{
						// --- Aggiorno la porta ---
						ret_code = _UpdatePort( db_conn, pport, SrvID, *uid );
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante l\'aggiornamento della porta", "SPVDBMSavePort" );
					}
				}
				// --- Porta non trovata ---
				else
				{
					// --- Salvataggio porta ---
					try
					{
						// --- Inserisco una nuova porta ---
						ret_code = _InsertPort( db_conn, pport, SrvID, uid );
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante l\'inserimento della porta", "SPVDBMSavePort" );
					}
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_SYS_PORT;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_COMPROT_PORT;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMSavePort" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se esiste una porta
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDBMCheckForPort( void * pport, char ** uid )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMCheckForPort" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _CheckForPort( db_conn, pport, uid );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante la verifica di esistenza di una porta", "SPVDBMCheckForPort" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione per eliminare le porte di comunicazione rimosse
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int	SPVDBMDeleteRemovedPorts( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMDeleteRemovedPorts" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _DeleteRemovedPorts(db_conn);
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante la cancellazione delle porte rimosse", "SPVDBMSaveDeviceStatus" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare lo stato di una periferica
///
/// \date [26.10.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int	SPVDBMSaveDeviceStatus( void * pdevice, bool multi_language )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	int 					fun_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE * 			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveDeviceStatus" );

	try
	{
		if ( SPVValidDevice( device ) )
		{
			// --- Verifico l'esistenza del DeviceStatus ---
			fun_code = _CheckForDeviceStatus(db_conn, pdevice);
			// --- DeviceStatus trovato ---
			if ( fun_code == SPV_DBM_DEVICE_STATUS_FOUND )
			{
				try
				{
					// --- Aggiorno il DeviceStatus ---
					ret_code = _UpdateDeviceStatus(db_conn, pdevice, multi_language );
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante l\'aggiornamento dello stato periferica", "SPVDBMSaveDeviceStatus" );
				}
			}
			// --- DeviceStatus non trovato ---
			else
			{
				// --- Salvataggio DeviceStatus ---
				try
				{
					// --- Inserisco un nuovo DeviceStatus ---
					ret_code = _InsertDeviceStatus( db_conn, pdevice, multi_language );
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante l\'inserimento dello stato periferica", "SPVDBMSaveDeviceStatus" );
				}
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMSaveDeviceStatus" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream di periferica
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int	SPVDBMSaveStream( void * pdevice, void * pstream )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR;
	int 					fun_code 		= SPV_DBM_NO_ERROR;
	SPV_DEVICE * 			device 			= (SPV_DEVICE*)pdevice;
	SPV_DEV_STREAM *		stream			= (SPV_DEV_STREAM*)pstream;
	SPV_DBI_CONN *			db_conn         = NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveStream" );

	try
	{
		if ( SPVValidDevice( device ) )
		{
			if ( SPVValidDevStream( stream ) )
			{
				// --- Verifico l'esistenza dello stream ---
				fun_code = _CheckForStream(db_conn, pdevice, pstream );
				// --- Stream trovato ---
				if ( fun_code == SPV_DBM_STREAM_FOUND )
				{
					try
					{
						// --- Aggiorno lo stream ---
						ret_code = _UpdateStream(db_conn, pdevice, pstream );
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante l\'aggiornamento dello stream", "SPVDBMSaveStream" );
					}
				}
				// --- Stream non trovato ---
				else
				{
					// --- Salvataggio stream ---
					try
					{
						// --- Inserisco un nuovo stream ---
						ret_code = _InsertStream( db_conn, pdevice, pstream );
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante l\'inserimento dello stream", "SPVDBMSaveStream" );
					}
				}
			}
			else
			{
				ret_code = SPV_DBM_INVALID_STREAM;
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMSaveStream" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

/*
 * Funzione per ottenere la severita' di uno stream di periferica
 */
int SPVDBMGetStreamSeverity( void * pdevice, void * pstream ){
	int 					fun_code 		= SPV_DBM_NO_ERROR;
	SPV_DEVICE * 			device 			= (SPV_DEVICE*)pdevice;
	SPV_DEV_STREAM *		stream			= (SPV_DEV_STREAM*)pstream;
	SPV_DBI_CONN *			db_conn         = NULL; // Ptr a struttura connessione
	int						sev_level		= -1; // Offline Severity Level

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMGetStreamSeverity" );

	try
	{
		if ( SPVValidDevice( device ) ){
			if ( SPVValidDevStream( stream ) ){
				// --- Recupero le severit� (degli stream) di una periferica ---
				sev_level = _GetStreamSeverity(db_conn, pdevice, pstream);
			}
			else{
				fun_code = SPV_DBM_INVALID_STREAM;
			}
		}
		else{
			fun_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...){
		fun_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, fun_code, "Eccezione generale", "SPVDBMGetStreamSeverity" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return sev_level;
}

//==============================================================================
/// Funzione per salvare nel DataBase il valore di tutti i campi di stream di periferica
///
/// \date [08.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDBMSaveStreamFields( void * pdevice, void * pstream )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE *			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEV_STREAM *        stream			= (SPV_DEV_STREAM*)pstream      ;
	SPV_DEV_STREAM_FIELD *  field 			= NULL							;
	SPV_DBI_CONN * 			db_conn     	= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveStreamFields" );

	try
	{
		if ( SPVValidDevice( device ) ) {
			if ( SPVValidDevStream( stream ) ) {
				// --- Ciclo di salvataggio dei campi di uno stream ---
				for ( int f = 0; f < stream->FieldNum; f++ ) {
					try
					{
						// --- Recupero l'f-esimo field dello stream ---
						field = SPVSearchStreamField( stream->FieldList, f );
						if ( SPVValidDevField( field ) ) {
							// --- Salvo sul DB l'f-esimo field (singolo o array) ---
							_SaveStreamField(db_conn, (void*)device, stream->ID, (void*)field );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante recupero e salvataggio field", "SPVDBMSaveStreamFields" );
					}
				}
			}
			else{
				ret_code = SPV_DBM_INVALID_STREAM;
			}
		}
		else{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMSaveStreamFields" );
	}

	//* DEBUG */ _Log( (SPV_DBI_CONN*)SPVSystem.DBConn, 1, ret_code, "DEBUG", "SPVDBMSaveStreamFields" );

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare un valore di riferimento di un campo
///
/// \date [09.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.09
//------------------------------------------------------------------------------
int	SPVDBMSaveReference( void * pdevice, int strid, void * pfield, int arrayid, char * dstrid, char * dfieldid, char * darrayid )
{
	int 				ret_code 	    = SPV_DBM_NO_ERROR;
	int 				fun_code 	    = SPV_DBM_NO_ERROR;
	unsigned __int16	ArrayID			= 0;
	char*	            RefID		    = NULL;
	char*	            DeltaStrID	    = NULL;
	char*	            DeltaFieldID	= NULL;
	char*	            DeltaArrayID	= NULL;
	char*	            ArrayID_value	= NULL;
	SPV_DBI_CONN * 	    db_conn     	= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveReference" );

	try
	{
		// --- Verifico l'esistenza di un reference ---
		fun_code = _CheckForReference(db_conn, pdevice, strid, pfield, arrayid, &RefID );
		if ( fun_code == SPV_DBM_REFERENCE_NOT_FOUND )
		{
			// --- Preparazione perametri ---
			try
			{
				ArrayID			= (unsigned __int16) arrayid;
				ArrayID_value 	= XMLType2ASCII( (void*)&ArrayID, t_u_int_16 ); 	// -MALLOC
				if ( strcmpi( dstrid, "i" ) == 0 ) {
					DeltaStrID = ULStrCpy( NULL, ArrayID_value ); // -MALLOC
				}
				else{
					DeltaStrID = ULStrCpy( NULL, dstrid ); // -MALLOC
				}
				if ( strcmpi( dfieldid, "i" ) == 0 ) {
					DeltaFieldID = ULStrCpy( NULL, ArrayID_value ); // -MALLOC
				}
				else{
					DeltaFieldID = ULStrCpy( NULL, dfieldid ); // -MALLOC
				}
				if ( strcmpi( darrayid, "i" ) == 0 ) {
					DeltaArrayID = ULStrCpy( NULL, ArrayID_value ); // -MALLOC
				}
				else{
					DeltaArrayID = ULStrCpy( NULL, darrayid ); // -MALLOC
				}
			}
			catch(...)
			{
				_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la preparazione dei parametri", "SPVDBMSaveReference" );
			}
			// --- Salvataggio reference ---
			try
			{
				// --- Inserisci un nuovo reference ---
				ret_code = _InsertReference( db_conn, pdevice, pfield, arrayid, DeltaStrID, DeltaFieldID, DeltaArrayID, &RefID );
				if ( ret_code == SPV_DBM_NO_ERROR ) {
					// --- Collego il nuovo reference ---
					ret_code = _LinkReference( db_conn, pdevice, strid, pfield, arrayid, RefID );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione durante il salvataggio del reference", "SPVDBMSaveReference" );
			}
			// --- Cancellazione parametri ---
			try
			{
				if ( ArrayID_value != NULL ) 	free( ArrayID_value );
				if ( DeltaStrID != NULL ) 		free( DeltaStrID );
				if ( DeltaFieldID != NULL ) 	free( DeltaFieldID );
				if ( DeltaArrayID != NULL ) 	free( DeltaArrayID );
			}
			catch(...)
			{
				_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione dei parametri", "SPVDBMSaveReference" );
			}
		}
		else
		{
			if ( fun_code != SPV_DBM_NO_ERROR && fun_code != SPV_DBM_REFERENCE_FOUND ) {
				ret_code = fun_code;
			}
		}
		// --- Cancellazione RfiID ---
		try
		{
			if ( RefID != NULL ) free( RefID );
		}
		catch(...)
		{
			_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione di RefID", "SPVDBMSaveReference" );
		}
		if ( ret_code != SPV_DBM_NO_ERROR ) {
			_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di un reference", "SPVDBMSaveReference" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMSaveReference" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare un valore di riferimento di un campo
///
/// \date [12.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
int	SPVDBMClearReference( void * pdevice, int strid, void * pfield, int arrayid )
{
	int 			ret_code 	= SPV_DBM_NO_ERROR;
	int 			fun_code 	= SPV_DBM_NO_ERROR;
	char*			RefID		= NULL;
	SPV_DBI_CONN * 	db_conn     = NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMClearReference" );

	try
	{
		// --- Verifico l'esistenza di un reference ---
		fun_code = _CheckForReference(db_conn, pdevice, strid, pfield, arrayid, &RefID );
		if ( fun_code == SPV_DBM_REFERENCE_FOUND )
		{
			// --- Scollego il reference ---
			ret_code = _UnLinkReference(db_conn, pdevice, strid, pfield, arrayid );
			if ( ret_code == SPV_DBM_NO_ERROR ) {
				// --- Cancella il reference ---
				ret_code = _DeleteReference(db_conn, pdevice, RefID );
			}
		}
		else
		{
			if ( fun_code != SPV_DBM_NO_ERROR && fun_code != SPV_DBM_REFERENCE_NOT_FOUND ) {
				ret_code = fun_code;
			}
		}
		// --- Cancellazione RfiID ---
		try
		{
			if ( RefID != NULL ) free( RefID );
		}
		catch(...)
		{
			_Log( db_conn, 2, SPV_DBM_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione di RefID", "SPVDBMClearReference" );
		}
		if ( ret_code != SPV_DBM_NO_ERROR ) {
			_Log( db_conn, 2, ret_code, "Errore durante la cancellazione di un reference", "SPVDBMClearReference" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMClearReference" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione per ripulire la tabella degli stream da quelli non pi� validi
///
/// \date [26.10.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int	SPVDBMCleanAllStreams( void * pdevice, bool multi_language)
{
	int 					ret_code 			= SPV_DBM_NO_ERROR		;
	SPV_DEVICE * 			device 				= (SPV_DEVICE*)pdevice	;
	SPV_DBI_CONN  * 		db_conn         	= NULL                  ; // Ptr a struttura connessione
	bool					conformity			= false					;
	bool					field_conformity	= false					;
	char * 					logMsg 				= NULL					;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMCleanAllStreams" );

	try
	{
		if ( SPVValidDevice( device ) )
		{
			if ( device->TypeStatus != SPV_DEVICE_TYPE_NEW )
			{
				if ( device->TypeStatus == SPV_DEVICE_TYPE_CHANGED )
				{
					logMsg = ULAddText( logMsg, "Executing stream, field and reference clean for device " );
					logMsg = ULAddText( logMsg, device->Name );
					logMsg = ULAddText( logMsg, " due to type or definition version change." );
					_Log( db_conn, 1, ret_code, logMsg, "SPVDBMCleanAllStreams" );
					ULFreeAndNullString(&logMsg);

					ret_code = _DeleteAllStreams(db_conn,pdevice);
					if (ret_code == SPV_DBM_NO_ERROR)
					{
						// --- Aggiorno il DeviceStatus ---
						ret_code = _UpdateDeviceStatus( db_conn, pdevice, multi_language );
					}
				}
				/*
				else
				{
					ret_code = _CheckStreamsConformity( db_conn, pdevice, &conformity, &field_conformity );
					if ( ret_code == SPV_DBM_NO_ERROR )
					{
						if ( !conformity )
						{
							if ( field_conformity ) {
								logMsg = ULAddText( logMsg, "Trovati stream non conformi della periferica " );
								logMsg = ULAddText( logMsg, device->Name );
								logMsg = ULAddText( logMsg, " per cambio attributi o cambio tipo periferica" );
								_Log( db_conn, 1, SPV_DBM_STREAM_CONFORMITY_NOT_VERIFIED, logMsg, "SPVDBMCleanAllStreams" );
							}
							else
							{
								logMsg = ULAddText( logMsg, "Trovati stream non conformi della periferica " );
								logMsg = ULAddText( logMsg, device->Name );
								logMsg = ULAddText( logMsg, " per variazione definizione campi" );
								_Log( db_conn, 1, SPV_DBM_FIELD_CONFORMITY_NOT_VERIFIED, logMsg, "SPVDBMCleanAllStreams" );
							}
							ULFreeAndNullString( &logMsg );

							ret_code = _DeleteAllStreams( db_conn, pdevice );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								// --- Aggiorno il DeviceStatus ---
								ret_code = _UpdateDeviceStatus( db_conn, pdevice );
							}
						}
					}
				}
				*/
			}
		}
		else
		{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}

	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione durante il controllo e pulizia degli stream della periferica", "SPVDBMCleanAllStreams" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DB un blocco di eventi
///
/// \date [27.08.2008]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveEvents( void * pdevice, void * pevents, int event_count )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR				;
	SPV_DEVICE *			device 			= (SPV_DEVICE*)pdevice			;
	SPV_DEVICE_EVENT *  	event_list		= (SPV_DEVICE_EVENT*)pevents	;
	SPV_DBI_CONN * 			db_conn     	= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveEvents" );

	try
	{
		if ( SPVValidDevice( device ) ) {
			if ( event_list != NULL ) {
				ret_code = _SaveEvents( db_conn, (void*)device, event_list, event_count );
				///_Log( db_conn, 1, ret_code, "Debug: Esito query", "SPVDBMSaveEvents" );
			}
			else{
				ret_code = SPV_DBM_INVALID_EVENT_LIST;
			}
		}
		else{
			ret_code = SPV_DBM_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale", "SPVDBMSaveEvents" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione interna per salvare i valori di tutte le procedure
///
/// \date [08.11.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveProcedures( SPV_DBI_CONN * db_conn, SPV_PROCEDURE * proc_list, int proc_count )
{
	int 			ret_code 		= SPV_DBM_NO_ERROR;
	char                	query               	[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len           	= 0; // Lunghezza stringa query
	SPV_PROCEDURE * 	proc            	= NULL;
	char * 			DevID_value     	= NULL;
	char * 			ProID_value     	= NULL;
	char * 			ExeCount_value  	= NULL;
	char * 			desLog 			= NULL;
	unsigned __int8       	ExeCount8       	= 0;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{

			try
			{

                        	//sprintf		( desLog, "Numero Procedure da Inserire: %d", proc_count);
                		//LLAddLogFile	( desLog );

				// --- Preparo la query ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

				// --- Scorro la lista delle procedure ---
				for ( int p = 0; p < proc_count; p++ ) // Ciclo sulle definizioni di procedura
				{
					try
					{
						// --- Recuparo la procedura ---
						proc = &proc_list[p]; // Prendo la prossima posizione libera

                                                //sprintf		( desLog, "Salvataggio Procedura: %s...", proc);
		                		//LLAddLogFile	( desLog );

						if ( proc != NULL )
						{
							try
							{
								ExeCount8 = (unsigned __int8)(proc->ExeCount<0||proc->ExeCount>255)?0:proc->ExeCount;
								// --- Coversione in formato ASCII ---
								DevID_value     = XMLType2ASCII( (void*)&proc->Device->DevID, t_u_int_64 ); // -MALLOC
								ProID_value     = XMLType2ASCII( (void*)&proc->ID, t_u_int_32 ); // -MALLOC
								ExeCount_value  = XMLType2ASCII( (void*)&ExeCount8, t_u_int_8 ); // -MALLOC
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveProcedures" );
							}

							//--- Aggiungo la query di inserimento per la singola procedura
							query_len = _SQL_INSERT_Procedures_01( query, query_len, DevID_value, ProID_value, proc->Name, ExeCount_value );
							try
							{
								// --- Cancellazione memoria ---
								if ( DevID_value    != NULL ) free( DevID_value    );
								if ( ProID_value    != NULL ) free( ProID_value    );
								if ( ExeCount_value != NULL ) free( ExeCount_value );
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveProcedures" );
							}
						}
						else
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Procedura non valida", "_SaveProcedures" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante un ciclo di preparazione query", "_SaveProcedures" );
					}
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Lancio la query per inserzione di una nuova riga ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code != SPV_DBM_NO_ERROR )
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveProcedures" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_SaveProcedures" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveProcedures" );
				}

			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante l\'accesso alla lista procedure applicative", "_SaveProcedures" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveProcedures" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveProcedures" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per salvare i valori di tutte le procedure
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDBMSaveProcedures( void * pproc_list, int proc_count )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_PROCEDURE * 		proc_list 			= (SPV_PROCEDURE*)pproc_list;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveProcedures" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveProcedures( db_conn, proc_list, proc_count );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio delle procedure", "SPVDBMSaveProcedures" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel database i dati della tabella Systems
///
/// \date [10.12.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveDeviceSystems( SPV_DBI_CONN * db_conn, SPV_DEVICE_SYSTEM *	device_system_list, int device_system_count )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len           = 0; // Lunghezza stringa query
	SPV_DBI_FIELD * 		field           	= NULL; // Ptr a struttura field
	SPV_DEVICE_SYSTEM * 	device_system       = NULL;
	char * 					ID_value			= NULL;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Scorro la lista delle tipologie di periferica ---
				for ( int p = 0; p < device_system_count; p++ ) // Ciclo sulla lista delle tipologia di periferica
				{
					try
					{
						// --- Recupero la singola tipologia di periferica ---
						device_system = &device_system_list[p]; // Prendo la prossima posizione libera
						if ( device_system != NULL )
						{
							try
							{
								// --- Conversione in formato ASCII ---
								ID_value = cnv_UInt32ToCharP(device_system->ID);
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveDeviceSystems" );
							}

							// --- Verifico esistenza del tipo nella tabella Systems ---
							try
							{
								// --- Preparo la query ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								// --- Preparo la query per verificare se riga esiste ---
								query_len = _SQL_SELECT_systems_01( query, query_len,ID_value );
								// --- Lancio la query ---
								try
								{

									ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
									if ( ret_code == SPV_DBM_NO_ERROR )
									{
										field = SPVDBGetField( db_conn, 0, 0 );
										// --- Verifico esistenza della riga nella tabella ---
										if ( field != NULL )
										{
											// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
											try
											{
												query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
												query_len = _SQL_UPDATE_systems_01( query, query_len, ID_value, device_system->Description );
												try
												{
													ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
													if ( ret_code != SPV_DBM_NO_ERROR )
													{
														_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceSystems:Update" );
														ret_code = SPV_DBM_DB_QUERY_FAILURE;
													}
												}
												catch(...)
												{
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPADTE_systems_01.", "_SaveDeviceSystems" );
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_GENERAL_ERROR;
												_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPADTE_systems_01.", "_SaveDeviceSystems" );
											}
										}
										else
										{
											// --- La riga non � presente nella tabella del DB per cui la inserisco ---
											try
											{
												query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
												query_len = _SQL_INSERT_systems_01( query, query_len, ID_value, device_system->Description );
												try
												{
													ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
													if ( ret_code != SPV_DBM_NO_ERROR )
													{
														_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceSystems:Insert" );
														ret_code = SPV_DBM_DB_QUERY_FAILURE;
													}
												}
												catch(...)
												{
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_systems_01.", "_SaveDeviceSystems" );
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_GENERAL_ERROR;
												_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_systems_01.", "_SaveDeviceSystems" );
											}

										}

									}
									else
									{
										_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceSystems:Select" );
										ret_code = SPV_DBM_DB_QUERY_FAILURE;
									}
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_systems_01.", "_SaveDeviceSystems" );
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_GENERAL_ERROR;
								_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_systems_01.", "_SaveDeviceSystems" );
							}

							try
							{
								// --- Cancellazione memoria ---
								if ( ID_value  != NULL ) free( ID_value );
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveDeviceSystems" );
							}

						}
						else
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Tipologia di periferica non valida", "_SaveDeviceSystems" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il ciclo di inserimento delle tipologie di periferica", "_SaveDeviceSystems" );
					}
				}


			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante l\'accesso alla lista delle tipologie di periferica", "_SaveDeviceSystems" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveDeviceSystems" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveDeviceSystems" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per salvare nel database i dati della tabella Systems
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int  SPVDBMSaveDeviceSystems( void * pdevice_system_list, int device_system_count )
{
	int					ret_code 			= SPV_DBM_NO_ERROR;
	SPV_DEVICE_SYSTEM *	device_system_list	= (SPV_DEVICE_SYSTEM*)pdevice_system_list;
	SPV_DBI_CONN *		db_conn         	= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveDeviceSystems" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveDeviceSystems( db_conn, device_system_list, device_system_count );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio delle tipologie di periferica", "SPVDBMSaveDeviceSystems" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel database i dati della tabella Vendors
///
/// \date [10.12.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveDeviceVendors( SPV_DBI_CONN * db_conn, SPV_DEVICE_VENDOR *	device_vendor_list, int device_vendor_count )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len           = 0; // Lunghezza stringa query
	SPV_DBI_FIELD * 		field           	= NULL; // Ptr a struttura field
	SPV_DEVICE_VENDOR * 	device_vendor       = NULL;
	char * 					ID_value			= NULL;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Scorro la lista dei costruttori di periferiche ---
				for ( int p = 0; p < device_vendor_count; p++ ) // Ciclo sulla lista dei costruttori di periferiche
				{
					try
					{
						// --- Recupero il singolo costruttore di periferica ---
						device_vendor = &device_vendor_list[p]; // Prendo la prossima posizione libera
						if ( device_vendor != NULL )
						{
							try
							{
								// --- Conversione in formato ASCII ---
								ID_value = cnv_UInt32ToCharP(device_vendor->ID);
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveDeviceVendors" );
							}

							// --- Verifico esistenza del tipo nella tabella Vendors ---
							try
							{
								// --- Preparo la query ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								// --- Preparo la query per verificare se riga esiste ---
								query_len = _SQL_SELECT_vendors_01( query, query_len,ID_value );
								// --- Lancio la query ---
								try
								{

									ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
									if ( ret_code == SPV_DBM_NO_ERROR )
									{
										field = SPVDBGetField( db_conn, 0, 0 );
										// --- Verifico esistenza della riga nella tabella ---
										if ( field != NULL )
										{
											// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
											try
											{
												query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
												query_len = _SQL_UPDATE_vendors_01( query, query_len, ID_value, device_vendor->Name, device_vendor->Description );
												try
												{
													ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
													if ( ret_code != SPV_DBM_NO_ERROR )
													{
														_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceVendors:Update" );
														ret_code = SPV_DBM_DB_QUERY_FAILURE;
													}
												}
												catch(...)
												{
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPADTE_vendors_01.", "_SaveDeviceVendors" );
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_GENERAL_ERROR;
												_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPADTE_vendors_01.", "_SaveDeviceVendors" );
											}
										}
										else
										{
											// --- La riga non � presente nella tabella del DB per cui la inserisco ---
											try
											{
												query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
												query_len = _SQL_INSERT_vendors_01( query, query_len, ID_value, device_vendor->Name, device_vendor->Description );
												try
												{
													ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
													if ( ret_code != SPV_DBM_NO_ERROR )
													{
														_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceVendors:Insert" );
														ret_code = SPV_DBM_DB_QUERY_FAILURE;
													}
												}
												catch(...)
												{
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_vendors_01.", "_SaveDeviceVendors" );
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_GENERAL_ERROR;
												_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_vendors_01.", "_SaveDeviceVendors" );
											}

										}

									}
									else
									{
										_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceVendors:Select" );
										ret_code = SPV_DBM_DB_QUERY_FAILURE;
									}
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_vendors_01.", "_SaveDeviceVendors" );
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_GENERAL_ERROR;
								_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_vendors_01.", "_SaveDeviceVendors" );
							}

							try
							{
								// --- Cancellazione memoria ---
								if ( ID_value  != NULL ) free( ID_value );
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveDeviceVendors" );
							}

						}
						else
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Costruttore di periferica non valida", "_SaveDeviceVendors" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il ciclo di inserimento dei costruttori di periferiche", "_SaveDeviceVendors" );
					}
				}


			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante l\'accesso alla lista dei costruttori di periferiche", "_SaveDeviceVendors" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveDeviceVendors" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveDeviceVendors" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per salvare nel database i dati della tabella Vendors
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int  SPVDBMSaveDeviceVendors( void * pdevice_vendor_list, int device_vendor_count )
{
	int					ret_code 			= SPV_DBM_NO_ERROR;
	SPV_DEVICE_VENDOR *	device_vendor_list	= (SPV_DEVICE_VENDOR*)pdevice_vendor_list;
	SPV_DBI_CONN *		db_conn         	= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveDeviceVendors" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveDeviceVendors( db_conn, device_vendor_list, device_vendor_count );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio delle tipologie di periferica", "SPVDBMSaveDeviceVendors" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel database i dati della tabella Device_type
///
/// \date [06.12.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveDeviceTypes( SPV_DBI_CONN * db_conn, SPV_DEVICE_TYPE *	device_type_list, int device_type_count )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len           = 0; // Lunghezza stringa query
	SPV_DBI_FIELD * 		field           	= NULL; // Ptr a struttura field
	SPV_DEVICE_TYPE * 		device_type        	= NULL;
	char * 					SystemID_value		= NULL;
	char * 					VendorID_value    	= NULL;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Scorro la lista dei tipi di periferica ---
				for ( int p = 0; p < device_type_count; p++ ) // Ciclo sulla lista dei tipi di periferica
				{
					try
					{
						// --- Recupero il singolo tipo di periferica ---
						device_type = &device_type_list[p]; // Prendo la prossima posizione libera
						if ( device_type != NULL )
						{
							try
							{
								// --- Conversione in formato ASCII ---
								SystemID_value = cnv_UInt32ToCharP(device_type->SystemID);
								VendorID_value = cnv_UInt32ToCharP(device_type->VendorID);
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveDeviceTypes" );
							}

							// --- Verifico esistenza del tipo nella tabella Device_Type ---
							try
							{
								// --- Preparo la query ---
								query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
								// --- Preparo la query per verificare se riga esiste ---
								query_len = _SQL_SELECT_device_type_01( query, query_len,device_type->Code );
								// --- Lancio la query ---
								try
								{

									ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
									if ( ret_code == SPV_DBM_NO_ERROR )
									{
										field = SPVDBGetField( db_conn, 0, 0 );
										// --- Verifico esistenza della riga nella tabella ---
										if ( field != NULL )
										{
											// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
											try
											{
												query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
												query_len = _SQL_UPDATE_device_type_01( query, query_len, device_type->Code, device_type->Name, SystemID_value, VendorID_value );
												try
												{
													ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
													if ( ret_code != SPV_DBM_NO_ERROR )
													{
														_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceTypes:Update" );
														ret_code = SPV_DBM_DB_QUERY_FAILURE;
													}
												}
												catch(...)
												{
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPADTE_device_type_01.", "_SaveDeviceTypes" );
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_GENERAL_ERROR;
												_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPADTE_device_type_01.", "_SaveDeviceTypes" );
											}
										}
										else
										{
											// --- La riga non � presente nella tabella del DB per cui la inserisco ---
											try
											{
												query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
												query_len = _SQL_INSERT_device_type_01( query, query_len, device_type->Code, device_type->Name, SystemID_value, VendorID_value );
												try
												{
													ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
													if ( ret_code != SPV_DBM_NO_ERROR )
													{
														_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceTypes:Insert" );
														ret_code = SPV_DBM_DB_QUERY_FAILURE;
													}
												}
												catch(...)
												{
													ret_code = SPV_DBM_FUNCTION_EXCEPTION;
													_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_device_type_01.", "_SaveDeviceTypes" );
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_GENERAL_ERROR;
												_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_device_type_01.", "_SaveDeviceTypes" );
											}

										}

									}
									else
									{
										_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDeviceTypes:Select" );
										ret_code = SPV_DBM_DB_QUERY_FAILURE;
									}
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_device_type_01.", "_SaveDeviceTypes" );
								}
							}
							catch(...)
							{
								ret_code = SPV_DBM_GENERAL_ERROR;
								_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_device_type_01.", "_SaveDeviceTypes" );
							}

							try
							{
								// --- Cancellazione memoria ---
								if ( SystemID_value  != NULL ) free( SystemID_value );
								if ( VendorID_value  != NULL ) free( VendorID_value );
							}
							catch(...)
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveDeviceTypes" );
							}

						}
						else
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Tipo di dispositivo non valido", "_SaveDeviceTypes" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante il ciclo di inserimento dei tipi di dispositivo", "_SaveDeviceTypes" );
					}
				}


			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante l\'accesso alla lista dei tipi di dispositivo", "_SaveDeviceTypes" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveDeviceTypes" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveDeviceTypes" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per salvare nel database i dati della tabella Device_Type
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int  SPVDBMSaveDeviceTypes( void * pdevice_type_list, int device_type_count )
{
	int					ret_code 			= SPV_DBM_NO_ERROR;
	SPV_DEVICE_TYPE *	device_type_list	= (SPV_DEVICE_TYPE*)pdevice_type_list;
	SPV_DBI_CONN *		db_conn         	= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveDeviceTypes" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveDeviceTypes( db_conn, device_type_list, device_type_count );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio degli dei tipi di periferica", "SPVDBMSaveDeviceTypes" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione interna per aggiornare le informazioni di una procedura applicativa
///
/// \date [13.11.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_DeleteUnusedRows( SPV_DBI_CONN * db_conn )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len           = 0; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Preparo la query ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
				//--- Inserisco la query di aggiornamneto della procedura
				query_len = _SQL_EXEC_DeleteUnusedRows_01( query, query_len );

				// --- Lancio la query ---
				try
				{
					ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
					if ( ret_code != SPV_DBM_NO_ERROR )
					{
						_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_DeleteUnusedRows" );
						ret_code = SPV_DBM_DB_QUERY_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_DeleteUnusedRows" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query.", "_DeleteUnusedRows" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteUnusedRows" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_DeleteUnusedRows" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per chiamare la Stored Proedure di pulizia delle righe non pi� usate
/// usate nelle varie tabelle
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SPVDBMDeleteUnusedRows( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMDeleteUnusedRows" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _DeleteUnusedRows( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio delle procedure", "SPVDBMDeleteUnusedRows" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;

}


//==============================================================================
/// Funzione interna per svuotare la tabella delle procedure
///
/// \date [08.11.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_TruncateProceduresTable( SPV_DBI_CONN * db_conn )
{
	int ret_code = SPV_DBM_NO_ERROR;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				SPVDBTruncateTable( db_conn, "procedures" );
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante lo svuotamento della tabella delle Procedures", "_TruncateProceduresTable" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_TruncateProceduresTable" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_TruncateProceduresTable" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per svuotare completamente la tabella 'procedures'
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDBMTruncateProceduresTable( void )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMTruncateProceduresTable" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _TruncateProceduresTable( db_conn );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante lo svuotamento della tabella Procedures", "SPVDBMTruncateProceduresTable" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per aggiornare le informazioni di una procedura applicativa
///
/// \date [20.12.2007]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	_UpdateProcedure( SPV_DBI_CONN * db_conn, SPV_PROCEDURE * procedure )
{
	int 					ret_code 		    = SPV_DBM_NO_ERROR;
	char                	query               [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len           = 0; // Lunghezza stringa query
	char * 					DevID_value         = NULL;
	char * 					ProID_value         = NULL;
	char * 					ExeCount_value      = NULL;
	char * 					LastExecution_value = NULL;
	char * 					InProgress_value    = NULL;
	unsigned __int8       	ExeCount8           = 0;
	unsigned __int8			in_progress_ui8		= 0;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				try
				{
					if ( SPVValidProcedure( procedure ) )
					{
						if ( SPVValidDevice( procedure->Device ) )
						{
							// --- Recupero i parametri ---
							ExeCount8 			= (unsigned __int8)(procedure->ExeCount<0||procedure->ExeCount>255)?0:procedure->ExeCount;
							in_progress_ui8 	= (unsigned __int8)(procedure->Active)?1:0;
        					// --- Coversione in formato ASCII ---
        					DevID_value     	= XMLType2ASCII( (void*)&procedure->Device->DevID, t_u_int_64 ); // -MALLOC
							ProID_value     	= XMLType2ASCII( (void*)&procedure->ID, t_u_int_32 ); // -MALLOC
        					ExeCount_value  	= XMLType2ASCII( (void*)&ExeCount8, t_u_int_8 ); // -MALLOC
							LastExecution_value = SPVDBGetTime( procedure->StartDT ); // -MALLOC
							InProgress_value    = XMLType2ASCII( (void*)&in_progress_ui8, t_u_int_8 ); // -MALLOC
						}
						else
						{
							ret_code = SPV_DBM_INVALID_DEVICE;
							_Log( db_conn, 2, ret_code, "Impossibile recuperare informazioni sulla periferica", "_UpdateProcedure" );
						}
					}
					else
					{
						ret_code = SPV_DBM_INVALID_PROCEDURE;
						_Log( db_conn, 2, ret_code, "Impossibile recuperare informazioni sulla procedura", "_UpdateProcedure" );
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_UpdateProcedure" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Preparo la query ---
					query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
					//--- Inserisco la query di aggiornamneto della procedura
					query_len = _SQL_UPDATE_Procedure_01( query, query_len, DevID_value, ProID_value, ExeCount_value, LastExecution_value, InProgress_value );

					// --- Lancio la query ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code != SPV_DBM_NO_ERROR )
						{
							_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_UpdateProcedure" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_UpdateProcedure" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_UpdateProcedure" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( DevID_value    		!= NULL ) free( DevID_value    		);
					if ( ProID_value    		!= NULL ) free( ProID_value    		);
					if ( ExeCount_value 		!= NULL ) free( ExeCount_value 		);
					if ( LastExecution_value	!= NULL ) free( LastExecution_value );
					if ( InProgress_value		!= NULL ) free( InProgress_value 	);
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_UpdateProcedure" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante l\'accesso alle informazioni di una procedura applicativa.", "_UpdateProcedure" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_UpdateProcedure" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdateProcedure" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per aggiornare le informazioni di una procedura applicativa
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDBMUpdateProcedure( void * pprocedure )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL                        	; // Ptr a struttura connessione
	SPV_PROCEDURE * 		procedure 		= (SPV_PROCEDURE*)pprocedure    ;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMUpdateProcedure" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _UpdateProcedure( db_conn, procedure );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l\'aggiornamento della procedura", "SPVDBMUpdateProcedure" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per caricare dal DB la lista degli ExeCount
///
/// \date [29.10.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int _LoadProcExeCounts( SPV_DBI_CONN * db_conn, SPV_PROCEDURE_COUNTER * count_list, int * count_num )
{
	int 					ret_code 	  = SPV_DBM_NO_ERROR;
	char                    query         [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                     query_len     = 0; // Lunghezza stringa query
	int                     row_num       = 0; // Numero di risultati
	SPV_DBI_FIELD         * field         = NULL; // Ptr a struttura field
	SPV_PROCEDURE_COUNTER   counter;
	SPV_PROCEDURE_COUNTER * pcounter      = NULL;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Preparo la query ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
				//--- Inserisco la query di selezione delle procedure
				query_len = _SQL_SELECT_procedures_01( query, query_len );

				// --- Lancio la query ---
				try
				{
					ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
					if ( ret_code != SPV_DBM_NO_ERROR )
					{
						_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_LoadProcExeCounts" );
						ret_code = SPV_DBM_DB_QUERY_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_LoadProcExeCounts" );
				}

				// --- Se la query � andata a buon fine senza errori ---
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					row_num = SPVDBGetRowNum( db_conn );
					*count_num = row_num;

					if ( row_num > 0 )
					{
						for ( int r = 0; r < row_num; r++ )
						{
							if ( r >= SPV_APPLIC_MAX_PROC )
							{
								ret_code = SPV_DBM_PROC_TABLE_EXCEEDS_MAX;
								_Log( db_conn, 2, ret_code, "La tabella \'procedures\' contiene un numero di righe superiore al massimo consentito.", "_LoadProcExeCounts" );
								break;
							}
							else
							{
								// --- Estraggo il DevID ---
								field = SPVDBGetField( db_conn, r, 0 );
								if ( field != NULL )
								{
									counter.DevID = cnv_CharPToUInt64( field->Data );
								}

								// --- Estraggo il ProID ---
								field = SPVDBGetField( db_conn, r, 1 );
								if ( field != NULL )
								{
									counter.ProID = cnv_CharPToInt32( field->Data );
								}

								// --- Estraggo l'ExeCount ---
								field = SPVDBGetField( db_conn, r, 2 );
								if ( field != NULL )
								{
									counter.ExeCount = cnv_CharPToInt32( field->Data );
								}

								// --- Azzero il flag di cambiamento ---
								counter.Changed = false;
								pcounter = &count_list[r];
								if ( pcounter != NULL )
								{
									try
									{
										*pcounter = counter; // Copio nella lista il contatore di esecuzioni r-esimo appena estratto
									}
									catch(...)
									{
										ret_code = SPV_DBM_FUNCTION_EXCEPTION;
										_Log( db_conn, 2, ret_code, "Eccezione critica durante l'accesso alla lista contatori di esecuzione", "_LoadProcExeCounts" );
										break;
									}
								}
							}
						} // end for
					}
					else
					{
						ret_code = SPV_DBM_DB_EMPTY_RESULT;
						_Log( db_conn, 1, ret_code, "La tabella \'procedures\' � vuota.", "_LoadProcExeCounts" );
					}
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il caricamento da DB della lista degli ExeCount.", "_LoadProcExeCounts" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_LoadProcExeCounts" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_LoadProcExeCounts" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per caricare dal DB la lista degli ExeCount
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMLoadProcExeCounts( void * pcount_list, void * pcount_num )
{
	int						ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *			db_conn         = NULL; // Ptr a struttura connessione
	SPV_PROCEDURE_COUNTER * count_list      = (SPV_PROCEDURE_COUNTER*) pcount_list;
	int * 					count_num 		= (int*) pcount_num;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMLoadProcExeCounts" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _LoadProcExeCounts( db_conn, count_list, count_num );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il caricamento della lista degli ExeCount", "SPVDBMLoadProcExeCounts" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione interna per rimuovere dal DB gli ACK
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int _RemoveExpiredAcks( SPV_DBI_CONN * db_conn )
{
	int					ret_code	= SPV_DBM_NO_ERROR;
	char				query		[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int					query_len	= 0; // Lunghezza stringa query

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Preparo la query ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
				//--- Inserisco la query di cancellazione ACK scaduti
				query_len = _SQL_DELETE_device_ack_01( query, query_len );

				// --- Lancio la query ---
				try
				{
					ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
					if ( ret_code != SPV_DBM_NO_ERROR )
					{
						_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_RemoveExpiredAcks" );
						ret_code = SPV_DBM_DB_QUERY_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_RemoveExpiredAcks" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante la cancellazione degli ACK scaduti.", "_RemoveExpiredAcks" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB.", "_RemoveExpiredAcks" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_RemoveExpiredAcks" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione interna per caricare dal DB la lista degli ACK
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int _LoadDeviceAcks( SPV_DBI_CONN * db_conn, SPV_DEV_ACK * ack_list, int * ack_num )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	char			query		[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int				query_len	= 0; // Lunghezza stringa query
	int				row_num		= 0; // Numero di risultati
	SPV_DBI_FIELD *	field		= NULL; // Ptr a struttura field
	SPV_DEV_ACK  	ack				;
	SPV_DEV_ACK   *	pack		= NULL;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Preparo la query ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
				//--- Inserisco la query di selezione delle procedure
				query_len = _SQL_SELECT_device_ack_01( query, query_len );

				// --- Lancio la query ---
				try
				{
					ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
					if ( ret_code != SPV_DBM_NO_ERROR )
					{
						_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_LoadDeviceAck" );
						ret_code = SPV_DBM_DB_QUERY_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_LoadDeviceAck" );
				}

				// --- Se la query � andata a buon fine senza errori ---
				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					row_num = SPVDBGetRowNum( db_conn );
					*ack_num = row_num;

					if ( row_num > 0 )
					{
						for ( int r = 0; r < row_num; r++ )
						{
							// --- Estraggo il DevID ---
							field = SPVDBGetField( db_conn, r, 0 );
							if ( field != NULL ){
								ack.DevID = cnv_CharPToUInt64( field->Data );
							}

							// --- Estraggo il StrID ---
							field = SPVDBGetField( db_conn, r, 1 );
							if ( field != NULL ){
								if ( field->Data == NULL )
									ack.StrID = -1;
								else
									ack.StrID = cnv_CharPToUInt32( field->Data );
							}

							// --- Estraggo il FieldID ---
							field = SPVDBGetField( db_conn, r, 2 );
							if ( field != NULL ){
								if ( field->Data == NULL )
									ack.FieldID = -1;
								else
									ack.FieldID = cnv_CharPToUInt32( field->Data );
							}
							
							// --- Inserisco l'ACK nella lista
							pack = &ack_list[r];
							if ( pack != NULL )
							{
								try
								{
									*pack = ack; // Copio nella lista il contatore di esecuzioni r-esimo appena estratto
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione critica durante l'accesso alla lista ACK.", "_LoadDeviceAck" );
									break;
								}
							}
						} // end for
					}
					else
					{
                        // Non sono stati registrati ACK di competenza di questo supervisore
						/*
						ret_code = SPV_DBM_DB_EMPTY_RESULT;
						_Log( db_conn, 1, ret_code, "La tabella \'procedures\' � vuota.", "_LoadDeviceAck" );
						*/
					}
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il caricamento da DB della lista degli ExeCount.", "_LoadDeviceAck" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_LoadDeviceAck" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_LoadDeviceAck" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per caricare dal DB la lista degli ACK
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBMLoadDeviceAcks( void * pack_list, void * pack_num )
{
	int				 ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	 db_conn	= NULL; // Ptr a struttura connessione
	SPV_DEV_ACK * 	ack_list	= (SPV_DEV_ACK*) pack_list;
	int * 			 ack_num	= (int*) pack_num;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMLoadDeviceAcks" );

	try
	{
		// --- Richiamo la funzione interna per eliminare gli ACK scaduti ---
		ret_code = _RemoveExpiredAcks( db_conn );
		// --- Richiamo la funzione interna per caricare la lista ACK ---
		ret_code = _LoadDeviceAcks( db_conn, ack_list, ack_num );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il caricamento della lista ACK.", "SPVDBMLoadDeviceAcks" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione interna per salvare nel DB la lista degli ExeCount
///
/// \date [17.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int _SaveProcExeCounts( SPV_DBI_CONN * db_conn, SPV_PROCEDURE_COUNTER * count_list, int count_num )
{
	int 					ret_code 		= SPV_DBM_NO_ERROR;
	char                	query           [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 	query_len       = 0; // Lunghezza stringa query
	SPV_DBI_FIELD * 		field           = NULL; // Ptr a struttura field
	SPV_PROCEDURE_COUNTER   counter;
	SPV_PROCEDURE_COUNTER * pcounter        = NULL;
	char * 					DevID_value     = NULL;
	char * 					ProID_value     = NULL;
	char * 					ExeCount_value  = NULL;
	SPV_PROCEDURE * 		procedure       = NULL;


	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Preparo la query ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

				// --- Scorro la lista dei contatori da aggiornare ---
				for ( int c = 0; c < count_num; c++ )
				{
					try
					{
						counter = count_list[c];
						if ( counter.Changed == true )
						{
							// --- Recupero la procedura associata al contatore ---
							procedure = SPVSearchProcedure( counter.DevID, counter.ProID );
							if ( procedure != NULL )
							{

								try
								{
									// --- Coversione in formato ASCII ---
									DevID_value     = XMLType2ASCII( (void*)&procedure->Device->DevID, t_u_int_64 ); // -MALLOC
									ProID_value     = XMLType2ASCII( (void*)&procedure->ID, t_u_int_32 ); // -MALLOC
									ExeCount_value  = XMLType2ASCII( (void*)&procedure->ExeCount, t_u_int_32 ); // -MALLOC
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveProcExeCounts" );
								}

								//--- Aggiungo una riga alla query di aggiornamento dell'ExeCount delle procedure
								query_len = _SQL_UPDATE_Procedure_02( query, query_len, DevID_value, ProID_value, ExeCount_value );

								try
								{
									// --- Cancellazione memoria ---
									if ( DevID_value    != NULL ) free( DevID_value    );
									if ( ProID_value    != NULL ) free( ProID_value    );
									if ( ExeCount_value != NULL ) free( ExeCount_value );
								}
								catch(...)
								{
									ret_code = SPV_DBM_FUNCTION_EXCEPTION;
									_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveProcExeCounts" );
								}
							}
							else
							{
								ret_code = SPV_DBM_FUNCTION_EXCEPTION;
								_Log( db_conn, 2, ret_code, "Procedura non valida", "_SaveProcExeCounts" );
							}
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante un ciclo di preparazione query", "_SaveProcExeCounts" );
					}
				} // end for

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Lancio la query per inserzione di una nuova riga ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code != SPV_DBM_NO_ERROR )
						{
							_Log( db_conn, 1, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveProcExeCounts" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_SaveProcExeCounts" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveProcExeCounts" );
				}

			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				SPVReportApplicEvent( NULL, 2, ret_code, "Eccezione critica durante l'accesso alla lista contatori di esecuzione", "_SaveProcExeCounts" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveProcExeCounts" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveProcExeCounts" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per salvare nel DB la lista degli ExeCount
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveProcExeCounts( void * pcount_list, int count_num )
{
	int							ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *				db_conn         = NULL; // Ptr a struttura connessione
	SPV_PROCEDURE_COUNTER * 	count_list 		= (SPV_PROCEDURE_COUNTER*)pcount_list;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveProcExeCounts" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveProcExeCounts( db_conn, count_list, count_num );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio della lista degli ExeCount", "SPVDBMSaveProcExeCounts" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per eliminare uno stream dal DB
///
/// \date [24.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_DeleteStream( SPV_DBI_CONN * db_conn, SPV_DEVICE * device, int StrID )
{
	int 				ret_code 		    = SPV_DBM_NO_ERROR;
	char                query               [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                 query_len           = 0; // Lunghezza stringa query
	char * 				DevID_64_value    	= NULL;
	char * 				StrID_value       	= NULL;
	unsigned __int64    DevID_64          	= 0;
	unsigned __int8     StrID_8           	= 0;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					DevID_64 = (unsigned __int64)device->DevID;
					StrID_8 = (unsigned __int8)StrID;
					// --- Coversione in formato ASCII ---
					DevID_64_value = XMLType2ASCII( (void*)&DevID_64, t_u_int_64 ); // -MALLOC
					StrID_value = XMLType2ASCII( (void*)&StrID_8, t_u_int_8 ); // -MALLOC
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_DeleteStream" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					// --- Preparo la query ---
					query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
					//--- Inserisco la query di eleiminazione di uno stream
					query_len = _SQL_DELETE_Stream_01( query, query_len, DevID_64_value, StrID_value );

					// --- Lancio la query ---
					try
					{
						ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( ret_code != SPV_DBM_NO_ERROR )
						{
							_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_DeleteStream" );
							ret_code = SPV_DBM_DB_QUERY_FAILURE;
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_DeleteStream" );
					}
				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_DeleteStream" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( DevID_64_value != NULL ) free( DevID_64_value );
					if ( StrID_value != NULL ) free( StrID_value );
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_DeleteStream" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante l\'eliminazione di uno stream.", "_DeleteStream" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_DeleteStream" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_DeleteStream" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione esterna per eliminare uno stream dal DB
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMDeleteStream( void * pdevice, int StrID )
{
	int					ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *		db_conn         = NULL; // Ptr a struttura connessione
	SPV_DEVICE * 		device 			= (SPV_DEVICE*)pdevice    ;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMDeleteStream" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _DeleteStream( db_conn, device, StrID );
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante la cancellazione di uno stream dal DB", "SPVDBMDeleteStream" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}



//==============================================================================
/// Funzione interna per caricare dal DataBase il buffer di uno stream di periferica
///
/// In caso di errore restituisce un buffer non valido.
///
/// \date [25.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DEV_STREAM_BUFFER _GetStream( SPV_DBI_CONN * db_conn, SPV_DEVICE * device, int streamID )
{
	int                     fun_code        = SPV_DBI_NO_ERROR; // Codice di funzione
	SPV_DEV_STREAM_BUFFER   stream_buffer   = { 0, NULL };
	char                    query           [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	int                     query_len       = 0; // Lunghezza stringa query
	char * 					StrID_value     = NULL; // Valore ASCII StrID
	SPV_DBI_FIELD *			field           = NULL; // Ptr a struttura field
	char * 					DevID_64_value  = NULL;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		fun_code = _StartConnCheck( db_conn );
		if ( fun_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					DevID_64_value  = XMLType2ASCII( (void*)&device->DevID, t_u_int_64 ); // -MALLOC
					StrID_value     = XMLType2ASCII( (void*)&streamID, t_u_int_8 ); // -MALLOC
				}
				catch(...)
				{
					fun_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, fun_code, "Eccezione durante la conversione ASCII dei valori", "_GetStream" );
				}

				if ( fun_code == SPV_DBM_NO_ERROR )
				{
					// --- Preparo la query ---
					query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
					//--- Inserisco la query di eleiminazione di uno stream
					query_len = _SQL_SELECT_streams_03( query, query_len, DevID_64_value, StrID_value );

					// --- Lancio la query ---
					try
					{
						fun_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
						if ( fun_code == SPV_DBM_NO_ERROR )
						{
							field = SPVDBGetField( db_conn, 0, 0 );
							// --- Se la riga � gi� presente nella tabella del DB ---
							if ( field != NULL )
							{
								stream_buffer.Len = field->DataLen;
								stream_buffer.Ptr = (unsigned char*)field->Data;
							}
						}
						else
						{
							fun_code = SPV_DBM_DB_QUERY_FAILURE;
							_Log( db_conn, 2, fun_code, db_conn->LastError, "_GetStream" );

						}
					}
					catch(...)
					{
						fun_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, fun_code, "Eccezione in fase di esecuzione della query", "_GetStream" );
					}
				}
				else
				{
					_Log( db_conn, 2, fun_code, "Impossibile eseguire la query causa parametri non recuperati", "_GetStream" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( DevID_64_value != NULL ) free( DevID_64_value );
					if ( StrID_value != NULL ) free( StrID_value );
				}
				catch(...)
				{
						fun_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, fun_code, "Eccezione durante la cancellazione dei valori", "_GetStream" );
				}
			}
			catch(...)
			{
				fun_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, fun_code, "Errore durante il caricamento dal DataBase del buffer di uno stream di periferica", "_GetStream" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, fun_code, "Errore durante i controlli iniziali DB", "_GetStream" );
		}
	}
	catch(...)
	{
		fun_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, fun_code, "Eccezione generale - L1", "_GetStream" );
	}

	return stream_buffer;
}



//==============================================================================
/// Funzione esterna per caricare dal DataBase il buffer di uno stream di periferica
///
/// In caso di errore restituisce un buffer non valido.
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMGetStream( void * pdevice, int streamID, void * pstream_buffer )
{
	int							ret_code 		= SPV_DBM_NO_ERROR;
	SPV_DEV_STREAM_BUFFER * 	stream_buffer 	= (SPV_DEV_STREAM_BUFFER*)pstream_buffer;
	SPV_DBI_CONN *				db_conn         = NULL; // Ptr a struttura connessione
	SPV_DEVICE * 				device 			= (SPV_DEVICE*)pdevice    ;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMGetStream" );

	try
	{
		// --- Richiamo la funzione interna ---
		*stream_buffer = _GetStream( db_conn, device, streamID );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il caricamento dal DataBase del buffer di uno stream di periferica", "SPVDBMGetStream" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}



//==============================================================================
/// Funzione interna per marchiare come rimossi tutti i record di una tabella
///
/// \date [29.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SigneRemoved( SPV_DBI_CONN * db_conn, char * table)
{
	int	ret_code = SPV_DBM_NO_ERROR;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			// --- Preparo e lancio la query di update della tabella ---
			try
			{
				ret_code = SPVDBUpdateTable( db_conn, table, "Removed = 1" );
				if ( ret_code != SPV_DBM_NO_ERROR )
				{
					_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SigneRemoved" );
					ret_code = SPV_DBM_DB_QUERY_FAILURE;
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_FUNCTION_EXCEPTION;
				_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query", "_SigneRemoved" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SigneRemoved" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SigneRemoved" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per marchiare come rimosse tutte le periferiche
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSigneRemovedAllDevices( void )
{
	int				ret_code 	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn		= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSigneRemovedAllDevices" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SigneRemoved( db_conn , "devices" );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l'operazione di marcamento come rimosse di tutte le periferiche", "_SigneRemovedAllDevices" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione interna per salvare nel DB una periferica
///
/// \date [01.03.2012]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	_SaveDevice( SPV_DBI_CONN * db_conn, SPV_DEVICE * device )
{
	int 				ret_code 		    	= SPV_DBM_NO_ERROR;
	char                query               	[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	long    			query_len           	= 0; // Lunghezza stringa query
	char * 				NodID_64_value  		= NULL;
	char * 				DevID_64_value  		= NULL;
	unsigned __int64    NodID_64        		= 0;
	unsigned __int32    SrvID_32        		= 0;
	char * 				SrvID_32_value  		= NULL;
	char * 				Name_value      		= NULL;
	char * 				Type_value      		= NULL;
	char * 				SN_value        		= NULL;
	SPV_TPG_Rack *		rack					= NULL;
	char *				RackRAWID_value			= NULL;
	unsigned __int16	RackCol					= 0;
	unsigned __int16	RackRow					= 0;
	SPV_COMPROT_PORT *	port					= NULL;
	char *				PortRAWID_value			= NULL;
	char * 				PortID_value    		= NULL;
	char * 				Addr_value      		= NULL;
	char * 				ProfileID_value 		= NULL;
	char * 				Offline_value  			= NULL;
	char * 				Active_value    		= NULL;
	char * 				Scheduled_value 		= NULL;
	char *				RackFID_value			= NULL;
	char *				RackCol_value			= NULL;
	char *				RackRow_value			= NULL;
	char *				DefinitionVersion_value	= NULL;
	char *				ProtocolVersion_value	= NULL;
	SPV_DBI_FIELD * 	field           		= NULL; // Ptr a struttura field
	int					supervisor_id			= 0;

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					// --- Recupero DevID (64bit) ---
					DevID_64_value  = XMLType2ASCII( (void*)&device->DevID, t_u_int_64 ); // -MALLOC
					// --- Recupero NodID (64bit) ---
					NodID_64 = SPVGetNodID( device );
					NodID_64_value  = XMLType2ASCII( &NodID_64, t_u_int_64 ); // -MALLOC
					// --- Recupero SrvID (32bit) ---
					SrvID_32 = SPVGetSrvID( device );
					SrvID_32_value  = XMLType2ASCII( &SrvID_32, t_u_int_32 ); // -MALLOC
					// --- Recupero Name ---
					Name_value = device->Name;
					// --- Recupero Type ---
					if ( device->Type != NULL )
					{
						Type_value = device->Type->Code;
					}
					else
					{
						Type_value = NULL;
					}
					// --- Recupero SN ---
					SN_value = device->SN;
					// --- Recupero RackID ---
					rack = (SPV_TPG_Rack*)device->Rack;
					if ( rack != NULL )
					{
						//* DEBUG */ SPVReportDBEvent( db_conn, 2, 3444, "Rack FOUND", NULL );
						RackRAWID_value = rack->GetID();
					}
					else
					{
						//* DEBUG */ SPVReportDBEvent( db_conn, 2, 3444, "Rack NOT FOUND", NULL );
					}
					//* DEBUG */ SPVReportDBEvent( db_conn, 2, 3444, RackRAWID_value, NULL );
					RackCol = (unsigned __int16)device->RackCol;
					RackRow = (unsigned __int16)device->RackRow;
					// --- Recupero PortID ---
					port = &SPVPortList[device->SupCfg.PortID];
					if ( port != NULL )
					{
						_CheckForPort(db_conn,port,&PortRAWID_value,false);
						//PortRAWID_value = SPVGetDBPortID( port ); // -MALLOC
						PortID_value = SPVDBGetUniqueID( PortRAWID_value ); // -MALLOC
					}
					//PortID_value = XMLType2ASCII( (void*)&device->SupCfg.PortID, t_u_int_8 ); // -MALLOC
					// --- Recupero Addr ---
					Addr_value = XMLType2ASCII( (void*)&device->SupCfg.Address, t_u_int_32 ); // -MALLOC
					// --- Recupero ProfileID ---
					ProfileID_value = XMLType2ASCII( (void*)&device->ProfileID, t_u_int_8 ); // -MALLOC
					// --- Recupero Offline ---
					Offline_value = XMLType2ASCII( (void*)&device->SupStatus.Offline, t_u_int_8 ); // -MALLOC
					// --- Recupero Active ---
					Active_value = XMLType2ASCII( (void*)&device->SupStatus.Active, t_u_int_8 ); // -MALLOC
					// --- Recupero Scheduled ---
					Scheduled_value = XMLType2ASCII( (void*)&device->SupStatus.Scheduled, t_u_int_8 ); // -MALLOC
					// --- Conversione ---
					RackFID_value = SPVDBGetUniqueID( RackRAWID_value ); //-MALLOC
					RackCol_value = XMLType2ASCII( (void*)&RackCol, t_u_int_16 ); // -MALLOC
					RackRow_value = XMLType2ASCII( (void*)&RackRow, t_u_int_16 ); // -MALLOC

					// --- Recupero la versione della definizione xml della periferica ---
					DefinitionVersion_value = device->DefinitionVersion;
					// --- Recupero la versione del protocollo xml della periferica ---
					ProtocolVersion_value = device->ProtocolVersion;
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveDevice" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{

						// --- Preparo la query per verificare se riga esiste e recuperare il tipo e versione definizione ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

						query_len = _SQL_SELECT_devices_01( query, query_len, DevID_64_value );

						// --- Lancio la query ---
						try
						{

							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 ); // Recupero il tipo
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									supervisor_id = XMLGetValueInt( device->Def, "supervisor_id" );
									// Se e' una periferica di mia competenza
									if ( supervisor_id == 0 )
									{
										// --- Verifico se il tipo � cambiato e setto di conseguenza TypeStatus ---
										if((field->Data != NULL) && (Type_value != NULL))
										{
											device->TypeStatus = (strcmpi(field->Data,Type_value)==0) ? SPV_DEVICE_TYPE_UNCHANGED : SPV_DEVICE_TYPE_CHANGED;
											if ( device->TypeStatus == SPV_DEVICE_TYPE_UNCHANGED )
											{
												field = SPVDBGetField( db_conn, 0, 1 ); // Recupero la versione def
												// --- Verifico se la versione def � cambiata e setto di conseguenza TypeStatus ---
												if (field->Data != NULL)
												{
													device->TypeStatus = (strcmpi(field->Data,DefinitionVersion_value)==0) ? SPV_DEVICE_TYPE_UNCHANGED : SPV_DEVICE_TYPE_CHANGED;
												}
												else
												{   //--- se nel database manca la colonna lo condicero cambiato
													device->TypeStatus = SPV_DEVICE_TYPE_CHANGED;
												}
											}
										}
										else
										{
											device->TypeStatus = SPV_DEVICE_TYPE_CHANGED;
										}
									}
									// Se non e' una periferica di mia competenza non controllo il tipo
									else
									{
										device->TypeStatus = SPV_DEVICE_TYPE_UNCHANGED;
									}

									// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										if ( device->Owned ) {
											query_len = _SQL_UPDATE_devices_01( query, query_len, DevID_64_value,
																			NodID_64_value, SrvID_32_value, Name_value,
																			Type_value, SN_value, PortID_value, Addr_value,
																			ProfileID_value, Active_value, Scheduled_value,
																			RackFID_value, RackCol_value, RackRow_value,
																			DefinitionVersion_value, ProtocolVersion_value );
											//_Log( db_conn, 2, 370, Name_value, "_SaveDevice insert" );
										}
										else
										{
											//_Log( db_conn, 2, 369, Name_value, "_SaveDevice update" );
											query_len = _SQL_UPDATE_devices_02( query, query_len, DevID_64_value );
										}
										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDevice:Update" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPDATE_devices_01", "_SaveDevice" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPDATE_devices_01", "_SaveDevice" );
									}
								}
								else
								{
									// --- Se la riga non esiste (ne creo una nuova) ---
									// --- setto di conseguenza TypeStatus ---
									device->TypeStatus = SPV_DEVICE_TYPE_NEW;

									if ( device->Owned )// viene inserita solo se 'owned'
									{
										// --- La riga non � presente nella tabella del DB per cui la inserisco ---
										try
										{
											query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
											query_len = _SQL_INSERT_devices_01( query, query_len, DevID_64_value,
																				NodID_64_value, SrvID_32_value, Name_value,
																				Type_value, SN_value, PortID_value, Addr_value,
																				ProfileID_value, Active_value, Scheduled_value,
																				RackFID_value, RackCol_value, RackRow_value,
																				DefinitionVersion_value, ProtocolVersion_value );
											//_Log( db_conn, 2, 370, Name_value, "_SaveDevice insert" );
											try
											{
												ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
												if ( ret_code != SPV_DBM_NO_ERROR )
												{
													_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDevice:Insert" );
													ret_code = SPV_DBM_DB_QUERY_FAILURE;
												}
											}
											catch(...)
											{
												ret_code = SPV_DBM_FUNCTION_EXCEPTION;
												_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_devices_01", "_SaveDevice" );
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_GENERAL_ERROR;
											_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_devices_01", "_SaveDevice" );
										}
									}
									else
									{
										//_Log( db_conn, 2, 369, Name_value, "_SaveDevice insert" );
									}
								}
							}
							else
							{
								_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDevice:Select" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_devices_01", "_SaveDevice" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_GENERAL_ERROR;
						_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_devices_01", "_SaveDevice" );
					}

				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveDevice" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( DevID_64_value  != NULL ) free( DevID_64_value  ); // -FREE
					if ( NodID_64_value  != NULL ) free( NodID_64_value  ); // -FREE
					if ( SrvID_32_value  != NULL ) free( SrvID_32_value  ); // -FREE
					if ( PortID_value    != NULL ) free( PortID_value    ); // -FREE
					if ( PortRAWID_value != NULL ) free( PortRAWID_value ); // -FREE
					if ( Addr_value      != NULL ) free( Addr_value      ); // -FREE
					if ( ProfileID_value != NULL ) free( ProfileID_value ); // -FREE
					if ( Offline_value   != NULL ) free( Offline_value   ); // -FREE
					if ( Active_value    != NULL ) free( Active_value    ); // -FREE
					if ( Scheduled_value != NULL ) free( Scheduled_value ); // -FREE
					if ( RackFID_value	 != NULL ) free( RackFID_value 	 ); // -FREE
					if ( RackCol_value	 != NULL ) free( RackCol_value 	 ); // -FREE
					if ( RackRow_value	 != NULL ) free( RackRow_value 	 ); // -FREE
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveDevice" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di una periferica.", "_SaveDevice" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveDevice" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveDevice" );
	}

	return ret_code;
}

/**
 * Funzione per aggiornare (versioni definizioni) e verificare su DB una periferica
 */
int	_UpdateDevice( SPV_DBI_CONN * db_conn, SPV_DEVICE * device ){
	int 				ret_code 		    	= SPV_DBM_NO_ERROR;
	char                query               	[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	long    			query_len           	= 0; // Lunghezza stringa query
	char * 				DevID_64_value  		= NULL;
	char * 				Type_value      		= NULL;
	SPV_COMPROT_PORT *	port					= NULL;
	char *				PortRAWID_value			= NULL;
	char * 				PortID_value    		= NULL;
	char *				DefinitionVersion_value	= NULL;
	char *				ProtocolVersion_value	= NULL;
	SPV_DBI_FIELD * 	field           		= NULL; // Ptr a struttura field

	try{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR ){
			try{
				// --- Recupero i parametri ---
				try{
					// --- Recupero DevID (64bit) ---
					DevID_64_value  = XMLType2ASCII( (void*)&device->DevID, t_u_int_64 ); // -MALLOC
					// --- Recupero Type ---
					if ( device->Type != NULL )
						Type_value = device->Type->Code;
					else
						Type_value = NULL;
					// --- Recupero PortID ---
					port = &SPVPortList[device->SupCfg.PortID];
					if ( port != NULL )
					{
						_CheckForPort(db_conn,port,&PortRAWID_value,false);
						PortID_value = SPVDBGetUniqueID( PortRAWID_value ); // -MALLOC
					}
					// --- Recupero la versione della definizione xml della periferica ---
					DefinitionVersion_value = device->DefinitionVersion;
					// --- Recupero la versione del protocollo xml della periferica ---
					ProtocolVersion_value = device->ProtocolVersion;
				}
				catch(...){
					ret_code = SPV_DBM_FUNCTION_EXCEPTION;
					_Log( db_conn, 2, ret_code, "Eccezione durante la conversione dei valori", "_UpdateDevice" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR ){
					try{
						// --- Preparo la query per verificare se la riga esiste e recuperare versione definizione ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

						query_len = _SQL_SELECT_devices_01( query, query_len, DevID_64_value );

						// --- Lancio la query ---
						try{
							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR ){
								field = SPVDBGetField( db_conn, 0, 0 ); // Recupero il tipo
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL ){
									if ( device->Owned ){
										// --- Verifico se il tipo � cambiato e setto di conseguenza TypeStatus ---
										if((field->Data != NULL) && (Type_value != NULL)){
											device->TypeStatus = (strcmpi(field->Data,Type_value)==0) ? SPV_DEVICE_TYPE_UNCHANGED : SPV_DEVICE_TYPE_CHANGED;
											if ( device->TypeStatus == SPV_DEVICE_TYPE_UNCHANGED ){
												field = SPVDBGetField( db_conn, 0, 1 ); // Recupero la versione def
												// --- Verifico se la versione def � cambiata e setto di conseguenza TypeStatus ---
												if (field->Data != NULL)
													device->TypeStatus = (strcmpi(field->Data,DefinitionVersion_value)==0) ? SPV_DEVICE_TYPE_UNCHANGED : SPV_DEVICE_TYPE_CHANGED;
												else
												{   //--- se nel database manca la colonna lo consicero nuovo
													device->TypeStatus = SPV_DEVICE_TYPE_NEW;
												}
											}
										}
										else{
											device->TypeStatus = SPV_DEVICE_TYPE_CHANGED;
										}
									}
									// Se non e' una periferica di mia competenza non controllo il tipo
									else{
										device->TypeStatus = SPV_DEVICE_TYPE_UNCHANGED;
									}

									// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
									try{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										if ( device->Owned ){
											query_len = _SQL_UPDATE_devices_03( query, query_len,
												DevID_64_value,
												PortID_value,
												DefinitionVersion_value,
												ProtocolVersion_value );
										}
										try{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR ){
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDevice:Update" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...){
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPDATE_devices_01", "_SaveDevice" );
										}
									}
									catch(...){
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPDATE_devices_01", "_SaveDevice" );
									}
								}
								else{
									// --- Se la riga non esiste (dovrebbe essere stata creata da un servizio esterno) ---
									// --- setto di conseguenza TypeStatus ---
									device->TypeStatus = SPV_DEVICE_TYPE_NEW;

									if ( device->Owned )// viene inserita solo se 'owned'
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, 363, "Errore: la riga relativa alla periferica non esiste nella tabella devices.", "_UpdateDevice" );
									}
								}
							}
							else
							{
								_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveDevice:Select" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_devices_01", "_SaveDevice" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_GENERAL_ERROR;
						_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_devices_01", "_SaveDevice" );
					}

				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveDevice" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( DevID_64_value  != NULL ) free( DevID_64_value  ); // -FREE
					if ( PortID_value    != NULL ) free( PortID_value    ); // -FREE
					if ( PortRAWID_value != NULL ) free( PortRAWID_value ); // -FREE
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveDevice" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di una periferica.", "_SaveDevice" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveDevice" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_UpdateDevice" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per salvare nel DB una periferica
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveDevice( void * pdevice )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn     = NULL; // Ptr a struttura connessione
	SPV_DEVICE * 	device 		= (SPV_DEVICE*)pdevice    ;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveDevice" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveDevice( db_conn, device );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio di una periferica nel DataBase", "SPVDBMSaveDevice" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

/**
 *
 */
int SPVDBMUpdateDevice( void * pdevice )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn     = NULL; // Ptr a struttura connessione
	SPV_DEVICE * 	device 		= (SPV_DEVICE*)pdevice    ;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMUpdateDevice" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _UpdateDevice( db_conn, device );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante aggiornamento di una periferica nel DB", "SPVDBMUpdateDevice" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione esterna per marchiare come rimossi tutti i nodi
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSigneRemovedAllNodes( void )
{
	int				ret_code 	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn		= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSigneRemovedAllNodes" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SigneRemoved( db_conn , "nodes" );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l'operazione di marcamento come rimossi di tutti i nodi", "_SigneRemovedAllNodes" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel DB un nodo
///
/// \date [30.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveNode( SPV_DBI_CONN * db_conn, SPV_NODE * node )
{
	int 				ret_code 		    	= SPV_DBM_NO_ERROR;
	char                query               	[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	long                query_len           	= 0; // Lunghezza stringa query
	char * 				NodID_64_value  		= NULL;
	unsigned __int64    ZonID_64        		= 0;
	char *              ZonID_64_value  		= NULL;
	char *              Name_value      		= NULL;
	SPV_DBI_FIELD * 	field           		= NULL; // Ptr a struttura field

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					// --- Recupero NodID (64bit) ---
					NodID_64_value = XMLType2ASCII( (void*) &node->NodID, t_u_int_64 ); // -MALLOC
					// --- Recupero ZonID (64bit) ---
					ZonID_64 = SPVGetZonID( node );
					ZonID_64_value = XMLType2ASCII( (void*) &ZonID_64, t_u_int_64 ); // -MALLOC
					// --- Recupero Name ---
					Name_value = node->Name;
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveNode" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{

						// --- Preparo la query per verificare se riga esiste ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

						query_len = _SQL_SELECT_nodes_01( query, query_len, NodID_64_value );

						// --- Lancio la query ---
						try
						{

							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 ); // Recupero il tipo
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_UPDATE_nodes_01( query, query_len, NodID_64_value, ZonID_64_value, Name_value );

										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveNode:Update" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPDATE_nodes_01", "_SaveNode" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPDATE_nodes_01", "_SaveNode" );
									}
								}
								else
								{
									// --- La riga non � presente nella tabella del DB per cui la inserisco ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_INSERT_nodes_01( query, query_len, NodID_64_value, ZonID_64_value, Name_value );
										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveNode:Insert" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_nodes_01", "_SaveNode" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_nodes_01", "_SaveNode" );
									}
								}
							}
							else
							{
								_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveNode:Select" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_nodes_01", "_SaveNode" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_GENERAL_ERROR;
						_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_nodes_01", "_SaveNode" );
					}

				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveNode" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( NodID_64_value  != NULL ) free( NodID_64_value  ); // -FREE
					if ( ZonID_64_value  != NULL ) free( ZonID_64_value  ); // -FREE
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveNode" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di un nodo", "_SaveNode" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveNode" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveNode" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per salvare nel DB un nodo
///
/// \date [08.05.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveNode( void * pnode )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn     = NULL; // Ptr a struttura connessione
	SPV_NODE * 		node 		= (SPV_NODE*)pnode    ;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveNode" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveNode( db_conn, node );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio di un nodo nel DataBase", "SPVDBMSaveNode" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione esterna per marchiare come rimosse tutte le zone
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSigneRemovedAllZones( void )
{
	int				ret_code 	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn		= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSigneRemovedAllZones" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SigneRemoved( db_conn , "zones" );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l'operazione di marcamento come rimosse di tutte le zone", "_SigneRemovedAllZones" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel DB una zona
///
/// \date [30.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveZone( SPV_DBI_CONN * db_conn, SPV_ZONE * zone )
{
	int 				ret_code 		    	= SPV_DBM_NO_ERROR;
	char                query               	[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	long                query_len           	= 0; // Lunghezza stringa query
	char * 				RegID_64_value  		= NULL;
	unsigned __int64    RegID_64        		= 0;
	char *              ZonID_64_value  		= NULL;
	char *              Name_value      		= NULL;
	SPV_DBI_FIELD * 	field           		= NULL; // Ptr a struttura field

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					// --- Recupero ZonID (64bit) ---
					ZonID_64_value = XMLType2ASCII( (void*) &zone->ZonID, t_u_int_64 ); // -MALLOC
					// --- Recupero RegID (64bit) ---
					RegID_64 = SPVGetRegID( zone );
					RegID_64_value = XMLType2ASCII( (void*) &RegID_64, t_u_int_64 ); // -MALLOC
					// --- Recupero Name ---
					Name_value = zone->Name;
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveZone" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{

						// --- Preparo la query per verificare se riga esiste ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

						query_len = _SQL_SELECT_zones_01( query, query_len, ZonID_64_value );

						// --- Lancio la query ---
						try
						{

							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 ); // Recupero il tipo
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_UPDATE_zones_01( query, query_len, ZonID_64_value, RegID_64_value, Name_value );

										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveZone:Update" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPDATE_zones_01", "_SaveZone" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPDATE_zones_01", "_SaveZone" );
									}
								}
								else
								{
									// --- La riga non � presente nella tabella del DB per cui la inserisco ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_INSERT_zones_01( query, query_len, ZonID_64_value, RegID_64_value, Name_value );
										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveZone:Insert" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_zones_01", "_SaveZone" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_zones_01", "_SaveZone" );
									}
								}
							}
							else
							{
								_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveZone:Select" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_zones_01", "_SaveZone" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_GENERAL_ERROR;
						_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_zones_01", "_SaveZone" );
					}

				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveZone" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( ZonID_64_value  != NULL ) free( ZonID_64_value  ); // -FREE
					if ( RegID_64_value  != NULL ) free( RegID_64_value  ); // -FREE
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveZone" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di una zona", "_SaveZone" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveZone" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveZone" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per salvare nel DB una zona
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveZone( void * pzone )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn     = NULL; // Ptr a struttura connessione
	SPV_ZONE * 		zone 		= (SPV_ZONE*)pzone;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveZone" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveZone( db_conn, zone );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio di una zona nel DataBase", "SPVDBMSaveZone" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione esterna per marchiare come rimosse tutti server
///
/// \date [17.12.2009]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
int SPVDBMSignRemovedAllServers( void )
{
	int				ret_code 	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn		= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSignRemovedAllServers" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SigneRemoved( db_conn , "servers" );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l'operazione di marcamento come rimosse di tutti i server", "SPVDBMSignRemovedAllServers" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}

//==============================================================================
/// Funzione esterna per marchiare come rimosse tutte le regioni
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSigneRemovedAllRegions( void )
{
	int				ret_code 	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn		= NULL; // Ptr a struttura connessione

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSigneRemovedAllRegions" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SigneRemoved( db_conn , "regions" );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante l'operazione di marcamento come rimosse di tutte le regioni", "_SigneRemovedAllRegions" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel DB una regione
///
/// \date [30.01.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	_SaveRegion( SPV_DBI_CONN * db_conn, SPV_REGION * region )
{
	int 				ret_code 		    	= SPV_DBM_NO_ERROR;
	char                query               	[SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	long                query_len           	= 0; // Lunghezza stringa query
	unsigned __int64 * 	RegID_64_ptr    		= NULL;
	char * 				RegID_64_value  		= NULL;
	char *              Name_value      		= NULL;
	SPV_DBI_FIELD * 	field           		= NULL; // Ptr a struttura field

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					// --- Recupero RegID (64bit) ---
					RegID_64_value = XMLType2ASCII( (void*) &region->RegID, t_u_int_64 ); // -MALLOC
					// --- Recupero Name ---
					Name_value = region->Name;
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveRegion" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{

						// --- Preparo la query per verificare se riga esiste ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

						query_len = _SQL_SELECT_regions_01( query, query_len, RegID_64_value );

						// --- Lancio la query ---
						try
						{

							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 ); // Recupero il tipo
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_UPDATE_regions_01( query, query_len, RegID_64_value, Name_value );

										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveRegion:Update" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPDATE_regions_01", "_SaveRegion" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPDATE_regions_01", "_SaveRegion" );
									}
								}
								else
								{
									// --- La riga non � presente nella tabella del DB per cui la inserisco ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_INSERT_regions_01( query, query_len, RegID_64_value, Name_value );
										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveRegion:Insert" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_regions_01", "_SaveRegion" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_regions_01", "_SaveRegion" );
									}
								}
							}
							else
							{
								_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveRegion:Select" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_regions_01", "_SaveRegion" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_GENERAL_ERROR;
						_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_regions_01", "_SaveRegion" );
					}

				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveRegion" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( RegID_64_value  != NULL ) free( RegID_64_value  ); // -FREE
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveRegion" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di una regione", "_SaveRegion" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveRegion" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveRegion" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per salvare nel DB una regione
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveRegion( void * pregion )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn     = NULL; // Ptr a struttura connessione
	SPV_REGION *   	region 		= (SPV_REGION*)pregion;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveRegion" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveRegion( db_conn, region );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio di una regione nel DataBase", "SPVDBMSaveRegion" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


//==============================================================================
/// Funzione interna per salvare nel DB un server
///
/// \date [04.09.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	_SaveServer( SPV_DBI_CONN * db_conn, SPV_SERVER * server )
{
	int 				ret_code		= SPV_DBM_NO_ERROR;
	char                query           [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
	long                query_len       = 0; // Lunghezza stringa query
	char *         		SrvID_32_value  = NULL;
	char *         		NodID_64_value  = NULL;
	char *         		Name_value      = NULL;
	char *         		Host_value      = NULL;
	char *		  		xml_text		= NULL;
	SPV_DBI_FIELD * 	field           = NULL; // Ptr a struttura field

	#define QUERY_COLUMNS       "SrvID,NodID,Name,Host,Removed,SupervisorSystemXML"

	try
	{
		// --- Controlli iniziali sulla connessione DB ---
		ret_code = _StartConnCheck( db_conn );
		if ( ret_code == SPV_DBM_NO_ERROR )
		{
			try
			{
				// --- Recupero i parametri ---
				try
				{
					// --- Recupero il file System.xml ---
					xml_text = ULReadASCIIFile(SPVSystem.Source->name);
					// --- Rimuovo eventuali apici ---
					ULStrSubstitute( xml_text, (char)39, (char)96 );
					// --- Recupero SrvID ---
					SrvID_32_value = XMLType2ASCII( (void*) &server->SrvID, t_u_int_32 ); // -MALLOC
					// --- Recuper NodID ---
					NodID_64_value = XMLType2ASCII( (void*) &server->NodID, t_u_int_64 ); // -MALLOC
					// --- Recupero Name ---
					Name_value = server->Name;
					// --- Recupero Host ---
					Host_value = server->Host;
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la conversione ASCII dei valori", "_SaveServer" );
				}

				if ( ret_code == SPV_DBM_NO_ERROR )
				{
					try
					{
						// --- Preparo la query per verificare se riga esiste ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );

						query_len = _SQL_SELECT_servers_01( query, query_len, SrvID_32_value );

						// --- Lancio la query ---
						try
						{

							ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
							if ( ret_code == SPV_DBM_NO_ERROR )
							{
								field = SPVDBGetField( db_conn, 0, 0 ); // Recupero il tipo
								// --- Se la riga � gi� presente nella tabella del DB ---
								if ( field != NULL )
								{
									// --- La riga � gi� presente nella tabella del DB per cui la modifico ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_UPDATE_servers_01( query, query_len, SrvID_32_value, NodID_64_value, Name_value, Host_value, xml_text );
										try
										{
											ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveServer:Update" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_UPDATE_servers_01", "_SaveServer" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_UPDATE_servers_01", "_SaveServer" );
									}
								}
								else
								{
									// --- La riga non � presente nella tabella del DB per cui la inserisco ---
									try
									{
										query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
										query_len = _SQL_INSERT_servers_01( query, query_len, SrvID_32_value, NodID_64_value, Name_value, Host_value, xml_text );
										try
										{
											// --- Lancio la (query) inserzione di una nuova riga ---
											ret_code = SPVDBWriteRow( db_conn, "servers", QUERY_COLUMNS, query, query_len );

											//ret_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
											if ( ret_code != SPV_DBM_NO_ERROR )
											{
												_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveServer:Insert" );
												ret_code = SPV_DBM_DB_QUERY_FAILURE;
											}
										}
										catch(...)
										{
											ret_code = SPV_DBM_FUNCTION_EXCEPTION;
											_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_INSERT_servers_01", "_SaveServer" );
										}
									}
									catch(...)
									{
										ret_code = SPV_DBM_GENERAL_ERROR;
										_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_INSERT_servers_01", "_SaveServer" );
									}
								}
							}
							else
							{
								_Log( db_conn, 2, SPV_DBM_EVENT_DB_QUERY_FAILURE, db_conn->LastError, "_SaveServer:Select" );
								ret_code = SPV_DBM_DB_QUERY_FAILURE;
							}
						}
						catch(...)
						{
							ret_code = SPV_DBM_FUNCTION_EXCEPTION;
							_Log( db_conn, 2, ret_code, "Eccezione in fase di esecuzione della query _SQL_SELECT_servers_01", "_SaveServer" );
						}
					}
					catch(...)
					{
						ret_code = SPV_DBM_GENERAL_ERROR;
						_Log( db_conn, 2, ret_code, "Errore durante la preparazione della query _SQL_SELECT_servers_01", "_SaveServer" );
					}

				}
				else
				{
					_Log( db_conn, 2, ret_code, "Impossibile eseguire la query causa parametri non recuperati", "_SaveServer" );
				}

				try
				{
					// --- Cancellazione memoria ---
					if ( SrvID_32_value != NULL ) free( SrvID_32_value ); // -FREE
					if ( NodID_64_value != NULL ) free( NodID_64_value ); // -FREE
					if ( xml_text != NULL ) free( xml_text ); // -FREE
				}
				catch(...)
				{
						ret_code = SPV_DBM_FUNCTION_EXCEPTION;
						_Log( db_conn, 2, ret_code, "Eccezione durante la cancellazione dei valori", "_SaveServer" );
				}
			}
			catch(...)
			{
				ret_code = SPV_DBM_GENERAL_ERROR;
				_Log( db_conn, 2, ret_code, "Errore durante il salvataggio di un server", "_SaveServer" );
			}

			// --- Controlli finali sulla connessione DB ---
			_FinishConnCheck( db_conn );
		}
		else
		{
			_Log( db_conn, 2, ret_code, "Errore durante i controlli iniziali DB", "_SaveServer" );
		}
	}
	catch(...)
	{
		ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale - L1", "_SaveServer" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione esterna per salvare nel DB un server
///
/// \date [08.05.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBMSaveServer( void * pserver )
{
	int				ret_code	= SPV_DBM_NO_ERROR;
	SPV_DBI_CONN *	db_conn     = NULL; // Ptr a struttura connessione
	SPV_SERVER *   	server 		= (SPV_SERVER*)pserver;

	// Recupero la struttura connessione DB
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn;
	// --- Blocco la connessione ---
	_LockConn( db_conn, "SPVDBMSaveServer" );

	try
	{
		// --- Richiamo la funzione interna ---
		ret_code = _SaveServer( db_conn, server );
	}
	catch(...)
	{
	   ret_code = SPV_DBM_FUNCTION_EXCEPTION;
		_Log( db_conn, 2, ret_code, "Eccezione generale durante il salvataggio di un server nel DataBase", "SPVDBMSaveServer" );
	}

	// --- Rilascio la connessione ---
	_ReleaseConn( db_conn );

	return ret_code;
}


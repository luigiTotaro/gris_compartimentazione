//==============================================================================
// Telefin Supervisor Server Main Form 1.0
//------------------------------------------------------------------------------
// Form principale del server (SPVServerMainForm.cpp)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:  0.12 (21.09.2004 -> 10.10.2007)
//
// Copyright: 2004-2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:	  Enrico Alborali (alborali@telefin.it)
//			  Mario Ferro (mferro@deltasistemi.it)
// Note:      richiede SPVServerMainForm.h, SPVServerMain.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVServerMainForm.h
//==============================================================================

#include  <vcl.h>
#pragma   hdrstop
#include  "SPVServerMainForm.h"
#include  "SPVServerMain.h"
#include  "SPVServerGUI.h"
#include  "SPVSystemViewForm.h"
#include  "SPVConfig.h"
#include  "SPVSTLC1000.h"
#include "UtilityLibrary.h"
#include "LogLibrary.h"

#pragma   package(smart_init)
#pragma   resource "*.dfm"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
TfServerMain * fServerMain;

//==============================================================================
// Metodi
//------------------------------------------------------------------------------

//==============================================================================
/// Metodo costruttore della classe TfServerMain.
///
/// \date [10.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.11
//------------------------------------------------------------------------------
__fastcall TfServerMain::TfServerMain( TComponent* Owner ) : TForm(Owner)
{
  int ret_code  = SPV_SERVER_MAIN_NO_ERROR;

  // --- Inizializzazione form ---
  this->tiServerMain->Hint      = "Telefin Supervisor 3.0\nServer in attesa";

  // --- Dimensioni form ---
  this->Left                      = 0;
  this->Top                       = 0;
  this->Width                     = 320;
  this->Height                    = 400;
  // --- Colori form ---
  this->Color                     = (TColor)0x00D8E9EC;

  // --- Dimensioni gbStatus ---
  this->gbStatus->Left            = 5;
  this->gbStatus->Top             = 32;
  this->gbStatus->Width           = 303;
  this->gbStatus->Height          = 303;
  // --- Colori gbStatus ---
  this->gbStatus->Color           = (TColor)0x00D8E9EC;
  // --- Font gbStatus ---
  this->gbStatus->Font->Color     = (TColor)0x00C56A31;
  this->gbStatus->Font->Name      = "MS Sans Serif";
  this->gbStatus->Font->Size      = 8;

  // --- Dimensioni lStatus ---
  this->lStatus->Left             = 30;
  this->lStatus->Top              = 16;
  this->lStatus->Width            = 102;
  this->lStatus->Height           = 18;
  // --- Colori lStatus ---
  this->lStatus->Color            = (TColor)0x00D8E9EC;
  // --- Font lStatus ---
  this->lStatus->Font->Color      = (TColor)0x00000000;
  this->lStatus->Font->Name       = "Trebuchet MS";
  this->lStatus->Font->Size       = 8;

  // --- Dimensioni lLevel ---
  this->lLevel->Left              = 132;
  this->lLevel->Top               = 16;
  this->lLevel->Width             = 167;
  this->lLevel->Height            = 16;
  // --- Colori lLevel ---
  this->lLevel->Color             = (TColor)0x00D8E9EC;
  // --- Font lLevel ---
  this->lLevel->Font->Color       = (TColor)0x00888888;
  this->lLevel->Font->Name        = "Trebuchet MS";
  this->lLevel->Font->Size        = 8;

  // --- Dimensioni pBluLineStatus ---
  this->pBluLineStatus->Left      = 2;
  this->pBluLineStatus->Top       = 39;
  this->pBluLineStatus->Width     = 299;
  this->pBluLineStatus->Height    = 3;
  // --- Colori pBluLineStatus ---
  this->pBluLineStatus->Color     = (TColor)0x00C56A31;

  // --- Dimensioni pStatus ---
  this->pStatus->Left             = 2;
  this->pStatus->Top              = 42;
  this->pStatus->Width            = 8;
  this->pStatus->Height           = 259;
  // --- Colori pStatus ---
  this->pStatus->Color            = (TColor)0x00CCFFFF;

  // --- Dimensioni mMainLog ---
  this->mMainLog->Left            = 10;
  this->mMainLog->Top             = 42;
  this->mMainLog->Width           = 291;
  this->mMainLog->Height          = 259;
  // --- Colori mMainLog ---
  this->mMainLog->Color           = (TColor)0x00CCFFFF;
  // --- Font mMainLog ---
  this->mMainLog->Font->Color     = (TColor)0x00000000;
  this->mMainLog->Font->Name      = "MS Sans Serif";
  this->mMainLog->Font->Size      = 8;

  // --- Dimensioni bStartServer ---
  this->bStartServer->Left        = 4;
  this->bStartServer->Top         = 4;
  this->bStartServer->Width       = 75;
  this->bStartServer->Height      = 25;
  // --- Font bStartServer ---
  this->bStartServer->Font->Color = (TColor)0x00000000;
  this->bStartServer->Font->Name  = "MS Sans Serif";
  this->bStartServer->Font->Size  = 8;

  // --- Dimensioni bStopServer ---
  this->bStopServer->Left         = 82;
  this->bStopServer->Top          = 4;
  this->bStopServer->Width        = 75;
  this->bStopServer->Height       = 25;
  // --- Font bStopServer ---
  this->bStopServer->Font->Color  = (TColor)0x00000000;
  this->bStopServer->Font->Name   = "MS Sans Serif";
  this->bStopServer->Font->Size   = 8;

  // --- Dimensioni bCloseServer ---
  this->bCloseServer->Left        = 232;
  this->bCloseServer->Top         = 4;
  this->bCloseServer->Width       = 75;
  this->bCloseServer->Height      = 25;
  // --- Font bCloseServer ---
  this->bCloseServer->Font->Color = (TColor)0x00000000;
  this->bCloseServer->Font->Name  = "MS Sans Serif";
  this->bCloseServer->Font->Size  = 8;

  // --- Colori sbServer ---
  this->sbServer->Color           = (TColor)0x00D8E9EC;
  // --- Font sbServer ---
  this->sbServer->Font->Color     = (TColor)0x00000000;
  this->sbServer->Font->Name      = "MS Sans Serif";
  this->sbServer->Font->Size      = 8;

  // --- Inizializzo il modulo GUI ---
  SPVServerGUIInit( );

  // --- Imposto la lista globale della icone di stato ---
  StatusIconList = (void*) this->ilStatus;

  this->SPVServerIdleStatus( "Server in caricamento", "Server in caricamento...", false );

  // --- Inizializzazione Modulo principale del Server ---
  SPVServerMainInit( (void*) this->mMainLog );

  // --- Carico il server ---
  ret_code = SPVServerMainLoad( );

  if ( SPVConfigInfo.ServerMain.Service )
  {
    try
    {
      this->Visible = true;
    }
    catch(...)
    {
    }
  }
  else
  {
    try
    {
      if ( SPVConfigInfo.ServerMain.TrayIcon )
      {
        this->tiServerMain->Visible = true;
      }
    }
    catch(...)
    {
    }
  }
  
  if ( ret_code == SPV_SERVER_MAIN_NO_ERROR )
  {
    this->SPVServerIdleStatus( "Server in attesa", "Server in attesa.\nCaricamento del Server avvenuto con successo.", true );
  }
  else
  {
    char * str_code = XMLType2ASCII( &ret_code, t_u_int_32 );
    this->SPVServerErrorStatus( ret_code, "Server in errore", strcat( "Server in Errore.\nSi � verificato un errore nel caricamento del server.\nCodice di errore: ", str_code ), true );
    if ( str_code != NULL ) free( str_code );
  }
}

//==============================================================================
/// Metodo per impostare lo stato di ERROR del server.
///
/// \date [09.02.2006]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int __fastcall TfServerMain::SPVServerErrorStatus( int code, char * caption, char * text, bool balloon )
{
  int         ret_code      = SPV_SERVER_MAIN_NO_ERROR;
  AnsiString  balloon_text  = "";

  this->ilTrayIcons->GetIcon( 3, Application->Icon );
  this->ilTrayIcons->GetIcon( 3, this->Icon );
  this->tiServerMain->IconIndex = 3;

  SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_ERROR );

  if ( caption != NULL )
  {
    this->lStatus->Caption        = AnsiString( caption );
  }
  else
  {
    this->lStatus->Caption        = AnsiString( "Server in errore" );
  }
  if ( text != NULL )
  {
    this->tiServerMain->Hint   = AnsiString( text );
    balloon_text                 = AnsiString( text );
  }
  else
  {
    this->tiServerMain->Hint   = AnsiString( "Server in errore.\nCodice di errore: " ) + AnsiString(code) + AnsiString("." );
    balloon_text                 = AnsiString( "Server in errore.\nCodice di errore: " ) + AnsiString(code) + AnsiString("." );
  }
  if ( balloon == true )
  {

    //this->tiServerMain->ShowBalloonHint( "Telefin Supervisor 3.0", balloon_text, itError, SPV_SERVERGUI_BALLOON_ERROR_TIMEOUT );
  }

  // --- Gestione LED Stato per STLC1000 ---
  if ( SPVConfigInfo.Hardware.Type == SPV_CFG_HARDWARE_TYPE_STLC1000 )
  {
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_GREEN_LED, false );
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_RED_LED, true );
  }

  return ret_code;
}

//==============================================================================
/// Metodo per impostare lo stato di WARNING del server.
///
/// \date [09.02.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int __fastcall TfServerMain::SPVServerWarningStatus( int code, char * caption, char * text, bool balloon )
{
  int         ret_code      = SPV_SERVER_MAIN_NO_ERROR;
  AnsiString  balloon_text  = "";

  this->ilTrayIcons->GetIcon( 2, Application->Icon );
  this->ilTrayIcons->GetIcon( 2, this->Icon );
  this->tiServerMain->IconIndex = 2;

  SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_WARNING );

  if ( caption != NULL )
  {
    this->lStatus->Caption          = AnsiString( caption );
  }
  else
  {
    this->lStatus->Caption          = AnsiString( "Server in attenzione" );
  }
  if ( text != NULL )
  {
    this->tiServerMain->Hint     = AnsiString( text );
    balloon_text                    = AnsiString( text );
  }
  else
  {
    this->tiServerMain->Hint     = AnsiString( "Server in attenzione.\nCodice di errore: " ) + AnsiString(code) + AnsiString(".");
    balloon_text                    = AnsiString( "Server in attenzione.\nCodice di errore: " ) + AnsiString(code) + AnsiString(".");
  }
  if ( balloon == true )
  {
    //this->tiServerMain->ShowBalloonHint( "Telefin Supervisor 3.0", balloon_text, itWarning, SPV_SERVERGUI_BALLOON_WARNING_TIMEOUT );
  }

  // --- Gestione LED Stato per STLC1000 ---
  if ( SPVConfigInfo.Hardware.Type == SPV_CFG_HARDWARE_TYPE_STLC1000 )
  {
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_GREEN_LED, true );
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_RED_LED, true );
  }

  return ret_code;
}

//==============================================================================
/// Metodo per impostare lo stato di IDLE del server.
///
/// \date [09.02.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int __fastcall TfServerMain::SPVServerIdleStatus( char * caption, char * text, bool balloon )
{
  int         ret_code      = SPV_SERVER_MAIN_NO_ERROR;
  AnsiString  balloon_text  = "";

  this->ilTrayIcons->GetIcon( 0, Application->Icon );
  this->ilTrayIcons->GetIcon( 0, this->Icon );
  this->tiServerMain->IconIndex = 0;

  SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_IDLE );

  if ( caption != NULL )
  {
    this->lStatus->Caption          = AnsiString( caption );
  }
  else
  {
    this->lStatus->Caption          = AnsiString( "Server Idle" );
  }
  if ( text != NULL )
  {
    this->tiServerMain->Hint     = AnsiString( text );
    balloon_text                    = AnsiString( text );
  }
  else
  {
    this->tiServerMain->Hint     = AnsiString( "Server in attesa." );
    balloon_text                    = AnsiString( "Server in attesa." );
  }
  if ( balloon == true )
  {
    //this->osMainTrayIcon->ShowBalloonHint( "Telefin Supervisor 3.0", balloon_text, itWarning, SPV_SERVERGUI_BALLOON_WARNING_TIMEOUT );
  }

  // --- Gestione LED Stato per STLC1000 ---
  if ( SPVConfigInfo.Hardware.Type == SPV_CFG_HARDWARE_TYPE_STLC1000 )
  {
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_GREEN_LED, false );
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_RED_LED, false );
  }

  return ret_code;
}

//==============================================================================
/// Metodo per impostare lo stato di OK del server.
///
/// \date [09.02.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int __fastcall TfServerMain::SPVServerOkStatus( char * caption, char * text, bool balloon )
{
  int         ret_code      = SPV_SERVER_MAIN_NO_ERROR;
  AnsiString  balloon_text  = "";

  this->ilTrayIcons->GetIcon( 1, Application->Icon );
  this->ilTrayIcons->GetIcon( 1, this->Icon );
  this->tiServerMain->IconIndex = 1;

  SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );

  if ( caption != NULL )
  {
    this->lStatus->Caption = AnsiString( caption );
  }
  else
  {
    this->lStatus->Caption = AnsiString( "Server avviato" );
  }
  if ( text != NULL )
  {
    this->tiServerMain->Hint     = AnsiString( text );
    balloon_text                    = AnsiString( text );
  }
  else
  {
    this->tiServerMain->Hint     = AnsiString( "Server avviato." );
    balloon_text                    = AnsiString( "Server avviato." );
  }
  if ( balloon == true )
  {
    //this->tiServerMain->ShowBalloonHint( "Telefin Supervisor 3.0", balloon_text, itWarning, SPV_SERVERGUI_BALLOON_OK_TIMEOUT );
  }

  // --- Gestione LED Stato per STLC1000 ---
  if ( SPVConfigInfo.Hardware.Type == SPV_CFG_HARDWARE_TYPE_STLC1000 )
  {
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_GREEN_LED, true );
    STLC1KSetSystemLED( SPV_STLC1000_SYSTEM_RED_LED, false );
  }

  return ret_code;
}

//==============================================================================
/// Metodo che cattura l'evento OnClick sull'oggetto TfServerMain::bStartServer.
///
/// \date [30.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfServerMain::bStartServerClick( TObject *Sender )
{
  int ret_code;


  ret_code = SPVServerMainStart();
  if ( ret_code == 0 )
  {
    this->SPVServerOkStatus( "Server in funzione", "Server in funzione.\nIl Server sta funzionando correttamente.", true );
    // --- Abilitazione / disabilitazione bottoni ---
    this->bStartServer->Enabled = false;
    this->bCloseServer->Enabled = false;
    this->miExit->Enabled       = false;
    this->bStopServer->Enabled  = true;
  }
  else
  {
    if ( ret_code == 1 )
    {
      this->SPVServerWarningStatus( ret_code, "Server in attenzione", "Server avviato.\nTuttavia si � verificato un errore nell'avvio del server.", true );
      // --- Abilitazione / disabilitazione bottoni ---
      this->bStartServer->Enabled = false;
      this->bCloseServer->Enabled = false;
      this->miExit->Enabled       = false;
      this->bStopServer->Enabled  = true;
    }
    else
    {
      this->SPVServerErrorStatus( ret_code, "Server in errore", NULL, true );
      // --- Abilitazione / disabilitazione bottoni ---
      this->bStopServer->Enabled  = false;
      this->bStartServer->Enabled = true;
      this->bCloseServer->Enabled = true;
      this->miExit->Enabled       = true;
    }
  }
}

//==============================================================================
/// Metodo che cattura l'evento OnClick sull'oggetto TfServerMain::bStopServer.
///
/// \date [30.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfServerMain::bStopServerClick( TObject *Sender )
{
  int ret_code = SPVServerMainStop();

  this->SPVServerIdleStatus( "Server in attesa", "Server arrestato dall'utente.", true );
  // --- Abilitazione / disabilitazione bottoni ---
  this->bStopServer->Enabled  = false;
  this->bStartServer->Enabled = true;
  this->bCloseServer->Enabled = true;
  this->miExit->Enabled       = true;
}

//==============================================================================
///
/// \date [21.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miServerViewClick( TObject *Sender )
{
  if ( fServerMain->miServerView->Checked )
  {
    SPVHideServerMainForm();
  }
  else
  {
    SPVShowServerMainForm();
  }
}

//==============================================================================
///
/// \date [24.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miSystemViewClick( TObject *Sender )
{
  if ( fServerMain->miSystemView->Checked )
  {
    SPVCloseSystemView();
  }
  else
  {
    SPVShowSystemView( &SPVSystem );
  }
}

//==============================================================================
///
/// \date [29.08.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miExitClick( TObject *Sender )
{
  int fun_code = 0;

  this->Close();
}

//==============================================================================
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miPortViewClick(TObject *Sender)
{
  if ( fServerMain->miPortView->Checked )
  {
    SPVClosePortView();
  }
  else
  {
    SPVShowPortView();
  }
}

//==============================================================================
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miDatabaseViewClick(TObject *Sender)
{
  if ( fServerMain->miDatabaseView->Checked )
  {
    SPVCloseDatabaseView();
  }
  else
  {
    SPVShowDatabaseView();
  }
}

//==============================================================================
/// Metodo che cattura l'evento di apertura del popupmenu principale
///
/// \date [07.07.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfServerMain::popMainMenuPopup( TObject * Sender )
{
  this->miStartServer->Enabled      = !SPVServerMainStatus.scheduling;
  this->miStopServer->Enabled       = SPVServerMainStatus.scheduling;

  this->miServerView->Checked       = this->Visible;
  this->miSystemView->Checked       = SPVGetSystemViewStatus();
  this->miApplicView->Checked       = SPVGetApplicViewStatus();
  this->miComProtView->Checked      = SPVGetComProtViewStatus();
  this->miPortView->Checked         = SPVGetPortViewStatus();
  this->miDatabaseView->Checked     = SPVGetDatabaseViewStatus();
}

//==============================================================================
/// Metodo di controllo della chiusura del server
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfServerMain::FormCloseQuery( TObject *Sender, bool &CanClose )
{
  if ( !SPVConfigInfo.ServerMain.Service )
  {
    CanClose = ( Application->MessageBoxA("Sei sicuro di voler chiudere il server di diagnostica Telefin?", "Richiesta di chiusura Server", MB_YESNO ) == IDYES	)? true:false;
  }  
}

void __fastcall TfServerMain::miApplicViewClick(TObject *Sender)
{
  if ( fServerMain->miApplicView->Checked )
  {
    SPVCloseApplicView();
  }
  else
  {
    SPVShowApplicView();
  }
}
//---------------------------------------------------------------------------

void __fastcall TfServerMain::miComProtViewClick(TObject *Sender)
{
  if ( fServerMain->miComProtView->Checked )
  {
    SPVCloseComProtView();
  }
  else
  {
    SPVShowComProtView();
  }
}
//---------------------------------------------------------------------------







//==============================================================================
///
/// \date [29.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfServerMain::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  int fun_code = 0;

  fun_code = SPVServerMainUnload();
}
//---------------------------------------------------------------------------

void __fastcall TfServerMain::FormDestroy(TObject *Sender)
{
  SPVServerMainClear( );
}
//---------------------------------------------------------------------------

void __fastcall TfServerMain::ctiMainTrayIconMinimizeToTray(
      TObject *Sender)
{
  SPVCloseSystemView  ( );
  SPVCloseApplicView  ( );
  SPVCloseComProtView ( );
  SPVClosePortView    ( );
  SPVCloseDatabaseView( );
}
//---------------------------------------------------------------------------



void __fastcall TfServerMain::bCloseServerClick(TObject *Sender)
{
	this->Close();
}
//---------------------------------------------------------------------------


//==============================================================================
///
/// \date [21.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miShowAllClick(TObject *Sender)
{

    SPVShowServerMainForm();
    SPVShowPortView();
    SPVShowDatabaseView();
    SPVShowApplicView();
    SPVShowComProtView();
    SPVShowSystemView( &SPVSystem );
}
//---------------------------------------------------------------------------

//==============================================================================
///
/// \date [21.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfServerMain::miCloseAllClick(TObject *Sender)
{
    SPVHideServerMainForm();
    SPVCloseSystemView();
    SPVClosePortView();
    SPVCloseDatabaseView();
    SPVCloseApplicView();
    SPVCloseComProtView();
}

//==============================================================================
/// Metodo che cattura l'evento di OnPaint del form
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfServerMain::FormPaint(TObject *Sender)
{
  // --- Gestione speciale dell'Icon Tray in modalit� servizio ---
  if ( SPVConfigInfo.ServerMain.Service )
  {
    // --- Se l'icona era gi� visibile la tolgo ---
    try
    {
      if ( this->tiServerMain->Visible )
      {
        this->tiServerMain->Visible = false;
      }
    }
    catch(...)
    {

    }
    // --- Visualizzo l'icona ---
    try
    {
      if ( SPVConfigInfo.ServerMain.TrayIcon )
      {
        this->tiServerMain->Visible = true;
      }
    }
    catch(...)
    {

    }
  }
}


void __fastcall TfServerMain::miExoprtThreadListClick(TObject *Sender)
{
	SYSExportThreadListManager();
}
//---------------------------------------------------------------------------

void __fastcall TfServerMain::miDestroyLogFileClick(TObject *Sender)
{
	LLDestroyLogFile();
}
//---------------------------------------------------------------------------


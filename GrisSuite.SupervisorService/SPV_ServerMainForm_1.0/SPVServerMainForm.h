//==============================================================================
// Telefin Supervisor Server Main Form 1.0
//------------------------------------------------------------------------------
// Header Form principale del server (SPVServerMainForm.h)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:  0.12 (21.09.2004 -> 10.10.2007)
//
// Copyright: 2004-2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:	  Enrico Alborali (alborali@telefin.it)
//			  Mario Ferro (mferro@deltasistemi.it)
// Note:      richiede SPVServerMainForm.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [21.09.2004]:
// - Prima versione del form.
// 0.02 [24.09.2004]:
// - Aggiunto il metodo TfServerMain::miServerViewClick.
// - Aggiunto il metodo TfServerMain::miSystemViewClick.
// - Aggiunto il metodo TfServerMain::ctiMainTrayIconStartup.
// 0.03 [01.02.2005]:
// - Modificato il metodo costruttore TfServerMain::TfServerMain.
// - Aggiunto il metodo TfServerMain::SPVServerErrorStatus.
// - Aggiunto il metodo TfServerMain::SPVServerWarningStatus.
// - Aggiunto il metodo TfServerMain::SPVServerIdleStatus.
// - Aggiunto il metodo TfServerMain::SPVServerOkStatus.
// - Modificato il metodo TfServerMain::bStartServerClick.
// 0.04 [24.05.2005]:
// - Modificato il metodo TfServerMain::SPVServerErrorStatus.
// - Modificato il metodo TfServerMain::SPVServerWarningStatus.
// - Modificato il metodo TfServerMain::SPVServerIdleStatus.
// - Modificato il metodo TfServerMain::SPVServerOkStatus.
// 0.05 [07.07.2005]:
// - Modificato il metodo TfServerMain::popMainMenuPopup.
// 0.06 [29.08.2005]:
// - Modificato il metodo TfServerMain::miExitClick.
// - Aggiunto il metodo TfServerMain::FormClose.
// 0.07 [22.09.2005]:
// - Modificato il metodo TfServerMain::TfServerMain.
// - Modificato il metodo TfServerMain::SPVServerErrorStatus.
// - Modificato il metodo TfServerMain::SPVServerWarningStatus.
// - Modificato il metodo TfServerMain::SPVServerIdleStatus.
// - Modificato il metodo TfServerMain::SPVServerOkStatus.
// 0.08 [29.09.2005]:
// - Modificato il metodo costruttore TfServerMain::TfServerMain.
// - Modificato il metodo TfServerMain::SPVServerErrorStatus.
// - Modificato il metodo TfServerMain::SPVServerWarningStatus.
// - Modificato il metodo TfServerMain::SPVServerIdleStatus.
// - Modificato il metodo TfServerMain::SPVServerOkStatus.
// - Modificato il metodo TfServerMain::ctiMainTrayIconStartup.
// - Modificato il metodo TfServerMain::miShowAllClick.
// - Modificato il metodo TfServerMain::miCloseAllClick.
// 0.09 [21.11.2005]:
// - Terminato l'utilizzo della classe TCoolTrayIcon.
// - Inclusa la libreria ORTUS SHELL COMPONENTS v2.34 FOR BORLAND® C++BUILDER 6
// - Sostituito l'oggetto ctiMainTrayIcon con osMainTrayIcon.
// - Modificato il metodo costruttore TfServerMain::TfServerMain.
// - Modificato il metodo TfServerMain::SPVServerErrorStatus.
// - Modificato il metodo TfServerMain::SPVServerWarningStatus.
// - Modificato il metodo TfServerMain::SPVServerIdleStatus.
// - Modificato il metodo TfServerMain::SPVServerOkStatus.
// - Modificato il metodo TfServerMain::miServerViewClick.
// - Modificato il metodo TfServerMain::miShowAllClick.
// - Modificato il metodo TfServerMain::miCloseAllClick.
// 0.10 [30.11.2005]:
// - Modificato il metodo costruttore TfServerMain::TfServerMain.
// - Modificato il metodo TfServerMain::SPVServerErrorStatus.
// - Modificato il metodo TfServerMain::SPVServerWarningStatus.
// - Modificato il metodo TfServerMain::SPVServerIdleStatus.
// - Modificato il metodo TfServerMain::SPVServerOkStatus.
// - Modificato il metodo TfServerMain::bStartServerClick.
// - Modificato il metodo TfServerMain::bStopServerClick.
// 0.11 [19.12.2005]:
// - Modificato il metodo costruttore TfServerMain::TfServerMain.
// - Modificato il metodo TfServerMain::FormCloseQuery.
// - Modificato il metodo TfServerMain::FormPaint.
// 0.12 [10.10.2007]:
// - Modificato il metodo costruttore TfServerMain::TfServerMain.
// - Modificato il metodo TfServerMain::bStartServerClick.
// - Modificato il metodo TfServerMain::bStopServerClick.
// - Modificato il metodo TfServerMain::FormClose.

//==============================================================================
#ifndef SPVServerMainFormH
#define SPVServerMainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Buttons.hpp>
#include <ToolWin.hpp>
#include <XPMan.hpp>

//==============================================================================
// Classi
//------------------------------------------------------------------------------
class TfServerMain : public TForm
{
__published:	// IDE-managed Components
  TButton *bStartServer;
  TStatusBar *sbServer;
  TButton         * bStopServer;
  TPopupMenu      * popMainMenu;
  TMenuItem       * miStartServer;
  TMenuItem       * miStopServer;
  TMenuItem       * miServerView;
  TMenuItem       * miSystemView;
  TMenuItem       * miExit;
  TMenuItem       * miPortView;
  TMenuItem       * miDatabaseView;
  TGroupBox       * gbStatus;
  TImage          * imgStatus;
  TLabel          * lStatus;
  TLabel          * lLevel;
  TPanel          * pBluLineStatus;
  TMemo           * mMainLog;
  TPanel          * pStatus;
  TMenuItem       * miAbout;
  TMenuItem       * N1;
  TMenuItem       * N3;
  TMenuItem       * N4;
  TMenuItem *miApplicView;
  TMenuItem *miComProtView;
  TMenuItem *N5;
  TListView *lvMain;
  TImageList *ilTrayIcons;
  TButton *bCloseServer;
  TMenuItem *N2;
  TMenuItem *miShowAll;
  TMenuItem *miCloseAll;
  TImageList *ilStatus;
	TTrayIcon *tiServerMain;
	TXPManifest *XPManifest1;
	TMenuItem *N6;
	TMenuItem *miExoprtThreadList;
	TMenuItem *miDestroyLogFile;
	TMenuItem *N7;
  void __fastcall   bStartServerClick       ( TObject *Sender );
  void __fastcall   bStopServerClick        ( TObject *Sender );
  void __fastcall   miServerViewClick       ( TObject *Sender );
  void __fastcall   miSystemViewClick       ( TObject *Sender );
  void __fastcall   miExitClick             ( TObject *Sender );
  void __fastcall   miPortViewClick         ( TObject *Sender );
  void __fastcall   miDatabaseViewClick     ( TObject *Sender );
  void __fastcall   popMainMenuPopup        ( TObject *Sender );
  /// Metodo di controllo della chiusura del server
  void __fastcall   FormCloseQuery          ( TObject *Sender, bool &CanClose );
  void __fastcall miApplicViewClick(TObject *Sender);
  void __fastcall miComProtViewClick(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
  void __fastcall FormDestroy(TObject *Sender);
  void __fastcall ctiMainTrayIconMinimizeToTray(TObject *Sender);
  void __fastcall bCloseServerClick(TObject *Sender);
  void __fastcall miShowAllClick(TObject *Sender);
  void __fastcall miCloseAllClick(TObject *Sender);
  void __fastcall FormPaint(TObject *Sender);
	void __fastcall miExoprtThreadListClick(TObject *Sender);
	void __fastcall miDestroyLogFileClick(TObject *Sender);
private:	// User declarations
  /*          
  TIcon           * iIdle;
  TIcon           * iOk;
  TIcon           * iWarning;
  TIcon           * iError;
  */
public:		// User declarations
  ///TCoolTrayIcon   * ctiMain; ///< Ptr all'oggetto che gesticse la tray icon
  /// Metodo costruttore della classe
      __fastcall    TfServerMain            ( TComponent* Owner );
  /// Metodo per impostare lo stato di ERROR del server.
  int __fastcall    SPVServerErrorStatus    ( int code, char * caption, char * text, bool balloon );
  /// Metodo per impostare lo stato di WARNING del server.
  int __fastcall    SPVServerWarningStatus  ( int code, char * caption, char * text, bool balloon );
  /// Metodo per impostare lo stato di IDLE del server.
  int __fastcall    SPVServerIdleStatus     ( char * caption, char * text, bool balloon );
  /// Metodo per impostare lo stato di OK del server.
  int __fastcall    SPVServerOkStatus       ( char * caption, char * text, bool balloon );
};

//==============================================================================
// Varibili globali
//------------------------------------------------------------------------------
extern PACKAGE TfServerMain *fServerMain;
#endif

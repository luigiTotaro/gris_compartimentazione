//==============================================================================
// Telefin Supervisor Applicative Module 1.0
//------------------------------------------------------------------------------
// Header Modulo Applicativo (SPVApplicative.h)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	1.21 (29.03.2004 -> 10.05.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVApplicative.cpp,XMLInterface.h,SPVComProt.h,
// 				SPVSystem.h,SYSKernel.h,SPVApplicLib.h
//------------------------------------------------------------------------------
// Version history:
// 0.00 [29.03.2004]:
// - Prima versione prototipo del modulo.
// 0.01 [31.03.2004]:
// - Aggiunta definizione struttura SPV_SERVER.
// - Aggiunta definizione struttura SPV_REGION.
// - Aggiunta definizione struttura SPV_ZONE.
// - Aggiunta definizione struttura SPV_NODE.
// - Aggiunta definizione struttura SPV_DEVICE.
// - Aggiunta definizione struttura SPV_CARD.
// - Aggiunta definizione struttura SPV_DEVICE_STATUS.
// 0.02 [05.04.2004]:
// - Aggiunta la funzione SPVNewProcedureType.
// - Aggiunta la funzione SPVNewProcedure.
// 0.03 [09.04.2004]:
// - Modificata la struttura SPV_PROCEDURE_TYPE con l'aggiunta di <class>.
// - Migliorata la funzione SPVNewProcedureType.
// 0.04 [15.04.2004]:
// - Modificata la struttura SPV_PROCEDURE con l'aggiunta di <thread>.
// - Aggiunta la funzione SPVProcedureFSM.
// 0.05 [16.04.2004]:
// - Aggiunta la definizione del tipo SPV_FSM_FUNCTION.
// - Aggiunta la definizione del tipo SPV_FSM_STATE.
// - Modificata la struttura SPV_PROCEDURE con l'aggiunta di <StateList>, di
//   <State> e di <StateCount>.
// - Aggiunta la funzione SPVNewFSMState.
// - Aggiunta la funzione SPVNewFSMStates.
// - Aggiunta la funzione SPVExtractFSMState.
// - Aggiunta la funzione SPVExtractFSMStates.
// - Aggiunta la funzione SPVSetFSMStateDefault.
// - Modificata la funzione SPVProcedureFSM.
// - Aggiunta la funzione SPVExecProcedure.
// - Modificata la funzione SPVNewProcedure.
// 0.06 [19.04.2004]:
// - Modificata la struttura SPV_FSM_STATE con l'aggiunta di <SeqParent>.
// - Modificata la struttura SPV_FSM_STATE con l'aggiunta di <SeqReturn>.
// - Modificata la struttura SPV_FSM_STATE con l'aggiunta di <SeqOrder>.
// - Modificata la funzione SPVSetFSMStateDefault con i campi sequenza.
// - Modificata la funzione SPVExtractFSMStates con gestione campi sequenza
// - Corretto il ciclo di ricerca elementi nella funzione SPVExtractFSMStates.
// - Aggiunta la funzione SPVGetFSMParentState.
// - Modificata la definizione di SPV_FSM_FUNCTION.
// - Aggiunta la definizione di SPV_FSM_FUNCTION_PTR.
// - Modificata la struttura SPV_FSM_STATE per il campo <Function>.
// - Modificata la struttura SPV_DEVICE_STATUS con l'aggiunta di <Offline>.
// - Modificata la struttura SPV_DEVICE_STATUS con l'aggiunta di <Unknown>.
// 0.07 [20.04.2004]:
// - Aggiunta la definizione del tipo SPV_DEVICE_CFG.
// - Modificata la funzione SPVNewFSMStates.
// - Modificata la funzione SPVExtractFSMStates.
// - Aggiunta la funzione SPVAddFSMFunction.
// - Aggiunta la funzione SPVGetFSMFunction.
// - Aggiunta la funzione SPVGetNextFSMState.
// 0.08 [06.05.2004]:
// - Corretta la funzione SPVGetNextFSMState nella gestione dei codici di stato.
// 0.09 [10.05.2004]:
// - Tolta la definizione del tipo SPV_COMPROT_FIELD.
// - Tolta la definizione del tipo SPV_COMPROT_FRAME.
// - Tolta la definizione del tipo SPV_COMPROT_COMMAND.
// 0.10 [11.05.2004]:
// - Tolta la definizione del tipo SPV_DEVICE_STATUS.
// - Tolta la definizione del tipo SPV_DEVICE_CFG.
// - Tolta la definizione del tipo SPV_SERVER.
// - Tolta la definizione del tipo SPV_REGION.
// - Tolta la definizione del tipo SPV_ZONE.
// - Tolta la definizione del tipo SPV_NODE.
// - Tolta la definizione del tipo SPV_DEVICE_TYPE.
// - Tolta la definizione del tipo SPV_DEVICE.
// - Tolta la definizione del tipo SPV_CARD.
// - Incluso il modulo SPVSystem.
// 0.11 [04.08.2004]:
// - Aggiunta la funzione SPVInitApplicative.
// 0.12 [11.08.2004]:
// - Aggiunto il codice di errore SPV_APP_INVALID_DEVICE.
// - Aggiunta la funzione SPVLoadProcedures.
// - Modificata la funzione SPVInitApplicative.
// 0.13 [31.08.2004]:
// - Aggiunto il codice di errore SPV_APP_INVALID_ITEM.
// - Modificata la funzione SPVLoadProcedures.
// 0.14 [01.09.2004]:
// - Modificata la funzione SPVExecProcedure.
// - Modificata la funzione SPVLoadProcedures.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_FUNCTION_LIST.
// - Aggiunta la funzione SPVLoadFSMFunctionList.
// - Spostata la definizione del tipo SPV_FSM_FUNCTION_PTR nella libreria
//   SPVApplicLib.
// 0.15 [07.09.2004]:
// - Modificata la funzione SPVProcedureFSM.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_DATA.
// - Aggiunta la funzione SPVNewFSMData.
// - Aggiunta la funzione SPVDeleteFSMData.
// - Aggiunta la funzione SPVDeleteAllFSMData.
// 0.16 [09.09.2004]:
// - Convertiti tutte le definizioni dei codici SPV_APP_* a SPV_APPLIC_*.
// - Aggiunta la definizione del tipo SPV_DEV_STREAM.
// - Aggiunta la definizione del tipo SPV_DEV_STREAM_FIELD.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_STREAM.
// - Aggiunta la funzione SPVNewDevField.
// - Aggiunta la funzione SPVDeleteDevField.
// - Aggiunta la funzione SPVLoadDevFieldList.
// - Aggiunta la funzione SPVDeleteDevFieldList.
// - Aggiunta la funzione SPVNewDevStream.
// - Aggiunta la funzione SPVDeleteDevStream.
// - Aggiunta la funzione SPVLoadDevStreams.
// - Aggiunta la funzione SPVDeleteDevStreamList.
// - Aggiunto il codice di errore SPV_APPLIC_STREAM_DEF_NOT_FOUND.
// - Aggiunto il codice di errore SPV_APPLIC_FIELD_DEF_NOT_FOUND.
// - Aggiunta la definizione del tipo SPV_APPLIC_INVALID_DEVICE_TYPE.
// 0.17 [22.09.2004]:
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_BUFFER.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_FIELD_LIST.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_ELEMENT.
// - Aggiunta la funzione SPVGetDevStreamSeverity.
// - Definiti i tipi di valore dei campi degli stream.
// - Modificata la funzione SPVLoadDevFieldList.
// - Aggiunta la funzione SPVGetFieldValueType.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_OFFSET.
// - Aggiunta la funzione SPLGetDevFieldSeverity.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_VALUE.
// - Aggiunto il codice di errore SPV_APPLIC_UNKNOWN_SEVERITY.
// - Modificata la definizione del tipo SPV_DEV_STREAM.
// - Modificata la definizione del tipo SPV_DEV_STREAM_FIELD.
// - Aggiunta la definizione del tipo SPV_DEV_STRAM_BUFFER.
// - Aggiunta la funzione SPVImportDevFrameBuffer.
// 0.18 [23.09.2004]:
// - Modificata la funzione SPVImportDevFrameBuffer.
// - Modificata la funzione SPVLoadDevStreamList.
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_SYSTEM.
// - Aggiunta la funzione SPVLoadSystemStreams.
// - Modificata la funzione SPVNewDevStream.
// - Modificata la funzione SPVNewDevField.
// - Modificata la funzione SPVDeleteDevField.
// 0.19 [04.10.2004]:
// - Aggiunta la funzione SPVLoadSystemProcedures.
// 0.20 [05.10.2004]:
// - Aggiunto il codice di errore SPV_APPLIC_INVALID_STREAM_LIST.
// - Aggiunta la funzione SPVSearchStream.
// - Modificata la funzione SPVLoadDevFieldList.
// - Modificata la funzione SPVGetDevFieldSeverity.
// 0.21 [06.10.2004]:
// - Modificata la funzione SPVGetDevFieldSeverity.
// 0.22 [07.05.2005]:
// - Modificata la funzione SPVExecProcedure.
// 0.23 [18.05.2005]:
// - Modificata la funzione SPVInitApplicative.
// 0.24 [23.05.2005]:
// - Aggiunta la funzione SPVSearchPayload.
// - Aggiunta la funzione SPVCountPayload.
// - Aggiunta la funzione SPVGetPayloadLen.
// - Aggiunta la funzione SPVExtractStream.
// 0.25 [24.05.2005]:
// - Modificata la funzione SPVSearchPayload.
// - Modificata la funzione SPVCountPayload.
// - Modificata la funzione SPVGetPayloadLen.
// - Modificata la funzione SPVExtractStream.
// - Aggiunta la funzione SPVRAMSaveStream.
// - Aggiunta la funzione SPVDBSaveStream.
// 0.26 [25.05.2005]:
// - Modificata la funzione SPVDBSaveStream.
// 0.27 [26.05.2005]:
// - Modificata la funzione SPVDBSaveStream.
// 0.28 [27.05.2005]:
// - Modificata la funzione SPVExtractStream.
// 0.29 [31.05.2005]:
// - Aggiunta la funzione SPVDBGetStream.
// - Modificata la definizione del tipo SPV_DEV_STREAM_BUFFER.
// 0.30 [01.06.2005]:
// - Modificata la funzione SPVDBGetStream.
// 0.31 [15.06.2005]:
// - Modificata la funzione SPVProcedureFSM.
// - Modificata la funzione SPVDBSaveStream.
// 0.32 [16.06.2005]:
// - Modificata la funzione SPVDBGetStream.
// 0.33 [20.06.2005]:
// - Eliminata la funzione SPVNewProcedureClass.
// - Aggiunta la funzione SPVNullProcedureClass.
// - Aggiunta la funzione SPVGetFirstFreeClassPos.
// - Aggiunta la funzione SPVGetProcedureClassPos.
// - Aggiunta la funzione SPVLoadProcedureClass.
// - Aggiunta la funzione SPVAddNewProcedureClass.
// 0.34 [22.06.2005]:
// - Modificata la funzione SPVNewProcedure.
// - Modificata la funzione SPVGetProcedureClassPos.
// - Modificata la funzione SPVAddNewProcedureClass.
// - Modificata la definizione del tipo SPV_PROCEDURE.
// 0.35 [23.06.2005]:
// - Aggiunta la funzione SPVEnterClassData.
// - Aggiunta la funzione SPVLeaveClassData.
// - Aggiunta la funzione SPVGetClassList.
// - Aggiunta la funzione SPVGetClassCount.
// - Aggiunta la funzione SPVEnterProcedureData.
// - Aggiunta la funzione SPVLeaveProcedureData.
// - Aggiunta la funzione SPVGetProcedureList.
// - Aggiunta la funzione SPVGetProcedureCount.
// 0.36 [27.06.2005]:
// - Modificata la funzione SPVProcedureFSM.
// - Modificata la funzione SPVNewProcedure.
// - Modificata la funzione SPVExecProcedure.
// 0.37 [28.06.2005]:
// - Modificata la funzione SPVNewDevStream.
// 0.38 [14.07.2005]:
// - Modificata la funzione SPVProcedureFSM.
// 0.39 [20.07.2005]:
// - Modificata la funzione SPVImportDevFrameBuffer.
// 0.40 [21.07.2005]:
// - Modificata la funzione SPVProcedureFSM.
// 0.41 [26.07.2005]:
// - Modificata la funzione SPVDBSaveStream.
// 0.42 [29.07.2005]:
// - Modificata la funzione SPVNewDevField.
// - Modificata la funzione SPVDeleteDevField.
// - Modificata la funzione SPVLoadDevFieldList.
// - Modificata la funzione SPVGetDevField.
// - Aggiunta la funzione SPVAddDescription.
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la definizione del tipo SPV_DEV_STREAM_FIELD.
// 0.43 [01.08.2005]:
// - Modificata la funzione SPVGetDevFieldSeverity.
// 0.44 [03.08.2005]:
// - Modificata la funzione SPVNewProcedure.
// - Modificata la funzione SPVExecProcedure.
// 0.45 [04.08.2005]:
// - Modificata la funzione SPVImportDevFrameBuffer.
// 0.46 [29.08.2005]:
// - Rinominata la funzione SPVInitApplicative in SPVapplicativeInit.
// - Aggiunta la funzione SPVApplicativeClear.
// 0.47 [30.08.2005]:
// - Modificata la funzione SPVExtractFSMState.
// - Aggiunta la funzione SPVFreeFSMState.
// - Modificata la funzione SPVExtractFSMStates.
// - Aggiunta la funzione SPVFreeFSMStateList.
// - Modificata la funzione SPVNewProcedure.
// - Aggiunta la funzione SPVFreeSystemProcedures.
// 0.48 [31.08.2005]:
// - Modificata la funzione SPVDeleteDevStream.
// - Aggiunta la funzione SPVFreeDevStreamList.
// - Modificata la funzione SPVLoadSystemStreams.
// - Aggiunta la funzione SPVFreeSystemStreams.
// 0.49 [19.09.2005]:
// - Modificata la definizione del tipo SPV_DEV_STREAM_FIELD.
// - Modificata la funzione SPVNewDevField.
// 0.50 [20.09.2005]:
// - Modificata la funzione SPVNewDevStream.
// - Modificata la funzione SPVImportDevFrameBuffer.
// - Modificata la funzione SPVAddDescription.
// - Modificata la funzione SPVExtractStream.
// - Modificata la definizione del tipo SPV_DEV_STREAM.
// 0.51 [21.09.2005]:
// - Modificata la funzione SPVApplicativeInit.
// - Modificata la funzione SPVNewDevField.
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la funzione SPVGetDevStreamSeverity.
// - Aggiunta la funzione SPVGetDevSeverity.
// - Modificata la definizione del tipo SPV_DEV_STREAM_FIELD.
// 0.52 [30.09.2005]:
// - Modificata la funzione SPVApplicativeInit.
// 0.53 [03.10.2005]:
// - Modificata la funzione SPVApplicativeInit.
// - Modificata la funzione SPVBuildDevEventMsg.
// 0.54 [04.10.2005]:
// - Modificata la funzione SPVApplicativeInit.
// - Aggiunta la funzione SPVReportDevEvent.
// 0.55 [05.10.2005]:
// - Modificata la funzione SPVReportDevEvent.
// 0.56 [06.10.2005]:
// - Modificata la funzione SPVApplicativeInit.
// 0.57 [07.10.2005]:
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la funzione SPVGetDevStreamSeverity.
// 0.58 [11.10.2005]:
// - Modificata la funzione SPVApplicativeInit.
// 0.59 [12.10.2005]:
// - Modificata la funzione SPVDBSaveStream.
// - Modificata la funzione SPVDBGetStream.
// 0.60 [13.10.2005]:
// - Modificata la funzione SPVDBSaveStream.
// - Modificata la funzione SPVDBGetStream.
// - Modificata la funzione SPVReportDBEvent.
// 0.61 [17.10.2005]:
// - Aggiunta la definizione del tipo SPV_DEV_EVENT.
// - Modificata la funzione SPVDBSaveEvent.
// 0.62 [18.10.2005]:
// - Modificata la definizione del tipo SPV_DEV_EVENT.
// - Aggiunta la funzione SPVDecodeSysEvent.
// - Modificata la funzione SPVDBSaveEvent.
// - Modificata la funzione SPVDBSaveStream.
// 0.63 [19.10.2005]:
// - Modificata la funzione SPVDBSaveEvent.
// - Modificata la funzione SPVDecodeSysEvent.
// - Modificata la funzione SPVDecodeDevEvent.
// 0.64 [20.10.2005]:
// - Modificata la funzione SPVDBSaveEvent.
// - Modificata la funzione SPVDecodeSysEvent.
// - Modificata la funzione SPVDecodeDevEvent.
// - Aggiunta la funzione SPVGetDevCardDesc.
// - Aggiunta la funzione SPVGetSysEventDesc.
// - Aggiunta la funzione SPVGetDevEventDesc.
// - Aggiunta la funzione SPVGetDevModuleDesc.
// - Modificata la definizione del tipo SPV_DEV_EVENT.
// 0.65 [24.10.2005]:
// - Modificata la funzione SPVDBSaveStream.
// 0.66 [27.10.2005]:
// - Eliminata la funzione SPVNewProcedureType.
// 0.67 [03.11.2005]:
// - Modificata la definizione del tipo SPV_DEV_STREAM_FIELD.
// - Modificata la funzione SPVNewDevField.
// - Modificata la funzione SPVDeleteDevField.
// 0.68 [04.11.2005]:
// - Modificata la funzione SPVLoadDevFieldList.
// 0.69 [07.11.2005]:
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la funzione SPVImportDevFrameBuffer.
// 0.70 [08.11.2005]:
// - Modificata la funzione SPVReportDevEvent.
// 0.71 [11.11.2005]:
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la funzione SPVGetDevStreamSeverity.
// 0.72 [14.11.2005]:
// - Aggiunta la funzione SPVGetFieldEventID.
// - Modificata la funzione SPVGetDevFieldSeverity.
// 0.73 [16.11.2005]:
// - Modificata la funzione SPVGetFieldEventID.
// 0.74 [17.11.2005]:
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la funzione SPVGetFieldEventID.
// - Modificata la funzione SPVReportDevEvent.
// - Modificata la definizione del tipo SPV_DEV_STREAM_FIELD.
// 0.75 [18.11.2005]:
// - Modificata la funzione SPVNewDevStream.
// - Aggiunta la funzione SPVGetStreamEventID.
// 0.76 [22.11.2005]:
// - Modificata la funzione SPVDBGetStream.
// - Modificata la funzione SPVGetDevStreamSeverity.
// - Modificata la funzione SPVDBSaveStream.
// - Aggiunta la funzione SPVAddText.
// 0.77 [23.11.2005]:
// - Aggiunta la funzione SPVReportApplicEvent.
// - Modificata la funzione SPVNewDevStream.
// - Modificata la funzione SPVLoadDevStreamList.
// - Modificata la funzione SPVGetDevStreamSeverity.
// - Modificata la funzione SPVGetDevSeverity.
// - Modificata la funzione SPVDBSaveStream.
// - Aggiunta la funzione SPVGetDeviceEventID.
// - Modificata la definizione del tipo SPV_DEV_STREAM.
// 0.78 [24.11.2005]:
// - Modificata la funzione SPVProcedureFSM.
// - Modificata la funzione SPVReportApplicEvent.
// 0.79 [28.11.2005]:
// - Modificata la funzione SPVGetNextFSMState.
// - Modificata la funzione SPVExtractFSMState.
// - Modificata la funzione SPVExtractFSMStates.
// - Modificata la funzione SPVGetProcedureClassPos.
// - Modificata la funzione SPVDefaultDevFrameBuffer.
// - Modificata la funzione SPVImportDevFrameBuffer.
// - Modificata la funzione SPVExtractStream.
// - Modificata la funzione SPVRAMSaveStream.
// - Modificata la funzione SPVDBGetStream.
// - Modificata la funzione SPVDBSaveEvent.
// - Modificata la funzione SPVSetDeviceStatus.
// 0.80 [19.12.2005]:
// - Modificata la funzione SPVGetFieldValueType.
// - Modificata la funzione SPVGetDevFieldSeverity.
// - Modificata la funzione SPVDBGetStream.
// - Aggiunta la funzione SPVDBSaveDevice.
// - Aggiunta la funzione SPVDBGetDevice.
// - Modificata la funzione SPVDBSaveNode.
// - Aggiunta la funzione SPVDBGetNode.
// - Modificata la funzione SPVDBSaveZone.
// - Aggiunta la funzione SPVDBGetZone.
// - Modificata la funzione SPVDBSaveRegion.
// - Aggiunta la funzione SPVDBGetRegion.
// - Modificata la funzione SPVDBSaveServer.
// - Aggiunta la funzione SPVDBGetServer.
// - Modificata la funzione SPVReportDevEvent.
// 0.81 [20.01.2006]:
// - Modificata SPVLoadProcedureClass().
// - Aggiunta SPVClearProcedureClasses().
// - Modificata SPVFreeSystemProcedures().
// 0.82 [24.01.2006]:
// - Modificata SPVGetDevSeverity().
// 0.83 [10.02.2006]:
// - Aggiunta SPVReportValueEvent().
// - Aggiunta SPVReportFieldEvent().
// - Aggiunta SPVReportStreamEvent().
// - Aggiunta SPVReportDeviceEvent().
// 0.84 [13.02.2006]:
// - Aggiunta SPVBuildApplicEventMsg().
// - Modificata SPVReportPortEvent().
// 0.85 [07.03.2006]:
// - Modificata SPVExecProcedure().
// 0.86 [09.03.2006]:
// - Modificata SPVDBGetRegion().
// - Modificata SPVDBSaveServer().
// 0.87 [13.03.2006]:
// - Modificata SPVDBSaveNode().
// - Modificata SPVDBSaveZone().
// - Modificata SPVDBSaveRegion().
// - Modificata la funzione SPVDBSaveDevice.
// 0.88 [14.03.2006]:
// - Modificata SPVValidDevField().
// - Modificata SPVDeleteDevFieldList().
// - Modificata SPVNewDevStream().
// - Modificata SPVDeleteDevStream().
// - Aggiunta SPVValidDevStream().
// - Modificata SPVLoadDevStreamList().
// - Modificata SPVFreeDevStreamList().
// - Modificata SPVGetDevField().
// - Modificata SPVDeleteDevStreamList().
// - Modificata SPVDBSaveStream().
// 0.89 [16.03.2006]:
// - Modificata SPVImportDevFrameBuffer().
// 0.90 [21.03.2006]:
// - Modificata SPVDBRemoveAllDevices().
// - Aggiunta SPVDBRemoveAllNodes().
// - Aggiunta SPVDBRemoveAllZones().
// - Aggiunta SPVDBRemoveAllRegions().
// 0.91 [27.03.2006]:
// - Modificata SPVDBGetStream().
// 0.92 [28.03.2006]:
// - Modificata SPVNewDevField().
// 0.93 [29.03.2006]:
// - Modificata SPVSearchStream().
// - Aggiunta SPVSearchStreamField().
// - Modificata SPVDBSaveField().
// 0.94 [30.03.2006]:
// - Aggiunta SPVDBSaveDeviceStatus().
// 0.95 [22.05.2006]:
// - Modificata SPVReportDBEvent().
// 0.96 [26.05.2006]:
// - Modificata SPVGetDevStreamSeverity().
// - Modificata SPVAddDescription().
// 0.97 [22.06.2006]:
// - Modificata la funzione SPVExtractStream.
// 0.98 [23.06.2006]:
// - Modificata SPVExtractStream().
// 0.99 [30.06.2006]:
// - Modificata SPVImportDevFieldBuffer().
// 1.00 [11.07.2006]:
// - Modificata SPVProcedureFSM().
// 1.01 [20.07.2006]:
// - Modificata la funzione SPVDBSaveDevice.
// - Modificata SPVReportSystemEvent().
// 1.02 [21.07.2006]:
// - Aggiunta la funzione SPVCheckValueDeps.
// 1.03 [24.07.2006]:
// - Modificata la funzione SPVCheckValueDeps.
// - Modificata la funzione SPVGetDevFieldSeverity.
// 1.04 [26.07.2006]:
// - Modificata SPVDeleteDevField().
// - Modificata SPVLoadDevFieldList().
// - Aggiunta _Floatize().
// - Aggiunta _ApplyFactor().
// - Aggiunta _ApplyFormat().
// - Modificata SPVPrintableFieldConv().
// 1.05 [22.08.2006]:
// - Modificata la definizione del tipo SPV_PROCEDURE (aggiunto ExeCount).
// - Aggiunto tipo SPV_PROCEDURE_COUNTER.
// - Aggiunta SPVDBLoadProcExeCounts().
// - Aggiunta SPVLoadProcedureCounters().
// - Modificata SPVReportApplicEvent().
// - Aggiunta SPVSearchProcedure().
// 1.06 [23.08.2006]:
// - Modificata SPVDBLoadProcExeCounts().
// - Modificata SPVLoadProcedureCounters().
// - Modificata SPVNewProcedure() (aggiunta inizializzazione ExeCount).
// - Modificata SPVLoadProcedures().
// - Aggiunta SPVDBSaveProcedures().
// 1.07 [24.08.2006]:
// - Modificato tipo SPV_PROCEDURE_COUNTER (aggiunto flag Changed).
// - Modificata SPVDBLoadProcExeCounts() (aggiunto azzeramento flag Changed).
// - Modificato tipo SPV_PROCEDURE (aggiunto flag ECChanged).
// - Modificata SPVNewProcedure() (aggiunto azzeramento flag ECChanged).
// - Modificata SPVLoadProcedureCounters() (aggiunta gestione flag ECChanged).
// - Aggiunta SPVIncProcedureExeCount().
// - Aggiunta SPVDecProcedureExeCount().
// - Aggiunta SPVSetProcedureContinueMode().
// - Aggiunta SPVClearProcedureExeCount().
// - Modificata SPVDBSaveProcedures().
// - Modificata SPVLoadSystemProcedures() (aggiunto salvataggio in DB).
// - Corretta SPVSearchProcedure() (controllo DevID).
// 1.08 [25.08.2006]:
// - Modificata SPVDBLoadProcExeCounts().
// - Aggiunta SPVDBUpdateProcedure().
// - Aggiunta SPVDBSaveProcExeCounts().
// - Aggiunta SPVSaveProcedureCounters().
// - Modificata SPVProcedureFSM() (aggiunto aggiornamento tabella 'procedures').
// - Modificata SPVDecProcedureExeCount().
// [02.10.2006]:
// - Modificata SPVBuildDevEventMsg().
// [03.10.2006]:
// - Aggiunta SPVProcedureCheckFunction().
// [04.10.2006]:
// - Corretta SPVBuildDevEventMsg().
// - Aggiunta SPVDBSaveStreamFields().
// [05.10.2006]:
// - Modificata SPVDBSaveStreamFields().
// [10.10.2006]:
// - Modificata SPVDBSaveStreamFields() (aggiunta cancellazione iniziale).
// [11.05.2007]:
// - Modificata SPVFreeSystemProcedures().
// - Modificata SPVProcedureCheckFunction().
// - Modificata SPVLoadProcedures().
// - Modificata SPVDBSaveProcedures().
// - Modificata SPVLoadProcedureCounters().
// - Modificata SPVSaveProcedureCounters().
// - Modificata SPVLaunchProcedures().
// [16.05.2007]:
// - Aggiunta SPVIsNegativeNumber
// - Aggiunta SPVGetAbsValue
// - Aggiunta SPVGetExpandedNumber
// - Aggiunta SPVCheckRange
// - Modificata SPVGetDevFieldSeverity
// [31.07.2007]:
// - Corretta la funzione SPVDBUpdateProcedure() - Eliminato memory leak.
// [24.08.2007]:
// - Corretta la funzione SPVGetDevStreamSeverity() - Eliminato memory leak
// 1.09 [21.09.2007]:
// - Assegnato un nome all'attributo Name di SPVProcedureCheckFunction  e di SPVProcedureFSM
// 1.10 [04.10.2007]:
// - Aggiunto salvataggio StepID in SPVProcedureFSM().
// 1.11 [05.10.2007]:
// - Aggiunta chiamata alla funzione SPVDBMCleanAllstreams in SPVLoadSystemStreams.
// - Modificata funzione SPVDBSaveDevice.
// 1.12 [18.10.2007]:
// - Modificata funzione _ApplyFormat().
// 1.13 [24.10.2007]:
// - Modificata la funzione SPVProcedureCheckClear() per eliminare i memory leak nella procedura.
// 1.14 [10.12.2007]:
// - Modificata la funzione SPVDBSaveProcedures().
// - Modificata la funzione SPVApplicativeClear().
// - Modificata la funzione SPVDBUpdateProcedure().
// - Aggiunta la funzione SPVDeleteUnusedRows().
// - Aggiunta la funzione SPVDBSaveDeviceSystems().
// - Aggiunta la funzione SPVDBSaveDeviceVendors().
// - Aggiunta la funzione SPVDBSaveDeviceTypes().
// 1.15 [13.12.2007]:
// - Modificata la funzione SPVDBSaveDevice
// 1.16 [21.12.2007]:
// - Aggiunto controllo cambio versione definizione in SPVDBSaveDevice().
// 1.17 [07.01.2008]:
// - Corretta la funzione SPVDBSaveDevice().
// 1.18 [11.01.2008]:
// - Corretto controllo di versione cambiata nella funzione SPVDBSaveDevice().
// - Corretta la funzione SPVProcedureFSM().
// 1.19 [14.01.2008]:
// - Modificata la funzione SPVProcedureCheckFunction().
// 1.20 [xx.xx.2008]:
// - Modificata la funzione SPVDBLoadProcExeCounts().
// - Modificata la funzione SPVDBDeleteStream().
// - Eliminata la funzione SPVDBSaveStream (SPV_DEVICE * device, int streamID, SPV_DEV_STREAM_BUFFER buffer).
// - Eliminata la funzione SPVDBDeleteFields.
// - Eliminata la funzione SPVDBSaveField().
// - Eliminata la funzione SPVDBSaveStreamFields().
// - Modificata la funzione SPVDBGetStream().
// - Commentata la funzione SPVDBSaveEvent().
// - Modificata la funzione SPVDBRemoveAllDevices().
// - Modificata la funzione SPVDBSaveDevice().
// - Modificata la funzione SPVDBRemoveAllNodes().
// - Modificata la funzione SPVDBSaveNode().
// - Modificata la funzione SPVDBRemoveAllZones().
// - Modificata la funzione SPVDBSaveZone().
// - Modificata la funzione SPVDBRemoveAllRegions().
// - Modificata la funzione SPVDBSaveRegion().
// - Modificata la funzione SPVDBSaveServer().
// - Introdotta nuova gestione delle eccezioni nelle seguenti funzioni:
//		- SPVApplicativeInit
//		- SPVApplicativeClear
//		- SPVDBSaveDeviceSystems
//		- SPVDBSaveDeviceVendors
//		- SPVDBSaveDeviceTypes
//		- SPVGetDevSeverity
//		- SPVDBSaveDeviceStatus
//		- SPVEnterClassData
//		- SPVLeaveClassData
//		- SPVGetFirstFreeClassPos
//		- SPVGetProcedureClassPos
//		- SPVAddNewProcedureClass
//		- SPVClearProcedureClasses
//		- SPVEnterProcedureData
//		- SPVLeaveProcedureData
//		- SPVProcedureCheckFunction
//		- SPVLoadProcedures
//		- SPVClearProcedureThreads
//		- SPVFreeSystemProcedures
//		- SPVGetProcedureThread
//		- SPVProcedureIsActive
//		- SPVProcedureHasState
//		- SPVDBSaveProcedures
//		- SPVDBUpdateProcedure
//		- SPVLoadProcedureCounters
//		- SPVSaveProcedureCounters
//		- SPVLaunchProcedures
//==============================================================================
#ifndef SPVApplicativeH
#define SPVApplicativeH

//==============================================================================
// Inclusioni
//------------------------------------------------------------------------------
#include "XMLInterface.h"
#include "SPVComProt.h"
#include "SPVSystem.h"
#include "SPVApplicLib.h"

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SPV_APPLIC_MAX_PROC_CLASS         	    	50
#define SPV_APPLIC_MAX_PROC_TYPE          	    	200
#define SPV_APPLIC_MAX_PROC               	    	2000
#define SPV_APPLIC_MAX_FSM_FUNC           	    	50
#define SPV_APPLIC_MAX_PROC_THREAD        	    	2000
#define SPV_APPLIC_MAX_FIELD_CAPACITY     	    	128
#define SPV_APPLIC_FIELD_VALUE_TYPE_EXACT 	    	1
#define SPV_APPLIC_FIELD_VALUE_TYPE_RANGE 	    	2
#define SPV_APPLIC_FIELD_VALUE_TYPE_MASK  	    	3
#define SPV_APPLIC_FIELD_VALUE_TYPE_OTHER 	    	4
#define SPV_APPLIC_FIELD_VALUE_TYPE_NXOR  	    	5
#define SPV_APPLIC_FIELD_VALUE_TYPE_EXTRACT	    	6
#define SPV_APPLIC_FIELD_VALUE_TYPE_2EXTRACT		7
#define SPV_APPLIC_INITIAL_DELAY_AUTO_CHECH			30000
#define SPV_APPLIC_DELAY_AUTO_CHECK 				5000

// --- VCCs ---
#define SPV_APPLIC_PROCEDURE_VALIDITY_CHECK_CODE		49333743
#define SPV_APPLIC_STREAM_VALIDITY_CHECK_CODE			2040254318
#define SPV_APPLIC_FIELD_VALIDITY_CHECK_CODE			1040483930
#define SPV_APPLIC_DEVICE_EVENT_VALIDITY_CHECK_CODE		334834365

// --- Livelli di severit� ---
#define SPV_APPLIC_SEVLEVEL_OK            0
#define SPV_APPLIC_SEVLEVEL_WARNING       1
#define SPV_APPLIC_SEVLEVEL_ERROR         2
#define SPV_APPLIC_SEVLEVEL_CRITICAL      3
#define SPV_APPLIC_SEVLEVEL_ALARM         4
#define SPV_APPLIC_SEVLEVEL_NOT_AVAILABLE 9
#define SPV_APPLIC_SEVLEVEL_UNKNOWN       255
// --- Descrizioni di default (italiano) livelli di severita' ---
#define SPV_APPLIC_SEVLEDEL_DES_DEFAULT_OK				"In servizio"
#define SPV_APPLIC_SEVLEVEL_DES_DEFAULT_WARNING			"Anomalia lieve"
#define SPV_APPLIC_SEVLEVEL_DES_DEFAULT_ERROR			"Anomalia grave"
#define SPV_APPLIC_SEVLEVEL_DES_DEFAULT_CRITICAL		"Critico"
#define SPV_APPLIC_SEVLEVEL_DES_DEFAULT_ALARM			"Allarme"
#define SPV_APPLIC_SEVLEVEL_DES_DEFAULT_NOT_AVAILABLE 	"Diagnostica non disponibile"
#define SPV_APPLIC_SEVLEVEL_DES_DEFAULT_UNKNOWN			"Sconosciuto"
// --- Etichette segnaposto per descrizioni livelli di severita' multilingua ---
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_OK			"SPVServerD_DeviceStatusMessageOK"
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_WARNING		"SPVServerD_DeviceStatusMessageWarning"
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_ERROR			"SPVServerD_DeviceStatusMessageError"
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_CRITICAL		"SPVServerD_DeviceStatusMessageCritical"
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_ALARM			"SPVServerD_DeviceStatusMessageAlarm"
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_NOT_AVAILABLE	"SPVServerD_DeviceStatusMessageNotAvailable"
#define SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_UNKNOWN		"SPVServerD_DeviceStatusMessageUnknown"

#define SPV_APPLIC_DEVICE_EVENT_TYPE_DEV  1
#define SPV_APPLIC_DEVICE_EVENT_TYPE_SYS  2

// --- File di Log di Windows ---
#define SPV_APPLIC_EVENT_LOG_FILE               "GrisSuite" //Era "TelefinSpvSystem"
// --- Categorie di eventi ---
#define SPV_APPLIC_CATEGORY_DEV_VALUE_EVENT     1
#define SPV_APPLIC_CATEGORY_DEV_FIELD_EVENT     2
#define SPV_APPLIC_CATEGORY_DEV_STREAM_EVENT    3
#define SPV_APPLIC_CATEGORY_DEV_EVENT           4
#define SPV_APPLIC_CATEGORY_SYSTEM_EVENT        5
#define SPV_APPLIC_CATEGORY_PORT_EVENT          6
#define SPV_APPLIC_CATEGORY_DB_EVENT            7
#define SPV_APPLIC_CATEGORY_APPLIC_EVENT        8
// --- Codici eventi DB ---
#define SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS     4000
#define SPV_APPLIC_EVENT_DB_CONNECT_FAILURE     4001
#define SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS  4002
#define SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE  4003
#define SPV_APPLIC_EVENT_DB_QUERY_SUCCESS       4004
#define SPV_APPLIC_EVENT_DB_QUERY_FAILURE       4005
// --- Descrizione eventi DB ---
#define SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS_DES     "Connessione al database riuscita"
#define SPV_APPLIC_EVENT_DB_CONNECT_FAILURE_DES     "Connessione al database fallita"
#define SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS_DES  "Disconnessione dal database riuscita"
#define SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE_DES  "Disconnessione dal database fallita"
#define SPV_APPLIC_EVENT_DB_QUERY_SUCCESS_DES       "Query sul database riuscita"
#define SPV_APPLIC_EVENT_DB_QUERY_FAILURE_DES       "Query sul database fallita"
// --- Codici eventi FIELD ---
#define SPV_APPLIC_EVENT_DEV_FIELD_OK             1000
#define SPV_APPLIC_EVENT_DEV_FIELD_WARNING        1001
#define SPV_APPLIC_EVENT_DEV_FIELD_ERROR          1002
#define SPV_APPLIC_EVENT_DEV_FIELD_CRITICAL       1003
#define SPV_APPLIC_EVENT_DEV_FIELD_ALARM          1004
#define SPV_APPLIC_EVENT_DEV_FIELD_UNKNOWN        1005
// --- Descrizione eventi FIELD ---
#define SPV_APPLIC_EVENT_DEV_FIELD_OK_DES         "Valore/i corretti"
#define SPV_APPLIC_EVENT_DEV_FIELD_WARNING_DES    "Valore/i in attenzione"
#define SPV_APPLIC_EVENT_DEV_FIELD_ERROR_DES      "Valore/i in errore"
#define SPV_APPLIC_EVENT_DEV_FIELD_CRITICAL_DES   "Valore/i critico/i"
#define SPV_APPLIC_EVENT_DEV_FIELD_ALARM_DES      "Valore/i in allarme"
#define SPV_APPLIC_EVENT_DEV_FIELD_UNKNOWN_DES    "Valore/i sconosciuto/i"
// --- Codici eventi STREAM ---
#define SPV_APPLIC_EVENT_DEV_STREAM_OK            2000
#define SPV_APPLIC_EVENT_DEV_STREAM_WARNING       2001
#define SPV_APPLIC_EVENT_DEV_STREAM_ERROR         2002
#define SPV_APPLIC_EVENT_DEV_STREAM_CRITICAL      2003
#define SPV_APPLIC_EVENT_DEV_STREAM_ALARM         2004
#define SPV_APPLIC_EVENT_DEV_STREAM_UNKNOWN       2005
// --- Descrizione eventi STREAM ---
#define SPV_APPLIC_EVENT_DEV_STREAM_OK_DES        "Parametro/i corretti"
#define SPV_APPLIC_EVENT_DEV_STREAM_WARNING_DES   "Parametro/i in attenzione"
#define SPV_APPLIC_EVENT_DEV_STREAM_ERROR_DES     "Parametro/i in errore"
#define SPV_APPLIC_EVENT_DEV_STREAM_CRITICAL_DES  "Parametro/i critico/i"
#define SPV_APPLIC_EVENT_DEV_STREAM_ALARM_DES     "Parametro/i in allarme"
#define SPV_APPLIC_EVENT_DEV_STREAM_UNKNOWN_DES   "Parametro/i sconosciuto/i"
// --- Codici eventi DEVICE ---
#define SPV_APPLIC_EVENT_DEV_OK                   3000
#define SPV_APPLIC_EVENT_DEV_WARNING              3001
#define SPV_APPLIC_EVENT_DEV_ERROR                3002
#define SPV_APPLIC_EVENT_DEV_CRITICAL             3003
#define SPV_APPLIC_EVENT_DEV_ALARM                3004
#define SPV_APPLIC_EVENT_DEV_UNKNOWN              3005
#define SPV_APPLIC_EVENT_DEV_ONLINE               3100
#define SPV_APPLIC_EVENT_DEV_OFFLINE              3101
#define SPV_APPLIC_EVENT_DEV_COMM_ERROR				3111
#define SPV_APPLIC_EVENT_DEV_DB_ERROR				3112
// --- Descrizione eventi DEVICE ---
#define SPV_APPLIC_EVENT_DEV_OK_DES               "Periferica in servizio"
#define SPV_APPLIC_EVENT_DEV_WARNING_DES          "Periferica in stato di attenzione"
#define SPV_APPLIC_EVENT_DEV_ERROR_DES            "Periferica in stato di errore"
#define SPV_APPLIC_EVENT_DEV_CRITICAL_DES         "Periferica in stato critico"
#define SPV_APPLIC_EVENT_DEV_ALARM_DES            "Periferica in stato di allarme"
#define SPV_APPLIC_EVENT_DEV_UNKNOWN_DES          "Evento sconosciuto"
#define SPV_APPLIC_EVENT_DEV_ONLINE_DES           "Periferica raggiungibile"
#define SPV_APPLIC_EVENT_DEV_OFFLINE_DES          "Periferica non raggiungibile"
#define SPV_APPLIC_EVENT_DEV_COMM_ERROR_DES			"Errore di comunicazione con la periferica"
#define SPV_APPLIC_EVENT_DEV_DB_ERROR_DES			"Errore di scrittura dati periferica nel DB"

#define SPV_APPLIC_EVENT_DATETIME_OFFSET_STANDARD 694220400

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_APPLIC_NO_ERROR               0
#define SPV_APPLIC_FUNCTION_SUCCESS       0
#define SPV_APPLIC_ALLOCATION_FAILURE     2000
#define SPV_APPLIC_INVALID_DEFINITION     2001
#define SPV_APPLIC_INVALID_DESTINATION    2002
#define SPV_APPLIC_INVALID_COUNT          2003
#define SPV_APPLIC_INVALID_FSM_STATE      2004
#define SPV_APPLIC_INVALID_PROCEDURE      2005
#define SPV_APPLIC_INVALID_THREAD         2006
#define SPV_APPLIC_INVALID_FUNCTION       2007
#define SPV_APPLIC_INVALID_CODE           2008
#define SPV_APPLIC_INVALID_LIST           2009
#define SPV_APPLIC_INVALID_DEVICE         2010
#define SPV_APPLIC_INVALID_ITEM           2011
#define SPV_APPLIC_INVALID_FUNCTION_LIST  2012
#define SPV_APPLIC_INVALID_DATA           2013
#define SPV_APPLIC_INVALID_STREAM         2014
#define SPV_APPLIC_INVALID_FIELD          2015
#define SPV_APPLIC_STREAM_DEF_NOT_FOUND   2016
#define SPV_APPLIC_FIELD_DEF_NOT_FOUND    2017
#define SPV_APPLIC_INVALID_DEVICE_TYPE    2018
#define SPV_APPLIC_INVALID_BUFFER         2019
#define SPV_APPLIC_INVALID_FIELD_LIST     2020
#define SPV_APPLIC_INVALID_ELEMENT        2021
#define SPV_APPLIC_INVALID_OFFSET         2022
#define SPV_APPLIC_INVALID_VALUE          2023
#define SPV_APPLIC_UNKNOWN_SEVERITY       2024
#define SPV_APPLIC_INVALID_SYSTEM         2025
#define SPV_APPLIC_INVALID_STREAM_LIST    2026
#define SPV_APPLIC_UPDATE_QUERY_FAILURE   2027
#define SPV_APPLIC_SELECT_QUERY_FAILURE   2028
#define SPV_APPLIC_DB_OPEN_FAILURE        2029
#define SPV_APPLIC_DB_CLOSE_FAILURE       2030
#define SPV_APPLIC_INSERT_QUERY_FAILURE   2031
#define SPV_APPLIC_INVALID_DB_CONNECTION  2032
#define SPV_APPLIC_DB_ROW_NOT_FOUND       2033
#define SPV_APPLIC_INVALID_RES_FIELD      2034
#define SPV_APPLIC_INVALID_PROC_CLASS     2035
#define SPV_APPLIC_PROC_CLASS_LIST_FULL   2036
#define SPV_APPLIC_CRITICAL_ERROR         2037
#define SPV_APPLIC_EXECUTING_PROCEDURE    2038
#define SPV_APPLIC_NOT_IMPLEMENTED        2039
#define SPV_APPLIC_INVALID_NODE           2040
#define SPV_APPLIC_INVALID_ZONE           2041
#define SPV_APPLIC_INVALID_REGION         2042
#define SPV_APPLIC_INVALID_PORT           2043
#define SPV_APPLIC_FIELD_BUFFER_TOO_SMALL 2044
#define SPV_APPLIC_INVALID_STREAM_ID      2045
#define SPV_APPLIC_INALID_FIELD_CAPACITY  2046
#define SPV_APPLIC_INVALID_EXECOUNT_LIST  2047
#define SPV_APPLIC_DELETE_QUERY_FAILURE   2048
#define SPV_APPLIC_FUNCTION_EXCEPTION     2049
#define SPV_APPLIC_INVALID_REG_KEY		  2050
#define SPV_APPLIC_THREAD_IS_LOCKED		  2051
#define SPV_APPLIC_INVALID_SERVER         2052
#define SPV_APPLIC_DEBUG_EVENT			  2053
#define SPV_APPLIC_THREAD_WAIT_FOR_TIMEOUT	2054
#define SPV_APPLIC_THREAD_DELETE_FAILURE	2055
#define SPV_APPLIC_EXISTING_THREAD_STRUCT	2056
#define SPV_APPLIC_FREE_FAILURE				2057
#define SPV_APPLIC_FIELD_DB_TABLE_NEEDS_DELETE	2058

#define SPV_APP_PAYLOAD_TYPE_GENERIC                0
#define SPV_APP_PAYLOAD_TYPE_NORMAL                 1
#define SPV_APP_PAYLOAD_TYPE_BLOCK                  2
#define SPV_APP_PAYLOAD_TYPE_SSP_DATABLOCK          3
#define SPV_APP_PAYLOAD_TYPE_SSP_INTEGER_DATABLOCK  4
#define SPV_APP_PAYLOAD_TYPE_SSP_NTSTRING_DATABLOCK 5

//==============================================================================
// Definizione di strutture e tipo
//------------------------------------------------------------------------------


//==============================================================================
/// Struttura dati per funzione di stato FSM.
///
/// \date [19.04.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_FSM_FUNCTION
{
  int                   ID          ; // Codice numerico identificativo
  char                  Code[12]    ; // Codice alfanumerico identificativo
  SPV_FSM_FUNCTION_PTR  Ptf         ; // Puntatore alla funzione
}
SPV_FSM_FUNCTION;

//==============================================================================
/// Struttura dati per uno stato di FSM.
///
/// \date [19.04.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_FSM_STATE
{
  int                 ID          ; // Codice numerico identificativo
  char              * Code        ; // Codice alfanumerico identificativo
  char              * Name        ; // Nome dello stato
  unsigned char       SeqParent   ; // ID dello stato parent
  unsigned char       SeqReturn   ; // Codice di ritorno del parent
  unsigned char       SeqOrder    ; // Ordine di esecuzione sequenziale
  void              * Def         ; // Ptr a struttura di definizione dello stato
  void              * DataIn      ; // Ptr alla struttura dati in ingresso
  void              * DataOut     ; // Ptr alla struttura dati in uscita
  SPV_FSM_FUNCTION  * Function    ; // Puntatore a funzione dello stato
  bool                Active      ; // Flag di stato attivo
  bool                Waiting     ; // Flag di stato in attesa
  bool                Success     ; // Flag di stato ultimato con successo
  bool                Failed      ; // Flag di stato fallito
  long                TimeStart   ; // Data ora di inizio stato
  long                TimeFinish  ; // Data ora di fine stato
  int                 RetCode     ; // Codice di ritorno dello stato
}
SPV_FSM_STATE;

//==============================================================================
/// Struttura dati per i tipi di procedura.
///
/// \date [09.04.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SPV_PROCEDURE_TYPE
{
  int           ID        ; // Indice numerico identificativo
  char        * Code      ; // Codice alfanumerico identificativo
  char        * Name      ; // Nome della procedura
  char        * Class     ; // Classe del tipo di procedura
  bool          Scheduled ; // Flag di procedura schedulata
  bool          Blocking  ; // Flag di procedura bloccante
  XML_ELEMENT * Def       ; // Puntatore all'elemento root della definizione
  void        * Tag       ; // Puntatore di servizio (?)
}
SPV_PROCEDURE_TYPE;

//==============================================================================
/// Struttura dati per la procedura.
///
/// \date [09.05.2008]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
typedef struct _SPV_PROCEDURE
{
	unsigned __int32		VCC			; // Validity Check Code
	int						ID			; // Codice numerico identificativo
	SPV_PROCEDURE_TYPE *	Type		; // Tipologia di procedura
	int						Priority	; // Priorit� della procedura
	int						ClassID		; // ID della classe di procedure
	bool					Scheduled	; // Flag di procedura schedulata
	bool					Active		; // Flag di procedura attiva (?)
	bool					Waiting		; // Flag di procedura in attesa (?)
	bool					Blocking	; // Flag di procedura bloccante
	char *					Name		; // Nome dell'entit� (?)
	SPV_DEVICE *			Device		; // Puntatore alla periferica di appartenenza (?)
	SYS_THREAD *			Thread		; // Puntatore al thread della procedura
	int						StateCount	; // Numero di stati nella lista <StateList>
	SPV_FSM_STATE *			StateList	; // Puntatore alla lista degli stati
	SPV_FSM_STATE *			State		; // Puntatore allo stato corrente
	int						LastResult	;	// Risultato ultima esecuzione
	int						RetryCount	; // Contatore dei tentativi ultima esecuzione
	long					StartDT		; // Data/ora dell'ultima partenza esecuzione
	long					FinishDT	; // Data/ora dell'ultima fine esecuzione
	int						ExeCount	; // Contatore delle esecuzioni
	bool					ECChanged	; // Flag cambiamento contatore delle esecuzioni
	void *					Tag			; // Puntatore di servizio (?)
}
SPV_PROCEDURE;

//==============================================================================
/// Struttura dati per classi di procedure.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_PROCEDURE_CLASS
{
  int                   ID        ; // Codice numerico identificativo
  char                * Name      ; // Nome classe di procedure
  char                * Code      ; // Codice alfanumerico identificativo
  bool                  Schedule  ; // Flag di classe di procedura schedulata
  bool                  Blocking  ; // Flag di classe di procedura bloccante
  void                * Tag       ; // Puntatore di servizio (?)
}
SPV_PROCEDURE_CLASS;

//==============================================================================
/// Struttura di un Thread di procedura.
///
/// \date [13.04.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_PROCEDURE_THREAD
{
  SPV_PROCEDURE         * Procedure ; // Ptr alla struttura della procedura
  SYS_THREAD            * SysThread ; // Puntatore alla struttura del Thread di sistema
  int                     State     ; // Stato (FSM) del Thread
  XML_ELEMENT           * Element   ; // Ptr all'elemento corrente
  char                  * Code      ; // Codice alfanumerico del campo
  char                  * Name      ; // Nome del campo
  char                  * Type      ; // Tipo di campo
  int                     Delay     ; // Ritardo prima dell'esecuzione (?)
  int                     Timeout   ; // Timeout per l'esecuzione (?)
  int                     Retry     ; // Numero di tentativi (?)
}
SPV_PROCEDURE_THREAD;

//==============================================================================
/// Struttura dati per classi di procedure.
///
/// \date [23.08.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_PROCEDURE_COUNTER
{
  unsigned __int64      DevID     ; // ID periferica (full 64bit coding)
  int                   ProID     ; // ID procedura
  int                   ExeCount  ; // Contatore esecuzioni procedura
  bool                  Changed   ; // Flag contatore modificato
}
SPV_PROCEDURE_COUNTER;

//==============================================================================
/// Definizione della struttura dati di IO per una procedura.
///
/// \date [07.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_FSM_DATA
{
  void              * Data      ; // Ptr ad un dato per FSM
  _SPV_FSM_DATA     * Next      ; // Ptr alla successiva struttura dati
}
SPV_FSM_DATA;

//==============================================================================
/// Definizione della struttura campo di stream di periferica
///
/// \date [26.03.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEV_STREAM_FIELD
{
	unsigned __int32		VCC				; // Validity Check Code
	int						ID				; // Codice numerico di identificazione
	char *					Name			; // Nome dello stream
	int						Type			; // Tipo di valore
	bool					ASCII			; // Valore in formato ASCII
	int						Len				; // Lunghezza del valore
	int						Capacity		; // Capacit� dell'array (se > 1 => � un array)
	char *					Format			; // Stringa di formattazione
	float					Addend			; // Addendo da sommare al valore
	float					Factor			; // Fattore di moltiplicazione valore
	bool					Visible			; // Flag di visibilita' del campo
	char **					Value			; // Lista di stringhe ASCII dei valori
	bool *					ValueChanged	; // Lista di flag di valore cambiato
	int						ValueVerNum		; // Numero di possibili verifiche di valore
	bool **					ValueVerified	; // Lista di flag di valore verificato
	char **					Descr			; // Lista di stringhe ASCII delle descrizioni valori
	char *					Default			; // Ptr alla stringa ASCII del valore di default
	int *					SevLevel		; // Livello di severit� del campo
	bool *					SevChanged		; // Flag di livello di severit� cambiato
	bool					Endian			; // Endian
	_SPV_DEV_STREAM_FIELD *	Next			; // Ptr al successivo field
	XML_ELEMENT *			Def				; // Ptr alla definizione XML
}
SPV_DEV_STREAM_FIELD;

//==============================================================================
/// Definizione della struttura buffer per stream di periferica
///
/// \date [31.05.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_DEV_STREAM_BUFFER
{
  int                           Len       ; // Lunghezza del buffer
  unsigned char               * Ptr       ; // Ptr al buffer binario
}
SPV_DEV_STREAM_BUFFER;

//==============================================================================
/// Definizione della struttura stream di periferica
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
typedef struct _SPV_DEV_STREAM
{
	unsigned __int32		VCC			; // Validity Check Code
	int						ID			; // Codice numerico di identificazione
	char *					Name		; // Nome dello stream
	bool					Visible		; // Flag di visibilita' all'operatore
	bool					Endian		; // Endian
	int                     SevLevel      ; // Livello di severit� del frame
	bool                    SevChanged    ; // Flag di severit� cambiata
	SPV_DEV_STREAM_BUFFER   Buffer        ; // Buffer binario dello stream
	bool                    BufferChanged ; // Flag di buffer cambiato
	int                     FieldNum      ; // Numero di campi dello stream
	SPV_DEV_STREAM_FIELD *	FieldList     ; // Ptr alla lista di strutture campo
	char *					Description   ; // Descrizione dello stato dello stream
	_SPV_DEV_STREAM *		Next          ; // Ptr al successivo stream
	XML_ELEMENT *			Def           ; // Ptr alla definizione XML
}
SPV_DEV_STREAM;

//==============================================================================
/// Definizione della struttura ACK di periferica
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEV_ACK
{
	unsigned __int32		VCC			; // Validity Check Code
	unsigned __int64		DevID		; // ID della periferica
	int						StrID		; // ID dello stream
	int						FieldID		; // ID del field
}
SPV_DEV_ACK;

//==============================================================================
/// Definizione della struttura di un evento scaricato da una periferica (obsoleta)
///
/// \date [20.10.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SPV_DEV_EVENT
{
  int                           ID            ; //
  int                           Type          ; //
  long                          DateTime      ; //
  int                           EventCode     ; //
  char                          EventDesc[128]; //
  int                           Data1         ; //
  int                           Data2         ; //
  int                           Data3         ; //
  int                           Data4         ; //
  char                          Data1Desc[128]; //
  char                          Data2Desc[128]; //
  char                          Data3Desc[128]; //
  char                          Data4Desc[128]; //
  char                          Note[128]     ; //
}
SPV_DEV_EVENT;

//==============================================================================
/// Definizione della struttura di un evento scaricato da una periferica
///
/// \date [08.08.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_EVENT
{
	unsigned __int32	VCC			; // Validity Check Code
	int					ID			; //
	int					Category	; //
	long				DateTime	; //
	char 				Data[32]	; //
	int					DataLen		; //
}
SPV_DEVICE_EVENT;

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------

extern  SPV_PROCEDURE_TYPE      SPVProcedureTypeList  [SPV_APPLIC_MAX_PROC_TYPE]  ;
extern  int                     SPVProcedureTypeCount                             ;
extern  SPV_PROCEDURE           SPVProcedureList      [SPV_APPLIC_MAX_PROC]       ;
extern  int                     SPVProcedureCount                                 ;
extern  SPV_COMPROT_COMMAND   * SPVComProtCommandList                             ;
extern  SPV_COMPROT_FIELD     * SPVComProtFieldList                               ;
extern  SPV_COMPROT_FRAME     * SPVComProtFrameList                               ;
extern  SPV_FSM_FUNCTION        SPVFSMFunctionList    [SPV_APPLIC_MAX_FSM_FUNC]   ;
extern  int                     SPVFSMFunctionCount                               ;
extern  SPV_PROCEDURE_THREAD    SPVProcedureThread                                ;
extern  SPV_EVENTLOG_MANAGER  * SPVApplicativeEventLog     ;

//==============================================================================
// Dichiarazione Funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo applicativo
int                     SPVApplicativeInit      ( void );
// Funzione per chiudere il modulo applicativo
int                     SPVApplicativeClear     ( void );
// Funzione per aggiungere funzioni FSM
int                     SPVAddFSMFunction       ( SPV_FSM_FUNCTION_PTR ptf, char code[12] );
// Funzione per ottenere funzioni FSM
int                     SPVGetFSMFunction       ( SPV_FSM_STATE * state, char code[12] );
// Funzione per caricare liste di funzioni FSM
int                     SPVLoadFSMFunctionList  ( SPV_FSM_FUNCTION_PTR * ptr_list, SPV_FSM_FUNCTION_CODE * code_list, int num );
// Funzione per creare un dato per stato FSM
SPV_FSM_DATA          * SPVNewFSMData           ( SPV_FSM_DATA * prev, void * data );
// Funzione per eliminare un dato per stato FSM
int                     SPVDeleteFSMData        ( SPV_FSM_DATA * data );
// Funzione per eliminare tutta una lista di dati per stato FSM
int                     SPVDeleteAllFSMData     ( SPV_FSM_DATA * first );
// Funzione per creare uno stato per FSM
int                     SPVNewFSMState          ( SPV_FSM_STATE * state );
// Funzione per creare stati per FSM
int                     SPVNewFSMStates         ( SPV_PROCEDURE * procedure );
// Funzione per risolvere il prossimo stato FSM
int                     SPVGetNextFSMState      ( SPV_PROCEDURE * procedure );
// Funzione per creare stati per FSM
int                     SPVExtractFSMState      ( SPV_FSM_STATE * state, XML_ELEMENT * element );
// Funzione per eliminare uno stato per FSM
int                     SPVFreeFSMState         ( SPV_FSM_STATE * state );
// Funzione per creare liste di stati per FSM
int                     SPVExtractFSMStates     ( SPV_PROCEDURE * procedure, XML_ELEMENT * element );
// Funzione per eliminare una lista di stati per FSM
int                     SPVFreeFSMStateList     ( SPV_PROCEDURE * procedure );
// Funzione per impostare valori di default a stati FSM
int                     SPVSetFSMStateDefault   ( SPV_FSM_STATE * state );
// Funzione per ottenere l'id dello stato parent
int                     SPVGetFSMParentState    ( XML_ELEMENT * element );
// Funzione per inizializzare il thread di controllo procedure.
int                     SPVProcedureCheckInit   ( void );
// Funzione per chiudere il thread di controllo procedure.
int                     SPVProcedureCheckClear  ( void );
// Funzione per controllare una procedura
int                     SPVProcedureCheck       ( SPV_PROCEDURE * procedure );
// Funzione di attivazione per thread Controllo Procedure.
unsigned long __stdcall SPVProcedureCheckFunction ( void * parameter );
// Funzione di attivazione per thread di FSM
unsigned long __stdcall SPVProcedureFSM         ( void * parameter );
// Funzione per verificare la validit� di un puntatore ad una struttura procedura
bool					SPVValidProcedure		( SPV_PROCEDURE * procedure );
// Funzione per creare struttura di procedure
int                     SPVNewProcedure         ( SPV_PROCEDURE * procedure, XML_ELEMENT * element );
// Funzione per verificare se una classe di procedura � nulla
bool                    SPVNullProcedureClass   ( SPV_PROCEDURE_CLASS class_item );
// Funzione per ottenere la prima posizione libera nella lista delle classi di procedure
int                     SPVGetFirstFreeClassPos ( void );
// Funzione per ottenere la posizione di una classe in base al suo codice
int                     SPVGetProcedureClassPos ( char * code );
// Funzione per caricare i dati di una nuova classe di procedure
SPV_PROCEDURE_CLASS     SPVLoadProcedureClass   ( XML_ELEMENT * element );
// Funzione per aggiungere alla lista una nuova classe di procedure
int                     SPVAddNewProcedureClass ( SPV_PROCEDURE_CLASS class_item );
// Funzione per cancellare tutta la lista delle cassi di procedure
int                     SPVClearProcedureClasses( void );
// Funzione per accedere ai dati della lista delle classi di procedure
void    				SPVEnterClassData		( char * label );
// Funzione per rilasciare i dati della lista delle classi di procedure
void                    SPVLeaveClassData       ( void );
// Funzione per ottenere il ptr alla lista delle classi di procedure
SPV_PROCEDURE_CLASS *   SPVGetClassList         ( void );
// Funzione per ottenre il numero di classi di procedure nella lista
int                     SPVGetClassCount        ( void );
// Funzione per accedere ai dati della lista delle procedure
void                    SPVEnterProcedureData   (  char *label );
// Funzione per rilasciare i dati della lista delle procedure
void                    SPVLeaveProcedureData   ( void );
// Funzione per ottenere il ptr alla lista delle procedure
SPV_PROCEDURE       *   SPVGetProcedureList     ( void );
// Funzione per ottenre il numero di procedure nella lista
int                     SPVGetProcedureCount    ( void );
// Funzione per cercare una procedura nella lista in base a DevID e ProID
SPV_PROCEDURE       *   SPVSearchProcedure      ( unsigned __int64 DevID, int ProID );
// Funzione per eliminare il thread di una procedura
int						SPVDeleteProcedureThread( SPV_PROCEDURE * procedure );
// Funzione per lanciare procedure
int                     SPVExecProcedure        ( SPV_PROCEDURE * procedure );
// Funzione per ritentare una procedura
int						SPVRetryProcedure		( SPV_PROCEDURE * procedure );
// Funzione per incrementare il numero di prossime esecuzioni di una procedura
int                     SPVIncProcedureExeCount ( SPV_PROCEDURE * procedure );
// Funzione per decrementare il numero di prossime esecuzioni di una procedura
int                     SPVDecProcedureExeCount ( SPV_PROCEDURE * procedure );
// Funzione per settare la modalita' di esecuzione continua per una procedura
int                     SPVSetProcedureContinueMode ( SPV_PROCEDURE * procedure );
// Funzione per togliere la modalita' di esecuzione continua per una procedura
int                     SPVClearProcedureExeCount   ( SPV_PROCEDURE * procedure );
// Funzione per caricare le procedure di un tipo periferica
int                     SPVLoadProcedures       ( SPV_DEVICE * device );
// Funzione per caricare le procedure del sistema.
int                     SPVLoadSystemProcedures ( SPV_SYSTEM * system );
// Funzione per chiamare la Stored Proedure di pulizia delle righe non usate
int 					SPVDeleteUnusedRows		( SPV_SYSTEM * system );
// Funzione per salvare nel database i dati della tabella Systems (Tipologie di periferica)
int 					SPVDBSaveDeviceSystems	( SPV_SYSTEM * system );
// Funzione per salvare nel database i dati della tabella Vendors
int 					SPVDBSaveDeviceVendors	( SPV_SYSTEM * system );
// Funzione per salvare nel database i dati della tabella Device_type
int 					SPVDBSaveDeviceTypes	( SPV_SYSTEM * system );
// Funzione per eliminare le procedure del sistema
int 					SPVClearProcedureThreads( SPV_SYSTEM * system );
// Funzione per eliminare le procedure del sistema
int                     SPVFreeSystemProcedures ( SPV_SYSTEM * system );
// Funzione per ottenere il thread di una procedura
SYS_THREAD *			SPVGetProcedureThread	( SPV_PROCEDURE * procedure );
// Funzione per sapere se una procedura e' attiva
bool					SPVProcedureIsActive	( SPV_PROCEDURE * procedure );
// Funzione per sapere se una procedura si trova in uno state
bool					SPVProcedureHasState	( SPV_PROCEDURE * procedure );
// Funzione per salvare nel database la tabella delle procedure applicative
int                     SPVDBSaveProcedures     ( SPV_SYSTEM * system );
// Funzione per chiamare la Stored Proedure di pulizia delle righe non pi� usate
int                     SPVDBDeleteUnusedRows   ( SPV_SYSTEM * system );
// Funzione per aggiornare le informazioni di una procedura applicativa
int                     SPVDBUpdateProcedure    ( SPV_SYSTEM * system, SPV_PROCEDURE * procedure );
// Funzione per caricare dal DB la lista degli ExeCount
int                     SPVDBLoadProcExeCounts  ( SPV_SYSTEM * system, SPV_PROCEDURE_COUNTER * count_list, int * count_num );
// Funzione per caricare i contatori di esecuzione delle procedure
int                     SPVLoadProcedureCounters( SPV_SYSTEM * system );
// Funzione per salvare nel DB la lista degli ExeCount
int                     SPVDBSaveProcExeCounts  ( SPV_SYSTEM * system, SPV_PROCEDURE_COUNTER * count_list, int count_num );
// Funzione per salvare i contatori di esecuzione delle procedure
int                     SPVSaveProcedureCounters( SPV_SYSTEM * system );
// Funzione per lanciare l'esecuzione delle procedure applicative
int                     SPVLaunchProcedures     ( SPV_SYSTEM * system );
// Funzione per creare una nuova struttura field di stream
SPV_DEV_STREAM_FIELD  * SPVNewDevField          ( int ID, char * name );
// Funzione per eliminare una struttura field di stream
int                     SPVDeleteDevField       ( SPV_DEV_STREAM_FIELD * field );
// Funzione per verificare la validita' di una struttura field
bool                    SPVValidDevField        ( SPV_DEV_STREAM_FIELD * field );
// Funzione per caricare i campi di uno stream di periferica
int                     SPVLoadDevFieldList     ( SPV_DEV_STREAM * stream );
// Funzione per eliminare una lista di strutture field
int                     SPVDeleteDevFieldList   ( SPV_DEV_STREAM_FIELD * fields, int num );
// Funzione per riallocare la struttura field.
int						SPVReallocDevFieldCapacity( SPV_DEV_STREAM_FIELD * field, int new_capacity );
// Funzione per ottenere il ptr ad una struttura field di uno stream
SPV_DEV_STREAM_FIELD  * SPVGetDevField          ( SPV_DEV_STREAM * stream, int num );
// Funzione per estrarre un field da uno stream
int                     SPVImportDevFieldBuffer ( SPV_DEV_STREAM_FIELD * field, char * buffer, int len, int * poffset );
// Funzione per creare una nuova struttura stream
SPV_DEV_STREAM        * SPVNewDevStream         ( int ID, char * name );
// Funzione per eliminare una struttura stream
int                     SPVDeleteDevStream      ( SPV_DEV_STREAM * stream );
// Funzione per verificare la validita' di una struttura stream
bool                    SPVValidDevStream       ( SPV_DEV_STREAM * stream );
// Funzione per caricare gli stream di una periferica
int                     SPVLoadDevStreamList    ( SPV_DEVICE * device );
// Funzione per eliminare gli stream di una periferica
int                     SPVFreeDevStreamList    ( SPV_DEVICE * device );
// Funzione per eliminare una lista di strutture stream
int                     SPVDeleteDevStreamList  ( SPV_DEV_STREAM * streams, int num );
// Funzione per ottenere il tipo di valore di un campo di stream
int                     SPVGetFieldValueType    ( XML_ELEMENT * value_e );
// Funzione per generare un buffer dello stream di un frame di periferica
SPV_DEV_STREAM_BUFFER   SPVDefaultDevFrameBuffer( SPV_DEVICE * device, int streamID );
// Funzione per importare un buffer in un frame
int                     SPVImportDevFrameBuffer ( SPV_DEV_STREAM * stream, char * buffer, int len );
// Funzione per accodare una stringa ad una descrizione
char *					SPVAddDescription       ( char * olddes, char * newdes );
// Funzione per verificare le dipendenze per un valore di field
bool                    SPVCheckValueDeps       ( SPV_DEVICE * device, char * dep, int eleID );
// Funzione per verificare se il valore e' un numero negativo
bool					SPVIsNegativeNumber		( char * value );
// Funzione per ottenere il valore assoluto di un numero
char *					SPVGetAbsValue			( char * value );
// Funzione per ottenere una stringa di un valore numerico con n cifre intere
char *					SPVGetExpandedNumber	( char * value, int len );
// Funzione per verificare se un valore �' in un range
bool					SPVCheckRange			( char * value, char * from, char * to );
// Funzione per ottenere la severit� di un campo di uno stream
int                     SPVGetDevFieldSeverity  ( SPV_DEV_STREAM_FIELD * field, int * max_sev, SPV_DEVICE * device, int streamID );
// Funzione per verificare se e' stato registrato un ACK
bool					SPVIsDevAck				( SPV_DEV_ACK * ack_list, int ack_num, unsigned __int64 DevID, int StrID, int FieldID );
// Funzione per ottenere la severit� di uno stream
int                     SPVGetDevStreamSeverity ( SPV_DEV_STREAM * stream, SPV_DEVICE * device, SPV_DEV_ACK * ack_list, int ack_num );
// Funzione per calcolare il livello di severit� di una periferica
int                     SPVGetDevSeverity       ( SPV_DEVICE * device, SPV_DEV_ACK * ack_list, int ack_num );
// Funzione per caricare la definizione dei frame delle periferiche
int                     SPVLoadSystemStreams    ( SPV_SYSTEM * system );
// Funzione per cancellare i dati delle periferiche del sistema
int                     SPVFreeSystemStreams    ( SPV_SYSTEM * system );
// Funzione per cercare una struttura stream all'interno di una lista
SPV_DEV_STREAM        * SPVSearchStream         ( SPV_DEV_STREAM * stream_list, int ID );
// Funzione per cercate una struttura field all'interno di una lista
SPV_DEV_STREAM_FIELD  * SPVSearchStreamField    ( SPV_DEV_STREAM_FIELD * field_list, int ID );
// Funzione per cercare il payload num-esimo con un determinato ID
SPV_COMPROT_PAYLOAD   * SPVSearchPayload        ( SPV_COMPROT_PAYLOAD * first, int ID, int num );
// Funzione per contare quanti payload hanno un determinato ID
int                     SPVCountPayload         ( SPV_COMPROT_PAYLOAD * first, int ID );
// Funzione per calcolare la lungehzza di tutti i payload con un determinato ID
int                     SPVGetPayloadLen        ( SPV_COMPROT_PAYLOAD * first, int ID );
// Funzione per estrarre uno stream da uno o pi� payload ricevuti
SPV_DEV_STREAM_BUFFER   SPVExtractStream        ( SPV_DEVICE * device, SPV_COMPROT_PAYLOAD * payload, int ID, int type, unsigned int * packet_count );
// Funzione per salvare in RAM il buffer di uno stream di periferica
int                     SPVRAMSaveStream        ( SPV_DEVICE * device, int streamID, SPV_DEV_STREAM_BUFFER buffer );
// Funzione per ottenere dalla RAM il buffer di uno stream di periferica
SPV_DEV_STREAM_BUFFER   SPVRAMGetStream         ( SPV_DEVICE * device, int streamID );
/// Funzione per eliminare uno stream dal DB
int                     SPVDBDeleteStream       ( SPV_DEVICE * device, int StrID );
// Funzione per salvare nel DB uno stream di periferica
int						SPVDBSaveStream			( SPV_DEVICE * device, SPV_DEV_STREAM * stream );
// Funzione per caricare dal DataBase il buffer di uno stream di periferica
SPV_DEV_STREAM_BUFFER   SPVDBGetStream          ( SPV_DEVICE * device, int streamID );
// Funzione per salvare nel DataBase un evento di una periferica (Mario: attualmente non serve ed essendo old style � preferibile non utilizzarla)
/*int                     SPVDBSaveEvent          ( SPV_DEVICE * device, SPV_DEV_EVENT * event );*/
// Funzione per convertire il valore di un field in un formato stampabile
char                  * SPVPrintableFieldConv   ( SPV_DEV_STREAM_FIELD * field, int arrayID );
// Funzione per marchiare come rimosse tutte le periferiche
int                     SPVDBRemoveAllDevices   ( void );
// Funzione per salvare nel DB una periferica
int                     SPVDBSaveDevice         ( SPV_DEVICE * device );
// Funzione per aggiornare nel DB una periferica
int                     SPVDBUpdateDevice       ( SPV_DEVICE * device );
// Funzione per caricare dal DB una periferica
int                     SPVDBGetDevice          ( SPV_DEVICE * device );
// Funzione per salvare nel DB lo stato periferica
int                     SPVDBSaveDeviceStatus   ( SPV_DEVICE * device );
// Funzione per cancellare nel DB gli stream di una periferica
int						SPVDBCleanAllStreams	( SPV_DEVICE * device );
// Funzione per marchiare come rimossi tutti i nodi
int                     SPVDBRemoveAllNodes     ( void );
// Funzione per salvare nel DB un nodo
int                     SPVDBSaveNode           ( SPV_NODE * node );
// Funzione per caricare dal DB un nodo
int                     SPVDBGetNode            ( SPV_NODE * node );
// Funzione per marchiare come rimosse tutte le zone
int                     SPVDBRemoveAllZones     ( void );
// Funzione per salvare nel DB una zona
int                     SPVDBSaveZone           ( SPV_ZONE * zone );
// Funzione per caricare dal DB una zona
int                     SPVDBGetZone            ( SPV_ZONE * zone );
// Funzione per marchiare come rimosse tutte le regioni
int                     SPVDBRemoveAllRegions   ( void );
// Funzione per salvare nel DB una regione
int                     SPVDBSaveRegion         ( SPV_REGION * region );
// Funzione per caricare dal DB una regione
int                     SPVDBGetRegion          ( SPV_REGION * region );
// Funzione per marchiare come rimossi tutti i server
int						SPVDBRemoveAllServers	( void );
// Funzione per salvare nel DB un server
int                     SPVDBSaveServer         ( SPV_SERVER * server );
// Funzione per caricare dal DB un server
int                     SPVDBGetServer          ( SPV_SERVER * server );
// Funzione per salvare i valori di riferimento di un campo
int						SPVDBSaveFieldReferences	( SPV_DEVICE * device, SPV_DEV_STREAM * stream, SPV_DEV_STREAM_FIELD * field );
// Funzione per salvare i valori di riferimento dei campi di uno stream
int						SPVDBSaveStreamReferences	( SPV_DEVICE * device, SPV_DEV_STREAM * stream );
// Funzione per decodificare un evento sistema ricevuto da una periferica
SPV_DEV_EVENT           SPVDecodeSysEvent       ( SPV_DEVICE * device, char * event_buffer );
// Funzione per decodificare un evento scheda ricevuto da una periferica
SPV_DEV_EVENT           SPVDecodeDevEvent       ( SPV_DEVICE * device, char * event_buffer );
// Funzione per recuperare la descrizione di un codice scheda
char                  * SPVGetDevCardDesc       ( SPV_DEVICE * device, int cardID );
// Funzione per recuperare la descrizione di un evento sistema
char                  * SPVGetSysEventDesc      ( SPV_DEVICE * device, int eventID );
// Funzione per recuperare la descrizione di un evento periferica
char                  * SPVGetDevEventDesc      ( SPV_DEVICE * device, int eventID );
// Funzione per recuparare il nome del modulo di una periferica
char                  * SPVGetDevModuleDesc     ( SPV_DEVICE * device, int moduleID );
// Funzione per ottenere l'ID di un evento a livello di field
int                     SPVGetFieldEventID      ( SPV_DEVICE * device, int streamID, int fieldID, int sev );
// Funzione per ottenere l'ID di un evento a livello di stream
int                     SPVGetStreamEventID     ( SPV_DEVICE * device, int streamID, int sev );
// Funzione per ottenere l'ID di un evento a livello di device
int                     SPVGetDeviceEventID     ( SPV_DEVICE * device, int sev );
// Funzione per settare lo stato di una periferica
int                     SPVSetDeviceStatus      ( SPV_DEVICE * device, int sev, char * des );
// Funzione per creare un messaggio per un evento di periferica
char                  * SPVBuildDevEventMsg     ( char * devid, char * devname, char * devtype, char * devstatus, char * port, char * eventsource, char * eventdes, char * note );
// Funzione per creare i patrametri per un evento di periferica
int                     SPVBuildDevEventParams  ( int category, int severity, int eventID );
// Funzione per creare un messaggio per un evento applicativo
char                  * SPVBuildApplicEventMsg  ( char * source, char * type, char * des, char * note );
// Funzione per riportare un evento periferica nell'EventLog di Windows
int                     SPVReportDevEvent       ( SPV_DEVICE * device, int streamID, int fieldID, int arrayID, int sevlevel, int eventID, char * des, char * note );
// Funzione per riportare un evento value nell'EventLog di Windows
int                     SPVReportValueEvent     ( void * pdevice, int streamID, int fieldID, int arrayID, int sevlevel, int eventID, char * des, char * note );
// Funzione per riportare un evento field nell'EventLog di Windows
int                     SPVReportFieldEvent     ( void * pdevice, int streamID, int fieldID, int arrayID, int sevlevel, int eventID, char * des );
// Funzione per riportare un evento stream nell'EventLog di Windows
int                     SPVReportStreamEvent    ( void * pdevice, int streamID, int sevlevel, int eventID, char * des );
// Funzione per riportare un evento device nell'EventLog di Windows
int                     SPVReportDeviceEvent    ( void * pdevice, int sevlevel, int eventID, char * des );
// Funzione per riportare un evento stato sistema nell'EventLog di Windows
int                     SPVReportSystemEvent    ( void * psystem, int sevlevel, int eventID, char * des, char * note );
// Funzione per riportare un evento porta di com. nell'EventLog di Windows
int                     SPVReportPortEvent      ( void * pport, int sevlevel, int eventID, char * des, char * note );
// Funzione per riportare un evento database nell'EventLog di Windows
int                     SPVReportDBEvent        ( void * conn, int sevlevel, int eventID, char * des, char * note );
// Funzione per riportare un evento applicativo nell'EventLog di Windows
int                     SPVReportApplicEvent    ( SPV_PROCEDURE * procedure, int sevlevel, int eventID, char * des, char * code );
// Funzione per riportare un evento di protocollo di comunicazione nell'EventLog di Windows
int						SPVReportComProtEvent	( SPV_COMPROT_COMMAND * command, int sevlevel, int eventID, char * des, char * code );
//
int						SPVUpdateLastAutoCheck	( void );

#endif

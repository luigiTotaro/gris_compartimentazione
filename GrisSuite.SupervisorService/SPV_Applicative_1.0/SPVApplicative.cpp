//==============================================================================
// Telefin Supervisor Applicative Module 1.0
//------------------------------------------------------------------------------
// MODULO APPLICATIVO (SPVApplicative.cpp)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	1.21 (29.03.2004 -> 20.05.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVApplicative.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVApplicative.h
//==============================================================================
#pragma hdrstop
#include "SPVApplicative.h"
#include "cnv_lib.h"
#include <math.h>
#include <time.h>
#include "SPVDBManager.h"
#include "SPVDBInterface.h"
#include "SPVTopography.h"
#include "UtilityLibrary.h"
#pragma package(smart_init)

//==============================================================================
// Definizioni locali al modulo
//------------------------------------------------------------------------------
#define SPV_APPLIC_NULL_PROC_CLASS { 0, NULL, NULL, false, false, NULL }

#define SPV_APPLIC_SERVICE_KEY				"SYSTEM\\CurrentControlSet\\Services\\StlcSPVService"
#define SPV_APPLIC_LASTAUTOCHECK_VALUE_NAME "LastAutoCheck"
//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
//static  _CRITICAL_SECTION        SPVSysDefCriticalSection;
static	SYS_LOCK				SPVSysDefLock;
//static  _CRITICAL_SECTION        SPVProcedureClassCriticalSection;
static  SYS_LOCK		        SPVProcedureClassLock;
static  int                     SPVProcedureClassCount;
static  SPV_PROCEDURE_CLASS     SPVProcedureClassList   [SPV_APPLIC_MAX_PROC_CLASS] ;
//static  _CRITICAL_SECTION        SPVProcedureTypeCriticalSection;
static  SYS_LOCK		        SPVProcedureTypeLock;
static  int                     SPVProcedureTypeCount;
static  SPV_PROCEDURE_TYPE      SPVProcedureTypeList    [SPV_APPLIC_MAX_PROC_TYPE];
//static  _CRITICAL_SECTION        SPVProcedureCriticalSection;
static  SYS_LOCK		        SPVProcedureLock;
		int                     SPVProcedureCount;
		SPV_PROCEDURE           SPVProcedureList        [SPV_APPLIC_MAX_PROC];
//static  _CRITICAL_SECTION        SPVFSMFunctionCriticalSection;
static  SYS_LOCK        		SPVFSMFunctionLock;
		int                     SPVFSMFunctionCount;
static  SPV_FSM_FUNCTION        SPVFSMFunctionList      [SPV_APPLIC_MAX_FSM_FUNC];
//static  _CRITICAL_SECTION        SPVProcedureThreadCriticalSection;
static  SYS_LOCK        		SPVProcedureThreadLock;
static  int                     SPVProcedureThreadCount;
static  SPV_PROCEDURE_THREAD    SPVProcedureThreadList  [SPV_APPLIC_MAX_PROC_THREAD];

		SPV_PROCEDURE_THREAD    SPVProcedureThread;
		SPV_COMPROT_COMMAND   * SPVComProtCommandList;
		SPV_COMPROT_FIELD     * SPVComProtFieldList;
		SPV_COMPROT_FRAME     * SPVComProtFrameList;

		SPV_EVENTLOG_MANAGER  * SPVApplicativeEventLog;

static  SYS_THREAD            * SPVProcedureCheckThread;


//==============================================================================
/// Funzione per convertire il valore in float
///
/// \date [12.02.2009]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
float _Floatize( char * value, int type )
{
	float             result      = 0.0;
	double				dresult		= 0.0;
	unsigned __int8   value_ui8   = 0;
	unsigned __int16  value_ui16  = 0;
	unsigned __int32  value_ui32  = 0;
	unsigned __int64  value_ui64  = 0;
	__int8            value_si8   = 0;
	__int16           value_si16  = 0;
	__int32           value_si32  = 0;
	__int64           value_si64  = 0;

	if ( value != NULL )
	{
		switch ( type )
		{
			case t_u_hex_8:
				value_ui8 = cnv_CharPToUHex8( value );
				result = (float)value_ui8;
			break;
			case t_u_hex_16:
				value_ui16 = cnv_CharPToUHex16( value );
				result = (float)value_ui16;
			break;
			case t_u_hex_32:
				value_ui32 = cnv_CharPToUHex32( value );
				result = (float)value_ui32;   // Attenzione! non funziona!
			break;
	  case t_u_hex_64:
		/*
        value_ui64 = cnv_CharPToUHex64( value );
        result = (float)value_ui64;
        */
        result = 0.0;
	  break;
	  case t_s_hex_8:
		value_si8 = cnv_CharPToSHex8( value );
		result = (float)value_si8;
	  break;
	  case t_s_hex_16:
		value_si16 = cnv_CharPToSHex16( value );
		result = (float)value_si16;
	  break;
	  case t_u_int_8:
        value_ui8 = cnv_CharPToUInt8( value );
        result = (float)value_ui8;
	  break;
      case t_u_int_16:
        value_ui16 = cnv_CharPToUInt8( value );
        result = (float)value_ui16;
      break;
      case t_u_int_32:
		value_ui32 = cnv_CharPToUInt32( value );
        result = (float)value_ui32;
	  break;
      case t_u_int_64:
        value_ui64 = cnv_CharPToUInt64( value );
        result = (float)value_ui64;
      break;
      case t_s_int_8:
        value_si8 = cnv_CharPToInt8( value );
        result = (float)value_si8;
      break;
      case t_s_int_16:
        value_si16 = cnv_CharPToInt16( value );
        result = (float)value_si16;
      break;
      case t_s_int_32:
        value_si32 = cnv_CharPToInt32( value );
        result = (float)value_si32;
      break;
      case t_s_int_64:
        value_si64 = cnv_CharPToInt64( value );
		result = (float)value_si64;
      break;
    }
  }

  return result;
}

//==============================================================================
/// Funzione per convertire il valore in double
///
/// \date [01.07.2009]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
double _Doublize( char * value, int type, bool ASCII )
{
	double				result      = 0.0;
	unsigned __int8		value_ui8   = 0;
	unsigned __int16	value_ui16  = 0;
	unsigned __int32	value_ui32  = 0;
	unsigned __int64	value_ui64  = 0;
	__int8				value_si8   = 0;
	__int16				value_si16  = 0;
	__int32				value_si32  = 0;
	__int64				value_si64  = 0;
	unsigned __int8	 *	pvalue_ui8   = 0;
	unsigned __int16 *	pvalue_ui16  = 0;
	unsigned __int32 *	pvalue_ui32  = 0;
	unsigned __int64 *	pvalue_ui64  = 0;
	__int8			 *	pvalue_si8   = 0;
	__int16			 *	pvalue_si16  = 0;
	__int32			 *	pvalue_si32  = 0;
	__int64			 *	pvalue_si64  = 0;

	if ( value != NULL )
	{
		switch ( type )
		{
			case t_u_hex_8:
				if (ASCII) {
					value_ui8 = cnv_CharPToUHex8( value );
				}
				else
				{
					pvalue_ui8 = (unsigned __int8 *)(void*)value;
					value_ui8 = *pvalue_ui8;
				}
				result = (double)value_ui8;
			break;
			case t_u_hex_16:
				if (ASCII) {
					value_ui16 = cnv_CharPToUHex16( value );
				}
				else
				{
					pvalue_ui16 = (unsigned __int16 *)(void*)value;
					value_ui16 = *pvalue_ui16;
				}
				result = (double)value_ui16;
			break;
			case t_u_hex_32:
				if (ASCII) {
					value_ui32 = cnv_CharPToUHex32( value );
				}
				else
				{
					pvalue_ui32 = (unsigned __int32 *)(void*)value;
					value_ui32 = *pvalue_ui32;
				}
				result = (double)value_ui32;
			break;
			case t_u_hex_64:
				/*
				value_ui64 = cnv_CharPToUHex64( value );
				result = (double)value_ui64;
				*/
				result = 0.0;
			break;
			case t_s_hex_8:
				if (ASCII) {
					value_si8 = cnv_CharPToSHex8( value );
				}
				else
				{
					pvalue_si8 = (__int8 *)(void*)value;
					value_si8 = *pvalue_si8;
				}
				result = (double)value_si8;
			break;
			case t_s_hex_16:
				if (ASCII) {
					value_si16 = cnv_CharPToSHex16( value );
				}
				else
				{
					pvalue_si16 = (__int16 *)(void*)value;
					value_si16 = *pvalue_si16;
				}
				result = (double)value_si16;
			break;
			case t_u_int_8:
				if (ASCII) {
					value_ui8 = cnv_CharPToUInt8( value );
				}
				else
				{
					pvalue_ui8 = (unsigned __int8 *)(void*)value;
					value_ui8 = *pvalue_ui8;
				}
				result = (double)value_ui8;
			break;
			case t_u_int_16:
				if (ASCII) {
					value_ui16 = cnv_CharPToUInt16( value );
				}
				else
				{
					pvalue_ui16 = (unsigned __int16 *)(void*)value;
					value_ui16 = *pvalue_ui16;
				}
				result = (double)value_ui16;
			break;
			case t_u_int_32:
				if (ASCII) {
					value_ui32 = cnv_CharPToUInt32( value );
				}
				else
				{
					pvalue_ui32 = (unsigned __int32 *)(void*)value;
					value_ui32 = *pvalue_ui32;
				}
				result = (double)value_ui32;
			break;
			case t_u_int_64:
				if (ASCII) {
					value_ui64 = cnv_CharPToUInt64( value );
				}
				else
				{
					pvalue_ui64 = (unsigned __int64 *)(void*)value;
					value_ui64 = *pvalue_ui64;
				}
				result = (double)value_ui64;
			break;
			case t_s_int_8:
				if (ASCII) {
					value_si8 = cnv_CharPToInt8( value );
				}
				else
				{
					pvalue_si8 = (__int8 *)(void*)value;
					value_si8 = *pvalue_si8;
				}
				result = (double)value_si8;
			break;
			case t_s_int_16:
				if (ASCII) {
					value_si16 = cnv_CharPToInt16( value );
				}
				else
				{
					pvalue_si16 = (__int16 *)(void*)value;
					value_si16 = *pvalue_si16;
				}
				result = (double)value_si16;
			break;
			case t_s_int_32:
				if (ASCII) {
					value_si32 = cnv_CharPToInt32( value );
				}
				else
				{
					pvalue_si32 = (__int32 *)(void*)value;
					value_si32 = *pvalue_si32;
				}
				result = (double)value_si32;
			break;
			case t_s_int_64:
				if (ASCII) {
					value_si64 = cnv_CharPToInt64( value );
				}
				else
				{
					pvalue_si64 = (__int64 *)(void*)value;
					value_si64 = *pvalue_si64;
				}
				result = (double)value_si64;
			break;
		}
	}

	return result;
}

//==============================================================================
/// Funzione per sommare l'addendo ad un valore di field
///
/// \date [26.03.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
float _ApplyAddend( SPV_DEV_STREAM_FIELD * field, double value )
{
	double result = 0.0;

	if ( SPVValidDevField( field ) )
	{
		result = value + ((double)field->Addend);
	}
	else
	{
		result = value;
	}

	return result;
}

//==============================================================================
/// Funzione per sommare l'addendo ad un valore di field
/// Caso calcolo diretto da stringa di fattore (no field)
///
/// \date [26.03.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
float _ApplyAddend( char * addend_string, double value )
{
	double result = 0.0;
	double addend;

	if ( addend_string != NULL )
	{
		addend = (double)cnv_CharPToFloat( addend_string );
		result = value * addend;
	}
	else
	{
		result = value;
	}

	return result;
}

//==============================================================================
/// Funzione per applicare il fattore di moltiplicazione ad un valore di field
///
/// \date [26.07.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
float _ApplyFactor( SPV_DEV_STREAM_FIELD * field, double value )
{
	double result = 0.0;

	if ( SPVValidDevField( field ) )
	{
		result = value * field->Factor;
	}
	else
	{
		result = value;
	}

	return result;
}

//==============================================================================
/// Funzione per applicare il fattore di moltiplicazione ad un valore
/// Caso calcolo diretto da stringa di fattore (no field)
///
/// \date [03.02.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
float _ApplyFactor( char * factor_string, double value )
{
	double result = 0.0;
	double factor;

	if ( factor_string != NULL )
	{
		factor = (double)cnv_CharPToFloat( factor_string );
		result = value * factor;
	}
	else
	{
		result = value;
	}

	return result;
}

//==============================================================================
/// Funzione per applicare la stringa di formattazione ad un valore di field
///
/// \date [03.07.2012]
/// \author Enrico Alborali
/// \version 1.02
//------------------------------------------------------------------------------
char * _ApplyFormat( SPV_DEV_STREAM_FIELD * field, double value )
{
	char * fstring = NULL;
	char   buffer[1024];

	if ( SPVValidDevField( field ) )
	{
		if ( field->Format != NULL )
		{
			// --- Caso conversione data-ora (con aggiustamento Telefin) ---
			if ( strcmpi( field->Format, "tlf_datetime" ) == 0 )
			{
				long 		date_time = 0;
				char * 		formatted_date_time = NULL;
				struct tm *	time_now  = NULL; // Data ora formato tm
				time_t		secs_now        ; // Data ora formato time_t

				date_time = (long)value;
				// Aggiustamento offset
				date_time += SPV_APPLIC_EVENT_DATETIME_OFFSET_STANDARD;
				secs_now = (time_t)date_time;
				time_now = localtime( &secs_now );
				if ( time_now->tm_isdst == 1 )
				{
					date_time -= 3600;
				}
                // Formattazione
				secs_now = (time_t)date_time;
				time_now = localtime( &secs_now );
				if ( time_now != NULL )
				{
					strftime( &buffer[0], 24, "%d/%m/%Y %H:%M:%S", time_now );
				}
				else
				{
					strcpy( &buffer[0], "n/a" );
                }
			}
			// --- Caso conversione ora ---
			else if ( strcmpi( field->Format, "hh:mm" ) == 0 )
			{
				int hh = 0;
				int mm = 0;
				hh = (int)floor( value / ( 60 * 60 ) );
				mm = ((int)floor( value / 60 )) % 60;
				sprintf( buffer, "%02i:%02i", hh, mm );
			}
			else
			{
				// Valore intero senza segno
				if ( strstr( field->Format, "%x" ) != NULL
					|| strstr( field->Format, "0x%X" ) != NULL
					|| strstr( field->Format, "0x%X" ) != NULL
					|| strstr( field->Format, "0x%02X" ) != NULL
					|| strstr( field->Format, "0x%04X" ) != NULL
					|| strstr( field->Format, "0x%08X" ) != NULL
					|| strstr( field->Format, "%u" ) != NULL
					|| strstr( field->Format, "%.2u" ) != NULL
					|| strstr( field->Format, "%.3u" ) != NULL
					|| strstr( field->Format, "%.4u" ) != NULL
					|| strstr( field->Format, "%.5u" ) != NULL
					|| strstr( field->Format, "%.6u" ) != NULL
					|| strstr( field->Format, "%.7u" ) != NULL
					|| strstr( field->Format, "%.8u" ) != NULL
					|| strstr( field->Format, "%.9u" ) != NULL
					|| strstr( field->Format, "%.10u" ) != NULL )
				{
					sprintf( buffer, field->Format, (unsigned long int)value );
				}
				// Valore intero con segno
				else if ( strstr( field->Format, "%i" ) != NULL
					|| strstr( field->Format, "%.2i" ) != NULL
					|| strstr( field->Format, "%.3i" ) != NULL
					|| strstr( field->Format, "%.4i" ) != NULL
					|| strstr( field->Format, "%.5i" ) != NULL
					|| strstr( field->Format, "%.6i" ) != NULL
					|| strstr( field->Format, "%.7i" ) != NULL
					|| strstr( field->Format, "%.8i" ) != NULL
					|| strstr( field->Format, "%.9i" ) != NULL
					|| strstr( field->Format, "%.10i" ) != NULL
					|| strstr( field->Format, "%d" ) != NULL
					|| strstr( field->Format, "%.2d" ) != NULL
					|| strstr( field->Format, "%.3d" ) != NULL
					|| strstr( field->Format, "%.4d" ) != NULL
					|| strstr( field->Format, "%.5d" ) != NULL
					|| strstr( field->Format, "%.6d" ) != NULL
					|| strstr( field->Format, "%.7d" ) != NULL
					|| strstr( field->Format, "%.8d" ) != NULL
					|| strstr( field->Format, "%.9d" ) != NULL
					|| strstr( field->Format, "%.10d" ) != NULL	)
				{
					sprintf( buffer, field->Format, (signed long int)value );
				}
				// Valore a virgola mobile
				else
				{
					sprintf( buffer, field->Format, value );
				}
			}
			fstring = XMLStrCpy( NULL, buffer );
		}
		else
		{
			sprintf( buffer, "%1.0f", value );
			fstring = XMLStrCpy( NULL, buffer ); // -MALLOC
		}
	}

	return fstring;
}

//==============================================================================
/// Funzione per applicare la stringa di formattazione ad un valore
/// Caso utilizzo di una stringa di formattazione direttamente (no field).
///
/// \date [03.02.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
char * _ApplyFormat( char * format, double value )
{
	char * fstring = NULL;
	char   buffer[1024];

	if ( format != NULL )
	{
		// --- Caso conversione data-ora ---
		if ( strcmpi( format, "hh:mm" ) == 0 )
		{
			int hh = 0;
			int mm = 0;
			hh = (int)floor( value / ( 60 * 60 ) );
			mm = ((int)floor( value / 60 )) % 60;
			sprintf( buffer, "%02i:%02i", hh, mm );
		}
		else
		{
			// Valore intero senza segno
			if ( strstr( format, "%x" ) != NULL
				|| strstr( format, "0x%X" ) != NULL
				|| strstr( format, "0x%X" ) != NULL
				|| strstr( format, "0x%02X" ) != NULL
				|| strstr( format, "0x%04X" ) != NULL
				|| strstr( format, "0x%08X" ) != NULL
				|| strstr( format, "%u" ) != NULL
				|| strstr( format, "%.2u" ) != NULL
				|| strstr( format, "%.3u" ) != NULL
				|| strstr( format, "%.4u" ) != NULL
				|| strstr( format, "%.5u" ) != NULL
				|| strstr( format, "%.6u" ) != NULL
				|| strstr( format, "%.7u" ) != NULL
				|| strstr( format, "%.8u" ) != NULL
				|| strstr( format, "%.9u" ) != NULL
				|| strstr( format, "%.10u" ) != NULL )
			{
				sprintf( buffer, format, (unsigned long int)value );
			}
			// Valore intero con segno
			else if ( strstr( format, "%i" ) != NULL
				|| strstr( format, "%.2i" ) != NULL
				|| strstr( format, "%.3i" ) != NULL
				|| strstr( format, "%.4i" ) != NULL
				|| strstr( format, "%.5i" ) != NULL
				|| strstr( format, "%.6i" ) != NULL
				|| strstr( format, "%.7i" ) != NULL
				|| strstr( format, "%.8i" ) != NULL
				|| strstr( format, "%.9i" ) != NULL
				|| strstr( format, "%.10i" ) != NULL
				|| strstr( format, "%d" ) != NULL
				|| strstr( format, "%.2d" ) != NULL
				|| strstr( format, "%.3d" ) != NULL
				|| strstr( format, "%.4d" ) != NULL
				|| strstr( format, "%.5d" ) != NULL
				|| strstr( format, "%.6d" ) != NULL
				|| strstr( format, "%.7d" ) != NULL
				|| strstr( format, "%.8d" ) != NULL
				|| strstr( format, "%.9d" ) != NULL
				|| strstr( format, "%.10d" ) != NULL )
			{
				sprintf( buffer, format, (signed long int)value );
			}
			// Valore a virgola mobile
			{
				sprintf( buffer, format, value );
			}
		}
		fstring = XMLStrCpy( NULL, buffer );
	}
	else
	{
		sprintf( buffer, "%1.0f", value );
		fstring = XMLStrCpy( NULL, buffer ); // -MALLOC
	}

	return fstring;
}

char * _StringUnString(char * value)
{
	int string_len = 0;
	char * new_value = NULL;
	int new_string_len = 0;
	char byte_string[3];
	char single_byte = 0;

	string_len = strlen(value);

	if (string_len > 0) {
		new_string_len = (string_len/2);
		new_value = (char*)malloc(sizeof(char)*(new_string_len+1));
		memset((void*)new_value,0,sizeof(char)*(new_string_len+1));
		for (int i = 0; i < new_string_len; i++) {
			byte_string[0] = value[(i*2)+0];
			byte_string[1] = value[(i*2)+1];
			byte_string[2] = 0;

			single_byte = cnv_CharPToUHex8( &byte_string[0] );
			new_value[i] = single_byte;
		}
	}

	return new_value;
}

//==============================================================================
/// Funzione per inizializzare il modulo applicativo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.12
//------------------------------------------------------------------------------
int SPVApplicativeInit( void )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	// --- Inizializzo la sezione critica globale per la definizione del sistema ---
	//InitializeCriticalSection( &SPVSysDefCriticalSection );
	SYSInitLock(&SPVSysDefLock,"SPVSysDefLock");

	// --- Inizializzo le variabili globali per le classi di procedure ---
	//InitializeCriticalSection( &SPVProcedureClassCriticalSection );
	SYSInitLock(&SPVProcedureClassLock,"SPVProcedureClassLock");
	SPVProcedureClassCount = 0;
	memset( &SPVProcedureClassList, sizeof(SPV_PROCEDURE_CLASS) * SPV_APPLIC_MAX_PROC_CLASS, 0 );

	// --- Inizializzo le variabili globali per i tipi di procedure ---
	//InitializeCriticalSection( &SPVProcedureTypeCriticalSection );
	SYSInitLock(&SPVProcedureTypeLock,"SPVProcedureTypeLock");
	SPVProcedureTypeCount = 0;
	memset( &SPVProcedureTypeList, sizeof(SPV_PROCEDURE_TYPE) * SPV_APPLIC_MAX_PROC_TYPE, 0 );

	// --- Inizializzo le variabuli globali per le procedure ---
	//InitializeCriticalSection( &SPVProcedureCriticalSection );
	SYSInitLock(&SPVProcedureLock,"SPVProcedureLock");
	SPVProcedureCount = 0;
	memset( &SPVProcedureList, sizeof(SPV_PROCEDURE) * SPV_APPLIC_MAX_PROC, 0 );

	// --- Inizializzo le variabili globali per le funzioni FSM ---
	//InitializeCriticalSection( &SPVFSMFunctionCriticalSection );
	SYSInitLock(&SPVFSMFunctionLock,"SPVFSMFunctionLock");
	SPVFSMFunctionCount = 0;
	memset( &SPVFSMFunctionList, sizeof(SPV_FSM_FUNCTION) * SPV_APPLIC_MAX_FSM_FUNC, 0 );

	// --- Inizializzo le variabili globali per i thread di procedura ---
	//InitializeCriticalSection( &SPVProcedureThreadCriticalSection );
	SYSInitLock(&SPVProcedureThreadLock,"SPVProcedureThreadLock");
	SPVProcedureThreadCount = 0;
	memset( &SPVProcedureThreadList, sizeof(SPV_PROCEDURE_THREAD) * SPV_APPLIC_MAX_PROC_THREAD, 0 );

	// --- Carico la libreria applicativa ---
	SPVApplicLibInit();
	SPVLoadFSMFunctionList( &SPVApplicLibFunctionList[0], &SPVApplicLibFunctionCodes[0], SPVApplicLibFunctionCount );

	// --- Inizializzo il modulo di protocollo di comunicazione ---
	SPVComProtInit( &SPVSysDefLock );

	// --- Inizializzo il file di EventLog di Windows ---
	int fun_code;
	fun_code = SYSEventLogCreateFile( SPV_APPLIC_EVENT_LOG_FILE );
	if ( fun_code == 0 )
	{
		fun_code = SYSEventLogCreateSource( SPV_APPLIC_EVENT_LOG_FILE, SPV_EVENTLOG_DEFAULT_MSWIN_LOG );
	}

	// --- Creo un nuovo EventLog manager ---
	SPVApplicativeEventLog = SPVNewEventLog( SPV_EVENTLOG_TYPE_MSWIN_LOG, SPV_EVENTLOG_TEMPLATE_NONE, SPV_EVENTLOG_DEFAULT_MSWIN_LOG );
	if ( SPVApplicativeEventLog != NULL )
	{
		// --- Apro l'EventLog Manager creato ---
		SPVOpenEventLog( SPVApplicativeEventLog->ID );
	}

	SPVSetComProtLogFunction( (void*)SPVReportComProtEvent );

	SPVDBMSetLogFunction( (void*)SPVReportDBEvent );

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo applicativo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVApplicativeClear( void )
{
  int ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  // --- Chiudo il thread di controllo procedure ---
  SPVProcedureCheckClear( );

  // --- Chiudo il modulo di protocollo di comunicazione ---
  SPVComProtClear( );

  // --- Resetto la lista delle funzioni della libreria applicativa ---
  SPVAppLibReset( );

  // --- Cancello le variabili globali per i thread di procedura ---
  //DeleteCriticalSection( &SPVProcedureThreadCriticalSection );
  SYSClearLock(&SPVProcedureThreadLock);
  SPVProcedureThreadCount = 0;
  memset( &SPVProcedureThreadList, sizeof(SPV_PROCEDURE_THREAD) * SPV_APPLIC_MAX_PROC_THREAD, 0 );

  // --- Cancello le variabili globali per le funzioni FSM ---
  //DeleteCriticalSection( &SPVFSMFunctionCriticalSection );
  SYSClearLock(&SPVFSMFunctionLock);
  SPVFSMFunctionCount = 0;
  memset( &SPVFSMFunctionList, sizeof(SPV_FSM_FUNCTION) * SPV_APPLIC_MAX_FSM_FUNC, 0 );

  // --- Cancello le variabili globali per le procedure ---
  //DeleteCriticalSection( &SPVProcedureCriticalSection );
  SYSClearLock(&SPVProcedureLock);
  SPVProcedureCount = 0;
  memset( &SPVProcedureList, sizeof(SPV_PROCEDURE) * SPV_APPLIC_MAX_PROC, 0 );

  // --- Cancello le variabili globali per i tipi di procedure ---
  //DeleteCriticalSection( &SPVProcedureTypeCriticalSection );
  SYSClearLock(&SPVProcedureTypeLock);
  SPVProcedureTypeCount = 0;
  memset( &SPVProcedureTypeList, sizeof(SPV_PROCEDURE_TYPE) * SPV_APPLIC_MAX_PROC_TYPE, 0 );

  // --- Cancello le variabili globali per le classi di procedure ---
  //DeleteCriticalSection( &SPVProcedureClassCriticalSection );
  SYSClearLock(&SPVProcedureClassLock);
  SPVProcedureClassCount = 0;
  memset( &SPVProcedureClassList, sizeof(SPV_PROCEDURE_CLASS) * SPV_APPLIC_MAX_PROC_CLASS, 0 );

  // --- Cancello la sezione critica globale per la definizione del sistema ---
  //DeleteCriticalSection( &SPVSysDefCriticalSection );
  SYSClearLock(&SPVSysDefLock);

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere funzioni FSM.
/// Dato un puntatore a funzione di stato FSM <ptf> e il suo codice alfanumerico
/// identificativo <code>, viene inserita una nuova struttura funzione stato
/// nella lista globale.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.04.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAddFSMFunction( SPV_FSM_FUNCTION_PTR ptf, char code[12] )
{
  int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;
  SPV_FSM_FUNCTION * function;

  if ( ptf ) // Controllo il puntatore alla funzione
  {
    function = &SPVFSMFunctionList[SPVFSMFunctionCount];
    function->ID    = SPVFSMFunctionCount;
    strcpy( function->Code, code );
    function->Ptf   = ptf ;
	SPVFSMFunctionCount++; // Incremento il contatore delle funzioni
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_FUNCTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere funzioni FSM.
/// Dato un certo codice identificativo alfanumerico <code> ricerca la funzione
/// di stato nella lista globale. <function> verr� riempito con il puntatore
/// alla struttura funzione.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [04.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetFSMFunction( SPV_FSM_STATE * state, char code[12] )
{
	int                 ret_code  = SPV_APPLIC_FUNCTION_SUCCESS ;
	SPV_FSM_FUNCTION  * current                                 ;
	bool                found     = false                       ;

	state->Function = NULL;
	if ( SPVFSMFunctionCount > 0 )
	{
		for ( int f = 0; f < SPVFSMFunctionCount; f++ )
		{
			current = &SPVFSMFunctionList[f];
			if ( strcmpi( current->Code, code ) == 0 )
			{
				state->Function = current;
				found = true;
				f = SPVFSMFunctionCount;
			}
		}
		if ( !found )
		{
			ret_code = SPV_APPLIC_INVALID_CODE;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_LIST;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare liste di funzioni FSM
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [01.08.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVLoadFSMFunctionList(SPV_FSM_FUNCTION_PTR * ptr_list,SPV_FSM_FUNCTION_CODE * code_list, int num )
{
  int ret_code = 0; // Codice di ritorno
  SPV_FSM_FUNCTION_PTR  ptr;

  if ( ptr_list != NULL && code_list != NULL )
  {
    for (int f=0; f<num; f++)
    {
	  ptr   = ptr_list[f];
      SPVAddFSMFunction(ptr,code_list[f]);
    }
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_FUNCTION_LIST;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare un dato per stato FSM
///
/// In caso di errore restituisce NULL.
///
/// \date [07.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_FSM_DATA * SPVNewFSMData( SPV_FSM_DATA * prev, void * data )
{
  SPV_FSM_DATA * new_data = NULL; // Ptr alla struttura dati per stato FSM

  new_data = (SPV_FSM_DATA*)malloc( sizeof(SPV_FSM_DATA) );

  if ( new_data != NULL )
  {
    new_data->Data  = data;
	new_data->Next  = NULL;
    if ( prev != NULL )
    {
      prev->Next = new_data;
    }
  }

  return new_data;
}

//==============================================================================
/// Funzione per eliminare un dato per stato FSM
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [07.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDeleteFSMData( SPV_FSM_DATA * data )
{
  int ret_code = 0; // Codice di ritorno

  if ( data != NULL )
  {
    if ( data->Data != NULL )
    {
      try
      {
        free( data->Data );
	  }
      catch(...)
      {
        /* TODO -oEnrico -cError : Gestire l'errore critico */
      }
    }
    try
    {
      free ( data );
    }
    catch(...)
    {
	  /* TODO -oEnrico -cError : Gestire l'errore critico */
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DATA;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare tutta una lista di dati per stato FSM
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [07.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDeleteAllFSMData( SPV_FSM_DATA * first )
{
  int             ret_code  = 0   ; // Codice di ritorno
  SPV_FSM_DATA  * data      = NULL; // Ptr ad una struttura dati per stato FSM
  SPV_FSM_DATA  * next      = NULL; // Ptr ad una struttura dati per stato FSM

  if ( first != NULL ) // Controllo il ptr
  {
    data = first;
    while ( data != NULL )
    {
      next = data->Next; // Salvo il ptr del dato successivo
      SPVDeleteFSMData( data ); // Cancello il dato corrente
      data = next; // Setto il prossimo dato da candellare
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DATA;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare uno stato per FSM.
/// Alloca la memoria per una nuova struttura di stato per FSM (Finite State
/// Machine) e riempie il contenuto della stessa con i valori di default.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [16.04.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVNewFSMState( SPV_FSM_STATE * state )
{
  int     ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;

  state = (SPV_FSM_STATE*) malloc( sizeof(SPV_FSM_STATE) ); // Alloco

  if ( state != NULL ) // Controllo l'allocazione
  {
    ret_code = SPVSetFSMStateDefault( state );
  }
  else // Allocazione fallita
  {
    ret_code = SPV_APPLIC_ALLOCATION_FAILURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare stati per FSM.
/// Alloca la memoria per una nuova lista di strutture di stato per FSM (Finite
/// State Machine) e le riempie con valori di default. La lista viene inserita
/// all'interno della struttura <perocedure>.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.04.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVNewFSMStates( SPV_PROCEDURE * procedure )
{
  int ret_code  = SPV_APPLIC_FUNCTION_SUCCESS;

  if ( procedure != NULL ) // Controllo la procedura
  {
    if ( procedure->StateCount > 0 ) // Controllo che il numero di stati sia valido
    {
      procedure->StateList = (SPV_FSM_STATE*) malloc( sizeof(SPV_FSM_STATE) * procedure->StateCount );

      if ( procedure->StateList != NULL ) // Se l'allocazione � andata a buon fine
      {
        for ( int s = 0; s < procedure->StateCount; s++ )
        {
          ret_code = SPVSetFSMStateDefault( &procedure->StateList[s] );
        }
      }
      else // Se l'allocazione � fallita
      {
        ret_code = SPV_APPLIC_ALLOCATION_FAILURE;
      }
    }
    else
    {
      ret_code = SPV_APPLIC_INVALID_COUNT;
	}
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per risolvere il prossimo stato FSM.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [15.05.2008]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SPVGetNextFSMState( SPV_PROCEDURE * procedure )
{
	int             ret_code    = SPV_APPLIC_FUNCTION_SUCCESS ;
	int             sretcode    = 0                           ;
	SPV_FSM_STATE * state       = NULL                        ;
	SPV_FSM_STATE * next        = NULL                        ;
	bool            condition   = true                        ;
	unsigned char   cSeqParent                                ;
	unsigned char   cSeqOrder                                 ;
	unsigned char   nSeqParent                                ;
	unsigned char   nSeqReturn                                ;
	unsigned char   nSeqOrder                                 ;

	// --- Entro nei dati procedure ---
	SPVEnterProcedureData("SPVGetNextFSMState");

	try
	{
		if ( procedure != NULL ) // Controllo la procedura
		{
			if ( procedure->StateList != NULL ) // Controllo la lista degli stati
			{
				///states  = procedure->StateList;
				state   = procedure->State;
				if ( state != NULL ) // Controllo lo stato corrente
				{
					///ID          = state->ID;
					sretcode    = state->RetCode;
					//--- Recupero i codici di sequenza ---
					cSeqParent  = state->SeqParent  ;
					cSeqParent  = (unsigned char)state->ID         ; /// ***
					///cSeqReturn  = state->SeqReturn  ;
					cSeqOrder   = state->SeqOrder   ;
					//--- Ricerca salto incondizionato ---
					for ( int s = 0; s < procedure->StateCount; s++ )
					{
						state = &procedure->StateList[s]; // Prendo lo stato s-esimo
						if ( state != NULL )
						{
							nSeqParent  = state->SeqParent  ;
							nSeqReturn  = state->SeqReturn  ;
							nSeqOrder   = state->SeqOrder   ;
							if ( nSeqParent == cSeqParent )
							{
								if ( nSeqReturn == 0 )
								{
									if ( nSeqOrder > ( cSeqOrder + 1 ) )
									{
										condition = false;
										next = state; // Ho trovato il prossimo stato
									}
								}
							}
						}
					}
					//--- Ricerca salto condizionato ---
					if ( condition )
					{
						condition = false;
						for ( int s = 0; s < procedure->StateCount; s++ )
						{
							state = &procedure->StateList[s]; // Prendo lo stato s-esimo
							if ( state )
							{
								nSeqParent  = state->SeqParent  ;
								nSeqReturn  = state->SeqReturn  ;
								nSeqOrder   = state->SeqOrder   ;
								if ( nSeqParent == cSeqParent )
								{
									if ( nSeqReturn == sretcode )
									{
										condition = true;
										next = state; // Ho trovato il prossimo stato
									}
								}
							}
						}
					}
				   /*	if ( next == NULL )
					{
						procedure->Active = false;
					}*/
				}
				else // Stato corrente nullo
				{
					if ( procedure->Active )
					{
						next = &procedure->StateList[0];
					}
				}
				procedure->State = next;
			}
			else
			{
				ret_code = SPV_APPLIC_INVALID_LIST;
			}
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_PROCEDURE;
		}
	}
	catch(...)
	{
		// --- Caso eccezione durante la ricerca dello stato successivo ---
		next = NULL;
		//procedure->Active = false;
		ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
	}

	// --- Esco dai dati procedure ---
	SPVLeaveProcedureData();

	return ret_code;
}

//==============================================================================
/// Funzione per creare stati per FSM.
/// Estrae i dati presenti nell'elemento XML <element> di definizione dello
/// stato per FSM (Finite State Machine) e li memorizza all'interno di una
/// struttura <state> precedentemente creata.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVExtractFSMState( SPV_FSM_STATE * state, XML_ELEMENT * element )
{
	int     ret_code  = SPV_APPLIC_FUNCTION_SUCCESS ;
	char  * value     = NULL                        ;

	if ( element != NULL ) // Controllo l'elemento
	{
		if ( state != NULL ) // Controllo la struttura dello stato
		{
			value = XMLGetName( element ); // Leggo il nome dell'elemento XML
			if ( strcmpi( value, "state" ) == 0 ) // Controllo il nome dell'elemento
			{
				// --- Estraggo l'ID ---
				state->ID   = XMLGetID( element );
				if ( state->ID == -1 ) state->ID = 0;
				// --- Estraggo il codice ---
				state->Code = XMLExtractValue( element, "code" );
				// --- Recupero la funzione FSM ---
				ret_code    = SPVGetFSMFunction( state, state->Code );
				// --- Estraggo il nome ---
				state->Name = XMLExtractValue( element, "name" );
				// --- Salvo l'elemento base della definizione XML ---
				state->Def  = (void *) element;
				// --- Estraggo il codice di ritorno ---
				state->SeqReturn = (unsigned char)XMLGetValueInt( element,"return" );
			}
			else
			{
				ret_code = XML_INT_ERROR_INVALID_NAME;
			}
    	}
		else // la struttura dello stato non � valida
		{
			ret_code = SPV_APPLIC_INVALID_FSM_STATE;
		}
	}
	else // l'elemento non � valido
	{
		ret_code = XML_ERROR_INVALID_ELEMENT;
	}

	return ret_code;

}

//==============================================================================
/// Funzione per eliminare uno stato per FSM
///
/// \date [30.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeFSMState( SPV_FSM_STATE * state )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS;

	if ( state != NULL )
	{
		// --- Cancello il codice ---
		if ( state->Code != NULL )
		{
			try
			{
				free( state->Code );
			}
			catch(...)
			{
				ret_code = SPV_APPLIC_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello il nome ---
		if ( state->Name != NULL )
		{
			try
			{
				free( state->Name );
			}
			catch(...)
			{
				ret_code = SPV_APPLIC_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_FSM_STATE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per creare liste di stati per FSM.
/// Partendo dall'elemento XML <elemento> vengono contati gli stati definiti e
/// il valore viene salvato nella lstruttura <procedure>. La funzione poi alloca
/// all'intenro di <procedure> una lista di strutture e le riempie con i valori
/// run-time di default e con i dati estratti dai singoli elementi di
/// definizione.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.11.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVExtractFSMStates( SPV_PROCEDURE * procedure, XML_ELEMENT * element )
{
  int           ret_code    = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
  int           state_count = 0                           ; // Conteggio stati FSM
  int           position    = 0                           ; // Posizione nella lista
  XML_ELEMENT * current     = NULL                        ; // Elemento XML corrente
  char        * value       = NULL                        ; // Stringa di appoggio
  int           seq_parent  = 0                           ; // Codice parent
  int           seq_order   = 0                           ; // Ordine di esecuzione

  if ( procedure != NULL ) // Controllo la procedura
  {
    if ( element != NULL ) // Controllo l'elemento
    {
      state_count = XMLGetChildCount( element, true );
      if ( state_count > 0 ) // Se c'� almento uno stato
      {
        procedure->StateCount = state_count; // Salvo il numero di stati
        ret_code = SPVNewFSMStates( procedure );
		if ( ret_code == 0 ) // Allocazione andata a buon fine
        {
          if ( procedure->StateList ) // Controllo la lista degli stati FSM
          {
            current = element->child; // Comincio dalla prima posizione
            seq_parent = 0; // Il parent � lo stato di partenza
            seq_order  = 0; // Resetto l'ordine di esecuzione
            while ( current != NULL ) // Ciclo sugli stati
            {
              value = XMLGetName( current ); // Leggo il nome dell'elemento XML
			  if ( strcmpi( value, "state" ) == 0 ) // Controllo il nome dell'elemento
              {
                ret_code = SPVExtractFSMState( &procedure->StateList[position], current );
                if ( ret_code == 0 )
                {
                  procedure->StateList[position].SeqParent  = (unsigned char)seq_parent; // Assegno il codice parent
                  procedure->StateList[position].SeqOrder   = (unsigned char)seq_order ; // Assegno l'ordine di esecuzione
                  position++;
                }
              }
              else
              {
                ret_code = XML_INT_ERROR_INVALID_NAME;
              }
              // --- Algoritmo per passare all'elemento successivo ---
              bool searching;
              if ( current->child != NULL )
              {
                seq_parent  = procedure->StateList[position-1].ID;
                seq_order   = 0;
				current     = current->child;
              }
              else
              {
                if ( current->next != NULL )
                {
                  current = current->next;
                  seq_order++;
                }
                else
                {
                  searching = true;
                  seq_order = 0; // Resetto l'ordine di esecuzione
                  while ( searching ) // Torno al livello superiore utile
                  {
					if ( current->parent == element )
                    {
                      current = NULL; // Esco dal ciclo normalmente
                      searching = false;
                    }
                    else
                    {
                      current = current->parent;
                      seq_parent = SPVGetFSMParentState( current ); // Ottengo l'id del parent
                      if ( current->next != NULL )
                      {
                        current = current->next;
                        searching = false;
                      }
                    }
				  }
				}
			  }
			  // --- Fine algoritmo ---
			}
            if ( position != state_count ) // Controllo se il conteggio � esatto
            {
              ret_code = SPV_APPLIC_INVALID_COUNT;
            }
          }
          else // Se la lista di stati non � valida
          {
            ret_code = SPV_APPLIC_INVALID_FSM_STATE;
          }
        }
        else // Se l'allocazione � fallita
        {
          ret_code = SPV_APPLIC_ALLOCATION_FAILURE;
          procedure->StateList = NULL;
        }
	  }
      else // La definizione non contiene stati
      {
        ret_code = SPV_APPLIC_INVALID_DEFINITION;
      }
    }
    else // L'elemento non � valido
    {
      ret_code = XML_ERROR_INVALID_ELEMENT;
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una lista di stati per FSM
///
/// \date [30.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeFSMStateList( SPV_PROCEDURE * procedure )
{
	int             ret_code  = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	SPV_FSM_STATE * state     = NULL; // Ptr a struttura stato FSM

	if ( procedure != NULL )
	{
		// --- Ciclo sugli stati e cancello i parametri ---
		for ( int s = 0; s < procedure->StateCount; s++ )
    	{
			state = &procedure->StateList[s];
			if ( state != NULL )
			{
				ret_code = SPVFreeFSMState( state );
			}
		}
		// --- Azzero la lista degli stati ---
		memset( procedure->StateList, 0, sizeof(SPV_FSM_STATE) * procedure->StateCount );
		// --- Elimino l'intera lista degli stati ---
		try
		{
			free( procedure->StateList );
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_CRITICAL_ERROR;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_PROCEDURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per impostare valori di default a stati FSM.
/// Allova la memoria per una nuova struttura di stato per FSM (Finite State
/// Machine) e riempie il contenuto della stessa con i valori acquisiti dall'-
/// elemento di definizione XML <element> e imposta tutti gli altri valori
/// runtime secondo i valori di default.
///
/// \date [19.04.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVSetFSMStateDefault( SPV_FSM_STATE * state )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;

	if ( state != NULL ) // Controllo l'esistenza dello stato FSM
	{
		state->ID         = 0     ;
		state->Code       = NULL  ;
		state->Name       = NULL  ;
		state->Def        = NULL  ;

		state->SeqParent  = 0     ;
		state->SeqReturn  = 0     ;
		state->SeqOrder   = 0     ;

		state->DataIn     = NULL  ;
		state->DataOut    = NULL  ;
		state->Function   = NULL  ;

		state->Active     = false ;
		state->Waiting    = false ;
		state->Success    = false ;
		state->Failed     = false ;
		state->TimeStart  = 0     ;
		state->TimeFinish = 0     ;
		state->RetCode    = 0     ;
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_FSM_STATE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere l'id dello stato parent.
/// Dato un elemento XML valido di definizione di uno stato FSM <element>, viene
/// estratto l'id del suo elemento parent.
/// Restituisce l'id (codice numerico) dello stato parent.
///
/// \date [19.04.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetFSMParentState(XML_ELEMENT * element)
{
	int parent_id = 0;

	if (element) // Controllo l'elemento XML
	{
		if (element->parent) // Controllo il parent
		{
			if (strcmpi(XMLGetName(element->parent),"state")==0)
			{
				parent_id = XMLGetID(element->parent);
			}
		}
	}

	return parent_id;
}

//==============================================================================
/// Funzione per inizializzare il thread di controllo procedure.
///
/// \date [03.10.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVProcedureCheckInit( void )
{
	int ret_code = SPV_APPLIC_NO_ERROR;
	int fun_code = SPV_APPLIC_NO_ERROR;

	try
	{
	        //SPVReportApplicEvent( NULL, 0, 525, "Caricamento del thread di controllo...", "SPVProcedureCheckInit()" );

		// --- Carico il thread di controllo ---
		SPVProcedureCheckThread = SYSNewThread( SPVProcedureCheckFunction, 0, CREATE_SUSPENDED ); // Alloco un nuovo thread

       	        //SPVReportApplicEvent( NULL, 0, 525, "Thread SPVProcedureCheckFunction Creato", "SPVProcedureCheckInit()" );

		fun_code = SYSSetThreadParameter( SPVProcedureCheckThread, (void*)SPVProcedureCheckThread ); // Gli passo come parametro la struttura scheduler

                //SPVReportApplicEvent( NULL, 0, 525, "Funzione SPVProcedureCheckThread associata al thread", "SPVProcedureCheckInit()" );

		if ( fun_code == SPV_APPLIC_NO_ERROR )
		{
			fun_code = SYSCreateThread( SPVProcedureCheckThread ); // Istanzio il nuovo thread

              	        //SPVReportApplicEvent( NULL, 0, 525, "Thread istanziato", "SPVProcedureCheckInit()" );

			if ( fun_code == SPV_APPLIC_NO_ERROR )
			{
				fun_code = SYSResumeThread( SPVProcedureCheckThread ); // Faccio partire il thread

                                //SPVReportApplicEvent( NULL, 0, 525, "Resume Thread eseguito", "SPVProcedureCheckInit()" );
			}
			else
			{

			}
		}
		else
		{

		}
	}
	catch(...)
	{
		SPVReportApplicEvent( NULL, 2, 525, "Eccezione generale", "SPVProcedureCheckInit()" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere il thread di controllo procedure.
///
/// \date [24.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVProcedureCheckClear( void )
{
	int ret_code = SPV_APPLIC_NO_ERROR;

	try
	{
		SPVProcedureCheckThread->Terminated = true;

		if ( SYSWaitForThread( SPVProcedureCheckThread ) == 0 ) // Se il thread � terminato
		{
			SYSDeleteThread( SPVProcedureCheckThread );
		}

	}
	catch(...)
	{
		SPVReportDBEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante terminazione e cancellazione thread", "SPVProcedureCheckClear" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per controllare una procedura
///
/// \date [12.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVProcedureCheck( SPV_PROCEDURE * procedure )
{
	int   ret_code    = SPV_APPLIC_NO_ERROR;
	long  start_time  = 0;
	long  this_time   = 0;
	long  delta_time  = 0;

	try
	{
		if ( procedure != NULL )
		{
			try
			{
				if ( procedure->Active )
				{
					//SPVReportApplicEvent( procedure, 1, 590, "Procedura attiva", "SPVProcedureCheck()" );
					try
					{
						this_time = (long) time( NULL );
						start_time = procedure->StartDT;
						delta_time = this_time - start_time;
						if ( delta_time > (60 * 30) )
						{
							SPVReportApplicEvent( procedure, 2, 599, "Procedura bloccata. Tentativo di terminazione", "SPVProcedureCheck()" );
							try
							{
								// --- Termino la procedura considerata bloccata ---
								SYSTerminateThread( procedure->Thread );
								procedure->Active = false;
							}
							catch(...)
							{
								SPVReportApplicEvent( procedure, 2, 603, "Eccezione L4 - Terminazione della procedura", "SPVProcedureCheck()" );
							}
						}
					}
					catch(...)
					{
						SPVReportApplicEvent( procedure, 2, 602, "Eccezione L3 - Controllo della procedura", "SPVProcedureCheck()" );
					}
				}
				else
				{
					//SPVReportApplicEvent( procedure, 1, 590, "Procedura non attiva", "SPVProcedureCheck()" );
				}
			}
			catch(...)
			{
				SPVReportApplicEvent( NULL, 2, 601, "Eccezione L2 - accesso alla procedura", "SPVProcedureCheck()" );
			}
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_PROCEDURE;
		}
	}
	catch(...)
	{
		SPVReportApplicEvent( NULL, 2, 600, "Eccezione L1", "SPVProcedureCheck()" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione di attivazione per thread Controllo Procedure.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.09
//------------------------------------------------------------------------------
unsigned long __stdcall SPVProcedureCheckFunction( void * parameter )
{
	unsigned long		ret_code    	= SPV_APPLIC_NO_ERROR	; // Codice di ritorno
	int                 fun_code    	= SPV_APPLIC_NO_ERROR   ;
	SYS_THREAD *		thread      	= NULL					; // Puntatore al thread
	int					delay			= 30000					;
	char * 				thread_name 	= NULL					;

	try
	{
		if ( parameter != NULL ) // Controllo il parametro passato
		{
			thread = (SYS_THREAD*) parameter; // Recupero il thread
			if ( SYSValidThread( thread ) )  // Controllo il thread
			{
				// assegno il nome al thread
				try
				{
					thread_name = ULAddText(thread_name,"ProcedureCheck");
				}
				catch(...)
				{
					fun_code = SPV_APPLIC_FUNCTION_EXCEPTION;
				}

				if (fun_code == SPV_APPLIC_NO_ERROR)
				{
					fun_code = SYSSetThreadName(thread,thread_name);
				}
				/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */

				Sleep( SPV_APPLIC_INITIAL_DELAY_AUTO_CHECH );
				while ( !thread->Terminated ) // Ciclo del thread
				{
					try
					{
						// --- Accedo ai dati procedure ---
						SPVEnterProcedureData("SPVProcedureCheckFunction");
						try
						{
							if (SYSCheckThreads())
							{
								// --- Aggiorno LastAutoCheck ---
                                                        	fun_code = SPVUpdateLastAutoCheck( );
								if ( fun_code != 0 ) SPVReportApplicEvent( NULL, 1, fun_code, "UpdateLastAutoCheck ERROR", "SPVProcedureCheckFunction()" );
							}
							else
							{
								SPVReportApplicEvent( NULL, 1, SPV_APPLIC_THREAD_IS_LOCKED, "Slow thread(s) found.", "SPVProcedureCheckFunction()" );
							}
						}
						catch(...)
						{
							SPVReportApplicEvent( NULL, 2, 602, "Eccezione L3 - Aggiornamento LastAutoCheck", "SPVProcedureCheckFunction()" );
						}
						// --- Esco dai dati procedure ---
						SPVLeaveProcedureData();
						delay = SPV_APPLIC_DELAY_AUTO_CHECK;
					}
					catch (...)
					{
						SPVReportApplicEvent( NULL, 2, 601, "Eccezione L2 - thread controllo delle procedure", "SPVProcedureCheckFunction()" );
					}
					Sleep( delay );

					// Aggiornamento delle informazioni di debug del thread
					SYSUpdateThreadDebugInfo(thread);
				}
			}
		}
	}
	catch (...)
	{
		SPVReportApplicEvent( NULL, 2, 600, "Eccezione L1", "SPVProcedureCheckFunction()" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione di attivazione per thread di FSM.
///
/// \date [19.02.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
unsigned long __stdcall SPVProcedureFSM( void * parameter )
{
	unsigned long			ret_code   	= SPV_APPLIC_NO_ERROR	; // Codice di ritorno
	int                 	fun_code   	= SPV_APPLIC_NO_ERROR   ;
	SPV_PROCEDURE         * procedure   = NULL        			; // Puntatore alla procedura
	SYS_THREAD            * thread      = NULL        			; // Puntatore al thread
	SPV_FSM_STATE         * state       = NULL        			; // Puntatore allo stato
	SPV_FSM_FUNCTION_PTR    ptf         = NULL        			; // Puntatore a funzione
	SPV_FSM_DATA            data_in     = {NULL,NULL} 			; // Struttura dati in ingresso
	SPV_FSM_DATA            data_out    = {NULL,NULL} 			; // Struttura dati in uscita
	int						state_ret	= 0						;
	char          		  * thread_name = NULL  				; // Stringa del nome del thread

	try
	{
		if ( parameter != NULL ) // Controllo il parametro passato
		{
			procedure = (SPV_PROCEDURE*) parameter; // Recupero la procedura
			if ( procedure != NULL ) // Se la procedura � valida
			{
				//* DEBUG */ SPVReportApplicEvent( procedure, 1, 2514, "Procedure Start", "SPVProcedureFSM()" );
				// --- Aggiorno sul DB le info della procedura ---
				SPVDBUpdateProcedure( &SPVSystem, procedure );
				data_in.Data = (void *) procedure; // Salvo il ptr alla struttura <procedure>
				// --- Recupero il thread della Procedura ---
				thread = procedure->Thread;
				//thread = SPVGetProcedureThread( procedure );
				if ( SYSValidThread( thread ) )  // Controllo il thread
				{
					// assegno il nome al thread
					try
					{
						if (procedure->Device != NULL)
						{
							thread_name = ULAddText(thread_name,procedure->Device->Name);

						}
						else
						{
							thread_name = ULAddText(thread_name,"Periferica sconosciuta");
						}
						thread_name = ULAddText(thread_name," - ");
						thread_name = ULAddText(thread_name,procedure->Name);
						thread_name = ULAddText(thread_name," - ProcedureFSM");
					}
					catch(...)
					{
						fun_code = SPV_APPLIC_FUNCTION_EXCEPTION;
					}

					if (fun_code == SPV_APPLIC_NO_ERROR)
					{
						fun_code = SYSSetThreadName(thread,thread_name);
					}
					/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */
					thread->StepID = 0;

					while ( !thread->Terminated ) // Ciclo del thread FSM
					{
						try
						{
							if ( procedure->Active == true )
							//if ( SPVProcedureIsActive( procedure ) )
							{
								if ( procedure->State != NULL )
								//if ( SPVProcedureHasState( procedure ) )
								{
									SPVEnterProcedureData("SPVProcedureFSM - 1");
									state             	= procedure->State; // Recupero il ptr allo stato FSM
									state->DataIn   	= (void*) &data_in  ; // Setto il ptr al dato in ingresso
									state->DataOut  	= (void*) &data_out ; // Setto il ptr al dato in uscita
									state->Active     	= true; // Setto il flag di stato FSM attivo
									state->Waiting    	= false; // Resetto il flag di attesa stato FSM
									state->TimeStart  	= time( &state->TimeStart );
									ptf               	= state->Function->Ptf;
									thread->StepID		= (unsigned long)state->ID;
									SPVLeaveProcedureData( );
									/* Eseguo lo stato FSM */
									try
									{
										state_ret    		= ptf( state->DataIn, state->DataOut );
									}
									catch(...)
									{
										state_ret			= SPV_APPLICLIB_STATE_ERROR;
										SPVReportApplicEvent( procedure, 2, 603, thread_name, "SPVProcedureFSM()" );
									}
									SPVEnterProcedureData("SPVProcedureFSM - 2");
									state->RetCode		= state_ret;
									state->TimeFinish 	= time( &state->TimeFinish );
									/* L'output dello stato FSM diventa l'input per lo stato successivo */
									data_in.Data      	= data_out.Data;
									data_in.Next      	= data_out.Next;
									data_out.Data     	= NULL;
									data_out.Next     	= NULL;
									SPVLeaveProcedureData( );
									/* Recupero lo stato FSM successivo */
									SPVGetNextFSMState( procedure );
								}
								else // Nessuno 'state'
								{
									SPVEnterProcedureData("SPVProcedureFSM - 3");
									if ( procedure->Device->SupStatus.Level != 255
									  && procedure->LastResult == 0
									  && procedure->RetryCount < 2 ) // MAX 2 tentativi
									{
										///Sleep( 1000 );
										SPVRetryProcedure( procedure );
									}
									else
									{
										// --- Salvo la data/ora di fine esecuzione ---
										procedure->FinishDT = time( NULL );
										procedure->Active = false;
										SPVGetNextFSMState( procedure );
									}
									SPVLeaveProcedureData( );
								}
							}
							else
							{
								SPVEnterProcedureData("SPVProcedureFSM - 4");
								if ( procedure->Device->SupStatus.Level != 255
								  && procedure->LastResult == 0
								  && procedure->RetryCount < 2 ) // MAX 2 tentativi
								{
									///Sleep( 1000 );
									SPVRetryProcedure( procedure );
								}
								else
								{
									// --- Salvo la data/ora di fine esecuzione ---
									procedure->FinishDT = time( NULL );
									//SYSExitThread(thread);
									thread->Terminated = true;
								}
								SPVLeaveProcedureData( );
							}
						}
						catch (...)
						{
							SPVReportApplicEvent( procedure, 2, 602, "Eccezione L3 - Esecuzione stato FSM", "SPVProcedureFSM()" );
							/* TODO -oEnrico -cError : Gestione dell'errore critico */
						}
						Sleep(20); //--- tempo di ritardo di sicurezza per non occupare costantemente la CPU
						// Aggiornamento delle informazioni di debug del thread
						SYSUpdateThreadDebugInfo(thread);
					}
					try
					{
						// --- Elimino tutta la memoria dati allocata per la procedura ---
						if ( data_in.Data  != NULL && data_in.Data != parameter ) free(data_in.Data);
						if ( data_out.Data != NULL ) free(data_out.Data);
						SPVDeleteAllFSMData( data_in.Next );
						SPVDeleteAllFSMData( data_out.Next );
					}
					catch (...)
					{
						SPVReportApplicEvent( procedure, 2, 601, "Eccezione L2 - Disallocazione dati FSM", "SPVProcedureFSM()" );
						/* TODO -oEnrico -cError : Gestione dell'errore critico */
					}
				}
				// --- Decremento il contatore delle esecuzioni della procedura ---
				SPVDecProcedureExeCount( procedure );
				// --- Aggiorno sul DB le info della procedura ---
				SPVDBUpdateProcedure( &SPVSystem, procedure );
				//* DEBUG */ SPVReportApplicEvent( procedure, 1, 2590, "Procedure Stop", "SPVProcedureFSM()" );
			}
		}
	}
	catch (...)
	{
		SPVReportApplicEvent( procedure, 2, 600, "Eccezione L1 - generica FSM", "SPVProcedureFSM()" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare la validit� di un puntatore ad una struttura
/// procedura.
///
/// \date [09.05.2008]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
bool SPVValidProcedure( SPV_PROCEDURE * procedure )
{
    bool valid = false;

	if ( procedure != NULL )
	{
		try
		{
			if ( procedure->VCC == SPV_APPLIC_PROCEDURE_VALIDITY_CHECK_CODE )
			{
				valid = true;
			}
		}
		catch(...)
		{
			valid = false;
		}
	}

	return valid;
}

//==============================================================================
/// Funzione per creare struttura di procedure.
/// Dato l'elemento di root della definizione di una procedura crea una nuova
/// definizione di procedura
///
/// \date [09.05.2008]
/// \author Enrico Alborali
/// \version 0.10
//------------------------------------------------------------------------------
int SPVNewProcedure( SPV_PROCEDURE * procedure, XML_ELEMENT * element )
{
	int                   ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	char                * value       = NULL;
	char                * class_code  = NULL; // Stringa del codice della classe di procedure
	SPV_PROCEDURE_CLASS   class_item  = SPV_APPLIC_NULL_PROC_CLASS;

	if ( element != NULL ) // Controllo se l'elemento esiste
	{
		if ( procedure != NULL ) // Controllo la struttura procedura (deve essere gi� allocata)
		{
			procedure->VCC = SPV_APPLIC_PROCEDURE_VALIDITY_CHECK_CODE;
			// --- Carico le informazioni generali ---
			value = XMLExtractValue( element, "ID" );
			if ( value != NULL )
			{
				procedure->ID = cnv_CharPToUInt16( value );
				free( value );
			}
			procedure->Name = XMLExtractValue( element, "name" );
			// --- Aggiungo un'eventuale nuova classe di procedure ---
			class_item = SPVLoadProcedureClass( element );
			if ( !SPVNullProcedureClass( class_item ) ) // Se � stata definita una nuova classe
			{
				SPVAddNewProcedureClass( class_item );
			}
			// --- Carico un'eventuale ID di classe procedure ---
			class_code = XMLGetValue( element, "class" );
			if ( class_code != NULL )
			{
				procedure->ClassID = SPVGetProcedureClassPos( class_code );
			}
			else
			{
				procedure->ClassID = -1; // La procedura non appartiene a nessuna classe
			}
			// --- Inizializzo stato e data/ora di inizio/fine ultima esecuzione ---
			procedure->LastResult 	= 255;
			procedure->RetryCount	= 0;
			procedure->StartDT 		= 0;
			procedure->FinishDT 	= 0;
			// --- Estraggo gli stati della FSM ---
			ret_code = SPVExtractFSMStates( procedure, element );
			// --- Inizializzo il ptr al thread della procedura ---
			procedure->Thread = NULL; // Il thread verr� creato prima di essere eseguito
			// --- Inizializzo il contatore delle esecuzioni ---
			procedure->ExeCount = 0;
			procedure->ECChanged = false;
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_DESTINATION;
		}
	}
	else
	{
    	ret_code = SPV_APPLIC_INVALID_DEFINITION;
    }

    return ret_code;
}

//==============================================================================
/// Funzione per verificare se una classe di procedura � nulla
///
/// Se la classe ha i campi tutti nulli restituisce 'true', altrimenti 'false'.
///
/// \date [20.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVNullProcedureClass( SPV_PROCEDURE_CLASS class_item )
{
  bool class_null = false;

  if ( class_item.ID        == 0
    && class_item.Name      == NULL
    && class_item.Code      == NULL
    && class_item.Schedule  == false
    && class_item.Blocking  == false
    && class_item.Tag       == NULL )
  {
    class_null = true;
  }

  return class_null;
}

//==============================================================================
/// Funzione per ottenere la prima posizione libera nella lista delle classi di procedure
///
/// In caso di errore restituisce -1.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetFirstFreeClassPos( void )
{
  int                   class_pos   = -1  ;
  SPV_PROCEDURE_CLASS * class_list  = NULL;
  SPV_PROCEDURE_CLASS * class_item  = NULL;
  int                   class_count = 0   ;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVProcedureClassCriticalSection );
  SPVEnterClassData("SPVGetFirstFreeClassPos");

  class_list  = SPVProcedureClassList;
  class_count = SPVProcedureClassCount;

  if ( class_list != NULL )
  {
	if ( class_count < SPV_APPLIC_MAX_PROC_CLASS )
	{
	  for ( int c = 0; c < SPV_APPLIC_MAX_PROC_CLASS; c++ )
	  {
		class_item = &class_list[c];
		if ( class_item != NULL )
		{
		  if ( SPVNullProcedureClass( *class_item ) )
		  {
			class_pos = c;
			c = SPV_APPLIC_MAX_PROC_CLASS; // Fine del ciclo for
		  }
		}
	  }
	}
  }

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVProcedureClassCriticalSection );
  SPVLeaveClassData( );

  return class_pos;
}

//==============================================================================
/// Funzione per ottenere la posizione di una classe in base al suo codice
///
/// In caso di errore restituisce -1.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVGetProcedureClassPos( char * code )
{
  int                   class_pos     = -1;
  SPV_PROCEDURE_CLASS * class_list  = NULL;
  SPV_PROCEDURE_CLASS * class_item  = NULL;
  /* DEPRECATED
  int                   class_count = 0   ;
  */

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVProcedureClassCriticalSection );
  SPVEnterClassData("SPVGetProcedureClassPos");

  class_list  = SPVProcedureClassList;
  //class_count = SPVProcedureClassCount;

  if ( code != NULL )
  {
    for ( int c = 0; c < SPV_APPLIC_MAX_PROC_CLASS; c++ )
    {
      class_item = &class_list[c];
      if ( class_item != NULL )
      {
        if ( class_item->Code != NULL )
        {
          if ( strcmpi( class_item->Code, code ) == 0 )
          {
            class_pos = c;
            c = SPV_APPLIC_MAX_PROC_CLASS; // Fine del ciclo for
          }
        }
      }
    }
  }

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVProcedureClassCriticalSection );
  SPVLeaveClassData( );

  return class_pos;
}

//==============================================================================
/// Funzione per caricare i dati di una nuova classe di procedure
///
/// In caso di errore restituisce una classe nulla.
///
/// \date [20.01.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_PROCEDURE_CLASS SPVLoadProcedureClass( XML_ELEMENT * element )
{
  SPV_PROCEDURE_CLASS   class_item = SPV_APPLIC_NULL_PROC_CLASS;
  char                * class_name = NULL;

  if ( element != NULL )
  {
    class_name = XMLExtractValue( element, "class" );
    if ( class_name != NULL )
    {
      // --- Se la classe esiste gi� ---
      if ( SPVGetProcedureClassPos( class_name ) != -1 )
      {
        // --- Restituisco una struttura nulla ---
        free( class_name ); // -FREE
      }
      // --- Se la classe non esiste ---
      else
      {
        // --- Restituisco una struttura valida ---
        class_item.ID       = 0;
		class_item.Name     = NULL;
        class_item.Code     = class_name;
        class_item.Schedule = false;
        class_item.Blocking = false;
        class_item.Tag      = NULL;
      }
    }
  }

  return class_item;
}

//==============================================================================
/// Funzione per aggiungere alla lista una nuova classe di procedure
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVAddNewProcedureClass( SPV_PROCEDURE_CLASS class_item )
{
  int                   ret_code    = SPV_APPLIC_FUNCTION_SUCCESS   ; // Codice di ritorno
  SPV_PROCEDURE_CLASS * class_list  = NULL;
  int                   class_count = 0   ;
  int                   class_pos   = -1  ;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVProcedureClassCriticalSection );
  SPVEnterClassData("SPVAddNewProcedureClass");

  // --- Recupero le variabili globali per la lista classi procedure ---
  class_list  = SPVProcedureClassList;
  class_count = SPVProcedureClassCount;

  if ( !SPVNullProcedureClass( class_item ) )
  {
	class_pos = SPVGetFirstFreeClassPos( );
	if ( class_pos != -1 )
	{
	  try
	  {
		class_list[class_pos] = class_item;
	  }
	  catch(...)
	  {
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
		ret_code = SPV_APPLIC_CRITICAL_ERROR;
	  }
	  if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
	  {
		class_count++;
	  }
	}
	else
	{
	  ret_code = SPV_APPLIC_PROC_CLASS_LIST_FULL;
	}
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_PROC_CLASS;
  }

  // --- Salvo i valori delle variabili globali ---
  SPVProcedureClassCount = class_count;

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVProcedureClassCriticalSection );
  SPVLeaveClassData( );

  return ret_code;
}

//==============================================================================
/// Funzione per cancellare tutta la lista delle cassi di procedure
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVClearProcedureClasses( void )
{
  int                   ret_code    = SPV_APPLIC_FUNCTION_SUCCESS   ; // Codice di ritorno
  SPV_PROCEDURE_CLASS * class_list  = NULL;
  int                   class_count = 0   ;
  SPV_PROCEDURE_CLASS   class_item        ;

  // --- Accedo alle classi di procedure ---
  SPVEnterClassData("SPVClearProcedureClasses");

  // --- Recupero le variabili globali per la lista classi procedure ---
  class_list  = SPVProcedureClassList;
  class_count = SPVProcedureClassCount;

  // --- Ciclo sulla lista delle classi di procedure e cancello i parametri allocati ---
  for ( int c = 0; c < class_count; c++ )
  {
    class_item = class_list[c];
    if ( !SPVNullProcedureClass( class_item ) )
    {
      if ( class_item.Name != NULL )
      {
        try
        {
		  free( class_item.Name );
        }
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
          ret_code = SPV_APPLIC_CRITICAL_ERROR;
        }
      }
      if ( class_item.Code != NULL )
      {
        try
        {
          free( class_item.Code );
        }
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
          ret_code = SPV_APPLIC_CRITICAL_ERROR;
        }
      }
    }
  }

  // --- Ripulisco la memoria occupata dalla lista ---
  memset( class_list, 0, SPV_APPLIC_MAX_PROC_CLASS );

  // --- Salvo i valori delle variabili globali ---
  SPVProcedureClassCount = 0;

  // --- Lascio le classi di procedure ---
  SPVLeaveClassData( );

  return ret_code;
}

//==============================================================================
/// Funzione per accedere ai dati della lista delle classi di procedure
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVEnterClassData( char * label )
{
	//EnterCriticalSection( &SPVProcedureClassCriticalSection );
	SYSLock(&SPVProcedureClassLock,label);
}

//==============================================================================
/// Funzione per rilasciare i dati della lista delle classi di procedure
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVLeaveClassData( void )
{
	//LeaveCriticalSection( &SPVProcedureClassCriticalSection );
	SYSUnLock(&SPVProcedureClassLock);

}

//==============================================================================
/// Funzione per ottenere il ptr alla lista delle classi di procedure
///
/// \date [23.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_PROCEDURE_CLASS * SPVGetClassList( void )
{
  return SPVProcedureClassList;
}

//==============================================================================
/// Funzione per ottenre il numero di calssi di procedure nella lista
///
/// \date [23.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetClassCount( void )
{
  return SPVProcedureClassCount;
}

//==============================================================================
/// Funzione per accedere ai dati della lista delle procedure
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVEnterProcedureData( char *label )
{
	SYSLock(&SPVProcedureLock,label);
	//EnterCriticalSection( &SPVProcedureCriticalSection );
}

//==============================================================================
/// Funzione per rilasciare i dati della lista delle procedure
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVLeaveProcedureData( void )
{
	//LeaveCriticalSection( &SPVProcedureCriticalSection );
	SYSUnLock(&SPVProcedureLock);

}

//==============================================================================
/// Funzione per ottenere il ptr alla lista delle procedure
///
/// \date [23.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_PROCEDURE * SPVGetProcedureList( void )
{
  return SPVProcedureList;
}

//==============================================================================
/// Funzione per ottenre il numero di procedure nella lista
///
/// \date [23.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetProcedureCount( void )
{
  return SPVProcedureCount;
}

//==============================================================================
/// Funzione per cercare una procedura nella lista in base a DevID e ProID
///
/// \date [24.08.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_PROCEDURE * SPVSearchProcedure( unsigned __int64 DevID, int ProID )
{
  SPV_PROCEDURE * procedure = NULL;

  for ( int p = 0; p < SPVProcedureCount; p++ )
  {
    procedure = &SPVProcedureList[p];
    if ( procedure != NULL )
    {
      try
      {
        if ( SPVValidDevice( procedure->Device ) )
        {
          if ( procedure->Device->DevID == DevID
            && procedure->ID == ProID )
		  {
            break; // Ho trovato la procedura giusta
          }
          else
          {
            procedure = NULL;
          }
        }
        else
        {
          procedure = NULL;
        }
      }
      catch(...)
      {
        procedure = NULL;
      }
	}
  }

  return procedure;
}

//==============================================================================
/// Funzione per eliminare il thread di una procedura
/// NOTA: Vengono eliminati solo i thread terminati
///
/// \date [09.06.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVDeleteProcedureThread( SPV_PROCEDURE * procedure )
{
	int				ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
	SYS_THREAD *	thread   = NULL ; // Ptr a struttura thread della procedura
	long			this_time = 0;
	int 			fun_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( procedure != NULL ) // Controllo la procedura
	{
		thread = procedure->Thread;
		if ( SYSValidThread( thread ) ) // Controllo se c'� un thread
		{
			// --- Se il thread � gi� stato eseguito e terminato ---
			if ( thread->Terminated )
			{
				// --- Aspetto la fine del thread ---
				//if ( SYSWaitForThread( thread, 30000 ) == 0 )
                                if ( SYSWaitForThread( thread ) == 0 )
				{
					// --- Elimino il thread terminato ---
					fun_code = SYSDeleteThread( thread );
					if (fun_code == SPV_APPLIC_FUNCTION_SUCCESS)
					{
						procedure->Thread = NULL;
					}
					else
					{
						SPVReportApplicEvent( procedure, 2, SPV_APPLIC_THREAD_DELETE_FAILURE, "Fallita cancellazione thread della procedura", "SPVDeleteProcedureThread()" );
					}
				}
				else
				{
					/* DEBUG
					SPVReportApplicEvent( procedure, 0, SPV_APPLIC_THREAD_WAIT_FOR_TIMEOUT, "Timeout attesa fine thread della procedura", "SPVDeleteProcedureThread()" );
					//*/
					// --- Elimino il thread terminato ---
					fun_code = SYSDeleteThread( thread );
					if (fun_code == SPV_APPLIC_FUNCTION_SUCCESS)
					{
						procedure->Thread = NULL;
					}
					else
					{
						SPVReportApplicEvent( procedure, 2, SPV_APPLIC_THREAD_DELETE_FAILURE, "Fallita cancellazione thread della procedura", "SPVDeleteProcedureThread()" );
					}
				}
			}
			else
			{
				ret_code = SPV_APPLIC_EXECUTING_PROCEDURE;
			}
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_THREAD;
		}
	}
	else // Procedura non valida
	{
		ret_code = SPV_APPLIC_INVALID_PROCEDURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per lanciare procedure.
/// Dato il puntatore <procedure> alla struttura della procedura da lanciare.
/// Vengono impostati i valori iniziali e viene ripristinato il suo thread (che
/// era stato istanziato con attributo CREATE_SUSPENDED).
/// In caso di errore restituisce un valore non nullo.
///
/// \date [15.05.2008]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVExecProcedure( SPV_PROCEDURE * procedure )
{
	int           ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
	SYS_THREAD  * thread   = NULL ; // Ptr a struttura thread della procedura

	if ( procedure != NULL ) // Controllo la procedura
	{
		if (procedure->Thread == NULL)
		{
			// --- Creo un nuovo thread per la procedura ---
			thread = SYSNewThread( SPVProcedureFSM, 0, CREATE_SUSPENDED ); // Istanzio un nuovo thread per la procedura
			SYSSetThreadParameter( thread, (void*) procedure ); // Gli passo come parametro la procedura
			if ( thread != NULL )
			{
				ret_code = SYSCreateThread( thread );
			}
			procedure->Thread = thread;
			// --- Imposto il contatore tentativi ---
			procedure->RetryCount = 1;
			// --- Ultimo risultato: esecuzione primo tentativo ---
			procedure->LastResult = procedure->RetryCount;
			// --- Imposto lo stato iniziale della procedura ---
			procedure->StartDT  = (long) time( NULL ); // Salvo la data/ora di inizio esecuzione
			procedure->Waiting  = false ; // La procedura non � pi� in attesa
			procedure->Active   = true  ; // La procedura ora � attiva
			if ( procedure->StateList != NULL )
			{
				procedure->State    = &procedure->StateList[0]; // Parto dal primo
			}
			else
			{
				procedure->State    = NULL  ; // Stato di partenza nullo
			}

			SYSResumeThread( thread )     ; // Ripristino il thread
		}
		else
		{
			SPVReportApplicEvent( procedure, 0, SPV_APPLIC_EXISTING_THREAD_STRUCT, "La struttura del thread non e' stata cancellata", "SPVExecProcedure()" );
        	}
	}
	else // Procedura non valida
	{
		ret_code = SPV_APPLIC_INVALID_PROCEDURE;
	}

  return ret_code;
}

//==============================================================================
/// Funzione per ritentare una procedura
///
/// \date [03.08.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVRetryProcedure( SPV_PROCEDURE * procedure )
{
  int           ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  if ( procedure != NULL ) // Controllo la procedura
  {
	  procedure->Active   = true  ; // La procedura ora � attiva
	  if ( procedure->StateList != NULL )
	  {
		procedure->State    = &procedure->StateList[0]; // Parto dal primo
	  }
	  else
	  {
		procedure->State    = NULL  ; // Stato di partenza nullo
	  }
	  // --- Incremento il contatore tentativi ---
	  procedure->RetryCount++;
	  // --- Ultimo risultato: esecuzione n-esimo tentativo ---
	  procedure->LastResult = procedure->RetryCount;
  }
  else // Procedura non valida
  {
	ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per incrementare il numero di prossime esecuzioni di una procedura
///
/// \date [24.08.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVIncProcedureExeCount( SPV_PROCEDURE * procedure )
{
  int           ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  if ( procedure != NULL ) // Controllo la procedura
  {
    try
    {
      if ( procedure->ExeCount <= 253 )
	  {
        procedure->ExeCount++;
        procedure->ECChanged = true;
      }
    }
    catch(...)
    {
      ret_code = SPV_APPLIC_CRITICAL_ERROR;
    }
  }
  else // Procedura non valida
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per decrementare il numero di prossime esecuzioni di una procedura
///
/// \date [25.08.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDecProcedureExeCount( SPV_PROCEDURE * procedure )
{
  int           ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  if ( procedure != NULL ) // Controllo la procedura
  {
    try
    {
      if ( procedure->ExeCount > 0 && procedure->ExeCount < 255 )
      {
        procedure->ExeCount--;
		procedure->ECChanged = true;
      }
    }
    catch(...)
    {
      ret_code = SPV_APPLIC_CRITICAL_ERROR;
    }
  }
  else // Procedura non valida
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per settare la modalita' di esecuzione continua per una procedura
///
/// \date [24.08.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVSetProcedureContinueMode( SPV_PROCEDURE * procedure )
{
  int           ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  if ( procedure != NULL ) // Controllo la procedura
  {
    try
    {
      if ( procedure->ExeCount != 255 )
      {
        procedure->ExeCount = 255;
        procedure->ECChanged = true;
      }
	}
    catch(...)
    {
      ret_code = SPV_APPLIC_CRITICAL_ERROR;
    }
  }
  else // Procedura non valida
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per togliere la modalita' di esecuzione continua per una procedura
///
/// \date [24.08.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVClearProcedureExeCount( SPV_PROCEDURE * procedure )
{
  int           ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  if ( procedure != NULL ) // Controllo la procedura
  {
    try
    {
      if ( procedure->ExeCount != 0 )
      {
        procedure->ExeCount = 0;
        procedure->ECChanged = true;
      }
    }
    catch(...)
	{
      ret_code = SPV_APPLIC_CRITICAL_ERROR;
    }
  }
  else // Procedura non valida
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}
                                    
//==============================================================================
/// Funzione per caricare le procedure di un tipo periferica
///
/// Dato il puntatore <device> alla struttura di periferica, cerca e carica
/// tutte le definizioni di procedure contenute nella definizione XML.
/// In caso di errore restituise un valore non nullo.
///
/// \date [09.02.2009]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int SPVLoadProcedures( SPV_DEVICE * device )
{
	int             ret_code      = 0   ; // Codice di ritorno
	XML_ELEMENT   * def           = NULL; // Ptr a elemento di def. tipo periferica
	XML_ELEMENT   * e_type        = NULL; // Ptr a elemento type
	XML_ELEMENT   * e_procedures  = NULL; // Ptr a elemento delle procedure
	XML_ELEMENT   * e_item        = NULL; // Ptr a elemento di una procedura
	SPV_PROCEDURE * proc          = NULL; // Ptr a stuttura di una procedura
	int             proc_num      = 0   ; // Numero di procedure definite

	if ( device != NULL ) // Verifico la validit� del ptr a periferica
	{
		if ( device->Owned )
		{
			def = device->Def; // Recupero la definizione della periferica
		    e_type = XMLGetNext( def->child, "type", -1 );
		    e_procedures = XMLGetNext( e_type->child, "procedures", -1 );
		    if ( e_procedures != NULL ) // Verifico la validit� del ptr a procedure
		    {
		    	proc_num = XMLGetChildCount( e_procedures, false );
		    	if ( proc_num > 0 ) // Se sono definite delle procedure
		    	{
		    		e_item = e_procedures->child; // Prendo il primo 'item'
		    		if ( e_item != NULL ) // Controllo il ptr al primo 'item'
		    		{
		    			// --- Accedo ai dati procedure ---
		    			SPVEnterProcedureData("SPVLoadProcedures");
		    			try
		    			{
		    				for ( int p = 0; p < proc_num; p++ ) // Ciclo sulle definizioni di procedura
		    				{
		    					try
		    					{
		    						proc          = &SPVProcedureList[SPVProcedureCount]; // Prendo la prossima posizione libera
		    						ret_code      = SPVNewProcedure( proc, e_item ); // Nuova procedura
		    						proc->Device  = device; // Collego la periferica alla precedura
		    						SPVProcedureCount++; // Incremento il contatore delle procedure
		    						e_item        = e_item->next; // Passo al successivo 'item'
		    					}
		    					catch(...)
		    					{
		    						SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante il caricamento di una procedura", "SPVLoadProcedures()" );
		    					}
		    				}
		    			}
		    			catch(...)
		    			{
		    				SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante il caricamento di una procedura", "SPVLoadProcedures()" );
		    			}
		    			// --- Esco dai dati procedure ---
		    			SPVLeaveProcedureData( );
		    		}
		    		else
		    		{
		    			ret_code = SPV_APPLIC_INVALID_ITEM;
		    		}
		    	}
		    	else
		    	{
		    		ret_code = SPV_APPLIC_INVALID_DEFINITION;
		    	}
		    }
		    else
			{
		    	ret_code = SPV_APPLIC_INVALID_DEFINITION;
			}
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_DEVICE;
	}

    return ret_code;
}

//==============================================================================
/// Funzione per caricare le procedure del sistema.
///
/// \date [25.11.2009]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
int SPVLoadSystemProcedures( SPV_SYSTEM * system )
{
  int ret_code = 0;

  if ( system != NULL )
  {
	SPVDoOnDevices( system, SPVLoadProcedures );

	if ( SPVOwnedDeviceCount > 0 ) ret_code = SPVDBSaveProcedures( system );
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_SYSTEM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiamare la Stored Proedure di pulizia delle righe non pi�
/// usate nelle varie tabelle
///
/// \date [04.07.2012]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDeleteUnusedRows( SPV_SYSTEM * system )
{
	int ret_code = 0;

	if ( system != NULL )
	{
		ret_code = SPVDBDeleteUnusedRows( system );
		if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS ) {
			// --- Elimino la topografia rimossa ---
			ret_code = SPVDBDeleteRemovedTopography();
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per terminare e cancellare i thread delle procedure applicative
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVClearProcedureThreads( SPV_SYSTEM * system )
{
	int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	SPV_PROCEDURE         * procedure   = NULL;
	SPV_PROCEDURE         * proc_list   = NULL;
	int                     proc_count  = 0   ;

	if ( system != NULL )
	{
		// --- Entro nei dati procedure ---
		SPVEnterProcedureData("SPVClearProcedureThreads");

		try
		{
			// --- Recupero le variabili globali ---
			proc_list   = SPVProcedureList;
			proc_count  = SPVProcedureCount;

			// --- Scorro la lista delle procedure ---
			for ( int p = 0; p < proc_count; p++ )
			{
				try
				{
					procedure = &proc_list[p];
					if ( procedure != NULL )
					{
						if (SYSValidThread(procedure->Thread))
						{
							procedure->Thread->Terminated = true;
							// --- Elimino un eventuale thread terminato ---
							SPVDeleteProcedureThread( procedure );
						}
					}
				}
				catch(...)
				{
					ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
					SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante la terminazione e cancellazione di un thread di una procedura", "SPVClearProcedureThreads()" );
					break;
				}
			}
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione generale", "SPVClearProcedureThreads()" );
		}
		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare le procedure del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVFreeSystemProcedures( SPV_SYSTEM * system )
{
	int             ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	int             proc_count  = 0   ;
	SPV_PROCEDURE * proc_list   = NULL;
	SPV_PROCEDURE * proc        = NULL;

	if ( system != NULL )
	{
		// --- Accedo ai dati procedure ---
		SPVEnterProcedureData("SPVFreeSystemProcedures");

		try
		{
			// --- Recupero le variabili globali ---
			proc_list   = SPVProcedureList;
			proc_count  = SPVProcedureCount;
			// --- Scorro la lista delle procedure ---
			for ( int p = 0; p < proc_count; p++ ) // Ciclo sulle definizioni di procedura
			{
				try
				{
					// --- Recuparo la procedura ---
					proc          = &SPVProcedureList[p]; // Prendo la prossima posizione libera
					if ( proc != NULL )
					{
						// --- Elimino gli stati FSM della procedura ---
						ret_code = SPVFreeFSMStateList( proc );
						// --- Elimino il nome della procedura ---
						if ( proc->Name != NULL )
						{
							try
							{
								free( proc->Name );
							}
							catch(...)
							{
								SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione di un nome procedura", "SPVFreeSystemProcedures()" );
							}
						}
					}
					else
					{
						SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Procedura non valida", "SPVFreeSystemProcedures()" );
					}
				}
				catch(...)
				{
					SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione stati di una procedura", "SPVFreeSystemProcedures()" );
				}
			}
			// --- Azzero la memoria occupata dalla lista globale procedure ---
			memset( proc_list, 0, sizeof(SPV_PROCEDURE) * SPV_APPLIC_MAX_PROC );
			SPVProcedureCount = 0;
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportApplicEvent( NULL, 2, ret_code, "Eccezione generale", "SPVFreeSystemProcedures()" );
		}
		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();

		// --- Cancello le classi di procedure ---
		SPVClearProcedureClasses();
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere il thread di una procedura
///
/// \date [11.02.2008]
/// \author Enrico Alborali,l Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
SYS_THREAD * SPVGetProcedureThread( SPV_PROCEDURE * procedure )
{
	SYS_THREAD * thread = NULL;

	// --- Accedo ai dati procedure ---
	SPVEnterProcedureData("SPVGetProcedureThread");


	try
	{
		if ( procedure != NULL ) {
			thread = procedure->Thread;
		}
	}
	catch(...)
	{
		thread = NULL;
	}

	// --- Esco dai dati procedure ---
	SPVLeaveProcedureData();

	return thread;
}

//==============================================================================
/// Funzione per sapere se una procedura e' attiva
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mari Ferro
/// \version 0.02
//------------------------------------------------------------------------------
bool SPVProcedureIsActive( SPV_PROCEDURE * procedure )
{
	bool active = false;

	// --- Accedo ai dati procedure ---
	SPVEnterProcedureData("SPVProcedureIsActive");


	try
	{
		if ( procedure != NULL ) {
			active = procedure->Active;
		}
	}
	catch(...)
	{
		active = false;
	}

	// --- Esco dai dati procedure ---
	SPVLeaveProcedureData();

	return active;
}

//==============================================================================
/// Funzione per sapere se una procedura si trova in uno state
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mari Ferro
/// \version 0.02
//------------------------------------------------------------------------------
bool SPVProcedureHasState( SPV_PROCEDURE * procedure )
{
	bool state = false;

	// --- Accedo ai dati procedure ---
	SPVEnterProcedureData("SPVProcedureHasState");


	try
	{
		if ( procedure != NULL ) {
			if ( procedure->State != NULL ) {
				state = true;
			}
		}
	}
	catch(...)
	{
		state = false;
	}

	// --- Esco dai dati procedure ---
	SPVLeaveProcedureData();


	return state;
}


//==============================================================================
/// Funzione per salvare nel database la tabella delle procedure applicative
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVDBSaveProcedures( SPV_SYSTEM * system )
{
	int                   ret_code        = SPV_APPLIC_FUNCTION_SUCCESS ;
	int                   proc_count      = 0                           ;
	SPV_PROCEDURE       * proc_list       = NULL                        ;

	if ( system != NULL )
	{
		// --- Accedo ai dati procedure ---
		SPVEnterProcedureData("SPVDBSaveProcedures");
		try
		{
			// --- Svuoto completamente la tabella 'procedures' ---
			ret_code = SPVDBMTruncateProceduresTable(); //SPVDBTruncateTable( db_conn, "procedures" );
			if (ret_code == SPV_APPLIC_NO_ERROR)
			{
				// --- Recupero le variabili globali delle procedure ---
				proc_list   = SPVProcedureList;
				proc_count  = SPVProcedureCount;

				ret_code = SPVDBMSaveProcedures(proc_list, proc_count);
			}
			else
			{
				ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
				SPVReportSystemEvent( system, 2, ret_code, "Eccezione durante lo svuotamento della Tabella Procedures", "SPVDBSaveProcedures()" );
			}
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportSystemEvent( system, 2, ret_code, "Eccezione generale", "SPVDBSaveProcedures()" );
		}

		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per chiamare la Stored Proedure di pulizia delle righe non pi�
/// usate nelle varie tabelle
///
/// \date [03.12.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBDeleteUnusedRows( SPV_SYSTEM * system )
{
	int                   ret_code        = SPV_APPLIC_FUNCTION_SUCCESS ;

	if ( system != NULL )
	{
		try
		{
			ret_code = SPVDBMDeleteUnusedRows();
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportSystemEvent( system, 2, ret_code, "Eccezione generale", "SPVDBDeleteUnusedRows()" );
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per salvare nel database i dati della tabella Systems
/// (Tipologie di periferica)
///
/// \date [11.02.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBSaveDeviceSystems( SPV_SYSTEM * system )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;
	int device_system_count = 0;
	SPV_DEVICE_SYSTEM * device_system_list = NULL;

	if ( system != NULL )
	{
		// --- Entro nella sezione critica ---
		SPVEnterSystemData("SPVDBSaveDeviceSystems");

		try
		{
			// --- Recupero le variabili globali della lista dei systems ---
			device_system_list  = SPVDeviceSystemList;
			device_system_count = SPVDeviceSystemCount;

			ret_code = SPVDBMSaveDeviceSystems(device_system_list, device_system_count);
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportSystemEvent( system, 2, ret_code, "Eccezione generale", "SPVDBSaveDeviceSystems()" );
		}

		// --- Esco dalla sezione critica ---
		SPVLeaveSystemData( );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel database i dati della tabella Vendors
///
/// \date [11.02.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBSaveDeviceVendors( SPV_SYSTEM * system )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;
	int device_vendor_count = 0;
	SPV_DEVICE_VENDOR * device_vendor_list = NULL;

	if ( system != NULL )
	{
		// --- Entro nella sezione critica ---
		SPVEnterSystemData("SPVDBSaveDeviceVendors");

		try
		{
			// --- Recupero le variabili globali della lista dei systems ---
			device_vendor_list  = SPVDeviceVendorList;
			device_vendor_count = SPVDeviceVendorCount;

			ret_code = SPVDBMSaveDeviceVendors(device_vendor_list, device_vendor_count);
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportSystemEvent( system, 2, ret_code, "Eccezione generale", "SPVDBSaveDeviceVendors()" );
		}

		// --- Esco dalla sezione critica ---
		SPVLeaveSystemData( );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per salvare nel database i dati della tabella Device_type
///
/// \date [11.02.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBSaveDeviceTypes( SPV_SYSTEM * system )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;
	int device_type_count = 0;
	SPV_DEVICE_TYPE * device_type_list = NULL;

	if ( system != NULL )
	{
		// --- Entro nella sezione critica ---
		SPVEnterSystemData("SPVDBSaveDeviceTypes");

		try
		{
			// --- Recupero le variabili globali della lista dei systems ---
			device_type_list  = SPVDeviceTypeList;
			device_type_count = SPVDeviceTypeCount;

			ret_code = SPVDBMSaveDeviceTypes(device_type_list, device_type_count);
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportSystemEvent( system, 2, ret_code, "Eccezione generale", "SPVDBSaveDeviceTypes()" );
		}

		// --- Esco dalla sezione critica ---
		SPVLeaveSystemData( );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per aggiornare le informazioni di una procedura applicativa
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
int SPVDBUpdateProcedure( SPV_SYSTEM * system, SPV_PROCEDURE * procedure )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;

	if ( system != NULL )
	{
		// --- Accedo ai dati procedure ---
		SPVEnterProcedureData("SPVDBUpdateProcedure");
		try
		{
			if ( procedure != NULL )
			{
				// --- Svuoto completamente la tabella 'procedures' ---
				ret_code = SPVDBMUpdateProcedure(procedure);
			}
			else
			{
				ret_code = SPV_APPLIC_INVALID_PROCEDURE;
			}
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportSystemEvent( system, 2, ret_code, "Eccezione generale", "SPVDBUpdateProcedure" );
		}

		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per caricare dal DB la lista degli ExeCount
///
/// \date [17.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDBLoadProcExeCounts( SPV_SYSTEM * system, SPV_PROCEDURE_COUNTER * count_list, int * count_num )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;

	if ( system != NULL )
	{
		if ( count_list != NULL )
		{
			// --- Richiamo la funzione di SPVDBManager che carica da DB la lista degli ExeCount ---
			ret_code = SPVDBMLoadProcExeCounts(count_list,count_num);
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_EXECOUNT_LIST;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per caricare i contatori di esecuzione delle procedure
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLoadProcedureCounters( SPV_SYSTEM * system )
{
	int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	SPV_PROCEDURE         * procedure   = NULL;
	SPV_PROCEDURE_COUNTER   count_list[SPV_APPLIC_MAX_PROC];
	SPV_PROCEDURE_COUNTER   counter           ;
	int                     count_num         ;

	if ( system != NULL )
	{
		// --- Entro nei dati procedure ---
		SPVEnterProcedureData("SPVLoadProcedureCounters");

		try
		{
			// --- Carico dal DB la lista (ordinata per DevID,ProID) degli ExeCount ---
			ret_code = SPVDBLoadProcExeCounts( system, &count_list[0], &count_num );
			if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
			{
				// --- Scorro i contatori trovati nel DB ---
				for ( int c = 0; c < count_num; c++ )
				{
					try
					{
						// --- Recupero la procedura associata al contatore ---
						counter = count_list[c];
						procedure = SPVSearchProcedure( counter.DevID, counter.ProID );
						if ( procedure != NULL )
						{
							// --- Aggiorno il flag di contatore cambiato ---
							if ( procedure->ExeCount != counter.ExeCount )
							{
								procedure->ECChanged = true;
							}
							// --- Aggiorno il contatore della procedura ---
							procedure->ExeCount = counter.ExeCount;
						}
					}
					catch(...)
					{
						ret_code = SPV_APPLIC_CRITICAL_ERROR;
						break;
					}
				}
			}
		}
		catch(...)
		{
			SPVReportApplicEvent( procedure, 2, 600, "Eccezione L1", "SPVLoadProcedureCounters()" );
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
		}

		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();

	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DB la lista degli ExeCount
///
/// \date [18.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBSaveProcExeCounts( SPV_SYSTEM * system, SPV_PROCEDURE_COUNTER * count_list, int count_num )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;

	if ( system != NULL )
	{
		if ( count_list != NULL && count_num > 0 )
		{
			// --- Richiamo la funzione di SPVDBManager che carica da DB la lista degli ExeCount ---
			ret_code = SPVDBMSaveProcExeCounts(count_list,count_num);
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_EXECOUNT_LIST;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per salvare i contatori di esecuzione delle procedure
///
/// \date [22.02.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int SPVSaveProcedureCounters( SPV_SYSTEM * system )
{
	int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS ;
	SPV_PROCEDURE         * procedure   = NULL;
	SPV_PROCEDURE         * proc_list   = NULL;
	int                     proc_count  = 0   ;
	SPV_PROCEDURE_COUNTER   count_list[SPV_APPLIC_MAX_PROC];
	SPV_PROCEDURE_COUNTER   counter           ;
	int                     count_num   = 0   ;

	if ( system != NULL )
	{
		// --- Entro nei dati procedure ---
		SPVEnterProcedureData("SPVSaveProcedureCounters");

		try
		{
			// --- Recupero le variabili globali ---
			proc_list   = SPVProcedureList;
			proc_count  = SPVProcedureCount;
			// --- Scorro la lista delle procedure ---
			for ( int p = 0; p < proc_count; p++ )
			{
				try
				{
					procedure = &proc_list[p];
					if ( procedure != NULL )
					{
						if ( procedure->ECChanged == true )
						{
							// --- Preparo il contatore da salvare nel DB ---
							counter.DevID = procedure->Device->DevID;
							counter.ProID = procedure->ID;
							counter.ExeCount = procedure->ExeCount;
							counter.Changed = true;
							// --- Aggiungo il contatore alla lista ---
							count_list[count_num] = counter;
							count_num++;
						}
					}
				}
				catch(...)
				{
					SPVReportApplicEvent( procedure, 2, 601, "Eccezione L2", "SPVSaveProcedureCounters()" );
					ret_code = SPV_APPLIC_CRITICAL_ERROR;
					break;
				}
			}
			if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
			{
				// --- Salvo la lista dei contatori nel DB ---
				SPVDBSaveProcExeCounts( system, &count_list[0], count_num );
			}
		}
		catch(...)
		{
			SPVReportApplicEvent( procedure, 2, 600, "Eccezione L1", "SPVSaveProcedureCounters()" );
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
		}
		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();

	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per lanciare l'esecuzione delle procedure applicative
///
/// \date [15.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLaunchProcedures( SPV_SYSTEM * system )
{
	int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	int                     fun_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	SPV_PROCEDURE         * procedure   = NULL;
	SPV_PROCEDURE         * proc_list   = NULL;
	int                     proc_count  = 0   ;
	int						launch_count = 0;
	int *					proc_id_list	=	NULL;
	int						app				= 0;
	int						pos				= 0;

	if ( system != NULL )
	{
		// --- Entro nei dati procedure ---
		SPVEnterProcedureData("SPVLaunchProcedures");

		try
		{
			// --- Recupero le variabili globali ---
			proc_list   = SPVProcedureList;
			proc_count  = SPVProcedureCount;
			///SPVReportApplicEvent( NULL, 1, 4001, "DEBUG: Launch cycle start.", "SPVLaunchProcedures()" );

			// Randomizzazione dell'ordine di partenza delle procedure
			if (proc_count > 0) {
				// Alloco l'array
				proc_id_list = (int*)malloc(sizeof(int)*proc_count);
				// Popolo l'array
				for (int i = 0; i < proc_count; i++) {
					proc_id_list[i] = i;
				}
				randomize();
				// Scambio le posizioni
				for (int i = 0; i < proc_count; i++) {
					pos = random(proc_count);
					app = proc_id_list[pos];
					proc_id_list[pos] = proc_id_list[i];
					proc_id_list[i] = app;
				}
			}

			// --- Scorro la lista delle procedure ---
			for ( int p = 0; p < proc_count; p++ )
			{
				try
				{
					procedure = &proc_list[proc_id_list[p]]; // Prendo l'id dalla lista randomizzata
					if (SPVValidProcedure( procedure ))
					{
						if ( procedure->Active == false )
						{
							// --- Elimino un eventuale thread terminato ---
							fun_code = SPVDeleteProcedureThread( procedure );
							// --- Se il thread della procedura non era gi� in esecuzione ---
							if ( fun_code != SPV_APPLIC_EXECUTING_PROCEDURE )
							{
								if ( procedure->ExeCount > 0 )
								{
									if ( launch_count < 5 )
									{
										// --- Esecuzione della procedura ---
										SPVExecProcedure( procedure );
										launch_count++;
										///SPVReportApplicEvent( NULL, 1, SPV_APPLIC_FUNCTION_EXCEPTION, "DEBUG: Procedure Launch", "SPVLaunchProcedures()" );
									}
									else
									{
										break;
                                    }
								}
								else {
									///SPVReportApplicEvent( NULL, 1, 3000, "DEBUG: Procedure counter null.", "SPVLaunchProcedures()" );
								}
							}
							else
							{
								///SPVReportApplicEvent( NULL, 1, 3001, "DEBUG: Executing procedure.", "SPVLaunchProcedures()" );
							}
						}
						else
						{
							///SPVReportApplicEvent( NULL, 1, 3002, "DEBUG: Procedure not active.", "SPVLaunchProcedures()" );
						}
					}
					else
					{
						///SPVReportApplicEvent( NULL, 1, 3003, "DEBUG: Procedure not valid.", "SPVLaunchProcedures()" );
					}
				}
				catch(...)
				{
					ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
					SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante il lancio di una procedura", "SPVLaunchProcedures()" );
					break;
				}
			}
			///SPVReportApplicEvent( NULL, 1, 4002, "DEBUG: Launch cycle stop", "SPVLaunchProcedures()" );
			if (proc_id_list != NULL) free(proc_id_list);
		}
		catch(...)
		{
			ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
			SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione generale", "SPVLaunchProcedures()" );
		}
		// --- Esco dai dati procedure ---
		SPVLeaveProcedureData();
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova struttura field di stream
///
/// \date [28.03.2006]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
SPV_DEV_STREAM_FIELD * SPVNewDevField( int ID, char * name )
{
	SPV_DEV_STREAM_FIELD * field = NULL; // Ptr a nuova struttura campo

	field = (SPV_DEV_STREAM_FIELD *)malloc(sizeof(SPV_DEV_STREAM_FIELD));

	if ( field != NULL ) // Se l'allocazione � andata a buon fine
	{
		// --- Imposto i valori passati come parametro ---
		field->VCC      = SPV_APPLIC_FIELD_VALIDITY_CHECK_CODE;
		field->ID       = ID    ;
		field->Name     = (char*)malloc(sizeof(char)*(strlen(name)+1));
		strcpy( field->Name, name );
		// --- Imposto gli altri valori di default ---
		field->Type         = 0     ;
		field->ASCII        = false ;
		field->Len          = 0     ;
		field->Capacity     = 0     ;
		field->Visible      = true  ;
		field->Next         = NULL  ;
		field->Value        = NULL  ;
		field->ValueChanged = NULL  ;
		field->Descr        = NULL  ;
		field->Default      = NULL  ;
		field->SevLevel     = NULL  ; // � un array dinamico di interi
		field->SevChanged   = NULL  ;
		field->Def          = NULL  ;
	}

	return field;
}

//==============================================================================
/// Funzione per eliminare una struttura field di stream
///
/// \date [26.07.2006]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int SPVDeleteDevField( SPV_DEV_STREAM_FIELD * field )
{
  int     ret_code  = 0   ; // Codice di ritorno
  char  * value     = NULL; //
  bool  * verifies  = NULL; //

  if ( SPVValidDevField( field ) ) // Controllo il ptr
  {
    try
    {
      // --- Cancello la stringa del nome ---
      if ( field->Name != NULL )
      {
		try
        {
          free( field->Name ); // -FREE
        }
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
	  }
      // --- Cancello la stringa di formattazione ---
	  if ( field->Format != NULL )
      {
        try
        {
          free( field->Format ); // -FREE
        }
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
	  }
      // --- Cancello le stringhe dei valori ---
      if ( field->Value != NULL )
      {
        for ( int v = 0; v < field->Capacity; v++ )
        {
          value = field->Value[v];
          if ( value != NULL )
          {
			try
            {
              free( value ); // -FREE
            }
            catch(...)
            {
              /* TODO -oEnrico -cError : Gestione dell'errore critico */
            }
          }
        }
        try
        {
		  free( field->Value ); // -FREE
        }
        catch(...)
		{
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
      }
      // --- Cancello la lista flag valori cambiati ---
      if ( field->ValueChanged != NULL )
      {
        try
        {
          free( field->ValueChanged );
        }
		catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
      }
      // --- Cancello la lista flag valori verificati ---
	  if ( field->ValueVerified != NULL )
      {
        for ( int c = 0; c < field->Capacity; c++ )
        {
          // --- Recupero il ptr all'array di flag ---
          verifies = field->ValueVerified[c];
          if ( verifies != NULL )
          {
            try
			{
              free( verifies );
            }
            catch(...)
            {
              /* TODO -oEnrico -cError : Gestione dell'errore critico */
            }
          }
        }
        try
		{
          free( field->ValueVerified );
		}
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
      }
      // --- Cancello le stringhe delle descrizioni ---
      if ( field->Descr != NULL )
	  {
        for ( int v = 0; v < field->Capacity; v++ )
        {
          value = field->Descr[v];
          if ( value != NULL )
          {
            try
            {
              free( value ); // -FREE
            }
            catch(...)
            {
              /* TODO -oEnrico -cError : Gestione dell'errore critico */
            }
          }
        }
        try
        {
          free( field->Descr ); // -FREE
        }
        catch(...)
		{
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
      }
      // --- Cancello la stringa del valore di default ---
	  if ( field->Default != NULL )
      {
        try
        {
          free( field->Default ); // -FREE
        }
        catch(...)
		{
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
		}
      }
      // --- Cancello la lista dei livelli di severit� ---
      if ( field->SevLevel != NULL )
      {
        try
        {
          free( field->SevLevel );
        }
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
      }
      // --- Cancello la lista dei flag di severit� cambiata ---
      if ( field->SevChanged != NULL )
      {
        try
        {
          free( field->SevChanged );
        }
        catch(...)
        {
          /* TODO -oEnrico -cError : Gestione dell'errore critico */
        }
      }
    }
	catch(...)
    {
      /* TODO -oEnrico -cError : Gestione dell'errore critico */
    }
    // --- Cancello la memoria occupata dalla struttura campo ---
    try
    {
      memset( field, 0, sizeof(SPV_DEV_STREAM_FIELD) );
	  free( field );
    }
    catch(...)
    {
	  /* TODO -oEnrico -cError : Gestione dell'errore critico */
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_FIELD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una struttura field
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
bool SPVValidDevField( SPV_DEV_STREAM_FIELD * field )
{
  bool valid = false;

  if ( field != NULL )
  {
	try
    {
      if ( field->VCC == SPV_APPLIC_FIELD_VALIDITY_CHECK_CODE )
      {
        valid = true;
	  }
    }
	catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per caricare i campi di uno stream di periferica
///
/// \date [26.07.2006]
/// \author Enrico Alborali
/// \version 0.10
//------------------------------------------------------------------------------
int SPVLoadDevFieldList( SPV_DEV_STREAM * stream )
{
	int                     ret_code		= 0   ; // Codice di ritorno
	SPV_DEV_STREAM_FIELD  * prev			= NULL; // Ptr di appoggio per field
	SPV_DEV_STREAM_FIELD  * field			= NULL; // Ptr ad un field
	XML_ELEMENT *			field_def		= NULL; // Ptr al descrittore XML di un field
	int                     field_num		= 0   ; // Numero di field per lo stream
	char *					field_default	= NULL; // Valore ASCII di default
	int                     value_ver_num	= 0   ; // Numero di possibili verifiche di valore
	bool *					value_vers		= NULL; // Array dei flag delle verifiche valori
	char *					factor_string	= NULL;
	char *                  addend_string	= NULL;

	if ( SPVValidDevStream( stream ) ) // Controllo il ptr allo stream
	{
		if ( stream->Def != NULL ) // Controllo il ptr alla def stream
		{
			field_def = stream->Def->child; // Prendo il primo child
			if ( field_def != NULL ) // Controllo il ptr alla def field
			{
				SPVDeleteDevFieldList( stream->FieldList, stream->FieldNum ); // Elimino un'eventuale lista
				while ( field_def != NULL ) // Ciclo sui child
				{
					field = SPVNewDevField( XMLGetValueInt( field_def, "id" ), XMLGetValue( field_def, "name" ) );
					if ( SPVValidDevField( field ) )
					{
						if ( stream->FieldList == NULL ) stream->FieldList = field; // Salvo il primo field
                        field->Type     = XMLGetTypeCode  ( XMLGetValue( field_def, "type" ) );
                        field->ASCII    = XMLGetValueBool ( field_def, "ascii" );
                        field->Len      = XMLGetValueInt  ( field_def, "len" );
                        field->Capacity = XMLGetValueInt  ( field_def, "capacity" );
                        // --- Recupero la stringa di formattazione ---
						field->Format   = XMLExtractValue ( field_def, "format" );
						field->Endian	= stream->Endian; // Copio l'endian dello stream
                        // --- Recupero l'addendo da sommare al valore ---
						addend_string = XMLGetValue( field_def, "addend" );
						if ( addend_string != NULL )
						{
							field->Addend	= cnv_CharPToFloat( addend_string );
						}
						else
						{
							field->Addend	= 0.0;
						}
						// --- Recupero il fattore di moltiplicazione ---
						factor_string   = XMLGetValue     ( field_def, "factor" );
						if ( factor_string != NULL )
						{
							field->Factor   = cnv_CharPToFloat( factor_string );
						}
						else
						{
							field->Factor   = 1.0;
                        }
						if ( field->Capacity < 1 ) field->Capacity = 1;
						if ( XMLGetValue( field_def, "visible" ) != NULL )
						{
							field->Visible = XMLGetValueBool( field_def, "visible" );
						}
						else
						{
							field->Visible = true;
                        }
						// --- Alloco la lista di stringhe dei valori ---
						field->Value        = (char**)malloc( sizeof(char*) * field->Capacity ); // -MALLOC
						memset( field->Value, 0, sizeof(char*) * field->Capacity );
						// --- Alloco la lista dei flag di valore cambiato ---
						field->ValueChanged = (bool*)malloc( sizeof(bool) * field->Capacity ); // -MALLOC
						memset( field->ValueChanged, 0, sizeof(bool) * field->Capacity );
						// --- Alloco la lista dei flag di valore verificato ---
						field->ValueVerified = (bool**)malloc( sizeof(bool*) * field->Capacity ); // -MALLOC
						memset( field->ValueVerified, 0, sizeof(bool*) * field->Capacity );
						value_ver_num = XMLGetChildCount( field_def, false );
						field->ValueVerNum = value_ver_num;
						for ( int c = 0; c < field->Capacity; c++ )
						{
							// --- Se sono definite delle possibili verifiche di valore per il campo ---
							if ( value_ver_num > 0 )
							{
								// --- Alloco un  nuovo array di flag ---
								value_vers = (bool*)malloc( sizeof(bool) * value_ver_num ); // -MALLOC
								memset( value_vers, 0, sizeof(bool) * value_ver_num );
								// --- Salvo l'array di flag ---
								field->ValueVerified[c] = value_vers;
							}
						}
						// --- Alloco la lista di stringhe delle descrizioni dei valori ---
						field->Descr    = (char**)malloc( sizeof(char*) * field->Capacity ); // -MALLOC
						memset( field->Descr, 0, sizeof(char*) * field->Capacity );
						field_default   = XMLExtractValue( field_def, "default" ); // -MALLOC
						// --- Valore mantenuto in formato ASCII ---
						if ( field->ASCII )
						{
							field->Default = field_default;
						}
						// --- Eventuale conversione ASCII -> non-ASCII ---
						else
						{
							field->Default = (char*)XMLASCII2Type( field_default, field->Type ); // -MALLOC
							free( field_default ); // -FREE
						}
						// --- Alloco la lista dei livelli di severit� ---
						field->SevLevel = (int*)malloc( sizeof(int) * field->Capacity ); // -MALLOC
						memset( field->SevLevel, 0, sizeof(int) * field->Capacity );
						// --- Alloco la lista dei flag di severit� cambiata ---
						field->SevChanged = (bool*)malloc( sizeof(bool) * field->Capacity ); // -MALLOC
						memset( field->SevChanged, 0, sizeof(bool) * field->Capacity );
						field->Def      = field_def; // Salvo il ptr alla definizione del field
						if ( prev != NULL ) prev->Next = field; // Accodo il field alla lista
						prev            = field; // Salvo il ptr del field corrente
						field_def       = field_def->next; // Passo all'elemento successivo (se esiste)
						field_num++;
					}
					else
					{
						field_def = NULL;
						ret_code = SPV_APPLIC_INVALID_FIELD;
					}
				}
				stream->FieldNum  = field_num; // Salvo il numero di field caricati
			}
			else
			{
				ret_code = SPV_APPLIC_FIELD_DEF_NOT_FOUND;
			}
		}
		else
		{
			ret_code = SPV_APPLIC_STREAM_DEF_NOT_FOUND;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_STREAM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare una lista di strutture field
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDeleteDevFieldList( SPV_DEV_STREAM_FIELD * fields, int num )
{
  int                     ret_code  = 0   ; // Codice di ritorno
  int                     counter   = 0   ; // Contatore field
  SPV_DEV_STREAM_FIELD  * field     = NULL; // Ptr a un field
  SPV_DEV_STREAM_FIELD  * next      = NULL; // Ptr a un field

  if ( SPVValidDevField( fields ) )
  {
	field = fields; // Parto dal primo field
    while ( SPVValidDevField( field ) ) // Ciclo sui field
    {
	  counter++;
      try
      {
        next = field->Next; // Salvo il ptr al field successivo
      }
      catch(...)
      {
        next = NULL;
      }
      SPVDeleteDevField( field );
      field = next; // Passo al field successivo
	}
	if ( counter != num )
	{
      ret_code = SPV_APPLIC_INVALID_COUNT;
    }
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_FIELD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riallocare la struttura field.
/// Da utilizzare in seguito ad un cambiamento runtime della capacita'.
///
/// \date [25.01.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
int SPVReallocDevFieldCapacity( SPV_DEV_STREAM_FIELD * field, int new_capacity )
{
	int ret_code = SPV_APPLIC_NO_ERROR;

	if ( SPVValidDevField( field ) )
	{
		if (field->Capacity != new_capacity) // Solo se la nuova capacita' e' diversa
		{
			try
			{
				for ( int c = 0; c < field->Capacity; c++ )
				{
					// Elimino i singoli valori
					if (field->Value[c] != NULL) free(field->Value[c]); // -FREE
					// Elimino le singole descrizioni
					if (field->Descr[c] != NULL) free(field->Descr[c]); // -FREE
					if (field->ValueVerNum > 0)
					{
						// Elimino i singoli flag di valore verificato
						if (field->ValueVerified[c] != NULL) free(field->ValueVerified[c]); // -FREE
					}
				}
				if (field->Value != NULL)			free(field->Value); // -FREE
				if (field->ValueChanged != NULL)	free(field->ValueChanged); // -FREE
				if (field->ValueVerified != NULL)	free(field->ValueVerified); // -FREE
				if (field->Descr != NULL)			free(field->Descr); // -FREE
				if (field->SevLevel != NULL)		free(field->SevLevel); // -FREE
				if (field->SevChanged != NULL)		free(field->SevChanged); // -FREE
			}
		    catch(...)
		    {
		    	ret_code = SPV_APPLIC_FREE_FAILURE;
		    }
		    try
		    {
		    	// Salvo la nuova capacita'
		    	field->Capacity = new_capacity;
		    	// --- Alloco la lista di stringhe dei valori ---
		    	field->Value        = (char**)malloc( sizeof(char*) * field->Capacity ); // -MALLOC
				memset( field->Value, 0, sizeof(char*) * field->Capacity );
				// --- Alloco la lista dei flag di valore cambiato ---
				field->ValueChanged = (bool*)malloc( sizeof(bool) * field->Capacity ); // -MALLOC
				memset( field->ValueChanged, 0, sizeof(bool) * field->Capacity );
				// --- Alloco la lista dei flag di valore verificato ---
				field->ValueVerified = (bool**)malloc( sizeof(bool*) * field->Capacity ); // -MALLOC
				memset( field->ValueVerified, 0, sizeof(bool*) * field->Capacity );
				// --- Alloco la lista di stringhe delle descrizioni dei valori ---
				field->Descr    = (char**)malloc( sizeof(char*) * field->Capacity ); // -MALLOC
				memset( field->Descr, 0, sizeof(char*) * field->Capacity );
				// --- Alloco la lista dei livelli di severit� ---
				field->SevLevel = (int*)malloc( sizeof(int) * field->Capacity ); // -MALLOC
				memset( field->SevLevel, 0, sizeof(int) * field->Capacity );
				// --- Alloco la lista dei flag di severit� cambiata ---
				field->SevChanged = (bool*)malloc( sizeof(bool) * field->Capacity ); // -MALLOC
				memset( field->SevChanged, 0, sizeof(bool) * field->Capacity );

				for ( int c = 0; c < field->Capacity; c++ )
				{
					// --- Se sono definite delle possibili verifiche di valore per il campo ---
					if ( field->ValueVerNum > 0 )
					{
						// --- Alloco un  nuovo array di flag ---
						field->ValueVerified[c] = (bool*)malloc( sizeof(bool) * field->ValueVerNum ); // -MALLOC
						memset( field->ValueVerified[c], 0, sizeof(bool) * field->ValueVerNum );
					}
				}
		    }
		    catch(...)
		    {
				ret_code = SPV_APPLIC_ALLOCATION_FAILURE;
			}
			if (field->Capacity > new_capacity && ret_code == SPV_APPLIC_NO_ERROR)
			{
				// Ci possono essere nel DB alcune righe in piu' rispetto all'attuale capacita'
				ret_code = SPV_APPLIC_FIELD_DB_TABLE_NEEDS_DELETE;
            }
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_FIELD;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova struttura stream
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
SPV_DEV_STREAM * SPVNewDevStream( int ID, char * name )
{
  SPV_DEV_STREAM * stream = NULL; // Ptr alla nuova struttra stream di periferica

  stream = (SPV_DEV_STREAM*)malloc( sizeof(SPV_DEV_STREAM) ); // -MALLOC

  if ( stream != NULL ) // Se l'allocazione � andata a buon fine
  {
    try
	{
      memset( stream, 0, sizeof(SPV_DEV_STREAM) );
    }
    catch(...)
    {
      /* TODO 1 -oEnrico -cError : Gestione dell'errore critico */
	}
	// --- Imposto i valori passati come parametri ---
    stream->VCC           = SPV_APPLIC_STREAM_VALIDITY_CHECK_CODE;
    stream->ID            = ID    ;
    stream->Name          = XMLStrCpy( stream->Name, name ); // -MALLOC [ -FREE ]
    // --- Imposto gli altri valori di default ---
	stream->Visible       = false ;
	stream->Endian			= true; // BIG ENDIAN
    stream->FieldNum      = 0     ;
	stream->FieldList     = NULL  ;
	stream->SevLevel      = -1	  ;  // -- di default illevel � offLine
    stream->SevChanged    = false ;
    stream->Buffer.Len    = 0     ;
    stream->Buffer.Ptr    = 0     ;
    stream->BufferChanged = false ;
    stream->Def           = NULL  ;
    stream->Next          = NULL  ;
  }

  return stream;
}

//==============================================================================
/// Funzione per eliminare una struttura stream
///
/// \date [02.08.2007]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDeleteDevStream( SPV_DEV_STREAM * stream )
{
  int ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

  if ( stream != NULL ) // Controllo il ptr allo stream
  {
    // --- Cancello il nomde della periferica ---
	if ( stream->Name != NULL )
	{
      try
      {
        free( stream->Name );
      }
      catch(...)
      {
        ret_code = SPV_APPLIC_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
	if ( stream->Buffer.Ptr != NULL )
    {
	  try
	  {
        free( stream->Buffer.Ptr );
	  }
	  catch(...)
	  {
		ret_code = SPV_APPLIC_CRITICAL_ERROR;
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
	  }
    }
    // --- Cancello la lista dei campi dello stream ---
    if ( stream->FieldList != NULL )
    {
      SPVDeleteDevFieldList( stream->FieldList, stream->FieldNum );
	}
	// --- Cancello la descrizione dello stream ---
	if ( stream->Description != NULL )
	{
	  try
      {
		free( stream->Description );
	  }
	  catch(...)
	  {
		ret_code = SPV_APPLIC_CRITICAL_ERROR;
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
	}
    // --- Elimino lo stream ---
    try
    {
      memset( stream, 0, sizeof(SPV_DEV_STREAM) );
      free( stream );
    }
    catch(...)
    {
      ret_code = SPV_APPLIC_CRITICAL_ERROR;
      /* TODO -oEnrico -cError : Gestione dell'errore critico */
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_STREAM;
  }

  return ret_code;
}

//==============================================================================
// Funzione per verificare la validita' di una struttura stream
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidDevStream( SPV_DEV_STREAM * stream )
{
  bool valid = false;

  if ( stream != NULL )
  {
    try
    {
      if ( stream->VCC == SPV_APPLIC_STREAM_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
	catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per caricare gli stream di una periferica
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVLoadDevStreamList( SPV_DEVICE * device )
{
  int               ret_code    = 0   ; // Codice di ritorno
  SPV_DEV_STREAM  * stream_list = NULL; // Ptr al primo stream della lista
  int               stream_num  = 0   ; // Numero di stream nella lista
  SPV_DEV_STREAM  * stream      = NULL; // Ptr di appoggio ad uno stream
  XML_ELEMENT     * stream_def  = NULL; // Ptr al descrittore XML degli stream
  SPV_DEV_STREAM  * prev        = NULL; // Ptr di appoggio ad uno stream
  char            * visible_str = NULL; // Ptr al valore visible di uno stream

  if ( SPVValidDevice( device ) ) // Controllo il ptr al tipo periferica
  {
    if ( device->Def != NULL ) // Controllo la def della periferica
	{
      stream_def = device->Def;
      if ( stream_def != NULL )
      {
        stream_def = stream_def->child; // Passo al primo child
      }
      if ( stream_def != NULL )
      {
        stream_def = stream_def->child; // Passo al primo child
      }
      stream_def = XMLGetNext( stream_def, "stream", -1 );
      if ( stream_def != NULL )
	  {
        stream_def = stream_def->child; // Parto dal primo child
        while ( stream_def != NULL ) // Ciclo sugli stream item
        {
		  stream      = SPVNewDevStream( XMLGetValueInt( stream_def, "id" ), XMLGetValue( stream_def, "name" ) );
          if ( stream_list == NULL ) stream_list = stream; // Salvo il primo stream
          if ( SPVValidDevStream( stream ) )
          {
            stream->Def = stream_def;
            // --- Recupero il flag visible per lo stream ---
            visible_str = XMLGetValue( stream_def, "visible" );
            if ( visible_str == NULL )
			{
              // --- Valore di default: true ---
			  stream->Visible = true;
			}
			else
			{
			  stream->Visible = XMLGetValueBool( stream_def, "visible" );
			}
			// Recupero dal DB la severita' precedente dello stream (se esiste)
			if (stream->Visible == true) {
			  stream->SevLevel = SPVDBMGetStreamSeverity( device, stream );
			}
			stream->Endian = device->Type->Endian; // Copio l'endian della periferica
            SPVLoadDevFieldList( stream ); // Carico la lista dei field
            if ( prev != NULL ) prev->Next = stream; // Accodo lo stream alla lista
			prev        = stream; // Salvo il ptr dello stream corrente
            stream_num++; // Incremento il contatore degli stream della periferica
            stream_def = stream_def->next; // Passo all'elemento successivo (se esiste)
          }
          else
          {
            stream_def = NULL;
          }
        }
        device->StreamList  = stream_list;
        device->StreamCount = stream_num;
      }
      else
      {
        ret_code = SPV_APPLIC_STREAM_DEF_NOT_FOUND;
      }
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE_TYPE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare gli stream di una periferica
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVFreeDevStreamList( SPV_DEVICE * device )
{
  int               ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
  int               stream_num  = 0   ; // Numero di stream nella lista
  SPV_DEV_STREAM  * stream      = NULL; // Ptr di appoggio ad uno stream
  SPV_DEV_STREAM  * next        = NULL; // Ptr di appoggio ad uno stream

  if ( SPVValidDevice( device ) )
  {
    stream      = (SPV_DEV_STREAM*)device->StreamList;
	if ( SPVValidDevStream( stream ) )
    {
      try
      {
        stream_num = device->StreamCount;
      }
      catch(...)
      {
        stream_num = 0;
      }
      // --- Ciclo sugli stream ---
      for ( int s = 0; s < stream_num; s++ )
      {
        try
        {
          next = stream->Next;
        }
		catch(...)
        {
          next = NULL;
        }
        if ( SPVValidDevStream( stream ) )
        {
		  ret_code  = SPVDeleteDevStream( stream );
          stream = next;
        }
        else
        {
          s = stream_num; // Finisco il ciclo for
		  ret_code = SPV_APPLIC_INVALID_STREAM;
        }
      }
    }
    else
    {
      ret_code = SPV_APPLIC_INVALID_STREAM;
    }
    try
    {
      device->StreamList = NULL;
      device->StreamCount = 0;
    }
    catch(...)
    {
      ret_code = SPV_APPLIC_INVALID_DEVICE;
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere il ptr ad una struttura field di uno stream
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_DEV_STREAM_FIELD * SPVGetDevField( SPV_DEV_STREAM * stream, int num )
{
  SPV_DEV_STREAM_FIELD * field    = NULL;

  if ( SPVValidDevStream( stream ) )
  {
    try
    {
	  field = stream->FieldList;
    }
    catch(...)
    {
      field = NULL;
    }
    for ( int s = 0; s < num; s++ )
    {
      if ( SPVValidDevField( field ) )
      {
        try
        {
          field = field->Next;
        }
        catch(...)
        {
          field = NULL;
        }
      }
    }
  }

  return field;
}

//==============================================================================
// Funzione per estrarre un field da uno stream
///
/// \date [30.06.2006]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVImportDevFieldBuffer( SPV_DEV_STREAM_FIELD * field, char * buffer, int len, int * poffset )
{
  int             ret_code              = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  int             offset                = 0     ;
  int             buffer_max_len        = 0     ;
  int             field_capacity        = 0     ; // Capacita' dell'array
  bool            field_ASCII           = false ;
  int             field_len             = 0     ;
  int             field_value_len       = 0     ;
  int             field_type            = 0     ;
  char          * field_buffer          = NULL  ;
  int             field_buffer_len      = 0     ;
  char          * field_old_buffer      = NULL  ;
  bool            field_value_changed   = false ;

	// --- Recupero l'attuale valore dell'offset ---
	if ( poffset != NULL )
	{
    try
    {
      offset = *poffset;
    }
    catch(...)
    {
      offset = 0;
    }
	}
	if ( SPVValidDevField( field ) )
	{
	    try
	    {
	      field_capacity  = field->Capacity;
	      field_ASCII     = field->ASCII;
	      field_len       = field->Len;
	      field_type      = field->Type;
	    }
	    catch(...)
	    {
	      ret_code = SPV_APPLIC_CRITICAL_ERROR;
	    }
	    if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
	    {
			if ( field_type == t_string )
			{
				// --- Ciclo sull'array di campi ---
		    	for ( int c = 0; c < field_capacity; c++ )
		    	{
		    	  // --- Alloco la memoria necessaria per contenere il nuovo valore ---
		    	  // --- Recupero il buffer del campo dal buffer del frame ---
		    	  try
		    	  {
		    		field_buffer = XMLGetToken( &buffer[offset], ";", c );
		    		if ( field_buffer != NULL )
		    		{
		    			field_buffer_len = strlen( field_buffer );

		    			//* CODICE SPERIMENTALE 25.02.2009
		    			if ( field_capacity == 1 && field_buffer_len > field_len )
		    			{
		    				// Riposizioni il NULL-terminatore
		    				field_buffer[field_len] = '\0';
		    				// Correggo la lunghezza (con il valore massimo)
		    				field_buffer_len = field_len-1;
		    			}
		    			//*/
		    		}
		    		else
		    		{
		    		  field_buffer_len = 0;
		    		}
		    	  }
		    	  catch(...)
		    	  {
		    		field_buffer = NULL;
		    		ret_code = SPV_APPLIC_CRITICAL_ERROR;
		    	  }
		    	  if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
		    	  {
		    		// --- Recupero un eventuale valore precedente ---
		    		try
		    		{
		    		  field_old_buffer = field->Value[c];
		    		}
		    		catch(...)
		    		{
		    		  field_old_buffer = NULL;
		    		  ret_code = SPV_APPLIC_CRITICAL_ERROR;
		    		}
		    		// --- Se c'era un valore precedente ---
		    		if ( field_old_buffer != NULL )
		    		{
		    		  if ( field_buffer != NULL )
		    		  {
		    			// --- Controllo byte per byte se il valore � cambiato ---
		    			field_value_changed = false;
		    			if ( strcmp( field_old_buffer, field_buffer ) != 0 )
		    			{
		    			  field_value_changed = true;
		    			}
		    			// --- Cancello il valore precedente ---
		    			try
		    			{
		    			  free( field_old_buffer );
		    			  field->Value[c] = NULL;
		    			}
		    			catch(...)
		    			{
		    			  ret_code = SPV_APPLIC_CRITICAL_ERROR;
		    			}
		    		  }
		    		  else
		    		  {
		    			field_value_changed = true;
		    		  }
		    		}
		    		else
		    		{
		    		  if ( field_buffer != NULL )
		    		  {
		    			field_value_changed = true;
		    		  }
		    		}
		    		// --- Marchio il buffer come cambiato oppure no ---
		    		try
		    		{
		    		  field->ValueChanged[c] = field_value_changed;
		    		}
		    		catch(...)
		    		{
		    		  ret_code = SPV_APPLIC_CRITICAL_ERROR;
		    		  break;
		    		}
		    		// --- Salvo il nuovo buffer del campo ---
		    		try
		    		{
		    		  field->Value[c] = field_buffer;
		    		  offset += field_buffer_len+1;
		    		}
		    		catch(...)
		    		{
		    		  ret_code = SPV_APPLIC_CRITICAL_ERROR;
		    		  break;
		    		}
		    	  }
		    	}
		    }
			else // Caso non-stringa
			{
		    	// --- Calcolo la lunghezza del buffer del campo (di ogni elemento dell'array) ---
				if ( field_ASCII == true )
		    	{
		    	  // --- Caso ASCII ---
				  field_buffer_len = field_len * XMLCharSizeOf( field_type );
		    	}
		    	else
		    	{
		    	  // --- Caso BINARIO ---
				  field_buffer_len = field_len * XMLByteSizeOf( field_type );
		    	}
		    	// --- Calcolo la lunghezza massima del buffer ricevuto ---
		    	buffer_max_len = len - offset;
		    	if ( buffer_max_len >= ( field_buffer_len * field_capacity ) )
		    	{
				    // --- Ciclo sull'array di campi ---
				    for ( int c = 0; c < field_capacity; c++ )
					{
						// --- Alloco la memoria necessaria per contenere il nuovo valore ---
						if ( field_buffer_len > 0 )
						{
							field_buffer = (char *)malloc( sizeof(char) * ( field_buffer_len + 1 ) );
							try
							{
								memset( field_buffer, 0, sizeof(char) * ( field_buffer_len + 1 ) );
							}
							catch(...)
							{
								field_buffer = NULL;
							}
						}
						// --- Recupero il buffer del campo dal buffer del frame ---
						if ( field_buffer != NULL )
						{
							try
							{
								memcpy( field_buffer, &buffer[offset], field_buffer_len );
							}
							catch(...)
							{
								ret_code = SPV_APPLIC_CRITICAL_ERROR;
							}
							// --- Swap in caso di dato LITTLE ENDIAN ASCII ---
							if (field_ASCII == true && field->Endian == false)
							{
								char * big_endian_buffer = cnv_SwapBytes(field_buffer,XMLByteSizeOf( field_type ),field_len,field_ASCII); // -MALLOC
								if (big_endian_buffer != NULL) {
									free(field_buffer); // -FREE
									field_buffer = big_endian_buffer;
								}
							}
							// --- Swap in caso di dato BIG ENDIAN BINARIO ---
							else if (field_ASCII == false && field->Endian == true) {
								char * little_endian_buffer = cnv_SwapBytes(field_buffer,XMLByteSizeOf( field_type ),field_len,field_ASCII); // -MALLOC
								if (little_endian_buffer != NULL) {
									free(field_buffer); // -FREE
									field_buffer = little_endian_buffer;
								}
							}
						}
						else
						{
							ret_code = SPV_APPLIC_CRITICAL_ERROR;
						}
						if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
						{
							// --- Recupero un eventuale valore precedente ---
							try
							{
								field_old_buffer = field->Value[c];
							}
							catch(...)
							{
								field_old_buffer = NULL;
								ret_code = SPV_APPLIC_CRITICAL_ERROR;
							}
						    // --- Se c'era un valore precedente ---
						    if ( field_old_buffer != NULL )
						    {
						    	// --- Controllo byte per byte se il valore � cambiato ---
						    	field_value_changed = false;
						    	for ( int k = 0; k < field_buffer_len; k++ )
						    	{
									if ( field_old_buffer[k] != field_buffer[k] )
									{
										field_value_changed = true;
										break;
									}
								}
								// --- Cancello il valore precedente ---
								try
								{
									free( field_old_buffer );
									field->Value[c] = NULL;
								}
								catch(...)
								{
						    		ret_code = SPV_APPLIC_CRITICAL_ERROR;
						    	}
						    }
						    else
						    {
						    	field_value_changed = true;
						    }
						    // --- Marchio il buffer come cambiato oppure no ---
						    try
						    {
						    	field->ValueChanged[c] = field_value_changed;
						    }
						    catch(...)
						    {
						    	ret_code = SPV_APPLIC_CRITICAL_ERROR;
						    	break;
						    }
						    // --- Salvo il nuovo buffer del campo ---
						    try
						    {
						    	field->Value[c] = field_buffer;
						    	offset += field_buffer_len;
						    }
						    catch(...)
						    {
						    	ret_code = SPV_APPLIC_CRITICAL_ERROR;
						    	break;
						    }
						}
				  }
				}
				else
		    	{
		    	  ret_code = SPV_APPLIC_FIELD_BUFFER_TOO_SMALL;
		    	}
			}
		}
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_FIELD;
  }
  // --- Salvo il nuovo valore dell'offset ---
  if ( poffset != NULL )
  {
    try
    {
      *poffset = offset;
    }
    catch(...)
    {
    }
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una lista di strutture stream
///
/// \date [14.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDeleteDevStreamList( SPV_DEV_STREAM * streams, int num )
{
  int               ret_code  = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  SPV_DEV_STREAM  * stream    = NULL; // Ptr ad uno stream
  SPV_DEV_STREAM  * next      = NULL; // Ptr di appoggio per uno stream
  int               counter   = 0   ; // Contatore degli stream

  if ( SPVValidDevStream( streams ) ) // Controllo il ptr al primo stream della lista
  {
    stream = streams; // Parto dal primo stream
    while ( SPVValidDevStream( stream ) ) // Ciclo sulla lista degli stream
    {
      try
      {
		next = stream->Next; // Mi salvo il ptr allo stream successivo
      }
      catch(...)
      {
        next = NULL;
        ret_code = SPV_APPLIC_INVALID_STREAM;
      }
      SPVDeleteDevStream( stream );
      stream = next; // Passo allo stream successivo (se esiste)
      counter++;
	}
    if ( counter != num ) // Se il contatore non coincide col valore specificato
    {
      ret_code = SPV_APPLIC_INVALID_COUNT;
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_STREAM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere il tipo di valore di un campo di stream
///
/// Restituisce il codice del tipo di valore.
/// In caso die errore restituisce 0.
///
/// \date [10.05.2012]
/// \author Enrico Alborali
/// \version 1.02
//------------------------------------------------------------------------------
int SPVGetFieldValueType( XML_ELEMENT * value_e )
{
	char  * type_str  = NULL; // Ptr per stringa del tipo
	int     type      = 0   ; // Codice del tipo di valore

	if ( value_e != NULL )
	{
		type_str = XMLGetValue( value_e, "type" );
		if ( type_str != NULL )
		{
			if ( strcmpi( type_str, "exact" 	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_EXACT;
		    if ( strcmpi( type_str, "range" 	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_RANGE;
		    if ( strcmpi( type_str, "mask"  	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_MASK;
			if ( strcmpi( type_str, "nxor"  	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_NXOR;
			if ( strcmpi( type_str, "extract" 	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_EXTRACT;
			if ( strcmpi( type_str, "2extract" 	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_2EXTRACT;
			if ( strcmpi( type_str, "other" 	) == 0 ) type = SPV_APPLIC_FIELD_VALUE_TYPE_OTHER;
		}
    }

    return type;
}

//==============================================================================
/// Funzione per generare un buffer dello stream di un frame di periferica
///
/// \date [24.08.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_DEV_STREAM_BUFFER SPVDefaultDevFrameBuffer( SPV_DEVICE * device, int streamID )
{
	SPV_DEV_STREAM_BUFFER   stream_buffer   = { 0, NULL } ; // Struttura del buffer dello stream
	SPV_DEV_STREAM        * stream          = NULL        ; // Ptr alla struttura dello stream
	SPV_DEV_STREAM_FIELD  * field           = NULL        ;
	int                     field_num       = 0           ; //
	int                     field_type      = 0           ;
	bool                    field_ASCII     = false       ;
	int                     field_len       = 0           ;
	int                     field_size      = 0           ;
	int                     field_capacity  = 0           ;
	unsigned char         * buffer          = NULL        ;
	int                     buffer_len      = 0           ;
	int                     buffer_offset   = 0           ;

	if ( device != NULL ) // Controllo il ptr alla struttura periferica
	{
		stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
		if ( stream != NULL ) // Se ho recuperato il ptr della struttura dello stream
		{
			field_num = stream->FieldNum;
            if ( field_num > 0 )
            {
				field = stream->FieldList;
				// --- Ciclo sui campi dello stream per calcolare la lunghezza totale ---
                for ( int f = 0; f < field_num; f++ )
				{
                    if ( field != NULL )
                    {
                    	field_type  = field->Type ;
                    	field_ASCII = field->ASCII;
                    	field_len   = field->Len  ;
                    	// --- Caso stringa (lunghezza non specificata) ---
                    	if ( field->Type == t_string ) {
                    		field_size = strlen( field->Default ) + 1; // Aggiungo ';'
                    	}
                    	// --- Caso normale (non stringa)
                    	else
						{
							if ( field->ASCII )
							{
							  field_size = XMLCharSizeOf( field->Type ) * field_len;
							}
							else
							{
							  field_size = XMLByteSizeOf( field->Type ) * field_len;
							}
						}
						field_capacity  = field->Capacity;
						buffer_len += field_size * field_capacity;
						field = field->Next;
					}
				}
				if ( buffer_len > 0 )
				{
					buffer = (unsigned char*) malloc( sizeof(unsigned char) * ( buffer_len + 1 ) );
					if ( buffer != NULL )
					{
						// --- Azzero il buffer ---
						memset( buffer, 0, sizeof(unsigned char) * ( buffer_len + 1 ) );
						field = stream->FieldList;
						// --- Ciclo sui campi dello stream per calcolare la lunghezza totale ---
						for ( int f = 0; f < field_num; f++ )
						{
							if ( field != NULL ) // Se il ptr alla struttura field � valida
							{
								field_type  = field->Type ;
								field_ASCII = field->ASCII;
								field_len   = field->Len  ;
								// --- Caso stringa (lunghezza non specificata) ---
								if ( field->Type == t_string )
								{
									field_size = strlen( field->Default );
								}
								else
								{
									if ( field->ASCII )
									{
										field_size = XMLCharSizeOf( field->Type ) * field_len;
									}
									else
									{
										field_size = XMLByteSizeOf( field->Type ) * field_len;
									}
								}
								field_capacity = field->Capacity;
								// --- Copio per tutto l'array il valore di default ---
								for ( int c = 0; c < field_capacity; c++ )
								{
									memcpy( &buffer[buffer_offset], field->Default, field_size );
									buffer_offset += field_size;
									// --- Nel caso di field stringa aggiungo ';' ---
									if ( field->Type == t_string )
									{
										buffer[buffer_offset] = ';';
										buffer_offset += 1;
									}
								}
								field = field->Next;
							}
						}
						stream_buffer.Len = buffer_len;
						stream_buffer.Ptr = buffer;
					}
					else
					{
					}
				}
				else
				{
					buffer = NULL;
				}
			}
			else
			{
				// --- Eventuale gestione dell'errore ---
			}
		}
		else
		{
			// --- Eventuale gestione dell'errore ---
		}
	}
	else
	{
		// --- Eventuale gestione dell'errore ---
	}

	return stream_buffer;
}

//==============================================================================
/// Funzione per importare un buffer in un frame
///
/// \date [16.03.2006]
/// \author Enrico Alborali
/// \version 0.10
//------------------------------------------------------------------------------
int SPVImportDevFrameBuffer( SPV_DEV_STREAM * stream, char * buffer, int len )
{
	int                     ret_code    = 0   ; // Codice di ritorno
	char                  * field_value = NULL; // Ptr a stringa ASCII del valore campo
	int                     field_len   = 0   ; // Lunghezza valore campo
	SPV_DEV_STREAM_FIELD  * field       = NULL; // Ptr ad una struttura campo
	char                  * value       = NULL;
	int                     value_len   = 0   ;
	int                     value_off   = 0   ;
	int                     offset      = 0   ;
	char                  * field_tok   = NULL;
	char                  * old_value   = NULL; // Ptr ad un vecchio valore da cancellare
	unsigned char         * old_buffer  = NULL; // Ptr ad un vecchio buffer da cancellare
	int                     ASCII_len   = 0   ;

	if ( SPVValidDevStream( stream ) )
	{
		// --- Resetto il flag di buffer cambiato ---
		stream->BufferChanged = false;
		// --- Controllo se la lunghezza � cambiata rispetto al precedente buffer ---
		if ( stream->Buffer.Len != len )
		{
			stream->BufferChanged = true; // Imposto il flag di buffer cambiato
		}
		else
		{
			// --- Controllo se il buffer � cambiato rispetto al precedente ---
			if ( stream->Buffer.Ptr != NULL && buffer != NULL )
			{
				/* BEGUG
				if ( stream->ID == 1 ) {
					memcpy( buffer, "60002000008A67B6A2C700000000018F018F", len );
				}
				//*/
				for ( int c = 0; c < len; c++ )
				{
					if ( stream->Buffer.Ptr[c] != buffer[c] )
					{
						stream->BufferChanged = true; // Imposto il flag di buffer cambiato
						break; // Fine del ciclo for
					}
				}
			}
		}
		// --- Importo il nuovo buffer (se valido) ---
		if ( buffer != NULL && len > 0 )
		{
			// --- Cancellazione eventuale vecchio buffer ---
			if ( stream->Buffer.Len > 0 && stream->Buffer.Ptr != NULL )
			{
				try
				{
					old_buffer = stream->Buffer.Ptr;
					free( old_buffer );
					stream->Buffer.Ptr = NULL;
				}
				catch (...)
				{
					/* TODO 2 -oEnrico -cError : Gestire l'errore critico */
				}
			}
			// --- Salvataggio nuovo buffer ---
			stream->Buffer.Len  = len;
			stream->Buffer.Ptr  = (unsigned char*)buffer;
			  if ( stream->FieldList != NULL && stream->FieldNum > 0 )
			{
			  field = stream->FieldList; // Parto dal primo
				while ( SPVValidDevField( field ) )
			  {
				ret_code = SPVImportDevFieldBuffer( field, buffer, len, &offset );
				if ( ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
				  {
				  field = field->Next; // Passo al campo successivo
	    		}
				else
	    		{
	    		  field = NULL;
	    		  break;
	    		}
	    	  }
	    	  }
	    	else
	    	{
	    	  ret_code = SPV_APPLIC_INVALID_FIELD_LIST;
	    	}
	    }
	    else
	    {
	    	ret_code = SPV_APPLIC_INVALID_BUFFER;
	    }
	}
	else
	{
	  ret_code = SPV_APPLIC_INVALID_STREAM;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per accodare una stringa ad una descrizione
///
/// \date [26.05.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
char * SPVAddDescription( char * olddes, char * newdes )
{
  char * description  = NULL;
  int    old_len      = 0   ;
  int    new_len      = 0   ;
  int    offset       = 0   ;

  if ( newdes != NULL )
  {
    new_len = strlen( newdes );
  }
  else
  {
    new_len = 0;
  }
  if ( olddes != NULL )
  {
    old_len = strlen( olddes );
  }
  else
  {
    old_len = 0;
  }
  if ( ( old_len + new_len ) > 0 )
  {
    description = (char*)malloc( sizeof(char) * ( old_len + 1 + new_len + 1 ) ); // -MALLOC
  }
  if ( old_len > 0 )
  {
    memcpy( description, olddes, sizeof(char) * old_len );
    description[ old_len ]    = ';';
	description[ old_len+1 ]  = '\0';
    offset = old_len+1;
    try
    {
      free( olddes ); // -FREE
    }
	catch(...)
    {
      /* TODO -oEnrico -cError : Gestione dell'errore critico */
    }
  }
  if ( new_len > 0 )
  {
    memcpy( &description[offset], newdes, new_len );
    description[ offset + new_len ] = '\0';
  }

  return description;
}


//==============================================================================
/// Funzione per verificare le dipendenze per un valore di field
///
/// \date [02.01.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
bool SPVCheckValueDeps( SPV_DEVICE * device, char * dep, int eleID )
{
	bool                    checked     = false;
	char *					single_dep  = NULL;
	char *					stream_dep  = NULL;
	char *					field_dep   = NULL;
	char *					element_dep = NULL;
	char *					value_dep   = NULL;
	int                     dep_count   = 0   ;
	void *					value       = NULL;
	int                     stream_id   = 0   ;
	int                     field_id    = 0   ;
	int                     element_id  = 0   ;
	int                     value_id    = 0   ;
	SPV_DEV_STREAM_FIELD *	field       = NULL;
	SPV_DEV_STREAM *		stream      = NULL;
	bool                    not_dep     = false;

	if ( device != NULL )
	{
		if ( dep == NULL )
		{
			checked = true;
		}
		else
		{
			// Controllo se sono in un caso OR (||) o AND (,)
			single_dep = XMLGetToken( dep, "||", 1 ); // -MALLOC
			// Caso OR (separatore "||")
			if ( single_dep != NULL )
			{
				free( single_dep ); // -FREE
				single_dep = XMLGetToken( dep, "||", dep_count ); // -MALLOC
				// --- Processo ogni singola dipendenza ---
				while ( single_dep != NULL )
				{
					// --- Dipendenza negativa ---
					if ( single_dep[0] == '!' )
					{
						not_dep = true;
						value = (void*)XMLGetToken( single_dep, "!", 1 ); // -MALLOC
						free( single_dep ); // -FREE
						single_dep = (char*)value;
					}
					// --- Dipendenza positiva ---
					else
					{
						not_dep = false;
					}
					// --- Recupero i parametri della dipendenza ---
					stream_dep  = XMLGetToken( single_dep, ".", 0 ); // -MALLOC
					field_dep   = XMLGetToken( single_dep, ".", 1 ); // -MALLOC
					element_dep = XMLGetToken( single_dep, ".", 2 ); // -MALLOC
					value_dep   = XMLGetToken( single_dep, ".", 3 ); // -MALLOC
					// --- Coversione da ASCII a intero ---
					value = XMLASCII2Type( stream_dep, t_u_int_16 ); // -MALLOC
					if ( value != NULL )
					{
						stream_id = (int)*((unsigned __int16 *)value);
						free( value ); // -FREE
					}
					value = XMLASCII2Type( field_dep, t_u_int_16 ); // -MALLOC
					if ( value != NULL )
					{
						field_id = (int)*((unsigned __int16 *)value);
						free( value ); // -FREE
					}
					if ( strcmpi( element_dep, "i" ) == 0 )
					{
						element_id = eleID;
					}
					else
					{
						value = XMLASCII2Type( element_dep, t_u_int_16 ); // -MALLOC
						if ( value != NULL )
						{
							element_id = (int)*((unsigned __int16 *)value);
							free( value ); // -FREE
						}
					}
					if ( strcmpi( value_dep, "i" ) == 0 )
					{
						value_id = eleID;
					}
					else
					{
						value = XMLASCII2Type( value_dep, t_u_int_16 ); // -MALLOC
						if ( value != NULL )
						{
							value_id = (int)*((unsigned __int16 *)value);
							free( value ); // -FREE
						}
					}
					// --- Verfica ---
					stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, stream_id );
					if ( SPVValidDevStream( stream ) )
					{
						field = SPVSearchStreamField( stream->FieldList, field_id );
						if ( SPVValidDevField( field ) )
						{
							if ( field->ValueVerified != NULL )
							{
								if ( element_id < field->Capacity )
								{
									try
									{
										// --- Verifica negativa ---
										if ( not_dep )
										{
											checked = !field->ValueVerified[element_id][value_id];
										}
										// --- Verifica positiva ---
										else
										{
											checked = field->ValueVerified[element_id][value_id];
										}
									}
									catch(...)
									{
										checked = false;
									}
								}
							}
						}
					}
					// --- Elimino i parametri della dipendenza ---
					if ( stream_dep != NULL   ) free( stream_dep  ); // -FREE
					if ( field_dep != NULL    ) free( field_dep   ); // -FREE
					if ( element_dep != NULL  ) free( element_dep ); // -FREE
					if ( value_dep != NULL    ) free( value_dep   ); // -FREE
					// --- Passo ad un eventuale dipendenza successiva ---
					dep_count++;
					free( single_dep ); // -FREE
					if ( checked == true )
					{
						single_dep = NULL;
					}
					else
					{
						single_dep = XMLGetToken( dep, "||", dep_count );
					}
				}
			}
			// Caso AND (separatore ",")
			else
			{
				single_dep = XMLGetToken( dep, ",", dep_count ); // -MALLOC
				// --- Processo ogni singola dipendenza ---
				while ( single_dep != NULL )
				{
					// --- Dipendenza negativa ---
					if ( single_dep[0] == '!' )
					{
						not_dep = true;
						value = (void*)XMLGetToken( single_dep, "!", 1 ); // -MALLOC
						free( single_dep ); // -FREE
						single_dep = (char*)value;
					}
					// --- Dipendenza positiva ---
					else
					{
						not_dep = false;
					}
					// --- Recupero i parametri della dipendenza ---
					stream_dep  = XMLGetToken( single_dep, ".", 0 ); // -MALLOC
					field_dep   = XMLGetToken( single_dep, ".", 1 ); // -MALLOC
					element_dep = XMLGetToken( single_dep, ".", 2 ); // -MALLOC
					value_dep   = XMLGetToken( single_dep, ".", 3 ); // -MALLOC
					// --- Coversione da ASCII a intero ---
					value = XMLASCII2Type( stream_dep, t_u_int_16 ); // -MALLOC
					if ( value != NULL )
					{
						stream_id = (int)*((unsigned __int16 *)value);
						free( value ); // -FREE
					}
					value = XMLASCII2Type( field_dep, t_u_int_16 ); // -MALLOC
					if ( value != NULL )
					{
						field_id = (int)*((unsigned __int16 *)value);
						free( value ); // -FREE
					}
					if ( strcmpi( element_dep, "i" ) == 0 )
					{
						element_id = eleID;
					}
					else
					{
						value = XMLASCII2Type( element_dep, t_u_int_16 ); // -MALLOC
						if ( value != NULL )
						{
							element_id = (int)*((unsigned __int16 *)value);
							free( value ); // -FREE
						}
					}
					if ( strcmpi( value_dep, "i" ) == 0 )
					{
						value_id = eleID;
					}
					else
					{
						value = XMLASCII2Type( value_dep, t_u_int_16 ); // -MALLOC
						if ( value != NULL )
						{
							value_id = (int)*((unsigned __int16 *)value);
							free( value ); // -FREE
						}
					}
					// --- Verfica ---
					stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, stream_id );
					if ( SPVValidDevStream( stream ) )
					{
						field = SPVSearchStreamField( stream->FieldList, field_id );
						if ( SPVValidDevField( field ) )
						{
							if ( field->ValueVerified != NULL )
							{
								if ( element_id < field->Capacity )
								{
									try
									{
										// --- Verifica negativa ---
										if ( not_dep )
										{
											checked = !field->ValueVerified[element_id][value_id];
										}
										// --- Verifica positiva ---
										else
										{
											checked = field->ValueVerified[element_id][value_id];
										}
									}
									catch(...)
									{
										checked = false;
									}
								}
							}
						}
					}
					// --- Elimino i parametri della dipendenza ---
					if ( stream_dep != NULL   ) free( stream_dep  ); // -FREE
					if ( field_dep != NULL    ) free( field_dep   ); // -FREE
					if ( element_dep != NULL  ) free( element_dep ); // -FREE
					if ( value_dep != NULL    ) free( value_dep   ); // -FREE
					// --- Passo ad un eventuale dipendenza successiva ---
					dep_count++;
					free( single_dep ); // -FREE
					if ( checked == false )
					{
						single_dep = NULL;
					}
					else
					{
						single_dep = XMLGetToken( dep, ",", dep_count );
					}
				}
			}
		}
	}

	return checked;
}

//==============================================================================
/// Funzione per verificare se il valore e' un numero negativo
///
/// \date [16.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVIsNegativeNumber( char * value )
{
	bool	negative	= false;
	char *  minus		= NULL;
	char *	va			= NULL;

	try
	{
		if ( value != NULL ) {
			va = XMLGetToken( value, "-", 1 ); // -MALLOC
			if ( va != NULL ) {
				negative = true;
				free( va ); // -FREE
			}
		}
	}
	catch(...)
	{
		negative = false;
	}

	return negative;
}

//==============================================================================
/// Funzione per ottenere il valore assoluto di un numero
///
/// \date [16.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetAbsValue( char * value )
{
	char * abs = NULL;

	try
	{
		if ( value != NULL ) {
			abs = XMLGetToken( value, "-", 1 ); // -MALLOC
			if ( abs == NULL ) {
				// --- Numero positivo ---
				abs = XMLGetToken( value, "-", 0 ); // -MALLOC
			}
		}
	}
	catch(...)
	{
		abs = NULL;
	}

	return abs;
}

//==============================================================================
/// Funzione per ottenere una stringa di un valore numerico con n cifre intere
///
/// \date [16.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetExpandedNumber( char * value, int len )
{
	char * expanded = NULL;
	int value_len = 0;
	int offset = 0;

	try
	{
		if ( value != NULL ) {
			value_len = strlen( value );
			if ( value_len < len ) {
				expanded = (char*)malloc(sizeof(char)*(len+1)); // -MALLOC
				memset( expanded, 0, sizeof(char)*(len+1) );
				memset( expanded, '0', sizeof(char)*(len) );
				offset = len - value_len;
				memcpy( &expanded[offset], value, sizeof(char)*(value_len+1) );
			}
			else
			{
				expanded = ULStrCpy( NULL, value ); // -MALLOC
			}
		}
	}
	catch(...)
	{
		expanded = NULL;
	}

	return expanded;
}

//==============================================================================
/// Funzione per verificare se un valore �' in un range
///
/// \date [16.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVCheckRange( char * value, char * from, char * to )
{
	bool			check			= true	;
	bool			from_minus		= false	; // From negativo
	bool			to_minus		= false	; // To negativo
	bool			value_minus		= false ; // Valore negativo
	char *			from_va			= NULL	; // Valore assoluto From
	char *			to_va			= NULL	; // Valore assoluto To
	char *			value_va		= NULL	; // Valore assoluto valore
	int				from_len		= 0		;
	int				to_len			= 0		;
	int				value_len		= 0		;
	int				max_len			= 0		;
	char *			from_f			= NULL	;
	char *			to_f			= NULL	;
	char *			value_f			= NULL	;

	try
	{
		// --- Controllo se sono numeri negativi ---
		from_minus	= SPVIsNegativeNumber( from );
		to_minus	= SPVIsNegativeNumber( to );
		value_minus	= SPVIsNegativeNumber( value );
		// --- Recupero i valori assoluti ---
		from_va		= SPVGetAbsValue( from ); 	// -MALLOC
		to_va		= SPVGetAbsValue( to ); 	// -MALLOC
		value_va	= SPVGetAbsValue( value ); 	// -MALLOC
		// --- Recupero lunghezze ---
		from_len	= strlen( from_va );
		to_len		= strlen( to_va );
		value_len	= strlen( value_va );
		// --- Calcolo massima lunghezza ---
		max_len		= (from_len>max_len)?from_len:max_len;
		max_len		= (to_len>max_len)?to_len:max_len;
		max_len		= (value_len>max_len)?value_len:max_len;
		// --- Espando le stringhe a max_len cifre ---
		from_f 		= SPVGetExpandedNumber( from_va, max_len );		// -MALLOC
		to_f		= SPVGetExpandedNumber( to_va, max_len );		// -MALLOC
		value_f		= SPVGetExpandedNumber( value_va, max_len );	// -MALLOC
	}
	catch(...)
	{

	}

	if ( from_f != NULL && to_f != NULL && value_f != NULL ) {
		if ( value_minus ) {
			if ( from_minus ) {
				if ( to_minus ) {
					  if ( strcmpi( value_f, from_f ) > 0 ) check = false;
					  if ( strcmpi( value_f, to_f ) < 0 )	check = false;
				}
				else{
					  if ( strcmpi( value_f, from_f ) > 0 ) check = false;
				}
	    	}
			else{
				check = false;
			}
	    }
	    else{
	    	if ( from_minus ) {
	    		if ( to_minus ) {
					check = false;
				}
				else{
					  if ( strcmpi( value_f, to_f ) > 0 )	check = false;
				}
			}
			else{
				if ( to_minus ) {
					check = false;
				}
	    		else{
					  if ( strcmpi( value_f, from_f ) < 0 ) check = false;
					  if ( strcmpi( value_f, to_f ) > 0 )	check = false;
	    		}
	    	}
		}
	}

	try
	{
		// --- Recupero i valori assoluti ---
		if ( from_va 	!= NULL )	free( from_va ); 	// -FREE
		if ( to_va		!= NULL )	free( to_va ); 		// -FREE
		if ( value_va	!= NULL )	free( value_va ); 	// -FREE
		// --- Recupero i valori formattati ---
		if ( from_f 	!= NULL )	free( from_f ); 	// -FREE
		if ( to_f		!= NULL )	free( to_f ); 		// -FREE
		if ( value_f	!= NULL )	free( value_f ); 	// -FREE
	}
	catch(...)
	{

	}

	return check;
}

//==============================================================================
/// Funzione per ottenere la severit� di un campo di uno stream
/// NOTA: Modifica anche la stringa della descrizione del valore prendendola
///       dall'attributo "des" dell'elemento <value> associato.
///
/// \date [10.05.2012]
/// \author Enrico Alborali
/// \version 0.20
//------------------------------------------------------------------------------
int SPVGetDevFieldSeverity( SPV_DEV_STREAM_FIELD * field, int * max_sev, SPV_DEVICE * device, int streamID )
{
	int					ret_code        = 0     ; // Codice di ritorno
	int					severity        = -1    ; // Livello di severit� del campo
	int					max             = -1    ; // Massimo livello di severit� trovato
	int					other_sev       = -1    ; // Livello di severit� per il caso other
	int					value_type      = 0     ; // Tipo di valore
	XML_ELEMENT *		field_e         = NULL  ; // Ptr a elemento XML del campo
	char *				exact_value     = NULL  ; // Ptr a stringa valore esatto
	char *				from_value      = NULL  ; // Ptr a stringa valore di partenza
	char *				to_value        = NULL  ; // Ptr a stringa valore di arrivo
	XML_ELEMENT *		value_e         = NULL  ; // Ptr a elemento XML di un valore
	bool				found           = false ; // Flag di almeno una corrispondenza valore trovata
	unsigned char *		bin_value       = NULL  ; // Ptr a valore binario (per maschere)
	int					bin_len         = 0     ; // Lunghezza in byte del valore binario
	char *				mask_value      = NULL  ; // Ptr a stringa valore maschera
	unsigned char		mask_byte       = 0     ; // Singolo byte per la maschera
	unsigned char		bin_byte        = 0     ; // Singolo byte per un valore binario
	char *				value           = NULL  ; // Ptr alla pos-esima stringa del valore in ASCII
	char *				description     = NULL  ; // Stringa della descrizione valore
	char *				single_des      = NULL  ; // Stringa di appoggio per creare la descrizione
	int					single_sev      = -1    ; // Variabile di appoggio per calcolare la severit�
	char *				other_des       = NULL  ; //
	int					eventID         = 0     ; //
	char *				note            = NULL  ; //
	int					fun_code        = 0     ; //
	int					arrayID         = -1    ; //
	bool				verified        = false ; // Flag di singola corrispondeza valore verificata
	int					v               = 0     ; // Indice di appoggio per scorrere in un array di flag
	int 				otherID         = 0     ; // ID della verifica di tipo OTHER per un campo
	bool *				value_vers      = NULL  ; // Array di flag di appoggio
	bool            	value_verified  = false ; // Flag di verifica valore di appoggio
	bool            	prev_verified   = false ; // Flag di precedente verifica valore
	bool            	other_prev_ver  = false ; // Flag di precedente verifica valore (solo caso OTHER)
	unsigned __int8		i8sev           = 255  	; //
	char *				sev_str         = NULL	; //
	bool				deps_verified   = false ; //
	char *				value_ASCII		= NULL	; // Valore convertito in formato ASCII
	int					element_index	= 0		; // Indice dell'elemento di una sequenza
	unsigned __int64	masked_value	= 0		; //
	double				dvalue			= 0.0	; //
	double				dvalue1			= 0.0	; //
	double				dvalue2			= 0.0	; //
	float				fvalue			= 0.0	; //
	float				fvalue1			= 0.0	; //
	float				fvalue2			= 0.0	; //
	char *				svalue			= NULL	; //
	char *				indexed			= NULL	;
	unsigned __int64	two_compl_check	= 0		;
	unsigned __int64	two_compl_xor	= 0		;
	double				dcheck			= 0.0	;
	double				dxor			= 0.0	;
	bool				two_compl		= false	;

	if ( field != NULL )
	{
		for ( int c = 0; c < field->Capacity; c++ )
		{
			// --- Se c'� un array ---
			if ( field->Capacity > 1 )
			{
				arrayID = c;
			}
			else
			{
				arrayID = -1;
			}
			// --- Resetto il flag di severit� (del campo) cambiata ---
			field->SevChanged[c] = false;
			description = NULL; // Resetto il ptr della descrizione del field c-esimo
			severity = -1;
			value = field->Value[c];
			// --- Conversione dei campi non-ASCII ---
			if ( !field->ASCII )
			{
				value_ASCII = XMLType2ASCII( value, field->Type, field->Len ); // -MALLOC
				value = value_ASCII;
			}
			value_vers = field->ValueVerified[c]; // Prendo l'array dell'elemento c-esimo
			if ( value != NULL ) // NOTA: value � una stringa. Il valore deve essere convertito in formato ASCII
			{
				field_e = field->Def;
				if ( field_e != NULL )
				{
					value_e = field_e->child; // Prendo il primo child
					v = 0; // Parto dal primo <value/>
					while ( value_e != NULL )
					{
						deps_verified = SPVCheckValueDeps( device, XMLGetValue( value_e, "dep" ), c );
						value_verified = false;
						value_type = SPVGetFieldValueType( value_e );
						switch ( value_type )
						{
							// --- Caso tipo EXACT ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_EXACT:
								exact_value = XMLGetValue( value_e, "equal" );
								if ( strcmpi( value, exact_value ) == 0 && deps_verified )
								{
									found = true; // Almeno un valore e' stato verificato
									value_verified = true; // Il valore e' verificato
									// --- Estraggo la severit� dello stato ---
									single_sev = XMLGetValueInt( value_e, "sev" );
									// --- Calcolo la severit� finale dello stato ---
									severity = ( single_sev > severity ) ? single_sev : severity;
									// --- Estraggo la descrizione associata ---
									single_des = XMLGetValue( value_e, "des" );
									// --- Aggiungo una stringa alla descrizione ---
									description = SPVAddDescription( description, single_des );
									// --- Aggiungo la severita' ---
									i8sev       = (unsigned __int8) single_sev;
									sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
									description = ULAddText( description, "=" );
									description = ULAddText( description, sev_str );
									free( sev_str ); // -FREE
									// --- Estraggo il codice di evento ---
									///eventID = XMLGetValueInt( value_e, "eID" );
									// --- Estraggo le note sull'evento ---
									///note = XMLGetValue( value_e, "note" );
									// --- Verifico se il value era precedentemente verificato ---
									prev_verified = value_vers[v];
									// --- Se il valore provoca un evento e la severit� o il valore sono cambiati
									if ( eventID > 0 && !prev_verified )
									{
										// --- Report a livello di value ---
										///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, single_sev, eventID, single_des, note );
									}
								}
							break;
							// --- Caso tipo RANGE ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_RANGE:
								from_value  = XMLGetValue( value_e, "from" );
								to_value    = XMLGetValue( value_e, "to" );
								verified = SPVCheckRange( value, from_value, to_value );
								if ( verified && deps_verified )
								{
									found = true; // Almeno un valore e' stato verificato
									value_verified = true; // Il valore e' verificato
									// --- Estraggo la severit� dello stato ---
									single_sev = XMLGetValueInt( value_e, "sev" );
									// --- Calcolo la severit� finale dello stato ---
									severity = ( single_sev > severity ) ? single_sev : severity;
									// --- Estraggo la descrizione associata ---
									single_des = XMLGetValue( value_e, "des" );
									// --- Aggiungo una stringa alla descrizione ---
									description = SPVAddDescription( description, single_des );
									// --- Aggiungo la severita' ---
									i8sev       = (unsigned __int8) single_sev;
									sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
									description = ULAddText( description, "=" );
									description = ULAddText( description, sev_str );
									free( sev_str ); // -FREE
									// --- Estraggo il codice di evento ---
									///eventID = XMLGetValueInt( value_e, "eID" );
									// --- Estraggo le note sull'evento ---
									///note = XMLGetValue( value_e, "note" );
									// --- Verifico se il value era precedentemente verificato ---
									prev_verified = value_vers[v];
									if ( eventID > 0 && !prev_verified )
									{
										// --- Report a livello di value ---
										///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, severity, eventID, single_des, note );
									}
								}
							break;
							// --- Caso tipo MASK ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_MASK:
								indexed = XMLGetValue( value_e, "index" );
								// Caso con indice sequenza
								if (indexed != NULL && field->Len > 1)
								{
									element_index = XMLGetValueInt( value_e, "index" );
									bin_value = (unsigned char*) XMLIndexedASCII2Type( value, field->Type, element_index ); // -MALLOC
									bin_len   = XMLByteSizeOf( field->Type );
								}
								// Caso senza indice sequenza
								else
								{
									bin_value = (unsigned char*) XMLASCII2Type( value, field->Type, field->Len ); // -MALLOC
									bin_len   = XMLByteSizeOf( field->Type )*field->Len;
								}
								mask_value  = XMLGetValue( value_e, "mask" );
								verified = true; // Di default la maschera � verificata
								// Ciclo di verifica maschera AND
								for ( int B = 0; B < bin_len; B++ )
								{
									bin_byte = bin_value[bin_len-1-B];
									mask_byte = 0;
									for ( int b = 0; b < 8; b++ )
									{
										mask_byte = mask_byte << 1;
										if ( mask_value[(B*8)+b] == '1' ) mask_byte++;
									}
									if ( (bin_byte & mask_byte) != mask_byte )
									{
										verified = false;
									}
								}
								if ( verified && deps_verified )
								{
									found = true; // Almeno un valore e' stato verificato
									value_verified = true; // Il valore e' verificato
									// --- Estraggo la severit� dello stato ---
									single_sev = XMLGetValueInt( value_e, "sev" );
									// --- Calcolo la severit� finale dello stato ---
									severity = ( single_sev > severity ) ? single_sev : severity;
									// --- Estraggo la descrizione associata ---
									single_des = XMLGetValue( value_e, "des" );
									// --- Aggiungo una stringa alla descrizione ---
									description = SPVAddDescription( description, single_des );
									// --- Aggiungo la severita' ---
									i8sev       = (unsigned __int8) single_sev;
									sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
									description = ULAddText( description, "=" );
									description = ULAddText( description, sev_str );
									free( sev_str ); // -FREE
									// --- Estraggo il codice di evento ---
									///eventID = XMLGetValueInt( value_e, "eID" );
									// --- Estraggo le note sull'evento ---
									///note = XMLGetValue( value_e, "note" );
									// --- Verifico se il value era precedentemente verificato ---
									prev_verified = value_vers[v];
									if ( eventID > 0 && !prev_verified )
									{
										// --- Report a livello di value ---
										///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, severity, eventID, single_des, note );
									}
								}
								free( bin_value ); // -FREE
							break;
							// --- Caso tipo NXOR ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_NXOR:
								indexed = XMLGetValue( value_e, "index" );
								// Caso con indice sequenza
								if (indexed != NULL && field->Len > 1)
								{
									element_index = XMLGetValueInt( value_e, "index" );
									bin_value = (unsigned char*) XMLIndexedASCII2Type( value, field->Type, element_index ); // -MALLOC
									bin_len   = XMLByteSizeOf( field->Type );
								}
								// Caso senza indice sequenza
								else
								{
									bin_value = (unsigned char*) XMLASCII2Type( value, field->Type, field->Len ); // -MALLOC
									bin_len   = XMLByteSizeOf( field->Type )*field->Len;
								}
								mask_value  = XMLGetValue( value_e, "mask" );
								verified = true; // Di default la maschera � verificata
								for ( int B = 0; B < bin_len; B++ )
								{
									bin_byte = bin_value[bin_len-1-B];
									for ( int b = 0; b < 8; b++ )
									{
										mask_byte = ( bin_byte >> (7-b) ) & 1;
										if ( mask_value[(B*8)+b] == '0' )
										{
											if ( mask_byte == 1 )
											{
												verified = false;
											}
										}
										else if ( mask_value[(B*8)+b] == '1' )
										{
											if ( mask_byte == 0 )
											{
												verified = false;
											}
										}
										// Se nella maschera ho un bit 'X' ignoro il valore del bit
									}
								}
								if ( verified && deps_verified )
								{
									found = true; // Almeno un valore e' stato verificato
									value_verified = true; // Il valore e' verificato
									// --- Estraggo la severit� dello stato ---
									single_sev = XMLGetValueInt( value_e, "sev" );
									// --- Calcolo la severit� finale dello stato ---
									severity = ( single_sev > severity ) ? single_sev : severity;
									// --- Estraggo la descrizione associata ---
									single_des = XMLGetValue( value_e, "des" );
									// --- Aggiungo una stringa alla descrizione ---
									description = SPVAddDescription( description, single_des );
									// --- Aggiungo la severita' ---
									i8sev       = (unsigned __int8) single_sev;
									sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
									description = ULAddText( description, "=" );
									description = ULAddText( description, sev_str );
									free( sev_str ); // -FREE
									// --- Estraggo il codice di evento ---
									///eventID = XMLGetValueInt( value_e, "eID" );
									// --- Estraggo le note sull'evento ---
									///note = XMLGetValue( value_e, "note" );
									// --- Verifico se il value era precedentemente verificato ---
									prev_verified = value_vers[v];
									if ( eventID > 0 && !prev_verified )
									{
										// --- Report a livello di value ---
										///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, severity, eventID, single_des, note );
									}
								}
								free( bin_value ); // -FREE
							break;
							// --- Caso tipo EXTRACT ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_EXTRACT:
								verified = true; // Di default questo caso e' verificato
								// Caso sequenza
								if (field->Len > 1)
								{
									element_index = XMLGetValueInt( value_e, "index" );
									bin_value = (unsigned char*) XMLIndexedASCII2Type( value, field->Type, element_index ); // -MALLOC
								}
								else
								{
									bin_value = (unsigned char*) XMLASCII2Type( value, field->Type ); // -MALLOC
								}
								// Applico maschera di bit
								mask_value  = XMLGetValue( value_e, "mask" );
								masked_value = 0;
								// Se c'e' una maschera di bit da applicare
								if (mask_value != NULL)
								{
									bin_len   = XMLByteSizeOf( field->Type );
									for ( int B = 0; B < bin_len; B++ )
									{
										bin_byte = bin_value[bin_len-1-B];
										for ( int b = 0; b < 8; b++ )
										{
											masked_value = masked_value << 1;
											mask_byte = ( bin_byte >> (7-b) ) & 1;
											if ( mask_value[(B*8)+b] == '1' && mask_byte == 1 )
											{
												masked_value++;
											}
										}
									}
									dvalue = (double)masked_value;
								}
								else
								{
									dvalue = _Doublize( bin_value, field->Type, false );
								}
								// Applico Complemento a 2
								two_compl = XMLGetValueBool(value_e, "2compl");
								if (two_compl == true) {
									bin_len   = XMLByteSizeOf( field->Type );
									for ( int B = 0; B < bin_len; B++ )
									{
										if (B == 0) {
											two_compl_check = 0x7F;
											two_compl_xor = 0xFF;
										}
										else
										{
											two_compl_check *= 0xFF;
											two_compl_check += 0xFF;
											two_compl_xor *= 0xFF;
											two_compl_xor += 0xFF;
										}
									}
									dcheck = (double)two_compl_check;
									dxor = (double)two_compl_xor;
									if (dvalue > dcheck) {
										dvalue = -(dxor-dvalue+1);
									}
								}
								if ( verified && deps_verified )
								{
									found = true; // Almeno un valore e' stato verificato
									value_verified = true; // Il valore e' verificato
									// --- Estraggo la severit� dello stato ---
									single_sev = XMLGetValueInt( value_e, "sev" );
									// --- Calcolo la severit� finale dello stato ---
									severity = ( single_sev > severity ) ? single_sev : severity;
									// --- Estraggo la descrizione associata ---
									single_des = XMLGetValue( value_e, "des" );
									// --- Applico eventuali fattori ---
									dvalue = _ApplyFactor( XMLGetValue( value_e, "factor" ), dvalue );
									// --- Applico eventuali formattazioni della descrizione ---
									svalue = _ApplyFormat( single_des, dvalue ); // -MALLOC
									// --- Aggiungo una stringa alla descrizione ---
									description = SPVAddDescription( description, svalue );
									// --- Aggiungo la severita' ---
									i8sev       = (unsigned __int8) single_sev;
									sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
									description = ULAddText( description, "=" );
									description = ULAddText( description, sev_str );
									free( sev_str ); // -FREE
									if (svalue != NULL) free( svalue ); // -FREE                                      
									// --- Estraggo il codice di evento ---
									///eventID = XMLGetValueInt( value_e, "eID" );
									// --- Estraggo le note sull'evento ---
									///note = XMLGetValue( value_e, "note" );
									// --- Verifico se il value era precedentemente verificato ---
									prev_verified = value_vers[v];
									if ( eventID > 0 && !prev_verified )
									{
										// --- Report a livello di value ---
										///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, severity, eventID, single_des, note );
									}
								}
								free( bin_value ); // -FREE
							break;
							// --- Caso tipo 2(DUBLE)EXTRACT ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_2EXTRACT:
								verified = true; // Di default questo caso e' verificato
								// --- Valore 1 (LSB) ---
								// Caso sequenza
								if (field->Len > 1)
								{
									element_index = XMLGetValueInt( value_e, "index1" );
									bin_value = (unsigned char*) XMLIndexedASCII2Type( value, field->Type, element_index ); // -MALLOC
								}
								else
								{
									bin_value = (unsigned char*) XMLASCII2Type( value, field->Type ); // -MALLOC
								}
								mask_value  = XMLGetValue( value_e, "mask1" );
								masked_value = 0;
								// Se c'e' una maschera di bit da applicare
								if (mask_value != NULL)
								{
									bin_len   = XMLByteSizeOf( field->Type );
									for ( int B = 0; B < bin_len; B++ )
									{
										bin_byte = bin_value[bin_len-1-B];
										for ( int b = 0; b < 8; b++ )
										{
											masked_value = masked_value << 1;
											mask_byte = ( bin_byte >> (7-b) ) & 1;
											if ( mask_value[(B*8)+b] == '1' && mask_byte == 1 )
											{
												masked_value++;
											}
										}
									}
									dvalue1 = (double)masked_value;
								}
								else
								{
									dvalue1 = _Doublize( bin_value, field->Type, false );
								}
								if ( bin_value != NULL ) free( bin_value ); // -FREE
								// --- Valore 2 (MSB) ---
								// Caso sequenza
								if (field->Len > 1)
								{
									element_index = XMLGetValueInt( value_e, "index2" );
									bin_value = (unsigned char*) XMLIndexedASCII2Type( value, field->Type, element_index ); // -MALLOC
								}
								else
								{
									bin_value = (unsigned char*) XMLASCII2Type( value, field->Type ); // -MALLOC
								}
								mask_value  = XMLGetValue( value_e, "mask2" );
								masked_value = 0;
								// Se c'e' una maschera di bit da applicare
								if (mask_value != NULL)
								{
									bin_len   = XMLByteSizeOf( field->Type );
									for ( int B = 0; B < bin_len; B++ )
									{
										bin_byte = bin_value[bin_len-1-B];
										for ( int b = 0; b < 8; b++ )
										{
											masked_value = masked_value << 1;
											mask_byte = ( bin_byte >> (7-b) ) & 1;
											if ( mask_value[(B*8)+b] == '1' && mask_byte == 1 )
											{
												masked_value++;
											}
										}
									}
									dvalue2 = (double)masked_value;
								}
								else
								{
									dvalue2 = _Doublize( bin_value, field->Type, false );
								}
								if ( bin_value != NULL ) free( bin_value ); // -FREE
								if ( verified && deps_verified )
								{
									found = true; // Almeno un valore e' stato verificato
									value_verified = true; // Il valore e' verificato
									// --- Estraggo la severit� dello stato ---
									single_sev = XMLGetValueInt( value_e, "sev" );
									// --- Calcolo la severit� finale dello stato ---
									severity = ( single_sev > severity ) ? single_sev : severity;
									// --- Estraggo la descrizione associata ---
									single_des = XMLGetValue( value_e, "des" );
									// --- Applico eventuali fattori ---
									dvalue1 = _ApplyFactor( XMLGetValue( value_e, "factor1" ), dvalue1 );
									dvalue2 = _ApplyFactor( XMLGetValue( value_e, "factor2" ), dvalue2 );
									// --- Sommo i due valori estratti ---
									dvalue = dvalue1 + dvalue2;
									// --- Applico eventuali formattazioni della descrizione ---
									svalue = _ApplyFormat( single_des, dvalue ); // -MALLOC
									// --- Aggiungo una stringa alla descrizione ---
									description = SPVAddDescription( description, svalue );
									// --- Aggiungo la severita' ---
									i8sev       = (unsigned __int8) single_sev;
									sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
									description = ULAddText( description, "=" );
									description = ULAddText( description, sev_str );
									free( sev_str ); // -FREE
									if (svalue != NULL) free( svalue ); // -FREE
									// --- Estraggo il codice di evento ---
									///eventID = XMLGetValueInt( value_e, "eID" );
									// --- Estraggo le note sull'evento ---
									///note = XMLGetValue( value_e, "note" );
									// --- Verifico se il value era precedentemente verificato ---
									prev_verified = value_vers[v];
									if ( eventID > 0 && !prev_verified )
									{
										// --- Report a livello di value ---
										///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, severity, eventID, single_des, note );
									}
								}
							break;
							// --- Caso tipo OTHER ---
							case SPV_APPLIC_FIELD_VALUE_TYPE_OTHER:
								otherID = v; // Salvo l'ID del caso Other
								other_sev = XMLGetValueInt( value_e, "sev" );
								// --- Estraggo la descrizione associata ---
								other_des = XMLGetValue( value_e, "des" );
								// --- Estraggo il codice di evento ---
								///eventID = XMLGetValueInt( value_e, "eID" );
								// --- Estraggo le note sull'evento ---
								///note = XMLGetValue( value_e, "note" );
								// --- Verifico se il value era precedentemente verificato ---
								other_prev_ver = value_vers[v];
							break;
						}
						// --- Segno se il valore e' stato verificato oppure no ---
						value_vers[v] = value_verified;
						// --- Passo all'elemento successivo (se esiste) ---
						value_e = value_e->next;
						v++;
					}
					if ( !found )
					{
						if ( other_sev != -1 )
						{
							severity = other_sev;
							// --- Aggiungo la stringa alla descrizione ---
							description = SPVAddDescription( description, other_des );
							// --- Aggiungo la severita' ---
							i8sev       = (unsigned __int8) other_sev;
							sev_str     = XMLType2ASCII( &i8sev, t_u_int_8 ); // -MALLOC
							description = ULAddText( description, "=" );
							description = ULAddText( description, sev_str );
							free( sev_str ); // -FREE
							if ( eventID > 0 && !other_prev_ver )
							{
								// --- Report a livello di value (Livello 1) ---
								///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, severity, eventID, other_des, note );
							}
							// --- Il valore e' stato verificato (come OTHER) ---
							value_vers[otherID] = true;
						}
						else
						{
							ret_code = SPV_APPLIC_UNKNOWN_SEVERITY;
						}
					}
				}
				else
				{
					ret_code = SPV_APPLIC_FIELD_DEF_NOT_FOUND;
				}
				// --- Cancellazione di un eventuale valore converito in ASCII
				if ( value_ASCII != NULL ) {
					try
					{
						free(value_ASCII); // -FREE
					}
					catch(...)
					{

					}
					value_ASCII = NULL;
				}
			}
			else
			{
				ret_code = SPV_APPLIC_INVALID_VALUE;
			}
			// --- Controllo se il livello di severit� � cambiato ---
			if ( field->SevLevel[c] != severity )
			{
				field->SevChanged[c] = true;
			}
			field->SevLevel[c] = severity;
			// --- Cancello un'eventuale descrizione precedente ---
			if ( field->Descr[c] != NULL ) free( field->Descr[c] );
			// --- Copio la nuova descrizione per il campo c-esimo ---
			field->Descr[c] = description;
			// --- Recpuero l'ID dell'evento a livello di field ---
			///eventID = SPVGetFieldEventID( device, streamID, field->ID, field->SevLevel[c] );
			// --- Se il field genera eventi ---
			if ( eventID > 0 && ( field->ValueChanged[c] || field->SevChanged[c] ) )
			{
				// --- Report a livello di field (nel caso di array uno per ogni elemento c-esimo) ---
				///fun_code = SPVReportDevEvent( device, streamID, field->ID, arrayID, field->SevLevel[c], eventID, field->Descr[c], NULL );
			}
			// --- Ricalcolo la massima severit� per tutto l'array ---
			max = ( max > severity ) ? max : severity;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_FIELD;
	}

	*max_sev = max;
	return ret_code;
}

//==============================================================================
/// Funzione per verificare se e' stato registrato un ACK
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVIsDevAck( SPV_DEV_ACK * ack_list, int ack_num, unsigned __int64 DevID, int StrID, int FieldID )
{
	bool			is_ack		= false;
	SPV_DEV_ACK		ack;

	try
	{
		// --- Ciclo sulla lista ACK ---
		for ( int a = 0; a < ack_num; a++ )
		{
			ack = ack_list[a];
			if (ack.DevID == DevID) {
				if (StrID == -1) {
					if (ack.StrID == -1) {
						is_ack = true;
					}
				}
				else
				{
					if (ack.StrID == StrID)
					{
						if (FieldID == -1) {
							if (ack.FieldID == -1) {
								is_ack = true;
							}
						}
						else
						{
							if (ack.FieldID == FieldID) {
                                is_ack = true;
							}
                        }
					}
                }
			}
        }
	}
	catch(...)
	{
		is_ack = false;
    }

	/* DEBUG
	if (is_ack == true)
		SPVReportApplicEvent( NULL, 1, 138, "ACK trovato.", "SPVIsDevAck()" );
    */
	return is_ack;
}

//==============================================================================
/// Funzione per ottenere la severit� di uno stream
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.13
//------------------------------------------------------------------------------
int SPVGetDevStreamSeverity( SPV_DEV_STREAM * stream, SPV_DEVICE * device, SPV_DEV_ACK * ack_list, int ack_num )
{
	int                     ret_code        = 0   ; // Codice di ritorno
	int                     fun_code        = 0   ; // Codice di ritorno
	int                     severity        = -1  ; // Livello di severit�
	int                     field_sev       = -1  ; // Massimo livello di severit� del campo
	SPV_DEV_STREAM_FIELD  * field           = NULL; // Ptr a campo dello stream
	int                     streamID        = 0   ; // ID dello stream
	int                     stream_sevlevel = -1  ; // Livello di severit� dello stream
	int                     stream_eventID  = 0   ; // ID dell'evento dello stream
	char                  * stream_des      = NULL; // Descrizione dell'evento stream
	char                  * description     = NULL; //
	char                  * arrayID_str     = NULL; //

	if ( device != NULL )
	{
		if ( stream != NULL ) // Controllo il ptr alla struttura stream
		{
			stream->SevChanged = false; // Resetto il flag di livello di severit� cambiato

			//*DEBUG*/ SPVReportApplicEvent( NULL, stream->SevLevel, 253, stream->Name, "SPVIsDevAckSPVGetDevStreamSeverity()" );

			if ( stream->Buffer.Ptr != NULL && stream->Buffer.Len > 0 ) // Controllo la validit� del buffer
			{
				if ( stream->FieldList != NULL && stream->FieldNum > 0 ) // Controllo la validit� dei campi del frame
				{
					field = stream->FieldList;
					while ( field != NULL )
					{
						ret_code = SPVGetDevFieldSeverity( field, &field_sev, device, stream->ID );
						if ( ret_code == 0 )
						{
							// --- Ciclo su un eventuale array ---
							for ( int e = 0; e < field->Capacity; e++ )
							{
								// --- Controllo se il livello di severit� del campo � cambiato ---
								if ( field->SevChanged[e] )
								{
									stream->SevChanged = true; // Imposto il flag di severit� cambiata
								}
                                // --- Se sono in un array di field (campi) ---
                                if ( field->Capacity > 1 )
								{
									// --- Aggiungo l'indice dell'elemento dell'array ---
									arrayID_str = XMLType2ASCII( &e, t_u_int_16 ); // -MALLOC

									description = SPVAddDescription( description, arrayID_str ); // -MALLOC
									description = ULAddText( description, ":" );                // -MALLOC
									description = ULAddText( description, field->Descr[e] );    // -MALLOC

									free( arrayID_str ); // -FREE
								}
								// --- Se ho un singolo field (non un array) ---
								else
								{
									// --- Aggiungo la descrizione del campo ---
									description = SPVAddDescription( description, field->Descr[e] ); // -MALLOC
								}
							}
							// Se esiste un ACK su questo field
							if (SPVIsDevAck( ack_list, ack_num, device->DevID, stream->ID, field->ID ))
								severity = ( severity > 0 ) ? severity : 0;
                            // Altrimenti propago la severita' allo stream
							else
								severity = ( severity > field_sev ) ? severity : field_sev;
                            // --- Vado al field successivo ---
                            field = field->Next;
						}
						else
						{
							field = NULL; // Fine del ciclo while
						}
					}
					// --- Salvo la nuova descrizione dello stato dello stream ---
					stream->Description = XMLStrCpy( stream->Description, description );
					// === Inizio Gestione Evento ===
					streamID = stream->ID;
					stream_sevlevel = severity;

					//*DEBUG*/ SPVReportApplicEvent( NULL, stream_sevlevel, 254, stream->Name, "SPVIsDevAckSPVGetDevStreamSeverity()" );

					// --- Recupero ID evento a livello di stream ---
					/* stream_eventID = SPVGetStreamEventID( device, streamID, stream_sevlevel );
					// --- Se il livello di severit� dello stream � cambiato o se il buffer dello stream � cambiato ---
					if ( stream_eventID > 0 && ( stream->SevChanged || stream->BufferChanged ) )
					{
						// --- Recupero la descrizione dello stato della periferica ---
						stream_des = stream->Description;
						// --- Report a livello di stream ---
						 fun_code = SPVReportDevEvent( device, streamID, -1, -1, stream_sevlevel, stream_eventID, stream_des, NULL );
					}
					*/
					// === Fine Gestione Evento ===
				}
				else
				{
					ret_code = SPV_APPLIC_INVALID_FIELD_LIST;
				}
			}
			else
			{
				// --- Se non ho ricevuto alcun buffer riferito allo stream ---
				description = SPVAddDescription( description, "Vuoto" ); // -MALLOC
				// --- Salvo la nuova descrizione dello stato dello stream ---
				stream->Description = XMLStrCpy( stream->Description, description );
				stream->SevLevel = SPV_APPLIC_SEVLEVEL_UNKNOWN;
			}
			stream->SevLevel = severity;
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_STREAM;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_DEVICE;
	}
	// --- BUGFIX 20070824 by Enrico ---
	try
	{
		if ( description != NULL )
		{
			free( description ); // -FREE
		}
	}
	catch(...)
	{

	}
	// ---
	return ret_code;
}

//==============================================================================
/// Funzione per calcolare il livello di severit� di una periferica
///
/// \date [02.03.2012]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
int SPVGetDevSeverity( SPV_DEVICE * device, SPV_DEV_ACK * ack_list, int ack_num )
{
	int               ret_code        = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
	int               fun_code        = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno funzione
	int               sev_level       = -1    ;
	SPV_DEV_STREAM  * stream          = NULL  ;
	bool              sev_changed     = false ;
	bool              stream_changed  = false ;
	char            * description     = NULL  ; // Stringa di descrizione riepilogativa stato
	int               eventID         = 0     ; // ID dell'evento

	// --- Accedo ai dati del sistema ---
	SPVEnterSystemData("SPVGetDevSeverity");

	try
	{
		if ( device != NULL )
		{
			//*DEBUG*/ SPVReportApplicEvent( NULL, device->SupStatus.Level, 333, device->Name, "SPVGetDevSeverity()" );

			stream = (SPV_DEV_STREAM*)device->StreamList; // Parto dal primo stream
			while ( stream != NULL ) // Ciclo sugli stream
			{
				// Se esiste un ACK su questo stream
				if (SPVIsDevAck( ack_list, ack_num, device->DevID, stream->ID, -1 ))
					sev_level = ( sev_level > 0 ) ? sev_level : 0;
				// Altrimenti propago la severita' alla periferica
				else
					// --- Ricalcolo il nuovo livello di severit� della periferica ---
					sev_level = (stream->SevLevel > sev_level)?stream->SevLevel:sev_level;

				//*DEBUG*/ SPVReportApplicEvent( NULL, stream->SevLevel, 200+stream->ID, device->Name, "Singolo stream di device" );

				// --- Utilizzo la descrizione dello stream solo se � dichiarato visibile ---
				if ( stream->Visible )
				{
					// --- Aggiungo alla descrizione stato periferica ---
					if ( description != NULL )
					{
						description = ULAddText( description, "; " );
					}
					description = ULAddText( description, stream->Name );
					description = ULAddText( description, ": " );
					if ( stream->Description != NULL )
					{
						description = ULAddText( description, stream->Description );
					}
					else
					{
						// --- Lo stream attualmente non ha una descrizione disponibile ---
						description = ULAddText( description, "n/d" );
					}
					// --- Controllo che lo stream sia cambiato ---
					if ( stream->SevChanged || stream->BufferChanged )
					{
						// --- Setto il flag di stream cambiato ---
						stream_changed = true;
					}
				}
				// --- Passo allo stream successivo ---
				stream = stream->Next;
			}
			// Se esiste un ACK su questa device
			if (SPVIsDevAck( ack_list, ack_num, device->DevID, -1, -1 ))
				sev_level = 9; // Diagnostica non disponibile
			// --- Controllo che il livello finale della severit� periferica sia cambiato ---
			if ( sev_level != device->SupStatus.Level )
			{
				sev_changed = true;
			}
			// --- Assegno il nuovo stato alla periferica (livello e descrizione) ---
			SPVSetDeviceStatus( device, sev_level, NULL );

			//*DEBUG*/ SPVReportApplicEvent( NULL, sev_level, 334, device->Name, "SPVGetDevSeverity()" );

			// === Gestione report evento ===
			/*if ( true || stream_changed || sev_changed )
			{
				// --- Recupero l'ID dell'evento ---
				eventID = SPVGetDeviceEventID( device, sev_level );
				// --- Report a livello di device ---
				fun_code = SPVReportDevEvent( device, 0, -1, -1, sev_level, eventID, description, NULL );
			}
			*/
			// === fine gestione report evento ===
			if ( description != NULL )
			{
				try
				{
					free( description );
				}
				catch(...)
				{
					/* TODO 1 -oEnrico -cError : Gestione dell'errore critico */
				}
			}
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_APPLIC_CRITICAL_ERROR;
	}

  // --- Esco dai dati del sistema ---
  SPVLeaveSystemData();

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione degli stream delle periferiche
///
/// \date [05.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVLoadSystemStreams( SPV_SYSTEM * system )
{
  int ret_code = SPV_APPLIC_FUNCTION_SUCCESS;

  if ( system != NULL )
  {
	ret_code = SPVDoOnDevices( system, SPVLoadDevStreamList );

	if (ret_code == SPV_APPLIC_FUNCTION_SUCCESS)
	{
		ret_code = SPVDoOnDevices(system, SPVDBCleanAllStreams);
	}

  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_SYSTEM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per cancellare i dati delle periferiche del sistema
///
/// \date [31.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeSystemStreams( SPV_SYSTEM * system )
{
  int ret_code = SPV_APPLIC_FUNCTION_SUCCESS;

  if ( system != NULL )
  {
    ret_code = SPVDoOnDevices( system, SPVFreeDevStreamList );
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_SYSTEM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per cercare una struttura stream all'interno di una lista
///
/// \date [29.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_DEV_STREAM * SPVSearchStream( SPV_DEV_STREAM * stream_list, int ID )
{
  SPV_DEV_STREAM  * stream    = NULL;
  SPV_DEV_STREAM  * current   = NULL;

  if ( SPVValidDevStream( stream_list ) )
  {
    current = stream_list; // Parto dal primo stream
    while ( current != NULL ) // Ciclo sugli stream
    {
	  if ( current->ID == ID )
      {
        stream = current;
		current = NULL; // Fine del ciclo while
      }
	  else
      {
        current = current->Next;
      }
    }
  }

  return stream;
}

//==============================================================================
/// Funzione per cercate una struttura field all'interno di una lista
///
/// \date [29.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DEV_STREAM_FIELD * SPVSearchStreamField( SPV_DEV_STREAM_FIELD * field_list, int ID )
{
  SPV_DEV_STREAM_FIELD * field    = NULL;
  SPV_DEV_STREAM_FIELD * current  = NULL;

  if ( SPVValidDevField( field_list ) )
  {
    current = field_list; // Parto dal primo stream
	while ( current != NULL ) // Ciclo sugli stream
    {
      if ( current->ID == ID )
      {
        field = current;
        current = NULL; // Fine del ciclo while
      }
      else
      {
		current = current->Next;
      }
    }
  }

  return field;
}

//==============================================================================
/// Funzione per cercare il payload num-esimo con un determinato ID
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_COMPROT_PAYLOAD * SPVSearchPayload( SPV_COMPROT_PAYLOAD * first, int ID, int num )
{
  SPV_COMPROT_PAYLOAD * payload = NULL;
  SPV_COMPROT_PAYLOAD * current = NULL;
  int                   counter = 0   ;

  if ( first != NULL ) // Controllo se il ptr alla lista payload � valida
  {
    current = first;
    while ( current != NULL ) // Ciclo sui payload
    {
      if ( current->ID == ID ) // Se l'ID corrisponde
      {
        if ( counter == num )
        {
          payload = current;
          current = NULL;
        }
        else
        {
          counter++;
		}
      }
      if ( current != NULL ) current = (SPV_COMPROT_PAYLOAD*) current->Next;
    }
  }

  return payload;
}

//==============================================================================
/// Funzione per contare quanti payload hanno un determinato ID
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCountPayload( SPV_COMPROT_PAYLOAD * first, int ID )
{
  int                   count   = 0   ;
  SPV_COMPROT_PAYLOAD * current = NULL;

  if ( first != NULL ) // Controllo se il ptr alla lista payload � valida
  {
    current = first;
	while ( current != NULL ) // Ciclo sui payload
    {
      if ( current->ID == ID ) // Se l'ID corrisponde
      {
        count++;
      }
      current = (SPV_COMPROT_PAYLOAD*) current->Next;
    }
  }

  return count;
}

//==============================================================================
/// Funzione per calcolare la lungehzza di tutti i payload con un determinato ID
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetPayloadLen( SPV_COMPROT_PAYLOAD * first, int ID )
{
  int                   len     = 0   ;
  SPV_COMPROT_PAYLOAD * current = NULL;

  if ( first != NULL ) // Controllo se il ptr alla lista payload � valida
  {
    current = first;
    while ( current != NULL ) // Ciclo sui payload
	{
      if ( current->ID == ID ) // Se l'ID corrisponde
      {
        len = len + current->Buffer->len;
      }
      current = (SPV_COMPROT_PAYLOAD*) current->Next;
    }
  }

  return len;
}

//==============================================================================
/// Funzione per estrarre uno stream da uno o pi� payload ricevuti
///
/// In caso di errore restituisce una struttura con campi nulli.
///
/// \date [23.06.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
SPV_DEV_STREAM_BUFFER SPVExtractStream( SPV_DEVICE * device, SPV_COMPROT_PAYLOAD * payload, int ID, int type, unsigned int * packet_count )
{
    SPV_DEV_STREAM_BUFFER   stream_buf  = { NULL, 0 };
    char                  * buffer      = NULL; // Buffer dello stream da restituire
    int                     buffer_len  = 0   ; // Lunghezza del buffer dell stream
    int                     count       = 0   ; // Numero di payload da riunire
    int                     streamID    = 0   ; // Codice del comando
    int                     offset      = 0   ; // Offset del buffer dello stream
    SPV_COMPROT_PAYLOAD   * current     = NULL; // Ptr di appoggio per payload
    int                     payload_off = 0   ; // Offset del buffer del payload
    int                     payload_len = 0   ; // Lunghezza del singolo payload da copiare
    int                     payload_size  = 0   ; // Lunghezza totale del payload
    unsigned __int16      * pvalue16      = NULL;
    char                  * value         = NULL;

	if ( payload != NULL )
	{
		buffer_len = SPVGetPayloadLen( payload, ID ); // Lunghezza totale dei payload ID
		count      = SPVCountPayload( payload, ID ); // Numero di payload ID
		if (packet_count != NULL) {
			*packet_count = count;
		}
		switch ( type )
		{
			// --- Caso generico: tutto il payload forma lo stream ---
			case SPV_APP_PAYLOAD_TYPE_GENERIC:
            	payload_off   = 0;
            	payload_size  = buffer_len - ( count * payload_off );
            break;
            // --- Caso normale: ogni payload ha due byte della lunghezza ---
            case SPV_APP_PAYLOAD_TYPE_NORMAL:
				payload_off   = 2;
            	payload_size  = buffer_len - ( count * payload_off );
            break;
            // --- Caso blocco: ogni payload ha due byte per numero blocco e due per lunghezza ---
            case SPV_APP_PAYLOAD_TYPE_BLOCK:
            	payload_off   = 4;
            	payload_size  = buffer_len - ( count * payload_off );
            break;
            // --- Caso DataBlock SSP ---
            case SPV_APP_PAYLOAD_TYPE_SSP_DATABLOCK:
            	payload_off   = 4;
            	payload_size  = buffer_len - ( count * payload_off );
            break;
            // --- Caso DataBlock INTERO (unsigned 16bit) ---
            case SPV_APP_PAYLOAD_TYPE_SSP_INTEGER_DATABLOCK:
            	payload_off   = 4;
            	payload_size  = count * XMLCharSizeOf( t_u_hex_16 );
            break;
            // --- Caso DataBlock STRINGA (NULL terminata) ---
            case SPV_APP_PAYLOAD_TYPE_SSP_NTSTRING_DATABLOCK:
            	payload_off   = 4;
            	payload_size  = buffer_len - ( count * payload_off );
            break;
            // --- Caso DEFAULT ---
            default:
            	payload_off   = 0;
            	payload_size  = buffer_len - ( count * payload_off );
		}
		if ( payload_size > 0 )
		{
			// --- Alloco memoria per il nuovo buffer ---
			buffer = (char*) malloc( sizeof(char) * ( payload_size + 1 ) ); // -MALLOC
			// --- Cancello la memoria del nuovo buffer ---
			memset( buffer, 0, sizeof(char) * ( payload_size + 1 ) );
			if ( buffer != NULL )
			{
				switch ( type )
				{
					// --- Caso DataBlock INTERO (unsigned 16bit) ---
					case SPV_APP_PAYLOAD_TYPE_SSP_INTEGER_DATABLOCK:
						// --- Ciclo sui payload e li aggiungo allo stream ---
                        for ( int p = 0; p < count; p++ )
                        {
                          current = SPVSearchPayload( payload, ID, p );
                          if ( current != NULL )
                          {
                        	if ( current->Buffer != NULL )
                        	{
                        	  // --- Calcolo la lunghezza del valore ---
                        	  payload_len  = XMLCharSizeOf( t_u_hex_16 );
                        	  // --- Converto il valore intero 16 bit ASCII in intero 16 bit non-ASCII ---
                        	  pvalue16 = (unsigned __int16*) XMLASCII2Type( (char*)&current->Buffer->buffer[payload_off], t_u_int_16 ); // -MALLOC
                        	  if ( pvalue16 != NULL )
                        	  {
                        		// --- Converto il valore intero 16 bit non-ASCII in esadecimale 16 bit ASCII ---
                        		value = XMLType2ASCII( (void*)pvalue16, t_u_hex_16 ); // -MALLOC
                        		// --- Elimino il valore di appoggio ---
                        		free( pvalue16 ); // -FREE
                        	  }
                        	  else
							  {
                        		value = NULL;
                        	  }
                        	  if ( value != NULL )
                        	  {
                        		// --- Copio il singolo buffer in quello di tutto lo stream ---
                        		memcpy( (void*)&buffer[offset], (void*)value, payload_len );
                        		// --- Elimino il valore di appoggio ---
                        		free( value ); // -FREE
                        	  }
                        	  else
                        	  {
                        		// --- Copio un buffer nullo ---
                        		memset( (void*)&buffer[offset], 0, payload_len );
                        	  }
                        	  offset += payload_len;
                        	}
                          }
                        }
					break;
		  // --- Caso DataBlock STRINGA (NULL terminata) ---
		  case SPV_APP_PAYLOAD_TYPE_SSP_NTSTRING_DATABLOCK:
			// --- Ciclo sui payload e li aggiungo allo stream ---
			for ( int p = 0; p < count; p++ )
			{
			  current = SPVSearchPayload( payload, ID, p );
			  if ( current != NULL )
              {
                if ( current->Buffer != NULL )
                {
				  if ( count > 0 && offset > 0 )
                  {
                    // --- Aggiungo il separatore ';' ---
                    buffer[offset-1] = ';';
                  }
                  // --- Copio il singolo buffer in quello di tutto lo stream ---
                  payload_len = current->Buffer->len - payload_off;
                  memcpy( (void*)&buffer[offset], (void*)&current->Buffer->buffer[payload_off], payload_len );
                  offset += payload_len;
                }
              }
            }
            payload_size--; // Tolgo il NULL terminatore
          break;
          // --- Caso GENERICO ---
          default:
            // --- Ciclo sui payload e li aggiungo allo stream ---
            for ( int p = 0; p < count; p++ )
            {
              current = SPVSearchPayload( payload, ID, p );
              if ( current != NULL )
              {
                if ( current->Buffer != NULL )
                {
				  // --- Copio il singolo buffer in quello di tutto lo stream ---
				  payload_len = current->Buffer->len - payload_off;
                  memcpy( (void*)&buffer[offset], (void*)&current->Buffer->buffer[payload_off], payload_len );
                  offset += payload_len;
                }
              }
			}
		}
      }
    }
  }

	stream_buf.Ptr = (unsigned char*)buffer;
    stream_buf.Len = payload_size;

    return stream_buf;
}

//==============================================================================
/// Funzione per salvare in RAM il buffer di uno stream di periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.11.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVRAMSaveStream( SPV_DEVICE * device, int streamID, SPV_DEV_STREAM_BUFFER buffer )
{
  int               ret_code  = SPV_APPLIC_FUNCTION_SUCCESS;
  SPV_DEV_STREAM  * stream    = NULL;

  if ( device != NULL ) // Controllo il ptr alla struttura periferica
  {
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL ) // Se ho trovato un valido ptr a struttura stream
	{
      // --- Salvo lo stream nella locazione all'interno della lista ---
      ret_code = SPVImportDevFrameBuffer( stream, (char*)buffer.Ptr, buffer.Len );
    }
    else
    {
	  ret_code = SPV_APPLIC_INVALID_STREAM;
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere dalla RAM il buffer di uno stream di periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [31.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DEV_STREAM_BUFFER SPVRAMGetStream( SPV_DEVICE * device, int streamID )
{
  SPV_DEV_STREAM_BUFFER   stream_buffer = { 0, NULL } ; // Buffer dello stream
  SPV_DEV_STREAM        * stream        = NULL        ; // Ptr alla struttura stream
  int                     fun_code      = SPV_APPLIC_FUNCTION_SUCCESS;

  if ( device != NULL ) // Controllo il ptr alla struttura periferica
  {
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL ) // Se ho trovato un valido ptr a struttura stream
	{
      stream_buffer = stream->Buffer; // Recupero la struttura buffer
    }
    else
    {
	  fun_code = SPV_APPLIC_INVALID_STREAM;
    }
  }
  else
  {
    fun_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return stream_buffer;
}

//==============================================================================
/// Funzione per eliminare uno stream dal DB
///
/// \date [24.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBDeleteStream( SPV_DEVICE * device, int StrID )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS ;

	if ( SPVValidDevice( device ) )
	{
		if ( StrID > 0 )
		{
			ret_code = SPVDBMDeleteStream(device,StrID);
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_STREAM_ID;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_DEVICE;
	}

  return ret_code;
}


//==============================================================================
/// Funzione per salvare nel DB uno stream di periferica
///
/// In caso di errore restituisce un buffer non valido.
///
/// \date [09.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDBSaveStream( SPV_DEVICE * device, SPV_DEV_STREAM * stream )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	if ( SPVValidDevice( device ) ) // Controllo se il ptr a periferica � valido
	{
		if ( SPVValidDevStream( stream ) )
		{
			ret_code = SPVDBMSaveStream( (void*)device, (void*)stream );
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_STREAM;
		}
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare dal DataBase il buffer di uno stream di periferica
///
/// In caso di errore restituisce un buffer non valido.
///
/// \date [25.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.12
//------------------------------------------------------------------------------
SPV_DEV_STREAM_BUFFER SPVDBGetStream( SPV_DEVICE * device, int streamID )
{
	SPV_DEV_STREAM_BUFFER stream_buffer = { 0, NULL }; // Buffer dello stream periferica

	if ( SPVValidDevice( device ) )
	{
		SPVDBMGetStream(device,streamID,&stream_buffer);
	}

	return stream_buffer;
 }

/* Mario: attualmente non serve ed essendo old style � preferibile non utilizzarla!
		  Se bisogner� utilizzarla bisogna provvedere alla migrazione della sue
		  parti che vanno su SPBDManager
//==============================================================================
/// Funzione per salvare nel DataBase un evento di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.11.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDBSaveEvent( SPV_DEVICE * device, SPV_DEV_EVENT * event )
{
  int             ret_code    = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
  int             fun_code    = SPV_DBI_NO_ERROR            ; // Codice di funzione
  SPV_DBI_CONN  * db_conn     = NULL                        ; // Ptr a struttura connessione
  char            query         [SPV_DBI_QUERY_MAX_LENGTH]  ; // Stringa query
  int             query_len   = 0                           ; // Lunghezza stringa query
  char          * devid_value = NULL                        ; // Valore ASCII DevID

  //SPV_DBI_FIELD * field       = NULL                        ; // Ptr a struttura field


  if ( device != NULL ) // Controllo se il ptr a periferica � valido
  {
	db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn; // Recupero la struttura connessione DB
	if ( db_conn != NULL ) // Controllo se il ptr a connssione � valido
	{
	  if ( !db_conn->Active ) // Controllo che la connessione sia aperta
	  {
		fun_code = SPVDBOpenConn( db_conn );
		if ( fun_code == SPV_DBI_NO_ERROR )
		{
		  SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS_DES, NULL );
		}
		else
		{
		  SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_CONNECT_FAILURE, db_conn->LastError, NULL );
		}
	  }
	  // --- Scrittura dello stream nel database ---
	  if ( fun_code == SPV_DBI_NO_ERROR )
	  {
		// --- Recupero il valore dell'UniID ---
		devid_value = SPVGetDeviceUniIDStr( device ); // -MALLOC

		#define QUERY_COLUMNS "UniID,Type,DateTime,EventCode,EventDesc,Data1,Data2,Data3,Data4,Data1Desc,Data2Desc,Data3Desc,Data4Desc,Note"
		#define QUERY_START_STOP "\'"
		#define QUERY_COMMA "\',\'"

		// --- Coversione in formato ASCII ---
		char * type_value = NULL;
		type_value = XMLType2ASCII( (void*)&event->Type, t_u_int_8 ); // -MALLOC
		char * datetime_value = NULL;
		datetime_value = SPVDBGetTime( event->DateTime ); // -MALLOC
		char * eventcode_value = NULL;
		eventcode_value = XMLType2ASCII( (void*)&event->EventCode, t_u_int_8 ); // -MALLOC
		char * data1_value = NULL;
		data1_value = XMLType2ASCII( (void*)&event->Data1, t_u_int_8 ); // -MALLOC
		char * data2_value = NULL;
		data2_value = XMLType2ASCII( (void*)&event->Data2, t_u_int_8 ); // -MALLOC
		char * data3_value = NULL;
		data3_value = XMLType2ASCII( (void*)&event->Data3, t_u_int_8 ); // -MALLOC
		char * data4_value = NULL;
		data4_value = XMLType2ASCII( (void*)&event->Data4, t_u_int_8 ); // -MALLOC

		// --- Preparo la query per scrivere una nuova riga ---
		query[0] = 0;
		query_len = 0;

		query_len = SPVDBAddToQuery( query, query_len, QUERY_START_STOP );
		query_len = SPVDBAddToQuery( query, query_len, devid_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, type_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, datetime_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, eventcode_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, event->EventDesc );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, data1_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, data2_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, data3_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, data4_value );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, event->Data1Desc );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, event->Data2Desc );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, event->Data3Desc );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, event->Data4Desc );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA );
		query_len = SPVDBAddToQuery( query, query_len, event->Note );
		query_len = SPVDBAddToQuery( query, query_len, QUERY_START_STOP );

		// --- Lancio la (query) inserzione di una nuova riga ---
		fun_code = SPVDBWriteRow( db_conn, "events", QUERY_COLUMNS, query, query_len );
		if ( fun_code != SPV_DBI_NO_ERROR )
		{
		  SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, NULL );
		  ret_code = SPV_APPLIC_INSERT_QUERY_FAILURE;
		}

		// --- Elimino le stringhe temporanee ---
		try
		{
		  free( devid_value ); // -FREE
		  free( type_value ); // -FREE
		  free( datetime_value ); // -FREE
		  free( eventcode_value ); // -FREE
		  free( data1_value ); // -FREE
		  free( data2_value ); // -FREE
		  free( data3_value ); // -FREE
		  free( data4_value ); // -FREE
		}
		catch(...)
		{
		  // TODO -oEnrico -cError : Gestione dell'errore critico
		}

		// --- Controllo che la connessione non sia persistente ---
		if ( !db_conn->Persistent )
		{
		  fun_code = SPVDBCloseConn( db_conn );
		  if ( fun_code != SPV_DBI_NO_ERROR && ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
		  {
			SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE, db_conn->LastError, NULL );
			ret_code = SPV_APPLIC_DB_CLOSE_FAILURE;
		  }
		  else
		  {
			SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS_DES, NULL );
		  }
		}
	  }
	}
	else
	{
	  ret_code = SPV_APPLIC_INVALID_DB_CONNECTION;
	}
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}
*/

//==============================================================================
/// Funzione per convertire il valore di un field in un formato stampabile
///
/// \date [26.03.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
char * SPVPrintableFieldConv( SPV_DEV_STREAM_FIELD * field, int arrayID )
{
	char  * print_value         = NULL  ;
	int     print_value_len     = 0     ;
	bool    print_value_copy    = false ;
	int     field_capacity      = 0     ;
	int     field_type          = 0     ;
	bool    field_ascii         = false ;
	int     field_len           = 0     ;
	char  * field_value         = NULL  ;
	void  * field_value_ptr     = NULL  ;
	char  * print_trunc_value	= NULL	;

	if ( SPVValidDevField( field ) )
	{
		// --- Recupero i parametri del field ---
		try
		{
			field_capacity  = field->Capacity;
			field_type      = field->Type;
			field_ascii     = field->ASCII;
			field_len       = field->Len;
		}
		catch(...)
		{
			field_capacity  = 0;
		}
		if ( arrayID >= 0 && arrayID < field_capacity )
		{
			// --- Caso formato binario (viene sostituito con "<dati binari>") ---
			if ( field->Format != NULL && strcmpi( field->Format, "%bin" ) == 0 ) {
				print_value_len = strlen("(dati binari)");
				print_value = (char*)malloc( sizeof(char) * ( print_value_len + 1 ) ); // -MALLOC
				if ( print_value != NULL )
				{
					try
					{
						strcpy( print_value, "(dati binari)" );
					}
					catch(...)
					{
						print_value = NULL;
						print_value_len = 0;
					}
				}
				print_value_copy = true;
			}
			else
			{
				// --- Recupero il valore del field ---
			    try
			    {
			    	field_value = field->Value[arrayID];
			    }
			    catch(...)
				{
			    	field_value = NULL;
			    }
			    // --- Se il ptr al valore e' valido ---
			    if ( field_value != NULL )
			    {
					// --- Caso STRINGA ---
			    	if ( field_type == t_string )
			    	{
			    		// --- Imposto la lunghezza ---
			    		if ( field_len == 0 )
			    		{
			    			try
			    			{
								print_value_len = strlen( field_value );
			    			}
			    			catch(...)
			    			{
								print_value_len = 0;
			    			}
			    		}
			    		else
						{
							print_value_len = field_len;
						}
						if (field->Format != NULL && strcmpi(field->Format,"tlf_string")==0) {
							print_value = _StringUnString( field_value );
							print_value_len = strlen( print_value );
							print_value_copy = true;
						}
					}
					else
					{
			    		// --- Caso ASCII ---
						if ( field_ascii == true )
			    		{
							// --- Caso esadecimale ---
			    			if ( XMLIsHexType( field_type ) == true )
			    			{
			    				// --- Calcolo la lunghezza ---
								print_value_len = XMLCharSizeOf( field_type ) * field_len;
			    				// --- Se ho un solo elemento faccio la conversione a decimale ---
			    				if ( field_len == 1 )
			    				{
									//float fvalue = 0.0;
									double dvalue = 0.0;
			    					char * svalue = NULL;
			    					try
			    					{
										dvalue = _Doublize( field_value, field_type, field_ascii );
										dvalue = _ApplyAddend( field, dvalue );
										dvalue = _ApplyFactor( field, dvalue );
										svalue = _ApplyFormat( field, dvalue ); // -MALLOC
										print_value_copy = true;
										print_value_len = strlen(svalue);
			    					}
			    					catch(...)
			    					{
			    						svalue = NULL;
									}
									print_value       = svalue;
								}
			    			}
			    			// --- Caso decimale o non convertibile ---
			    			else
			    			{
								try
			    				{
			    					print_value_len = strlen( field_value );
			    				}
			    				catch(...)
			    				{
			    					print_value_len = 0;
			    				}
			    			}
			    		}
			    		// --- Caso non-ASCII ---
			    		else
						{
                            // Caso multi elemento
							if (field_len > 1) {
								try
								{
									print_value = XMLType2ASCII( field_value, field_type, field_len ); // -MALLOC
									print_value_copy = true;
									print_value_len = strlen( print_value );
								}
								catch(...)
								{
									print_value_len = 0;
								}
							}
							else
							{
								if ( field->Format != NULL || field->Factor != NULL )
								{
									double dvalue = 0.0;
									char * svalue = NULL;
									try
									{
										dvalue = _Doublize( field_value, field_type, field_ascii );
										dvalue = _ApplyAddend( field, dvalue );
										dvalue = _ApplyFactor( field, dvalue );
										svalue = _ApplyFormat( field, dvalue ); // -MALLOC
										print_value_copy = true;
										print_value_len = strlen(svalue);
									}
									catch(...)
									{
										svalue = NULL;
									}
									print_value       = svalue;
								}
								else
								{
								    // --- Caso esadecimale ---
								    if ( XMLIsHexType( field_type ) == true )
								    {
								    	try
								    	{
								    		print_value = XMLType2ASCII( field_value, XMLHex2DecIntType( field_type )); // -MALLOC
								    		print_value_copy = true;
											print_value_len = strlen( print_value );
								    	}
								    	catch(...)
								    	{
											print_value_len = 0;
								    	}
								    }
								    // --- Caso decimale o binario ---
								    else
									{
								    	try
								    	{
								    		print_value = XMLType2ASCII( field_value, field_type); // -MALLOC
								    		print_value_copy = true;
								    		print_value_len = strlen( print_value );
								    	}
								    	catch(...)
								    	{
								    		print_value_len = 0;
								    	}
									}
								}
							}
						}
					}
				}
			}
			if ( print_value_len > 0 )
			{
				if ( print_value_copy == false )
				{
					print_value = (char*)malloc( sizeof(char) * ( print_value_len + 1 ) ); // -MALLOC
					if ( print_value != NULL )
					{
						try
						{
							strcpy( print_value, field_value );
						}
						catch(...)
						{
							print_value = NULL;
							print_value_len = 0;
						}
					}
				}
				// --- Troncamento (max 1024 caratteri sul DB) ---
				if ( print_value_len > 1022 ) {
					print_trunc_value = (char*)malloc( sizeof(char) * ( 1024 ) ); // -MALLOC
					if ( print_trunc_value != NULL ) {
						try
						{
							memset( print_trunc_value, 0, 1024 ); // Azzero la memoria
							memcpy( print_trunc_value, print_value, 1023 ); // Copio il valore troncato (lascio un carattere x il NULL terminatore)
							free( print_value ); // -FREE
							print_value = print_trunc_value;
							print_trunc_value = NULL;
						}
						catch(...)
						{
							print_value = NULL;
						}
					}
				}
			}
			else
			{
				print_value = NULL;
			}
		}
	}

	return print_value;
}


//==============================================================================
/// Funzione per marchiare come rimosse tutte le periferiche
///
/// \date [29.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDBRemoveAllDevices( void )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPVDBMSigneRemovedAllDevices( );

	return ret_code;
}


//==============================================================================
/// Funzione per salvare nel DB una periferica
///
/// \date [29.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.12
//------------------------------------------------------------------------------
int SPVDBSaveDevice( SPV_DEVICE * device )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( SPVValidDevice( device ) ) // Controllo se il ptr a periferica � valido
	{
		ret_code = SPVDBMSaveDevice( device );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_DEVICE;
	}

	return ret_code;
}

/*
 *
 */
int SPVDBUpdateDevice( SPV_DEVICE * device )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( SPVValidDevice( device ) ) // Controllo se il ptr a periferica � valido
	{
		ret_code = SPVDBMUpdateDevice( device );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare dal DB una periferica
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBGetDevice( SPV_DEVICE * device )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPV_APPLIC_NOT_IMPLEMENTED;

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DB lo stato periferica
///
/// \date [26.10.2010]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
int SPVDBSaveDeviceStatus( SPV_DEVICE * device )
{
	int                 ret_code          = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	// --- Accedo ai dati del sistema ---
	SPVEnterSystemData("SPVDBSaveDeviceStatus");

	ret_code = SPVDBMSaveDeviceStatus( (void*)device, SPVSystem.MultiLangauge );

	// --- Esco dai dati del sistema ---
	SPVLeaveSystemData();

	return ret_code;
}

//==============================================================================
/// Funzione per cancellare nel DB gli stream di una periferica
///
/// \date [26.10.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
int SPVDBCleanAllStreams( SPV_DEVICE * device )
{
	int                 ret_code          = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	// --- Accedo ai dati del sistema ---
	SPVEnterSystemData("SPVDBCleanAllStreams");

	ret_code = SPVDBMCleanAllStreams( (void*)device, SPVSystem.MultiLangauge );

	// --- Esco dai dati del sistema ---
	SPVLeaveSystemData();

	return ret_code;
}

//==============================================================================
/// Funzione per marchiare come rimossi tutti i nodi
///
/// \date [30.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBRemoveAllNodes( void )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPVDBMSigneRemovedAllNodes( );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DB un nodo
///
/// \date [30.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDBSaveNode( SPV_NODE * node )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( SPVValidNode( node ) ) // Controllo se il ptr a nodo � valido
	{
		ret_code = SPVDBMSaveNode( node );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_NODE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare dal DB un nodo
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBGetNode ( SPV_NODE * node )
{
	int ret_code  = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPV_APPLIC_NOT_IMPLEMENTED;

	return ret_code;
}

//==============================================================================
/// Funzione per marchiare come rimosse tutte le zone
///
/// \date [30.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBRemoveAllZones( void )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPVDBMSigneRemovedAllZones( );

	return ret_code;
}


//==============================================================================
/// Funzione per salvare nel DB una zona
///
/// \date [30.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDBSaveZone( SPV_ZONE * zone )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( SPVValidZone( zone ) ) // Controllo se il ptr a nodo � valido
	{
		ret_code = SPVDBMSaveZone( zone );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_ZONE;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per caricare dal DB una zona
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBGetZone( SPV_ZONE * zone )
{
  int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

  ret_code = SPV_APPLIC_NOT_IMPLEMENTED;

  return ret_code;
}

//==============================================================================
/// Funzione per marchiare come rimosse tutte le regioni
///
/// \date [31.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBRemoveAllRegions( void )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPVDBMSigneRemovedAllRegions( );

	return ret_code;
}


//==============================================================================
/// Funzione per salvare nel DB una regione
///
/// \date [31.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDBSaveRegion( SPV_REGION * region )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( SPVValidRegion( region ) ) // Controllo se il ptr a nodo � valido
	{
		ret_code = SPVDBMSaveRegion( region );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_REGION;
	}
	return ret_code;
}


//==============================================================================
/// Funzione per caricare dal DB una regione
///
/// \date [09.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBGetRegion( SPV_REGION * region )
{
	int             ret_code        = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	if ( SPVValidRegion( region ) )
	{
		ret_code = SPV_APPLIC_NOT_IMPLEMENTED;
	}
	else
	{

	}

	return ret_code;
}

//==============================================================================
/// Funzione per marchiare come rimossi tutti i server
///
/// \date [17.12.2009]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
int SPVDBRemoveAllServers( void )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPVDBMSignRemovedAllServers( );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DB un server
///
/// \date [31.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDBSaveServer( SPV_SERVER * server )
{
	int	ret_code = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno

	if ( SPVValidServer( server ) ) // Controllo se il ptr a nodo � valido
	{
		ret_code = SPVDBMSaveServer( server );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SERVER;
	}
	return ret_code;
}
/*{
	int             ret_code        = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	int             fun_code        = SPV_DBI_NO_ERROR            ; // Codice di funzione
	SPV_DBI_CONN  * db_conn         = NULL                        ; // Ptr a struttura connessione
	char            query             [SPV_DBI_QUERY_MAX_LENGTH]  ; // Stringa query
	int             query_len       = 0                           ; // Lunghezza stringa query
	SPV_DBI_FIELD * field           = NULL                        ; // Ptr a struttura field

	char          * SrvID_32_value  = NULL                        ;
	char          * Name_value      = NULL                        ;
	char          * Host_value      = NULL                        ;
	char		  * xml_text		= NULL						  ;

	#define QUERY_COLUMNS       "SrvID,Name,Host,SupervisorSystemXML"
	#define QUERY_START         "=\'"
	#define QUERY_START_STOP    "\'"
	#define QUERY_COMMA         "\',\'"
	#define QUERY_SIMPLE_COMMA  ","
	#define _LOCAL_DATA_COMMA   ","

	if ( server != NULL ) // Controllo se il ptr al SERVER � valido
	{
		db_conn = (SPV_DBI_CONN*)SPVSystem.DBConn; // Recupero la struttura connessione DB
		if ( db_conn != NULL ) // Controllo se il ptr a connssione � valido
		{
			if ( !db_conn->Active ) // Controllo che la connessione sia aperta
			{
				fun_code = SPVDBOpenConn( db_conn );
				if ( fun_code == SPV_DBI_NO_ERROR )
				{
					SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS_DES, NULL );
				}
				else
				{
					SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_CONNECT_FAILURE, db_conn->LastError, NULL );
				}
			}
			// --- Scrittura del SERVER nel database ---
			if ( fun_code == SPV_DBI_NO_ERROR )
			{
				// --- Recupero il file System.xml ---
				xml_text = ULReadASCIIFile(SPVSystem.Source->name);
				// --- Rimuovo eventuali apici ---
				ULStrSubstitute( xml_text, (char)39, (char)96 );
				// --- Recupero SrvID ---
				SrvID_32_value = XMLType2ASCII( (void*) &server->SrvID, t_u_int_32 ); // -MALLOC
				// --- Recupero Name ---
				Name_value = server->Name;
				// --- Recupero Host ---
				Host_value = server->Host;

				// --- Preparo la query per verificare se riga esiste ---
				query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
				query_len = SPVDBAddToQuery( query, query_len, "SELECT SrvID FROM servers WHERE SrvID" );
				query_len = SPVDBAddToQuery( query, query_len, QUERY_START );
				query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value );
				query_len = SPVDBAddToQuery( query, query_len, QUERY_START_STOP );
				// --- Lancio la query ---
				fun_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
				if ( fun_code == SPV_DBI_NO_ERROR )
				{
					field = SPVDBGetField( db_conn, 0, 0 );
					// --- Se la riga � gi� presente nella tabella del DB (la modifico) ---
					if ( field != NULL )
					{
						// --- Preparo la query per scrivere una nuova riga ---
						query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
						query_len = SPVDBAddToQuery( query, query_len, "UPDATE servers SET " );
						query_len = SPVDBAddToQuery( query, query_len, "Name" );
						query_len = SPVDBAddToQuery( query, query_len, QUERY_START );
						query_len = SPVDBAddToQuery( query, query_len, Name_value );
						query_len = SPVDBAddToQuery( query, query_len, QUERY_START_STOP );
						query_len = SPVDBAddToQuery( query, query_len, _LOCAL_DATA_COMMA );
						query_len = SPVDBAddToQuery( query, query_len, "Host" );
						query_len = SPVDBAddToQuery( query, query_len, QUERY_START );
						query_len = SPVDBAddToQuery( query, query_len, Host_value );
						query_len = SPVDBAddToQuery( query, query_len, QUERY_START_STOP );
						query_len = SPVDBAddToQuery( query, query_len, _LOCAL_DATA_COMMA );
						query_len = SPVDBAddToQuery( query, query_len, "SupervisorSystemXML = CONVERT(xml,'" );
						query_len = SPVDBAddToQuery( query, query_len, xml_text );
						query_len = SPVDBAddToQuery( query, query_len, "')" );
						query_len = SPVDBAddToQuery( query, query_len, " WHERE SrvID = " );
						query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value );


			// --- Lancio la query ---
			fun_code = SPVDBQuery( db_conn, query, query_len, NULL, 0 );
			if ( fun_code != SPV_DBI_NO_ERROR )
			{
			  SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
			  ret_code = SPV_APPLIC_UPDATE_QUERY_FAILURE;
			}
		  }
		  // --- Se la riga non esiste (ne creo una nuova) ---
		  else
		  {
			// --- Preparo la query per scrivere una nuova riga ---
			query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
			query_len = SPVDBAddToQuery( query, query_len, QUERY_START_STOP );
			query_len = SPVDBAddToQuery( query, query_len, SrvID_32_value   );
			query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA      );
			query_len = SPVDBAddToQuery( query, query_len, Name_value       );
			query_len = SPVDBAddToQuery( query, query_len, QUERY_COMMA      );
			query_len = SPVDBAddToQuery( query, query_len, Host_value       );
			query_len = SPVDBAddToQuery( query, query_len, "\',"      );
			query_len = SPVDBAddToQuery( query, query_len, "CONVERT(xml,'" );
			query_len = SPVDBAddToQuery( query, query_len, xml_text );
			query_len = SPVDBAddToQuery( query, query_len, "')" );


			// --- Lancio la (query) inserzione di una nuova riga ---
			fun_code = SPVDBWriteRow( db_conn, "servers", QUERY_COLUMNS, query, query_len );
			if ( fun_code != SPV_DBI_NO_ERROR )
			{
			  SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
			  ret_code = SPV_APPLIC_INSERT_QUERY_FAILURE;
			}
		  }
		}
		else
		{
		  SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
		  ret_code = SPV_APPLIC_UPDATE_QUERY_FAILURE;
		}
		// --- Controllo che la connessione non sia persistente ---
		if ( !db_conn->Persistent )
		{
		  // --- Chiusura DB ---
		  fun_code = SPVDBCloseConn( db_conn );
		  if ( fun_code != SPV_DBI_NO_ERROR && ret_code == SPV_APPLIC_FUNCTION_SUCCESS )
		  {
			SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE, db_conn->LastError, NULL );
			ret_code = SPV_APPLIC_DB_CLOSE_FAILURE;
		  }
		  else
		  {
			SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS_DES, NULL );
		  }
		}
		// --- Elimino le stringhe temporanee ---
		try
		{
					if ( SrvID_32_value != NULL ) free( SrvID_32_value ); // -FREE
		}
		catch(...)
		{
		  // TODO -oEnrico -cError : Gestione dell'errore critico
		}
	  }
	}
	else
	{
	  ret_code = SPV_APPLIC_INVALID_DB_CONNECTION;
	}
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_REGION;
  }

  return ret_code;
} */


//==============================================================================
/// Funzione per caricare dal DB un server
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBGetServer( SPV_SERVER * server )
{
	int ret_code = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno

	ret_code = SPV_APPLIC_NOT_IMPLEMENTED;

	return ret_code;
}

//==============================================================================
/// Funzione per salvare i valori di riferimento di un campo
///
/// \date [20.04.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDBSaveFieldReferences( SPV_DEVICE * device, SPV_DEV_STREAM * stream, SPV_DEV_STREAM_FIELD * field )
{
	int		ret_code	    = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	int		fun_code	    = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	char*	ref_dep		    = NULL; // Stringa dipendenze reference
	char*	delta_code	    = NULL; // Stringa che identifica il delta code.
	char*	DeltaStrID	    = NULL;
	char*	DeltaFieldID	= NULL;
	char*	DeltaArrayID	= NULL;

	try
	{
		// --- Controllo device ---
		if ( SPVValidDevice( device ) ) {
			// --- Controllo stream ---
			if ( SPVValidDevStream( stream ) ) {
				// --- Controllo field ---
				if ( SPVValidDevField( field ) ) {
					// --- Recupero la stringa di dipendenze reference del campo ---
					ref_dep = XMLGetValue( field->Def, "ref" );
					// --- Ciclo sugli elementi dell'array del campo ---
					for ( int c = 0; c < field->Capacity; c++ ) {
						try
						{
							// --- Controllo se sono specificate delle dipendenze ---
							if ( ref_dep != NULL ) {
								// --- Verifico le dipendenze dell'elemenpo c-esimo del campo ---
								if ( ( strcmpi( ref_dep, "*" ) == 0 ) || SPVCheckValueDeps( device, ref_dep, c ) ) {
									// --- Recupero i riferimento allo scostamento assoluto (delta) ---
									try
									{
										delta_code = XMLGetValue( field->Def, "delta" );
										if ( delta_code != NULL ) {
											DeltaStrID		= XMLGetToken( delta_code, ".", 0 ); // -MALLOC
											DeltaFieldID	= XMLGetToken( delta_code, ".", 1 ); // -MALLOC
											DeltaArrayID	= XMLGetToken( delta_code, ".", 2 ); // -MALLOC
										}
										else
										{
											DeltaStrID		= NULL;
											DeltaFieldID	= NULL;
											DeltaArrayID	= NULL;
										}
									}
									catch(...)
									{
										SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante il recupero parametri delta", "SPVDBSaveFieldReferences()" );
									}
									try
									{
										// --- Salvo il reference ---
										fun_code = SPVDBMSaveReference( (void*)device, stream->ID, (void*)field, c, DeltaStrID, DeltaFieldID, DeltaArrayID );
										if ( fun_code != SPV_APPLIC_FUNCTION_SUCCESS ) {
											SPVReportApplicEvent( NULL, 2, fun_code, "Errore durante il salvataggio di un singolo reference", "SPVDBSaveFieldReferences()" );
										}
									}
									catch(...)
									{
										SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante il salvataggio di un reference", "SPVDBSaveFieldReferences()" );
									}
									// --- Cancellazione parametri delta ---
									try
									{
										if ( DeltaStrID		!= NULL ) free( DeltaStrID	 ); // -FREE
									    if ( DeltaFieldID	!= NULL ) free( DeltaFieldID ); // -FREE
										if ( DeltaArrayID	!= NULL ) free( DeltaArrayID ); // -FREE
									}
									catch(...)
									{
										SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante la cancellazioni parametri delta", "SPVDBSaveFieldReferences()" );
									}
								}
								// --- Condizione di salvataggio non verificata ---
								else
								{
									try
									{
										// --- Cancello un eventuale reference precedente ---
										fun_code = SPVDBMClearReference( (void*)device, stream->ID, (void*)field, c );
										if ( fun_code != SPV_APPLIC_FUNCTION_SUCCESS ) {
											SPVReportApplicEvent( NULL, 2, fun_code, "Errore durante la cancellazione di un singolo reference", "SPVDBSaveFieldReferences()" );
										}
									}
									catch(...)
									{
										SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione di un reference", "SPVDBSaveFieldReferences()" );
									}
								}
							}
							// --- Dipendenze non verificate ---
							else
							{
								try
							    {
									// --- Cancello un eventuale reference precedente ---
							    	fun_code = SPVDBMClearReference( (void*)device, stream->ID, (void*)field, c );
							    	if ( fun_code != SPV_APPLIC_FUNCTION_SUCCESS ) {
							    		SPVReportApplicEvent( NULL, 2, fun_code, "Errore durante la cancellazione di un singolo reference", "SPVDBSaveFieldReferences()" );
							    	}
							    }
							    catch(...)
							    {
							    	SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante la cancellazione di un reference", "SPVDBSaveFieldReferences()" );
								}
							}
						}
						catch(...)
						{
							SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione un ciclo gestione reference", "SPVDBSaveFieldReferences()" );
						}
					} // for
				}
				else
				{
					ret_code = SPV_APPLIC_INVALID_FIELD;
				}
			}
			else
			{
				ret_code = SPV_APPLIC_INVALID_STREAM;
			}
		}
		else
		{
			ret_code = SPV_APPLIC_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, ret_code, "Eccezione generale", "SPVDBSaveFieldReferences()" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per salvare i valori di riferimento dei campi di uno stream
///
/// \date [14.04.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	SPVDBSaveStreamReferences( SPV_DEVICE * device, SPV_DEV_STREAM * stream )
{
	int						ret_code	= SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	int						fun_code	= SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	SPV_DEV_STREAM_FIELD* 	field		= NULL;

	try
	{
	    // --- Controllo device ---
	    if ( SPVValidDevice( device ) ) {
	    	// --- Controllo stream ---
	    	if ( SPVValidDevStream( stream ) ) {
	    		// --- Ciclo sui field dello stream ---
	    		for ( int f = 0; f < stream->FieldNum ; f++ ) {
	    			try
	    			{
	    				// --- Recupero e controllo il field f-esimo ---
						field = SPVSearchStreamField( stream->FieldList, f );
						if ( SPVValidDevField( field ) )
						{
							// --- Salvo i valori di riferimento dal campo ---
							fun_code = SPVDBSaveFieldReferences( device, stream, field );
							if ( fun_code != SPV_APPLIC_FUNCTION_SUCCESS ) {
								SPVReportApplicEvent( NULL, 2, fun_code, "Errore durante il salvataggio dei reference di un campo", "SPVDBSaveStreamReferences()" );
							}
	    				}
	    				else
	    				{
	    					SPVReportApplicEvent( NULL, 2, SPV_APPLIC_INVALID_FIELD, "Trovato field non valido", "SPVDBSaveStreamReferences()" );
	    				}
					}
					catch(...)
	    			{
						SPVReportApplicEvent( NULL, 2, SPV_APPLIC_FUNCTION_EXCEPTION, "Eccezione durante il salvataggio di un singolo reference", "SPVDBSaveStreamReferences()" );
	    			}
	    		} // for
			}
	    	else
	    	{
	    		ret_code = SPV_APPLIC_INVALID_STREAM;
	    	}
	    }
	    else
	    {
	    	ret_code = SPV_APPLIC_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_APPLIC_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, ret_code, "Eccezione generale", "SPVDBSaveStreamReferences()" );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per decodificare un evento ricevuto da una periferica
///
/// \date [20.12.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
SPV_DEV_EVENT SPVDecodeSysEvent( SPV_DEVICE * device, char * event_buffer )
{
	SPV_DEV_EVENT event             ; // Struttura evento
	char          datetime_str  [9] ; //
	char          eventcode_str [3] ; //
	char          modulecode_str[3] ; //
	char          devaddr_str   [3] ; //
	char          cardcode_str  [3] ; //
	long          datetime          ; //
	int           eventcode         ; //
	int           modulecode        ; //
	int           devaddr           ; //
	int           cardcode          ; //
	char        * eventdesc         = NULL; //
	char        * moduledesc        = NULL; //
	char        * carddesc          = NULL; //

	if ( device != NULL && event_buffer != NULL )
	{
		try
		{
			// --- Estraggo tutte le sottostringhe dal buffer ---
			memcpy( datetime_str, &event_buffer[0], 8 );
			datetime_str[8] = '\0';
			memcpy( eventcode_str, &event_buffer[8], 2 );
			eventcode_str[2] = '\0';
			memcpy( modulecode_str, &event_buffer[10], 2 );
			modulecode_str[2] = '\0';
			memcpy( devaddr_str, &event_buffer[12], 2 );
			devaddr_str[2] = '\0';
			memcpy( cardcode_str, &event_buffer[14], 2 );
			cardcode_str[2] = '\0';

			// --- Estraggo il campo Data/Ora ---
			datetime = cnv_CharPToUHex32( datetime_str );
			// --- Estraggo il campo Codice Evento ---
			eventcode = cnv_CharPToUHex8( eventcode_str );
			// --- Estraggo il campo Identificativo Modulo di rilevamento evento ---
			modulecode = cnv_CharPToUHex8( modulecode_str );
			// --- Estraggo il campo Indirizzo Periferica ---
			devaddr = cnv_CharPToUHex8( devaddr_str );
			// --- Estraggo il campo Tipo Periferica ---
			cardcode = cnv_CharPToUHex8( cardcode_str );

	  // --- Recupero il ptr alla descrizione evento dalla definizione XML ---
	  eventdesc = SPVGetSysEventDesc( device, eventcode );
	  // --- Recupero il ptr alla descrizione modulo dalla definizione XML ---
	  moduledesc = SPVGetDevModuleDesc( device, modulecode );
	  // --- Recupero il ptr alla descrizione scheda dalla definizione XML ---
	  carddesc = SPVGetDevCardDesc( device, cardcode );

	  // --- Rimepimento dei dati nella struttura Evento ---
	  event.Type          = SPV_APPLIC_DEVICE_EVENT_TYPE_SYS;
	  event.DateTime      = SPV_APPLIC_EVENT_DATETIME_OFFSET_STANDARD + datetime;
	  event.EventCode     = eventcode;
	  strcpy( event.EventDesc, eventdesc );
	  event.Data1         = modulecode;
	  event.Data2         = devaddr;
	  event.Data3         = cardcode;
	  event.Data4         = 0;
	  event.Data1Desc[0]  = 0;
	  if ( moduledesc != NULL )
	  {
		strcat( event.Data1Desc, "Modulo periferica: " );
		strcat( event.Data1Desc, moduledesc );
	  }
	  else
	  {
		strcat( event.Data1Desc, "n/d" );
	  }
	  event.Data2Desc[0]  = 0;
	  if ( devaddr_str != NULL )
	  {
		strcat( event.Data2Desc, "Indirizzo periferica: " );
		strcat( event.Data2Desc, devaddr_str );
	  }
	  else
	  {
		strcat( event.Data2Desc, "n/d" );
	  }
	  event.Data3Desc[0]  = 0;
	  if ( carddesc != NULL )
	  {
		strcat( event.Data3Desc, "Tipo scheda: " );
		strcat( event.Data3Desc, carddesc );
	  }
	  else
	  {
		strcat( event.Data3Desc, "n/d" );
	  }
	  event.Data4Desc[0]  = 0;
	  strcat( event.Data4Desc, "n/u" );
	  event.Note     [0]  = 0;
	}
	catch(...)
	{
	  // --- Report dell'errore critico ---
	  SPVReportApplicEvent( NULL, 2, 3333, "Catturata eccezione decodifica evento sistema", "SPVDecodeSysEvent()" );
	}
  }
  else
  {
	memset( &event, 0, sizeof( SPV_DEV_EVENT ) );
  }

  return event;
}

//==============================================================================
/// Funzione per decodificare un evento scheda ricevuto da una periferica
///
/// \date [20.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_DEV_EVENT SPVDecodeDevEvent( SPV_DEVICE * device, char * event_buffer )
{
  SPV_DEV_EVENT event             ; // Struttura evento
  char          datetime_str  [9] ; //
  char          eventcode_str [3] ; //
  char          linecode_str  [3] ; //
  char          phonecode_str [3] ; //
  char          data_str      [17]; //
  char          typecode_str  [3] ; //
  long          datetime          ; //
  int           eventcode         ; //
  int           linecode          ; //
  int           phonecode         ; //
  int           typecode          ; //
  char        * eventdesc         = NULL; //

  if ( device != NULL && event_buffer != NULL )
  {
    try
    {
	  // --- Estraggo tutte le sottostringhe dal buffer ---
      memcpy( datetime_str, &event_buffer[0], 8 );
      datetime_str[8] = '\0';
      memcpy( eventcode_str, &event_buffer[8], 2 );
      eventcode_str[2] = '\0';
	  memcpy( linecode_str, &event_buffer[10], 2 );
      linecode_str[2] = '\0';
	  memcpy( phonecode_str, &event_buffer[12], 2 );
	  phonecode_str[2] = '\0';
      memcpy( data_str, &event_buffer[14], 16 );
      data_str[16] = '\0';
      memcpy( typecode_str, &event_buffer[30], 2 );
      typecode_str[2] = '\0';

      // --- Estraggo il campo Data/Ora ---
      datetime = cnv_CharPToUHex32( datetime_str );
      // --- Estraggo il campo Codice Evento ---
      eventcode = cnv_CharPToUHex8( eventcode_str );
      // --- Estraggo il campo Identificativo Linea ---
      linecode = cnv_CharPToUHex8( linecode_str );
      // --- Estraggo il campo Identificativo Telefono ---
      phonecode = cnv_CharPToUHex8( phonecode_str );
      // --- Estraggo il campo Tipo Linea/Servizio ---
      typecode = cnv_CharPToUHex8( typecode_str );

      // --- Recupero il ptr alla descrizione evento dalla definizione XML ---
      eventdesc = SPVGetDevEventDesc( device, eventcode );

      // --- Rimepimento dei dati nella struttura Evento ---
      event.Type          = SPV_APPLIC_DEVICE_EVENT_TYPE_DEV;
      event.DateTime      = SPV_APPLIC_EVENT_DATETIME_OFFSET_STANDARD + datetime;
      event.EventCode     = eventcode;
      strcpy( event.EventDesc, eventdesc );
      event.Data1         = linecode;
      event.Data2         = phonecode;
      event.Data3         = typecode;
	  event.Data4         = 0;
      event.Data1Desc[0]  = 0;
      if ( linecode_str != NULL )
      {
        strcat( event.Data1Desc, "ID Linea: " );
		strcat( event.Data1Desc, linecode_str );
	  }
	  else
      {
        strcat( event.Data1Desc, "n/d" );
      }
      event.Data2Desc[0]  = 0;
      if ( phonecode_str != NULL )
      {
        strcat( event.Data2Desc, "ID Telefono: " );
        strcat( event.Data2Desc, phonecode_str );
      }
      else
      {
        strcat( event.Data2Desc, "n/d" );
      }
      event.Data3Desc[0]  = 0;
      if ( typecode_str != NULL )
      {
        strcat( event.Data3Desc, "ID Tipo linea/servizio: " );
        strcat( event.Data3Desc, typecode_str );
      }
      else
      {
        strcat( event.Data3Desc, "n/d" );
	  }
      event.Data4Desc[0]  = 0;
      strcat( event.Data4Desc, "n/u" );
      event.Note     [0]  = 0;
      strcat( event.Note, "Dati supplementari: " );
      strcat( event.Note, data_str );
	}
    catch(...)
    {
      // --- Report dell'errore critico ---
      SPVReportApplicEvent( NULL, 2, 3333, "Catturata eccezione decodifica evento periferica", "SPVDecodeDevEvent()" );
	}
  }
  else
  {
    memset( &event, 0, sizeof( SPV_DEV_EVENT ) );
  }

  return event;
}

//==============================================================================
/// Funzione per recuperare la descrizione di un codice scheda
///
/// NOTA: Il puntatore restituito NON punta ad una copia del valore!
///
/// \date [20.10.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetDevCardDesc( SPV_DEVICE * device, int cardID )
{
  char        * desc          = NULL;
  XML_ELEMENT * def           = NULL;
  XML_ELEMENT * code_element  = NULL;
  XML_ELEMENT * card_element  = NULL;
  XML_ELEMENT * item          = NULL;
  int           code          = 0   ;

  if ( device != NULL )
  {
    def = device->Def;
	if ( def != NULL )
    {
      if ( def->child != NULL )
	  {
		code_element = XMLGetNext( def->child->child, "code", -1 );
		if ( code_element != NULL )
        {
          card_element = XMLGetNext( code_element->child, "cardcodes", -1 );
          if ( card_element != NULL )
          {
            // --- Parto dal primo child ---
            item = card_element->child;
            // --- Ciclo sugli elementi ---
            while ( item != NULL )
            {
              // --- Estraggo il codice dell'evento ---
              code = XMLGetValueInt( item, "code" );
              if ( code == cardID )
			  {
                desc = XMLGetValue( item, "des" );
                item = NULL; // Fine del ciclo
              }
              else
              {
                item = item->next;
              }
            }
          }
        }
      }
    }
  }

  return desc;
}

//==============================================================================
/// Funzione per recuperare la descrizione di un evento sistema
///
/// NOTA: Il puntatore restituito NON punta ad una copia del valore!
///
/// \date [20.10.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetSysEventDesc( SPV_DEVICE * device, int eventID )
{
  char        * desc          = NULL;
  XML_ELEMENT * def           = NULL;
  XML_ELEMENT * code_element  = NULL;
  XML_ELEMENT * event_element = NULL;
  XML_ELEMENT * item          = NULL;
  int           code          = 0   ;

  if ( device != NULL )
  {
    def = device->Def;
    if ( def != NULL )
    {
      if ( def->child != NULL )
      {
        code_element = XMLGetNext( def->child->child, "code", -1 );
        if ( code_element != NULL )
        {
          event_element = XMLGetNext( code_element->child, "syseventcodes", -1 );
          if ( event_element != NULL )
          {
            // --- Parto dal primo child ---
            item = event_element->child;
            // --- Ciclo sugli elementi ---
            while ( item != NULL )
            {
			  // --- Estraggo il codice dell'evento ---
              code = XMLGetValueInt( item, "code" );
			  if ( code == eventID )
			  {
                desc = XMLGetValue( item, "des" );
				item = NULL; // Fine del ciclo
			  }
              else
              {
                item = item->next;
              }
            }
          }
        }
      }
    }
  }

  return desc;
}

//==============================================================================
/// Funzione per recuperare la descrizione di un evento periferica
///
/// NOTA: Il puntatore restituito NON punta ad una copia del valore!
///
/// \date [20.10.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetDevEventDesc( SPV_DEVICE * device, int eventID )
{
  char        * desc          = NULL;
  XML_ELEMENT * def           = NULL;
  XML_ELEMENT * code_element  = NULL;
  XML_ELEMENT * event_element = NULL;
  XML_ELEMENT * item          = NULL;
  int           code          = 0   ;

  if ( device != NULL )
  {
	def = device->Def;
    if ( def != NULL )
    {
      if ( def->child != NULL )
      {
        code_element = XMLGetNext( def->child->child, "code", -1 );
        if ( code_element != NULL )
        {
          event_element = XMLGetNext( code_element->child, "deveventcodes", -1 );
          if ( event_element != NULL )
          {
            // --- Parto dal primo child ---
            item = event_element->child;
            // --- Ciclo sugli elementi ---
            while ( item != NULL )
            {
              // --- Estraggo il codice dell'evento ---
              code = XMLGetValueInt( item, "code" );
              if ( code == eventID )
              {
                desc = XMLGetValue( item, "des" );
                item = NULL; // Fine del ciclo
              }
              else
              {
				item = item->next;
              }
            }
          }
        }
      }
	}
  }

  return desc;
}

//==============================================================================
/// Funzione per recuparare il nome del modulo di una periferica
///
/// NOTA: Il puntatore restituito NON punta ad una copia del valore!
///                                            
/// \date [20.10.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetDevModuleDesc( SPV_DEVICE * device, int moduleID )
{
  char        * desc          = NULL;
  XML_ELEMENT * def           = NULL;
  XML_ELEMENT * code_element  = NULL;
  XML_ELEMENT * mod_element   = NULL;
  XML_ELEMENT * item          = NULL;
  int           code          = 0   ;

  if ( device != NULL )
  {
    def = device->Def;
    if ( def != NULL )
    {
      if ( def->child != NULL )
      {
        code_element = XMLGetNext( def->child->child, "code", -1 );
        if ( code_element != NULL )
        {
          mod_element = XMLGetNext( code_element->child, "modulecodes", -1 );
		  if ( mod_element != NULL )
		  {
            // --- Parto dal primo child ---
            item = mod_element->child;
            // --- Ciclo sugli elementi ---
            while ( item != NULL )
			{
              // --- Estraggo il codice dell'evento ---
              code = XMLGetValueInt( item, "code" );
              if ( code == moduleID )
              {
                desc = XMLGetValue( item, "des" );
                item = NULL; // Fine del ciclo
              }
              else
              {
                item = item->next;
              }
            }
		  }
        }
      }
    }
  }

  return desc;
}

//==============================================================================
/// Funzione per ottenere l'ID di un evento a livello di field
///
/// \date [17.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVGetFieldEventID( SPV_DEVICE * device, int streamID, int fieldID, int sev )
{
  int                     eventID = 0;
  SPV_DEV_STREAM        * stream  = NULL;
  SPV_DEV_STREAM_FIELD  * field   = NULL;
  XML_ELEMENT           * field_e = NULL; // Ptr a elemento XML di un valore

  if ( device != NULL )
  {
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL ) // Se ho trovato un valido ptr a struttura stream
    {
      field = SPVGetDevField( stream, fieldID );
      if ( field != NULL )
	  {
        field_e = field->Def;
        if ( field_e != NULL )
        {
          switch ( sev )
          {
            // --- Caso OK ---
            case SPV_APPLIC_SEVLEVEL_OK:
              eventID = XMLGetValueInt( field_e, "eOk" );
            break;
            // --- Caso WARNING ---
            case SPV_APPLIC_SEVLEVEL_WARNING:
              eventID = XMLGetValueInt( field_e, "eWarning" );
            break;
            // --- Caso ERROR ---
            case SPV_APPLIC_SEVLEVEL_ERROR:
              eventID = XMLGetValueInt( field_e, "eError" );
            break;
            // --- Caso CRITICAL ---
            case SPV_APPLIC_SEVLEVEL_CRITICAL:
              eventID = XMLGetValueInt( field_e, "eCritical" );
			break;
			// --- Caso ALARM ---
            case SPV_APPLIC_SEVLEVEL_ALARM:
			  eventID = XMLGetValueInt( field_e, "eAlarm" );
            break;
            // --- Caso UNKNOWN ---
            default:
              eventID = SPV_APPLIC_EVENT_DEV_FIELD_UNKNOWN;
		  }
		}
      }
    }
  }

  return eventID;
}

//==============================================================================
/// Funzione per ottenere l'ID di un evento a livello di stream
///
/// \date [18.11.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetStreamEventID( SPV_DEVICE * device, int streamID, int sev )
{
  int                     eventID   = 0;
  SPV_DEV_STREAM        * stream    = NULL;
  XML_ELEMENT           * stream_e  = NULL; // Ptr a elemento XML

  if ( device != NULL )
  {
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL ) // Se ho trovato un valido ptr a struttura stream
    {
	  stream_e = stream->Def;
	  if ( stream_e != NULL )
      {
        switch ( sev )
		{
          // --- Caso OK ---
          case SPV_APPLIC_SEVLEVEL_OK:
            eventID = XMLGetValueInt( stream_e, "eOk" );
          break;
		  // --- Caso WARNING ---
          case SPV_APPLIC_SEVLEVEL_WARNING:
            eventID = XMLGetValueInt( stream_e, "eWarning" );
          break;
          // --- Caso ERROR ---
          case SPV_APPLIC_SEVLEVEL_ERROR:
            eventID = XMLGetValueInt( stream_e, "eError" );
		  break;
          // --- Caso CRITICAL ---
          case SPV_APPLIC_SEVLEVEL_CRITICAL:
            eventID = XMLGetValueInt( stream_e, "eCritical" );
          break;
          // --- Caso ALARM ---
          case SPV_APPLIC_SEVLEVEL_ALARM:
            eventID = XMLGetValueInt( stream_e, "eAlarm" );
          break;
          // --- Caso UNKNOWN ---
          default:
            eventID = SPV_APPLIC_EVENT_DEV_STREAM_UNKNOWN;
        }
      }
    }
  }

  return eventID;
}

//==============================================================================
/// Funzione per ottenere l'ID di un evento a livello di device
///
/// \date [23.11.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetDeviceEventID( SPV_DEVICE * device, int sev )
{
  int                     eventID   = 0;

  if ( device != NULL )
  {
    switch ( sev )
    {
      // --- Caso OK ---
	  case SPV_APPLIC_SEVLEVEL_OK:
        eventID = SPV_APPLIC_EVENT_DEV_OK;
      break;
      // --- Caso WARNING ---
      case SPV_APPLIC_SEVLEVEL_WARNING:
        eventID = SPV_APPLIC_EVENT_DEV_WARNING;
      break;
      // --- Caso ERROR ---
      case SPV_APPLIC_SEVLEVEL_ERROR:
        eventID = SPV_APPLIC_EVENT_DEV_ERROR;
      break;
      // --- Caso CRITICAL ---
	  case SPV_APPLIC_SEVLEVEL_CRITICAL:
        eventID = SPV_APPLIC_EVENT_DEV_CRITICAL;
      break;
      // --- Caso ALARM ---
      case SPV_APPLIC_SEVLEVEL_ALARM:
        eventID = SPV_APPLIC_EVENT_DEV_ALARM;
	  break;
	  // --- Caso UNKNOWN ---
      default:
        eventID = SPV_APPLIC_EVENT_DEV_UNKNOWN;
    }
  }

  return eventID;
}

//==============================================================================
/// Funzione per settare lo stato di una periferica
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVSetDeviceStatus( SPV_DEVICE * device, int sev, char * des )
{
	int     ret_code  = SPV_APPLIC_FUNCTION_SUCCESS ; // Codice di ritorno
	bool multi_language = SPVSystem.MultiLangauge;

	if ( device != NULL )
	{
		// --- Setto il livello di severit� della periferica ---
		device->SupStatus.Level = sev;
		// --- Setto la descrizione dello stato della periferica ---
		if ( des != NULL )
		{
			device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, des ); // -FREE -MALLOC
		}
		else
		{
			if (multi_language) {
				switch ( sev )
				{
					// --- Caso OK (0) ---
					case SPV_APPLIC_SEVLEVEL_OK:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_OK ); // -FREE -MALLOC
					break;
					// --- Caso WARNING (1) ---
					case SPV_APPLIC_SEVLEVEL_WARNING:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_WARNING ); // -FREE -MALLOC
					break;
					// --- Caso ERROR (2) ---
					case SPV_APPLIC_SEVLEVEL_ERROR:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_ERROR ); // -FREE -MALLOC
					break;
					// --- Caso CRITICAL (3) ---
					case SPV_APPLIC_SEVLEVEL_CRITICAL:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_CRITICAL ); // -FREE -MALLOC
					break;
					// --- Caso ALARM (4) ---
					case SPV_APPLIC_SEVLEVEL_ALARM:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_ALARM ); // -FREE -MALLOC
					break;
					// --- Caso NOT AVAILABLE (9) ---
					case SPV_APPLIC_SEVLEVEL_NOT_AVAILABLE:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_NOT_AVAILABLE ); // -FREE -MALLOC
					break;
					// --- Caso UNKNOWN (altro) ---
					default:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description,SPV_APPLIC_SEVLEVEL_DES_MULTILANGUAGE_UNKNOWN  ); // -FREE -MALLOC
				}
			}
			// Caso mono lingua (default = italiano)
			else{
				switch ( sev )
				{
					// --- Caso OK (0) ---
					case SPV_APPLIC_SEVLEVEL_OK:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEDEL_DES_DEFAULT_OK ); // -FREE -MALLOC
					break;
					// --- Caso WARNING (1) ---
					case SPV_APPLIC_SEVLEVEL_WARNING:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_WARNING ); // -FREE -MALLOC
					break;
					// --- Caso ERROR (2) ---
					case SPV_APPLIC_SEVLEVEL_ERROR:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_ERROR ); // -FREE -MALLOC
					break;
					// --- Caso CRITICAL (3) ---
					case SPV_APPLIC_SEVLEVEL_CRITICAL:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_CRITICAL ); // -FREE -MALLOC
					break;
					// --- Caso ALARM (4) ---
					case SPV_APPLIC_SEVLEVEL_ALARM:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_ALARM ); // -FREE -MALLOC
					break;
					// --- Caso NOT AVAILABLE (9) ---
					case SPV_APPLIC_SEVLEVEL_NOT_AVAILABLE:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_NOT_AVAILABLE ); // -FREE -MALLOC
					break;
					// --- Caso UNKNOWN (altro) ---
					default:
						device->SupStatus.Description = XMLStrCpy( device->SupStatus.Description, SPV_APPLIC_SEVLEVEL_DES_DEFAULT_UNKNOWN ); // -FREE -MALLOC
				}
			}
		}
	}
	else
	{
		ret_code == SPV_APPLIC_INVALID_DEVICE;
	}
	return ret_code;
}

//==============================================================================
/// Funzione per creare un messaggio per un evento di periferica
///
/// \date [26.10.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
char * SPVBuildDevEventMsg( char * devid, char * devname, char * devtype, char * devstatus, char * port, char * eventsource, char * eventdes, char * note )
{
  char  * msg     = NULL;

  try
  {
    // --- Aggiungo l'intestazione ID periferica ---
	msg = XMLAddToString( NULL, "\nDevice ID: " );
	// --- Aggiungo l'ID periferica ---
	if ( devid != NULL )
	{
	  msg = XMLAddToString( msg, devid );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Nome periferica ---
	msg = XMLAddToString( msg, "\nDevice Name: " );
	// --- Aggiungo il Nome periferica ---
	if ( devname != NULL )
	{
	  msg = XMLAddToString( msg, devname );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Tipo periferica ---
	msg = XMLAddToString( msg, "\nDevice Type: " );
	// --- Aggiungo il Tipo periferica ---
	if ( devtype != NULL )
	{
	  msg = XMLAddToString( msg, devtype );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Stato periferica ---
	msg = XMLAddToString( msg, "\nDevice Status: " );
	// --- Aggiungo il Stato periferica ---
	if ( devstatus != NULL )
	{
	  msg = XMLAddToString( msg, devstatus );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Porta ---
	msg = XMLAddToString( msg, "\nComm Port: " );
	// --- Aggiungo la Porta ---
	if ( port != NULL )
	{
	  msg = XMLAddToString( msg, port );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Sorgente evento ---
	msg = XMLAddToString( msg, "\nEvent Source: " );
	// --- Aggiungo la Sorgente evento ---
	if ( eventsource != NULL )
	{
	  msg = XMLAddToString( msg, eventsource );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Descrizione evento ---
	msg = XMLAddToString( msg, "\nEvent Description: " );
	// --- Aggiungo il Descrizione evento ---
	if ( eventdes != NULL )
	{
	  msg = XMLAddToString( msg, eventdes );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Note ---
	msg = XMLAddToString( msg, "\nNotes: " );
	// --- Aggiungo le Note ---
	if ( note != NULL )
	{
	  msg = XMLAddToString( msg, note );
	}
	else
	{
	  msg = XMLAddToString( msg, "n/a" );
	}
  }
  catch(...)
  {
	msg = NULL;
  }

  return msg;
}

//==============================================================================
// Funzione per creare un messaggio per un evento applicativo
//
// \date [13.02.2006]
// \author Enrico Alborali
// \version 0.01
//------------------------------------------------------------------------------
char * SPVBuildApplicEventMsg( char * source, char * type, char * des, char * note )
{
	char  * msg     = NULL;

	try
	{
	// --- Aggiungo l'intestazione Sorgente evento ---
	msg = XMLAddToString( msg, "\nEvent Source: " );

	// --- Aggiungo la Sorgente evento ---
	if ( source != NULL )
	{
		msg = XMLAddToString( msg, source );
	}
	else
	{
		msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Tipo Sorgente evento ---
	msg = XMLAddToString( msg, "\nSource Type: " );
	// --- Aggiungo il Tipo Sorgente evento ---
	if ( type != NULL )
	{
		msg = XMLAddToString( msg, type );
	}
	else
	{
		msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Descrizione evento ---
	msg = XMLAddToString( msg, "\nEvent Description: " );
	// --- Aggiungo il Descrizione evento ---
	if ( des != NULL )
	{
		msg = XMLAddToString( msg, des );
	}
	else
	{
		msg = XMLAddToString( msg, "n/a" );
	}

	// --- Aggiungo l'intestazione Note ---
	msg = XMLAddToString( msg, "\nNotes: " );
	// --- Aggiungo le Note ---
	if ( note != NULL )
	{
		msg = XMLAddToString( msg, note );
	}
	else
	{
		msg = XMLAddToString( msg, "n/a" );
	}
	}
	catch(...)
	{
		msg = NULL;
	}

	return msg;
}

//==============================================================================
/// Funzione per creare i patrametri per un evento di periferica
///
/// NOTE: eventID deve essere compreso tra 0 e 4095.
///
/// \date [04.20.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVBuildDevEventParams( int category, int severity, int eventID )
{
  int params  = 0;
  int ID      = 0;

  ID = ( eventID < 4096 )? ( ( eventID >= 0 ) ? eventID : 0 ) : 4095 ;

  params = ( 65536 * category ) + ( 4096 * severity ) + ID;

  return params;
}

//==============================================================================
/// Funzione per riportare un evento periferica nell'EventLog di Windows
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVReportDevEvent( SPV_DEVICE * device, int streamID, int fieldID, int arrayID, int sevlevel, int eventID, char * des, char * note )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
  char                  * msg         = NULL;
  int                     msg_len     = 0   ;
  int                     params      = 0   ;
  char                  * devid       = NULL;
  SYS_PORT              * port        = NULL;
  char                  * port_name   = NULL;
  SPV_DEV_STREAM        * stream      = NULL; // Ptr ad una struttura stream
  char                  * stream_name = NULL;
  char                  * type_name   = NULL;
  SPV_DEV_STREAM_FIELD  * field       = NULL; // Ptr ad una struttura campo (field)
  char                  * field_name  = NULL; // Nome del campo (field)
  char                  * source_name = NULL; // Nome della sorgente dell'evento
  int                     source_len  = 0   ;
  char                  * arrayID_str = NULL;

  if ( device != NULL )
  {
    // --- Recupero i campi del messaggio ---
    devid = SPVGetDeviceUniIDStr( device ); // -MALLOC
    port = (SYS_PORT*)device->SupCfg.Port;
    if ( port != NULL )
    {
      port_name = port->Name;
    }
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL )
	{
      stream_name = stream->Name;
      if ( stream_name != NULL )
      {
        source_len += strlen( stream_name ) + 2;
      }
      if ( fieldID != -1 )
	  {
        field = SPVGetDevField( stream, fieldID );
        if ( field != NULL )
        {
		  field_name = field->Name;
          if ( field_name != NULL )
          {
            source_len += strlen( field_name ) + 2;
		  }
		}
      }
      if ( arrayID != -1 )
      {
        arrayID_str = XMLType2ASCII( &arrayID, t_u_int_16 ); // -MALLOC
        source_len += strlen( arrayID_str ) + 2;
      }
      source_name = (char*)malloc( sizeof(char) * source_len ); // -MALLOC
      source_name[0] = '\0';
      // --- Compongo il nome della sorgente dell'evento ---
      if ( stream_name != NULL )
      {
        strcat( source_name, stream_name );
        if ( field_name != NULL )
        {
          strcat( source_name, "->" );
          strcat( source_name, field_name );
          if ( arrayID_str != NULL )
          {
            strcat( source_name, "[" );
			strcat( source_name, arrayID_str );
            strcat( source_name, "]" );
            free( arrayID_str );
          }
        }
	  }
    }
	if ( device->Type != NULL )
    {
      type_name = device->Type->Name;
    }
    // --- Preparo il messaggio ---
    msg = SPVBuildDevEventMsg( devid, device->Name, type_name,
                               device->SupStatus.Description,
							   port_name, source_name, des, note ); // -MALLOC
	// --- Caclcolo la lunghezza del messaggio ---
    msg_len = strlen( msg );
    // --- Preparo i parametri dell'evento ---
    params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_DEV_EVENT, sevlevel, eventID );
    // --- Scrivo l'evento nell'EventLog di Windows ---
    SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );
    // --- Cancello i dati temporanei ---
    free( msg ); // -FREE
    free( devid ); // -FREE
    if ( source_name != NULL )
    {
      try
      {
        free( source_name ); // -FREE
      }
      catch(...)
      {
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento value nell'EventLog di Windows
///
/// \date [10.02.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVReportValueEvent( void * pdevice, int streamID, int fieldID, int arrayID, int sevlevel, int eventID, char * des, char * note )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  SPV_DEVICE            * device      = (SPV_DEVICE*)pdevice; // Ptr a struttura device
  char                  * msg         = NULL;
  int                     msg_len     = 0   ;
  int                     params      = 0   ;
  char                  * devid       = NULL;
  SYS_PORT              * port        = NULL;
  char                  * port_name   = NULL;
  SPV_DEV_STREAM        * stream      = NULL; // Ptr ad una struttura stream
  char                  * stream_name = NULL;
  char                  * type_name   = NULL;
  SPV_DEV_STREAM_FIELD  * field       = NULL; // Ptr ad una struttura campo (field)
  char                  * field_name  = NULL; // Nome del campo (field)
  char                  * source_name = NULL; // Nome della sorgente dell'evento
  int                     source_len  = 0   ;
  char                  * arrayID_str = NULL;

  if ( device != NULL )
  {
	// --- Recupero i campi del messaggio ---
    devid = SPVGetDeviceUniIDStr( device ); // -MALLOC
    port = (SYS_PORT*)device->SupCfg.Port;
    if ( port != NULL )
    {
      port_name = port->Name;
    }
	stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL )
    {
      stream_name = stream->Name;
      if ( stream_name != NULL )
	  {
		source_len += strlen( stream_name ) + 2;
      }
      if ( fieldID != -1 )
      {
        field = SPVGetDevField( stream, fieldID );
        if ( field != NULL )
        {
          field_name = field->Name;
          if ( field_name != NULL )
          {
			source_len += strlen( field_name ) + 2;
          }
        }
      }
      if ( arrayID != -1 )
      {
        arrayID_str = XMLType2ASCII( &arrayID, t_u_int_16 ); // -MALLOC
        source_len += strlen( arrayID_str ) + 2;
      }
      source_name = (char*)malloc( sizeof(char) * source_len ); // -MALLOC
      source_name[0] = '\0';
      // --- Compongo il nome della sorgente dell'evento ---
      if ( stream_name != NULL )
	  {
        strcat( source_name, stream_name );
        if ( field_name != NULL )
        {
          strcat( source_name, "->" );
          strcat( source_name, field_name );
          if ( arrayID_str != NULL )
		  {
            strcat( source_name, "[" );
            strcat( source_name, arrayID_str );
            strcat( source_name, "]" );
			free( arrayID_str );
		  }
        }
      }
    }
    if ( device->Type != NULL )
	{
      type_name = device->Type->Name;
    }
    // --- Preparo il messaggio ---
    msg = SPVBuildDevEventMsg( devid, device->Name, type_name,
                               device->SupStatus.Description,
                               port_name, source_name, des, note ); // -MALLOC
    // --- Caclcolo la lunghezza del messaggio ---
    msg_len = strlen( msg );
    // --- Preparo i parametri dell'evento ---
    params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_DEV_VALUE_EVENT, sevlevel, eventID );
    // --- Scrivo l'evento nell'EventLog di Windows ---
    SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );
    // --- Cancello i dati temporanei ---
    free( msg ); // -FREE
    free( devid ); // -FREE
    if ( source_name != NULL )
    {
      try
	  {
        free( source_name ); // -FREE
      }
      catch(...)
      {
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
	}
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento field nell'EventLog di Windows
///
/// \date [10.02.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVReportFieldEvent( void * pdevice, int streamID, int fieldID, int arrayID, int sevlevel, int eventID, char * des )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  SPV_DEVICE            * device      = (SPV_DEVICE*)pdevice; // Ptr a struttura device
  char                  * msg         = NULL;
  int                     msg_len     = 0   ;
  int                     params      = 0   ;
  char                  * devid       = NULL;
  SYS_PORT              * port        = NULL;
  char                  * port_name   = NULL;
  SPV_DEV_STREAM        * stream      = NULL; // Ptr ad una struttura stream
  char                  * stream_name = NULL;
  char                  * type_name   = NULL;
  SPV_DEV_STREAM_FIELD  * field       = NULL; // Ptr ad una struttura campo (field)
  char                  * field_name  = NULL; // Nome del campo (field)
  char                  * source_name = NULL; // Nome della sorgente dell'evento
  int                     source_len  = 0   ;
  char                  * arrayID_str = NULL;

  if ( device != NULL )
  {
	// --- Recupero i campi del messaggio ---
	devid = SPVGetDeviceUniIDStr( device ); // -MALLOC
    port = (SYS_PORT*)device->SupCfg.Port;
    if ( port != NULL )
    {
      port_name = port->Name;
    }
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL )
    {
      stream_name = stream->Name;
      if ( stream_name != NULL )
      {
        source_len += strlen( stream_name ) + 2;
      }
      if ( fieldID != -1 )
      {
        field = SPVGetDevField( stream, fieldID );
        if ( field != NULL )
        {
          field_name = field->Name;
          if ( field_name != NULL )
          {
            source_len += strlen( field_name ) + 2;
          }
        }
	  }
	  if ( arrayID != -1 )
      {
        arrayID_str = XMLType2ASCII( &arrayID, t_u_int_16 ); // -MALLOC
        source_len += strlen( arrayID_str ) + 2;
      }
      source_name = (char*)malloc( sizeof(char) * source_len ); // -MALLOC
      source_name[0] = '\0';
	  // --- Compongo il nome della sorgente dell'evento ---
	  if ( stream_name != NULL )
	  {
        strcat( source_name, stream_name );
        if ( field_name != NULL )
        {
          strcat( source_name, "->" );
          strcat( source_name, field_name );
          if ( arrayID_str != NULL )
          {
            strcat( source_name, "[" );
            strcat( source_name, arrayID_str );
            strcat( source_name, "]" );
            free( arrayID_str );
          }
        }
      }
    }
    if ( device->Type != NULL )
    {
      type_name = device->Type->Name;
    }
	// --- Preparo il messaggio ---
    msg = SPVBuildDevEventMsg( devid, device->Name, type_name,
                               device->SupStatus.Description,
                               port_name, source_name, des, NULL ); // -MALLOC
    // --- Caclcolo la lunghezza del messaggio ---
    msg_len = strlen( msg );
    // --- Preparo i parametri dell'evento ---
	params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_DEV_FIELD_EVENT, sevlevel, eventID );
    // --- Scrivo l'evento nell'EventLog di Windows ---
    SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );
    // --- Cancello i dati temporanei ---
    free( msg ); // -FREE
    free( devid ); // -FREE
    if ( source_name != NULL )
	{
	  try
      {
        free( source_name ); // -FREE
      }
      catch(...)
      {
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento stream nell'EventLog di Windows
///
/// \date [10.02.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVReportStreamEvent( void * pdevice, int streamID, int sevlevel, int eventID, char * des )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  SPV_DEVICE            * device      = (SPV_DEVICE*)pdevice; // Ptr a struttura device
  char                  * msg         = NULL;
  int                     msg_len     = 0   ;
  int                     params      = 0   ;
  char                  * devid       = NULL;
  SYS_PORT              * port        = NULL;
  char                  * port_name   = NULL;
  SPV_DEV_STREAM        * stream      = NULL; // Ptr ad una struttura stream
  char                  * stream_name = NULL;
  char                  * type_name   = NULL;
  char                  * source_name = NULL; // Nome della sorgente dell'evento
  int                     source_len  = 0   ;

  if ( device != NULL )
  {
    // --- Recupero i campi del messaggio ---
    devid = SPVGetDeviceUniIDStr( device ); // -MALLOC
	port = (SYS_PORT*)device->SupCfg.Port;
    if ( port != NULL )
    {
      port_name = port->Name;
    }
    stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
    if ( stream != NULL )
    {
      stream_name = stream->Name;
      if ( stream_name != NULL )
      {
        source_len += strlen( stream_name ) + 2;
      }
      source_name = (char*)malloc( sizeof(char) * source_len ); // -MALLOC
      source_name[0] = '\0';
      // --- Compongo il nome della sorgente dell'evento ---
      if ( stream_name != NULL )
      {
        strcat( source_name, stream_name );
	  }
    }
    if ( device->Type != NULL )
    {
      type_name = device->Type->Name;
	}
	// --- Preparo il messaggio ---
	msg = SPVBuildDevEventMsg( devid, device->Name, type_name,
                               device->SupStatus.Description,
                               port_name, source_name, des, NULL ); // -MALLOC
    // --- Caclcolo la lunghezza del messaggio ---
	msg_len = strlen( msg );
    // --- Preparo i parametri dell'evento ---
    params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_DEV_STREAM_EVENT, sevlevel, eventID );
    // --- Scrivo l'evento nell'EventLog di Windows ---
    SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );
    // --- Cancello i dati temporanei ---
    free( msg ); // -FREE
    free( devid ); // -FREE
    if ( source_name != NULL )
    {
      try
      {
        free( source_name ); // -FREE
      }
      catch(...)
      {
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento device nell'EventLog di Windows
///
/// \date [10.02.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVReportDeviceEvent( void * pdevice, int sevlevel, int eventID, char * des )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  SPV_DEVICE            * device      = (SPV_DEVICE*)pdevice; // Ptr a struttura device
  char                  * msg         = NULL;
  int                     msg_len     = 0   ;
  int                     params      = 0   ;
  char                  * devid       = NULL;
  SYS_PORT              * port        = NULL;
  char                  * port_name   = NULL;
  char                  * type_name   = NULL;

  if ( device != NULL )
  {
    // --- Recupero i campi del messaggio ---
    devid = SPVGetDeviceUniIDStr( device ); // -MALLOC
    port = (SYS_PORT*)device->SupCfg.Port;
    if ( port != NULL )
    {
      port_name = port->Name;
    }
    if ( device->Type != NULL )
    {
      type_name = device->Type->Name;
    }
	// --- Preparo il messaggio ---
	msg = SPVBuildDevEventMsg( devid, device->Name, type_name,
                               device->SupStatus.Description,
                               port_name, NULL, des, NULL ); // -MALLOC
	// --- Caclcolo la lunghezza del messaggio ---
	msg_len = strlen( msg );
    // --- Preparo i parametri dell'evento ---
    params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_DEV_EVENT, sevlevel, eventID );
	// --- Scrivo l'evento nell'EventLog di Windows ---
    SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );
    // --- Cancello i dati temporanei ---
    free( msg ); // -FREE
    free( devid ); // -FREE
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento stato sistema nell'EventLog di Windows
///
/// \date [26.10.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SPVReportSystemEvent( void * psystem, int sevlevel, int eventID, char * des, char * note )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS; // Codice di ritorno
  SPV_SYSTEM            * system      = NULL;
  char                  * msg         = NULL;
  int                     params      = 0   ;

  system = (SPV_SYSTEM*)psystem;
  if ( system != NULL )
  {
	// --- Preparo il messaggio ---
	msg = SPVBuildApplicEventMsg( system->Name, "Telefin Monitoring System", des, note ); // -MALLOC
	// --- Preparo i parametri dell'evento ---
	params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_SYSTEM_EVENT, sevlevel, eventID );
	// --- Scrivo l'evento nell'EventLog di Windows ---
	SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, strlen( msg ), params );
	// --- Cancello i dati temporanei ---
	free( msg ); // -FREE
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_SYSTEM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento porta di com. nell'EventLog di Windows
///
/// \date [13.02.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVReportPortEvent( void * pport, int sevlevel, int eventID, char * des, char * note )
{
  int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
  SYS_PORT              * port        = (SYS_PORT*)pport;
  char                  * port_type   = NULL;
  char                  * msg         = NULL;
  int                     params      = 0   ;

  if ( port != NULL )
  {
	port_type = SPVGetPortTypeName( port->Type ); // -MALLOC
	// --- Preparo il messaggio ---
	msg = SPVBuildApplicEventMsg( port->Name, port_type, des, note ); // -MALLOC
	// --- Preparo i parametri dell'evento ---
	params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_PORT_EVENT, sevlevel, eventID );
	// --- Scrivo l'evento nell'EventLog di Windows ---
	SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, strlen( msg ), params );
	// --- Cancello i dati temporanei ---
	if ( msg ) free( msg ); // -FREE
	if ( port_type ) free( port_type ); // -FREE
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento database nell'EventLog di Windows
///
/// \date [22.05.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVReportDBEvent( void * conn, int sevlevel, int eventID, char * des, char * note )
{
  int                 ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
  char              * msg         = NULL;
  int                 params      = 0   ;
  SPV_DBI_CONN      * dbconn      = NULL;

  dbconn = (SPV_DBI_CONN*)conn;
  if ( dbconn != NULL )
  {
	// --- Preparo il messaggio ---
	msg = SPVBuildApplicEventMsg( dbconn->Name, SPVDBGetTypeName( dbconn->Type ), des, note ); // -MALLOC
	// --- Preparo i parametri dell'evento ---
	params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_DB_EVENT, sevlevel, eventID );
	// --- Scrivo l'evento nell'EventLog di Windows ---
	SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, strlen( msg ), params );
	free( msg ); // -FREE
  }
  else
  {
	ret_code = SPV_APPLIC_INVALID_DB_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento applicativo nell'EventLog di Windows
///
/// \date [26.10.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SPVReportApplicEvent( SPV_PROCEDURE * procedure, int sevlevel, int eventID, char * des, char * code )
{
	int                 ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;
	char              * msg         = NULL;
	int                 msg_len     = 0   ;
	int                 params      = 0   ;

	try
	{
		if ( procedure != NULL )
		{
			// --- Preparo il messaggio ---
			msg     = ULAddText( msg, "\nProcedure: " );
			msg     = ULAddText( msg, procedure->Name );
		}
		// --- Preparo il messaggio ---
		msg     = ULAddText( msg, "\nEvent: " );
		msg     = ULAddText( msg, des );
		msg     = ULAddText( msg, "\nModule: " );
		msg     = ULAddText( msg, code );
		msg_len = strlen( msg );
		// --- Preparo i parametri dell'evento ---
		params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_APPLIC_EVENT, sevlevel, eventID );
		// --- Scrivo l'evento nell'EventLog di Windows ---
		SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );

		free( msg ); // -FREE
	}
	catch(...)
	{
		ret_code = SPV_APPLIC_CRITICAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per riportare un evento di protocollo nell'EventLog di Windows
///
/// \date [01.02.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVReportComProtEvent( SPV_COMPROT_COMMAND * command, int sevlevel, int eventID, char * des, char * code )
{
	int ret_code;

	ret_code = SPVReportApplicEvent( NULL, sevlevel, eventID, des, code );

	return ret_code;
}

//==============================================================================
/// Funzione per scrivere su chiave di registro lo UnixTime corrente
///
/// \date [27.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	SPVUpdateLastAutoCheck( void )
{
	int						ret_code	= SPV_APPLIC_FUNCTION_SUCCESS;
	void        			* key    		= NULL;
	char        			* value  		= NULL;
	unsigned __int32    utime			= 0;
	int								  size 			= 0;
	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SPV_APPLIC_SERVICE_KEY, true );
	if ( key != NULL )
	{
		utime = (unsigned __int32)  ULGetUnixTime( );
		size = 4;

		ret_code = SYSSetRegValue( key, SPV_APPLIC_LASTAUTOCHECK_VALUE_NAME, &utime, REG_DWORD, size );
		SYSCloseRegKey( key );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_REG_KEY;
	}
	return ret_code;
}

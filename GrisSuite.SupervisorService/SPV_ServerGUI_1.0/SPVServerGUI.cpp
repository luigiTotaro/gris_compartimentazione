//==============================================================================
// Telefin Supervisor Server GUI Module 1.0
//------------------------------------------------------------------------------
// Mudulo di gestione interfaccia grafica del server (SPVServerGUI.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.16 (31.08.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVServerGUI.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVServerGUI.h
//==============================================================================

#pragma hdrstop
#include <dateutils.hpp>
#include "SPVServerGUI.h"
#include "SPVSystemViewForm.h"
#include "SPVApplicative.h"
#include "SPVPortViewForm.h"
#include "SPVDBViewForm.h"
//#include "frmComProtLog.h"
#include "SPVApplicativeViewForm.h"
#include "SPVComProtViewForm.h"
//#include "SPVServerGUIRes.h"
#include "SPVServerMainForm.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------

static int            ProcList[SPV_SERVERGUI_MAX_PROC]; /// Lista ID procedure in tabella
static int            ProcNextID       ; /// Prossimo ID per procedura
static int            ProcCount        ; /// Numero procedure in tabella
static TStringGrid  * ProcStatusTable  ; /// Tabella stati procedure
       void         * StatusIconList   ;         

//==============================================================================
/// Funzione per inizializzare il modulo
///
/// \date [30.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void SPVServerGUIInit( void )
{
  memset( &ProcList[0], 0, sizeof(int)*SPV_SERVERGUI_MAX_PROC );
  ProcNextID      = 12345;
  ProcCount       = 0;
  ProcStatusTable = NULL;

  // --- Resetto la lista della Icone di stato ---
  StatusIconList  = NULL;
}

//==============================================================================
/// Funzione per impostare la tabella delle procedure
///
/// \date [01.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVSetProcStatusTable( void * table )
{
  TStringGrid * string_grid = NULL;

  string_grid     = (TStringGrid*) table;
  ProcStatusTable = string_grid;
}

//==============================================================================
/// Funzione per aggiungere lo stato di una procedura alla tabella
///
/// \date [01.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAddProcStatus( SPV_PROCEDURE * procedure )
{
  int procID    = 0;
  int current   = 0;
  int position  = -1;

  if ( procedure != NULL )
  {
    if ( ProcCount < SPV_SERVERGUI_MAX_PROC )
    {
      procID = ProcNextID;
      ProcNextID++;
      while( position == -1 )
      {
        if ( ProcList[current] == 0 )
        {
          position = current;
        }
        else
        {
          current++;
        }
      }
      ProcList[position] = procID;
      ProcCount++;
      ProcStatusTable->RowCount = ProcCount+1;
    }
  }

  return procID;
}

//==============================================================================
/// Funzione per ottenere la posizione in tabella di una procedura
///
/// \date [01.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetProcIDPosition(int procID)
{
  int current = 0;
  int position = -1;

  while( position == -1 )
  {
    if ( ProcList[current] == procID )
    {
      position = current;
    }
    else
    {
      current++;
    }
  }

  return position;
}

//==============================================================================
/// Funzione per visualizzare lo stato di una procedura applicativa
///
/// \date [01.09.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
int SPVShowProcStatus( SPV_PROCEDURE * procedure )
{
  int           ret_code  = 0 ; // Codice di ritorno
  AnsiString    cell          ;
  TStrings    * row           ;
  TDateTime     date_time     ;
  int           pos       = -1;

  if ( procedure != NULL )
  {
    if ( procedure->Tag != 0 ) // Il tag rappresenta ID dell'istanza della procedura (deve essere diverso da 0)
    {
      pos = SPVGetProcIDPosition( (int)procedure->Tag );
      ProcStatusTable->Cells[0][pos+1] = "[" + AnsiString(procedure->ID) + "-" + AnsiString((int)procedure->Tag) + "] " + AnsiString(procedure->Name);
      ProcStatusTable->Cells[1][pos+1] = (procedure->Active)?"attiva":"non attiva";
      ProcStatusTable->Cells[2][pos+1] = "["+AnsiString(procedure->Device->ID)+"] "+AnsiString(procedure->Device->Name);
      if ( procedure->State != NULL )
      {
        date_time = UnixToDateTime(procedure->State->TimeStart);
        ProcStatusTable->Cells[3][pos+1] = "[" + AnsiString(procedure->State->Code) + "] " + AnsiString(procedure->State->Name);
        ProcStatusTable->Cells[4][pos+1] = AnsiString(FormatDateTime( "hh:nn:ss", date_time));
      }
      else
      {
        ProcStatusTable->Cells[3][pos+1] = AnsiString("nessuno");
        ProcStatusTable->Cells[4][pos+1] = AnsiString("__.__.__");
      }
    }
    else
    {
      ret_code = SPV_SERVERGUI_UNSPECIFIED_TAG;
    }
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_PROCEDURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per visualizzare il form principale
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVShowServerMainForm( void )
{
  int           ret_code  = 0 ; // Codice di ritorno

  if ( fServerMain != NULL )
  {
    fServerMain->Show();
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_FORM;
  }

  return ret_code;
}
//==============================================================================
/// Funzione per nascondere il form principale
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVHideServerMainForm( void )
{
  int           ret_code  = 0 ; // Codice di ritorno

  if ( fServerMain != NULL )
  {
    fServerMain->Hide();
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_FORM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per visualizzare il form sinottico delle porte di comunicazione
///
/// \date [24.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVShowSystemView( void * system )
{
  fSystemView->Show();
  if ( fSystemView->SystemTreeView->Items->Count == 0 )
  {
    fSystemView->ViewSystemTree( fSystemView->SystemTreeView, system );
    fSystemView->SystemTreeViewClick( (TObject*) fSystemView );
  }
  //ComProtLogForm->Show();
}

//==============================================================================
/// Funzione per chiudere il sinottico del sistema
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCloseSystemView( void )
{
  fSystemView->Close();
}

//==============================================================================
/// Funzione per visualizzare il sinottico applicativo
///
/// \date [15.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVShowApplicView( void )
{
  int ret_code = 0;

  fApplicativeView->Show();

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il sinottico applicativo
///
/// \date [15.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCloseApplicView( void )
{
  int ret_code = 0;

  fApplicativeView->Close();

  return ret_code;
}

//==============================================================================
/// Funzione per visualizzare il sinottico di protocollo
///
/// \date [04.10.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVShowComProtView( void )
{
  int ret_code = 0;

  fComProtView->Show();

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il sinottico di protocollo
///
/// \date [04.10.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCloseComProtView( void )
{
  int ret_code = 0;

  fComProtView->Close();

  return ret_code;
}

//==============================================================================
/// Funzione per visualizzare il form sinottico delle porte di comunicazione
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVShowPortView( void )
{
  fPortView->Show();
  fPortView->ViewSystemPortStatus();
  fPortView->ViewAutoSize();
}

//==============================================================================
/// Funzione per chiudere il sinottico delle porte di comunicazione
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVClosePortView( void )
{
  fPortView->Close();
}

//==============================================================================
/// Funzione per visualizzate il form sinottico dei database
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVShowDatabaseView( void )
{
  fDatabaseView->Show();
}

//==============================================================================
/// Funzione per chiudere il sinottico dei database
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCloseDatabaseView( void )
{
  fDatabaseView->Close();
}

//==============================================================================
/// Funzione per ottenere lo stato del sinottico del sistema
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetSystemViewStatus( void )
{
  int status = 0;

  status = (fSystemView->Showing)?1:0;

  return status;
}

//==============================================================================
/// Funzione per ottenere lo stato del sinottico applicativo
///
/// \date [15.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetApplicViewStatus( void )
{
  int status = 0;

  status = (fApplicativeView->Showing)?1:0;

  return status;
}

//==============================================================================
/// Funzione per ottenere lo stato del sinottico del protocollo di comunicazione
///
/// \date [04.10.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetComProtViewStatus( void )
{
  int status = 0;

  status = (fComProtView->Showing)?1:0;

  return status;
}

//==============================================================================
/// Funzione per ottenere lo stato del sinottico delle porte di comunicazione
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetPortViewStatus( void )
{
  int status = 0;

  status = (fPortView->Visible)?1:0;

  return status;
}

//==============================================================================
/// Funzione per ottenere lo stato del sinottico dei database
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetDatabaseViewStatus( void )
{
  int status = 0;

  status = (fDatabaseView->Visible)?1:0;

  return status;
}

//==============================================================================
/// Funzione per aggiungere una procedura alla coda
///
/// \date [.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVRequestProcedure( int devID, int procID )
{
  int ret_code = 0;


  return ret_code;
}

//==============================================================================
/// Funzione per costruire il popup menu di una periferica
///
/// \date [21.07.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVBuildDevicePopUpMenu( void * menu, SPV_DEVICE * device )
{
  int                       ret_code  = SPV_SERVERGUI_NO_ERROR;
  TPopupMenu              * popup     = (TPopupMenu*)menu     ;
  TMenuItem               * item      = NULL                  ;
  SPV_PROCEDURE           * procedure = NULL                  ;
  SPV_GUI_FUNCTION_PTR      ptr       = NULL                  ;
  SPV_GUI_FUNCTION_PARAMS   params                      ;
  SPV_GUI_FUNCTION        * function  = NULL                  ;

  if ( popup != NULL )
  {
    if ( device != NULL )
    {
      // --- Aggiungo l'elemento propriet� ---
      item          = new TMenuItem( NULL ); // -NEW
      item->Caption = AnsiString( "Propriet� periferica..." );
      item->Hint    = AnsiString( "Apre la finestra delle propriet� della periferica" );
      item->Visible = true;
      item->Enabled = false;
      item->Checked = false;
      item->OnClick = fSystemView->popDeviceClick;
      item->Tag     = (int)device;
      popup->Items->Add( item );

      // --- Aggiungo una riga ---
      item          = new TMenuItem( NULL ); // -NEW
      item->Caption = AnsiString( "-" );
      item->Hint    = AnsiString( "" );
      item->Visible = true;
      item->Enabled = true;
      item->Checked = false;
      item->OnClick = NULL;
      item->Tag     = 0;
      popup->Items->Add( item );

      // --- Aggiungo l'elemento toggle 'active' ---
      item          = new TMenuItem( NULL ); // -NEW
      item->Caption = AnsiString( "Abilitazione periferica" );
      item->Hint    = AnsiString( "Abilita/Disabilita la periferica" );
      item->Visible = true;
      item->Enabled = true;
      item->Checked = device->SupStatus.Active;
      item->OnClick = fSystemView->ToggleDeviceActive;
      item->Tag     = (int)device;
      popup->Items->Add( item );

      // --- Aggiungo l'elemento toggle 'scheduled' ---
      item          = new TMenuItem( NULL ); // -NEW
      item->Caption = AnsiString( "Schedulazione periferica" );
      item->Hint    = AnsiString( "Attiva/Disattiva la schedulazione della periferica" );
      item->Visible = true;
      item->Enabled = true;
      item->Checked = device->SupStatus.Scheduled;
      item->OnClick = fSystemView->ToggleDeviceScheduled;
      item->Tag     = (int)device;
      popup->Items->Add( item );

      // --- Aggiungo una riga ---
      item          = new TMenuItem( NULL ); // -NEW
      item->Caption = AnsiString( "-" );
      item->Hint    = AnsiString( "" );
      item->Visible = true;
      item->Enabled = true;
      item->Checked = false;
      item->OnClick = NULL;
      item->Tag     = 0;
      popup->Items->Add( item );

      for ( int p = 0; p < SPVProcedureCount; p++ )
      {
        procedure = &SPVProcedureList[p];
        if ( procedure != NULL )
        {
          if ( procedure->Device == device )
          {
            item          = new TMenuItem( NULL ); // -NEW
            item->Caption = AnsiString( procedure->Name );
            item->Hint    = AnsiString( procedure->Name );
            item->Visible = true;
            item->Enabled = (procedure->Active)?false:true;
            item->Checked = false;
            item->OnClick = fSystemView->popSystemClick;

            function = (SPV_GUI_FUNCTION*)malloc( sizeof(SPV_GUI_FUNCTION) ); // -MALLOC
            if ( function != NULL )
            {
              ptr               = (SPV_GUI_FUNCTION_PTR)    SPVExecProcedure;
              params            = (SPV_GUI_FUNCTION_PARAMS) procedure;
              function->params  = params;
              function->ptr     = ptr;
              item->Tag         = (int)function;
            }
            else
            {
              item->Tag = 0;
            }
            popup->Items->Add( item );
          }
        }
      }
    }
    else
    {
      ret_code = SPV_SERVERGUI_INVALID_DEVICE;
    }
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_POPUP;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare il popup menu di una periferica
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVClearDevicePopUpMenu( void * menu )
{
  int                       ret_code  = SPV_SERVERGUI_NO_ERROR;
  TPopupMenu              * popup     = (TPopupMenu*)menu     ;
  TMenuItem               * items     = NULL                  ;
  TMenuItem               * item      = NULL                  ;
  SPV_GUI_FUNCTION        * function  = NULL                  ;
  int                       count     = 0                     ;

  if ( popup != NULL )
  {
    items = popup->Items;
    if ( items != NULL )
    {
      count = items->Count;
      if ( count > 0 )
      {
        for ( int i = 5; i < count; i++ )
        {
          item = items->Items[i];
          if ( item != NULL )
          {
            function  = (SPV_GUI_FUNCTION*)((void*)item->Tag);
            if ( function != NULL )
            {
              try
              {
                free( function );
              }
              catch(...)
              {

              }
            }
          }
        }
        popup->Items->Clear();
      }
    }
    else
    {
      ret_code = SPV_SERVERGUI_INVALID_ITEMLIST;
    }
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_POPUP;
  }

  return ret_code;
}


//==============================================================================
/// Metodo per eseguire l'autodimensionamento di una tabella di stato
///
/// \date [13.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVTableAutoSize( void * table )
{
  #define MAX_COL_COUNT 16

  int               ret_code      = SPV_SERVERGUI_NO_ERROR ;
  TStringGrid     * sgtable       = NULL    ;
  TListView       * lv_app        = NULL    ;
  TComponent      * component     = NULL    ;
  int               row_count     = 0       ;
  int               col_count     = 0       ;
  int               tot_width     = 0       ;
  int               table_width   = 0       ;
  int               string_width  = 0       ;
  int               rem_width     = 0       ;
  int               col_width[MAX_COL_COUNT];

  sgtable = (TStringGrid*) table;

  if ( sgtable != NULL )
  {
    component       = sgtable->GetParentComponent();
    lv_app          = fServerMain->lvMain;
    row_count       = sgtable->RowCount;
    col_count       = sgtable->ColCount;
    table_width     = sgtable->ClientWidth;

    rem_width = table_width;
    // --- Ciclo sulle colonne ---
    for ( int c = 0; c < col_count; c++ )
    {
      col_width[c] = 0;
      // --- Ciclo sulle righe ---
      for ( int r = 0; r < row_count; r++ )
      {
        try
        {
					string_width = (int)lv_app->StringWidth( sgtable->Cells[c][r] ) + (int)8;
        }
        catch(...)
				{
        }
        col_width[c] = ( string_width > col_width[c] ) ? string_width:col_width[c];
      }
      rem_width -= col_width[c];
      if ( c == ( col_count - 1 ) ) col_width[c] += rem_width;
      sgtable->ColWidths[c] = col_width[c];
    }
    if ( rem_width < 0 ) rem_width = 0;
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_STRING_GRID;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per eseguire l'aggiornamento dello stato di un comando di protocollo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [14.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVUpdateCommandStatus( int pos )
{
  int ret_code = SPV_SERVERGUI_NO_ERROR;
  
  /////ret_code = fComProtView->ViewCommandStatus( pos );

  return ret_code;
}

//==============================================================================
/// Metodo per eseguire l'aggiornamento dello stato di una procedura
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [15.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVUpdateProcedureStatus( int pos )
{
  int ret_code = SPV_SERVERGUI_NO_ERROR;

  ret_code = fApplicativeView->ViewProcedureStatus( pos );

  return ret_code;
}

//==============================================================================
/// Metodo per eseguire l'aggiornamento dello stato di una periferica
///
/// \date [22.09.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVUpdateDeviceStatus( SPV_DEVICE * device )
{
  SPV_DEVICE * sel_device = NULL;

  if ( fSystemView != NULL )
  {
    if ( fSystemView->SystemTreeView->Selected != NULL )
    {
      // --- Aggiornamento anche se la periferica non � aggiornata ---
      /* TODO 1 -oEnrico -cGUI : Modificare l'icona della periferica in base al suo stato */
      // --- Aggiornamento solo se la periferica � quella selezionata ---
      sel_device = (SPV_DEVICE*)fSystemView->SystemTreeView->Selected->Data;
      if ( sel_device == device )
      {
        fSystemView->ViewDeviceInfo( device );
        fSystemView->ViewDeviceStatus( device );
      }
    else
    {
#define SPV_DEVICE_OFFLINE                  24
#define SPV_DEVICE_OK                       25
#define SPV_DEVICE_WARNING                  26
#define SPV_DEVICE_ERROR                    27
#define SPV_DEVICE_ALARM                    28
#define SPV_DEVICE_DISABLED                 29

      int code = SPV_DEVICE_OK;

    if ( device->SupStatus.Active )
    {
      if ( device->SupStatus.Alarm )
      {
        code = SPV_DEVICE_ALARM;
      }
      else
      {
        if ( device->SupStatus.Offline )
        {
          code = SPV_DEVICE_OFFLINE;
        }
        else
        {
          if ( device->SupStatus.Level == 0 )
          {
            code = SPV_DEVICE_OK;
          }
          if ( device->SupStatus.Level == 1 )
          {
            code = SPV_DEVICE_WARNING;
          }
          if ( device->SupStatus.Level == 2 )
          {
            code = SPV_DEVICE_ERROR;
          }
        }
      }
    }
    else
    {
      code = SPV_DEVICE_DISABLED;
    }


      fSystemView->SetTreeNodeStatus( (TTreeNode*)device->Tag, code );
    }
    }
  }
}

//==============================================================================
/// Metodo per impostare un'icona di stato
///
/// \date [30.11.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVSetStatusIcon( void * icon, int icon_index )
{
  int           ret_code  = SPV_SERVERGUI_NO_ERROR;
  TIcon       * picon     = (TIcon*)icon;
  TImageList  * plist     = NULL;

  if ( picon != NULL )
  {
    plist = (TImageList*)StatusIconList;
    if ( plist != NULL )
    {
      plist->GetIcon( icon_index, picon );
    }
    else
    {
      ret_code = SPV_SERVERGUI_INVALID_IMAGELIST;
    }
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_ICON;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire il click sul bottone di avvio del server
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVClickStartButton( void )
{
  int ret_code = SPV_SERVERGUI_NO_ERROR;

  if ( fServerMain != NULL )
  {
    fServerMain->bStartServerClick( (TObject*)fServerMain->bStartServer );
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_FORM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire il click sul bottone di arresto del server
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVClickStopButton( void )
{
  int ret_code = SPV_SERVERGUI_NO_ERROR;

  if ( fServerMain != NULL )
  {
    fServerMain->bStopServerClick( (TObject*)fServerMain->bStopServer );
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_FORM;
  }

  return ret_code;
}
//==============================================================================
/// Funzione per eseguire il click sul bottone di chiusura del server
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVClickCloseButton( void )
{
  int ret_code = SPV_SERVERGUI_NO_ERROR;

  if ( fServerMain != NULL )
  {
    fServerMain->bCloseServerClick( (TObject*)fServerMain->bCloseServer );
  }
  else
  {
    ret_code = SPV_SERVERGUI_INVALID_FORM;
  }

  return ret_code;
}
                                            

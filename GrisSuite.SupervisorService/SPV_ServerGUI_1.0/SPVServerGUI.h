//==============================================================================
// Telefin Supervisor Server GUI Module 1.0
//------------------------------------------------------------------------------
// Header Mudulo di gestione interfaccia grafica del server (SPVServerGUI.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.16 (31.08.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVServerGUI.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [31.08.2004]:
// - Prima versione prototipo del modulo.
// 0.02 [01.09.2004]:
// - Aggiunta la funzione SPVServerGUIInit.
// - Aggiunta la funzione SPVSetProcStatusTable.
// - Aggiunta la funzione SPVAddProcStatus.
// - Aggiunta la funzione SPVGetProcIDPosition.
// - Modificata la funzione SPVShowProcStatus.
// 0.03 [27.09.2004]:
// - Aggiunta la funzione SPVShowSystemView.
// - Aggiunta la funzione SPVShowPortView.
// - Aggiunta la funzione SPVShowDatabaseView.
// 0.04 [25.01.2005]:
// - Aggiunta la funzione SPVBuildDevicePopUpMenu.
// 0.05 [26.01.2005]:
// - Aggiunta la definizione del tipo SPV_GUI_FUNCTION_PTR.
// - Aggiunta la definizione del tipo SPV_GUI_FUNCTION_PARAMS.
// - Aggiunta la definizione del tipo SPV_GUI_FUNCTION.
// - Modificata la funzione SPVBuildDevicePopUpMenu.
// 0.06 [07.04.2005]:
// - Modificata la funzione SPVBuildDevicePopUpMenu.
// - Aggiunta la funzione SPVClearDevicePopUpMenu.
// 0.07 [24.05.2005]:
// - Modificata la funzione SPVServerGUIInit.
// 0.08 [13.06.2005]:
// - Aggiunta la funzione SPVTableAutoSize.
// 0.09 [14.06.2005]:
// - Aggiunta la funzione SPVUpdateCommandStatus.
// 0.10 [15.06.2005]:
// - Modificata la funzione SPVShowApplicView.
// - Modificata la funzione SPVCloseApplicView.
// - Modificata la funzione SPVGetApplicViewStatus.
// - Aggiunta la funzione SPVUpdateProcedureStatus.
// 0.11 [30.06.2005]:
// - Modificata la funzione SPVBuildDevicePopUpMenu.
// - Modificata la funzione SPVClearDevicePopUpMenu.
// 0.12 [21.07.2005]:
// - Modificata la funzione SPVBuildDevicePopUpMenu.
// - Aggiunta la funzione SPVUpdateDeviceStatus.
// 0.13 [26.07.2005]:
// - Modificata la funzione SPVUpdateDeviceStatus.
// 0.14 [22.09.2005]:
// - Modificata la funzione SPVUpdateDeviceStatus.
// 0.15 [30.11.2005]:
// - Eliminata la variabile globale imgOk.
// - Eliminata la variabile globale imgWarning.
// - Eliminata la variabile globale imgError.
// - Eliminata la macro SPV_GUI_BMP_CLASS.
// - Eliminata la macro SPV_GUI_BMP_FILENAME_OK.
// - Eliminata la macro SPV_GUI_BMP_FILENAME_WARNING.
// - Eliminata la macro SPV_GUI_BMP_FILENAME_ERROR.
// - Modificata la funzione SPVServerGUIInit.
// - Aggiunta la funzione SPVSetStatusIcon.
// 0.16 [19.12.2005]:
// - Aggiunta la funzione SPVShowServerMainForm.
// - Aggiunta la funzione SPVHideServerMainForm.
// - Aggiunta la funzione SPVClickStartButton.
// - Aggiunta la funzione SPVClickStopButton.
// - Aggiunta la funzione SPVClickCloseButton.
//==============================================================================

#ifndef SPVServerGUIH
#define SPVServerGUIH
#include "SPVApplicative.h"
#include <Graphics.hpp>

#define SPV_SERVERGUI_MAX_PROC                256
#define SPV_SERVERGUI_MAX_COMMAND             50

#define SPV_SERVERGUI_BALLOON_ERROR_TIMEOUT   10
#define SPV_SERVERGUI_BALLOON_WARNING_TIMEOUT 5
#define SPV_SERVERGUI_BALLOON_OK_TIMEOUT      5

#define SPV_SERVERGUI_STATUS_ICON_OK        0
#define SPV_SERVERGUI_STATUS_ICON_WARNING   1
#define SPV_SERVERGUI_STATUS_ICON_ERROR     2
#define SPV_SERVERGUI_STATUS_ICON_CRITICAL  3
#define SPV_SERVERGUI_STATUS_ICON_ALARM     4
#define SPV_SERVERGUI_STATUS_ICON_UNKNOWN   5
#define SPV_SERVERGUI_STATUS_ICON_OFFLINE   6
#define SPV_SERVERGUI_STATUS_ICON_IDLE      7

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------

#define SPV_SERVERGUI_NO_ERROR            0
#define SPV_SERVERGUI_INVALID_PROCEDURE   10000
#define SPV_SERVERGUI_UNSPECIFIED_TAG     10001
#define SPV_SERVERGUI_INVALID_DEVICE      10002
#define SPV_SERVERGUI_INVALID_POPUP       10003
#define SPV_SERVERGUI_INVALID_ITEMLIST    10004
#define SPV_SERVERGUI_INVALID_STRING_GRID 10005
#define SPV_SERVERGUI_INVALID_ICON        10006
#define SPV_SERVERGUI_INVALID_IMAGELIST   10007
#define SPV_SERVERGUI_INVALID_FORM        10008

//==============================================================================
/// Definizione del tipo funzione per GUI
/// \date [26.01.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef int( * SPV_GUI_FUNCTION_PTR )(void*);

//==============================================================================
/// Definizione del tipo parametro per funzione per GUI
/// \date [26.01.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef void * SPV_GUI_FUNCTION_PARAMS;

//==============================================================================
/// Struttura di lancio funzione
/// \date [26.01.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_GUI_FUNCTION
{
  SPV_GUI_FUNCTION_PARAMS params;
  SPV_GUI_FUNCTION_PTR    ptr;
} SPV_GUI_FUNCTION;
//------------------------------------------------------------------------------

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
extern void * StatusIconList;         

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

/// Funzione per inizializzare il modulo
void    SPVServerGUIInit          ( void );
/// Funzione per impostare la tabella delle procedure
void    SPVSetProcStatusTable     ( void * table );
/// Funzione per aggiungere lo stato di una procedura alla tabella
int     SPVAddProcStatus          ( SPV_PROCEDURE * procedure );
/// Funzione per ottenere la posizione in tabella di una procedura
int     SPVGetProcIDPosition      ( int procID  );
/// Funzione per visualizzare lo stato di una procedura applicativa
int     SPVShowProcStatus         ( SPV_PROCEDURE * procedure );
/// Funzione per visualizzare il form principale
int     SPVShowServerMainForm     ( void );
/// Funzione per nascondere il form principale
int     SPVHideServerMainForm     ( void );
/// Funzione per visualizzare il sinottico del sistema
int     SPVShowSystemView         ( void * system );
/// Funzione per chiudere il sinottico del sistema
int     SPVCloseSystemView        ( void );
/// Funzione per visualizzare il sinottico applicativo
int     SPVShowApplicView         ( void );
/// Funzione per chiudere il sinottico applicativo
int     SPVCloseApplicView        ( void );
/// Funzione per visualizzare il sinottico di protocollo
int     SPVShowComProtView        ( void );
/// Funzione per chiudere il sinottico di protocollo
int     SPVCloseComProtView       ( void );
/// Funzione per visualizzare il sinottico delle porte di comunicazione
int     SPVShowPortView           ( void );
/// Funzione per chiudere il sinottico delle porte di comunicazione
int     SPVClosePortView          ( void );
/// Funzione per visualizzate il sinottico dei database
int     SPVShowDatabaseView       ( void );
/// Funzione per chiudere il sinottico dei database
int     SPVCloseDatabaseView      ( void );
/// Funzione per ottenere lo stato del sinottico del sistema
int     SPVGetSystemViewStatus    ( void );
/// Funzione per ottenere lo stato del sinottico applicativo
int     SPVGetApplicViewStatus    ( void );
/// Funzione per ottenere lo stato del sinottico del protocollo di comunicazione
int     SPVGetComProtViewStatus   ( void );
/// Funzione per ottenere lo stato del sinottico delle porte di comunicazione
int     SPVGetPortViewStatus      ( void );
/// Funzione per ottenere lo stato del sinottico dei database
int     SPVGetDatabaseViewStatus  ( void );
/// Funzione per aggiungere una procedura alla coda
int     SPVRequestProcedure       ( int devID, int procID );
/// Funzione per costruire il popup menu di una periferica
int     SPVBuildDevicePopUpMenu   ( void * menu, SPV_DEVICE * device );
/// Funzione per eliminare il popup menu di una periferica
int     SPVClearDevicePopUpMenu   ( void * menu );
/// Metodo per eseguire l'autodimensionamento di una tabella di stato
int     SPVTableAutoSize          ( void * table );

/// Metodo per eseguire l'aggiornamento dello stato di un comando di protocollo
int     SPVUpdateCommandStatus    ( int pos );

/// Metodo per eseguire l'aggiornamento dello stato di una procedura
int     SPVUpdateProcedureStatus  ( int pos );
/// Metodo per eseguire l'aggiornamento dello stato di una periferica
int     SPVUpdateDeviceStatus     ( SPV_DEVICE * device );
/// Metodo per impostare un'icona di stato
int     SPVSetStatusIcon          ( void * icon, int icon_index );

/// Funzione per eseguire il click sul bottone di avvio del server
int     SPVClickStartButton       ( void );
/// Funzione per eseguire il click sul bottone di arresto del server
int     SPVClickStopButton        ( void );
/// Funzione per eseguire il click sul bottone di chiusura del server
int     SPVClickCloseButton       ( void );

//------------------------------------------------------------------------------
#endif

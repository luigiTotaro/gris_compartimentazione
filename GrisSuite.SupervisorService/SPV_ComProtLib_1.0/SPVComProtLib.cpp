//==============================================================================
// Telefin Supervisor Communication Protocol Library 1.0
//------------------------------------------------------------------------------
// Libreria per il protocollo di comunicazione (SPVComProtLib.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.23 (09.09.2004 -> 29.08.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVComProtLib.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVComProtLib.h
//==============================================================================
#pragma hdrstop
#include "SPVComProtLib.h"
#include "SPVComProt.h"
#include "XMLInterface.h"
#include <time.h>
#include <math.h>
#pragma package(smart_init)

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------

//==============================================================================
// Implementazione funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione di caricamento della libreria
///
/// Non restituisce nessun valore.
///
/// \date [13.01.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
void SPVComProtLibInit( void )
{
	XMLAddLibFunction( 3434 , SPVCPL_343_4                  , "t_u_hex_16"  , "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_int_8,t_u_int_8,t_u_int_8" );
	XMLAddLibFunction( 3435 , SPVCPL_343_5                  , "t_u_hex_16"  , "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8" );
    XMLAddLibFunction( 34351, SPVCPL_343_5_A                , "t_u_hex_16"  , "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_16,t_u_int_16,t_u_int_16,t_u_int_16,t_u_int_16" );
    XMLAddLibFunction( 3439 , SPVCPL_343_9                  , "t_u_hex_16"  , "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8" );
    XMLAddLibFunction( 3430 , SPVCPL_343_X                  , "t_u_hex_16"  , "t_u_hex_16,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8,t_u_int_8" );
    XMLAddLibFunction( 34352, SPVCPL_343_5_S                , "t_u_hex_16"  , "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8" );
	XMLAddLibFunction( 34322, SPVCPL_CRC_X_S                , "t_u_hex_16"  , "t_u_hex_16,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8" );
    XMLAddLibFunction( 34321, SPVCPL_CRC_X_A                , "t_u_hex_16"  , "t_u_hex_16,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8" );
	XMLAddLibFunction( 53322, SPVCPL_BCC_X_S				, "t_u_hex_8"	, "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8" );
    XMLAddLibFunction( 53321, SPVCPL_BCC_X_A				, "t_u_hex_8"	, "t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8,t_u_hex_8" );
	XMLAddLibFunction( 3842 , SPVCPL_DIV_2                  , "t_u_hex_8"   , "t_u_hex_8" );
	XMLAddLibFunction( 38428, SPVCPL_DIV_2_INT              , "t_u_hex_8"   , "t_u_int_8" );
	XMLAddLibFunction( 3153 , SPVCPL_3153_DateTime          , "t_string"    , "t_string"  );
	XMLAddLibFunction( 535  , SPVCPL_535_DataBlock          , "t_string"    , "t_u_hex_8,t_hex_8,t_u_int_16,t_u_int_16,t_u_int_16" );
	XMLAddLibFunction( 5353 , SPVCPL_5353_End               , "t_u_hex_8"   , "t_u_hex_8,t_hex_8,t_u_int_16,t_u_int_16,t_u_hex_8" );
	XMLAddLibFunction( 5355 , SPVCPL_5355_Buffer            , "t_string"    , "t_u_hex_8,t_hex_8,t_u_int_16,t_u_int_16,t_u_hex_8" );
	XMLAddLibFunction( 5359 , SPVCPL_5359_Len               , "t_string"    , "t_u_hex_8,t_hex_8,t_u_int_16,t_u_int_16,t_u_hex_8" );
	XMLAddLibFunction( 533  , SPVCPL_533_TestConfigData     , "t_string"    , "t_u_int_8"   );
	XMLAddLibFunction( 5322 , SPVCPL_5322_GetDevStreamBuffer, "t_string"    , "t_u_int_8"   );
	XMLAddLibFunction( 5596 , SPVCPL_5596_GetLenFromField   , "t_u_int_16"  , "t_u_int_16"  );
	XMLAddLibFunction( 5429 , SPVCPL_5429_GetFieldStringLen , "t_u_int_16"  , "t_string"    );
	XMLAddLibFunction( 5430 , SPVCPL_5430_GetFieldStringLen , "t_u_int_16"	, "t_string" 		);
	XMLAddLibFunction( 54468, SPVCPL_54468_GetLenUHex8FromField, "t_u_hex_8", "t_u_hex_8" );
	XMLAddLibFunction( 54488, SPVCPL_54488_GetLenUInt8FromField, "t_u_hex_8", "t_u_int_8" );
}

//==============================================================================
/// Funzione per chiudere la libreria
///
/// Non restituisce nessun valore.
///
/// \date [29.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVComProtLibClear( void )
{
  XMLInterfaceLibClear( );
}

//==============================================================================
/// Funzione per generare frame contenenti blocchi di dati
///
/// Dal buffer binario <buffer> lungo <buflen> byte viene estratto il blocco
/// <num>-esimo di dati lungo al massimo <maxlen> per utilizzarlo poi in un
/// frame. Alla fine del blocco viene aggiunto un codice di fine blocco (ETB) o
/// un codice di fine dati (ETX).
///
/// In caso di errore restituisce una struttura con lunghezza 0 e buffer NULL.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void * SPVBuildDataBlock( char * buffer, int buflen, int maxlen, int num )
{
  SPV_COMPROT_BIN_BUFFER  * blockbuf  = NULL  ; // Struttura del blocco da restituire
  int                       blocklen  = 0     ; // Lunghezza del blocco da generare
  int                       blockmax  = 0     ; // Il massimo numero di blocchi da generare dal buffer
  int                       modlen    = 0     ; // Resto
  int                       offset    = 0     ; // Offset del buffer da dove prendere il blocco
  bool                      endblock  = false ; // Flag di blocco finale

  blockbuf = SPVNewBinBuffer( );
  if ( buffer != NULL && buflen > 0 ) // Il ptr al buffer e la sua lunghezza devono essere validi
  {
    if ( maxlen > 0 ) // La lunghezza massima per il blocco deve essere valida
    {
      // --- Calcolo la lunghezza del blocco ---
      blockmax = (int)ceil( (double)buflen / (double)maxlen );
      if ( blockmax > 0 ) // Se posso genrare almeno un blocco dal buffer
      {
        if ( num >= 0 && num < blockmax ) // Se il numero del blocco e' valido
        {
          if ( num < ( blockmax - 1 ) ) // Se non � l'ultimo blocco
          {
            blocklen = maxlen; // Lunghezza massima
          }
          else
          {
            modlen   = buflen % maxlen;
            blocklen = ( modlen > 0 ) ? modlen : maxlen; // Resto o lunghezza massima (se resto nullo)
            endblock = true;
          }
          // --- Genero il blocco dati ---
          if ( blocklen > 0 ) // Se la lunghezza del blocco � valida
          {
            offset = num * maxlen;
            if ( SPVValidBinBuffer( blockbuf ) )
            {
              if ( endblock )
              {
                blockbuf->buffer[blocklen] = 0x03; // ETX
              }
              else
              {
                blockbuf->buffer[blocklen] = 0x17; // ETB
              }
              try
              {
                blockbuf->len    = blocklen;
                memcpy( blockbuf->buffer, &buffer[offset], sizeof(char) * blocklen );
              }
              catch(...)
              {
                blockbuf->len = 0;
              }
            }
          }
        }
      }
    }
  }

  return (void*) blockbuf;
}

//==============================================================================
/// Funzione di CheckSum CRC 16
/// Se specificato il puntatore <checksum> la funzione lo riempir� con i 16 bit
/// di checksum calcolati sulla sequenza di byte <buffer> di lunghezza <len>.
/// Se viene passato un ptr <checksum> NULL la funzione alloca un nuovo buffer
/// da 2 byte per contenere il checksum calcolato.
/// Nota: il polinomio usato � x^16 + x^15 + x^2 + 1.
///
/// In caso di errore restituisce NULL.
///
/// \date [09.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVCheckSumCRC16( char * buffer, long len, char * checksum )
{
  int     crc       = 0   ; // Valore di appoggio per il calcolo del checksum
  int     ncrc      = 0   ; // Valore di appoggio per il calcolo del checksum
  char  * buffer_cs = NULL; // Buffer del checksum

  if ( buffer != NULL && len > 0 ) // Controllo il ptr e la lunghezza del buffer su cui calcolare il cs
  {
    if ( checksum == NULL ) // Non ho specificato un buffer preesistente per il cs
    {
      buffer_cs = (char*)malloc(sizeof(char)*2);
    }
    else
    {
      buffer_cs = checksum;
    }
    /* Calcolo il checksum CRC16 lungo tutta la lunghezza del buffer */
    for ( long i = 0; i < len; i++ )
    {
	    ncrc = crc << 8 ;
	  	ncrc ^= buffer[i] ;
      if ( (crc & 0x8000) != 0 ) ncrc ^= 0x8303;
      if ( (crc & 0x4000) != 0 ) ncrc ^= 0x8183;
      if ( (crc & 0x2000) != 0 ) ncrc ^= 0x80c3;
      if ( (crc & 0x1000) != 0 ) ncrc ^= 0x8063;
      if ( (crc & 0x0800) != 0 ) ncrc ^= 0x8033;
      if ( (crc & 0x0400) != 0 ) ncrc ^= 0x801b;
      if ( (crc & 0x0200) != 0 ) ncrc ^= 0x800f;
      if ( (crc & 0x0100) != 0 ) ncrc ^= 0x8005;
      crc = ncrc ;
    }
    /* Copio il risultato del checksum nel buffer apposito */
    buffer_cs[0] = (char)( ncrc >> 8 );
    buffer_cs[1] = (char)( ncrc );
  }

	return buffer_cs;
}

//==============================================================================
/// Funzione per verificare che un parametro sia valido
///
///
/// \date [13.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidParam( void * param_list, int num )
{
  XML_INT_PARAM * param = NULL  ; // Ptr alla struttura del parametro
  int             id    = 0     ; // ID del parametro
  bool            valid = false ; // Flag di parametro valido

  if ( param_list != NULL )
  {
    param = &(((XML_INT_PARAM*)param_list)[num]);
    if ( param != NULL )
    {
      try
      {
        id = param->id;
      }
      catch (...)
      {
        id = 666;
      }
      if ( id != 666 )
      {
        valid = true;
      }
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per ottenere parametri char ptr in formato ASCII
///
/// \date [13.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetCharPtrASCIIParam( void * param_list, int num )
{
  char            * value     = NULL  ; // Valore da restituire
  XML_INT_PARAM   * param     = NULL  ; // Ptr alla struttura del parametro

  if ( param_list != NULL ) // Controllo il ptr alla lista
  {
    param = &(((XML_INT_PARAM*)param_list)[num]);
    if ( SPVValidParam( param, 0 ) ) // Controllo la validit� del parametro
    {
      if ( param->ASCII )
      {
        value = (char*) param->value;
      }
      else
      {
        value = XMLType2ASCII( param->value, param->type ); // -MALLOC
      }
    }
    else
    {
      // --- Gestione dell'errore ---
    }
  }
  else
  {
    // --- Gestione dell'errore ---
  }

  return value;
}

//==============================================================================
/// Funzione per ottenere parametri char ptr
///
/// \date [13.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetCharPtrParam( void * param_list, int num )
{
  char            * value     = NULL  ; // Valore da restituire
  XML_INT_PARAM   * param     = NULL  ; // Ptr alla struttura del parametro

  if ( param_list != NULL ) // Controllo il ptr alla lista
  {
    param = &(((XML_INT_PARAM*)param_list)[num]);
    if ( SPVValidParam( param, 0 ) ) // Controllo la validit� del parametro
    {
      value = (char*) param->value;
    }
    else
    {
      // --- Gestione dell'errore ---
    }
  }
  else
  {
    // --- Gestione dell'errore ---
  }

  return value;
}

//==============================================================================
/// Funzione per ottenere parametri int in formato ASCII
///
/// \date [13.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetIntASCIIParam( void * param_list, int num )
{
  char            * value     = NULL  ; // Valore da restituire
  XML_INT_PARAM   * param     = NULL  ; // Ptr alla struttura del parametro

  if ( param_list != NULL )
  {
    param = &(((XML_INT_PARAM*)param_list)[num]);
    if ( SPVValidParam( param, 0 ) ) // Controllo la validit� del parametro
    {
      if ( param->ASCII )
      {
        value = (char*) param->value;
      }
      else
      {
        value = XMLType2ASCII( param->value, param->type ); // -MALLOC
      }
    }
    else
    {
      // --- Gestione dell'errore ---
    }
  }
  else
  {
    // --- Gestione dell'errore ---
  }

  return value;
}

//==============================================================================
/// Funzione per ottenere parametri int
///
/// \date [07.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetIntParam( void * param_list, int num )
{
  int                 value     = 0     ; // Valore da restituire
  XML_INT_PARAM     * param     = NULL  ; // Ptr alla struttura del parametro
  int               * ptr_value = NULL  ; // Ptr al parametro buffer
  __int8            * value_8   = NULL  ;
  __int16           * value_16  = NULL  ;
  __int32           * value_32  = NULL  ;
  unsigned __int8   * value_u8  = NULL  ;
  unsigned __int16  * value_u16 = NULL  ;
  unsigned __int32  * value_u32 = NULL  ;

  if ( param_list != NULL )
  {
    param = &(((XML_INT_PARAM*)param_list)[num]);
    if ( SPVValidParam( param, 0 ) ) // Controllo la validit� del parametro
    {
      if ( param->ASCII )
      {
        switch ( param->type )
        {
          case t_u_int_8:
            value  = *(__int8*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_u_int_16:
            value_u16 = (unsigned __int16*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
            value  = *value_u16;
            if ( value == 9 || value == 11 || value == 12 )
            {
              value = value;
            }
          break;
          case t_u_int_32:
            value  = *(__int32*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_s_int_8:
            value  = *(__int8*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_s_int_16:
            value  = *(__int16*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_s_int_32:
            value  = *(__int32*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_u_hex_8:
            value  = *(__int8*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_u_hex_16:
            value  = *(__int16*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_u_hex_32:
            value  = *(__int32*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_s_hex_8:
            value  = *(__int8*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_s_hex_16:
            value  = *(__int16*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          case t_s_hex_32:
            value  = *(__int32*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
          break;
          default:
            value  = *(int*)XMLASCII2Type( (char*)param->value, param->type ); // -MALLOC
        }
      }
      else
      {
        switch ( param->type )
        {
          case t_u_int_8:
            value = *((__int8*) param->value);
          break;
          case t_u_int_16:
            value = *((__int16*) param->value);
          break;
          case t_u_int_32:
            value = *((__int32*) param->value);
          break;
          case t_s_int_8:
            value = *((__int8*) param->value);
          break;
          case t_s_int_16:
            value = *((__int16*) param->value);
          break;
          case t_s_int_32:
            value = *((__int32*) param->value);
          break;
          case t_u_hex_8:
            value = *((__int8*) param->value);
          break;
          case t_u_hex_16:
            value = *((__int16*) param->value);
          break;
          case t_u_hex_32:
            value = *((__int32*) param->value);
          break;
          case t_s_hex_8:
            value = *((__int8*) param->value);
          break;
          case t_s_hex_16:
            value = *((__int16*) param->value);
          break;
          case t_s_hex_32:
            value = *((__int32*) param->value);
          break;
          default:
            value = *((int*) param->value);
        }
      }
    }
    else
    {
      // --- Gestione dell'errore ---
    }
  }
  else
  {
    // --- Gestione dell'errore ---
  }

  return value;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su 4 byte
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.03.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_343_4( void * output, void * input )
{
  int ret_code = 0;

  unsigned __int8     in_8[4]       ;
  unsigned __int8     in_ASCII[4]   ;
  char                buffer[8]     ;
  int                 buffer_len    ;
  int                 buffer_offset ;
  char              * ASCII_value   ;
  unsigned __int16  * out_16 = (unsigned __int16*)malloc(sizeof(unsigned __int16));

  XML_INT_PARAM * output_param = (XML_INT_PARAM*) output;
  XML_INT_PARAM * input_param  = (XML_INT_PARAM*) input;

  if ( input_param[0].value != NULL
    && input_param[1].value != NULL
    && input_param[2].value != NULL
    && input_param[3].value != NULL
    && input_param[4].value != NULL
    && input_param[5].value != NULL
    && input_param[6].value != NULL
    && input_param[7].value != NULL )
  {
    in_8[0]     = *((unsigned __int8 *) input_param[0].value);
    in_8[1]     = *((unsigned __int8 *) input_param[1].value);
    in_8[2]     = *((unsigned __int8 *) input_param[2].value);
    in_8[3]     = *((unsigned __int8 *) input_param[3].value);
    in_ASCII[0] = *((unsigned __int8 *) input_param[4].value);
    in_ASCII[1] = *((unsigned __int8 *) input_param[5].value);
    in_ASCII[2] = *((unsigned __int8 *) input_param[6].value);
    in_ASCII[3] = *((unsigned __int8 *) input_param[7].value);

    buffer_len    = 0;
    buffer_offset = 0;

    for (int i=0; i<4; i++)
    {
      if (in_ASCII[i] == 1)
      {
        ASCII_value = XMLType2ASCII((void*)&in_8[i],t_u_hex_8);
        if (ASCII_value)
        {
          buffer[buffer_offset]   = ASCII_value[0];
          buffer[buffer_offset+1] = ASCII_value[1];
          free(ASCII_value);
        }
        else
        {
          buffer[buffer_offset]   = 0;
          buffer[buffer_offset+1] = 0;
        }
        buffer_offset += 2;
      }
      else
      {
        buffer[buffer_offset]     = in_8[i];
        buffer_offset++;
      }
    }

    buffer_len = buffer_offset;

    SPVCheckSumCRC16(buffer,buffer_len,(char*)&out_16[0]);

    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
    ret_code = 1;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su 5 byte
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.03.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_343_5( void * output, void * input )
{
  int ret_code = 0;

  unsigned __int8     in_8[5]       ;
  unsigned __int8     in_ASCII[5]   ;
  char                buffer[10]    ;
  int                 buffer_len    ;
  int                 buffer_offset ;
  char              * ASCII_value   ;
  unsigned __int16  * out_16 = (unsigned __int16*)malloc(sizeof(unsigned __int16));

  XML_INT_PARAM * output_param = (XML_INT_PARAM*) output;
  XML_INT_PARAM * input_param  = (XML_INT_PARAM*) input;

  if ( input_param[0].value != NULL
    && input_param[1].value != NULL
    && input_param[2].value != NULL
    && input_param[3].value != NULL
    && input_param[4].value != NULL
    && input_param[5].value != NULL
    && input_param[6].value != NULL
    && input_param[7].value != NULL
    && input_param[8].value != NULL
    && input_param[9].value != NULL )
  {
    in_8[0]     = *((unsigned __int8 *) input_param[0].value);
    in_8[1]     = *((unsigned __int8 *) input_param[1].value);
    in_8[2]     = *((unsigned __int8 *) input_param[2].value);
    in_8[3]     = *((unsigned __int8 *) input_param[3].value);
    in_8[4]     = *((unsigned __int8 *) input_param[4].value);
    in_ASCII[0] = *((unsigned __int8 *) input_param[5].value);
    in_ASCII[1] = *((unsigned __int8 *) input_param[6].value);
    in_ASCII[2] = *((unsigned __int8 *) input_param[7].value);
    in_ASCII[3] = *((unsigned __int8 *) input_param[8].value);
    in_ASCII[4] = *((unsigned __int8 *) input_param[9].value);

    buffer_len    = 0;
    buffer_offset = 0;

    for (int i=0; i<5; i++)
    {
      if (in_ASCII[i] == 1)
      {
        ASCII_value = XMLType2ASCII((void*)&in_8[i],t_u_hex_8);
        if (ASCII_value)
        {
          buffer[buffer_offset]   = ASCII_value[0];
          buffer[buffer_offset+1] = ASCII_value[1];
          free(ASCII_value);
        }
        else
        {
          buffer[buffer_offset]   = 0;
          buffer[buffer_offset+1] = 0;
        }
        buffer_offset += 2;
      }
      else
      {
        buffer[buffer_offset]     = in_8[i];
        buffer_offset++;
      }
    }

    buffer_len = buffer_offset;

    SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] );

    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
    ret_code            = 1;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su 5 array di byte
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.03.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCPL_343_5_A( void * output, void * input )
{
  int                 ret_code      = 0     ;

  unsigned __int8   * in_8            [5]   ; // Array di byte
  unsigned __int8     in_ASCII        [5]   ; // Flag di valori in formato ASCII
  unsigned __int16    in_dim          [5]   ; // Dimensioni degli array (16 bit!)
  char              * buffer        = NULL  ;
  int                 buffer_len            ;
  int                 buffer_offset         ;
  char              * ASCII_value           ;
  unsigned __int16  * out_16        = (unsigned __int16*)malloc(sizeof(unsigned __int16));
  int                 array_dim             ; // Dimensione di un array di byte
  unsigned __int8   * array         = NULL  ;

  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

  // --- Controllo gli input passati alla funzione ---
  if ( input_param[0].value   != NULL
    && input_param[1].value   != NULL
    && input_param[2].value   != NULL
    && input_param[3].value   != NULL
    && input_param[4].value   != NULL
    && input_param[5].value   != NULL
    && input_param[6].value   != NULL
    && input_param[7].value   != NULL
    && input_param[8].value   != NULL
    && input_param[9].value   != NULL
    && input_param[10].value  != NULL
    && input_param[11].value  != NULL
    && input_param[12].value  != NULL
    && input_param[13].value  != NULL
    && input_param[14].value  != NULL )
  {
    in_8[0]     =  ((unsigned __int8 *) input_param[0].value);
    in_8[1]     =  ((unsigned __int8 *) input_param[1].value);
    in_8[2]     =  ((unsigned __int8 *) input_param[2].value);
    in_8[3]     =  ((unsigned __int8 *) input_param[3].value);
    in_8[4]     =  ((unsigned __int8 *) input_param[4].value);
    in_ASCII[0] = *((unsigned __int8 *) input_param[5].value);
    in_ASCII[1] = *((unsigned __int8 *) input_param[6].value);
    in_ASCII[2] = *((unsigned __int8 *) input_param[7].value);
    in_ASCII[3] = *((unsigned __int8 *) input_param[8].value);
    in_ASCII[4] = *((unsigned __int8 *) input_param[9].value);
    in_dim[0]   = *((unsigned __int16 *) input_param[10].value);
    in_dim[1]   = *((unsigned __int16 *) input_param[11].value);
    in_dim[2]   = *((unsigned __int16 *) input_param[12].value);
    in_dim[3]   = *((unsigned __int16 *) input_param[13].value);
    in_dim[4]   = *((unsigned __int16 *) input_param[14].value);

    buffer = (char*)malloc(sizeof(char)*((in_dim[0]*2)+(in_dim[1]*2)+(in_dim[2]*2)+(in_dim[3]*2)+(in_dim[4]*2)+1));
    buffer_len    = 0;
    buffer_offset = 0;
    // --- Ciclo sugli input ---
    for ( int i=0; i<5; i++ )
    {
      array     = in_8[i];
      array_dim = in_dim[i];
      // --- Ciclo sugli elementi e-esimi dell'array i-esimo di input ---
      for ( int e=0; e<array_dim; e++ )
      {
        if ( in_ASCII[i] == 1 )
        {
          ASCII_value = XMLType2ASCII((void*)(&array[e]),t_u_hex_8);
          if ( ASCII_value != NULL ) // Se ho ricevuto un valore non NULL
          {
            buffer[buffer_offset]   = ASCII_value[0];
            buffer[buffer_offset+1] = ASCII_value[1];
            free(ASCII_value);
          }
          else // Se il valore � NULL
          {
            buffer[buffer_offset]   = 0;
            buffer[buffer_offset+1] = 0;
          }
          buffer_offset += 2;
        }
        else
        {
          buffer[buffer_offset]     = array[e];
          buffer_offset++;
        }
      }
    }

    buffer_len = buffer_offset;

    SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] );

    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
    free( buffer );
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
    ret_code            = 1;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su 9 byte
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.03.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_343_9( void * output, void * input )
{
  int ret_code = 0;

  unsigned __int8     in_8[9]       ;
  unsigned __int8     in_ASCII[9]   ;
  char                buffer[18]    ;
  int                 buffer_len    ;
  int                 buffer_offset ;
  char              * ASCII_value   ;
  unsigned __int16  * out_16 = (unsigned __int16*)malloc(sizeof(unsigned __int16));

  XML_INT_PARAM * output_param = (XML_INT_PARAM*) output;
  XML_INT_PARAM * input_param  = (XML_INT_PARAM*) input;

  if ( input_param[0].value  != NULL
    && input_param[1].value  != NULL
    && input_param[2].value  != NULL
    && input_param[3].value  != NULL
    && input_param[4].value  != NULL
    && input_param[5].value  != NULL
    && input_param[6].value  != NULL
    && input_param[7].value  != NULL
    && input_param[8].value  != NULL
    && input_param[9].value  != NULL
    && input_param[10].value != NULL
    && input_param[11].value != NULL
    && input_param[12].value != NULL
    && input_param[13].value != NULL
    && input_param[14].value != NULL
    && input_param[15].value != NULL
    && input_param[16].value != NULL
    && input_param[17].value != NULL )
  {
    in_8[0]     = *((unsigned __int8 *) input_param[0].value);
    in_8[1]     = *((unsigned __int8 *) input_param[1].value);
    in_8[2]     = *((unsigned __int8 *) input_param[2].value);
    in_8[3]     = *((unsigned __int8 *) input_param[3].value);
    in_8[4]     = *((unsigned __int8 *) input_param[4].value);
    in_8[5]     = *((unsigned __int8 *) input_param[5].value);
    in_8[6]     = *((unsigned __int8 *) input_param[6].value);
    in_8[7]     = *((unsigned __int8 *) input_param[7].value);
    in_8[8]     = *((unsigned __int8 *) input_param[8].value);
    in_ASCII[0] = *((unsigned __int8 *) input_param[9].value);
    in_ASCII[1] = *((unsigned __int8 *) input_param[10].value);
    in_ASCII[2] = *((unsigned __int8 *) input_param[11].value);
    in_ASCII[3] = *((unsigned __int8 *) input_param[12].value);
    in_ASCII[4] = *((unsigned __int8 *) input_param[13].value);
    in_ASCII[5] = *((unsigned __int8 *) input_param[14].value);
    in_ASCII[6] = *((unsigned __int8 *) input_param[15].value);
    in_ASCII[7] = *((unsigned __int8 *) input_param[16].value);
    in_ASCII[8] = *((unsigned __int8 *) input_param[17].value);

    buffer_len    = 0;
    buffer_offset = 0;

    for (int i=0; i<9; i++)
    {
      if (in_ASCII[i] == 1)
      {
        ASCII_value = XMLType2ASCII((void*)&in_8[i],t_u_hex_8);
        if (ASCII_value)
        {
          buffer[buffer_offset]   = ASCII_value[0];
          buffer[buffer_offset+1] = ASCII_value[1];
          free(ASCII_value);
        }
        else
        {
          buffer[buffer_offset]   = 0;
          buffer[buffer_offset+1] = 0;
        }
        buffer_offset += 2;
      }
      else
      {
        buffer[buffer_offset]     = in_8[i];
        buffer_offset++;
      }
    }

    buffer_len = buffer_offset;

    SPVCheckSumCRC16(buffer,buffer_len,(char*)&out_16[0]);

    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
    ret_code            = 1;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su sequenza di byte generalizzata
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.03.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCPL_343_X( void * output, void * input )
{
  int                 ret_code      = 0     ;
  unsigned __int16    length        = 0     ; // Lunghezza della sequenza di byte (1 - 65536)
  unsigned __int8     in_8          [65536] ;
  unsigned __int8     in_ASCII      [65536] ;
  char                buffer        [131070];
  int                 sequence              ;
  int                 buffer_len            ;
  int                 buffer_offset         ;
  char              * ASCII_value           ;
  unsigned __int16  * out_16        = (unsigned __int16*)malloc(sizeof(unsigned __int16));

  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input;

  if ( input_param[0].value != NULL )
  {
    length = *((unsigned __int16 *) input_param[0].value);
    if ( length >= 1 && length <= 65535 )
    {
      buffer_len = (int) length;
      // --- Ciclo di controllo input ---
      for ( int i = 1; i <= (buffer_len*2); i++ )
      {
        if ( input_param[0].value != NULL )
        {
          // --- Carico i valori di input ---
          in_8    [i] = *((unsigned __int8 *) input_param[i].value);
          in_ASCII[i] = *((unsigned __int8 *) input_param[buffer_len+i].value);
        }
        else
        {
          // --- Input i-esimo non valido ---
          ret_code = 1;
          break;
        }
      }
      if ( ret_code == 0 )
      {
        buffer_offset = 0;

        for ( int i = 1; i <= buffer_len; i++ )
        {
          if ( in_ASCII[i] == 1 ) // Se il valore � espresso in formato ASCII
          {
            ASCII_value = XMLType2ASCII((void*)&in_8[i],t_u_hex_8); // Conversione
            if ( ASCII_value != NULL )
            {
              buffer[buffer_offset]   = ASCII_value[0];
              buffer[buffer_offset+1] = ASCII_value[1];
              free(ASCII_value);
            }
            else
            {
              buffer[buffer_offset]   = 0;
              buffer[buffer_offset+1] = 0;
            }
            buffer_offset += 2;
          }
          else
          {
            buffer[buffer_offset]     = in_8[i];
            buffer_offset++;
          }
        }

        buffer_len = buffer_offset; // Ricalcolo la lunghezza del buffer

        SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] ); // Calcolo il checksum
      }
    }
    else
    {
      // --- Valore della lunghezza fuori dal range ---
      ret_code = 1;
    }
  }
  else
  {
    // --- Input della lunghezza non valido ---
    ret_code = 1;
  }

  if ( ret_code == 0 ) // Nessun errore
  {
    output_param->id    = 0;
    output_param->type  = t_u_int_16;
    output_param->value = (void*) out_16;
  }
  else // Si � verificato un errore
  {
    output_param->id    = 0;
    output_param->type  = t_u_int_16;
    output_param->value = NULL;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su sequenza generalizzata di byte
///
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [14.03.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCPL_CRC_X_S( void * output, void * input )
{
  int                 ret_code      = 0   ;
  unsigned __int8   * data          = NULL; // Ptr ad un dato
  char                buffer[64]          ;
  int                 buffer_len    = 0   ;
  int                 buffer_offset = 0   ;
  unsigned __int16  * out_16        = NULL;
  unsigned __int16    length        = 0   ; // Lunghezza della sequenza di byte (1 - 65535)
  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input;

  try
  {
    if ( input_param[0].value != NULL )
    {
      data    = (unsigned __int8*) input_param[0].value;
      length  = *((unsigned __int16*)data);
      if ( length >= 1 && length <= 32 )
      {
        buffer_len    = (int) length;
        buffer_offset = 0;
        // --- Ciclo di controllo input ---
        for ( int i = 1; i <= buffer_len; i++ )
        {
          if ( input_param[i].value != NULL )
          {
            data = (unsigned __int8 *)input_param[i].value;
            if ( input_param[i].ASCII == true )
            {
              buffer[buffer_offset]   = data[0];
              buffer[buffer_offset+1] = data[1];
              buffer_offset += 2;
            }
            else
            {
              buffer[buffer_offset]   = data[0];
              buffer_offset++;
            }
          }
          else
          {
            ret_code = 1;
            break;
          }
        }
      }
    }
    else
    {
      ret_code = 1;
    }
  }
  catch(...)
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == 0 )
  {
    buffer_len = buffer_offset;

    out_16 = (unsigned __int16*)malloc(sizeof(unsigned __int16));
    SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] );
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su sequenza generalizzata di array di byte
///
/// NOTA: Per campi di input in formato ASCII � possibile non specificare la
/// lunghezza passando il valore 0. La lunghezza sar� considerata variabile e
/// verr� calcolata dinamicamente basandosi sul valore che dovr� essere perci�
/// NULL-terminato.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVCPL_CRC_X_A( void * output, void * input )
{
  int                 ret_code      = 0     ;
  unsigned __int8   * data          = NULL  ; // Ptr ad un dato
  char                buffer[4080]          ;
  int                 buffer_len    = 0     ;
  int                 buffer_offset = 0     ;
  int                 data_offset   = 0     ;
  unsigned __int16  * out_16        = NULL  ;
  unsigned __int16    length        = 0     ; // Lunghezza della sequenza di byte (1 - 16)
  unsigned __int8   * array_len     = NULL  ; // Lunghezza dell'array (0-255)
  int                 array_dim     = 0     ;
  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;
  unsigned __int8   * local_data8   = NULL  ;

  try
  {
    if ( input_param[0].value != NULL )
    {
      data    = (unsigned __int8*) input_param[0].value;
      length  = *((unsigned __int16*)data);
      if ( length >= 1 && length <= 16 )
      {
        buffer_len    = (int) length;
        buffer_offset = 0;
        // --- Ciclo di controllo input ---
        for ( int i = 1; i <= buffer_len; i++ )
        {
          if ( input_param[i].value         != NULL
            && input_param[length+i].value  != NULL )
          {
            data = (unsigned __int8 *)input_param[length+i].value;
            if ( input_param[length+i].ASCII == true )
            {
              local_data8 = (unsigned __int8 *)XMLASCII2Type( (char*)data, t_u_hex_8 );
              array_len = local_data8;
            }
            else
            {
              array_len = data;
            }
            try
            {
              array_dim = (int) *array_len;
            }
            catch(...)
            {
              array_dim = 0;
            }
            // --- Gestione della lunghezza variabile ---
            if ( array_dim == 0 )
            {
              if ( input_param[i].ASCII == true ) // Solo nel caso di valori ASCII
              {
                array_dim = strlen( (char*) input_param[i].value ) / 2; // Ricavo la lunghezza direttamente dalla stringa
              }
            }
            data = (unsigned __int8 *)input_param[i].value;
            data_offset = 0;
            for ( int e = 0; e < array_dim; e++ )
            {
              if ( input_param[i].ASCII == true )
              {
				try
				{
					buffer[buffer_offset]   = data[data_offset+0];
					buffer[buffer_offset+1] = data[data_offset+1];
				}
				catch(...)
				{
					ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
					break;
					/*
					buffer[buffer_offset]   = 0x00;
					buffer[buffer_offset+1]	= 0x00;
					*/
				}
				buffer_offset += 2;
				data_offset   += 2;
			  }
			  else
			  {
				try
				{
				  buffer[buffer_offset]   = data[data_offset+0]; // -AV
				}
				catch(...)
				{
					ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
					break;
				}
				buffer_offset++;
                data_offset++;
              }
            }
          }
          else
          {
            ret_code = 1;
            break;
          }
          if ( local_data8 != NULL )
          {
            try
            {
              free( local_data8 );
            }
            catch(...)
            {
              /* TODO 1 -oEnrico -cError : Gestire l'errore critico */
            }
            local_data8 = NULL;
          }
        }
      }
    }
    else
    {
      ret_code = 1;
    }
  }
  catch(...)
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == 0 )
  {
    buffer_len = buffer_offset;
    out_16 = (unsigned __int16*)malloc(sizeof(unsigned __int16));
    SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] );
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum CRC16 su 5 byte singoli
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCPL_343_5_S( void * output, void * input )
{
  int                 ret_code      = 0   ;
  unsigned __int8   * data          = NULL; // Ptr ad un dato
  char                buffer[10]    ;
  int                 buffer_len    ;
  int                 buffer_offset ;
  unsigned __int16  * out_16        = (unsigned __int16*)malloc(sizeof(unsigned __int16));
  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input;

  try
  {
    if ( input_param[0].value != NULL
      && input_param[1].value != NULL
      && input_param[2].value != NULL
      && input_param[3].value != NULL
      && input_param[4].value != NULL )
    {
      buffer_len    = 0;
      buffer_offset = 0;

      for ( int i = 0; i < 5; i++ )
      {
        data = (unsigned __int8 *)input_param[i].value;
        if ( input_param[i].ASCII == true )
        {
          buffer[buffer_offset]   = data[0];
          buffer[buffer_offset+1] = data[1];
          buffer_offset += 2;
        }
        else
        {
          buffer[buffer_offset]   = data[0];
          buffer_offset++;
        }
      }
    }
    else
    {
      ret_code = 1;
    }
  }
  catch(...)
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == 0 )
  {
    buffer_len = buffer_offset;
    SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] );
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
    ret_code            = 1;
  }

  return ret_code;
}

/**
*  Calcola il Block Character Check
*
*  @param[in]   pData  buffer dati di cui calcolare checksum
*  @param[in]   Count  numero di caratteri
*
*  @return      valore del checksum calcolato
*/
unsigned __int8 _BCC_Checksum ( unsigned __int8 * pData, size_t Count )
{
	unsigned __int8 bcc = 0;

	while (Count-- > 0)
	bcc ^= *pData++;

	return bcc;
}

//==============================================================================
/// Funzione di generazione checksum BCC su sequenza generalizzata di byte
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [01.08.2012]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SPVCPL_BCC_X_S( void * output, void * input )
{
	int                 ret_code      	= 0   ;
	unsigned __int8 *	data          	= NULL; // Ptr ad un dato
	unsigned __int8     buffer[255]      	  ;
	unsigned int        buffer_len    	= 0   ;
	unsigned int        buffer_offset 	= 0   ;
	unsigned __int8		length        	= 0   ; // Lunghezza della sequenza di byte (1 - 16)
	XML_INT_PARAM *		output_param  	= (XML_INT_PARAM*) output;
	XML_INT_PARAM *		input_param   	= (XML_INT_PARAM*) input;
	unsigned __int8 *	out_8			= NULL	;
	unsigned __int8		bcc				= 0		;

	try
	{
		if ( input_param[0].value != NULL )
		{
			data    = (unsigned __int8*) input_param[0].value;
			length  = *((unsigned __int8*)data);
			if ( length >= 1 && length <= 16 )
			{
				buffer_len    = (int) length;
				buffer_offset = 0;
				// --- Ciclo di controllo input ---
				for ( int i = 1; i <= buffer_len; i++ )
				{
					if ( input_param[i].value != NULL )
					{
						data = (unsigned __int8 *)input_param[i].value;
						buffer[buffer_offset]   = data[0];
						buffer_offset++;
					}
					else
					{
						ret_code = 1;
						break;
					}
				}
			}
		}
		else
		{
			ret_code = 1;
		}
	}
	catch(...)
	{
		ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	if ( ret_code == 0 )
	{
		buffer_len = buffer_offset;

		out_8 = (unsigned __int8*)malloc(sizeof(unsigned __int8));

		bcc = _BCC_Checksum( buffer, buffer_len );
		*out_8 = bcc;

		output_param->id    = 0;
		output_param->type  = t_u_hex_8;
		output_param->value = (void*) out_8;
	}
	else
	{
		output_param->id    = 0;
		output_param->type  = t_u_hex_8;
		output_param->value = NULL;
	}

	return ret_code;
}

//==============================================================================
/// Funzione di generazione checksum BCC su sequenza generalizzata di array di byte
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [01.08.2012]
/// \author Enrico Alborali
/// \version 1.02
//------------------------------------------------------------------------------
int SPVCPL_BCC_X_A( void * output, void * input )
{
	int                 ret_code      	= 0   ;
	unsigned __int8 *	data          	= NULL; // Ptr ad un dato
	unsigned __int8 *	data_length_p	= NULL; // Ptr alla lunghezza
	unsigned __int8		data_length		= 0	; // Lunghezza (in byte) di un dato (1..255)
	unsigned __int8     buffer[255]      	  ;
	unsigned int        buffer_len    	= 0   ;
	unsigned int        buffer_offset 	= 0   ;
	unsigned __int8		length        	= 0   ; // Lunghezza della sequenza di byte (1..16)
	XML_INT_PARAM *		output_param  	= (XML_INT_PARAM*) output;
	XML_INT_PARAM *		input_param   	= (XML_INT_PARAM*) input;
	unsigned __int8 *	out_8			= NULL	;
	unsigned __int8		bcc				= 0		;

	try
	{
		if ( input_param[0].value != NULL )
		{
			data    = (unsigned __int8*) input_param[0].value;
			length  = *((unsigned __int8*)data);
			if ( length >= 1 && length <= 16 )
			{
				buffer_len    = (int) length;
				buffer_offset = 0;
				// --- Ciclo di controllo input ---
				for ( int i = 1; i <= length; i++ )
				{
					if ( input_param[i].value != NULL && input_param[length+i].value != NULL )
					{
						data = (unsigned __int8 *)input_param[i].value;
						data_length_p = (unsigned __int8 *)input_param[length+i].value;
						data_length = *((unsigned __int8*)data_length_p); // Prendo il contenuto (lunghezza dato)
						for (int b = 0; b < data_length; b++) // Ciclo sul numero di byte del singolo dato
						{
							buffer[buffer_offset]   = data[b];	// b-esimo byte
							buffer_offset++;
						}
					}
					else
					{
						ret_code = 1;
						break;
					}
				}
			}
		}
		else
		{
			ret_code = 1;
		}
	}
	catch(...)
	{
		ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	if ( ret_code == 0 )
	{
		buffer_len = buffer_offset;

		out_8 = (unsigned __int8*)malloc(sizeof(unsigned __int8));

		bcc = _BCC_Checksum( buffer, buffer_len );
		*out_8 = bcc;

		output_param->id    = 0;
		output_param->type  = t_u_hex_8;
		output_param->value = (void*) out_8;
	}
	else
	{
		output_param->id    = 0;
		output_param->type  = t_u_hex_8;
		output_param->value = NULL;
	}

	return ret_code;
}

//==============================================================================
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [08.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCPL_DIV_2( void * output, void * input )
{
  int                 ret_code      = SPV_COMPROTLIB_NO_ERROR ;
  unsigned __int8     value         = 0  ;
  unsigned __int8   * in         = NULL  ;
  unsigned __int8   * out        = NULL  ;
  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

  if ( input_param[0].value != NULL )
  {
    in    = (unsigned __int8*) XMLASCII2Type( (char*)input_param[0].value, t_u_hex_8 );
    if ( in != NULL )
    {
      value    = *in;
    }
    else
    {
      in = (unsigned __int8*) malloc( sizeof(unsigned __int8) );
      value = 0;
    }
	value    = (unsigned __int8) (value/2);

	if (value==120) {
    	int err = 1;
	}

    *in      = value;
    out      = in;
    output_param->id    = 0;
    output_param->type  = t_u_hex_8;
    output_param->value = (void*) out;
  }
  else
  {
    out  = (unsigned __int8*) malloc(sizeof(char));
    *out = 0;
    output_param->id    = 0;
    output_param->type  = t_u_hex_8;
    output_param->value = out;
  }

  return ret_code;
}

//==============================================================================
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [08.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVCPL_DIV_2_INT( void * output, void * input )
{
  int                 ret_code      = SPV_COMPROTLIB_NO_ERROR ;
  unsigned __int8     value         = 0  ;
  unsigned __int8   * in         = NULL  ;
  unsigned __int8   * out        = NULL  ;
  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

  if ( input_param[0].value != NULL )
  {
    in    = (unsigned __int8*) XMLASCII2Type( (char*)input_param[0].value, t_u_hex_8 );
    if ( in != NULL )
    {
      value = *in;
    }
    else
    {
      in = (unsigned __int8*) malloc( sizeof(unsigned __int8) );
      value = 0;
    }
    value    = (unsigned __int8) (value/2);
    *in      = value;
    out      = in;
    output_param->id    = 0;
    output_param->type  = t_u_int_8;
    output_param->value = (void*) out;
  }
  else
  {
    out  = (unsigned __int8*) malloc(sizeof(char));
    *out = 0;
    output_param->id    = 0;
    output_param->type  = t_u_int_8;
    output_param->value = out;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per fornire una data/ora formattata stringa
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.10.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_3153_DateTime( void * output, void * input )
{
	int               ret_code      = SPV_COMPROTLIB_NO_ERROR ;
	time_t            date_time                               ;
	struct tm       * dt_struct                               ;
	char            * dt_string                               ;
	int               month_value   = 0                       ;
	char              month[3]                                ;
	char            * out           = NULL                    ;
	XML_INT_PARAM   * output_param  = (XML_INT_PARAM*) output ;
	XML_INT_PARAM   * input_param   = (XML_INT_PARAM*) input  ;
	unsigned __int8   type          = 0                       ;

	if ( output_param != NULL && input_param != NULL )
	{
		if ( input_param[0].value != NULL )
		{
			type = *((unsigned __int8*)input_param[0].value); // Recupero il tipo che voglio
			switch ( type )
			{
				// --- Formato Standard ---
				case 0x30 :
					out         = (char*) malloc( sizeof(char) * 29 );
					date_time   = time( NULL );
					dt_struct   = localtime( &date_time );
					dt_string   = asctime( dt_struct );
					month_value = dt_struct->tm_mon + 1;
					if ( month_value < 10 )
					{
						sprintf( month, "0%i", month_value );
					}
					else
					{
						sprintf( month, "%i", month_value );
					}
					// --- Giorno ---
					out[0]    = '0';
					out[1]    = dt_string[8];
					out[2]    = '0';
					out[3]    = dt_string[9];
					// --- Mese ---
					out[4]    = '0';
					out[5]    = month[0];
					out[6]    = '0';
					out[7]    = month[1];
					// --- Anno ---
					out[8]    = '0';
					out[9]    = dt_string[20];
					out[10]   = '0';
					out[11]   = dt_string[21];
					out[12]   = '0';
					out[13]   = dt_string[22];
					out[14]   = '0';
					out[15]   = dt_string[23];
					// --- Ore ---
					out[16]   = '0';
					out[17]   = dt_string[11];
					out[18]   = '0';
					out[19]   = dt_string[12];
					// --- Minuti ---
					out[20]   = '0';
					out[21]   = dt_string[14];
					out[22]   = '0';
					out[23]   = dt_string[15];
					// --- Secondi ---
					out[24]   = '0';
					out[25]   = dt_string[17];
					out[26]   = '0';
					out[27]   = dt_string[18];
					// --- NT ---
					out[28]   = '\0';
				break;
				// --- Formato Dimezzato (DT02000 e altre) ---
				case 0x31 :
					out         = (char*) malloc( sizeof(char) * 15 );
					date_time   = time( NULL );
					dt_struct   = localtime( &date_time );
					dt_string   = asctime( dt_struct );
					month_value = dt_struct->tm_mon + 1;
					if ( month_value < 10 )
					{
						sprintf( month, "0%i", month_value );
					}
					else
					{
						sprintf( month, "%i", month_value );
					}
					// --- Anno ---
					out[0]    = dt_string[20];
					out[1]   = dt_string[21];
					out[2]   = dt_string[22];
					out[3]   = dt_string[23];
					// --- Mese ---
					out[4]    = month[0];
					out[5]    = month[1];
					// --- Giorno ---
					out[6]    = dt_string[8];
					out[7]    = dt_string[9];
					// --- Ore ---
					out[8]   = dt_string[11];
					out[9]   = dt_string[12];
					// --- Minuti ---
					out[10]   = dt_string[14];
					out[11]   = dt_string[15];
					// --- Secondi ---
					out[12]   = dt_string[17];
					out[13]   = dt_string[18];
					// --- NT ---
					out[14]   = '\0';
				break;
				// --- TEST Formato Dimezzato ---
				case 0x35 :
					out         = (char*) malloc( sizeof(char) * 15 );
					// --- Anno 2012 ---
					out[0] = 0x32;
					out[1] = 0x30;
					out[2] = 0x31;
					out[3] = 0x32;
					// --- Mese 07 ---
					out[4] = 0x30;
					out[5] = 0x37;
					// --- Giorno 25 ---
					out[6] = 0x32;
					out[7] = 0x35;
					// --- Ore 12---
					out[8] = 0x31;
					out[9] = 0x32;
					// --- Minuti 34 ---
					out[10] = 0x33;
					out[11] = 0x34;
					// --- Secondi 23 ---
					out[12] = 0x32;
					out[13] = 0x33;
					// --- NT ---
					out[14] = '\0';
				break;
				default:
					ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
			}
		}
		else
		{
			ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
		}
	}
	else
	{
		ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
	{
		output_param->id    = 0;
		output_param->type  = t_string;
		output_param->value = (void*) out;
		output_param->ASCII = true;
	}
	else
	{
		output_param->id    = 0;
		output_param->type  = t_string;
		output_param->value = NULL;
		output_param->ASCII = true;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per recuparare la lunghezza di un campo XML
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_GetValueLen( void * output, void * input )
{
  int                 ret_code      = SPV_COMPROTLIB_NO_ERROR ;
  unsigned __int8   * data          = NULL  ; // Ptr ad un dato
  char                buffer[4080]          ;
  int                 buffer_len    = 0     ;
  int                 buffer_offset = 0     ;
  int                 data_offset   = 0     ;
  unsigned __int16  * out_16        = NULL  ;
  unsigned __int16    length        = 0     ; // Lunghezza della sequenza di byte (1 - 16)
  unsigned __int8   * array_len     = NULL  ; // Lunghezza dell'array (0-255)
  int                 array_dim     = 0     ;
  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

  if ( input_param[0].value != NULL )
  {
    data    = (unsigned __int8*) input_param[0].value;
    length  = *((unsigned __int16*)data);
    if ( length >= 1 && length <= 16 )
    {
      buffer_len    = (int) length;
      buffer_offset = 0;
      // --- Ciclo di controllo input ---
      for ( int i = 1; i <= buffer_len; i++ )
      {
        if ( input_param[i].value         != NULL
          && input_param[length+i].value  != NULL )
        {
          data = (unsigned __int8 *)input_param[length+i].value;
          if ( input_param[length+i].ASCII == true )
          {
			array_len = (unsigned __int8 *)XMLASCII2Type( (char*)data, t_u_hex_8 );
          }
          else
          {
            array_len = data;
          }
          array_dim = (int) *array_len;
          data = (unsigned __int8 *)input_param[i].value;
          data_offset = 0;
          for ( int e = 0; e < array_dim; e++ )
          {
            if ( input_param[i].ASCII == true )
            {
              buffer[buffer_offset]   = data[data_offset+0];
              buffer[buffer_offset+1] = data[data_offset+1];
              buffer_offset += 2;
              data_offset   += 2;
            }
            else
            {
              buffer[buffer_offset]   = data[data_offset+0];
              buffer_offset++;
              data_offset++;
            }
          }
        }
        else
        {
          ret_code = 1;
          break;
        }
      }
    }
  }
  else
  {
    ret_code = 1;
  }

  if ( ret_code == 0 )
  {
    buffer_len = buffer_offset;
    out_16 = (unsigned __int16*)malloc(sizeof(unsigned __int16));
    SPVCheckSumCRC16( buffer, buffer_len, (char*)&out_16[0] );
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = (void*) out_16;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_16;
    output_param->value = NULL;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per generare un blocco dati per frame
///
/// Formato del blocco generato da questa funzione:
/// +----------+----------------------+----------+
/// |    LEN   |         DATA         | ETB/ETX  |
/// +----------+----------------------+----------+
/// |  2 BYTE  |        N BYTE        |  1 BYTE  |
/// +----------+----------------------+----------+
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_535_DataBlock( void * output, void * input )
{
  // --- Definizione dei tipi di blocco dati ---
  #define DATA_BLOCK_TYPE_UNKNOWN 0
  #define DATA_BLOCK_TYPE_BIN     1
  #define DATA_BLOCK_TYPE_ASCII   2

  int                       ret_code      = SPV_COMPROTLIB_NO_ERROR ; // Codice di ritorno
  XML_INT_PARAM           * output_param  = (XML_INT_PARAM*) output ; // Ptr a struttura output
  XML_INT_PARAM           * input_param   = (XML_INT_PARAM*) input  ; // Ptr a struttura input
  unsigned __int8         * ptr_type      = NULL  ; // Ptr al parametro tipo blocco
  unsigned __int8         * ptr_buffer    = NULL  ; // Ptr al parametro buffer
  unsigned __int16        * ptr_bufferlen = NULL  ; // Ptr al parametro lunghezza buffer
  unsigned __int16        * ptr_maxblock  = NULL  ; // Ptr al parametro massima lunghezza blocco
  unsigned __int16        * ptr_numblock  = NULL  ; // Ptr al parametro numero del blocco
  char                      type          = DATA_BLOCK_TYPE_UNKNOWN ; // Tipo blocco
  char                    * buffer        = NULL  ; // Buffer dati
  int                       bufferlen     = 0     ; // Lunghezza buffer
  int                       maxblock      = 0     ; // Massima lunghezza blocco
  int                       numblock      = 0     ; // Numero del blocco
  SPV_COMPROT_BIN_BUFFER  * binbuffer     = NULL  ; // Struttura buffer binario
  unsigned __int8         * block         = NULL  ; // Ptr al blocco risultante
  int                       blocklen      = 0     ;
  char                    * ASCIIlen      = NULL  ;

  if ( input_param[0].value != NULL   // Controllo il primo parametro (tipo blocco)
    && input_param[1].value != NULL   // Controllo il primo parametro (buffer dati)
    && input_param[2].value != NULL   // Controllo il secondo parametro (lunghezza buffer)
    && input_param[3].value != NULL   // Controllo il terzo paremtro (max lungh. blocco)
    && input_param[4].value != NULL ) // Controllo il quarto parametro (num. blocco)
  {
    // --- Recupero i ptr ai valori dei parametri ---
    ptr_type      = ( unsigned __int8*  ) input_param[0].value;
    ptr_buffer    = ( unsigned __int8*  ) input_param[1].value;
    ptr_bufferlen = ( unsigned __int16* ) input_param[2].value;
    ptr_maxblock  = ( unsigned __int16* ) input_param[3].value;
    ptr_numblock  = ( unsigned __int16* ) input_param[4].value;
    // --- Recupero i valori dei parametri ---
    type      = (char) *( ( unsigned __int8* ) ptr_type );
    buffer    = (char*) ptr_buffer;
    bufferlen = (int) *( ( unsigned __int16* ) ptr_bufferlen );
    maxblock  = (int) *( ( unsigned __int16* ) ptr_maxblock );
    numblock  = (int) *( ( unsigned __int16* ) ptr_numblock );
    // --- Generazione del blocco dati ---
    binbuffer = (SPV_COMPROT_BIN_BUFFER*) SPVBuildDataBlock( buffer, bufferlen, maxblock, numblock );

    if ( SPVValidBinBuffer( binbuffer ) )
    {
      if ( binbuffer->buffer != NULL && binbuffer->len > 0 )
      {
        switch ( type )
        {
          // --- Caso blocco dati binario (lunghezza normale) ---
          case DATA_BLOCK_TYPE_BIN:
            block = (char*) malloc( sizeof(char) * (binbuffer->len+3) ); // - MALLOC
            blocklen = binbuffer->len;
            ASCIIlen = XMLType2ASCII( (void*) &blocklen, t_u_hex_8 );
            memcpy( &block[0], ASCIIlen, sizeof(char) * 2 ); // Copio la lunghezza ASCII
            memcpy( &block[2], binbuffer->buffer, sizeof(char) * (binbuffer->len+1) ); // Copio la lunghezza ASCII
          break;
          // --- Caso blocco dati ASCII (lunghezza dimezzata) ---
          case DATA_BLOCK_TYPE_ASCII:
            block = (char*) malloc( sizeof(char) * (binbuffer->len+3) ); // - MALLOC
            blocklen = (int) ( binbuffer->len / 2 ); // Divido la lunghezza per due
            ASCIIlen = XMLType2ASCII( (void*) &blocklen, t_u_hex_8 );
            memcpy( &block[0], ASCIIlen, sizeof(char) * 2 ); // Copio la lunghezza ASCII
            memcpy( &block[2], binbuffer->buffer, sizeof(char) * (binbuffer->len+1) ); // Copio la lunghezza ASCII
          break;
          // --- Caso blocco dati sconosciuto (no lunghezza) ---
          default:
            block = NULL;
        }
      }
      else
      {
        ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
    }

    SPVDeleteBinBuffer( binbuffer );
  }
  else
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = (void*) block;
    output_param->ASCII = false;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = NULL;
    output_param->ASCII = false;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per generare un campo di fine blocco dati
///
/// Formato del blocco generato da questa funzione:
/// +----------+
/// | ETB/ETX  |
/// +----------+
/// |  1 BYTE  |
/// +----------+
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVCPL_5353_End( void * output, void * input )
{
  // --- Definizione dei tipi di blocco dati ---
  #define DATA_BLOCK_TYPE_UNKNOWN 0
  #define DATA_BLOCK_TYPE_BIN     1
  #define DATA_BLOCK_TYPE_ASCII   2

  int                       ret_code      = SPV_COMPROTLIB_NO_ERROR ; // Codice di ritorno
  XML_INT_PARAM           * output_param  = (XML_INT_PARAM*) output ; // Ptr a struttura output
  XML_INT_PARAM           * input_param   = (XML_INT_PARAM*) input  ; // Ptr a struttura input
  unsigned __int8         * ptr_type      = NULL  ; // Ptr al parametro tipo blocco
  unsigned __int8         * ptr_buffer    = NULL  ; // Ptr al parametro buffer
  unsigned __int16        * ptr_bufferlen = NULL  ; // Ptr al parametro lunghezza buffer
  unsigned __int16        * ptr_maxblock  = NULL  ; // Ptr al parametro massima lunghezza blocco
  unsigned __int8         * ptr_numblock  = NULL  ; // Ptr al parametro numero del blocco
  void                    * ptr_void      = NULL  ;
  char                      type          = DATA_BLOCK_TYPE_UNKNOWN ; // Tipo blocco
  char                    * buffer        = NULL  ; // Buffer dati
  int                       bufferlen     = 0     ; // Lunghezza buffer
  int                       maxblock      = 0     ; // Massima lunghezza blocco
  int                       numblock      = 0     ; // Numero del blocco
  SPV_COMPROT_BIN_BUFFER  * binbuffer     = NULL  ; // Struttura buffer binario
  unsigned __int8         * block         = NULL  ; // Ptr al blocco risultante

  if ( input_param[0].value != NULL   // Controllo il primo parametro (tipo blocco)
    && input_param[1].value != NULL   // Controllo il primo parametro (buffer dati)
    && input_param[2].value != NULL   // Controllo il secondo parametro (lunghezza buffer)
    && input_param[3].value != NULL   // Controllo il terzo paremtro (max lungh. blocco)
    && input_param[4].value != NULL ) // Controllo il quarto parametro (num. blocco)
  {
    // --- Recupero i ptr ai valori dei parametri ---
    ptr_type      = ( unsigned __int8*  ) input_param[0].value;
    ptr_buffer    = ( unsigned __int8*  ) input_param[1].value;
    ptr_bufferlen = ( unsigned __int16* ) input_param[2].value;
    ptr_maxblock  = ( unsigned __int16* ) input_param[3].value;
    ptr_numblock  = ( unsigned __int8*  ) input_param[4].value;
    // --- Recupero i valori dei parametri ---
    type      = (char) *( ( unsigned __int8* ) ptr_type );
    buffer    = (char*) ptr_buffer;
    bufferlen = (int) *( ( unsigned __int16* ) ptr_bufferlen );
	maxblock  = (int) *( ( unsigned __int16* ) ptr_maxblock );
    if ( input_param[4].ASCII )
    {
	  ptr_void  = XMLASCII2Type( (char*)ptr_numblock, t_u_hex_8 );
      numblock  = (int) *( ( unsigned __int8*  ) ptr_void );
      free( ptr_void );
    }
    else
    {
      numblock  = (int) *( ( unsigned __int8*  ) ptr_numblock );
    }
    // --- Generazione del blocco dati ---
    binbuffer = (SPV_COMPROT_BIN_BUFFER*) SPVBuildDataBlock( buffer, bufferlen, maxblock, numblock );

    if ( SPVValidBinBuffer( binbuffer ) )
    {
      if ( binbuffer->buffer != NULL && binbuffer->len > 0 )
      {
        switch ( type )
        {
          // --- Caso blocco dati binario (lunghezza normale) ---
          case DATA_BLOCK_TYPE_BIN:
            block = (char*) malloc( sizeof(char) ); // - MALLOC
            memcpy( &block[0], &binbuffer->buffer[binbuffer->len], sizeof(char) ); // Copio la lunghezza ASCII
          break;
          // --- Caso blocco dati ASCII (lunghezza dimezzata) ---
          case DATA_BLOCK_TYPE_ASCII:
            block = (char*) malloc( sizeof(char) ); // - MALLOC
            memcpy( &block[0], &binbuffer->buffer[binbuffer->len], sizeof(char) ); // Copio la lunghezza ASCII
          break;
          // --- Caso blocco dati sconosciuto (no lunghezza) ---
          default:
            block = NULL;
        }
      }
      else
      {
        ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
    }

    SPVDeleteBinBuffer( binbuffer );
  }
  else
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_8;
    output_param->value = (void*) block;
    output_param->ASCII = false;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_u_hex_8;
    output_param->value = NULL;
    output_param->ASCII = false;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per generare un campo buffer di un blocco dati
///
/// Formato del blocco generato da questa funzione:
/// +----------------------+
/// |         DATA         |
/// +----------------------+
/// |        N BYTE        |
/// +----------------------+
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_5355_Buffer( void * output, void * input )
{
  // --- Definizione dei tipi di blocco dati ---
  #define DATA_BLOCK_TYPE_UNKNOWN 0
  #define DATA_BLOCK_TYPE_BIN     1
  #define DATA_BLOCK_TYPE_ASCII   2

  int                       ret_code      = SPV_COMPROTLIB_NO_ERROR ; // Codice di ritorno
  XML_INT_PARAM           * output_param  = (XML_INT_PARAM*) output ; // Ptr a struttura output
  XML_INT_PARAM           * input_param   = (XML_INT_PARAM*) input  ; // Ptr a struttura input
  unsigned __int8         * ptr_type      = NULL  ; // Ptr al parametro tipo blocco
  unsigned __int8         * ptr_buffer    = NULL  ; // Ptr al parametro buffer
  unsigned __int16        * ptr_bufferlen = NULL  ; // Ptr al parametro lunghezza buffer
  unsigned __int16        * ptr_maxblock  = NULL  ; // Ptr al parametro massima lunghezza blocco
  unsigned __int16        * ptr_numblock  = NULL  ; // Ptr al parametro numero del blocco
  char                      type          = DATA_BLOCK_TYPE_UNKNOWN ; // Tipo blocco
  char                    * buffer        = NULL  ; // Buffer dati
  int                       bufferlen     = 0     ; // Lunghezza buffer
  int                       maxblock      = 0     ; // Massima lunghezza blocco
  int                       numblock      = 0     ; // Numero del blocco
  SPV_COMPROT_BIN_BUFFER  * binbuffer     = NULL  ; // Struttura buffer binario
  unsigned __int8         * block         = NULL  ; // Ptr al blocco risultante

  if ( input_param[0].value != NULL   // Controllo il primo parametro (tipo blocco)
    && input_param[1].value != NULL   // Controllo il primo parametro (buffer dati)
    && input_param[2].value != NULL   // Controllo il secondo parametro (lunghezza buffer)
    && input_param[3].value != NULL   // Controllo il terzo paremtro (max lungh. blocco)
    && input_param[4].value != NULL ) // Controllo il quarto parametro (num. blocco)
  {
    // --- Recupero i ptr ai valori dei parametri ---
    ptr_type      = ( unsigned __int8*  ) input_param[0].value;
    ptr_buffer    = ( unsigned __int8*  ) input_param[1].value;
    ptr_bufferlen = ( unsigned __int16* ) input_param[2].value;
    ptr_maxblock  = ( unsigned __int16* ) input_param[3].value;
    // --- Recupero i valori dei parametri ---
    type      = (char) *( ( unsigned __int8* ) ptr_type );
    buffer    = (char*) ptr_buffer;
    bufferlen = (int) *( ( unsigned __int16* ) ptr_bufferlen );
    maxblock  = (int) *( ( unsigned __int16* ) ptr_maxblock );
    numblock  = SPVGetIntParam( (void*) input_param, 4 );
    // --- Generazione del blocco dati ---
    binbuffer = (SPV_COMPROT_BIN_BUFFER*) SPVBuildDataBlock( buffer, bufferlen, maxblock, numblock );

    if ( SPVValidBinBuffer( binbuffer ) )
    {
      if ( binbuffer->buffer != NULL && binbuffer->len > 0 )
      {
        switch ( type )
        {
          // --- Caso blocco dati binario (lunghezza normale) ---
          case DATA_BLOCK_TYPE_BIN:
            block = (char*) malloc( sizeof(char) * ( binbuffer->len + 1 ) ); // - MALLOC
            memcpy( &block[0], binbuffer->buffer, sizeof(char) * ( binbuffer->len ) ); // Copio il buffer dei dati
            block[binbuffer->len] = '\0';
          break;
          // --- Caso blocco dati ASCII (lunghezza dimezzata) ---
          case DATA_BLOCK_TYPE_ASCII:
            block = (char*) malloc( sizeof(char) * ( binbuffer->len + 1 ) ); // - MALLOC
            memcpy( &block[0], binbuffer->buffer, sizeof(char) * ( binbuffer->len ) ); // Copio il buffer dei dati
            block[binbuffer->len] = '\0';
          break;
          // --- Caso blocco dati sconosciuto (no lunghezza) ---
          default:
            block = NULL;
        }
      }
      else
      {
        ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
    }
    SPVDeleteBinBuffer( binbuffer );
  }
  else
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = (void*) block;
    output_param->ASCII = false;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = NULL;
    output_param->ASCII = false;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per generare un campo lunghezza di un blocco dati
///
/// Formato del blocco generato da questa funzione:
/// +----------+
/// |    LEN   |
/// +----------+
/// |  2 BYTE  |
/// +----------+
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVCPL_5359_Len( void * output, void * input )
{
  // --- Definizione dei tipi di blocco dati ---
  #define DATA_BLOCK_TYPE_UNKNOWN 0
  #define DATA_BLOCK_TYPE_BIN     1
  #define DATA_BLOCK_TYPE_ASCII   2

  int                       ret_code      = SPV_COMPROTLIB_NO_ERROR ; // Codice di ritorno
  XML_INT_PARAM           * output_param  = (XML_INT_PARAM*) output ; // Ptr a struttura output
  XML_INT_PARAM           * input_param   = (XML_INT_PARAM*) input  ; // Ptr a struttura input
  unsigned __int8         * ptr_type      = NULL  ; // Ptr al parametro tipo blocco
  unsigned __int8         * ptr_buffer    = NULL  ; // Ptr al parametro buffer
  unsigned __int16        * ptr_bufferlen = NULL  ; // Ptr al parametro lunghezza buffer
  unsigned __int16        * ptr_maxblock  = NULL  ; // Ptr al parametro massima lunghezza blocco
  unsigned __int16        * ptr_numblock  = NULL  ; // Ptr al parametro numero del blocco
  char                      type          = DATA_BLOCK_TYPE_UNKNOWN ; // Tipo blocco
  char                    * buffer        = NULL  ; // Buffer dati
  int                       bufferlen     = 0     ; // Lunghezza buffer
  int                       maxblock      = 0     ; // Massima lunghezza blocco
  int                       numblock      = 0     ; // Numero del blocco
  SPV_COMPROT_BIN_BUFFER  * binbuffer     = NULL  ; // Struttura buffer binario
  unsigned __int8         * block         = NULL  ; // Ptr al blocco risultante
  int                       blocklen      = 0     ;
  char                    * ASCIIlen      = NULL  ;

  if ( input_param[0].value != NULL   // Controllo il primo parametro (tipo blocco)
    && input_param[1].value != NULL   // Controllo il primo parametro (buffer dati)
    && input_param[2].value != NULL   // Controllo il secondo parametro (lunghezza buffer)
    && input_param[3].value != NULL   // Controllo il terzo paremtro (max lungh. blocco)
    && input_param[4].value != NULL ) // Controllo il quarto parametro (num. blocco)
  {
    // --- Recupero i ptr ai valori dei parametri ---
    ptr_type      = ( unsigned __int8*  ) input_param[0].value;
    ptr_buffer    = ( unsigned __int8*  ) input_param[1].value;
    ptr_bufferlen = ( unsigned __int16* ) input_param[2].value;
    ptr_maxblock  = ( unsigned __int16* ) input_param[3].value;
    // --- Recupero i valori dei parametri ---
    type      = (char) *( ( unsigned __int8* ) ptr_type );
    buffer    = (char*) ptr_buffer;
    bufferlen = (int) *( ( unsigned __int16* ) ptr_bufferlen );
    maxblock  = (int) *( ( unsigned __int16* ) ptr_maxblock );
    numblock = SPVGetIntParam( (void*) input_param, 4 );

    // --- Generazione del blocco dati ---
    binbuffer = (SPV_COMPROT_BIN_BUFFER*) SPVBuildDataBlock( buffer, bufferlen, maxblock, numblock );

    if ( SPVValidBinBuffer( binbuffer ) )
    {
      if ( binbuffer->buffer != NULL && binbuffer->len > 0 )
      {
        switch ( type )
        {
          // --- Caso blocco dati binario (lunghezza normale) ---
          case DATA_BLOCK_TYPE_BIN:
            blocklen = binbuffer->len;
            ASCIIlen = XMLType2ASCII( (void*) &blocklen, t_u_hex_8 ); // -MALLOC
            block = ASCIIlen;
          break;
          // --- Caso blocco dati ASCII (lunghezza dimezzata) ---
          case DATA_BLOCK_TYPE_ASCII:
            blocklen = (int) ( binbuffer->len / 2 ); // Divido la lunghezza per due
            ASCIIlen = XMLType2ASCII( (void*) &blocklen, t_u_hex_8 ); // -MALLOC
            block = ASCIIlen;
          break;
          // --- Caso blocco dati sconosciuto (no lunghezza) ---
          default:
            block = NULL;
        }
      }
      else
      {
        ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
    }
    SPVDeleteBinBuffer( binbuffer );
  }
  else
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = (void*) block;
    output_param->ASCII = true;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = NULL;
    output_param->ASCII = true;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere un buffer di configurazione di test
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [18.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_533_TestConfigData( void * output, void * input )
{
  int                       ret_code      = SPV_COMPROTLIB_NO_ERROR ; // Codice di ritorno
  XML_INT_PARAM           * output_param  = (XML_INT_PARAM*) output ; // Ptr a struttura output
  XML_INT_PARAM           * input_param   = (XML_INT_PARAM*) input  ; // Ptr a struttura input
  char                    * buffer        = NULL  ; // Buffer dati
  unsigned __int8         * ptr_code      = NULL  ; // Ptr al parametro codice
  unsigned __int8           code          = 0     ; // Valore parametro codice

  if ( input_param[0].value != NULL ) // Controllo il primo parametro (codice)
  {
    // --- Recupero i ptr ai valori dei parametri ---
    ptr_code  = ( unsigned __int8*  ) input_param[0].value;
    // --- Recupero i valori dei parametri ---
    code      =  *( ( unsigned __int8* ) ptr_code );
    // --- Genero il buffer dello stream ---
    buffer  = (char*) malloc( sizeof(char) * 1499 ); // -MALLOC
    memcpy( &buffer[0]    , "553F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F003F3F3F3F3F3F0121000101AAAA0000AAAA00000101FFFF0F0F0101AAAA", 128 );
    memcpy( &buffer[128]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[256]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[384]  , "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF", 128 );
    memcpy( &buffer[512]  , "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 128 );
    memcpy( &buffer[640]  , "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[768]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[896]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[1024] , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[1152] , "0000000000000000000000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F", 128 );
    memcpy( &buffer[1280] , "0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[1408] , "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000AA", 90 );
  }
  else
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = (void*) buffer;
    output_param->ASCII = true;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = NULL;
    output_param->ASCII = true;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere il buffer di uno stream di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [30.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_5322_GetDevStreamBuffer( void * output, void * input )
{
  int                       ret_code      = SPV_COMPROTLIB_NO_ERROR ; // Codice di ritorno
  XML_INT_PARAM           * output_param  = (XML_INT_PARAM*) output ; // Ptr a struttura output
  XML_INT_PARAM           * input_param   = (XML_INT_PARAM*) input  ; // Ptr a struttura input
  char                    * buffer        = NULL  ; // Buffer dati
  unsigned __int8         * ptr_code      = NULL  ; // Ptr al parametro codice
  unsigned __int8           code          = 0     ; // Valore parametro codice

  if ( input_param[0].value != NULL ) // Controllo il primo parametro (codice)
  {
    // --- Recupero i ptr ai valori dei parametri ---
    ptr_code  = ( unsigned __int8*  ) input_param[0].value;
    // --- Recupero i valori dei parametri ---
    code      =  *( ( unsigned __int8* ) ptr_code );
    // --- Genero il buffer dello stream ---
    buffer  = (char*) malloc( sizeof(char) * 1499 ); // -MALLOC
    memcpy( &buffer[0]    , "553F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F003F3F3F3F3F3F0121000101AAAA0000AAAA00000101FFFF0F0F0101AAAA", 128 );
    memcpy( &buffer[128]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[256]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[384]  , "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF", 128 );
    memcpy( &buffer[512]  , "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 128 );
    memcpy( &buffer[640]  , "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[768]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[896]  , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[1024] , "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[1152] , "0000000000000000000000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F", 128 );
    memcpy( &buffer[1280] , "0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000000000000000000000000000000000000000000000000000000000000000000000000", 128 );
    memcpy( &buffer[1408] , "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000AA", 90 );
  }
  else
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
  }

  if ( ret_code == SPV_COMPROTLIB_NO_ERROR )
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = (void*) buffer;
    output_param->ASCII = true;
  }
  else
  {
    output_param->id    = 0;
    output_param->type  = t_string;
    output_param->value = NULL;
    output_param->ASCII = true;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere la lunghezza di uno stream di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [30.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_5329_GetDevStreamLen( void * output, void * input )
{

}

//==============================================================================
/// Funzione per ottenere una lunghezza di un filed dal valore di un field ricevuto
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_5596_GetLenFromField( void * output, void * input )
{
  int                 ret_code      = SPV_COMPROTLIB_NO_ERROR ;
  unsigned __int16  * input_16      = NULL;
  unsigned __int16  * output_16     = NULL;

  XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
  XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

  try
  {
    // --- Alloco la memoria per il valore da restituire ---
    output_16   = (unsigned __int16*) malloc( sizeof(unsigned __int16) );
    *output_16  = 0;

    if ( input_param[0].value != NULL )
    {
      // --- Recupero il valore dal I parametro ---
      input_16 = (unsigned __int16*) input_param[0].value;
      if ( input_16 != NULL )
      {
        // --- Copio in uscita il valore passato ---
        *output_16 = *input_16;
      }
    }
    // --- Preparo la struttura da restituire ---
    output_param->id    = 0;
    output_param->type  = t_u_int_16;
    output_param->value = (void*)output_16;
  }
  catch(...)
  {
    ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere una lunghezza di un filed dal valore di un field ricevuto
/// in formato unisgned 8 bit esadecimale.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.01.2010]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_54468_GetLenUHex8FromField( void * output, void * input )
{
	int                 ret_code      = SPV_COMPROTLIB_NO_ERROR ;
	unsigned __int8  * input_8      = NULL;
	unsigned __int8  * output_8     = NULL;

	XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
	XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

	try
	{
		// --- Alloco la memoria per il valore da restituire ---
		output_8   = (unsigned __int8*) malloc( sizeof(unsigned __int8) );
		*output_8  = 0;

		if ( input_param[0].value != NULL )
		{
			// --- Recupero il valore dal I parametro ---
			input_8 = (unsigned __int8*) input_param[0].value;
			if ( input_8 != NULL )
			{
				// --- Copio in uscita il valore passato ---
				*output_8 = *input_8;
			}
		}
        // --- Preparo la struttura da restituire ---
        output_param->id    = 0;
        output_param->type  = t_u_hex_8;
		output_param->value = (void*)output_8;
	}
	catch(...)
	{
		ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere una lunghezza di un filed dal valore di un field ricevuto
/// in formato unisgned 8 bit intero.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.01.2010]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_54488_GetLenUInt8FromField( void * output, void * input )
{
    int                 ret_code      = SPV_COMPROTLIB_NO_ERROR ;
    unsigned __int8     value         = 0  ;
    unsigned __int8   * in         = NULL  ;
    unsigned __int8   * out        = NULL  ;
    XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
    XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

    if ( input_param[0].value != NULL )
	{
		in 	= (unsigned __int8*) input_param[0].value;
		//in    = (unsigned __int8*) XMLASCII2Type( (char*)input_param[0].value, t_u_hex_8 );
		if ( in != NULL )
		{
			value = *in;
		}
		else
		{
			in = (unsigned __int8*) malloc( sizeof(unsigned __int8) );
			value = 0;
		}
		out      			= XMLType2ASCII((void*)&value, t_u_int_8 );
		output_param->id    = 0;
    	output_param->type  = t_u_int_8;
		output_param->value = (void*) out;
		output_param->ASCII = false;
	}
	else
	{
		out  = (unsigned __int8*) malloc(sizeof(char));
		*out = 0;
		output_param->id    = 0;
		output_param->type  = t_u_int_8;
		output_param->value = out;
		output_param->ASCII = false;
    }

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere la lunghezza (t_u_int16) di un valore stringa (con NT)
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [27.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_5429_GetFieldStringLen( void * output, void * input )
{
	int                 ret_code      = SPV_COMPROTLIB_NO_ERROR;
	unsigned __int16  * output_16     = NULL;
	char              * input_string  = NULL;
	unsigned __int16    len           = 0;

	XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
	XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

	try
	{
		// --- Alloco la memoria per il valore da restituire ---
		output_16   = (unsigned __int16*) malloc( sizeof(unsigned __int16) );
		*output_16  = 0;

		if ( input_param[0].value != NULL )
		{
			// --- Recupero il valore dal I parametro ---
			input_string = (char*) input_param[0].value;
			if ( input_string != NULL )
			{
				len = strlen( input_string ) + 1;
				// --- Copio in uscita il valore passato ---
				*output_16 = len;
			}
		}
		// --- Preparo la struttura da restituire ---
		output_param->id    = 0;
		output_param->type  = t_u_int_16;
		output_param->value = (void*)output_16;
	}
	catch(...)
	{
		ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere la lunghezza (t_u_int16) di un valore stringa (senza NT)
/// In caso di errore restituisce un valore non nullo.
///
/// \date [02.02.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCPL_5430_GetFieldStringLen ( void * output, void * input )
{
	int                 ret_code      = SPV_COMPROTLIB_NO_ERROR;
	unsigned __int16  * output_16     = NULL;
	char              * input_string  = NULL;
	unsigned __int16    len           = 0;

	XML_INT_PARAM     * output_param  = (XML_INT_PARAM*) output ;
	XML_INT_PARAM     * input_param   = (XML_INT_PARAM*) input  ;

	try
	{
		// --- Alloco la memoria per il valore da restituire ---
		output_16   = (unsigned __int16*) malloc( sizeof(unsigned __int16) );
		*output_16  = 0;

		if ( input_param[0].value != NULL )
		{
			// --- Recupero il valore dal I parametro ---
			input_string = (char*) input_param[0].value;
			if ( input_string != NULL )
			{
				len = strlen( input_string ); // Lunghezza stringa senza NULL terminatore
				// --- Copio in uscita il valore passato ---
				*output_16 = len;
			}
		}
		// --- Preparo la struttura da restituire ---
		output_param->id    = 0;
		output_param->type  = t_u_int_16;
		output_param->value = (void*)output_16;
	}
	catch(...)
	{
		ret_code = SPV_COMPROTLIB_GENERAL_ERROR;
	}

	return ret_code;
}


//==============================================================================
// Telefin Supervisor Communication Protocol Library 1.0
//------------------------------------------------------------------------------
// Header Libreria per il protocollo di comunicazione (SPVComProtLib.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.23 (09.09.2004 -> 29.08.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVComProtLib.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [09.09.2004]:
// - Prima versiona della libreria.
// 0.02 [10.09.2004]:
// - Modificata la funzione SPVComProtLibInit.
// - Modificata la funzione SPVCPL_343_5.
// - Aggiunta la funzione SPVCPL_343_5.
// 0.03 [27.01.2005]:
// - Aggiunta la funzione SPVCPL_343_4.
// - Modificata la funzione SPVComProtLibInit.
// 0.04 [28.01.2005]:
// - Modificata la funzione SPVCPL_343_4.
// 0.05 [03.02.2005]:
// - Modificata la funzione SPVCPL_343_9.
// 0.06 [11.02.2005]:
// - Modificata la funzione SPVComProtLibInit.
// - Aggiunta la funzione SPVCPL_343_X.
// 0.07 [18.02.2005]:
// - Modificata la funzione SPVComProtLibInit.
// - Aggiunta la funzione SPVCPL_343_5_A.
// 0.08 [09.03.2005]:
// - Modificata la funzione SPVCPL_343_4.
// - Modificata la funzione SPVCPL_343_5.
// - Modificata la funzione SPVCPL_343_5_A.
// - Modificata la funzione SPVCPL_343_9.
// - Modificata la funzione SPVComProtLibInit.
// 0.09 [10.03.2005]:
// - Aggiunta la funzione SPVCPL_343_5_S.
// - Modificata la funzione SPVComProtLibInit.
// 0.10 [11.03.2005]:
// - Modificata la funzione SPVComProtLibInit.
// - Modificata la funzione SPVCPL_343_X.
// - Aggiunta la funzione SPVCPL_CRC_X_S.
// 0.11 [14.03.2005]:
// - Modificata la funzione SPVCPL_CRC_X_S.
// - Aggiunta la funzione SPVCPL_CRC_X_A.
// 0.12 [15.03.2005]:
// - Modificata la funzione SPVCPL_CRC_X_A.
// 0.13 [01.04.2005]:
// - Aggiunta la fuznione SPVCPL_DIV_2.
// - Aggiunta la fuznione SPVCPL_DIV_2_INT.
// - Modificata la funzione SPVComProtLibInit.
// 0.14 [02.05.2005]:
// - Modificata la funzione SPVCPL_3153_DateTime.
// 0.15 [06.05.2005]:
// - Aggiunta la funzione SPVBuildDataBlock.
// 0.16 [09.05.2005]:
// - Modificata la funzione SPVBuildDataBlock.
// - Aggiunta la funzione SPVCPL_535.
// - Modificata la funzione SPVComProtLibInit.
// 0.17 [10.05.2005]:
// - Modificata la funzione SPVCPL_535.
// - Modificata la funzione SPVComProtLibInit.
// 0.18 [11.05.2005]:
// - Modificata la funzione SPVComProtLibInit.
// - Aggiunta la funzione SPVCPL_5353_End.
// - Aggiunta la funzione SPVCPL_5355_Buffer.
// - Aggiunta la funzione SPVCPL_5359_Len.
// 0.19 [13.05.2005]:
// - Modificata la funzione SPVCPL_5353_End.
// - Modificata la funzione SPVCPL_5355_Buffer.
// - Modificata la funzione SPVCPL_5359_Len.
// - Aggiunta la funzione SPVValidParam.
// - Aggiunta la funzione SPVGetCharPtrASCIIParam.
// - Aggiunta la funzione SPVGetCharPtrParam.
// - Aggiunta la funzione SPVGetIntASCIIParam.
// - Aggiunta la funzione SPVGetIntParam.
// 0.20 [16.05.2005]:
// - Modificata la funzione SPVCPL_CRC_X_A.
// 0.21 [18.05.2005]:
// - Modificata la funzione SPVComProtLibInit.
// - Aggiunta la funzione SPVCPL_533_TestConfigData.
// 0.22 [07.06.2005]:
// - Modificata la funzione SPVComProtLibInit.
// - Modificata la funzione SPVGetIntParam.
// 0.23 [29.08.2005]:
// - Aggiunta la funzione SPVComProtLibClear.
// [06.10.2006]:
// - Aggiunto formato dimezzato (params=1) in SPVCPL_3153_DateTime().
//==============================================================================
#ifndef SPVComProtLibH
#define SPVComProtLibH

#define SPV_COMPROTLIB_FUNCTION_COUNT     50

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_COMPROTLIB_NO_ERROR           0
#define SPV_COMPROTLIB_GENERAL_ERROR      1
#define SPV_COMPROTLIB_NOT_IMPLEMENTED   -1

//==============================================================================
// Dichiarazione funzioni
//------------------------------------------------------------------------------

/// Funzione di caricamento della libreria
void    SPVComProtLibInit   ( void );
/// Funzione per chiudere la libreria
void    SPVComProtLibClear  ( void );
/// Funzione per generare blocchi di dati per frame
void  * SPVBuildDataBlock   ( char * buffer, int buflen, int maxlen, int num );
/// Funzione di CheckSum CRC16
char  * SPVCheckSumCRC16    ( char * buffer, long len, char * checksum );
/// Funzione per verificare che un parametro sia valido
bool    SPVValidParam       ( void * param_list, int num );
/// Funzione per ottenere parametri char ptr
char  * SPVGetCharPtrParam  ( void * param_list, int num );
/// Funzione per ottenere parametri int in formato ASCII
char  * SPVGetIntASCIIParam ( void * param_list, int num );
/// Funzione per ottenere parametri int
int     SPVGetIntParam      ( void * param_list, int num );
/// Funzione di generazione checksum CRC16 su 4 byte
int     SPVCPL_343_4        ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su 5 byte
int     SPVCPL_343_5        ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su 5 array di byte
int     SPVCPL_343_5_A      ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su 9 byte
int     SPVCPL_343_9        ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su sequenza di byte generalizzata
int     SPVCPL_343_X        ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su sequenza generalizzata di byte
int     SPVCPL_CRC_X_S      ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su sequenza generalizzata di array di byte
int     SPVCPL_CRC_X_A      ( void * output, void * input );
/// Funzione di generazione checksum CRC16 su 5 byte singoli
int     SPVCPL_343_5_S      ( void * output, void * input );
/// Funzione di generazione checksum BCC su sequenza generalizzata di byte
int     SPVCPL_BCC_X_S      ( void * output, void * input );
/// Funzione di generazione checksum BCC su sequenza generalizzata di array di byte
int     SPVCPL_BCC_X_A      ( void * output, void * input );
///
int     SPVCPL_DIV_2        ( void * output, void * input );
///
int     SPVCPL_DIV_2_INT    ( void * output, void * input );
/// Funzione per fornire una data/ora formattata stringa
int     SPVCPL_3153_DateTime( void * output, void * input );
/// Funzione per recuparare la lunghezza di un campo XML
int     SPVCPL_GetValueLen  ( void * output, void * input );
/// Funzione per generare un blocco dati per frame
int     SPVCPL_535_DataBlock( void * output, void * input );
/// Funzione per generare un campo di fine blocco dati
int     SPVCPL_5353_End     ( void * output, void * input );
/// Funzione per generare un campo buffer di un blocco dati
int     SPVCPL_5355_Buffer  ( void * output, void * input );
/// Funzione per generare un campo lunghezza di un blocco dati
int     SPVCPL_5359_Len     ( void * output, void * input );
/// Funzione per ottenere un buffer di configurazione di test
int     SPVCPL_533_TestConfigData ( void * output, void * input );
/// Funzione per ottenere il buffer di uno stream di una periferica
int     SPVCPL_5322_GetDevStreamBuffer( void * output, void * input );
/// Funzione per ottenere la lunghezza di uno stream di una periferica
int     SPVCPL_5329_GetDevStreamLen   ( void * output, void * input );
// Funzione per ottenere una lunghezza di un filed dal valore di un field ricevuto
int     SPVCPL_5596_GetLenFromField   ( void * output, void * input );
// Funzione per ottenere la lunghezza (t_u_int16) di un valore stringa (con NT)
int     SPVCPL_5429_GetFieldStringLen ( void * output, void * input );
// Funzione per ottenere la lunghezza (t_u_int16) di un valore stringa (senza NT)
int     SPVCPL_5430_GetFieldStringLen ( void * output, void * input );
/// Funzione per ottenere una lunghezza di un filed dal valore di un field ricevuto UHex8
int		SPVCPL_54468_GetLenUHex8FromField( void * output, void * input );
/// Funzione per ottenere una lunghezza di un filed dal valore di un field ricevuto UInt8
int		SPVCPL_54488_GetLenUInt8FromField( void * output, void * input );
//------------------------------------------------------------------------------
#endif

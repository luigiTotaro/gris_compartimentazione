//------------------------------------------------------------------------------
// Telefin Supervisor Port Monitor Form 1.0
//------------------------------------------------------------------------------
// FORM DI MONITOR PORTE DI COMUNICAZIONE (frmPortMonitor.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.03 (30.07.2004 -> 20.09.2004)
//
// Copyright: 2004 Telefin s.r.l.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede frmPortMonitor.h
//------------------------------------------------------------------------------
// Version history: vedere in frmPortMonitor.h
//------------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "SPVPortMonitorForm.h"
//------------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//------------------------------------------------------------------------------

//==============================================================================
/// Metodo costruttore della classe TPortMonitorForm.
///
/// \date [30.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
__fastcall TfPortMonitor::TfPortMonitor(TComponent* Owner)
  : TForm(Owner)
{
  StatusRX = false;
  StatusTX = false;
}

//==============================================================================
/// Metodo per aggiornare le informazioni sulla porta.
///
/// \date [17.03.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::UpdateInfo( SYS_PORT * port )
{
  this->UpdateCaption     ( port );
  this->UpdateGeneralInfo ( port );
  this->UpdateInputInfo   ( port );
  this->UpdateOutputInfo  ( port );
}

//==============================================================================
/// Metodo per aggiornare il titolo della finestra
///
/// \date [17.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::UpdateCaption( SYS_PORT * port )
{
  AnsiString caption;

  if ( port != NULL )
  {
    caption = AnsiString( port->Name ) + " ";
    if ( this->StatusRX == true )
    {
      caption = caption + AnsiString("[RX]");
    }
    if ( this->StatusTX == true )
    {
      caption = caption + AnsiString("[TX]");
    }
    this->Caption = caption;
  }
}

//==============================================================================
/// Metodo per aggiornare le informazioni generali della porta.
///
/// \date [22.02.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::UpdateGeneralInfo( SYS_PORT * port )
{
  if ( port != NULL )
  {
    switch ( port->Type )
    {
      case SYS_KERNEL_PORT_TYPE_SERIAL:
        this->lType->Caption      = "Serial";
        this->lAddress->Caption   = AnsiString( port->Name );
        this->lStatus->Caption    = ( port->Active )?"Aperta":"Chiusa";
      break;
      case SYS_KERNEL_PORT_TYPE_SOCKET:
      case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
      case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
        this->lType->Caption      = "Socket";
        this->lAddress->Caption   = AnsiString( port->Name );
        this->lStatus->Caption    = ( port->Active )?"Connessa":"Disconnessa";
      break;
      default:
        this->lType->Caption      = "Sconociuto";
        this->lAddress->Caption   = AnsiString( port->Name );
        this->lStatus->Caption    = ( port->Active )?"Attiva":"Non attiva";
    }
  }
  else
  {
    this->lType->Caption     = "Sconosciuto";
    this->lAddress->Caption  = "Sconosciuto";
    this->lStatus->Caption   = "Sconosciuto";
  }
}

//==============================================================================
/// Metodo per aggiornare le informazioni di input della porta.
///
/// \date [04.08.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::UpdateInputInfo( SYS_PORT * port )
{
  float use = 0.0 ; // Percentuale di utilizzo della FIFO
  long  tot = 0   ; // Byte contenuti nella FIFO

  if (port != NULL) // Controllo la porta
  {
    if (port->InBuffer != NULL)
    {
      // Calcolo la percentuale di utilizzo della FIFO
      use = port->InBuffer->Count*100;
      use = use/port->InBuffer->Dim;
      // Calcolo il numero di byte contenuti nella FIFO
      tot = SYSGetInFIFOByteCount(port);

      this->lInFIFOByteCount->Caption = AnsiString(tot);
      this->lInFrameCount->Caption    = AnsiString(port->InBuffer->Count);
      this->lInFrameMax->Caption      = AnsiString(port->InBuffer->Dim);
      this->lInFIFOUse->Caption       = AnsiString(use)+" %";
      this->lInByteCount->Caption     = AnsiString(port->InBufferCount);
    }
    else
    {
      this->lInFIFOByteCount->Caption = "Non disponibile";
      this->lInFrameCount->Caption    = "Non disponibile";
      this->lInFrameMax->Caption      = "Non disponibile";
      this->lInFIFOUse->Caption       = "Non disponibile";
      this->lInByteCount->Caption     = "Non disponibile";
    }
  }
  else
  {
    this->lInFIFOByteCount->Caption = "Sconosciuto";
    this->lInFrameCount->Caption    = "Sconosciuto";
    this->lInFrameMax->Caption      = "Sconosciuto";
    this->lInFIFOUse->Caption       = "Sconosciuto";
    this->lInByteCount->Caption     = "Sconosciuto";
  }
}

//==============================================================================
/// Metodo per aggiornare le informazioni di output della porta.
///
/// \date [30.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::UpdateOutputInfo( SYS_PORT * port )
{
}

//==============================================================================
/// Metodo per impostare l'indicatore di ricezione della porta.
///
/// \date [30.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::SetRXStatus        ( bool          status  )
{
  this->lRX->Font->Color = (status)?clGreen:clGray;
  this->StatusRX = status;
}

//==============================================================================
/// Metodo per impostare l'indicatore di trasmissione della porta.
///
/// \date [30.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortMonitor::SetTXStatus        ( bool          status  )
{
  this->lTX->Font->Color = (status)?clRed:clGray;
  this->StatusTX = status;
}

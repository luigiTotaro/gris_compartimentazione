//------------------------------------------------------------------------------
// Telefin Supervisor Port Monitor Form 1.0
//------------------------------------------------------------------------------
// FORM DI MONITOR PORTE DI COMUNICAZIONE (frmPortMonitor.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.03 (30.07.2004 -> 20.09.2004)
//
// Copyright: 2004 Telefin s.r.l.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede frmPortMonitor.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [30.07.2004]:
// - Prima versione del form.
// 0.01 [02.08.2004]:
// - Modificato il metodo TPortMonitorForm::UpdateInputInfo.
// 0.02 [04.08.2004]:
// - Modificato il metodo TPortMonitorForm::UpdateGeneralInfo.
// - Modificato il metodo TPortMonitorForm::UpdateInputInfo.
// 0.03 [20.09.2004]:
// - Modificati il metodo TPortMonitorForm::UpdateGeneralInfo.
//------------------------------------------------------------------------------
#ifndef SPVPortMonitorFormH
#define SPVPortMonitorFormH
//------------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "SYSKernel.h"
//------------------------------------------------------------------------------
class TfPortMonitor : public TForm
{
__published:	// IDE-managed Components
  TGroupBox *gbGeneral;
  TGroupBox *gbBufferIN;
  TGroupBox *gbBufferOUT;
  TLabel *Label1;
  TLabel *Label2;
  TLabel *Label3;
  TLabel *Label4;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label13;
  TGroupBox *GroupBox1;
  TLabel *lRX;
  TGroupBox *GroupBox2;
  TLabel *lTX;
  TLabel *lInFrameCount;
  TLabel *lInFrameMax;
  TLabel *lInFIFOByteCount;
  TLabel *lInFIFOUse;
  TLabel *lInByteCount;
  TMemo *mLog;
  TLabel *lOutFrameCount;
  TLabel *lOutFIFOByteCount;
  TLabel *lOutFrameMax;
  TLabel *lOutFIFOUse;
  TLabel *lOutByteCount;
  TGroupBox *gbLog;
  TLabel *lType;
  TLabel *lAddress;
  TLabel *lStatus;
private:	// User declarations
  bool    StatusRX;
  bool    StatusTX;
public:		// User declarations
/* Metodo costruttore della classe TPortMonitorForm. */
  __fastcall TfPortMonitor   ( TComponent  * Owner   );
/* Metodo per aggiornare le informazioni sulla porta. */
  void __fastcall UpdateInfo         ( SYS_PORT    * port    );
/// Metodo per aggiornare il titolo della finestra
  void __fastcall UpdateCaption      ( SYS_PORT    * port    );
/* Metodo per aggiornare le informazioni generali della porta. */
  void __fastcall UpdateGeneralInfo  ( SYS_PORT    * port    );
/* Metodo per aggiornare le informazioni di input della porta. */
  void __fastcall UpdateInputInfo    ( SYS_PORT    * port    );
/* Metodo per aggiornare le informazioni di output della porta. */
  void __fastcall UpdateOutputInfo   ( SYS_PORT    * port    );
/* Metodo per impostare l'indicatore di ricezione della porta. */
  void __fastcall SetRXStatus        ( bool          status  );
/* Metodo per impostare l'indicatore di trasmissione della porta. */
  void __fastcall SetTXStatus        ( bool          status  );
};
//------------------------------------------------------------------------------
#endif                                 

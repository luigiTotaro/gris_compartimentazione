object fPortMonitor: TfPortMonitor
  Left = 272
  Top = 242
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Port Monitor'
  ClientHeight = 516
  ClientWidth = 312
  Color = clBtnFace
  Constraints.MaxWidth = 320
  Constraints.MinHeight = 550
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001001800680300001600000028000000100000002000
    0000010018000000000040030000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000C0C0C0808080000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000C0C0C08080800000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000C0C0C0C0C0C0808080000000000000000000000000000000000000000000
    C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8080800000000000000000
    00000000000000000000000000C0C0C0FFFFFF80808080808080808080808080
    8080808080000000000000000000000000000000000000000000000000FFFFFF
    8080800000000000000000000000000000000000000000000000000000000000
    0000000000000000000080808080808080808000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    00000000000000808080808080808080FFFFFF00000000000000000000000000
    0000000000000000000000000000000000000000808080C0C0C0C0C0C0C0C0C0
    C0C0C0FFFFFF8080800000000000000000000000000000000000000000000000
    00000000808080C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF80808000000000000000
    0000000000000000000000000000000000000000808080C0C0C0C0C0C0C0C0C0
    C0C0C0FFFFFF8080800000000000000000000000000000000000000000000000
    00000000808080C0C0C0C0C0C0C0C0C0C0C0C0FFFFFF80808000000000000000
    0000000000000000000000000000000000808080C0C0C0C0C0C0C0C0C0C0C0C0
    C0C0C0C0C0C0FFFFFF8080800000000000000000000000000000000000000000
    0000969600DCDC00DCDC00DCDC00DCDC00DCDC00DCDC8EFFFF80808000000000
    000000000000000000000000000000000000808000FFFF00808000FFFF008080
    00FFFF00808000FFFF000000000000000000000000000000000000000000FFF1
    0000FFF10000FC010000F8030000F0070000F1FF0000E1FF0000E1FF0000C0FF
    0000807F0000807F0000807F0000807F0000003F0000003F0000807F0000}
  OldCreateOrder = False
  DesignSize = (
    312
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object gbGeneral: TGroupBox
    Left = 4
    Top = 2
    Width = 271
    Height = 59
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Informazioni generali'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 13
      Width = 24
      Height = 13
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 26
      Width = 41
      Height = 13
      Caption = 'Indirizzo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 39
      Width = 28
      Height = 13
      Caption = 'Stato:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lType: TLabel
      Left = 150
      Top = 13
      Width = 59
      Height = 13
      Caption = 'Sconosciuto'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lAddress: TLabel
      Left = 150
      Top = 26
      Width = 59
      Height = 13
      Caption = 'Sconosciuto'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lStatus: TLabel
      Left = 150
      Top = 39
      Width = 59
      Height = 13
      Caption = 'Sconosciuto'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object gbBufferIN: TGroupBox
    Left = 4
    Top = 64
    Width = 303
    Height = 85
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Buffer in ingresso'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label4: TLabel
      Left = 8
      Top = 13
      Width = 75
      Height = 13
      Caption = 'Byte nella FIFO:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 26
      Width = 69
      Height = 13
      Caption = 'Numero frame:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 8
      Top = 39
      Width = 55
      Height = 13
      Caption = 'Max. frame:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 52
      Width = 62
      Height = 13
      Caption = 'Utilizzo FIFO:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 8
      Top = 65
      Width = 61
      Height = 13
      Caption = 'Byte ricevuti:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lInFrameCount: TLabel
      Left = 150
      Top = 26
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lInFrameMax: TLabel
      Left = 150
      Top = 39
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lInFIFOByteCount: TLabel
      Left = 150
      Top = 13
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lInFIFOUse: TLabel
      Left = 150
      Top = 52
      Width = 17
      Height = 13
      Caption = '0 %'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lInByteCount: TLabel
      Left = 150
      Top = 65
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object gbBufferOUT: TGroupBox
    Left = 4
    Top = 152
    Width = 303
    Height = 85
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Buffer in uscita'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label9: TLabel
      Left = 8
      Top = 13
      Width = 75
      Height = 13
      Caption = 'Byte nella FIFO:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 8
      Top = 26
      Width = 69
      Height = 13
      Caption = 'Numero frame:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 8
      Top = 39
      Width = 55
      Height = 13
      Caption = 'Max. frame:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 8
      Top = 52
      Width = 62
      Height = 13
      Caption = 'Utilizzo FIFO:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 8
      Top = 65
      Width = 61
      Height = 13
      Caption = 'Byte ricevuti:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lOutFrameCount: TLabel
      Left = 150
      Top = 26
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lOutFIFOByteCount: TLabel
      Left = 150
      Top = 13
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lOutFrameMax: TLabel
      Left = 150
      Top = 39
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lOutFIFOUse: TLabel
      Left = 150
      Top = 52
      Width = 17
      Height = 13
      Caption = '0 %'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lOutByteCount: TLabel
      Left = 150
      Top = 65
      Width = 6
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 278
    Top = 2
    Width = 28
    Height = 29
    Color = clBtnFace
    ParentColor = False
    TabOrder = 3
    object lRX: TLabel
      Left = 4
      Top = 10
      Width = 18
      Height = 13
      Caption = 'RX'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 278
    Top = 32
    Width = 28
    Height = 29
    TabOrder = 4
    object lTX: TLabel
      Left = 4
      Top = 10
      Width = 17
      Height = 13
      Caption = 'TX'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object gbLog: TGroupBox
    Left = 4
    Top = 240
    Width = 303
    Height = 275
    Anchors = [akLeft, akTop, akBottom]
    Caption = 'Log Esadecimale/ASCII'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object mLog: TMemo
    Left = 8
    Top = 254
    Width = 293
    Height = 255
    Anchors = [akLeft, akTop, akBottom]
    Color = clNavy
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 6
  end
end

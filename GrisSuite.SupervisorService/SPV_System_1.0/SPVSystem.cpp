//==============================================================================
// Telefin Supervisor System Module 1.0
//------------------------------------------------------------------------------
// MODULO SISTEMA (SPVSystem.cpp)
// Progetto:  Telefin Supervisor Server 1.4
//
// Revisione:	0.43 (13.04.2004 -> 06.04.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland C++ Builder 2006-2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:    	richiede SPVSystem.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVSystem.h
//==============================================================================
#pragma hdrstop
#include "SPVSystem.h"
#include "cnv_lib.h"
#include "SPVDBInterface.h"
#include "SPVApplicative.h"
#include "SPVConfig.h"
#include "SPVTopography.h"
#include "UtilityLibrary.h"
#include "SYSSocket.h"
//------------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
	SPV_SYSTEM        	SPVSystem;
	XML_SOURCE * 		SPVSystemSource;

	SPV_DEVICE_SYSTEM	SPVDeviceSystemList	    [SPV_SYSTEM_MAX_DEVICE_SYSTEM_COUNT];
	SPV_DEVICE_VENDOR	SPVDeviceVendorList		[SPV_SYSTEM_MAX_DEVICE_VENDOR_COUNT];
	SPV_DEVICE_TYPE   	SPVDeviceTypeList 		[SPV_SYSTEM_MAX_DEVICE_TYPE_COUNT];
	SPV_DEVICE        	SPVDeviceList     		[SPV_SYSTEM_MAX_DEVICE_COUNT];
	SPV_NODE * 			SPVNodeList;

	int               	SPVDeviceSystemCount;
	int               	SPVDeviceVendorCount;
	int               	SPVDeviceTypeCount;
	int               	SPVDeviceCount;
	int               	SPVNodeCount;
	int					SPVOwnedDeviceCount;

	//static  _CRITICAL_SECTION  		SPVSystemCriticalSection;
	static  SYS_LOCK  				SPVSystemLock;
	static	SPV_TPG_Topography *	SPVSystemTopography;

//==============================================================================
// Implementazione Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per inizializzare il modulo
///
/// Non restituisce alcun valore.
///
/// \date [29.10.2009]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.00
//------------------------------------------------------------------------------
void SPVSystemInit( void )
{
	// --- Inizializzo la sezione critica ---
	//InitializeCriticalSection( &SPVSystemCriticalSection );
	SYSInitLock(&SPVSystemLock,"SPVSystemLock");

	// --- Inizializzo la lista della tipologia periferica ---
	SPVDeviceSystemCount = 0;
	memset( &SPVDeviceSystemList, 0, sizeof(SPV_DEVICE_SYSTEM)*SPV_SYSTEM_MAX_DEVICE_SYSTEM_COUNT );

	// --- Inizializzo la lista dei produttori di periferiche ---
	SPVDeviceVendorCount = 0;
	memset( &SPVDeviceVendorList, 0, sizeof(SPV_DEVICE_VENDOR)*SPV_SYSTEM_MAX_DEVICE_VENDOR_COUNT );

	// --- Inizializzo la lista dei tipi periferica ---
	SPVDeviceTypeCount = 0;
	memset( &SPVDeviceTypeList, 0, sizeof(SPV_DEVICE_TYPE)*SPV_SYSTEM_MAX_DEVICE_TYPE_COUNT );

	SPVOwnedDeviceCount = 0; // Resetto il contatore delle periferiche da diagnosticare

	// --- Resetto il ptr alla struttura della sorgente sistema ---
	SPVSystemSource = NULL;
	// --- Resetto il ptr alla struttura informazioni topografiche ---
	SPVSystemTopography = NULL;
}

//==============================================================================
/// Funzione per chiudere il modulo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
void SPVSystemClear( void )
{
  // --- Elimina la sezione critica ---
  //DeleteCriticalSection( &SPVSystemCriticalSection );
  SYSClearLock(&SPVSystemLock);

  // --- Resetta la lista dei tipi periferica ---
  SPVDeviceTypeCount = 0;
  memset( &SPVDeviceTypeList, 0, sizeof(SPV_DEVICE_TYPE)*SPV_SYSTEM_MAX_DEVICE_TYPE_COUNT );

  // --- Resetta la lista dei produttori di periferiche ---
  SPVDeviceVendorCount = 0;
  memset( &SPVDeviceVendorList, 0, sizeof(SPV_DEVICE_VENDOR)*SPV_SYSTEM_MAX_DEVICE_VENDOR_COUNT );

  // --- Resetta la lista delle tipologie di periferica ---
  SPVDeviceSystemCount = 0;
  memset( &SPVDeviceSystemList, 0, sizeof(SPV_DEVICE_SYSTEM)*SPV_SYSTEM_MAX_DEVICE_SYSTEM_COUNT );
}

//==============================================================================
/// Funzione per entrare nella sezione critica dei dati di sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVEnterSystemData( char * label )
{
	// --- Entro nella sezione critica ---
	//EnterCriticalSection( &SPVSystemCriticalSection );
	SYSLock(&SPVSystemLock,label);
}

//==============================================================================
/// Funzione per uscire dalla sezione critica dei dati di sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVLeaveSystemData( void )
{
	// --- Esco dalla sezione critica ---
	//LeaveCriticalSection( &SPVSystemCriticalSection );
	SYSUnLock(&SPVSystemLock);
}


//==============================================================================
/// Funzione per verificare in lista la presenza di una tipologia di periferica
///
/// \date [11.02.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
bool SPVIsDeviceSystemInList( unsigned __int32 SystemID )
{
	bool isInList = false;
	SPV_DEVICE_SYSTEM * current       = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVIsDeviceSystemInList");

	if ( SPVDeviceSystemCount > 0 )
	{
		for ( int t=0; t<SPVDeviceSystemCount; t++ )
		{
			current = &SPVDeviceSystemList[t];
			if ( current != NULL )
			{
				// Se l'id corrisponde
				if ( SystemID == current->ID )
				{
					isInList = true;
					t = SPVDeviceSystemCount; // Fine del ciclo for
				}
			}
		}
	}

	  // --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return isInList;
}

//==============================================================================
/// Funzione per verificare in lista la presenza di un vendor di periferica
///
/// \date [11.02.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
bool SPVIsDeviceVendorInList( unsigned __int32 VendorID )
{
	bool isInList = false;
	SPV_DEVICE_VENDOR * current	= NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVIsDeviceVendorInList");

	if ( SPVDeviceVendorCount > 0 )
	{
		for ( int t=0; t<SPVDeviceVendorCount; t++ )
		{
			current = &SPVDeviceVendorList[t];
			if ( current != NULL )
			{
				// Se l'id corrisponde
				if ( VendorID == current->ID )
				{
					isInList = true;
					t = SPVDeviceVendorCount; // Fine del ciclo for
				}
			}
		}
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return isInList;
}

//==============================================================================
/// Funzione per ottenere una struttura di tipo periferica
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
SPV_DEVICE_TYPE * SPVGetDeviceType( char * type )
{
	SPV_DEVICE_TYPE * device_type = NULL;
	SPV_DEVICE_TYPE * current     = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVGetDeviceType");

	if ( type != NULL ) // Controllo che la stringa codice del tipo sia valida
	{
		if ( SPVDeviceTypeCount > 0 )
		{
			for ( int t=0; t<SPVDeviceTypeCount; t++ )
			{
				current = &SPVDeviceTypeList[t];
				if ( current != NULL )
				{
					// Se il codice corrisponde
					if ( strcmpi ( type, current->Code ) == 0 )
					{
						device_type = current;
						t = SPVDeviceTypeCount; // Fine del ciclo for
					}
				}
			}
		}
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return device_type;
}

//==============================================================================
/// Funzione per caricare un tipo periferica
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLoadDeviceType( XML_ELEMENT * e_type )
{
	int ret_code = SPV_SYSTEM_NO_ERROR;
	SPV_DEVICE_TYPE * device_type = NULL;
	SPV_DEVICE_SYSTEM * device_system = NULL;
	SPV_DEVICE_VENDOR * device_vendor = NULL;
	char * endian_string = NULL;
	bool	endian_bool = false;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVLoadDeviceType");

	if ( e_type != NULL )
	{
		if ( strcmpi( e_type->name, "type" ) == 0 ) // Controllo che l'elemento sia una root type
		{
			// Recupero il ptr al prossimo tipo periferica
			device_type = &SPVDeviceTypeList[SPVDeviceTypeCount];
			if ( device_type != NULL )
			{
				device_type->ID           	= XMLExtractValue 	( e_type, "ID"         	);
				device_type->Code         	= XMLExtractValue 	( e_type, "code"       	);
				device_type->Name         	= XMLExtractValue 	( e_type, "name"       	);
				device_type->VendorID     	= XMLGetValueUInt32	( e_type, "vendor_id"  	);
				device_type->SystemID     	= XMLGetValueUInt32	( e_type, "system_id"  	);
				device_type->Version      	= XMLExtractValue 	( e_type, "version"    	);
				endian_string 				= XMLGetValue		( e_type, "endian"  	);
				if (endian_string != NULL)
				{
					if (strcmpi(endian_string,"little")==0) {
						endian_bool = false; // 0 (false) = LITTLE ENDIAN
					}
					else
					{
						endian_bool = true; // qualsiasi altro valore: 1 (true) = BIG ENDIAN
                    }
				}
				else
				{
					endian_bool = true; // default: 1 (true) = BIG ENDIAN
                }
				device_type->Endian		= endian_bool;
				device_type->Definition   = NULL;
				device_type->ComProtDef   = NULL;
				device_type->ComProt      = NULL;
				device_type->GenStatusDef = NULL;
				device_type->GenCfgDef    = NULL;
				device_type->Tag          = NULL;
				SPVDeviceTypeCount++; // Incremento il contatore dei tipi periferica

				//Caricamento delle tipologie di periferica

				// Se non � gi� presente aggiungo la tipologia di periferica alla propria lista
				if (!SPVIsDeviceSystemInList(device_type->SystemID))
				{
					// Recupero il ptr al prossimo system periferica
					device_system = &SPVDeviceSystemList[SPVDeviceSystemCount];
					if ( device_system != NULL )
					{
						device_system->ID = device_type->SystemID;
						device_system->Description = XMLExtractValue ( e_type, "system_description");
						SPVDeviceSystemCount++; // Incremento il contatore dei system periferica
					}
				}

				if (ret_code == SPV_SYSTEM_NO_ERROR)
				{
					//Caricamento dei produttori delle varie periferiche

					// Se non � gi� presente aggiungo il produttore di periferica alla propria lista
					if (!SPVIsDeviceVendorInList(device_type->VendorID))
					{
						// Recupero il ptr al prossimo vendor periferica
						device_vendor = &SPVDeviceVendorList[SPVDeviceVendorCount];
						if ( device_vendor != NULL )
						{
							device_vendor->ID = device_type->VendorID;
							device_vendor->Name = XMLExtractValue ( e_type, "vendor_name");
							device_vendor->Description = XMLExtractValue ( e_type, "vendor_description");
							SPVDeviceVendorCount++; // Incremento il contatore dei vendor periferica
						}
					}
				}

			}
			else
			{
				ret_code = SPV_SYSTEM_INVALID_DEVICE_TYPE;
			}
		}
		else
		{
			ret_code = SPV_SYSTEM_NOT_TYPE_ELEMENT;
		}
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_ELEMENT;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la struttura un tipo periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVFreeDeviceType( SPV_DEVICE_TYPE * type )
{
  int               ret_code    = SPV_SYSTEM_NO_ERROR;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVFreeDeviceType");

  if ( type != NULL )
  {
	// --- Cancello l'ID del tipo periferica ---
	if ( type->ID != NULL )
	{
	  try
	  {
		free( type->ID ); // -FREE
	  }
	  catch(...)
	  {
		ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
	  }
	}
	// --- Cancello il codice del tipo periferica ---
	if ( type->Code != NULL )
	{
	  try
	  {
		free( type->Code ); // -FREE
	  }
	  catch(...)
	  {
		ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
	  }
	}
	// --- Cancello il nome del tipo periferica ---
	if ( type->Name != NULL )
	{
	  try
	  {
		free( type->Name ); // -FREE
	  }
	  catch(...)
	  {
		ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
	  }
	}
	// --- Cancello la versione del tipo periferica ---
	if ( type->Version != NULL )
	{
	  try
	  {
		free( type->Version ); // -FREE
	  }
	  catch(...)
	  {
		ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		/* TODO -oEnrico -cError : Gestione dell'errore critico */
	  }
	}
	// --- Resetto la memoria della struttura ---
	memset( type, 0, sizeof(SPV_DEVICE_TYPE) );
  }
  else
  {
	ret_code = SPV_SYSTEM_INVALID_DEVICE_TYPE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare la struttura sistema periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.02.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeDeviceSystem( SPV_DEVICE_SYSTEM * system )
{
	int ret_code = SPV_SYSTEM_NO_ERROR;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVFreeDeviceSystem");

	if ( system != NULL )
	{
		// --- Cancello la descrizione del sistema periferica ---
		if ( system->Description != NULL )
		{
			try
			{
				free( system->Description ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Resetto la memoria della struttura ---
		memset( system, 0, sizeof(SPV_DEVICE_SYSTEM) );
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_DEVICE_TYPE;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la struttura vendor periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.05.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeDeviceVendor( SPV_DEVICE_VENDOR * vendor )
{
	int ret_code = SPV_SYSTEM_NO_ERROR;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVFreeDeviceVendor");

	if ( vendor != NULL )
	{
		// --- Cancello la descrizione del nome periferica ---
		if ( vendor->Name != NULL )
		{
			try
			{
				free( vendor->Name ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello la descrizione del vendor periferica ---
		if ( vendor->Description != NULL )
		{
			try
			{
				free( vendor->Description ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Resetto la memoria della struttura ---
		memset( vendor, 0, sizeof(SPV_DEVICE_VENDOR) );
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_DEVICE_TYPE;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la lista dei sistemi periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.05.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeDeviceSystemList( void )
{
	int ret_code = SPV_SYSTEM_NO_ERROR;
	SPV_DEVICE_SYSTEM * current       = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVFreeDeviceSystemList");

	if ( SPVDeviceSystemCount > 0 )
	{
		for ( int t=0; t<SPVDeviceSystemCount; t++ )
		{
			current = &SPVDeviceSystemList[t];
			ret_code = SPVFreeDeviceSystem( current );
		}
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la lista dei vendor periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.05.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVFreeDeviceVendorList( void )
{
	int ret_code = SPV_SYSTEM_NO_ERROR;
	SPV_DEVICE_VENDOR * current       = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVFreeDeviceVendorList");

	if ( SPVDeviceVendorCount > 0 )
	{
		for ( int t=0; t<SPVDeviceVendorCount; t++ )
		{
			current = &SPVDeviceVendorList[t];
			ret_code = SPVFreeDeviceVendor( current );
		}
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere il SrvID (32bit) di una periferica
///
/// \date [07.12.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int32 SPVGetSrvID( SPV_DEVICE * device )
{
	unsigned __int32    SrvID_32  = 0   ;
  SPV_SERVER        * server    = NULL;
  SPV_REGION        * region    = NULL;
  SPV_ZONE          * zone      = NULL;
  SPV_NODE          * node      = NULL;

  if ( SPVValidDevice( device ) )
	{
    try
    {
      node = (SPV_NODE*)device->Node;
    }
    catch(...)
    {
			node = NULL;
		}
		if ( SPVValidNode( node ) )
    {
      try
			{
        zone = (SPV_ZONE*)node->Zone;
      }
      catch(...)
      {
        zone = NULL;
      }
      if ( SPVValidZone( zone ) )
      {
        try
        {
          region = (SPV_REGION*)zone->Region;
        }
        catch(...)
        {
          region = NULL;
        }
        if ( SPVValidRegion( region ) )
        {
          try
          {
            server = (SPV_SERVER*)region->Server;
		  }
          catch(...)
          {
            server = NULL;
          }
          if ( SPVValidServer( server ) )
					{
            try
            {
							SrvID_32 = server->SrvID;
						}
						catch(...)
						{
							SrvID_32 = 0;
            }
          }
        }
      }
    }
  }

	return SrvID_32;
}

//==============================================================================
/// Funzione per ottenere il SrvID (32bit) di un nodo
///
/// \date [07.12.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int32 SPVGetSrvID( SPV_NODE * node )
{
	unsigned __int32    SrvID_32  = 0   ;
  SPV_SERVER        * server    = NULL;
  SPV_REGION        * region    = NULL;
  SPV_ZONE          * zone      = NULL;

  if ( SPVValidNode( node ) )
  {
    try
    {
      zone = (SPV_ZONE*)node->Zone;
    }
    catch(...)
    {
      zone = NULL;
    }
    if ( SPVValidZone( zone ) )
    {
      try
      {
        region = (SPV_REGION*)zone->Region;
      }
      catch(...)
      {
        region = NULL;
      }
      if ( SPVValidRegion( region ) )
      {
        try
        {
          server = (SPV_SERVER*)region->Server;
        }
        catch(...)
        {
          server = NULL;
        }
        if ( SPVValidServer( server ) )
        {
          try
          {
						SrvID_32 = server->SrvID;
					}
					catch(...)
					{
						SrvID_32 = 0;
					}
				}
			}
		}
	}

	return SrvID_32;
}

//==============================================================================
/// Funzione per ottenere il SrvID (32bit) di una zona
///
/// \date [07.12.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int32 SPVGetSrvID( SPV_ZONE * zone )
{
	unsigned __int32    SrvID_32  = 0   ;
  SPV_SERVER        * server    = NULL;
  SPV_REGION        * region    = NULL;

	if ( SPVValidZone( zone ) )
  {
    try
    {
			region = (SPV_REGION*)zone->Region;
    }
    catch(...)
    {
      region = NULL;
    }
		if ( SPVValidRegion( region ) )
    {
      try
      {
				server = (SPV_SERVER*)region->Server;
      }
      catch(...)
      {
		server = NULL;
      }
      if ( SPVValidServer( server ) )
      {
        try
        {
					SrvID_32 = server->SrvID;
				}
				catch(...)
				{
					SrvID_32 = 0;
				}
			}
		}
	}

	return SrvID_32;
}

//==============================================================================
/// Funzione per ottenere il SrvID (32bit) di una regione
///
/// \date [07.12.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
unsigned __int32 SPVGetSrvID( SPV_REGION * region )
{
	unsigned __int32    SrvID_32  = 0   ;
  SPV_SERVER        * server    = NULL;

  if ( SPVValidRegion( region ) )
  {
    try
    {
      server = (SPV_SERVER*)region->Server;
    }
    catch(...)
    {
	  server = NULL;
    }
    if ( SPVValidServer( server ) )
    {
      try
      {
				SrvID_32 = server->SrvID;
			}
			catch(...)
			{
				SrvID_32 = 0;
			}
		}
	}

	return SrvID_32;
}

//==============================================================================
/// Funzione per calcolare il codice unico (UniID) di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVGetDeviceUniID( SPV_DEVICE * device )
{
  int                 ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  unsigned __int16    RegID_16      = 0   ;
  unsigned __int16    ZonID_16      = 0   ;
  unsigned __int16    NodID_16      = 0   ;
  unsigned __int16  * NodID_16_ptr  = NULL;
  unsigned __int16  * DevID_16_ptr  = NULL;
  SPV_NODE          * node          = NULL;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVGetDeviceUniID");

  if ( SPVValidDevice( device ) )
  {
    node = (SPV_NODE*)device->Node;
    if ( SPVValidNode( node ) )
    {
      // --- Recupero il NodID (16bit) dal NodID (64bit) ---
      try
      {
        NodID_16_ptr = (unsigned __int16*) &node->NodID;
        NodID_16 = NodID_16_ptr[2];
      }
      catch(...)
      {
        NodID_16 = 0;
      }
      // --- Recupero il ZonID (16bit) dal NodID (64bit) ---
      try
      {
        ZonID_16 = NodID_16_ptr[1];
      }
      catch(...)
      {
        ZonID_16 = 0;
      }
      // --- Recupero il RegID (16bit) dal NodID (64bit) ---
      try
      {
        RegID_16 = NodID_16_ptr[0];
	  }
      catch(...)
      {
        RegID_16 = 0;
      }
      // --- Registro NodID (16bit), ZonID (16bit) e RegID (16bit) nel DevID (64bit) ---
      try
      {
        DevID_16_ptr = (unsigned __int16*) &device->DevID;
        DevID_16_ptr[0] = RegID_16;
		DevID_16_ptr[1] = ZonID_16;
        DevID_16_ptr[2] = NodID_16;
	  }
	  catch(...)
	  {
		ret_code = SPV_SYSTEM_CRITICAL_ERROR;
	  }
	}
	else
	{
	  ret_code = SPV_SYSTEM_INVALID_NODE;
	}
  }
  else
  {
	ret_code = SPV_SYSTEM_INVALID_DEVICE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per generare una stringa del codice unico (UniID) di una periferica
///
/// In caso di errore restituisce NULL.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
char * SPVGetDeviceUniIDStr( SPV_DEVICE * device )
{
	char              * UniID_x5_value  = NULL;
	unsigned __int32    SrvID_32        = 0   ;
	char              * SrvID_32_value  = NULL;
	unsigned __int16    RegID_16        = 0   ;
	char              * RegID_16_value  = NULL;
	unsigned __int16    ZonID_16        = 0   ;
	char              * ZonID_16_value  = NULL;
	unsigned __int16    NodID_16        = 0   ;
	char              * NodID_16_value  = NULL;
	unsigned __int16    DevID_16        = 0   ;
	char              * DevID_16_value  = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVGetDeviceUniIDStr");

	if ( SPVValidDevice( device ) )
	{
		// --- Recupero il SrvID (32bit) della regione ---
		try
		{
			SrvID_32 = SPVGetSrvID( device );
			SrvID_32_value = XMLType2ASCII( (void*)&SrvID_32, t_u_int_32 ); // -MALLOC
		}
		catch(...)
		{
			SrvID_32_value = NULL;
		}
		// --- Recupero il RegID (16bit) dal DevID (64bit) ---
		try
		{
			RegID_16 = ((unsigned __int16*)&device->DevID)[0];
			RegID_16_value = XMLType2ASCII( (void*)&RegID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			RegID_16_value;
		}
		// --- Recupero il ZonID (16bit) dal DevID (64bit) ---
		try
		{
			ZonID_16 = ((unsigned __int16*)&device->DevID)[1];
			ZonID_16_value = XMLType2ASCII( (void*)&ZonID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			ZonID_16_value;
		}
		// --- Recupero il NodID (16bit) dal DevID (64bit) ---
		try
		{
			NodID_16 = ((unsigned __int16*)&device->DevID)[2];
			NodID_16_value = XMLType2ASCII( (void*)&NodID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			NodID_16_value;
		}
		// --- Recupero il DevID (16bit) dal DevID (64bit) ---
		try
		{
			DevID_16 = ((unsigned __int16*)&device->DevID)[3];
			DevID_16_value = XMLType2ASCII( (void*)&DevID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			DevID_16_value;
		}
		UniID_x5_value = XMLAddToString( UniID_x5_value, SrvID_32_value );
		UniID_x5_value = XMLAddToString( UniID_x5_value, "." );
		UniID_x5_value = XMLAddToString( UniID_x5_value, RegID_16_value );
		UniID_x5_value = XMLAddToString( UniID_x5_value, "." );
		UniID_x5_value = XMLAddToString( UniID_x5_value, ZonID_16_value );
		UniID_x5_value = XMLAddToString( UniID_x5_value, "." );
		UniID_x5_value = XMLAddToString( UniID_x5_value, NodID_16_value );
		UniID_x5_value = XMLAddToString( UniID_x5_value, "." );
		UniID_x5_value = XMLAddToString( UniID_x5_value, DevID_16_value );

		if ( SrvID_32_value != NULL ) free( SrvID_32_value );
		if ( RegID_16_value != NULL ) free( RegID_16_value );
		if ( ZonID_16_value != NULL ) free( ZonID_16_value );
		if ( NodID_16_value != NULL ) free( NodID_16_value );
		if ( DevID_16_value != NULL ) free( DevID_16_value );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return UniID_x5_value;
}

//==============================================================================
/// Funzione per impostare la topografia di una periferica
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	SPVSetDeviceTopography( SPV_DEVICE * device )
{
	int                 ret_code		= SPV_SYSTEM_NO_ERROR; // Codice di ritorno
	SPV_TPG_Rack *		rack			= NULL	;
	XML_ELEMENT	*		element			= NULL	;
	char *				position		= NULL	;
	char *				position_col	= NULL	;
	char *				position_row	= NULL	;
	unsigned __int16	station_xmlid	= 0   	;
	unsigned __int16	building_xmlid	= 0   	;
	unsigned __int16	rack_xmlid		= 0   	;
	unsigned __int8		rack_col		= 0		;
	unsigned __int8		rack_row		= 0		;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVSetDeviceTopography");

	if ( SPVValidDevice( device ) )
	{
		//* DEBUG */ SPVReportApplicEvent( NULL, 2, 2222, "DEVICE OK", "SPVSetDeviceTopography()" );
		try
		{
			element = device->Def;
			if ( element != NULL ) {
				//* DEBUG */ SPVReportApplicEvent( NULL, 2, 2223, "ELEMENT OK", "SPVSetDeviceTopography()" );
				// --- Recupero l'armadio ---
				station_xmlid 	= XMLGetValueInt( element, "station" );
				building_xmlid 	= XMLGetValueInt( element, "building" );
				rack_xmlid 		= XMLGetValueInt( element, "location" );
				if ( SPVSystemTopography != NULL ) {
					//* DEBUG */ SPVReportApplicEvent( NULL, 2, 2224, "TOPOGRAPHY OK", "SPVSetDeviceTopography()" );
					rack = SPVSystemTopography->GetRack( station_xmlid, building_xmlid, rack_xmlid );
					if ( rack != NULL ) {
						//* DEBUG */ SPVReportApplicEvent( NULL, 2, 2228, "RACK OK", "SPVSetDeviceTopography()" );
						// --- Recupero posizione nell'armadio ---
						position = XMLGetValue( element, "position" );
						if ( position != NULL ) {
							//* DEBUG */ SPVReportApplicEvent( NULL, 2, 2229, "POSITION OK", "SPVSetDeviceTopography()" );
							position_col = XMLGetToken( position, ",", 0 ); // -MALLOC
							position_row = XMLGetToken( position, ",", 1 ); // -MALLOC
							rack_col = cnv_CharPToInt8( position_col );
							rack_row = cnv_CharPToInt8( position_row );
							try
							{
								if ( position_col != NULL ) free( position_col ); // -FREE
								if ( position_row != NULL ) free( position_row ); // -FREE
							}
							catch(...)
							{

                            }
						}
				    	else
						{
				    		ret_code = SPV_SYSTEM_INVALID_POSITION;
				    	}
				    }
				    else
					{
				    	ret_code = SPV_SYSTEM_INVALID_RACK;
					}
				}
				else
				{
                    ret_code = SPV_SYSTEM_INVALID_TOPOGRAPHY;
                }
			}
			else
			{
				ret_code = SPV_SYSTEM_INVALID_DEFINITION;
			}
			// --- Salvo i dati ---
			device->Rack = rack;
			device->RackCol = rack_col;
			device->RackRow = rack_row;
		}
		catch(...)
		{
			ret_code = SPV_SYSTEM_FUNCTION_EXCEPTION;
			device->Rack = NULL;
			device->RackCol = 0;
			device->RackRow = 0;
		}
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_DEVICE;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere in NodID di una periferica
///
/// \date [13.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
unsigned __int64 SPVGetNodID( SPV_DEVICE * device )
{
  unsigned __int64    NodID_64  = 0   ;
  SPV_NODE          * node      = NULL;

  if ( SPVValidDevice( device ) )
  {
    node = (SPV_NODE*)device->Node;
    if ( SPVValidNode( node ) )
    {
      try
      {
        NodID_64 = node->NodID;
      }
      catch(...)
      {
		NodID_64 = 0;
      }
    }
  }

  return NodID_64;
}

//==============================================================================
/// Funzione per calcolare il codice unico (UniID) di un nodo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVGetNodeUniID( SPV_NODE * node )
{
  int                 ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  unsigned __int16    RegID_16      = 0   ;
  unsigned __int16    ZonID_16      = 0   ;
  unsigned __int16  * ZonID_16_ptr  = NULL;
  unsigned __int16  * NodID_16_ptr  = NULL;
  SPV_ZONE          * zone          = NULL;
	SPV_REGION *		region			= NULL;
	SPV_SERVER *		server			= NULL;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVGetNodeUniID");

  if ( SPVValidNode( node ) )
  {
    zone = (SPV_ZONE*)node->Zone;
    if ( SPVValidZone( zone ) )
    {
      // --- Recupero il ZonID (16bit) dal ZonID (64bit) ---
      try
      {
        ZonID_16_ptr = (unsigned __int16*) &zone->ZonID;
        ZonID_16 = ZonID_16_ptr[1];
      }
      catch(...)
      {
        ZonID_16 = 0;
      }
      // --- Recupero il RegID (16bit) dal ZonID (64bit) ---
	  try
      {
        RegID_16 = ZonID_16_ptr[0];
      }
      catch(...)
      {
        RegID_16 = 0;
      }
      // --- Registro il ZonID (16bit) e RegID (16bit) nel NodID (64bit) ---
      try
      {
        NodID_16_ptr = (unsigned __int16*) &node->NodID ;
        NodID_16_ptr[0] = RegID_16;
		NodID_16_ptr[1] = ZonID_16;

			// --- Gestione del flag Local ---
			if ( node->Local == true ) {
					region = (SPV_REGION*)zone->Region;
					if ( region != NULL ) {
						server = (SPV_SERVER*)region->Server;
						if (server != NULL) {
							if (server->NodID == 0 ) {
								server->NodID = node->NodID;
							}
						}
					}
			}

	  }
	  catch(...)
	  {
		ret_code = SPV_SYSTEM_CRITICAL_ERROR;
	  }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_ZONE;
	}
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_NODE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per generare una stringa del codice unico (UniID) di un nodo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
char * SPVGetNodeUniIDStr( SPV_NODE * node )
{
	char              * UniID_x4_value  = NULL;
	unsigned __int32    SrvID_32        = 0   ;
	char              * SrvID_32_value  = NULL;
	unsigned __int16    RegID_16        = 0   ;
    char              * RegID_16_value  = NULL;
    unsigned __int16    ZonID_16        = 0   ;
    char              * ZonID_16_value  = NULL;
    unsigned __int16    NodID_16        = 0   ;
    char              * NodID_16_value  = NULL;

    // --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVGetNodeUniIDStr");

    if ( SPVValidNode( node ) )
    {
    	// --- Recupero il SrvID (32bit) della regione ---
		try
		{
			SrvID_32 = SPVGetSrvID( node );
			SrvID_32_value = XMLType2ASCII( (void*)&SrvID_32, t_u_int_32 ); // -MALLOC
		}
		catch(...)
		{
			SrvID_32_value = NULL;
    	}
		// --- Recupero il RegID (16bit) dal NodID (64bit) ---
		try
		{
			RegID_16 = ((unsigned __int16*)&node->NodID)[0];
			RegID_16_value = XMLType2ASCII( (void*)&RegID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			RegID_16_value;
		}
		// --- Recupero il ZonID (16bit) dal NodID (64bit) ---
		try
		{
			ZonID_16 = ((unsigned __int16*)&node->NodID)[1];
			ZonID_16_value = XMLType2ASCII( (void*)&ZonID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			ZonID_16_value;
		}
		// --- Recupero il NodID (16bit) dal NodID (64bit) ---
		try
		{
			NodID_16 = ((unsigned __int16*)&node->NodID)[2];
			NodID_16_value = XMLType2ASCII( (void*)&NodID_16, t_u_int_16 ); // -MALLOC
		}
		catch(...)
		{
			NodID_16_value;
		}
		UniID_x4_value = XMLAddToString( UniID_x4_value, SrvID_32_value );
		UniID_x4_value = XMLAddToString( UniID_x4_value, "." );
		UniID_x4_value = XMLAddToString( UniID_x4_value, RegID_16_value );
		UniID_x4_value = XMLAddToString( UniID_x4_value, "." );
		UniID_x4_value = XMLAddToString( UniID_x4_value, ZonID_16_value );
		UniID_x4_value = XMLAddToString( UniID_x4_value, "." );
		UniID_x4_value = XMLAddToString( UniID_x4_value, NodID_16_value );

		if ( SrvID_32_value != NULL ) free( SrvID_32_value );
		if ( RegID_16_value != NULL ) free( RegID_16_value );
		if ( ZonID_16_value != NULL ) free( ZonID_16_value );
		if ( NodID_16_value != NULL ) free( NodID_16_value );
   	}

    // --- Esco dalla sezione critica ---
    SPVLeaveSystemData( );

    return UniID_x4_value;
}

//==============================================================================
/// Funzione per ottenere il ZonID di un nodo
///
/// \date [07.12.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int64 SPVGetZonID( SPV_NODE * node )
{
	unsigned __int64    ZonID_64  = 0   ;
  SPV_ZONE          * zone      = NULL;

  if ( SPVValidNode( node ) )
  {
    zone = (SPV_ZONE*)node->Zone;
    if ( SPVValidZone( zone ) )
    {
      try
      {
        ZonID_64 = zone->ZonID;
      }
      catch(...)
      {
        ZonID_64 = 0;
      }
    }
  }

  return ZonID_64;
}

//==============================================================================
/// Funzione per calcolare il codice unico (UniID) di una zona
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVGetZoneUniID( SPV_ZONE * zone )
{
  int                 ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  unsigned __int16    RegID_16      = 0   ;
  unsigned __int16  * RegID_16_ptr  = NULL;
  unsigned __int16  * ZonID_16_ptr  = NULL;
  SPV_REGION        * region        = NULL;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVGetZoneUniID");

  if ( SPVValidZone( zone ) )
  {
    region = (SPV_REGION*)zone->Region;
    if ( SPVValidRegion( region ) )
    {
      // --- Recupero il RegID (16bit) dal RegID (64bit) ---
      try
      {
        RegID_16_ptr = (unsigned __int16*) &region->RegID;
        RegID_16 = RegID_16_ptr[0];
      }
      catch(...)
      {
        RegID_16 = 0;
      }
      // --- Registro il RegID (16bit) nel ZonID (64bit) ---
      try
      {
        ZonID_16_ptr = (unsigned __int16*) &zone->ZonID;
        ZonID_16_ptr[0] = RegID_16;
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
      }
    }
	else
    {
      ret_code = SPV_SYSTEM_INVALID_REGION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_ZONE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per generare una stringa del codice unico (UniID) di una zona
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
char * SPVGetZoneUniIDStr( SPV_ZONE * zone )
{
  char              * UniID_x3_value  = NULL;
	unsigned __int32    SrvID_32        = 0   ;
	char              * SrvID_32_value  = NULL;
  unsigned __int16    RegID_16        = 0   ;
  char              * RegID_16_value  = NULL;
  unsigned __int16    ZonID_16        = 0   ;
  char              * ZonID_16_value  = NULL;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVGetZoneUniIDStr");

	if ( SPVValidZone( zone ) )
  {
		// --- Recupero il SrvID (32bit) della regione ---
	try
		{
			SrvID_32 = SPVGetSrvID( zone );
			SrvID_32_value = XMLType2ASCII( (void*)&SrvID_32, t_u_int_32 ); // -MALLOC
    }
    catch(...)
    {
			SrvID_32_value = NULL;
    }
    // --- Recupero il RegID (16bit) dal ZonID (64bit) ---
    try
    {
      RegID_16 = ((unsigned __int16*)&zone->ZonID)[0];
      RegID_16_value = XMLType2ASCII( (void*)&RegID_16, t_u_int_16 ); // -MALLOC
    }
    catch(...)
    {
      RegID_16_value = NULL;
    }
    // --- Recupero il ZonID (16bit) dal ZonID (64bit) ---
    try
    {
      ZonID_16 = ((unsigned __int16*)&zone->ZonID)[1];
      ZonID_16_value = XMLType2ASCII( (void*)&ZonID_16, t_u_int_16 ); // -MALLOC
    }
    catch(...)
    {
      ZonID_16_value = NULL;
    }

    UniID_x3_value = XMLAddToString( UniID_x3_value, SrvID_32_value );
    UniID_x3_value = XMLAddToString( UniID_x3_value, "." );
    UniID_x3_value = XMLAddToString( UniID_x3_value, RegID_16_value );
    UniID_x3_value = XMLAddToString( UniID_x3_value, "." );
		UniID_x3_value = XMLAddToString( UniID_x3_value, ZonID_16_value );

		if ( SrvID_32_value != NULL ) free( SrvID_32_value );
    if ( RegID_16_value != NULL ) free( RegID_16_value );
    if ( ZonID_16_value != NULL ) free( ZonID_16_value );
	}

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return UniID_x3_value;
}

//==============================================================================
/// Funzione per ottenere il RegID di una zona
///
/// \date [13.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
unsigned __int64 SPVGetRegID( SPV_ZONE * zone )
{
  unsigned __int64    RegID_64  = 0   ;
  SPV_REGION        * region    = NULL;

  if ( SPVValidZone( zone ) )
  {
    region = (SPV_REGION*)zone->Region;
    if ( SPVValidRegion( region ) )
    {
      try
      {
        RegID_64 = region->RegID;
      }
      catch(...)
      {
        RegID_64 = 0;
      }
    }
  }

  return RegID_64;
}

//==============================================================================
/// Funzione per generare una stringa del codice unico (UniID) di una regione
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
char * SPVGetRegionUniIDStr( SPV_REGION * region )
{
	char              * UniID_x2_value  = NULL;
	unsigned __int32    SrvID_32        = 0   ;
	char              * SrvID_32_value  = NULL;
	unsigned __int16    RegID_16        = 0   ;
	char              * RegID_16_value  = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVGetRegionUniIDStr");

	if ( SPVValidRegion( region ) )
	{
		// --- Recupero il SrvID (16bit) della regione ---
		SrvID_32 = SPVGetSrvID( region );
		SrvID_32_value = XMLType2ASCII( (void*)&SrvID_32, t_u_int_32 ); // -MALLOC
		// --- Recupero il RegID (16bit) dal RegID (64bit) ---
		RegID_16 = ((unsigned __int16*)&region->RegID)[0];
		RegID_16_value = XMLType2ASCII( (void*)&RegID_16, t_u_int_16 ); // -MALLOC

		UniID_x2_value = XMLAddToString( UniID_x2_value, SrvID_32_value );
		UniID_x2_value = XMLAddToString( UniID_x2_value, "." );
		UniID_x2_value = XMLAddToString( UniID_x2_value, RegID_16_value );

		if ( SrvID_32_value != NULL ) free( SrvID_32_value ); // -FREE
		if ( RegID_16_value != NULL ) free( RegID_16_value ); // -FREE
	}

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return UniID_x2_value;
}

//==============================================================================
/// Funzione per caricare la definizione di una periferica
///
/// \date [26.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.24
//------------------------------------------------------------------------------
int SPVLoadDevice( SPV_DEVICE * device, XML_ELEMENT * e_device )
{
	int                 ret_code            = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
	char              * inc_name            = NULL; // Ptr di appoggio
	char              * type                = NULL; // Codice del tipo periferica
	char                type_file_name[1024]      ; // Nome del file XML del tipo periferica
	SPV_DEVICE_TYPE   * device_type         = NULL; // Puntatore alla struttura di tipo periferica
	FILE              * device_type_file    = NULL; // Handler del file XML del tipo periferica
	XML_ELEMENT       * e_type              = NULL; // Elemento XML per il tipo periferica
	XML_ELEMENT       * e_comprot 		  = NULL;
	XML_ATTRIBUTE     * attrib              = NULL; // Attributo XML di appoggio
	XML_ENTITY        * ent_device          = NULL; // Ptr a entit� XML periferica
	int                 ent_level           = -1  ; // Livello dell'entit� XML periferica
	XML_ENTITY        * new_entity          = NULL; // Ptr di appoggio per entit� XML
	XML_ENTITY        * old_entity          = NULL; // Ptr di appoggio per entit� XML
	unsigned __int16    DevID_16            = 0   ; //
	unsigned __int16  * DevID_16_ptr        = NULL; //
	char              * def_file_name       = NULL; // Nome del file di definizione periferica
	char              * def_path            = NULL; // Percorso delle definizioni periferica
	char              * test_file_name      = NULL; // Percorso e nome file di definizione
	char              * addr_buffer         = NULL;
	char              * addr_token_0        = NULL;
	char              * addr_token_1        = NULL;
	char              * addr_token_2        = NULL;
	char              * addr_token_3        = NULL;
	unsigned __int8   * value_8             = NULL;
	unsigned __int8   * addr_ptr            = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVLoadDevice");

	if ( device != NULL ) // Controllo la validit� del ptr a struttura periferica
	{
		if ( e_device != NULL ) // Controllo la validit� dell'elemento XML di definizione
		{
			device->VCC               = SPV_SYSTEM_DEVICE_VALIDITY_CHECK_CODE;
			device->ID                = XMLGetValueInt  ( e_device, "ID"   );
			device->DevID             = 0x0000000000000000;
			device->SN                = XMLExtractValue ( e_device, "SN"   );
			device->Name              = XMLExtractValue ( e_device, "name" );
			// --- Recupero il tipo di periferica ---
			type                      = XMLGetValue     ( e_device, "type" );
			device_type               = SPVGetDeviceType( type );
			if ( device_type != NULL ) // Se il tipo � gi� stato caricato
			{
				device->Type = device_type;
			}
			else
			{
				device->Type = NULL;
			}
			addr_buffer               = XMLGetValue     ( e_device, "addr" );
			if ( addr_buffer != NULL )
			{
				addr_token_0 = XMLGetToken( addr_buffer, ".", 0 );
				addr_token_1 = XMLGetToken( addr_buffer, ".", 1 );
				addr_token_2 = XMLGetToken( addr_buffer, ".", 2 );
				addr_token_3 = XMLGetToken( addr_buffer, ".", 3 );
				// --- Caso indirizzo 1 ottetto esadecimale ---
				if ( addr_token_1 == NULL )
				{
					value_8 = (unsigned __int8*) XMLASCII2Type( addr_token_0, t_u_hex_8 );
					if ( value_8 != NULL )
					{
						device->SupCfg.Address = (int) *value_8;
						free( value_8 );
					}
					if ( addr_token_0 != NULL ) free( addr_token_0 );
				}
				// --- Caso indirizzo 4 ottetti decimali ---
				else
				{
					addr_ptr = (unsigned __int8*) &device->SupCfg.Address;
					value_8 = (unsigned __int8*) XMLASCII2Type( addr_token_0, t_u_int_8 );
					if ( value_8 != NULL )
					{
						addr_ptr[3] = (unsigned __int8) *value_8;
						free( value_8 );
					}
					value_8 = (unsigned __int8*) XMLASCII2Type( addr_token_1, t_u_int_8 );
					if ( value_8 != NULL )
					{
						addr_ptr[2] = (unsigned __int8) *value_8;
						free( value_8 );
					}
					value_8 = (unsigned __int8*) XMLASCII2Type( addr_token_2, t_u_int_8 );
					if ( value_8 != NULL )
					{
						addr_ptr[1] = (unsigned __int8) *value_8;
						free( value_8 );
					}
					value_8 = (unsigned __int8*) XMLASCII2Type( addr_token_3, t_u_int_8 );
					if ( value_8 != NULL )
					{
						addr_ptr[0] = (unsigned __int8) *value_8;
						free( value_8 );
					}
					if ( addr_token_0 != NULL ) free( addr_token_0 );
					if ( addr_token_1 != NULL ) free( addr_token_1 );
					if ( addr_token_2 != NULL ) free( addr_token_2 );
					if ( addr_token_3 != NULL ) free( addr_token_3 );
				}
			}
			//device->SupCfg.Address      = XMLGetValueInt  ( e_device, "addr" );
			device->SupCfg.PortID       = XMLGetValueInt  ( e_device, "port" );
			device->SupStatus.Active    = XMLGetValueBool ( e_device, "active" );
			device->SupStatus.Scheduled = XMLGetValueBool ( e_device, "scheduled" );
			// --- VALORI TEMPORANEI ---
			device->SupStatus.Level     = 255     ; // All'inizio la severita' e' sconosciuta
			device->SupStatus.Unknown   = true    ;
			device->SupStatus.Warning   = false   ;
			device->SupStatus.Alarm     = false   ;
			device->SupStatus.Offline   = false   ;
			device->SupStatus.CommErrors	= 0		;
			device->GenStatus           = NULL    ;
			device->GenCfg              = NULL    ;
			device->Node                = NULL    ;
			device->CardList            = NULL    ;
			device->CardCount           = 0       ;
			device->StreamList          = NULL    ;
			device->StreamCount         = 0       ;
			device->ProfileID           = XMLGetValueInt( e_device, "profile" ); // ID profilo nullo := nessun profilo
			device->Def                 = e_device;
			device->PopUpMenu           = NULL    ; // Il menu pop-up inizialmente non c'�
			device->Tag                 = NULL    ;
			// --- Registro la parte a 16 bit del DevID ---
			DevID_16 = (unsigned __int16) XMLGetValueInt( e_device, "DevID" );
			DevID_16_ptr = (unsigned __int16*) &device->DevID;
			DevID_16_ptr[3] = DevID_16;

			// --- Inserisco nella struttura un riferimento per l'inclusione della definizione XML della periferica ---
			if ( type != NULL ) // Se � specificato il tipo di periferica
			{
				// --- Recupera il percorso delle definizioni periferica ---
				def_path = SPVConfigInfo.Path.XMLDefinitionsPath;
				// --- Imposto il percorso di default per le definizioni periferiche ---
				if ( def_path != NULL ){
					XMLSetDefaultPath( def_path );
				}
				else{
					def_path = SPVGetPath( SPV_CFG_XML_DEFINITIONS_DEFAULT );
					XMLSetDefaultPath( def_path );
				}
				// --- Genero il nome del file di definizione periferica ---
				def_file_name = XMLStrCat( NULL, type );
				def_file_name = XMLStrCat( def_file_name, "d.xml" );
				// --- Salvo il nome file della definizione periferica ---
				if ( def_file_name != NULL )
				{
					strcpy( &type_file_name[0], def_file_name );
					test_file_name = XMLStrCat( NULL, XMLGetDefaultPath() );
					test_file_name = XMLStrCat( test_file_name, def_file_name );
					free( def_file_name );
				}
				else
				{
					type_file_name[0] = 0;
				}
				// --- Apro il file di definizione periferica ---
				if ( test_file_name != NULL )
				{
					try
					{
						device_type_file = fopen( test_file_name, "r" );
					}
					catch(...)
					{
						device_type_file = NULL;
					}
					try
					{
						free( test_file_name );
					}
					catch(...)
					{
					}
				}
				else
				{
					device_type_file = NULL;
				}
				if ( device_type_file != NULL ) // Se il file esiste
				{
					fclose( device_type_file ); // Chiudo il file
					ent_device = XMLGetEntity( SPVSystemSource->first_entity, (void *)e_device );
					if ( ent_device != NULL )
					{
						ent_level   = ent_device->level;
						// --- Creo un nuovo elemento <include> per l'inclusione della definizione periferica ---
						old_entity  = ent_device;
						inc_name    = (char*)malloc(sizeof(char)*8); // -MALLOC
						strcpy( inc_name, "include" );
						e_type      = XMLCreateElement( inc_name, NULL, NULL );
						new_entity  = XMLCreateEntity ( NULL, NULL, (void *)e_type, XML_ENTITY_ELEMENT, 0 );
						new_entity->level = ent_level+1;
						ret_code    = XMLAddEntity    ( old_entity, new_entity );
						// --- Creo un attributo id="0" ---
						old_entity  = new_entity;
						attrib      = XMLNewAttrib    ( e_type, "id", "0" );
						ret_code    = XMLAddAttrib    ( e_type, attrib );
						new_entity  = XMLCreateEntity ( NULL, NULL, (void *)attrib, XML_ENTITY_ATTRIBUTE, 0 );
						new_entity->level = ent_level+2;
						ret_code    = XMLAddEntity    ( old_entity, new_entity );
						// -- Creo un attributo type="xml" ---
						old_entity  = new_entity;
						attrib      = XMLNewAttrib( e_type, "type", "xml" );
						ret_code    = XMLAddAttrib( e_type, attrib );
						new_entity  = XMLCreateEntity( NULL, NULL, (void *)attrib, XML_ENTITY_ATTRIBUTE, 1 );
						new_entity->level = ent_level+2;
						ret_code    = XMLAddEntity( old_entity, new_entity );
						// --- Creo un attributo name="type" ---
						old_entity  = new_entity;
						attrib      = XMLNewAttrib( e_type, "name", "type" );
						ret_code    = XMLAddAttrib( e_type, attrib );
						new_entity  = XMLCreateEntity( NULL, NULL, (void *)attrib, XML_ENTITY_ATTRIBUTE, 2 );
						new_entity->level = ent_level+2;
						ret_code    = XMLAddEntity( old_entity, new_entity );
						// --- Creo un attributo src=<type_file_name> ---
						old_entity  = new_entity;
						attrib      = XMLNewAttrib( e_type, "src", type_file_name );
						ret_code    = XMLAddAttrib( e_type, attrib );
						new_entity  = XMLCreateEntity( NULL, NULL, (void *)attrib, XML_ENTITY_ATTRIBUTE, 3 );
						new_entity->level = ent_level+2;
						ret_code    = XMLAddEntity( old_entity, new_entity );
						XMLAddChild( e_device, e_type );
						// --- Effettuo l'inclusione del file XML della definizione tipo periferica ---
						while ( ret_code == SPV_SYSTEM_NO_ERROR && XMLGetFirst( SPVSystemSource, "include" ) != NULL )
						{
							ret_code = XMLProcInc( SPVSystemSource ); // Processo l'inclusione del file XML
						}
						device->DefinitionVersion = XMLExtractValue ( e_device->child, "version" );
						e_comprot = XMLGetNext( e_device->child->child, "comprot", -1 );
						device->ProtocolVersion = XMLExtractValue ( e_comprot, "version" );
						if ( ret_code == 0 && device->Type == NULL ) // Se il tipo periferica non � stato gi� caricato prima
						{
							ret_code = SPVLoadDeviceType( e_device->child );
							if ( ret_code == SPV_SYSTEM_NO_ERROR )
							{
								device->Type = SPVGetDeviceType( type );
							}
						}
					}
					else
					{
						ret_code = SPV_SYSTEM_INVALID_DEFINITION;
					}
				}
				else
				{
					ret_code = SPV_SYSTEM_INVALID_FILE;
				}
			}
			else
			{
				ret_code = SPV_SYSTEM_INVALID_DEFINITION;
			}
		}
		else
		{
			ret_code = SPV_SYSTEM_INVALID_ELEMENT;
		}
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_DEVICE;
	}

	if ( ret_code == 0 )
	{
		//SPVReportSystemEvent( (void*)&SPVSystem, 0, 333, "Device definition loaded correctly.", type );
	}
	else
	{
		unsigned __int16 ier = (unsigned __int16) ret_code;
		char * err = XMLType2ASCII( &ier, t_u_int_16 );
		char * msg = XMLStrCat( NULL, "Device definition not loaded. - Error #" );
		msg = XMLStrCat( msg, err );
		SPVReportSystemEvent( (void*)&SPVSystem, 2, 693, msg, type );
		free( msg );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare una struttura periferica
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVFreeDevice( SPV_DEVICE * device )
{
	int ret_code = SPV_SYSTEM_NO_ERROR;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVFreeDevice");

	if ( SPVValidDevice( device ) )
	{
		// --- Cancello il serial number della periferica ---
		if ( device->SN != NULL )
		{
			try
			{
				free( device->SN ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello il nome della periferica ---
		if ( device->Name != NULL )
		{
			try
			{
				free( device->Name ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello la struttura del tipo periferica ---
		if ( device->Type != NULL )
		{
			ret_code = SPVFreeDeviceType( device->Type );
		}
		// --- Cancello la descrizione dello stato ---
		if ( device->SupStatus.Description != NULL )
		{
			try
			{
				free( device->SupStatus.Description ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello la versione di definizione della periferica ---
		if ( device->DefinitionVersion != NULL )
		{
			try
			{
				free( device->DefinitionVersion ); // -FREE
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello la versione del protocollo della periferica ---
		if ( device->ProtocolVersion != NULL )
		{
			try
			{
				free( device->ProtocolVersion );
			}
			catch(...)
			{
				ret_code = SPV_COMPROT_CRITICAL_ERROR;
				/* DONE -oEnrico -cError : Gestione dell'errore critico */
			}
		}
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_DEVICE;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [13.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidDevice( SPV_DEVICE * device )
{
  bool valid = false;

  if ( device != NULL )
  {
    try
    {
	  if ( device->VCC == SPV_SYSTEM_DEVICE_VALIDITY_CHECK_CODE )
      {
		valid = true;
      }
    }
    catch(...)
	{
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per caricare la definizione di un nodo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [26.08.2014]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.05
//------------------------------------------------------------------------------
int SPVLoadNode( SPV_NODE * node, XML_ELEMENT * e_node, SPV_ZONE * zone )
{
	int                 ret_code      	= SPV_SYSTEM_NO_ERROR; // Codice di ritorno
	SPV_DEVICE        * device_list   	= NULL	; // Ptr a lista di periferiche
	int                 device_count  	= 0   	; // Numero di periferiche
	XML_ELEMENT       * e_device      	= NULL	; // Ptr a elemento XML per periferiche
	unsigned __int16    NodID_16      	= 0   	; //
	unsigned __int16  * NodID_16_ptr  	= NULL	; //
	int					device_index	= 0		; // Indice per le periferiche
	int					supervisor_id	= 0		;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVLoadNode");

	if ( node != NULL ) // Verifico la validit� del ptr a struttura del nodo
	{
		if ( e_node != NULL ) // Verifico la validit� del ptr a elemento XML di definizione
		{
			node->VCC			= SPV_SYSTEM_NODE_VALIDITY_CHECK_CODE;
			node->ID			= XMLGetValueInt  ( e_node, "ID"    );
			node->NodID			= 0x000000000000FFFF;
			node->Code			= XMLExtractValue ( e_node, "code"  );
			node->Name			= XMLExtractValue ( e_node, "name"  );
			node->Local			= XMLGetValueBool( e_node, "local" );
			node->Zone			= zone;
			node->DeviceList	= NULL; // Valore temporaneo!
			node->DeviceCount	= NULL; // Valore temporaneo!
			node->Def			= e_node;
			node->Tag			= NULL;
			node->Level			= 0;

			// --- Registro la parte a 16 bit del NodID ---
			NodID_16 = (unsigned __int16) XMLGetValueInt( e_node, "NodID" );
			NodID_16_ptr = (unsigned __int16*) &node->NodID;
			NodID_16_ptr[2] = NodID_16;

			device_count = XMLGetChildCount( e_node, false ); // Conto le periferiche
			if ( device_count > 0 ) // Se ho trovato almeno una periferica
			{
				// --- Alloco e inizializzo la memoria per le periferiche ---
				device_list = (SPV_DEVICE*) malloc( sizeof(SPV_DEVICE) * device_count ); // -MALLOC
				memset( device_list, 0, sizeof(SPV_DEVICE) * device_count );
				e_device    = e_node->child; // Prendo il primo child
				// --- Ciclo sulle periferiche ---
				for ( int d = 0; d < device_count; d++ )
				{
					supervisor_id = XMLGetValueInt( e_device, "supervisor_id" );
					if ( supervisor_id == 0 ) // La diagnostica della periferica e' assegnata a questo supervisore
					{
						ret_code = SPVLoadDevice( &device_list[device_index], e_device );
						device_list[device_index].Node = (void*)node; // Assegno questo nodo alla periferica d-esima
						device_list[device_index].Owned = true;
						device_index++;
						SPVOwnedDeviceCount++; // Incremento il contatore delle periferiche da diagnosticare
					}
					else // Altrimenti la periferica viene ignorata
					{
						/* SPVSTD-4
						ret_code = SPVLoadDevice( &device_list[device_index], e_device );
						device_list[device_index].Node = (void*)node; // Assegno questo nodo alla periferica d-esima
						device_list[device_index].Owned = false;
						device_list[device_index].SupStatus.Active = false;
						device_list[device_index].SupStatus.Scheduled = false;
						device_index++;
						*/
					}
					if ( e_device != NULL ) e_device = e_device->next; // Prendo l'elemento XML successivo
				}
				// --- Alloco e inizializzo la memoria definitiva per le periferiche ---
				node->DeviceList = (SPV_DEVICE*) malloc( sizeof(SPV_DEVICE) * device_index ); // -MALLOC
				memcpy(node->DeviceList,device_list,sizeof(SPV_DEVICE) * device_index);
				node->DeviceCount = device_index;
				free(device_list); // -FREE
			}
			else
			{
				// Il nodo non contiene periferiche
			}
		}
		else
		{
			ret_code = SPV_SYSTEM_INVALID_ELEMENT;
		}
    }
    else
	{
		ret_code = SPV_SYSTEM_INVALID_NODE;
	}

    // --- Esco dalla sezione critica ---
    SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare una struttura nodo
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVFreeNode( SPV_NODE * node )
{
  int ret_code = SPV_SYSTEM_NO_ERROR;
  SPV_DEVICE  * device_list   = NULL; // Ptr a lista di periferiche
  int           device_count  = 0   ; // Numero di periferiche

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVFreeNode");

  if ( SPVValidNode( node ) )
  {
    // --- Recupero la lista delle periferiche ---
    device_list   = node->DeviceList ;
    device_count  = node->DeviceCount;
    // --- Ciclo sulle periferiche ---
    for ( int d = 0; d < device_count; d++ )
    {
	  ret_code = SPVFreeDevice( &device_list[d] );
    }
    // --- Cancello la lista delle periferiche ---
    if ( device_list != NULL )
	{
      memset( device_list, 0, sizeof(SPV_DEVICE) * device_count );
      try
      {
        free( device_list ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Cancello il codice del nodo ---
    if ( node->Code != NULL )
    {
      try
      {
        free( node->Code ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Cancello il nome del nodo ---
    if ( node->Name != NULL )
    {
      try
      {
        free( node->Name ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Resetto la struttura nodo ---
	memset( node, 0, sizeof( SPV_NODE ) ) ;
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_NODE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di un nodo
///
/// \date [13.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidNode( SPV_NODE * node )
{
  bool valid = false;

  if ( node != NULL )
  {
    try
    {
      if ( node->VCC == SPV_SYSTEM_NODE_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per caricare la definizione di una zona
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLoadZone( SPV_ZONE * zone, XML_ELEMENT * e_zone, SPV_REGION * region )
{
  int           		ret_code    	= SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  SPV_NODE *			node_list   	= NULL; // Ptr a lista di nodi della zona
  int           		node_count  	= 0   ; // Numero di nodi
  XML_ELEMENT *			e_node      	= NULL; // Ptr a elemento XML per i nodi
  unsigned __int16		ZonID_16        = 0   ; //
  unsigned __int16 *	ZonID_16_ptr    = NULL; //

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVLoadZone");

  if ( zone != NULL ) // Controllo la validit� del ptr alla struttura zona
  {
    if ( e_zone != NULL ) // Controllo la validit� del ptr a elemento XML
    {
      zone->VCC       = SPV_SYSTEM_ZONE_VALIDITY_CHECK_CODE;
      zone->ID        = XMLGetValueInt  ( e_zone, "ID"    );
      zone->ZonID     = 0x00000000FFFFFFFF;
      zone->Code      = XMLExtractValue ( e_zone, "code"  );
      zone->Name      = XMLExtractValue ( e_zone, "name"  );
      zone->Region    = region  ;
      zone->NodeList  = NULL    ; // Valore temporaneo!
      zone->NodeCount = NULL    ; // Valore temporaneo!
      zone->Def       = e_zone  ;
      zone->Level     = 0       ;
      zone->Tag       = NULL    ;

      // --- Registro la parte a 16 bit del ZonID ---
      ZonID_16 = (unsigned __int16) XMLGetValueInt( e_zone, "ZonID" );
	  ZonID_16_ptr = (unsigned __int16*) &zone->ZonID;
      ZonID_16_ptr[1] = ZonID_16;

      node_count = XMLGetChildCount( e_zone, false ); // Conto i nodi
      if ( node_count > 0 ) // Se ho trovato almeno un nodo
      {
        // --- Alloco e inizializzo la memoria per i nodi ---
        node_list = (SPV_NODE*)malloc( sizeof(SPV_NODE) * node_count ); // -MALLOC
        memset( node_list, 0, sizeof(SPV_NODE) * node_count );
        e_node    = e_zone->child; // Prendo il primo child
        // --- Ciclo sui nodi ---
        for ( int n = 0; n < node_count; n++ )
        {
		  ret_code = SPVLoadNode( &node_list[n], e_node, zone );
		  ///node_list[n].Zone = (void*)zone; // Assegno questa zona al nodo n-esimo
          if ( e_node != NULL ) e_node = e_node->next; // Prendo l'elemento XML successivo
        }
        zone->NodeList  = node_list;
        zone->NodeCount = node_count;
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_DEFINITION;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_ELEMENT;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_ZONE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una struttura zona
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVFreeZone( SPV_ZONE * zone )
{
  int           ret_code    = SPV_SYSTEM_NO_ERROR;
  SPV_NODE    * node_list   = NULL; // Ptr a lista di nodi della zona
  int           node_count  = 0   ; // Numero di nodi

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVFreeZone");

  if ( SPVValidZone( zone ) )
  {
    // --- Recupero la lista dei nodi ---
    node_list  = zone->NodeList ;
    node_count = zone->NodeCount;
    // --- Ciclo sui nodi ---
    for ( int n = 0; n < node_count; n++ )
    {
	  ret_code = SPVFreeNode( &node_list[n] );
    }
    // --- Cancello la lista dei nodi ---
    if ( node_list != NULL )
    {
      memset( node_list, 0, sizeof(SPV_NODE) * node_count );
      try
      {
        free( node_list ); // -FREE
      }
      catch(...)
	  {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Cancello il codice della zona ---
    if ( zone->Code != NULL )
    {
      try
      {
        free( zone->Code ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Cancello il nome della zona ---
    if ( zone->Name != NULL )
    {
      try
      {
        free( zone->Name ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Resetto la struttura zona ---
    memset( zone, 0, sizeof( SPV_ZONE ) );
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_NODE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la valididta' di una zona
///
/// \date [13.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidZone( SPV_ZONE * zone )
{
  bool valid = false;

  if ( zone != NULL )
  {
    try
    {
      if ( zone->VCC == SPV_SYSTEM_ZONE_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per caricare la definizione di una regione
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVLoadRegion( SPV_REGION * region, XML_ELEMENT * e_region, SPV_SERVER * server )
{
  int                 ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  SPV_ZONE          * zone_list     = NULL; // Ptr a lista di zone gestite nella regione
  int                 zone_count    = 0   ; // Numero di zone
  XML_ELEMENT       * e_zone        = NULL; // Ptr a elemento XML per zone
  unsigned __int16    RegID_16      = 0   ;
  unsigned __int16  * RegID_16_ptr  = NULL;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVLoadRegion");

  if ( region != NULL ) // Verifico la validit� del ptr a struttura regione
  {
    if ( e_region != NULL ) // Verifco la validit� ptr a elemento XML di definizione
    {
      region->VCC       = SPV_SYSTEM_REGION_VALIDITY_CHECK_CODE;
      region->ID        = XMLGetValueInt  ( e_region, "ID"    );
      region->RegID     = 0x0000FFFFFFFFFFFF;
      region->Code      = XMLExtractValue ( e_region, "code"  );
      region->Name      = XMLExtractValue ( e_region, "name"  );
      region->Server    = server    ;
      region->ZoneList  = NULL      ; // Valore temporaneo!
      region->ZoneCount = NULL      ; // Valore temporaneo!
      region->Def       = e_region  ;
      region->Level     = 0         ;
      region->Tag       = NULL      ;

      // --- Registro la parte a 16 bit del RegID ---
      RegID_16 = (unsigned __int16) XMLGetValueInt( e_region, "RegID" );
      RegID_16_ptr = (unsigned __int16*) &region->RegID;
      RegID_16_ptr[0] = RegID_16;

      zone_count = XMLGetChildCount( e_region, false ); // Conto le zone
      if ( zone_count > 0 ) // Se ho trovato almeno una zona
      {
        // --- Alloco e inizializzo la memoria per le zone ---
		zone_list = (SPV_ZONE*) malloc( sizeof(SPV_ZONE) * zone_count ); // -MALLOC
        memset( zone_list, 0, sizeof(SPV_ZONE) * zone_count );
        // --- Ciclo sulle zone ---
        e_zone    = e_region->child; // Prendo il primo child
        for ( int z = 0; z < zone_count; z++ )
        {
		  ret_code = SPVLoadZone( &zone_list[z], e_zone, region );
          ///zone_list[z].Region = (void*)region; // Assegno questa regione alla zona z-esima
          if ( e_zone != NULL ) e_zone = e_zone->next; // Prendo l'elemento XML successivo
        }
        region->ZoneList  = zone_list;
        region->ZoneCount = zone_count;
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_DEFINITION;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_ELEMENT;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_REGION;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una regione
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [09.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidRegion( SPV_REGION * region )
{
  bool valid = false;

  if ( region != NULL )
  {
    try
    {
      if ( region->VCC == SPV_SYSTEM_REGION_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per eliminare una struttura regione
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVFreeRegion( SPV_REGION * region )
{
  int         ret_code    = SPV_SYSTEM_NO_ERROR;
  SPV_ZONE  * zone_list   = NULL; // Ptr a lista di zone gestite nella regione
  int         zone_count  = 0   ; // Numero di zone

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVFreeRegion");

  if ( SPVValidRegion( region ) )
  {
    // --- Recupero la lista delle zone ---
    zone_list  = region->ZoneList ;
    zone_count = region->ZoneCount;
    // --- Cancello la lista delle zone ---
    for ( int z = 0; z < zone_count; z++ )
    {
	  ret_code = SPVFreeZone( &zone_list[z] );
    }
    if ( zone_list != NULL )
    {
      memset( zone_list, 0, sizeof(SPV_ZONE) * zone_count );
      try
      {
        free( zone_list ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Cancello il codice della regione ---
    if ( region->Code != NULL )
    {
      try
      {
        free( region->Code ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
	}
    // --- Cancello il nome della regione ---
    if ( region->Name != NULL )
    {
      try
      {
        free( region->Name ); // -FREE
      }
      catch(...)
      {
        ret_code = SPV_SYSTEM_CRITICAL_ERROR;
        /* TODO -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Resetto la struttura regione ---
    memset( region, 0, sizeof( SPV_REGION ) );
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_REGION;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di un server.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
int SPVLoadServer( SPV_SERVER * server, XML_ELEMENT * e_server )
{
	int           ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
	SPV_REGION  * region_list   = NULL; // Ptr a lista di regioni
	int           region_count  = 0   ; // Numero di regioni gestite dal server
	XML_ELEMENT * e_region      = NULL; // Ptr a elemento XML per regioni

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVLoadServer");

	if ( server != NULL ) // Controllo la validit� del ptr a server
	{
		if ( e_server != NULL ) // Controllo la validit� del ptr a elemento XML
		{
			server->VCC         = SPV_SYSTEM_SERVER_VALIDITY_CHECK_CODE;
			server->ID          = XMLGetValueInt  ( e_server, "ID"    );
			server->SrvID       = (unsigned __int32)XMLGetValueUInt32  ( e_server, "SrvID" ); // Valore UInt32
			server->NodID		= 0; // Nessun nodo locale (viene valorizzato in seguito)
			//server->Host        = XMLExtractValue ( e_server, "host"  );  sostituito con la chiamata di sistema sottostante
			server->Host        = SYSGetHostName();
			server->Name        = XMLExtractValue ( e_server, "name"  );
			server->Def         = e_server  ;
			server->System      = NULL      ;
			server->RegionList  = NULL      ; // Valore temporaneo!
			server->RegionCount = NULL      ; // Valore temporaneo!
			server->Level       = 0         ;
			server->Tag         = NULL      ;

			region_count = XMLGetChildCount( e_server, false ); // Conto le regioni
			if ( region_count > 0 ) // Se ho trovato almeno una regione
			{
				// --- Alloco e inizializzo la memoria per le regoni ---
				region_list = (SPV_REGION*) malloc( sizeof(SPV_REGION) * region_count ); // -MALLOC
				memset( region_list, 0, sizeof(SPV_REGION) * region_count );
				e_region    = e_server->child; // Prendo il primo child
				// --- Ciclo sulle regioni ---
				for ( int r = 0; r < region_count; r++ )
				{
					ret_code = SPVLoadRegion( &region_list[r], e_region, server );
					///region_list[r].Server = (void*)server; // Assegno questo server alla regione r-esima
					if ( e_region != NULL ) e_region = e_region->next; // Prendo l'elemento XML successivo
				}
				server->RegionList  = region_list;
				server->RegionCount = region_count;
			}
			else
			{
				ret_code = SPV_SYSTEM_INVALID_DEFINITION;
			}
		}
		else
		{
			ret_code = SPV_SYSTEM_INVALID_ELEMENT;
		}
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_SERVER;
	}

    // --- Esco dalla sezione critica ---
    SPVLeaveSystemData( );

    return ret_code;
}

//==============================================================================
/// Funzione per eliminare la struttura di un server
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVFreeServer( SPV_SERVER * server )
{
	int           ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
    SPV_REGION  * region_list   = NULL; // Ptr a lista di regioni
    int           region_count  = 0   ; // Numero di regioni gestite dal server

    // --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVFreeServer");

    if ( SPVValidServer( server ) )
    {
		// --- Cancello la lista delle regioni ---
		region_list  = server->RegionList ;
		region_count = server->RegionCount;
		// --- Ciclo sulle regioni ---
		for ( int r = 0; r < region_count; r++ )
		{
			ret_code = SPVFreeRegion( &region_list[r] );
        }
        if ( region_list != NULL )
		{
			memset( region_list, 0, sizeof(SPV_REGION) * region_count );
			try
			{
				free( region_list ); // -FREE
			}
            catch(...)
            {
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
                /* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello il codice del server ---
		if ( server->Host != NULL )
		{
			try
			{
				free( server->Host );
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
				/* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
		// --- Cancello il nome del server ---
		if ( server->Name != NULL )
		{
            try
			{
				free( server->Name );
			}
			catch(...)
			{
				ret_code = SPV_SYSTEM_CRITICAL_ERROR;
                /* TODO -oEnrico -cError : Gestione dell'errore critico */
			}
		}
        // --- Resetto la struttura server ---
		memset( server, 0, sizeof( SPV_SERVER ) );
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_SERVER;
	}

	// --- Esco dalla sezione critica ---
    SPVLeaveSystemData( );

    return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di un server
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [13.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidServer( SPV_SERVER * server )
{
	bool valid = false;

	if ( server != NULL )
	{
		try
		{
		    if ( server->VCC == SPV_SYSTEM_SERVER_VALIDITY_CHECK_CODE )
		    {
		    	valid = true;
		    }
    	}
    	catch(...)
    	{
    	}
    }

    return valid;
}

//==============================================================================
/// Funzione per caricare la struttura del sistema.
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [26.08.2014]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.17
//------------------------------------------------------------------------------
int SPVLoadSystem( SPV_SYSTEM * system, XML_ELEMENT * e_system, XML_ELEMENT * e_topography, bool multi_language )
{
	int           		ret_code      	= SPV_SYSTEM_NO_ERROR; // Codice di ritorno
	SPV_SERVER *		server_list   	= NULL; // Lista dei server
	int           		server_count  	= 0   ; // Numero di server
	XML_ELEMENT *		e_server     	= NULL; // Elemento XML per server
	SPV_SERVER * 		server			= NULL;
	unsigned __int32	SrvID			= 0;
	char *          	SrvID_value		= NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSystemData("SPVLoadSystem");

	if ( system != NULL ) // Verifico la validit� del ptr a struttura sistema
	{
		if ( e_system != NULL ) // Verifico la validit� del ptr a elemento XML di definizione
		{
			system->ID          = XMLGetValueInt  ( e_system, "ID"    );
			system->Code        = XMLExtractValue ( e_system, "code"  );
			system->Name        = XMLExtractValue ( e_system, "name"  );
			system->MultiLangauge	= multi_language;
			system->ServerList  = NULL; // Valore temporaneo!
			system->ServerCount = 0;    // Valore temporaneo!
			system->DBConn      = NULL;
			system->Def         = e_system;
			system->Source      = SPVSystemSource;
			system->Tag         = NULL;
			system->Level       = 0;

			// --- Caricamento DB ---
			ret_code = SPVLoadDatabases( system );
			if ( ret_code == 0 )
			{
				//ret_code = SPVCheckTables( system );
			}
			// --- Caricamento topografia ---
			SPVSystemTopography = SPVLoadTopography( e_topography );
			// --- Rimuovo la topografia precedente ---
			//SPVSTD-3// SPVDBRemoveTopography( );
			// --- Salvo nel DB la topografia ---
			//SPVSTD-3// SPVDBSaveTopography( );
			// --- Caricamento Sistema ---
			server_count = XMLGetChildCount( e_system, false ); // Conto i server
			if ( server_count > 0 ) // Se ho trovato almeno un server
			{
				// --- Alloco e inizializzo la memoria per i server ---
				server_list = (SPV_SERVER*)malloc( sizeof(SPV_SERVER) * server_count ); // -MALLOC
				memset( server_list, 0, sizeof(SPV_SERVER) * server_count );
				e_server    = e_system->child; // Prendo il primo child
				// --- Ciclo sui server ---
				for ( int s = 0; s < server_count; s++ )
				{
					ret_code = SPVLoadServer( &server_list[s], e_server );
					server_list[s].System = (void*)system; // Assegno questo sistema al server s-esimo
					if ( e_server != NULL ) e_server = e_server->next; // Prendo l'elemento XML successivo
				}
				system->ServerList  = server_list;
				system->ServerCount = server_count;
				// --- Recupero SrvID dal primo server ---
				try
				{
					server = &server_list[0];
					if ( server != NULL ) {
						SrvID		= (unsigned __int32) server->SrvID;
						SrvID_value = XMLType2ASCII( &SrvID, t_u_int_32 ); // -MALLOC
					}
				}
				catch(...)
				{}
				// --- Aggiorno il DB per i server ---
				//SPVSTD-3// SPVDoOnServers( system, SPVDBSaveServer );
				// --- Marchio la tabella regions Removed=1 ---
				//SPVSTD-3// SPVDBRemoveAllRegions( );
				// --- Aggiorno il DB per tutte le regioni ---
				//SPVSTD-3// SPVDoOnRegions( system, SPVDBSaveRegion );
				// --- Calcolo l'ID unico per tutte le zone ---
				SPVDoOnZones( system, SPVGetZoneUniID );
				// --- Marchio la tabella zones Removed=1 ---
				//SPVSTD-3// SPVDBRemoveAllZones( );
				// --- Aggiorno il DB per tutte le zone ---
				//SPVSTD-3// SPVDoOnZones( system, SPVDBSaveZone );
				// --- Calcolo l'ID unico per tutti i nodi ---
				SPVDoOnNodes( system, SPVGetNodeUniID );
				// --- Marchio la tabella nodes Removed=1 ---
				//SPVSTD-3// SPVDBRemoveAllNodes( );
				// --- Aggiorno il DB per tutti i nodi ---
				//SPVSTD-3// SPVDoOnNodes( system, SPVDBSaveNode );

				// --- Marchio la tabella servers Removed=1 ---
				//SPVSTD-3// SPVDBRemoveAllServers( );
				// --- Aggiorno il DB per i server ---
				//SPVSTD-3// SPVDoOnServers( system, SPVDBSaveServer );

				// --- Marchio la tabella port Removed=1
				//SPVDBRemoveAllPorts( );
				// --- Salvo le porte di comunicazione ---
				//SPVDBSavePorts( SrvID_value );
				// --- Elimino le porte rimosse
				//SPVDBDeleteRemovedPorts( );
				// --- Calcolo l'ID unico per tutte le periferiche ---
				SPVDoOnDevices( system, SPVGetDeviceUniID );
				// --- Recupero i dati topografici delle periferiche ---
				SPVDoOnDevices( system, SPVSetDeviceTopography );
				// --- Marchio la tabella devices Removed=1 ---
				//SPVSTD-3// SPVDBRemoveAllDevices( );
				// --- Aggiorno la tabella dei Systems ---
				//SPVSTD-3// SPVDBSaveDeviceSystems( system );
				// --- Aggiorno la tabella dei Vendors ---
				//SPVSTD-3// SPVDBSaveDeviceVendors( system );
				// --- Aggiorno la tabella dei Device_type ---
				//SPVSTD-3// SPVDBSaveDeviceTypes( system );
				// --- Aggiorno il DB per tutte le periferiche ---
				//SPVSTD-3// SPVDoOnDevices( system, SPVDBSaveDevice );
				// --- Aggiorno il DB per tutte le periferiche (porta e versioni) ---
				SPVDoOnDevices( system, SPVDBUpdateDevice );
				try
				{
					if ( SrvID_value != NULL ) free( SrvID_value ); // -FREE
				}
				catch(...)
				{}
			}
			else
			{
				ret_code = SPV_SYSTEM_INVALID_DEFINITION;
			}
			// --- Elimino la topografia rimossa ---
			//SPVDBDeleteRemovedTopography( );
		}
		else
		{
		  ret_code = SPV_SYSTEM_INVALID_ELEMENT;
		}
	}
	else
	{
		ret_code = SPV_SYSTEM_INVALID_SYSTEM;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSystemData( );

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la struttura del sistema
///
/// \date [12.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVFreeSystem( SPV_SYSTEM * system )
{
	int           ret_code      = SPV_SYSTEM_NO_ERROR;
    SPV_SERVER  * server_list   = NULL; // Lista dei server
    int           server_count  = 0   ; // Numero di server

    // --- Entro nella sezione critica ---
    SPVEnterSystemData("SPVFreeSystem");

    if ( system != NULL )
    {
    	// --- Cancello la lista dei server (e le sottostrutture) ---
    	server_list  = system->ServerList ;
    	server_count = system->ServerCount;
    	// --- Ciclo sui server ---
    	for ( int s = 0; s < server_count; s++ )
    	{
			ret_code = SPVFreeServer( &server_list[s] );
		}
		if ( server_list != NULL )
		{
			memset( server_list, 0, sizeof(SPV_SERVER) * server_count );
			try
		    {
		    	free( server_list ); // -FREE
		    }
		    catch(...)
		    {
		    	ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		    	/* TODO -oEnrico -cError : Gestione dell'errore critico */
		    }
		}
		// --- Cancello la lista dei sistemi periferica ---
		SPVFreeDeviceSystemList( );
		// --- Cancello la lista dei vendor periferica ---
		SPVFreeDeviceVendorList( );
		// --- Cancello il codice del server ---
		if ( system->Code != NULL )
    	{
			try
		    {
		    	free( system->Code );
		    }
		    catch(...)
		    {
		    	ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		    	/* TODO -oEnrico -cError : Gestione dell'errore critico */
		    }
		}
		// --- Cancello il nome del server ---
		if ( system->Name != NULL )
		{
		    try
		    {
		    	free( system->Name );
		    }
		    catch(...)
		    {
		    	ret_code = SPV_SYSTEM_CRITICAL_ERROR;
		    	/* TODO -oEnrico -cError : Gestione dell'errore critico */
		    }
		}
		// --- Cancello le informazioni topografiche ---
		ret_code = SPVClearTopography( SPVSystemTopography );
		// --- Cancello le informazioni del database ---
		ret_code = SPVClearDatabases( system );
		// --- Cancello la struttura del sistema ---
		memset( system, 0, sizeof(SPV_SYSTEM) );
	}
    else
    {
    	ret_code = SPV_SYSTEM_INVALID_SYSTEM;
    }

    // --- Esco dalla sezione critica ---
    SPVLeaveSystemData( );

    return ret_code;
}

//==============================================================================
/// Funzione per creare una struttura di sistema.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLoadSystemSource( XML_SOURCE * system_source )
{
  int ret_code = SPV_SYSTEM_NO_ERROR; // Codice di ritorno

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVLoadSystemSource");

  if ( system_source != NULL )
  {
    if ( system_source->name != NULL )
    {
      ret_code = XMLOpen( system_source, NULL, 'R' );
      if ( ret_code == 0 )
      {
        ret_code = XMLRead( system_source, system_source->root_element );
        if ( ret_code == 0 )
        {

        }
        XMLClose( system_source );
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_FILE_NAME;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SOURCE;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per cancellare una struttura di sistema
///
/// \date [29.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeSystemSource( XML_SOURCE * system_source )
{
  int ret_code = SPV_SYSTEM_NO_ERROR; // Codice di ritorno

  if ( system_source != NULL )
  {
    ret_code = XMLFree( system_source );
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SOURCE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire funzioni sui server del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnServers( SPV_SYSTEM * system, SPV_SYSTEM_SERVER_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnServers");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
	if ( function != NULL ) // Controllo il ptr a funzione
	{
	  if ( system->ServerList != NULL && system->ServerCount > 0 )
	  {
		for ( int s = 0; s < system->ServerCount; s++ )
		{
		  server = &system->ServerList[s];
		  if ( server != NULL )
		  {
			ret_code = function( server );
		  }
		  else
		  {
			ret_code = SPV_SYSTEM_INVALID_SERVER;
		  }
		  if ( ret_code != 0 )
		  {
			s = system->ServerCount; // Fine del ciclo for
		  }
		}
	  }
	  else
	  {
		ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
	  }
	}
	else
	{
	  ret_code = SPV_SYSTEM_INVALID_SERVER_FUNCTION;
	}
  }
  else
  {
	ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire funzioni sulle regioni del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnRegions( SPV_SYSTEM * system, SPV_SYSTEM_REGION_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server
  SPV_REGION  * region    = NULL; // Pta ad una struttura regione

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnRegions");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
    if ( function != NULL ) // Controllo il ptr a funzione
    {
      if ( system->ServerList != NULL && system->ServerCount > 0 )
      {
        for ( int s = 0; s < system->ServerCount; s++ )
        {
          server = &system->ServerList[s];
          if ( server != NULL )
          {
            if ( server->RegionList != NULL && server->RegionCount > 0 )
            {
              for ( int r = 0; r < server->RegionCount; r++ )
              {
                region = &server->RegionList[r];
                if ( region != NULL )
                {
                  ret_code = function( region );
                }
                else
                {
                  ret_code = SPV_SYSTEM_INVALID_REGION;
                }
                if ( ret_code != 0 )
                {
                  r = server->RegionCount; // Fine del ciclo for
                }
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_REGION_LIST;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_INVALID_SERVER;
          }
          if ( ret_code != 0 )
          {
            s = system->ServerCount; // Fine del ciclo for
          }
        }
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_REGION_FUNCTION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire funzioni sulle zone del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnZones( SPV_SYSTEM * system, SPV_SYSTEM_ZONE_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server
  SPV_REGION  * region    = NULL; // Ptr ad una struttura regione
  SPV_ZONE    * zone      = NULL; // Ptr ad una struttura zona

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnZones");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
    if ( function != NULL ) // Controllo il ptr a funzione
    {
      if ( system->ServerList != NULL && system->ServerCount > 0 )
      {
        for ( int s = 0; s < system->ServerCount; s++ )
        {
          server = &system->ServerList[s];
          if ( server != NULL )
          {
            if ( server->RegionList != NULL && server->RegionCount > 0 )
            {
              for ( int r = 0; r < server->RegionCount; r++ )
			  {
                region = &server->RegionList[r];
                if ( region != NULL )
                {
                  if ( region->ZoneList != NULL && region->ZoneCount > 0 )
                  {
                    for ( int z = 0; z < region->ZoneCount; z++ )
                    {
                      zone = &region->ZoneList[z];
                      if ( zone != NULL )
                      {
                        ret_code = function( zone );
                      }
                      else
                      {
                        ret_code = SPV_SYSTEM_INVALID_ZONE;
                      }
                      if ( ret_code != 0 )
                      {
                        z = region->ZoneCount; // Fine del ciclo for
                      }
                    }
                  }
                  else
                  {
                    ret_code = SPV_SYSTEM_INVALID_ZONE_LIST;
                  }
                }
                else
                {
                  ret_code = SPV_SYSTEM_INVALID_REGION;
                }
                if ( ret_code != 0 )
                {
                  r = server->RegionCount; // Fine del ciclo for
                }
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_REGION_LIST;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_INVALID_SERVER;
          }
          if ( ret_code != 0 )
          {
            s = system->ServerCount; // Fine del ciclo for
          }
        }
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_ZONE_FUNCTION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire funzioni sui nodi del sistema
///
/// \date [11.02.2006]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnNodes( SPV_SYSTEM * system, SPV_SYSTEM_NODE_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server
  SPV_REGION  * region    = NULL; // Ptr ad una struttura regione
  SPV_ZONE    * zone      = NULL; // Ptr ad una struttura zona
  SPV_NODE    * node      = NULL; // Ptr ad una struttura nodo

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnNodes");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
    if ( function != NULL ) // Controllo il ptr a funzione
    {
      if ( system->ServerList != NULL && system->ServerCount > 0 )
      {
        for ( int s = 0; s < system->ServerCount; s++ )
        {
          server = &system->ServerList[s];
          if ( server != NULL )
          {
            if ( server->RegionList != NULL && server->RegionCount > 0 )
            {
              for ( int r = 0; r < server->RegionCount; r++ )
              {
                region = &server->RegionList[r];
                if ( region != NULL )
                {
                  if ( region->ZoneList != NULL && region->ZoneCount > 0 )
                  {
                    for ( int z = 0; z < region->ZoneCount; z++ )
                    {
                      zone = &region->ZoneList[z];
                      if ( zone != NULL )
                      {
                        if ( zone->NodeList != NULL && zone->NodeCount > 0 )
                        {
                          for ( int n = 0; n < zone->NodeCount; n++ )
                          {
                            node = &zone->NodeList[n];
                            if ( node != NULL )
                            {
                              ret_code = function( node );
                            }
                            else
                            {
                              ret_code = SPV_SYSTEM_INVALID_NODE;
                            }
                            if ( ret_code != 0 )
                            {
                              n = zone->NodeCount; // Fine del ciclo for
                            }
                          }
                        }
                        else
                        {
                          ret_code = SPV_SYSTEM_INVALID_NODE_LIST;
                        }
                      }
                      else
                      {
                        ret_code = SPV_SYSTEM_INVALID_ZONE;
                      }
                      if ( ret_code != 0 )
                      {
                        z = region->ZoneCount; // Fine del ciclo for
                      }
                    }
                  }
                  else
                  {
                    ret_code = SPV_SYSTEM_INVALID_ZONE_LIST;
                  }
                }
                else
                {
                  ret_code = SPV_SYSTEM_INVALID_REGION;
                }
                if ( ret_code != 0 )
                {
                  r = server->RegionCount; // Fine del ciclo for
                }
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_REGION_LIST;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_INVALID_SERVER;
          }
          if ( ret_code != 0 )
          {
            s = system->ServerCount; // Fine del ciclo for
          }
        }
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_NODE_FUNCTION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire funzioni sulle periferiche del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnDevices( SPV_SYSTEM * system, SPV_SYSTEM_DEVICE_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server
  SPV_REGION  * region    = NULL; // Ptr ad una struttura regione
  SPV_ZONE    * zone      = NULL; // Ptr ad una struttura zona
  SPV_NODE    * node      = NULL; // Ptr ad una struttura nodo
  SPV_DEVICE  * device    = NULL; // Ptr ad una struttura periferica

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnDevices");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
	if ( function != NULL ) // Controllo il ptr a funzione
	{
	  if ( system->ServerList != NULL && system->ServerCount > 0 )
	  {
		for ( int s = 0; s < system->ServerCount; s++ )
		{
		  server = &system->ServerList[s];
		  if ( server != NULL )
		  {
			if ( server->RegionList != NULL && server->RegionCount > 0 )
			{
			  for ( int r = 0; r < server->RegionCount; r++ )
			  {
				region = &server->RegionList[r];
				if ( region != NULL )
				{
				  if ( region->ZoneList != NULL && region->ZoneCount > 0 )
				  {
					for ( int z = 0; z < region->ZoneCount; z++ )
					{
					  zone = &region->ZoneList[z];
					  if ( zone != NULL )
					  {
						if ( zone->NodeList != NULL && zone->NodeCount > 0 )
						{
						  for ( int n = 0; n < zone->NodeCount; n++ )
						  {
							node = &zone->NodeList[n];
							if ( node != NULL )
							{
							  if ( node->DeviceList != NULL && node->DeviceCount > 0 )
							  {
								for ( int d = 0; d < node->DeviceCount; d++ )
								{
								  device = &node->DeviceList[d];
								  if ( device != NULL )
								  {
									ret_code = function( device );
								  }
								  else
								  {
									ret_code = SPV_SYSTEM_INVALID_DEVICE;
								  }
								  if ( ret_code != 0 )
								  {
									/////d = node->DeviceCount; // Fine del ciclo for
								  }
								}
							  }
							  else
							  {
								//ret_code = SPV_SYSTEM_INVALID_DEVICE_LIST;
							  }
							}
							else
							{
							  ret_code = SPV_SYSTEM_INVALID_NODE;
							}
							if ( ret_code != 0 )
							{
							  /////n = zone->NodeCount; // Fine del ciclo for
							}
						  }
						}
						else
						{
						  ret_code = SPV_SYSTEM_INVALID_NODE_LIST;
						}
					  }
					  else
					  {
						ret_code = SPV_SYSTEM_INVALID_ZONE;
					  }
					  if ( ret_code != 0 )
					  {
						/////z = region->ZoneCount; // Fine del ciclo for
					  }
					}
				  }
				  else
				  {
					ret_code = SPV_SYSTEM_INVALID_ZONE_LIST;
				  }
				}
				else
				{
				  ret_code = SPV_SYSTEM_INVALID_REGION;
				}
				if ( ret_code != 0 )
				{
				  /////r = server->RegionCount; // Fine del ciclo for
				}
			  }
			}
			else
			{
			  ret_code = SPV_SYSTEM_INVALID_REGION_LIST;
			}
		  }
		  else
		  {
			ret_code = SPV_SYSTEM_INVALID_SERVER;
		  }
		  if ( ret_code != 0 )
		  {
			/////s = system->ServerCount; // Fine del ciclo for
		  }
		}
	  }
	  else
	  {
		ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
	  }
	}
	else
	{
	  ret_code = SPV_SYSTEM_INVALID_DEVICE_FUNCTION;
	}
  }
  else
  {
	ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}


//==============================================================================
/// Funzione per eseguire funzioni sulle periferiche del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnDevices( SPV_SYSTEM * system, SPV_SYSTEM_VOID_DEVICE_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server
  SPV_REGION  * region    = NULL; // Ptr ad una struttura regione
  SPV_ZONE    * zone      = NULL; // Ptr ad una struttura zona
  SPV_NODE    * node      = NULL; // Ptr ad una struttura nodo
  SPV_DEVICE  * device    = NULL; // Ptr ad una struttura periferica

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnDevices");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
	if ( function != NULL ) // Controllo il ptr a funzione
	{
	  if ( system->ServerList != NULL && system->ServerCount > 0 )
	  {
		for ( int s = 0; s < system->ServerCount; s++ )
		{
		  server = &system->ServerList[s];
		  if ( server != NULL )
		  {
			if ( server->RegionList != NULL && server->RegionCount > 0 )
			{
			  for ( int r = 0; r < server->RegionCount; r++ )
			  {
				region = &server->RegionList[r];
				if ( region != NULL )
				{
				  if ( region->ZoneList != NULL && region->ZoneCount > 0 )
				  {
					for ( int z = 0; z < region->ZoneCount; z++ )
					{
					  zone = &region->ZoneList[z];
					  if ( zone != NULL )
					  {
						if ( zone->NodeList != NULL && zone->NodeCount > 0 )
						{
						  for ( int n = 0; n < zone->NodeCount; n++ )
						  {
							node = &zone->NodeList[n];
							if ( node != NULL )
							{
							  if ( node->DeviceList != NULL && node->DeviceCount > 0 )
							  {
								for ( int d = 0; d < node->DeviceCount; d++ )
								{
								  device = &node->DeviceList[d];
								  if ( device != NULL )
								  {
									ret_code = function( (void*)device );
								  }
								  else
								  {
									ret_code = SPV_SYSTEM_INVALID_DEVICE;
								  }
								  if ( ret_code != 0 )
								  {
									d = node->DeviceCount; // Fine del ciclo for
								  }
								}
							  }
							  else
							  {
								ret_code = SPV_SYSTEM_INVALID_DEVICE_LIST;
							  }
							}
							else
							{
							  ret_code = SPV_SYSTEM_INVALID_NODE;
							}
							if ( ret_code != 0 )
							{
							  n = zone->NodeCount; // Fine del ciclo for
							}
						  }
						}
						else
						{
						  ret_code = SPV_SYSTEM_INVALID_NODE_LIST;
						}
					  }
					  else
					  {
						ret_code = SPV_SYSTEM_INVALID_ZONE;
					  }
					  if ( ret_code != 0 )
					  {
						z = region->ZoneCount; // Fine del ciclo for
					  }
					}
				  }
				  else
				  {
					ret_code = SPV_SYSTEM_INVALID_ZONE_LIST;
				  }
				}
				else
				{
				  ret_code = SPV_SYSTEM_INVALID_REGION;
				}
				if ( ret_code != 0 )
				{
				  r = server->RegionCount; // Fine del ciclo for
				}
			  }
			}
			else
			{
			  ret_code = SPV_SYSTEM_INVALID_REGION_LIST;
			}
		  }
		  else
		  {
			ret_code = SPV_SYSTEM_INVALID_SERVER;
		  }
		  if ( ret_code != 0 )
		  {
			s = system->ServerCount; // Fine del ciclo for
		  }
		}
	  }
	  else
	  {
		ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
	  }
	}
	else
	{
	  ret_code = SPV_SYSTEM_INVALID_DEVICE_FUNCTION;
	}
  }
  else
  {
	ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire funzioni sulle schede del sistema
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDoOnCards( SPV_SYSTEM * system, SPV_SYSTEM_CARD_FUNCTION_PTR function )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server
  SPV_REGION  * region    = NULL; // Ptr ad una struttura regione
  SPV_ZONE    * zone      = NULL; // Ptr ad una struttura zona
  SPV_NODE    * node      = NULL; // Ptr ad una struttura nodo
  SPV_DEVICE  * device    = NULL; // Ptr ad una struttura periferica
  SPV_CARD    * card      = NULL; // Ptr ad una struttura scheda

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVDoOnCards");

  if ( system != NULL ) // Controllo il ptr alla struttura sistema
  {
    if ( function != NULL ) // Controllo il ptr a funzione
    {
      if ( system->ServerList != NULL && system->ServerCount > 0 )
      {
        for ( int s = 0; s < system->ServerCount; s++ )
        {
          server = &system->ServerList[s];
          if ( server != NULL )
          {
            if ( server->RegionList != NULL && server->RegionCount > 0 )
            {
              for ( int r = 0; r < server->RegionCount; r++ )
              {
                region = &server->RegionList[r];
                if ( region != NULL )
                {
                  if ( region->ZoneList != NULL && region->ZoneCount > 0 )
                  {
                    for ( int z = 0; z < region->ZoneCount; z++ )
                    {
                      zone = &region->ZoneList[z];
                      if ( zone != NULL )
                      {
                        if ( zone->NodeList != NULL && zone->NodeCount > 0 )
                        {
                          for ( int n = 0; n < zone->NodeCount; n++ )
                          {
                            node = &zone->NodeList[n];
                            if ( node != NULL )
                            {
                              if ( node->DeviceList != NULL && node->DeviceCount > 0 )
                              {
                                for ( int d = 0; d < node->DeviceCount; d++ )
                                {
                                  device = &node->DeviceList[d];
                                  if ( device != NULL )
                                  {
                                    if ( device->CardList != NULL && device->CardCount > 0 )
                                    {
                                      for ( int c = 0; c < device->CardCount; c++ )
                                      {
                                        card = &device->CardList[c];
                                        if ( card != NULL )
                                        {
                                          ret_code = function( card );
                                        }
                                        else
                                        {
                                          ret_code = SPV_SYSTEM_INVALID_CARD;
                                        }
                                        if ( ret_code != 0 )
                                        {
                                          c = device->CardCount; // Fine del ciclo
                                        }
                                      }
                                    }
                                    else
                                    {
                                      ret_code = SPV_SYSTEM_INVALID_CARD_LIST;
                                    }
                                  }
                                  else
                                  {
                                    ret_code = SPV_SYSTEM_INVALID_DEVICE;
                                  }
                                  if ( ret_code != 0 )
                                  {
                                    d = node->DeviceCount; // Fine del ciclo for
                                  }
                                }
                              }
                              else
                              {
                                ret_code = SPV_SYSTEM_INVALID_DEVICE_LIST;
                              }
                            }
                            else
                            {
                              ret_code = SPV_SYSTEM_INVALID_NODE;
                            }
                            if ( ret_code != 0 )
                            {
                              n = zone->NodeCount; // Fine del ciclo for
                            }
                          }
                        }
                        else
                        {
                          ret_code = SPV_SYSTEM_INVALID_NODE_LIST;
                        }
                      }
                      else
                      {
                        ret_code = SPV_SYSTEM_INVALID_ZONE;
                      }
                      if ( ret_code != 0 )
                      {
                        z = region->ZoneCount; // Fine del ciclo for
                      }
                    }
                  }
                  else
                  {
                    ret_code = SPV_SYSTEM_INVALID_ZONE_LIST;
                  }
                }
                else
                {
                  ret_code = SPV_SYSTEM_INVALID_REGION;
                }
                if ( ret_code != 0 )
                {
                  r = server->RegionCount; // Fine del ciclo for
                }
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_REGION_LIST;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_INVALID_SERVER;
          }
          if ( ret_code != 0 )
          {
            s = system->ServerCount; // Fine del ciclo for
          }
        }
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_SERVER_LIST;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_CARD_FUNCTION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

void _SimpleDecript( char * cripted )
{
  int pass_len = 0;
  int pos = 0;

  if ( cripted != NULL )
  {
    pass_len = strlen( cripted ) / 2L;
    if ( pass_len > 0 )
    {
      for ( int c = 0; c < pass_len; c++ )
      {
		pos = c * 2;
		if (cripted[pos] == '!') {
        	cripted[c] = cripted[pos];
		}
		else
		{
			cripted[c] = cripted[pos] - 1;
		}
      }
      cripted[pass_len] = '\0';
    }
  }
}

//==============================================================================
/// Funzione per caricare la configurazione del database
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLoadDatabases( SPV_SYSTEM * system )
{
  int             ret_code      = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  SPV_DBI_CONN  * db_conn       = NULL; // Ptr a struttura connessione DB
  XML_ELEMENT   * db_element    = NULL; // Ptr a elemento XML per i DB
  XML_ELEMENT   * conn_element  = NULL;
  XML_ELEMENT   * sch_element   = NULL;
  char          * name          = NULL;
  int             id_val        = 0;
  char          * name_val      = NULL;
  char          * type_val      = NULL;
  int             type_int_val  = 0;
  char          * version_val   = NULL;
  char          * host_val      = NULL;
  char          * user_val      = NULL;
  char          * password_val  = NULL;
  char          * schema_val    = NULL;
  bool            persistent    = false;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVLoadDatabases");

  if ( system != NULL ) // Controllo il ptr alla struttura del sistema
  {
    db_element = XMLGetFirst( system->Source, "database" );
    if ( db_element != NULL )
    {
      conn_element = XMLGetNext( db_element->child, "connection", -1 );
      if ( conn_element != NULL ) // Se il ptr � valido
      {
        name = conn_element->name;
        if ( strcmpi( name, "connection" ) == 0 )
        {
          sch_element = XMLGetNext( conn_element->child, "schema", -1 );
          if ( sch_element != NULL )
          {
            // --- Recupero i valori della connessione/schema DB ---
            id_val        = XMLGetValueInt  ( conn_element, "id"       );
            name_val      = XMLGetValue     ( conn_element, "name"     );
            type_val      = XMLGetValue     ( conn_element, "type"     );
            version_val   = XMLGetValue     ( conn_element, "version"  );
            host_val      = XMLGetValue     ( conn_element, "host"     );
            user_val      = XMLGetValue     ( conn_element, "user"     );
            password_val  = XMLGetValue     ( conn_element, "password" );
            schema_val    = XMLGetValue     ( sch_element,  "name"     );
            persistent    = XMLGetValueBool ( conn_element, "persistent" );
            // --- Controllo i valori recuperati ---
            if ( id_val       >= 0
              && name_val     != NULL
              && type_val     != NULL
              && version_val  != NULL
              && host_val     != NULL
              && user_val     != NULL
              && password_val != NULL
              && schema_val   != NULL )
            {
              // --- Decodifico il tipo di database ---
              type_int_val = SPVDBGetTypeCode( type_val );
              // --- Decripto la password --
              _SimpleDecript( password_val );
              // --- Creo una nuova connessione DB (utilizzo var globale) ---
              db_conn = SPVDBNewConn( id_val, name_val, type_int_val, host_val, user_val, password_val, schema_val );
              if ( db_conn != NULL ) // Se l'allocazione della nuova connessione a database � andata a buon fine
              {
                db_conn->Persistent = persistent;
                db_conn->Def = (void*) conn_element;
                system->DBConn = (void*) db_conn;
              }
              else
              {
                ret_code = SPV_SYSTEM_ALLOCATION_ERROR;
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_DEFINITION;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_INVALID_DEFINITION;
          }
        }
        else
        {
          ret_code = SPV_SYSTEM_INVALID_DEFINITION;
        }
      }
      else
      {
        ret_code = SPV_SYSTEM_INVALID_ELEMENT;
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_DEFINITION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare dalla memoria la configurazione del database
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVClearDatabases( SPV_SYSTEM * system )
{
  int             ret_code  = SPV_SYSTEM_NO_ERROR;
  int             fun_code  = SPV_DBI_NO_ERROR;
  SPV_DBI_CONN  * db_conn   = NULL;

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVClearDatabases");

  if ( system != NULL )
  {
    if ( system->DBConn != NULL )
    {
      db_conn = (SPV_DBI_CONN*) system->DBConn;
      // --- Se la connessione al DB � ancora attiva la chiudo ---
      if ( db_conn->Active )
      {
        fun_code = SPVDBCloseConn( db_conn );
        if ( fun_code != SPV_SYSTEM_NO_ERROR )
        {
          SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE, db_conn->LastError, NULL );
        }
        else
        {
          SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS_DES, NULL );
        }
      }
      ret_code = SPVDBDeleteConn( db_conn );
      ret_code = (ret_code == SPV_SYSTEM_NO_ERROR)?fun_code:ret_code;
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_DB_CONNECTION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );

  return ret_code;
}

//==============================================================================
/// Funzione per verificare le tabelle del database
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
int SPVCheckTables( SPV_SYSTEM * system )
{
  int             ret_code        = SPV_SYSTEM_NO_ERROR; // Codice di ritorno
  int             fun_code        = SPV_DBI_NO_ERROR; // Codice di funzione
  SPV_DBI_CONN  * db_conn         = NULL; // Ptr a struttura connessione
  XML_ELEMENT   * conn_element    = NULL; // Ptr a elemento XML connessione
  XML_ELEMENT   * schema_element  = NULL; // Ptr a elemento XML schema (database)
  XML_ELEMENT   * table_element   = NULL; // Ptr a elemento XML tabella
  XML_ELEMENT   * column_element  = NULL; // Ptr a elemento XML colonna
  int             table_count     = 0   ; // Contatore tabelle
  int             column_count    = 0   ; // Contatore colonne
  char          * schema_name     = NULL; // Nome dello schema (database)
  char          * table_name      = NULL; // Nome della tabella
  char          * name            = NULL; // Nome della colonna
  char          * type            = NULL; // Tipo di colonna
  char          * params          = NULL; // Parametri della colonna
  char            query           [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
  char            query_1         [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
  char            query_2         [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query
  char            query_3         [SPV_DBI_QUERY_MAX_LENGTH]; // Stringa query

  // --- Entro nella sezione critica ---
  SPVEnterSystemData("SPVCheckTables");

  if ( system != NULL )
  {
    db_conn = (SPV_DBI_CONN*) system->DBConn;
    if ( db_conn != NULL )
    {
      switch ( db_conn->Type ) // Switcho sui tipi di connessione database
      {
        // --- Caso MySQL (API C) ---
        case SPV_DBI_CONN_TYPE_mySQL:
          if ( !db_conn->Active )
          {
            fun_code = SPVDBOpenConn( db_conn );
            if ( fun_code == SPV_DBI_NO_ERROR )
            {
              SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS_DES, NULL );
            }
            else
            {
              SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_CONNECT_FAILURE, db_conn->LastError, NULL );
            }
            if ( fun_code == SPV_DBI_WARNING_DATABASE_NOT_FOUND )
            {
              // --- Creo lo schema (database) ---
              schema_name = db_conn->Database;
              query[0] = 0; // Inizializzo la stringa della query
              strcat( query, "CREATE DATABASE " );
              strcat( query, schema_name );
              fun_code = SPVDBQuery( db_conn, query, 0, NULL, 0 );
              if ( fun_code == SPV_DBI_NO_ERROR )
              {
                fun_code = SPVDBUse( db_conn, schema_name );
              }
              else
              {
								SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
              }
            }
          }
          if ( db_conn->Active && fun_code == SPV_DBI_NO_ERROR )
          {
            conn_element = (XML_ELEMENT*) db_conn->Def;
            if ( conn_element != NULL )
            {
              schema_element = conn_element->child;
              if ( schema_element != NULL )
              {
                // --- Controllo ed eventualmente ricreo tutte le tabelle ---
                table_count = XMLGetChildCount( schema_element, false );
                if ( table_count > 0 )
                {
                  table_element = schema_element->child;
                  // --- Ciclo sulle tabelle definite via XML ---
                  for ( int t = 0; t < table_count; t++ )
                  {
                    if ( table_element != NULL )
                    {
                      // --- Recupero le informazioni di una tabella ---
                      table_name = XMLGetValue( table_element, "name" );
                      column_count = XMLGetChildCount( table_element, false );
                      if ( column_count > 0 )
                      {
                        // --- Generazione query ---
                        query[0] = 0; // Inizializzo la stringa della query
                        strcat( query, "CREATE TABLE IF NOT EXISTS " );
                        strcat( query, table_name );
                        strcat( query, " (" );
                        column_element = table_element->child;
                        // --- Ciclo sulle colonne della tabella ---
                        for ( int c = 0; c < column_count; c++ )
                        {
                          if ( column_element != NULL )
                          {
                            // --- Recupero le informazioni relative alla singola colonna colonna ---
                            name   = XMLGetValue( column_element, "name" );
                            type   = XMLGetValue( column_element, "type" );
                            params = XMLGetValue( column_element, "params" );
                            if ( name != NULL && type != NULL ) // Controllo di avere almeno nome e tipo
                            {
                              strcat( query, name );
                              strcat( query, " " );
                              strcat( query, type );
                              if ( params != NULL )
                              {
                                strcat( query, " " );
                                strcat( query, params );
                              }
                              if ( c < ( column_count - 1 ) )
                              {
                                strcat( query, ", " );
                              }
                            }
                            else
                            {
                              ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                              t = table_count; // Fine del ciclo for
                            }
                            column_element = column_element->next;
                          }
                          else
                          {
                            ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                            t = table_count; // Fine del ciclo for
                          }
                        }
                        strcat( query, ")" );
                        // --- Lancio la query ---
                        if ( query != NULL )
                        {
                           if ( SPVDBQuery( db_conn, query, 0, NULL, 0 ) != 0 )
                           {
														 SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
                             ret_code = SPV_SYSTEM_DB_QUERY_FAILURE;
                           }
                        }
                        else
                        {
                          ret_code = SPV_SYSTEM_INVALID_QUERY;
                        }
                      }
                      else
                      {
                        ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                        t = table_count; // Fine del ciclo for
                      }
                      table_element = table_element->next;
                    }
                    else
                    {
                      ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                      t = table_count; // Fine del ciclo for
                    }
                  }
                }
                else
                {
                  ret_code = SPV_SYSTEM_NO_TABLE_FOUND;
                }
              }
              else
              {
                ret_code = SPV_SYSTEM_INVALID_DB_SCHEMA_DEF;
              }
              // --- Controllo che la connessione non sia persistente ---
              if ( !db_conn->Persistent )
              {
                ret_code = SPVDBCloseConn( db_conn );
                if ( ret_code != SPV_SYSTEM_NO_ERROR )
                {
                  SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE, db_conn->LastError, NULL );
                }
                else
                {
                  SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS_DES, NULL );
                }
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_DB_CONN_DEF;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_DB_OPEN_FAILURE;
          }
        break;
        // --- Caso Microsoft SQL Server ---
        case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
          // --- Controllo che ci sia lo SCHEMA ---
          if ( !db_conn->Active )
          {
            fun_code = SPVDBOpenConn( db_conn );
            if ( fun_code == SPV_DBI_NO_ERROR )
            {
              SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS_DES, NULL );
            }
            else
            {
              SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_CONNECT_FAILURE, db_conn->LastError, NULL );
            }
            if ( fun_code == SPV_DBI_WARNING_DATABASE_NOT_FOUND )
            {
              // --- Creo lo schema (database) ---
              schema_name = db_conn->Database;
              query[0]    = 0; // Inizializzo la stringa della query
              query_2[0]  = 0; // Inizializzo la stringa della query
              strcat( query, "CREATE DATABASE " );
              strcat( query, schema_name );
              fun_code = SPVDBQuery( db_conn, query, 0, NULL, 0 );
              if ( fun_code == SPV_DBI_NO_ERROR )
              {
                fun_code = SPVDBUse( db_conn, schema_name );
              }
              else
              {
								SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
              }
            }
          }
          // --- Controllo l'esistenza delle TABELLE ---
          if ( db_conn->Active && fun_code == SPV_DBI_NO_ERROR )
          {
            conn_element = (XML_ELEMENT*) db_conn->Def;
            if ( conn_element != NULL )
            {
              schema_element = conn_element->child;
              if ( schema_element != NULL )
              {
                // --- Controllo ed eventualmente ricreo tutte le tabelle ---
                table_count = XMLGetChildCount( schema_element, false );
                if ( table_count > 0 )
                {
                  table_element = schema_element->child;
                  // --- Ciclo sulle tabelle definite via XML ---
                  for ( int t = 0; t < table_count; t++ )
                  {
                    if ( table_element != NULL )
                    {
                      // --- Recupero le informazioni di una tabella ---
                      table_name = XMLGetValue( table_element, "name" );
                      column_count = XMLGetChildCount( table_element, false );
                      if ( column_count > 0 )
                      {
                        // --- Generazione query CREATE ---
                        query[0] = 0; // Inizializzo la stringa della query
                        strcat( query, "CREATE TABLE " );
                        strcat( query, table_name );
                        strcat( query, " (" );
                        // --- Generazione query SELECT * ---
                        query_1[0] = 0;
                        strcat( query_1, "SELECT * FROM " );
                        // --- Generazione query SELECT ---
                        query_2[0] = 0;
                        strcat( query_2, "SELECT " );
                        // --- Generazione query DROP ---
                        query_3[0] = 0;
                        strcat( query_3, "DROP TABLE " );
                        column_element = table_element->child;
                        // --- Ciclo sulle colonne della tabella ---
                        for ( int c = 0; c < column_count; c++ )
                        {
                          if ( column_element != NULL )
                          {
                            // --- Recupero le informazioni relative alla singola colonna colonna ---
                            name   = XMLGetValue( column_element, "name" );
                            type   = XMLGetValue( column_element, "type" );
                            params = XMLGetValue( column_element, "params" );
                            if ( name != NULL && type != NULL ) // Controllo di avere almeno nome e tipo
                            {
                              // --- Query CREATE ---
                              strcat( query, name );
                              strcat( query, " " );
                              strcat( query, type );
                              if ( params != NULL )
                              {
                                strcat( query, " " );
                                strcat( query, params );
                              }
                              // --- Query SELECT ---
                              strcat( query_2, name );
                              if ( c < ( column_count - 1 ) )
                              {
                                // --- Query CREATE ---
                                strcat( query, ", " );
                                // --- Query SELECT ---
                                strcat( query_2, "," );
                              }
                            }
                            else
                            {
                              ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                              t = table_count; // Fine del ciclo for
                            }
                            column_element = column_element->next;
                          }
                          else
                          {
                            ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                            t = table_count; // Fine del ciclo for
                          }
                        }
                        // --- Query CREATE ---
                        strcat( query, ")" );
                        // --- Query SELECT * ---
                        strcat( query_1, table_name );
                        // --- Query SELECT ---
                        strcat( query_2, " FROM " );
                        strcat( query_2, table_name );
                        // --- Query DROP ---
                        strcat( query_3, table_name );
                        // --- Lancio la query SELECT * ---
                        if ( SPVDBQuery( db_conn, query_1, 0, NULL, 0 ) != 0 )
                        {
                          // --- La tabella non esiste ---
                          if ( db_conn->LastErrorCode == 8180 )
                          {
                            // --- Lancio la query CREATE ---
                            if ( SPVDBQuery( db_conn, query, 0, NULL, 0 ) != 0 )
                            {
															SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
                              ret_code = SPV_SYSTEM_DB_QUERY_FAILURE;
                            }
                            else
                            {
                              ret_code = SPV_SYSTEM_NO_ERROR;
                            }
                          }
                          else
                          {
														SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
                            ret_code = SPV_SYSTEM_DB_QUERY_FAILURE;
                          }
                        }
                        else
                        {
                          // --- La tabella esiste ---
                          // --- Lancio la query SELECT ---
                          if ( SPVDBQuery( db_conn, query_2, 0, NULL, 0 ) != 0 )
                          {
                            // --- La tabella non ha tutte le colonne richieste ---
                            // --- Lancio la query DROP ---
                            if ( SPVDBQuery( db_conn, query_3, 0, NULL, 0 ) != 0 )
                            {
															SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
															ret_code = SPV_SYSTEM_DB_QUERY_FAILURE;
														}
														else
														{
															// --- Lancio la query CREATE ---
															if ( SPVDBQuery( db_conn, query, 0, NULL, 0 ) != 0 )
															{
																SPVReportDBEvent( db_conn, 1, SPV_APPLIC_EVENT_DB_QUERY_FAILURE, db_conn->LastError, query );
                                ret_code = SPV_SYSTEM_DB_QUERY_FAILURE;
                              }
                              else
                              {
                                ret_code = SPV_SYSTEM_NO_ERROR;
                              }
                            }
                          }
                          else
                          {
                            // --- La tabella e' corretta ---
                            ret_code = SPV_SYSTEM_NO_ERROR;
                          }
                        }
                      }
                      else
                      {
                        ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                        t = table_count; // Fine del ciclo for
                      }
                      table_element = table_element->next;
                    }
                    else
                    {
                      ret_code = SPV_SYSTEM_INVALID_DEFINITION;
                      t = table_count; // Fine del ciclo for
                    }
                  }
                }
                else
                {
                  ret_code = SPV_SYSTEM_NO_TABLE_FOUND;
                }
              }
              else
              {
                ret_code = SPV_SYSTEM_INVALID_DB_SCHEMA_DEF;
              }
              // --- Controllo che la connessione non sia persistente ---
              if ( !db_conn->Persistent )
              {
                ret_code = SPVDBCloseConn( db_conn );
                if ( ret_code != SPV_SYSTEM_NO_ERROR )
                {
                  SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_DISCONNECT_FAILURE, db_conn->LastError, NULL );
                }
                else
                {
                  SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_DISCONNECT_SUCCESS_DES, NULL );
                }
              }
            }
            else
            {
              ret_code = SPV_SYSTEM_INVALID_DB_CONN_DEF;
            }
          }
          else
          {
            ret_code = SPV_SYSTEM_DB_OPEN_FAILURE;
          }
        break;
        // --- Non supportato! ---
        default:
          if ( !db_conn->Active )
          {
            fun_code = SPVDBOpenConn( db_conn );
            if ( fun_code == SPV_DBI_NO_ERROR )
            {
              SPVReportDBEvent( db_conn, 0, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS, SPV_APPLIC_EVENT_DB_CONNECT_SUCCESS_DES, NULL );
            }
            else
            {
              SPVReportDBEvent( db_conn, 2, SPV_APPLIC_EVENT_DB_CONNECT_FAILURE, db_conn->LastError, NULL );
              ret_code = SPV_SYSTEM_DB_OPEN_FAILURE;
            }
          }
      }
    }
    else
    {
      ret_code = SPV_SYSTEM_INVALID_DB_CONNECTION;
    }
  }
  else
  {
    ret_code = SPV_SYSTEM_INVALID_SYSTEM;
  }

  // --- Esco dalla sezione critica ---
  SPVLeaveSystemData( );
  
  return ret_code;
}



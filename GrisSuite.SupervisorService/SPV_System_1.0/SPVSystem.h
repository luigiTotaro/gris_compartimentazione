//==============================================================================
// Telefin Supervisor System Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO SISTEMA (SPVSystem.h)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:	0.42 (13.04.2004 -> 29.10.2009)
//
// Copyright:	2004-2009 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland C++ Builder 2006-2007
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVSystem.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [13.04.2004]:
// - Prima versione prototipo del modulo.
// 0.01 [11.05.2004]:
// - Aggiunta la definizione del tipo SPV_DEVICE_STATUS.
// - Aggiunta la definizione del tipo SPV_DEVICE_CFG.
// - Aggiunta la definizione del tipo SPV_SERVER.
// - Aggiunta la definizione del tipo SPV_REGION.
// - Aggiunta la definizione del tipo SPV_ZONE.
// - Aggiunta la definizione del tipo SPV_NODE.
// - Aggiunta la definizione del tipo SPV_DEVICE_TYPE.
// - Aggiunta la definizione del tipo SPV_DEVICE.
// - Aggiunta la definizione del tipo SPV_CARD.
// - Incluso il modulo XMLInterface.
// 0.02 [20.05.2004]:
// - Modificata la definizione del tipo SPV_DEVICE_CFG.
// 0.03 [21.06.2004]:
// - Modificata la definizione del tipo SPV_DEVICE.
// - Modificata la definizione del tipo SPV_NODE.
// - Modificata la definizione del tipo SPV_ZONE.
// - Modificata la definizione del tipo SPV_REGION.
// - Modificata la definizione del tipo SPV_SERVER.
// - Aggiunta la funzione SPVLoadDevice.
// - Aggiunta la funzione SPVLoadNode.
// - Aggiunta la funzione SPVLoadZone.
// - Aggiunta la funzione SPVLoadRegion.
// - Aggiunta la funzione SPVLoadServer.
// - Aggiunta la funzione SPVLoadSystem.
// - Aggiunta la funzione SPVNewSystem.
// - Aggiunta la definizione del tipo SPV_SYSTEM.
// 0.04 [22.06.2004]:
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_FILE.
// - Modificata la funzione SPVLoadDevice.
// 0.05 [19.07.2004]:
// - Modificata la definizione del tipo SPV_DEVICE_TYPE.
// 0.06 [09.09.2004]:
// - Aggiunto il campo StreamList nella definizione del tipo SPV_DEVICE_TYPE.
// - Aggiunto il campo StreamNum nella definizione del tipo SPV_DEVICE_TYPE.
// 0.07 [20.09.2004]:
// - Aggiunta la funzione SPVSystemInit.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_SERVER_FUNCTION.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_REGION_FUNCTION.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_ZONE_FUNCTION.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_NODE_FUNCTION.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_DEVICE_FUNCTION.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_CARD_FUNCTION.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_SERVER_LIST.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_REGION_LIST.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_ZONE_LIST.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_NODE_LIST.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_DEVICE_LIST.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_CARD_LIST.
// - Aggiunta la funzione SPVDoOnServers.
// - Aggiunta la funzione SPVDoOnRegions.
// - Aggiunta la funzione SPVDoOnZones.
// - Aggiunta la funzione SPVDoOnNodes.
// - Aggiunta la funzione SPVDoOnDevices.
// - Aggiunta la funzione SPVDoOnCards.
// - Aggiunto il codice di errore SPV_SYSTEM_INVALID_CARD.
// 0.08 [23.09.2004]:
// - Modifcata la definizionde del tipo SPV_DEVICE_TYPE.
// - Modificata la definizione del tipo SPV_DEVICE.
// 0.09 [29.09.2004]:
// - Aggiunto il campo <Type> alla definizione del tipo SPV_CARD.
// - Aggiunto il campo <Type> alla definizione del tipo SPV_NODE.
// - Aggiunto il campo <Type> alla definizione del tipo SPV_ZONE.
// - Aggiunto il campo <Type> alla definizione del tipo SPV_REGION.
// - Aggiunto il campo <Type> alla definizione del tipo SPV_SERVER.
// - Aggiunto il campo <Level> alla definizione del tipo SPV_CARD.
// - Aggiunto il campo <Level> alla definizione del tipo SPV_NODE.
// - Aggiunto il campo <Level> alla definizione del tipo SPV_ZONE.
// - Aggiunto il campo <Level> alla definizione del tipo SPV_REGION.
// - Aggiunto il campo <Level> alla definizione del tipo SPV_SERVER.
// - Aggiunto il campo <Level> alla definizione del tipo SPV_SYSTEM.
// 0.10 [19.01.2005]:
// - Aggiunto il campo <PopUpMenu> alla definizione del tipo SPV_DEVICE.
// - Modificata la funzione SPVLoadDevice.
// 0.11 [20.01.2005]:
// - Aggiunta la funzione SPVGetDeviceType.
// - Aggiunta la funzione SPVLoadDeviceType.
// 0.12 [21.01.2005]:
// - Modificata la funzione SPVGetDeviceType.
// - Modificata la funzione SPVLoadDeviceType.
// 0.13 [31.01.2005]:
// - Modificata la funzione SPVLoadDevice.
// 0.14 [18.05.2005]:
// - Modificata la funzione SPVSystemInit.
// - Aggiunta la funzione SPVLoadDatabases.
// 0.15 [19.05.2005]:
// - Sostituita la funzione SPVNewSystem con SPVLoadSystemSource.
// 0.16 [24.05.2005]:
// - Modificata la funzione SPVLoadDatabases.
// - Aggiunta la funzione SPVCheckTables.
// 0.17 [25.05.2005]:
// - Modificata la funzione SPVCheckTables.
// 0.18 [26.05.2005]:
// - Modificata la funzione SPVClearDatabases.
// 0.19 [22.06.2005]:
// - Modificata la funzione SPVLoadDevice.
// - Modificata la definizione del tipo SPV_DEVICE.
// 0.20 [07.07.2005]:
// - Modificata la funzione SPVLoadDevice.
// 0.21 [26.07.2005]:
// - Modificata la funzione SPVSystemInit.
// - Aggiunta la funzione SPVEnterSystemData.
// - Aggiunta la funzione SPVLeaveSystemData.
// - Modificata la funzione SPVGetDeviceType.
// - Modificata la funzione SPVLoadDeviceType.
// - Aggiunta la funzione SPVGetDeviceUniID.
// - Aggiunta la funzione SPVGetDeviceUniIDStr.
// - Modificata la funzione SPVLoadDevice.
// - Modificata la funzione SPVLoadNode.
// - Modificata la funzione SPVLoadZone.
// - Modificata la funzione SPVLoadRegion.
// - Modificata la funzione SPVLoadServer.
// - Modificata la funzione SPVLoadSystem.
// - Modificata la funzione SPVLoadSystemSource.
// - Modificata la funzione SPVDoOnServers.
// - Modificata la funzione SPVDoOnRegions.
// - Modificata la funzione SPVDoOnZones.
// - Modificata la funzione SPVDoOnNodes.
// - Modificata la funzione SPVDoOnDevices.
// - Modificata la funzione SPVDoOnCards.
// - Modificata la funzione SPVLoadDatabases.
// - Modificata la funzione SPVClearDatabases.
// - Modificata la funzione SPVCheckTables.
// - Modificata la definizione del tipo SPV_DEVICE.
// - Modificata la definizione del tipo SPV_NODE.
// - Modificata la definizione del tipo SPV_ZONE.
// - Modificata la definizione del tipo SPV_REGION.
// - Modificata la definizione del tipo SPV_SERVER.
// 0.22 [29.07.2005]:
// - Modificata la funzione SPVGetDeviceUniIDStr.
// 0.23 [29.08.2005]:
// - Modificata la funzione SPVSystemClear.
// - Aggiunta la funzione SPVFreeSystemSource.
// 0.24 [31.08.2005]:
// - Aggiunta la funzione SPVFreeServer.
// - Aggiunta la funzione SPVFreeSystem.
// 0.25 [01.09.2005]:
// - Aggiunta la funzione SPVFreeDeviceType.
// - Aggiunta la funzione SPVFreeDevice.
// - Modificata la funzione SPVLoadNode.
// - Aggiunta la funzione SPVFreeNode.
// - Modificata la funzione SPVLoadZone.
// - Aggiunta la funzione SPVFreeZone.
// - Modificata la funzione SPVLoadRegion.
// - Aggiunta la funzione SPVFreeRegion.
// - Modificata la funzione SPVLoadServer.
// - Aggiunta la funzione SPVFreeServer.
// - Aggiunta la funzione SPVFreeSystem.
// - Modificata la funzione SPVClearDatabases.
// - Modificata la funzione SPVCheckTables.
// 0.26 [06.09.2005]:
// - Modificata la funzione SPVSystemInit.
// - Modificata la funzione SPVLoadDevice.
// - Modificata la funzione SPVLoadSystem.
// - Modificata la funzione SPVLoadSystemSource.
// 0.27 [06.10.2005]:
// - Modificata la funzione SPVLoadDevice.
// 0.28 [12.10.2005]:
// - Modificata la funzione SPVLoadDatabases.
// - Modificata la funzione SPVCheckTables.
// 0.29 [13.10.2005]:
// - Modificata la funzione SPVClearDatabases.
// - Modificata la funzione SPVCheckTables.
// 0.30 [19.12.2005]:
// - Aggiunta la funzione SPVGetNodeUniID.
// - Aggiunta la funzione SPVGetNodeUniIDStr.
// - Aggiunta la funzione SPVGetZoneUniID.
// - Aggiunta la funzione SPVGetZoneUniIDStr.
// - Aggiunta la funzione SPVGetRegionUniID.
// - Aggiunta la funzione SPVGetRegionUniIDStr.
// - Modificata la funzione SPVLoadDevice.
// - Modificata la funzione SPVFreeDevice.
// - Modificata la fuznioen SPVLoadSystem.
// 0.31 [14.06.2006]:
// - Modificata la funzione SPVLoadDevice.
// 0.32 [20.07.2006]:
// - Modificata la funzione SPVLoadDevice.
// 0.33 [13.12.2006]:
// - Corretta il caricamento del SrvID a 32bit SPVLoadServer().
// 0.34 [31.07.2007]:
// - Aggiunta cancellazione topografia in SPVFreeSystem().
// 0.35 [01.08.2007]:
// - Modificato criterio caricamento tipo periferica in SPVLoadDevice().
// 0.36 [12.10.2007]:
// - Aggiunta nuova implementazione di SPVDoOnDevices().
// 0.37 [04.12.2007]:
// - Aggiunta logica per gestione dei Vendors e dei Systems legati ai tipi di periferica
// - Modificata la definizione del tipo SPV_DEVICE_TYPE.
// - Aggiunta la definizione del tipo SPV_DEVICE_SYSTEM.
// - Aggiunta la definizione del tipo SPV_DEVICE_VENDOR.
// - Aggiunta la funzione SPVGetDeviceSystem.
// - Aggiunta la funzione SPVGetDeviceVendor.
// - Modificata la funzione SPVLoadDeviceType.
// - Modificata la funzione SPVFreeDeviceType.
// - Modificata la funzione SPVLoadSystem.
// - Modificata la funzione SPVSystemInit.
// - Modificata la funzione SPVSystemClear.
// 0.38 [13.12.2007]:
// - Modificata la funzione SPVLoadServer
// - Modificata la definizione del tipo SPV_DEVICE
// - Modificata la funzione SPVLoadDevice
// - Modificata la funzione SPVFreeDevice
// 0.39 [18.12.2007]:
// - Modificata la funzione SPVLoadDeviceType;
// 0.40 [11.02.2008]:
// - Introdotta nuova gestione delle eccezioni nelle funzioni
//		- SPVSystemInit;
//		- SPVSystemClear
//		- SPVEnterSystemData
//		- SPVLeaveSystemData
//		- SPVIsDeviceSystemInList
//		- SPVIsDeviceVendorInList
//		- SPVGetDeviceType
//		- SPVLoadDeviceType
//		- SPVFreeDeviceType
//		- SPVGetDeviceUniID
//		- SPVGetDeviceUniIDStr
//		- SPVSetDeviceTopography;
//		- SPVGetNodeUniID
//		- SPVGetNodeUniIDStr
//		- SPVGetZoneUniID
//		- SPVGetZoneUniIDStr
//		- SPVGetRegionUniIDStr
//		- SPVLoadDevice
//		- SPVFreeDevice
//		- SPVLoadNode
//		- SPVFreeNode
//		- SPVLoadZone
//		- SPVFreeZone
//		- SPVLoadRegion
//		- SPVFreeRegion
//		- SPVLoadServer
//		- SPVFreeServer
//		- SPVLoadSystem
//		- SPVFreeSystem
//		- SPVLoadSystemSource
//		- SPVDoOnServers
//		- SPVDoOnRegions
//		- SPVDoOnZones
//		- SPVDoOnNodes
//		- SPVDoOnDevices
//		- SPVDoOnCards
//		- SPVLoadDatabases
//		- SPVClearDatabases
//		- SPVCheckTables
//==============================================================================
#ifndef SPVSystemH
#define SPVSystemH
//------------------------------------------------------------------------------
#include "SPVDeviceObjectLayer.h"
#include "XMLInterface.h"
//------------------------------------------------------------------------------

//==============================================================================
// Definizione valori
//------------------------------------------------------------------------------
#define SPV_SYSTEM_MAX_DEVICE_SYSTEM_COUNT  50
#define SPV_SYSTEM_MAX_DEVICE_VENDOR_COUNT  50
#define SPV_SYSTEM_MAX_DEVICE_TYPE_COUNT    50
#define SPV_SYSTEM_MAX_DEVICE_COUNT         2000


#define SPV_SYSTEM_SERVER_VALIDITY_CHECK_CODE     2040234434
#define SPV_SYSTEM_REGION_VALIDITY_CHECK_CODE     2040435896
#define SPV_SYSTEM_ZONE_VALIDITY_CHECK_CODE       2040196300
#define SPV_SYSTEM_NODE_VALIDITY_CHECK_CODE       2040693300
#define SPV_SYSTEM_DEVICE_VALIDITY_CHECK_CODE     2040334833

#define SPV_SERVER_OFFLINE                  0
#define SPV_SERVER_OK                       1
#define SPV_SERVER_WARNING                  2
#define SPV_SERVER_ERROR                    3
#define SPV_SERVER_ALARM                    5

#define SPV_DEVICE_OFFLINE                  24
#define SPV_DEVICE_OK                       25
#define SPV_DEVICE_WARNING                  26
#define SPV_DEVICE_ERROR                    27
#define SPV_DEVICE_ALARM                    28
#define SPV_DEVICE_DISABLED                 29
#define SPV_DEVICE_UNKNOWN                  30

#define SPV_DEVICE_TYPE_CHANGED				1
#define SPV_DEVICE_TYPE_UNCHANGED           2
#define SPV_DEVICE_TYPE_NEW                 3

//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define SPV_SYSTEM_NO_ERROR                 0
#define SPV_SYSTEM_INVALID_SOURCE           5000
#define SPV_SYSTEM_INVALID_FILE_NAME        5001
#define SPV_SYSTEM_INVALID_ELEMENT          5002
#define SPV_SYSTEM_INVALID_DEFINITION       5003
#define SPV_SYSTEM_INVALID_FILE             5004
#define SPV_SYSTEM_INVALID_SYSTEM           5010
#define SPV_SYSTEM_INVALID_SERVER           5011
#define SPV_SYSTEM_INVALID_REGION           5012
#define SPV_SYSTEM_INVALID_ZONE             5013
#define SPV_SYSTEM_INVALID_NODE             5014
#define SPV_SYSTEM_INVALID_DEVICE           5015
#define SPV_SYSTEM_INVALID_SERVER_FUNCTION  5016
#define SPV_SYSTEM_INVALID_REGION_FUNCTION  5017
#define SPV_SYSTEM_INVALID_ZONE_FUNCTION    5018
#define SPV_SYSTEM_INVALID_NODE_FUNCTION    5019
#define SPV_SYSTEM_INVALID_DEVICE_FUNCTION  5020
#define SPV_SYSTEM_INVALID_CARD_FUNCTION    5021
#define SPV_SYSTEM_INVALID_SERVER_LIST      5022
#define SPV_SYSTEM_INVALID_REGION_LIST      5023
#define SPV_SYSTEM_INVALID_ZONE_LIST        5024
#define SPV_SYSTEM_INVALID_NODE_LIST        5025
#define SPV_SYSTEM_INVALID_DEVICE_LIST      5026
#define SPV_SYSTEM_INVALID_CARD_LIST        5027
#define SPV_SYSTEM_INVALID_CARD             5028
#define SPV_SYSTEM_NOT_TYPE_ELEMENT         5029
#define SPV_SYSTEM_INVALID_DEVICE_TYPE      5030
#define SPV_SYSTEM_ALLOCATION_ERROR         5031
#define SPV_SYSTEM_INVALID_DB_CONNECTION    5032
#define SPV_SYSTEM_INVALID_DB_CONN_DEF      5033
#define SPV_SYSTEM_INVALID_DB_SCHEMA_DEF    5034
#define SPV_SYSTEM_NO_TABLE_FOUND           5035
#define SPV_SYSTEM_DB_QUERY_FAILURE         5036
#define SPV_SYSTEM_INVALID_QUERY            5037
#define SPV_SYSTEM_DB_OPEN_FAILURE          5038
#define SPV_SYSTEM_CRITICAL_ERROR           5039
#define SPV_SYSTEM_FUNCTION_EXCEPTION		5040
#define SPV_SYSTEM_INVALID_RACK				5041
#define SPV_SYSTEM_INVALID_POSITION			5042
#define SPV_SYSTEM_INVALID_TOPOGRAPHY		5043
//==============================================================================
// Definizione strutture e tipi
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura dati per la scheda.
///
/// \date [29.09.2004]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SPV_CARD
{
  char        * ID      ; // Codice identificativo
  char        * Name    ; // Nome dell'entit�
  int           Type    ; // Tipo di scheda
  int           Level   ; // Livello di severit�
  void        * Device  ; // Puntatore alla periferica di appartenenza
  void        * Tag     ; // Puntatore di servizio
}
SPV_CARD;

//==============================================================================
/// Struttura dati per lo stato di supervisione di una periferica.
///
/// \date [11.05.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_STATUS
{
  int         Level       ; // Livello di severit� dello stato
  char      * Description ; // Descrizione dello stato
  bool        Unknown     ; // Flag di stato periferica sconosciuta
  bool        Active      ; // Flag si periferica attiva
  bool        Scheduled   ; // Flag di periferica schedulata
  bool        Warning     ; // Flag di periferica in warning
  bool        Alarm       ; // Flag di periferica in allarme
  bool        Offline     ; // Flag di periferica offline
  unsigned int	CommErrors	; // Numero di errori di comunicazione dall'ultimo contatto
}
SPV_DEVICE_STATUS;

//==============================================================================
/// Struttura dati per la configurazione di supervisione di una periferica.
///
/// \date [06.12.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_CFG
{
  unsigned __int32  Address     ; // Indirizzo della periferica
  unsigned char     DataLink    ; // Tipologia di collegamento
  /////unsigned char     ComProtCode ; // Tipologia di protocollo di comunicazione
  int               PortID      ; // ID della porta di comunicazione
  void            * ComProt     ;
  void            * Port        ;
}
SPV_DEVICE_CFG;


//==============================================================================
/// Struttura dati per la tipologia di periferica.
///
/// \date [06.12.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_SYSTEM
{
	unsigned __int32    ID    		; // Codice numerico identificativo del System
	char              * Description	; // Descrizione del System
}
SPV_DEVICE_SYSTEM;


//==============================================================================
/// Struttura dati per il produttore di periferica.
///
/// \date [06.12.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_VENDOR
{
	unsigned __int32    ID    		; // Codice numerico identificativo del Vendor
	char              * Name  		; // Nome del Vendor
	char              * Description	; // Descrizione del Vendor (campo facoltativo)
}
SPV_DEVICE_VENDOR;


//==============================================================================
/// Struttura dati per il tipo periferica.
///
/// \date [04.12.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_TYPE
{
	char              * ID          ; // Codice numerico identificativo
	char              * Code        ; // Codice alfanumerico identificativo
	char              * Name        ; // Nome del tipo periferica
	unsigned __int32	VendorID    ; // Codice numerico identificativo del Vendor
	unsigned __int32	SystemID    ; // Codice numerico identificativo del System
	char              * Version     ; // Versione del tipo periferica
	bool				Endian		; // Endian 0=little 1=big
	XML_SOURCE        * Definition  ; // Ptr a definizione tipo periferica in XML
	XML_ELEMENT       * ComProtDef  ; // Ptr all'elemento root per la def. protocollo liv.2
	void              * ComProt     ; // Ptr alla struttura del tipo protocollo (SPV_COMPROT_TYPE*)
	XML_ELEMENT       * GenStatusDef; // Ptr alla definizione dello stato generale
	XML_ELEMENT       * GenCfgDef   ; // Ptr alla definizione della configurazione generale
	void              * Tag         ; // Puntatore di servizio
}
SPV_DEVICE_TYPE;

//==============================================================================
/// Struttura dati per la periferica.
///
/// \date [09.02.2009]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE
{
	unsigned __int32		VCC         		; // Validity Check Code
	int						ID          		; // Codice numerico identificativo
	bool					Owned				; // Flag per verificare l'assegnazione del supervisore
	char *					SN          		; // Serial Number
	unsigned __int64		DevID       		; // ID della periferica
	char *					Name        		; // Nome dell'entit�
	SPV_DEVICE_TYPE *		Type        		; // Puntatore alla struttura di def. del tipo
	SPV_DEVICE_STATUS		SupStatus   		; // Stato di supervisione
	SPV_DEVICE_CFG			SupCfg      		; // Configurazione di supervisione
	XML_ELEMENT *			GenStatus   		; // Stato generale
	XML_ELEMENT *			GenCfg      		; // Configurazione generale
	void*					Node        		; // Puntatore al nodo di appartenenza
	void*					Rack				; // Puntatore all'armadio della periferica
	int						RackCol				; // Posizione colonna nell'armadio
	int						RackRow				; // Posizione riga nell'armadio
	SPV_DEVICE_OBJECT *		ObjectList			; // Lista degli oggetti applicativi
	int						ObjectCount			; // Numero di oggetti applicativi
	SPV_CARD *				CardList    		; // Puntatore alla lista di schede
	int						CardCount   		; // Numero di schede
	int						StreamCount 		; // Numero di stream
	void *					StreamList  		; // Lista degli stream della periferica
	int						ProfileID   		; // ID del profilo di schedulazione
	XML_ELEMENT*			Def         		; // Ptr all'elemento XML della definizione
	void *					PopUpMenu   		; // Ptr al pop-up menu della periferica
	int						TypeStatus			; // Stato del tipo (Nuovo, Cambiato, Uguale)
	char *					DefinitionVersion	; // Versione del file xml di definizione della periferica
	char *					ProtocolVersion		; // Versione del file xml di protocollo della periferica
	void *					Tag         		; // Puntatore di servizio
}
SPV_DEVICE;

//==============================================================================
/// Struttura dati per il nodo.
///
/// \date [04.09.2008]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
typedef struct _SPV_NODE
{
	unsigned __int32    VCC         ; // Validity Check Code
	int                 ID          ; // Codice numerico identificativo
	unsigned __int64    NodID       ; // ID del nodo
	char              * Code        ; // Codice alfanumerico identificativo
	char              * Name        ; // Nome dell'entit�
	bool				Local		; // Flag nodo locale alla macchina
	int                 Type        ; // Tipo di nodo
	int                 Level       ; // Livello di severit�
	void              * Zone        ; // Puntatore alla zona di appartenenza
	SPV_DEVICE        * DeviceList  ; // Puntatore alla lista di periferiche.
	int                 DeviceCount ; // Numero di periferiche
	XML_ELEMENT       * Def         ; // Ptr a elemento XML di definizione
	void              * Tag         ; // Puntatore di servizio
}
SPV_NODE;

//==============================================================================
/// Struttura dati per una zona di supervisione.
///
/// \date [09.03.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
typedef struct _SPV_ZONE
{
	unsigned __int32    VCC       ; // Validity Check Code
	int                 ID        ; // Codice numerico identificativo
	unsigned __int64    ZonID     ; // ID della zona
	char              * Code      ; // Codice alfanumerico identificativo
	char              * Name      ; // Nome dell'entit�
	int                 Type      ; // Tipo di zona
	int                 Level     ; // Livello di severit�
	void              * Region    ; // Puntatore all'area di appartenenza
	SPV_NODE          * NodeList  ; // Puntatore alla lista di nodi
	int                 NodeCount ; // Numero di nodi
	XML_ELEMENT       * Def       ; // Ptr a elemento XML di definizione
	void              * Tag       ; // Puntatore di servizio
}
SPV_ZONE;

//==============================================================================
/// Struttura dati per un'area di supervisione.
///
/// \date [09.03.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
typedef struct _SPV_REGION
{
	unsigned __int32    VCC       ; // Validity Check Code
	int                 ID        ; // Codice numerico identificativo
	unsigned __int64    RegID     ; // ID della regione
	char              * Code      ; // Codice alfanumerico identificativo
	char              * Name      ; // Nome dell'entit�
	int                 Type      ; // Tipo di regione
	int                 Level     ; // Livello di severit�
	void              * Server    ; // Puntatore al server di appartenenza
	SPV_ZONE          * ZoneList  ; // Puntatore alla lista di zone di supervisione
	int                 ZoneCount ; // Numero di zone di supervisione
	XML_ELEMENT       * Def       ; // Ptr a elemento XML di definizione
	void              * Tag       ; // Puntatore di servizio
}
SPV_REGION;

//==============================================================================
/// Struttura dati per il server di supervisione.
///
/// \date [04.09.2008]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
typedef struct _SPV_SERVER
{
	unsigned __int32    VCC         ; // Validity Check Code
	int                 ID          ; // Codice numerico identificativo
	unsigned __int32    SrvID       ; // ID del server
	unsigned __int64    NodID       ; // ID del nodo
	char              * Name        ; // Nome dell'entit�
	char              * Host        ; // Host del server
	int                 Type        ; // Tipo di server
	int                 Level       ; // Livello di severit�
	void              * System      ; // Ptr alla struttura del sistema
	SPV_REGION        * RegionList  ; // Puntatore alla lista di Aree di supervisione
	int                 RegionCount ; // Numero di aree di supervisione
	XML_ELEMENT       * Def         ; // Ptr a elemento XML di definizione
	void              * Tag         ; // Puntatore di servizio
}
SPV_SERVER;

//==============================================================================
/// Struttura dati per il sistema di supervisione.
///
/// \date [26.10.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
typedef struct _SPV_SYSTEM
{
  int           ID          	; // Codice numerico identificativo
  char        * Code        	; // Codice alfanumerico identificativo
  char        * Name        	; // Nome dell'entit�
  bool			MultiLangauge	; // Flag MultiLingua
  int           Level       	; // Livello di severit�
  SPV_SERVER  * ServerList  	; // Ptr a lista dei server
  int           ServerCount 	; // Numero di server di supervisione
  void        * DBConn      	; // Ptr alla struttura Connessione Database
  XML_ELEMENT * Def         	; // Ptr a elemento XML root di definizione sistema
  XML_SOURCE  * Source      	; // Ptr a sorgente XML di definizione sistema
  void        * Tag         	; // Puntatore di servizio
}
SPV_SYSTEM;

//==============================================================================
/// Puntatori a funzione di esecuzione sul sistema
///
/// \date [20.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef int(* SPV_SYSTEM_SERVER_FUNCTION_PTR) ( SPV_SERVER * );
typedef int(* SPV_SYSTEM_REGION_FUNCTION_PTR) ( SPV_REGION * );
typedef int(* SPV_SYSTEM_ZONE_FUNCTION_PTR)   ( SPV_ZONE * );
typedef int(* SPV_SYSTEM_NODE_FUNCTION_PTR)   ( SPV_NODE * );
typedef int(* SPV_SYSTEM_DEVICE_FUNCTION_PTR) ( SPV_DEVICE * );
typedef int(* SPV_SYSTEM_VOID_DEVICE_FUNCTION_PTR) ( void * );
typedef int(* SPV_SYSTEM_CARD_FUNCTION_PTR)   ( SPV_CARD * );

//==============================================================================
// Varibili globali
//------------------------------------------------------------------------------
extern  SPV_SYSTEM			SPVSystem;
extern  XML_SOURCE *		SPVSystemSource;

extern  SPV_DEVICE_SYSTEM   SPVDeviceSystemList[SPV_SYSTEM_MAX_DEVICE_SYSTEM_COUNT];
extern  SPV_DEVICE_VENDOR   SPVDeviceVendorList[SPV_SYSTEM_MAX_DEVICE_VENDOR_COUNT];
extern  SPV_DEVICE_TYPE   	SPVDeviceTypeList[SPV_SYSTEM_MAX_DEVICE_TYPE_COUNT];
extern  SPV_DEVICE        	SPVDeviceList[SPV_SYSTEM_MAX_DEVICE_COUNT];
extern  SPV_NODE *			SPVNodeList;

extern  int					SPVDeviceSystemCount;
extern  int					SPVDeviceVendorCount;
extern  int					SPVDeviceTypeCount;
extern  int					SPVDeviceCount;
extern  int					SPVNodeCount;
extern	int             	SPVOwnedDeviceCount;

//==============================================================================
// Prototipi Funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo
void              SPVSystemInit       ( void );
// Funzione per entrare nella sezione critica dei dati di sistema
void              SPVEnterSystemData  ( char * label );
// Funzione per uscire dalla sezione critica dei dati di sistema
void              SPVLeaveSystemData		( void );
// Funzione per verificare in lista la presenza di una tipologia di periferica
bool 			  SPVIsDeviceSystemInList	( unsigned __int32 SystemID );
// Funzione per verificare in lista la presenza di un vendor di periferica
bool 			  SPVIsDeviceVendorInList	( unsigned __int32 VendorID );
// Funzione per ottenere una struttura di tipo periferica
SPV_DEVICE_TYPE * SPVGetDeviceType			( char * type );
// Funzione per caricare un tipo periferica
int               SPVLoadDeviceType		( XML_ELEMENT * e_type );
// Funzione per eliminare la struttura un tipo periferica
int               SPVFreeDeviceType		( SPV_DEVICE_TYPE * type );
// Funzione per eliminare la struttura sistema periferica
int               SPVFreeDeviceSystem	( SPV_DEVICE_SYSTEM * system );
// Funzione per eliminare la struttura vendor periferica
int               SPVFreeDeviceVendor   ( SPV_DEVICE_VENDOR * vendor );
// Funzione per eliminare la lista dei sistemi periferica
int					SPVFreeDeviceSystemList		( void );
// Funzione per eliminare la lista dei vendor periferica
int					SPVFreeDeviceVendorList		( void );
// Funzione per ottenere il SrvID (16bit) di una periferica
unsigned __int32  SPVGetSrvID			( SPV_DEVICE * device );
// Funzione per ottenere il SrvID (16bit) di un nodo
unsigned __int32  SPVGetSrvID			( SPV_NODE * node );
// Funzione per ottenere il SrvID (16bit) di una zona
unsigned __int32  SPVGetSrvID         ( SPV_ZONE * zone );
// Funzione per ottenere il SrvID (16bit) di una regione
unsigned __int32  SPVGetSrvID         ( SPV_REGION * region );
// Funzione per calcolare il codice unico (UniID) di una periferica
int               SPVGetDeviceUniID   ( SPV_DEVICE * device );
// Funzione per generare una stringa del codice unico (UniID) di una periferica
char            * SPVGetDeviceUniIDStr( SPV_DEVICE * device );
// Funzione per impostare la topografia di una periferica
int					SPVSetDeviceTopography	( SPV_DEVICE * device );
// Funzione per ottenere il NodID di una periferica
unsigned __int64  SPVGetNodID         ( SPV_DEVICE * device );
// Funzione per calcolare il codice unico (UniID) di un nodo
int               SPVGetNodeUniID     ( SPV_NODE * node );
// Funzione per generare una stringa del codice unico (UniID) di un nodo
char            * SPVGetNodeUniIDStr  ( SPV_NODE * node );
// Funzione per ottenere il ZonID di un nodo
unsigned __int64  SPVGetZonID         ( SPV_NODE * node );
// Funzione per calcolare il codice unico (UniID) di una zona
int               SPVGetZoneUniID     ( SPV_ZONE * zone );
// Funzione per generare una stringa del codice unico (UniID) di una zona
char            * SPVGetZoneUniIDStr  ( SPV_ZONE * zone );
// Funzione per ottenere il RegID di una zona
unsigned __int64  SPVGetRegID         ( SPV_ZONE * zone );
// Funzione per generare una stringa del codice unico (UniID) di una regione
char            * SPVGetRegionUniIDStr( SPV_REGION * region );
// Funzione per caricare la definizione di una periferica
int               SPVLoadDevice       ( SPV_DEVICE * device, XML_ELEMENT * e_device, SPV_NODE * node );
// Funzione per eliminare una struttura periferica
int               SPVFreeDevice       ( SPV_DEVICE * device );
// Funzione per verificare la validita' di una periferica
bool              SPVValidDevice      ( SPV_DEVICE * device );
// Funzione per caricare la definizione di un nodo
int               SPVLoadNode         ( SPV_NODE * node, XML_ELEMENT * e_zone, SPV_ZONE * zone );
// Funzione per eliminare una struttura nodo
int               SPVFreeNode         ( SPV_NODE * node );
// Funzione per verificare la validita' di un nodo
bool              SPVValidNode        ( SPV_NODE * node );
// Funzione per caricare la definizione di una zona
int               SPVLoadZone         ( SPV_ZONE * zone, XML_ELEMENT * e_zone, SPV_REGION * region );
// Funzione per eliminare una struttura zona
int               SPVFreeZone         ( SPV_ZONE * zone );
// Funzione per verificare la valididta' di una zona
bool              SPVValidZone        ( SPV_ZONE * zone );
/// Funzione per caricare la definizione di una regione
int               SPVLoadRegion       ( SPV_REGION * region, XML_ELEMENT * e_region, SPV_SERVER * server );
// Funzione per verificare la validita' di una regione
bool              SPVValidRegion      ( SPV_REGION * region );
/// Funzione per eliminare una struttura regione
int               SPVFreeRegion       ( SPV_REGION * region );
/// Funzione per caricare la definizione di un server
int               SPVLoadServer       ( SPV_SERVER * server, XML_ELEMENT * e_server );
/// Funzione per eliminare la struttura di un server
int               SPVFreeServer       ( SPV_SERVER * server );
// Funzione per verificare la validita' di un server
bool              SPVValidServer      ( SPV_SERVER * server );
/// Funzione per caricare la struttura del sistema
int               SPVLoadSystem       ( SPV_SYSTEM * system, XML_ELEMENT * e_system, XML_ELEMENT * e_topography, bool multi_language );
/// Funzione per eliminare la struttura del sistema
int               SPVFreeSystem       ( SPV_SYSTEM * system );
/// Funzione per creare una struttura di sistema
int               SPVLoadSystemSource ( XML_SOURCE * system_source );
/// Funzione per cancellare una struttura di sistema
int               SPVFreeSystemSource ( XML_SOURCE * system_source );
/// Funzione per eseguire funzioni sui server del sistema
int               SPVDoOnServers      ( SPV_SYSTEM * system, SPV_SYSTEM_SERVER_FUNCTION_PTR function );
/// Funzione per eseguire funzioni sulle regioni del sistema
int               SPVDoOnRegions      ( SPV_SYSTEM * system, SPV_SYSTEM_REGION_FUNCTION_PTR function );
/// Funzione per eseguire funzioni sulle zone del sistema
int               SPVDoOnZones        ( SPV_SYSTEM * system, SPV_SYSTEM_ZONE_FUNCTION_PTR function );
/// Funzione per eseguire funzioni sui nodi del sistema
int               SPVDoOnNodes        ( SPV_SYSTEM * system, SPV_SYSTEM_NODE_FUNCTION_PTR function );
/// Funzione per eseguire funzioni sulle periferiche del sistema
int               SPVDoOnDevices      ( SPV_SYSTEM * system, SPV_SYSTEM_DEVICE_FUNCTION_PTR function );
/// Funzione per eseguire funzioni sulle periferiche del sistema
int               SPVDoOnDevices      ( SPV_SYSTEM * system, SPV_SYSTEM_VOID_DEVICE_FUNCTION_PTR function );
/// Funzione per eseguire funzioni sulle schede del sistema
int               SPVDoOnCards        ( SPV_SYSTEM * system, SPV_SYSTEM_CARD_FUNCTION_PTR function );
/// Funzione per caricare la configurazione dei database
int               SPVLoadDatabases    ( SPV_SYSTEM * system );
/// Funzione per eliminare dalla memoria la configurazione dei database
int               SPVClearDatabases   ( SPV_SYSTEM * system );
/// Funzione per verificare le tabelle del database
int               SPVCheckTables      ( SPV_SYSTEM * system );
/// Funzione per chiudere il modulo
void              SPVSystemClear      ( void );

#endif

//==============================================================================
// Telefin Device Object Layer Module 1.0
//------------------------------------------------------------------------------
// Modulo Layer Oggetti di Periferica (SPVDeviceObjectLayer.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.01 (01.03.2007 -> 01.03.2007)
//
// Copyright: 2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland C++ Builder 2006
// Autore:    Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVDeviceObjectLayer.h
//==============================================================================

#pragma hdrstop

#include "SPVDeviceObjectLayer.h"
#include "SPVSystem.h"
#include "XMLInterface.h"

#pragma package(smart_init)

//==============================================================================
/// Funzione per verificare la validita' di un oggetto di periferica
///
/// \date [01.03.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidDeviceObject( SPV_DEVICE_OBJECT * object )
{
	bool valid = false;

	try
	{
		if ( object != NULL )
		{
			if ( object->VCC == SPV_DEVICE_OBJECT_VALIDITY_CHECK_CODE ) {
				valid = true;
			}
		}
	}
	catch(...)
	{
		valid = false;
	}

	return valid;
}

//==============================================================================
/// Funzione per allocare una lista di oggetti di periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_DEVICE_OBJECT * SPVNewDeviceObjectList( void * device, int num )
{
	SPV_DEVICE_OBJECT * list 		= NULL;
	SPV_DEVICE_OBJECT * object 	= NULL;
	SPV_DEVICE				* pdevice = NULL;

	pdevice = (SPV_DEVICE*)device;

	if ( SPVValidDevice( pdevice ) )
	{
		if ( SPVValidDeviceObject( pdevice->ObjectList ) )
		{
			SPVClearDeviceObjectList( pdevice );
		}
		// --- Alloco la lista ---
		list = (SPV_DEVICE_OBJECT *)malloc( sizeof(SPV_DEVICE_OBJECT)*num );
		try
		{
			if ( list != NULL )
			{
			  // --- Azzero la memoria ---
				memset( list, 0, sizeof(SPV_DEVICE_OBJECT)*num );
				pdevice->ObjectList = list;
				pdevice->ObjectCount = num;
				for ( int i = 0; i < num; i++ )
				{
				  // --- Setto i parametri fondamentali dell'oggetto periferica ---
					object = &list[i];
					if ( object != NULL ) {
						object->VCC = SPV_DEVICE_OBJECT_VALIDITY_CHECK_CODE;
						object->Device	= device;
					}
				}
			}
		}
		catch(...)
		{
			list = NULL;
		}
	}

	return list;
}

//==============================================================================
/// Funzione per cancellare una lista di oggetti di periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDeleteDeviceObjectList( SPV_DEVICE_OBJECT * list )
{
	int ret_code = SPV_DEVICE_OBJECT_NO_ERROR;

	try
	{
		if ( SPVValidDeviceObject( list ) )
		{
			try
			{
				free( list );
			}
			catch(...)
			{
				ret_code = SPV_DEVICE_OBJECT_FREE_LIST_FAILURE;
			}
		}
		else
		{
			ret_code = SPV_DEVICE_OBJECT_INVALID_OBJECT_LIST;
		}
	}
	catch (...)
	{
		ret_code = SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare una lista di oggetti di periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVLoadDeviceObjectList( void * device )
{
	int 				      	ret_code 			= SPV_DEVICE_OBJECT_NO_ERROR;
	SPV_DEVICE	      * pdevice 			= NULL;
	XML_ELEMENT	      * device_def		= NULL;
	XML_ELEMENT	      * objects_def		= NULL;
	XML_ELEMENT	      * item_def			= NULL;
	int					      	object_count	= 0;
	char			        * object_name		= NULL;
	int					      	object_type		= 0;
	SPV_DEVICE_OBJECT * object_list		= NULL;
	SPV_DEVICE_OBJECT * object_item		= NULL;

	pdevice = (SPV_DEVICE*)device;

	try
	{
		if ( SPVValidDevice( pdevice ) )
		{
			device_def = (XML_ELEMENT*)pdevice->Def;
			if ( device_def != NULL )
			{
				objects_def = XMLGetNext( device_def->child, "objects", -1 );
				if ( objects_def != NULL )
				{
					object_count = XMLGetChildCount( objects_def, false );
					if ( object_count > 0 )
					{
						object_list = SPVNewDeviceObjectList( pdevice, object_count );
						if ( SPVValidDeviceObject( object_list ) )
						{
							for ( int i = 0; i < object_count; i++ )
							{
								try
								{
									object_item = &object_list[i];
									item_def = XMLGetNext( objects_def->child, "item", i );
									if ( item_def != NULL )
									{
										// --- Recupero i parametri dell'oggetto ---
										object_name = XMLGetValue( item_def, "name" );
										object_type = XMLGetValueInt( item_def, "type" );
										// --- Salvo i parametri dell'oggetto ---
										object_item->ID				= i;
										object_item->Name 		= XMLStrCpy( NULL, object_name );
										object_item->Type			= object_type;
										object_item->Present 	= true;
										object_item->Severity = 5;
										object_item->Def			= item_def;
									}
									else
									{
										object_item->ID				= i;
										object_item->Present 	= false;
										object_item->Severity = 5;
									}
								}
								catch(...)
								{
									/* TODO -oEnrico -cErrors : Segnalare questa eccezione */
								}
							}
							pdevice->ObjectList = object_list;
							pdevice->ObjectCount = object_count;
						}
						else
						{
							ret_code = SPV_DEVICE_OBJECT_ALLOCATION_FAILURE;
						}
					}
					else
					{
						ret_code = SPV_DEVICE_OBJECT_NO_ITEM_FOUND;
					}
				}
				else
				{
					ret_code = SPV_DEVICE_OBJECT_ELEMENT_NOT_FOUND;
				}
			}
			else
			{
				ret_code = SPV_DEVICE_OBJECT_INVALID_DEVICE_DEFINITION;
			}
		}
		else
		{
			ret_code = SPV_DEVICE_OBJECT_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per pulire una lista di oggetti di periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVClearDeviceObjectList( void * device )
{
	int 							            	ret_code 		  = SPV_DEVICE_OBJECT_NO_ERROR;
	SPV_DEVICE				            * pdevice 		  = NULL;
	SPV_DEVICE_OBJECT	            * object_list   = NULL;
	int								            	object_count	= 0;
	SPV_DEVICE_OBJECT							* object_item		= NULL;

	pdevice = (SPV_DEVICE*)device;

	try
	{
		if ( SPVValidDevice( pdevice ) )
		{
			object_list 	= pdevice->ObjectList;
			object_count	= pdevice->ObjectCount;
			if ( SPVValidDeviceObject( object_list ) && object_count > 0 )
			{
				// --- Cancello parametri e casi dei signoli oggetti ---
				for ( int i = 0; i < object_count; i++ )
				{
					object_item = &object_list[i];
					// --- Cancello name ---
					if ( object_item->Name != NULL ) free( object_item->Name );
					// --- Cancello lista casi ---
					SPVClearStatusCaseList( object_item );
				}
				// --- Cancello la lista degli oggetti ---
				try
				{
					memset( object_list, 0, sizeof(SPV_DEVICE_OBJECT) * object_count );
					ret_code = SPVDeleteDeviceObjectList( object_list );
					pdevice->ObjectCount = 0;
					pdevice->ObjectList = NULL;
				}
				catch(...)
				{
					ret_code = SPV_DEVICE_OBJECT_MEMSET_FAILURE;
				}
			}
		}
		else
		{
			ret_code = SPV_DEVICE_OBJECT_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di un caso stato per un oggetto
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
bool SPVValidStatusCase( SPV_DEVICE_OBJECT_STATUS_CASE * status_case )
{
	bool valid = false;

	try
	{
		if ( status_case != NULL )
		{
			if ( status_case->VCC == SPV_STATUS_CASE_VALIDITY_CHECK_CODE )
			{
				valid = true;
			}
		}
	}
	catch(...)
	{
		valid = false;
	}

	return valid;
}

//==============================================================================
/// Funzione per allocare una lista di casi di stato per un oggetto periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_DEVICE_OBJECT_STATUS_CASE * SPVNewStatusCaseList( SPV_DEVICE_OBJECT * object, int num )
{
	SPV_DEVICE_OBJECT_STATUS_CASE * status_list	= NULL;
	SPV_DEVICE_OBJECT_STATUS_CASE * status_case	= NULL;

	if ( SPVValidDeviceObject( object ) )
	{
		if ( SPVValidStatusCase( object->StatusCaseList ) )
		{
			SPVClearStatusCaseList( object );
		}
		// --- Alloco la lista ---
		status_list = (SPV_DEVICE_OBJECT_STATUS_CASE *)malloc( sizeof(SPV_DEVICE_OBJECT_STATUS_CASE)*num );
		try
		{
			if ( status_list != NULL )
			{
				// --- Azzero la memoria ---
				memset( status_list, 0, sizeof(SPV_DEVICE_OBJECT_STATUS_CASE)*num );
				object->StatusCaseList = status_list;
				object->StatusCaseCount = num;
				for ( int i = 0; i < num; i++ )
				{
					// --- Setto i parametri fondamentali del caso oggetto ---
					status_case = &status_list[i];
					if ( status_case != NULL ) {
						status_case->VCC = SPV_STATUS_CASE_VALIDITY_CHECK_CODE;
						status_case->DeviceObject = (void*)object;
					}
				}
			}
		}
		catch(...)
		{
			status_list = NULL;
		}
	}

	return status_list;
}

//==============================================================================
/// Funzione per cancellare una lista di casi di stato per un oggetto periferica
///
/// \date [01.03.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDeleteStatusCaseList( SPV_DEVICE_OBJECT_STATUS_CASE * list )
{
	int ret_code = SPV_DEVICE_OBJECT_NO_ERROR;

	try
	{
		if ( SPVValidStatusCase( list ) )
		{
			try
			{
				free( list );
			}
			catch(...)
			{
				ret_code = SPV_DEVICE_OBJECT_FREE_LIST_FAILURE;
			}
		}
		else
		{
			ret_code = SPV_DEVICE_OBJECT_INVALID_STATUS_CASE_LIST;
		}
	}
	catch(...)
	{
		ret_code = SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare una lista di casi di stato per un oggetto periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	SPVLoadStausCaseList( SPV_DEVICE_OBJECT * object )
{
	int 										      	ret_code 			= SPV_DEVICE_OBJECT_NO_ERROR;
	XML_ELEMENT	      			      * object_def		= NULL;
	XML_ELEMENT	      			      * status_def		= NULL;
	SPV_DEVICE_OBJECT_STATUS_CASE * status_list		= NULL;
	SPV_DEVICE_OBJECT_STATUS_CASE	* status_item		= NULL;
	int											      	status_count	= 0;
	int											      	status_sev		= 0;
	int											      	status_msg_id	= 0;
	char										      *	status_msg		= NULL;
	int											      	status_rule		= 0;

	try
	{
		if ( SPVValidDeviceObject( object ) )
		{
			object_def = (XML_ELEMENT*)object->Def;
			if ( object_def != NULL )
			{
				status_count = XMLGetChildCount( object_def, false );
				if ( status_count > 0 )
				{
					status_list = SPVNewStatusCaseList( object, status_count );
					if ( SPVValidStatusCase( status_list ) )
					{
						for ( int i = 0; i < status_count; i++ )
						{
							try
							{
								status_item = &status_list[i];
								status_def = XMLGetNext( object_def->child, "case", i );
								if ( status_def != NULL )
								{
									// --- Recupero i parametri del caso stato ---
									status_sev		= XMLGetValueInt( status_def, "severity" );
									status_msg_id	=	XMLGetValueInt( status_def, "msg" );
									status_msg		= XMLGetValue( status_def, "des" );
									status_rule		= XMLGetValueInt( status_def, "rule" );
									// --- Salvo i parametri del caso stato ---
									status_item->ID					= i;
									status_item->RuleID			= status_rule;
									status_item->MessageID	= status_msg_id;
									status_item->Message		= XMLStrCpy( NULL, status_msg );
									status_item->Used				= true;
									status_item->Visible		= true;
									status_item->Severity		= status_sev;
									status_item->Def				= status_def;
								}
								else
								{
									status_item->ID				= i;
									status_item->Used 		= false;
								}
							}
							catch(...)
							{
								/* TODO -oEnrico -cErrors : Segnalare questa eccezione */
							}
						}
						object->StatusCaseList	= status_list;
						object->StatusCaseCount	= status_count;
					}
					else
					{
						ret_code = SPV_DEVICE_OBJECT_ALLOCATION_FAILURE;
					}
				}
				else
				{
					ret_code = SPV_DEVICE_OBJECT_NO_ITEM_FOUND;
				}
			}
			else
			{
				ret_code = SPV_DEVICE_OBJECT_ELEMENT_NOT_FOUND;
			}
		}
		else
		{
			ret_code = SPV_DEVICE_OBJECT_INVALID_OBJECT;
		}
	}
	catch(...)
	{
		ret_code = SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per pulire una lista di casi di stato per un oggetto periferica
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	SPVClearStatusCaseList( SPV_DEVICE_OBJECT	* object )
{
	int 														ret_code 			= SPV_DEVICE_OBJECT_NO_ERROR;
	SPV_DEVICE_OBJECT_STATUS_CASE * status_list 	= NULL;
	int															status_count 	= 0;
	SPV_DEVICE_OBJECT_STATUS_CASE * status_item 	= NULL;

	try
	{
		if ( SPVValidDeviceObject( object ) )
		{
			status_list 	= object->StatusCaseList;
			status_count	= object->StatusCaseCount;
			if ( SPVValidStatusCase( status_list ) && status_count > 0 )
			{
				try
				{
					// --- Cancello parametri dei signoli casi ---
					for ( int i = 0; i < status_count; i++ )
				  {
						status_item = &status_list[i];
						// --- Cancello message ---
						if ( status_item->Message != NULL ) free( status_item->Message );
					}
					memset( status_list, 0, sizeof(SPV_DEVICE_OBJECT_STATUS_CASE) * status_count );
					ret_code = SPVDeleteStatusCaseList( status_list );
					object->StatusCaseCount	= 0;
					object->StatusCaseList 	= NULL;
				}
				catch(...)
				{
					ret_code = SPV_DEVICE_OBJECT_MEMSET_FAILURE;
				}
			}
		}
		else
		{
			ret_code = SPV_DEVICE_OBJECT_INVALID_OBJECT;
		}
	}
	catch(...)
	{
		ret_code = SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

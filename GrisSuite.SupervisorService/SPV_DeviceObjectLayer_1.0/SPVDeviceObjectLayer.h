//==============================================================================
// Telefin Device Object Layer Module 1.0
//------------------------------------------------------------------------------
// Header Modulo Layer Oggetti di Periferica (SPVDeviceObjectLayer.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.01 (01.03.2007 -> 01.03.2007)
//
// Copyright: 2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland C++ Builder 2006
// Autore:    Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVDeviceObjectLayer.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [01.03.2007]:
// - Prima versione del modulo.
//==============================================================================

#ifndef SPVDeviceObjectLayerH
#define SPVDeviceObjectLayerH
//---------------------------------------------------------------------------

#define SPV_DEVICE_OBJECT_VALIDITY_CHECK_CODE			2043349573
#define SPV_STATUS_CASE_VALIDITY_CHECK_CODE				2042513123

#define SPV_DEVICE_OBJECT_NO_ERROR						0
#define	SPV_DEVICE_OBJECT_GENERIC_OBJECT				39000
#define SPV_DEVICE_OBJECT_INVALID_OBJECT				39001
#define SPV_DEVICE_OBJECT_INVALID_OBJECT_LIST			39002
#define SPV_DEVICE_OBJECT_GENERIC_FUNCTION_EXCEPTION	39003
#define SPV_DEVICE_OBJECT_FREE_LIST_FAILURE				39004
#define SPV_DEVICE_OBJECT_INVALID_DEVICE				39005
#define SPV_DEVICE_OBJECT_NO_ITEM_FOUND					39006
#define SPV_DEVICE_OBJECT_INVALID_DEVICE_DEFINITION		39007
#define SPV_DEVICE_OBJECT_ELEMENT_NOT_FOUND				39008
#define SPV_DEVICE_OBJECT_ALLOCATION_FAILURE			39009
#define SPV_DEVICE_OBJECT_MEMSET_FAILURE				39010
#define SPV_DEVICE_OBJECT_INVALID_STATUS_CASE_LIST		39011

//==============================================================================
// Definizione strutture e tipi
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura dati per caso stato di oggetto periferica.
///
/// \date [02.03.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_OBJECT_STATUS_CASE
{
	unsigned __int32    VCC				; // Validity Check Code
	int                 ID				; // Codice numerico identificativo
	int					RuleID			; // ID della regola associata al caso
	int					MessageID		; // ID del messaggio
	char*				Message			; // Messaggio
	bool				Used			; // Flag USATO
	bool				Visible			; // Flag VISIBLE
	int					Severity		; // Severita' complessiva dell'oggetto
	void*				DeviceObject	; // Ptr all'oggetto di appartenenza
	void*				Def				; // Elemento XML definizione caso
	void*				PopUpMenu		; // Ptr al pop-up menu della periferica
	void*				Tag				; // Puntatore di servizio
}
SPV_DEVICE_OBJECT_STATUS_CASE;

//==============================================================================
/// Struttura dati per oggetto di periferica.
///
/// \date [01.03.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DEVICE_OBJECT
{
	unsigned __int32                VCC             ; // Validity Check Code
	int                             ID              ; // Codice numerico identificativo
	char                          * Name            ; // Nome dell'entit�
	int                             Type			; // Tipo di oggetto
	bool							Present			; // Flag di presenza dell'oggetto
	int								Severity		; // Severita' complessiva dell'oggetto
	SPV_DEVICE_OBJECT_STATUS_CASE*	StatusCaseList	; //
	int								StatusCaseCount	; //
	void*							Device			; // Ptr alla periferica di appartenenza
	void*							Def				; // Elemento XML definizione oggetto
	void*							PopUpMenu       ; // Ptr al pop-up menu della periferica
	void*							Tag             ; // Puntatore di servizio
}
SPV_DEVICE_OBJECT;

//==============================================================================
// Prototipi Funzioni
//------------------------------------------------------------------------------

// Funzione per verificare la validita' di un oggetto di periferica
bool								SPVValidDeviceObject		( SPV_DEVICE_OBJECT * object );
// Funzione per allocare una lista di oggetti di periferica
SPV_DEVICE_OBJECT*					SPVNewDeviceObjectList		( void * device, int num );
// Funzione per cancellare una lista di oggetti di periferica
int									SPVDeleteDeviceObjectList	( SPV_DEVICE_OBJECT * list );
// Funzione per caricare una lista di oggetti di periferica
int									SPVLoadDeviceObjectList		( void * device );
// Funzione per pulire una lista di oggetti di periferica
int									SPVClearDeviceObjectList	( void * device );
// Funzione per verificare la validita' di un caso stato per un oggetto
bool								SPVValidStatusCase			( SPV_DEVICE_OBJECT_STATUS_CASE * status_case );
// Funzione per allocare una lista di casi di stato per un oggetto periferica
SPV_DEVICE_OBJECT_STATUS_CASE*		SPVNewStatusCaseList		( SPV_DEVICE_OBJECT * object, int num );
// Funzione per cancellare una lista di casi di stato per un oggetto periferica
int									SPVDeleteStatusCaseList		( SPV_DEVICE_OBJECT_STATUS_CASE * list );
// Funzione per caricare una lista di casi di stato per un oggetto periferica
int									SPVLoadStausCaseList		( SPV_DEVICE_OBJECT * object );
// Funzione per pulire una lista di casi di stato per un oggetto periferica
int									SPVClearStatusCaseList		( SPV_DEVICE_OBJECT	* object );

#endif

//------------------------------------------------------------------------------
// Telefin Supervisor Event Log Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO GESTIONE LOG EVENTI (SPVEventLog.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.10 (21.07.2004 -> 07.10.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVComProt.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [21.07.2004]:
// - Prima versione prototipo del modulo.
// 0.01 [26.07.2004]:
// - Modificata la funzione SPVPrintEventLog.
// - Aggiunto il codice di errore SPV_EVENTLOG_NOT_OPENED.
// 0.02 [02.08.2004]:
// - Aggiunto il codice di template SPV_EVENTLOG_TEMPLATE_COMPORT.
// - Aggiunta la funzione SPVFormatEventLog.
// 0.03 [03.08.2004]:
// - Rinominata la funzione SPVFormatEventLog in SPVFormatEventLogLine.
// - Modificata la funzione SPVFormatEventLogLine.
// - Aggiunta la funzione SPVGetEventLogLinesCount.
// - Modificata la funzione SPVPrintEventLog.
// - Modificata la funzione SPVCloseEventLog.
// - Aggiunto il codice di errore SPV_EVENTLOG_INVALID_LENGTH.
// 0.04 [04.08.2004]:
// - Corretta la funzione SPVFormatEventLogLine.
// 0.05 [13.08.2004]:
// - Corretta la disallocazione nella funzione SPVPrintEventLog.
// - Corretta la disallocazione nella funzione SPVFormatEventLogLine.
// - Corretta la disallocazione nella funzione SPVNewEventLog.
// 0.06 [17.02.2005]:
// - Rinominata la funzione SPVInitEventLog in SPVEventLogInit.
// - Modificata la funzione SPVEventLogInit.
// - Aggiunto il template di EventLog SPV_EVENTLOG_TEMPLATE_DATETIME.
// 0.07 [29.09.2005]:
// - Incluso il modulo SYSKernel 1.0.0.29.
// - Aggiunto il tipo di EventLog SPV_EVENTLOG_TYPE_MSWIN_LOG.
// - Modificata la funzione SPVNewEventLog.
// - Modificata la funzione SPVDeleteEventLog.
// - Modificata la funzione SPVOpenEventLog.
// - Modificata la funzione SPVCloseEventLog.
// - Modificata la funzione SPVPrintEventLog.
// 0.08 [30.09.2005]:
// - Modificata la funzione SPVPrintEventLog.
// 0.09 [04.10.2005]:
// - Modificata la funzione SPVPrintEventLog.
// 0.10 [07.10.2005]:
// - Modificata la funzione SPVOpenEventLog.
//------------------------------------------------------------------------------
#ifndef SPVEventLogH
#define SPVEventLogH
//------------------------------------------------------------------------------

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_EVENTLOG_NO_ERROR             0
#define SPV_EVENTLOG_UNKNOWN_ERROR        39000
#define SPV_EVENTLOG_INVALID_ID           39001
#define SPV_EVENTLOG_NOT_FOUND            39002
#define SPV_EVENTLOG_INVALID_TYPE         39003
#define SPV_EVENTLOG_NOT_OPENED           39004
#define SPV_EVENTLOG_INVALID_LENGTH       39005
#define SPV_EVENTLOG_FORMAT_ERROR         39006
#define SPV_EVENTLOG_FILE_CLOSE_FAILURE   39007
#define SPV_EVENTLOG_FILE_OPEN_FAILURE    39008
#define SPV_EVENTLOG_INVALID_FILE_HANDLER 39009
#define SPV_EVENTLOG_FILE_WRITE_FAILURE   39010
//------------------------------------------------------------------------------

//==============================================================================
// Macro
//------------------------------------------------------------------------------
#define SPV_EVENTLOG_MAX_MANAGER          25
#define SPV_EVENTLOG_MEMO_MAX_LINES       25
#define SPV_EVENTLOG_TYPE_UNKNOWN         0x0100
#define SPV_EVENTLOG_TYPE_MEMO            0x0101
#define SPV_EVENTLOG_TYPE_ASCII_FILE      0x0102
#define SPV_EVENTLOG_TYPE_MSWIN_LOG       0x0103
#define SPV_EVENTLOG_TEMPLATE_NONE        0x0201
#define SPV_EVENTLOG_TEMPLATE_COMPORT     0x0202
#define SPV_EVENTLOG_TEMPLATE_DATETIME    0x0203
#define SPV_EVENTLOG_DEFAULT_MSWIN_LOG    "GrisSuite.SupervisorService" //Era "Supervisor Server 1.0"
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura dati per un manager di log eventi.
///
/// \date [21.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_EVENTLOG_MANAGER
{
  int     ID          ; // Identificatore numerico
  char  * Name        ; // Nome dell'EventLog Manager
  bool    Opened      ; // Flag di EventLog Manager aperto
  int     Type        ; // Tipo di EventLog manager
  void  * Handler     ; // Handler dell'oggetto di destinazione
  int     TemplateID  ; // ID di un eventuale template
}
SPV_EVENTLOG_MANAGER;

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
extern SPV_EVENTLOG_MANAGER  SPVEventLogManagerList        [SPV_EVENTLOG_MAX_MANAGER];
extern int                   SPVEventLogManagerCount                                 ;
extern int                   SPVEventLogNextID                                       ;

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

/// Funzione per inizializzare le variabili del modulo
void                    SPVEventLogInit           ( void    );
/// Funzione per ottenere la prima cella libera per un nuovo EventLog Manager
int                     SPVGetFirstFreeEventLog   ( void    );
/// Funzione per creare un nuovo EventLog Manager
SPV_EVENTLOG_MANAGER  * SPVNewEventLog            ( int type, int temID, void * name );
/// Funzione per eliminare un EventLog Manager
int                     SPVDeleteEventLog         ( int ID  );
/// Funzione per ottenere un EventLog Manager
SPV_EVENTLOG_MANAGER  * SPVGetEventLog            ( int ID  );
/// Funzione per aprire un EventLog Manager
int                     SPVOpenEventLog           ( int ID  );
/// Funzione per chiudere un EventLog Manager
int                     SPVCloseEventLog          ( int ID  );
/// Funzione per stampare una stringa sull'EventLog
int                     SPVPrintEventLog          ( int ID, char * str, int len, int params );
/// Funzione per stampare una N.T. stringa sull'EventLog
int                     SPVPrintEventLog          ( int ID, char * str );
/// Funzione per ottenere il numero di righe per un EventLog
int                     SPVGetEventLogLinesCount  ( int temID, int len );
/// Funzione per formattare una riga per l'EventLog
char *                  SPVFormatEventLogLine     ( int temID, char * str, int len, int line, int params );
/// Funzione per Sovvrascivere una stringa sull'EventLog
int 					SPVSubscribeEventLog	  ( int ID, char * str, int len, int params );

//------------------------------------------------------------------------------
#endif

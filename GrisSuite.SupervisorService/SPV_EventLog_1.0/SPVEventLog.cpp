//------------------------------------------------------------------------------
// Telefin Supervisor Event Log Module 1.0
//------------------------------------------------------------------------------
// MODULO GESTIONE LOG EVENTI (SPVEventLog.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	0.10 (21.07.2004 -> 07.10.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVComProt.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVComProt.h
//------------------------------------------------------------------------------
#pragma hdrstop
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <alloc.h>
#include <math.h>
#include "SPVEventLog.h"
#include "cnv_lib.h"
#include <time.h>
#include "SYSKernel.h"

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#pragma package(smart_init)
//------------------------------------------------------------------------------

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
SPV_EVENTLOG_MANAGER  SPVEventLogManagerList        [SPV_EVENTLOG_MAX_MANAGER];
int                   SPVEventLogManagerCount                                 ;
int                   SPVEventLogNextID                                       ;

//==============================================================================
/// Funzione per inizializzare le variabili del modulo
///
/// In caso di errore restituisce NULL
///
/// \date [17.02.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void SPVEventLogInit( void )
{
  SPVEventLogManagerCount = 0;
  SPVEventLogNextID       = 1; // Gli ID degli EventLog Manager partono da 1 (ID=0 significa vuoto)
  memset( &SPVEventLogManagerList[0], 0x00, sizeof(SPV_EVENTLOG_MANAGER)*SPV_EVENTLOG_MAX_MANAGER );
}

//==============================================================================
/// Funzione per ottenere la prima cella libera per un nuovo EventLog Manager
///
/// Restituisce l'indice della cella libera nell'array globale degli EventLog
/// Manager.
/// In caso di errore restituisce -1
///
/// \date [17.02.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetFirstFreeEventLog( void )
{
  int                     cell_id   = -1  ; // Indice della cella
  SPV_EVENTLOG_MANAGER  * eventlog  = NULL; // Ptr a EventLog Manager

  for ( int c=0; c<SPV_EVENTLOG_MAX_MANAGER; c++ )
  {
    eventlog = &SPVEventLogManagerList[c];
    if ( eventlog != NULL )
    {
      if ( eventlog->ID == 0 ) // Se l'ID = 0 significa che la cella � libera
      {
        cell_id = c; // Mi copio l'indice della cella vuota
        break; // Fine del ciclo
      }
    }
  }

  return cell_id;
}

//==============================================================================
/// Funzione per creare un nuovo EventLog Manager
///
/// In caso di errore restituisce NULL
///
/// \date [29.09.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
SPV_EVENTLOG_MANAGER * SPVNewEventLog( int type, int temID, void * name )
{
  SPV_EVENTLOG_MANAGER  * eventlog      = NULL; // Ptr a EventLog Manager
  int                     eventlog_cell = -1  ; // ID della cella
  TMemo                 * memo          = NULL; // Ptr a oggetto Memo
  char                  * eventlog_name = NULL; // Nome dell'EventLog Manager
  int                     name_len      = 0   ; // Lunghezza del nome

  if ( type != SPV_EVENTLOG_TYPE_UNKNOWN ) // Se il tipo � valido
  {
    eventlog_cell = SPVGetFirstFreeEventLog( );
    if ( eventlog_cell != -1 )
    {
      eventlog =  &SPVEventLogManagerList[eventlog_cell];
    }
  }

  if ( eventlog != NULL )
  {
    switch ( type ) // Switcho sul tipo di EventLog Manager
    {
      // --- Caso EventLog tipo Memo ---
      case SPV_EVENTLOG_TYPE_MEMO:
        eventlog->ID          = SPVEventLogNextID;
        eventlog->Opened      = false;
        eventlog->Type        = type;
        eventlog->Handler     = name;
        eventlog->TemplateID  = temID;
        memo                  = (TMemo*)name;
        if ( memo != NULL )
        {
          name_len            = strlen( memo->Name.c_str() );
          eventlog->Name      = (char*)malloc( sizeof(char)*(name_len+1) );
          strcpy( eventlog->Name, memo->Name.c_str() );
        }
        else
        {
          name_len            = 4;
          eventlog->Name      = (char*)malloc( sizeof(char)*(name_len+1) );
          strcpy( eventlog->Name, "Memo" );
        }
        SPVEventLogManagerCount++;
        SPVEventLogNextID++;
      break;
      // --- Caso EventLog tipo file di testo ASCII ---
      case SPV_EVENTLOG_TYPE_ASCII_FILE:
        eventlog->ID          = SPVEventLogNextID;
        eventlog->Opened      = false;
        eventlog->Type        = type;
        eventlog->Handler     = NULL;
        eventlog_name         = (char*)name;
        if ( eventlog_name != NULL )
        {
          name_len            = strlen( eventlog_name );
          eventlog->Name      = (char*)malloc( sizeof(char)*(name_len+1) );
          strcpy( eventlog->Name, eventlog_name );
        }
        else
        {
          name_len            = 4;
          eventlog->Name      = (char*)malloc( sizeof(char)*(name_len+1) );
          strcpy( eventlog->Name, "ASCII" );
        }
        eventlog->TemplateID  = temID;
        SPVEventLogManagerCount++;
        SPVEventLogNextID++;
      break;
      // --- Caso EventLog tipo Lod di MicrosoftWindows ---
      case SPV_EVENTLOG_TYPE_MSWIN_LOG:
        eventlog->ID          = SPVEventLogNextID;
        eventlog->Opened      = false;
        eventlog->Type        = type;
        eventlog->Handler     = NULL;
        if ( name == NULL )
        {
          eventlog->Name      = SPV_EVENTLOG_DEFAULT_MSWIN_LOG;
        }
        else
        {
          eventlog->Name      = (char*)name;
        }
        eventlog->TemplateID  = temID;
        SPVEventLogManagerCount++;
        SPVEventLogNextID++;
      break;
      // --- Caso non definito ---
      default:
        free( eventlog );
        eventlog = NULL;
    }
  }

  return eventlog;
}

//==============================================================================
/// Funzione per eliminare un EventLog Manager
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [01.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDeleteEventLog( int ID )
{
  int                     ret_code      = SPV_EVENTLOG_NO_ERROR ; // Codice di ritorno
  SPV_EVENTLOG_MANAGER  * eventlog      = NULL                  ; // Ptr a EventLog Manager
  FILE                  * file_handler  = NULL                  ; // Handler del file

  if ( ID > 0 ) // Controllo la validit� dell'ID (deve essere positivo e diverso da 0)
  {
	eventlog = SPVGetEventLog( ID ); // Ricavo il ptr della struttura EventLog
	if ( eventlog != NULL ) // Se ho trovato l'EventLog
	{
	  switch ( eventlog->Type ) // Switcho in base al tipo di EventLog
	  {
		// --- Caso EventLog tipo Memo ---
		case SPV_EVENTLOG_TYPE_MEMO:
		  memset( eventlog, 0x00, sizeof(SPV_EVENTLOG_MANAGER) ); // Cancello la memoria occupata dall'EventLog Manager
		  SPVEventLogManagerCount--;
        break;
        // --- Caso EventLog tipo file di testo ASCII ---
        case SPV_EVENTLOG_TYPE_ASCII_FILE:
          file_handler = (FILE*)eventlog->Handler;
          if ( file_handler != NULL )
          {
            fclose( file_handler ); // Chiudo il file
          }
		  memset( eventlog, 0x00, sizeof(SPV_EVENTLOG_MANAGER) ); // Cancello la memoria occupata dall'EventLog Manager
          SPVEventLogManagerCount--;
        break;
        // --- Caso EventLog tipo Lod di MicrosoftWindows ---
        case SPV_EVENTLOG_TYPE_MSWIN_LOG:
          SPVEventLogManagerCount--;
        break;
        // --- Caso non definito ---
        default:
          ret_code = SPV_EVENTLOG_INVALID_TYPE;
      }
    }
    else
    {
      ret_code = SPV_EVENTLOG_NOT_FOUND;
    }
  }
  else
  {
    ret_code = SPV_EVENTLOG_INVALID_ID;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere un EventLog Manager
///
/// In caso di errore restituisce NULL.
///
/// \date [17.02.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_EVENTLOG_MANAGER * SPVGetEventLog( int ID )
{
  SPV_EVENTLOG_MANAGER * eventlog = NULL; // Ptr a EventLog Manager

  if ( ID > 0 ) // Controllo la validit� dell'ID
  {
    for ( int l=0; l<SPVEventLogManagerCount; l++ )
    {
      if ( SPVEventLogManagerList[l].ID == ID )
      {
        eventlog  = &SPVEventLogManagerList[l];
        l         = SPVEventLogManagerCount; // Fine del ciclo
      }
    }
  }

  return eventlog;
}

//==============================================================================
/// Funzione per aprire un EventLog Manager
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [07.10.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVOpenEventLog( int ID )
{
  int                     ret_code      = 0    ; // Codice di ritorno
  SPV_EVENTLOG_MANAGER  * eventlog      = NULL ; // Ptr a EventLog Manager
  FILE                  * file_handler  = NULL ; // Handler del file
  void                  * source        = NULL ; // Ptr di appoggio

  if ( ID > 0 ) // Controllo la validit� dell'ID
  {
    eventlog = SPVGetEventLog(ID);
    if ( eventlog != NULL ) // Se ho trovato l'EventLog
    {
      switch ( eventlog->Type )
      {
        // --- Caso EventLog tipo Memo ---
        case SPV_EVENTLOG_TYPE_MEMO:
          eventlog->Opened = true;
        break;
        // --- Caso EventLog tipo file di testo ASCII ---
        case SPV_EVENTLOG_TYPE_ASCII_FILE:
          file_handler = fopen( eventlog->Name, "a+" );
          if ( file_handler != NULL )
          {
            eventlog->Opened    = true;
            eventlog->Handler   = (void*)file_handler;
          }
          else
          {
            eventlog->Opened    = false;
            eventlog->Handler   = NULL;
            ret_code = SPV_EVENTLOG_FILE_OPEN_FAILURE;
          }
        break;
        // --- Caso EventLog tipo Lod di Microsoft Windows ---
        case SPV_EVENTLOG_TYPE_MSWIN_LOG:
          source = SYSEventLogRegisterSource( eventlog->Name );
          if ( source != NULL )
          {
            eventlog->Opened    = true;
            eventlog->Handler = source;
          }
          else
          {
            eventlog->Opened    = false;
            eventlog->Handler   = NULL;
            ret_code = SPV_EVENTLOG_FILE_OPEN_FAILURE;
          }
        break;
        // --- Caso non definito ---
        default:
          ret_code = SPV_EVENTLOG_INVALID_TYPE;
      }
    }
    else
    {
      ret_code = SPV_EVENTLOG_NOT_FOUND;
    }
  }
  else
  {
    ret_code = SPV_EVENTLOG_INVALID_ID;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere un EventLog Manager
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [29.09.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVCloseEventLog( int ID )
{
  int                     ret_code      = SPV_EVENTLOG_NO_ERROR; // Codice di ritorno
  SPV_EVENTLOG_MANAGER  * eventlog      = NULL ; // Ptr a EventLog Manager
  FILE                  * file_handler  = NULL ; // Handler del file

  if ( ID > 0 ) // Controllo la validit� dell'ID
  {
	eventlog = SPVGetEventLog( ID );
	if ( eventlog != NULL ) // Se ho trovato l'EventLog
	{
	  switch (eventlog->Type)
	  {
		// --- Caso EventLog tipo Memo ---
		case SPV_EVENTLOG_TYPE_MEMO:
		  eventlog->Opened = false;
		break;
		// --- Caso EventLog tipo file di testo ASCII ---
		case SPV_EVENTLOG_TYPE_ASCII_FILE:
		  file_handler = (FILE*) eventlog->Handler;
		  if ( file_handler != NULL )
		  {
			if ( fclose( file_handler ) != 0 ) ret_code = SPV_EVENTLOG_FILE_CLOSE_FAILURE;
		  }
		  else
		  {
			ret_code = SPV_EVENTLOG_INVALID_FILE_HANDLER;
		  }
		  eventlog->Opened = false;
		  eventlog->Handler = NULL;
		break;
		// --- Caso EventLog tipo Lod di Microsoft Windows ---
		case SPV_EVENTLOG_TYPE_MSWIN_LOG:
		  if ( eventlog->Handler != NULL )
		  {
			if ( SYSEventLogDeregisterSource( eventlog->Handler ) != SPV_EVENTLOG_NO_ERROR )
			{
			  ret_code = SPV_EVENTLOG_FILE_CLOSE_FAILURE;
			}
		  }
		  else
		  {
			ret_code = SPV_EVENTLOG_INVALID_FILE_HANDLER;
		  }
		  eventlog->Opened  = false;
		  eventlog->Handler = NULL;
		break;
		// --- Caso non definito ---
		default:
		  ret_code = SPV_EVENTLOG_INVALID_TYPE;
	  }
	}
	else
	{
	  ret_code = SPV_EVENTLOG_NOT_FOUND;
	}
  }
  else
  {
	ret_code = SPV_EVENTLOG_INVALID_ID;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per stampare una stringa sull'EventLog
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [04.10.2005]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int SPVPrintEventLog( int ID, char * str, int len, int params )
{
  int                     ret_code      = SPV_EVENTLOG_NO_ERROR; // Codice di ritorno
  SPV_EVENTLOG_MANAGER  * eventlog      = NULL; // Ptr a EventLog Manager
  TMemo                 * memo          = NULL; // Ptr a un TMemo
  FILE                  * file_handler  = NULL; // Handler del file
  int                     line_num      = 0   ; // Numero di righe
  char                  * line          = NULL; // Ptr ad una righe
  int                     wl_category   = 0   ;
  int                     wl_ID         = 0   ;
  int                     wl_sevlevel   = 0   ;
  int                     wl_sevcode    = 0   ;
  int                     wl_str_num    = 0   ;
  int                     wl_str_count  = 0   ;
  char                 ** wl_str_list   = NULL;
  char                  * wl_str_single = NULL; // Singola stringa

  if ( ID > 0 ) // Controllo la validit� dell'ID
  {
    eventlog = SPVGetEventLog( ID );
    if ( eventlog != NULL ) // Se ho trovato l'EventLog
    {
      if ( eventlog->Opened )
      {
        line_num = SPVGetEventLogLinesCount( eventlog->TemplateID, len );
        if ( line_num > 0 ) // Se ho un numero di righe accettabile
        {
          for ( int l=0; l<line_num; l++ ) // Ciclo sulle righe
          {
            line = SPVFormatEventLogLine( eventlog->TemplateID, str, len, l, params );
            if ( line != NULL )
            {
              switch ( eventlog->Type )
              {
                // --- Caso EventLog tipo Memo ---
                case SPV_EVENTLOG_TYPE_MEMO:
                  memo = (TMemo*)eventlog->Handler;
                  ///if (memo->Lines->Count > SPV_EVENTLOG_MEMO_MAX_LINES)
                  /*
                  if (memo->Lines->Count > ((int)((memo->Height+1) / 13) - 4))
                  {
                    memo->Lines->Delete(0);
                  }
                  //*/
                  memo->Lines->Add( AnsiString( line ) );
                break;
                // --- Caso EventLog tipo file di testo ASCII ---
                case SPV_EVENTLOG_TYPE_ASCII_FILE:
                  file_handler  = (FILE*)eventlog->Handler;
                  if ( file_handler != NULL )
                  {
                    if ( fputs( line, file_handler ) < 0 )
                    {
                      ret_code = SPV_EVENTLOG_FILE_WRITE_FAILURE;
                    }
                    else
                    {
                      if ( fputs( "\n", file_handler ) < 0 )
                      {
                        ret_code = SPV_EVENTLOG_FILE_WRITE_FAILURE;
                      }
                    }
                  }
                  else
                  {
                    ret_code = SPV_EVENTLOG_INVALID_FILE_HANDLER;
                  }
                break;
                // --- Caso EventLog tipo Lod di Microsoft Windows ---
                case SPV_EVENTLOG_TYPE_MSWIN_LOG:
                  if ( eventlog->Handler != NULL )
                  {
                    wl_category = (int) floor( params / 65536 );
                    wl_ID       = (int) ( ( params % 65536 ) % 4096 );
                    wl_sevlevel = (int) floor( ( params % 65535 ) / 4096 );
                    // --- Elaboro il codice di severit� ---
                    wl_sevcode  = EVENTLOG_INFORMATION_TYPE;
                    if ( wl_sevlevel == 1 ) wl_sevcode = EVENTLOG_WARNING_TYPE;
                    if ( wl_sevlevel == 2 ) wl_sevcode = EVENTLOG_ERROR_TYPE;
                    // --- Ciclo di composizione lista di stringhe (max 8 strighe) ---
                    if ( str != NULL )
                    {
                      wl_str_num    = 10; // Massimo 10 stringhe
                      wl_str_list   = (char**) malloc( sizeof(char*) * wl_str_num ); // -MALLOC
                      wl_str_count  = 0;
                      // --- Ciclo sulle varie sottostringhe ---
                      for ( int s = 0; s < wl_str_num; s++ )
                      {
                        wl_str_single = XMLGetToken( str, "|", s ); // -MALLOC
                        if ( wl_str_single != NULL )
                        {
                          wl_str_list[s] = wl_str_single;
                          wl_str_count++;
                        }
                      }
                      if ( SYSEventLogReport( eventlog->Handler, wl_sevcode, wl_category, wl_ID, wl_str_count, wl_str_list, 0, NULL ) != SPV_EVENTLOG_NO_ERROR )
                      {
                        ret_code = SPV_EVENTLOG_FILE_WRITE_FAILURE;
                      }
                      // --- Elimino la sottostringhe ---
                      for ( int s = 0; s < wl_str_count; s++ )
                      {
                        wl_str_single = wl_str_list[s];
                        if ( wl_str_single != NULL )
                        {
                          try
                          {
                            free( wl_str_single ); // -FREE
                          }
                          catch(...)
                          {
                          }
                        }
                      }
                      // --- Elimino la lista delle sottostringhe ---
                      free( wl_str_list ); // -FREE
                    }
                  }
                  else
                  {
                    ret_code = SPV_EVENTLOG_INVALID_FILE_HANDLER;
                  }
                break;
                // --- Caso non definito ---
                default:
                  ret_code = SPV_EVENTLOG_INVALID_TYPE;
              }
              free( line );
            }
            else
            {
              ret_code = SPV_EVENTLOG_FORMAT_ERROR;
            }
          }
        }
        else
        {
          ret_code = SPV_EVENTLOG_INVALID_LENGTH;
        }
      }
      else
      {
        ret_code = SPV_EVENTLOG_NOT_OPENED;
      }
    }
    else
    {
      ret_code = SPV_EVENTLOG_NOT_FOUND;
    }
  }
  else
  {
    ret_code = SPV_EVENTLOG_INVALID_ID;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per stampare una N.T. stringa sull'EventLog
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [29.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVPrintEventLog( int ID, char * str )
{
  int ret_code = 0;

  ret_code = SPVPrintEventLog( ID, str, strlen( str ), 0 );

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere il numero di righe per un EventLog
///
/// In caso di errore restituisce 0
///
/// \date [03.08.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetEventLogLinesCount( int temID, int len )
{
  int count = 0; // Numero di righe
  int hpr   = 8;

  if (len > 0) // Se la lunghezza della stringa � valida
  {
    switch (temID) // Switcho in base al template
    {
      case SPV_EVENTLOG_TEMPLATE_NONE:
        count = 1;
      break;
      case SPV_EVENTLOG_TEMPLATE_DATETIME:
        count = 1;
      break;
      case SPV_EVENTLOG_TEMPLATE_COMPORT:
        count = (int)ceil( ( (float) len ) / hpr );
      break;
    }
  }

  return count;
}

//==============================================================================
/// Funzione per formattare una riga per l'EventLog
///
/// In caso di errore restituisce NULL
///
/// \date [17.02.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
char * SPVFormatEventLogLine( int temID, char * str, int len, int line, int params )
{
  char          * new_str       = NULL                    ;
  int             row           = 0                       ;
  int             new_len       = 0                       ;
  unsigned char   achar         = 0                       ;
  char          * new_char      = NULL                    ;
  int             hpr           = 8;
  int             data_len      = 0;
  char            ASCIIchar     = ' ';
  int             offset        = 0;
  struct tm     * time_now        ; // Data ora formato tm
  time_t          secs_now        ; // Data ora formato time_t

  if (str != NULL) // Controllo la validit� del ptr alla stringa originale
  {
    if (len > 0) // Controllo la validit� della lunghezza della stringa
    {
      if (line >= 0) // Controllo la validit� del numero di riga
      {
        switch (temID) // Switcho sul template
        {
          case SPV_EVENTLOG_TEMPLATE_NONE:
            new_str = (char*) malloc(sizeof(char)*(len+1));
            memcpy(new_str,str,len);
            new_str[len] = '\0';
          break;
          case SPV_EVENTLOG_TEMPLATE_DATETIME:
            time(&secs_now);
            time_now = localtime(&secs_now);
            new_str = (char*) malloc(sizeof(char)*(len+1+14+8));
            strftime( new_str, 15+8, "%d/%m/%Y %H:%M:%S - ", time_now );
            memcpy(&new_str[14+8],str,len);
            new_str[14+8+len] = '\0';
          break;
          case SPV_EVENTLOG_TEMPLATE_COMPORT:
            row = SPVGetEventLogLinesCount(temID,len);
            // Calcolo la lunghezza della riga formattata
            if (line < (row-1)) // Se non � l'ultima riga
            {
              data_len = hpr;
            }
            else
            {
              data_len = len % hpr;
              if (data_len == 0) data_len = hpr;
            }
            offset = line*hpr;
            new_len = 6+(hpr*4)+1;
            // Alloco la memoria necessaria per la nuova riga formattata
            new_str = (char*) malloc(sizeof(char)*new_len);
            for (int c=0; c<(new_len-1); c++)
            {
              new_str[c] = ' ';
            }
            new_str[new_len-1] = '\0';
            // Aggiungo l'intestazione
            if (params == 1) // IN
            {
              new_str[0]   = 'R';
              new_str[1]   = 'X';
              new_str[2]   = ' ';
              new_str[3]   = '=';
              new_str[4]   = '>';
              new_str[5]   = ' ';
            }
            else //OUT
            {
              new_str[0]   = 'T';
              new_str[1]   = 'X';
              new_str[2]   = ' ';
              new_str[3]   = '<';
              new_str[4]   = '=';
              new_str[5]   = ' ';
            }
            // Elaboro i caratteri della riga originale
            for (int d=0; d<hpr; d++)
            {
              if (d<data_len)
              {
                ASCIIchar = (str[offset+d] >= 32 && str[offset+d] <= 126)?str[offset+d]:'x';
                achar = (unsigned char) str[offset+d];
                new_char = cnv_UHex8ToCharP(achar);
                new_str[6 + (d*3) + 0 ] = new_char[0];
                new_str[6 + (d*3) + 1 ] = new_char[1];
                new_str[6 + (hpr*3) + d] = ASCIIchar;
                free(new_char);
              }
              else
              {
                new_str[6 + (d*3) + 0 ] = '.';
                new_str[6 + (d*3) + 1 ] = '.';
                new_str[6 + (hpr*3) + d] = '.';
              }
            }
          break;
        }
      }
	}
  }

  return new_str;
}


//==============================================================================
/// Funzione per Sovvrascivere una stringa sull'EventLog
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [14.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SPVSubscribeEventLog( int ID, char * str, int len, int params )
{
  int                     ret_code      = SPV_EVENTLOG_NO_ERROR; // Codice di ritorno
  SPV_EVENTLOG_MANAGER  * eventlog      = NULL; // Ptr a EventLog Manager
  TMemo                 * memo          = NULL; // Ptr a un TMemo
  FILE                  * file_handler  = NULL; // Handler del file
  int                     line_num      = 0   ; // Numero di righe
  char                  * line          = NULL; // Ptr ad una righe
  int                     wl_category   = 0   ;
  int                     wl_ID         = 0   ;
  int                     wl_sevlevel   = 0   ;
  int                     wl_sevcode    = 0   ;
  int                     wl_str_num    = 0   ;
  int                     wl_str_count  = 0   ;
  char                 ** wl_str_list   = NULL;
  char                  * wl_str_single = NULL; // Singola stringa

  if ( ID > 0 ) // Controllo la validit� dell'ID
  {
	eventlog = SPVGetEventLog( ID );
	if ( eventlog != NULL ) // Se ho trovato l'EventLog
	{
	  if ( eventlog->Opened )
	  {
		line_num = SPVGetEventLogLinesCount( eventlog->TemplateID, len );
		if ( line_num > 0 ) // Se ho un numero di righe accettabile
		{
		  for ( int l=0; l<line_num; l++ ) // Ciclo sulle righe
		  {
			line = SPVFormatEventLogLine( eventlog->TemplateID, str, len, l, params );
			if ( line != NULL )
			{
			  switch ( eventlog->Type )
			  {
				// --- Caso EventLog tipo Memo ---
				case SPV_EVENTLOG_TYPE_MEMO:
				  memo = (TMemo*)eventlog->Handler;
				  ///if (memo->Lines->Count > SPV_EVENTLOG_MEMO_MAX_LINES)
				  /*
				  if (memo->Lines->Count > ((int)((memo->Height+1) / 13) - 4))
				  {
					memo->Lines->Delete(0);
				  }
				  //*/
				  memo->Lines->Add( AnsiString( line ) );
				break;
				// --- Caso EventLog tipo file di testo ASCII ---
				case SPV_EVENTLOG_TYPE_ASCII_FILE:
				  file_handler  = (FILE*)eventlog->Handler;
				  if ( file_handler != NULL )
				  {

					fseek ( file_handler , 0 , SEEK_SET );
					rewind( file_handler );

					if ( fputs( line, file_handler ) < 0 )
					{
					  ret_code = SPV_EVENTLOG_FILE_WRITE_FAILURE;
					}
					else
					{
					  /*if ( fputs( "\n", file_handler ) < 0 )
					  {
						ret_code = SPV_EVENTLOG_FILE_WRITE_FAILURE;
					  } */
					}
				  }
				  else
				  {
					ret_code = SPV_EVENTLOG_INVALID_FILE_HANDLER;
				  }
				  fseek ( file_handler , 0 , SEEK_SET );
//				  rewind( file_handler );
				break;
				// --- Caso EventLog tipo Lod di Microsoft Windows ---
				case SPV_EVENTLOG_TYPE_MSWIN_LOG:
				  if ( eventlog->Handler != NULL )
				  {
					wl_category = (int) floor( params / 65536 );
					wl_ID       = (int) ( ( params % 65536 ) % 4096 );
					wl_sevlevel = (int) floor( ( params % 65535 ) / 4096 );
					// --- Elaboro il codice di severit� ---
					wl_sevcode  = EVENTLOG_INFORMATION_TYPE;
					if ( wl_sevlevel == 1 ) wl_sevcode = EVENTLOG_WARNING_TYPE;
					if ( wl_sevlevel == 2 ) wl_sevcode = EVENTLOG_ERROR_TYPE;
					// --- Ciclo di composizione lista di stringhe (max 8 strighe) ---
					if ( str != NULL )
					{
					  wl_str_num    = 10; // Massimo 10 stringhe
					  wl_str_list   = (char**) malloc( sizeof(char*) * wl_str_num ); // -MALLOC
					  wl_str_count  = 0;
					  // --- Ciclo sulle varie sottostringhe ---
					  for ( int s = 0; s < wl_str_num; s++ )
					  {
						wl_str_single = XMLGetToken( str, "|", s ); // -MALLOC
						if ( wl_str_single != NULL )
						{
						  wl_str_list[s] = wl_str_single;
						  wl_str_count++;
						}
					  }
					  if ( SYSEventLogReport( eventlog->Handler, wl_sevcode, wl_category, wl_ID, wl_str_count, wl_str_list, 0, NULL ) != SPV_EVENTLOG_NO_ERROR )
					  {
						ret_code = SPV_EVENTLOG_FILE_WRITE_FAILURE;
					  }
					  // --- Elimino la sottostringhe ---
					  for ( int s = 0; s < wl_str_count; s++ )
					  {
						wl_str_single = wl_str_list[s];
						if ( wl_str_single != NULL )
						{
						  try
						  {
							free( wl_str_single ); // -FREE
						  }
						  catch(...)
						  {
						  }
						}
					  }
					  // --- Elimino la lista delle sottostringhe ---
					  free( wl_str_list ); // -FREE
					}
				  }
				  else
				  {
					ret_code = SPV_EVENTLOG_INVALID_FILE_HANDLER;
				  }
				break;
				// --- Caso non definito ---
				default:
				  ret_code = SPV_EVENTLOG_INVALID_TYPE;
			  }
			  free( line );
			}
			else
			{
			  ret_code = SPV_EVENTLOG_FORMAT_ERROR;
			}
		  }
		}
		else
		{
		  ret_code = SPV_EVENTLOG_INVALID_LENGTH;
		}
	  }
	  else
	  {
		ret_code = SPV_EVENTLOG_NOT_OPENED;
	  }
	}
	else
	{
	  ret_code = SPV_EVENTLOG_NOT_FOUND;
	}
  }
  else
  {
	ret_code = SPV_EVENTLOG_INVALID_ID;
  }

  return ret_code;
}


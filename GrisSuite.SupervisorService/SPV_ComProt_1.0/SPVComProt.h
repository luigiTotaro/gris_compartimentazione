//==============================================================================
// Telefin Supervisor Communication Protocol Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO PROTOCOLLO DI COMUNICAZIONE (SPVComProt.h)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:	0.88 (06.05.2004 -> 11.02.2008)
//
// Copyright:	2004-2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland DBS 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVComProt.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [06.05.2004]:
// - Prima versione prototipo del modulo.
// 0.01 [10.05.2004]:
// - Aggiunta la definizione del tipo SPV_COMPROT_FIELD.
// - Aggiunta la definizione del tipo SPV_COMPROT_FRAME.
// - Aggiunta la definizione del tipo SPV_COMPROT_COMMAND.
// - Aggiunta la definizione del tipo SPV_COMPROT_FUNCTION_PTR.
// - Aggiunta la definizione del tipo SPV_COMPROT_FUNCTION.
// - Aggiunta la definizione del tipo SPV_COMPROT_STATE.
// - Aggiunta la definizione del tipo SPV_COMPROT_THREAD.
// 0.02 [11.05.2004]:
// - Modificata la definizione del tipo SPV_COMPROT_STATE.
// - Modificata la dafinizione del tipo SPV_COMPROT_COMMAND.
// - Aggiunta la definizione del tipo SPV_COMPROT_TYPE.
// - Incluso il modulo SYSKernel.
// - Incluso il modulo SPVSystem.
// - Aggiunta la funzione SPVLoadComProt.
// 0.03 [12.05.2004]:
// - Aggiunta la funzione SPVLoadCommand.
// - Aggiunti alcuni codici di errore.
// - Modificata la definizione del tipo SPV_COMPROT_COMMAND.
// 0.04 [13.05.2004]:
// - Modificata la funzione SPVLoadCommand.
// - Modificata la definizione del tipo SPV_COMPROT_COMMAND.
// - Aggiunta la funzione SPVLoadFrame.
// 0.05 [14.05.2004]:
// - Modificata la definizione del tipo SPV_COMPROT_FIELD.
// - Modificata la definizione del tipo SPV_COMPROT_FRAME.
// - Modificata la definizione del tipo SPV_COMPROT_COMMAND.
// - Modificata la definizione del tipo SPV_COMPROT_TYPE.
// - Aggiunta la funzione SPVLoadField.
// - Modificata la funzione SPVLoadFrame.
// - Aggiunta la definizione del tipo SPV_COMPROT_STEP.
// 0.06 [17.05.2004]:
// - Modificata la funzione SPVLoadField.
// - Modificata la funzione SPVLoadFrame.
// 0.07 [19.05.2004]:
// - Modificata la funzione SPVLoadField.
// - Modificata la funzione SPVLoadFrame.
// - Aggiunta la funzione SPVLoadStep.
// - Modificata la funzione SPVLoadCommand.
// - Modificata la funzione SPVLoadComProt.
// - Aggiunta la funzione SPVComProtThread.
// - Aggiunta la funzione SPVNewComProtThread.
// - Aggiunta la funzione SPVDelComProtThread.
// - Aggiunta la funzione SPVStartComProtThread.
// - Aggiunta la funzione SPVStopComProtThread.
// 0.08 [20.05.2004]:
// - Aggiunto alcuni codici di errore.
// - Modificata la funzione SPVNewComProtThread.
// 0.09 [21.05.2004]:
// - Modificata la funzione SPVStartComProtThread.
// - Modificata la funzione SPVStopComProtThread.
// 0.10 [24.05.2004]:
// - Modificata la funzione SPVComProtThread.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_PARAMETER.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_PORT.
// - Aggiunti alcuni codici di tipo step e valori di default.
// - Aggiunta la definizione del tipo SPV_COMPROT_BIN_BUFFER.
// - Modificata la definizione del tipo SPV_COMPROT_THREAD.
// - Modificata la funzione SPVNewComProtThread.
// - Aggiunta la funzione SPVExecStep.
// - Aggiunta la funzione SPVNextStep.
// - Aggiunta la funzione SPVBuildFrame.
// - Aggiunta la funzione SPVSendFrame.
// - Aggiunta la funzione SPVReceiveFrame.
// - Aggiunta la funzione SPVCheckFrame.
// 0.l1 [25.05.2004]:
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_BUFFER.
// - Modificata la funzione SPVBuildFrame.
// 0.12 [27.05.2004]:
// - Modificata la funzione SPVBuildFrame.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_FRAME.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_FIELD.
// - Aggiunta la definizione di SPV_COMPROT_MAX_FRAME_LENGTH.
// - Modificata la definizione del tipo SPV_COMPROT_THREAD.
// - Modificata la funzione SPVSendFrame.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVNewComProtThread.
// 0.13 [31.05.2004]:
// - Aggiunto il codice di errore SPV_COMPROT_PORT_NOT_ACTIVE.
// - Aggiunto il codice di errore SPV_COMPROT_PORT_BUSY.
// - Modificata la funzione SPVSendFrame.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVCheckFrame.
// 0.14 [01.06.2004]:
// - Aggiunto il campo <Section> nella definizione del tipo SPV_COMPROT_FIELD.
// - Aggiunte le definizioni delle sezioni di appertenenza dei campi.
// - Aggiunta identificazione della sezione nella funzione SPVLoadField.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_FRAME_LENGTH.
// - Modificata la funzione SPVCheckFrame.
// - Aggiunto il codice di errore SPV_COMPROT_FRAME_HEADER_NOT_FUOND.
// - Aggiunto il codice di errore SPV_COMPROT_FRAME_PAYLOAD_NOT_FUOND.
// - Aggiunto il codice di errore SPV_COMPROT_FRAME_TRAILER_NOT_FUOND.
// - Modificata la funzione SPVBuildFrame.
// 0.15 [15.06.2004]:
// - Modificata la funzione SPVBuildFrame.
// 0.16 [17.06.2004]:
// - Aggiunto il campo ASCII nella definizione del tipo SPV_COMPROT_FIELD.
// - Aggiunto il supporto del campo ASCII nella funzione SPVLoadField.
// - Modificata la funzione SPVBuildFrame.
// - Aggiunta la funzione SPVGetInternalValue.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_VALUE.
// 0.17 [18.06.2004]:
// - Ultimata la funzione SPVGetInternalValue.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_NAME.
// - Modificata la funzione SPVBuildFrame.
// 0.18 [24.06.2004]:
// - Aggiunto il campo <Count> nella definizione del tipo SPV_COMPROT_STEP.
// - Aggiunto il campo <Cycle> nella definizione del tipo SPV_COMPROT_STEP.
// - Aggiunti i codici per definire i tipi di cicli di step.
// - Modificata la funzione SPVLoadStep.
// 0.19 [28.06.2004]:
// - Aggiunta la funzione SPVGetFieldLen.
// 0.20 [29.06.2004]:
// - Modificata la funzione SPVSendFrame.
// - Modificata la funzione SPVReceiveFrame.
// 0.21 [05.07.2004]:
// - Aggiunta la funzione SPVCheckTimeout.
// - Aggiunta la funzione SPVUpdateInBuffer.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_FIFO.
// 0.22 [07.07.2004]:
// - Corretta la funzione SPVUpdateInBuffer.
// 0.23 [08.07.2004]:
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_FIFO_COORDS.
// - Aggiunta la funzione SPVSearchFrame.
// - Aggiunto il codice di errore SPV_COMPROT_FIFO_ERROR.
// - Modificata la funzione SPVCheckFrame.
// - Aggiunto il codice di errore SPV_COMPROT_FRAME_NOT_FOUND.
// - Aggiunta la funzione SPVGetMinFrameLength.
// 0.24 [09.07.2004]:
// - Modificata la funzione SPVCheckFrame.
// - Modificata la funzione SPVSearchFrame.
// 0.25 [12.07.2004]:
// - Modificata la funzione SPVSearchFrame.
// - Aggiunta la definizione del tipo SPV_CYCLE.
// - Modificata la funzione SPVExecStep.
// 0.26 [14.07.2004]:
// - Aggiunto il campo <FailID> nella definizione del tipo SPV_COMPROT_STEP.
// - Modificata la funzione SPVLoadStep.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVExecStep.
// - Aggiunto il campo <FirstPayload> nella definizione del tipo SPV_COMPROT_THREAD.
// - Aggiunta la definizione del tipo SPV_COMPROT_PAYLOAD.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_PAYLOAD.
// - Aggiunta la funzione SPVNewPayload.
// - Aggiunta la funzione SPVDeletePayl.
// - Aggiunta la funzione SPVAddPayload.
// - Aggiunta la funzione SPVGetPayload.
// 0.27 [15.07.2004]:
// - Aggiunta la funzione SPVGetNextPayload.
// - Modificata la funzione SPVCheckFrame.
// - Modificata la funzione SPVSearchFrame.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVExecStep.
// 0.28 [16.07.2004]:
// - Modificata la funzione SPVExecStep.
// - Modificata la definizione del tipo SPV_COMPROT_THREAD.
// - Rinominata la definizione del tipo SPV_CYCLE in SPV_COMPROT_CYCLE.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_CYCLE.
// - Aggiunta la funzione SPVNewCycle.
// - Aggiunta la funzione SPVDeleteCycle.
// - Aggiunta la funzione SPVAddCycle.
// - Aggiunta la funzione SPVRemoveCycle.
// 0.29 [19.07.2004]:
// - Corretta la funzione SPVLoadStep.
// - Aggiunto il codice di errore SPV_COMPROT_FUNCTION_NOT_IMPLEMENTED.
// - Aggiunta la funzione SPVNewComProt.
// - Aggiunta la funzione SPVDeleteComProt.
// - Aggiunta la funzione SPVAddComProt.
// - Aggiunta la funzione SPVGetComProt.
// - Aggiunta la funzione SPVRemoveComProt.
// - Aggiunta la funzione VDeleteField.
// - Aggiunta la funzione VDeleteFrame.
// - Aggiunta la funzione VDeleteStep.
// - Aggiunta la funzione VDeleteCommand.
// 0.30 [20.07.2004]:
// - Corretta la funzione SPVDeleteFrame.
// - Corretta la funzione SPVGetComProt.
// - Modificata la funzione SPVComProtThread.
// - Aggiunto il campo <Counter> nella definizione del tipo SPV_COMPROT_THREAD.
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVNextStep.
// - Modificata la funzione SPVNewComProtThread.
// 0.31 [21.07.2004]:
// - Corretta la funzione SPVLoadStep.
// - Corretta la funzione SPVExecStep.
// - Corretta la funzione SPVReceiveFrame.
// 0.32 [22.07.2004]:
// - Corretta la funzione SPVLoadStep.
// - Corretta la funzione SPVSearchFrame.
// - Corretta la funzione SPVReceiveFrame.
// 0.33 [23.07.2004]:
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVStartComProtThread.
// - Modificata la funzione SPVStopComProtThread.
// - Modificata la funzione SPVUpdateInBuffer.
// - Modificata la funzione SPVSendFrame.
// 0.34 [04.08.2004]:
// - Aggiunta la variabile globale SPVComProtSysDefCriticalSection.
// - Modificata la funzione SPVComProtInit.
// - Modificata la funzione SPVLoadComProt.
// 0.35 [05.08.2004]:
// - Aggiunto il campo <FinishCode> nella definizione del tipo
//   SPV_COMPROT_THREAD.
// - Aggiunti alcuni codici di errore.
// - Modificata la funzione SPVComProtThread.
// - Modificata la funzione SPVExecCommand.
// - Aggiunta la funzione SPVDeletePayloadList.
// 0.36 [10.08.2004]:
// - Corretta la funzione SPVReceiveFrame.
// - Corretta la funzione SPVNextStep.
// - Corretta la funzione SPVNewComProtThread.
// - Aggiornata la funzione SPVCheckTimeout.
// 0.37 [11.08.2004]:
// - Modificata la funzione SPVNextStep.
// - Modificata la funzione SPVComProtThread.
// 0.38 [13.08.2004]:
// - Corretta l'allocazione nella funzione SPVExecStep.
// 0.39 [30.08.2004]:
// - Corretta l'allocazione nella funzione SPVCheckFrame.
// - Corretta l'allocazione nella funzione SPVReceiveFrame.
// - Modificata la funzione SPVCheckTimeout.
// 0.40 [31.08.2004]:
// - Modificata la funzione SPVExecStep.
// 0.41 [10.09.2004]:
// - Modificata la funzione SPVComProtInit.
// 0.42 [13.09.2004]:
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_ELEMENT.
// - Aggiunta la definizione del tipo SPV_COMPROT_PORT.
// - Aggiunta la variabile globale SPVPortList.
// - Aggiunta la variabile globale SPVPortCount.
// - Aggiunta la funzione SPVLoadPorts.
// - Aggiunta la funzione SPVGetPortType.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_PORT_TYPE.
// - Aggiunto il codice di errore SPV_COMPROT_EXTRACT_FAILURE.
// - Aggiunto il codice di errore SPV_COMPROT_SET_FAILURE.
// 0.43 [20.09.2004]:
// - Modificata la funzione SPVSendFrame.
// - Modificata la funzione SPVUpdateInBuffer.
// - Modificata la funzione SPVLoadPorts.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_DEV_TYPE_LIST.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_DEV_TYPE_NUM.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_DEV_TYPE.
// - Aggiunta la funzione SPVLoadSystemComProt.
// - Aggiunto il codice di errore SPV_COMPROT_LOAD_FAILURE.
// 0.44 [21.09.2004]:
// - Aggiunta la funzione SPVAssignPort.
// - Aggiunta la funzione SPVAssignSystemPorts.
// - Aggiunto il codice di errore SPV_COMPROT_INVALID_SYSTEM.
// 0.45 [30.09.2004]:
// - Aggiunta la funzione SPVOpenPorts.
// 0.46 [19.01.2005]:
// - Modificata la funzione SPVGetPortType.
// 0.47 [31.01.2005]:
// - Modificata la funzione SPVLoadDevComProt.
// 0.48 [11.02.2005]:
// - Modificata la funzione SPVCheckFrame.
// 0.49 [18.02.2005]:
// - Modificata la funzione SPVCheckFrame.
// 0.50 [05.04.2005]:
// - Modificata la funzione SPVLoadField.
// - Modificata la funzione SPVGetMinFrameLength.
// - Modificata la funzione SPVSearchFrame.
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVNextStep.
// 0.51 [06.04.2005]:
// - Modificata la funzione SPVNewPayload.
// 0.52 [07.04.2005]:
// - Modificata la funzione SPVLoadCommand.
// - Modificata la funzione SPVGetMinFrameLength.
// - Modificata la funzione SPVExecCommand.
// 0.53 [27.04.2005]:
// - Modificata la funzione SPVLoadField.
// - Modificata la funzione SPVLoadFrame.
// - Modificata la funzione SPVDeleteField.
// - Aggiuna la funzione SPVClearField.
// 0.54 [03.05.2005]:
// - Modificata la funzione SPVExecStep.
// 0.55 [10.05.2005]:
// - Modificata la funzione SPVGetInternalValue.
// - Modificata la funzione SPVBuildFrame.
// 0.56 [13.05.2005]:
// - Modificata la funzione SPVBuildFrame.
// 0.57 [16.05.2005]:
// - Modificata la funzione SPVBuildFrame.
// - Modificata la funzione SPVNextStep.
// 0.58 [17.05.2005]:
// - Modificata la funzione SPVExecStep.
// 0.59 [24.05.2005]:
// - Rinominato il campo StepID in ID nella struttura SPV_COMPROT_PAYLOAD.
// - Aggiunto il campo PID nella struttura SPV_COMPROT_STEP.
// - Modificata la funzione SPVLoadStep.
// - Modificata la funzione SPVNewPayload.
// - Modificata la funzione SPVGetPayload.
// - Modificata la funzione SPVGetNextPayload.
// - Modificata la funzione SPVExecStep.
// 0.60 [26.05.2005]:
// - Modificata la funzione SPVExecStep.
// 0.61 [31.05.2005]:
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVGetInternalValue.
// - Modificata la funzione SPVNewComProtThread.
// - Modificata la funzione SPVExecCommand.
// 0.62 [01.06.2005]:
// - Modificata la funzione SPVLoadField.
// - Modificata la funzione SPVGetPayload.
// - Modificata la funzione SPVGetInternalValue.
// - Modificata la funzione SPVExecStep.
// 0.63 [06.06.2005]:
// - Modificata la funzione SPVBuildFrame.
// 0.64 [14.06.2005]:
// - Modificata la funzione SPVComProtThread.
// 0.65 [24.06.2005]:
// - Modificata la funzione SPVComProtThread.
// 0.66 [05.07.2005]:
// - Modificata la funzione SPVComProtInit.
// - Aggiunta la funzione SPVEnterThreadData.
// - Aggiunta la funzione SPVLeaveThreadData.
// - Aggiunta la funzione SPVGetFirstFreeThreadPos.
// - Modificata la funzione SPVNewComProtThread.
// - Modificata la funzione SPVDelComProtThread
// - Modificata la funzione SPVStartComProtThread.
// 0.67 [06.07.2005]:
// - Aggiunta la funzione SPVGetComProtThreadPos.
// 0.68 [07.07.2005]:
// - Modificata la funzione SPVComProtInit.
// - Modificata la funzione SPVGetComProtThreadPos.
// - Modificata la funzione SPVExecCommand.
// - Aggiunta la funzione SPVCommandCheckThread.
// 0.69 [08.07.2005]:
// - Aggiunta la funzione SPVGetComProtThread.
// - Aggiunta la funzione SPVRemoveComProtThread.
// 0.70 [11.07.2005]:
// - Aggiunta la variabile globale SPVComProtThreadFirst.
// - Modificata la definizione del tipo SPV_COMPROT_THREAD.
// - Modificata la funzione SPVComProtInit.
// - Modificata la funzione SPVLoadCommand.
// - Modificata la funzione SPVLoadComProt.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVCheckFrame.
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVGetFirstFreeThreadPos.
// - Aggiunta la funzione SPVGetFirstClosedThread.
// - Aggiunta la funzione SPVGetFirstThreadPos.
// - Modificata la funzione SPVGetComProtThread.
// - Modificata la funzione SPVDelComProtThread.
// - Modificata la funzione SPVRemoveComProtThread.
// - Modificata la funzione SPVExecCommand.
// - Modificata la funzione SPVLoadPorts.
// - Modificata la funzione SPVAssignPort.
// 0.71 [13.07.2005]:
// - Modificata la funzione SPVExecStep.
// - Aggiunta la funzione SPVKillCommand.
// - Modificata la funzione SPVExecCommand.
// - Modificata la funzione SPVComProtThread.
// - Modificata la funzione SPVGetFirstClosedThread.
// - Modificata la funzione SPVGetFirstThreadPos.
// - Modificata la funzione SPVDelComProtThread.
// 0.72 [14.07.2005]:
// - Modificata la funzione SPVComProtInit.
// - Modificata la funzione SPVBuildFrame.
// - Modificata la funzione SPVUpdateInBuffer.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVCheckFrame.
// - Modificata la funzione SPVExecStep.
// - Modificata la funzione SPVComProtThread.
// - Modificata la funzione SPVNewComProtThread.
// - Modificata la funzione SPVExecCommand.
// - Modificata la funzione SPVKillCommand.
// 0.73 [15.07.2005]:
// - Modificata la funzione SPVExecCommand.
// 0.74 [29.07.2005]:
// - Modificata la funzione SPVLoadField.
// 0.75 [03.08.2005]:
// - Modificata la funzione SPVExecCommand.
// 0.76 [29.08.2005]:
// - Aggiunta la funzione SPVComProtClear.
// 0.77 [31.08.2005]:
// - Modificata la funzione SPVDeleteField.
// - Modificata la funzione SPVDeleteFrame.
// - Modificata la funzione SPVDeleteStep.
// - Modificata la funzione SPVDeleteCommand.
// - Modificata la funzione SPVNewComProt.
// - Modificata la funzione SPVDeleteComProt.
// - Aggiunta la funzione SPVFreeSystemComProt.
// 0.78 [01.09.2005]:
// - Modificata la funzione SPVDeleteCommand.
// 0.79 [06.09.2005]:
// - Modificata la funzione SPVFreePorts.
// 0.80 [15.09.2005]:
// - Modificata la funzione SPVGetPortType.
// 0.81 [27.10.2005]:
// - Tolto il campo <Code> dalla definizione del tipo SPV_COMPROT_STEP.
// - Tolto il campo <Code> dalla definizione del tipo SPV_COMPROT_COMMAND.
// - Tolto il campo <Code> dalla definizione del tipo SPV_COMPROT_FIELD.
// - Tolto il campo <Code> dalla definizione del tipo SPV_COMPROT_FRAME.
// 0.82 [24.11.2005]:
// - Modificata la funzione SPVReportApplicEvent.
// 0.83 [28.11.2005]:
// - Modificata la funzione SPVGetInternalValue.
// - Modificata la funzione SPVBuildFrame.
// - Modificata la funzione SPVSendFrame.
// - Modificata la funzione SPVUpdateInBuffer.
// - Modificata la funzione SPVReceiveFrame.
// - Modificata la funzione SPVCheckFrame.
// - Modificata la funzione SPVSearchFrame.
// 0.84 [19.12.2005]:
// - Modificata la funzione SPVAssignPort.
// 0.85 [21.09.2007]:
// - Aggiunto la funzione SPVGetCommandName.
// - Assegnato un nome all'attributo Name di SPVComPortThread e di SPVCommandCheckThread
// 0.86 [04.10.2007]:
// - Aggiunto salvataggio StepID in SPVComProtThread().
// 0.87 [11.01.2008]:
// - Corretta la funzione SPVComProtThread
// 0.88 [11.02.2008]:
// - Introdotta nuova gestione delle eccezioni nelle funzioni
//		- SPVEnterThreadData();
//		- SPVLeaveThreadData();
// 		- SPVExecStep();
//		- SPVComProtInit();
//		- SPVLoadComProt();
//		- SPVComProtClear();
//		- SPVExecCommand();
//		- SPVComProtThread();
//==============================================================================
#ifndef SPVComProtH
#define SPVComProtH
//------------------------------------------------------------------------------
#include "SYSKernel.h"
#include "XMLInterface.h"
#include "SPVSystem.h"
#include "SPVEventLog.h"
#include "SPVComProtLib.h"

//==============================================================================
// Definizione valori
//------------------------------------------------------------------------------
#define SPV_COMPROT_THREAD_SLEEP_DEFAULT      50
#define SPV_COMPROT_STEP_TYPE_UNKNOWN         -1
#define SPV_COMPROT_STEP_TYPE_SEND            0
#define SPV_COMPROT_STEP_TYPE_RECEIVE         1
#define SPV_COMPROT_FIELD_SECTION_UNKNOWN     0
#define SPV_COMPROT_FIELD_SECTION_HEADER      1
#define SPV_COMPROT_FIELD_SECTION_PAYLOAD     2
#define SPV_COMPROT_FIELD_SECTION_TRAILER     3
#define SPV_COMPROT_FIELD_SECTION_USER        4
#define SPV_COMPROT_FIELD_SECTION_HIDDEN      5
#define SPV_COMPROT_STEP_CYCLE_NONE           0
#define SPV_COMPROT_STEP_CYCLE_FOR            1
#define SPV_COMPROT_STEP_CYCLE_WHILE          2
#define SPV_COMPROT_MAX_PORT                  32
#define SPV_COMPROT_MAX_THREAD                2000
#define SPV_COMPROT_COMMAND_KILL_TIMEOUT      60
#define SPV_COMPROT_COMMAND_RETRY_DEFAULT     3
#define SPV_COMPROT_COMMAND_RETRY_DELAY       500

#define SPV_COMPROT_THREAD_VALIDITY_CHECK_CODE      2040564313
#define SPV_COMPROT_BIN_BUFFER_VALIDITY_CHECK_CODE  2045865744


//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define SPV_COMPROT_NO_ERROR                  0
#define SPV_COMPROT_INVALID_ALLOCATION        3000
#define SPV_COMPROT_INVALID_COMPROT_TYPE      3001
#define SPV_COMPROT_INVALID_COMMAND           3002
#define SPV_COMPROT_INVALID_ID                3003
#define SPV_COMPROT_INVALID_DEFINITION        3004
#define SPV_COMPROT_INVALID_COUNT             3005
#define SPV_COMPROT_INVALID_STEP              3006
#define SPV_COMPROT_INVALID_DEVICE            3007
#define SPV_COMPROT_INVALID_THREAD            3008
#define SPV_COMPROT_INVALID_PARAMETER         3009
#define SPV_COMPROT_INVALID_PORT              3010
#define SPV_COMPROT_INVALID_BUFFER            3011
#define SPV_COMPROT_INVALID_FRAME             3012
#define SPV_COMPROT_INVALID_FIELD             3013
#define SPV_COMPROT_INVALID_BUFFER_LENGTH     3014
#define SPV_COMPROT_PORT_NOT_ACTIVE           3015
#define SPV_COMPROT_PORT_BUSY                 3016
#define SPV_COMPROT_INVALID_FRAME_LENGTH      3017
#define SPV_COMPROT_FRAME_HEADER_NOT_FOUND    3018
#define SPV_COMPROT_FRAME_PAYLOAD_NOT_FOUND   3019
#define SPV_COMPROT_FRAME_TRAILER_NOT_FOUND   3020
#define SPV_COMPROT_INVALID_VALUE             3021
#define SPV_COMPROT_INVALID_NAME              3022
#define SPV_COMPROT_INVALID_FIELD_ORDER       3023
#define SPV_COMPROT_FRAME_HEADER_ERROR        3024
#define SPV_COMPROT_FRAME_TRAILER_ERROR       3025
#define SPV_COMPROT_INVALID_FIFO              3026
#define SPV_COMPROT_INVALID_FIFO_COORDS       3027
#define SPV_COMPROT_FIFO_ERROR                3028
#define SPV_COMPROT_FRAME_NOT_FOUND           3029
#define SPV_COMPROT_INVALID_PAYLOAD           3030
#define SPV_COMPROT_INVALID_CYCLE             3031
#define SPV_COMPROT_NEW_THREAD_FAILURE        3032
#define SPV_COMPROT_START_THREAD_FAILURE      3033
#define SPV_COMPROT_WAIT_THREAD_FAILURE       3034
#define SPV_COMPROT_THREAD_ERROR              3035
#define SPV_COMPROT_INVALID_ELEMENT           3036
#define SPV_COMPROT_INVALID_PORT_TYPE         3037
#define SPV_COMPROT_EXTRACT_FAILURE           3038
#define SPV_COMPROT_SET_FAILURE               3039
#define SPV_COMPROT_INVALID_DEV_TYPE_LIST     3040
#define SPV_COMPROT_INVALID_DEV_TYPE_NUM      3041
#define SPV_COMPROT_INVALID_DEV_TYPE          3042
#define SPV_COMPROT_LOAD_FAILURE              3043
#define SPV_COMPROT_INVALID_SYSTEM            3044
#define SPV_COMPROT_INVALID_PORT_LIST         3045
#define SPV_COMPROT_COMMAND_LIST_OVERFLOW     3046
#define SPV_COMPROT_STEP_LIST_OVERFLOW        3047
#define SPV_COMPROT_FRAME_LIST_OVERFLOW       3048
#define SPV_COMPROT_FIFO_EMPTY                3049
#define SPV_COMPROT_INVALID_POINTER           3050
#define SPV_COMPROT_INVALID_FIELD_BUFFER      3051
#define SPV_COMPROT_THREAD_LIST_FULL          3052
#define SPV_COMPROT_THREAD_NOT_PRESENT        3053
#define SPV_COMPROT_INVALID_PORT_ID           3054
#define SPV_COMPROT_CRITICAL_ERROR            3055
#define SPV_COMPROT_RECEIVED_BUFFER_CORRUPTED 3056
#define SPV_COMPROT_RECEIVED_BUFFER_WRONG     3057
#define SPV_COMPROT_PORT_OPEN_FAILURE         3058
#define SPV_COMPROT_PORT_HW_SET_FAILURE       3059
#define SPV_COMPROT_FUNCTION_EXCEPTION				3060
//==============================================================================
// Dichiarazione stati step del thread
//------------------------------------------------------------------------------
#define SPV_COMPROT_STEP_UNKNOWN                0
#define SPV_COMPROT_STEP_IDLE                   3100
#define SPV_COMPROT_STEP_FAILURE                3101
#define SPV_COMPROT_STEP_START                  3102
#define SPV_COMPROT_STEP_READ_FIFO_START        3103
#define SPV_COMPROT_STEP_READ_FIFO_TIMEOUT      3104
#define SPV_COMPROT_STEP_READ_FIFO_FAILURE      3105
#define SPV_COMPROT_STEP_WRITE_FIFO_START       3106
#define SPV_COMPROT_STEP_WRITE_FIFO_TIMEOUT     3107
#define SPV_COMPROT_STEP_WRITE_FIFO_FAILURE     3108
#define SPV_COMPROT_STEP_WRITE_FIFO_FULL        3109
#define SPV_COMPROT_STEP_READ_FIFO_EMPTY        3110
#define SPV_COMPROT_STEP_FRAME_NOT_FOUND        3111
#define SPV_COMPROT_STEP_FRAME_CORRUPTED        3112
#define SPV_COMPROT_STEP_FIELD_CORRUPTED        3113
#define SPV_COMPROT_STEP_HEADER_CHECK_FAILURE   3114
#define SPV_COMPROT_STEP_TRAILER_CHECK_FAILURE  3115
#define SPV_COMPROT_STEP_SUCCESS                3116
#define SPV_COMPROT_STEP_WRITE_FIFO_SUCCESS     3117
#define SPV_COMPROT_STEP_FRAME_BUILD_START      3118
#define SPV_COMPROT_STEP_FRAME_BUILD_SUCCESS    3119
#define SPV_COMPROT_STEP_FRAME_BUILD_FAILURE    3120
#define SPV_COMPROT_STEP_READ_FIFO_SUCCESS      3121
#define SPV_COMPROT_STEP_TIMEOUT                3122
#define SPV_COMPROT_STEP_FRAME_SEARCH_START     3123
#define SPV_COMPROT_STEP_FRAME_FOUND            3124
#define SPV_COMPROT_STEP_FRAME_EXTRACT_START    3125
#define SPV_COMPROT_STEP_FRAME_EXTRACT_SUCCESS  3126
#define SPV_COMPROT_STEP_FRAME_EXTRACT_FAILURE  3127
//==============================================================================
// Dichiarazione stati thread
//------------------------------------------------------------------------------
#define SPV_COMPROT_THREAD_OK                 3200
#define SPV_COMPROT_THREAD_FINISH             3201
#define SPV_COMPROT_THREAD_FAILURE            3202
#define SPV_COMPROT_THREAD_KILLED             3203
#define SPV_COMPROT_THREAD_ABORT              3204
#define SPV_COMPROT_FUNCTION_NOT_IMPLEMENTED  3999
//------------------------------------------------------------------------------

//==============================================================================
// Dichiarazione Tipi di dati
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura dati per un buffer binario (non NT-buffer).
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_BIN_BUFFER
{
  unsigned __int32      VCC                                       ; // Validity Check Code
  int                   len                                       ; // Dimensione del buffer
  unsigned char         buffer[SYS_KERNEL_PORT_MAX_BUFFER_LENGTH] ; // Ptr al buffer
  int                   tag                                       ; // Tag
}
SPV_COMPROT_BIN_BUFFER;

//==============================================================================
/// Struttura dati per un payload.
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_PAYLOAD
{
  int                       ID      	; // Codice identificativo del payload (pID)
  SPV_COMPROT_BIN_BUFFER  * Buffer  	; // Buffer binario che contiene il payload
  void                    * Next    	; // Ptr ad un eventuale payload successivo
}
SPV_COMPROT_PAYLOAD;

//==============================================================================
/// Struttura dati per i campi dei frame.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_FIELD
{
  int                   ID        ; // Codice numerico identificativo del campo
  char                * Name      ; // Nome del campo
  int                   Section   ; // Sezione del campo (Header, Payload o Trailer)
  int                   Type      ; // Tipo di campo
  bool                  ASCII     ; // Formato ASCII
  int                   Len       ; // Lunghezza del valore
  int                   Count     ; // Numero di elementi (array)
  char                * Value     ; // Puntatore alla stringa di byte del valore
  XML_ELEMENT         * Def       ; // Ptr all'elemento XML di definizione
}
SPV_COMPROT_FIELD;

//==============================================================================
/// Struttura dati per i frame.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_FRAME
{
  int                   ID        ; // Codice numerico identificativo del frame
  char                * Name      ; // Nome del frame
  SPV_COMPROT_FIELD   * FieldList ; // Puntatore alla lista di campi
  int                   FieldCount; // Numero di campi
  XML_ELEMENT         * Def       ; // Ptr all'elemento XML di definizione
}
SPV_COMPROT_FRAME;

//==============================================================================
/// Struttura dati per gli step dei comandi di protocollo.
///
/// \date [15.01.2010]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_STEP
{
	int					ID        ; // Codice numerico dello step
	int					PID       ; // Codice del payload da estrarre
	char *				Name      ; // Nome dello step
	int					FrameID   ; // ID del frame assegnato allo step
	int					PrevID    ; // ID dello step precedente
	int					Type      ; // Tipo di step (1=SEND,2=RECEIVE)
	int					Timeout   ; // Timeout dello step
	int					Retry     ; // Tentativi dello step
	int					Delay     ; // Pausa alla fine dello step
	int					Count     ; // Numero di ripetizioni dello step
	int					Cycle     ; // Tipo di ciclo
	int					FailID    ; // Id dello step a cui saltare in caso di fallimento
	int					SuccessID ; // Id dello step a cui saltare in caso di successo
	XML_ELEMENT *		Def       ; // Ptr all'elemento XML di definizione
}
SPV_COMPROT_STEP;

//==============================================================================
/// Struttura dati per i comandi.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_COMMAND
{
  int                   ID        ; // Codice numerico identificat. del comando
  char                * Name      ; // Nome del comando
  char                * Type      ; // Tipo di comando
  bool                  Blocking  ; // Flag di comando bloccante
  SPV_COMPROT_STEP    * StepList  ; // Lista degli step del comando
  int                   StepCount ; // Numero degli step del comando
  XML_ELEMENT         * Def       ; // Ptr all'elemento XML di definizione
}
SPV_COMPROT_COMMAND;

//==============================================================================
/// Definizione di puntatore a funzione di protocollo.
///
/// \date [10.05.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef int(* SPV_COMPROT_FUNCTION_PTR)(void*,void*);

//==============================================================================
/// Struttura dati per funzione di protocollo.
///
/// \date [10.05.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_FUNCTION
{
  int                       ID          ; // Codice numerico identificativo
  char                      Code[12]    ; // Codice alfanumerico identificativo
  SPV_COMPROT_FUNCTION_PTR  Ptf         ; // Puntatore alla funzione
}
SPV_COMPROT_FUNCTION;

//==============================================================================
/// Struttura dati per un tipo di protocollo di comunicazione.
///
/// \date [14.05.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_TYPE
{
  int                   ID          ; // Codice numerico identificativo
  char                * Code        ; // Codice alfanumerico di identificativo
  char                * Name        ; // Nome del tipo di protocollo
  SPV_COMPROT_FRAME   * FrameList   ; // Lista dei frame di protocollo
  int                   FrameCount  ; // Numero di frame di protocollo
  SPV_COMPROT_COMMAND * CommandList ; // Lista dei comandi del protocollo
  int                   CommandCount; // Numero di comandi in lista
  XML_ELEMENT         * Def         ; // Puntatore all'elemento root della definizione
  void                * Tag         ; // Puntatore di servizio (?)
}
SPV_COMPROT_TYPE;

//==============================================================================
/// Struttura dati per uno stato di protocollo.
///
/// \date [13.05.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_STATE
{
  int                     ID          ; // Codice numerico identificativo
  char                  * Code        ; // Codice alfanumerico identificativo
  char                  * Name        ; // Nome dello stato
  int                     SeqParent   ; // ID dello stato parent
  int                     SeqReturn   ; // Codice di ritorno del parent
  int                     SeqOrder    ; // Ordine di esecuzione sequenziale
  XML_ELEMENT           * Def         ; // Ptr a struttura di definizione dello stato
  SPV_COMPROT_FRAME     * Frame       ; // Puntatore a funzione dello stato
  bool                    Active      ; // Flag di stato attivo
  bool                    Waiting     ; // Flag di stato in attesa
  bool                    Success     ; // Flag di stato ultimato con successo
  bool                    Failed      ; // Flag di stato fallito
  long                    TimeStart   ; // Data ora di inizio stato
  long                    TimeFinish  ; // Data ora di fine stato
  int                     RetCode     ; // Codice di ritorno dello stato
}
SPV_COMPROT_STATE;

//==============================================================================
/// Struttura dati per un ciclo di step.
///
/// \date [16.07.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_CYCLE
{
  int                     Type        ; // Tipo di ciclo (FOR,WHILE)
  int                     Step        ; // Step che ha originato il ciclo
  int                     Num         ; // Numero di cicli effettuati
  void                  * Upper       ; // Eventuale ciclo in cui si � annidato
}
SPV_COMPROT_CYCLE;

//==============================================================================
/// Struttura dati per un thread di protocollo di comunicazione.
///
/// \date [25.05.2006]
/// \author Enrico Alborali
/// \version 0.14
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_THREAD
{
  unsigned __int32      VCC           ; // Validity Check Code
  unsigned long         ID            ; // Codice numerico identificativo
  bool                  Active        ; // Flag di thread protocollo attivo
  bool                  Waiting       ; // Flag di thread protocollo in attesa
  SPV_DEVICE          * Device        ; // Periferica associata al thread
  SPV_COMPROT_TYPE    * Type          ; // Tipologia di protocollo di comunicazione
  SYS_PORT            * Port          ; // Porta di comunicazione
  int                   Command       ; // ID del comando di protocollo da eseguire
  int                   CmdRetryCount ; // Numero di ripetizioni massime per il comando
  int                   CmdRetry      ; // Contatore delle ripetizioni del comando
  int                   CmdRetryDelay ; // Secondi di attesa tra tentativi di un comando
  int                   Counter       ; // Contatore delle esecuzioni dello step
  SPV_COMPROT_CYCLE   * Cycle         ; // Ptr ad un eventuale ciclo/i attivo/i
  int                   PrevStep      ; // ID dello step precedente
  int                   Step          ; // ID dello step di protocollo corrente
  bool                  StepSuccess   ; // Flag di stato ultimato con successo
  bool                  StepFailure   ; // Flag di stato fallito
  long                  StepStart     ; // Data ora di inizio stato
  long                  StepFinish    ; // Data ora di fine stato
  int                   StepRetCode   ; // Codice di ritorno dello step
  int                   Retry         ; // Numero di tentativi
  int                   Sleep         ; // Millisecondi di attesa ad ogni ciclo del thread
  int                   FinishCode    ; // Codice di fine thread
  int                   FailureStep   ; // ID dell'ultimo step fallito
  int                   FailureCode   ; // Codice di ritorno dell'ultimo step fallito
  SPV_COMPROT_PAYLOAD * FirstPayload  ; // Ptr al primo dei payload ricevuti
  SYS_THREAD          * Thread        ; // Puntatore al thread del protocollo
  XML_ELEMENT         * Def           ; // Puntatore all'elemento root della definizione
  void                * Tag           ; // Puntatore di servizio
}
SPV_COMPROT_THREAD;

//==============================================================================
/// Struttura dati per una porta di comunicazione
///
/// \date [23.02.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_COMPROT_PORT
{
  int                   ID          ; // ID della porta di comunicazione
  SYS_PORT            * Port        ; // Ptr alla struttura di descrizione della porta
}
SPV_COMPROT_PORT;

/// Tipo funzione di log
typedef int(* _SPV_COMPROT_LOG_FUNCTION_PTR)(SPV_COMPROT_COMMAND*, int, int, char *, char * );

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo
//int                       SPVComProtInit          ( _CRITICAL_SECTION * sys_def_cs );
int                       SPVComProtInit          ( SYS_LOCK * sys_def_cs );
// Funzione per chiudere il modulo
int                       SPVComProtClear         ( void );
// Funzione per creare un nuovo buffer binario
SPV_COMPROT_BIN_BUFFER  * SPVNewBinBuffer         ( void );
// Funzione per eliminare un buffer binario
int                       SPVDeleteBinBuffer      ( SPV_COMPROT_BIN_BUFFER * bbuffer );
// Funzione per verificare la validita' di un buffer binario
bool                      SPVValidBinBuffer       ( SPV_COMPROT_BIN_BUFFER * bbuffer );
// Funzione per cancellare i campi di una struttura field
int                     SPVClearField           ( SPV_COMPROT_FIELD * field );
// Funzione per caricare la definizione di un campo
int                     SPVLoadField            ( SPV_COMPROT_FIELD * field, XML_ELEMENT * e_field );
// Funzione per caricare la definizione di un frame
int                     SPVLoadFrame            ( SPV_COMPROT_FRAME * frame, XML_ELEMENT * e_frame );
// Funzione per caricare la definizione di uno step
int                     SPVLoadStep             ( SPV_COMPROT_STEP * step, XML_ELEMENT * e_step );
// Funzione per caricare la definizione di un comando
int                     SPVLoadCommand          ( SPV_COMPROT_COMMAND * command, XML_ELEMENT * e_command );
// Funzione per caricare la definizione di un protocollo di comunicazione
int                     SPVLoadComProt          ( SPV_COMPROT_TYPE * comprot, XML_ELEMENT * e_comprot );
// Funzione per eliminare la definizione di un campo
int                     SPVDeleteField          ( SPV_COMPROT_FIELD * field );
// Funzione per eliminare la definizione di un frame
int                     SPVDeleteFrame          ( SPV_COMPROT_FRAME * frame );
// Funzione per eliminare la definizione di uno step
int                     SPVDeleteStep           ( SPV_COMPROT_STEP * step );
// Funzione per eliminare la definizione di un comando
int                     SPVDeleteCommand        ( SPV_COMPROT_COMMAND * command );
// Funzione per creare una nuova definizione di protocollo
SPV_COMPROT_TYPE      * SPVNewComProt           ( void );
// Funzione per eliminare una definizione di protocollo
int                     SPVDeleteComProt        ( SPV_COMPROT_TYPE * comprot );
// Funzione per eliminare dalla memoria una struttura di protocollo
int                     SPVFreeDevComProt       ( SPV_DEVICE * device );
// Funzione per aggiungere una def. di protoccllo alla lista globale
int                     SPVAddComProt           ( SPV_COMPROT_TYPE * comprot );
// Funzione per ottenere una def. di protocollo dalla lista globale
SPV_COMPROT_TYPE      * SPVGetComProt           ( int id );
// Funzione per togliere una def. di protocollo dalla lista
int                     SPVRemoveComProt        ( int id );
// Funzione per caricare il protocollo di comunicazione di una periferica
int                     SPVLoadDevComProt       ( SPV_DEVICE * device );
// Funzione per eliminare il protocollo di comunicazione di una periferica
int                     SPVFreeDevComProt       ( SPV_DEVICE * device );
// Funzione per caricare il protocollo di comunicazione di tutto il sistema
int                     SPVLoadSystemComProt    ( SPV_SYSTEM * system );
// Funzione per eliminare i protocolli di comunicazione di tutto il sistema
int                     SPVFreeSystemComProt    ( SPV_SYSTEM * system );
// Funzione per creare un nuovo payload
SPV_COMPROT_PAYLOAD   * SPVNewPayload           ( int id, SPV_COMPROT_BIN_BUFFER * buffer );
// Funzione per eliminare un payload
int                     SPVDeletePayload        ( SPV_COMPROT_PAYLOAD * payload );
// Funzione per aggiungere un payload ad un thread di protocollo
int                     SPVAddPayload           ( SPV_COMPROT_THREAD * thread, SPV_COMPROT_PAYLOAD * payload );
// Funzione per ottenere un payload ad un thread di protocollo
SPV_COMPROT_PAYLOAD   * SPVGetPayload           ( SPV_COMPROT_THREAD * thread, int id );
// Funzione per ottenere il prossimo payload
SPV_COMPROT_PAYLOAD   * SPVGetNextPayload       ( SPV_COMPROT_PAYLOAD * payload );
// Funzione per eliminare una lista di payload
int                     SPVDeletePayloadList    ( SPV_COMPROT_PAYLOAD * payload );
// Funzione per creare un nuovo descrittore di ciclo
SPV_COMPROT_CYCLE     * SPVNewCycle             ( int step_id, int type );
// Funzione per eliminare un descrittore di ciclo
int                     SPVDeleteCycle          ( SPV_COMPROT_CYCLE * cycle );
// Funzione per aggiungere un (sotto)ciclo al thread
int                     SPVAddCycle             ( SPV_COMPROT_THREAD * thread, SPV_COMPROT_CYCLE * cycle );
// Funzione per rimuovere il (sotto)ciclo dal thread
int                     SPVRemoveCycle          ( SPV_COMPROT_THREAD * thread );
// Funzione per ricavare la lunghezza di un campo di un frame
int                     SPVGetFieldLen          ( SPV_COMPROT_FIELD * field );
// Funzione per recuperare valori interni al programma
char                  * SPVGetInternalValue     ( SPV_DEVICE * device, SPV_COMPROT_THREAD * thread, char * name );
// Funzione per ottnere la lunghezza minima di un frame
int                     SPVGetMinFrameLength    ( SPV_COMPROT_FRAME * frame );
// Funzione per comporre un frame di protocollo
int                     SPVBuildFrame           ( SPV_COMPROT_BIN_BUFFER * bbuffer, SPV_COMPROT_FRAME * frame, SPV_DEVICE * device, SPV_COMPROT_THREAD * thread );
// Funzione per inviare un frame di protocollo
int                     SPVSendFrame            ( SPV_COMPROT_BIN_BUFFER * frame, SYS_PORT * port, int timeout );
// Funzione per ricevere un frame di protocollo
int                     SPVReceiveFrame         ( SPV_DEVICE * device, SPV_COMPROT_BIN_BUFFER * bframe, SPV_COMPROT_BIN_BUFFER * bpayload, SYS_FIFO * fifo, SYS_FIFO_COORDS * coords );
// Funzione per controllare il timeout in un buffer
int                     SPVCheckTimeout         ( SYS_FIFO * fifo, long time );
// Funzione per aggiornare il buffer in ingresso di una porta
int                     SPVUpdateInBuffer       ( SYS_PORT * port, bool timeout );
// Funzione per analizzare un frame di protocollo
int                     SPVCheckFrame           ( SPV_COMPROT_BIN_BUFFER * bbuffer, int * frame_length, SPV_COMPROT_FRAME * frame, SPV_DEVICE * device, int * payload_offset, int * payload_length );
// Funzione per cercare un frame all'interno di un buffer FIFO
int                     SPVSearchFrame          ( SYS_FIFO * fifo, SPV_COMPROT_FRAME * frame, SPV_DEVICE * device, SYS_FIFO_COORDS * coords );
// Funzione per eseguire uno step di thread protocollo
int                     SPVExecStep             ( SPV_COMPROT_THREAD * thread );
// Funzione per passare al successivo step di thread protocollo
int                     SPVNextStep             ( SPV_COMPROT_THREAD * thread );
// Funzione per accedere ai dati della lista thread di protocollo
void                    SPVEnterThreadData      ( void );
// Funzione per uscire dai dati della lista thread di protocollo
void                    SPVLeaveThreadData      ( void );
// Funzione di attivazione per un thread di un comando
unsigned long __stdcall SPVComProtThread        ( void * parameter );
// Funzione per ottenere la prima posizione libera nella lista dei thread
int                     SPVGetFirstFreeThreadPos( void );
// Funzione per ottenere l'ID del primo thread concluso
unsigned long           SPVGetFirstClosedThread ( void );
// Funzione per ottenere la posizione del primo thread da visualizzare
int                     SPVGetFirstThreadPos    ( void );
// Funzione per ottenere la posizione di un thread di protocollo
int                     SPVGetComProtThreadPos  ( SPV_COMPROT_THREAD * thread );
// Funzione per recuperare il ptr di un thread di protocollo dal suo ID
SPV_COMPROT_THREAD    * SPVGetComProtThread     ( unsigned long ID );
// Funzione per generare un thread di un comando
int                     SPVNewComProtThread     ( SPV_COMPROT_THREAD * thread, SPV_DEVICE * device, int command );
// Funzione per verificare la validita' di una struttura thread comando
bool                    SPVValidComProtThread   ( SPV_COMPROT_THREAD * thread );
// Funzione per eliminare un thread di un comando
int                     SPVDelComProtThread     ( SPV_COMPROT_THREAD * thread );
// Funzione per rimuovere un thread di un comando dalla lista globale
int                     SPVRemoveComProtThread  ( unsigned long ID );
// Funzione per far partire un thread di un comando
int                     SPVStartComProtThread   ( SPV_COMPROT_THREAD * thread );
// Funzione per interrompere un thread di un comando
int                     SPVStopComProtThread    ( SPV_COMPROT_THREAD * thread );
// Funzione per eseguire un comando di protocollo
SPV_COMPROT_PAYLOAD   * SPVExecCommand          ( SPV_DEVICE * device, int command, int * retcode, SPV_COMPROT_PAYLOAD * payload_out );
// Funzione per terminare l'esecuzione di un comando di protocollo
int                     SPVKillCommand          ( SPV_COMPROT_THREAD * thread );
// Funzione di attivazione del thread di controllo comandi di protocollo
unsigned long __stdcall SPVCommandCheckThread   ( void * parameter );
// Funzione per ottenere il nome di un comando
char                  * SPVGetCommandName		( SPV_COMPROT_THREAD * thread );
// Funzione per ricavare il tipo di porta
int                     SPVGetPortType          ( XML_ELEMENT * p_element );
// Funzione per ricavare il nome di un tipo di porta
char                  * SPVGetPortTypeName      ( int port_type );
// Funzione per caricare tutte le porte di comunicazione
int                     SPVLoadPorts            ( XML_ELEMENT * p_element );
// Funzione per eliminare tutte le strutture delle porte di comunicazione
int                     SPVFreePorts            ( void );
// Funzione per assegnare una porta ad una periferica
int                     SPVAssignPort           ( SPV_DEVICE * device );
// Funzione per assegnare le porte alle periferiche del sistema
int                     SPVAssignSystemPorts    ( SPV_SYSTEM * system );
// Funzione per aprire tutte le porte del sistema
int                     SPVOpenPorts            ( void );
//
int						SPVSetComProtLogFunction( void * ptf );
// Funzione per ottenere l'UID della porta dal DB
char * 					SPVGetDBPortID			( SPV_COMPROT_PORT * port );
// Funzione per marchiare nel DB come rimosse tutte le porte di comunicazione
int						SPVDBRemoveAllPorts		( void );
// Funzione per salvare tutte le porte nel DB
int						SPVDBSavePorts			( char * SrvID );
// Funzione per eliminare dal DB le porte marchiate come rimosse
int						SPVDBDeleteRemovedPorts	( void );

//==============================================================================
// Variabili Globali Esterne
//------------------------------------------------------------------------------
extern SPV_COMPROT_THREAD        SPVComProtThreadList     [SPV_COMPROT_MAX_THREAD];
extern int                       SPVComProtThreadCount                          ;
extern SPV_EVENTLOG_MANAGER    * SPVComProtEventLog                             ;
extern SPV_COMPROT_PORT          SPVPortList              [SPV_COMPROT_MAX_PORT];
extern int                       SPVPortCount                                   ;

#endif

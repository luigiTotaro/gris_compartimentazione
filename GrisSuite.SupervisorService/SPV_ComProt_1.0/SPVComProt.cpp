//==============================================================================
// Telefin Supervisor Communication Protocol Module 1.0
//------------------------------------------------------------------------------
// MODULO PROTOCOLLO DI COMUNICAZIONE (SPVComProt.cpp)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:	0.88 (06.05.2004 -> 11.02.2008)
//
// Copyright:	2004-2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland DBS 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVComProt.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVComProt.h
//==============================================================================
#pragma hdrstop
#include "SPVComProt.h"
#include "cnv_lib.h"            
#include "SPVEventLog.h"
#include "SYSSocket.h"
#include "SPVDBManager.h"
#include "UtilityLibrary.h"
//------------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
//        SPV_COMPROT_TYPE        SPVComProtTypeList      [10]              ;
//        int                     SPVComProtTypeCount                       ;

//static  _CRITICAL_SECTION				SPVComProtThreadCriticalSection       ;
static  SYS_LOCK						SPVComProtThreadLock       				  ;
		SPV_COMPROT_THREAD				SPVComProtThreadList              [SPV_COMPROT_MAX_THREAD];
		int								SPVComProtThreadCount                 ;
		unsigned long					SPVComProtThreadFirst                 ; // Posizione del primo thread di protocollo da visualizzare
static  unsigned long					SPVComProtThreadNextID                ; // Prossimo ID da assegnare ad un thread di protocollo
		SPV_EVENTLOG_MANAGER *			SPVComProtEventLog              = NULL;
//static  _CRITICAL_SECTION *				SPVComProtSysDefCriticalSection = NULL;
static  SYS_LOCK *						SPVComProtSysDefLock			= NULL;
		SPV_COMPROT_PORT        		SPVPortList                       [SPV_COMPROT_MAX_PORT];
		int                     		SPVPortCount                          ;
static  SYS_THREAD *					SPVComProtCommandCheckThread          ; // Ptr a thread di controllo comandi
static  _SPV_COMPROT_LOG_FUNCTION_PTR	SPVComProtPrintLog				= NULL;

//==============================================================================
// Implementazione Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per inizializzare il modulo
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [01.08.2006]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int SPVComProtInit( SYS_LOCK * sys_def_lock )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  // --- Inizializzo il modulo socket ---
  SYSSocketInit( );
  /* Carico la libreria di protocollo di comunicazione */
  SPVComProtLibInit( );
  /* Assegno la sezione critica globale per la definizione del sistema */
  //SPVComProtSysDefCriticalSection = sys_def_cs;
  SPVComProtSysDefLock = sys_def_lock;
  /* Inizializzo la sezione critica globale per i thread di protocollo */
  //InitializeCriticalSection(&SPVComProtThreadCriticalSection);
  SYSInitLock(&SPVComProtThreadLock,"SPVComProtThreadLock");

  // --- Inizializzo le variabili globali dei thread di protocollo ---
  memset( SPVComProtThreadList, 0, sizeof(SPV_COMPROT_THREAD) * SPV_COMPROT_MAX_THREAD );
  SPVComProtThreadCount   = 0;
  SPVComProtThreadNextID  = 1;
  SPVComProtThreadFirst   = 0;

  // --- Inizializzo le variabili globali del thread di controllo comandi ---
  SPVComProtCommandCheckThread = NULL;

	/*
	SPVComProtEventLog = SPVNewEventLog( SPV_EVENTLOG_TYPE_ASCII_FILE, SPV_EVENTLOG_TEMPLATE_NONE, "comprot.log" );
	if ( SPVComProtEventLog != NULL )
	{
		// --- Apro l'EventLog Manager creato ---
		ret_code = SPVOpenEventLog( SPVComProtEventLog->ID );
		SPVPrintEventLog( SPVComProtEventLog->ID, "Inizio log" );
	}
	//*/

  return ret_code;
}                                           

//==============================================================================
/// Funzione per chiudere il modulo
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [20.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVComProtClear( void )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  // --- Chiudo la libreria di protocollo di comunicazione ---
  SPVComProtLibClear( );
  // --- Chiudo il modulo socket ---
  SYSSocketClear( );
  // --- Chiudo la sezione critica globale per i thread di protocollo ---
  //DeleteCriticalSection( &SPVComProtThreadCriticalSection );
  SYSClearLock(&SPVComProtThreadLock);
	/*
	ret_code = SPVCloseEventLog( SPVComProtEventLog->ID );
	//*/

	return ret_code;
}

//==============================================================================
/// Funzione per creare un nuovo buffer binario
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_COMPROT_BIN_BUFFER * SPVNewBinBuffer( void )
{
  SPV_COMPROT_BIN_BUFFER * bbuffer = NULL;

  bbuffer = (SPV_COMPROT_BIN_BUFFER*)malloc(sizeof(SPV_COMPROT_BIN_BUFFER));
  if ( bbuffer != NULL )
  {
    try
    {
      memset( (void*)bbuffer, 0, sizeof(SPV_COMPROT_BIN_BUFFER) );
      bbuffer->VCC  = SPV_COMPROT_BIN_BUFFER_VALIDITY_CHECK_CODE;
      bbuffer->len  = 0;
    }
    catch(...)
	{
      bbuffer = NULL;
    }
  }

  return bbuffer;
}

//==============================================================================
/// Funzione per eliminare un buffer binario
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDeleteBinBuffer( SPV_COMPROT_BIN_BUFFER * bbuffer )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( SPVValidBinBuffer( bbuffer ) )
  {
    try
    {
      memset( bbuffer, 0, sizeof(SPV_COMPROT_BIN_BUFFER) );
      free( bbuffer );
    }
    catch(...)
    {
      ret_code = SPV_COMPROT_CRITICAL_ERROR;
	}
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_BUFFER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di un buffer binario
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidBinBuffer( SPV_COMPROT_BIN_BUFFER * bbuffer )
{
  bool valid = false;

	if ( bbuffer != NULL )
  {
    try
    {
	  if ( bbuffer->VCC == SPV_COMPROT_BIN_BUFFER_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per cancellare i campi di una struttura field
///
/// Elimina con la funzione free() i campi allocati della struttura e azzera
/// tutti i valori della struttura.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVClearField( SPV_COMPROT_FIELD * field )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( field != NULL )
  {
    // Elimino i campi allocati
    try
		{
	  if ( field->Name  != NULL ) free( field->Name  ); // -FREE
      if ( field->Value != NULL ) free( field->Value ); // -FREE
    }
    catch(...)
    {
      ret_code = SPV_COMPROT_INVALID_POINTER;
    }
    // Azzero tutta la struttura
    memset( field, 0, sizeof( SPV_COMPROT_FIELD ) );
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_FIELD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di un campo
/// Dato il puntatore ad una preesistente struttura di un campo <field>, vi
/// carica gli attributi estraendoli dall'elemento XML <e_field>
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 1.03
//------------------------------------------------------------------------------
int SPVLoadField( SPV_COMPROT_FIELD * field, XML_ELEMENT * e_field )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( field != NULL ) // Controllo se la struttura del campo � gia stata allocata
  {
	memset( field, 0, sizeof(SPV_COMPROT_FIELD) ); // Azzero la memoria del field
    if ( e_field != NULL ) // Controllo che l'elemento XML di definizione sia valido
		{
      field->ID       = XMLGetValueInt  ( e_field, "id"    );
      field->Name     = XMLExtractValue ( e_field, "name"  );
      field->Type     = XMLGetTypeCode  ( XMLGetValue( e_field, "type" ) );
      field->ASCII    = XMLGetValueBool ( e_field, "ASCII" );
      field->Len      = XMLGetValueInt  ( e_field, "len"   );
      field->Count    = XMLGetValueInt  ( e_field, "count" );
      field->Value    = NULL; // Per ora il valore del campo non � definito
      field->Def      = (XML_ELEMENT*) e_field;
      field->Section  = SPV_COMPROT_FIELD_SECTION_UNKNOWN;
      // --- Identificazione della sezione di appartenenza del campo ---
      if ( strcmpi( XMLGetName( e_field ), "header"  ) == 0 ) field->Section = SPV_COMPROT_FIELD_SECTION_HEADER   ;
      if ( strcmpi( XMLGetName( e_field ), "payload" ) == 0 ) field->Section = SPV_COMPROT_FIELD_SECTION_PAYLOAD  ;
      if ( strcmpi( XMLGetName( e_field ), "trailer" ) == 0 ) field->Section = SPV_COMPROT_FIELD_SECTION_TRAILER  ;
      if ( strcmpi( XMLGetName( e_field ), "user"    ) == 0 ) field->Section = SPV_COMPROT_FIELD_SECTION_USER     ;
      if ( strcmpi( XMLGetName( e_field ), "hidden"  ) == 0 ) field->Section = SPV_COMPROT_FIELD_SECTION_HIDDEN   ;
    }
    else
    {
	  ret_code = SPV_COMPROT_INVALID_DEFINITION;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_ALLOCATION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di un frame
/// Dato il puntatore ad una preesistente struttura di un frame <frame>, vi
/// carica gli attributi estraendoli dall'elemento XML <e_frame>
/// Non vengono inseriti nella definizione del frame i campi 'user'.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SPVLoadFrame( SPV_COMPROT_FRAME * frame, XML_ELEMENT * e_frame )
{
  SPV_COMPROT_FIELD * field_list  = NULL  ; // Ptr alla lista delle strutture campo del frame
  int                 field_count = 0     ; // Numero di cami effettivi del frame
  int                 ret_code    = 0     ; // Codice di ritorno
  XML_ELEMENT       * e_field     = NULL  ; // Ptr a elemento XML campo
  SPV_COMPROT_FIELD * field       = NULL  ; // Ptr a struttura campo

  if ( frame != NULL ) // Controllo se la struttura del frame � gia stata allocata
  {
    if ( e_frame != NULL ) // Controllo che l'elemento XML di definizione sia valido
    {
      frame->ID         = XMLGetValueInt  ( e_frame, "id"   );
      frame->Name       = XMLExtractValue ( e_frame, "name" );
      frame->Def        = (XML_ELEMENT*)    e_frame;
      frame->FieldCount = XMLGetChildCount( e_frame, false  ); // Conto i campi
	  if ( frame->FieldCount > 0 ) // Se ho almeno un campo
      {
        // --- Prima fase: allocazione temporanea e caricamento/analisi dei campi ---
        field_list = (SPV_COMPROT_FIELD*) malloc( sizeof(SPV_COMPROT_FIELD) * frame->FieldCount );
        e_field = e_frame->child; // Prendo il primo child
        for ( int f = 0; f < frame->FieldCount; f++ ) // Ciclo per caricare le def dei campi
        {
          field = &(field_list[field_count]); // Prendo la locazione successiva per il frame
          if ( e_field != NULL ) // Controllo che l'elemento XML del campo f-esimo sia valido
		  {
            ret_code  = SPVLoadField( field, e_field ); // Carico il campo
            if ( ret_code == SPV_COMPROT_NO_ERROR ) // Se il campo � stato caricato senza errori
            {
			  if ( field->Section != SPV_COMPROT_FIELD_SECTION_USER ) // Se il campo non � di tipo 'user'
              {
                field_count++; // Allora avanzo nelle posizioni della lista
              }
							else
              {
                SPVClearField( field );
              }
            }
            else
            {
              SPVClearField( field );
            }
            e_field   = e_field->next; // Prendo il prossimo campo
          }
          else
          {
            f = frame->FieldCount;
            ret_code = SPV_COMPROT_INVALID_COUNT;
          }
        }
        // --- Seconda fase: allocazione definitiva a copia della lista campi ---
        if ( field_count > 0 ) // Se ho caricato almeno un field valido
        {
          frame->FieldCount = field_count;
		  frame->FieldList  = (SPV_COMPROT_FIELD*) malloc( sizeof(SPV_COMPROT_FIELD) * field_count ); // Allocazione definitiva
          if ( frame->FieldList != NULL ) // Controllo che l'allocazione si andata a buon fine
          {
            memcpy( frame->FieldList, field_list, sizeof(SPV_COMPROT_FIELD) * field_count ); // Copia della lista
          }
		  else
          {
            ret_code = SPV_COMPROT_INVALID_ALLOCATION;
          }
		}
        else
        {
		  frame->FieldCount = 0;
          frame->FieldList  = NULL;
        }
        free( field_list ); // Elimino la lista temporanea
			}
      else
      {
        frame->FieldCount = 0;
        frame->FieldList  = NULL;
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_DEFINITION;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_ALLOCATION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di uno step
/// Dato il puntatore ad una preesistente struttura di step <step>, vi
/// carica gli attributi estraendoli dall'elemento XML <e_step>
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [15.01.2010]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int SPVLoadStep( SPV_COMPROT_STEP * step, XML_ELEMENT * e_step )
{
	int             ret_code  = 0     ; // Codice di ritorno
	XML_ELEMENT   * e_parent  = NULL  ; // Ptr a elemento XML parent
	char          * value     = NULL  ; // Ptr a stringa di appoggio

	if ( step != NULL ) // Controllo se il ptr a struttra step � valido
	{
		if ( e_step != NULL ) // Controllo se il ptr a elemento XML step
		{
			step->ID      	= XMLGetValueInt	( e_step, "ID"      );
			step->PID     	= XMLGetValueInt	( e_step, "pID"     );
			step->Name    	= XMLExtractValue	( e_step, "name"    );
			step->FrameID 	= XMLGetValueInt	( e_step, "frame"   );
			step->Type    	= XMLGetValueInt	( e_step, "type"    );
			step->Timeout 	= XMLGetValueInt	( e_step, "timeout" );
			step->Retry   	= XMLGetValueInt	( e_step, "retry"   );
			step->Retry   	= (step->Retry > 1)?step->Retry:1; // Il numero di tentativi � almeno 1!
			step->Delay   	= XMLGetValueInt	( e_step, "delay"   );
			step->Count   	= XMLGetValueInt	( e_step, "count"   );
			step->Count   	= (step->Count > 1)? step->Count:1; // Il conteggio � minimo 1
			step->Def     	= (XML_ELEMENT*)  e_step;
			step->Cycle   	= SPV_COMPROT_STEP_CYCLE_NONE;
			step->FailID	= XMLGetValueInt	( e_step, "fail"    );
			step->SuccessID	= XMLGetValueInt	( e_step, "success" );
			value			= XMLGetValue		( e_step, "cycle"   );
			if ( value != NULL ) // Controllo se l'attributo cycle � stato specificato
			{
				if ( strcmpi( value, "FOR" )   == 0 ) step->Cycle = SPV_COMPROT_STEP_CYCLE_FOR;
				if ( strcmpi( value, "WHILE" ) == 0 ) step->Cycle = SPV_COMPROT_STEP_CYCLE_WHILE;
			}
			e_parent = e_step->parent; // Recupero il ptr all'elemento parent
			if ( e_parent != NULL )
			{
				if ( strcmpi( e_parent->name, "step" ) == 0 ) // Se il parent � un altro step
				{
					step->PrevID = XMLGetValueInt( e_parent, "ID" );
				}
				else // Se non ho uno step parent
				{
					step->PrevID = -1; // Ovviamente solo il primo step
				}
			}
			else
			{
				ret_code = SPV_COMPROT_INVALID_DEFINITION;
			}
		}
		else
		{
			ret_code = SPV_COMPROT_INVALID_DEFINITION;
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_STEP;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di un comando
/// Dato il puntatore ad una preesistente struttura di comando <command>, vi
/// carica gli attributi estraendoli dall'elemento XML <e_command>
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [27.10.2005]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int SPVLoadCommand( SPV_COMPROT_COMMAND * command, XML_ELEMENT * e_command )
{
	int                       ret_code  = 0     ; // Codice di ritorno
	XML_CHILD_FUNCTION_PTR    ptf       = NULL  ; // Ptr a funzione child

	if ( command != NULL ) // Controllo il ptr struttura comando
	{
		if ( e_command != NULL ) // Controllo l'elemento root di definizione comando
		{
			command->ID         = XMLGetValueInt  ( e_command, "ID"       ) ;
			command->Name       = XMLExtractValue ( e_command, "name"     ) ;
			command->Type       = XMLExtractValue ( e_command, "type"     ) ;
			command->Blocking   = XMLGetValueBool ( e_command, "blocking" ) ;
			command->Def        = (XML_ELEMENT*) e_command;
			//--- Carico gli step ---
			command->StepCount  = XMLGetChildCount( e_command, true ); // Conto quanti step ci sono
			if ( command->StepCount > 0 ) // Se c'� almeno uno step
			{
				command->StepList = (SPV_COMPROT_STEP*) malloc(sizeof(SPV_COMPROT_STEP)*command->StepCount);
				memset( (void*)command->StepList, 0, sizeof(SPV_COMPROT_STEP)*command->StepCount );
				ptf = (XML_CHILD_FUNCTION_PTR) SPVLoadStep;
				ret_code = XMLChildFunction( e_command, command->StepList, sizeof(SPV_COMPROT_STEP), ptf );
			}
			else
			{
				ret_code = SPV_COMPROT_INVALID_COUNT;
			}
		}
		else
		{
			ret_code = SPV_COMPROT_INVALID_DEFINITION;
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_COMMAND;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di un protocollo di comunicazione
/// Dato il puntatore ad una preesistente struttura di protocollo <comprot>, vi
/// carica gli attributi estraendoli dall'elemento XML <e_comprot>.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [11.07.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVLoadComProt( SPV_COMPROT_TYPE * comprot, XML_ELEMENT * e_comprot )
{
  XML_ELEMENT         * e_command = NULL; // Ptr a elemento XML comando
  XML_ELEMENT         * e_frame   = NULL; // Ptr a elemento XML frame
  SPV_COMPROT_COMMAND * command   = NULL; // Ptr a struttura comando
  SPV_COMPROT_FRAME   * frame     = NULL; // Ptr a struttura frame
  int                   ret_code  = 0   ; // Codice di ritorno

  if ( comprot != NULL ) // Controllo se il ptr a struttura protocollo � valido
  {
	if ( e_comprot != NULL ) // Controllo l'elemento root di definizione protocollo
	{
	  //EnterCriticalSection( SPVComProtSysDefCriticalSection );
	  SYSLock(SPVComProtSysDefLock,"SPVLoadComProt");   //? M //
	  //--- Estraggo i campi del tipo protocollo ---
	  comprot->ID   = XMLGetValueInt  ( e_comprot, "ID"   );
	  comprot->Code = XMLExtractValue ( e_comprot, "Code" );
	  comprot->Name = XMLExtractValue ( e_comprot, "Name" );
	  comprot->Def  = (XML_ELEMENT*) e_comprot;
	  comprot->Tag  = NULL;
	  //--- Estraggo le liste di frame ---
	  e_frame = XMLGetNext( e_comprot->child, "frames", -1 );
	  if ( e_frame != NULL ) // Se ho trovato l'elemento <frames>
	  {
		comprot->FrameCount = XMLGetChildCount( e_frame, false );
		if ( comprot->FrameCount > 0 ) // Se c'� almeno una def di frame
		{
		  comprot->FrameList = (SPV_COMPROT_FRAME*)malloc(sizeof(SPV_COMPROT_FRAME)*comprot->FrameCount); // Alloco la lista
		  e_frame = e_frame->child; // Prendo il primo <item>
		  for( int f=0; f<comprot->FrameCount; f++ ) // Ciclo sui frame
		  {
			frame     = &comprot->FrameList[f];
			ret_code  = SPVLoadFrame( frame, e_frame ); // Carico il frame
			e_frame   = e_frame->next;
		  }
				}
		else
		{
		  ret_code = SPV_COMPROT_INVALID_COUNT;
		}
	  }
	  else
	  {
		ret_code = SPV_COMPROT_INVALID_DEFINITION;
	  }
	  //--- Estraggo le liste di comandi ---
	  e_command = XMLGetNext( e_comprot->child, "commands", -1 );
	  if ( e_command != NULL ) // Se ho trovato l'elemento <commands>
	  {
		comprot->CommandCount = XMLGetChildCount(e_command,false);
		if ( comprot->CommandCount > 0 ) // Se c'� almeno una def di comando
		{
		  comprot->CommandList = (SPV_COMPROT_COMMAND*)malloc(sizeof(SPV_COMPROT_COMMAND)*(comprot->CommandCount)); // Alloco la lista
		  memset( (void*)comprot->CommandList, 0, sizeof(SPV_COMPROT_COMMAND)*(comprot->CommandCount));
		  e_command = e_command->child; // Prendo il primo <item>
		  for( int f=0; f<comprot->CommandCount; f++ ) // Ciclo sui comandi
		  {
			command   = &comprot->CommandList[f];
			ret_code  = SPVLoadCommand(command,e_command); // Carico il comando
			e_command = e_command->next;
		  }
		}
		else
		{
		  ret_code = SPV_COMPROT_INVALID_COUNT;
		}
	  }
	  else
	  {
		ret_code = SPV_COMPROT_INVALID_DEFINITION;
	  }
	  //LeaveCriticalSection(SPVComProtSysDefCriticalSection);
	  SYSUnLock(SPVComProtSysDefLock);
	}
	else
	{
	  ret_code = SPV_COMPROT_INVALID_DEFINITION;
	}
  }
  else
  {
	ret_code = SPV_COMPROT_INVALID_COMPROT_TYPE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare la definizione di un campo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [02.04.2007]
/// \author Enrico Alborali
/// \version 1.03
//------------------------------------------------------------------------------
int SPVDeleteField( SPV_COMPROT_FIELD * field )
{
	int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

	if ( field != NULL ) // Verifico la validit� del ptr a campo
	{
		// --- Cancello il nome del campo ---
		if ( field->Name  != NULL )
		{
		    try
			{
		    	free( field->Name );
		    }
		    catch(...)
		    {
		    	ret_code = SPV_COMPROT_CRITICAL_ERROR;
		    	SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del nome", "SPVDeleteField" );
		    	/* DONE -oEnrico -cError : Gestione dell'errore critico */
			}
		}
	}
	// --- Cancello il valore del campo ---
	if ( field->Value != NULL )
	{
		try
		{
			free( field->Value );
		}
		catch(...)
		{
			ret_code = SPV_COMPROT_CRITICAL_ERROR;
			SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del valore", "SPVDeleteField" );
			/* DONE -oEnrico -cError : Gestione dell'errore critico */
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_FIELD;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la definizione di un frame
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [02.04.2007]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDeleteFrame( SPV_COMPROT_FRAME * frame )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( frame != NULL ) // Verifico la validit� del ptr a frame
  {
    // --- Cancello il nome del frame ---
    if ( frame->Name != NULL )
    {
      try
      {
        free( frame->Name );
      }
      catch(...)
	  {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del nome", "SPVDeleteFrame" );
		/* DONE -oEnrico -cError : Gestione dell'errore critico */
      }
    }
    // --- Cancello la lista dei campi ---
    if ( frame->FieldList != NULL && frame->FieldCount>0 )
    {
      for ( int f = 0; f < frame->FieldCount; f++ )
      {
        ret_code = SPVDeleteField( &frame->FieldList[f] );
      }
      try
      {
        free( frame->FieldList );
      }
      catch(...)
	  {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione della lista field", "SPVDeleteFrame" );
		/* DONE -oEnrico -cError : Gestione dell'errore critico */
	  }
    }
  }
  else
	{
    ret_code = SPV_COMPROT_INVALID_FRAME;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare la definizione di uno step
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [02.04.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDeleteStep( SPV_COMPROT_STEP * step )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( step != NULL )
  {
	// --- Cancello il nome dello step ---
	if ( step->Name != NULL )
	{
	  try
	  {
		free( step->Name );
	  }
	  catch(...)
	  {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del nome", "SPVDeleteStep" );
		/* DONE -oEnrico -cError : Gestione dell'errore critico */
      }
    }
  }
  else
	{
    ret_code = SPV_COMPROT_INVALID_STEP;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare la definizione di un comando
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [02.04.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVDeleteCommand( SPV_COMPROT_COMMAND * command )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( command != NULL )
  {
	// --- Cancello il nome del comando ---
    if ( command->Name != NULL )
		{
      try
      {
        free( command->Name ); // -FREE
	  }
      catch(...)
      {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		/* DONE -oEnrico -cError : Gestione dell'errore critico */
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del nome", "SPVDeleteCommand" );
	  }
	}
	// --- Cancello il tipo del comando ---
	if ( command->Type != NULL )
	{
	  try
	  {
		free( command->Type ); // -FREE
	  }
	  catch(...)
	  {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		/* DONE -oEnrico -cError : Gestione dell'errore critico */
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del tipo", "SPVDeleteCommand" );
	  }
	}
	// --- Cancello la lista degli step ---
	if ( command->StepList != NULL && command->StepCount > 0 )
	{
	  for ( int s = 0; s < command->StepCount; s++ )
	  {
		ret_code = SPVDeleteStep( &command->StepList[s] );
	  }
	  try
	  {
		// --- Resetto la lista ---
		memset( command->StepList, 0, sizeof(SPV_COMPROT_STEP) * command->StepCount );
	  }
	  catch(...)
	  {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante azzeramento memoria lista step", "SPVDeleteCommand" );
	  }
	  // --- Cancello tutta la lista ---
	  try
	  {
		free( command->StepList ); // -FREE
			}
	  catch(...)
	  {
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
		/* DONE -oEnrico -cError : Gestione dell'errore critico */
		SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione della lista step", "SPVDeleteCommand" );
	  }
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_COMMAND;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova definizione di protocollo
///
/// \date [31.08.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_COMPROT_TYPE * SPVNewComProt( void )
{
  SPV_COMPROT_TYPE * comprot = NULL;

  comprot = (SPV_COMPROT_TYPE*)malloc( sizeof(SPV_COMPROT_TYPE) );

  if ( comprot != NULL )
  {
    memset( comprot, 0, sizeof(SPV_COMPROT_TYPE) );
  }

  return comprot;
}

//==============================================================================
/// Funzione per eliminare una definizione di protocollo
///
/// \date [02.04.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDeleteComProt( SPV_COMPROT_TYPE * comprot )
{
	int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

	if ( comprot != NULL )
	{
		// --- Cancello il codice del protocollo ---
		if ( comprot->Code != NULL )
		{
			try
			{
				free( comprot->Code );
			}
			catch(...)
			{
				ret_code = SPV_COMPROT_CRITICAL_ERROR;
				/* DONE -oEnrico -cError : Gestione dell'errore critico */
				SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del codice", "SPVDeleteComProt" );
			}
		}
		// --- Cancello il nome del protocollo ---
		if ( comprot->Name != NULL )
		{
			try
			{
				free( comprot->Name );
			}
    		catch(...)
    		{
    			ret_code = SPV_COMPROT_CRITICAL_ERROR;
    			/* DONE -oEnrico -cError : Gestione dell'errore critico */
				SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del nome", "SPVDeleteComProt" );
			}
		}
		// --- Cancello la lista dei frame del protocollo ---
		if ( comprot->FrameList != NULL && comprot->FrameCount > 0 )
		{
			for ( int f = 0; f < comprot->FrameCount; f++ )
			{
				ret_code = SPVDeleteFrame( &comprot->FrameList[f] );
			}
			try
			{
				free( comprot->FrameList );
			}
			catch(...)
			{
				ret_code = SPV_COMPROT_CRITICAL_ERROR;
				/* DONE -oEnrico -cError : Gestione dell'errore critico */
				SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione lista frame", "SPVDeleteComProt" );
			}
		}
		// --- Cancello la lista dei comandi del protocollo ---
		if ( comprot->CommandList != NULL && comprot->CommandCount > 0 )
		{
			for ( int c = 0; c < comprot->CommandCount; c++ )
			{
				ret_code = SPVDeleteCommand( &comprot->CommandList[c] );
			}
			try
			{
				free( comprot->CommandList );
			}
			catch(...)
			{
				ret_code = SPV_COMPROT_CRITICAL_ERROR;
				/* DONE -oEnrico -cError : Gestione dell'errore critico */
				SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione lista comandi", "SPVDeleteComProt" );
			}
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_COMPROT_TYPE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiungere una def. di protoccllo alla lista globale
///
/// \date [19.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAddComProt( SPV_COMPROT_TYPE * comprot )
{
  int ret_code = 0; // Codice di ritorno

  if (comprot != NULL) // Controllo che il prt a protocollo sia valido
  {
    /////memcpy(&SPVComProtTypeList[SPVComProtTypeCount],comprot,sizeof(SPV_COMPROT_TYPE));
    /////SPVComProtTypeCount++;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere una def. di protocollo dalla lista globale
///
/// \date [20.07.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
SPV_COMPROT_TYPE * SPVGetComProt( int id )
{
	SPV_COMPROT_TYPE * comprot = NULL;

  if (id >= 0)
  {
    /////if (SPVComProtTypeCount > 0)
    /*
    {
      for (int t=0; t<SPVComProtTypeCount; t++)
      {
		if (SPVComProtTypeList[t].ID == id) // Se � il tipo protocollo che cercavo
        {
          comprot = &SPVComProtTypeList[t];
          t = SPVComProtTypeCount; // Fine del ciclo
        }
      }
    }
    */
  }

  return comprot;
}

//==============================================================================
/// Funzione per togliere una def. di protocollo dalla lista
///
/// \date [19.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVRemoveComProt( int id )
{
  int ret_code = SPV_COMPROT_FUNCTION_NOT_IMPLEMENTED;

  return ret_code;
}

//==============================================================================
/// Funzione per caricare il protocollo di comunicazione di una periferica
///
/// \date [31.01.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVLoadDevComProt( SPV_DEVICE * device )
{
  int                 ret_code  = 0   ; // Codice di ritorno
  XML_ELEMENT       * comprot_e = NULL; //
  SPV_COMPROT_TYPE  * comprot   = NULL; //

  if ( device != NULL )
  {
    comprot   = SPVNewComProt();
    comprot_e = XMLGetNext( device->Def->child->child, "comprot", -1 );

    ret_code = SPVLoadComProt( comprot, comprot_e );
    if ( ret_code == 0 )
    {
      device->SupCfg.ComProt = comprot;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_DEVICE;
  }

  return ret_code;
}
//==============================================================================
/// Funzione per eliminare il protocollo di comunicazione di una periferica
///
/// \date [02.04.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVFreeDevComProt( SPV_DEVICE * device )
{
	int                 ret_code  = SPV_COMPROT_NO_ERROR; // Codice di ritorno
	SPV_COMPROT_TYPE  * comprot   = NULL; //

	if ( device != NULL )
	{
		// --- Recupero il ptr alla struttura protocollo ---
		comprot = (SPV_COMPROT_TYPE*) device->SupCfg.ComProt;
		if ( comprot != NULL )
		{
			// --- Elimino i parametri di definizione allocati per il protocollo ---
			ret_code = SPVDeleteComProt( comprot );
			// --- Elimino la struttura del protocollo ---
			try
			{
				free( comprot );
			}
			catch(...)
			{
				ret_code = SPV_COMPROT_CRITICAL_ERROR;
				/* DONE -oEnrico -cError : Gestione dell'errore critico */
				SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del protocollo", "SPVFreeDevComProt" );
			}
			device->SupCfg.ComProt = NULL;
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_DEVICE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare il protocollo di comunicazione di tutto il sistema
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVLoadSystemComProt( SPV_SYSTEM * system )
{
  int ret_code  = 0 ; // Codice di ritorno

  ret_code = SPVDoOnDevices( system, SPVLoadDevComProt );

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare i protocolli di comunicazione di tutto il sistema
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [31.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreeSystemComProt( SPV_SYSTEM * system )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( system != NULL )
  {
    ret_code = SPVDoOnDevices( system, SPVFreeDevComProt );
  }
  else
  {
	ret_code = SPV_COMPROT_INVALID_SYSTEM;
  }

	return ret_code;
}

//==============================================================================
/// Funzione per creare un nuovo payload
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_COMPROT_PAYLOAD * SPVNewPayload( int id, SPV_COMPROT_BIN_BUFFER * buffer )
{
  SPV_COMPROT_PAYLOAD * payload = NULL;

  payload = (SPV_COMPROT_PAYLOAD*) malloc( sizeof(SPV_COMPROT_PAYLOAD) ); // -MALLOC

  if ( payload != NULL ) // Se l'allocazione � andata a buon fine
  {
    payload->ID     = id      ;
	payload->Buffer = buffer  ;
    payload->Next   = NULL    ;
  }

  return payload;
}

//==============================================================================
/// Funzione per eliminare un payload
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDeletePayload( SPV_COMPROT_PAYLOAD * payload )
{
  int ret_code = 0; // Codice di ritorno

	if ( payload != NULL )
  {
    try
    {
      if ( payload->Buffer != NULL )
      {
        SPVDeleteBinBuffer( payload->Buffer );
      }
      free( payload );
    }
    catch(...)
    {
      ret_code = SPV_COMPROT_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_PAYLOAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere un payload ad un thread di protocollo
///
/// \date [14.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAddPayload( SPV_COMPROT_THREAD * thread, SPV_COMPROT_PAYLOAD * payload )
{
  int                   ret_code  = 0   ; // Codice di ritorno
  SPV_COMPROT_PAYLOAD * current   = NULL;

  if ( thread != NULL )
  {
		if ( payload != NULL )
    {
      current = thread->FirstPayload;

      if ( current == NULL )
      {
        thread->FirstPayload = payload;
      }
      else
      {
        while ( current->Next != NULL ) // Cerco l'ultimo payload
        {
          current = (SPV_COMPROT_PAYLOAD *) current->Next;
        }
        current->Next = payload; // Appendo il payload alla lista
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_PAYLOAD;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere un payload da un thread di protocollo
///
/// In caso di errore restituisce NULL
///
/// \date [01.06.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_COMPROT_PAYLOAD * SPVGetPayload( SPV_COMPROT_THREAD * thread, int id )
{
  SPV_COMPROT_PAYLOAD * payload = NULL;
  SPV_COMPROT_PAYLOAD * current = NULL;

  if ( thread != NULL )
  {
    current = thread->FirstPayload; // Parto dal primo payload
    while ( current != NULL )
    {
      if ( current->ID == id )
      {
        payload = current;
        return payload;
      }
      current = (SPV_COMPROT_PAYLOAD *) current->Next;
    }
  }

  return payload;
}

//==============================================================================
/// Funzione per ottenere il prossimo payload
///
/// In caso di errore restituisce NULL
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_COMPROT_PAYLOAD * SPVGetNextPayload( SPV_COMPROT_PAYLOAD * payload )
{
  SPV_COMPROT_PAYLOAD * next    = NULL;
  SPV_COMPROT_PAYLOAD * current = NULL;

  if ( payload != NULL )
  {
    current = payload;
    while ( current->Next != NULL )
    {
      if ( current->ID == payload->ID)
      {
        next = current;
        return next;
      }
      current = (SPV_COMPROT_PAYLOAD *) current->Next;
		}
  }

  return next;
}

//==============================================================================
/// Funzione per eliminare una lista di payload
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.08.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDeletePayloadList( SPV_COMPROT_PAYLOAD * payload )
{
  int                   ret_code  = 0   ; // Codice di ritorno
  SPV_COMPROT_PAYLOAD * current   = NULL; // Ptr ad un elemento della lista di payload
  SPV_COMPROT_PAYLOAD * next      = NULL; // Ptr ad un elemento della lista di payload

  if (payload != NULL) // Controllo la validit�
  {
    current = payload;
    while (current) // Ciclo sulla lista dei payload
    {
      next    = (SPV_COMPROT_PAYLOAD*) current->Next;
      SPVDeletePayload(current);
      current = next;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_PAYLOAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare un nuovo descrittore di ciclo
///
/// In caso di errore restituisce NULL.
///
/// \date [16.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_COMPROT_CYCLE * SPVNewCycle( int step_id, int type )
{
  SPV_COMPROT_CYCLE * cycle = NULL; // Ptr alla nuova struttura descrittore di ciclo

  cycle = new SPV_COMPROT_CYCLE;

  if (cycle != NULL) // Se l'allocazione � andata a buon fine
  {
    cycle->Type   = type;
    cycle->Step   = step_id;
    cycle->Num    = 0;
    cycle->Upper  = NULL;
  }

  return cycle;
}

//==============================================================================
/// Funzione per eliminare un descrittore di ciclo
///
/// \date [16.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDeleteCycle( SPV_COMPROT_CYCLE * cycle )
{
  int ret_code = 0; // Codice di ritorno

  if (cycle != NULL)
  {
    delete cycle;
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_CYCLE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiungere un (sotto)ciclo al thread
///
/// \date [16.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAddCycle( SPV_COMPROT_THREAD * thread, SPV_COMPROT_CYCLE * cycle )
{
  int ret_code = 0; // Codice di ritorno

  if (thread != NULL)
  {
    if (cycle != NULL)
    {
      cycle->Upper  = thread->Cycle;
      thread->Cycle = cycle;
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_CYCLE;
	}
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per rimuovere il (sotto)ciclo dal thread
///
/// \date [16.07.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVRemoveCycle( SPV_COMPROT_THREAD * thread )
{
  int ret_code = 0; // Codice di ritorno

  if (thread != NULL)
  {
    if (thread->Cycle != NULL)
    {
      thread->Cycle = (SPV_COMPROT_CYCLE*)thread->Cycle->Upper;
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_CYCLE;
    }
  }
  else
  {
	ret_code = SPV_COMPROT_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ricavare la lunghezza di un campo di un frame
/// La lunghezza si sriferisce al numero di byte o caratteri nel buffer del
/// frame.
/// In caso di errore la funzione restituisce 0.
///
/// \date [28.06.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVGetFieldLen( SPV_COMPROT_FIELD * field )
{
  int len       = 0;
  int field_len = 0; // Lunghezza dei dati del campo

  // --- Blocco il cestino XML ---
  //XMLLockTrash();

  if ( field  ) // Controllo la validit� del ptr a struttura campo
  {
	field_len = XMLGetValueInt( field->Def, "len" );
	field_len = XMLGetValueInt( field->Def, "len" );
	if ( field_len < 0 ) field_len = 0; // La lunghezza minima � 0
    if ( field->ASCII )
    {
      len = XMLCharSizeOf( field->Type ) * field_len; // Recupero la lunghezza in caratteri ASCII
    }
    else
	{
	  len = XMLByteSizeOf( field->Type ) * field_len; // Recupero la lunghezza in byte
	}
  }

  // --- Svuoto il cestino XML ---
  //XMLEmptyTrash( );

  // --- Sblocco il cestino XML ---
  //XMLUnlockTrash( );

  return len;
}

//==============================================================================
/// Funzione per recuperare valori interni al programma
/// Dato il ptr alla struttura di una periferica <device> o il ptr alla
/// struttura di un thread <thread> e una stringa <name> che specifica il nome
/// di un dato interno al programma, viene recuperato il suo valore binario e
/// restituito dalla funzione.
/// In caso di errore la funzione restituisce NULL.
///
/// \date [26.09.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
char * SPVGetInternalValue( SPV_DEVICE * device, SPV_COMPROT_THREAD * thread, char * name )
{
  char                * value   = NULL; // Ptr al buffer che conterr� il valore
  int                   PID     = 0   ;
  SPV_COMPROT_COMMAND * command = NULL; // Ptr comando di protocollo
  SPV_COMPROT_STEP    * step    = NULL; // Ptr Step di thread protocollo
  SPV_COMPROT_PAYLOAD * payload = NULL; // Ptr a struttura payload

  if ( name != NULL ) // Verifico la validit� del ptr al nome del dato
  {
    // --- Caso Indirizzo periferica ---
    if ( strcmpi( name, "__device.addr" ) == 0 )
    {
      if ( device != NULL )
      {
        value = (char*)malloc( sizeof( device->SupCfg.Address ) ); // -MALLOC
        *value = (char)device->SupCfg.Address; // Copio il valore
      }
    }
    // --- Caso Buffer Stream Perifierica ---
    if ( strcmpi( name, "__thread.payload_buffer" ) == 0 )
    {
      if ( device != NULL )
      {
        if ( thread != NULL )
        {
          command = &thread->Type->CommandList[thread->Command];
          if ( command != NULL )
          {
            if ( thread->Step >= 0 && thread->Step < command->StepCount ) // Controllo che non ci sia un overflow
            {
              step = &command->StepList[thread->Step];
              if ( step != NULL )
			  {
                PID = step->PID;
                if ( PID > 0 )
                {
                  payload = SPVGetPayload( thread, PID );
                  if ( payload != NULL )
                  {
                    value = (char*)malloc( sizeof( char ) * (payload->Buffer->len + 1) ); // -MALLOC
                    memcpy( value, payload->Buffer->buffer, sizeof( char ) * payload->Buffer->len );
                    value[payload->Buffer->len] = NULL; // Aggiungo un NULL-terminatore di stringa
				  }
                }
              }
            }
          }
        }
      }
    }
    // --- Caso Lunghezza Stream Periferica ---
    if ( strcmpi( name, "__thread.payload_len" ) == 0 )
    {
      if ( device != NULL )
      {
        if ( thread != NULL )
        {
          command = &thread->Type->CommandList[thread->Command];
          if ( command != NULL )
          {
            if ( thread->Step >= 0 && thread->Step < command->StepCount ) // Controllo che non ci sia un overflow
            {
              step = &command->StepList[thread->Step];
              if ( step != NULL )
              {
                PID = step->PID;
                if ( PID > 0 )
                {
                  payload = SPVGetPayload( thread, PID );
				  if ( payload != NULL )
                  {
                    value = (char*)malloc( sizeof( payload->Buffer->len ) ); // -MALLOC
                    *value = (char)payload->Buffer->len; // Copio il valore
                  }
                }
              }
			}
          }
        }
      }
    }
    // --- Caso Contatore del thread ---
    if ( strcmpi( name, "__thread.counter" ) == 0 )
    {
      if ( thread != NULL )
      {
        value = (char*)malloc( sizeof( thread->Counter ) ); // -MALLOC
        *value = (char)thread->Counter; // Copio il valore
      }
    }
    // --- Caso Contatore del thread ---
    if ( strcmpi( name, "__thread.counter-1" ) == 0 )
    {
      if ( thread != NULL )
      {
        value = (char*)malloc( sizeof( thread->Counter ) ); // -MALLOC
        *value = (char)(thread->Counter-1); // Copio il valore
      }
    }
    // --- Caso Contatore del ciclo thread ---
    if ( strcmpi( name, "__thread.cycle.counter" ) == 0 )
    {
      if ( thread != NULL )
      {
        if ( thread->Cycle != NULL )
        {
		  value = (char*)malloc( sizeof( thread->Cycle->Num ) ); // -MALLOC
          *value = (char)thread->Cycle->Num; // Copio il valore
        }
        else
		{
          value = (char*)malloc( sizeof( thread->Cycle->Num ) ); // -MALLOC
          *value = 0; // Copio il valore
        }
      }
    }
    /* Aggiungere qui di seguito i nuovi casi */
  }

  return value;
}

//==============================================================================
/// Funzione per ottenere la lunghezza minima di un frame
/// La minima lunghezza di un frame � calcolata sommando tutte le lunghezze dei
/// singoli campi che lo compongono.
/// Se un campo non ha lunghezza specificata viene preso come valore 1 (per
/// questo � la lunghezza minima perch� potrebbe essere maggiore).
/// Restuisce la lunghezza del frame in byte.
/// In caso di errore restituisce 0.
///
/// \date [07.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVGetMinFrameLength( SPV_COMPROT_FRAME * frame )
{
  int                   min_length  = 0   ; // Lunghezza minima del frame
  SPV_COMPROT_FIELD   * field       = NULL; // Ptr a campo
  int                   field_len   = 0   ; // Lunghezza del campo

  if ( frame != NULL ) // Controllo la valisit� del ptr al frame
  {
	for ( int f = 0; f < frame->FieldCount; f++ )
	{
      field = &frame->FieldList[f];
      if ( field != NULL )
      {
        // --- Identificazione della sezione di appartenenza del campo ---
        if ( field->Section == SPV_COMPROT_FIELD_SECTION_HEADER
          || field->Section == SPV_COMPROT_FIELD_SECTION_TRAILER )
        {
          field_len = SPVGetFieldLen( field );
          field_len = ( field_len > 0 ) ? field_len : 1;
        }
        else if ( field->Section == SPV_COMPROT_FIELD_SECTION_PAYLOAD )
        {
          field_len = field->Len;
          field_len = ( field_len >= 0 ) ? field_len : 0;
        }
        // Per gli altri tipi (es: USER) non viene calcolata la lunghezza
        // perch� non fanno parte del frame effettivo
      }
      else
      {
        field_len = 0;
      }
      min_length += field_len;
    }
  }

  return min_length;
}

//==============================================================================
/// Funzione per comporre un frame di protocollo
/// Dato il ptr alla struttura di definizione di un frame di protocollo <frame>
/// e una struttura dati periferica puntata dal ptr <device>, viene generato un
/// frame e immagazzinato nel buffer binario <bbuffer>.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.15
//------------------------------------------------------------------------------
int SPVBuildFrame( SPV_COMPROT_BIN_BUFFER * bbuffer, SPV_COMPROT_FRAME * frame, SPV_DEVICE * device, SPV_COMPROT_THREAD * thread )
{
  int                 ret_code    = 0     ; // Codice di ritorno
  SPV_COMPROT_FIELD * field       = NULL  ; // Ptr a struttura campo di frame
  int                 offset      = 0     ; // Offset del buffer binario del frame
  char              * buf_field   = NULL  ; // Buffer per il valore di un campo
  int                 len_field   = 0     ; // Dimensione in byte (8bit) del campo
  char                buf_frame[SYS_KERNEL_PORT_MAX_BUFFER_LENGTH]; // Buffer frame
  int                 len_frame   = 0     ; // Dimensione del buffer del frame
  char              * value       = NULL  ; // Ptr al valore del campo
  char              * internalval = NULL  ; // Ptr ad valore di un dato interno
  bool                ASCII       = false ; // Flag di campo da rappresentare in formato ASCII
  char              * buf_ASCII   = NULL  ; // Ptr ad un buffer ASCII
  XML_ATTRIBUTE     * attrib      = NULL  ; // Ptra ad una struttura attributo XML
  char              * int_ASCII   = NULL  ; // Ptr ad un valore di un dato interno in formato ASCII

  if ( SPVValidBinBuffer( bbuffer ) ) // Controllo validit� ptr buffer binario
  {
    if ( frame != NULL ) // Controllo validit� ptr frame
    {
      if ( SPVValidDevice( device ) ) // Controllo validit� ptr periferica
      {
        for ( int f = 0; f < frame->FieldCount; f++ )
        {
          field = &frame->FieldList[f];
          if ( field != NULL ) // Verifico la validit� del ptr a struttura field
		  {
			ASCII = field->ASCII; // Recupero il flag per vedere se il dato devo rappresentarlo in formato ASCII
            value = XMLGetValue( field->Def ); // Recupero il valore (se � fissato o relativo)
            if ( value != NULL && strcmpi( value, "NULL" ) != 0 ) // Verifico la validit� del ptr al valore
            {
              if ( value[0] == '_' && value[1] == '_' ) // Se il valore � relativo ad un dato interno al programma
			  {
                internalval = SPVGetInternalValue( device, thread, value ); // Recupero il valore interno (dalla struttura device o thread)
                buf_field = internalval; // Riporto il ptr sul buffer
                // --- Salvo il valore interno nella struttura XML cosi' lo posso leggere in un secondo momento (es. calcolo CRC) ---
                int_ASCII = XMLType2ASCII( internalval, field->Type ); // Salvo il valore in formato ASCII -MALLOC
                attrib = XMLGetAttrib( field->Def, "int_value" );
                if ( attrib != NULL ) // Se c'� gi� un attributo 'value'
                {
                  ret_code = XMLSetValue( attrib, int_ASCII );
                }
                else
                {
                  attrib = XMLNewAttrib( field->Def, "int_value", int_ASCII );
                  ret_code = XMLAddAttrib( field->Def, attrib );
                }
                if ( int_ASCII != NULL )
                {
                  try
                  {
                    free( int_ASCII ); // -FREE
                  }
                  catch(...)
                  {
                  }
                }
              }
              else // Se il valore � recuparabile dalla struttura XML
              {
				/* TODO 3 -oEnrico -cConversion : Nel caso di array di valori non funziona perch� non viene elaborato su tutta la lunghezza del buffer binario */
				buf_field = (char *) XMLASCII2Type( value, field->Type ); // -MALLOC
              }
              if ( field->Section != SPV_COMPROT_FIELD_SECTION_HIDDEN )
              {
                if ( buf_field != NULL ) // Se ho ottenuto un buffer (per il campo) valido
                {
                  if ( ASCII == true ) // Formato ASCII
                  {
					int len_ASCII = XMLGetValueInt( field->Def, "len" );
                    int len_data  = XMLCharSizeOf( field->Type );
                    int len_byte  = XMLByteSizeOf( field->Type );
                    char * buf_data = NULL;
                    // --- Calcolo dinamico della lunghezza di un campo ASCII ---
                    if ( len_ASCII == 0 ) // Se la lunghezza � nulla (vale solo per il caso ASCII!!!)
                    {
                      len_ASCII = strlen( buf_field ); // Allora la posso calcolare dinamicamente dalla stringa ASCII
                      // ATTENZIONE: Assicurarsi che il campo <buf_field> sia NULL-terminato!!!
                    }
                    len_field = len_data * len_ASCII; // Recupero la lunghezza ASCII
                    buf_ASCII = (char*)malloc( sizeof(char) * ( len_field + 1 ) ); // -MALLOC
                    // --- Ciclo di conversione field ASCII ---
                    for ( int d = 0; d < len_ASCII; d++ )
                    {
                      /* DONE 3 -oEnrico -cConversion : Nel caso di array di valori non funziona perch� non viene elaborato su tutta la lunghezza della stringa */
                      buf_data = XMLType2ASCII( &buf_field[len_byte*d], field->Type ); // -MALLOC
                      memcpy( &buf_ASCII[len_data*d], buf_data, len_data );
                      free( buf_data ); // -FREE
                    }
                    buf_ASCII[ len_field ] = 0; // NULL-terminator
                    free( buf_field ); // -FREE
                    buf_field = buf_ASCII;
                  }
                  else // Formato binario
                  {
					len_field = XMLByteSizeOf( field->Type ) * XMLGetValueInt( field->Def, "len" ); // Recupero la lunghezza binaria
                  }
                  if ( len_field > 0 ) // Se la lunghezza del buffer del field � valida
                  {
                    for ( int c = 0; c < len_field; c++ ) // Copio il campo nel frame
                    {
                      buf_frame[offset+c] = buf_field[c]; // Copio il c-esimo carattere del campo
                      len_frame++; // Incremento la dimensione del frame
                    }
                  }
                  else
				  {
                    ret_code = SPV_COMPROT_INVALID_BUFFER_LENGTH;
                    f = frame->FieldCount;
                  }
                  free( buf_field ); // Elimino la memoria occupata dal buffer del campo -FREE
                }
                else
                {
                  ret_code = SPV_COMPROT_INVALID_FIELD_BUFFER;
                  f = frame->FieldCount;
                }
              }
              else
              {
                // I campi di tipo HIDDEN non vengono considerati per la generazione del frame
                len_field = 0;
              }
            }
            else // Valore non valido
            {
              ret_code = SPV_COMPROT_INVALID_VALUE;
              f = frame->FieldCount;
            }
		  }
          else
          {
            ret_code = SPV_COMPROT_INVALID_FIELD;
            f = frame->FieldCount;
          }
          offset += len_field; // Aggiungo la dim del campo all'offset su buffer frame
        }
        if ( len_frame > 0 && len_frame <= SYS_KERNEL_PORT_MAX_BUFFER_LENGTH ) // Se il frame ha una dimensione corretta
        {
          bbuffer->len    = len_frame;
          // --- Copio il buffer del frame ---
          memcpy( bbuffer->buffer, buf_frame, sizeof(unsigned char) * len_frame );
        }
		else
        {
          ret_code = SPV_COMPROT_INVALID_FRAME_LENGTH;
        }
      }
      else
      {
        ret_code = SPV_COMPROT_INVALID_DEVICE;
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_FRAME;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_BUFFER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per inviare un frame di protocollo
/// Dato il buffer binario che contiene il frame da inviare, invia i byte sulla
/// porta specificata.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.03.2006]
/// \author Enrico Alborali
/// \version 0.10
//------------------------------------------------------------------------------
int SPVSendFrame( SPV_COMPROT_BIN_BUFFER * bframe, SYS_PORT * port, int timeout )
{
  int                     ret_code    = SPV_COMPROT_NO_ERROR; // Codice di ritorno
  SPV_EVENTLOG_MANAGER  * event_log   = NULL  ;
  long                    time        = 0     ; // Unix Time
  SYS_BUFFER            * bbuffer     = NULL  ;

  if ( SPVValidBinBuffer( bframe ) ) // Controllo la validit� del buffer del frame
  {
    if ( bframe->len > 0 ) // Se la lunghezza � valida
    {
      if ( port != NULL ) // Controllo la valididt� della struttura porta di com.
      {
		time = ULGetUnixTime();
				// --- Creo una struttura buffer binario ---
				bbuffer = SYSNewBuffer( bframe->buffer, bframe->len, time );
				// --- Aggiungo il nuovo buffer alla FIFO in uscita ---
				ret_code = SYSWriteFIFOItem( port->OutBuffer, (void*)bbuffer );
				/*
				if ( SPVComProtPrintLog != NULL )
				{
					SPVComProtPrintLog( NULL, 2, 777, (char*)bbuffer->Buffer, "CODE" );
				}
				//*/
				/*
				if ( ret_code == SPV_COMPROT_NO_ERROR )
				{
					SPVPrintEventLog( SPVComProtEventLog->ID, (char*)bbuffer->Buffer, bbuffer->BufferLen, 1 );
				}
				//*/
      }
      else
      {
        ret_code = SPV_COMPROT_INVALID_PORT;
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_BUFFER_LENGTH;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_BUFFER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per controllare il timeout in un buffer
/// Dato il ptr <fifo> al buffer FIFO e l'istante <time> di riferimento, viene
/// scansionata la FIFO finch� vengono trovati elementi in timeout.
/// Gli elementi che vengono trovati in timeout vengono eliminati dalla FIFO.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [18.02.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SPVCheckTimeout( SYS_FIFO * fifo, long time )
{
	int           	ret_code  	= 	0; // Codice di ritorno
	int				fun_code	=	0;	//
	bool			timeout		=	true; // Flag di elemento non in timeout
	SYS_BUFFER *	buffer		=	NULL; //

	if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
	{
		while( timeout ) // Ciclo finch� trovo elementi in timeout
		{
			////SPVComProtPrintLog( NULL, 1, ret_code, "DEBUG: Inizio ciclo while", "SPVCheckTimeout" );
			if ( fifo->Count > 0 ) // La la FIFO non � vuota
			{
				buffer = (SYS_BUFFER*) fifo->Item[fifo->OutPos];
				if ( SYSValidBuffer( buffer ) ) // Se l'elemento <buffer> � valido
				{
					try
					{
						if ( ((time - buffer->Time)*1000) > fifo->TimeOut ) // L'elemento � in timeout
						{
							///SPVComProtPrintLog( NULL, 1, ret_code, "DEBUG: prima del read", "SPVCheckTimeout" );
							buffer = (SYS_BUFFER*) SYSReadFIFOItem( fifo );
							///SPVComProtPrintLog( NULL, 1, ret_code, "DEBUG: prima del delete", "SPVCheckTimeout" );
							SYSDeleteBuffer( buffer );
							///SPVComProtPrintLog( NULL, 1, ret_code, "DEBUG: dopo il delete", "SPVCheckTimeout" );
						}
						else
						{
							timeout = false;
						}
					}
					catch(...)
					{
						timeout = false;
						ret_code = SPV_COMPROT_CRITICAL_ERROR;
						/* DONE 1 -oEnrico -cError : Gestire l'errore critico */
						SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione in lettura della FIFO", "SPVCheckTimeout" );
						try
						{
							fun_code = SYSClearBufferFIFO( fifo );
						}
						catch(...)
						{
							SPVComProtPrintLog( NULL, 2, fun_code, "Eccezione durante lo svuotamento della FIFO (1)", "SPVCheckTimeout" );
						}
					}
				}
				else
				{
					timeout = false;
					ret_code = SPV_COMPROT_INVALID_BUFFER;
					SPVComProtPrintLog( NULL, 2, ret_code, "Buffer non valido", "SPVCheckTimeout" );
					try
					{
						fun_code = SYSClearBufferFIFO( fifo );
					}
					catch(...)
					{
						SPVComProtPrintLog( NULL, 2, fun_code, "Eccezione durante lo svuotamento della FIFO (2)", "SPVCheckTimeout" );
					}
				}
			}
			else
			{
				timeout = false;
			}
			////SPVComProtPrintLog( NULL, 1, ret_code, "DEBUG: fine ciclo while", "SPVCheckTimeout" );
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_FIFO;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiornare il buffer in ingresso di una porta.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [24.03.2006]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SPVUpdateInBuffer( SYS_PORT * port, bool timeout )
{
  int                     ret_code    = 0   ; // Codice di ritorno
  SYS_BUFFER            * bbuffer     = NULL; // Buffer binario di ricezione
  int                     bytes_rcvd  = 0   ; // Numero effettivo di byte ricevuti
  unsigned char           tbuffer[SYS_KERNEL_PORT_MAX_BUFFER_LENGTH]; // Buffer di ricezione
  long                    time        = 0   ; // Unix Time
  SPV_EVENTLOG_MANAGER  * event_log   = NULL;

  if ( port != NULL ) // Controllo la validit� del ptr a struttura porta
  {
    ret_code = SYSPortGetStatus( port ); // Leggo lo stato della porta
    if ( ret_code == 0 ) // Stato della porta recuperato
    {
			if ( port->Active ) // Se la porta � attiva
      {
        bbuffer = SYSNewBuffer( NULL, 0, 0 ); // -MALLOC
        if ( SYSValidBuffer( bbuffer ) ) // Se l'allocazione � andata a buon fine
        {
		  time = ULGetUnixTime();
          if ( SYSReadPort( port, (char*)&tbuffer[0], SYS_KERNEL_PORT_MAX_BUFFER_LENGTH, &bytes_rcvd ) == 0 ) // Se la lettura � andata a buon fine
          {
            if ( bytes_rcvd > 0 ) // Se ho ricevuto un buffer
            {
              bbuffer->BufferLen  = bytes_rcvd;
              bbuffer->Time       = time;
              memcpy( bbuffer->Buffer, &tbuffer[0], bytes_rcvd ); // Copio il buffer ricevuto
              SYSWriteFIFOItem( port->InBuffer, (void*)bbuffer ); // Aggiungo il buffer binario alla FIFO di input
              if ( port->EventLog != NULL )
              {
                event_log = (SPV_EVENTLOG_MANAGER *) port->EventLog;
								//SPVPrintEventLog( event_log->ID, (char*)bbuffer->Buffer, bbuffer->BufferLen, 1 );
              }
            }
            else
            {
              SYSDeleteBuffer( bbuffer ); // -FREE
			}
          }
          else
          {
            SYSDeleteBuffer( bbuffer ); // -FREE
          }
          if ( timeout ) SPVCheckTimeout( port->InBuffer, time );
        }
        else
        {
          ret_code = SPV_COMPROT_INVALID_ALLOCATION;
        }
      }
			else // Se la porta non � attiva
      {
        ret_code = SPV_COMPROT_PORT_NOT_ACTIVE;
      }
      SYSUpdatePortMonitor( port );
    } 
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggregare tutti i buffer potenzialmente frammentati di un frame
///
/// \date [20.08.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDefragFrames( SYS_FIFO * fifo )
{
	int				ret_code		= 0     ; // Codice di ritorno
	SYS_BUFFER *	buffer			= NULL  ; //
	int				i				= 0		;
	SYS_BUFFER *	defbuf			= NULL	;
	unsigned char 	defbuf_buffer[SYS_KERNEL_PORT_MAX_BUFFER_LENGTH];
	int				defbuf_len		= 0		;
	long			defbuf_time		= 0		;
	int				defbuf_tag		= 0		;
	bool			fragbuf			= false	;


	if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
	{
		if ( fifo->Count > 1 ) // La la FIFO contiene buffer potenzialmente frammentati
		{
			// --- Ciclo di controllo e conteggio ---
			for ( i = 0; i < 128; i++ )
			{
				buffer = (SYS_BUFFER*) fifo->Item[i]; // prendo il buffer i-esimo

				if ( SYSValidBuffer( buffer ) ) // Se l'elemento <buffer> � valido
				{
					try
					{
						if ( defbuf_tag == 0 ) {
							defbuf_tag = buffer->Tag;
							defbuf_time = buffer->Time;
							fragbuf = true;
						}
						else
						{
							if ( defbuf_tag == buffer->Tag ) {
								fragbuf = true;
							}
							else {
								fragbuf = false;
							}
						}
						if (fragbuf) {
							if ((defbuf_len+buffer->BufferLen)>SYS_KERNEL_PORT_MAX_BUFFER_LENGTH) {
    							defbuf = buffer;
    							memcpy( &defbuf_buffer[defbuf_len], buffer->Buffer, buffer->BufferLen );
								defbuf_len += buffer->BufferLen;
    							if (defbuf_time < buffer->Time) {
    								defbuf_time = buffer->Time;
								}
								// --- Reset del buffer deframmentato ---
								buffer->Tag = 0;
								buffer->Time = 0;
								buffer->BufferLen = 1;
								buffer->Buffer[0] = 0;
							}
						}
					}
					catch(...)
					{
						ret_code = SPV_COMPROT_CRITICAL_ERROR;
						/* DONE 1 -oEnrico -cError : Gestire l'errore critico */
						SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione nel ciclo", "SPVDefragFrames" );
					}
				}
				else
				{
					ret_code = SPV_COMPROT_INVALID_BUFFER;
				}
			}

			// --- Modifica ultimo buffer ---
			if ( defbuf != NULL ){
				defbuf->BufferLen = defbuf_len;
				memcpy(defbuf->Buffer,defbuf_buffer,defbuf_len);
				defbuf->Time = defbuf_time;
				defbuf->Tag = defbuf_tag;
			}
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_FIFO;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ricevere un frame di protocollo
/// Dato il buffer binario che dovr� contenere il frame da ricevere, legge i
/// byte (se disponibili) dalla porta specificata.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [24.03.2006]
/// \author Enrico Alborali
/// \version 0.16
//------------------------------------------------------------------------------
int SPVReceiveFrame( SPV_DEVICE * device, SPV_COMPROT_BIN_BUFFER * bframe, SPV_COMPROT_BIN_BUFFER * bpayload, SYS_FIFO * fifo, SYS_FIFO_COORDS * coords )
{
  int           ret_code    = 0   ; // Codice di ritorno
  SYS_BUFFER  * buffer      = NULL; // Buffer binario ricevuto
  SYS_BUFFER  * new_buffer  = NULL; // Nuovo buffer binario (in caso di estrazione parziale)
  unsigned char * sb_buffer   = NULL;
  int             sb_len      = 0   ;
  long            sb_time     = 0   ;

  if ( SPVValidBinBuffer( bframe ) ) // Controllo la validit� del buffer per il frame
  {
		if ( fifo != NULL ) // Controllo la validit� del ptr alla FIFO di ricezione
	{
	  if ( coords != NULL ) // Controllo la validit� del ptr a coordinate della FIFO
	  {
		buffer = (SYS_BUFFER*) SYSGetFIFOItem( fifo, coords->ID );
		if ( ( coords->offset + coords->length ) <= buffer->BufferLen ) // Controllo che le coordiante siano compatibili con buffer
		{
		  // --- Caso ESTRAZIONE BUFFER INTERO ---
		  if ( coords->offset == 0 && coords->length == buffer->BufferLen )
		  {
			buffer = (SYS_BUFFER*) SYSExtractFIFOItem( fifo, coords->ID ); // Estrazione dalla FIFO
			if ( SYSValidBuffer( buffer ) )
			{
			  if ( buffer->Tag == (int)(void*)device )
			  {
				bframe->len     = buffer->BufferLen;
				memcpy( bframe->buffer, buffer->Buffer, buffer->BufferLen );
			  }
			  else
			  {
				ret_code = SPV_COMPROT_RECEIVED_BUFFER_WRONG;
			  }
							SYSDeleteBuffer( buffer );
			}
			else
			{
			  ret_code = SPV_COMPROT_FIFO_ERROR;
			}
		  }
		  // --- Caso ESTRAZIONE BUFFER PARZIALE ---
		  else
		  {
			bframe->len = coords->length; // Assegno la lunghezza specificata nelle coordinate
			memcpy(bframe->buffer,&buffer->Buffer[coords->offset],coords->length); // Copio solo la parte che mmi interessa
			// --- Ricaolcolo i parametri per la nuova struttura ---
			sb_len    = buffer->BufferLen - coords->length;
			sb_buffer = (char*) malloc( sizeof(char) * sb_len );
			sb_time   = buffer->Time;
			// --- Ricostrisco il buffer ---
			if ( coords->offset > 0 ) // Copio eventuali byte prima del buffer estratto
			{
			  memcpy( sb_buffer, buffer->Buffer, coords->offset );
			}
			if ( ( buffer->BufferLen - coords->length - coords->offset ) > 0 ) // Copio eventuali byte dopo il buffer estratto
			{
			  memcpy( &sb_buffer[coords->offset], &buffer->Buffer[coords->offset + coords->length], buffer->BufferLen - coords->length - coords->offset );
			}
			// --- Alloco la nuova struttura ---
			new_buffer = SYSNewBuffer( sb_buffer, sb_len, sb_time );
			// --- Libero sb_buffer
			try
			{
				free(sb_buffer);
			}
			catch(...)
			{
				SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante la cancellazione del Buffer", "SPVReceiveFrame" );
			}
			// --- Sostituisco il vecchio buffer con il nuovo ---
			ret_code = SYSEditFIFOItem( fifo, coords->ID, (void*)new_buffer ); // Sostituisco l'elemento della FIFO
			if ( ret_code == 0 )
			{
			  SYSDeleteBuffer( buffer );
			}
			else
			{
			  SYSDeleteBuffer( new_buffer );
			}
		  }
		  if ( ret_code == 0 && SPVValidBinBuffer( bpayload ) ) // Se ho ricevuto correttamente il buffer e voglio estrarre il payload
		  {
			if ( coords->PayloadLength > 0 && coords->PayloadOffset + coords->PayloadLength <= bframe->len ) // Se il buffer � compatibile con il payload richiesto
			{
			  bpayload->len     = coords->PayloadLength;
			  memcpy( bpayload->buffer, &bframe->buffer[coords->PayloadOffset], coords->PayloadLength );
			}
			else
			{
			  bpayload->len     = 0   ;
			  bpayload->buffer[0]  = 0;
			}
		  }
		}
		else
		{
		  ret_code = SPV_COMPROT_INVALID_BUFFER;
		}
	  }
	  else
	  {
		ret_code = SPV_COMPROT_INVALID_FIFO_COORDS;
	  }
	}
	else
	{
	  ret_code = SPV_COMPROT_INVALID_FIFO;
	}
  }
  else
  {
	ret_code = SPV_COMPROT_INVALID_BUFFER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per analizzare un frame di protocollo.
/// Partendo dal primo byte del buffer binario <bbuffer> fornito tenta di
/// riconoscere il frame definito da <frame> secondo i parametri della
/// periferica definita da <device>.
/// Il valore puntato da <frame_length> viene riempito con l'effettiva lunghezza
/// del frame riconosciuto.
/// Se il frame viene riconosciuto viene restituito un valore nullo altrimenti
/// viene restituito un valore non nullo.
///
/// \date [02.08.2007]
/// \author Enrico Alborali
/// \version 0.18
//------------------------------------------------------------------------------
int SPVCheckFrame( SPV_COMPROT_BIN_BUFFER * bbuffer, int * frame_length, SPV_COMPROT_FRAME * frame, SPV_DEVICE * device, int * payload_offset, int * payload_length )
{
    int                 ret_code              = 0     ; // Codice di ritorno
    bool                frame_header_ok       = false ; // Flag di frame Header corretto
    bool                frame_trailer_ok      = false ; // Flag di frame Trailer corretto
    int                 frame_payload_offset  = -1    ; // Offset del payload nel buffer ricevuto
    int                 frame_payload_len     = 0     ; // Lunghezza del payload nel buffer ricevuto
    int                 frame_len             = 0     ; // Lunghezza totale del frame
	int                 field_ASCII_len       = 0     ; // Lunghezza in caratteri di un campo ASCII
    int                 field_ASCII_offset    = 0     ; // Offset nel buffer del campo in fomrmato ASCII
    int                 field_offset          = 0     ; // Offset nel buffer del campo
    char              * field_value           = NULL  ; // Ptr al valore del campo
    char              * field_buf             = NULL  ; // Buffer binario del campo
    char              * field_ASCII_buf       = NULL  ; // Ptr al buffer del campo in formato ASCII
    char              * field_ASCII_temp_buf  = NULL  ; // Ptr al buffer temporaneo per una parte dei campo in formato ASCII
    int                 field_buf_len         = 0     ; // Lunghezza in byte del buffer del campo
    int                 field_len             = 0     ; // Lunghezza dei dati del campo
    bool                field_ASCII           = false ; // Flag di campo in formato ASCII
    XML_ATTRIBUTE     * attrib                = NULL  ; // Ptr ad attributo XML
	SPV_COMPROT_FIELD * field                 = NULL  ; // Ptr a struttura campo di frame
    int                 offset                = 0     ; // Offset del buffer binario del frame
    char                char_a                = '\0'  ; // Carattere di appoggio
	char                char_b                = '\0'  ; // Carattere di appoggio

	if ( SPVValidBinBuffer( bbuffer ) ) // Controllo validit� ptr buffer binario
	{
		if ( frame != NULL ) // Controllo validit� ptr frame
		{
			if ( SPVValidDevice( device ) ) // Controllo validit� ptr periferica
			{
				frame_header_ok   = true;
				frame_trailer_ok  = true;
				frame_len         = 0   ;

				for ( int f = 0; f < frame->FieldCount; f++ )
				{
					field = &frame->FieldList[f];
					if ( field != NULL ) // Verifico la validit� del ptr a struttura field
					{
						field_ASCII   = field->ASCII; // Recupero il flag per vedere se il dato devo rappresentarlo in formato ASCII
						field_buf_len = SPVGetFieldLen( field ); // Recupero la lunghezza del buffer del campo
						field_len     = XMLGetValueInt( field->Def, "len" ); // Recupero la lunghezza dei dati del campo
						if ( field->Section == SPV_COMPROT_FIELD_SECTION_HEADER
							|| field->Section == SPV_COMPROT_FIELD_SECTION_TRAILER ) // Se il campo fa parte della sezione Header o Trailer
						{
							//--- Ricavo il buffer del campo secondo la definzione dello stesso ---
							field_value = XMLGetValue( field->Def ); // Recupero il valore (se � fissato o relativo) - Senza riallocarlo!
							if ( field_value != NULL ) // Se il valore del campo � definito (devo verificare che sia uguale a quello ricevuto)
							{
								if ( field_ASCII ) // Se il campo � in formato ASCII
								{
									field_buf = (char*)malloc( sizeof(char) * field_buf_len );
									memcpy( field_buf, field_value, field_buf_len ); // Copio il valore del campo nel buffer del campo
								}
								else // Se il campo e' in formato binario
								{
									field_buf = (char *) XMLASCII2Type( field_value, field->Type ); // Converto il valore in formato binario
								}
								if ( field_buf == NULL ) // Non ho ottenuto un buffer per il campo valido
								{
									ret_code = SPV_COMPROT_INVALID_BUFFER;
									f = frame->FieldCount; // Interrompo il ciclo sui campi del frame
								}
							}
							else // Il valore non � specificato, leggo quello che ho ricevuto e lo uso come valore temporaneo
							{
								field_value = (char*) &bbuffer->buffer[offset];
								field_buf   = (char*) malloc(sizeof(char)*(field_buf_len));
								memcpy( field_buf, field_value, field_buf_len ); // Copio il valore del campo nel buffer del campo
								if ( field_ASCII == true ) // Se il buffer del campo non � in formato ASCII
                                {
									field_ASCII_buf = (char*)malloc( sizeof(char) * ( field_buf_len + 1 ) ); // Alloco un nuovo buffer (con un byte in pi� per il NT) -MALLOC
									memcpy(field_ASCII_buf,field_buf,field_buf_len);
									field_ASCII_buf[field_buf_len] = 0; // Aggiungo il NT
									free (field_buf); // Elimino il vecchio buffer del campo
									field_buf = field_ASCII_buf;
								}
								else
								{
									// NOTA: nella struttura dati XML i dati vengono salvati sempre in formato ASCII !!!
								    field_ASCII_len = XMLCharSizeOf(field->Type);
								    field_ASCII_buf = (char*) malloc( sizeof(char) * ( field_ASCII_len * field_len + 1 ) ); // -MALLOC
								    field_ASCII_offset = 0; // Resetto l'offset nel campo in formato ASCII
								    field_offset = 0; // Resetto l'offset nel campo in formato binario
									for ( int l = 0; l < field_len; l++ ) // Ciclo sul numero di dati del campo
									{
										field_ASCII_temp_buf = XMLType2ASCII( &field_buf[field_offset], field->Type ); // Converto l'l-esima parte del buffer del campo in formato ASCII
										memcpy( &field_ASCII_buf[field_ASCII_offset], field_ASCII_temp_buf, field_ASCII_len + 1 ); // Copio la parte nel buffer in formato ASCII
										field_ASCII_offset += field_ASCII_len; // Aggiorno l'offset del buffer ASCII
									}
									free( field_buf );
									field_buf = field_ASCII_buf;
								}
								// --- Creo un attributo 'value' temporaneo ---
								attrib = XMLGetAttrib( field->Def, "value" );
								if ( attrib != NULL ) // Se c'� gi� un attributo 'value'
								{
									ret_code = XMLSetValue( attrib, field_buf );
								}
								else
								{
									attrib = XMLNewAttrib( field->Def, "value", field_buf );
									ret_code = XMLAddAttrib( field->Def, attrib );
								}
								/*
                                if ( field_buf != NULL )
                                {
                                  try
                                  {
                                	free( field_buf ); // -FREE
                                  }
                                  catch(...)
                                  {
                                  }
                                }
                                //*/
                                // --- Metto l'attributo 'temp'="true" ---
                                attrib = XMLGetAttrib( field->Def, "temp" );
                                if ( attrib != NULL )
                                {
									ret_code = XMLSetValue( attrib, "true" );
								}
								else
								{
									attrib = XMLNewAttrib( field->Def, "temp", "true" );
									ret_code = XMLAddAttrib( field->Def, attrib );
								}
							}
							if ( ret_code == 0 ) // Se ho recuperato correttamente il buffer del campo
							{
								if ( field->Section == SPV_COMPROT_FIELD_SECTION_HEADER )
								{
									for ( int b = 0; b < field_buf_len; b++ )
								    {
								    	try
								    	{
								    	  char_a = field_buf[b];
								    	  char_b = bbuffer->buffer[offset+b];
										  if ( char_a != char_b ) // Se il buffer non � corretto!
								    	  {
								    		frame_header_ok = false;
								    		b = field_buf_len; // Interrompo il ciclo sui byte del campo
								    	  }
								    	}
								    	catch(...)
								    	{
								    		frame_header_ok = false;
											b = field_buf_len;
											/* DONE -oEnrico -cError : Gestione dell'errore critico */
											SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante comparazione buffer Header", "SPVCheckFrame" );
										}
									}
									frame_len += field_buf_len; // Aggiorno la lunghezza del frame
								}
								if ( field->Section == SPV_COMPROT_FIELD_SECTION_TRAILER )
								{
									for ( int b = 0; b < field_buf_len; b++ )
									{
										try
										{
                                            char_a = field_buf[b];
                                            char_b = bbuffer->buffer[offset+b];
											if ( char_a != char_b ) // Se il buffer non � corretto!
                                            {
                                            	frame_trailer_ok = false;
												b = field_buf_len; // Interrompo il ciclo sui byte del campo
											}
										}
										catch(...)
										{
											frame_trailer_ok = false;
											b = field_buf_len;
											/* DONE -oEnrico -cError : Gestione dell'errore critico */
											SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante comparazione buffer Trailer", "SPVCheckFrame" );
										}
									}
									frame_len += field_buf_len; // Aggiorno la lunghezza del frame
								}
								// --- Elimino il buffer temporaneo per il field ---
                                if ( field_buf != NULL )
                                {
									try
									{
										free( field_buf );
									}
									catch(...)
									{
										/* DONE 1 -oEnrico -cError : Gestire l'errore critico */
										SPVComProtPrintLog( NULL, 2, ret_code, "Eccezione durante cancellazione buffer", "SPVCheckFrame" );
									}
								    field_buf = NULL;
								}
							}
							else
							{
								if ( field->Section == SPV_COMPROT_FIELD_SECTION_HEADER )   frame_header_ok   = false;
								if ( field->Section == SPV_COMPROT_FIELD_SECTION_TRAILER )  frame_trailer_ok  = false;
								f = frame->FieldCount; // Interrompo il ciclo sui campi del frame
							}
						}
						if ( field->Section == SPV_COMPROT_FIELD_SECTION_PAYLOAD ) // Se il campo fa parte della sezione Payload
						{
							// Se il frame � gi� stato riconoscuto allora recupero il payload
							if ( frame_header_ok )
							{
                                // --- Salvo il valore ricevuto in un nuovo attributo 'value' ---
                                field_value = (char*)&bbuffer->buffer[offset];
                                field_buf   = (char*) malloc( sizeof(char)*(field_buf_len) );
                                memcpy( field_buf, field_value, field_buf_len ); // Copio il valore del campo nel buffer del campo
                                if (field_buf != NULL) // Se ho ottenuto un buffer (per il campo) valido
								{
									/* VISUAL DEBUG
									SPVPrintEventLog( SPVComProtEventLog->ID, field_buf, field_buf_len, 1 );
									//*/
								}
								if ( field_ASCII == true ) // Se il buffer del campo � in formato ASCII
								{
									field_ASCII_buf = (char*)malloc( sizeof(char)* ( field_buf_len + 1 ) ); // Alloco un nuovo buffer (con un byte in pi� per il NT) -MALLOC
									memcpy( field_ASCII_buf, field_buf, field_buf_len );
									field_ASCII_buf[field_buf_len] = 0; // Aggiungo il NT
									free ( field_buf ); // Elimino il vecchio buffer del campo
									field_buf = field_ASCII_buf;
								}
								else
								{
									// NOTA: nella struttura dati XML i dati vengono salvati sempre in formato ASCII !!!
								    field_ASCII_len     = XMLCharSizeOf( field->Type );
								    field_ASCII_buf     = (char*) malloc( sizeof(char)*( ( field_ASCII_len * field_len ) + 1 ) ); // -MALLOC
								    field_ASCII_offset  = 0; // Resetto l'offset nel campo in formato ASCII
								    field_offset        = 0; // Resetto l'offset nel campo in formato binario
								    // --- Caso tipo stringa ---
								    if ( field->Type == t_string )
								    {
										memset( &field_ASCII_buf[0], 0, sizeof(char)*( ( field_ASCII_len * field_len ) + 1 ) );
										memcpy( &field_ASCII_buf[0], &field_buf[0], sizeof(char)*( ( field_ASCII_len * field_len ) ) );
									}
									// --- Altri casi ---
									else
									{
										for ( int l = 0; l < field_len; l++ ) // Ciclo sul numero di dati del campo
										{
											field_ASCII_temp_buf = XMLType2ASCII( &field_buf[field_offset], field->Type ); // Converto l'l-esima parte del buffer del campo in formato ASCII -MALLOC
											if ( XMLIsIntType( field->Type ) )
											{
												field_ASCII_len = strlen( field_ASCII_temp_buf );
											}
											try
											{
												if ( field_ASCII_temp_buf != NULL ) {
													memcpy( &field_ASCII_buf[field_ASCII_offset], field_ASCII_temp_buf, field_ASCII_len + 1 ); // Copio la parte nel buffer in formato ASCII
													free( field_ASCII_temp_buf ); // -FREE
												}
											}
											catch(...)
											{

											}
											field_ASCII_offset += field_ASCII_len; // Aggiorno l'offset del buffer ASCII
											//* SPERIMENTALE AIM 14.01.2010
											field_offset += XMLByteSizeOf(field->Type);
											//*/
										}
								    }
								    free( field_buf );
								    field_buf = field_ASCII_buf;
								}
								// --- Creo un attributo 'value' per il valore di payload ricevuto ---
								attrib = XMLGetAttrib( field->Def, "value" );
								if ( attrib != NULL ) // Se c'� gi� un attributo 'value'
								{
									ret_code = XMLSetValue( attrib, field_buf ); // Imposto il nuovo valore
								}
								else
								{
									attrib = XMLNewAttrib( field->Def, "value", field_buf ); // Creo un nuovo attributo
                                	ret_code = XMLAddAttrib( field->Def, attrib ); // Lo aggiungo all'elemento del campo
                                }
                                if ( field_buf != NULL )
								{
                                  try
                                  {
									free( field_buf ); // -FREE
                                  }
                                  catch(...)
                                  {
                                  }
                                }
								// --- Impostazione dell'offset ---
								if ( frame_payload_offset == -1 ) frame_payload_offset = offset; // Setto l'offset del payload

								field_buf_len = SPVGetFieldLen( field ); // Recupero la lunghezza del buffer del campo

								frame_payload_len += field_buf_len; // Aggiorno la lunghezza del payload
								frame_len         += field_buf_len; // Aggiorno la lunghezza del frame
			  }
			  else
			  {
								// Non pu� esserci un campo di sezione payload prima di un header (corretto)
				ret_code = SPV_COMPROT_INVALID_FIELD_ORDER;
                f = frame->FieldCount; // Interrompo il ciclo sui campi del frame
              }
            }
			if ( field->Section == SPV_COMPROT_FIELD_SECTION_UNKNOWN ) // Se il campo fa parte di una sezione sconoscita
            {
              // Significa che c'� un'errore nella definizione locale del frame
              ret_code = SPV_COMPROT_INVALID_DEFINITION;
              f = frame->FieldCount; // Interrompo il ciclo sui campi del frame
            }
		  }
          else
          {
            ret_code = SPV_COMPROT_INVALID_FIELD;
          }
		  offset += field_buf_len; // Aggiungo la dim del campo all'offset su buffer frame
        }
        if ( !frame_trailer_ok )
        {
          ret_code = SPV_COMPROT_FRAME_TRAILER_ERROR;
		}
        if ( !frame_header_ok )
        {
          ret_code = SPV_COMPROT_FRAME_HEADER_ERROR;
        }
        if ( ret_code == 0 ) // Se ho riconosciuto il frame
        {
          *frame_length = frame_len;
					if ( payload_offset != NULL )
          {
            *payload_offset = frame_payload_offset;
          }
          if ( payload_length != NULL )
          {
			*payload_length = frame_payload_len;
					}
        }
      }
      else
      {
        ret_code = SPV_COMPROT_INVALID_DEVICE;
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_FRAME;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_BUFFER;
  }
  /* DONE 3 -oEnrico -cComProt : Verificare i ritorni di questa funzione */
  return ret_code;
}

//==============================================================================
/// Funzione per cercare un frame all'interno di un buffer FIFO
///
/// In caso di errore restituisce un valore non nullo.
///
/// NOTA: Questa funzione effettua la ricerca del frame su una FIFO che potrebbe
/// essere condivisa il lettura e scrittura da pi� processi. Si rende necessario
/// accedervi attraverso l'uso della sezione critica e lo stesso risultato avr�
/// validit� soltanto fino al rilascio della sezione critica stessa in quanto le
/// coordinate riferite alla FIFO potrebbero variare.
///
/// \date [19.05.2006]
/// \author Enrico Alborali
/// \version 0.13
//------------------------------------------------------------------------------
int SPVSearchFrame( SYS_FIFO * fifo, SPV_COMPROT_FRAME * frame, SPV_DEVICE * device, SYS_FIFO_COORDS * coords )
{
	int                       ret_code        = SPV_COMPROT_NO_ERROR; // Codice di ritorno
	int                       check_code      = 0         			; // Codice di controllo
	SYS_BUFFER              * frame_buffer    = NULL                ; // Ptr a buffer di frame
	SYS_FIFO_COORDS           frame_coords    = {-1,-1,-1}          ; // Coordinate del frame all'interno della FIFO
	SPV_COMPROT_BIN_BUFFER  * bbuffer         = NULL                ; // Buffer binario per il frame
	int                       min_len         = 0                   ; // Lunghezza minima del frame
	int                       payload_offset  = 0                   ; // Offset del payload ricevuto
	int                       payload_length  = 0                   ; // Lunghezza del payload ricevuto

	if ( fifo != NULL ) // Controllo la validit� del ptr alla FIFO
	{
		if ( frame != NULL ) // Controllo la validit� del ptr al frame
		{
			if ( device != NULL ) // Controllo la validit� del ptr alla periferica
			{
				if ( coords != NULL ) // Controllo la validit� del ptr a coordinate FIFO
				{
					if ( fifo->Count > 0 )
					{
						for ( int b = 0; b < fifo->Count; b++ )
						{
							//* SPERIMENTALE FREJUS 25.03.2009
							ret_code = SPV_COMPROT_NO_ERROR;
							//*/
							frame_buffer = (SYS_BUFFER*) SYSGetFIFOItem( fifo, b );
							if ( SYSValidBuffer( frame_buffer ) ) // Verifico la validita'
							{
								/*
								if ( SPVComProtPrintLog != NULL )
								{
									SPVComProtPrintLog( NULL, 2, 888, (char*)frame_buffer->Buffer, "CODE" );
								}
								//*/
								min_len = SPVGetMinFrameLength( frame ); // Calcolo la lunghezza minima del frame da cercare
								if ( min_len > 0 ) // Se la lunghezza minima � valida
								{
									//SPVPrintEventLog( SPVComProtEventLog->ID, (char*)frame_buffer->Buffer, frame_buffer->BufferLen, 1 );
									if ( ( frame_buffer->BufferLen - min_len ) >= 0 )
									{
										bbuffer = SPVNewBinBuffer( );
										for ( int o = 0; o <= ( frame_buffer->BufferLen - min_len ); o++ )
										{
											frame_coords.offset = o;
											try
											{
												bbuffer->len      = frame_buffer->BufferLen-o ;
												memcpy( bbuffer->buffer, (unsigned char*)&frame_buffer->Buffer[o], sizeof(unsigned char)*(bbuffer->len) );
											}
											catch(...)
											{
												ret_code = SPV_COMPROT_RECEIVED_BUFFER_CORRUPTED;
												break; // Esco dal ciclo for
											}
											// --- Se il buffer � a posto ---
											if ( ret_code == 0 )
											{
												check_code = SPVCheckFrame( bbuffer, &frame_coords.length, frame, device, &payload_offset, &payload_length );
												/* SPERIMENTALE FREJUS 25.03.2009
												if ( check_code == 0 && frame_buffer->Tag != NULL ) // Ho trovato il frame
												{
													int kkk = 0;
												}
												if ( check_code != 0 ) {
													int jjj = 0;
												}
												//*/
												if ( check_code == 0 && frame_buffer->Tag == 0 ) // Ho trovato il frame
												{
													// --- Marchio il frame come utilizzato ---
													frame_buffer->Tag = (int)(void*)device;

													frame_coords.ID       = b                   ;
													coords->ID            = frame_coords.ID     ;
													coords->offset        = frame_coords.offset ;
													coords->length        = frame_coords.length ;
													coords->PayloadOffset = payload_offset      ;
													coords->PayloadLength = payload_length      ;
													b                     = fifo->Count         ; // Interrompo il ciclo sugli elementi della FIFO
													o                     = frame_buffer->BufferLen-min_len; // Interrompo il ciclo sull'offset
												}
											}
										}
										SPVDeleteBinBuffer( bbuffer );
									}
                                    else
									{
                                    	ret_code = SPV_COMPROT_INVALID_BUFFER_LENGTH;
                                    }
								}
								else
								{
								  ret_code = SPV_COMPROT_INVALID_DEFINITION;
								}
							}
							else
							{
								/////b = fifo->Count;
								/* SPERIMENTALE FREJUS 25.03.2009
								ret_code = SPV_COMPROT_FIFO_ERROR;
								//*/
							}
						}
					}
					else
					{
						ret_code = SPV_COMPROT_FIFO_EMPTY;
					}
					if ( ret_code == 0 && frame_coords.ID == -1 ) // Non ho trovato il frame
					{
						ret_code = SPV_COMPROT_FRAME_NOT_FOUND;
					}
				}
				else
				{
					ret_code = SPV_COMPROT_INVALID_FIFO_COORDS;
				}
			}
			else
			{
				ret_code = SPV_COMPROT_INVALID_DEVICE;
			}
		}
		else
		{
			ret_code = SPV_COMPROT_INVALID_FRAME;
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_FIFO;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eseguire uno step di thread protocollo
///
/// \date [08.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.32
//------------------------------------------------------------------------------
int SPVExecStep( SPV_COMPROT_THREAD * thread )
{
	int                       ret_code    = 0           ; // Codice di ritorno
	SPV_DEVICE              * device      = NULL        ; // Ptr a periferica
	SPV_COMPROT_COMMAND     * command     = NULL        ; // Ptr comando di protocollo
	SPV_COMPROT_STEP        * step        = NULL        ; // Ptr Step di thread protocollo
	SPV_COMPROT_STEP        * sndstep     = NULL        ; // Ptr Step di trasmissione di thread protocollo
	SPV_COMPROT_STEP        * rcvstep     = NULL        ; // Ptr Step di ricezione di thread protocollo
	SPV_COMPROT_FRAME       * frame       = NULL        ; // Ptr frame di protocollo
	SPV_COMPROT_FRAME       * sndframe    = NULL        ; // Ptr frame di trasmissione di protocollo
	SPV_COMPROT_FRAME       * rcvframe    = NULL        ; // Ptr frame di ricezione di protocollo
	SYS_PORT                * port        = NULL        ; // Ptr porta di comunicazione
	SPV_COMPROT_BIN_BUFFER  * bbuffer     = NULL        ; // Buffer binario per il frame
	SPV_COMPROT_BIN_BUFFER  * pbuffer     = NULL        ; // Buffer binario per il payload
	SYS_FIFO_COORDS           coords      = {0,0,0,0,0} ; // Coordinate della FIFO di ricezione
	SPV_COMPROT_PAYLOAD     * payload     = NULL        ; // Ptr a payload
	int                       first_step  = 0           ; // ID del primo step del livello
	SPV_COMPROT_CYCLE       * cycle       = NULL        ; // Ptr a ciclo
	long                      time        = 0           ; // Data Ora (Unix Time)
	long                      step_delay  = 0           ;
	unsigned __int32          port_baud     = 0           ; // Baud della porta
	int                       port_timeout  = 0           ; // Timeout della porta

	try
	{
		if ( SPVValidComProtThread( thread ) ) // Controllo che il ptr a struttura thread sia valido
		{
			if ( thread->Device != NULL ) // Controllo la validita della periferica
			{
				device = thread->Device;
				if ( thread->Type != NULL ) // Controllo la validit� del tipo protocollo
				{
					if ( thread->Port != NULL ) // Controllo la validit� della porta di comunicazione
					{
						port = thread->Port;
						/* TODO 1 -oEnrico -cPort : Aggiungere la gestione delle porte non persistenti */
						try
						{
							if ( !port->Active ) {
								SYSOpenPort( port );
							}
						}
						catch(...)
						{
							SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione durante riapertura porta", "SPVExecStep" );
						}

						if ( port->Active )
						{
							if ( thread->Command >= 0 && thread->Command < thread->Type->CommandCount ) // Controllo che non ci sia un overflow
							{
								command = &thread->Type->CommandList[thread->Command];
								if ( command != NULL )
								{
									if ( thread->Step >= 0 && thread->Step < command->StepCount ) // Controllo che non ci sia un overflow
									{
										step = &command->StepList[thread->Step];
										if ( step != NULL )
										{
											if ( step->FrameID >= 0 && step->FrameID < thread->Type->FrameCount )
											{
												frame   = &thread->Type->FrameList[step->FrameID];
												if ( frame != NULL )
												{
													thread->StepRetCode = SPV_COMPROT_STEP_START;
													thread->Counter++; // Incremento il contatore delle esecuzioni del thread (serve per le ripetizioni)
													thread->Retry = 0; // Azzero il contatore dei tentativi del thread
													thread->StepStart = ULGetUnixTime();
													first_step = thread->Step; // Salvo l'ID del primo step di questo 'livello'

													thread->StepFailure = false;
													thread->StepSuccess = false;
													// --- Ciclo con i diversi tentativi ---
													while ( thread->Retry < step->Retry )
													{
														// --- CASO FRAME TX ---
														if ( step->Type == SPV_COMPROT_STEP_TYPE_SEND )
														{
															for ( int s = 0; s < command->StepCount; s++ )
															{
																if ( command->StepList[s].PrevID == step->PrevID )
																{
																	// --- Recupero lo step s-esimo e il suo frame ---
																	sndstep = &command->StepList[s];
																	sndframe = &thread->Type->FrameList[sndstep->FrameID];
																	// --- Blocco Trash XML ---
																	XMLLockTrash("SPVExecStep 1");
																	try
																	{
																		thread->StepRetCode = SPV_COMPROT_STEP_FRAME_BUILD_START;
																		// --- Provo a costruire il frame ---
																		bbuffer = SPVNewBinBuffer( );
																		ret_code = SPVBuildFrame( bbuffer, sndframe, device, thread ); // Preparo il frame da inviare
																		// --- Cancello Trask XML ---
																		XMLEmptyTrash();
																	}
																	catch(...)
																	{
																		ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
																		SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione durante costruzione frame", "SPVExecStep" );
																	}
																	// --- Sblocco Trash XML ---
																	XMLUnlockTrash();
																	if ( ret_code == 0 )
																	{
																		thread->StepRetCode = SPV_COMPROT_STEP_FRAME_BUILD_SUCCESS;
																		// --- Entro nella sezione critica generale della porta ---
																		if ( !command->Blocking )
																		{
																			//EnterCriticalSection( &port->CriticalSection );
																			SYSLock(&port->GeneralLock,"SPVExecStep 1");
																		}
																		/*
																		// --- Entro nella sezione critica ---
																		EnterCriticalSection( &port->InCriticalSection );
																		*/
																		// --- Entro nella sezione critica del buffer in uscita ---
																		//EnterCriticalSection( &port->OutCriticalSection );
																		SYSLock(&port->OutLock,"SPVExecStep 1");
																		try
																		{
																			thread->StepRetCode = SPV_COMPROT_STEP_WRITE_FIFO_START;
																			ret_code = SPVSendFrame( bbuffer, port, step->Timeout ); // Invio il frame
																		}
																		catch(...)
																		{
																			ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
																			SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione durante invio frame", "SPVExecStep" );
																		}
																		// --- Esco dalla sezione critica del buffer in uscita ---
																		//LeaveCriticalSection( &port->OutCriticalSection );
																		SYSUnLock(&port->OutLock);

																		if ( ret_code == 0 )
																		{
																			thread->StepRetCode = SPV_COMPROT_STEP_WRITE_FIFO_SUCCESS;
																			thread->StepSuccess = true;
																			// --- Recupero baud e timeout della porta ---
																			port_baud = SYSGetPortBaud( port );
																			if ( port_baud == 0 ) port_baud = 9600;
																			port_timeout = SYSGetPortTimeout( port );
																			// --- Formula per il calcolo del delay standard ---
																			///step_delay = (( ( (SYS_KERNEL_PORT_MAX_BUFFER_LENGTH/4) + bbuffer->len ) * 11 * 1000 ) / port_baud );
																			///step_delay = 100 + port_timeout + (( ( bbuffer->len ) * 11 * 1000 ) / port_baud );
																			step_delay = 100 + (( ( bbuffer->len ) * 11 * 1000 ) / port_baud );
																			// --- Utilizzo il delay maggiore ---
																			step_delay = ( step_delay > step->Delay ) ? step_delay : step->Delay;
																			// --- Limito il delay ad un massimo di 3,5 sec. ---
																			step_delay = ( step_delay > 4500 ) ? 4500 : step_delay;
																			// --- Entro nella sezione critica ---
																			//EnterCriticalSection( &port->InCriticalSection );
																			SYSLock(&port->InLock,"SPVExecStep 1");
																			try
																			{
																				Sleep( step_delay );
																			}
																			catch(...)
																			{
																				SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione durante delay", "SPVExecStep" );
																			}
																			// --- Esco dalla sezione critica del buffer in uscita ---
																			//LeaveCriticalSection( &port->InCriticalSection );
																			SYSUnLock(&port->InLock);
																		}
																		else
																		{
																		  thread->StepRetCode = SPV_COMPROT_STEP_WRITE_FIFO_FAILURE;
																		  thread->StepFailure = true;
																		}
																		// --- Esco dalla sezione critica generale della porta ---
                            											if ( !command->Blocking )
                            											{
                            												//LeaveCriticalSection( &port->CriticalSection );
                            												SYSUnLock(&port->GeneralLock);
                            											}
                            											/*
                            											// --- Esco dalla sezione critica del buffer in uscita ---
                            											LeaveCriticalSection( &port->InCriticalSection );
                            											*/
                            										}
                            										else
                            										{
                            											thread->StepFailure = true;
                            											thread->StepRetCode = SPV_COMPROT_STEP_FRAME_BUILD_FAILURE;
                            											// --- Se c'� stato un errore ed � in corso un cyclo WHILE in caso di INVIO ---
                            											if ( thread->Cycle != NULL ) // Se c'� un ciclo attivo
                            											{
                            												// Se c'� un ciclo while attivo e l'ultimo frame non � stato possibile generarlo
                            												if ( thread->Cycle->Type == SPV_COMPROT_STEP_CYCLE_WHILE )
                            												{
                            													// --- Se rientro in uno degli errori che danno origine ad un fine ciclo WHILE ---
                            													if ( ret_code == SPV_COMPROT_INVALID_BUFFER_LENGTH
                            														|| ret_code == SPV_COMPROT_INVALID_FIELD_BUFFER
                            														|| ret_code == SPV_COMPROT_INVALID_VALUE )
                            													{
                            														thread->StepSuccess = true;
                            														ret_code = SPV_COMPROT_NO_ERROR;
                            													}
                            												}
                            											}
                            										}
                            										SPVDeleteBinBuffer( bbuffer );
                            										if ( thread->StepSuccess ) // Se lo step si � concluso con successo
                            										{
                            											thread->Retry = step->Retry; // Annullo i rimanenti tentativi
                            										}
                            										else
                            										{
                            											thread->Retry++;
                            											thread->StepFailure = true;
                            											time = ULGetUnixTime();
                            											if ( ( ( time * 1000 ) - ( thread->StepStart * 1000 ) ) > step->Timeout )
                            											{
                            												thread->Retry = step->Retry; // Sono in timeout perci� esco dal ciclo while
                            											}
                            										}
                            									}
                            								}
                            								// --- Gestione dei cicli e timeout per i frame da inviare ---
                            								if ( thread->StepSuccess ) // Se lo step si � concluso con successo
                            								{
                            									thread->StepRetCode = SPV_COMPROT_STEP_SUCCESS;
                            									thread->Retry = step->Retry;
                            									if ( thread->Cycle != NULL ) // Se c'� un ciclo attivo
                            									{
                            										// Se c'� un ciclo while attivo su� primo step di questo 'livello' ed � terminato
                            										if ( thread->Cycle->Type == SPV_COMPROT_STEP_CYCLE_WHILE
                            											&& thread->Cycle->Step == first_step
                            											&& thread->StepFailure )
                            										/// && first_step != thread->Step )
                            										{
                            											cycle = thread->Cycle; // Salvo il ptr al ciclo attuale
                            											SPVRemoveCycle( thread ); // Lo rimuovo dal thread
                            											SPVDeleteCycle( cycle ); // E poi lo elimino definitvamente
                            										}
                            									}
                            								}
                            								else
                            								{
                            									thread->Retry++;
                            									thread->StepFailure = true;
                            									thread->StepRetCode = SPV_COMPROT_STEP_FAILURE;
                            									time = ULGetUnixTime();
                            									if ( ( ( time * 1000 ) - ( thread->StepStart * 1000 ) ) > step->Timeout )
                            									{
                            										thread->Retry = step->Retry; // Sono in timeout perci� esco dal ciclo while
                            										thread->StepRetCode = SPV_COMPROT_STEP_TIMEOUT;
                            									}
                            								}
                            								// --- Fine gestione cicli e timeout ---
                            							}
                            							//--- CASO FRAME RX ---
                            							else
                            							{
                            								for ( int s = 0; s < command->StepCount; s++ )
                            								{
                            									if ( command->StepList[s].PrevID == step->PrevID )
                            									{
																	////SPVComProtPrintLog( command, 1, ret_code, "DEBUG:Prima del lock", "SPVExecStep" );
																	rcvstep = &command->StepList[s];
																	rcvframe = &thread->Type->FrameList[rcvstep->FrameID];
																	// --- Entro nella sezione critica generale della porta ---
																	if ( !command->Blocking )
																	{
																		//EnterCriticalSection( &port->CriticalSection );
																		SYSLock(&port->GeneralLock,"SPVExecStep 2");
																	}
																	// --- Entro nella sezione critica del buffer in ingresso ---
																	//EnterCriticalSection( &port->InCriticalSection );
																	SYSLock(&port->InLock,"SPVExecStep 2");
																	////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: dopo il lock", "SPVExecStep" );
																	try
																	{
																		// --- Gestisco un eventuale timeout della FIFO in ingresso ---
																		time = ULGetUnixTime();
																		////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima del check timeout", "SPVExecStep" );
																		SPVCheckTimeout( port->InBuffer, time );
																		////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima del defrag", "SPVExecStep" );
																		SPVDefragFrames( port->InBuffer );
																		////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima lock xml", "SPVExecStep" );
																		XMLLockTrash("SPVExecStep 2");
																		////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: dopo lock xml", "SPVExecStep" );
																		try
																		{
																			// --- Cerco il frame nel buffer FIFO in ingresso ---
																			thread->StepRetCode = SPV_COMPROT_STEP_FRAME_SEARCH_START;
																			ret_code = SPVSearchFrame( port->InBuffer, rcvframe, device, &coords );
																			XMLEmptyTrash();
																		}
																		catch(...)
																		{
																			ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
																			SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione durante ricerca frame", "SPVExecStep" );
																		}
																		////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima unlock xml", "SPVExecStep" );
																		XMLUnlockTrash();
																		////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: dopo unlock xml", "SPVExecStep" );
																		if ( ret_code == 0 ) // Ho trovato il frame
																		{
																			////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima nuovo buffer", "SPVExecStep" );
																			thread->StepRetCode = SPV_COMPROT_STEP_FRAME_FOUND;
																			thread->Step = command->StepList[s].ID; // Aggiorno l'ID dello step corrente
																			bbuffer = SPVNewBinBuffer( ); // -MALLOC
																			pbuffer = SPVNewBinBuffer( ); // -MALLOC
																			////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima step 3", "SPVExecStep" );
																			// --- Ricevo il frame dal buffer FIFO in ingresso ---
																			XMLLockTrash("SPVExecStep 3");
																			try
																			{
																				thread->StepRetCode = SPV_COMPROT_STEP_FRAME_EXTRACT_START;
																				ret_code = SPVReceiveFrame( device, bbuffer, pbuffer, port->InBuffer, &coords );

																				XMLEmptyTrash();
																			}
																			catch(...)
																			{
																				ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
																				SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione durante ricezione frame", "SPVExecStep" );
																			}
																			XMLUnlockTrash();
																			////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: dopo unlock xml 3", "SPVExecStep" );
																			if ( ret_code == 0 )
																			{
																				thread->StepRetCode = SPV_COMPROT_STEP_FRAME_EXTRACT_SUCCESS;
																				// --- Salvataggio payload (solo se specificato un PID dello step!!!) ---
																				if ( pbuffer->buffer != NULL && rcvstep->PID > 0 )
																				{
																					payload = SPVNewPayload( rcvstep->PID, pbuffer );
																					SPVAddPayload( thread, payload );
																				}
																				else
																				{
																					free( pbuffer ); // -FREE
																				}
																				thread->StepSuccess = true;
																				s = command->StepCount; // Fine del ciclo
																				////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: frame estratto", "SPVExecStep" );
																				/* DEBUG
																				SPVComProtPrintLog( command, 1, 3001, "DEBUG: Frame estratto correttamente", "SPVExecStep" );
																				//*/
																			}
																			else
																			{
																				thread->StepRetCode = SPV_COMPROT_STEP_FRAME_EXTRACT_FAILURE;
																				// --- Elimino il buffer del payload ---
																				SPVDeleteBinBuffer( pbuffer );
																				thread->StepSuccess = false;
																				////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: estrazione fallita", "SPVExecStep" );
																				/* DEBUG
																				SPVComProtPrintLog( command, 1, 3002, "DEBUG: Estrazione frame fallita", "SPVExecStep" );
																				//*/
																			}
																			/* TODO 1 -oEnrico -cUser : Prima di eliminare il buffer grezzo ricevuto potrei salvarlo da qualche parte per l'utente */
																			////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: prima delete buffer", "SPVExecStep" );
																			// --- Elimino il buffer 'grezzo' reicevuto ---
																			SPVDeleteBinBuffer( bbuffer );
																			////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: dopo delete buffer", "SPVExecStep" );
																		}
                            											else
                            											{
                            												thread->StepSuccess = false;
                            												if ( ret_code == SPV_COMPROT_FIFO_EMPTY )
                            												{
																				thread->StepRetCode = SPV_COMPROT_STEP_READ_FIFO_EMPTY;
																				////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: fifo vuota", "SPVExecStep" );
																				/* DEBUG
                            													SPVComProtPrintLog( command, 1, 3003, "DEBUG: FIFO in ricezione vuota", "SPVExecStep" );
                            													//*/
                            												}
                            												else
                            												{
                            													thread->StepRetCode = SPV_COMPROT_STEP_FRAME_NOT_FOUND;
                            													// --- Registro il fallimento dello step ---
                            													thread->FailureCode = ret_code;
																				thread->FailureStep = thread->Step;
																				////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: frame non riconosciuto", "SPVExecStep" );
																				/* DEBUG
																				SPVComProtPrintLog( command, 1, 3004, "DEBUG: Frame non riconosciuto", "SPVExecStep" );
																				//*/
																			}
																		}
																	}
																	catch(...)
																	{
																		ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
																		SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione generale ricezione s-esimo step", "SPVExecStep" );
																	}
																	// --- Esco dalla sezione critica del buffer ini ingresso ---
																	//LeaveCriticalSection( &port->InCriticalSection );
																	SYSUnLock(&port->InLock);
																	////SPVComProtPrintLog( command, 1, ret_code, "DEBUG: dopo unlock finale", "SPVExecStep" );

																	// --- Esco dalla sezione critica generale della porta ---
																	if ( !command->Blocking )
																	{
																		//LeaveCriticalSection( &port->CriticalSection );
																		SYSUnLock(&port->GeneralLock);
                            										}
                            									}
                            								}
                            								// --- Gestione dei cicli e timeout per i frame da ricevere ---
                            								if ( thread->StepSuccess ) // Se lo step si � concluso con successo
                            								{
                            									thread->Retry = step->Retry;
                            									if ( thread->Cycle != NULL ) // Se c'� un ciclo attivo
                            									{
                            										// Se c'� un ciclo while attivo su� primo step di questo 'livello' ed � terminato
                            										if ( thread->Cycle->Type == SPV_COMPROT_STEP_CYCLE_WHILE
                            											&& thread->Cycle->Step == first_step
                            											&& first_step != thread->Step )
                            										{
                            											cycle = thread->Cycle; // Salvo il ptr al ciclo attuale
                            											SPVRemoveCycle( thread ); // Lo rimuovo dal thread
                            											SPVDeleteCycle( cycle ); // E poi lo elimino definitvamente
                            										}
                            									}
                            								}
                            								else
                            								{
                            									thread->Retry++;
                            									thread->StepFailure = true;
                            									time = ULGetUnixTime();
                            									if ( ( ( time * 1000 ) - ( thread->StepStart * 1000 ) ) > step->Timeout )
                            									{
                            										thread->Retry = step->Retry; // Sono in timeout perci� esco dal ciclo while
                            									}
                            								}
                            								// --- Fine gestione cicli e timeout ---
                            								Sleep( step->Delay ); //Aspetto il tempo di ritardo
                            							}
                            							///Sleep( step->Delay ); //Aspetto il tempo di ritardo
                            							thread->StepFinish = ULGetUnixTime();
													}
												}
												else
												{
													ret_code = SPV_COMPROT_INVALID_FRAME;
												}
											}
											else
											{
												ret_code = SPV_COMPROT_FRAME_LIST_OVERFLOW;
											}
										}
										else
										{
											ret_code = SPV_COMPROT_INVALID_STEP;
										}
									}
									else
									{
										ret_code = SPV_COMPROT_STEP_LIST_OVERFLOW;
									}
								}
								else
								{
									ret_code = SPV_COMPROT_INVALID_COMMAND;
								}
							}
							else
							{
								ret_code = SPV_COMPROT_COMMAND_LIST_OVERFLOW;
							}
						}
						else
						{
							thread->StepFailure = true; // Step fallito
							ret_code = SPV_COMPROT_PORT_NOT_ACTIVE;
							SPVComProtPrintLog( command, 2, ret_code, "Porta non attiva. Impossibile aprire la porta", "SPVExecStep" );
						}
					}
					else
					{
						ret_code = SPV_COMPROT_INVALID_PORT;
					}
				}
				else
				{
					ret_code = SPV_COMPROT_INVALID_COMPROT_TYPE;
				}
			}
			else
			{
				ret_code = SPV_COMPROT_INVALID_DEVICE;
			}
		}
		else
		{
			ret_code = SPV_COMPROT_INVALID_THREAD;
		}
	}
	catch (...)
	{
		ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
	}
  /*
  if ( ret_code == SPV_COMPROT_CRITICAL_ERROR )
  {
	// --- Sblocco il cestino XML ---
	XMLUnlockTrash();
	if ( port != NULL )
	{
	  // --- Esco dalla sezione critica generale della porta ---
	  LeaveCriticalSection( &port->CriticalSection );
	  // --- Esco dalla sezione critica del buffer ini ingresso ---
	  LeaveCriticalSection( &port->InCriticalSection );
	  // --- Esco dalla sezione critica del buffer in uscita ---
	  LeaveCriticalSection( &port->OutCriticalSection );
	}
  }
  */
	return ret_code;
}

//==============================================================================
/// Funzione per passare al successivo step di thread protocollo
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [15.01.2010]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int SPVNextStep( SPV_COMPROT_THREAD * thread )
{
  int                   ret_code      = 0     ; // Codice di ritorno
  int                   current_step          ; // ID Step corrente
  int                   next_step     = -1    ; // ID Step successivo
  bool                  next_search   = false ; // Flag di ricerca dello step successivo
  SPV_COMPROT_STEP    * next          = NULL  ; // Ptr a step successivo
  SPV_COMPROT_STEP    * step          = NULL  ; // Ptr a step corrente
  SPV_COMPROT_TYPE    * type          = NULL  ; // Ptr a tipo protocollo
  SPV_COMPROT_COMMAND * command       = NULL  ; // Ptr a comando
  SPV_COMPROT_CYCLE   * cycle         = NULL  ; // Ptr a ciclo

  if ( thread != NULL ) // Controllo la validit� del ptr a thread di protocollo
  {
    type = thread->Type;
    if ( type != NULL ) // Controllo la validit� del ptr alla struttura tipo
    {
      if ( thread->Command < type->CommandCount ) // Controllo che non ci sia un overflow
      {
        command = &type->CommandList[thread->Command];
        if ( command != NULL ) // Controllo il ptr alla struttura comando
		{
          if ( thread->Step < command->StepCount ) // Controllo che non ci sia un overflow
          {
            step = &command->StepList[thread->Step];
            if ( step != NULL ) // Controllo il ptr alla struttura step
            {
              current_step  = thread->Step;
              if ( thread->StepFailure ) // Se lo step precedente � fallito
              {
                if ( step->FailID != 0 ) // Se per lo step � specificato un salto in caso di fallimento
                {
                  thread->Step      = step->FailID;
                  thread->PrevStep  = current_step;
                }
                else
                {
                  thread->Waiting = false;
                  ret_code = SPV_COMPROT_STEP_FAILURE;
                }
              }
				else
				{
					if ( thread->StepSuccess ) // Se lo step precedente � andato a buon fine
					{
						/*
						if ( step->SuccessID != 0 ) { // Salto in caso di successo
							thread->Waiting = false;
							thread->Step      = step->SuccessID;
							thread->PrevStep  = current_step;
						}
						else
						*/
						{
							thread->Waiting = false;
							if ( step->Cycle == SPV_COMPROT_STEP_CYCLE_NONE ) // Se non c'� nessun ciclo in questo step
							{
								if ( step->Count > 1 && thread->Counter < step->Count ) // Se ripeto lo step
								{
									next_step = thread->Step; // Lo step rimane quello attuale
								}
								else // Se l'esecuzione dello step � singola
								{
									next_search = true;
								}
							}
							if ( step->Cycle == SPV_COMPROT_STEP_CYCLE_FOR ) // Se c'� un ciclo FOR
							{
								if ( step->Count > 1 ) // Se ho un valore valido di count
								{
									if ( thread->Cycle == NULL ) // Se in questo thread non � attivo nessun ciclo
									{
										cycle = SPVNewCycle( step->ID, step->Cycle ); // Creo un nuovo ciclo per questo step
										if ( cycle != NULL )
										{
											cycle->Num = 1;
											SPVAddCycle( thread, cycle ); // Aggiungo il ciclo al thread
										}
										else
										{
											// ERRORE: Impossibile allocare un nuovo ciclo
										}
										next_search = true; // Poi cerco lo step successivo
									}
									else
									{
										if ( thread->Cycle->Step == step->ID ) // Se � gia attivo questo ciclo
										{
											if ( thread->Cycle->Num < ( step->Count - 1 ) ) // Se il ciclo non si � concluso
											{
												thread->Cycle->Num++; // Incremento il contatore del ciclo
											}
											else // Se il ciclo si � concluso
											{
												cycle = thread->Cycle; // Salvo il ptr al ciclo attuale
												SPVRemoveCycle( thread ); // Lo rimuovo dal thread
												SPVDeleteCycle( cycle ); // E poi lo elimino definitvamente
											}
											next_search = true; // Poi cerco lo step successivo
										}
										else // E' attivo un altro ciclo (far� un uovo ciclo annidato)
										{
											cycle = SPVNewCycle( step->ID, step->Cycle );
											if ( cycle != NULL )
											{
												cycle->Num = 1;
												SPVAddCycle( thread, cycle ); // Aggiungo il ciclo al thread
											}
											else
											{
												// ERRORE: Impossibile allocare un nuovo ciclo
											}
											next_search = true; // Poi cerco lo step successivo
										}
									}
								}
								else // Se il valore di count � non valido (<2) allora vado allo step successivo
								{
									next_search = true; // Poi cerco lo step successivo
								}
							}
							if ( step->Cycle == SPV_COMPROT_STEP_CYCLE_WHILE ) // Se c'� un ciclo WHILE
							{
								if ( thread->Cycle == NULL ) // Se in questo thread non � attivo nessun ciclo
								{
									cycle = SPVNewCycle( step->ID, step->Cycle ); // Creo un nuovo ciclo per questo step
									if ( cycle != NULL )
									{
										cycle->Num = 1;
										SPVAddCycle( thread, cycle ); // Aggiungo il ciclo al thread
									}
									else
									{
										// ERRORE: Impossibile allocare un nuovo ciclo
									}
									next_search = true; // Poi cerco lo step successivo
								}
								else
								{
									if ( thread->Cycle->Step == step->ID ) // Se � gia attivo questo ciclo
									{
										thread->Cycle->Num++; // Incremento il contatore del ciclo
										next_search = true; // Poi cerco lo step successivo
									}
									else // E' attivo un altro ciclo (far� un uovo ciclo annidato)
									{
										cycle = SPVNewCycle( step->ID, step->Cycle );
										if ( cycle != NULL )
										{
											cycle->Num = 1;
											SPVAddCycle( thread, cycle ); // Aggiungo il ciclo al thread
										}
										else
										{
											// ERRORE: Impossibile allocare un nuovo ciclo
										}
										next_search = true; // Poi cerco lo step successivo
									}
								}
							}
							if ( next_search ) // Se � stato attivato il flag di ricerca dello step successivo
							{
								thread->Counter = 0; // Resetto il contatore delle ripetizioni di uno step
								if ( step->SuccessID != 0 ) { // Salto in caso di successo
									next_step	= step->SuccessID;
								}
								else
								{
									for ( int s = 0; s < command->StepCount; s++ ) // Ciclo sulla lista di step
									{
										next = &command->StepList[s];
										if ( next->PrevID == current_step && s != current_step )
										{
											next_step = s;
											s = command->StepCount; // Fine ciclo
										}
									}
								}
							}
							if ( next_step == -1 ) // Se non ho trovato nessuno step successivo
							{
								if ( thread->Cycle != NULL ) // Se c'� un ciclo attivo
								{
									next_step = thread->Cycle->Step; // Ritorno al primo step del ciclo
								}
								else // Altrimenti lo step � proprio finito
								{
									thread->Active = false;
									ret_code = SPV_COMPROT_THREAD_FINISH;
								}
							}
							if ( next_step != -1 ) // Se ho trovato lo step successivo
							{
								thread->Step      = next_step;
								thread->PrevStep  = current_step;
							}
						}
					}
				}
            }
            else
            {
              ret_code = SPV_COMPROT_INVALID_STEP;
            }
          }
		  else
          {
			ret_code = SPV_COMPROT_STEP_LIST_OVERFLOW;
          }
        }
        else
        {
          ret_code = SPV_COMPROT_INVALID_COMMAND;
        }
      }
      else
      {
        ret_code = SPV_COMPROT_COMMAND_LIST_OVERFLOW;
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_DEV_TYPE;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per accedere ai dati della lista thread di protocollo
///
/// \date [06.02.2008]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void SPVEnterThreadData( void )
{
	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SPVEnterThreadData");
	// --- Entro nella sezione critica ---
	//EnterCriticalSection( &SPVComProtThreadCriticalSection );
	SYSLock( &SPVComProtThreadLock, "SPVEnterThreadData");
}

//==============================================================================
/// Funzione per uscire dai dati della lista thread di protocollo
///
/// \date [05.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVLeaveThreadData( void )
{
	// --- Esco dalla sezione critica ---
	//LeaveCriticalSection( &SPVComProtThreadCriticalSection );
	SYSUnLock(&SPVComProtThreadLock);
	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();
}

//==============================================================================
/// Funzione di attivazione per un thread di un comando
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.20
//------------------------------------------------------------------------------
unsigned long __stdcall SPVComProtThread( void * parameter )
{
	unsigned long         ret_code  	= SPV_COMPROT_NO_ERROR  ; // Codice di ritorno
	int                   fun_code 		= SPV_COMPROT_NO_ERROR  ;
	SPV_COMPROT_THREAD  * cpthread  	= NULL					; // Ptr a struttura thread
	SYS_THREAD          * thread	   	= NULL					; // Puntatore al thread
	SPV_COMPROT_COMMAND * command   	= NULL					; // Ptr ad una struttura comando
	char          		* thread_name 	= NULL  				; // Stringa del nome del thread

	try
	{
		if ( parameter != NULL ) // Controllo il parametro passato
		{
			cpthread = (SPV_COMPROT_THREAD*) parameter;
			if ( SPVValidComProtThread( cpthread ) ) // Controllo la struttura del thread di protocollo
			{
				// --- Entro nella sezione critica ---
				SPVEnterThreadData( );
				try
				{
					cpthread->FinishCode = SPV_COMPROT_THREAD_OK;
					thread = (SYS_THREAD*) cpthread->Thread; // Recupero il thread
				}
				catch(...)
				{
					thread = NULL;
				}
				// --- Esco dalla sezione critica ---
				SPVLeaveThreadData( );

				if ( SYSValidThread( thread ) )  // Controllo il thread
				{
					// assegno il nome al thread
					try
					{
						if (cpthread->Device != NULL)
						{
							thread_name = ULAddText(thread_name,cpthread->Device->Name);
						}
						else
						{
							thread_name = ULAddText(thread_name,"Periferica sconosciuta");
						}
						thread_name = ULAddText(thread_name," - ");
						thread_name = ULAddText(thread_name,SPVGetCommandName(cpthread));
						thread_name = ULAddText(thread_name," - Command");
					}
					catch(...)
					{
						fun_code = SPV_COMPROT_FUNCTION_EXCEPTION;
					}

					if (fun_code == SPV_COMPROT_NO_ERROR)
					{
						fun_code = SYSSetThreadName(thread,thread_name);
					}
					/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */
                    thread->StepID = 0;
					//* DEBUG */ SPVComProtPrintLog( NULL, 1, 1111, "Inizio Thread", "SPVComProtThread" );
					///SPVUpdateCommandStatus( SPVGetComProtThreadPos( cpthread ) );
					while ( !thread->Terminated ) // Ciclo del thread di protocollo
					{
						try
						{
							if ( cpthread->Active )
							{
			                    thread->StepID = (unsigned long)cpthread->Step;
								ret_code = SPVExecStep( cpthread );
								Sleep( cpthread->Sleep ); // Aspetto un tempo di ritardo
								if ( ret_code == 0 ) // Esecuzione avvenuta con successo
								{
									// --- Entro nella sezione critica ---
									SPVEnterThreadData( );
									try
									{
										ret_code = SPVNextStep( cpthread ); // Cerco lo step successivo
										if ( ret_code != 0 )
										{
											if ( ret_code == SPV_COMPROT_THREAD_FINISH ) // Thread concluso
											{
												cpthread->Active = false;
											}
											else // L'esecuzione dello step � fallita
											{
		    			    					cpthread->Active = false;
		    			    					cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
											}
										}
		    			    		}
		    			    		catch(...)
									{
		    			    			// --- Eccezione in fase di ricerca dello step successivo ---
		    							cpthread->Active = false;
										cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
		    			    			ret_code == SPV_COMPROT_THREAD_FINISH;
		    			    		}
									// --- Esco dalla sezione critica ---
									SPVLeaveThreadData( );
		    			    	}
		    			    	else
		    			    	{
		    			    		if ( cpthread->StepFailure )
									{
										// --- Entro nella sezione critica ---
										SPVEnterThreadData( );
										try
										{
											cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
											ret_code = SPVNextStep(cpthread); // Cerco lo step successivo
											if ( ret_code != 0 )
											{
												if ( ret_code == SPV_COMPROT_THREAD_FINISH ) // Thread concluso
												{
													cpthread->Active = false;
												}
												else // L'esecuzione dello step � fallita
												{
													cpthread->Active = false;
												}
											}
										}
										catch(...)
										{
											// --- Eccezione in fase di ricerca dello step successivo ---
											cpthread->Active = false;
											cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
											ret_code == SPV_COMPROT_THREAD_FINISH;
										}
										// --- Esco dalla sezione critica ---
										SPVLeaveThreadData( );
									}
									else
									{
										// --- Entro nella sezione critica ---
										SPVEnterThreadData( );
										try
										{
											cpthread->Active = false;
											cpthread->FinishCode = SPV_COMPROT_THREAD_ERROR;
										}
										catch(...)
										{
											// --- Eccezione ---
											cpthread->Active = false;
											cpthread->FinishCode = SPV_COMPROT_THREAD_ERROR;
										}
										// --- Esco dalla sezione critica ---
										SPVLeaveThreadData( );
									}
								}
							}
							else
							{
								// --- Gestione tentativo del comando ---
								if ( cpthread->FinishCode == SPV_COMPROT_THREAD_FAILURE
								&& cpthread->CmdRetryCount < ( cpthread->CmdRetry - 1 ) )
								{
									// --- Entro nella sezione critica ---
									SPVEnterThreadData( );
									try
									{
										// --- Aggiorno i contatore dei tentetivi del comando ---
										cpthread->CmdRetryCount++;
										// --- Nuovo tentativo ---
										cpthread->Active = true;
										cpthread->Step = 0;
										command = &cpthread->Type->CommandList[cpthread->Command];
										if ( command != NULL )
										{
											// --- Se il comando � bloccante ---
											if ( command->Blocking )
											{
												// --- Attendo un tempo di ritardo ---
												Sleep( cpthread->CmdRetryDelay );
												// --- Svuoto la FIFO in ingresso ---
												SYSClearBufferFIFO( cpthread->Port->InBuffer );
											}
										}
									}
									catch(...)
									{
										// --- Eccezione in fase di ricerca dello step successivo ---
										cpthread->Active = false;
										cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
										//*** CONTROLLARE
										ret_code == SPV_COMPROT_THREAD_FINISH;
									}
									// --- Esco dalla sezione critica ---
									SPVLeaveThreadData( );
								}
								else
								{
									if ( cpthread->Step == 0 )
									{
										if ( cpthread->FinishCode != SPV_COMPROT_THREAD_ERROR
											&& cpthread->FinishCode != SPV_COMPROT_THREAD_FAILURE
											&& cpthread->FinishCode != SPV_COMPROT_THREAD_KILLED )
										{
											// --- Entro nella sezione critica ---
											SPVEnterThreadData( );
											try
											{
												cpthread->Active = true;
											}
											catch(...)
											{
												// --- Eccezione  ---
												cpthread->Active = false;
												cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
												//*** CONTROLLARE
												ret_code == SPV_COMPROT_THREAD_FINISH;
											}
											// --- Esco dalla sezione critica ---
											SPVLeaveThreadData( );
										}
										else
										{
											// --- Entro nella sezione critica ---
											SPVEnterThreadData( );
											try
											{
												cpthread->Active = false;
											}
											catch(...)
											{
												// --- Eccezione in fase di ricerca dello step successivo ---
												cpthread->Active = false;
												cpthread->FinishCode = SPV_COMPROT_THREAD_FAILURE;
											}
											// --- Esco dalla sezione critica ---
											SPVLeaveThreadData( );
											//* DEBUG */ SPVComProtPrintLog( NULL, 1, 1112, "Fine Thread", "SPVComProtThread" );
											SYSExitThread( cpthread->Thread );
										}
									}
									else
									{
										//* DEBUG */ SPVComProtPrintLog( NULL, 1, 1113, "Fine Thread", "SPVComProtThread" );
										SYSExitThread( cpthread->Thread );
									}
								}
							}
						}
						catch (...)
						{
							/* DONE 1 -oEnrico -cError : Gestire l'errore critico */
							SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione dentro al ciclo while", "SPVComProtThread" );
						}
						///SPVUpdateCommandStatus( SPVGetComProtThreadPos( cpthread ) );

						Sleep(50); //--- tempo di ritardo di sicurezza per non occupare costantemente la CPU

						// Aggiornamento delle informazioni di debug del thread
						SYSUpdateThreadDebugInfo(thread);
					}
					// --- Se il thread viene terminato ma il comando risulta ancora attivo ---
					if ( cpthread->Active == true )
					{
						// --- Entro nella sezione critica ---
						SPVEnterThreadData( );
						try
						{
							cpthread->Active = false;
							cpthread->FinishCode = SPV_COMPROT_THREAD_KILLED;
							/* TODO 2 -oEnrico -cThread : Capire la causa di questa condizione */
						}
						catch(...)
						{
							// --- Eccezione in fase di ricerca dello step successivo ---
							cpthread->Active = false;
							cpthread->FinishCode = SPV_COMPROT_THREAD_KILLED;
							SPVComProtPrintLog( NULL, 2, SPV_COMPROT_FUNCTION_EXCEPTION, "Eccezione in caso terminazione comando", "SPVComProtThread" );
						}
						// --- Esco dalla sezione critica ---
						SPVLeaveThreadData( );
					}
					//* DEBUG */ SPVComProtPrintLog( NULL, 1, 1114, "Fine Thread", "SPVComProtThread" );
					///SPVUpdateCommandStatus( SPVGetComProtThreadPos( cpthread ) );
				}
		    	else
		    	{
					ret_code = SPV_COMPROT_INVALID_THREAD;
				}
			}
			else
			{
				ret_code = SPV_COMPROT_INVALID_THREAD;
			}
		}
		else
		{
			ret_code = SPV_COMPROT_INVALID_PARAMETER;
		}
	}
	catch (...)
	{
		XMLUnlockTrash();

		if ( cpthread != NULL )
		{
			if ( cpthread->Port != NULL )
			{
				//LeaveCriticalSection( &cpthread->Port->InCriticalSection );
				SYSUnLock(&cpthread->Port->InLock);
				//LeaveCriticalSection( &cpthread->Port->OutCriticalSection );
				SYSUnLock(&cpthread->Port->OutLock);
				//LeaveCriticalSection( &cpthread->Port->CriticalSection );
				SYSUnLock(&cpthread->Port->GeneralLock);
			}
		}
		ret_code = SPV_COMPROT_CRITICAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere la prima posizione libera nella lista dei thread
///
/// In caso di errore o di posizione libera non trovata restituisce -1.
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVGetFirstFreeThreadPos( void )
{
	int                   thread_pos  = -1  ;
  SPV_COMPROT_THREAD  * thread      = NULL;
  unsigned long         thread_id   = 0   ;

  // --- Entro nella sezione critica ---
	SPVEnterThreadData( );

	try
	{
	  if ( SPVComProtThreadCount < SPV_COMPROT_MAX_THREAD )
	  {
	  	for ( int t = 0; t < SPV_COMPROT_MAX_THREAD; t++ )
	  	{
	  		thread = &SPVComProtThreadList[t];
	  		if ( thread != NULL )
	  		{
	  			try
	  			{
	  				thread_id = thread->ID;
	  			}
				catch (...)
	  			{
	  				thread_id = -1;
	  			}
	  			if ( thread_id == 0 ) // Se la posizione � libera
	  			{
	  				thread_pos = t; // Salvo la posizione
	  				t = SPV_COMPROT_MAX_THREAD; // Fine del ciclo for
	  			}
	  		}
	  	}
		}
	}
	catch(...)
	{
		thread_pos = -1;
	}

  // --- Esco dalla sezione critica ---
  SPVLeaveThreadData( );

  return thread_pos;
}

//==============================================================================
/// Funzione per ottenere l'ID del primo thread concluso
///
/// In caso di errore o di thread non trovato restituisce 0.
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
unsigned long SPVGetFirstClosedThread( void )
{
  SPV_COMPROT_THREAD  * thread        = NULL  ; // Ptr a struttura thread di protocollo
  bool                  thread_active = false ; // Flag di attivit� del thread di protocollo
  int                   thread_code   = 0     ; // Codice di fine thread di protocollo
	unsigned long         thread_id     = 0     ; // ID del thread di protocollo
  int                   thread_pos    = 0     ; // Posizione in lista del thread

  // --- Entro nella sezione critica ---
	SPVEnterThreadData( );

	try
	{
	  for ( int t = 0; t < SPV_COMPROT_MAX_THREAD; t++ )
    {
      // --- Faccio partire il ciclo dal puntatore al primo thread ---
      thread_pos = t + SPVComProtThreadFirst;
      if ( ( thread_pos + 1 ) > SPV_COMPROT_MAX_THREAD )
      {
        thread_pos -= SPV_COMPROT_MAX_THREAD;
      }
      thread = &SPVComProtThreadList[thread_pos];
      if ( thread != NULL )
      {
        try
        {
          thread_active = thread->Active;
          thread_code   = thread->FinishCode;
        }
        catch (...)
        {
          thread_active = true;
          thread_code   = 0;
					thread_id = 0;
        }
        if ( !thread_active && thread_code != 0 ) // Verifico che sia terminato
        {
          thread_id = thread->ID; // Salvo l'ID del thread concluso
          break; // Fine del ciclo for
        }
      }
		}
	}
	catch(...)
	{
		thread_id = 0;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveThreadData( );

	return thread_id;
}

//==============================================================================
/// Funzione per ottenere la posizione del primo thread da visualizzare
///
/// In caso di errore o di thread non trovato restituisce -1.
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVGetFirstThreadPos( void )
{
  int                   thread_pos    = -1  ; // Posizione in lista di un thread
  SPV_COMPROT_THREAD  * thread        = NULL; // Ptr alla struttura thread di protocollo
  unsigned long         thread_id     = 0   ; // ID del thread di protocollo
  unsigned long         thread_id_min = 0   ; // Minimo ID del thread di protocollo

  // --- Entro nella sezione critica ---
	SPVEnterThreadData( );

	try
	{
	  thread_id_min--;

	  // --- Ciclo sulla lista dei thread di protocollo ---
	  for ( int t = 0; t < SPV_COMPROT_MAX_THREAD; t++ )
	  {
	  	thread = &SPVComProtThreadList[t];
	  	if ( thread != NULL )
	  	{
	  		try
	  		{
	  			thread_id = thread->ID;
	  		}
	  		catch (...)
	  		{
					thread_id = -1;
				}
				if ( thread_id > 0 && thread_id < thread_id_min ) // Se l'ID � valido e minore del min
				{
					thread_id_min = thread_id; // Salvo l'ID
					thread_pos = t; // Salvo la posizione
				}
			}
		}
	}
	catch(...)
	{
		thread_id = -1;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveThreadData( );

	return thread_pos;
}

//==============================================================================
/// Funzione per ottenere la posizione di un thread di protocollo
///
/// In caso di errore o di thread non trovato restituisce -1.
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVGetComProtThreadPos( SPV_COMPROT_THREAD * thread )
{
  int                   thread_pos  = -1  ;
  SPV_COMPROT_THREAD  * current     = NULL;

  if ( thread != NULL )
  {
    // --- Entro nella sezione critica ---
		SPVEnterThreadData( );

		try
		{
		  for ( int t = 0; t < SPV_COMPROT_MAX_THREAD; t++ )
		  {
		  	current = &SPVComProtThreadList[t];
		  	if ( current != NULL )
		  	{
		  		if ( thread == current ) // Se la posizione � libera
		  		{
		  			thread_pos = t; // Salvo la posizione
		  			t = SPV_COMPROT_MAX_THREAD; // Fine del ciclo for
		  		}
		  	}
			}
		}
		catch(...)
		{
			thread_pos = -1;
		}

		// --- Esco dalla sezione critica ---
		SPVLeaveThreadData( );
	}

	return thread_pos;
}

//==============================================================================
/// Funzione per recuperare il ptr di un thread di protocollo dal suo ID
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_COMPROT_THREAD * SPVGetComProtThread( unsigned long ID )
{
  SPV_COMPROT_THREAD * thread   = NULL;
  SPV_COMPROT_THREAD * current  = NULL;

  if ( ID > 0 ) // Controllo il thread
  {
    // --- Entro nella sezione critica ---
		SPVEnterThreadData( );

  	try
		{
		  // --- Ciclo sulla lista globale dei thread di protocollo ---
		  for ( int t = 0; t < SPV_COMPROT_MAX_THREAD; t++ )
		  {
		  	current = &SPVComProtThreadList[t];
		  	if ( current != NULL )
		  	{
		  		if ( current->ID == ID ) // Verifico l'ID
		  		{
		  			thread = current; // Salvo il ptr alla struttura thread
		  			t = SPV_COMPROT_MAX_THREAD; // Fine del ciclo for
		  		}
		  	}
			}
		}
		catch(...)
		{
			thread = NULL;
		}

		// --- Esco dalla sezione critica ---
		SPVLeaveThreadData( );
	}

	return thread;
}

//==============================================================================
/// Funzione per generare un thread di un comando
/// Dato il puntatore alla struttura di tipo protocollo precedentemente caricata
/// <comprot>, genera i parametri di un nuovo thread di protocollo assegnato al
/// comando specificato <command>. <thread> conterr� il puntatore al nuovo
/// thread che far� parte dell'array globale SPVComProtThreadList.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [25.05.2006]
/// \author Enrico Alborali
/// \version 0.13
//------------------------------------------------------------------------------
int SPVNewComProtThread( SPV_COMPROT_THREAD * thread, SPV_DEVICE * device, int command )
{
  int                 ret_code  = SPV_COMPROT_NO_ERROR; // Codice di ritorno
  SPV_COMPROT_TYPE  * comprot   = NULL  ; // Ptr a struttura tipo di protocollo
  XML_ELEMENT       * def       = NULL  ; // Ptr a elemento XML di definizione
  SYS_PORT          * port      = NULL  ; // Ptr a struttura porta

  if ( device != NULL ) // Controllo che il ptr a struttura periferica
  {
    comprot               = (SPV_COMPROT_TYPE*) device->SupCfg.ComProt;
    def                   = (XML_ELEMENT*) comprot->Def;
    port                  = (SYS_PORT*) device->SupCfg.Port;
    //thread                = &SPVComProtThreadList[SPVComProtThreadCount]; // Recupero ptr thread nuovo
	//--- Imposto i parametri generali del nuovo thread ---
    if ( SPVComProtThreadNextID == 0 ) SPVComProtThreadNextID++;
    thread->VCC           = SPV_COMPROT_THREAD_VALIDITY_CHECK_CODE;
    thread->ID            = SPVComProtThreadNextID++;
    thread->Active        = false         ;
    thread->Waiting       = false         ;
	thread->Device        = device        ;
    thread->Type          = comprot       ;
    thread->Port          = port          ;
    thread->Command       = command       ;
    thread->CmdRetry      = SPV_COMPROT_COMMAND_RETRY_DEFAULT;
    thread->CmdRetryCount = 0             ;
    thread->CmdRetryDelay = SPV_COMPROT_COMMAND_RETRY_DELAY;
    thread->Counter       = 0             ; // Azzero il contatore delle ripetizioni dello step
    thread->Retry         = 0             ;
    thread->PrevStep      = -1            ; // Nessuno step precedente
    thread->Step          = 0             ; // Parto dal primo step
	thread->StepSuccess   = false         ;
    thread->StepFailure   = false         ;
    thread->StepFinish    = 0             ;
    thread->StepStart     = 0             ;
    thread->StepRetCode   = SPV_COMPROT_STEP_IDLE;
    thread->Sleep         = SPV_COMPROT_THREAD_SLEEP_DEFAULT;
    thread->FinishCode    = 0             ;
    thread->FailureStep   = -1            ;
    thread->FailureCode   = 0             ;
    thread->FirstPayload  = NULL          ;
    thread->Def           = def           ;
    thread->Tag           = NULL          ;
		//---
	thread->Thread = SYSNewThread(SPVComProtThread,0,CREATE_SUSPENDED); // Alloco un nuovo thread per il protocollo
    ret_code = SYSSetThreadParameter(thread->Thread,(void*)thread); // Gli passo come parametro la struttura del thread
    if ( ret_code == SPV_COMPROT_NO_ERROR )
    {
      ret_code = SYSCreateThread(thread->Thread); // Istanzio il nuovo thread
      if ( ret_code != SPV_COMPROT_NO_ERROR )
      {
				thread->FinishCode = SPV_COMPROT_THREAD_ABORT;
      }
    }
    SPVComProtThreadCount++; // Incremento il contatore di thread di protocollo
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_COMPROT_TYPE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una struttura thread comando
///
/// \date [22.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVValidComProtThread( SPV_COMPROT_THREAD * thread )
{
  bool  valid = false;

  if ( thread != NULL )
  {
    try
    {
      if ( thread->VCC == SPV_COMPROT_THREAD_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per eliminare un thread di un comando
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SPVDelComProtThread( SPV_COMPROT_THREAD * thread )
{
  int           ret_code  = SPV_COMPROT_NO_ERROR; // Codice di ritorno
  unsigned long thread_id = 0; // ID del thread di protocollo

  // --- Entro nella sezione critica ---
	SPVEnterThreadData( );

	try
	{
	  if ( SPVValidComProtThread( thread ) )
	  {
	  	try
	  	{
	  		thread_id = thread->ID;
	  	}
	  	catch (...)
	  	{
	  		thread_id = 0;
	  	}
	  	if ( thread_id > 0 ) // Se la posizione � occupata
	  	{
	  		memset( thread, 0, sizeof(SPV_COMPROT_THREAD) ); // Cancello la memoria occupata dal thread
	  		SPVComProtThreadCount--;
	  		// --- Cerco la posizone del thread pi� vecchio ---
			SPVComProtThreadFirst = SPVGetFirstThreadPos( );
			}
	  }
	  else
	  {
			ret_code = SPV_COMPROT_INVALID_THREAD;
		}
	}
	catch(...)
	{
		ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
	}

  // --- Esco dalla sezione critica ---
  SPVLeaveThreadData( );

  return ret_code;
}

//==============================================================================
/// Funzione per rimuovere un thread di un comando dalla lista globale
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVRemoveComProtThread( unsigned long ID )
{
  int                   ret_code    = SPV_COMPROT_NO_ERROR; // Codice di ritorno
  int                   thread_pos  = -1  ; // Posizione del thread di protocollo
  SPV_COMPROT_THREAD  * thread      = NULL; // Ptr alla struttura thread di protocollo
  SPV_COMPROT_THREAD    temp_list[SPV_COMPROT_MAX_THREAD];
  int                   thread_left = 0   ;

  // --- Entro nella sezione critica ---
	SPVEnterThreadData( );

	try
	{
	  thread = SPVGetComProtThread( ID );

	  if ( thread != NULL ) // Controllo il ptr al thread di protocollo
	  {
	  	thread_pos = SPVGetComProtThreadPos( thread );
	  	if ( thread_pos >= 0 && thread_pos < SPV_COMPROT_MAX_THREAD )
	  	{
	  		// --- Azzero la lista temporanea dei thread di protocollo ---
	  		memset( &temp_list, 0, sizeof(SPV_COMPROT_THREAD) * SPV_COMPROT_MAX_THREAD );
	  		thread_left = SPV_COMPROT_MAX_THREAD - thread_pos - 1;
	  		// --- Se non � l'ultimo elemento della lista ---
	  		if ( thread_left > 0 )
	  		{
	  			// --- Copio la parte di lista successiva al thread da rimuovere ---
	  			memcpy( &temp_list, &SPVComProtThreadList[thread_pos+1], sizeof(SPV_COMPROT_THREAD) * ( thread_left ) );
	  			// --- Copio la parte di lista successiva partendo dalla posizione del thread eliminato ---
	  			memcpy( &SPVComProtThreadList[thread_pos], &temp_list, sizeof(SPV_COMPROT_THREAD) * ( thread_left ) );
	  		}
	  		// --- Elimino l'ultimo thread dalla lista ---
	  		memset( &SPVComProtThreadList[SPV_COMPROT_MAX_THREAD-1], 0, sizeof(SPV_COMPROT_THREAD) );
	  		// --- Cerco la posizone del thread pi� vecchio ---
	  		SPVComProtThreadFirst = SPVGetFirstThreadPos( );
	  	}
	  	else
	  	{
	  		ret_code = SPV_COMPROT_THREAD_NOT_PRESENT;
			}
	  }
	  else
	  {
	  	ret_code = SPV_COMPROT_INVALID_THREAD;
		}
	}
	catch(...)
	{
		ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveThreadData( );

	return ret_code;
}

//==============================================================================
/// Funzione per far partire un thread di un comando
///
/// \date [05.07.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVStartComProtThread( SPV_COMPROT_THREAD * thread )
{
  int ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno

  if ( thread != NULL )
  {
    thread->Active = true;

    ret_code = SYSResumeThread( thread->Thread );
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per interrompere un thread di un comando
///
/// \date [23.07.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
int SPVStopComProtThread(SPV_COMPROT_THREAD * thread)
{
  int ret_code = 0; // Codice di ritorno

  if ( thread != NULL )
  {
    ret_code = SYSSuspendThread( thread->Thread );
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eseguire un comando di protocollo
/// NOTA: Se il comando � bloccante entro nelle sezioni critiche prima della sua
///       esecuzione ed esco alla fine della sua esecuzione. Se non � bloccante
///       si entra ed esce dalle sezioni critiche soltanto all'inizio e alla
///       fine di un singolo step del comando.
///
/// \date [09.06.2008]
/// \author Enrico Alborali
/// \version 0.17
//------------------------------------------------------------------------------
SPV_COMPROT_PAYLOAD * SPVExecCommand( SPV_DEVICE * device, int command, int * retcode, SPV_COMPROT_PAYLOAD * payload_out )
{
	int                   ret_code    = SPV_COMPROT_NO_ERROR; // Codice di ritorno
	int                   fun_code    = SPV_COMPROT_NO_ERROR; // Codice di ritorno di funzioni
	int                   wait_code   = 0   ; // Codice di attesa thread
	int                   thread_pos  = -1  ; // Posizione in lista del thread
	SPV_COMPROT_THREAD  * thread      = NULL; // Ptr a thread di protocollo
	SPV_COMPROT_PAYLOAD * payload     = NULL; // Ptr ad eventuale payload raccolto
	SPV_COMPROT_COMMAND * commandp    = NULL; // Ptr ad una struttura comando
	unsigned long         thread_id   = 0   ;

	try
	{
		if ( device != NULL ) // Controllo la validit� del ptr a periferica
		{
			if ( command >= 0 ) // Controllo la validit� del ID a comando
			{
				// --- Entro nella sezione critica ---
				SPVEnterThreadData( );
				try
				{
					// --- Recupero la prima posizione libera nella lista dei thread ---
					thread_pos = SPVGetFirstFreeThreadPos( );
					if ( thread_pos == -1 ) // Se la lista � piena
					{
						// --- Cerco il primo thread concluso ---
						thread_id = SPVGetFirstClosedThread( );
						if ( thread_id > 0 ) // Se c'� un thread concluso da cancellare
						{
							thread = SPVGetComProtThread( thread_id );
							if ( thread != NULL )
							{
								thread_pos = SPVGetComProtThreadPos( thread );
								if ( thread_pos != -1 )
								{
									// --- Elimino il thread concluso ---
									fun_code = SPVDelComProtThread( thread );
									// fun_code = SPVRemoveComProtThread( thread_id );
									if ( fun_code != SPV_COMPROT_NO_ERROR ) // Se c'� stato un errore
									{
										thread_pos = -1;
									}
								}
							}
							thread = NULL;
						}
					}
				}
				catch(...)
				{
					thread_pos = -1;
				}
				if ( thread_pos >= 0 )
				{
					try
					{
						thread = &SPVComProtThreadList[thread_pos];
						ret_code = SPVNewComProtThread( thread, device, command );
					}
					catch(...)
					{
						ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
					}
					// --- Esco dalla sezione critica ---
					SPVLeaveThreadData( );
					if ( ret_code == 0 ) // Se il nuovo thread � stato creato con successo
					{
						if ( thread != NULL )
						{
							///SPVUpdateCommandStatus( SPVGetComProtThreadPos( thread ) ); // -GUI
							if ( thread->Type != NULL )
							{
								if ( command < thread->Type->CommandCount )
								{
									commandp = &thread->Type->CommandList[thread->Command];
									if ( commandp != NULL )
									{
										// --- Se il comando � bloccante ---
										if ( commandp->Blocking )
										{
											//EnterCriticalSection( &thread->Port->CriticalSection );
											SYSLock(&thread->Port->GeneralLock,"SPVExecCommand");
											SYSClearBufferFIFO( thread->Port->InBuffer );
										}
										// --- Gestione della non-persistenza della porta ---
                    					try
                    					{
											if ( !thread->Port->Active )
                    						{
												SYSOpenPort( thread->Port );
											}
										}
										catch(...)
										{

										}
										try
										{
											// --- Aggiungo un eventuale payload in uscita ---
											if ( payload_out != NULL )
											{
												thread->FirstPayload = payload_out;
											}
											// --- Faccio partire il thread del comando ---
											ret_code = SPVStartComProtThread( thread );
											if ( ret_code == 0 ) // Se l'avvio del thread � andato a buon fine
											{
												wait_code = SYSWaitForThread( thread->Thread ); // Aspetto la fine del thread
												if ( wait_code == 0 ) // Se il thread � terminato
												{
													SYSDeleteThread( thread->Thread ); // Elimino la struttura thread di sistema
													thread->Thread = NULL; // Annullo il ptr al thread di sistema appena eliminato
													if ( thread->FinishCode == SPV_COMPROT_THREAD_OK ) // Se il thread � stato eseguito con successo
													{
														ret_code = SPV_COMPROT_THREAD_OK;
														payload = thread->FirstPayload; // Restituisco il payload raccolto dal thread
													}
													else // Se qualche step del thread � fallito.
													{
														ret_code = SPV_COMPROT_THREAD_FAILURE;
														// *** DA VERIFICARE ***
														// --- Cancello una eventuale lista di payload ricevuti ---
														SPVDeletePayloadList( thread->FirstPayload );
														/*
														// --- Cancello il payload eventualmente ricevuto ---
														SPVDeletePayload( thread->FirstPayload );
														*/
														thread->FirstPayload = NULL;
														payload = NULL;
													}
												}
												else
												{
													ret_code = SPV_COMPROT_WAIT_THREAD_FAILURE;
												}
											}
											else
											{
												ret_code = SPV_COMPROT_START_THREAD_FAILURE;
											}
										}
										catch(...)
										{
											// Gestire l'eccezione
										}
										// --- Gestione della non-persistenza della porta ---
										try
										{
											if ( !thread->Port->Persistent )
											{
												SYSClosePort( thread->Port );
											}
										}
										catch(...)
										{

										}
										// --- Se il comando � bloccante ---
										if ( commandp->Blocking )
										{
											//LeaveCriticalSection( &thread->Port->CriticalSection );
											SYSUnLock(&thread->Port->GeneralLock);
										}
										/*
										// --- Entro nella sezione critica ---
										SPVEnterThreadData( );
										// --- Elimino il thread di protocollo che � appena terminato ---
										SPVDelComProtThread( thread );
										// --- Esco dalla sezione critica ---
										SPVLeaveThreadData( );
										*/
									}
									else
									{
										ret_code = SPV_COMPROT_INVALID_COMMAND;
									}
								}
								else
								{
									ret_code = SPV_COMPROT_INVALID_COMMAND;
								}
							}
							else
							{
								ret_code = SPV_COMPROT_INVALID_COMPROT_TYPE;
							}
						}
						else
						{
							ret_code = SPV_COMPROT_INVALID_THREAD;
						}
					}
					else
					{
						ret_code = SPV_COMPROT_NEW_THREAD_FAILURE;
					}
				}
				else
				{
					// --- Esco dalla sezione critica ---
					SPVLeaveThreadData( );
					ret_code = SPV_COMPROT_THREAD_LIST_FULL;
				}
			}
			else
			{
				ret_code = SPV_COMPROT_INVALID_COMMAND;
			}
		}
		else
		{
			ret_code = SPV_COMPROT_INVALID_DEVICE;
		}
	}
	catch (...)
	{

	}

	*retcode = ret_code;
	return payload;
}

//==============================================================================
/// Funzione per terminare l'esecuzione di un comando di protocollo
///
/// \date [11.01.2007]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int SPVKillCommand( SPV_COMPROT_THREAD * thread )
{
  int ret_code = SPV_COMPROT_NO_ERROR;
  int fun_code = SPV_COMPROT_NO_ERROR;

  // --- Entro nella sezione critica ---
	SPVEnterThreadData( );

	try
	{
	  if ( SPVValidComProtThread( thread ) )
	  {
	  	if ( thread->Active || ( !thread->Active && thread->StepStart == 0 ) )
	  	{
	  		// --- Cancello un'eventuale struttura thread ---
	  		if ( thread->Thread != NULL )
	  		{
	  			if ( !thread->Thread->Terminated )
	  			{
	  				SYSTerminateThread( thread->Thread );
	  			}
	  		}
	  		// --- Marchio il thread come non attivo e killed ---
	  		if ( fun_code == SPV_COMPROT_NO_ERROR )
	  		{
	  			thread->Active      = false;
	  			thread->Waiting     = false;
	  			thread->FinishCode  = SPV_COMPROT_THREAD_KILLED;
	  		}
	  	}
			///SPVUpdateCommandStatus( SPVGetComProtThreadPos( thread ) );
	  }
	  else
	  {
		ret_code = SPV_COMPROT_INVALID_THREAD;
		}
	}
	catch(...)
	{
		ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveThreadData( );

  return ret_code;
}

//==============================================================================
/// Funzione di attivazione del thread di controllo comandi di protocollo
///
/// \date [21.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
unsigned long __stdcall SPVCommandCheckThread( void * parameter )
{
	unsigned long         ret_code  	= SPV_COMPROT_NO_ERROR  ; // Codice di ritorno
	int                   fun_code 		= SPV_COMPROT_NO_ERROR  ;
	SPV_COMPROT_THREAD  * cpthread  	= NULL					; // Ptr a struttura thread
	SYS_THREAD          * thread        = NULL 					; // Puntatore al thread
	char          		* thread_name 	= NULL  				; // Stringa del nome del thread

	thread = (SYS_THREAD*) parameter;

	// --- Controllo che il ptr alla struttura thread sia valido ---
	if ( SYSValidThread( thread ) )
	{
		// assegno il nome al thread
		try
		{
			thread_name = ULAddText(thread_name,"CommandCheck");
		}
		catch(...)
		{
			fun_code = SPV_COMPROT_FUNCTION_EXCEPTION;
		}

		if (fun_code == SPV_COMPROT_NO_ERROR)
		{
			fun_code = SYSSetThreadName(thread,thread_name);
		}
		/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */

		// --- Ciclo di vita del thread di controllo comandi ---
		while ( !thread->Terminated ) // Ciclo del thread di protocollo
		{
			// --- Entro nella sezione critica ---
			SPVEnterThreadData( );
			try
			{
				// --- Ciclo sulla lista dei thread di protocollo ---
				for ( int t = 0; t < SPV_COMPROT_MAX_THREAD; t++ )
				{
					cpthread = &SPVComProtThreadList[t];
					if ( cpthread != NULL )
					{
						if ( cpthread->Active == true )
						{
						/* TODO 0 -oEnrico  : Completare la funzione */
						}
						else
						{
							if ( cpthread->Waiting == false )
							{

							}
						}
					}
					else
					{
					/* TODO 1 -oEnrico -cError : Gestire l'errore critico */
					}
				}
			}
			catch(...)
			{
				ret_code = SPV_COMPROT_FUNCTION_EXCEPTION;
			}
			// --- Esco dalla sezione critica ---
			SPVLeaveThreadData( );
			// --- Tempo di ritardo ---
			Sleep( 2000 );

			// Aggiornamento delle informazioni di debug del thread
			SYSUpdateThreadDebugInfo(thread);
		}
		// --- Termine del thread ---
	}

	return ret_code;
}

//==============================================================================
// Funzione per ottenere il nome di un comando
///
/// In caso di errore restituisce Null.
///
/// \date [21.09.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetCommandName ( SPV_COMPROT_THREAD * thread )
{
	SPV_COMPROT_TYPE * type  		; // Tipologia di protocollo di comunicazione
	int commandId      		 		; // ID del comando di protocollo da eseguire
	SPV_COMPROT_COMMAND * command	;
	char * commandName = NULL		;

	try
	{
		if (SPVValidComProtThread(thread))
		{
			type = thread->Type;
			commandId = thread->Command;
			if ((type != NULL) && (commandId < type->CommandCount))
			{
				command = &type->CommandList[commandId];
				if (command != NULL)
				{
					commandName = command->Name;
				}
			}
		}
	}
	catch(...)
	{
		commandName = NULL;
	}
	return commandName;
}

//==============================================================================
/// Funzione per ricavare il tipo di porta
///
/// Dato il ptr all'elemento XML di definizione di una porta di comunicazione,
/// estrae la stringa che definisce il tipo di porta e ne ricava il rispettivo
/// codice di tipo porta e lo restituisce.
///
/// In caso di errore restituisce -1.
///
/// \date [02.10.2014]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int SPVGetPortType( XML_ELEMENT * p_element )
{
  char  * type_cp   = NULL                        ; // Ptr alla stringa del tipo porta
  int     type      = SYS_KERNEL_PORT_TYPE_UNKNOWN; // Tipo di porta

  if ( p_element != NULL ) // Controllo il ptr all'elemento descrittore della porta
  {
    type_cp = XMLGetValue( p_element, "type" );
    if ( type_cp != NULL ) // Se trovo un parametro di tipo nella definizione
    {
      // --- Caso porta SERIALE ---
	  if ( strcmpi( type_cp, "serial" ) == 0 || strcmpi( type_cp, "RS422" ) == 0 || strcmpi( type_cp, "RS485" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_SERIAL;
	  }
      // --- Caso porta SOCKET ---
      if ( strcmpi( type_cp, "socket" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_SOCKET;
      }
	  // --- Caso porta MODEM ---
      if ( strcmpi( type_cp, "modem" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_MODEM;
      }
      // --- Caso porta RS232 ---
      if ( strcmpi( type_cp, "RS232" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_RS232;
      }
      // --- Caso porta STLC1000 RS485 ---
      if ( strcmpi( type_cp, "STLC1000_RS485" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_STLC1000_RS485;
      }
      // --- Caso porta STLC1000 RS422 ---
      if ( strcmpi( type_cp, "STLC1000_RS422" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_STLC1000_RS422;
      }
      // --- Caso porta TCP Client ---
      if ( strcmpi( type_cp, "TCP_Client" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_TCP_CLIENT;
      }
      // --- Caso porta UDP Client ---
      if ( strcmpi( type_cp, "UDP_Client" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_UDP_CLIENT;
      }
      // --- Caso porta TCP Server ---
      if ( strcmpi( type_cp, "TCP_Server" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_TCP_SERVER;
      }
      // --- Caso porta UDP Server ---
      if ( strcmpi( type_cp, "UDP_Server" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_UDP_SERVER;
      }
      // --- Caso porta SNMP Manager (via SSP) ---
      if ( strcmpi( type_cp, "SNMP_Manager" ) == 0 )
      {
        type = SYS_KERNEL_PORT_TYPE_SNMP_MANAGER;
			}
			// --- Caso porta XML ---
			if ( strcmpi( type_cp, "XML_Port" ) == 0 )
			{
				type = SYS_KERNEL_PORT_TYPE_XML_PORT;
			}
    }
    else
    {
      type = -1;
    }
  }
  else
  {
    type = -1;
  }

  return type;
}

//==============================================================================
/// Funzione per ricavare il nome di un tipo di porta
///
/// In caso di errore restituisce NULL.
///
/// \date [31.01.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
char * SPVGetPortTypeName( int port_type )
{
  char    type_temp[100]    ;
  char  * type      = NULL  ;
  int     type_len  = 0     ;

  switch ( port_type )
  {
    case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_SERIAL );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_SERIAL );
    break;
    case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_STLC1000_RS485 );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_STLC1000_RS485 );
    break;
    case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_STLC1000_RS422 );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_STLC1000_RS422 );
    break;
    case SYS_KERNEL_PORT_TYPE_SERIAL:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_SERIAL );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_SERIAL );
    break;
    case SYS_KERNEL_PORT_TYPE_SOCKET:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_SOCKET );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_SOCKET );
    break;
    case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_TCP_CLIENT );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_TCP_CLIENT );
    break;
    case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_UDP_CLIENT );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_UDP_CLIENT );
    break;
    case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_TCP_SERVER );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_TCP_SERVER );
    break;
    case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_UDP_SERVER );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_UDP_SERVER );
    break;
    case SYS_KERNEL_PORT_TYPE_MODEM:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_MODEM );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_MODEM );
    break;
    case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
			strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_SNMP_MANAGER );
			type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_SNMP_MANAGER );
		break;
		case SYS_KERNEL_PORT_TYPE_XML_PORT:
			strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_XML_PORT );
			type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_XML_PORT );
		break;
    default:
      strcpy( type_temp, SYS_KERNEL_PORT_TYPE_NAME_UNKNOWN );
      type_len = strlen( SYS_KERNEL_PORT_TYPE_NAME_UNKNOWN );
  }

  if ( type_len > 0 )
  {
    type = (char*)malloc( sizeof(char)*(type_len+1) ); // -MALLOC
    strcpy( type, type_temp );
  }

  return type;
}

//==============================================================================
/// Funzione per caricare tutte le porte di comunicazione
///
/// Dato il ptr all'elemtno XML di definizione delle porte di comunicazione del
/// sistema di supervisione, crea tutte le strutture di descrizione delle porte
/// e ne setta i valori secondo i parametri definiti.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [03.07.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVLoadPorts( XML_ELEMENT * p_element )
{
  int                 ret_code  = 0   ; // Codice di ritorno
  XML_ELEMENT       * i_element = NULL; // Ptr all'elemento XML di def di una porta
  SYS_PORT          * port      = NULL; // Ptr ad una struttura porta
  int                 port_num  = 0   ; // Numero di porte definite
  int                 port_type = 0   ; // Tipo di porta
  int                 counter   = 0   ; // Contatore delle porte

  if ( p_element != NULL ) // Controllo il ptr alla definizione porte di com.
  {
    port_num = XMLGetChildCount( p_element, false ); // Guardo quanti child contiene
    if ( port_num > 0 && port_num <= SPV_COMPROT_MAX_PORT ) // Se il numero di child � valido
    {
      i_element = p_element->child; // Parto dal primo child
      while ( i_element != NULL ) // Ciclo sui child
      {
        port_type = SPVGetPortType( i_element );
        if ( port_type != SYS_KERNEL_PORT_TYPE_UNKNOWN && port_type != -1 )
        {
          /* Alloco una nuova struttura porta di comunicazione */
          port = SYSNewPort( port_type );
          port->Def = i_element; // Associo la definizione XML alla porta
          if ( port != NULL ) // Se l'allocazione � andata a buon fine
          {
            /* Estraggo dalla definizione XML i parametri della porta */
            ret_code = SYSExtractPortParams( port );
            if ( ret_code == 0 )
            {
              /* Imposto i parametri della porta */
              ///ret_code = SYSPortSetParams( port );
              if ( ret_code == 0 )
              {
                /* Aggiungo la nuova porta alla lista globale */
                SPVPortList[counter].Port = port;
                SPVPortList[counter].ID   = XMLGetID( i_element );
                counter++;
                i_element = i_element->next; // Passo all'elemento successivo (se esiste)
              }
              else
              {
                SYSDeletePort( port ); // Cancello la porta appena creata
                i_element = NULL; // Fine del ciclo
                ret_code = SPV_COMPROT_SET_FAILURE;
              }
            }
            else
            {
              SYSDeletePort( port ); // Cancello la porta appena creata
              i_element = NULL; // Fine del ciclo
              ret_code = SPV_COMPROT_EXTRACT_FAILURE;
            }
          }
          else
          {
            i_element = NULL; // Fine del ciclo
            ret_code = SPV_COMPROT_INVALID_ALLOCATION;
          }
        }
        else
        {
          i_element = NULL; // Fine del ciclo
          ret_code = SPV_COMPROT_INVALID_PORT_TYPE;
        }
      }
      SPVPortCount = counter;
      if ( ret_code == 0 && counter != port_num ) // Controllo se il contatore coincide con il numero child
      {
        ret_code = SPV_COMPROT_INVALID_COUNT;
      }
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_DEFINITION;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_ELEMENT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare tutte le strutture delle porte di comunicazione
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVFreePorts( void )
{
  int         ret_code = SPV_COMPROT_NO_ERROR; // Codice di ritorno
  SYS_PORT  * port     = NULL; // Ptr ad una struttura porta

  for ( int p = 0; p < SPVPortCount; p++ )
  {
    port = SPVPortList[p].Port; // Recupero il ptr alla struttura porta
    ret_code = SYSDeletePort( port );
  }
  memset( SPVPortList, 0, sizeof(SPV_COMPROT_PORT)*SPV_COMPROT_MAX_PORT );
  SPVPortCount = 0;

  return ret_code;
}

//==============================================================================
/// Funzione per assegnare una porta ad una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVAssignPort( SPV_DEVICE * device )
{
  int           ret_code  = 0   ; // Codice di ritorno
  XML_ELEMENT * device_e  = NULL; // Elemento XML di definizione periferica
  int           port_id   = 0   ; // ID della porta di comunicazione
  SYS_PORT    * port      = NULL; // Ptr a struttura porta di comunicazione

  if ( device != NULL )
  {
    device_e  = device->Def;
    port_id   = device->SupCfg.PortID;
	port      = SPVPortList[port_id].Port;
    if ( port != NULL )
    {
      device->SupCfg.Port = port;
    }
    else
    {
      ret_code = SPV_COMPROT_INVALID_PORT_ID;
    }
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per assegnare le porte alle periferiche del sistema
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAssignSystemPorts( SPV_SYSTEM * system )
{
  int ret_code = 0;

  if ( system != NULL )
  {
    SPVDoOnDevices( system, SPVAssignPort );
  }
  else
  {
    ret_code = SPV_COMPROT_INVALID_SYSTEM;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aprire tutte le porte del sistema
///
/// \date [30.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVOpenPorts( void )
{
  int                 ret_code    = 0   ;
  int                 fun_code    = 0   ;
  SPV_COMPROT_PORT  * port        = NULL;
  bool                hw_error    = false;
  bool                open_error  = false;

  if ( SPVPortList != NULL && SPVPortCount > 0 )
  {
    for ( int p = 0; p < SPVPortCount; p++ )
    {
      port = &SPVPortList[p];
      fun_code = SYSSetPortHardware( port->Port );
      if ( fun_code != 0 )
      {
        hw_error = true;
      }
	}
	for ( int p = 0; p < SPVPortCount; p++ )
    {
      port = &SPVPortList[p];
	  fun_code = SYSOpenPort( port->Port );
      if ( fun_code != 0 )
      {
        open_error = true;
      }
    }
    if ( open_error == true )
    {
      ret_code = SPV_COMPROT_PORT_OPEN_FAILURE;
    }
    if ( hw_error == true )
    {
      ret_code = SPV_COMPROT_PORT_HW_SET_FAILURE;
    }
  }
  else
  {
	ret_code = SPV_COMPROT_INVALID_PORT_LIST;
  }

  return ret_code;
}

int	SPVSetComProtLogFunction( void * ptf )
{
	SPVComProtPrintLog = (_SPV_COMPROT_LOG_FUNCTION_PTR)ptf;
}

//==============================================================================
/// Funzione per ottenere l'UID della porta dal DB
///
/// \date [08.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVGetDBPortID( SPV_COMPROT_PORT * port )
{
	char * PortUID = NULL;

	SPVDBMCheckForPort( (void*)port, &PortUID );

	return PortUID;
}

//==============================================================================
/// Funzione per marchiare nel DB come rimosse tutte le porte di comunicazione
///
/// \date [08.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDBRemoveAllPorts( void )
{
	int ret_code = SPV_COMPROT_NO_ERROR;

	ret_code = SPVDBMRemoveAllPorts();

	return ret_code;
}

//==============================================================================
/// Funzione per salvare tutte le porte nel DB
///
/// \date [08.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDBSavePorts( char * SrvID )
{
	int 				ret_code 	= SPV_COMPROT_NO_ERROR;
	int                 fun_code 	= 0   ;
	SPV_COMPROT_PORT  * port     	= NULL;
	char *				PortUID		= NULL;

	if ( SPVPortList != NULL && SPVPortCount > 0 )
	{
		for ( int p = 0; p < SPVPortCount; p++ )
		{
			try
			{
				port = &SPVPortList[p];
				fun_code = SPVDBMSavePort( (void*)port, SrvID, &PortUID ); // -MALLOC
				if ( PortUID != NULL ) free( PortUID ); // -FREE
			}
			catch(...){

			}
		}
	}
	else
	{
		ret_code = SPV_COMPROT_INVALID_PORT_LIST;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare dal DB le porte marchiate come rimosse
///
/// \date [08.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDBDeleteRemovedPorts( void )
{
	int ret_code = SPV_COMPROT_NO_ERROR;

	ret_code = SPVDBMDeleteRemovedPorts();

	return ret_code;
}

/*
//==============================================================================
/// Funzione per riportare un evento applicativo nell'EventLog di Windows
///
/// \date [24.11.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVReportApplicEvent( SPV_PROCEDURE * procedure, int sevlevel, int eventID, char * des, char * code )
{
  int                 ret_code    = 0   ;
  char              * msg         = NULL;
  int                 msg_len     = 0   ;
  int                 params      = 0   ;

  if ( procedure != NULL )
  {
    // --- Preparo il messaggio ---
    msg     = SPVAddText( msg, "\nEvento: " );
    msg     = SPVAddText( msg, des );
    msg     = SPVAddText( msg, "\nModulo: " );
    msg     = SPVAddText( msg, code );
    msg_len = strlen( msg );
    // --- Preparo i parametri dell'evento ---
    params = SPVBuildDevEventParams( SPV_APPLIC_CATEGORY_APPLIC_EVENT, sevlevel, eventID );
    // --- Scrivo l'evento nell'EventLog di Windows ---
		SPVPrintEventLog( SPVApplicativeEventLog->ID, msg, msg_len, params );
    free( msg ); // -FREE
  }
  else
  {
    ret_code = SPV_APPLIC_INVALID_PROCEDURE;
  }

  return ret_code;
}
*/

//==============================================================================
// Telefin Supervisor Topographic Module 1.0
//------------------------------------------------------------------------------
// Modulo Topografico (SPVTopography.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.11 (23.04.2007 -> 31.07.2007)
//
// Copyright: 2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland DBS 2006
// Autore:    Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVTopography.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVTopography.h
//==============================================================================


#pragma hdrstop

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "SPVTopography.h"
#include "SPVDBManager.h"
#include "SPVDBInterface.h"
#include "XMLInterface.h"
#include "cnv_lib.h"
#include "UtilityLibrary.h"

#pragma package(smart_init)

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
static SPV_TPG_Topography * SPVTopography;

//==============================================================================
/// Metodo costruttore oggetto topografico generico
///
/// \date [30.07.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_TPG_Object::SPV_TPG_Object( void )
{
	try
	{
		// --- Azzero le proprieta' ---
		this->ID		= NULL;
		this->XMLID		= 0;
		this->Parent	= NULL;
		this->Name		= NULL;
		this->Note		= NULL;
		this->Type		= NULL;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo costruttore oggetto topografico generico con parametri
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SPV_TPG_Object::SPV_TPG_Object( char* name, char* note, void* parent )
{
	try
	{
		// --- Azzero le proprieta' ---
		this->ID		= NULL;
		this->XMLID		= 0;
		this->Name 		= NULL;
		this->Note		= NULL;
		this->Parent	= NULL;
		// --- Imposto le proprieta' ---
		this->SetName( name );
		this->SetNote( note );
		this->SetParent( parent );
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo distruttore oggetto topografico generico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Object::~SPV_TPG_Object( void )
{
	try
	{
		this->Parent	= NULL;
		if ( this->ID 	!= NULL ) free( this->ID 	);
		if ( this->Name	!= NULL ) free( this->Name 	);
		if ( this->Note	!= NULL ) free( this->Note 	);
		if ( this->Type != NULL ) free( this->Type	);
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per impostare il parent di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPV_TPG_Object::SetParent( void* parent )
{
	try
	{
		this->Parent = parent;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per impostare l'ID di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPV_TPG_Object::SetID( char* id )
{
	try
	{
		this->ID = ULStrCpy( this->ID, id );
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per ottenere l'ID di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char* SPV_TPG_Object::GetID( void )
{
	char* ID = NULL;

	try
	{
		ID = this->ID;
	}
	catch(...)
	{

	}

	return ID;
}

//==============================================================================
/// Metodo per impostare l'ID XML di un oggetto topografico
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPV_TPG_Object::SetXMLID( int id )
{
	try
	{
		this->XMLID = id;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per ottenere l'ID XML di un oggetto topografico
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPV_TPG_Object::GetXMLID( void )
{
	int XMLID = 0;

	try
	{
		XMLID = this->XMLID;
	}
	catch(...)
	{

	}

	return XMLID;
}

//==============================================================================
/// Metodo per impostare il nome di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPV_TPG_Object::SetName( char* name )
{
	try
	{
		this->Name = ULStrCpy( this->Name, name );
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per ottenere il nome di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char* SPV_TPG_Object::GetName( void )
{
	char* Name = NULL;

	try
	{
		Name = this->Name;
	}
	catch(...)
	{

	}

	return Name;
}

//==============================================================================
/// Metodo per impostare le note di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPV_TPG_Object::SetNote( char* note )
{
	try
	{
		this->Note = ULStrCpy( this->Note, note );
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per ottenere le note di un oggetto topografico
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char* SPV_TPG_Object::GetNote( void )
{
	char* Note = NULL;

	try
	{
		Note = this->Note;
	}
	catch(...)
	{

	}

	return Note;
}

//==============================================================================
/// Metodo per impostare il tipo di un oggetto topografico
///
/// \date [03.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPV_TPG_Object::SetType( char * type )
{
	try
	{
		this->Type = ULStrCpy( this->Type, type );
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per ottenere il tipo di un oggetto topografico
///
/// \date [03.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPV_TPG_Object::GetType( void )
{
	char* type = NULL;

	try
	{
		type = this->Type;
	}
	catch(...)
	{

	}

	return type;
}

//==============================================================================
/// Metodo per ottenere il parent (building) di un armadio (rack)
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void * SPV_TPG_Rack::GetParent( void )
{
	void * parent = NULL;

	try
	{
		parent = this->Parent;
	}
	catch(...)
	{
		parent = NULL;
	}

	return parent;
}

//==============================================================================
/// Metodo per caricare l'armadio da struttura XML
///
/// \date [04.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	SPV_TPG_Rack::LoadFromXML( void )
{
	int 			ret_code	= SPV_TPG_NO_ERROR;
	XML_ELEMENT *	main_ele	= (XML_ELEMENT*)this->XMLElement;

	try
	{
		this->SetName( XMLGetValue( main_ele, "name" ) );
		this->SetType( XMLGetValue( main_ele, "type" ) );
		this->SetNote( XMLGetValue( main_ele, "note" ) );
	}
	catch(...)
	{
        ret_code = SPV_TPG_FUNCTION_EXCEPTION;
    }

	return ret_code;
}

//==============================================================================
/// Metodo per salvare nel DB le informazioni dell'armadio
///
/// \date [03.05.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SPV_TPG_Rack::DBSave( void )
{
	int		ret_code 	= SPV_TPG_NO_ERROR;
	int 	count 		= 0;
	char * 	RackUID		= NULL;

	try
	{
		ret_code = SPVDBMSaveRack( (void*)this, &RackUID ); // -MALLOC
		if ( ret_code == SPV_TPG_NO_ERROR ) {
			this->SetID( RackUID );
		}
		try
		{
			if ( RackUID != NULL ) free( RackUID ); // -FREE
		}
		catch(...)
		{

        }
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
    }

	return ret_code;
}

//==============================================================================
/// Metodo per leggere dal DB le informazioni dell'armadio
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Rack::DBRead( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	ret_code = SPV_TPG_NOT_IMPLEMENTED;

	return ret_code;
}

//==============================================================================
/// Metodo distruttore armadio
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Rack::~SPV_TPG_Rack( void )
{
	try
	{
		this->Parent	= NULL;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per ottenere il parent (station) di un edificio (building)
///
/// \date [23.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void * SPV_TPG_Building::GetParent( void )
{
	void * station = NULL;

	try
	{
		station = this->Parent;
	}
	catch(...)
	{

	}

	return station;
}

//==============================================================================
/// Metodo per caricare l'edificio da struttura XML
///
/// \date [30.07.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SPV_TPG_Building::LoadFromXML( void )
{
	SPV_TPG_Rack * 	rack 		= NULL;
	XML_ELEMENT *	main_ele	= (XML_ELEMENT*)this->XMLElement;
	XML_ELEMENT	* 	element 	= NULL;
	int				count		= 0;

	this->RackList	= NULL;
	this->RackCount	= 0;

	this->SetName( XMLGetValue( main_ele, "name" ) );
	this->SetNote( XMLGetValue( main_ele, "note" ) );

	count = XMLGetChildCount( main_ele, false );
	if ( count > 0 ) {
		this->RackList = (SPV_TPG_Rack**)malloc(sizeof(SPV_TPG_Rack*) * count);
		for ( int r = 0; r < count; r++ ) {
			element = XMLGetNext( main_ele->child, "location", r );
			if ( element != NULL ) {
				rack = new SPV_TPG_Rack;
				rack->XMLElement = element;
				rack->SetXMLID( r );
				rack->LoadFromXML();
				this->AddRack( rack );
			}
		}
	}
}

//==============================================================================
/// Metodo per aggiungere un armadio
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Building::AddRack( SPV_TPG_Rack * rack )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	if ( rack != NULL ) {
		rack->SetParent( (void*)this );
		this->RackList[this->RackCount] = rack;
		this->RackCount++;
	}
	else
	{
        ret_code = SPV_TPG_INVALID_RACK;
    }

	return ret_code;
}

//==============================================================================
/// Metodo per ottenere il numero di armadi
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Building::GetRackCount( void )
{
	int	count = 0;

	count = this->RackCount;

	return count;
}

//==============================================================================
/// Metodo per ottenere un armadio
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Rack * SPV_TPG_Building::GetRack( int xmlid )
{
	SPV_TPG_Rack * rack = NULL;

	if ( xmlid >= 0 && xmlid < this->RackCount ) {
		rack = this->RackList[xmlid];
	}

	return rack;
}

//==============================================================================
/// Metodo per salvare nel DB le informazioni dell'edificio
///
/// \date [05.05.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SPV_TPG_Building::DBSave( void )
{
	int					ret_code 	= SPV_TPG_NO_ERROR;
	int 				count 		= 0;
	SPV_TPG_Rack * 		rack 		= NULL;
	char *				BuildingUID	= NULL;

	try
	{
		ret_code = SPVDBMSaveBuilding( (void*)this, &BuildingUID ); // -MALLOC
		if ( ret_code == SPV_TPG_NO_ERROR ) {
			try
			{
				this->SetID( BuildingUID );
				try
				{
					if ( BuildingUID != NULL ) free( BuildingUID ); // -FREE
				}
				catch(...)
				{

				}
				count = this->GetRackCount();
				for ( int r = 0 ; r < count; r++ ) {
					try
					{
						rack = this->GetRack( r );
						if ( rack != NULL ) {
							rack->DBSave();
						}
					}
					catch(...)
					{

					}
				}
			}
			catch(...)
			{

			}
		}
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Metodo per leggere dal DB le informazioni dell'edificio
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Building::DBRead( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	ret_code = SPV_TPG_NOT_IMPLEMENTED;

	return ret_code;
}

//==============================================================================
/// Metodo distruttore edificio
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Building::~SPV_TPG_Building( void )
{
	SPV_TPG_Rack * rack = NULL;

	try
	{
		this->Parent	= NULL;
		for ( int r = 0; r < this->RackCount; r++ )
		{
			rack = this->RackList[r];
			if ( rack != NULL ) {
				delete rack;
			}
		}
		free( this->RackList );
		this->RackCount = 0;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per caricare la stazione da struttura XML
///
/// \date [30.07.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SPV_TPG_Station::LoadFromXML( void )
{
	SPV_TPG_Building * 	building 	= NULL;
	XML_ELEMENT *		main_ele	= (XML_ELEMENT*)this->XMLElement;
	XML_ELEMENT	* 		element 	= NULL;
	int					count		= 0;

	this->BuildingList	= NULL;
	this->BuildingCount	= 0;

	this->SetName( XMLGetValue( main_ele, "name" ) );
	this->SetNote( XMLGetValue( main_ele, "note" ) );

	count = XMLGetChildCount( main_ele, false );
	if ( count > 0 ) {
		this->BuildingList = (SPV_TPG_Building**)malloc(sizeof(SPV_TPG_Building*) * count );
		for ( int b = 0; b < count; b++ ) {
			element = XMLGetNext( main_ele->child, "building", b );
			if ( element != NULL ) {
				building = new SPV_TPG_Building;
				building->XMLElement = element;
				building->SetXMLID( b );
				building->LoadFromXML();
				this->AddBuilding( building );
			}
		}
	}
}

//==============================================================================
/// Metodo per aggiungere un edificio
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Station::AddBuilding( SPV_TPG_Building * building )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	if ( building != NULL ) {
		building->SetParent( (void*)this );
		this->BuildingList[this->BuildingCount] = building;
		this->BuildingCount++;
	}
	else
	{
		ret_code = SPV_TPG_INVALID_BUILDING;
	}

	return ret_code;
}

//==============================================================================
/// Metodo per ottenere il numero di edifici
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Station::GetBuildingCount( void )
{
	int count = 0;

	count = this->BuildingCount;

	return count;
}

//==============================================================================
/// Metodo per ottenere un edificio
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Building * SPV_TPG_Station::GetBuilding( int xmlid )
{
	SPV_TPG_Building * building = NULL;

	building = this->BuildingList[xmlid];

	return building;
}

//==============================================================================
/// Metodo per salvare nel DB le informazioni della stazione
///
/// \date [05.05.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int	SPV_TPG_Station::DBSave( void )
{
	int					ret_code 	= SPV_TPG_NO_ERROR;
	int 				count 		= 0;
	SPV_TPG_Building *	building 	= NULL;
	char * 				StationUID	= NULL;

	try
	{
		ret_code = SPVDBMSaveStation( (void*)this, &StationUID ); // -MALLOC
		if ( ret_code == SPV_TPG_NO_ERROR ) {
			try
			{
				this->SetID( StationUID );
				try
				{
					if ( StationUID != NULL ) free( StationUID ); // -FREE
				}
				catch(...)
				{

				}
				count = this->GetBuildingCount();
				for ( int b = 0 ; b < count; b++ ) {
					try
					{
						building = this->GetBuilding( b );
						if ( building != NULL ) {
							building->DBSave();
						}
					}
					catch(...)
					{

					}
				}
			}
			catch(...)
			{

			}
		}
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
    }

	return ret_code;
}

//==============================================================================
/// Metodo per leggere dal DB le informazioni della stazione
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Station::DBRead( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	ret_code = SPV_TPG_NOT_IMPLEMENTED;

	return ret_code;
}

//==============================================================================
/// Metodo distruttore stazione
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Station::~SPV_TPG_Station( void )
{
	SPV_TPG_Building * building = NULL;

	try
	{
		this->Parent	= NULL;
		for ( int r = 0; r < this->BuildingCount; r++ )
		{
			building = this->BuildingList[r];
			if ( building != NULL ) {
				delete building;
			}
		}
		free( this->BuildingList );
		this->BuildingCount = 0;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Metodo per impostare il puntatore all'elemento XML
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Topography::SetXMLElement( void * element )
{
	this->XMLElement = element;
}

//==============================================================================
/// Metodo per ottenere il puntatore all'elemento XML
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void * SPV_TPG_Topography::GetXMLElement( void )
{
	return this->XMLElement;
}

//==============================================================================
/// Metodo per caricare la informazioni tutpografiche da struttura XML
///
/// \date [30.07.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SPV_TPG_Topography::LoadFromXML( void )
{
	SPV_TPG_Station * 	station 	= NULL;
	XML_ELEMENT *		main_ele	= (XML_ELEMENT*)this->XMLElement;
	XML_ELEMENT	* 		element 	= NULL;
	int					count		= 0;

	this->StationList	= NULL;
	this->StationCount	= 0;

	count = XMLGetChildCount( main_ele, false );
	if ( count > 0 ) {
		this->StationList = (SPV_TPG_Station**)malloc(sizeof(SPV_TPG_Station*) * count );
		for ( int s = 0; s < count; s++ ) {
			element = XMLGetNext( main_ele->child, "station", s );
			if ( element != NULL ) {
				station = new SPV_TPG_Station;
				station->XMLElement = element;
				station->SetXMLID( s );
				station->LoadFromXML();
				this->AddStation( station );
			}
		}
	}
}

//==============================================================================
/// Metodo per aggiungere una stazione
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Topography::AddStation( SPV_TPG_Station * station )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	if ( station != NULL ) {
		station->SetParent( (void*)this );
		this->StationList[this->StationCount] = station;
		this->StationCount++;
	}
	else
	{
		ret_code = SPV_TPG_INVALID_STATION;
	}

	return ret_code;
}

//==============================================================================
/// Metodo per ottenere una stazione
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Station * SPV_TPG_Topography::GetStation( int xmlid )
{
	SPV_TPG_Station * station = NULL;

	station = this->StationList[xmlid];

	return station;
}

//==============================================================================
/// Metodo per ottenere il numero di stazioni
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPV_TPG_Topography::GetStationCount( void )
{
	return this->StationCount;
}

//==============================================================================
/// Metodo per salvare nel DB le informazioni topografiche
///
/// \date [05.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	SPV_TPG_Topography::DBSave( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;
	int count = 0;
	SPV_TPG_Station * station = NULL;

	try
	{
		count = this->GetStationCount();
		for ( int s = 0 ; s < count; s++ ) {
			try
			{
				station = this->GetStation( s );
				if ( station != NULL ) {
					station->DBSave();
				}
			}
			catch(...)
			{

			}
		}
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
    }

	return ret_code;
}

//==============================================================================
/// Metodo per recuperare un edificio
///
/// \date [06.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Building * SPV_TPG_Topography::GetBuilding( int station_xmlid, int building_xmlid )
{
	SPV_TPG_Station * 	station 	= NULL;
	SPV_TPG_Building * 	building 	= NULL;

	try
	{
		if ( SPVTopography != NULL ) {
			station = SPVTopography->GetStation( station_xmlid );
			if ( station != NULL ) {
				building = station->GetBuilding( building_xmlid );
			}
		}
		else
		{
		}
	}
	catch(...)
	{
		building = NULL;
	}

	return building;
}

//==============================================================================
/// Metodo per recuperare un armadio
///
/// \date [07.05.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_TPG_Rack * SPV_TPG_Topography::GetRack( int station_xmlid, int building_xmlid, int rack_xmlid )
{
	SPV_TPG_Station * 	station 	= NULL;
	SPV_TPG_Building * 	building 	= NULL;
	SPV_TPG_Rack * 		rack 		= NULL;

	try
	{
		station = this->GetStation( station_xmlid );
		if ( station != NULL ) {
			building = station->GetBuilding( building_xmlid );
			if ( building != NULL ) {
				rack = building->GetRack( rack_xmlid );
			}
		}
	}
	catch(...)
	{
		rack = NULL;
	}

	return rack;
}

//==============================================================================
/// Metodo costruttore
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Topography::SPV_TPG_Topography( void )
{
	// --- Inizializzazione attributi ---
	this->XMLElement 	= NULL;
	this->StationCount 	= 0;
	this->StationList 	= NULL;
}

//==============================================================================
/// Metodo distruttore
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Topography::~SPV_TPG_Topography( void )
{
	SPV_TPG_Station * station = NULL;

	try
	{
		for ( int s = 0; s < this->StationCount; s++ )
		{
		station = this->StationList[s];
		if ( station != NULL ) {
				delete station;
			}
		}
		free( this->StationList );
		this->StationCount = 0;
	}
	catch(...)
	{

	}
}

//==============================================================================
/// Funzione per caricare la topografia del sistema diagnosticato
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SPV_TPG_Topography * SPVLoadTopography( void * XMLElement )
{
	SPV_TPG_Topography * topography = NULL;

	try
	{
	    SPVTopography = new SPV_TPG_Topography;

		SPVTopography->SetXMLElement( XMLElement );

		SPVTopography->LoadFromXML();

		topography = SPVTopography;
	}
	catch(...)
	{

	}

	return topography;
}

//==============================================================================
/// Funzione per cancellare la topografia del sistema diagnosticato
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVClearTopography( SPV_TPG_Topography * topography )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	try
	{
		if ( topography != NULL )
		{
			delete topography;
		}
	}
	catch(...)
	{

	}

	return ret_code;
}

//==============================================================================
/// Funzione per salvare nel DB la topografia del sistema diagnosticato
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBSaveTopography( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	try
	{
		SPVTopography->DBSave();
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per marchiare come rimossa la topografia
///
/// \date [05.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDBRemoveTopography( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	try
	{
		// --- Rimuovo tutte le stazioni ---
		SPVDBMRemoveAllStations( );
		// --- Rimovo tutti gli edifici ---
		SPVDBMRemoveAllBuildings( );
		// --- Rimovo tutti gli armadi ---
		SPVDBMRemoveAllRacks( );
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare la topografia marchiata come rimossa
///
/// \date [05.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SPVDBDeleteRemovedTopography( void )
{
	int	ret_code = SPV_TPG_NO_ERROR;

	try
	{
		// --- Elimino gli aramdi rimossi ---
		SPVDBMDeleteRemovedRacks( );
		// --- Cancello gli edifici rimossi ---
		SPVDBMDeleteRemovedBuildings( );
		// --- Elimino le stazioni rimosse ---
		SPVDBMDeleteRemovedStations( );
	}
	catch(...)
	{
		ret_code = SPV_TPG_FUNCTION_EXCEPTION;
	}

	return ret_code;
}



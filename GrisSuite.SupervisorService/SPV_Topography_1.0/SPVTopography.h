//==============================================================================
// Telefin Supervisor Topographic Module 1.0
//------------------------------------------------------------------------------
// Header Modulo Topografico (SPVTopography.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.11 (23.04.2007 -> 31.07.2007)
//
// Copyright: 2007 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland DBS 2006
// Autore:    Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVTopography.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [23.04.2007]:
// - Prima versione del modulo.
// 0.02 [24.04.2007]:
// - Modificata la classe SPV_TPG_Object.
// 0.03 [26.04.2007]:
// - Modificata la classe SPV_TPG_Object.
// - Modificati i due metodi costruttori di SPV_TPG_Object.
// - Aggiunto SPV_TPG_Object::SetXMLID().
// - Aggiunto SPV_TPG_Object::GetXMLID().
// - Aggiunto SPV_TPG_Rack::DBRead() (non implementato).
// - Aggiunto SPV_TPG_Building::DBRead() (non implementato).
// - Aggiutno SPV_TPG_Station::DBRead() (non implementato).
// 0.04 [02.05.2007]:
// - Modificata la classe SPV_TPG_Object.
// - Aggiunta la classe SPV_TPG_Topography.
// - Aggiunto il metodo SPV_TPG_Topography::GetRack.
// - Aggiunto SPV_TPG_Building::AddRack().
// - Aggiunto SPV_TPG_Building::GetRackCount().
// - Aggiunto SPV_TPG_Building::GetRack().
// - Aggiunto SPV_TPG_Station::AddBuilding().
// - Aggiunto SPV_TPG_Station::GetBuildingCount().
// - Aggiunto SPV_TPG_Station::GetBuilding().
// - Aggiunto SPV_TPG_Topography::SetXMLElement().
// - Aggiunto SPV_TPG_Topography::GetXMLElement().
// - Aggiunto SPV_TPG_Topography::AddStation().
// - Aggiunto SPV_TPG_Topography::GetStation().
// - Aggiunto SPV_TPG_Topography::GetStationCount().
// - Aggiunta SPVLoadTopography().
// - Aggiunta SPVDBSaveTopography().
// 0.05 [03.05.2007]:
// - Aggiunto SPV_TPG_Object::SetType().
// - Aggiunto SPV_TPG_Object::GetType().
// - Modificato SPV_TPG_Rack::DBSave().
// 0.06 [04.05.2007]:
// - Modificato SPV_TPG_Rack::LoadFromXML().
// 0.07 [05.05.2007]:
// - Modificato SPV_TPG_Building::DBSave().
// - Modificato SPV_TPG_Station::DBSave().
// - Modificato SPV_TPG_Topography::DBSave().
// - Aggiunta SPVDBRemoveTopography().
// - Aggiunta SPVDBDeleteRemovedTopography().
// 0.08 [06.05.2007]:
// - Aggiunto SPV_TPG_Topography::GetBuilding().
// 0.09 [07.05.2007]:
// - Modificato SPV_TPG_Building::LoadFromXML().
// - Modificato SPV_TPG_Station::LoadFromXML().
// - Modificato SPV_TPG_Topography::LoadFromXML().
// - Modificato SPV_TPG_Topography::GetRack().
// 0.10 [30.07.2007]:
// - Corretto il metodo costruttore SPV_TPG_Object::SPV_TPG_Object().
// - Corretto il metodo SPV_TPG_Building::LoadFromXML().
// - Corretto il metodo SPV_TPG_Station::LoadFromXML().
// - Corretto il metodo SPV_TPG_Topography::LoadFromXML().
// 0.11 [31.07.2007]:
// - Aggiunta la funzione SPVClearTopography().
//==============================================================================

#ifndef SPVTopographyH
#define SPVTopographyH

#define SPV_TPG_NO_ERROR			0
#define SPV_TPG_GENERAL_ERROR		5500
#define SPV_TPG_INVALID_RACK		5501
#define SPV_TPG_INVALID_BUILDING	5502
#define SPV_TPG_INVALID_STATION		5503
#define SPV_TPG_FUNCTION_EXCEPTION	5504
#define SPV_TPG_INVALID_TOPOGRAPHY	5505
#define SPV_TPG_NOT_IMPLEMENTED		5599


//==============================================================================
/// Classe oggetto topografico generico
///
/// \date [26.04.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
class SPV_TPG_Object
{
	private:	// User declarations
	public:		// User declarations
		// Codice identificativo unico (UID)
		char *				ID;
		// Codice identificativo XML
		int					XMLID;
		// Nome dell'oggetto
		char *				Name;
		// Note riuardo al posizionamento dell'oggetto
		char *				Note;
		// Puntatore all'oggetto parent (tipo diverso a seconda dell'oggetto)
		void *				Parent;
		// Stringa tipo oggetto (opzionale)
		char *				Type;
		// Coordinata X
		int					X;
		// Coordinata Y
		int					Y;
		// Elemento XML
		void *				XMLElement;
		// Metodo per impostare l'ID di un oggetto topografico
		void				SetID		( char* id );
		// Metodo per ottenere l'ID di un oggetto topografico
		char *				GetID		( void );
		// Metodo per impostare l'ID XML di un oggetto topografico
		void				SetXMLID	( int id );
		// Metodo per ottenere l'ID XML di un oggetto topografico
		int					GetXMLID	( void );
		// Metodo per impostare il nome di un oggetto topografico
		void				SetName		( char* name );
		// Metodo per ottenere il nome di un oggetto topografico
		char *				GetName		( void );
		// Metodo per impostare le note di un oggetto topografico
		void				SetNote		( char* note );
		// Metodo per ottenere le note di un oggetto topografico
		char *				GetNote		( void );
		// Metodo per impostare il tipo di un oggetto topografico
		void				SetType		( char* type );
		// Metodo per ottenere il tipo di un oggetto topografico
		char *				GetType		( void );
		// Metodo per impostare il parent di un oggetto topografico
		void				SetParent	( void* parent );
		// Metodo costruttore oggetto topografico generico
							SPV_TPG_Object( void );
		// Metodo costruttore oggetto topografico generico con parametri
							SPV_TPG_Object( char* name, char* note, void* parent );
		// Metodo distruttore oggetto topografico generico
							~SPV_TPG_Object( void );
};

//==============================================================================
/// Classe topografica Armadio
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
class SPV_TPG_Rack : public SPV_TPG_Object
{
	private:	// User declarations
	public:		// User declarations
		// Metodo per ottenere il parent (building) di un armadio (rack)
		void *	GetParent	( void );
		// Metodo per caricare l'armadio da struttura XML
		int		LoadFromXML( void );
		// Metodo per salvare nel DB le informazioni dell'armadio
		int		DBSave( void );
		// Metodo per leggere dal DB le informazioni dell'armadio
		int		DBRead( void );
		// Metodo distruttore armadio
				~SPV_TPG_Rack( void );
};

//==============================================================================
/// Classe topografica Edificio
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
class SPV_TPG_Building : public SPV_TPG_Object
{
	private:	// User declarations
		SPV_TPG_Rack **	RackList; // Lista armadi
		int				RackCount; // Numero armadi
	public:		// User declarations
		// Metodo per ottenere il parent (station) di un edificio (building)
		void *			GetParent	( void );
		// Metodo per caricare l'edificio da struttura XML
		int				LoadFromXML( void );
		// Metodo per aggiungere un armadio
		int             AddRack( SPV_TPG_Rack * rack );
		// Metodo per ottenere il numero di armadi
		int				GetRackCount( void );
		// Metodo per ottenere un armadio
		SPV_TPG_Rack *	GetRack( int xmlid );
		// Metodo per salvare nel DB le informazioni dell'edificio
		int				DBSave( void );
		// Metodo per leggere dal DB le informazioni dell'edificio
		int				DBRead( void );
		// Metodo distruttore edificio
						~SPV_TPG_Building( void );
};

//==============================================================================
/// Classe topografica Stazione
///
/// \date [31.07.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
class SPV_TPG_Station : public SPV_TPG_Object
{
	private:	// User declarations
		SPV_TPG_Building **	BuildingList; // Lista edifici
		int					BuildingCount; // Numero edifici
	public:		// User declarations
		// Metodo per caricare la stazione da struttura XML
		int					LoadFromXML( void );
		// Metodo per aggiungere un edificio
		int					AddBuilding( SPV_TPG_Building * building );
		// Metodo per ottenere il numero di edifici
		int					GetBuildingCount( void );
		// Metodo per ottenere un edificio
		SPV_TPG_Building *	GetBuilding( int xmlid );
		// Metodo per salvare nel DB le informazioni della stazione
		int					DBSave( void );
		// Metodo per leggere dal DB le informazioni della stazione
		int					DBRead( void );
		// Metodo distruttore stazione
							~SPV_TPG_Station( void );
};

//==============================================================================
/// Classe informazioni topografiche
///
/// \date [02.05.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
class SPV_TPG_Topography
{
	private:
		void *				XMLElement; // Elemento XML
		int					StationCount; // Numero stazioni
	public:
		SPV_TPG_Station	** 	StationList; // Lista stazioni
		// Metodo per impostare il puntatore all'elemento XML
		int					SetXMLElement( void * element );
		// Metodo per ottenere il puntatore all'elemento XML
		void *				GetXMLElement( void );
		// Metodo per caricare la informazioni topografiche da struttura XML
		int					LoadFromXML( void );
		// Metodo per aggiungere una stazione
		int					AddStation( SPV_TPG_Station * station );
		// Metodo per ottenere una stazione
		SPV_TPG_Station *	GetStation( int xmlid );
		// Metodo per ottenere il numero di stazioni
		int					GetStationCount( void );
		// Metodo per salvare nel DB le informazioni topografiche
		int					DBSave( void );
		// Metodo per recuperare un edificio
		SPV_TPG_Building *	GetBuilding( int station_xmlid, int building_xmlid );
		// Metodo per recuperare un armadio
		SPV_TPG_Rack *		GetRack( int station_xmlid, int building_xmlid, int rack_xmlid );
		// Metodo costruttore
							SPV_TPG_Topography( void );
		// Metodo distruttore
							~SPV_TPG_Topography( void );
};

// Funzione per caricare la topografia del sistema diagnosticato
SPV_TPG_Topography *	SPVLoadTopography( void * XMLElement );
// Funzione per cancellare la topografia del sistema diagnosticato
int						SPVClearTopography( SPV_TPG_Topography * topography );
// Funzione per salvare nel DB la topografia del sistema diagnosticato
int 					SPVDBSaveTopography( void );
// Funzione per marchiare come rimossa la topografia
int						SPVDBRemoveTopography( void );
// Funzione per eliminare la topografia marchiata come rimossa
int						SPVDBDeleteRemovedTopography( void );

#endif

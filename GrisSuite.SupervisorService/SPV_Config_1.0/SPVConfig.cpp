//==============================================================================
// Telefin Supervisor Configuration Module 1.0
//------------------------------------------------------------------------------
// Modulo di configurazione del software (SPVConfig.cpp)
// Progetto:  	Telefin Supervisor Server 1.0
//
// Revisione:  	0.09 (25.07.2005 -> 11.02.2008)
//
// Copyright: 	2005-2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland DBS 2006
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      	richiede SPVConfig.h
//------------------------------------------------------------------------------
// Version history: vedere SPVConfig.h
//==============================================================================

#pragma hdrstop

//==============================================================================
// Inclusioni
//------------------------------------------------------------------------------
#include "SPVConfig.h"
#include "XMLInterface.h"
#include <windows.h>
#include "SYSKernel.h"
#include "UtilityLibrary.h"
///#include "SPVServerGUI.h"
#include "LogLibrary.h"


#pragma package(smart_init)

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SPV_CFG_SERVER_KEY                  "SYSTEM\\CurrentControlSet\\Services\\StlcSPVService"
#define SPV_CFG_XML_CONFIG_VALUE_NAME       "XMLConfig"
#define SPV_CFG_XML_SYSTEM_VALUE_NAME       "XMLSystem"
#define SPV_CFG_XML_STATUS_VALUE_NAME       "XMLStatus"
#define SPV_CFG_XML_DEFINITIONS_VALUE_NAME  "XMLDefinitions"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
//static  _CRITICAL_SECTION    SPVConfigCriticalSection  ; // Sezione critica della configurazione
static  SYS_LOCK    		SPVConfigLock  			  ; // Lock della configurazione
static  XML_SOURCE        * SPVConfigSource           ; // Sorgente XML delle info di configurazione
static  XML_ELEMENT       * SPVConfigRoot             ; // Ptr all'elemento principale di configurazione
		SPV_CFG_INFO        SPVConfigInfo             ; // Struttura delle info di configurazione


//==============================================================================
/// Funzione per attivare il lock delle variabili globali
///
/// \date [11.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
void SPVEnterConfigData( char * label )
{
	SYSLock( &SPVConfigLock, label);
}

//==============================================================================
/// Funzione per disattivare il lock delle variabili globali
///
/// \date [11.02.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVLeaveConfigData( void )
{
	SYSUnLock( &SPVConfigLock );
}


//==============================================================================
/// Funzione per inizializzare il modulo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void SPVConfigInit( void )
{
	// --- Inizializzo la sezione critica globale per la configurazione ---
	SYSInitLock(&SPVConfigLock,"SPVConfigLock");

	// --- Inizializzo le variabili globali ---
	memset( &SPVConfigInfo, 0, sizeof(SPV_CFG_INFO) );
	SPVConfigSource = NULL;
	SPVConfigRoot   = NULL;
}

/**
 *
 */
char * SPVGetPath( char * append ){
	char * full_path 	= NULL;
	char * exe_path 	= NULL;
	char * command_line 	= NULL;

	int 	path_len 	= 0;
	bool 	logEnabled 	= SYSSetKeyDebugLogEnabled();

	command_line = ULAddText( (char*) GetCommandLine(), "");

	if (logEnabled == true) LLAddLogFile( "GetCommandLine:" );
	if (logEnabled == true) LLAddLogFile( command_line );

	path_len = strlen(command_line);

        if(command_line[path_len - 1] != '"')
        {
	        path_len -= 14;
                if (logEnabled == true) LLAddLogFile( "NON Trovate virgolette nel path restituito da GetCommandLine" );

              	command_line[path_len] 		= 0;
        }
        else
        {
		path_len -= 15;
                if (logEnabled == true) LLAddLogFile( "Trovate virgolette nel path restituito da GetCommandLine" );

	       	command_line[path_len] 		= 0;

                int j = 0;
                for(int k = 0; k < path_len; k++)
                {
                    if(command_line[k] != '"')
                    {
                    	command_line[j++] = command_line[k];
                    }
                }

                command_line[j++] = 0;
        }

	exe_path = ULAddText( command_line, "" );

	if (logEnabled == true) LLAddLogFile( "Exe path:" );
	if (logEnabled == true) LLAddLogFile( exe_path );

	full_path = ULAddText( exe_path, append );

	if (logEnabled == true) LLAddLogFile( "Full path:" );
	if (logEnabled == true) LLAddLogFile( full_path );

	ULFreeAndNullString( &command_line );
	ULFreeAndNullString( &exe_path );

	return full_path;
}

//==============================================================================
/// Funzione per caricare le informazioni di configurazione
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVLoadConfig( void )
{
  int           ret_code        = SPV_CFG_NO_ERROR;
  XML_SOURCE  * cfg_source      = NULL            ;
  XML_ELEMENT * element         = NULL            ;
  void        * key             = NULL            ;
  char        * xml_config_file = NULL            ;
  bool		logEnabled 	= SYSSetKeyDebugLogEnabled(); 

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVConfigCriticalSection );
  SPVEnterConfigData("SPVLoadConfig");

	if (logEnabled == true) {
		LLCreateLogFile();
		LLAddLogFile( "Log SPVLoadConfig abilitato" );
	}

  xml_config_file = SPVGetXMLConfigPathFileName( );
  if ( xml_config_file != NULL )
  {
	LLAddLogFile( "Percorso config.xml recuperato da registro:" );
	LLAddLogFile( xml_config_file );
	// --- Recupero la struttura globale della configurazione ---
	SPVConfigSource = XMLCreate( xml_config_file ); // -MALLOC
	ULFreeAndNullString( &xml_config_file );
  }
  else
  {
	// --- Recupero la struttura globale della configurazione (percorso file di default) ---
	xml_config_file = SPVGetPath( SPV_CFG_XML_CONFIG_DEFAULT );

	LLAddLogFile( "Percorso config.xml recuperato da default define:" );
	LLAddLogFile( xml_config_file );
	SPVConfigSource = XMLCreate( xml_config_file ); // -MALLOC
  }

  cfg_source = SPVConfigSource;

  if ( cfg_source != NULL ) // Controllo il ptr alla source
  {
	ret_code = XMLOpen( cfg_source, NULL, 'R' );
	if ( ret_code == SPV_CFG_NO_ERROR )
	{
	  //ret_code = XMLRead( cfg_source, cfg_source->root_element );
	  ret_code = XMLRead( cfg_source, NULL );
	  if ( ret_code == SPV_CFG_NO_ERROR )
	  {
		SPVConfigRoot = cfg_source->root_element;
		// --- Recupero l'elemento <config> ---
		element = XMLGetNext( cfg_source->root_element->child, "config", -1 );
		if ( element != NULL )
		{
		  SPVConfigInfo.XML = (void*)element;
		  // --- Carico la configurazione hardware ---
		  SPVLoadHardwareConfig() ;
		  // --- Carico la configurazione del modulo generale ---
		  SPVLoadServerMainConfig( );
		  // --- Carico la configurazione del modulo applicativo ---
		  SPVLoadApplicativeConfig( );
		  // --- Carico la configurazione dei path ---
		  SPVLoadPathConfig( logEnabled );
		}
		else
		{
		  ret_code = SPV_CFG_INVALID_CONFIGURATION;

                  if (logEnabled == true) {
  			LLAddLogFile( "File config.xml NON Valido" );
	          }
		}
	  }
	  XMLClose( cfg_source );
	}
        else
        {
	  	if (logEnabled == true) {
  			LLAddLogFile( "File config.xml NON Trovato o NON Caricato correttamente" );
        	}
        }
  }
  else
  {
	ret_code = SPV_CFG_INVALID_SOURCE;

  	if (logEnabled == true) {
  		LLAddLogFile( "File config.xml NON Trovato o NON Caricato correttamente" );
        }
  }

  if (logEnabled == true) LLDestroyLogFile( );

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVConfigCriticalSection );
  SPVLeaveConfigData( );

  return ret_code;
}


//==============================================================================
/// Funzione per caricare la configurazione hardware
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVLoadHardwareConfig( void )
{
  int           ret_code  = SPV_CFG_NO_ERROR;
  int           hw_type   = SPV_CFG_HARDWARE_TYPE_UNKNOWN;
  XML_ELEMENT * element   = NULL            ;
  char        * value     = NULL;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVConfigCriticalSection );
  SPVEnterConfigData("SPVLoadHardwareConfig");

  element = (XML_ELEMENT*) SPVConfigInfo.XML;
  if ( element != NULL )
  {
	// --- Recupero l'elemento <hardware> ---
	element = XMLGetNext( element->child, "hardware", -1 );
	if ( element != NULL )
	{
	  // --- Recupero le informazioni hardware ---
	  value = XMLGetValue( element, "type" );
	  if ( value != NULL )
	  {
		if ( strcmpi( value, "pc" ) == 0 )
		{
		  hw_type = SPV_CFG_HARDWARE_TYPE_PC;
		}
		if ( strcmpi( value, "stlc1000" ) == 0 )
		{
		  hw_type = SPV_CFG_HARDWARE_TYPE_STLC1000;
		}
	  }

	  SPVConfigInfo.Hardware.Type  = hw_type;
	  // --- Salvo l'elemento XML ---
	  SPVConfigInfo.Hardware.XML = (void*)element;
	}
	else
	{
	  ret_code = SPV_CFG_HARDWARE_CONFIGURATION_NOT_FOUND;
	}
  }
  else
  {
	ret_code = SPV_CFG_INVALID_CONFIGURATION;
  }

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVConfigCriticalSection );
  SPVLeaveConfigData( );

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la configurazione principale
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SPVLoadServerMainConfig( void )
{
  int           ret_code  = SPV_CFG_NO_ERROR;
  XML_ELEMENT * element   = NULL;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVConfigCriticalSection );
  SPVEnterConfigData("SPVLoadServerMainConfig");

  element = (XML_ELEMENT*) SPVConfigInfo.XML;
  if ( element != NULL )
  {
    // --- Recupero l'elemento <servermain> ---
    element = XMLGetNext( element->child, "servermain", -1 );
    if ( element != NULL )
    {
      // --- Recupero le informazioni generali ---
      SPVConfigInfo.ServerMain.AutoStart  = XMLGetValueBool( element, "autostart" );
      SPVConfigInfo.ServerMain.Service    = XMLGetValueBool( element, "service" );
      SPVConfigInfo.ServerMain.TrayIcon   = XMLGetValueBool( element, "trayicon" );
      SPVConfigInfo.ServerMain.ScheDelay  = XMLGetValueInt ( element, "scheduler_delay" );
      // --- Salvo l'elemento XML ---
      SPVConfigInfo.ServerMain.XML = (void*)element;
      // --- Recupero l'elemento <form> ---
      element = XMLGetNext( element, "form", -1 );
      if ( element != NULL )
      {
        SPVConfigInfo.ServerMain.Form.XML = (void*)element;
      }
	  else
      {
      }
    }

  }
  else
  {
    ret_code = SPV_CFG_INVALID_CONFIGURATION;
  }

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVConfigCriticalSection );
  SPVLeaveConfigData( );

  return ret_code;
}

//==============================================================================
/// Funzione per caricare la configurazione del modulo applicativo
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVLoadApplicativeConfig( void )
{
	int           ret_code  = SPV_CFG_NO_ERROR;
	XML_ELEMENT * element   = NULL;

	// --- Entro nella sezione critica ---
	//EnterCriticalSection( &SPVConfigCriticalSection );
	SPVEnterConfigData("SPVLoadApplicativeConfig");

	element = (XML_ELEMENT*) SPVConfigInfo.XML;
	if ( element != NULL )
	{
		// --- Recupero l'elemento <applicative> ---
		element = XMLGetNext( element->child, "applicative", -1 );
		if ( element != NULL )
		{
			// --- Carico informazioni generali ---
			SPVConfigInfo.Applicative.MultiLanguage = XMLGetValueBool( element, "multilanguage" );
			// --- Salvo l'elemento XML ---
			SPVConfigInfo.Applicative.XML = (void*)element;
			// --- Recupero l'elemento <events> ---
			element = XMLGetNext( element, "events", -1 );
			if ( element != NULL )
			{
				SPVConfigInfo.Applicative.ReportAllDeviceEvents = XMLGetValueBool( element, "alldevice" );
				SPVConfigInfo.Applicative.ReportAllStreamEvents = XMLGetValueBool( element, "allstream" );
				SPVConfigInfo.Applicative.ReportAllFieldEvents  = XMLGetValueBool( element, "allfield" );
				SPVConfigInfo.Applicative.ReportAllValueEvents  = XMLGetValueBool( element, "allvalue" );
			}
			else
			{
				SPVConfigInfo.Applicative.ReportAllDeviceEvents = false;
				SPVConfigInfo.Applicative.ReportAllStreamEvents = false;
				SPVConfigInfo.Applicative.ReportAllFieldEvents  = false;
				SPVConfigInfo.Applicative.ReportAllValueEvents  = false;
			}
		}
	}
	else
	{
		ret_code = SPV_CFG_INVALID_CONFIGURATION;
	}

	// --- Esco dalla sezione critica ---
	//LeaveCriticalSection( &SPVConfigCriticalSection );
	SPVLeaveConfigData( );

	return ret_code;
}

/*
 *
 */
int SPVLoadPathConfig( bool logEnabled ){
	int           	ret_code  		= SPV_CFG_NO_ERROR;
	XML_ELEMENT * 	element   		= NULL;
	XML_ELEMENT * 	sub_element		= NULL;
	char * 			path_reg_key	= NULL;

	// --- Accedo ai dati di configurazione ---
	SPVEnterConfigData("SPVLoadPathConfig");

	// --- Inizializzo i path ---
	SPVConfigInfo.Path.XMLSystemPath = NULL;
	SPVConfigInfo.Path.XMLDefinitionsPath = NULL;
	SPVConfigInfo.Path.XMLStatusPath = NULL;

	element = (XML_ELEMENT*) SPVConfigInfo.XML;
	if ( element != NULL ){
		if (logEnabled == true) LLAddLogFile( "Elemento XML valido" );
		// --- Recupero l'elemento <path> ---
		element = XMLGetNext( element->child, "path", -1 );
		if ( element != NULL ){
			if (logEnabled == true) LLAddLogFile( "Elemento XML path trovato nel file config.xml" );
			// --- Salvo l'elemento XML ---
			SPVConfigInfo.Path.XML = (void*)element;
			// -- Recupero un'eventuale chiave di registro dove sono memorizzati i path ---
			path_reg_key = XMLExtractValue( element, "regkey" );
			if (logEnabled == true) LLAddLogFile( "Registry Key:" );
			if (logEnabled == true) LLAddLogFile( path_reg_key );
			// --- Recupero l'elemento <system> ---
			sub_element = XMLGetNext( element->child, "system", -1 );
			if ( sub_element != NULL ){
				if (logEnabled == true) LLAddLogFile( "Elemento XML system trovato nel file config.xml" );
				SPVConfigInfo.Path.XMLSystemPath = XMLExtractValue( sub_element, "pathfilename" );
				if (logEnabled == true) LLAddLogFile( SPVConfigInfo.Path.XMLSystemPath );
			}
			else
				if (logEnabled == true) LLAddLogFile( "Elemento XML system NON trovato nel file config.xml" );
			// --- Recupero l'elemento <definitions> ---
			sub_element = XMLGetNext( element->child, "definitions", -1 );
			if ( sub_element != NULL ){
				if (logEnabled == true) LLAddLogFile( "Elemento XML definitions trovato nel file config.xml" );
				SPVConfigInfo.Path.XMLDefinitionsPath = XMLExtractValue( sub_element, "path" );
				if (logEnabled == true) LLAddLogFile( SPVConfigInfo.Path.XMLDefinitionsPath );
			}
			else
				if (logEnabled == true) LLAddLogFile( "Elemento XML definitions NON trovato nel file config.xml" );
			// --- Recupero l'elemento <status> ---
			sub_element = XMLGetNext( element->child, "status", -1 );
			if ( sub_element != NULL ){
				if (logEnabled == true) LLAddLogFile( "Elemento XML status trovato nel file config.xml" );
				SPVConfigInfo.Path.XMLStatusPath = XMLExtractValue( sub_element, "pathfilename" );
				if (logEnabled == true) LLAddLogFile( SPVConfigInfo.Path.XMLStatusPath );
			}
			else
				if (logEnabled == true) LLAddLogFile( "Elemento XML status NON trovato nel file config.xml" );
		}
		else{
			if (logEnabled == true) LLAddLogFile( "Elemento XML path NON trovato nel file config.xml" );
		}
	}
	else{
		ret_code = SPV_CFG_INVALID_CONFIGURATION;
		if (logEnabled == true) LLAddLogFile( "Elemento XML NON valido nel file config.xml" );
	}

	// --- Se i path non sono specificati nel file config.xml li cerco da altre fonti ---
	if (SPVConfigInfo.Path.XMLSystemPath == NULL){
		SPVConfigInfo.Path.XMLSystemPath = SPVGetXMLSystemPathFileName( path_reg_key );
	}
	if (SPVConfigInfo.Path.XMLDefinitionsPath == NULL){
		SPVConfigInfo.Path.XMLDefinitionsPath = SPVGetXMLDefinitionsPath( path_reg_key );
	}
	if (SPVConfigInfo.Path.XMLStatusPath == NULL){
		SPVConfigInfo.Path.XMLStatusPath = SPVGetXMLStatusPathFileName( path_reg_key );
	}

	// --- Esco dai dati di configurazione ---
	SPVLeaveConfigData( );

	return ret_code;
}

//==============================================================================
/// Funzione per conoscere se e' attiva la modalita' multilingua
///
/// \date [26.10.2010]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
bool SPVConfigIsMultiLanguage( void )
{
	bool ml = false;

	// --- Entro nella sezione critica ---
	SPVEnterConfigData("SPVConfigIsMultiLanguage");

	ml =SPVConfigInfo.Applicative.MultiLanguage;

	// --- Esco dalla sezione critica ---
	SPVLeaveConfigData();

	return ml;
}

//==============================================================================
/// Funzione per ottenere il percorso e il nome file della configurazione server
///
/// \date [21.09.2015]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
char * SPVGetXMLConfigPathFileName( void )
{
	void        * key               = NULL;
	char        * xml_config_value  = NULL;

	// --- Accedo ai dati di configurazione ---
	SPVEnterConfigData("SPVGetXMLConfigPathFileName");

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SPV_CFG_SERVER_KEY, false );
	if ( key != NULL ){
		xml_config_value = (char*) SYSGetRegValue( key, SPV_CFG_XML_CONFIG_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	// --- Esco dai dati di configurazione ---
	SPVLeaveConfigData( );

	return xml_config_value;
}

//==============================================================================
/// Funzione per ottenere il percorso e il nome file della configurazione sistema
///
/// \date [21.09.2012]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
char * SPVGetXMLSystemPathFileName( char * reg_key_path )
{
	void        * key               = NULL;
	char        * xml_system_value  = NULL;

	// --- Accedo ai dati di configurazione ---
	SPVEnterConfigData("SPVGetXMLSystemPathFileName");

	if ( reg_key_path != NULL ){
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, reg_key_path, false);
	}
	if ( key == NULL ) {
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SPV_CFG_SERVER_KEY, false);
	}
	if ( key != NULL ){
		xml_system_value = (char*) SYSGetRegValue( key, SPV_CFG_XML_SYSTEM_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	// --- Esco dai dati di configurazione ---
	SPVLeaveConfigData( );

	return xml_system_value;
}

//==============================================================================
/// Funzione per ottenere il percorso e il nome file dello stato server
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
char * SPVGetXMLStatusPathFileName( char * reg_key_path )
{
	void        * key               = NULL;
	char        * xml_status_value  = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterConfigData("SPVGetXMLStatusPathFileName");

	if ( reg_key_path != NULL ){
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, reg_key_path, false);
	}
	if ( key == NULL ) {
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SPV_CFG_SERVER_KEY, false);
	}
	if ( key != NULL )
	{
		xml_status_value = (char*) SYSGetRegValue( key, SPV_CFG_XML_STATUS_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveConfigData( );

	return xml_status_value;
}

//==============================================================================
/// Funzione per ottenere il percorso delle definizioni periferiche
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
char * SPVGetXMLDefinitionsPath( char * reg_key_path )
{
	void        * key                   = NULL;
	char        * xml_definitions_value = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterConfigData("SPVGetXMLDefinitionsPath");

	if ( reg_key_path != NULL ){
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, reg_key_path, false);
	}
	if ( key == NULL ) {
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SPV_CFG_SERVER_KEY, false);
	}
	if ( key != NULL )
	{
		xml_definitions_value = (char*) SYSGetRegValue( key, SPV_CFG_XML_DEFINITIONS_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveConfigData( );

	return xml_definitions_value;
}

//==============================================================================
/// Funzione per salvare le informazioni di configurazione
///
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SPVSaveConfig( void )
{
  int ret_code = SPV_CFG_NO_ERROR;

  // --- Entro nella sezione critica ---
  //EnterCriticalSection( &SPVConfigCriticalSection );
  SPVEnterConfigData("SPVSaveConfig");

  ret_code = SPV_CFG_NOT_IMPLEMENTED;

  // --- Esco dalla sezione critica ---
  //LeaveCriticalSection( &SPVConfigCriticalSection );
  SPVLeaveConfigData( );

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void  SPVConfigClear( void )
{
  // --- Cancella la sezione critica della configurazione ---
  //DeleteCriticalSection( &SPVConfigCriticalSection );
  SYSClearLock(&SPVConfigLock);
  // --- Elimino l'intera sorgente XML dalla memoria ---
  XMLFree( SPVConfigSource ); // -FREE
}

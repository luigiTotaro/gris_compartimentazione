object fApplicativeView: TfApplicativeView
  Left = 38
  Top = 212
  Width = 640
  Height = 200
  Caption = 'Finestra Procedure Applicative'
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = FormResize
  DesignSize = (
    632
    166)
  PixelsPerInch = 96
  TextHeight = 13
  object gbStatus: TGroupBox
    Left = 4
    Top = 4
    Width = 623
    Height = 145
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Elenco delle procedure disponibili'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      623
      145)
    object imgStatus: TImage
      Left = 10
      Top = 16
      Width = 16
      Height = 16
      Transparent = True
    end
    object lStatus: TLabel
      Left = 30
      Top = 16
      Width = 131
      Height = 18
      AutoSize = False
      Caption = 'Procedure attive'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lProcNum: TLabel
      Left = 428
      Top = 16
      Width = 187
      Height = 16
      Alignment = taRightJustify
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'Numero procedure: n/d'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = cl3DDkShadow
      Font.Height = -11
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object pBluLineStatus: TPanel
      Left = 2
      Top = 39
      Width = 619
      Height = 3
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      Color = clHotLight
      TabOrder = 0
    end
    object sgProcedures: TStringGrid
      Left = 2
      Top = 42
      Width = 619
      Height = 101
      TabStop = False
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsNone
      Color = cl3DLight
      ColCount = 3
      DefaultRowHeight = 17
      FixedCols = 0
      RowCount = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridLineWidth = 0
      Options = [goFixedVertLine, goFixedHorzLine, goDrawFocusSelected, goColSizing, goRowSelect]
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 1
      ColWidths = (
        50
        17
        223)
    end
  end
  object sbProcedures: TStatusBar
    Left = 0
    Top = 147
    Width = 632
    Height = 19
    Panels = <>
    SimplePanel = False
  end
end

//==============================================================================
// Telefin Supervisor Applicative View Form 1.0
//------------------------------------------------------------------------------
// Header del form del livello applicativo (SPVApplicativeViewForm.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.06 (30.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVApplicativeViewForm.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [30.09.2004]:
// - Prima versione prototipo del modulo.
// 0.02 [15.06.2005]:
// - Incluso il modulo SPVServerGUI.
// - Aggiunti alcuni codici di errore.
// - Modificato il metodo costruttore TfApplicativeView::TfApplicativeView.
// - Aggiunto il metodo TfApplicativeView::FormResize.
// - Aggiunto il metodo TfApplicativeView::ClearAllProceduresStatus.
// - Aggiutno il metodo TfApplicativeView::ViewProcedureStatus.
// - Aggiunto il metodo TfApplicativeView::ViewAllProceduresStatus.
// 0.03 [24.06.2005]:
// - Modificato il metodo TfApplicativeView::ViewProcedureStatus.
// 0.04 [30.06.2005]:
// - Modificato il metodo TfApplicativeView::ClearAllProceduresStatus.
// - Modificato il metodo TfApplicativeView::ViewProcedureStatus.
// 0.05 [22.09.2005]:
// - Modificato il metodo costruttore TfApplicativeView::TfApplicativeView.
// 0.06 [19.12.2005]:
// - Modificato il metodo costruttore TfApplicativeView::TfApplicativeView.
//==============================================================================

#ifndef SPVApplicativeViewFormH
#define SPVApplicativeViewFormH

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_APPLICATIVEVIEW_NO_ERROR              0
#define SPV_APPLICATIVEVIEW_INVALID_STRING_GRID   24001
#define SPV_APPLICATIVEVIEW_INVALID_THREAD        24002
#define SPV_APPLICATIVEVIEW_INVALID_PROCEDURE_ID  24003
#define SPV_APPLICATIVEVIEW_INVALID_PROCEDURE     24004

//==============================================================================
// Classi
//------------------------------------------------------------------------------
class TfApplicativeView : public TForm
{
__published:	// IDE-managed Components
  TGroupBox   * gbStatus;
  TImage      * imgStatus;
  TLabel      * lStatus;
  TLabel *lProcNum;
  TPanel      * pBluLineStatus;
  TStringGrid * sgProcedures;
  TStatusBar *sbProcedures;
  /// Metodo che cattura l'evento Resize del form
  void  __fastcall FormResize               ( TObject * Sender );
private:	// User declarations
public:		// User declarations
  /// Metodo costruttore della classe TfApplicativeView
        __fastcall TfApplicativeView        ( TComponent * Owner );
  /// Metodo per visualizzare lo stato di tutte le procedure applicative
  int   __fastcall ClearAllProceduresStatus ( void );
  /// Metodo per visualizzare lo stato di una procedura applicativa
  int   __fastcall ViewProcedureStatus      ( int ID );
  /// Metodo per visualizzare lo stato di tutte le procedure applicative
  int   __fastcall ViewAllProceduresStatus  ( void );
};

//==============================================================================
// Variabili globali esterne
//------------------------------------------------------------------------------
extern PACKAGE TfApplicativeView *fApplicativeView;

#endif

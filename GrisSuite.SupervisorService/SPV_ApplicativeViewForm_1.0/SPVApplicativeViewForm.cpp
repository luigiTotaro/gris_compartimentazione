//==============================================================================
// Telefin Supervisor Applicative View Form 1.0
//------------------------------------------------------------------------------
// Form del livello applicativo (SPVApplicativeViewForm.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.06 (30.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVApplicativeViewForm.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVApplicativeViewForm.h
//==============================================================================

#include <vcl.h>
#pragma hdrstop

#include "SPVApplicativeViewForm.h"
#include "SPVApplicative.h"
#include "SPVServerGUI.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
TfApplicativeView * fApplicativeView;

//==============================================================================
/// Metodo costruttore della classe TfApplicativeView
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
__fastcall TfApplicativeView::TfApplicativeView( TComponent * Owner ):TForm( Owner )
{
  // --- Dimensioni form ---
  this->Left    = 320;
  this->Top     = 400;
  this->Width   = 640;
  this->Height  = 200;
  // --- Colori form ---
  this->Color   = (TColor)0x00D8E9EC;

  // --- Dimensioni gbStatus ---
  this->gbStatus->Left        = 4;
  this->gbStatus->Top         = 4;
  this->gbStatus->Width       = 623;
  this->gbStatus->Height      = 140;
  // --- Colori gbStatus ---
  this->gbStatus->Color       = (TColor)0x00D8E9EC;
  // --- Font gbStatus ---
  this->gbStatus->Font->Color = (TColor)0x00C56A31;
  this->gbStatus->Font->Name  = "MS Sans Serif";
  this->gbStatus->Font->Size  = 8;

  // --- Dimensioni lStatus ---
  this->lStatus->Left         = 30;
  this->lStatus->Top          = 16;
  this->lStatus->Width        = 131;
  this->lStatus->Height       = 18;
  // --- Colori lStatus ---
  this->lStatus->Color        = (TColor)0x00D8E9EC;
  // --- Font lStatus ---
  this->lStatus->Font->Color  = (TColor)0x00000000;
  this->lStatus->Font->Name   = "Trebuchet MS";
  this->lStatus->Font->Size   = 8;

  // --- Dimensioni lProcNum ---
  this->lProcNum->Left          = 428;
  this->lProcNum->Top           = 16;
  this->lProcNum->Width         = 187;
  this->lProcNum->Height        = 16;
  // --- Colori lProcNum ---
  this->lProcNum->Color         = (TColor)0x00D8E9EC;
  // --- Font lProcNum ---
  this->lProcNum->Font->Color   = (TColor)0x00888888;
  this->lProcNum->Font->Name    = "Trebuchet MS";
  this->lProcNum->Font->Size    = 8;

  // --- Dimensioni pBluLineStatus ---
  this->pBluLineStatus->Left   = 2;
  this->pBluLineStatus->Top    = 39;
  this->pBluLineStatus->Width  = 619;
  this->pBluLineStatus->Height = 3;
  // --- Colori pBluLineStatus ---
  this->pBluLineStatus->Color  = (TColor)0x00C56A31;

  // --- Dimensioni sgProcedures ---
  this->sgProcedures->Left   = 2;
  this->sgProcedures->Top    = 42;
  this->sgProcedures->Width  = 619;
  this->sgProcedures->Height = 96;
  // --- Colori sgProcedures ---
  this->sgProcedures->Color       = (TColor)0x00CCFFFF;
  // --- Font sgProcedures ---
  this->sgProcedures->Font->Color = (TColor)0x00000000;
  this->sgProcedures->Font->Name  = "MS Sans Serif";
  this->sgProcedures->Font->Size  = 8;

  // --- Colori sbProcedures ---
  this->sbProcedures->Color       = (TColor)0x00D8E9EC;
  // --- Font sbProcedures ---
  this->sbProcedures->Font->Color = (TColor)0x00000000;
  this->sbProcedures->Font->Name  = "MS Sans Serif";
  this->sbProcedures->Font->Size  = 8;


  this->ClearAllProceduresStatus();
}

//==============================================================================
/// Metodo che cattura l'evento Resize del form
///
/// \date [15.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfApplicativeView::FormResize( TObject * Sender )
{
  this->ViewAllProceduresStatus();

  // --- Ridimensionamento automatico della tabella procedure ---
  SPVTableAutoSize( (void*)this->sgProcedures );
}

//==============================================================================
/// Metodo per visualizzare lo stato di tutte le procedure applicative
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int __fastcall TfApplicativeView::ClearAllProceduresStatus( void )
{
  int ret_code = SPV_APPLICATIVEVIEW_NO_ERROR;

  if ( this->sgProcedures != NULL )
  {
    // --- Inizializzo la tabella delle procedure ---
    this->sgProcedures->ColCount    = 8;
    this->sgProcedures->RowCount    = 2;
    this->sgProcedures->Cells[0][0] = "      Nome";
    this->sgProcedures->Cells[1][0] = "ID";
    this->sgProcedures->Cells[2][0] = "Periferica";
    this->sgProcedures->Cells[3][0] = "Tipo";
    this->sgProcedures->Cells[4][0] = "Operazione";
    this->sgProcedures->Cells[5][0] = "Stato";
    this->sgProcedures->Cells[6][0] = "Inizio ultima esecuzione";
    this->sgProcedures->Cells[7][0] = "Fine ultima esecuzione";
    this->sgProcedures->Cells[0][1] = "<Nessuna procedura>";
  }
  else
  {
    ret_code = SPV_APPLICATIVEVIEW_INVALID_STRING_GRID;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di una procedura applicativa
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfApplicativeView::ViewProcedureStatus( int ID )
{
  int                     ret_code    = SPV_APPLICATIVEVIEW_NO_ERROR;
  int                     proc_count  = 0;
  SPV_PROCEDURE         * proc        = NULL;
  SPV_PROCEDURE_THREAD  * proc_thread = NULL;

  char                  * char_value  = NULL;
  AnsiString              name            = "n/d";
  AnsiString              id              = "n/d";
  AnsiString              device          = "n/d";
  AnsiString              type            = "n/d";
  AnsiString              step            = "n/d";
  AnsiString              status          = "n/d";
  AnsiString              last_code       = "n/d";
  AnsiString              start           = "n/d";
  AnsiString              finish          = "n/d";

  proc_count = SPVProcedureCount;

  if ( ID >= 0 && ID < proc_count )
  {
    proc = &SPVProcedureList[ID];
    if ( proc != NULL )
    {
      // --- Preparo le stringhe dello stato da inserire in tabella ---
      name    = AnsiString( proc->Name );
      id      = AnsiString( proc->ID );
      device  = AnsiString( proc->Device->Name )
              + AnsiString( " (" ) + AnsiString( proc->Device->ID ) + AnsiString( ")" );
      if ( proc->State != NULL )
      {
        step    = AnsiString( proc->State->Name )
                + AnsiString( " (" ) + AnsiString( proc->State->Code ) + AnsiString( ")" );
      }
      if ( proc->Active )
      {
        status = AnsiString( "Attivo" );
      }
      else
      {
        status = AnsiString( "Idle" );
      }
      if ( proc->StartDT > 0 )
      {
        char_value = XMLType2ASCII( &proc->StartDT, t_datetime ); // -MALLOC
        if ( char_value != NULL )
        {
          start = AnsiString( char_value );
          free( char_value ); // -FREE
        }
      }
      if ( proc->FinishDT > 0 )
      {
        char_value = XMLType2ASCII( &proc->FinishDT, t_datetime ); // -MALLOC
        if ( char_value != NULL )
        {
          finish = AnsiString( char_value );
          free( char_value ); // -FREE
        }
      }
    }
    else
    {
      ret_code = SPV_APPLICATIVEVIEW_INVALID_PROCEDURE;
    }
    // --- Controllo se c'� la riga sulla tabella ---
    if ( this != NULL )
    {
      if ( this->sgProcedures != NULL )
      {
        if (  ( ID + 2 ) > this->sgProcedures->RowCount )
        {
          this->sgProcedures->RowCount = ID + 2 ;
        }
        // --- Inserisco in tabella le stringhe di stato ---
        this->sgProcedures->Cells[0][ID+1] = name     ;
        this->sgProcedures->Cells[1][ID+1] = id       ;
        this->sgProcedures->Cells[2][ID+1] = device   ;
        this->sgProcedures->Cells[3][ID+1] = last_code;
        this->sgProcedures->Cells[4][ID+1] = step     ;
        this->sgProcedures->Cells[5][ID+1] = status   ;
        this->sgProcedures->Cells[6][ID+1] = start    ;
        this->sgProcedures->Cells[7][ID+1] = finish   ;
        Application->ProcessMessages();
      }
    }
  }
  else
  {
    ret_code = SPV_APPLICATIVEVIEW_INVALID_PROCEDURE_ID;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di tutte le procedure applicative
///
/// \date [15.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfApplicativeView::ViewAllProceduresStatus( void )
{
  int ret_code = SPV_APPLICATIVEVIEW_NO_ERROR;

  if ( this->sgProcedures != NULL ) // Controllo il ptr all'oggetto StringGrid
  {
    if ( SPVProcedureCount > 0 )
    {
      for ( int p = 0; p < SPVProcedureCount; p++ )
      {
        ViewProcedureStatus( p );
      }
    }
    else
    {
      this->ClearAllProceduresStatus();
    }
  }
  else
  {
    ret_code = SPV_APPLICATIVEVIEW_INVALID_STRING_GRID;
  }

  return ret_code;
}

//==============================================================================
// Telefin Supervisor Main Thread Module 1.0
//------------------------------------------------------------------------------
// Header Modulo Thread Principale (SPVMainThread.h)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:	0.01 (08.10.2007 -> 08.10.2007)
//
// Copyright:	2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland C++ Builder 2006
// Autori:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVMainThread.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [08.10.2007]:
// - Prima versione del modulo.
//==============================================================================

#ifndef SPVMainThreadH
#define SPVMainThreadH



//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_MAIN_THREAD_NO_ERROR                    0
#define SPV_MAIN_THREAD_UNKNOWN_ERROR               271000
#define SPV_MAIN_THREAD_CRITICAL_ERROR              271001
#define SPV_MAIN_THREAD_ALLOCATION_FAILURE          271002
#define SPV_MAIN_THREAD_INVALID_THREAD              271003

//==============================================================================
// Prototipi Funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo del thread principale
int SPVMainThreadInit( void );
// Funzione per chiudere il modulo del thread principale
int SPVMainThreadClear( void );
// Funzione per avviare il thread principale
int SPVMainThreadStart( void );
// Funzione per arrestare il thread principale
int SPVMainThreadStop( void );
// Funzione di attivazione per il thread principale
unsigned long __stdcall SPVMainThreadActivationFunction( void * parameter );


//---------------------------------------------------------------------------
#endif

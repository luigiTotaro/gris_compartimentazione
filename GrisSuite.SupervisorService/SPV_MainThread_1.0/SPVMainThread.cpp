//==============================================================================
// Telefin Supervisor Main Thread Module 1.0
//------------------------------------------------------------------------------
// Modulo Thread Principale (SPVMainThread.cpp)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:	0.01 (08.10.2007 -> 08.10.2007)
//
// Copyright:	2007 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, Borland C++ Builder 2006
// Autori:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVMainThread.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVMainThread.h
//==============================================================================

#pragma hdrstop

#include "SPVMainThread.h"
#include "SYSKernel.h"
#include "SPVServerMain.h"
#include "SPVMainService.h"

#include "LogLibrary.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
static SYS_THREAD * MainThread;



//==============================================================================
// Implementazione Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per inizializzare il modulo del thread principale
///
/// \date [08.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SPVMainThreadInit( void )
{
  int ret_code = SPV_MAIN_THREAD_NO_ERROR;

  MainThread = SYSNewThread( SPVMainThreadActivationFunction, 0, CREATE_SUSPENDED ); // Alloco il thread principale
  if ( MainThread != NULL )
  {

    ret_code = SYSSetThreadParameter( MainThread, (void*) MainThread ); // Gli passo come parametro la procedura
    if ( ret_code == SPV_MAIN_THREAD_NO_ERROR )
    {

      ret_code = SYSCreateThread( MainThread );
      if ( ret_code == SPV_MAIN_THREAD_NO_ERROR )
      {
        // --- Inizializzazione Modulo principale del Server ---
      	int ret_tmp = SPVServerMainInit( (void*) NULL );

        if(ret_tmp != SPV_SERVER_MAIN_NO_ERROR)
        {
            ret_code = ret_tmp;
        }
		/* SPERIMENTALE SPOSTATO
		// --- Carico il server ---
		ret_code = SPVServerMainLoad( );
		*/

                //ret_code = SPVServerMainLoad( );

        	SYSAddThreadToList( MainThread );
      }
    }
  }
  else
  {
    ret_code = SPV_MAIN_THREAD_ALLOCATION_FAILURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo del thread principale
///
/// \date [01.08.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVMainThreadClear( void )
{
  int  ret_code 	= SPV_MAIN_THREAD_NO_ERROR;

  bool logEnabled 	= SYSSetKeyDebugLogEnabled();

  if (logEnabled == true) {
  	LLCreateLogFile();
  }

  if (logEnabled == true) {
      	LLAddLogFile( "SPVMainThreadClear..." );
  }


  if ( SYSValidThread( MainThread ) )
  {
    ret_code = SYSTerminateThread( MainThread );

  if (logEnabled == true) {
      	LLAddLogFile( "Richiesta SYSTerminateThread INVIATA" );
  }

    // --- Aspetto la fine del thread ---

    /*
    if ( SYSWaitForThread( MainThread ) != 0 )
    {
      // --- Aspetto 250 ms ---
      Sleep( 250 );
    }
    */

    if ( SYSWaitForThread( MainThread ) != 0 )
    {
    	if (logEnabled == true) {
      		LLAddLogFile( "In Attesa Arresto Main Thread" );
  	}

        // --- Aspetto 1000 ms ---
        Sleep( 1000 );
    }

    if (logEnabled == true) {
    	LLAddLogFile( "Main Thread ARRESTATO" );
    }

    // --- Elimino il thread terminato ---
    ret_code = SYSDeleteThread( MainThread );

    if (logEnabled == true) {
    	LLAddLogFile( "Delete MainThread ESEGUITO" );
    }

  }
  else
  {
    ret_code = SPV_MAIN_THREAD_INVALID_THREAD;
  }

  if (logEnabled == true) {
      	LLAddLogFile( "SPVMainThreadClear ESEGUITO" );
  }

  return ret_code;
}

//==============================================================================
/// Funzione per avviare il thread principale
///
/// \date [01.07.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVMainThreadStart( void )
{
  int  ret_code 	= SPV_MAIN_THREAD_NO_ERROR;

  bool logEnabled 	= SYSSetKeyDebugLogEnabled();

  if (logEnabled == true) {
  	LLCreateLogFile();
  }

  if (logEnabled == true) {
      	LLAddLogFile( "" );
      	LLAddLogFile( "Riavvio del servizio..." );
  }

  ret_code = SYSResumeThread( MainThread );

  return ret_code;
}

//==============================================================================
/// Funzione per arrestare il thread principale
///
/// \date [01.07.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVMainThreadStop( void )
{
  int ret_code 		= SPV_MAIN_THREAD_NO_ERROR;

  ret_code 		= SYSSuspendThread( MainThread );

  bool logEnabled 	= SYSSetKeyDebugLogEnabled();

  if (logEnabled == true) {
  	LLCreateLogFile();
  }

  if (logEnabled == true) {
       	LLAddLogFile( "SPVMainThreadStop ESEGUITO" );
  }

  return ret_code;
}

//==============================================================================
/// Funzione di attivazione per il thread principale
///
/// \date [08.10.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
unsigned long __stdcall SPVMainThreadActivationFunction( void * parameter )
{
	unsigned long           ret_code        = SPV_MAIN_THREAD_NO_ERROR; // Codice di ritorno
	int                     fun_code        = SPV_MAIN_THREAD_NO_ERROR; // Codice di ritorno
	SYS_THREAD            * thread          = NULL                    ; // Puntatore al thread

        bool 			logEnabled 	= SYSSetKeyDebugLogEnabled();

	if ( parameter != NULL ) // Controllo il parametro passato
	{
		thread = (SYS_THREAD*) parameter; // Recupero il thread
		if ( thread != NULL ) // Se la procedura � valida
		{
			try
			{
        			if (logEnabled == true) {
					LLCreateLogFile();
        			}
				//* SPERIMENTALE
				// --- Carico il server ---
				ret_code = SPVServerMainLoad( );

                                if (logEnabled == true) LLAddLogFile( "SPVServerMainLoad ESEGUITO" );

				ret_code = SPVServerMainStart();

                              	if (logEnabled == true) LLAddLogFile( "SPVServerMainStart ESEGUITO" );

                                int nContat = 0;

				while ( thread->Terminated == false ) // Ciclo del thread
				{
                                	if (logEnabled == true) LLAddLogFile( "nStlcSPVService - Alive" );

                                        nContat = (nContat + 1) % 10;

                                        if(nContat == 0)
                                        {
                                        	SYSExportThreadListManager();
                                        }

                                        Sleep( 100 );
				}
			}
			catch (...)
			{
                               	if (logEnabled == true) LLAddLogFile( "Eccezione: SPVMainThreadActivationFunction" );

				ret_code = SPV_MAIN_THREAD_CRITICAL_ERROR;
			}
		}
	}

	return ret_code;
}

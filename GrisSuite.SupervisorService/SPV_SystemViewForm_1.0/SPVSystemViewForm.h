//==============================================================================
// Telefin Supervisor System View Form 1.0
//------------------------------------------------------------------------------
// Header Form sinottico del sistema (SPVSystemViewForm.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.14 (27.09.2004 -> 30.11.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVSystemViewForm.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [27.09.2004]:
// - Prima versione del modulo.
// 0.02 [19.01.2005]:
// - Aggiunte le definizioni di alcuni valori costanti.
// 0.03 [25.01.2005]:
// - Modificato il metodo TfSystemView::popSystemPopup.
// - Aggiunto il metodo TfSystemView::popSystemClick.
// 0.04 [01.02.2005]:
// - Modificato il metodo TfSystemView::ViewDeviceInfo.
// 0.05 [30.06.2005]:
// - Modificato il metodo TfSystemView::TfSystemView.
// - Aggiunto il metodo TfSystemView::popDeviceClick.
// - Aggiunto il metodo TfSystemView::ToggleDeviceActive.
// - Aggiunto il metodo TfSystemView::ToggleDeviceScheduled.
// - Aggiunto il metodo TfSystemView::SystemTreeViewDblClick.
// 0.06 [21.07.2005]:
// - Modificato il metodo TfSystemView::SystemTreeViewDblClick.
// 0.07 [25.07.2005]:
// - Modificato il metodo costruttore TfSystemView::TfSystemView.
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// 0.08 [26.07.2005]:
// - Modificato il metodo TfSystemView::ViewDeviceInfo.
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// 0.09 [29.07.2005]:
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// 0.10 [04.08.2005]:
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// 0.11 [12.09.2005]:
// - Aggiunto il metodo TfSystemView::SetTreeNodeStatus.
// - Modificato il metodo TfSystemView::ViewDeviceInfo.
// - Modificato il metodo TfSystemView::ViewNodeInfo.
// - Modificato il metodo TfSystemView::ViewZoneInfo.
// - Modificato il metodo TfSystemView::ViewRegionInfo.
// - Modificato il metodo TfSystemView::ViewServerInfo.
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// 0.12 [25.11.2005]:
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// - Modificato il metodo TfSystemView::ViewCardInfo.
// - Modificato il metodo TfSystemView::TfSystemView.
// 0.13 [28.11.2005]:
// - Modificato il metodo TfSystemView::ViewCardInfo.
// - Modificato il metodo TfSystemView::ViewDeviceInfo.
// - Modificato il metodo TfSystemView::ViewNodeInfo.
// - Modificato il metodo TfSystemView::ViewZoneInfo.
// - Modificato il metodo TfSystemView::ViewRegionInfo.
// - Modificato il metodo TfSystemView::ViewServerInfo.
// - Modificato il metodo TfSystemView::ViewCardStatus.
// - Modificato il metodo TfSystemView::ViewDeviceStatus.
// - Modificato il metodo TfSystemView::ViewNodeStatus.
// - Modificato il metodo TfSystemView::ViewZoneStatus.
// - Modificato il metodo TfSystemView::ViewRegionStatus.
// - Modificato il metodo TfSystemView::ViewServerStatus.
// 0.14 [30.11.2005]:
// - Modificato il metodo costruttore TfSystemView::TfSystemView.
//==============================================================================
#ifndef SPVSystemViewFormH
#define SPVSystemViewFormH
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>

//==============================================================================
// Definizione valori
//------------------------------------------------------------------------------
#define SPV_SYSTEMVIEW_SERVER_OBJECT_LEVEL  0
#define SPV_SYSTEMVIEW_REGION_OBJECT_LEVEL  1
#define SPV_SYSTEMVIEW_ZONE_OBJECT_LEVEL    2
#define SPV_SYSTEMVIEW_NODE_OBJECT_LEVEL    3
#define SPV_SYSTEMVIEW_DEVICE_OBJECT_LEVEL  4

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_SYSTEMVIEW_NO_ERROR           0
#define SPV_SYSTEMVIEW_INVALID_CARD       21001
#define SPV_SYSTEMVIEW_INVALID_DEVICE     21002
#define SPV_SYSTEMVIEW_INVALID_NODE       21003
#define SPV_SYSTEMVIEW_INVALID_ZONE       21004
#define SPV_SYSTEMVIEW_INVALID_REGION     21005
#define SPV_SYSTEMVIEW_INVALID_SERVER     21006
#define SPV_SYSTEMVIEW_INVALID_SYSTEM     21007
#define SPV_SYSTEMVIEW_INVALID_TREE       21008
#define SPV_SYSTEMVIEW_INVALID_TREE_NODE  21009

//==============================================================================
// Classi
//------------------------------------------------------------------------------

class TfSystemView : public TForm
{
__published:	// IDE-managed Components
  TTreeView   * SystemTreeView  ;
  TStatusBar *sbSystemView;
  TImageList  * TreeIconList    ;
  TGroupBox   * gbInfo          ;
  TLabel      * lType           ;
  TLabel      * lName           ;
  TImage      * imgInfo         ;
  TLabel      * lInfo           ;
  TPanel      * pLine           ;
  TPanel      * pBluLineInfo    ;
  TPanel      * pInfo           ;
  TGroupBox   * gbStatus        ;
  TImage      * imgStatus       ;
  TLabel      * lStatus         ;
  TLabel      * lLevel          ;
  TPanel      * pBluLineStatus  ;
  TMemo       * mStatus         ;
  TPanel      * pStatus         ;
  TPopupMenu  * popSystem       ;
  TImageList *ilInfo;
  /// Metodo per visualizzare le informazioni e lo stato dell'entit� selezionata
  void __fastcall SystemTreeViewClick     ( TObject *Sender );
  void __fastcall SystemTreeViewChange    ( TObject *Sender, TTreeNode *Node );
  void __fastcall popSystemPopup          ( TObject *Sender );
  void __fastcall SystemTreeViewMouseDown ( TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y );
  void __fastcall SystemTreeViewDblClick  ( TObject *Sender );
private:	// User declarations
  /*
  TIcon             * icoServer;
  TIcon             * icoRegion;
  TIcon             * icoZone;
  TIcon             * icoNode;
  TIcon             * icoDevice;
  TIcon             * icoCard;
  TIcon             * icoOk;
  TIcon             * icoWarning;
  TIcon             * icoError;
  //*/
public:		// User declarations
  /// Metodo costruttore della classe
          __fastcall  TfSystemView    ( TComponent * Owner );
  /// Metodo per impostare il puntatore al sistema
  void *  __fastcall  AddTreeNode     ( TTreeView * tree, TTreeNode * parent, void * item, AnsiString name, int icon );
  /// Metodo per modificare lo stato di un nodo
  void * __fastcall   SetTreeNodeStatus( TTreeNode * node, int status );
  /// Metodo per visualizzare le informazioni e lo stato di una scheda
  int     __fastcall  ViewCardInfo    ( void * data );
  /// Metodo per visualizzare le informazioni e lo stato di una periferica
  int     __fastcall  ViewDeviceInfo  ( void * data );
  /// Metodo per visualizzare le informazioni e lo stato di un nodo
  int     __fastcall  ViewNodeInfo    ( void * data );
  /// Metodo per visualizzare le informazioni e lo stato di una zona
  int     __fastcall  ViewZoneInfo    ( void * data );
  /// Metodo per visualizzare le informazioni e lo stato di una regione
  int     __fastcall  ViewRegionInfo  ( void * data );
  /// Metodo per visualizzare le informazioni e lo stato di un server
  int     __fastcall  ViewServerInfo  ( void * data );
  /// Metodo per visualizzare lo stato di una scheda
  int     __fastcall  ViewCardStatus  ( void * data );
  /// Metodo per visualizzare lo stato di una periferica
  int     __fastcall  ViewDeviceStatus( void * data );
  /// Metodo per visualizzare lo stato di un nodo
  int     __fastcall  ViewNodeStatus( void * data );
  /// Metodo per visualizzare lo stato di una zona
  int     __fastcall  ViewZoneStatus( void * data );
  /// Metodo per visualizzare lo stato di una regione
  int     __fastcall  ViewRegionStatus( void * data );
  /// Metodo per visualizzare lo stato di un server
  int     __fastcall  ViewServerStatus( void * data );
  /// Metodo per visualizzare l'albero della periferica
  int     __fastcall  ViewDeviceTree  ( TTreeView * tree, void * pdevice );
  /// Metodo per visualizzare l'albero del nodo
  int     __fastcall  ViewNodeTree    ( TTreeView * tree, void * pnode );
  /// Metodo per visualizzare l'albero della zona
  int     __fastcall  ViewZoneTree    ( TTreeView * tree, void * pzone );
  /// Metodo per visualizzare l'albero della regione
  int     __fastcall  ViewRegionTree  ( TTreeView * tree, void * pregion );
  /// Metodo per visualizzate l'albero del server
  int     __fastcall  ViewServerTree  ( TTreeView * tree, void * pserver );
  /// Metodo per visualizzare l'albero del sistema
  int     __fastcall  ViewSystemTree  ( TTreeView * tree, void * psystem );
  /// Metodo per rimuovere un nodo da un albero
  int     __fastcall  RemoveTreeNode  ( TTreeView * tree, TTreeNode * node );
  /// Metodo per cancellare l'albero della periferica
  int     __fastcall  ClearDeviceTree ( TTreeView * tree, void * pdevice );
  /// Metodo per cancellare l'albero del nodo
  int     __fastcall  ClearNodeTree   ( TTreeView * tree, void * pnode );
  /// Metodo per cancellare l'albero della zona
  int     __fastcall  ClearZoneTree   ( TTreeView * tree, void * pzone );
  /// Metodo per cancellare l'albero della regione
  int     __fastcall  ClearRegionTree ( TTreeView * tree, void * pregion );
  /// Metodo per cancellare l'albero del server
  int     __fastcall  ClearServerTree ( TTreeView * tree, void * pserver );
  /// Metodo per cancellare l'albero del sistema
  int     __fastcall  ClearSystemTree ( TTreeView * tree, void * psystem );
  /// Metodo che gestisce l'evento di click su un elemento del popup menu
  void    __fastcall  popSystemClick  ( TObject * Sender );
  /// Metodo che gestisce l'evento di click su un elemento propriet� periferica
  void    __fastcall  popDeviceClick  ( TObject * Sender );

  void __fastcall ToggleDeviceActive( TObject * Sender );

  void __fastcall ToggleDeviceScheduled( TObject * Sender );
};

//==============================================================================
// Varibili globali
//------------------------------------------------------------------------------

extern  PACKAGE TfSystemView  * fSystemView;
#endif

//==============================================================================
// Telefin Supervisor System View Form 1.0
//------------------------------------------------------------------------------
// Form sinottico del sistema (SPVSystemViewForm.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.14 (27.09.2004 -> 30.11.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVSystemViewForm.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVSystemViewForm.h
//==============================================================================
#include <vcl.h>
#pragma hdrstop
#include "SPVSystemViewForm.h"
#include "SPVSystem.h"
#include "SPVServerGUI.h"
#include "SPVConfig.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
TfSystemView  * fSystemView;

//==============================================================================
// Metodi e Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Metodo costruttore della classe TFSystemView
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
__fastcall TfSystemView::TfSystemView( TComponent* Owner ) : TForm( Owner )
{
  // --- Impostazione dei colori della finestra ---
  this->Top     = 0;
  this->Left    = 320;
  this->Width   = 640;
  this->Height  = 400;
  this->Color   = (TColor)0x00D8E9EC;

  this->SystemTreeView->Left   = 6;
  this->SystemTreeView->Top    = 8;
  this->SystemTreeView->Width  = 308;
  this->SystemTreeView->Height = 338;
  this->SystemTreeView->Font->Name = "MS Sans Serif";
  this->SystemTreeView->Font->Size = 8;

  this->gbInfo->Color  = (TColor)0x00D8E9EC;
  this->gbInfo->Left   = 318;
  this->gbInfo->Top    = 3;
  this->gbInfo->Width  = 309;
  this->gbInfo->Height = 203;
  this->gbInfo->Font->Color   = (TColor)0x00C56A31;
  this->gbInfo->Font->Name = "MS Sans Serif";
  this->gbInfo->Font->Size = 8;

  this->pBluLineInfo->Left    = 2;
  this->pBluLineInfo->Top     = 54;
  this->pBluLineInfo->Width   = 305;
  this->pBluLineInfo->Height  = 4;
  this->pBluLineInfo->Color   = (TColor)0x00C56A31;

  this->pInfo->Left   = 2;
  this->pInfo->Top    = 58;
  this->pInfo->Width  = 8;
  this->pInfo->Height = 143;
  this->pInfo->Color          = (TColor)0x00CCFFFF;

  this->lInfo->Color          = (TColor)0x00CCFFFF;
  this->lInfo->Left   = 10;
  this->lInfo->Top    = 58;
  this->lInfo->Width  = 297;
  this->lInfo->Height = 143;
  this->lInfo->Font->Name = "MS Sans Serif";
  this->lInfo->Font->Size = 8;

  this->lName->Left   = 48;
  this->lName->Top    = 14;
  this->lName->Width  = 253;
  this->lName->Height = 18;
  this->lName->Font->Name = "Trebuchet MS";
  this->lName->Font->Size = 10;
  this->lName->Font->Color = (TColor)0x00000000;
  this->lName->Color = (TColor)0x00D8E9EC;

  this->pLine->Left   = 48;
  this->pLine->Top    = 32;
  this->pLine->Width  = 251;
  this->pLine->Height = 2;
  this->pLine->Color  = (TColor)0x00D8E9EC;

  this->lType->Color = (TColor)0x00D8E9EC;
  this->lType->Left   = 48;
  this->lType->Top    = 32;
  this->lType->Width  = 249;
  this->lType->Height = 16;
  this->lType->Font->Name = "Trebuchet MS";
  this->lType->Font->Size = 8;
  this->lType->Font->Color = (TColor)0x00888888;

  this->gbStatus->Left   = 318;
  this->gbStatus->Top    = 208;
  this->gbStatus->Width  = 309;
  this->gbStatus->Height = 138;
  this->gbStatus->Color = (TColor)0x00D8E9EC;
  this->gbStatus->Font->Color = (TColor)0x00C56A31;
  this->gbStatus->Font->Name  = "MS Sans Serif";
  this->gbStatus->Font->Size  = 8;

  this->lStatus->Color = (TColor)0x00D8E9EC;
  this->lStatus->Left   = 36;
  this->lStatus->Top    = 16;
  this->lStatus->Width  = 133;
  this->lStatus->Height = 18;
  this->lStatus->Font->Name = "Trebuchet MS";
  this->lStatus->Font->Size = 9;
  this->lStatus->Font->Color = (TColor)0x00000000;

  this->pBluLineStatus->Color = (TColor)0x00C56A31;
  this->pBluLineStatus->Left    = 2;
  this->pBluLineStatus->Top     = 39;
  this->pBluLineStatus->Width   = 305;
  this->pBluLineStatus->Height  = 3;

  this->lLevel->Color   = (TColor)0x00D8E9EC;
  this->lLevel->Left    = 170;
  this->lLevel->Top     = 16;
  this->lLevel->Width   = 133;
  this->lLevel->Height  = 16;
  this->lLevel->Font->Name  = "Trebuchet MS";
  this->lLevel->Font->Size  = 8;
  this->lLevel->Font->Color = (TColor)0x00888888;

  this->pStatus->Color   = (TColor)0x00CCFFFF;
  this->pStatus->Left    = 2;
  this->pStatus->Top     = 42;
  this->pStatus->Width   = 8;
  this->pStatus->Height  = 94;

  this->mStatus->Color   = (TColor)0x00CCFFFF;
  this->mStatus->Left    = 10;
  this->mStatus->Top     = 42;
  this->mStatus->Width   = 297;
  this->mStatus->Height  = 94;
  this->mStatus->Font->Name  = "MS Sans Serif";
  this->mStatus->Font->Size  = 8;
  this->mStatus->Font->Color = (TColor)0x00000000;

  this->sbSystemView->Color = (TColor)0x00D8E9EC;

  // --- Eventuale autostart del server ---
  if ( SPVConfigInfo.ServerMain.AutoStart )
  {
    SPVClickStartButton( );
  }
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato dell'entit� selezionata
///
/// \date [28.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfSystemView::SystemTreeViewClick(TObject *Sender)
{
  /*
  TTreeNode   * tnode   = NULL                  ;

  tnode = this->SystemTreeView->Selected;

  if ( tnode != NULL )
  {
    switch ( tnode->Level )
    {
      case 0:
        this->ViewServerInfo( tnode->Data );
      break;
      case 1:
        this->ViewRegionInfo( tnode->Data );
      break;
      case 2:
        this->ViewZoneInfo( tnode->Data );
      break;
      case 3:
        this->ViewNodeInfo( tnode->Data );
      break;
      case 4:
        this->ViewDeviceInfo( tnode->Data );
      break;
      case 5:
        this->ViewCardInfo( tnode->Data );
      break;
      default: ;
    }
  }
  */
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato dell'entit� selezionata
///
/// \date [29.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfSystemView::SystemTreeViewChange( TObject * Sender, TTreeNode * Node )
{
  if ( Node != NULL )
  {
    switch ( Node->Level )
    {
      case 0:
        this->ViewServerInfo( Node->Data );
        this->ViewServerStatus( Node->Data );
      break;
      case 1:
        this->ViewRegionInfo( Node->Data );
        this->ViewRegionStatus( Node->Data );
      break;
      case 2:
        this->ViewZoneInfo( Node->Data );
        this->ViewZoneStatus( Node->Data );
      break;
      case 3:
        this->ViewNodeInfo( Node->Data );
        this->ViewNodeStatus( Node->Data );
      break;
      case 4:
        this->ViewDeviceInfo( Node->Data );
        this->ViewDeviceStatus( Node->Data );
      break;
      case 5:
        this->ViewCardInfo( Node->Data );
        this->ViewCardStatus( Node->Data );     
      break;
      default: ;
    }
  }
}

//==============================================================================
/// Metodo per aggiungere un nodo ad un albero
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void * __fastcall TfSystemView::AddTreeNode( TTreeView * tree, TTreeNode * parent, void * item, AnsiString name, int icon )
{
  TTreeNode * node;

  node                 = tree->Items->AddChild( parent, name );
  node->ImageIndex     = node->Level*6 +1;
  node->SelectedIndex  = node->Level*6 +1;
  node->Data           = item;

  return (void*)node;
}

//==============================================================================
/// Metodo per modificare lo stato di un nodo
///
/// \date [12.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void * __fastcall TfSystemView::SetTreeNodeStatus( TTreeNode * node, int status )
{
  if ( node != NULL )
  {
    node->ImageIndex = status;
    node->SelectedIndex = status;
  }
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato di una scheda
///
/// \date [28.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewCardInfo( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_CARD    * card      = NULL;
  AnsiString    title     = "Informazioni";
  AnsiString    name      = "Nome sconosciuto";
  AnsiString    type      = "Tipo sconosciuto";
  AnsiString    info      = "Nessuna informazione";

  /* TODO -oEnrico -cGUI : Inserire un'icona per la scheda (card) */
  ilInfo->GetIcon( 5, this->imgInfo->Picture->Icon );
  card = (SPV_CARD*) data;
  if ( card != NULL )
  {
    title = "Informazioni sulla scheda";
    name  = AnsiString( card->Name );
  }

  this->gbInfo->Caption         = title;
  this->lName->Caption          = name;
  this->lType->Caption          = type;
  this->lInfo->Caption          = info;

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato di una periferica
///
/// \date [28.11.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewDeviceInfo( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_DEVICE  * device  = NULL                  ;
  AnsiString    title   = "Informazioni"        ;
  AnsiString    name    = "Nome sconosciuto"    ;
  AnsiString    type    = "Tipo sconosciuto"    ;
  AnsiString    info    = "Nessuna informazione";
  char        * UniID   = NULL                  ;

  device = (SPV_DEVICE*) data;
  if ( device != NULL )
  {
    title = "Informazioni sulla periferica";
    name  = AnsiString( device->Name );
    if ( device->Type != NULL )
    {
      type = ( device->Type->Name != NULL ) ? AnsiString( device->Type->Name ):AnsiString("Periferica generica ");
      type = type + " ver. ";
      type = type + (( device->Type->Version != NULL ) ? AnsiString( device->Type->Version ):AnsiString("sconosciuta"));
    }
    /*
    info = "";
    info = info + "ID: "                    + AnsiString(device->ID)                      + "\n";
    info = info + "Codice periferica: "     + AnsiString(device->Code)                    + "\n";
    info = info + "Serial Number: "         + AnsiString(device->SN)                      + "\n\n";
    info = info + "Nodo di appartenenza: "  + AnsiString(((SPV_NODE*)device->Node)->Name) + "\n\n";
    info = info + "Numero di schede: "      + AnsiString(device->CardCount)               + "\n";
    */

    UniID = SPVGetDeviceUniIDStr( device ); // -MALLOC

    info = "";
    info = info + AnsiString( "ID unico: " )              + AnsiString( UniID )                         + AnsiString( "\n" );
    info = info + AnsiString( "Serial Number: " )         + AnsiString( device->SN )                    + AnsiString( "\n\n" );
    info = info + AnsiString( "Nodo di appartenenza: " )  + AnsiString(((SPV_NODE*)device->Node)->Name) + AnsiString( "\n\n" );

    free( UniID ); // -FREE
  }

  ilInfo->GetIcon( 4, this->imgInfo->Picture->Icon );

  this->gbInfo->Caption         = title;
  this->lName->Caption          = name;
  this->lType->Caption          = type;
  this->lInfo->Caption          = info;

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato di un nodo
///
/// \date [22.05.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewNodeInfo( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_NODE    * node    = NULL;
  AnsiString    title   = "Informazioni";
  AnsiString    name    = "Nome sconosciuto";
  AnsiString    type    = "Tipo sconosciuto";
  AnsiString    info    = "Nessuna informazione";
  char        * UniID   = NULL                  ;

  node = (SPV_NODE*) data;
  if ( node != NULL )
  {
    title = "Informazioni sul nodo";
    name  = AnsiString( node->Name );
    type  = "Nodo di supervisione";

    UniID = SPVGetNodeUniIDStr( node ); // -MALLOC

    info = "";
    info = info + "ID unico: " + AnsiString( UniID ) + "\n";
    info = info + "Codice sistema: " + AnsiString( node->Code ) + "\n\n";

    info = info + "Zona di appartenenza: " + AnsiString(((SPV_ZONE*)node->Zone)->Name) + "\n\n";

    info = info + "Numero di periferiche: " + AnsiString( node->DeviceCount );

    free( UniID ); // -FREE
  }

  ilInfo->GetIcon( 3, this->imgInfo->Picture->Icon );

  this->gbInfo->Caption         = title;
  this->lName->Caption          = name;
  this->lType->Caption          = type;
  this->lInfo->Caption          = info;

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato di una zona
///
/// \date [22.05.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewZoneInfo( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_ZONE    * zone      = NULL;
  AnsiString    title     = "Informazioni";
  AnsiString    name      = "Nome sconosciuto";
  AnsiString    type      = "Tipo sconosciuto";
  AnsiString    info      = "Nessuna informazione";
  char        * UniID     = NULL                  ;

  zone = (SPV_ZONE*) data;
  if ( zone != NULL )
  {
    title = "Informazioni sulla zona";
    name  = AnsiString( zone->Name );
    type  = "Zona di supervisione";

    UniID = SPVGetZoneUniIDStr( zone ); // -MALLOC

    info = "";
    info = info + "ID unico: " + AnsiString( UniID ) + "\n";
    info = info + "Codice sistema: " + AnsiString( zone->Code ) + "\n\n";

    info = info + "Regione di appartenenza: " + AnsiString(((SPV_REGION*)zone->Region)->Name) + "\n\n";

    info = info + "Numero di nodi: " + AnsiString( zone->NodeCount );

    free( UniID ); // -FREE
  }

  ilInfo->GetIcon( 2, this->imgInfo->Picture->Icon );

  this->gbInfo->Caption         = title;
  this->lName->Caption          = name;
  this->lType->Caption          = type;
  this->lInfo->Caption          = info;

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato di una regione
///
/// \date [22.05.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewRegionInfo( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_REGION  * region    = NULL;
  AnsiString    title     = "Informazioni";
  AnsiString    name      = "Nome sconosciuto";
  AnsiString    type      = "Tipo sconosciuto";
  AnsiString    info      = "Nessuna informazione";
  char        * UniID     = NULL                  ;

  region = (SPV_REGION*) data;
  if ( region != NULL )
  {
    title = "Informazioni sulla regione";
    name  = AnsiString( region->Name );
    type  = "Regione di supervisione";

    UniID = SPVGetRegionUniIDStr( region ); // -MALLOC

    info = "";
    info = info + "ID unico: " + AnsiString( UniID ) + "\n";
    info = info + "Codice sistema: " + AnsiString( region->Code ) + "\n\n";

    info = info + "Server di appartenenza: " + AnsiString(((SPV_SERVER*)region->Server)->Name) + "\n\n";

    info = info + "Numero di zone: " + AnsiString( region->ZoneCount );

    free( UniID ); // -FREE
  }

  ilInfo->GetIcon( 1, this->imgInfo->Picture->Icon );

  this->gbInfo->Caption         = title;
  this->lName->Caption          = name;
  this->lType->Caption          = type;
  this->lInfo->Caption          = info;

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare le informazioni e lo stato di un server
///
/// \date [22.05.2006]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewServerInfo( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_SERVER  * server    = NULL;
  AnsiString    title     = "Informazioni";
  AnsiString    name      = "Nome sconosciuto";
  AnsiString    type      = "Tipo sconosciuto";
  AnsiString    info      = "Nessuna informazione";

  server  = (SPV_SERVER*) data;
  if ( server != NULL )
  {
    title = "Informazioni sul server";
    name  = AnsiString( server->Name );
    type  = "Server di diagnostica Telefin";

    info = "";
    info = info + "ID unico: " + AnsiString( server->SrvID ) + "\n";
    info = info + "Codice sistema: " + AnsiString( server->Host ) + "\n\n";

    info = info + "Numero di regioni: " + AnsiString( server->RegionCount );
  }

  ilInfo->GetIcon( 0, this->imgInfo->Picture->Icon );

  this->gbInfo->Caption         = title;
  this->lName->Caption          = name;
  this->lType->Caption          = type;
  this->lInfo->Caption          = info;

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di una scheda
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewCardStatus( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR;
  SPV_CARD    * card      = NULL;
  AnsiString    title     = "Stato";
  AnsiString    status    = "Stato sconosciuto";
  AnsiString    stream    = "Nessun dato";
  AnsiString    level     = "Livello: n/d";
  TColor        color     = clWindowText;

  card = (SPV_CARD*) data;
  if ( card != NULL )
  {
    title   = "Stato della scheda";
    status  = "Scheda in funzione";
    SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
    level   = "Livello: " + AnsiString(card->Level);
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_CARD;
  }

  this->gbStatus->Caption         = title;
  this->lStatus->Caption          = status;
  this->lLevel->Caption           = level;
  this->lStatus->Font->Color      = color;
  this->mStatus->Lines->Clear();
  this->mStatus->Lines->Add(stream);

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di una periferica
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewDeviceStatus( void * data )
{
  int                     ret_code  = SPV_SYSTEMVIEW_NO_ERROR ;
  SPV_DEVICE            * device    = NULL                    ;
  AnsiString              title     = "Stato"                 ;
  AnsiString              status    = "Stato sconosciuto"     ;
  AnsiString              stream    = "Nessun dato"           ;
  AnsiString              level     = "Livello: n/d"          ;
  TColor                  color     = clWindowText            ;
  SPV_DEV_STREAM        * pstream   = NULL                    ;
  char                  * sbuffer   = NULL                    ;
  SPV_DEV_STREAM_FIELD  * pfield    = NULL                    ;
  char                  * pvalue    = NULL                    ;
  AnsiString              field     = ""                      ;
  AnsiString              value     = ""                      ;

  device = (SPV_DEVICE*) data;
  if ( device != NULL )
  {
    title = "Stato della periferica";
    if ( device->SupStatus.Active )
    {
      if ( device->SupStatus.Alarm )
      {
        status  = "Periferica in allarme";
        SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_ALARM );
        color   = clRed;
        this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_ALARM );
      }
      else
      {
        if ( device->SupStatus.Offline )
        {
          status  = "Periferica offline";
          SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OFFLINE );
          color   = clRed;
          this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_OFFLINE );
        }
        else
        {
          if ( device->SupStatus.Unknown )
          {
            status  = "Periferica sconosciuta";
            SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_UNKNOWN );
            color   = clBlue;
            this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_UNKNOWN );
          }
          else
          {
            if ( device->SupStatus.Level == 0 )
            {
              status  = "Periferica in funzione";
              SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
              this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_OK );
            }
            if ( device->SupStatus.Level == 1 )
            {
              status  = "Periferica in attenzione";
              SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_WARNING );
              color   = (TColor)0x0000AAFF;
              this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_WARNING );
            }
            if ( device->SupStatus.Level == 2 )
            {
              status  = "Periferica in errore";
              SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_ERROR );
              color   = clRed;
              this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_ERROR );
            }
          }
        }
      }
      level = "Livello: " + AnsiString( device->SupStatus.Level );
    }
    else
    {
      status  = "Diagnostica disabilitata";
      SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_IDLE );
      this->SetTreeNodeStatus( (TTreeNode*)device->Tag, SPV_DEVICE_DISABLED );
    }
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_DEVICE;
  }

  if ( device->StreamCount > 0 )
  {
    stream = AnsiString( "Lista dati ricevuti:" );
  }

  this->gbStatus->Caption         = title;
  this->lStatus->Caption          = status;
  this->lLevel->Caption           = level;
  this->lStatus->Font->Color      = color;
  this->mStatus->Lines->Clear();
  this->mStatus->Lines->Add( stream );
  this->mStatus->Lines->Add( "" );

  //* SPERIMENTALE
  for ( int s = 0; s < device->StreamCount; s++ )
  {
    pstream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, s+1 );
    if ( pstream != NULL )
    {
      stream  = AnsiString( "[" ) + AnsiString( pstream->ID ) + AnsiString( "] " )
              + AnsiString( pstream->Name ) + AnsiString( " : " );

      if ( pstream->Buffer.Len > 0 && pstream->Buffer.Ptr != NULL )
      {
        sbuffer = (char*)malloc( sizeof(char) * ( pstream->Buffer.Len + 1 ) ); // -MALLOC
        memcpy( sbuffer, (char*)pstream->Buffer.Ptr, sizeof(char) * pstream->Buffer.Len );
        sbuffer[pstream->Buffer.Len] = '\0';
        stream = stream + AnsiString( sbuffer ) + AnsiString( " (Livello: " ) + AnsiString( pstream->SevLevel ) + AnsiString( ")" );
        this->mStatus->Lines->Add( stream );
        free( sbuffer ); // -FREE
        field = "";
        // --- Ciclo sulla lista dei campi ---
        for ( int f = 0; f < pstream->FieldNum; f++ )
        {
          // --- Recupero il ptr ad una struttura campo dello stream ---
          pfield = SPVGetDevField( pstream, f );
          if ( pfield != NULL )
          {
            value = "";
            for ( int c = 0; c < pfield->Capacity; c++ )
            {
              pvalue = pfield->Value[c];
              if ( pvalue != NULL )
              {
                value = AnsiString( "� " ) + AnsiString( pfield->Name ) + AnsiString( "[" )
                      + AnsiString( c ) + AnsiString( "]: " ) + AnsiString( pvalue )
                      + AnsiString( " (Livello: " ) + AnsiString( pfield->SevLevel[c] ) + AnsiString( ")" );
                pvalue = pfield->Descr[c];
                if ( pvalue != NULL )
                {
                  value = value + AnsiString( " := " ) + AnsiString( pvalue );
                }
                else
                {
                  value = value + AnsiString( " := " ) + AnsiString( "Valore sconosciuto" );
                }
                this->mStatus->Lines->Add( value );
              }
              else
              {
                value = AnsiString( "� " ) + AnsiString( "n/d" );
                this->mStatus->Lines->Add( value );
              }
            }
          }
          else
          {
            field = field + AnsiString( "� " ) + AnsiString( "n/d" );
            this->mStatus->Lines->Add( field );
          }
        }
      }
      else
      {
        stream = stream + AnsiString( "<non ricevuto>" ) + AnsiString( " (" ) + AnsiString( "n/d" ) + AnsiString( ")" );
        this->mStatus->Lines->Add( stream );
      }
    }
  }
  //*/

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di un nodo
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewNodeStatus( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR ;
  SPV_NODE    * node      = NULL                    ;
  AnsiString    title     = "Stato"                 ;
  AnsiString    status    = "Stato sconosciuto"     ;
  AnsiString    stream    = "Nessun dato"           ;
  AnsiString    level     = "Livello: n/d"          ;
  TColor        color     = clWindowText            ;

  node = (SPV_NODE*) data;
  if ( node != NULL )
  {
    title   = "Stato del nodo";
    status  = "Nodo in funzione";
    SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
    level   = "Livello: " + AnsiString(node->Level);
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_NODE;
  }

  this->gbStatus->Caption         = title ;
  this->lStatus->Caption          = status;
  this->lLevel->Caption           = level ;
  this->lStatus->Font->Color      = color ;
  this->mStatus->Lines->Clear();
  this->mStatus->Lines->Add(stream);

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di una zona
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewZoneStatus( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR ;
  SPV_ZONE    * zone      = NULL                    ;
  AnsiString    title     = "Stato"                 ;
  AnsiString    status    = "Stato sconosciuto"     ;
  AnsiString    stream    = "Nessun dato"           ;
  AnsiString    level     = "Livello: n/d"          ;
  TColor        color     = clWindowText            ;

  zone = (SPV_ZONE*) data;
  if ( zone != NULL )
  {
    title   = "Stato della zona";
    status  = "Zona in funzione";
    SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
    level   = "Livello: " + AnsiString(zone->Level);
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_ZONE;
  }

  this->gbStatus->Caption         = title;
  this->lStatus->Caption          = status;
  this->lLevel->Caption           = level;
  this->lStatus->Font->Color      = color;
  this->mStatus->Lines->Clear();
  this->mStatus->Lines->Add(stream);

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di una regione
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewRegionStatus( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR ;
  SPV_REGION  * region    = NULL;
  AnsiString    title     = "Stato";
  AnsiString    status    = "Stato sconosciuto";
  AnsiString    stream    = "Nessun dato";
  AnsiString    level     = "Livello: n/d";
  TColor        color     = clWindowText;

  region = (SPV_REGION*) data;
  if ( region != NULL )
  {
    title   = "Stato della regione";
    status  = "Regione in funzione";
    SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
    level   = "Livello: " + AnsiString(region->Level);
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_REGION;
  }

  this->gbStatus->Caption         = title ;
  this->lStatus->Caption          = status;
  this->lLevel->Caption           = level ;
  this->lStatus->Font->Color      = color ;
  this->mStatus->Lines->Clear();
  this->mStatus->Lines->Add(stream);

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di un server
///
/// \date [01.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewServerStatus( void * data )
{
  int           ret_code  = SPV_SYSTEMVIEW_NO_ERROR ;
  SPV_SERVER  * server    = NULL                    ;
  AnsiString    title     = "Stato"                 ;
  AnsiString    status    = "Stato sconosciuto"     ;
  AnsiString    stream    = "Nessun dato"           ;
  AnsiString    level     = "Livello: n/d"          ;
  TColor        color     = clWindowText            ;

  server = (SPV_SERVER*) data;
  if ( server != NULL )
  {
    title   = "Stato del server";
    status  = "Server in funzione";
    SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
    level   = "Livello: " + AnsiString(server->Level);
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_SERVER;
  }

  this->gbStatus->Caption         = title;
  this->lStatus->Caption          = status;
  this->lLevel->Caption           = level;
  this->lStatus->Font->Color      = color;
  this->mStatus->Lines->Clear();
  this->mStatus->Lines->Add(stream);

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare l'albero della periferica
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewDeviceTree( TTreeView * tree, void * pdevice )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_DEVICE  * device    = NULL; // Ptr a struttura periferica
  SPV_CARD    * card      = NULL; // Ptr a struttura scheda

  device = (SPV_DEVICE*)pdevice;
  if ( device != NULL )
  {
    for ( int c = 0; c < device->CardCount; c++ )
    {
      card = &device->CardList[c];
      if ( card != NULL )
      {
        card->Tag = (void*) this->AddTreeNode( tree, (TTreeNode*)device->Tag, (void*)card, AnsiString(card->Name), 0 );
      }
      else
      {
        c = device->CardCount; // Fine del ciclo for
        ret_code = SPV_SYSTEMVIEW_INVALID_CARD;
      }
    }
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare l'albero del nodo
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewNodeTree( TTreeView * tree, void * pnode )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_NODE    * node      = NULL; // Ptr a struttura nodo
  SPV_DEVICE  * device    = NULL; // Ptr a struttura periferica

  node = (SPV_NODE*) pnode;
  if ( node != NULL )
  {
    for ( int d = 0; d < node->DeviceCount; d++ )
    {
      device = &node->DeviceList[d];
      if ( device != NULL )
      {
        device->Tag = (void*)this->AddTreeNode( tree, (TTreeNode*)node->Tag, (void*)device, AnsiString(device->Name), 0 );
        this->ViewDeviceTree( tree, (void*)device );
      }
      else
      {
        d = node->DeviceCount; // Fine del ciclo for
        ret_code = SPV_SYSTEMVIEW_INVALID_DEVICE;
      }
    }
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_NODE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare l'albero della zona
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewZoneTree( TTreeView * tree, void * pzone )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_ZONE    * zone      = NULL; // Ptr a struttura zone
  SPV_NODE    * node      = NULL; // Ptr a struttura node

  zone = (SPV_ZONE*) pzone;
  if ( zone != NULL )
  {
    for ( int n = 0; n < zone->NodeCount; n++ )
    {
      node = &zone->NodeList[n];
      if ( node != NULL )
      {
        node->Tag = (void*) this->AddTreeNode( tree, (TTreeNode*)zone->Tag, (void*)node, AnsiString(node->Name), 0 );
        this->ViewNodeTree( tree, node );
      }
      else
      {
        n = zone->NodeCount;
        ret_code = SPV_SYSTEMVIEW_INVALID_NODE;
      }
    }
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_ZONE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare l'albero della regione
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewRegionTree( TTreeView * tree, void * pregion )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_REGION  * region    = NULL; // Ptr a struttura regione
  SPV_ZONE    * zone      = NULL; // Ptr a struttura zone

  region = (SPV_REGION*)pregion;
  if ( region != NULL )
  {
    for ( int z = 0; z < region->ZoneCount; z++ )
    {
      zone = &region->ZoneList[z];
      if ( zone != NULL )
      {
        zone->Tag = (void*) this->AddTreeNode( tree, (TTreeNode*)region->Tag, (void*)zone, AnsiString(zone->Name), 0 );
        this->ViewZoneTree( tree, zone );
      }
      else
      {
        z = region->ZoneCount; // Fine del ciclo for
        ret_code = SPV_SYSTEMVIEW_INVALID_ZONE;
      }
    }
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_REGION;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzate l'albero del server
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewServerTree( TTreeView * tree, void * pserver )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr a struttura server
  SPV_REGION  * region    = NULL; // Ptr a struttura region

  server = (SPV_SERVER*)pserver;
  if ( server != NULL )
  {
    for ( int r = 0; r < server->RegionCount; r++ )
    {
      region = &server->RegionList[r];
      if ( region != NULL )
      {
        region->Tag = (void*) this->AddTreeNode( tree, (TTreeNode*)server->Tag, (void*)region, AnsiString(region->Name), 0 );
        this->ViewRegionTree( tree, region );
      }
      else
      {
        r = server->RegionCount; // Fine del ciclo for
        ret_code = SPV_SYSTEMVIEW_INVALID_REGION;
      }
    }
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_SERVER;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare l'albero del sistema
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ViewSystemTree( TTreeView * tree, void * psystem )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SYSTEM  * system    = NULL; // Ptr ad una struttura sistema
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server

  system = (SPV_SYSTEM*) psystem;
  if ( system != NULL )
  {
    for ( int s = 0; s < system->ServerCount; s++ )
    {
      server = &system->ServerList[s];
      if ( server != NULL )
      {
        server->Tag = (void*) this->AddTreeNode( tree, NULL, (void*)server, AnsiString(server->Name), 0 );
        tree->Select( (TTreeNode*) server->Tag );
        this->ViewServerTree( tree, server );
      }
      else
      {
        s = system->ServerCount;
        ret_code = SPV_SYSTEMVIEW_INVALID_SERVER;
      }
    }
    tree->FullExpand();
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_SYSTEM;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per rimuovere un nodo da un albero
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::RemoveTreeNode( TTreeView * tree, TTreeNode * node )
{
  int           ret_code  = 0   ; // Codice di ritorno

  if ( node != NULL )
  {
    node->Delete();
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_TREE_NODE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per cancellare l'albero della periferica
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ClearDeviceTree( TTreeView * tree, void * pdevice )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_DEVICE  * device    = NULL; // Ptr ad una struttura periferica

  device = (SPV_DEVICE*)pdevice;

  if ( device != NULL )
  {
    ret_code = this->RemoveTreeNode( tree, (TTreeNode*)device->Tag );
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_DEVICE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per cancellare l'albero del nodo
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ClearNodeTree( TTreeView * tree, void * pnode )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_NODE    * node      = NULL; // Ptr ad una struttura nodo

  node = (SPV_NODE*)pnode;

  if ( node != NULL )
  {
    ret_code = this->RemoveTreeNode( tree, (TTreeNode*)node->Tag );
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_NODE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per cancellare l'albero della zona
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ClearZoneTree( TTreeView * tree, void * pzone )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_ZONE    * zone      = NULL; // Ptr ad una struttura zona

  zone = (SPV_ZONE*)pzone;

  if ( zone != NULL )
  {
    ret_code = this->RemoveTreeNode( tree, (TTreeNode*)zone->Tag );
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_ZONE;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per cancellare l'albero della regione
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ClearRegionTree( TTreeView * tree, void * pregion )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_REGION  * region    = NULL; // Ptr ad una struttura regione

  region = (SPV_REGION*)pregion;

  if ( region != NULL )
  {
    ret_code = this->RemoveTreeNode( tree, (TTreeNode*)region->Tag );
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_REGION;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per cancellare l'albero del server
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ClearServerTree( TTreeView * tree, void * pserver )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SERVER  * server    = NULL; // Ptr ad una struttura server

  server = (SPV_SERVER*)pserver;

  if ( server != NULL )
  {
    ret_code = this->RemoveTreeNode( tree, (TTreeNode*)server->Tag );
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_SERVER;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per cancellare l'albero del sistema
///
/// \date [27.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfSystemView::ClearSystemTree( TTreeView * tree, void * psystem )
{
  int           ret_code  = 0   ; // Codice di ritorno
  SPV_SYSTEM  * system    = NULL; // Ptr ad una struttura sistema

  system = (SPV_SYSTEM*)psystem;
  if ( system != NULL )
  {
    tree->Items->Clear();
  }
  else
  {
    ret_code = SPV_SYSTEMVIEW_INVALID_SYSTEM;
  }

  return ret_code;
}


//==============================================================================
/// Metodo che gestisce l'evento di pop-up
///
/// \date [07.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void __fastcall TfSystemView::popSystemPopup( TObject * Sender )
{
  int             fun_code    = SPV_SYSTEMVIEW_NO_ERROR;
  int             node_level  = -1  ;
  TTreeNode     * node        = NULL;
  SPV_DEVICE    * device      = NULL;
  void          * data        = NULL;

  if ( Sender != NULL )
  {
    node = this->SystemTreeView->Selected; // Recupero il puntatore al nodo selezionato
    if ( node != NULL )
    {
      switch ( this->popSystem->Tag )
      {
        case SPV_SYSTEMVIEW_SERVER_OBJECT_LEVEL:
          this->popSystem->Items->Clear();
        break;
        case SPV_SYSTEMVIEW_REGION_OBJECT_LEVEL:
          this->popSystem->Items->Clear();
        break;
        case SPV_SYSTEMVIEW_ZONE_OBJECT_LEVEL:
          this->popSystem->Items->Clear();
        break;
        case SPV_SYSTEMVIEW_NODE_OBJECT_LEVEL:
          this->popSystem->Items->Clear();
        break;
        case SPV_SYSTEMVIEW_DEVICE_OBJECT_LEVEL:
          fun_code  = SPVClearDevicePopUpMenu( (void*)this->popSystem );
        break;
        default:
          this->popSystem->Items->Clear();
      }
      node_level = node->Level; // Recupero il livello del nodo selezionato
      switch ( node_level )
      {
        case SPV_SYSTEMVIEW_SERVER_OBJECT_LEVEL:
        break;
        case SPV_SYSTEMVIEW_REGION_OBJECT_LEVEL:
        break;
        case SPV_SYSTEMVIEW_ZONE_OBJECT_LEVEL:
        break;
        case SPV_SYSTEMVIEW_NODE_OBJECT_LEVEL:
        break;
        case SPV_SYSTEMVIEW_DEVICE_OBJECT_LEVEL:
          data      = node->Data;
          device    = (SPV_DEVICE*)data;
          fun_code  = SPVBuildDevicePopUpMenu( (void*)this->popSystem, device );
        break;
      }
      this->popSystem->Tag = node_level;
    }
  }
}

//==============================================================================
/// Metodo che gestisce l'evento di click su un elemento del popup menu
///
/// \date [25.01.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfSystemView::popSystemClick( TObject * Sender )
{
  TMenuItem         * item      = (TMenuItem*)Sender;
  SPV_GUI_FUNCTION  * function  = NULL              ;

  if ( item != NULL )
  {
    function = ( SPV_GUI_FUNCTION * )item->Tag;
    if ( function != NULL )
    {
      if ( function->ptr != NULL )
      {
        function->ptr(function->params);
      }
    }
  }
}

//==============================================================================
/// Metodo che gestisce l'evento di click su un elemento propriet� periferica
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfSystemView::popDeviceClick( TObject * Sender )
{
  TMenuItem         * item      = (TMenuItem*)Sender;
  SPV_DEVICE        * device  = NULL              ;

  if ( item != NULL )
  {
    device = ( SPV_DEVICE * )item->Tag;
    if ( device != NULL )
    {
    }
  }
}


void __fastcall TfSystemView::SystemTreeViewMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  TTreeNode   * node        = NULL          ;
  TShiftState   shift_state = TShiftState() ;

  if ( Button == mbRight )
  {
    node = this->SystemTreeView->GetNodeAt( X, Y );
    if ( node != NULL )
    {
      this->SystemTreeView->Select( node, shift_state );
    }
  }
}
//---------------------------------------------------------------------------

//==============================================================================
///
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfSystemView::ToggleDeviceActive( TObject * Sender )
{
  TMenuItem         * item      = (TMenuItem*)Sender;
  SPV_DEVICE        * device  = NULL              ;

  if ( item != NULL )
  {
    device = ( SPV_DEVICE * )item->Tag;
    if ( device != NULL )
    {
      device->SupStatus.Active = !device->SupStatus.Active;
    }
  }
}

//==============================================================================
///
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfSystemView::ToggleDeviceScheduled( TObject * Sender )
{
  TMenuItem         * item      = (TMenuItem*)Sender;
  SPV_DEVICE        * device  = NULL              ;

  if ( item != NULL )
  {
    device = ( SPV_DEVICE * )item->Tag;
    if ( device != NULL )
    {
      device->SupStatus.Scheduled = !device->SupStatus.Scheduled;
    }
  }
}


//==============================================================================
///
/// \date [21.07.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfSystemView::SystemTreeViewDblClick(TObject *Sender)
{
  /*
  TTreeNode * node = NULL ;

  node = this->SystemTreeView->Selected;
  if ( node != NULL )
  {
    // --- Caso device ---
    if ( node->Level == 4 )
    {
      fDeviceView->pdevice = (void*) node->Data;
      fDeviceView->ShowModal();
    }
  }
  */
}




//==============================================================================
// Telefin Supervisor DB View Form 1.0
//------------------------------------------------------------------------------
// Header del Form dei database (SPVDBView.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.03 (21.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVDBView.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [21.09.2004]:
// - Prima versione del Form.
// 0.02 [22.09.2005]:
// - Modificato il metodo costruttore TfDatabaseView::TfDatabaseView.
// - Aggiunto il metodo TfDatabaseView::FormResize.
// 0.03 [19.12.2005]:
// - Modificato il metodo costruttore TfDatabaseView::TfDatabaseView.
//==============================================================================

#ifndef SPVDBViewFormH
#define SPVDBViewFormH

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>


//==============================================================================
// Classi
//------------------------------------------------------------------------------
class TfDatabaseView : public TForm
{
__published:	// IDE-managed Components
  TStatusBar *sbDBs;
  TGroupBox   * gbStatus;
  TImage      * imgStatus;
  TLabel      * lStatus;
  TLabel *lDBNum;
  TPanel      * pBluLineStatus;
  TStringGrid * sgDBs;
  void __fastcall FormResize(TObject *Sender);
private:	// User declarations
public:		// User declarations
  __fastcall TfDatabaseView(TComponent* Owner);
};

//==============================================================================
// Varibili globali
//------------------------------------------------------------------------------
extern PACKAGE TfDatabaseView *fDatabaseView;

#endif

//==============================================================================
// Telefin Supervisor DB View Form 1.0
//------------------------------------------------------------------------------
// Form dei database (SPVDBView.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.03 (21.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVDBView.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVDBView.h
//==============================================================================

#include <vcl.h>
#pragma hdrstop

#include "SPVDBViewForm.h"
#include "SPVServerGUI.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
TfDatabaseView *fDatabaseView;

//==============================================================================
// Metodi
//------------------------------------------------------------------------------

//==============================================================================
/// Metodo costruttore della classe TfDatabaseView.
///
/// \date [20.12.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
__fastcall TfDatabaseView::TfDatabaseView(TComponent* Owner) : TForm(Owner)
{
  // --- Dimensioni form ---
  this->Left                    = 0;
  this->Top                     = 400;
  this->Width                   = 320;
  this->Height                  = 200;
  // --- Colori form ---
  this->Color                   = (TColor)0x00D8E9EC;

  // --- Dimensioni gbStatus ---
  this->gbStatus->Left          = 4;
  this->gbStatus->Top           = 4;
  this->gbStatus->Width         = 303;
  this->gbStatus->Height        = 145;
  // --- Colori gbStatus ---
  this->gbStatus->Color         = (TColor)0x00D8E9EC;
  // --- Font gbStatus ---
  this->gbStatus->Font->Color   = (TColor)0x00C56A31;
  this->gbStatus->Font->Name    = "MS Sans Serif";
  this->gbStatus->Font->Size    = 8;

  // --- Dimensioni lStatus ---
  this->lStatus->Left           = 30;
  this->lStatus->Top            = 16;
  this->lStatus->Width          = 143;
  this->lStatus->Height         = 18;
  // --- Colori lStatus ---
  this->lStatus->Color          = (TColor)0x00D8E9EC;
  // --- Font lStatus ---
  this->lStatus->Font->Color    = (TColor)0x00000000;
  this->lStatus->Font->Name     = "Trebuchet MS";
  this->lStatus->Font->Size     = 8;

  // --- Dimensioni lDBNum ---
  this->lDBNum->Left            = 132;
  this->lDBNum->Top             = 16;
  this->lDBNum->Width           = 167;
  this->lDBNum->Height          = 16;
  // --- Colori lDBNum ---
  this->lDBNum->Color           = (TColor)0x00D8E9EC;
  // --- Font lDBNum ---
  this->lDBNum->Font->Color     = (TColor)0x00888888;
  this->lDBNum->Font->Name      = "Trebuchet MS";
  this->lDBNum->Font->Size      = 8;

  // --- Dimensioni pBluLineStatus ---
  this->pBluLineStatus->Left    = 2;
  this->pBluLineStatus->Top     = 39;
  this->pBluLineStatus->Width   = 299;
  this->pBluLineStatus->Height  = 3;
  // --- Colori pBluLineStatus ---
  this->pBluLineStatus->Color   = (TColor)0x00C56A31;

  // --- Dimensioni sgDBs ---
  this->sgDBs->Left             = 2;
  this->sgDBs->Top              = 42;
  this->sgDBs->Width            = 299;
  this->sgDBs->Height           = 101;
  // --- Colori sgServer ---
  this->sgDBs->Color            = (TColor)0x00CCFFFF;
  // --- Font sbServer ---
  this->sgDBs->Font->Color      = (TColor)0x00000000;
  this->sgDBs->Font->Name       = "MS Sans Serif";
  this->sgDBs->Font->Size       = 8;

  // --- Colori sbServer ---
  this->sbDBs->Color            = (TColor)0x00D8E9EC;
  // --- Font sbServer ---
  this->sbDBs->Font->Color      = (TColor)0x00000000;
  this->sbDBs->Font->Name       = "MS Sans Serif";
  this->sbDBs->Font->Size       = 8;

  // --- Inizializzo la tabella dei comandi ---
  this->sgDBs->ColCount         = 3;
  this->sgDBs->RowCount         = 2;
  this->sgDBs->DefaultRowHeight = 17;
  this->sgDBs->Cells[0][0]      = "Host";
  this->sgDBs->Cells[1][0]      = "Tipo";
  this->sgDBs->Cells[2][0]      = "Stato";

  // --- Visualizzazione statica di prova ---
  this->sgDBs->Cells[0][1]      = "localhost";
  this->sgDBs->Cells[1][1]      = "MS SQL Server 8.0";
  this->sgDBs->Cells[2][1]      = "Attivo";
}

//==============================================================================
/// Metodo per catturare l'evento OnResize del form
///
/// \date [22.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfDatabaseView::FormResize(TObject *Sender)
{
  // --- Ridimensionamento automatico della tabella comandi ---
  SPVTableAutoSize( (void*)this->sgDBs );

}
//---------------------------------------------------------------------------


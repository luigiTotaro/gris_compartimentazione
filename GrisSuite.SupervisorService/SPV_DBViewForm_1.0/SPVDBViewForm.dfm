object fDatabaseView: TfDatabaseView
  Left = 0
  Top = 400
  Width = 320
  Height = 200
  Caption = 'Finestra Stato Database'
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = FormResize
  DesignSize = (
    312
    166)
  PixelsPerInch = 96
  TextHeight = 13
  object sbDBs: TStatusBar
    Left = 0
    Top = 147
    Width = 312
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object gbStatus: TGroupBox
    Left = 4
    Top = 4
    Width = 303
    Height = 145
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Elenco dei database'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      303
      145)
    object imgStatus: TImage
      Left = 10
      Top = 16
      Width = 16
      Height = 16
      Transparent = True
    end
    object lStatus: TLabel
      Left = 30
      Top = 16
      Width = 143
      Height = 18
      AutoSize = False
      Caption = 'DB attivi'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lDBNum: TLabel
      Left = 174
      Top = 16
      Width = 121
      Height = 16
      Alignment = taRightJustify
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'Numero DB: n/d'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = cl3DDkShadow
      Font.Height = -11
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object pBluLineStatus: TPanel
      Left = 2
      Top = 39
      Width = 299
      Height = 3
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      Color = clHotLight
      TabOrder = 0
    end
    object sgDBs: TStringGrid
      Left = 2
      Top = 42
      Width = 299
      Height = 101
      TabStop = False
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsNone
      Color = cl3DLight
      ColCount = 3
      DefaultRowHeight = 17
      FixedCols = 0
      RowCount = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridLineWidth = 0
      Options = [goFixedVertLine, goFixedHorzLine, goDrawFocusSelected, goColSizing, goRowSelect]
      ParentFont = False
      ScrollBars = ssNone
      TabOrder = 1
      ColWidths = (
        117
        74
        109)
    end
  end
end

//==============================================================================
// Telefin Supervisor Port View Form 1.0
//------------------------------------------------------------------------------
// Form sinottico delle porte di comunicazione (SPVPortViewForm.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.10 (30.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVPortViewForm.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVPortViewForm.h
//==============================================================================
#include <vcl.h>
#pragma hdrstop
#include "SPVPortViewForm.h"
#include "SPVComProt.h"
#include "SPVServerGUI.h"
#include "SPVPortMonitorForm.h"
#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
TfPortView * fPortView;

//==============================================================================
// Metodi e Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Metodo costruttore della classe TfPortView
///
/// \date [19.12.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
__fastcall TfPortView::TfPortView( TComponent * Owner ):TForm( Owner )
{
  this->icoApp          = new TIcon;

  this->sgPorts->DefaultRowHeight = 17;
  this->sgPorts->ColCount = 3;
  this->sgPorts->RowCount = 2;
  this->sgPorts->Cells[0][0]  = "      Nome";
  this->sgPorts->Cells[1][0]  = "Tipo";
  this->sgPorts->Cells[2][0]  = "Stato";

  this->sgPorts->Cells[0][1]  = "<nessuna porta>";

  // --- Dimensioni form ---
  this->Left    = 0;
  this->Top     = 600;
  this->Width   = 320;
  this->Height  = 200;
  // --- Colori form ---
  this->Color   = (TColor)0x00D8E9EC;

  // --- Dimensioni gbStatus ---
  this->gbStatus->Left        = 4;
  this->gbStatus->Top         = 4;
  this->gbStatus->Width       = 303;
  this->gbStatus->Height      = 145;
  // --- Colori gbStatus ---
  this->gbStatus->Color       = (TColor)0x00D8E9EC;
  // --- Font gbStatus ---
  this->gbStatus->Font->Color = (TColor)0x00C56A31;
  this->gbStatus->Font->Name  = "MS Sans Serif";
  this->gbStatus->Font->Size  = 8;

  // --- Dimensioni lStatus ---
  this->lStatus->Left         = 30;
  this->lStatus->Top          = 16;
  this->lStatus->Width        = 143;
  this->lStatus->Height       = 18;
  // --- Colori lStatus ---
  this->lStatus->Color        = (TColor)0x00D8E9EC;
  // --- Font lStatus ---
  this->lStatus->Font->Color  = (TColor)0x00000000;
  this->lStatus->Font->Name   = "Trebuchet MS";
  this->lStatus->Font->Size   = 8;

  // --- Dimensioni lPortNum ---
  this->lPortNum->Left          = 132;
  this->lPortNum->Top           = 16;
  this->lPortNum->Width         = 167;
  this->lPortNum->Height        = 16;
  // --- Colori lPortNum ---
  this->lPortNum->Color         = (TColor)0x00D8E9EC;
  // --- Font lPortNum ---
  this->lPortNum->Font->Color   = (TColor)0x00888888;
  this->lPortNum->Font->Name    = "Trebuchet MS";
  this->lPortNum->Font->Size    = 8;

  // --- Dimensioni pBluLineStatus ---
  this->pBluLineStatus->Left   = 2;
  this->pBluLineStatus->Top    = 39;
  this->pBluLineStatus->Width  = 299;
  this->pBluLineStatus->Height = 3;
  // --- Colori pBluLineStatus ---
  this->pBluLineStatus->Color  = (TColor)0x00C56A31;

  // --- Dimensioni sgPorts ---
  this->sgPorts->Left           = 2;
  this->sgPorts->Top            = 42;
  this->sgPorts->Width          = 299;
  this->sgPorts->Height         = 101;
  // --- Colori sgPorts ---
  this->sgPorts->Color       = (TColor)0x00CCFFFF;
  // --- Font sgPorts ---
  this->sgPorts->Font->Color = (TColor)0x00000000;
  this->sgPorts->Font->Name  = "MS Sans Serif";
  this->sgPorts->Font->Size  = 8;

  // --- Colori sbPorts ---
  this->sbPorts->Color       = (TColor)0x00D8E9EC;
  // --- Font sbPorts ---
  this->sbPorts->Font->Color = (TColor)0x00000000;
  this->sbPorts->Font->Name  = "MS Sans Serif";
  this->sbPorts->Font->Size  = 8;
}

//==============================================================================
/// Metodo per visualizzare lo stato di una porta
///
/// \date [13.02.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int __fastcall TfPortView::ViewPortStatus( int ID )
{
  int                 ret_code  = SPV_PORTVIEW_NO_ERROR;
  SPV_COMPROT_PORT  * port      = NULL;
  AnsiString          name      = "n/d";
  AnsiString          type      = AnsiString( SYS_KERNEL_PORT_TYPE_NAME_UNKNOWN );
  AnsiString          status    = "n/d";
  char              * type_str  = NULL;

  if ( ID >= 0 && ID < SPVPortCount )
  {
    port = &SPVPortList[ID];
    if ( port != NULL )
    {
      if ( port->Port != NULL )
      {
        name     = "      " + AnsiString(port->Port->Name);
        if ( port->Port->Active == true ) status = "Attiva";
        else status = "Non attiva";
        type_str = SPVGetPortTypeName( port->Port->Type );
        if ( type_str != NULL )
        {
          type = AnsiString( type_str );
          free( type_str );
        }
      }
      else
      {
        ret_code = SPV_PORTVIEW_INVALID_PORT_STRUCT;
      }
    }
    else
    {
      ret_code = SPV_PORTVIEW_INVALID_PORT;
    }

    this->sgPorts->Cells[0][ID+1] = name    ;
    this->sgPorts->Cells[1][ID+1] = type    ;
    this->sgPorts->Cells[2][ID+1] = status  ;
  }
  else
  {
    ret_code = SPV_PORTVIEW_INVALID_PORT_ID;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per visualizzare lo stato di tutte le porte del sistema
///
/// \date [30.11.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int __fastcall TfPortView::ViewSystemPortStatus( void )
{
  int ret_code = SPV_PORTVIEW_NO_ERROR;
  int err_count = 0;

  if ( SPVPortList != NULL && SPVPortCount > 0 )
  {
    this->lPortNum->Caption = "Numero porte: " + AnsiString( SPVPortCount );
    this->sgPorts->RowCount = SPVPortCount+1;
    for ( int p = 0; p < SPVPortCount; p++ )
    {
      ret_code = this->ViewPortStatus( p );
      if ( ret_code != 0 ) err_count++;
    }
    if ( err_count > 0 )
    {
      SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_ERROR );
      this->lStatus->Caption = "Porte in errore ("+AnsiString(err_count)+")";
    }
    else
    {
      SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_OK );
      this->lStatus->Caption = "Porte in funzione";
    }
  }
  else
  {
    SPVSetStatusIcon( (void*)this->imgStatus->Picture->Icon, SPV_SERVERGUI_STATUS_ICON_WARNING );
    this->lStatus->Caption = "Nessuna porta";
    ret_code = SPV_PORTVIEW_INVALID_PORT_LIST;
  }

  return ret_code;
}

//==============================================================================
/// Metodo per eseguire l'autodimensionamento della tabella di stato
///
/// \date [30.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfPortView::ViewAutoSize( void )
{
  int               ret_code      = 0 ;
  int               table_width   = 0 ;
  int               col1_width    = 0 ;
  int               col2_width    = 0 ;
  int               col3_width    = 0 ;
  int               string_width  = 0 ;
  int               rem_width     = 0 ;

  table_width = this->sgPorts->ClientWidth;

  for ( int r = 0; r < this->sgPorts->RowCount; r++ )
  {
    string_width = clv->StringWidth( this->sgPorts->Cells[0][r] ) + 8;
    col1_width = (string_width > col1_width) ? string_width:col1_width;
    string_width = clv->StringWidth( this->sgPorts->Cells[1][r] ) + 8;
    col2_width = (string_width > col2_width) ? string_width:col2_width;
    string_width = clv->StringWidth( this->sgPorts->Cells[2][r] ) + 8;
    col3_width = (string_width > col3_width) ? string_width:col3_width;
  }

  rem_width = table_width - col1_width - col2_width - col3_width;
  if ( rem_width < 0 ) rem_width = 0;
  this->sgPorts->ColWidths[0] = col1_width;
  this->sgPorts->ColWidths[1] = col2_width;
  this->sgPorts->ColWidths[2] = col3_width + rem_width;


  return ret_code;
}

//==============================================================================
/// Metodo per disegnare l'icona di una porta
///
/// \date [22.02.2006]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
void __fastcall TfPortView::sgPortsDrawCell( TObject *Sender, int ACol, int ARow, TRect &Rect, TGridDrawState State )
{
  TCanvas           * canvas  = NULL;
  TImage            * image   = NULL;
  SPV_COMPROT_PORT  * port    = NULL;

  if ( ACol == 0 && ARow > 0 && ARow <= SPVPortCount )
  {
    port = &SPVPortList[ARow-1];

    if ( port != NULL )
    {
      if ( port->Port != NULL )
      {
        image = new TImage( NULL );

        switch ( port->Port->Type )
        {
          case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
            ilPortIcons->GetIcon( 1, this->icoApp );
          break;
          case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
            ilPortIcons->GetIcon( 2, this->icoApp );
          break;
          case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
            ilPortIcons->GetIcon( 3, this->icoApp );
          break;
          case SYS_KERNEL_PORT_TYPE_SERIAL:
            ilPortIcons->GetIcon( 0, this->icoApp );
          break;
          case SYS_KERNEL_PORT_TYPE_SOCKET:
          case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
          case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
          case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
          case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
            ilPortIcons->GetIcon( 4, this->icoApp );
          break;
          default:
            ilPortIcons->GetIcon( 0, this->icoApp );
        }
       image->Picture->Icon = this->icoApp;

        canvas = this->sgPorts->Canvas;
        canvas->Draw( 1, ARow * 17, image->Picture->Graphic );

        delete image;
      }
    }
  }
}

//==============================================================================
/// Metodo che cattura l'evento di ridimensionamento del form.
///
/// \date [14.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void __fastcall TfPortView::FormResize( TObject * Sender )
{
  SPVTableAutoSize( (void*)this->sgPorts );
}

//==============================================================================
/// Metodo che cattura l'apertura del menu popup.
///
/// \date [04.10.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortView::popPortsPopup( TObject * Sender )
{
  int                 rowID     = 0     ;
  SPV_COMPROT_PORT  * port      = NULL  ;
  SYS_PORT          * sys_port  = NULL  ;
  bool                checked   = false ;
  bool                enabled   = false ;
  TfPortMonitor     * monitor   = NULL  ;

  rowID = this->sgPorts->Row;

  if ( rowID > 0 && rowID <= SPVPortCount )
  {
    port = (SPV_COMPROT_PORT*) &SPVPortList[rowID-1];
    if ( port != NULL )
    {
      sys_port = (SYS_PORT*) port->Port;
      if (sys_port != NULL)
      {
        enabled = true;
        monitor = (TfPortMonitor*) sys_port->Monitor;
        if ( monitor != NULL )
        {
          if ( monitor->Showing == true )
          {
            checked = true;
          }
        }
      }
    }
  }

  this->miPortMonitor->Enabled  = enabled;
  this->miPortMonitor->Checked  = checked;
}

//==============================================================================
/// Metodo che cattura l'evento di pressione di un tasto del mouse.
///
/// \date [04.10.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortView::sgPortsMouseDown( TObject * Sender, TMouseButton Button, TShiftState Shift, int X, int Y )
{
  if ( Button == mbRight )
  {
    if ( Y >= this->sgPorts->DefaultRowHeight && Y < ( this->sgPorts->DefaultRowHeight * ( SPVPortCount + 1 ) ) )
    {
      this->sgPorts->Row = (int) Y / this->sgPorts->DefaultRowHeight;
    }
  }
}


//==============================================================================
/// Metodo per visualizzare o nascondere un form port monitor.
///
/// \date [17.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void __fastcall TfPortView::miPortMonitorClick( TObject * Sender )
{
  int                 rowID     = 0   ;
  SPV_COMPROT_PORT  * port      = NULL;
  SYS_PORT          * sys_port  = NULL;
  TfPortMonitor     * monitor   = NULL;

  rowID = this->sgPorts->Row;

  if ( rowID > 0 && rowID <= SPVPortCount )
  {
    port = (SPV_COMPROT_PORT*) &SPVPortList[rowID-1];
    if ( port != NULL )
    {
      sys_port = (SYS_PORT*) port->Port;
      if ( sys_port != NULL )
      {
        monitor = (TfPortMonitor*) sys_port->Monitor;
        if ( monitor != NULL )
        {
          if ( monitor->Showing == false )
          {
            monitor->Show();
          }
          else
          {
            monitor->Close();
          }
        }
        else
        {
          // --- Creo un nuovo Port Monitor per la porta ---
          monitor  = new TfPortMonitor(this);
          SPV_EVENTLOG_MANAGER * event_log = SPVNewEventLog(SPV_EVENTLOG_TYPE_MEMO,SPV_EVENTLOG_TEMPLATE_COMPORT,monitor->mLog);
          SPVOpenEventLog(event_log->ID);
          sys_port->Monitor   = monitor;
          sys_port->EventLog  = event_log;
          monitor->Show();
        }
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TfPortView::FormClose(TObject *Sender, TCloseAction &Action)
{
	delete this->icoApp;
}
//---------------------------------------------------------------------------


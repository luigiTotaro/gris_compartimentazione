//==============================================================================
// Telefin Supervisor Port View Form 1.0
//------------------------------------------------------------------------------
// Header Form sinottico delle porte di comunicazione (SPVPortViewForm.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.10 (30.09.2004 -> 19.12.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SPVPortViewForm.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [30.09.2004]:
// - Prima versione del form.
// 0.02 [19.01.2005]:
// - Aggiunto un nuovo nome di tipo porta SPV_PORTVIEW_TYPE_MODEM.
// 0.03 [17.03.2005]:
// - Aggiunto il metodo TfPortView::miPortMonitorClick.
// 0.04 [24.05.2005]:
// - Modificato il metodo TfPortView::ViewSystemPortStatus.
// 0.05 [14.06.2005]:
// - Modificato il metodo TfPortView::FormResize.
// 0.06 [15.09.2005]:
// - Modificato il metodo costruttore TfPortView::TfPortView.
// - Modificato il metodo TfPortView::sgPortsDrawCell.
// 0.07 [22.09.2005]:
// - Modificato il metodo costruttore TfPortView::TfPortView.
// 0.08 [28.09.2005]:
// - Modificato il metodo costruttore TfPortView::TfPortView.
// - Modificato il metodo TfPortView::ViewPortStatus.
// - Modificato il metodo TfPortView::sgPortsDrawCell.
// 0.09 [30.11.2005]:
// - Modificato il metodo TfPortView::ViewSystemPortStatus.
// 0.10 [19.12.2005]:
// - Modificato il metodo costruttore TfPortView::TfPortView.
//==============================================================================

#ifndef SPVPortViewFormH
#define SPVPortViewFormH
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ImgList.hpp>

//==============================================================================
// Definizione valori
//------------------------------------------------------------------------------

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_PORTVIEW_NO_ERROR             0
#define SPV_PORTVIEW_INVALID_PORT_ID      22000
#define SPV_PORTVIEW_INVALID_PORT         22001
#define SPV_PORTVIEW_INVALID_PORT_LIST    22002
#define SPV_PORTVIEW_INVALID_PORT_STRUCT  22003

//==============================================================================
// Classi
//------------------------------------------------------------------------------
class TfPortView : public TForm
{
__published:	// IDE-managed Components
  TStatusBar *sbPorts;
  TListView         * clv;
  TGroupBox         * gbStatus;
  TImage            * imgStatus;
  TLabel            * lStatus;
  TLabel            * lPortNum;
  TPanel            * pBluLineStatus;
  TStringGrid       * sgPorts;
  TPopupMenu        * popPorts;
  TMenuItem         * miPortMonitor;
  TImageList *ilPortIcons;
  /// Metodo per disegnare l'icona di una porta
  void __fastcall     sgPortsDrawCell( TObject * Sender, int ACol, int ARow, TRect &Rect, TGridDrawState State );
  /// Metodo che cattura l'evento di ridimensionamento del form.
  void __fastcall     FormResize( TObject * Sender );
  /// Metodo che cattura l'apertura del menu popup.
  void __fastcall     popPortsPopup( TObject * Sender );
  /// Metodo che cattura l'evento di pressione di un tasto del mouse.
  void __fastcall     sgPortsMouseDown( TObject * Sender, TMouseButton Button, TShiftState Shift, int X, int Y );
  void __fastcall miPortMonitorClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
  TIcon             * icoApp;
public:		// User declarations
  /// Metodo costruttore della classe
        __fastcall    TfPortView( TComponent * Owner );
  /// Metodo per visualizzare lo stato di una porta
  int   __fastcall    ViewPortStatus( int ID );
  /// Metodo per visualizzare lo stato di tutte le porte del sistema
  int   __fastcall    ViewSystemPortStatus( void );
  /// Metodo per eseguire l'autodimensionamento della tabella di stato
  int   __fastcall    ViewAutoSize( void );
};

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
extern PACKAGE TfPortView *fPortView;

#endif

//---------------------------------------------------------------------------
#include "SPVMainService.h"
#include "SPVMainThread.h"
#include "LogLibrary.h"

#include "stdio.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TStlcSPVService *StlcSPVService;
//---------------------------------------------------------------------------
__fastcall TStlcSPVService::TStlcSPVService(TComponent* Owner)
	: TService(Owner)
{
}

TServiceController __fastcall TStlcSPVService::GetServiceController(void)
{
	return (TServiceController) ServiceController;
}

void __stdcall ServiceController(unsigned CtrlCode)
{
	StlcSPVService->Controller(CtrlCode);
}
//---------------------------------------------------------------------------
void __fastcall TStlcSPVService::ServiceStart(TService *Sender, bool &Started)
{
	SPVMainThreadInit	( );

	SPVMainThreadStart	( );

  	this->LogMessage( "\nStlcSPVService - Servizio avviato", EVENTLOG_INFORMATION_TYPE, 0, 25145 );
}
//---------------------------------------------------------------------------
void __fastcall TStlcSPVService::ServiceStop(TService *Sender, bool &Stopped)
{
  SPVMainThreadClear( );

  SPVMainThreadStop( );

  this->LogMessage( "\nStlcSPVService - Servizio arrestato", EVENTLOG_INFORMATION_TYPE, 0, 2590 );
}
//---------------------------------------------------------------------------

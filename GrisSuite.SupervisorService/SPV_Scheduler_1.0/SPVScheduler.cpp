//==============================================================================
// Telefin Supervisor Scheduler Module 1.0
//------------------------------------------------------------------------------
// Modulo di schedulazione di procedure applicative (SPVScheduler.cpp)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	0.23 (17.06.2005 -> 11.02.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVScheduler.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVScheduler.h
//==============================================================================

#pragma hdrstop
#include "SPVScheduler.h"
#include "SPVApplicative.h"
#include <time.h>
#include "SPVConfig.h"
#include "UtilityLibrary.h"
#include "LogLibrary.h"
#pragma package(smart_init)

//==============================================================================
// Definizioni locali
//------------------------------------------------------------------------------
#define SPV_SCHEDULER_NULL_PROFILE  { 0, NULL, NULL, 0, NULL }
#define SPV_SCHEDULER_NULL_ACTIVITY { 0, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL }

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
//static	SYS_LOCK		  SPVSchedulerLock;
//static _CRITICAL_SECTION  SPVSchedulerCriticalSection; // Sezione critica per le variabili globali del modulo
static  SPV_SCHED_PROFILE SPVSchedulerProfileList[SPV_SCHEDULER_MAX_PROFILES]; // Array dei profili di schedulazione
static  int               SPVSchedulerProfileCount; // Numero di profili di schedulazione contenuti nell'array
static  SPV_SCHED_INFO    SPVScheduler; // Struttura di informazione

//==============================================================================
// Implementazione Funzioni
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per cercare la definizione dello scheduler in quella di sistema
///
/// In caso di errore restituisce NULL.
///
/// \date [24.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
XML_ELEMENT * SPVSearchSchedulerDef( XML_ELEMENT * system_xml )
{
	XML_ELEMENT * scheduler_xml = NULL; // Ptr elemento di def. dello scheduler

	if ( system_xml != NULL )
	{
		scheduler_xml = XMLGetNext( system_xml, "scheduler", -1 );
	}

	return scheduler_xml;
}

//==============================================================================
/// Funzione per entrare nella sezione critica delle variabili globali
///
/// \date [20.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVEnterSchedulerData( char * label )
{
//	EnterCriticalSection( &SPVSchedulerCriticalSection );
//	SYSLock( &SPVSchedulerLock, label);
}

//==============================================================================
/// Funzione per uscire dalla sezione critica delle variabili globali
///
/// \date [20.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVLeaveSchedulerData( void )
{
//	LeaveCriticalSection( &SPVSchedulerCriticalSection );
//	SYSUnLock( &SPVSchedulerLock );
}


//==============================================================================
/// Funzione per ottenere la posizione in lista di un profilo di schedulazione
///
/// In caso di errore o di profile inesistente restituisce -1.
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetProfilePos( int ID )
{
	int                 fun_code      = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_PROFILE * profile_list  = NULL;
	SPV_SCHED_PROFILE * profile       = NULL;
	int                 profile_pos   = -1  ;
	int                 profile_id    = -1  ;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVGetProfilePos");

	try
	{
		// --- Recupero le variabili della lista globale dei profili ---
		profile_list  = SPVSchedulerProfileList;

		if ( profile_list != NULL )
		{
			// --- Ciclo sulla lista dei profili di schedulazione ---
			for ( int p = 0; p < SPV_SCHEDULER_MAX_PROFILES; p++ )
			{
				try
				{
					profile = &profile_list[p];
					if ( profile != NULL )
					{
						try
						{
							profile_id = profile->ID;
						}
						catch(...)
						{
							SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante recupero ID profilo", "SPVGetProfilePos()" );
						}
						if ( fun_code == SPV_SCHEDULER_NO_ERROR )
						{
							if ( profile_id == ID )
							{
								profile_pos = p;
								p = SPV_SCHEDULER_MAX_PROFILES; // Fine del ciclo
							}
						}
					}
					else
					{
						SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Profilo non valido", "SPVGetProfilePos()" );
					}
				}
				catch(...)
				{
					SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante accesso p-esimo profilo", "SPVGetProfilePos()" );
				}
			}
		}
		else
		{
			SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Lista profili non valida", "SPVGetProfilePos()" );
		}
	}
	catch(...)
	{
		profile_pos = -1;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVGetProfilePos()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return profile_pos;
}


//==============================================================================
/// Funzione per verificare se posso schedulare in un giorno della settimana
///
/// \date [23.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVCheckDayOfWeek( char DOW, long now )
{
	bool        schedulable = false;
	struct tm * now_struct  = NULL;
	int         day         = 0   ;
	char        day_code    = 0x00;

	now_struct  = localtime( &((time_t)now) );
	day         = now_struct->tm_wday;
	if ( ( DOW & 0x80 ) == 0x80 ) // Controllo se la verifice del giorno � abilitata
	{
		switch ( day )
		{
			// --- Luned� ---
			case 1:
				day_code = 0x40;
			break;
			// --- Marted� ---
			case 2:
				day_code = 0x20;
			break;
			// --- Mercoled� ---
			case 3:
				day_code = 0x10;
			break;
			// --- Gioved� ---
			case 4:
				day_code = 0x08;
			break;
			// --- Venerd� ---
			case 5:
				day_code = 0x04;
			break;
			// --- Sabato ---
			case 6:
				day_code = 0x02;
			break;
			// --- Domenica ---
			case 0:
				day_code = 0x01;
			break;
		}
		if ( ( DOW & day_code ) == day_code )
		{
			schedulable = true;
		}
	}
	else
	{
		schedulable = true;
	}

	return schedulable;
}

//==============================================================================
/// Funzione per verificare se posso schedulare in un orario giornaliero
///
/// \date [29.06.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
bool SPVCheckFromTo( long from, long to, long now )
{
	bool        schedulable = false ;
	struct tm * from_ptm    = NULL  ;
	struct tm   from_tm             ;
	time_t      from_time   = 0     ;
	struct tm * to_ptm      = NULL  ;
	struct tm   to_tm               ;
	time_t      to_time     = 0     ;
	struct tm * now_ptm     = NULL  ;
	struct tm   now_tm              ;
	time_t      now_time    = 0     ;

	// --- Acquisisco e formatto l'ora attuale ---
	now_time        = (time_t) now;
	now_ptm         = localtime( &now_time );
	now_tm.tm_sec   = now_ptm->tm_sec;
	now_tm.tm_min   = now_ptm->tm_min;
	now_tm.tm_hour  = now_ptm->tm_hour;
	now_time        = now_tm.tm_sec + ( now_tm.tm_min * 60 ) + ( now_tm.tm_hour * 3600 );

	// --- Acquisisco e formatto l'ora di inizio ---
	from_time       = (time_t) from;

	// --- Acquisisco e formatto l'ora di fine ---
	to_time         = (time_t) to;

	// --- Caso inizio < fine ---
	if ( from_time < to_time )
	{
	  if ( from_time <= now_time && now_time <= to_time )
	  {
		schedulable = true;
	  }
	}
	// --- Caso inizio > fine ---
	else if ( from_time > to_time )
	{
	  if ( from_time <= now_time || now_time <= to_time )
	  {
		schedulable = true;
	  }
	}
	// --- Caso inizio = fine ---
	else
	{
	  to_time = to_time + 5; // Aumento il range di 5 secondi (precisione di 5 sec)
	  if ( from_time <= now_time && now_time <= to_time )
	  {
		schedulable = true;
	  }
	}

	return schedulable;
}

//==============================================================================
/// Funzione per verificare se posso lanciare un'esecuzione dopo un tempo
///
/// \date [29.06.2005]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
bool SPVCheckInterval( long last, long interval, long now )
{
	bool        schedulable     = false ; // Flag di intervallo superato
	double      diff                    ; // Secondi tra <last> e <now>

	diff = difftime( now, last );
	if ( diff >= interval )
	{
		schedulable = true;
	}

	return schedulable;
}

//==============================================================================
/// Funzione per verificare se posso lanciare un'esecuzione in un preciso istante
///
/// \date [11.07.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
bool SPVCheckFixedStart( long start, long now )
{
	bool        schedulable   = false ;

	schedulable = SPVCheckFromTo( start, start + SPV_SCHEDULER_FIXED_PRECISION, now );

	return schedulable;
}


//==============================================================================
/// Funzione per processare una singola attivit� di un profilo di schedulazione
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
int SPVProcessActivity( SPV_DEVICE * device, SPV_SCHED_ACTIVITY * activity, long now )
{
	int                   ret_code        = SPV_SCHEDULER_NO_ERROR; // Codice di ritorno
	int                   fun_code        = SPV_SCHEDULER_NO_ERROR; // Codice di ritorno
	struct tm           * now_struct      = NULL  ;
	bool                  dow_passed      = false ;
	bool                  from_to_passed  = false ;
	int                   class_id        = 0     ; // ID (pos) della classe
	int                   class_count     = 0     ; // Numero di classi
	SPV_PROCEDURE_CLASS * class_list      = NULL  ; // Lista delle classi
	SPV_PROCEDURE_CLASS * class_item      = NULL  ; // Ptr a una classe
	SPV_PROCEDURE       * procedure       = NULL  ;
	SPV_PROCEDURE       * procedure_list  = NULL  ;
	int                   procedure_count = 0     ;
	long                  last_finish     = 0     ;
	long                  fixed_start     = 0     ;

	if ( device != NULL ) // Controllo il ptr alla struttura periferica
	{
		if ( activity != NULL ) // Controllo il ptr alla struttura attivit�
		{
			// Controllo il giorno della settimana
			dow_passed = SPVCheckDayOfWeek( activity->DOW, now );
			if ( dow_passed )
			{
				// Controllo l'orario giornaliero
				from_to_passed = SPVCheckFromTo( activity->From, activity->To, now );
				if ( from_to_passed )
				{
					// --- Entro nella sezione critica delle classi di procedure ---
					SPVEnterClassData("SPVProcessActivity");

					try
					{
						// --- Recupero le variabili globali della lista di classi ---
						class_count = SPVGetClassCount( );
						class_list  = SPVGetClassList( );

						if ( class_count > 0 && class_list != NULL )
						{
							// --- Recupero ID e ptr alla classe di procedure collagata ---
							class_id = activity->Class;
							if ( class_id != -1 )
							{
							  class_item = &class_list[class_id];
							}
							if ( class_item != NULL )
							{
								// --- Entro nella sezione critica delle procedure ---
								SPVEnterProcedureData("SPVProcessActivity");

								try
								{
									// --- Recupero le variabili globali della lista procedure ---
									procedure_count = SPVGetProcedureCount( );
									procedure_list  = SPVGetProcedureList( );
									if ( procedure_count > 0 && procedure_list != NULL )
									{
										// --- Ciclo sulla lista delle procedure ---
										for ( int p = 0; p < procedure_count; p++ )
										{
											try
											{
												procedure = &procedure_list[p];
												if ( procedure != NULL ) // Controllo il ptr alla stru
												{
													if ( device == procedure->Device ) // Se � una procedura della periferica
													{
														if ( class_id == procedure->ClassID ) // Se la procedura fa parte della classe
														{
															if ( procedure->ExeCount == 0 ) // Se la procedura non � in esecuzione
															///if ( !procedure->Active ) // Se la procedura non � in esecuzione
															{
																// --- Switcho in base alla modalit� di schedulazione ---
																switch ( activity->Mode )
																{
																	case SPV_SCHEDULER_MODE_CONTINUE:
																		// --- Metto in coda una nuova esecuzione della procedura ---
																		SPVIncProcedureExeCount( procedure );
																	break;
																	case SPV_SCHEDULER_MODE_INTERVAL:
																		// Recupero l'ultimo orario di fine esecuzione della procedura
																		//* SPERIMENTALE FREJUS 25.03.2009
																		last_finish = procedure->FinishDT;
																		//last_finish = procedure->StartDT;
																		//*/
																		if ( SPVCheckInterval( last_finish, activity->Interval, now ) )
																		{
																			// --- Metto in coda una nuova esecuzione della procedura ---
																			SPVIncProcedureExeCount( procedure );
																		}
																	break;
																	case SPV_SCHEDULER_MODE_FIXED:
																		for ( int s = 0; s < activity->StartCount; s++ )
																		{
																			fixed_start = activity->Start[s];
																			 if ( SPVCheckFixedStart( fixed_start, now ) )
																			{
																				// --- Metto in coda una nuova esecuzione della procedura ---
																				SPVIncProcedureExeCount( procedure );
																				s = activity->StartCount; // Fine del ciclo
																			}
																		}
																	break;
																	case SPV_SCHEDULER_MODE_STARTUP:
																		// --- Se la procedure non e' mai stata eseguita ---
																		if ( procedure->StartDT == 0 )
																		{
																			// --- Metto in coda una nuova esecuzione della procedura ---
																			SPVIncProcedureExeCount( procedure );
																		}
																	break;
																	default:
																		ret_code = SPV_SCHEDULER_INVALID_ACTIVITY_MODE;
																}
															}
														}
													}
												}
											}
											catch(...)
											{
												SPVReportApplicEvent( procedure, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante elaborazione singola procedura", "SPVProcessActivity()" );
											}
										}
									}
									else
									{
										ret_code = SPV_SCHEDULER_INVALID_PROCEDURE_LIST;
									}
								}
								catch(...)
								{
									ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
									SPVReportApplicEvent( procedure, 2, ret_code, "Eccezione generale L2", "SPVProcessActivity()" );
								}

								// --- Esco dalla sezione critica delle procedure ---
								SPVLeaveProcedureData();

							}
						}
						else
						{
							ret_code = SPV_SCHEDULER_INVALID_CLASS_LIST;
						}
					}
					catch(...)
					{
						ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
						SPVReportApplicEvent( procedure, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale L1", "SPVProcessActivity()" );
					}

					// --- Esco dalla sezione critica delle classi di procedure ---
					SPVLeaveClassData( );
				}
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_ACTIVITY;
		}
	}
	else
	{
		ret_code = SPV_SCHEDULER_INVALID_DEVICE;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per processare un profilo di schedulazione
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVProcessProfile( SPV_DEVICE * device, SPV_SCHED_PROFILE * profile, long now )
{
	int                   ret_code        = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_ACTIVITY  * activity        = NULL;
	SPV_SCHED_ACTIVITY  * activity_list   = NULL;
	int                   activity_count  = 0   ;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVProcessProfile");

	try
	{
		if ( device != NULL ) // Controllo il ptr a struttura periferica
		{
			if ( profile != NULL ) // Controllo il ptr a struttura profilo
			{
				activity_list = profile->ActivityList;
				activity_count = profile->ActivityCount;
				if ( activity_list != NULL && activity_count > 0 )
				{
					// --- Ciclo sulle attivit� del profilo di schedulazione ---
					for ( int a = 0; a < activity_count; a++ )
					{
						try
						{
							activity = &activity_list[a];
							SPVProcessActivity( device, activity, now );
						}
						catch(...)
						{
							SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante accesso a-esima attivita", "SPVProcessProfile()" );
						}
					}
				}
				else
				{
					ret_code = SPV_SCHEDULER_INVALID_ACTIVITY_LIST;
				}
			}
			else
			{
				ret_code = SPV_SCHEDULER_INVALID_PROFILE;
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_DEVICE;
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVProcessProfile()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}

//==============================================================================
/// Funzione per processare la schedulazione di una periferica
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVScheduleDevice( SPV_DEVICE * device )
{
	int                 ret_code      = SPV_SCHEDULER_NO_ERROR;
	int                 profile_id    = 0   ;
	int                 profile_pos   = -1  ;
	SPV_SCHED_PROFILE * profile       = NULL;
	SPV_SCHED_PROFILE * profile_list  = NULL;
	long                now           = 0   ; // Istante corrente (UNIX time)

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVScheduler->SPVScheduleDevice");

	try
	{
		// --- Recupero le variabili della lista globale dei profili ---
		profile_list  = SPVSchedulerProfileList;

		if ( device != NULL )
		{
			if ( device->SupStatus.Active )
			{
				if ( device->SupStatus.Scheduled )
				{
					profile_id = device->ProfileID;
					if ( profile_id > 0 ) // Controllo se la periferica ha un profilo di sched.
					{
						now         = time( NULL );
						profile_pos = SPVGetProfilePos( profile_id );
						if ( profile_pos >= 0 )
						{
							profile     = &profile_list[profile_pos];
							ret_code    = SPVProcessProfile( device, profile, now );
						}
						else
						{
							ret_code = SPV_SCHEDULER_INVALID_PROFILE_ID;
							SPVReportApplicEvent( NULL, 2, ret_code, "Posizione profilo non valido", "SPVScheduleDevice()" );
						}
					}
					else
					{
						ret_code =  SPV_SCHEDULER_INVALID_PROFILE_ID;
						SPVReportApplicEvent( NULL, 2, ret_code, "ID profilo non valido", "SPVScheduleDevice()" );
					}
				}
				else
				{
					//SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Periferica non schedulata", "SPVScheduleDevice()" );
				}
			}
			else
			{
				//SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Periferica non attiva", "SPVScheduleDevice()" );
			}
		}
		else
		{
			SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Periferica non valida", "SPVScheduleDevice()" );
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVScheduleDevice()" );
	}
	ret_code = SPV_SCHEDULER_NO_ERROR;

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}


//==============================================================================
/// Funzione per schedulare le attivita' sulla procedure applicative
///
/// \date [23.08.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVScheduleProcedures( SPV_SYSTEM * system )
{
	int                     ret_code    = SPV_APPLIC_FUNCTION_SUCCESS;

	if ( system != NULL )
	{
		ret_code = SPVDoOnDevices( system, SPVScheduleDevice );
	}
	else
	{
		ret_code = SPV_APPLIC_INVALID_SYSTEM;
	}

	return ret_code;
}


//==============================================================================
/// Funzione di attivazione per il thread dello scheduler
///
/// \date [29.10.2009]
/// \author Enrico Alborali, Mario Ferro
/// \version 1.00
//------------------------------------------------------------------------------
unsigned long __stdcall SPVSchedulerThreadFunction( void * parameter )
{
	unsigned long     ret_code  	= SPV_SCHEDULER_NO_ERROR; // Codice di ritorno
	int               fun_code  	= SPV_SCHEDULER_NO_ERROR; // Codice di ritorno funzione
	SYS_THREAD * 	  thread    	= NULL					; // Puntatore al thread
	SPV_SCHED_INFO *  scheduler 	= NULL					;
	int               r_delay   	= 0   					;
	char * 			  thread_name 	= NULL  				; // Stringa del nome del thread

    try
	{
		if ( parameter != NULL ) // Controllo il parametro passato
		{
			scheduler = (SPV_SCHED_INFO*) parameter;
			if ( scheduler != NULL ) // Controllo la struttura del thread di protocollo
			{
				thread = (SYS_THREAD*) scheduler->Thread; // Recupero il thread
				if ( SYSValidThread( thread ) )  // Controllo il thread
				{
					// assegno il nome al thread
					try
					{
						thread_name = ULAddText(thread_name,"Scheduler");
					}
					catch(...)
					{
						fun_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
					}

					if (fun_code == SPV_SCHEDULER_NO_ERROR)
					{
						fun_code = SYSSetThreadName(thread,thread_name);
					}
					/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */
					while ( !thread->Terminated ) // Ciclo del thread di protocollo
					{
						try
						{
							if ( scheduler->Active ) // Solo se lo schedulatore e' attivo
							{
								if ( SPVOwnedDeviceCount > 0 ) // Solo se ci sono periferiche da diagnosticare
								{
								    try
								    {
								    	//* DEBUG */	SPVReportApplicEvent( NULL, 1, 500, "Scheduler debug", "ACTIVE START" );
								    	// --- Caricamento contatori DB ---
								    	fun_code = SPVLoadProcedureCounters( &SPVSystem );
								    	//* DEBUG */    SPVReportApplicEvent( NULL, 1, 501, "Scheduler debug", "ACTIVE LOAD COUNTERS OK" );
								    	// --- Schedulazione attivita' ---
										fun_code = SPVScheduleProcedures( &SPVSystem );
								    	//* DEBUG */    SPVReportApplicEvent( NULL, 1, 502, "Scheduler debug", "ACTIVE SCHEDULE OD" );
								    	// --- Esecuzione procedure ---
								    	fun_code = SPVLaunchProcedures( &SPVSystem );
								    	//* DEBUG */    SPVReportApplicEvent( NULL, 1, 503, "Scheduler debug", "ACTIVE LAUNCH OK" );
								    	// --- Salvataggio contatori DB ---
								    	fun_code = SPVSaveProcedureCounters( &SPVSystem );
								    	//* DEBUG */    SPVReportApplicEvent( NULL, 1, 504, "Scheduler debug", "ACTIVE COUNTER SAVE OK" );
								    }
								    catch(...)
								    {
								    	SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante esecuzione ciclo schedulatore", "SPVSchedulerThreadFunction()" );
									}
								}
							}
							else
							{
								SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Thread Schedulatore Terminato", "SPVSchedulerThreadFunction()" );
							}
						}
						catch(...)
						{
							SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante accesso dati schedulatore", "SPVSchedulerThreadFunction()" );
						}
						// --- Generazione semi-random del tempo di delay ---
						randomize(); // Genero i semi per la randomizzazione
						r_delay = random( SPVConfigInfo.ServerMain.ScheDelay );
						// --- Attendo un tempo ritardo semi-random ---
						Sleep( SPV_SCHEDULER_MIN_DELAY + r_delay ); // Base pi� random configurabile

						// Aggiornamento delle informazioni di debug del thread
						SYSUpdateThreadDebugInfo(thread);
					}
				}
				else
				{
					ret_code = SPV_SCHEDULER_INVALID_THREAD;
				}
			}
			else
			{
				ret_code = SPV_SCHEDULER_INVALID_STRUCT;
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_PARAMETER;
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVSchedulerThreadFunction()" );
	}

	return ret_code;
}


//==============================================================================
/// Funzione per convertire una modalit� di schedulazione da stringa a codice
///
/// \date [29.08.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetSchedulerModeCode( char * mode_string )
{
	int mode_code = SPV_SCHEDULER_MODE_INTERVAL; // Default: "interval"

	if ( mode_string != NULL )
	{
		if ( strcmpi( mode_string, "continue" ) == 0 )
		{
			mode_code = SPV_SCHEDULER_MODE_CONTINUE;
		}
		else if ( strcmpi( mode_string, "interval" ) == 0 )
		{
			mode_code = SPV_SCHEDULER_MODE_INTERVAL;
		}
		else if ( strcmpi( mode_string, "fixed" ) == 0 )
		{
			mode_code = SPV_SCHEDULER_MODE_FIXED;
		}
		else if ( strcmpi( mode_string, "startup" ) == 0 )
		{
			mode_code = SPV_SCHEDULER_MODE_STARTUP;
		}
	}

	return mode_code;
}


//==============================================================================
/// Funzione per caricare la definizione di un'attivit�
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
SPV_SCHED_ACTIVITY SPVLoadActivity( XML_ELEMENT * activity_xml )
{
	SPV_SCHED_ACTIVITY    activity    = SPV_SCHEDULER_NULL_ACTIVITY;
	char                * value       = NULL;
	char                * value_uhex8 = NULL;
	long                * value_time  = NULL;
	char                * class_value = NULL;

	if ( activity_xml != NULL )
	{
		activity.ID     = XMLGetValueInt( activity_xml, "id" );
		activity.Name   = XMLExtractValue( activity_xml, "name" ); // -MALLOC
		class_value     = XMLGetValue( activity_xml, "class" );
		activity.Class  = SPVGetProcedureClassPos( class_value );
		// --- Conversione di DOW ---
		value = XMLGetValue( activity_xml, "dow" );
		if ( value != NULL )
		{
			value_uhex8 = (char*) XMLASCII2Type( value, t_u_hex_8 ); // -MALLOC
			activity.DOW    = *value_uhex8;
			free( value_uhex8 ); // -FREE
		}
		else
		{
			value_uhex8 = (char*) XMLASCII2Type( "FF", t_u_hex_8 ); // -MALLOC
			activity.DOW    = *value_uhex8;
			free( value_uhex8 ); // -FREE
		}
		// --- Conversione di From ---
		value = XMLGetValue( activity_xml, "from" );
		if ( value != NULL )
		{
			value_time      = (long*) XMLASCII2Type( value, t_time ); // -MALLOC
			activity.From   = *value_time;
			free( value_time ); // -FREE
		}
		else
		{
			value_time      = (long*) XMLASCII2Type( "00:00:00", t_time ); // -MALLOC
			activity.From   = *value_time;
			free( value_time ); // -FREE
		}
		// --- Conversione di To ---
		value = XMLGetValue( activity_xml, "to" );
		if ( value != NULL )
		{
			value_time      = (long*) XMLASCII2Type( value, t_time ); // -MALLOC
			activity.To     = *value_time;
			free( value_time ); // -FREE
		}
		else
		{
			value_time      = (long*) XMLASCII2Type( "23:59:59", t_time ); // -MALLOC
			activity.To     = *value_time;
			free( value_time ); // -FREE
		}
		// --- Conversione di Mode ---
		value = XMLGetValue( activity_xml, "mode" );
		activity.Mode   = SPVGetSchedulerModeCode( value );
		// --- Conversione di Interval ---
		value = XMLGetValue( activity_xml, "interval" );
		if ( value != NULL )
		{
			value_time      = (long*) XMLASCII2Type( value, t_time ); // -MALLOC
			activity.Interval = *value_time;
			free( value_time ); // -FREE
		}
		else
		{
			value_time      = (long*) XMLASCII2Type( "00:10:00", t_time ); // -MALLOC
			activity.Interval = *value_time;
			free( value_time ); // -FREE
		}
		// --- Conversione di Start ---
		value = XMLGetValue( activity_xml, "start" );
		if ( value != NULL )
		{
			int     start_id    	= 0;
			char  * start_item  	= value;
			bool	item_present	= true;
			while ( item_present )
			{
				start_item = XMLGetToken( value, ",", start_id ); // -MALLOC
				if ( start_item != NULL )
				{
					value_time      = (long*) XMLASCII2Type( start_item, t_time ); // -MALLOC
					activity.Start[0] = *value_time;
					try
					{
						free( start_item ); // -FREE
						if ( value_time != NULL ) free( value_time ); // -FREE
					}
					catch(...)
					{

					}
					start_id++;
				}
				else
				{
					// --- Esco dal ciclo ---
					item_present = false;
				}
			}
			activity.StartCount = start_id;
		}
		else
		{
			value_time      = (long*) XMLASCII2Type( "12:00:00", t_time ); // -MALLOC
			activity.Start[0] = *value_time;
			free( value_time ); // -FREE
			activity.StartCount = 1;
		}
		// --- Salvo il ptr alla definizione xml ---
		activity.Def = activity_xml;
	}

	return activity;
}


//==============================================================================
/// Funzione per caricare una lista di attivit� di un profilo di schedulazione
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVLoadActivityList( SPV_SCHED_PROFILE * profile )
{
	int                   ret_code        = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_ACTIVITY    activity        = SPV_SCHEDULER_NULL_ACTIVITY;
	XML_ELEMENT         * activity_xml    = NULL;
	int                   activity_count  = 0;
	SPV_SCHED_ACTIVITY  * activity_list   = NULL;

	if ( profile != NULL )
	{
		activity_count = XMLGetChildCount( profile->Def, false );
		if ( activity_count > 0 )
		{
			if ( activity_count > SPV_SCHEDULER_MAX_ACTIVITIES )
			{
				activity_count = SPV_SCHEDULER_MAX_ACTIVITIES;
			}
			activity_list = (SPV_SCHED_ACTIVITY*)malloc(sizeof(SPV_SCHED_ACTIVITY)*activity_count); // -MALLOC
			if ( activity_list != NULL )
			{
				for ( int a = 0; a < activity_count; a++ )
				{
					try
					{
						activity_xml = XMLGetNext( profile->Def->child, "activity", a );
						activity = SPVLoadActivity( activity_xml );
						activity_list[a] = activity;
					}
					catch(...)
					{

					}
				}
				profile->ActivityList   = activity_list;
				profile->ActivityCount  = activity_count;
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_DEFINITION_ERROR;
		}
	}
	else
	{
		ret_code = SPV_SCHEDULER_INVALID_PROFILE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per caricare la definizione di un profilo
///
/// Nel caso di errore restituisce un profile nullo.
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_SCHED_PROFILE SPVLoadProfile( XML_ELEMENT * profile_xml )
{
	SPV_SCHED_PROFILE profile   = SPV_SCHEDULER_NULL_PROFILE;
	int               fun_code  = SPV_SCHEDULER_NO_ERROR;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVLoadProfile");

	try
	{
		if ( profile_xml != NULL )
		{
			if ( strcmpi( profile_xml->name, "profile" ) == 0 )
			{
				profile.ID            = XMLGetValueInt( profile_xml, "id" );
				profile.Name          = XMLExtractValue( profile_xml, "name" );
				profile.ActivityList  = NULL;
				profile.ActivityCount = 0;
				profile.Def           = profile_xml;
				fun_code = SPVLoadActivityList( &profile );
				if ( fun_code != SPV_SCHEDULER_NO_ERROR )
				{
					SPVReportApplicEvent( NULL, 2, fun_code, "Problema durante caricamento lista attivita", "SPVLoadProfile()" );
				}
			}
			else
			{
				SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Elemento XML sbagliato", "SPVLoadProfile()" );
			}
		}
	}
	catch(...)
	{
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVLoadProfile()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return profile;
}


//==============================================================================
/// Funzione per verificare se un profilo � nullo
///
/// Se il profilo � nullo restituisce 'true', altrimenti 'false'.
///
/// \date [20.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SPVNullProfile( SPV_SCHED_PROFILE profile )
{
	bool profile_null = false;

	if ( profile.ID             == 0
	  && profile.Name           == NULL
	  && profile.ActivityList   == NULL
	  && profile.ActivityCount  == 0
	  && profile.Def            == NULL )
	{
		profile_null = true;
	}

	return profile_null;
}


//==============================================================================
/// Funzione per cercare la prima posizione libera
///
/// Restituisce la prima posizione libera dell'array globale dei profili di
/// schedulazione. Il primo elemento � in posizione 0.
/// Nel caso di errore restituisce -1
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVGetFirstFreeProfilePos( void )
{
	int                 ret_code      = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_PROFILE * profile_list  = NULL;
	int                 profile_count = 0   ;
	SPV_SCHED_PROFILE * profile       = NULL;
	int                 profile_pos   = -1  ;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVGetFirstFreeProfilePos");

	try
	{
		// --- Recupero le variabili della lista globale dei profili ---
		profile_list  = SPVSchedulerProfileList;
		profile_count = SPVSchedulerProfileCount;

		if ( profile_list != NULL )
		{
			if ( profile_count < SPV_SCHEDULER_MAX_PROFILES )
			{
				// --- Ciclo sulla lista dei profili di schedulazione ---
				for ( int p = 0; p < SPV_SCHEDULER_MAX_PROFILES; p++ )
				{
					try
					{
						try
						{
							profile = &profile_list[p];
						}
						catch (...)
						{
							profile = NULL;
							/* TODO -oEnrico -cError : Gestione dell'errore critico */
						}
						if ( profile != NULL )
						{
							if ( SPVNullProfile( *profile ) )
							{
								profile_pos = p;
								p = SPV_SCHEDULER_MAX_PROFILES; // Fine del ciclo
							}
						}
						else
						{
							ret_code = SPV_SCHEDULER_CRITICAL_ERROR;
							SPVReportApplicEvent( NULL, 2, ret_code, "Profilo di schedulazione non valido", "SPVGetFirstFreeProfilePos()" );
						}
					}
					catch(...)
					{
						SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante elaborazione singolo profilo", "SPVGetFirstFreeProfilePos()" );
					}
				}
			}
			else
			{
				ret_code = SPV_SCHEDULER_PROFILE_LIST_FULL;
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_PROFILE_LIST;
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, ret_code, "Eccezione generale L1", "SPVGetFirstFreeProfilePos()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return profile_pos;
}


//==============================================================================
/// Funzione per aggiungere un nuovo profilo di schedulazione alla lista
///
/// Nel caso di errore restituisce un valore non nullo.
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVAddNewProfile( SPV_SCHED_PROFILE profile )
{
	int                 ret_code      = SPV_SCHEDULER_NO_ERROR;
	int                 profile_pos   = -1  ; // Posizione dell'array in cui salvare il profilo
	SPV_SCHED_PROFILE * profile_list  = NULL;
	int                 profile_count = 0   ;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVAddNewProfile");

	try
	{
		// --- Recupero le variabili della lista globale dei profili ---
		profile_list  = SPVSchedulerProfileList;
		profile_count = SPVSchedulerProfileCount;

		if ( !SPVNullProfile( profile ) ) // Controllo che il profilo sia non-nullo
		{
			profile_pos = SPVGetFirstFreeProfilePos( );
			if ( profile_pos != -1 )
			{
				try
				{
					profile_list[profile_pos] = profile;
				}
				catch(...)
				{
					ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
					SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante salvataggio profilo", "SPVAddNewProfile()" );
				}
				if ( ret_code == SPV_SCHEDULER_NO_ERROR )
				{
					profile_count++;
				}
			}
			else
			{
				ret_code = SPV_SCHEDULER_PROFILE_LIST_FULL;
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_PROFILE;
		}

		// --- Salvo le variabili della lista globale dei profili ---
		SPVSchedulerProfileCount = profile_count;
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVAddNewProfile()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}


//==============================================================================
/// Funzione per caricare tutti i parametri dello scheduler
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVLoadScheduler( XML_ELEMENT * scheduler_xml )
{
	int                 ret_code      = SPV_SCHEDULER_NO_ERROR;
	int                 fun_code      = SPV_SCHEDULER_NO_ERROR;
	SYS_THREAD        * thread        = NULL;
	int                 profile_count = 0   ;
	SPV_SCHED_PROFILE   profile       = SPV_SCHEDULER_NULL_PROFILE;
	XML_ELEMENT       * profile_xml   = NULL;

	bool 		    logEnabled 	   = SYSSetKeyDebugLogEnabled();

  	if (logEnabled == true) {
  		LLCreateLogFile	( );
        	LLAddLogFile	( "SPVLoadScheduler: Scrive!" );
  	}

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVLoadScheduler");

	try
	{
		if ( scheduler_xml != NULL )
		{
	                if (logEnabled == true) {
		        	LLAddLogFile	( "SPVLoadScheduler: XML buono" );
                        }

			// --- Carico il thread di schedulazione ---
			thread = SYSNewThread( SPVSchedulerThreadFunction, 0, CREATE_SUSPENDED ); // Alloco un nuovo thread
			fun_code = SYSSetThreadParameter( thread, (void*)&SPVScheduler ); // Gli passo come parametro la struttura scheduler
			if ( fun_code == SPV_SCHEDULER_NO_ERROR )
			{
                	        if (logEnabled == true) {
			        	LLAddLogFile	( "SPVLoadScheduler: Thread Inizializzato" );
	                        }

				fun_code = SYSCreateThread( thread ); // Istanzio il nuovo thread
				if ( fun_code == SPV_SCHEDULER_NO_ERROR )
				{
	                	        if (logEnabled == true) {
				        	LLAddLogFile	( "SPVLoadScheduler: Thread Creato" );
	        	                }

					SPVScheduler.Thread     = thread;
					SPVScheduler.Active     = false;
					SPVScheduler.StartTime  = 0;
					SPVScheduler.StopTime   = 0;
					SPVScheduler.Def        = scheduler_xml;

                                        // --- Carico i profili di schedulazione ---
					profile_count = XMLGetChildCount( scheduler_xml, false );
					if ( profile_count > 0 )
					{
						for ( int p = 1; p <= profile_count; p++ )
						{
							try
							{
								profile_xml = XMLGetNext( scheduler_xml->child, "profile", p );
								profile = SPVLoadProfile( profile_xml );
								if ( !SPVNullProfile( profile ) )
								{
									fun_code = SPVAddNewProfile( profile );
								}
								else
								{
									ret_code = SPV_SCHEDULER_DEFINITION_ERROR;
								}
							}
							catch(...)
							{
								SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante p-esimo profilo", "SPVLoadScheduler()" );
							}
						}
					}
					else
					{
						ret_code = SPV_SCHEDULER_INVALID_DEFINITION;
					}
				}
                                else
	                                SPVReportApplicEvent( NULL, 2, fun_code, "Thread non creato", "SPVLoadScheduler()" );
			}
                        else
				SPVReportApplicEvent( NULL, 2, fun_code, "Thread non inizializzato", "SPVLoadScheduler()" );
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_DEFINITION;
                        SPVReportApplicEvent( NULL, 2, ret_code, "No XML", "SPVLoadScheduler()" );
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVLoadScheduler()" );
	}
	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

        if (logEnabled == true) {
        	LLAddLogFile	( "SPVLoadScheduler: Fine!" );
        }

	return ret_code;
}


//==============================================================================
/// Funzione per cancellare la lista di attivit� di un profilo di schedulazione
///
/// Nel caso di errore restituisce un profile nullo.
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVClearActivityList( SPV_SCHED_PROFILE * profile )
{
	int                   ret_code        = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_ACTIVITY    activity        = SPV_SCHEDULER_NULL_ACTIVITY;
	int                   activity_count  = 0   ;
	SPV_SCHED_ACTIVITY  * activity_list   = NULL;
	char                * activity_name   = NULL;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVClearActivityList");

	try
	{
		if ( profile != NULL ) // Controllo il ptr alla struttura profilo
		{
			activity_list   = profile->ActivityList;
			activity_count  = profile->ActivityCount;
			if ( activity_list != NULL && activity_count > 0 )
			{
				for ( int a = 0; a < activity_count; a++ )
				{
					try
					{
						activity = activity_list[a];
						try
						{
							activity_name = activity.Name;
						}
						catch(...)
						{
							activity_name = NULL;
							SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante recupero nome", "SPVClearActivityList()" );
						}
						if ( activity_name != NULL )
						{
							free( activity_name ); // -FREE
						}
					}
					catch(...)
					{
						SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante cancellazione singola attivita", "SPVClearActivityList()" );
					}
				}
				try
				{
					free( activity_list ); // -FREE
				}
				catch(...)
				{
					SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante cancellazione lista attivita", "SPVClearActivityList()" );
				}
				activity_count = 0;
			}
			else
			{
				ret_code = SPV_SCHEDULER_INVALID_ACTIVITY_LIST;
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_PROFILE;
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVClearActivityList()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}


//==============================================================================
/// Funzione per togliere dalla lista un profilo di schedulazione
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SPVClearProfile( int ID )
{
	int                 ret_code      = SPV_SCHEDULER_NO_ERROR;
	int                 fun_code      = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_PROFILE * profile_list  = NULL;
	SPV_SCHED_PROFILE * profile       = NULL;
	int                 profile_pos   = -1  ;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVClearProfile");

	try
	{
		// --- Recupero le variabili della lista globale dei profili ---
		profile_list  = SPVSchedulerProfileList;

		profile_pos = SPVGetProfilePos( ID );
		if ( profile_pos != -1 )
		{
			profile = &profile_list[profile_pos];
			if ( profile != NULL )
			{
				try
				{
					// --- Elimino la lista delle attivita' ---
					fun_code = SPVClearActivityList( profile );
					if ( fun_code != SPV_SCHEDULER_NO_ERROR )
					{
						SPVReportApplicEvent( NULL, 2, fun_code, "Problema durante cancellazione lista attivita", "SPVClearProfile()" );
					}
					if ( profile->Name != NULL )
					{
						// --- Cancello la memoria allocata ---
						try
						{
							free( profile->Name );
						}
						catch(...)
						{
							ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
							SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante cancellazione nome profilo", "SPVClearProfile()" );
						}
					}
					// --- Pulisco la memoria ---
					try
					{
						memset( profile, 0, sizeof(SPV_SCHED_PROFILE) );
					}
					catch(...)
					{
						ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
						SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante azzeramento memoria profilo", "SPVClearProfile()" );
					}
				}
				catch(...)
				{
					ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
					SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante cancellazione profilo", "SPVClearProfile()" );
				}
			}
		}
		else
		{
			ret_code = SPV_SCHEDULER_INVALID_PROFILE_ID;
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVClearProfile()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}


//==============================================================================
/// Funzione per cancellare tutti i parametri dello scheduler
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVFreeScheduler( void )
{
	int                 ret_code      = SPV_SCHEDULER_NO_ERROR;
	SPV_SCHED_PROFILE * profile_list  = NULL;
	SPV_SCHED_PROFILE * profile       = NULL;
	int                 profile_count = 0   ;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVFreeScheduler");

	try
	{
		try
		{
			// --- Termino il thread dello shceduler ---
			//SYSSuspendThread( SPVScheduler.Thread );
			//SYSExitThread( SPVScheduler.Thread );

			SPVScheduler.Thread->Terminated = true;

			if ( SYSWaitForThread( SPVScheduler.Thread ) == 0 ) // Se il thread � terminato
			{
				SYSDeleteThread( SPVScheduler.Thread );
			}

		}
		catch(...)
		{
			SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante terminazione e cancellazione thread", "SPVFreeScheduler()" );
		}

		// --- Recupero le variabili della lista globale dei profili ---
		profile_list  = SPVSchedulerProfileList;
		profile_count = SPVSchedulerProfileCount;

		// --- Ciclo sulla lista dei profili ---
		for ( int p = 0; p < profile_count; p++ )
		{
			try
			{
				profile = &profile_list[p];
				if ( profile != NULL )
				{
					SPVClearProfile( profile->ID );
				}
			}
			catch(...)
			{
				SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione durante cancellazione singolo profilo", "SPVFreeScheduler()" );
			}
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVFreeScheduler()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}


//==============================================================================
/// Funzione di inizializzazione del modulo
///
/// \date [24.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVSchedulerInit( XML_ELEMENT * system_xml )
{
	int           ret_code      = SPV_SCHEDULER_NO_ERROR;
	XML_ELEMENT * scheduler_xml = NULL;

	// --- Inizializzo la sezione critica delle varibili globali ---
	//InitializeCriticalSection( &SPVSchedulerCriticalSection );
	//SYSInitLock(&SPVSchedulerLock,"SPVSchedulerLock");
	// --- Azzero la memoria della lista dei profili di schedulazione ---
	memset( SPVSchedulerProfileList, 0, sizeof(SPV_SCHED_PROFILE) * SPV_SCHEDULER_MAX_PROFILES );
	SPVSchedulerProfileCount = 0;

	// --- Carico lo scheduler ---
	scheduler_xml = SPVSearchSchedulerDef( system_xml );
	if ( scheduler_xml != NULL )
	{
		ret_code = SPVLoadScheduler( scheduler_xml );
	}
	else
	{
		ret_code = SPV_SCHEDULER_INVALID_DEFINITION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo di schedulazione
///
/// \date [29.08.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVSchedulerClear( void )
{
	int ret_code = SPV_SCHEDULER_NO_ERROR; // Codice di ritorno

	// --- Elimino i parametri dello schedulatore ---
	ret_code = SPVFreeScheduler( );

	// --- Cancello la sezione critica delle varibili globali ---
	//DeleteCriticalSection( &SPVSchedulerCriticalSection );
	//SYSClearLock(&SPVSchedulerLock);

	// --- Azzero la memoria della lista dei profili di schedulazione ---
	memset( SPVSchedulerProfileList, 0, sizeof(SPV_SCHED_PROFILE) * SPV_SCHEDULER_MAX_PROFILES );
	SPVSchedulerProfileCount = 0;

	return ret_code;
}



//==============================================================================
/// Funzione per ottenere la data/ora di riferimento
///
/// 1 luglio 2002 ore 9:00 (ora solare)
///
/// \date [28.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void SPVGetRefDateTime( void * date_time )
{
	struct tm * dt_struct;

	if ( date_time != NULL )
	{
		dt_struct = (tm *)date_time;

		dt_struct->tm_sec    = 0  ;
		dt_struct->tm_min    = 0  ;
		dt_struct->tm_hour   = 0  ;
		dt_struct->tm_mday   = 1  ;
		dt_struct->tm_mon    = 6  ;
		dt_struct->tm_year   = 102; // 2002
		dt_struct->tm_wday   = 1  ;
		dt_struct->tm_yday   = 181;
		dt_struct->tm_isdst  = 0  ;
	}
}




//==============================================================================
/// Funzione per convertire una modalit� di schedulazione da codice a stringa
///
/// \date [29.08.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * SPVGetSchedulerModeString( int mode_code )
{
	char * mode_string = NULL;

    if ( mode_code == SPV_SCHEDULER_MODE_CONTINUE )
    {
		mode_string = (char*)malloc( sizeof(char) * ( strlen( "continue" ) + 1 ) ); // -MALLOC
		strcpy( mode_string, "continue" );
	}
	else if ( mode_code == SPV_SCHEDULER_MODE_FIXED )
	{
		mode_string = (char*)malloc( sizeof(char) * ( strlen( "fixed" ) + 1 ) ); // -MALLOC
		strcpy( mode_string, "fixed" );
	}
	else if ( mode_code == SPV_SCHEDULER_MODE_INTERVAL )
	{
		mode_string = (char*)malloc( sizeof(char) * ( strlen( "interval" ) + 1 ) ); // -MALLOC
		strcpy( mode_string, "interval" );
	}
	else if ( mode_code == SPV_SCHEDULER_MODE_STARTUP )
	{
		mode_string = (char*)malloc( sizeof(char) * ( strlen( "startup" ) + 1 ) ); // -MALLOC
		strcpy( mode_string, "startup" );
    }

    return mode_string;
}


//==============================================================================
/// Funzione per avviare l'esecuzione dello scheduler
///
/// \date [12.05.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVStartScheduler( void )
{
	int ret_code = SPV_SCHEDULER_NO_ERROR;
	int fun_code = SPV_SCHEDULER_NO_ERROR;

    // --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVScheduler->SPVStartScheduler");

	try
	{
		fun_code = SYSResumeThread( SPVScheduler.Thread );
		if ( fun_code == SPV_SCHEDULER_NO_ERROR )
		{
			// --- Salvo le propriet� di avvio dello scheduler ---
			SPVScheduler.Active     = true;
			SPVScheduler.StartTime  = (long) time( NULL );
		}
		else
		{
			ret_code = SPV_SCHEDULER_CRITICAL_ERROR;
                        char sDescr[255];

                        sprintf(sDescr, "Problema durante avvio del thread %u", fun_code);

//			SPVReportApplicEvent( NULL, 2, fun_code, "Problema durante avvio del thread", "SPVStartScheduler()" );
			SPVReportApplicEvent( NULL, 2, fun_code, "Avvio Thread Fallito", "SPVStartScheduler()" );
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVStartScheduler()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}

//==============================================================================
/// Funzione per interrompere l'esecuzione dello scheduler
///
/// \date [21.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVStopScheduler( void )
{
	int ret_code = SPV_SCHEDULER_NO_ERROR;
	int fun_code = SPV_SCHEDULER_NO_ERROR;

	// --- Entro nella sezione critica ---
	SPVEnterSchedulerData("SPVCheduler->SPVStopScheduler");

	try
	{
		/////fun_code = SYSSuspendThread( SPVScheduler.Thread );
		if ( fun_code == SPV_SCHEDULER_NO_ERROR )
	    {
	    	// --- Salvo le propriet� di interruzione dello scheduler ---
	    	SPVScheduler.Active   = false;
	    	SPVScheduler.StopTime = time( NULL );
	    }
	    else
		{
			ret_code = SPV_SCHEDULER_CRITICAL_ERROR;
			SPVReportApplicEvent( NULL, 2, fun_code, "Problema durante arresto del thread", "SPVStopScheduler()" );
		}
	}
	catch(...)
	{
		ret_code = SPV_SCHEDULER_FUNCTION_EXCEPTION;
		SPVReportApplicEvent( NULL, 2, SPV_SCHEDULER_FUNCTION_EXCEPTION, "Eccezione generale", "SPVStopScheduler()" );
	}

	// --- Esco dalla sezione critica ---
	SPVLeaveSchedulerData( );

	return ret_code;
}


//==============================================================================
/// Funzione per verificare che un'attivit� non sia gi� in esecuzione
///
/// \date [21.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVCheckActivity( SPV_SCHED_ACTIVITY * activity )
{
  int ret_code = SPV_SCHEDULER_NO_ERROR;

  // --- Entro nella sezione critica ---
  SPVEnterSchedulerData("SPVCheduler->SPVCheckActivity");

  ret_code = SPV_SCHEDULER_NOT_IMPLEMENTED;

  // --- Esco dalla sezione critica ---
  SPVLeaveSchedulerData( );

  return ret_code;
}

//==============================================================================
/// Funzione per lanciare l'esecuzione di un'attivit�
///
/// \date [21.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVLaunchActivity( SPV_SCHED_ACTIVITY * activity )
{
  int ret_code = SPV_SCHEDULER_NO_ERROR;

  // --- Entro nella sezione critica ---
  SPVEnterSchedulerData("SPVCheduler->SPVLaunchActivity");

  ret_code = SPV_SCHEDULER_NOT_IMPLEMENTED;
  
  // --- Esco dalla sezione critica ---
  SPVLeaveSchedulerData( );

  return ret_code;
}


//==============================================================================
// Telefin Supervisor Scheduler Module 1.0
//------------------------------------------------------------------------------
// Header modulo di schedulazione di procedure applicative (SPVScheduler.h)
// Progetto:  	Telefin Supervisor Server 1.0
//
// Revisione:	0.23 (17.06.2005 -> 11.02.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVScheduler.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [17.06.2005]:
// - Prima versione del modulo.
// 0.02 [20.06.2005]:
// - Aggiunta la funzione SPVSchedulerInit.
// - Aggiunta la funzione SPVEnterSchedulerData.
// - Aggiunta la funzione SPVLeaveSchedulerData.
// - Aggiunta la funzione SPVNullProfile.
// - Aggiunta la funzione SPVGetFirstFreeProfilePos.
// - Aggiunta la funzione SPVGetSchedulerModeCode.
// - Aggiunta la funzione SPVGetSchedulerModeString.
// - Aggiunta la funzione SPVLoadActivity.
// - Aggiunta la funzione SPVLoadActivityList.
// - Aggiunta la funzione SPVLoadProfile.
// - Modificata la definizione del tipo SPV_SCHED_ACTIVITY.
// 0.03 [21.06.2005]:
// - Aggiunta la definizione del tipo SPV_SCHED_INFO.
// - Aggiunta la funzione SPVClearActivityList.
// - Modificata ala funzione SPVLoadProfile.
// - Aggiunta la funzione SPVAddNewProfile.
// - Aggiunta la funzione SPVGetProfilePos.
// - Aggiunta la funzione SPVClearProfile.
// - Aggiunta la funzione SPVSchedulerThreadFunction.
// - Aggiunta la funzione SPVLoadScheduler.
// - Aggiunta la funzione SPVClearScheduler.
// - Aggiunta la funzione SPVStartScheduler.
// - Aggiunta la funzione SPVStopScheduler.
// - Aggiunta la funzione SPVProcessProfile.
// - Aggiunta la funzione SPVCheckActivity.
// - Aggiunta la funzione SPVLaunchActivity.
// 0.04 [22.06.2005]:
// - Modificata la funzione SPVSchedulerThreadFunction.
// - Modificata la funzione SPVProcessProfile.
// - Modificata la funzione SPVScheduleDevice.
// 0.05 [23.06.2005]:
// - Inclusa la libreria di gestione delle funzioni data/ora <time.h>.
// - Aggiunta la funzione SPVCheckDayOfWeek.
// - Aggiunta la funzione SPVCheckFromTo.
// - Aggiunta la funzione SPVCheckInterval.
// - Aggiunta la funzione SPVCheckFixedStart.
// - Aggiunta la funzione SPVProcessActivity.
// - Modificata la funzione SPVProcessProfile.
// - Modificata la funzione SPVScheduleDevice.
// 0.06 [24.06.2005]:
// - Modificata la funzione SPVSchedulerInit.
// - Aggiunta la funzione SPVSearchSchedulerDef.
// - Aggiunta la funzione SPVGetRefDateTime.
// - Modificata la funzione SPVLoadActivity.
// - Modificata la funzione SPVAddNewProfile.
// - Modificata la funzione SPVLoadScheduler.
// - Modificata la funzione SPVCheckFromTo.
// - Modificata la funzione SPVCheckInterval.
// - Modificata la funzione SPVProcessActivity.
// - Modificata la funzione SPVScheduleDevice.
// 0.07 [27.06.2005]:
// - Modificata la funzione SPVCheckInterval.
// - Modificata la funzione SPVProcessActivity.
// 0.08 [28.06.2005]:
// - Modificata la funzione SPVGetRefDateTime.
// - Modificata la funzione SPVSchedulerThreadFunction.
// - Modificata la funzione SPVCheckInterval.
// 0.09 [29.06.2005]:
// - Modificata la funzione SPVLoadActivity.
// - Modificata la funzione SPVCheckFromTo.
// - Modificata la funzione SPVCheckInterval.
// - Modificata la funzione SPVCheckFixedStart.
// 0.10 [30.06.2005]:
// - Modificata la funzione SPVScheduleDevice.
// 0.11 [11.07.2005]:
// - Modificata la funzione SPVClearProfile.
// - Modificata la funzione SPVCheckFixedStart.
// 0.12 [29.08.2005]:
// - Aggiunta la funzione SPVSchedulerClear.
// - Rinominata la funzione SPVClearScheduler in SPVFreeScheduler.
// - Modificafa la funzione SPVFreeScheduler.
// 0.13 [11.01.2006]:
// - Modificato SPVClearProfile().
// 0.14 [23.05.2006]:
// - Modificato SPVFreeScheduler().
// 0.15 [21.08.2006]:
// - Modificata SPVSchedulerThreadFunction() (parametrizzato delay).
// 0.16 [22.08.2006]:
// - Modificata SPVSchedulerThreadFunction().
// 0.17 [23.08.2006]:
// - Modificata SPVSchedulerThreadFunction().
// 0.18 [24.08.2006]:
// - Modificata SPVProcessActivity().
// 0.19 [25.08.2006]:
// - Modificata SPVSchedulerThreadFunction() (aggiunto salvataggio contatori).
// 0.20 [29.08.2006]:
// - Aggiunto nuovo tipo di schedulazione 'startup'.
// 0.21 [12.05.2007]:
// - Modificata SPVProcessActivity().
// - Modificata SPVGetFirstFreeProfilePos().
// - Modificata SPVLoadActivity().
// - Modificata SPVLoadActivityList().
// - Modificata SPVClearActivityList().
// - Modificata SPVLoadProfile().
// - Modificata SPVAddNewProfile().
// - Modificata SPVGetProfilePos().
// - Modificata SPVClearProfile().
// - Modificata SPVSchedulerThreadFunction().
// - Modificata SPVLoadScheduler().
// - Modificata SPVFreeScheduler().
// - Modificata SPVStartScheduler().
// - Modificata SPVProcessProfile().
// - Modificata SPVScheduleDevice().
// 0.22 [21.09.2007]:
// - Assegnato un nome all'attributo Name di SPVSchedulerThreadFunction
// 0.23 [11.02.2008]:
// - Introdotta nuova gestione delle eccezioni nelle seguenti funzioni:
//		- SPVProcessActivity

//==============================================================================

#ifndef SPVSchedulerH
#define SPVSchedulerH

//==============================================================================
// Inclusioni
//------------------------------------------------------------------------------
#include "XMLInterface.h"
#include "SYSKernel.h"
#include "SPVSystem.h"

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SPV_SCHEDULER_MAX_PROFILES              32
#define SPV_SCHEDULER_MAX_ACTIVITIES            16
#define SPV_SCHEDULER_MAX_FIXED_STARTS          32

#define SPV_SCHEDULER_MIN_DELAY                 4000
#define SPV_SCHEDULER_FROM_TO_PRECISION         5
#define SPV_SCHEDULER_FIXED_PRECISION           5

#define SPV_SCHEDULER_MODE_CONTINUE         	365
#define SPV_SCHEDULER_MODE_INTERVAL         	865
#define SPV_SCHEDULER_MODE_FIXED            	482
#define SPV_SCHEDULER_MODE_STARTUP          	251

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_SCHEDULER_NO_ERROR                	0
#define SPV_SCHEDULER_NOT_IMPLEMENTED           2300
#define SPV_SCHEDULER_PROFILE_LIST_FULL         2301
#define SPV_SCHEDULER_INVALID_PROFILE_LIST      2302
#define SPV_SCHEDULER_CRITICAL_ERROR            2303
#define SPV_SCHEDULER_DEFINITION_ERROR          2304
#define SPV_SCHEDULER_INVALID_PROFILE           2305
#define SPV_SCHEDULER_INVALID_PROFILE_ID        2306
#define SPV_SCHEDULER_INVALID_ACTIVITY_LIST     2307
#define SPV_SCHEDULER_INVALID_DEFINITION        2308
#define SPV_SCHEDULER_INVALID_DEVICE            2309
#define SPV_SCHEDULER_INVALID_ACTIVITY          2310
#define SPV_SCHEDULER_INVALID_PROCEDURE_LIST    2311
#define SPV_SCHEDULER_INVALID_CLASS_LIST        2312
#define SPV_SCHEDULER_INVALID_ACTIVITY_MODE     2313
#define SPV_SCHEDULER_FUNCTION_EXCEPTION	    2314
#define SPV_SCHEDULER_INVALID_THREAD            2315
#define SPV_SCHEDULER_INVALID_STRUCT			2316
#define SPV_SCHEDULER_INVALID_PARAMETER			2317

//==============================================================================
// Definizione di strutture e tipo
//------------------------------------------------------------------------------

//==============================================================================
/// Definizione della struttura attivit� di schedulazione
///
/// \date [20.06.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_SCHED_ACTIVITY
{
  int                   ID        ; // Codice numerico di identificazione
  char                * Name      ; // Nome dell'attivit� di schedulazione
  int                   Class     ; // Classe della procedura applicativa
  char                  DOW       ; // Day Of Week
  long                  From      ; // Inizio periodo di schedulazione
  long                  To        ; // Fine periodo di schedulazione
  int                   Mode      ; // Modalit� di schedulazione
  long                  Interval  ; // Intervallo di schedulazione
  long                  Start[SPV_SCHEDULER_MAX_FIXED_STARTS]; // Istanti di schedulazione fissa
  int                   StartCount; // Numero di istanti di schedulazione fissa
  XML_ELEMENT         * Def       ; // Ptr alla definizione XML dell'attivit�
}
SPV_SCHED_ACTIVITY;

//==============================================================================
/// Definizione della struttura profilo di schedulazione
///
/// \date [17.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_SCHED_PROFILE
{
  int                   ID            ; // Codice numerico di identificazione
  char                * Name          ; // Nome del profilo
  SPV_SCHED_ACTIVITY  * ActivityList  ; // Lista delle attivit�
  int                   ActivityCount ; // Numero delle attivit�
  XML_ELEMENT         * Def           ; // Ptr alla definizione XML del profilo
}
SPV_SCHED_PROFILE;

//==============================================================================
/// Definizione della struttura informazioni scheduler
///
/// \date [21.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_SCHED_INFO
{
  SYS_THREAD  * Thread    ; // Puntatore al thread
  bool          Active    ; // Flag di scheduler attivo/non attivo
  long          StartTime ; // Istante ultimo avvio dello scheduler
  long          StopTime  ; // Istante ultima interruzione dello scheduler
  XML_ELEMENT * Def       ; // Ptr alla definizione XML dello shceduler
}
SPV_SCHED_INFO;

//==============================================================================
// Variabili Globali esterne
//------------------------------------------------------------------------------

//==============================================================================
// Dichiarazione Funzioni
//------------------------------------------------------------------------------

// Funzione di inizializzazione del modulo
int                     SPVSchedulerInit          ( XML_ELEMENT * system_xml );
// Funzione per chiudere il modulo di schedulazione
int                     SPVSchedulerClear         ( void );
// Funzione per avviare l'esecuzione dello scheduler
int                     SPVStartScheduler         ( void );
// Funzione per interrompere l'esecuzione dello scheduler
int                     SPVStopScheduler          ( void );
/*
// Funzione per entrare nella sezione critica delle variabili globali
void                    SPVEnterSchedulerData     ( void );
// Funzione per uscire dalla sezione critica delle variabili globali
void                    SPVLeaveSchedulerData     ( void );
// Funzione per cercare la definizione dello scheduler in quella di sistema
XML_ELEMENT           * SPVSearchSchedulerDef     ( XML_ELEMENT * system_xml );
// Funzione per ottenere la data/ora di riferimento
void                    SPVGetRefDateTime         ( void * date_time );
// Funzione per verificare se un profilo � nullo
bool                    SPVNullProfile            ( SPV_SCHED_PROFILE profile );
// Funzione per cercare la prima posizione libera
int                     SPVGetFirstFreeProfilePos ( void );
// Funzione per convertire una modalit� di schedulazione da stringa a codice
int                     SPVGetSchedulerModeCode   ( char * mode_string );
// Funzione per convertire una modalit� di schedulazione da codice a stringa
char                  * SPVGetSchedulerModeString ( int mode_code );
// Funzione per caricare la definizione di un'attivit�
SPV_SCHED_ACTIVITY      SPVLoadActivity           ( XML_ELEMENT * activity_xml );
// Funzione per caricare una lista di attivit� di un profilo di schedulazione
int                     SPVLoadActivityList       ( SPV_SCHED_PROFILE * profile );
// Funzione per cancellare la lista di attivit� di un profilo di schedulazione
int                     SPVClearActivityList      ( SPV_SCHED_PROFILE * profile );
// Funzione per caricare la definizione di un profilo
SPV_SCHED_PROFILE       SPVLoadProfile            ( XML_ELEMENT * profile_xml );
// Funzione per aggiungere un nuovo profilo di schedulazione alla lista
int                     SPVAddNewProfile          ( SPV_SCHED_PROFILE profile );
// Funzione per ottenere la posizione in lista di un profilo di schedulazione
int                     SPVGetProfilePos          ( int ID );
// Funzione per togliere dalla lista un profilo di schedulazione
int                     SPVClearProfile           ( int ID );
// Funzione di attivazione per il thread dello scheduler
unsigned long __stdcall SPVSchedulerThreadFunction( void * parameter );
// Funzione per caricare tutti i parametri dello scheduler
int                     SPVLoadScheduler          ( XML_ELEMENT * scheduler_xml );
// Funzione per cancellare tutti i parametri dello scheduler
int                     SPVFreeScheduler          ( void );
// Funzione per verificare se posso schedulare in un giorno della settimana
bool                    SPVCheckDayOfWeek         ( char DOW, long now );
// Funzione per verificare se posso schedulare in un orario giornaliero
bool                    SPVCheckFromTo            ( long from, long to, long now );
// Funzione per verificare se posso lanciare un'esecuzione dopo un tempo
bool                    SPVCheckInterval          ( long last, long interval, long now );
// Funzione per verificare se posso lanciare un'esecuzione in un preciso istante
bool                    SPVCheckFixedStart        ( long start, long now );
// Funzione per processare una singola attivit� di un profilo di schedulazione
int                     SPVProcessActivity        ( SPV_DEVICE * device, SPV_SCHED_ACTIVITY * activity, long now );
// Funzione per processare un profilo di schedulazione
int                     SPVProcessProfile         ( SPV_DEVICE * device, SPV_SCHED_PROFILE * profile, long now );
// Funzione per processare la schedulazione di una periferica
int                     SPVScheduleDevice         ( SPV_DEVICE * device );
// Funzione per schedulare le attivita' sulla procedure applicative
int                     SPVScheduleProcedures     ( SPV_SYSTEM * system );
// Funzione per verificare che un'attivit� non sia gi� in esecuzione
int                     SPVCheckActivity          ( SPV_SCHED_ACTIVITY * activity );
// Funzione per lanciare l'esecuzione di un'attivit�
int                     SPVLaunchActivity         ( SPV_SCHED_ACTIVITY * activity );
*/
#endif

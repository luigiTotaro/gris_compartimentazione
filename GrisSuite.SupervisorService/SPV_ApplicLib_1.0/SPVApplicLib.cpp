//==============================================================================
// Telefin Supervisor Applicative Library 1.0
//------------------------------------------------------------------------------
// Libreria Applicativa (SPVApplicLib.cpp)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	0.26 (01.09.2004 -> 02.10.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVApplicLib.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVApplicLib.h
//==============================================================================


#pragma hdrstop
#include "SPVApplicLib.h"
#include "SPVApplicative.h"
#include "SPVComProt.h"
#include "SPVDBManager.h"
#include "SPVDBInterface.h"
#include "cnv_lib.h"
#include <time.h>

#pragma package(smart_init)

//==============================================================================
// Variabili globali (extern)
//------------------------------------------------------------------------------
SPV_FSM_FUNCTION_PTR    SPVApplicLibFunctionList  [SPV_APPLICLIB_FUNCTION_COUNT];
SPV_FSM_FUNCTION_CODE   SPVApplicLibFunctionCodes [SPV_APPLICLIB_FUNCTION_COUNT];
int                     SPVApplicLibFunctionCount = 0;

//==============================================================================
/// Funzione per correggere la data ricavata dagli eventi di periferiche Telefin
/// Toglie un'ora nel caso DST
///
/// \date [11.11.2008]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
long _AdjustEventDateTime( long date_time )
{
	struct tm *	time_now  = NULL; // Data ora formato tm
	time_t		secs_now        ; // Data ora formato time_t
	long		adjusted_time = date_time;

	if ( date_time != 0 )
	{
    	date_time += SPV_APPLIC_EVENT_DATETIME_OFFSET_STANDARD;
		secs_now = (time_t)date_time;
		time_now = localtime( &secs_now );
		if ( time_now->tm_isdst == 1 )
		{
			adjusted_time = date_time - 3600;
		}
		else
		{
			adjusted_time = date_time;
		}
	}

	return adjusted_time;
}

//==============================================================================
/// Funzione di caricamento della libreria
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [01.08.2012]
/// \author Enrico Alborali
/// \version 1.00
//------------------------------------------------------------------------------
void  SPVApplicLibInit( void )
{
	// --- Inizializzo la lista delle funzioni di libreria ---
	SPVAppLibReset();
	// --- Aggiungo le funzioni di libreria alla lista ---
	SPVAppLibAddFunction( "100" , SPVApplicLib_100_ExecComProtCommand 		);
	SPVAppLibAddFunction( "999" , SPVApplicLib_999_Error              		);
	SPVAppLibAddFunction( "2533", SPVAPPLIB_2533_StreamDefault         		);
	SPVAppLibAddFunction( "2541", SPVAPPLIB_2541_GenericStreamSave     		);
	SPVAppLibAddFunction( "2542", SPVAPPLIB_2542_TLFBlockStreamSave    		);
	SPVAppLibAddFunction( "7145", SPVAPPLIB_7145_MultiArrayFieldBlockStreamSave );
	SPVAppLibAddFunction( "2553", SPVAPPLIB_2553_StreamGet            		);
	SPVAppLibAddFunction( "2543", SPVAPPLIB_2543_SSP133StreamSave         	);
	SPVAppLibAddFunction( "2544", SPVAPPLIB_2544_SSP133IntegerStreamSave  	);
	SPVAppLibAddFunction( "2545", SPVAPPLIB_2545_SSP133NTStringStreamSave 	);
	SPVAppLibAddFunction( "2562", SPVAPPLIB_2562_TLFBlockEventStreamSave	);
	SPVAppLibAddFunction( "2397", SPVAPPLIB_2397_SetCondDeviceOffline		);
	SPVAppLibAddFunction( "2398", SPVAPPLIB_2398_SetDeviceOnline          	);
	SPVAppLibAddFunction( "2399", SPVAPPLIB_2399_SetDeviceOffline         	);
	SPVAppLibAddFunction( "2400", SPVAPPLIB_2400_DeletePayload         		);
	SPVAppLibAddFunction( "3334", SPVAPPLIB_3334_DecodeDevEventFile       	);
	SPVAppLibAddFunction( "3234", SPVAPPLIB_3234_DecodeSysEventFile       	);
	SPVAppLibAddFunction( "5325", SPVAPPLIB_5325_Test       	);
}

//==============================================================================
/// Funzione per azzerare la lista delle funzioni di libreria
///
/// Non restituisce alcun valore.
///
/// \date [26.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void SPVAppLibReset( void )
{
	SPVApplicLibFunctionCount = 0;
	memset( SPVApplicLibFunctionList  , 0, sizeof(SPV_FSM_FUNCTION_PTR)  * SPV_APPLICLIB_FUNCTION_COUNT );
	memset( SPVApplicLibFunctionCodes , 0, sizeof(SPV_FSM_FUNCTION_CODE) * SPV_APPLICLIB_FUNCTION_COUNT );
}

//==============================================================================
/// Funzione per aggiungere una funzione alla lista della libreria
///
/// NOTA: Questa funzione deve essere usata solo per l'inizializzazione della
///       libreria.
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [26.05.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAppLibAddFunction( char * code_string, SPV_FSM_FUNCTION_PTR ptf )
{
	int                     ret_code  = SPV_APPLICLIB_NO_ERROR;

	if ( code_string != NULL )
	{
		if ( ptf != NULL )
		{
			if ( SPVApplicLibFunctionCount < SPV_APPLICLIB_FUNCTION_COUNT )
			{
				// --- Copio i parametri della funzione nella lista ---
				SPVApplicLibFunctionList[SPVApplicLibFunctionCount] = ptf;
				strcpy( SPVApplicLibFunctionCodes[SPVApplicLibFunctionCount], code_string );
				// --- Aggiorno il contatore globale delle funzioni ---
				SPVApplicLibFunctionCount++;
			}
			else
			{
				ret_code = SPV_APPLICLIB_LIST_FULL;
			}
		}
		else
		{
			ret_code = SPV_APPLICLIB_INVALID_PTF;
		}
	}
	else
	{
		ret_code = SPV_APPLICLIB_INVALID_CODE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione generica per salvare stream ricavati da un payload
///
/// In caso di errore restituisce un valore non nullo.
///
/// NOTA: 	Dato1: Procedura
///         Dato2: Payload
///
/// \date [02.03.2012]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SPVAppLibStreamSave( void * input, void * output, int payload_type  )
{
	int                   ret_code  = SPV_APPLICLIB_STATE_NO_ERROR;
	int                   fun_code  = SPV_APPLIC_FUNCTION_SUCCESS;
    int                   streamID  = 0   ; // Codice del comando
    SPV_PROCEDURE       * procedure = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA        * data_in   = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA        * data_out  = NULL; // Ptr ai dati in uscita
    SPV_COMPROT_PAYLOAD * payload   = NULL; // Ptr a payload
    SPV_DEV_STREAM      * stream        = NULL; // Ptr a stream di periferica
    SPV_DEV_STREAM_BUFFER stream_buffer = { 0, NULL }; // Buffer dello stream periferica
	SPV_DEVICE          * device        = NULL; // Ptr a struttura periferica
	SPV_DEV_ACK			  ack_list[20000]	; // Lista ACK
	int					  ack_num	= 0		; // Numero di ACK

    // --- Recupero i puntatori delle strutture dati di IO ---
    data_in   = (SPV_FSM_DATA*) input;
    data_out  = (SPV_FSM_DATA*) output;
    // --- Recupero dato #1 ---
    if ( data_in != NULL ) // Controllo il ptr dell'input
    {
    	procedure = (SPV_PROCEDURE*) data_in->Data;
    	if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
    	{
			device = procedure->Device;
			// --- Se la periferica prima era offline ---
			if ( device->SupStatus.Offline == true )
			{
				fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
			}
			device->SupStatus.Offline = false;
			device->SupStatus.Unknown = false;
			device->SupStatus.CommErrors = 0;
			if ( procedure->State != NULL )
			{
				streamID = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
			}
			// --- Recupero dato #2 - Payload ---
			if ( data_in->Next != NULL )
			{
				payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
				if ( payload != NULL )
				{
					// --- Estrazione dello stream dai payload ---
					stream_buffer = SPVExtractStream( device, payload, streamID, payload_type, NULL );
				    // --- Salvataggio stream in memoria ---
				    fun_code = SPVRAMSaveStream( device, streamID, stream_buffer );
				    // --- Recupero il ptr alla struttura stream ---
					stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
					// --- Recupero la lista degli ACK ---
					fun_code = SPVDBMLoadDeviceAcks( &ack_list[0], &ack_num );
				    // --- Calcolo la severita' dello stream ---
					fun_code = SPVGetDevStreamSeverity( stream, device, &ack_list[0], ack_num );
					// --- Ricalcolo la severita' della periferica a cui appartiene lo stream ---
					fun_code = SPVGetDevSeverity( device, &ack_list[0], ack_num );
				    // --- Salvataggio stato periferica nel database ---
				    fun_code = SPVDBSaveDeviceStatus( device );
				    // --- Salvataggio stream nel database ---
					fun_code = SPVDBSaveStream( device, stream );
				    // --- Salvataggio field in RAM e nel database ---
				    fun_code = SPVDBMSaveStreamFields( (void*)device, (void*)stream );
					// --- Salvataggio reference ---
				    SPVDBSaveStreamReferences( device, stream );
				    // --- Cancellazione payload ---
				    SPVDeletePayloadList( payload );
				    // --- Tolgo la lista payload dai dati della procedura ---
					data_in->Next->Data = NULL;
				    // --- Tolgo la lista payload dai dati della procedura ---
				    data_in->Next->Next->Next->Next->Data = NULL;
				    /* TODO 1 -oEnrico -cErrors : Gestire gli errori delle funzioni */
				}
			}
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione generica per salvare stream ricavati da un payload
///
/// In caso di errore restituisce un valore non nullo.
///
/// NOTA: 	Dato1: Procedura
///         Dato2: Payload
///
/// \date [05.03.2012]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SPVAppLibMultiArrayFieldStreamSave( void * input, void * output, int payload_type  )
{
	int						ret_code			= SPV_APPLICLIB_STATE_NO_ERROR;
	int						fun_code			=	SPV_APPLIC_FUNCTION_SUCCESS;
	int						streamID			=	0   ; // Codice del comando
	SPV_PROCEDURE *			procedure			=	NULL; // Ptr a struttura procedura
	SPV_FSM_DATA *			data_in				=	NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA *			data_out			=	NULL; // Ptr ai dati in uscita
	SPV_COMPROT_PAYLOAD *	payload				=	NULL; // Ptr a payload
	SPV_DEV_STREAM_FIELD *	field				=	NULL; // Ptr a campo di stream di periferica
	SPV_DEV_STREAM *		stream				=	NULL; // Ptr a stream di periferica
	SPV_DEV_STREAM_BUFFER 	stream_buffer 		= 	{ 0, NULL }; // Buffer dello stream periferica
	SPV_DEV_STREAM_BUFFER 	field_buffer		= 	{ 0, NULL }; // Buffer dello stream field periferica
	SPV_DEVICE *			device        		= 	NULL; // Ptr a struttura periferica
	char *					params_string		=	NULL;
	int						params_count		=	0	;
	char *					value_string		=	NULL;
	int						value_int			=	0	;
	int						field_count			=	0	;
	int						field_id_array[24]			;
	int						payload_id_array[24]		;
	unsigned int			packet_count		=	0	;
	unsigned char *			curr_buffer			=	NULL;
	unsigned char *			prev_buffer			= 	NULL;
	int						curr_buffer_len		=	0	;
	int						prev_buffer_len		=	0	;
	SPV_DEV_ACK			  	ack_list[20000]	; // Lista ACK
	int					  	ack_num	= 0		; // Numero di ACK

    // --- Recupero i puntatori delle strutture dati di IO ---
    data_in   = (SPV_FSM_DATA*) input;
    data_out  = (SPV_FSM_DATA*) output;
	// --- Recupero dato #1 ---
    if ( data_in != NULL ) // Controllo il ptr dell'input
    {
    	procedure = (SPV_PROCEDURE*) data_in->Data;
    	if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
    	{
			device = procedure->Device;
			// --- Se la periferica prima era offline ---
			if ( device->SupStatus.Offline == true )
			{
				fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
			}
			device->SupStatus.Offline = false;
			device->SupStatus.Unknown = false;
			device->SupStatus.CommErrors = 0;
			if ( procedure->State != NULL )
			{
				// Recuper i parametri necessari
				params_string = XMLGetValue( (XML_ELEMENT*)procedure->State->Def, "params" );
				if ( params_string != NULL ) // Controllo la stringa dei parametri
				{
					params_count = XMLGetParamsCount( params_string );
					if ( params_count > 0 ) // Se ho almeno un parametro
					{
						for ( int p = 0; p < params_count; p++ ) // Ciclo sui parametri
						{
							value_string = XMLGetToken( params_string, ",", p ); // Estraggo il p-esimo valore -MALLOC
							if ( value_string != NULL )
							{
								value_int = cnv_CharPToUInt8( value_string );
								free( value_string ); // -FREE
								// ID dello stream
								if (p == 0) {
									streamID = value_int;
								}
								// Numero di campi dello stream
								if (p == 1) {
									field_count = value_int;
								}
								// ID dei campi dello stream
								if (p > 1 && p < (1+field_count+1)) {
									field_id_array[p-2] = value_int;
								}
								// ID dei payload da estrarre
								if (p > (1+field_count) && p < (1+(field_count*2)+1)) {
                                    payload_id_array[p-2-field_count] = value_int;
								}
							}
							else
							{
								/* MANCA UN PARAMETRO */
                            }
						}
					}
				}
			}
			// --- Recupero dato #2 - Payload ---
			if ( data_in->Next != NULL )
			{
				payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
				if ( payload != NULL )
				{
					// --- Recupero il ptr alla struttura stream ---
					stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
					for ( int f = 0; f < field_count; f++ ) // Ciclo sui field/payload
					{
						// --- Estrazione dello stream dai payload ---
						field_buffer = SPVExtractStream( device, payload, payload_id_array[f], payload_type, &packet_count );

						if (prev_buffer != NULL) free(prev_buffer); // -FREE
						prev_buffer = curr_buffer;
						prev_buffer_len = curr_buffer_len;
						curr_buffer_len += field_buffer.Len;
						curr_buffer = (unsigned char *)malloc(sizeof(unsigned char)*(curr_buffer_len)); // -MALLOC
						if (prev_buffer_len>0) memcpy( (void*)curr_buffer,prev_buffer,sizeof(unsigned char)*(prev_buffer_len));
						memcpy( (void*)&curr_buffer[prev_buffer_len],(void*)field_buffer.Ptr,sizeof(unsigned char)*(field_buffer.Len));

						field = SPVSearchStreamField( stream->FieldList, f );
						if ( SPVValidDevField( field ) )
						{
							SPVReallocDevFieldCapacity( field, packet_count );
							//field->Capacity = packet_count; // Numero pacchetti = capacita' array field
						}
						if (field_buffer.Ptr != NULL) free(field_buffer.Ptr); // -FREE
					}

					if (prev_buffer != NULL) free(prev_buffer); // -FREE
					stream_buffer.Len = curr_buffer_len;
					stream_buffer.Ptr = curr_buffer;

					// --- Salvataggio stream in memoria ---
					fun_code = SPVRAMSaveStream( device, streamID, stream_buffer );
					// --- Recupero la lista degli ACK ---
					fun_code = SPVDBMLoadDeviceAcks( &ack_list[0], &ack_num );
					// --- Calcolo la severita' dello stream ---
					fun_code = SPVGetDevStreamSeverity( stream, device, &ack_list[0], ack_num );
					// --- Ricalcolo la severita' della periferica a cui appartiene lo stream ---
					fun_code = SPVGetDevSeverity( device, &ack_list[0], ack_num );
					// --- Salvataggio stato periferica nel database ---
					fun_code = SPVDBSaveDeviceStatus( device );
					// --- Salvataggio stream nel database ---
					fun_code = SPVDBSaveStream( device, stream );
				    // --- Salvataggio field in RAM e nel database ---
				    fun_code = SPVDBMSaveStreamFields( (void*)device, (void*)stream );
					// --- Salvataggio reference ---
				    //SPVDBSaveStreamReferences( device, stream );
				    // --- Cancellazione payload ---
				    SPVDeletePayloadList( payload );
				    // --- Tolgo la lista payload dai dati della procedura ---
				    data_in->Next->Data = NULL;
				    // --- Tolgo la lista payload dai dati della procedura ---
				    data_in->Next->Next->Next->Next->Data = NULL;
				    /* TODO 1 -oEnrico -cErrors : Gestire gli errori delle funzioni */
				}
			}
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione generica per salvare stream ricavati dai propri valori di default
///
/// In caso di errore restituisce un valore non nullo.
///
/// NOTE:	Dato1: Procedura
///			il parametro params della funzione per ora non e' utilizzato
///
/// \date [05.03.2012]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVAppLibStreamDefault( void * input, void * output, int params )
{
	int                   ret_code  = SPV_APPLICLIB_STATE_NO_ERROR;
	int                   fun_code  = SPV_APPLIC_FUNCTION_SUCCESS;
	int                   streamID  = 0   ; // Codice del comando
	SPV_PROCEDURE       * procedure = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA        * data_in   = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA        * data_out  = NULL; // Ptr ai dati in uscita
	SPV_DEV_STREAM      * stream        = NULL; // Ptr a stream di periferica
	SPV_DEV_STREAM_BUFFER stream_buffer = { 0, NULL }; // Buffer dello stream periferica
	SPV_DEVICE          * device        = NULL; // Ptr a struttura periferica
	SPV_DEV_ACK			  ack_list[20000]	; // Lista ACK
	int					  ack_num	= 0		; // Numero di ACK

	// --- Recupero i puntatori delle strutture dati di IO ---
	data_in   = (SPV_FSM_DATA*) input;
	data_out  = (SPV_FSM_DATA*) output;
	// --- Recupero dato #1 ---
	if ( data_in != NULL ) // Controllo il ptr dell'input
	{
		procedure = (SPV_PROCEDURE*) data_in->Data;
		if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
		{
			device = procedure->Device;
			// --- Se la periferica prima era offline ---
			if ( device->SupStatus.Offline == true )
			{
				fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
			}
			device->SupStatus.Offline = false;
			device->SupStatus.Unknown = false;

			if ( procedure->State != NULL )
			{
				streamID = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
			}
			// --- Ottengo il buffer di default per lo stream ---
			stream_buffer = SPVDefaultDevFrameBuffer( device, streamID );
			// --- Salvataggio stream in memoria ---
			fun_code = SPVRAMSaveStream( device, streamID, stream_buffer );
			// --- Recupero il ptr alla struttura stream ---
			stream = SPVSearchStream( (SPV_DEV_STREAM*)device->StreamList, streamID );
			// --- Recupero la lista degli ACK ---
			fun_code = SPVDBMLoadDeviceAcks( &ack_list[0], &ack_num );
			// --- Calcolo la severita' dello stream ---
			fun_code = SPVGetDevStreamSeverity( stream, device, &ack_list[0], ack_num );
			// --- Ricalcolo la severita' della periferica a cui appartiene lo stream ---
			fun_code = SPVGetDevSeverity( device, &ack_list[0], ack_num );
			// --- Salvataggio stato periferica nel database ---
			fun_code = SPVDBSaveDeviceStatus( device );
			// --- Salvataggio stream nel database ---
			fun_code = SPVDBSaveStream( device, stream );
			// --- Salvataggio field in RAM e nel database ---
			fun_code = SPVDBMSaveStreamFields( (void*)device, (void*)stream );
			// --- Salvataggio reference ---
			SPVDBSaveStreamReferences( device, stream );
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione generica per decodifcare uno stream di eventi Telefin
///
/// \date [01.09.2011]
/// \author Enrico Alborali
/// \version 1.02
//------------------------------------------------------------------------------
SPV_DEVICE_EVENT * _EventStreamDecode( SPV_DEV_STREAM_BUFFER * buffer, int event_type, int event_len, int * count )
{
	SPV_DEVICE_EVENT * event_list = NULL;
	int event_count = 0;
	int e = 0;
	char datetime_str[9]={0,0,0,0,0,0,0,0,0};
	long datetime_int = 0;
	char * big_endian_buffer = NULL;

	if ( buffer != NULL )
	{
		switch (event_type)
		{
			case SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_SYS: // 1
				event_count = buffer->Len / 16;
				event_list = (SPV_DEVICE_EVENT*)malloc(sizeof(SPV_DEVICE_EVENT)* event_count);

				for (e = 0; e < event_count; e++)
				{
					memset( (void*)&(event_list[e].Data[0]), 0, 32 );
					event_list[e].VCC		= SPV_APPLIC_DEVICE_EVENT_VALIDITY_CHECK_CODE;
					event_list[e].ID		= e;
					event_list[e].Category	= SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_SYS;

					memcpy( &datetime_str[0], (void*)&buffer->Ptr[(e*16)+0], 8 ); // DataOra
					datetime_str[8] = '\0';
					datetime_int = cnv_CharPToUHex32( datetime_str );
					datetime_int = _AdjustEventDateTime(datetime_int);
					event_list[e].DateTime = datetime_int;

					memcpy((void*)&(event_list[e].Data[0]), (void*)&buffer->Ptr[(e*16)+8], 8);
					event_list[e].DataLen	= 8;
				}
			break;
			case SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_APP: // 2
				event_count = buffer->Len / 32;
				event_list = (SPV_DEVICE_EVENT*)malloc(sizeof(SPV_DEVICE_EVENT)* event_count);

				for (e = 0; e < event_count; e++) {
					memset( (void*)&(event_list[e].Data[0]), 0, 32 );
					event_list[e].VCC		= SPV_APPLIC_DEVICE_EVENT_VALIDITY_CHECK_CODE;
					event_list[e].ID		= e;
					event_list[e].Category	= SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_APP;

					memcpy( &datetime_str[0], (void*)&buffer->Ptr[(e*32)+0], 8 ); // DataOra
					datetime_str[8] = '\0';
					datetime_int = cnv_CharPToUHex32( datetime_str );
					datetime_int = _AdjustEventDateTime(datetime_int);
					event_list[e].DateTime = datetime_int;

					memcpy((void*)&(event_list[e].Data[0]), (void*)&buffer->Ptr[(e*32)+8], 24);
					event_list[e].DataLen	= 24;
				}
			break;
			case SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_FDS: // 3
				event_count = buffer->Len / 20;
				event_list = (SPV_DEVICE_EVENT*)malloc(sizeof(SPV_DEVICE_EVENT)* event_count);

				for (e = 0; e < event_count; e++) {
					memset( (void*)&(event_list[e].Data[0]), 0, 32 );
					event_list[e].VCC		= SPV_APPLIC_DEVICE_EVENT_VALIDITY_CHECK_CODE;
					event_list[e].ID		= e;
					event_list[e].Category	= SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_FDS;

					memcpy( &datetime_str[0], (void*)&buffer->Ptr[(e*20)+0], 8 ); // DataOra
					datetime_str[8] = '\0';
					datetime_int = cnv_CharPToUHex32( datetime_str );
					datetime_int = _AdjustEventDateTime(datetime_int);
					event_list[e].DateTime = datetime_int;

					memcpy((void*)&(event_list[e].Data[0]), (void*)&buffer->Ptr[(e*20)+8], 12);
					event_list[e].DataLen	= 12;
				}
			break;
			case SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_PZi: // 4
				event_count = buffer->Len / 28;
				event_list = (SPV_DEVICE_EVENT*)malloc(sizeof(SPV_DEVICE_EVENT)* event_count);

				for (e = 0; e < event_count; e++) {
					memset( (void*)&(event_list[e].Data[0]), 0, 32 );
					event_list[e].VCC		= SPV_APPLIC_DEVICE_EVENT_VALIDITY_CHECK_CODE;
					event_list[e].ID		= e;
					event_list[e].Category	= SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_PZi;

					memcpy( &datetime_str[0], (void*)&buffer->Ptr[(e*28)+0], 8 ); // DataOra
					datetime_str[8] = '\0';
					// Conversione in BigEndian
					big_endian_buffer = cnv_SwapBytes(&datetime_str[0],4,1,true); // -MALLOC
					if (big_endian_buffer != NULL) {
						memcpy( &datetime_str[0], (void*)big_endian_buffer, 8 );
						free(big_endian_buffer); // -FREE
					}
					datetime_int = cnv_CharPToUHex32( datetime_str );
					datetime_int = _AdjustEventDateTime(datetime_int);
					event_list[e].DateTime = datetime_int;

					memcpy((void*)&(event_list[e].Data[0]), (void*)&buffer->Ptr[(e*28)+8], 20);
					event_list[e].DataLen	= 20;
				}
			break;
			case SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_EVE: // 5
				// eventt_len e' usato per determinare la lunghezza totale dell'evento (compresi i 2*4=8 byte per la data e ora)
				event_count = buffer->Len / (2*event_len);
				event_list = (SPV_DEVICE_EVENT*)malloc(sizeof(SPV_DEVICE_EVENT)* event_count);

				for (e = 0; e < event_count; e++)
				{
					memset( (void*)&(event_list[e].Data[0]), 0, 32 );
					event_list[e].VCC		= SPV_APPLIC_DEVICE_EVENT_VALIDITY_CHECK_CODE;
					event_list[e].ID		= e;
					event_list[e].Category	= SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_SYS;

					memcpy( &datetime_str[0], (void*)&buffer->Ptr[(e*(2*event_len))+0], 8 ); // DataOra
					datetime_str[8] = '\0';
					datetime_int = cnv_CharPToUHex32( datetime_str );
					datetime_int = _AdjustEventDateTime(datetime_int);
					event_list[e].DateTime = datetime_int;

					memcpy((void*)&(event_list[e].Data[0]), (void*)&buffer->Ptr[(e*(2*event_len))+8], ((2*event_len)-8));
					event_list[e].DataLen	= ((2*event_len)-8);
				}
			break;
		}
	}

	*count = event_count;
	return event_list;
}

//==============================================================================
/// Funzione generica per salvare stream eventi ricavati da un payload
///
/// In caso di errore restituisce un valore non nullo.
///
/// NOTA: 	Dato1: Procedura
///         Dato2: Payload
///
/// \date [01.09.2011]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int _EventStreamSave( void * input, void * output, int payload_type  )
{
	int						ret_code  		= SPV_APPLICLIB_STATE_NO_ERROR;
	int						fun_code  		= SPV_APPLIC_FUNCTION_SUCCESS;
	int						payload_id		= 0		; // ID del payload
	int						event_category  = 0   ; // Categoria eventi
	int						event_len		= 0		; // Lunghezza evento
	SPV_PROCEDURE *			procedure		= NULL; // Ptr a struttura procedura
	SPV_FSM_DATA *			data_in			= NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA *			data_out		= NULL; // Ptr ai dati in uscita
	SPV_COMPROT_PAYLOAD *	payload			= NULL; // Ptr a payload
	SPV_DEV_STREAM *		stream			= NULL; // Ptr a stream di periferica
	SPV_DEV_STREAM_BUFFER 	stream_buffer	= { 0, NULL }; // Buffer dello stream periferica
	SPV_DEVICE *			device			= NULL; // Ptr a struttura periferica
	int						event_count 	= 0;
	SPV_DEVICE_EVENT *		event_list		= NULL;

	// --- Recupero i puntatori delle strutture dati di IO ---
    data_in   = (SPV_FSM_DATA*) input;
    data_out  = (SPV_FSM_DATA*) output;
    // --- Recupero dato #1 ---
    if ( data_in != NULL ) // Controllo il ptr dell'input
    {
    	procedure = (SPV_PROCEDURE*) data_in->Data;
    	if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
    	{
			device = procedure->Device;
			// --- Se la periferica prima era offline ---
			if ( device->SupStatus.Offline == true )
			{
				fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
			}
			device->SupStatus.Offline = false;
			device->SupStatus.Unknown = false;
            device->SupStatus.CommErrors = 0;
			// --- Recupero ID payload e categoria dell'evento ---
			if ( procedure->State != NULL )
			{
				payload_id = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
				event_category = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "category" );
				event_len = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "event_len" );
			}
			// --- Recupero dato #2 - Payload ---
			if ( data_in->Next != NULL )
			{
				payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
				if ( payload != NULL )
				{
					// --- Estrazione dello stream dai payload ---
					stream_buffer = SPVExtractStream( device, payload, payload_id, payload_type, NULL );

					try
					{
						if ( stream_buffer.Len > 0 )
						{
							// --- Decodifico lo stream che contiene il blocco degli eventi ---
							event_list = _EventStreamDecode( &stream_buffer, event_category, event_len, &event_count );
							if ( event_list != NULL )
							{
								// --- Salvo nel DB gli eventi ---
								fun_code = SPVDBMSaveEvents( (void*)device, (void*)event_list, event_count );
								free( event_list );
							}
							else
							{
								ret_code = SPV_APPLICLIB_STATE_ERROR;
							}
							if ( stream_buffer.Ptr != NULL ) {
								free( stream_buffer.Ptr );
							}
						}
						else
						{
							ret_code = SPV_APPLICLIB_STATE_ERROR;
						}
					}
					catch(...)
					{
						ret_code = SPV_APPLICLIB_STATE_ERROR;
					}

					if ( fun_code != SPV_DBM_NO_ERROR )
					{
						ret_code = SPV_APPLICLIB_STATE_ERROR;
					}

					// --- Cancellazione payload ---
					SPVDeletePayloadList( payload );
					// --- Tolgo la lista payload dai dati della procedura ---
					data_in->Next->Data = NULL;
					// --- Tolgo la lista payload dai dati della procedura ---
					data_in->Next->Next->Next->Next->Data = NULL;
				}
			}
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione di esecuzione comando di protocollo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [03.08.2007]
/// \author Enrico Alborali
/// \version 0.08
//------------------------------------------------------------------------------
int SPVApplicLib_100_ExecComProtCommand( void * input, void * output )
{
  int                   ret_code    = 0   ; // Codice di ritono
  int                   commandID   = 0   ; // Codice del comando
  SPV_DEVICE          * device      = NULL; // Ptr a struttura periferica
  SPV_COMPROT_PAYLOAD * payload     = NULL; // Ptr ad eventuale payload
  SPV_COMPROT_PAYLOAD * payload_out = NULL; // Ptr ad eventuale payload in uscita
  SPV_PROCEDURE       * procedure   = NULL; // Ptr a struttura procedura
  SPV_FSM_DATA        * data_in     = NULL; // Ptr ai dati in ingresso
  SPV_FSM_DATA        * data_out    = NULL; // Ptr ai dati in uscita
  SPV_FSM_DATA        * new_data    = NULL; // Ptr a nuovi dati
  char                * device_str  = NULL;
  char                * command_str = NULL;

  /* Recupero i puntatori delle strutture dati di IO */
  data_in   = (SPV_FSM_DATA*) input;
  data_out  = (SPV_FSM_DATA*) output;

  // --- Input #1: Procedura ---
  if ( data_in != NULL ) // Controllo il ptr dell'input
  {
    procedure = (SPV_PROCEDURE*) data_in->Data;
    if ( procedure != NULL )
    {
      // --- Input #2: Payload Out ---
      if ( data_in->Next != NULL )// Se ho un secondo dato di input
      {
        payload_out = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data; // Recupero il payload in uscita
      }
      if ( procedure->Device != NULL )
      {
        device    = procedure->Device;
        if ( device != NULL )
        {
          if ( procedure->State->Def != NULL )
          {
            // --- Recupero l'ID del comando ---
            commandID = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
			// --- Esecuzione del comando di protocollo di comunicazione ---
			payload   = SPVExecCommand( device, commandID, &ret_code, payload_out );
			// --- Eventuale secondo tentativo ---
			if ( ret_code != SPV_COMPROT_THREAD_OK && device->SupStatus.Level != 255 )
			{
				payload   = SPVExecCommand( device, commandID, &ret_code, payload_out );
			}
			ret_code  = (ret_code == SPV_COMPROT_THREAD_OK)? SPV_APPLICLIB_STATE_NO_ERROR:SPV_APPLICLIB_STATE_ERROR;
            // --- Output #1: Procedura ---
            data_out->Data = data_in->Data;
            data_out->Next = data_in->Next;
            // --- Se c'� un payload ---
            if ( payload != NULL )
            {
              // --- Output #2: Payload ---
              new_data = SPVNewFSMData( data_out, (void*)payload );
              // --- Output #3: Stringa N.T. Device ID ---
              device_str = XMLType2ASCII( &device->ID, t_u_int_8 ); // -MALLOC
              new_data = SPVNewFSMData( new_data, (void*)device_str );
              // --- Output #4: Stringa N.T. Command ID ---
              command_str = XMLType2ASCII( &commandID, t_u_int_8 ); // -MALLOC
              new_data = SPVNewFSMData( new_data, (void*)command_str );
              // --- Output #5: Struttura Buffer del payload ---
              new_data = SPVNewFSMData( new_data, (void*)(payload->Buffer) );
            }
          }
          else
          {
            ret_code = SPV_APPLICLIB_STATE_ERROR;
          }
        }
        else
        {
          ret_code = SPV_APPLICLIB_STATE_ERROR;
        }
      }
      else
      {
        ret_code = SPV_APPLICLIB_STATE_ERROR;
      }
    }
    else
    {
      ret_code = SPV_APPLICLIB_STATE_ERROR;
    }
  }
  else
  {
    ret_code = SPV_APPLICLIB_STATE_ERROR;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di gestione dell'errore
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.10.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVApplicLib_999_Error( void * input, void * output )
{
  int                   ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;

  SPV_FSM_DATA        * data_in     = NULL; // Ptr ai dati in ingresso
  SPV_FSM_DATA        * data_out    = NULL; // Ptr ai dati in uscita
  SPV_COMPROT_PAYLOAD * payload     = NULL; // Ptr a payload

  /* Recupero i puntatori delle strutture dati di IO */
  data_in   = (SPV_FSM_DATA*) input;
  data_out  = (SPV_FSM_DATA*) output;

  if ( data_out != NULL )
  {
    data_in         = (SPV_FSM_DATA*) input;
    data_out->Data  = data_in->Data;
    data_out->Next  = data_in->Next;
  }
  else
  {
    ret_code  = SPV_APPLICLIB_STATE_ERROR;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream con i suoi valori di default
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.08.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_2533_StreamDefault( void * input, void * output )
{
	int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

	ret_code = SPVAppLibStreamDefault( input, output, 0 );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream ricavato dal payload generico
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.07.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_2541_GenericStreamSave( void * input, void * output )
{
	int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

	ret_code = SPVAppLibStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_GENERIC );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream ricavato dal payload multy array field
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [16.02.2010]
/// \author Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SPVAPPLIB_7145_MultiArrayFieldBlockStreamSave( void * input, void * output )
{
	int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

	ret_code = SPVAppLibMultiArrayFieldStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_GENERIC );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream ricavato dal payload generico
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.07.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_5325_Test( void * input, void * output )
{
	int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

	ret_code = SPVAppLibMultiArrayFieldStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_GENERIC );

	return ret_code;
}



//==============================================================================
/// Funzione per salvare uno stream ricavato dal payload di periferiche Telefin
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [09.07.2008]
/// \author Enrico Alborali
/// \version 0.13
//------------------------------------------------------------------------------
int SPVAPPLIB_2542_TLFBlockStreamSave( void * input, void * output )
{
	int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

	ret_code = SPVAppLibStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_BLOCK );

	return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream ricavato da un GetDeviceObjectResponse SSP
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [22.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVAPPLIB_2543_SSP133StreamSave( void * input, void * output )
{
  int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

  ret_code = SPVAppLibStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_SSP_DATABLOCK );

  return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream intero ricavato da un GetDeviceObjectResponse SSP
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_2544_SSP133IntegerStreamSave( void * input, void * output )
{
  int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

  ret_code = SPVAppLibStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_SSP_INTEGER_DATABLOCK );

  return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream ntstring ricvato da un GetDeviceObjectResponse SSP
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_2545_SSP133NTStringStreamSave( void * input, void * output )
{
  int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

  ret_code = SPVAppLibStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_SSP_NTSTRING_DATABLOCK );

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere uno stream da utilizzare come payload
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [23.06.2005]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SPVAPPLIB_2553_StreamGet( void * input, void * output )
{
	int                       ret_code      = SPV_APPLICLIB_STATE_NO_ERROR;
	int                       fun_code      = SPV_APPLIC_FUNCTION_SUCCESS;
	int                       streamID      = 0   ; // Codice del comando
	SPV_PROCEDURE           * procedure     = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA            * data_in       = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA            * data_out      = NULL; // Ptr ai dati in uscita
	SPV_DEV_STREAM_BUFFER     stream_buffer = { 0, NULL }; // Buffer dello stream periferica
	SPV_COMPROT_PAYLOAD     * payload       = NULL; // Ptr a payload
	SPV_COMPROT_BIN_BUFFER  * bin_buffer    = NULL;
	SPV_DEVICE              * device        = NULL; // Ptr a struttura periferica

	// --- Recupero i puntatori delle strutture dati di IO ---
	data_in   = (SPV_FSM_DATA*) input;
	data_out  = (SPV_FSM_DATA*) output;
	// --- Input #1: Procedure ---
	if ( data_in != NULL ) // Controllo il ptr dell'input
	{
		procedure = (SPV_PROCEDURE*) data_in->Data;
		if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
		{
			device = procedure->Device;
			if ( procedure->State != NULL )
			{
				streamID = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
				// --- Tentativo #1: Estrazione dello stream dalla RAM ---
				stream_buffer = SPVRAMGetStream( device, streamID );
				if ( stream_buffer.Len > 0 && stream_buffer.Ptr != NULL )
				{
					// --- Preparo lo stream (recuparato dalla RAM) da mettere nel payload ---
					bin_buffer = SPVNewBinBuffer( ); // -MALLOC
					bin_buffer->len = stream_buffer.Len;
					memcpy( bin_buffer->buffer, stream_buffer.Ptr, sizeof( unsigned char ) * stream_buffer.Len );
				}
				else
				{
					// --- Tentativo #2: Estrazione dello stream dal DB ---
					stream_buffer = SPVDBGetStream( device, streamID );
					if ( stream_buffer.Len > 0 && stream_buffer.Ptr != NULL )
					{
						// --- Preparo lo stream (recuperato dal DB) da mettere nel payload ---
						bin_buffer = SPVNewBinBuffer( ); // -MALLOC
						bin_buffer->len = stream_buffer.Len;
						memcpy( bin_buffer->buffer, stream_buffer.Ptr, sizeof( unsigned char ) * stream_buffer.Len );
					}
					else
					{
						// --- Tentativo #3: Estrazione dello stream dalla definizione di default ---
						stream_buffer = SPVDefaultDevFrameBuffer( device, streamID );
						if ( stream_buffer.Len > 0 && stream_buffer.Ptr != NULL )
						{
							// --- Preparo lo stream (recuperato dalla definizione di default) da mettere nel payload ---
							bin_buffer = SPVNewBinBuffer( ); // -MALLOC
							bin_buffer->len = stream_buffer.Len;
							memcpy( bin_buffer->buffer, stream_buffer.Ptr, sizeof( unsigned char ) * stream_buffer.Len );
						}
						else
						{
							/* TODO 1 -oEnrico -cError : Gestione dell'errore */
						}
					}
				}
				// --- Verifico lo stream ---
				if ( bin_buffer != NULL )
				{
					// --- Preparo il payload (out) ---
					payload = SPVNewPayload( streamID, bin_buffer );
				}
				else
				{
					payload = NULL;
				}
			}
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		// --- Output #1: Procedura ---
		data_out->Data  = data_in->Data;
		data_out->Next  = NULL;
		if ( payload != NULL )
		{
			// --- Output #2: Payload (out) ---
			data_out      = SPVNewFSMData( data_out, (void*)payload );
		}
		else
		{
			ret_code = SPV_APPLICLIB_STATE_ERROR;
		}
	}
    else
    {
    	ret_code  = SPV_APPLICLIB_STATE_ERROR;
    }

    return ret_code;
}

//==============================================================================
/// Funzione per salvare uno stream di eventi ricavato dal payload di periferiche Telefin
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.08.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_2562_TLFBlockEventStreamSave( void * input, void * output )
{
	int ret_code = SPV_APPLICLIB_STATE_NO_ERROR;

	ret_code = _EventStreamSave( input, output, SPV_APP_PAYLOAD_TYPE_BLOCK );

	return ret_code;
}

//==============================================================================
/// Funzione per impostare in modo condizionale il flag di offline di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2012]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVAPPLIB_2397_SetCondDeviceOffline( void * input, void * output )
{
	int                       ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;
	SPV_PROCEDURE           * procedure   = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA            * data_in     = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA            * data_out    = NULL; // Ptr ai dati in uscita
	SPV_DEVICE              * device    = NULL; // Ptr a struttura periferica
	SPV_COMPROT_PAYLOAD 	* payload	= NULL; // Ptr a payload
	int                       fun_code    = 0   ;
	SPV_DEV_ACK			  ack_list[20000]	; // Lista ACK
	int					  ack_num	= 0		; // Numero di ACK

	// --- Recupero i puntatori delle strutture dati di IO ---
	data_in   = (SPV_FSM_DATA*) input;
	data_out  = (SPV_FSM_DATA*) output;
	// --- Input #1: Procedure ---
	if ( data_in != NULL ) // Controllo il ptr dell'input
	{
		procedure = (SPV_PROCEDURE*) data_in->Data;
		if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
		{
			device = procedure->Device;
			// --- Recupero la lista degli ACK ---
			fun_code = SPVDBMLoadDeviceAcks( &ack_list[0], &ack_num );
			// Se esiste un ACK su questa device
			if (SPVIsDevAck( ack_list, ack_num, device->DevID, -1, -1 ))
			{
				device->SupStatus.Offline = false;
				// --- Aggiorno severita ---
				device->SupStatus.Level = 9; // Diagnostica non disponibile
				// --- Assegno il nuovo stato alla periferica ---
				SPVSetDeviceStatus( device, device->SupStatus.Level, NULL );
			}
			else
			{
				if ( device->SupStatus.CommErrors >= 2 )
    			{
    				// --- Se la periferica prima non era offline ---
    				if ( device->SupStatus.Offline == false )
    				{
    					fun_code = SPVReportDevEvent( device, 0, -1, -1, 2, SPV_APPLIC_EVENT_DEV_OFFLINE, "Device offline", "Check port or link status." );
    				}
    				device->SupStatus.Offline = true;
    			}
    			else
    			{
    				device->SupStatus.CommErrors++;
				}
			}
			// --- Salvataggio stato periferica nel database ---
			fun_code = SPVDBSaveDeviceStatus( device );
			// --- Recupero dato #2 - Payload ---
			if ( data_in->Next != NULL )
			{
				payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
				if ( payload != NULL )
				{
					// --- Cancellazione payload ---
                    SPVDeletePayloadList( payload );
                    // --- Tolgo la lista payload dai dati della procedura ---
                    data_in->Next->Data = NULL;
                    // --- Tolgo la lista payload dai dati della procedura ---
                    data_in->Next->Next->Next->Next->Data = NULL;
				}
			}
			// --- Imposto lo stato della procedura come fallita ---
			procedure->LastResult = 0;
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		// --- Output #1: Procedura ---
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per impostare il flag di online di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2012]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVAPPLIB_2398_SetDeviceOnline( void * input, void * output )
{
	int                       ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;
	SPV_PROCEDURE           * procedure   = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA            * data_in     = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA            * data_out    = NULL; // Ptr ai dati in uscita
	SPV_DEVICE              * device      = NULL; // Ptr a struttura periferica
	int                       fun_code    = 0   ;
	SPV_DEV_ACK			  ack_list[20000]	; // Lista ACK
	int					  ack_num	= 0		; // Numero di ACK

	// --- Recupero i puntatori delle strutture dati di IO ---
	data_in   = (SPV_FSM_DATA*) input;
	data_out  = (SPV_FSM_DATA*) output;
	// --- Input #1: Procedure ---
	if ( data_in != NULL ) // Controllo il ptr dell'input
	{
		procedure = (SPV_PROCEDURE*) data_in->Data;
		if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
		{
			device = procedure->Device;
			// --- Se la periferica prima era offline ---
			if ( device->SupStatus.Offline == true )
			{
				fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
			}
			device->SupStatus.Offline = false;
			device->SupStatus.Unknown = false;
			device->SupStatus.CommErrors = 0;
			// --- Recupero la lista degli ACK ---
			fun_code = SPVDBMLoadDeviceAcks( &ack_list[0], &ack_num );
			// Se esiste un ACK su questa device
			if (SPVIsDevAck( ack_list, ack_num, device->DevID, -1, -1 ))
			{
				// --- Aggiorno severita ---
				device->SupStatus.Level = 9; // Diagnostica non disponibile
			}
			else
			{
				// --- Aggiorno severita ---
				device->SupStatus.Level = (device->SupStatus.Level==255)?0:device->SupStatus.Level;
			}
			// --- Assegno il nuovo stato alla periferica ---
			SPVSetDeviceStatus( device, device->SupStatus.Level, NULL );
			// --- Salvataggio stato periferica nel database ---
			fun_code = SPVDBSaveDeviceStatus( device );
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		// --- Output #1: Procedura ---
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per impostare il flag di offline di una periferica
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2012]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int SPVAPPLIB_2399_SetDeviceOffline( void * input, void * output )
{
	int                       ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;
	SPV_PROCEDURE           * procedure   = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA            * data_in     = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA            * data_out    = NULL; // Ptr ai dati in uscita
	SPV_DEVICE              * device    = NULL; // Ptr a struttura periferica
	SPV_COMPROT_PAYLOAD 	* payload	= NULL; // Ptr a payload
	int                       fun_code    = 0   ;
	SPV_DEV_ACK			  ack_list[20000]	; // Lista ACK
	int					  ack_num	= 0		; // Numero di ACK

	// --- Recupero i puntatori delle strutture dati di IO ---
	data_in   = (SPV_FSM_DATA*) input;
	data_out  = (SPV_FSM_DATA*) output;
	// --- Input #1: Procedure ---
	if ( data_in != NULL ) // Controllo il ptr dell'input
	{
		procedure = (SPV_PROCEDURE*) data_in->Data;
		if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
		{
			device = procedure->Device;
			// --- Recupero la lista degli ACK ---
			fun_code = SPVDBMLoadDeviceAcks( &ack_list[0], &ack_num );
			// Se esiste un ACK su questa device
			if (SPVIsDevAck( ack_list, ack_num, device->DevID, -1, -1 ))
			{
				device->SupStatus.Offline = false;
				// --- Aggiorno severita ---
				device->SupStatus.Level = 9; // Diagnostica non disponibile
				// --- Assegno il nuovo stato alla periferica ---
				SPVSetDeviceStatus( device, device->SupStatus.Level, NULL );
			}
			else
			{
    			// --- Se la periferica prima non era offline ---
    			if ( device->SupStatus.Offline == false )
    			{
    				fun_code = SPVReportDevEvent( device, 0, -1, -1, 2, SPV_APPLIC_EVENT_DEV_OFFLINE, "Device offline", "Check port or link status." );
    			}
    			device->SupStatus.Offline = true;
			}
			// --- Salvataggio stato periferica nel database ---
			fun_code = SPVDBSaveDeviceStatus( device );
			device->SupStatus.CommErrors++;
			// --- Recupero dato #2 - Payload ---
			if ( data_in->Next != NULL )
			{
				payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
				if ( payload != NULL )
				{
					// --- Cancellazione payload ---
                    SPVDeletePayloadList( payload );
                    // --- Tolgo la lista payload dai dati della procedura ---
                    data_in->Next->Data = NULL;
                    // --- Tolgo la lista payload dai dati della procedura ---
                    data_in->Next->Next->Next->Next->Data = NULL;
				}
			}
			// --- Imposto lo stato della procedura come fallita ---
			procedure->LastResult = 0;
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		// --- Output #1: Procedura ---
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare eventuali payload scaricati
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.08.2008]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVAPPLIB_2400_DeletePayload( void * input, void * output )
{
	int                       ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;
	SPV_PROCEDURE           * procedure   = NULL; // Ptr a struttura procedura
	SPV_FSM_DATA            * data_in     = NULL; // Ptr ai dati in ingresso
	SPV_FSM_DATA            * data_out    = NULL; // Ptr ai dati in uscita
	SPV_COMPROT_PAYLOAD 	* payload	= NULL; // Ptr a payload
	int                       fun_code    = 0   ;

	// --- Recupero i puntatori delle strutture dati di IO ---
	data_in   = (SPV_FSM_DATA*) input;
	data_out  = (SPV_FSM_DATA*) output;
	// --- Input #1: Procedure ---
	if ( data_in != NULL ) // Controllo il ptr dell'input
	{
		procedure = (SPV_PROCEDURE*) data_in->Data;
		if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
		{
			// --- Recupero dato #2 - Payload ---
			if ( data_in->Next != NULL )
			{
				payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
				if ( payload != NULL )
				{
					// --- Cancellazione payload ---
					SPVDeletePayloadList( payload );
					// --- Tolgo la lista payload dai dati della procedura ---
					data_in->Next->Data = NULL;
					// --- Tolgo la lista payload dai dati della procedura ---
					data_in->Next->Next->Next->Next->Data = NULL;
				}
			}
			// --- Imposto lo stato della procedura come fallita ---
			procedure->LastResult = 0;
		}
	}
	// --- Preparo il dato in uscita ---
	if ( data_out != NULL )
	{
		data_in         = (SPV_FSM_DATA*) input;
		// --- Output #1: Procedura ---
		data_out->Data  = data_in->Data;
		data_out->Next  = data_in->Next;
	}
	else
	{
		ret_code  = SPV_APPLICLIB_STATE_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per decodificare e salvare nel DB il file eventi per. standard
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [29.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SPVAPPLIB_3334_DecodeDevEventFile( void * input, void * output )
{
  #define LOCAL_EVENT_BUFFER_LEN 32
  int                     ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;
  int                     fun_code    = SPV_APPLIC_FUNCTION_SUCCESS;
  int                     streamID    = 0             ; // Codice del comando
  SPV_PROCEDURE         * procedure   = NULL          ; // Ptr a struttura procedura
  SPV_FSM_DATA          * data_in     = NULL          ; // Ptr ai dati in ingresso
  SPV_FSM_DATA          * data_out    = NULL          ; // Ptr ai dati in uscita
  SPV_COMPROT_PAYLOAD   * payload     = NULL          ; // Ptr a payload
  SPV_DEV_STREAM        * stream      = NULL          ; // Ptr a stream di periferica
  SPV_DEVICE            * device        = NULL        ; // Ptr a struttura periferica
  SPV_DEV_STREAM_BUFFER   stream_buffer = { 0, NULL } ; // Buffer dello stream periferica
  SPV_DEV_EVENT           event                       ; // Struttura per un evento periferica
  char                    event_buffer[LOCAL_EVENT_BUFFER_LEN]; // Buffer dell'evento di sistema periferica
  int                     event_count   = 0           ; // Numero di eventi nel buffer

  // --- Recupero i puntatori delle strutture dati di IO ---
  data_in   = (SPV_FSM_DATA*) input;
  data_out  = (SPV_FSM_DATA*) output;

  // --- Recupero Dato #1 (Procedura) ---
  if ( data_in != NULL ) // Controllo il ptr dell'input
  {
    procedure = (SPV_PROCEDURE*) data_in->Data;
    if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
    {
      // --- Recupero il ptr alla struttura periferica ---
      device = procedure->Device;
      if ( device != NULL )
      {
        // --- Se la periferica prima era offline ---
        if ( device->SupStatus.Offline == true )
        {
		  fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
        }
		device->SupStatus.Offline = false;
		device->SupStatus.CommErrors = 0;
        if ( procedure->State != NULL )            
        {
          streamID = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
        }
        // --- Recupero dato #2 (Payload) ---
        if ( data_in->Next != NULL )
        {
          payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
          if ( payload != NULL )
          {
            // --- Estrazione dello stream dai payload ---
			stream_buffer = SPVExtractStream( device, payload, streamID, SPV_APP_PAYLOAD_TYPE_BLOCK, NULL );
            if ( stream_buffer.Ptr != NULL && stream_buffer.Len != 0 ) // Controllo il buffer ricevuto
            {
              event_count = stream_buffer.Len / LOCAL_EVENT_BUFFER_LEN ;
              for ( int e = 0; e < event_count; e++ )
              {
                // --- Copio il buffer del singolo evento e-esimo ---
                memcpy( &event_buffer[0], &stream_buffer.Ptr[ e * LOCAL_EVENT_BUFFER_LEN ], LOCAL_EVENT_BUFFER_LEN );
                // --- Decodifico l'evento e-esimo ---
				event = SPVDecodeDevEvent( device, event_buffer );
				/*
				La funzione SPVDBSaveEvent � old style e attualemnte non serve per cui la commento
				Se sar� necessario reintrodurla bisogner� prima migrarla al new style
				// --- Salvo l'evento e-esimo nel DB ---
				fun_code = SPVDBSaveEvent( device, &event );
				*/
              }
              // --- Cancello dalla memoria il buffer dello stream ---
              free( stream_buffer.Ptr );
            }
            else
            {
              ret_code = SPV_APPLICLIB_STATE_ERROR;
            }
          }
        }
        else
        {
          ret_code  = SPV_APPLICLIB_STATE_ERROR;
        }
      }
      else
      {
        ret_code  = SPV_APPLICLIB_STATE_ERROR;
      }
    }
  }

  if ( data_out != NULL )
  {
    data_in         = (SPV_FSM_DATA*) input;
    data_out->Data  = data_in->Data;
    data_out->Next  = data_in->Next;
  }
  else
  {
	ret_code  = SPV_APPLICLIB_STATE_ERROR;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per decodificare e salvare nel DB il file eventi sistema standard
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [29.01.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVAPPLIB_3234_DecodeSysEventFile( void * input, void * output )
{
  #define LOCAL_EVENT_BUFFER_LEN 16
  int                     ret_code    = SPV_APPLICLIB_STATE_NO_ERROR;
  int                     fun_code    = SPV_APPLIC_FUNCTION_SUCCESS;
  int                     streamID    = 0             ; // Codice del comando
  SPV_PROCEDURE         * procedure   = NULL          ; // Ptr a struttura procedura
  SPV_FSM_DATA          * data_in     = NULL          ; // Ptr ai dati in ingresso
  SPV_FSM_DATA          * data_out    = NULL          ; // Ptr ai dati in uscita
  SPV_COMPROT_PAYLOAD   * payload     = NULL          ; // Ptr a payload
  SPV_DEV_STREAM        * stream      = NULL          ; // Ptr a stream di periferica
  SPV_DEVICE            * device        = NULL        ; // Ptr a struttura periferica
  SPV_DEV_STREAM_BUFFER   stream_buffer = { 0, NULL } ; // Buffer dello stream periferica
  SPV_DEV_EVENT           event                       ; // Struttura per un evento periferica
  char                    event_buffer[LOCAL_EVENT_BUFFER_LEN]; // Buffer dell'evento di sistema periferica
  int                     event_count   = 0           ; // Numero di eventi nel buffer

  // --- Recupero i puntatori delle strutture dati di IO ---
  data_in   = (SPV_FSM_DATA*) input;
  data_out  = (SPV_FSM_DATA*) output;

  // --- Recupero Dato #1 (Procedura) ---
  if ( data_in != NULL ) // Controllo il ptr dell'input
  {
	procedure = (SPV_PROCEDURE*) data_in->Data;
	if ( procedure != NULL ) // Controllo il ptr alla struttura procedura
	{
	  // --- Recupero il ptr alla struttura periferica ---
	  device = procedure->Device;
	  if ( device != NULL )
	  {
		// --- Se la periferica prima era offline ---
		if ( device->SupStatus.Offline == true )
		{
		  fun_code = SPVReportDevEvent( device, 0, -1, -1, 0, SPV_APPLIC_EVENT_DEV_ONLINE, "Device online", "Link is up." );
		}
		device->SupStatus.Offline = false;
		device->SupStatus.CommErrors = 0;
		if ( procedure->State != NULL )
		{
		  streamID = XMLGetValueInt( (XML_ELEMENT*)procedure->State->Def, "params" );
		}
		// --- Recupero dato #2 (Payload) ---
		if ( data_in->Next != NULL )
		{
		  payload     = (SPV_COMPROT_PAYLOAD*) data_in->Next->Data;
		  if ( payload != NULL )
		  {
			// --- Estrazione dello stream dai payload ---
			stream_buffer = SPVExtractStream( device, payload, streamID, SPV_APP_PAYLOAD_TYPE_BLOCK, NULL );
			if ( stream_buffer.Ptr != NULL && stream_buffer.Len != 0 ) // Controllo il buffer ricevuto
			{
			  event_count = stream_buffer.Len / LOCAL_EVENT_BUFFER_LEN ;
			  for ( int e = 0; e < event_count; e++ )
			  {
				// --- Copio il buffer del singolo evento e-esimo ---
				memcpy( &event_buffer[0], &stream_buffer.Ptr[ e * LOCAL_EVENT_BUFFER_LEN ], LOCAL_EVENT_BUFFER_LEN );
				// --- Decodifico l'evento e-esimo ---
				event = SPVDecodeSysEvent( device, event_buffer );
				/*
				La funzione SPVDBSaveEvent � old style e attualemnte non serve per cui la commento
				Se sar� necessario reintrodurla bisogner� prima migrarla al new style
				// --- Salvo l'evento e-esimo nel DB ---
				fun_code = SPVDBSaveEvent( device, &event );
				*/
			  }
			  // --- Cancello dalla memoria il buffer dello stream ---
			  free( stream_buffer.Ptr );
			}
			else
			{
			  ret_code = SPV_APPLICLIB_STATE_ERROR;
			}
		  }
		}
        else
        {
          ret_code  = SPV_APPLICLIB_STATE_ERROR;
        }
      }
      else
      {
        ret_code  = SPV_APPLICLIB_STATE_ERROR;
      }
    }
  }

  if ( data_out != NULL )
  {
    data_in         = (SPV_FSM_DATA*) input;
    data_out->Data  = data_in->Data;
    data_out->Next  = data_in->Next;
  }
  else
  {
    ret_code  = SPV_APPLICLIB_STATE_ERROR;
  }

  return ret_code;
}


//==============================================================================
// Telefin Supervisor Applicative Library 1.0
//------------------------------------------------------------------------------
// Header Libreria Applicativa (SPVApplicLib.h)
// Progetto:	Telefin Supervisor Server 1.0
//
// Revisione:	0.26 (01.09.2004 -> 02.10.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:		richiede SPVApplicLib.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [01.09.2004]:
// - Prima versione della libreria.
// 0.02 [07.09.2004]:
// - Aggiunta la funzione SPVApplicLib_200_SavePayload.
// - Modificata la funzione SPVApplicLibInit.
// 0.03 [08.09.2004]:
// - Modificata la funzione SPVApplicLib_100_ExecComProtCommand.
// - Modificata la funzione SPVApplicLib_200_SavePayload.
// 0.04 [05.10.2004]:
// - Aggiunta la funzione SPVApplicLib_210_SaveStream.
// - Aggiunta la funzione SPVApplicLib_220_GetStreamSeverity.
// - Aggiunta la funzione SPVApplicLib_999_Error.
// 0.05 [26.05.2005]:
// - Modificata la funzione SPVApplicLibInit.
// - Aggiunta la funzione SPVAppLibReset.
// - Aggiunta la funzione SPVAppLibAddFunction.
// - Aggiunta la funzione SPVAPPLIB_2542_StreamSave.
// 0.06 [31.05.2005]:
// - Modificata la funzione SPVApplicLib_100_ExecComProtCommand.
// - Aggiunta la funzione SPVAPPLIB_2553_StreamGet.
// 0.07 [01.06.2005]:
// - Modificata la funzione SPVApplicLibInit.
// - Modificata la funzione SPVApplicLib_100_ExecComProtCommand.
// - Modificata la funzione SPVAPPLIB_2553_StreamGet.
// 0.08 [08.06.2005]:
// - Modificata la funzione SPVAPPLIB_2553_StreamGet.
// 0.09 [16.06.2005]:
// - Modificata la funzione SPVAPPLIB_2553_StreamGet.
// 0.10 [20.07.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// 0.11 [28.07.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// 0.12 [04.08.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// 0.13 [07.09.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// 0.14 [12.09.2005]:
// - Modificata la funzione SPVAPPLIB_2542_StreamSave.
// - Aggiunta la funzione SPVAPPLIB_2399_SetDeviceOffline.
// 0.15 [21.09.2005]:
// - Modificata la funzione SPVApplicLib_210_SaveStream.
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// - Modificata la funzione SPVAPPLIB_2542_StreamSave.
// - Modificata la funzione SPVAPPLIB_2399_SetDeviceOffline.
// 0.16 [30.09.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// 0.17 [06.10.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// - Modificata la funzione SPVAPPLIB_2542_StreamSave.
// - Modificata la funzione SPVAPPLIB_2399_SetDeviceOffline.
// 0.18 [17.10.2005]:
// - Modificata la funzione SPVApplicLibInit.
// - Aggiunta la funzione SPVAPPLIB_3334_DecodeDevEventFile.
// - Aggiunta la funzione SPVAPPLIB_3234_DecodeSysEventFile.
// 0.19 [18.10.2005]:
// - Modificata la funzione SPVAPPLIB_3234_DecodeSysEventFile.
// 0.20 [19.10.2005]:
// - Modificata la funzione SPVAPPLIB_3234_DecodeSysEventFile.
// 0.21 [20.10.2005]:
// - Modificata la funzione SPVAPPLIB_3334_DecodeDevEventFile.
// - Modificata la funzione SPVAPPLIB_3234_DecodeSysEventFile.
// 0.22 [24.10.2005]:
// - Modificata la funzione SPVAPPLIB_2542_StreamSave.
// 0.23 [08.11.2005]:
// - Modificata la funzione SPVApplicLib_220_GetStreamSeverity.
// - Modificata la funzione SPVAPPLIB_2542_StreamSave.
// - Modificata la funzione SPVAPPLIB_2399_SetDeviceOffline.
// - Modificata la funzione SPVAPPLIB_3334_DecodeDevEventFile.
// - Modificata la funzione SPVAPPLIB_3234_DecodeSysEventFile.
// 0.24 [19.12.2005]:
// - Modificata la funzione SPVApplicLibInit.
// - Aggiunta la funzione SPVAPPLIB_2398_SetDeviceOnline.
// [30.03.2006]:
// - Modififcta la funzione SPVAPPLIB_2542_StreamSave.
// [21.06.2006]:
// - Aggiunta la funzione SPVAPPLIB_2133_SSP113StreamSave.
// - Modificata la funzione SPVApplicLibInit.
// 0.25 [29.01.2008]:
// - Modificata la funzione SPVAPPLIB_3334_DecodeDevEventFile.
// - Modificata la funzione SPVAPPLIB_3234_DecodeSysEventFile.
//==============================================================================

#ifndef SPVApplicLibH
#define SPVApplicLibH

#define SPV_APPLICLIB_FUNCTION_COUNT  50

#define SPV_APPLICLIB_NO_ERROR        0
#define SPV_APPLICLIB_INVALID_CODE    29001
#define SPV_APPLICLIB_INVALID_PTF     29002
#define SPV_APPLICLIB_LIST_FULL       29003

#define SPV_APPLICLIB_STATE_NO_ERROR  0
#define SPV_APPLICLIB_STATE_ERROR     1

#define SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_SYS	1
#define SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_APP 2
#define SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_FDS 3
#define SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_PZi 4
#define SPV_APPLICLIB_DEVICE_EVENT_TYPE_TLF_EVE 5

//==============================================================================
/// Definizione di ptr a funzione di stato FSM.
///
/// \date [01.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef int(* SPV_FSM_FUNCTION_PTR)(void*,void*);

//==============================================================================
/// Definizione di codice di funzione di stato FSM.
///
/// \date [01.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef char SPV_FSM_FUNCTION_CODE[12];

//==============================================================================
/// Variabili globali esterne
//------------------------------------------------------------------------------
extern SPV_FSM_FUNCTION_PTR   SPVApplicLibFunctionList  [SPV_APPLICLIB_FUNCTION_COUNT];
extern SPV_FSM_FUNCTION_CODE  SPVApplicLibFunctionCodes [SPV_APPLICLIB_FUNCTION_COUNT];
extern int                    SPVApplicLibFunctionCount   ;

//==============================================================================
/// Funzioni
//------------------------------------------------------------------------------

/// Funzione di caricamento della libreria
void  SPVApplicLibInit                    ( void );
/// Funzione per azzerare la lista delle funzioni di libreria
void  SPVAppLibReset                      ( void );
/// Funzione per aggiungere una funzione alla lista della libreria
int   SPVAppLibAddFunction                ( char * code_string,  SPV_FSM_FUNCTION_PTR ptf );

//==============================================================================
/// Funzioni Interne
//------------------------------------------------------------------------------
// Funzione generica per salvare stream ricavati da un payload
int   	SPVAppLibStreamSave                 ( void * input, void * output, int payload_type  );
// Funzione generica per salvare stream ricavati dai propri valori di default
int     SPVAppLibStreamDefault				( void * input, void * output, int params );

//==============================================================================
/// Funzioni di libreria
//------------------------------------------------------------------------------
/// Funzione di esecuzione comando di protocollo
int   SPVApplicLib_100_ExecComProtCommand		( void * input, void * output );
/// Funzione di gestione dell'errore
int   SPVApplicLib_999_Error					( void * input, void * output );

/// Funzione per salvare uno stream con i suoi valori di default
int   SPVAPPLIB_2533_StreamDefault				( void * input, void * output );
/// Funzione per settare uno stream in un payload in uscita
int   SPVAPPLIB_2553_StreamGet            		( void * input, void * output );
/// Funzione per salvare uno stream ricavato dal payload
int   SPVAPPLIB_2541_GenericStreamSave     		( void * input, void * output );
/// Funzione per salvare uno stream ricavato dal payload
int   SPVAPPLIB_2542_TLFBlockStreamSave    		( void * input, void * output );
// Funzione per salvare uno stream ricavato dal payload multy array field
int		SPVAPPLIB_7145_MultiArrayFieldBlockStreamSave( void * input, void * output );
// Funzione per salvare uno stream ricavato da un GetDeviceObjectResponse SSP
int   SPVAPPLIB_2543_SSP133StreamSave			( void * input, void * output );
// Funzione per salvare uno stream intero ricavato da un GetDeviceObjectResponse SSP
int   SPVAPPLIB_2544_SSP133IntegerStreamSave	( void * input, void * output );
// Funzione per salvare uno stream ntstring ricvato da un GetDeviceObjectResponse SSP
int   SPVAPPLIB_2545_SSP133NTStringStreamSave	( void * input, void * output );
// Funzione per salvare uno stream eventi ricavato dal payload telefin
int   SPVAPPLIB_2562_TLFBlockEventStreamSave	( void * input, void * output );
// Funzione per impostare in modo condizionale il flag di offline di una periferica
int SPVAPPLIB_2397_SetCondDeviceOffline			( void * input, void * output );
// Funzione per impostare il flag di online di una periferica
int   SPVAPPLIB_2398_SetDeviceOnline			( void * input, void * output );
// Funzione per impostare il flag di offline di una periferica
int   SPVAPPLIB_2399_SetDeviceOffline			( void * input, void * output );
// Funzione per eliminare eventuali payload scaricati
int	SPVAPPLIB_2400_DeletePayload			( void * input, void * output );

/// Funzione per decodificare e salvare nel DB il file eventi per. standard
int   SPVAPPLIB_3334_DecodeDevEventFile			( void * input, void * output );
/// Funzione per decodificare e salvare nel DB il file eventi sistema standard
int   SPVAPPLIB_3234_DecodeSysEventFile			( void * input, void * output );

int   SPVAPPLIB_5325_Test			( void * input, void * output );

//------------------------------------------------------------------------------
#endif

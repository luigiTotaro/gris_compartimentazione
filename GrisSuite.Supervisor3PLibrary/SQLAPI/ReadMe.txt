****************************************************************
               Welcome to SQLAPI++ Library 3.7.22
****************************************************************

1. INTRODUCTION
Thank you for using SQLAPI++ Library.
This file contains important notes about the installation and the
product itself.

2a. INSTALLATION (Win32 version)
Files under 'SQLAPI' folder after unpacking the SQLAPI++ self-extracting archive:

ReadMe.txt 		- this file
license.txt		- SQLAPI++ license agreement and warranty
bin\*.*			- SQLAPI++ DLL and sample binaries (read more at bin\index.txt)
lib\*.*			- dynamic and static librares (read more at lib\index.txt)
include\*.h		- public include files
doc\*.*			- SQLAPI++ documentation in HTML format
examples\*.cpp		- SQLAPI++ steps
amd64\*			- SQLAPI++ precompiled binaries for AMD64 platforms

These files are only for registered SQLAPI++ version:

src\*.cpp		- SQLAPI++ sources
src\*.h			- SQLAPI++ private headers
src\Makefile.mvc	- SQLAPI++ makefile for MSVC++
src\sqlapi_msvc.bat	- batch file to compile SQLAPI++ (release & debug) for MSVC++
src\Makefile.bcc	- SQLAPI++ makefile for Borland C++ compiler
src\sqlapi_bcc.bat	- batch file to compile SQLAPI++ (release & debug) for Borland C++ compiler
src\Makefile.mingw	- SQLAPI++ makefile for MinGW
src\sqlapi_mingw.bat	- batch file to compile SQLAPI++ (release & debug) for MinGW


SQLAPI++ binaries are compiled with Microsoft Visual Studio 6.0 (x86) and
Visual Studio 2005 (x86_64). The multithread- and DLL-specific version of
the run-time libraries are used. You must recompile the binaries for another
run-time library configurations. The same is true for Borland
(Borland C++ Compiler 5.5 is used) and MinGW (MinGW g++ 3.4.2 is used) binaries.


2b. INSTALLATION (Linux/Unix version)
Files after the installation:
ReadMe.txt		- this file
license.txt		- SQLAPI++ license agreement and warranty
bin\*			- simple executable to test database connection
doc\*.*			- SQLAPI++ documentation in HTML format
examples\*.cpp		- SQLAPI++ steps
examples\Makefile 	- makefile for building SQLAPI++ steps
include\*.h		- public include files
lib\*.*			- shared and static SQLAPI++ libraries
g++-*.tar.gz		- archives of SQLAPI++ binaries compiled with different g++ versions

These files are only for registered SQLAPI++ version:
src\*.cpp		- SQLAPI++ sources
src\*.h			- SQLAPI++ private headers
src\Makefile		- SQLAPI++ makefile for GNU make
src\sqlapi_gcc		- batch file to compile SQLAPI++ (release & debug)

We recommend to recompile the SQLAPI++ binaries on platform it should be used.

3. LATEST CHANGES
Version 3.7.22
- PostgreSQL: Fixed string escaping bug (buffer size too small, thanks combit.net).
- SQL Server: Added statement preparation for each executing when option "ICommandPrepare" = "SetParameterInfo" used (thanks to Chris Hecker).
- SQL Server: Fixed string parameters buffer size bug on x86_64/SQLOLEDB.
- MySQL: Fixed access violation bug with fetching empty text field data (thanks Albert Perdon).
- MySQL: Fixed bug with non-initialized statement handle (thanks Mario Lavalliere).
- ODBC: The code modified to work correctly with long/blob fields under Linux and FreeTDS driver.
- ODBC: SAConnection::isAlive() uses SQLGetConnectAttr/SQL_ATTR_CONNECTION_DEAD now.
- SQL Server: Added SAConnection parameter "OLEDBProvider" (SQLOLEDB connection).

Version 3.7.21
- Oracle: "APPNAME" SAConnection parameter added (OCI_ATTR_CLIENT_IDENTIFIER is used, thanks Frank Hilliger).
- MySQL: Fixed bug with SAConnection::isAlive().
- SQL Server: "ICommandPrepare" SACommand parameter value "SetParameterInfo" added (fixes http://support.microsoft.com/kb/235053).
- MySQL: "UseStatement" SACommand parameter added (SQLAPI++ can use MySQL statement API now).
- MySQL: Fixed long character type support with UNICODE.
- Oracle: Fixed CLOB type support with UNICODE.
- ODBC: Fixed access violation bug at IodbcConnection::Check (UNICODE, thanks Jay Sridhar).
- DB2: Fixed possible access violation bug at Idb2Connection::Check (UNICODE).

Version 3.7.20
- Sybase: Version 15.x support for Linux/UNIX (.so names changed - thanks Ronan O'Sullivan).
- Oracle: Crash fixed with the OCIDateTime descriptor freeing.
- DB2: Unicode support improved.
- General: Added common SAConnection option: "APPNAME", "WSID" (they are supported for SQL Server and Sybase at the moment).
- SQL Server (OLDEDB): Added SAConnection options SSPROP_INIT_FILENAME, SSPROP_INIT_ENCRYPT, SSPROP_INIT_WSID.
- PostgreSQL: Added using of the PQescape* functions if they are available.
- DB2: SA_dtString procedure parameter type is used for the LONG VARCHAR database type.
- MySQL: Added support for multiple result set.
- SQL Server (OLDEDB): x86_64 fixes.
- DB2: All diagnostic messages are combined into SAException error text now.
- Sybase: CS_HAFAILOVER connection parameter added.
- General: Fixed a SADateTime::TmFromDate problem with the rounding.
- DB2: XML type support added.
- DB2: Fixed x86_64 support.

Version 3.7.19
- Oracle: Added procedure/function support for Oracle PL/SQL BOOLEAN type.
- Oracle: Support for BINARY_FLOAT and BINARY_DOUBLE types.
- Oracle: Memory leak fixed with SYS_REFCURSOR & TIMESTAMP/BLOB/CLOB fields.
- General: Added bool& SAGlobals::UnloadAPI() - prevent from DBMS API unloading.
- General: SADateTime::SADateTime(double dt) - round the fraction result to microseconds.
- General: Added SAInterval class for time intervals (experimental, only MySQL support for now).
- MySQL: Support for multi-result statements.
- MySQL: Set by default connection flags CLIENT_MULTI_STATEMENTS and CLIENT_MULTI_RESULTS.
- SQLBase: Fixed access violation at sqldes() C API function with SQLBase version 10.
- MySQL: Added support for the 'bit' type.
- PostgreSQL: Fixed problem with bytea/text on AIX 64-bit (seems common 64-bit platform problem).
- Sybase: Fixed MinGW version.
- ODBC:	Windows x64 fixes. Should work now.
- Oracle: Fixed bug with DateTime parameters and an execution of prepared statements.

Version 3.7.18
- General: Added SACommand::operator << (unsigned short Value) and SACommand::operator << (unsigned long Value).
- MySQL: Added "CharacterSet" SAConnection option.
- Oracle: Fixed bug with SADateTime to Oracle OCIDateTime conversion.
- SQLServer: Fixed bug when it was impossible to fetch several result sets with single query.
- MySQL: Added mysql_thread_init() and mysql_thread_end() API functions.
- MySQL: Added mysql_server_end() call before MySQL client library is released.

Version 3.7.17
- General: AIX, HP-UX and MacOS X support improved.
- SQLServer: Added SSPROP_INIT_MARSCONNECTION connection (OLEDB) option.
- PostgreSQL: Server side cursor implemented for 'SELECT ...' statements.
- InterBase: Modified to start transaction only when query executed (thanks Fabrice Aeschbacher).
- General: New SQLAPI++ data types added: SA_dtUShort and SA_dtULong.
- General: Added SAString methods SAString::MakeUpper(), SAString::MakeLower().
- General: Experimental 'scrollable cursor' functionality implemented (PostgreSQL,MSSQL,ODBC thanks combit.net).
- General: Fixed SACommand::ParseInputMarkers for '=' parameter name delimiter.
- Oracle: Fixed memory leaks and troubles with BLOB and TIMESTAMP descriptors.
- Oracle: UNICODE version uses UCS2 client character set.
- Oracle: Fixed bug at IoraConnection::CnvtNumericToInternal with numbers like [-]0.001
- Oracle: Fixed memory leak with temporary BLOBs (thanks Frank Hilliger).
- General: Experimental methods SAConnection::Destroy() added (to destroy broken connection).
- General: Experimental method SAConnection::isAlive() added (to check if a remote database is alive).
- MySQL: mysql_ping was removed from SAConnection::IsConnected().
- ODBC: Fixed column size and precision for SADateTime input parameters.
- Sybase: Fixed "CS_HOSTNAME" connection option (thanks Ulrich Gievers).
- Sybase: Bug fixed with INTEGER-s on x86-64.
- MySQL: Bug fixed long text/binary procedure parameters.
- Oracle: New SAConnection "UseTimeStamp" option (controls if SQLAPI should use Oracle TIMESTAMP functions).
- Oracle: Fixed bug with procedure datetime parameters.
- DB2: Fixed bug with INTEGER-s on x86-64.
- ODBC:	Fixed bug with SAException::ErrMsg() at UNICODE configuration. UNICODE support improved.
- General: Fixed bug at SAString::FormatV(...) on Linux/UNIX (thanks Michael Teske)
- SQLServer: DB-Library client included into MinGW build
- MySQL: Fixed the procedure parameters parsing code

Version 3.7.16
- General: SAString::Replace(...) - serious bug fixed.

Version 3.7.15
- General: Fixed SAString::FormatV(...) (thansk Bernd Holz).
- ODBC: Support for GUID type (thanks Alexander Horak).
- Oracle: SADateTime fraction support.
- General: SAString::Replace(...) was improved (thanks Arthur Finkel).
- MySQL: Fixed the procedure parameters parsing code (thanks Znamenacek Pavel).

Version 3.7.14
- General: Fixed memory leak in SAConnection destructor (thanks Diane Downie).
- MySQL: Support for NEWDECIMAL data type.
- SQLBase: Linux/UNIX support.
- General: Fixed UNIX implementation of the SAString::Format for 64-bit integer (thanks Boris Daniel).
- MySQL: Added support for a procedure result set.
- InterBase: Boolean support (InterBase v7).
- PostgreSQL: New connection option "ClientEncoding".
- Oracle: New connection option "ConnectAs" (SYSDBA or SYSOPER, OCI8 only).
- General: MinGW (GNU C++ for Win32) support.
- Interbase: New global option "ClientLibraryName" to define the API library name.
- General: SAString::FormatV fixed for ISO C99 va_copy (thanks Mathew Kuzhithattil Aniyan).
- General: Microsoft Platform SDK integration. X86, IA64, AMD64 target support.
- PostgreSQL: field size, precision, scale support (thanks Alexander Horak).

Version 3.7.12
- SAConnection::~SAConnection(): possible GPF fixed (thanks Diane Downie).
- SANumeric: 64-bit integer operators and constructor added.
- MySQL: added alpha support for MySQL 5.x procedures.
- SQL Server(OLEDB): MSSQL native error code is returned by SAExeption::ErrNativeCode() now.
- Interbase/Firebird: SADateTime::Fraction() support (thanks Fabrice Aeschbacher).
- DB2: support for SQL_GRAPHIC, SQL_VARGRAPHIC, SQL_LONGVARGRAPHIC  types added.
- General: fixes to compile with VS.NET 2005 under AMD64 (thanks Peter Klotz).
- General: some improvements for threaded applications.
- General: Unusual (not ".so") extensions for shared libraries can be defined at compilation process.
- SACommand::ParseInputMarkers(...): bug fixed with ':' symbol in string constants.

Version 3.7.11
- ODBC: connection-level option added to control ODBC's connection pooling. 
- MySQL: added support for MySQL clients version 4.1 and higher.
- MySQL: connection-level options added to control client_flag parameter when connecting to MySQL.
- SQL Server: command-level option added: DBPROP_REMOVEDELETED.
- Sybase: command-level option added: ct_cursor.
- General: small memory leak fixed.

Version 3.7.10
- ODBC: bulk fetching support improved as a work-around for some non-compliant drivers.
- Oracle: added support for new date/times types in Oracle 9i.
- Oracle: added read support for interval types. They are defaulted to strings.
- SQL Server: added read support for fields of type VARIANT. They are defaulted to strings.
- SAString:Format(): bug fixed with "I64" option implementation on Windows platform.
- SAString::Format(): small change to better support variable argument list on some picky UNIX platforms.

Version 3.7.9
- SQL Server: new command related option ("SSPROP_INIT_APPNAME") added.
- Oracle: small memory leak fixed with Oracle 9i client.

Version 3.7.8
- PostgreSQL: support for TIMESTAMPTZOID type added.
- Oracle: bulk fetch implementation improved for columns of type NUMBER.

Version 3.7.7
- SQL Server (OLE DB): bug fixed when reading large NTEXT columns. 
- SQLBase: bug fixed when using Receive Long String variables in stored procedures. 

Version 3.7.6
- PostgreSQL: int8 datatype support improved. 
- Sybase: money datatype support improved. 
- MySQL: support for MySQL 4.x client improved.

Version 3.7.5
- Borland C++ Builder 6 support improved.
- ODBC: new command related option ("PreFetchRows") added.
- DB2: Bug fixed when fetching with "PreFetchRows" option.
- Oracle: "PreFetchRows" option implementation improved.

Version 3.7.4
- InterBase: new connection related options("TPB_LockResolution", "TPB_AccessMode") added.
- SQL Server (OLE DB): new command related option ("Execute_riid") to set the requested interface for the rowset added.
- Oracle: support for pls_integer and binary_integer types added.

Version 3.7.3
- PostgreSQL, MySQL: support for binding/retrieving double values improved (bug fixed when current locale's decimal point is not '.').

Version 3.7.2
- SQLBase: support added for stored commands.
- Oracle: new parameter and field related options added ("OCI_ATTR_CHARSET_ID", "OCI_ATTR_CHARSET_FORM").
- Linux/Unix: some changes applied to be compatible with Alpha platform.

Version 3.7.1
- SQL Server (OLE DB): memory leaks fixed in SAConnection::ClientVersion(), SAConnection::ServerVersion() and SAConnection::ServerVersionString().
- SQL Server (OLE DB): new connection related option("CoInitializeEx_COINIT") to set the COM library initialization mode added.
- DB2: new command related option added ("ReturnStatus").

Version 3.6.9
- ODBC (iODBC on Linux/Unix): bug fixed when executing DELETEs or UPDATEs that don't affect any rows (function sequence error).
- PostgreSQL: small memory leak fixed.
- Sybase: new connection related option ("CS_VERSION") to set the version of Client-Library behavior added.

Version 3.6.8
- Oracle (Linux/Unix): bug fixed when calling a stored procedure while server is shutting down.
- Multithreading support improved.

Version 3.6.7
- SADateTime::GetCurrentTime() method renamed to SADateTime::currentDateTime().
- SQLBase: bug fixed with in/out parameters of type NUMBER.
- SQLBase: bug (microseconds truncation) fixed when selecting date/time values.
- Oracle: support added for logging in using external authentication.

Version 3.6.6
- SQL Server (OLE DB): New command related option("DBPROP_COMMANDTIMEOUT ") to set command time-out added.
- Oracle: Bug fixed when selecting ROWID.
- DB2: Support added for stored procedure return status.

Version 3.6.5
- SQL Server (OLE DB): New command related option ("ICommandPrepare") to control current command preparation added.
- _SA macros changed to _TSA macros to be compatible with Microsoft Visual C++.NET.

Version 3.6.4
- Oracle: Memory leak bug when calling a stored procedure with REF CURSOR parameter(s) fixed.
- SQL Server: Bug when selecting text and image fields with PreFetchRows option fixed.
- InterBase: Big and high precision numbers support improved.

Version 3.6.3
- Oracle: Bug when binding using setAsNumeric() has been fixed.
- DB2 (Linux/Unix): Bug in NUMERIC and DECIMAL data support has been fixed.

Version 3.6.2
- Sybase: Support has been added for new OpenClient 12.5 features.
- SQL Server (OLE DB): Some new rowset options have been added.

Version 3.6.1
- Default parameters value support has been improved.

Version 3.5.9
- Big and high precision numbers support has been improved (Oracle, InterBase, SQLBase, Informix).

Version 3.5.8
- Oracle: bug when retrieving numeric values fixed.

Version 3.5.7
- DB2: big and high precision numbers support has been improved.

Version 3.5.6
- Oracle: bug fixed (access violation) when selecting numeric fileds.

Version 3.5.5
- Sybase: new command option ("CS_BULK_LOGIN") to describe whether or not a connection can perform bulk copy operations into a database is available.
- Big and high precision numbers support has been improved (Oracle, Sybase, SQL Server, MySQL and PostgreSQL).
- MySQL: reporting precision, scale and nullability has been improved.

Version 3.5.4
- Documentation improved.

Version 3.5.3
- Oracle: new option to specify which overload to call is now available for Oracle command ("Overload").
- Oracle: bug fixed when reading data of RAW type.
- DB2: new option is now available for DB2 command ("PreFetchRow").
- SQL Server (OLE DB): new option ("SSPROP_INIT_AUTOTRANSLATE") for configuring OEM/ANSI character translation is available.

Version 3.5.2
- InterBase: commit management improved.
- Oracle (OCI7): native error number reporting fixed.
- SQL Server (OLE DB): bug fixed when reading data of NTEXT type.

Version 3.5.1
- PostgreSQL support.
- SQL Server (OLE DB): the ability to use DBPROP_SERVERCURSOR rowset property added.
- String and binary data support has been improved.

Version 3.4.7
- InterBase: bug fixed when binding NULL values.
- MySQL: setting transaction isolation level implemented.

Version 3.4.6
- C type 'bool' supporting added.

Version 3.4.4
- SQL Server (OLE DB): COM initialization improved.

Version 3.4.3
- SQL Server (DB-Library): error when returning output parameters from stored procedures is fixed.
- InterBase: FireBird client support added.

Version 3.4.2
- Sybase (Sun Solaris): error when connecting is fixed.

Version 3.4.1
- MySQL suport.
- Sybase: New option is now available for Sybase command (PreFetchNext).
- Documentation improved.

Version 3.3.4
- InterBase: error (stack overflow) when calling stored procedures with no parameters is fixed.
- Oracle, SQLBase: SACommand::isResultSet implementation fixed. Now it returns false after result set is completely fetched.
- SQL Server (OLE DB): bug fixed when reading text or image collumns with SQL Server 7.0 client.

Version 3.3.2
- Documentation improved.

Version 3.3.1
- Support for using SQL Server OLE DB API added. Default API for accessing SQL Server changed from DB-Library to OLE DB.
- DB2: fetching multiple result sets from stored procedures implemented.
- SACommand::FetchParamsNext method deprecated.
- Documentation improved.

Version 3.2.5
- SQL Server: the ability to use trusted connection added.
- Numeric data reading methods now support type conversion from string data.
- Documentation improved.

Version 3.2.4
- Sybase: minor changes made to improve multi-threading support.

Version 3.2.3
- Minor changes made in SQLAPI++ source codes to make it compatible with Sun Workshop Pro compiler.

Version 3.2.2
- InterBase: Truncation of VARCHAR fields. Bug fixed.

Version 3.2.1
- Sun SPARC Solaris: SIGBUS error (non-alignment of numeric data). Bug fixed.
- Oracle 8i: Ability to bind BLob(CLob) parameters in stored procedures is implemented.

Version 3.1.6
- SQL Server: bug with smalldatetime type fixed.

Version 3.1.5
- SQL Server: Ability to connect to named instances of SQL Server 2000 added.

Version 3.1.4
- Sybase: New options are now available for Sybase connection ("CS_PACKETSIZE", "CS_APPNAME", "CS_HOSTNAME").

Version 3.1.3
- Oracle: Executing a function in Oracle 8i resulted in ORA-01426. Bug fixed.
- SQLServer: Bug fixed when binding parameters to a command with "OpenCursor" option set.
- Sybase: Executing a stored procedure through "Exec" statement resulted in access violation. Bug fixed.

Version 3.1.2
- Documentation improved (Server Specific Guide).
- DB2, Informix, ODBC: native error code returned by SAException::NativeError() was invalid. Bug fixed.
- InterBase: Support for database connection cache added.

Version 3.1.1
- Oracle 8: Access violation when updating/inserting CLob fields. Bug fixed.
- Oracle (Linux/Unix): Error "Unimplemented or unreasonable conversion requested" when working with output (return) parameters is fixed.
- Oracle: update/insert SQL statement with two or more BLob (CLob) parameters results in "OCI_INVALID_HANDLE" exception. Bug fixed.
- ODBC: bug with DateTime values fixed.

Version 3.0.1
- Unix version released.
- Oracle: Support for Oracle REF CURSORs and nested cursors added.

Version 2.3.11
- SAString::Right method: bug fixed.
- SQLBase: Automatic detecting of stored procedure parameters was incorrect when parameter's name include numeric digits, underscore (_) or special characters (#, @ or $). Bug fixed.
- SQL Server: "Open Cursor" option improved.
- SQL Server (version 7, version 2000): data type for datetime parameters in stored procedures has been detected as SA_dtUnknown. Bug fixed.

Version 2.3.10
- Sybase: support for returning status result code from stored procedures.

Version 2.3.9
- SQLServer: bug fixed when calling a stored procedure with "owner.proc_name" syntax on SQLServer 2000.
- ODBC (MS Access): bug fixed when binding long data.

Version 2.3.8
- SQLServer: BIT datatype support added.
- SQLServer: bug (assertion) fixed when working with DECIMAL or NUMERIC fields in debug version.

Version 2.3.7
- Cancelling queries support added.

Version 2.3.6
- Informix: error "String data right truncation" when binding string parameters is fixed.

Version 2.3.5
- SQLServer: Support for using DB-Library cursor functions  added.

Version 2.3.4
- InterBase: Support for client character set declaring added.
- InterBase: Support for connection with a role added.

Version 2.3.3
- Date/time support improved. SADateTime::GetCurrentTime method added.

Version 2.3.2
- SQLBase: Support for Cursor-context preservation.
- SAString &SAConnection::Option(const SAString&) is replaced by SAConnection::setOption
- SAString &SACommand::Option(const SAString&) is replaced by SACommand::setOption

Version 2.3.1
- InterBase: Support for SQL Dialect 2 and 3 added. SQL Dialect is now customizable.
- InterBase: Bug fixed when reading DECIMAL and NUMERIC fields from database

Version 2.2.1
- Fraction of a second (milli, micro, nano seconds) support added.
- Date/time support improved.

Version 2.1.1
- Date/time support improved.

Version 2.0.1
- Linux version released. 
- Informix: error when calling stored procedures with no parameters is fixed. 
- Informix: error when describing stored procedure input parameters is fixed. 
- Informix, DB2: GPF when handling errors fixed.
- Oracle OCI7: multi-threaded support added.
- Documentation is improved. A great number of examples added.

Version 1.6.1
- Sybase: returning multiple result sets from stored procedure or batch is implemented. 
- SQL server: returning multiple result sets from stored procedure or batch is implemented. 
- Error when compiling with Borland compilers using data alignment other than 8 bytes fixed.

Version 1.5.1
- Sybase support (both ASE and ASA)
- Oracle8: Error reporting improved when calling non-existent procedure(function) from existent package (f.ex., sys.dbms_output.aaaaaa). 
- SQL server: the abillity to connect to default database is implemented. 
- SQL Server: SAException::ErrNativeCode always returned 0 (not native error code). Fixed. 
- DB2, Informix, ODBC: bug fixed when calling stored procedures with schema (f. ex., tester.TestProc) 
- Microsoft VC++ 5.0 backward compatible. 
- Oracle 7.3.3 client - loading error fixed. 
- DB2, Informix, ODBC: bug fixed when a searched update or delete statement that does not affect any rows is executed. 
- DB2, Informix, ODBC: bug when returning strings from stored procedures fixed. 
- Error when linknig with Borland compilers fixed 

Version 1.4.1
- Informix support 
- Documentation improved

Version 1.3.1
- DB2 support 
- BFILE support in Oracle OCI8 
- Bug in Oracle when binding BLobs fixed 
- Bug in SQLServer when reading timestamp fields fixed

Version 1.2.1
- Bug in ODBC implementation when fetching character data (in some cases data can be truncated) fixed 
- Possibility of getting error offset (SAExecption::ErrPos) 
- Autocommit management (SAConnection::AutoCommit and SAConnection::setAutoCommit) 
- Documentation improved

Version 1.1.1
- Support for managing transaction isolation levels added. 
- Transaction policy on SQLServer changed. Now all commands from one connection are in the same transaction. 
- Reported bugs fixed. 
- Documentation improved.

Version 1.0.1
- the first version released

4. ORDERING (for trial version only)
If you like SQLAPI++ Library and you want to receive the registered version
you should purchase SQLAPI++ Personal license for EVERY developer, 
or one Sile (Site+) license for all developers in your company.
Visit http://www.sqlapi.com/Order for more information.

//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		AutoConfigurator.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		17.05.2007
//

using System;

using GrisSuite.Util.AutoConfigurator.Class;

namespace GrisSuite.Util.AutoConfigurator
{
    class Program
    {
        #region Token

        private const string    TOK_READ        = @"R";
        private const string    TOK_WRITE       = @"W";

        #endregion

        #region Private Members

        private static ManagerBase  _oManager = null;

        #endregion


        // args: null autoconfigurazione      
        //       READ                       READ        Legge configurazione da dispositivo e genera file 
        //                                  WRITE       Scrive configurazione da file e scrive in EEProm dispositivo 
        //       FILE                                   Nome del file contenente la configurazione
        //       Addr                       00          indirizzo periferica
        //       2,9600,N,8,1,T,T           2           porta seriale
        //                                  9600        baud rate
        //                                  N(E)(O)     parit� N=nessuna, E Even, O Odd
        //                                  8           data
        //                                  1           stop bit 0=nessuno, 1 = 1 stop bit, 1.5, 2
        //                                  T           T=232, F=485/422
        //                                  T           T=Echo on, F= Echo Off
        //		gestione dei servizi		Y			Y=yes, N=no
        //      tipo periferica             A           A=Ampli, PZ=Pannello Zone
        //
		[STAThread]
        static void Main(string[] args)
        {
            try
            {
                GrisSuite.AppCfg.AppCfg.Default.LogAllCfg();

                DeviceType deviceType = DeviceType.DEFAULT;

                // Parametri: AUTO
                // Parametri: AMPLI
                // Parametri: PZ
                // Parametri: PZV3
                if (args.Length == 1) 
                {
                    // load config
                    //
                    bool loadingOk = false;
                    if ( args[0] == "AUTO"
                        || args[0] == "RESTORE" )
                    {
                        foreach (string dev in Enum.GetNames(typeof(DeviceType)))
                        {
                            if (dev == "DEFAULT") continue;

                            Common.TOK_PGM_LOG_FILE = dev + "_autoconfig.log";
                            Common.DumpLogNewLine();
                            Common.DumpLogMessage(string.Format("*** AutoConfig Versione: {0}, Data: {1:dd-MM-yyyy HH:mm} ***", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DateTime.Now));
                            
                            deviceType = (DeviceType)Enum.Parse(typeof(DeviceType), dev, true);
                            _oManager = ManagerBase.GetManager(deviceType);

                            loadingOk = _oManager.LoadConfig();

                            if (loadingOk)
                            {
                                if (args[0] == "AUTO")
                                {
                                    _oManager.DoAutoConfig();
                                }
                                else if (args[0] == "RESTORE")
                                {
                                    _oManager.Restore();
                                }
                            }
                        }
                    }
                    else if (Enum.IsDefined(typeof(DeviceType), args[0]))
                    {
                        Common.TOK_PGM_LOG_FILE = args[0] + "_autoconfig.log";
                        Common.DumpLogNewLine();
                        Common.DumpLogMessage(string.Format("*** AutoConfig Versione: {0}, Data: {1:dd-MM-yyyy HH:mm} ***", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DateTime.Now));
                        deviceType = (DeviceType)Enum.Parse(typeof(DeviceType), args[0], true);                        
                        _oManager = ManagerBase.GetManager(deviceType);
                        loadingOk = _oManager.LoadConfig();
                        if (loadingOk)
                            _oManager.DoAutoConfig();
                    }
                    else
                    {
                        _oManager = ManagerBase.GetManager(deviceType);
                        _oManager.DumpProgramArgs();
                    }                                        
                }
                // Parametri: DIS 3,9600,N,8,1,F,T 
                else if (args.Length == 2) 
                {
                    if (args[0] == "DIS")
                    {
                        Common.TOK_PGM_LOG_FILE = "discovery.log";
                        Common.DumpLogNewLine();
                        Common.DumpLogMessage(string.Format("*** AutoConfig Versione: {0}, Data: {1:dd-MM-yyyy HH:mm} ***", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DateTime.Now));
                        ManagerBase.GetManager(DeviceType.DEFAULT).Discovery(args[1]);
                    }
                }
                else
                {
                    // Parametri lettura cfg Amplificatore:R ".\Config\cfgAmpli.txt"   05 3,9600,N,8,1,F,T N AMPLI
                    // Parametri lettura cfg Pannello Zone:R ".\Config\cfgPZ.txt"      04 3,9600,N,8,1,F,T N PZ
                    // Parametri lettura cfg Pannello Zone:R ".\Config\cfgPZV3.txt"    04 3,9600,N,8,1,F,T N PZV3
                    
                    if (args.Length != 6)
                    {
                        _oManager = ManagerBase.GetManager(deviceType);
    					_oManager.DumpProgramArgs();
                    }
    				else
    				{
                        // Commentato issue 11702 "Modifica a AutoConfigurator"
                        //if ( String.Compare(args[4], "Y", StringComparison.OrdinalIgnoreCase) == 0 && !_oManager.StopSTLCServices() )
                        //{
                        //    Common.DumpLogMessage(PzManager.TOK_ERROR_SERVICE_STOP_TEST);
                        //    return;
                        //}

                        if (Enum.IsDefined(typeof(DeviceType), args[5]))
                        {
                            deviceType = (DeviceType)Enum.Parse(typeof(DeviceType), args[5], true);
                           _oManager = ManagerBase.GetManager(deviceType);

                            switch (args[0])
                            {
                                case TOK_READ:
                                    Common.TOK_PGM_LOG_FILE = args[5] + "_read.log";
                                    Common.DumpLogNewLine();
                                    Common.DumpLogMessage(string.Format("*** AutoConfig Versione: {0}, Data: {1:dd-MM-yyyy HH:mm} ***", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DateTime.Now));
                                    _oManager.DoReadConfig(args[2], args[1], args[3]);
                                    break;

                                case TOK_WRITE:
                                     Common.TOK_PGM_LOG_FILE = args[5] + "_write.log";
                                     Common.DumpLogNewLine();
                                     Common.DumpLogMessage(string.Format("*** AutoConfig Versione: {0}, Data: {1:dd-MM-yyyy HH:mm} ***", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, DateTime.Now));
                                    _oManager.DoWriteConfig(args[2], args[1], args[3]);
                                    break;

                                default:
                                    _oManager.DumpProgramArgs();
                                    break;
                            }
                        }                        
                        else {
                            Common.DumpLogMessage(@"ERRORE PARAMETRI");
                            _oManager.DumpProgramArgs();
                        }

                        // Commentato issue 11702 "Modifica a AutoConfigurator"
                        //if ( String.Compare(args[4], "Y", StringComparison.OrdinalIgnoreCase) == 0 && !_oManager.StartSTLCServices() )
                        //{
                        //    Common.DumpLogMessage(PzManager.TOK_ERROR_SERVICE_START_TEST);
                        //}
    				}
					
                }
            }
            catch (Exception e)
            {
                Common.DumpLogMessage(e);

                // Commentato issue 11702 "Modifica a AutoConfigurator"
                //if ( args.Length == 5 && String.Compare(args[4], "Y", StringComparison.OrdinalIgnoreCase) == 0 && !_oManager.StartSTLCServices() )
                //{
                //    Common.DumpLogMessage(PzManager.TOK_ERROR_SERVICE_START_TEST);
                //}
            }        
        }
    }
}

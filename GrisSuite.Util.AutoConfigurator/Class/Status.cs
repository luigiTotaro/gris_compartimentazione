
//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		AmpliStatus.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		17.05.2007
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;


namespace GrisSuite.Util.AutoConfigurator.Class
{
    public abstract class Status
    {
        #region Public Methods

        /// <summary>
        /// Load members from bytes stream
        /// </summary>
        /// <param name="btTmp">input bytes stream</param>
        public abstract void LoadFromStream(byte[] btInput, int nLen);
        
        /// <summary>
        /// Dump status
        /// </summary>
        public abstract void DumpStatus();        

        #endregion
    }
}

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace GrisSuite.Util.AutoConfigurator.Class
{
    public class PzV3Config : PzConfig
    {
        public PzV3Config()
        { 
             TOK_BYTES_BUFFER_LEN = 896;
        }

        #region Token

        protected const int TOK_NUMBER_OF_FILLER = 3;

        #endregion

        #region Private Members

        private UInt16 _testTimeout;
        private UInt16[] _nAttenuationTensionsRange;
        private UInt16[] _nFiller;

        #endregion

        #region Public  Properties

        public UInt16 TestTimeout
        {
            get { return _testTimeout; }
            set { _testTimeout = value; }
        }

        public UInt16[] AttenuationTensionsRange
        {
            get { return _nAttenuationTensionsRange; }
            set { _nAttenuationTensionsRange = value; }
        }

        public UInt16[] Filler
        {
            get { return _nFiller; }
            set { _nFiller = value; }
        }

        #endregion

        public void ApplyDefault(PzV3Status oStatus)
        {
            base.ApplyDefault(oStatus);

            for (int nIdx = 0; nIdx < TensionsRange.Length; nIdx++)
            {
                AttenuationTensionsRange[nIdx] = (UInt16)(0.75 * oStatus.RefAttenuationTensionValue[nIdx]);
            }

            #if DEBUG
                DumpConfig();
            #endif
        }

        /// <summary>
        /// Serialize class to byte stream
        /// </summary>
        /// <returns></returns>
        public override void Serialize2Stream()
        {
            try
            {
                int nOffset = 0;
                byte[] btSerialize = new Byte[2048];
                byte[] btTmp;
                
                // clear buffer
                //
                for (int nIdx = 0; nIdx < 2048; nIdx++)
                    btSerialize[nIdx] = 0;

                // config version
                //
                btTmp = Common.ConvertCharArray2Alfa(_cConfigVersion, _cConfigVersion.Length);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // start night time
                //
                btTmp = Common.ConvertUInt162Alfa(_nStartNightTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // end night time
                //
                btTmp = Common.ConvertUInt162Alfa(_nEndNightTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // zone to disable
                //
                btTmp = Common.ConvertUInt162Alfa(_nZonesToDisabled);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // ampli attenuation
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertByte2Alfa(_nAmpliAttenuation[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // test start time
                //
                btTmp = Common.ConvertUInt162Alfa(_nTestStartTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // test end time
                //
                btTmp = Common.ConvertUInt162Alfa(_nTestEndTime);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // test period
                //
                btTmp = Common.ConvertUInt162Alfa(_nTestPeriod);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // zone to test
                //
                btTmp = Common.ConvertUInt162Alfa(_nZonesToTest);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // test level
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertByte2Alfa(_nTestLevel[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                //  tension range
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertUInt162Alfa(_nTensionsRange[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // resistance range
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                {
                    btTmp = Common.ConvertUInt162Alfa(_nResistanceRange[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // amplifiers mode
                //
                btTmp = Common.ConvertByte2Alfa(_nAmplifiersMode);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                // micros
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                {
                    // priority
                    //
                    btTmp = Common.ConvertByte2Alfa(_Micros[nIdx].Priority);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;

                    // equalization
                    //
                    btTmp = Common.ConvertByte2Alfa(_Micros[nIdx].Equalization);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // inputs
                //
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                {
                    // microid
                    //
                    btTmp = Common.ConvertByte2Alfa(_Inputs[nIdx].MicroId);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;

                    // ampli level
                    //
                    for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                    {
                        btTmp = Common.ConvertSByte2Alfa(_Inputs[nIdx].AmpliLevel[nTmp]);
                        btTmp.CopyTo(btSerialize, nOffset);
                        nOffset += btTmp.Length;
                    }

                    // Zones
                    //
                    btTmp = Common.ConvertUInt162Alfa(_Inputs[nIdx].Zones);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                // program block
                //
                for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                {
                    for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                    {
                        btTmp = Common.ConvertByte2Alfa(_PrgBlocks[nIdx].Params[nTmp]);
                        btTmp.CopyTo(btSerialize, nOffset);
                        nOffset += btTmp.Length;
                    }
                }

                //TestTimeout
                btTmp = Common.ConvertUInt162Alfa(_testTimeout);
                btTmp.CopyTo(btSerialize, nOffset);
                nOffset += btTmp.Length;

                //AttenuationTensionRange
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    btTmp = Common.ConvertUInt162Alfa(_nAttenuationTensionsRange[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                //Filler
                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_FILLER; nIdx++)
                {
                    btTmp = Common.ConvertUInt162Alfa(_nFiller[nIdx]);
                    btTmp.CopyTo(btSerialize, nOffset);
                    nOffset += btTmp.Length;
                }

                _btSerialize = new Byte[nOffset];

                for (int nIdx = 0; nIdx < nOffset; nIdx++)
                    _btSerialize[nIdx] = btSerialize[nIdx];

#if DEBUG

                Common.DumpLogMessage(" >>> Serialize Config Dump <<<");
                Common.DumpLogMessage(TOK_BUFFER_LEN_STR + _btSerialize.Length.ToString());
                Common.DumpLogMessage(TOK_BUFFER_STR + Common.Byte2HexString(_btSerialize));

#endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        public override void DumpConfig()
        {
            base.DumpConfig();

            //TestTimeout
            Common.DumpLogMessage(string.Format("        TestTimeout: 0x{0}", _testTimeout.ToString("X4")));

            //AttenuationTensionRange
            for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                Common.DumpLogMessage(string.Format("        AttenuationTensionsRange[{0}]: 0x{1}", nIdx.ToString("X"), _nAttenuationTensionsRange[nIdx].ToString("X4")));

            //Filler

        }

        public override void LoadFromfile(string sFile)
        {
            base.LoadFromfile(sFile);

            try
            {
                _nAttenuationTensionsRange = new UInt16[TOK_NUMBER_OF_AMPLI];
                _nFiller = new UInt16[TOK_NUMBER_OF_FILLER];

                string sTmp = Common.ReadKeyIniValue("General Config", "TestTimeout", sFile);
                _testTimeout = Convert.ToUInt16(sTmp);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    sTmp = Common.ReadKeyIniValue("General Config", "AttenuationTensionsRange" + (nIdx + 1).ToString(), sFile);
                    _nAttenuationTensionsRange[nIdx] = Convert.ToUInt16(sTmp);
                }

            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        public override void LoadFromStream(byte[] btInput, int nLen)
        {
            try
            {
                int nCount = 0;
                int nOffset = 0;
                Byte[] btTmp = new Byte[1024];

                // check input len
                //
                if (btInput == null || nLen != TOK_BYTES_BUFFER_LEN)
                    throw new Exception(TOK_ERROR_INPUT_BYTE_LEN);

                _cConfigVersion = new char[TOK_CONFIG_VERSION_LENGTH];
                _nAmpliAttenuation = new Byte[TOK_NUMBER_OF_AMPLI];
                _nTestLevel = new Byte[TOK_NUMBER_OF_AMPLI];
                _nTensionsRange = new UInt16[TOK_NUMBER_OF_AMPLI];
                _nResistanceRange = new UInt16[TOK_NUMBER_OF_ZONES];
                _Micros = new Micro[TOK_NUMBER_OF_MICRO];
                _Inputs = new Input[TOK_NUMBER_OF_INPUTS];
                _PrgBlocks = new FunctionParams[TOK_MAX_PLC_PROG_BLOCKS];
                _nAttenuationTensionsRange = new UInt16[TOK_NUMBER_OF_AMPLI];
                _nFiller = new UInt16[TOK_NUMBER_OF_FILLER];

                nOffset = 0;
                for (nCount = 0; nCount < TOK_CONFIG_VERSION_LENGTH * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _cConfigVersion = Common.ConvertAlfa2CharArray(btTmp, TOK_CONFIG_VERSION_LENGTH);

                nOffset += TOK_CONFIG_VERSION_LENGTH * 2;
                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nStartNightTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nEndNightTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);
                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesToDisabled = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nAmpliAttenuation[nIdx] = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);
                }

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTestStartTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTestEndTime = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nTestPeriod = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nZonesToTest = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nTestLevel[nIdx] = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nTensionsRange[nIdx] = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nResistanceRange[nIdx] = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }

                for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _nAmplifiersMode = Common.ConvertAlfa2Byte(btTmp);

                nOffset += (sizeof(Byte) * 2);

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Micros[nIdx].Priority = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);

                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Micros[nIdx].Equalization = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);

                }

                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Inputs[nIdx].MicroId = Common.ConvertAlfa2Byte(btTmp);

                    nOffset += (sizeof(Byte) * 2);

                    _Inputs[nIdx].AmpliLevel = new SByte[TOK_NUMBER_OF_AMPLI];

                    for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                    {
                        for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                            btTmp[nCount] = btInput[nOffset + nCount];

                        _Inputs[nIdx].AmpliLevel[nTmp] = Common.ConvertAlfa2SByte(btTmp);

                        nOffset += (sizeof(SByte) * 2);
                    }

                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _Inputs[nIdx].Zones = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }

                for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                {
                    _PrgBlocks[nIdx].Params = new Byte[TOK_PARAMETERS_LENGTH];

                    for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                    {
                        for (nCount = 0; nCount < sizeof(Byte) * 2; nCount++)
                            btTmp[nCount] = btInput[nOffset + nCount];

                        _PrgBlocks[nIdx].Params[nTmp] = Common.ConvertAlfa2Byte(btTmp);

                        nOffset += (sizeof(Byte) * 2);
                    }
                }

                for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                    btTmp[nCount] = btInput[nOffset + nCount];

                _testTimeout = Common.ConvertAlfa2UInt16(btTmp);

                nOffset += (sizeof(UInt16) * 2);


                for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                {
                    for (nCount = 0; nCount < sizeof(UInt16) * 2; nCount++)
                        btTmp[nCount] = btInput[nOffset + nCount];

                    _nAttenuationTensionsRange[nIdx] = Common.ConvertAlfa2UInt16(btTmp);

                    nOffset += (sizeof(UInt16) * 2);
                }


#if DEBUG
                DumpConfig();
#endif
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        public override void Save2File(string sFile)
        {
            try
            {
                string sTmp = string.Empty;

                // dump configuration to file
                //
                using (TextWriter oWriter = new StreamWriter(sFile))
                {
                    oWriter.WriteLine("[Telefin PDS Config File]");
                    for (int nIdx = 0; nIdx < TOK_CONFIG_VERSION_LENGTH; nIdx++)
                        sTmp += _cConfigVersion[nIdx];
                    oWriter.WriteLine("Version=" + sTmp);
                    oWriter.WriteLine("");

                    oWriter.WriteLine("[General Config]");
                    sTmp = ((int)(_nStartNightTime / 60)).ToString("00") + "." + ((int)(_nStartNightTime % 60)).ToString("00");
                    oWriter.WriteLine("StartNightTime=" + sTmp);
                    sTmp = ((int)(_nEndNightTime / 60)).ToString("00") + "." + ((int)(_nEndNightTime % 60)).ToString("00");
                    oWriter.WriteLine("EndNightTime=" + sTmp);
                    oWriter.WriteLine("AttenuationAmpli1=" + ((int)_nAmpliAttenuation[0]).ToString());
                    oWriter.WriteLine("AttenuationAmpli2=" + ((int)_nAmpliAttenuation[1]).ToString());
                    sTmp = ((int)(_nTestStartTime / 60)).ToString("00") + "." + ((int)(_nTestStartTime % 60)).ToString("00");
                    oWriter.WriteLine("TestStartTime=" + sTmp);
                    sTmp = ((int)(_nTestEndTime / 60)).ToString("00") + "." + ((int)(_nTestEndTime % 60)).ToString("00");
                    oWriter.WriteLine("TestEndTime=" + sTmp);
                    oWriter.WriteLine("TestPeriod=" + _nTestPeriod.ToString());
                    oWriter.WriteLine("ZonesToTest=" + Common.GetBitPositionString(_nZonesToTest));
                    oWriter.WriteLine("ZonesToDisabled=" + Common.GetBitPositionString(_nZonesToDisabled));
                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                        oWriter.WriteLine("TestLevelAmpli" + (nIdx + 1).ToString() + "=" + ((int)_nTestLevel[nIdx]).ToString());
                    oWriter.WriteLine("AmplifiersMode=" + ((int)_nAmplifiersMode).ToString());
                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                        oWriter.WriteLine("TensionsRangeAmpli" + (nIdx + 1).ToString() + "=" + ((int)_nTensionsRange[nIdx]).ToString());
                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_ZONES; nIdx++)
                        oWriter.WriteLine("ResistanceRange" + (nIdx + 1).ToString() + "=" + ((int)_nResistanceRange[nIdx]).ToString());
                    oWriter.WriteLine("TestTimeout=" + (_testTimeout).ToString());
                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_AMPLI; nIdx++)
                        oWriter.WriteLine("AttenuationTensionsRange" + (nIdx + 1).ToString() + "=" + ((int)_nAttenuationTensionsRange[nIdx]).ToString());

                    oWriter.WriteLine("");

                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_MICRO; nIdx++)
                    {
                        oWriter.WriteLine("[MicroInputs" + (nIdx + 1).ToString() + "]");
                        oWriter.WriteLine("Priority=" + ((int)_Micros[nIdx].Priority).ToString());
                        oWriter.WriteLine("Equalization=" + ((int)_Micros[nIdx].Equalization).ToString());
                        oWriter.WriteLine("");
                    }

                    for (int nIdx = 0; nIdx < TOK_NUMBER_OF_INPUTS; nIdx++)
                    {
                        oWriter.WriteLine("[Input" + (nIdx + 1).ToString() + "]");
                        oWriter.WriteLine("MicroId=" + ((int)_Inputs[nIdx].MicroId).ToString());
                        for (int nTmp = 0; nTmp < TOK_NUMBER_OF_AMPLI; nTmp++)
                            oWriter.WriteLine("LevelAmpi" + (nTmp + 1).ToString() + "=" + ((int)_Inputs[nIdx].AmpliLevel[nTmp]).ToString());
                        oWriter.WriteLine("Zones=" + Common.GetBitPositionString((int)_Inputs[nIdx].Zones));
                        oWriter.WriteLine("");
                    }

                    for (int nIdx = 0; nIdx < TOK_MAX_PLC_PROG_BLOCKS; nIdx++)
                    {
                        oWriter.WriteLine("[PrgBlock" + (nIdx + 1).ToString() + "]");
                        for (int nTmp = 0; nTmp < TOK_PARAMETERS_LENGTH; nTmp++)
                            oWriter.WriteLine("Param" + (nTmp + 1).ToString() + "=" + ((int)_PrgBlocks[nIdx].Params[nTmp]).ToString());
                        oWriter.WriteLine("");
                    }
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                sbError.Append(e.ToString());

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

    }
}


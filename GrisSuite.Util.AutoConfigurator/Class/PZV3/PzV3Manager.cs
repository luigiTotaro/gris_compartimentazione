using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Microsoft.Win32;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.ServiceProcess;

namespace GrisSuite.Util.AutoConfigurator.Class
{
    class PzV3Manager:PzManager
    {
        #region Token

        //protected string TOK_CMD_ASK_STATUS_EXT = @"d";
        //protected string TOK_CMD_ASK_CONFIG_EXT = @"a";
        //protected string TOK_CMD_WRITE_CONFIG_EXT = @"r";
        //protected string TOK_CMD_WRITE_CONFIG_EEPROM_EXT = @"h";

        #endregion

        public PzV3Manager(): base()
        {
            TOK_XPATH_DEVICES_QUERY = "./telefin/system/server/region/zone/node/device[@type=\"TT10210V3\"]/.";

            TOK_CMD_ASK_STATUS = @"d";
            TOK_CMD_ASK_CONFIG = @"a";
            TOK_CMD_WRITE_CONFIG = @"r";
            TOK_CMD_WRITE_EEPROM_CONFIG = @"h";
            
            SW_VERSIONS.Add("GW30263 1.00");    // GW30263 1.00
        }
        
        #region Accessor

        private PzV3Config Config
        {
            get { return _oConfig as PzV3Config; }
        }

        private PzV3Status Status
        {
            get { return _oStatus as PzV3Status; }
        }

        #endregion

        public override void DoAutoConfig()
        {
            try
            {
                bool bRun;
                bool bFirst = false;

                _nState = ManageState.None;

                foreach (Item oItem in _olItems)
                {
                    PzItem oPZItem = oItem as PzItem;
                    bRun = true;
                    bFirst = true;
                    _nState = ManageState.None;

                    do
                    {
                        switch (_nState)
                        {
                            // init state
                            // 
                            case ManageState.None:

                                if (this.StepInitItem(oPZItem))
                                    _nState = ManageState.Start;

                                break;

                            // start
                            //
                            case ManageState.Start:
                                {
                                    if (this.StepStart())
                                        _nState = ManageState.Start;

                                    _nState = ManageState.OpenComPort;

                                    break;
                                }
                            // open com port
                            //
                            case ManageState.OpenComPort:

                                if (this.StepOpenComPort(oPZItem))
                                    _nState = ManageState.Start;

                                _nState = ManageState.ReadSoftwareVersion;

                                break;

                            // Read software version
                            //
                            case ManageState.ReadSoftwareVersion:

                                if (this.StepReadSoftwareVersion(oPZItem))
                                    _nState = ManageState.ReadStatus;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // Read Status
                            //
                            case ManageState.ReadStatus:

                                if (this.StepReadStatus(oPZItem))
                                    _nState = ManageState.ReadConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // Read PZ config
                            //
                            case ManageState.ReadConfiguration:

                                if (this.StepReadConfiguration(oPZItem))
                                    _nState = ManageState.ProcessConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // process configuration
                            //
                            case ManageState.ProcessConfiguration:

                                if (this.StepProcessConfiguration())
                                    _nState = ManageState.WriteConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // write configuration
                            //
                            case ManageState.WriteConfiguration:

                                if (this.StepWriteEEPromConfiguration(oPZItem))
                                    _nState = ManageState.SendAutoSetup;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // send auto setup
                            //
                            case ManageState.SendAutoSetup:

                                if (this.StepSendAutoSetup(oPZItem))
                                    _nState = ManageState.StartInternalTest;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // send start internal test
                            //
                            case ManageState.StartInternalTest:

                                if (this.StepStartInternalTest(oPZItem))
                                    _nState = ManageState.EndInternalTest;
                                else
                                    _nState = ManageState.ErrorStatus;


                                break;

                            // end internal test
                            //
                            case ManageState.EndInternalTest:

                                if ((Status.ZonesTestFail & Config.ZonesToTest) == 0 && bFirst == false)
                                    _nState = ManageState.WriteEEPromConfiguration;
                                else
                                {
                                    // flag to force the first test
                                    //
                                    bFirst = false;

                                    Config.ApplyDefault(Status);

                                    _nState = ManageState.WriteConfiguration;
                                }

                                break;

                            // write configuration to EEProm
                            //
                            case ManageState.WriteEEPromConfiguration:

                                if (this.StepWriteEEPromConfiguration(oPZItem))
                                    _nState = ManageState.ResetFlagConfigurazione;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // reset flag configuration changed
                            //
                            case ManageState.ResetFlagConfigurazione:

                                if (this.StepResetFlagConfigurazione(oPZItem))
                                    _nState = ManageState.CloseComPort;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            case ManageState.CloseComPort:

                                if (this.StepCloseComPort(oPZItem))
                                    _nState = ManageState.End;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            case ManageState.End:
                                {
                                    if (this.StepEnd(oPZItem))
                                    {
                                        _nState = ManageState.None;
                                        bRun = false;
                                    }
                                    else
                                    {
                                        _nState = ManageState.ErrorStatus;
                                    }

                                    break;
                                }
                            // error status
                            //
                            case ManageState.ErrorStatus:
                                {
                                    _nState = ManageState.CloseComPort;
                                    break;
                                }
                        }

                    } while (bRun == true);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        #region Protected Methods

        protected override Item CreateItem()
        {
            return new PzV3Item();
        }

        protected override Config CreateConfig()
        {
            return new PzV3Config();
        }

        protected override Status CreateStatus()
        {
            return new PzV3Status();
        }

        #endregion

    }
}

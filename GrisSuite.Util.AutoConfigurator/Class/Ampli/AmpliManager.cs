using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Reflection;

namespace GrisSuite.Util.AutoConfigurator.Class
{
    public class AmpliManager : ManagerBase
    {
        #region Enums

        private enum ManageState : int
        {
            None,
            Start,
            OpenComPort,
            CloseComPort,
            ReadSoftwareVersion,
            ReadStatus,
            ReadConfiguration,
            ProcessConfiguration,
            WriteConfiguration,
            WriteEEPromConfiguration,
            ResetFlagConfigurazione,
            End,
            ErrorStatus
        }

        #endregion

        public AmpliManager()
        {
            TOK_XPATH_DEVICES_QUERY = "./telefin/system/server/region/zone/node/device[@type=\"DT04000\"]/.";
            TOK_ERROR_XML_DEVICE_NOT_PRESENT = @"Il file di configurazione non contiene nessuna definizione di amplificatore [Code=1]";
            
            TOK_MSG_ITEM_NAME = @"Lettura configurazione Amplificatore: ";        
            TOK_MSG_START_SAVE_CONFIG = @"Salvataggio configurazione Amplificatore su file...";
            TOK_MSG_START_WRITE_CONFIG = @"Scrittura configurazione Amplificatore da file...";

            SW_VERSIONS.Add("CTRL ADS 0.00");    // GW30196 Versione 0.00
            SW_VERSIONS.Add("CTRL ADS 0.01");    // GW30196 Versione 0.01
            SW_VERSIONS.Add("CTRL ADS 0.02");    // GW30196 Versione 0.02
            SW_VERSIONS.Add("CTRL ADS 0.03");    // GW30196 Versione 0.03
            SW_VERSIONS.Add("CTRL ADS 1.00");    // GW30196 Versione 1.00


            configFilename  = @"Config\AmpliConfig_COM{0}_ADDRESS{1}.txt";
            configBackupFilename = @"Config\AmpliConfig_COM{0}_ADDRESS{1}_Backup.txt";
        }
        

        #region Private Members

        private ManageState _nState = ManageState.None;

        #endregion

        protected override Item CreateItem()
        {
            return new AmpliItem();
        }

        protected override Status CreateStatus()
        {
            return new AmpliStatus();
        }

        protected override Config CreateConfig()
        {
            return new AmpliConfig();
        }

        protected virtual bool StepProcessConfiguration()
        {
            (_oConfig as AmpliConfig).ApplyDefault();
            return true;
        }

        /// <summary>
        /// Do Autoconfig
        /// </summary>
        public override void DoAutoConfig()
        {
            try
            {
                bool bRun;
                _nState = ManageState.None;

                foreach (AmpliItem oItem in _olItems)
                {
                    bRun = true;
                    _nState = ManageState.None;

                    do
                    {
                        switch (_nState)
                        {
                            // init state
                            // 
                            case ManageState.None:

                                if (this.StepInitItem(oItem))
                                    _nState = ManageState.Start;
                                break;

                            // start
                            //
                            case ManageState.Start:
                                {
                                    if (this.StepStart())
                                        _nState = ManageState.OpenComPort;

                                    break;
                                }
                            // open com port
                            //
                            case ManageState.OpenComPort:

                                if (this.StepOpenComPort(oItem))
                                    _nState = ManageState.ReadSoftwareVersion;

                                break;

                            // Read software version
                            //
                            case ManageState.ReadSoftwareVersion:

                                if (this.StepReadSoftwareVersion(oItem))
                                    _nState = ManageState.ReadStatus;
                                else
                                    _nState = ManageState.ErrorStatus; 

                                break;

                            // Read Status
                            //
                            case ManageState.ReadStatus:

                                if (this.StepReadStatus(oItem))
                                    _nState = ManageState.ReadConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // Read Ampli config
                            //
                            case ManageState.ReadConfiguration:

                                if (this.StepReadConfiguration(oItem))
                                    _nState = ManageState.ProcessConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // process configuration
                            //
                            case ManageState.ProcessConfiguration:

                                if (this.StepProcessConfiguration())
                                    _nState = ManageState.WriteEEPromConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // write configuration to EEProm
                            //
                            case ManageState.WriteEEPromConfiguration:

                                if (this.StepWriteEEPromConfiguration(oItem))
                                    _nState = ManageState.ResetFlagConfigurazione;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // reset flag configuration changed
                            //
                            case ManageState.ResetFlagConfigurazione:
                                {
                                    if (this.StepResetFlagConfigurazione(oItem))
                                        _nState = ManageState.CloseComPort;
                                    else
                                        _nState = ManageState.ErrorStatus;                                    

                                    break;
                                }
                            case ManageState.CloseComPort:
                                {
                                    if (this.StepCloseComPort(oItem))
                                        _nState = ManageState.End;
                                    else
                                        _nState = ManageState.ErrorStatus;

                                    break;
                                }
                            case ManageState.End:
                                {
                                    if (this.StepEnd(oItem))
                                    {
                                        _nState = ManageState.None;
                                        bRun = false;
                                    }
                                    else
                                    {
                                        _nState = ManageState.ErrorStatus;
                                    }

                                    break;
                                }
                            // error status
                            //
                            case ManageState.ErrorStatus:
                                {
                                    _nState = ManageState.CloseComPort;
                                    break;
                                }
                        }

                    } while (bRun == true);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        public override void DoReadConfig(string sAddress, string sFile, string sComSettings)
        {
            if (!base.DoReadConfigInternal(sAddress, sFile, sComSettings))
                _nState = ManageState.ErrorStatus;
        }

        public override void DoWriteConfig(string sAddress, string sFile, string sComSettings)
        {
            if (!base.DoWriteConfigInternal(sAddress, sFile, sComSettings))
                _nState = ManageState.ErrorStatus;
            else
                _nState = ManageState.CloseComPort;
        }
    }
}


//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		Manager.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		31.05.2007
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Microsoft.Win32;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.ServiceProcess;

namespace GrisSuite.Util.AutoConfigurator.Class
{
    class PzManager:ManagerBase
    {
        #region Token

        protected string TOK_MSG_INTERNAL_TESTS = @"Test interni ";
        protected string TOK_MSG_AUTO_TESTS = @"Auto test ";
        

        private string TOK_CMD_SEND_AUTOSETUP = @"S";
        private string TOK_CMD_START_TESTS = @"T";
        
        private int TOK_TIMEOUT_AUTO_SETUP_MINUTE = 10;
        private UInt16 TOK_ID_AUTOSETUP_RUNNING = 0x0040;
        private UInt16 TOK_ID_TEST_RUNNING = 0x0080;
        private UInt16 TOK_ID_TEST_SUSPEND = 0x0100;


        private string TOK_ERROR_WRITE_AUTO_SETUP = @"*** Errore in scrittura auto setup dispositivo ***";
        private string TOK_ERROR_WRITE_START_TESTS = @"*** Errore in scrittura start tests dispositivo ***";


        #endregion

        #region Enums

        protected enum ManageState : int
        {
            None,
            Start,
            OpenComPort,
            CloseComPort,
            ReadSoftwareVersion,
            ReadStatus,
            ReadConfiguration,
            ProcessConfiguration,
            WriteConfiguration,
            SendAutoSetup,
            StartInternalTest,
            EndInternalTest,
            WriteEEPromConfiguration,
            ResetFlagConfigurazione,
            End,
            ErrorStatus
        }

        #endregion

        public PzManager(): base()
        {            
            TOK_XPATH_DEVICES_QUERY = "./telefin/system/server/region/zone/node/device[@type=\"TT10210\"]/.";
            TOK_ERROR_XML_DEVICE_NOT_PRESENT = @"Il file di configurazione non contiene nessuna definizione di pannelli zona [Code=1]";

            TOK_MSG_ITEM_NAME = @"Lettura configurazione Pannello Zone: ";
            TOK_MSG_START_SAVE_CONFIG = @"Salvataggio configurazione dispositivo zone su file...";
            TOK_MSG_START_WRITE_CONFIG = @"Scrittura configurazione dispositivo zone da file...";
            
            SW_VERSIONS.Add("PDS Rel 1.00");   // GW30197 Versione 1.00 (firmware non compatibile)
            SW_VERSIONS.Add("PDS Rel 1.01");   // GW30197 Versione 1.01 (firmware non compatibile)
            SW_VERSIONS.Add("PDS Rel 0.00");   // GW30197-GW30263 Versione 0.00 (firmware non compatibile)
            SW_VERSIONS.Add("PDS Rel 0.01");   // GW30263 Versione 0.01 (firmware non compatibile)
            SW_VERSIONS.Add("PDS Rel 0.02");   // GW30263 Versione 0.02 (firmware non compatibile)

            configFilename = @"Config\PZConfig_COM{0}_ADDRESS{1}.txt";
            configBackupFilename = @"Config\PZConfig_COM{0}_ADDRESS{1}_Backup.txt";
        }


        #region protected Members

        protected ManageState     _nState             = ManageState.None;

        #endregion

        #region Accessor

        private PzConfig Config
        {
            get { return _oConfig as PzConfig; }
        }

        private PzStatus Status
        {
            get { return _oStatus as PzStatus; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Main function
        /// </summary>
        public override void DoAutoConfig()
        {
            try
            {
                bool            bRun;
                bool            bFirst  = false;

                _nState = ManageState.None;

                foreach (Item oItem in _olItems)
                {
                    PzItem oPZItem = oItem as PzItem;
                    bRun    = true;
                    bFirst  = true;
                    _nState = ManageState.None;

                    do
                    {
                        switch (_nState)
                        {
                            // init state
                            // 
                            case ManageState.None:
                                                                
                                if (this.StepInitItem(oPZItem))                                
                                    _nState = ManageState.Start;
                                
                                break;

                            // start
                            //
							case ManageState.Start:
								{
                                    if (this.StepStart())                                
                                        _nState = ManageState.Start;
                                                                    
									_nState = ManageState.OpenComPort;

									break;
								}
                            // open com port
                            //
                            case ManageState.OpenComPort:

                                if (this.StepOpenComPort(oPZItem))                                
                                    _nState = ManageState.Start;

                                _nState = ManageState.ReadSoftwareVersion;

                                break;

                            // Read software version
                            //
                            case ManageState.ReadSoftwareVersion:

                                if (this.StepReadSoftwareVersion(oPZItem))
                                    _nState = ManageState.ReadStatus;
                                else
                                    _nState = ManageState.ErrorStatus;                               

                                break;

                            // Read Status
                            //
                            case ManageState.ReadStatus:

                                if (this.StepReadStatus(oPZItem))
                                    _nState = ManageState.ReadConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;
                                
                                break;

                            // Read PZ config
                            //
                            case ManageState.ReadConfiguration:

                                if (this.StepReadConfiguration(oPZItem))
                                    _nState = ManageState.ProcessConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // process configuration
                            //
                            case ManageState.ProcessConfiguration:

                                if (this.StepProcessConfiguration())
                                    _nState = ManageState.WriteConfiguration;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // write configuration
                            //
                            case ManageState.WriteConfiguration:

                                if (this.StepWriteEEPromConfiguration(oPZItem))
                                    _nState = ManageState.SendAutoSetup;
                                else
                                    _nState = ManageState.ErrorStatus;
                                
                                break;

                            // send auto setup
                            //
                            case ManageState.SendAutoSetup:

                                if (this.StepSendAutoSetup(oPZItem))
                                    _nState = ManageState.StartInternalTest;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // send start internal test
                            //
                            case ManageState.StartInternalTest:

                                if (this.StepStartInternalTest(oPZItem))
                                    _nState = ManageState.EndInternalTest;
                                else
                                    _nState = ManageState.ErrorStatus;


                                break;

                            // end internal test
                            //
                            case ManageState.EndInternalTest:

                                if ((Status.ZonesTestFail & Config.ZonesToTest) == 0 && bFirst == false)
                                    _nState = ManageState.WriteEEPromConfiguration;
                                else
                                {
                                    // flag to force the first test
                                    //
                                    bFirst = false;

                                    Config.ApplyDefault(Status);

                                    _nState = ManageState.WriteConfiguration;                                    
                                }

                                break;

                            // write configuration to EEProm
                            //
                            case ManageState.WriteEEPromConfiguration:

                                if (this.StepWriteEEPromConfiguration(oPZItem))
                                    _nState = ManageState.ResetFlagConfigurazione;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;

                            // reset flag configuration changed
                            //
							case ManageState.ResetFlagConfigurazione:

                                if (this.StepResetFlagConfigurazione(oPZItem))
                                    _nState = ManageState.CloseComPort;
                                else
                                    _nState = ManageState.ErrorStatus;

                                break;									
								
							case ManageState.CloseComPort:

                                if (this.StepCloseComPort(oPZItem))
                                    _nState = ManageState.End;
                                else
                                    _nState = ManageState.ErrorStatus;

								break;
								
							case ManageState.End:
								{
                                    if (this.StepEnd(oPZItem))
                                    {
                                        _nState = ManageState.None;
                                        bRun = false;
                                    }
                                    else
                                    {
                                        _nState = ManageState.ErrorStatus;
                                    }

									break;
								}
                            // error status
                            //
							case ManageState.ErrorStatus:
								{
									_nState = ManageState.CloseComPort;
									break;
								}
                        }

                    } while (bRun == true);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
                #if DEBUG
                    sbError.Append(e.ToString());
                #else
                    sbError.Append(e.Message);
                #endif
                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        public override void DoReadConfig(string sAddress, string sFile, string sComSettings)
        {
            if (!base.DoReadConfigInternal(sAddress, sFile, sComSettings))
                _nState = ManageState.ErrorStatus;
        }

        public override void DoWriteConfig(string sAddress, string sFile, string sComSettings)
        {
            if (!base.DoWriteConfigInternal(sAddress, sFile, sComSettings))
                _nState = ManageState.ErrorStatus;
            else
                _nState = ManageState.CloseComPort;
        }

        #endregion

        #region protected Methods

        protected override Item CreateItem()
        {
            return new PzItem();
        }

        protected override Config CreateConfig()
        {
            return new PzConfig();
        }

        protected override Status CreateStatus()
        {
            return new PzStatus();
        }



        protected virtual bool StepProcessConfiguration()
        {
            Config.ApplyZone2Test();
            return true;
        }


        protected virtual bool StepSendAutoSetup(Item oPZItem)
        {
            Common.DumpLogMessage(TOK_MSG_AUTO_TESTS);

            bool bRet = false;
            if (oPZItem.SerialWrap.SendCommand(TOK_CMD_SEND_AUTOSETUP, oPZItem.Address))
            {
                oPZItem.SerialWrap.CloseSentCommand();

                // must wait to start autosetup
                //
                Thread.Sleep(300);

                UInt16[] nFlags = new UInt16[3];

                nFlags[0] = TOK_ID_AUTOSETUP_RUNNING;
                nFlags[1] = TOK_ID_TEST_RUNNING;
                nFlags[2] = TOK_ID_TEST_SUSPEND;

                bRet = WaitStatusFlag(oPZItem, nFlags, TOK_TIMEOUT_AUTO_SETUP_MINUTE);
            }
            else
                oPZItem.SerialWrap.CloseSentCommand();

            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_WRITE_AUTO_SETUP);
                return false;
            }
            else
                return true;
        }

        private bool WaitStatusFlag(Item oPZItem, UInt16[] nFlags, int nTimeOut)
        {
            try
            {
                bool bWait = true;
                DateTime dtStart = DateTime.Now;
                TimeSpan tsDiff;
                bool bError = false;
                bool bCheckFlag = false;

                bError = false;

                do
                {
                    // send ask status command
                    //
                    if (oPZItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oPZItem.Address))
                    {
                        bool bIsEnd = false;

                        do
                        {
                            // read command buffer
                            //
                            if (oPZItem.SerialWrap.ReadCommandBuffer(oPZItem.Address, ref bIsEnd) == false)
                            {
                                bError = true;
                                break;
                            }

                        } while (bIsEnd == false);
                    }
                    else
                        bError = true;

                    oPZItem.SerialWrap.CloseSentCommand();

                    if (bError)
                        return (false);

                    if (_oStatus == null)
                        _oStatus = new PzStatus();

                    // load object from stream
                    //
                    Status.LoadFromStream(oPZItem.SerialWrap.ReadBuffer, oPZItem.SerialWrap.ReadBufferLen);

                    // check flag status
                    //
                    bCheckFlag = false;
                    for (int nIdx = 0; nIdx < nFlags.Length; nIdx++)
                    {
                        if ((Status.StatusFlags & nFlags[nIdx]) == nFlags[nIdx])
                        {
                            bCheckFlag = true;
                            break;
                        }
                    }

                    if (bCheckFlag)
                    {
                        Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);

                        System.Threading.Thread.Sleep(TOK_MS_WAIT);

                        tsDiff = DateTime.Now - dtStart;

                        if (tsDiff.Minutes >= nTimeOut)
                        {
                            Common.DumpLogMessage(TOK_ERROR_TIMEOUT_CHECK_STATUS);
                            bError = true;
                            _oStatus.DumpStatus();
                            return (false);
                        }
                    }
                    else
                    {
                        //Common.DumpLogNewLine();
                        _oStatus.DumpStatus();
                        bWait = false;
                    }

                } while (bWait == true);

                return (true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        protected virtual bool StepStartInternalTest(Item oPZItem)
        {
            Common.DumpLogMessage(TOK_MSG_INTERNAL_TESTS);

            bool bRet = false;
            if (oPZItem.SerialWrap.SendCommand(TOK_CMD_START_TESTS, oPZItem.Address))
            {
                oPZItem.SerialWrap.CloseSentCommand();

                // must wait to start autosetup
                //
                Thread.Sleep(300);

                UInt16[] nFlags = new UInt16[3];

                nFlags[0] = TOK_ID_AUTOSETUP_RUNNING;
                nFlags[1] = TOK_ID_TEST_RUNNING;
                nFlags[2] = TOK_ID_TEST_SUSPEND;

                bRet = WaitStatusFlag(oPZItem, nFlags, TOK_TIMEOUT_AUTO_SETUP_MINUTE);
            }
            else
                oPZItem.SerialWrap.CloseSentCommand();

            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_WRITE_START_TESTS);
                return false;
            }
            else
                return true;
        }

        #endregion

    }
}

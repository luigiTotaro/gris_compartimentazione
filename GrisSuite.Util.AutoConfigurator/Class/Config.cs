
//
// Telefin
//
// Progetto:	AutoConfigurator
// File:		AmpliConfig.cs
// Modulo:
// Autore:		Mirco Vanini
// Data:		24.05.2007
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace GrisSuite.Util.AutoConfigurator.Class
{
    public abstract class Config
    {
        protected Byte[] _btSerialize;           // serialize buffer;
        protected char[] _cConfigVersion;        // versione configurazione sistema 

        #region Accessor

        public Byte[] SerializeBuffer
        {
            get
            {
                return (_btSerialize);
            }
        }

        public char[] ConfigVersion
        {
            get
            {
                return (_cConfigVersion);
            }
            set
            {
                _cConfigVersion = value;
            }
        }

        #endregion

        /// <summary>
        /// Load members from bytes stream
        /// </summary>
        /// <param name="btTmp">input bytes stream</param>
        public abstract void LoadFromStream(byte[] btInput, int nLen);

        /// <summary>
        /// Serialize class to byte stream
        /// </summary>
        /// <returns></returns>
        public abstract void Serialize2Stream();

        /// <summary>
        /// Dump config
        /// </summary>
        public abstract void DumpConfig();

        /// <summary>
        /// Save configuration to file
        /// </summary>
        /// <param name="sFile">file name</param>
        public abstract void Save2File(string sFile);

        /// <summary>
        /// Load configuration from file
        /// </summary>
        /// <param name="sFile">file name</param>
        public abstract void LoadFromfile(string sFile);

    }
}
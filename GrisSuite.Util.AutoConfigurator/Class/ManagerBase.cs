using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.ServiceProcess;
using System.Xml;
using Microsoft.Win32;
using System.IO;
using System.Reflection;

namespace GrisSuite.Util.AutoConfigurator.Class
{
    public enum DeviceType
    {
        DEFAULT,
        AMPLI,
        PZ,
        PZV3
    }

    public abstract class ManagerBase
    {

        protected int TOK_MAX_BLOCK_SIZE = 128;
        
        protected string configFilename  = string.Empty;
        protected string configBackupFilename = string.Empty;


        #region Token

        protected string TOK_RS232_TYPE = @"STLC1000_RS232";
        protected string TOK_XPATH_DEVICES_QUERY = "./telefin/system/server/region/zone/node/device[@type=\"TT10210\"]/.";
        protected string TOK_XPATH_PORT_QUERY = "./telefin/port/item[@id=\"@PORT_NUMBER\"]/.";
        protected string TOK_ERROR_XML_DEVICE_NOT_PRESENT = @"Il file di configurazione non contiene nessuna definizione di pannelli zona [Code=1]";
        protected IList<string> SW_VERSIONS = new List<string>();

        protected string TOK_MSG_WAIT_STEP = @".";
        protected string TOK_MSG_ITEM_NAME = @"Lettura configurazione : ";
        protected string TOK_MSG_LOAD_CONFIG = @"Lettura configurazione sistema...";
        protected string TOK_MSG_NODE_NAME = @" nodo: ";
        protected string TOK_MSG_START_AUTOCONF = @"Inizio auto configurazione dispositivo: ";
        protected string TOK_MSG_END_AUTOCONF = @"Fine auto configurazione dispositivo: ";
        protected string TOK_MSG_SW_VERSION = @"Versione Software: ";

        protected string TOK_CMD_ASK_SW_VERSION = @"N";
        protected string TOK_CMD_ASK_STATUS = @"D";
        protected string TOK_CMD_ASK_CONFIG = @"A";
        protected string TOK_CMD_WRITE_CONFIG = @"R";
        protected string TOK_CMD_WRITE_EEPROM_CONFIG = @"H";
        protected string TOK_CMD_RESET_FLAG_CONFIG = @"J";

        protected int TOK_TIMEOUT_AUTO_TESTS_MINUTE = 10;
        protected int TOK_MS_WAIT = 2000;

        protected string TOK_MSG_WRITE_CONFIG = @"Scrittura configurazione ";
        protected string TOK_MSG_WRITE_END_CONFIG = @"Scrittura configurazione finale";
        protected string TOK_MSG_START_SAVE_CONFIG = @"Salvataggio configurazione dispositivo zone su file...";
        protected string TOK_MSG_END_SAVE_CONFIG = @"Salvataggio configurazione concluso";
        protected string TOK_MSG_START_WRITE_CONFIG = @"Scrittura configurazione dispositivo zone da file...";
        protected string TOK_MSG_END_WRITE_CONFIG = @"Scrittura configurazione concluso";



        protected string TOK_ERROR_READ_SW_VERSION = @"*** Errore in lettura versione software ***";
        protected string TOK_ERROR_READ_SW_VERSION_ERROR = @"*** versione software errata ***";
        protected string TOK_ERROR_READ_STATUS = @"*** Errore in lettura stato dispositivo ***";
        protected string TOK_ERROR_READ_CONFIGURATION = @"*** Errore in lettura configurazione dispositivo ***";
        protected string TOK_ERROR_WRITE_CONFIGURATION = @"*** Errore in scrittura configurazione dispositivo ***";
        protected string TOK_ERROR_TIMEOUT_CHECK_STATUS = @"*** Errore di Timout su controllo stato dispositivo ***";
        protected string TOK_ERROR_XML_NOT_EXIST = @"Il file xml di configurazione non � presente [Code=0]";
        protected string TOK_ERROR_XML_PORT_NOT_PRESENT = @"Il file di configurazione non contiene la definizione della porta numero @PORT_NUMBER [Code=2]";
        internal const string TOK_ERROR_SERVICE_START_TEST = @"*** Impossibile avviare il servizio 'STLC Manager Service'. ***";
        internal const string TOK_ERROR_SERVICE_STOP_TEST = @"*** Impossibile arrestare il servizio 'STLC Manager Service' o 'STLC Supervisor Server Service'. ***";

        public string TOK_STLCMANAGERSERVICE = @"STLCManagerService";
        public string TOK_SPVSERVERDAEMON = @"SPVServerDaemon";
        private string TOK_MSG_STARTING_MGR_SERVICE = @"Avvio del servizio ""STLC Manager Service"" in corso...";
        private string TOK_MSG_STARTED_MGR_SERVICE = @"Servizio ""STLC Manager"" avviato !";
        private string TOK_MSG_STOPPING_MGR_SERVICE = @"Arresto del servizio ""STLC Manager Service"" in corso...";
        private string TOK_MSG_STOPPED_MGR_SERVICE = @"Servizio ""STLC Manager"" arrestato !";
        private string TOK_MSG_STOPPING_SPV_SERVICE = @"Arresto del servizio ""Telefin Supervisor Server"" in corso...";
        private string TOK_MSG_STOPPED_SPV_SERVICE = @"Servizio ""Telefin Supervisor Server"" arrestato !";

        private bool _bSTLCManagerServiceStopped = true;
        protected List<Item> _olItems = null;
        protected Status _oStatus = null;
        protected Config _oConfig = null;

        #endregion

        public ManagerBase()
        {

        }

        public static ManagerBase GetManager(DeviceType deviceType)
        {
            switch (deviceType)
            {
                case DeviceType.AMPLI:
                    return new AmpliManager();
                case DeviceType.PZ:
                    return new PzManager();
                case DeviceType.PZV3:
                    return new PzV3Manager();
                default:
                    return new PzManager();
            }
        }


        #region "Public"

        public bool StartSTLCServices()
        {
            if (_bSTLCManagerServiceStopped)
            {
                try
                {
                    using (ServiceController oServiceControl = new ServiceController(TOK_STLCMANAGERSERVICE))
                    {
                        if (oServiceControl != null)
                        {
                            if (oServiceControl.Status == ServiceControllerStatus.Stopped)
                            {
                                Common.DumpLogMessage(TOK_MSG_STARTING_MGR_SERVICE);

                                oServiceControl.Start();
                                _bSTLCManagerServiceStopped = false;

                                do
                                {
                                    Thread.Sleep(500);
                                    oServiceControl.Refresh();
                                    Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);
                                } while (oServiceControl.Status != ServiceControllerStatus.Running);

                                Common.DumpLogMessage(TOK_MSG_STARTED_MGR_SERVICE);
                                return true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        public bool STLCServiceExists(string serviceName)
        {
            //ServiceController[] servs = ServiceController.GetServices(".");

            //foreach ( ServiceController service in servs )
            //{
            //    if ( serviceName == service.ServiceName )
            //    {
            //        return true;
            //    }
            //}

            ServiceController oServiceControl = new ServiceController(TOK_STLCMANAGERSERVICE);

            try
            {
                string name = oServiceControl.DisplayName;
                return true;
            }
            catch (InvalidOperationException)
            {
                return false;
            }

        }

        public bool StopSTLCServices()
        {
            try
            {
                using (ServiceController oServiceControl = new ServiceController(TOK_STLCMANAGERSERVICE))
                {
                    if (oServiceControl != null)
                    {
                        if (oServiceControl.Status == ServiceControllerStatus.Running)
                        {
                            Common.DumpLogMessage(TOK_MSG_STOPPING_MGR_SERVICE);

                            oServiceControl.Stop();
                            _bSTLCManagerServiceStopped = true;

                            do
                            {
                                Thread.Sleep(500);
                                oServiceControl.Refresh();
                                Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);
                            } while (oServiceControl.Status != ServiceControllerStatus.Stopped);

                            Common.DumpLogMessage(TOK_MSG_STOPPED_MGR_SERVICE);
                        }
                    }
                }

                using (ServiceController oServiceControl = new ServiceController(TOK_SPVSERVERDAEMON))
                {
                    if (oServiceControl != null)
                    {
                        if (oServiceControl.Status == ServiceControllerStatus.Running)
                        {
                            Common.DumpLogMessage(TOK_MSG_STOPPING_SPV_SERVICE);

                            oServiceControl.Stop();

                            do
                            {
                                Thread.Sleep(500);
                                oServiceControl.Refresh();
                                Common.DumpLogInlineMessage(TOK_MSG_WAIT_STEP);
                            } while (oServiceControl.Status != ServiceControllerStatus.Stopped);

                            Common.DumpLogMessage(TOK_MSG_STOPPED_SPV_SERVICE);
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Dump program args
        /// </summary>
        public void DumpProgramArgs()
        {
            StringBuilder sbTemp = new StringBuilder();

            sbTemp.Append("usare: AutoConfigurator DIS 3,9600,N,8,1,F,T");
            sbTemp.Append("\r\n");
            sbTemp.Append("DIS              DIS       Discovery. Visualizza le versioni software per gli address da 0-15");
            sbTemp.Append("\r\n");
            sbTemp.Append("usare: AutoConfigurator AUTO");
            sbTemp.Append("\r\n");
            sbTemp.Append("AUTO             AUTO   AUTO=tutti i dispositivi. PZ=Pannello Zone, AMPLI=Amplificatore, PZV3=Pannello Zone V3");
            sbTemp.Append("\r\n");
            sbTemp.Append("usare: AutoConfigurator R File.cds 04 3,9600,N,8,1,F,T Y PZ");
            sbTemp.Append("\r\n");
            sbTemp.Append("R                R       Legge configurazione da dispositivo e genera file ");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 W       Scrive configurazione da file in EEProm dispositivo");
            sbTemp.Append("\r\n");
            sbTemp.Append("FILE                     Nome del file contenente la configurazione");
            sbTemp.Append("\r\n");
            sbTemp.Append("04               Address Indirizzo del dispositivo ");
            sbTemp.Append("\r\n");
            sbTemp.Append("2,9600,N,8,1,T,T 2       porta seriale");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 9600    baud rate");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 N(E)(O) parit� N (Nessuna), E (Even), O (Odd)");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 8       data");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 1       stop bit 0 (Nessuno), 1 (1 stop bit), 1.5, 2");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 T       T=RS232, F=RS485/RS422");
            sbTemp.Append("\r\n");
            sbTemp.Append("                 T       T=Echo on, F= Echo Off");
            sbTemp.Append("\r\n");
            sbTemp.Append("Y                Y       Y=riavvia servizi, N=non riavviare i servizi");
            sbTemp.Append("\r\n");
            sbTemp.Append("PZ               PZ      PZ=Pannello Zone, AMPLI=Amplificatore, PZV3=Pannello Zone V3");
            sbTemp.Append("\r\n");

            Common.DumpLogMessage(sbTemp.ToString(), false);
        }

        /// <summary>
        /// Main function
        /// </summary>
        /// 
        public abstract void DoAutoConfig();

        public abstract void DoReadConfig(string sAddress, string sFile, string sComSettings);

        public abstract void DoWriteConfig(string sAddress, string sFile, string sComSettings);

        /// <summary>
        /// Load xml config file
        /// </summary>
        /// <returns>true = Ok false = error</returns>
        public virtual bool LoadConfig()
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_LOAD_CONFIG);

                string sXmlFileName = AppCfg.AppCfg.Default.sXMLSystem;
                if (File.Exists(sXmlFileName) == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_XML_NOT_EXIST);
                    return (false);
                }

                XmlDocument oDoc = new XmlDocument();

                oDoc.Load(sXmlFileName);

                XmlNodeList oNodeList = oDoc.SelectNodes(TOK_XPATH_DEVICES_QUERY);

                if (oNodeList == null || oNodeList.Count == 0)
                {
                    Common.DumpLogMessage(TOK_ERROR_XML_DEVICE_NOT_PRESENT);
                    return (false);
                }

                XmlElement oPortEle = null;
                XmlElement oEle = null;
                Item oItem = null;
                _olItems = new List<Item>();

                foreach (XmlNode oNode in oNodeList)
                {
                    oItem = CreateItem();

                    oEle = (XmlElement)oNode;

                    oItem.Name = oEle.Attributes["name"].Value.ToString();
                    oItem.SerialNumber = oEle.Attributes["SN"].Value.ToString();
                    oItem.IDSerialPort = XmlConvert.ToInt32(oEle.Attributes["port"].Value);
                    oItem.Address = oEle.Attributes["addr"].Value.ToString();
                    oItem.NodeName = oEle.ParentNode.Attributes["name"].Value.ToString();

                    oPortEle = (XmlElement)oDoc.SelectSingleNode(TOK_XPATH_PORT_QUERY.Replace("@PORT_NUMBER", oItem.IDSerialPort.ToString()));
                    if (oPortEle == null)
                    {
                        Common.DumpLogMessage(TOK_ERROR_XML_PORT_NOT_PRESENT.Replace("@PORT_NUMBER", oItem.IDSerialPort.ToString()));
                        return (false);
                    }

                    oItem.IDPort = XmlConvert.ToInt32(oPortEle.Attributes["id"].Value);
                    oItem.PortName = oPortEle.Attributes["name"].Value;
                    oItem.PortType = oPortEle.Attributes["type"].Value;
                    oItem.ComID = XmlConvert.ToInt32(oPortEle.Attributes["com"].Value);
                    oItem.ComBaud = XmlConvert.ToInt32(oPortEle.Attributes["baud"].Value);
                    oItem.ComEcho = XmlConvert.ToBoolean(oPortEle.Attributes["echo"].Value);
                    oItem.ComDataLen = XmlConvert.ToInt32(oPortEle.Attributes["data"].Value);
                    oItem.ComDataStop = XmlConvert.ToInt32(oPortEle.Attributes["stop"].Value);
                    oItem.ComParity = oPortEle.Attributes["parity"].Value;
                    oItem.ComTimeout = XmlConvert.ToInt32(oPortEle.Attributes["timeout"].Value);

                    _olItems.Add(oItem);

                    StringBuilder oBuilder = new StringBuilder();
                    oBuilder.Append(TOK_MSG_ITEM_NAME);
                    oBuilder.Append(oItem.Name);
                    oBuilder.Append(TOK_MSG_NODE_NAME);
                    oBuilder.Append(oItem.NodeName);

                    Common.DumpLogMessage(oBuilder.ToString());
                }

                return (true);
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif
                Common.DumpLogMessage(sbError.ToString());

                return (false);
            }
        }

        public void Discovery(string sComSettings)
        {
            try
            {
                Item oItem = CreateItem();                
                string[] sComArg = sComSettings.Split(',');

                oItem.SerialWrap = new SerialWrapper();
                oItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                               Convert.ToInt32(sComArg[1]),
                                               sComArg[2],
                                               Convert.ToInt32(sComArg[3]),
                                               Convert.ToInt32(sComArg[4]) - 1,
                                               1000,
                                               sComArg[5] == "T" ? true : false,
                                               sComArg[6] == "T" ? true : false);

                oItem.SerialWrap.OpenComPort();

                for (int i = 0; i <= 15; i++)
                {
                    oItem.Address = i.ToString().PadLeft(2, '0');

                    try
                    {
                        if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_SW_VERSION, oItem.Address, false))
                        {
                            bool bIsEnd = false;
                            if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd))
                            {
                                if (bIsEnd)
                                {
                                    string sVersion = Common.Byte2String(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                                    Common.DumpLogMessage(oItem.Address + ": " + sVersion);
                                }
                            }
                        }
                        else
                        {
                            Common.DumpLogMessage(oItem.Address + ": Comando non disponibile");
                            Common.DumpLogMessage('\t' + Common.Byte2HexString(oItem.SerialWrap.TempBuffer, 7));
                        }
                    }
                    catch
                    { 
                    
                    }
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }


        public void Restore()
        {
            try
            {
                foreach (Item oItem in _olItems)
                {
                    bool isRS232 = oItem.PortType == TOK_RS232_TYPE;
                    string sComSettings = string.Format("{0},{1},{2},{3},{4},{5},{6}", oItem.ComID, 
                                                                    oItem.ComBaud, 
                                                                    oItem.ComParity,
                                                                    oItem.ComDataLen, 
                                                                    oItem.ComDataStop, 
                                                                    isRS232 ? "T" : "F",
                                                                    oItem.ComEcho ? "T" : "F");

                    DoWriteConfig(oItem.Address, String.Format(configBackupFilename, oItem.ComID, oItem.Address), sComSettings);
                }
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }


        /// <summary>
        /// dump device config to file
        /// </summary>
        /// <param name="sAddress">Address device</param>
        /// <param name="sFile">Output file</param>
        /// <param name="sComSettings">Com settings</param>
        protected virtual bool DoReadConfigInternal(string sAddress, string sFile, string sComSettings)
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_START_SAVE_CONFIG);

                string[] sComArg = sComSettings.Split(',');

                Item oItem = CreateItem();
                _oConfig = CreateConfig();

                oItem.Address = sAddress;
                oItem.SerialWrap = new SerialWrapper();
                oItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                               Convert.ToInt32(sComArg[1]),
                                               sComArg[2],
                                               Convert.ToInt32(sComArg[3]),
                                               Convert.ToInt32(sComArg[4]) - 1,
                                               1000,
                                               sComArg[5] == "T" ? true : false,
                                               sComArg[6] == "T" ? true : false);

                oItem.SerialWrap.OpenComPort();

                // Richiesta versione SW -----------
                bool bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_SW_VERSION, oItem.Address))
                {
                    bool bIsEnd = false;
                    if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd))
                    {
                        bRet = true;

                        string sVersion = Common.Byte2String(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);

                        Common.DumpLogMessage(TOK_MSG_SW_VERSION + sVersion);

                        if (!SW_VERSIONS.Contains(sVersion))
                        {
                            bRet = false;
                            Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION_ERROR);
                        }


                    }
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION);
                    return false;
                }


                // Richiesta Stato -------------
                bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oItem.Address))
                {
                    bool bIsEnd = false;

                    do
                    {
                        if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;

                    } while (bIsEnd == false);
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_STATUS);
                    return false;
                }
                else
                {
                    _oStatus = CreateStatus();
                    _oStatus.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                    _oStatus.DumpStatus();
                }

                // Prelievo Configurazione ------------------
                bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_CONFIG, oItem.Address))
                {
                    bool bIsEnd = false;

                    do
                    {
                        if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (bIsEnd == false);
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_READ_CONFIGURATION);                   
                    oItem.SerialWrap.Close();
                    return false;
                }
                else
                {
                    _oConfig.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                    _oConfig.Save2File(sFile);
                }

                oItem.SerialWrap.Close();

                Common.DumpLogMessage(TOK_MSG_END_SAVE_CONFIG);

                return true;
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        /// <summary>
        /// write config from file to device
        /// </summary>
        /// <param name="sFile">Input file</param>
        /// <param name="sComSettings">Com settings</param>
        public virtual bool DoWriteConfigInternal(string sAddress, string sFile, string sComSettings)
        {
            try
            {
                Common.DumpLogMessage(TOK_MSG_START_WRITE_CONFIG);

                string[] sComArg = sComSettings.Split(',');

                Item oItem = CreateItem();
                _oConfig = CreateConfig();

                if (!File.Exists(sFile)) sFile = Common.GetProgramPath() + Path.DirectorySeparatorChar + sFile;
                _oConfig.LoadFromfile(sFile);
                _oConfig.Serialize2Stream();

                oItem.Address = sAddress;
                oItem.SerialWrap = new SerialWrapper();
                oItem.SerialWrap.InitComPort(Convert.ToInt32(sComArg[0]),
                                               Convert.ToInt32(sComArg[1]),
                                               sComArg[2],
                                               Convert.ToInt32(sComArg[3]),
                                               Convert.ToInt32(sComArg[4]),
                                               1000,
                                               sComArg[5] == "T" ? true : false,
                                               sComArg[6] == "T" ? true : false);

                oItem.SerialWrap.OpenComPort();

                bool bRet = false;
                if (oItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oItem.Address))
                {
                    bool bIsEnd = false;
                    Byte[] btBlock = null;
                    int nBlock = 0;
                    int nLen = 0;
                    int nOffset = 0;
                    Byte[] btSerialize = _oConfig.SerializeBuffer;
                    decimal nBlockTot = Math.Ceiling((decimal)btSerialize.Length / (decimal)TOK_MAX_BLOCK_SIZE);

                    do
                    {
                        nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                        nLen = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                        btBlock = new Byte[nLen];

                        for (int nIdx = 0; nIdx < nLen; nIdx++)
                            btBlock[nIdx] = btSerialize[nOffset + nIdx];

                        bIsEnd = nBlock == nBlockTot - 1; //TOK_MAX_BLOCK_SIZE ? false : true;

                        if (oItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                        {
                            bRet = false;
                            break;
                        }
                        else
                            bRet = true;
                    } while (nBlock < nBlockTot);
                }

                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                    oItem.SerialWrap.Close();
                    return false;
                }

                bRet = oItem.SerialWrap.SendCommand(TOK_CMD_RESET_FLAG_CONFIG, oItem.Address);
                oItem.SerialWrap.CloseSentCommand();

                if (bRet == false)
                {
                    Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                    return false;
                }

                oItem.SerialWrap.Close();

                Common.DumpLogMessage(TOK_MSG_END_WRITE_CONFIG);

                return true;
            }
            catch (Exception e)
            {
                StringBuilder sbError = new StringBuilder();

                sbError.Append(Common.TOK_ERROR_PREFIX);
                sbError.Append(GetType().Name + "." + MethodInfo.GetCurrentMethod().Name);
                sbError.Append(Common.TOK_ERROR_SUFFIX);
#if DEBUG
                sbError.Append(e.ToString());
#else
                    sbError.Append(e.Message);
#endif

                Common.DumpLogMessage(sbError.ToString());

                throw e;
            }
        }

        #endregion

        #region "Protected Method"

        protected abstract Item CreateItem();
        protected abstract Status CreateStatus();
        protected abstract Config CreateConfig();

        protected virtual bool StepInitItem(Item oItem)
        {
            StringBuilder oBuilder = new StringBuilder();
            oItem.SerialWrap = new SerialWrapper();
            oItem.SerialWrap.InitComPort(oItem.ComID,
                                           oItem.ComBaud,
                                           oItem.ComParity,
                                           oItem.ComDataLen,
                                           oItem.ComDataStop,
                                           oItem.ComTimeout,
                                           oItem.PortType == TOK_RS232_TYPE,
                                           oItem.ComEcho);

            oBuilder = new StringBuilder();
            oBuilder.Append(TOK_MSG_START_AUTOCONF);
            oBuilder.Append(oItem.Name);
            oBuilder.Append(TOK_MSG_NODE_NAME);
            oBuilder.Append(oItem.NodeName);
            Common.DumpLogMessage(oBuilder.ToString());

            return true;
        }

        protected virtual bool StepStart()
        {
            // Commentato issue 11702 "Modifica a AutoConfigurator"
            //if (this.STLCServiceExists(TOK_STLCMANAGERSERVICE) && this.STLCServiceExists(TOK_SPVSERVERDAEMON) && !this.StopSTLCServices())
            //{
            //    Common.DumpLogMessage(TOK_ERROR_SERVICE_STOP_TEST);
            //    return false;
            //}
            return true;
        }

        protected virtual bool StepOpenComPort(Item oItem)
        {
            oItem.SerialWrap.OpenComPort();
            return true;
        }

        protected virtual bool StepReadSoftwareVersion(Item oItem)
        {
            bool bRet = false;
            if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_SW_VERSION, oItem.Address))
            {
                bool bIsEnd = false;
                if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd))
                {
                    bRet = true;
                    string sVersion = Common.Byte2String(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);

                    Common.DumpLogMessage(TOK_MSG_SW_VERSION + sVersion);

                    if (!SW_VERSIONS.Contains(sVersion))
                    {
                        bRet = false;
                        Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION_ERROR);
                    }

                }
            }

            oItem.SerialWrap.CloseSentCommand();

            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_READ_SW_VERSION);
                return false;
            }
            else
                return true;
        }

        protected virtual bool StepReadStatus(Item oItem)
        {
            bool bRet = false;
            if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_STATUS, oItem.Address))
            {
                bool bIsEnd = false;

                do
                {
                    if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                    {
                        bRet = false;
                        break;
                    }
                    else
                        bRet = true;
                } while (bIsEnd == false);
            }

            oItem.SerialWrap.CloseSentCommand();

            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_READ_STATUS);
                return false;
            }
            else
            {
                _oStatus = CreateStatus();
                _oStatus.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                _oStatus.DumpStatus();
                return true;

            }
        }

        protected virtual bool StepReadConfiguration(Item oItem)
        {
            bool bRet = false;
            if (oItem.SerialWrap.SendCommand(TOK_CMD_ASK_CONFIG, oItem.Address))
            {
                bool bIsEnd = false;

                do
                {
                    if (oItem.SerialWrap.ReadCommandBuffer(oItem.Address, ref bIsEnd) == false)
                    {
                        bRet = false;
                        break;
                    }
                    else
                        bRet = true;
                } while (bIsEnd == false);
            }

            oItem.SerialWrap.CloseSentCommand();

            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_READ_CONFIGURATION);
                return false;
            }
            else
            {
                _oConfig = CreateConfig();
                _oConfig.LoadFromStream(oItem.SerialWrap.ReadBuffer, oItem.SerialWrap.ReadBufferLen);
                _oConfig.Save2File(string.Format(configBackupFilename, oItem.ComID, oItem.Address));
                return true;
            }
        }

        protected virtual bool StepWriteEEPromConfiguration(Item oItem)
        {
            bool bRet = false;

            _oConfig.Serialize2Stream();

            if (oItem.SerialWrap.SendCommand(TOK_CMD_WRITE_EEPROM_CONFIG, oItem.Address))
            {
                bool bIsEnd = false;
                Byte[] btBlock = null;
                int nBlock = 0;
                int nLen = 0;
                int nOffset = 0;
                Byte[] btSerialize = _oConfig.SerializeBuffer;
                decimal nBlockTot = Math.Ceiling((decimal)btSerialize.Length / (decimal)TOK_MAX_BLOCK_SIZE);

                do
                {
                    nOffset = nBlock * TOK_MAX_BLOCK_SIZE;
                    nLen = (nOffset + TOK_MAX_BLOCK_SIZE) < btSerialize.Length ? TOK_MAX_BLOCK_SIZE : btSerialize.Length - (nBlock * TOK_MAX_BLOCK_SIZE);
                    btBlock = new Byte[nLen];

                    for (int nIdx = 0; nIdx < nLen; nIdx++)
                        btBlock[nIdx] = btSerialize[nOffset + nIdx];

                    bIsEnd = nBlock == nBlockTot - 1; //TOK_MAX_BLOCK_SIZE ? false : true;

                    if (oItem.SerialWrap.SendBufferBlock(btBlock, nBlock++, bIsEnd) == false)
                    {
                        bRet = false;
                        break;
                    }
                    else
                        bRet = true;
                } while (nBlock < nBlockTot);
            }

            oItem.SerialWrap.CloseSentCommand();

            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                return false;
            }
            else
                return true;
        }

        protected virtual bool StepResetFlagConfigurazione(Item oItem)
        {
            bool bRet = oItem.SerialWrap.SendCommand(TOK_CMD_RESET_FLAG_CONFIG, oItem.Address);
            oItem.SerialWrap.CloseSentCommand();

            bool retValue = false;
            if (bRet == false)
            {
                Common.DumpLogMessage(TOK_ERROR_WRITE_CONFIGURATION);
                retValue = false;
            }
            else
                retValue = true;

            Common.DumpLogMessage(TOK_MSG_WRITE_END_CONFIG);

            return retValue;
        }

        protected virtual bool StepCloseComPort(Item oItem)
        {
            oItem.SerialWrap.Close();

            return true;
        }

        protected virtual bool StepEnd(Item oItem)
        {
            if (_oConfig != null)
            {
                _oConfig.Save2File(string.Format(configFilename, oItem.ComID, oItem.Address));
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append(TOK_MSG_END_AUTOCONF);
                oBuilder.Append(oItem.Name);
                oBuilder.Append(TOK_MSG_NODE_NAME);
                oBuilder.Append(oItem.NodeName);
                Common.DumpLogMessage(oBuilder.ToString());
            }

            // Commentato issue 11702 "Modifica a AutoConfigurator"
            //if (this.STLCServiceExists(TOK_STLCMANAGERSERVICE) && this.STLCServiceExists(TOK_SPVSERVERDAEMON) && !this.StartSTLCServices())
            //{
            //    Common.DumpLogMessage(TOK_ERROR_SERVICE_START_TEST);
            //}

            return true;
        }

        #endregion

    }
}

﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AutoConfigurator")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Telefin")]
[assembly: AssemblyProduct("AutoConfigurator")]
[assembly: AssemblyCopyright("Copyright © Telefin 2007 - 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1cb76332-c5d3-4e02-a926-1f4b652d0f10")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//      Revision 9 meaning DEBUG VERSION of any other revision (9 excluded)
[assembly: AssemblyVersion("1.0.5.2")]
[assembly: AssemblyFileVersion("1.0.5.2")]

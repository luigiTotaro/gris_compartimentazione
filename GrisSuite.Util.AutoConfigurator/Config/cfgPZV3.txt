[Telefin PDS Config File]
Version=PDS Config 2.00

[General Config]
StartNightTime=22.00
EndNightTime=06.00
AttenuationAmpli1=0
AttenuationAmpli2=0
TestStartTime=00.00
TestEndTime=23.59
TestPeriod=30
ZonesToTest=1,2,7,8
ZonesToDisabled=0
TestLevelAmpli1=128
TestLevelAmpli2=128
AmplifiersMode=1
TensionsRangeAmpli1=10
TensionsRangeAmpli2=10
ResistanceRange1=50
ResistanceRange2=50
ResistanceRange3=50
ResistanceRange4=50
ResistanceRange5=50
ResistanceRange6=50
ResistanceRange7=50
ResistanceRange8=50
ResistanceRange9=50
ResistanceRange10=50
ResistanceRange11=50
ResistanceRange12=50
TestTimeout=5
AttenuationTensionsRange1=1286
AttenuationTensionsRange2=5376

[MicroInputs1]
Priority=0
Equalization=128

[MicroInputs2]
Priority=0
Equalization=128

[MicroInputs3]
Priority=0
Equalization=128

[Input1]
MicroId=1
LevelAmpi1=0
LevelAmpi2=127
Zones=1,2

[Input2]
MicroId=2
LevelAmpi1=127
LevelAmpi2=0
Zones=7,8

[Input3]
MicroId=3
LevelAmpi1=0
LevelAmpi2=0
Zones=1,2,7,8

[Input4]
MicroId=1
LevelAmpi1=127
LevelAmpi2=0
Zones=8

[Input5]
MicroId=2
LevelAmpi1=0
LevelAmpi2=127
Zones=2

[Input6]
MicroId=3
LevelAmpi1=0
LevelAmpi2=0
Zones=1,7

[Input7]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input8]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input9]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input10]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input11]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input12]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input13]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input14]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input15]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[Input16]
MicroId=1
LevelAmpi1=127
LevelAmpi2=127
Zones=0

[PrgBlock1]
Param1=10
Param2=81
Param3=53
Param4=0
Param5=0
Param6=0

[PrgBlock2]
Param1=10
Param2=86
Param3=6
Param4=0
Param5=0
Param6=0

[PrgBlock3]
Param1=10
Param2=87
Param3=7
Param4=0
Param5=0
Param6=0

[PrgBlock4]
Param1=10
Param2=85
Param3=5
Param4=0
Param5=0
Param6=0

[PrgBlock5]
Param1=10
Param2=82
Param3=1
Param4=0
Param5=0
Param6=0

[PrgBlock6]
Param1=10
Param2=83
Param3=2
Param4=0
Param5=0
Param6=0

[PrgBlock7]
Param1=10
Param2=84
Param3=3
Param4=0
Param5=0
Param6=0

[PrgBlock8]
Param1=10
Param2=66
Param3=1
Param4=0
Param5=0
Param6=0

[PrgBlock9]
Param1=10
Param2=67
Param3=2
Param4=0
Param5=0
Param6=0

[PrgBlock10]
Param1=10
Param2=68
Param3=3
Param4=0
Param5=0
Param6=0

[PrgBlock11]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock12]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock13]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock14]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock15]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock16]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock17]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock18]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock19]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock20]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock21]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock22]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock23]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock24]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock25]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock26]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock27]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock28]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock29]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock30]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock31]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock32]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock33]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock34]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock35]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock36]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock37]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock38]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock39]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock40]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock41]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock42]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock43]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock44]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock45]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock46]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock47]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0

[PrgBlock48]
Param1=0
Param2=0
Param3=0
Param4=0
Param5=0
Param6=0


// #define DEBUG_LOG

using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using System.Security;
using System.Web;
using System.Xml;
using GrisSuite.Properties;
using GrisSuite.AppCfg;
using Microsoft.Win32;

namespace GrisSuite
{
    public class LocalUtil
    {
        private static readonly string _logSource = AppDomain.CurrentDomain.FriendlyName;

        public static void WriteDebugLogToFile(string text)
        {
            try
            {
                if (Settings.Default.EnableDebugLog)
                {
                    string sfile = GetFileName();
                    using (TextWriter wr = new StreamWriter(sfile, true))
                    {
                        wr.WriteLine("{0}: {1}<br/>", DateTime.Now, HtmlNewLine(text));
                        wr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(_logSource, "Error on write debug log:" + ex.Message + Environment.NewLine + ex.StackTrace,
                    EventLogEntryType.Warning);
            }
        }

        public static void WriteLog(DataRow rowOld, DataRow rowNew)
        {
            try
            {
                if (Settings.Default.EnableDebugLog)
                {
                    string sfile = GetFileName();
                    using (TextWriter wr = new StreamWriter(sfile, true))
                    {
                        DataTable dt = (rowOld != null ? rowOld.Table : rowNew.Table);

                        wr.WriteLine("<TABLE BORDER=1><TR><TD>&nbsp;</TD>");
                        foreach (DataColumn col in dt.Columns)
                        {
                            wr.Write("<TD>{0}</TD>", col.ColumnName);
                        }

                        if (rowOld != null)
                        {
                            wr.WriteLine("</TR><TR><TD>Old</TD>");
                            foreach (DataColumn col in dt.Columns)
                            {
                                if (rowOld[col.ColumnName] == null || rowOld[col.ColumnName] == DBNull.Value)
                                {
                                    wr.Write("<TD>(null)</TD>");
                                }
                                else
                                {
                                    wr.Write("<TD>{0}</TD>", rowOld[col.ColumnName]);
                                }
                            }
                        }
                        if (rowNew != null)
                        {
                            wr.WriteLine("</TR><TR><TD>New</TD>");
                            foreach (DataColumn col in dt.Columns)
                            {
                                if (rowNew[col.ColumnName] == null || rowNew[col.ColumnName] == DBNull.Value)
                                {
                                    wr.Write("<TD>(null)</TD>");
                                }
                                else
                                {
                                    wr.Write("<TD>{0}</TD>", rowNew[col.ColumnName]);
                                }
                            }
                        }

                        wr.WriteLine("</TR></TABLE>");

                        wr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(_logSource, "Error on write debug log:" + ex.Message + Environment.NewLine + ex.StackTrace,
                    EventLogEntryType.Warning);
            }
        }

        public static string HtmlNewLine(string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                return HttpUtility.HtmlEncode(input).Replace("\n", "<br/>");
            }
            return string.Empty;
        }

        internal static string GetFullHostName()
        {
            string domainName = "";
            string fullhostName;
            string hostname = Environment.MachineName;

            using (ManagementObject cs = new ManagementObject("Win32_ComputerSystem.Name='" + hostname + "'"))
            {
                try
                {
                    cs.Get();

                    if (cs["partOfDomain"] != null && (bool) cs["partOfDomain"])
                    {
                        domainName = cs["domain"].ToString();
                    }
                }
                catch (ManagementException)
                {
                    domainName = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters", "Domain", "").ToString();
                }
            }

            if (domainName == "")
            {
                fullhostName = hostname;
            }
            else
            {
                fullhostName = string.Format("{0}.{1}", hostname, domainName);
            }

            return fullhostName;
        }

        private static string GetFileName()
        {
            string sfile;

            try
            {
                sfile = Path.Combine(GrisSuite.AppCfg.AppCfg.Default.sPathLogFolder, AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".log.htm"));
            }
            catch
            {
                return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".log.htm"));
            }

            if (File.Exists(sfile))
            {
                FileInfo f = new FileInfo(sfile);
                long debugLogMaxSize = Settings.Default.DebugLogMaxSize;
                if (f.Length > debugLogMaxSize && debugLogMaxSize > 0)
                {
                    string sfileOld = sfile.Replace(".log.htm", "_old.log.htm");

                    if (File.Exists(sfileOld))
                    {
                        File.Delete(sfileOld);
                    }

                    f.MoveTo(sfileOld);
                }
            }

            return sfile;
        }

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimeMillis()
        {
            return (long) (DateTime.UtcNow - epoch).TotalSeconds;
        }

        public static DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddSeconds(unixTime).ToLocalTime();
        }

        private const string HOST_XML_NODE = "//telefin/system/server";
        private const string TELEFIN_SYSTEM_SERVER_SRVID_ATTRIBUTE = "SrvID";
        private const string TELEFIN_SYSTEM_SERVER_HOST_ATTRIBUTE = "host";
        private const string TELEFIN_SYSTEM_SERVER_NAME_ATTRIBUTE = "name";
        private const string TELEFIN_SYSTEM_SERVER_TYPE_ATTRIBUTE = "type";
        private const string REGION_XML_NODE = "//telefin/system/server/region";
        private const string TELEFIN_SYSTEM_SERVER_REGID_ATTRIBUTE = "RegID";
        private const string ZONE_XML_NODE = "//telefin/system/server/region/zone";
        private const string TELEFIN_SYSTEM_SERVER_ZONID_ATTRIBUTE = "ZonID";
        private const string NODE_XML_NODE = "//telefin/system/server/region/zone/node";
        private const string TELEFIN_SYSTEM_SERVER_NODID_ATTRIBUTE = "NodID";

        public static ServerParkingData LoadSystemXmlConfigData()
        {
            long serverId;
            string host;
            string name;
            string type;
            string clientRegId;
            string clientZonId;
            string clientNodId;
            string hardwareProfile;

            ServerParkingData spd;

            string systemXmlFilePath = GrisSuite.AppCfg.AppCfg.Default.sXMLSystem;

            if (File.Exists(systemXmlFilePath))
            {
                // Se esiste il System.xml, cerca di caricare i dati da quello
                GetServerDataFromSystemXml(systemXmlFilePath, out serverId, out host, out name, out type, out clientRegId, out clientZonId,
                    out clientNodId, out hardwareProfile);

                spd = new ServerParkingData(serverId, host, name, type, clientRegId, clientZonId, clientNodId, hardwareProfile, ServerParkingData.ParkingKind.PreviouslyParkedServer);
            }
            else
            {
                // Siamo nel caso di server privo di configurazione, che quindi deve essere segnalato come parcheggiato senza configurazione
                GetSTLCManagerAppSettings(out hardwareProfile, out serverId);
                host = GetComputerName();

                spd = new ServerParkingData((serverId > 0) ? (long?)serverId : null, host, null, null, null, null, null, hardwareProfile, ServerParkingData.ParkingKind.MissingConfigurationServer);
            }

            return spd;
        }

        private static string GetRegistryValue(string keyName, string valueName, string defaultValue)
        {
            // il Registry.GetValue restituisce comunque null se non esiste la keyName
            return (Registry.GetValue(keyName, valueName, null) ?? GetConfigValue(valueName, defaultValue)).ToString();
        }

        private static string GetConfigValue(string configKey, string defaultValue)
        {
            string value = ConfigurationManager.AppSettings[configKey];
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }
            return value;
        }

        private static string GetProgramFilesx86Folder()
        {
            if (IntPtr.Size == 8 || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }

        private static string GetGrisSuiteAppPath()
        {
            // Recupero path da registry, su STLC 1000 e Windows Embedded
            string keyValue = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Telefin\STLC1000", "AppPath", null) as string;
            if (!String.IsNullOrEmpty(keyValue))
            {
                return keyValue.Trim();
            }

            // Recupero path da registry, su pc generico e windows 7 o simile
            keyValue = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Telefin\STLC1000", "AppPath", null) as string;
            if (!String.IsNullOrEmpty(keyValue))
            {
                return keyValue.Trim();
            }

            // Ritorniamo un valore standard, legato all'architettura del PC
            return Path.Combine(GetProgramFilesx86Folder(), "GrisSuite");
        }

        private static void GetSTLCManagerAppSettings(out string hardwareProfile, out long serverId)
        {
            string grisSuiteAppPath = GetGrisSuiteAppPath();
            hardwareProfile = null;
            serverId = 0;

#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("GetSTLCManagerHardwareProfileValue - grisSuiteAppPath: {0}", grisSuiteAppPath));
#endif

            if (!String.IsNullOrEmpty(grisSuiteAppPath))
            {
                string stlcManagerConfigPath = Path.Combine(grisSuiteAppPath, @"STLCManager\STLCManagerService.exe.config");

                if (!String.IsNullOrEmpty(stlcManagerConfigPath))
                {
                    if (File.Exists(stlcManagerConfigPath))
                    {
#if DEBUG_LOG
                        LocalUtil.WriteDebugLogToFile(String.Format("GetSTLCManagerHardwareProfileValue - stlcManagerConfigPath: {0}", stlcManagerConfigPath));
#endif

                        XmlDocument stlcManagerConfigXml = new XmlDocument();
                        stlcManagerConfigXml.Load(stlcManagerConfigPath);

                        XmlNode appSettingsNode = stlcManagerConfigXml.SelectSingleNode("//configuration/appSettings");
                        XmlElement appSettingsElement = (XmlElement) appSettingsNode;
                        if (appSettingsElement != null)
                        {
                            foreach (XmlElement appSetting in appSettingsElement.ChildNodes)
                            {
                                if (appSetting.GetAttribute("key").Equals("HardwareProfile", StringComparison.OrdinalIgnoreCase))
                                {
#if DEBUG_LOG
                                    LocalUtil.WriteDebugLogToFile(String.Format("GetSTLCManagerAppSettings - hardwareProfile from //configuration/appSettings: {0}", appSetting.GetAttribute("value")));
#endif

                                    hardwareProfile = appSetting.GetAttribute("value");
                                }

                                if (appSetting.GetAttribute("key").Equals("ForcedServerID", StringComparison.OrdinalIgnoreCase))
                                {
#if DEBUG_LOG
                                    LocalUtil.WriteDebugLogToFile(String.Format("GetSTLCManagerAppSettings - ForcedServerID from //configuration/appSettings: {0}", appSetting.GetAttribute("value")));
#endif
                                    if (!String.IsNullOrEmpty(appSetting.GetAttribute("value")))
                                    {
                                        long.TryParse(appSetting.GetAttribute("value"), out serverId);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (String.IsNullOrEmpty(hardwareProfile))
            {
                hardwareProfile = GetConfigValue("HardwareProfile", "STLC");

#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("GetSTLCManagerHardwareProfileValue - hardwareProfile from local config: {0}", hardwareProfile));
#endif
            }

            if (String.IsNullOrEmpty(hardwareProfile))
            {
                hardwareProfile = "STLC";

#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("GetSTLCManagerHardwareProfileValue - hardwareProfile default value: {0}", hardwareProfile));
#endif
            }
        }

        private static void GetServerDataFromSystemXml(string systemXmlFilePath,
            out long serverId,
            out string host,
            out string name,
            out string type,
            out string clientRegId,
            out string clientZonId,
            out string clientNodId,
            out string hardwareProfile)
        {
            serverId = 0;
            host = "N/D";
            name = "N/D";
            type = "N/D";
            clientRegId = "N/D";
            clientZonId = "N/D";
            clientNodId = "N/D";
            hardwareProfile = "N/D";

            if (File.Exists(systemXmlFilePath))
            {
                XmlDocument sysXml = new XmlDocument();
                sysXml.Load(systemXmlFilePath);

                XmlNode serverNode = sysXml.SelectSingleNode(HOST_XML_NODE);
                XmlElement serverElement = (XmlElement) serverNode;
                if (serverElement != null)
                {
                    long.TryParse(serverElement.GetAttribute(TELEFIN_SYSTEM_SERVER_SRVID_ATTRIBUTE), out serverId);
                    host = serverElement.GetAttribute(TELEFIN_SYSTEM_SERVER_HOST_ATTRIBUTE);
                    name = serverElement.GetAttribute(TELEFIN_SYSTEM_SERVER_NAME_ATTRIBUTE);
                    type = serverElement.GetAttribute(TELEFIN_SYSTEM_SERVER_TYPE_ATTRIBUTE);
                }

                XmlNode regionNode = sysXml.SelectSingleNode(REGION_XML_NODE);
                XmlElement regionElement = (XmlElement) regionNode;
                if (regionElement != null)
                {
                    clientRegId = regionElement.GetAttribute(TELEFIN_SYSTEM_SERVER_REGID_ATTRIBUTE);
                }

                XmlNode zoneNode = sysXml.SelectSingleNode(ZONE_XML_NODE);
                XmlElement zoneElement = (XmlElement) zoneNode;
                if (zoneElement != null)
                {
                    clientZonId = zoneElement.GetAttribute(TELEFIN_SYSTEM_SERVER_ZONID_ATTRIBUTE);
                }

                XmlNode nodeNode = sysXml.SelectSingleNode(NODE_XML_NODE);
                XmlElement nodeElement = (XmlElement) nodeNode;
                if (nodeElement != null)
                {
                    clientNodId = nodeElement.GetAttribute(TELEFIN_SYSTEM_SERVER_NODID_ATTRIBUTE);
                }

                hardwareProfile = GetHardwareProfileStringFromConfig();
            }
        }

        internal static HardwareProfile GetHardwareProfileFromConfig()
        {
            string hardwareProfile = GetHardwareProfileStringFromConfig();

            if (String.IsNullOrEmpty(hardwareProfile))
            {
                return HardwareProfile.STLC;
            }

            // Dato che pu� essere associato al profilo hardware anche un sottotipo, dopo il punto, eliminiamo la parte rimanente, se esiste
            // Ad esempio:
            // 100332068.01 > computer industriale di Almaviva senza porte seriali
            // 100332068.02 > computer industriale di Almaviva con porte seriali

            if (hardwareProfile.IndexOf('.') > 0)
            {
                hardwareProfile = hardwareProfile.Substring(0, hardwareProfile.IndexOf('.'));
            }

            switch (hardwareProfile.ToLowerInvariant())
            {
                case "stlc":
                    return HardwareProfile.STLC;
                case "generic":
                    return HardwareProfile.Generic;
                case "100332068":
                    return HardwareProfile.Almaviva;
                case "100332045":
                    return HardwareProfile.ASTS;
                default:
                    return HardwareProfile.Generic;
            }
        }

        internal static string GetHardwareProfileStringFromConfig()
        {
            return GrisSuite.AppCfg.AppCfg.Default.sHardwareProfile;
        }

        /// <summary>
        ///     Ritorna un booleano che indica, in base al server id, se la macchina su cui giriamo � un STLC o meno
        /// </summary>
        /// <param name="serverId">ServerID della macchina</param>
        /// <param name="hardwareProfile">Profilo hardware corrente</param>
        /// <returns>Booleano che indica se siamo su STLC o meno</returns>
        public static bool IsOnServerSTLC(long serverId, HardwareProfile hardwareProfile)
        {
#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("IsOnServerSTLC - serverId: {0}, hardwareProfile: {1}", serverId.ToString(), Enum.GetName(typeof(HardwareProfile), hardwareProfile)));
#endif

            if (serverId <= 0)
            {
                return false;
            }

            if (hardwareProfile == HardwareProfile.STLC)
            {
                uint encodedSerialNumber = (uint) serverId;
                // ReSharper disable once UnusedVariable
                byte byte1 = (byte) ((encodedSerialNumber & 0xFF000000) >> 24);
                // ReSharper disable once UnusedVariable
                byte byte2 = (byte) ((encodedSerialNumber & 0x00FF0000) >> 16);
                // Il calcolo originale shifta di 15 bit, quindi per questa word, dobbiamo eliminare il primo bit
                // Lo facciamo con maschera a 01111111 11111111 binaria (0x7FFF)
                ushort short3 = (ushort) ((encodedSerialNumber & 0x0000FFFF) & 0x7FF);

#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("IsOnServerSTLC - Decoded Serial Number: {0}.{1}.{2}", byte1.ToString("XX"), byte2.ToString("XX"), short3.ToString("XXXX")));
#endif

                // Il server ID � codificato secondo la seguente regola, nell'STLCManager
                // - Se � un STLC, il primo byte indica l'anno di produzione, il secondo la settimana, il terzo il progressivo
                // - Se non � un STLC, il primo byte � il terzultimo byte del MAC Address della prima scheda di rete, il secondo il penultimo
                // e il terzo l'ultimo byte del MAC address, aumentato di 1024 (0x400)
                // Questo ultimo valore permette di discriminare un STLC da un altro server
                if (short3 >= 0x400)
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        // Ritorna il nome del computer, cos� come implementato in STLCManager
        public static string GetComputerName()
        {
            string sName = String.Empty;

            try
            {
                sName = Environment.GetEnvironmentVariable("COMPUTERNAME");
            }
            // ReSharper disable once UnusedVariable
            catch (ArgumentNullException ex)
            {
#if DEBUG_LOG
                LocalUtil.WriteDebugLogToFile(String.Format("GetComputerName - Unable to get computer name, Exception: {0}", ex.Message));
#endif
            }
            // ReSharper disable once UnusedVariable
            catch (SecurityException ex)
            {
#if DEBUG_LOG
                LocalUtil.WriteDebugLogToFile(String.Format("GetComputerName - Unable to get computer name, Exception: {0}", ex.Message));
#endif
            }

            return sName;
        }
    }

    /// <summary>
    ///     Indica il tipo di macchina su cui si sta eseguendo l'applicazione
    /// </summary>
    public enum HardwareProfile : short
    {
        /// <summary>
        ///     Apparato STLC1000
        /// </summary>
        STLC,

        /// <summary>
        ///     Diagnostica standalone, che non centralizza in Gris
        /// </summary>
        Generic,

        /// <summary>
        ///     Apparati di Almaviva che centralizzano in Gris
        /// </summary>
        Almaviva,

        /// <summary>
        ///     Apparati di ASTS (Ansaldo) che centralizzano in Gris
        /// </summary>
        ASTS
    }

    public class ServerParkingData
    {
        public long? ServerId { get; private set; }
        public string Host { get; private set; }
        public string Name { get; private set; }
        public string Type { get; private set; }
        public string ClientRegionId { get; private set; }
        public string ClientZoneId { get; private set; }
        public string ClientNodeId { get; private set; }
        public string HardwareProfile { get; private set; }
        public ParkingKind ParkingType { get; private set; }

        public ServerParkingData(long? serverId,
            string host,
            string name,
            string type,
            string clientRegId,
            string clientZonId,
            string clientNodId,
            string hardwareProfile,
            ParkingKind parkingType)
        {
            this.ServerId = serverId;
            this.Host = host;
            this.Name = name;
            this.Type = type;
            this.ClientRegionId = clientRegId;
            this.ClientZoneId = clientZonId;
            this.ClientNodeId = clientNodId;
            this.HardwareProfile = hardwareProfile;
            this.ParkingType = parkingType;
        }

        /// <summary>
        /// Tipi di segnalazione parking
        /// </summary>
        public enum ParkingKind
        {
            /// <summary>
            /// Sconosciuto
            /// </summary>
            Unknown = 0,

            /// <summary>
            /// Server con configurazione, con problemi su database o simili
            /// </summary>
            ConfiguredServer = 1,

            /// <summary>
            /// Server privo di System.xml
            /// </summary>
            MissingConfigurationServer = 2,

            /// <summary>
            /// Server che � stato parcheggiato, mantenuto per storico, ma che adesso centralizza
            /// </summary>
            PreviouslyParkedServer = 3
        }
    }

    internal class SystemClock
    {
        private struct SYSTEMTIME
        {
            // ReSharper disable NotAccessedField.Local
            public ushort wYear, wMonth, wDayOfWeek, wDay, wHour, wMinute, wSecond, wMilliseconds;
            // ReSharper restore NotAccessedField.Local
        }

        [DllImport("kernel32.dll")]
        private static extern uint SetSystemTime(ref SYSTEMTIME lpSystemTime);

        internal static bool Set(DateTime d)
        {
            DateTime utc = d.ToUniversalTime();

            SYSTEMTIME st = new SYSTEMTIME();
            st.wDay = (ushort) utc.Day;
            st.wDayOfWeek = (ushort) utc.DayOfWeek;
            st.wHour = (ushort) utc.Hour;
            st.wMilliseconds = (ushort) utc.Millisecond;
            st.wMinute = (ushort) utc.Minute;
            st.wMonth = (ushort) utc.Month;
            st.wSecond = (ushort) utc.Second;
            st.wYear = (ushort) (utc.Year);
            SetSystemTime(ref st);
            return (SetSystemTime(ref st) != 0);
        }
    }
}
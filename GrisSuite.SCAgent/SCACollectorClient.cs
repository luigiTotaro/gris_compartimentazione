 #define DEBUG_LOG

using System;
using System.Net;
using GrisSuite.Properties;
using GrisSuite.SCACollectorWS;
using System.Web.Services.Protocols;

namespace GrisSuite
{
    internal class SCACollectorClient
    {
        private readonly SCACollector _scc;
        private readonly SCACollector _sccBackup;

        // Nel caso il server sia privo di licenza, usiamo questo flag per loggare una sola volta il blocco
        // della centralizzazione. Nel caso la centralizzazione parta (perch� licenziato) e poi si interrrompa, avremo un nuovo log
        private bool withoutLicenseSignaled;

        // Tiene traccia dello stato della licenza del server
        private bool? isServerLicensed = null;

        internal SCACollectorClient()
        {
            this._scc = new SCACollector();
            this._scc.Credentials = GetCredentials();
            this._sccBackup = new SCACollector();
            this._sccBackup.Credentials = this._scc.Credentials;
            this._sccBackup.Url = Settings.Default.SCACollectorAddressBackup;
        }

        public int SrvId { get; set; }

        private static ICredentials GetCredentials()
        {
            if (string.IsNullOrEmpty(WebServiceAccount.Default.Username))
            {
                return CredentialCache.DefaultCredentials;
            }

            string username = WebServiceAccount.Default.Username;
            string password = WebServiceAccount.Default.Password;
            string domain = WebServiceAccount.Default.Domain;
            if (string.IsNullOrEmpty(domain))
            {
                return new NetworkCredential(username, password);
            }

            return new NetworkCredential(username, password, domain);
        }

        #region SCACollector Method Caller

        internal Guid IsAlive()
        {
            if (this.ShouldCentralizeByLicense(true))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile("IsAlive");

                    return this._scc.IsAliveById(this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: IsAlive with Backup Address. {0}", ex.Message));
                    return this._sccBackup.IsAliveById(this.SrvId);
                }
            }

            return Guid.Empty;
        }

        internal Guid UpdateConfig(byte[] cds, int rowCount)
        {
            if (this.ShouldCentralizeByLicense(true))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("UpdateConfig, {0} rows", rowCount.ToString()));

                    return this._scc.UpdateConfigById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: UpdateConfig with Backup Address. {0}", ex.Message));
                    return this._sccBackup.UpdateConfig(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid ReplaceConfig(byte[] cds, int rowCount)
        {
            if (this.ShouldCentralizeByLicense(true))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("ReplaceConfig, {0} rows", rowCount.ToString()));

                    return this._scc.ReplaceConfigById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: ReplaceConfig with Backup Address. {0}", ex.Message));
                    return this._sccBackup.ReplaceConfig(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid ReplaceSTLCParameters(byte[] cds)
        {
            if (this.ShouldCentralizeByLicense(false))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile("ReplaceSTLCParametersById");

                    return this._scc.ReplaceSTLCParametersById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: ReplaceSTLCParameters with Backup Address. {0}", ex.Message));
                    return this._sccBackup.ReplaceSTLCParameters(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid UpdateStatus(byte[] cds, int rowCount)
        {
            if (this.ShouldCentralizeByLicense(false))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("UpdateStatus, {0} rows", rowCount.ToString()));

                    return this._scc.UpdateStatusById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: UpdateStatus with Backup Address. {0}", ex.Message));
                    return this._sccBackup.UpdateStatus(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid UpdateEvents(byte[] cds, int rowCount)
        {
            if (this.ShouldCentralizeByLicense(false))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("UpdateEvents, {0} rows", rowCount.ToString()));

                    return this._scc.UpdateEventsById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: UpdateEvents with Backup Address. {0}", ex.Message));
                    return this._sccBackup.UpdateEvents(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid UpdateAcks(byte[] cds, int rowCount)
        {
            if (this.ShouldCentralizeByLicense(false))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("UpdateAcks, {0} rows ", rowCount.ToString()));

                    return this._scc.UpdateAcksById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: UpdateAcks with Backup Address. {0}", ex.Message));
                    return this._sccBackup.UpdateAcks(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid ReplaceStatus(byte[] cds, int rowCount)
        {
            if (this.ShouldCentralizeByLicense(false))
            {
                try
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("ReplaceStatus, {0} rows ", rowCount));

                    return this._scc.ReplaceStatusById(cds, this.MACAddress, this.SrvId);
                }
                catch (WebException ex)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("Warning: ReplaceStatus with Backup Address. {0}", ex.Message));
                    return this._sccBackup.ReplaceStatus(cds, this.MACAddress);
                }
            }

            return Guid.Empty;
        }

        internal Guid UpdateParkedServer(ServerParkingData.ParkingKind forcedParkingType)
        {
            // Questo metodo centralizza sempre, a prescindere dallo stato di licenza del server
            ServerParkingData spd = LocalUtil.LoadSystemXmlConfigData();

            if (spd == null)
            {
                LocalUtil.WriteDebugLogToFile(" spd = null ");
                return Guid.Empty;
            }

            ServerParkingData.ParkingKind _settingParkingType = ServerParkingData.ParkingKind.Unknown;

            if (forcedParkingType != ServerParkingData.ParkingKind.Unknown)
            {
                _settingParkingType = forcedParkingType;
            }
            else if (spd.ParkingType != ServerParkingData.ParkingKind.Unknown)
            {
                _settingParkingType = spd.ParkingType;
            }

            try
            {
                LocalUtil.WriteDebugLogToFile(string.Format("UpdateParkedServer using ParkingType: {0} ({1}) , spd.ParkingType: {2} ({3}), forcedParkingType: {4} ({5})",
                        ((byte)_settingParkingType).ToString(), Enum.GetName(typeof(ServerParkingData.ParkingKind), _settingParkingType),
                        ((byte)spd.ParkingType).ToString(), Enum.GetName(typeof(ServerParkingData.ParkingKind), spd.ParkingType),
                        ((byte)forcedParkingType).ToString(), Enum.GetName(typeof(ServerParkingData.ParkingKind), forcedParkingType)));

                LocalUtil.WriteDebugLogToFile(string.Format("ServerId {0}, Host {1}, Name {2}, Type {3}",
                        (this.SrvId != null) ? this.SrvId.ToString() : "null",
                        (spd.Host != null) ? spd.Host : "null",
                        (spd.Name != null) ? spd.Name : "null",
                        (spd.Type != null) ? spd.Type.ToString() : "null"));
                LocalUtil.WriteDebugLogToFile(string.Format("ClientRegionId {0}, ClientZoneId {1}, ClientNodeId {2}, HardwareProfile {3}",
                        (spd.ClientRegionId != null) ? spd.ClientRegionId : "null",
                        (spd.ClientZoneId != null) ? spd.ClientZoneId : "null",
                        (spd.ClientNodeId != null) ? spd.ClientNodeId : "null",
                        (spd.HardwareProfile != null) ? spd.HardwareProfile : "null"));

                Guid guid = this._scc.UpdateParkedServer(
                                                this.SrvId,
                                                spd.Host,
                                                spd.Name,
                                                spd.Type,
                                                spd.ClientRegionId,
                                                spd.ClientZoneId,
                                                spd.ClientNodeId,
                                                spd.HardwareProfile,
                                                (byte)_settingParkingType);

                return guid;
            }

            catch (WebException ex)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("UpdateParkedServer with Backup Address, ParkingType: {0} ({1}). Exception: {2}", ((byte)spd.ParkingType).ToString(), Enum.GetName(typeof(ServerParkingData.ParkingKind), spd.ParkingType), ex.Message));

                return this._sccBackup.UpdateParkedServer(spd.ServerId, spd.Host, spd.Name, spd.Type, spd.ClientRegionId, spd.ClientZoneId,
                                spd.ClientNodeId, spd.HardwareProfile, (byte)spd.ParkingType);
            }
            catch (SoapException exc)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("EXCEPTION (SoapException): {0}", exc.Message));
            }
            catch (InvalidOperationException exc)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("EXCEPTION (InvalidOperationException): {0}", exc.Message));
            }
            catch (Exception exc)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("EXCEPTION (Generic exception): {0}", exc.Message));
            }
            LocalUtil.WriteDebugLogToFile("Unable to load server data to be sent to servers_parking central table.");

            return Guid.Empty;
        }

        /// <summary>
        ///     Ritorna un valore che indica se il server attuale � con licenza o meno.
        ///     Se � un STLC, � di default con licenza, altrimenti verifica lo stato della licenza sul server centrale.
        /// </summary>
        /// <returns></returns>
        internal byte IsServerLicensed()
        {
            string hardwareProfileString = LocalUtil.GetHardwareProfileStringFromConfig();

#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("IsServerLicensed - hardwareProfileString: {0}, isServerLicensed: {1}", hardwareProfileString, (isServerLicensed.HasValue ? isServerLicensed.Value.ToString() : "N/A")));
#endif

            if (LocalUtil.IsOnServerSTLC(this.SrvId, LocalUtil.GetHardwareProfileFromConfig()))
            {
                LocalUtil.WriteDebugLogToFile("IsServerLicensed - Return value: 255 (STLC)");

                return 0xFF;
            }

            try
            {
                byte isLicensed = this._scc.IsServerLicensed(this.SrvId, hardwareProfileString);

                LocalUtil.WriteDebugLogToFile(String.Format("IsServerLicensed - Return value: {0}", isLicensed.ToString()));

                return isLicensed;
            }
            catch (WebException ex)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("Warning: IsServerLicensed with Backup Address. {0}", ex.Message));
                return this._sccBackup.IsServerLicensed(this.SrvId, hardwareProfileString);
            }
        }

        /// <summary>
        ///     Incapsula la logica di test licenza, rendendola booleana
        ///     0xFF indica licenza implicita di STLC
        ///     0x00 indica server privo di licenza, cos� come restituito da server centrale
        ///     0x01 indica server con licenza, cos� come restituito da server centrale
        /// </summary>
        /// <returns></returns>
        internal bool ShouldCentralizeByLicense(bool checkByServer)
        {
#if DEBUG_LOG
            LocalUtil.WriteDebugLogToFile(String.Format("ShouldCentralizeByLicense - checkByServer: {0}, isServerLicensed: {1}", checkByServer.ToString(), (isServerLicensed.HasValue ? isServerLicensed.Value.ToString() : "N/A")));
#endif

            if ((!this.isServerLicensed.HasValue) || checkByServer)
            {
                // Se la variabile di backup locale non ha valore, non � mai stato eseguito un controllo
                // Eseguiamo il controllo completo anche nella forzatura
                if (this.IsServerLicensed() > 0)
                {
                    this.withoutLicenseSignaled = false;
                    this.isServerLicensed = true;
                    return true;
                }

                if (!this.withoutLicenseSignaled)
                {
                    LocalUtil.WriteDebugLogToFile(string.Format("WARNING: ServerId ({0}) is not licensed, centralization not active.",
                        this.SrvId.ToString()));
                    this.withoutLicenseSignaled = true;
                }

                this.isServerLicensed = false;
                return false;
            }

            // La variabile di backup � valorizzata con l'ultimo stato valido, restituiamo quello senza appesantire il tutto con nuove elaborazioni
            return this.isServerLicensed.Value;
        }

        #endregion

        private string MACAddress
        {
            get
            {
                // Il MAC address per la centralizzazione pu� essere forzato da configurazione
                // per permettere di centralizzare da due STLC differenti, sullo stesso server in centrale
                // In periferia i dati sulla tabella server e STLC Parameters (MAC2) restano quelli effettivi,
                // lo spoofing � effettivo solo in fase di centralizzazione
                // Tale valore � preso nell'ordine: file di configurazione -> Chiave di rgistro -> MAC address della scheda di rete.
                return AppCfg.AppCfg.Default.sETH1ForcedMAC;
            }
        }
    }
}
﻿using System;

namespace GrisSuite
{
    public static class EnumExtension
    {
        public static bool TryParse<TEnum>(string value, out TEnum result)
            where TEnum : struct, IConvertible
        {
            var retValue = value != null && Enum.IsDefined(typeof(TEnum), value);
            result = retValue ? (TEnum)Enum.Parse(typeof(TEnum), value) : default(TEnum);
            return retValue;
        }
    }
}

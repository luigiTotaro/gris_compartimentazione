﻿namespace GrisSuite.SCAgent
{
    /// <summary>
    ///   Contiene le informazioni relative al file in cache
    /// </summary>
    public class FileBasedWriteFilterFileCacheDetail
    {
        /// <summary>
        ///   Nome del file
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        ///   Indica la dimensione della cache usata dal file
        /// </summary>
        public long CacheSize { get; private set; }

        /// <summary>
        ///   Numero di handle attualmente aperti
        /// </summary>
        public long OpenHandleCount { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "fileName">Nome del file</param>
        /// <param name = "cacheSize">Indica la dimensione della cache usata dal file</param>
        /// <param name = "openHandleCount">Numero di handle attualmente aperti</param>
        public FileBasedWriteFilterFileCacheDetail(string fileName, long cacheSize, long openHandleCount)
        {
            this.FileName = fileName;
            this.CacheSize = cacheSize;
            this.OpenHandleCount = openHandleCount;
        }
    }
}
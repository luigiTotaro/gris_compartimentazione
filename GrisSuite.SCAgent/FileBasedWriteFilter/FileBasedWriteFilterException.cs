﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.SCAgent
{
    [Serializable]
    public class FileBasedWriteFilterException : Exception
    {
        public FileBasedWriteFilterException()
        {
        }

        public FileBasedWriteFilterException(string message) : base(message)
        {
        }

        public FileBasedWriteFilterException(string message, Exception inner) : base(message, inner)
        {
        }

        protected FileBasedWriteFilterException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿namespace GrisSuite.SCAgent
{
    /// <summary>
    ///   Contiene le informazioni relative ad un volume protetto
    /// </summary>
    public class FileBasedWriteFilterFileSystemInfo
    {
        /// <summary>
        ///   Numero di unità di allocazione per l'utente associato al thread chiamante
        /// </summary>
        public long TotalAllocationUnits { get; private set; }

        /// <summary>
        ///   Numero di unità di allocazione disponibili per l'utente associato al thread chiamante
        /// </summary>
        public long AvailableAllocationUnits { get; private set; }

        /// <summary>
        ///   Numero di settori in ogni unità di allocazione
        /// </summary>
        public long SectorsPerAllocationUnit { get; private set; }

        /// <summary>
        ///   Numero di byte per settore
        /// </summary>
        public long BytesPerSector { get; private set; }

        /// <summary>
        ///   Numero di byte del volume
        /// </summary>
        public long TotalVolumeBytes
        {
            get { return this.TotalAllocationUnits*this.SectorsPerAllocationUnit*this.BytesPerSector; }
        }

        /// <summary>
        ///   Numero di byte del volume
        /// </summary>
        public long TotalFreeVolumeBytes
        {
            get { return this.AvailableAllocationUnits*this.SectorsPerAllocationUnit*this.BytesPerSector; }
        }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "totalAllocationUnits">Numero di unità di allocazione per l'utente associato al thread chiamante</param>
        /// <param name = "availableAllocationUnits">Numero di unità di allocazione disponibili per l'utente associato al thread chiamante</param>
        /// <param name = "sectorsPerAllocationUnit">Numero di settori in ogni unità di allocazione</param>
        /// <param name = "bytesPerSector">Numero di byte per settore</param>
        public FileBasedWriteFilterFileSystemInfo(long totalAllocationUnits, long availableAllocationUnits, long sectorsPerAllocationUnit,
                                                  long bytesPerSector)
        {
            this.TotalAllocationUnits = totalAllocationUnits;
            this.AvailableAllocationUnits = availableAllocationUnits;
            this.SectorsPerAllocationUnit = sectorsPerAllocationUnit;
            this.BytesPerSector = bytesPerSector;
        }
    }
}
﻿using System.Runtime.InteropServices;

namespace GrisSuite.SCAgent
{
    internal class FBWFAPIHelper
    {
        /// <summary>
        ///   The operation completed successfully.
        /// </summary>
        public const uint NO_ERROR = 0;

        /// <summary>
        ///   At least one parameter is NULL.
        /// </summary>
        public const uint ERROR_INVALID_PARAMETER = 87;

        /// <summary>
        ///   The specified volume is valid but does not exist or is not protected.
        /// </summary>
        public const uint ERROR_INVALID_DRIVE = 15;

        /// <summary>
        ///   Requested function is not available or invalid.
        /// </summary>
        public const uint ERROR_INVALID_FUNCTION = 1;

        /// <summary>
        ///   More data is available.
        /// </summary>
        public const uint ERROR_MORE_DATA = 234;

        /// <summary>
        ///   There are no more files.
        /// </summary>
        public const uint ERROR_NO_MORE_FILES = 18;

        /// <summary>
        ///   The system cannot find the file specified.
        /// </summary>
        public const uint ERROR_FILE_NOT_FOUND = 2;

        /// <summary>
        ///   Not enough storage is available to process this command.
        /// </summary>
        public const uint ERROR_NOT_ENOUGH_MEMORY = 8;

        public const uint FBWF_MIN_CACHE_THRESHOLD = 16;
        public const uint FBWF_DEFAULT_CACHE_THRESHOLD = 64;
        public const uint FBWF_MAX_CACHE_THRESHOLD = 1024;

        /// <summary>
        ///   Used to represent a 64-bit signed integer value.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 8)]
        public struct LARGE_INTEGER
        {
            /// <summary>
            ///   Signed 64-bit integer.
            /// </summary>
            public long QuadPart;
        }

        /// <summary>
        ///   Contains information about the FBWF cache.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct FbwfCacheDetail
        {
            /// <summary>
            ///   Specifies the size of the cache used by the file.
            /// </summary>
            public uint CacheSize;

            /// <summary>
            ///   Number of currently opened handles.
            /// </summary>
            public uint OpenHandleCount;

            /// <summary>
            ///   File name length in bytes.
            /// </summary>
            public uint FileNameLength;

            /// <summary>
            ///   File name (may not be NULL terminated).
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)] public string FileName;
        };

        /// <summary>
        ///   Contains information on FBWF memory usage.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct FbwfMemoryUsage
        {
            /// <summary>
            ///   Cache threshold in bytes for the current session.
            /// </summary>
            public uint CurrCacheThreshold;

            /// <summary>
            ///   Cache threshold in bytes for the next session.
            /// </summary>
            public uint NextCacheThreshold;

            /// <summary>
            ///   Memory in bytes used to store directory structure.
            /// </summary>
            public uint DirStructure;

            /// <summary>
            ///   Memory in bytes used to cache file data.
            /// </summary>
            public uint FileData;
        };

        /// <summary>
        ///   Used to query sector size information for a file system volume.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct FbwfFSInfo
        {
            /// <summary>
            ///   Total number of allocation units on the volume that are available to the user associated with the calling thread. If per-user quotas are in use, this value may be less than the total number of allocation units on the disk.
            /// </summary>
            public LARGE_INTEGER TotalAllocationUnits;

            /// <summary>
            ///   Total number of free allocation units on the volume that are available to the user associated with the calling thread. If per-user quotas are in use, this value may be less than the total number of free allocation units on the disk.
            /// </summary>
            public LARGE_INTEGER AvailableAllocationUnits;

            /// <summary>
            ///   Number of sectors in each allocation unit.
            /// </summary>
            public uint SectorsPerAllocationUnit;

            /// <summary>
            ///   Number of bytes in each sector.
            /// </summary>
            public uint BytesPerSector;
        };

        /// <summary>
        ///   Retrieves the list of protected volumes for either the current or next session.
        /// </summary>
        /// <param name = "currSession">[in] Boolean value. If true, the list of protected volumes in the current session will be returned, otherwise the function will return the list of volumes to be protected in the next session.</param>
        /// <param name = "volumeList">[out] Caller allocated buffer receiving the volume list as a multi-string.</param>
        /// <param name = "size">[in, out] Size of the buffer pointed to by volumeList. If this buffer size is too small, the function will fail and size will contain the required buffer size.</param>
        /// <returns>NO_ERROR (the operation completed successfully), ERROR_INVALID_PARAMETER (the volumeList or size parameter is NULL), ERROR_MORE_DATA (the volumeList buffer is too small, in which case size will point to a ULONG containing the required buffer size)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfGetVolumeList(uint currSession, char[] volumeList, ref uint size);

        /// <summary>
        ///   Retrieves the status of the FBWF for the current and next sessions.
        /// </summary>
        /// <param name = "currentSession">[out] Boolean value, true if FBWF is enabled for the current session.</param>
        /// <param name = "nextSession">[out] Boolean value, true if FBWF will be enabled on the next session.</param>
        /// <returns>NO_ERROR (the operation completed successfully) or ERROR_INVALID_PARAMETER (the currentSession or nextSession parameter is NULL)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfIsFilterEnabled(ref uint currentSession, ref uint nextSession);

        /// <summary>
        ///   Retrieves the protected status for a specified volume in both the current and next sessions.
        /// </summary>
        /// <param name = "volume">[in] NULL terminated wide string containing the name of the volume to be queried.</param>
        /// <param name = "currentSession">[out] Boolean value, true if the volume is protected in the current session.</param>
        /// <param name = "nextSession">[out] Boolean value, true if the volume will be protected in the next session.</param>
        /// <returns>NO_ERROR (the operation completed successfully) or ERROR_INVALID_PARAMETER (the volume, currSession, or nextSession parameter is NULL)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfIsVolumeProtected(string volume, ref uint currentSession, ref uint nextSession);

        /// <summary>
        ///   Retrieves a FbwfMemoryUsage structure containing information about the cache memory used by FBWF.
        /// </summary>
        /// <param name = "usage">[out] FbwfMemoryUsage structure containing information about the current cache state.</param>
        /// <returns>NO_ERROR (the operation completed successfully) or ERROR_INVALID_PARAMETER (the parameter usage is NULL)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfGetMemoryUsage(ref FbwfMemoryUsage usage);

        /// <summary>
        ///   Retrieves information about the first file in the FBWF cache.
        /// </summary>
        /// <param name = "volume">[in] NULL terminated wide string containing the name of the volume.</param>
        /// <param name = "cacheDetail">[out] Caller allocated buffer containing the first cache detail.</param>
        /// <param name = "size">[in, out] On input, contains the size of cacheDetail. If the function returns ERROR_MORE_DATA, size will contain the size of the required buffer.</param>
        /// <returns>NO_ERROR (the operation completed successfully), ERROR_INVALID_DRIVE (the specified volume is valid but does not exist or is not protected), ERROR_INVALID_FUNCTION (the filter is disabled for the current session), ERROR_INVALID_PARAMETER (volume, cacheDetail, or size is NULL), ERROR_MORE_DATA (the cacheDetail buffer is too small, in which case size will contain the required buffer size), ERROR_FILE_NOT_FOUND (the specified volume has no cached files) or ERROR_NOT_ENOUGH_MEMORY (there is not enough memory for allocation of internal FbwfFindFirst/FbwfFindNext buffers)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfFindFirst(string volume, ref FbwfCacheDetail cacheDetail, ref uint size);

        /// <summary>
        ///   Retrieves information about the next file in the FBWF memory cache.
        /// </summary>
        /// <param name = "cacheDetail">[in] Caller allocated buffer containing cache detail.</param>
        /// <param name = "size">[in, out] On input, contains the size of cacheDetail. If the function returns ERROR_MORE_DATA, size will contain the size of the required buffer.</param>
        /// <returns>NO_ERROR (the operation completed successfully), ERROR_INVALID_FUNCTION (the filter is disabled for the current session or there was no successful FbwfFindFirst call preceding this call), ERROR_INVALID_PARAMETER (the cacheDetail or size parameter is NULL), ERROR_MORE_DATA (the cacheDetail buffer is too small, in which case the size parameter will point to a ULONG containing the required buffer size) or ERROR_NO_MORE_FILES (there are no more entries in the cached files list)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfFindNext(ref FbwfCacheDetail cacheDetail, ref uint size);

        /// <summary>
        ///   Closes the FbwfFindFirst/FbwfFindNext search.
        /// </summary>
        /// <returns>NO_ERROR (the operation completed successfully) or ERROR_INVALID_FUNCTION (there was no successful FbwfFindFirst call preceding this call)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfFindClose();

        /// <summary>
        ///   Retrieves the status of the compression flag for the current and next sessions.
        /// </summary>
        /// <param name = "currentSession">[out] Boolean value, true if compression is enabled for the current session.</param>
        /// <param name = "nextSession">[out] Boolean value, true if compression will be enabled in the next session.</param>
        /// <returns>NO_ERROR (the operation completed successfully) or ERROR_INVALID_PARAMETER (the currentSession or nextSession parameter is NULL)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfIsCompressionEnabled(ref uint currentSession, ref uint nextSession);

        /// <summary>
        ///   Retrieves the state of the pre-allocation flag for both the current and next sessions.
        /// </summary>
        /// <param name = "currentSession">[out] Boolean value, true if the pre-allocation is enabled for the current session.</param>
        /// <param name = "nextSession">[out] Boolean value, true if pre-allocation will be enabled for the next session.</param>
        /// <returns>NO_ERROR (the operation completed successfully) or ERROR_INVALID_PARMETER (the currentSession or nextSession parameter is NULL)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfIsCachePreAllocationEnabled(ref uint currentSession, ref uint nextSession);

        /// <summary>
        ///   Retrieves the list of files and directories in the FBWF exclusion list.
        /// </summary>
        /// <param name = "volume">[in] NULL terminated wide string containing the volume name.</param>
        /// <param name = "currSession">[in] Boolean value. If true, the exclusion list for the current session will be retrieved. Otherwise the exclusion list for the next session will be retrieved.</param>
        /// <param name = "exclusionList">[in] User allocated buffer which will receive the exclusion list as a multi-string.</param>
        /// <param name = "size">[in, out] Points to a ULONG containing the size of exclusionList. If the function fails with ERROR_MORE_DATA, size will point to a ULONG containing the required buffer size.</param>
        /// <returns>NO_ERROR (the operation completed successfully), ERROR_INVALID_DRIVE (the specified volume is valid but does not exist or is not protected for the specified session), ERROR_INVALID_PARAMETER (the volume, exclusionList or size is NULL) or ERROR_MORE_DATA (the exclusionList buffer is too small, in which case size will point to a ULONG containing the required buffer size)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfGetExclusionList(string volume, uint currSession, char[] exclusionList, ref uint size);

        /// <summary>
        ///   Returns the structure FbwfFSInfo containing volume sector size information in virtual numbers.
        /// </summary>
        /// <param name = "volume">[in] NULL terminated wide string indicating volume name. This can either be a drive letter or volume device name.</param>
        /// <param name = "fsInfo">[out] Pointer to user-allocated FbwfFSInfo structure populated with the virtual size data.</param>
        /// <returns>NO_ERROR (the operation completed successfully), ERROR_INVALID_DRIVE (the specified volume is valid but does not exist or is not protected), ERROR_INVALID_FUNCTION (FBWF is disabled for the next session) or ERROR_INVALID_PARAMETER (the volume parameter is NULL)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfGetVirtualSize(string volume, ref FbwfFSInfo fsInfo);

        /// <summary>
        ///   Returns the structure FbwfFSInfo containing volume sector size information in actual numbers.
        /// </summary>
        /// <param name = "volume">[in] NULL terminated wide string indicating volume name. This can either be a drive letter or volume device name.</param>
        /// <param name = "fsInfo">[out] Pointer to user-allocated FbwfFSInfo structure populated with the actual size data.</param>
        /// <returns>NO_ERROR (the operation completed successfully), ERROR_INVALID_DRIVE (the specified volume is valid but does not exist or is not protected), ERROR_INVALID_FUNCTION (FBWF is disabled for the next session), ERROR_INVALID_PARAMETER (the volume parameter is NULL) or other errors from file system driver (calling the underlying volume fails)</returns>
        [DllImport("fbwflib.dll", CharSet = CharSet.Unicode)]
        public static extern uint FbwfGetActualSize(string volume, ref FbwfFSInfo fsInfo);
    }
}
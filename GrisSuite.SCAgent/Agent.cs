using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using GrisSuite.CentralWS;
using GrisSuite.Common;
using GrisSuite.Data;
using GrisSuite.Data.dsConfigTableAdapters;
using GrisSuite.Data.dsDeviceAcksTableAdapters;
using GrisSuite.Data.dsEventsTableAdapters;
using GrisSuite.Data.dsSTLCParametersTableAdapters;
using GrisSuite.Properties;
using GrisSuite.ServiceReference1;

namespace GrisSuite.SCAgent
{
    public class Agent
    {
        private readonly string _fileDeviceStatusCache = AppDomain.CurrentDomain.BaseDirectory + "\\DeviceStatusCache.xml";
        private readonly string _fileConfigCache = AppDomain.CurrentDomain.BaseDirectory + "\\ConfigCache.xml";
        private readonly SCACollectorClient _collector = new SCACollectorClient();
        private bool _isConfigValid;
        private bool _isDeviceStatusValid;
        private bool _shouldSendParkedServer;
        private ServerParkingData.ParkingKind _parkingServerStatus = ServerParkingData.ParkingKind.Unknown;

        public Agent()
        {
            this.InitializeServers();
        }

        public Agent(bool loadServers)
        {
            // Costruttore con parametro da usare nei test, se si vuole evitare di fare accesso alla base dati
            if (loadServers)
            {
                this.InitializeServers();
            }
        }

        private void InitializeServers()
        {
            dsConfig.ServersDataTable servers = this.GetServerData();
            if (servers != null && servers.Count > 0)
            {
                this._collector.SrvId = servers[0].SrvID;

                LocalUtil.WriteDebugLogToFile(string.Format("InitializeServers: ServerId= {0}", this._collector.SrvId.ToString()));
            }
            else
            {
                ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
                STLCInfoRequest Request = new STLCInfoRequest { clientType = RequestingClientType.COMMAND_LINE };
                STLCInfoResult Result = CommandServiceClient.getSTLCInfo(Request);

                LocalUtil.WriteDebugLogToFile(string.Format("InitializeServers: get the ServerId from STCManager: ServerId= {0}", ParseSerialNumber(Result.data.serialNumber).ToString()));
                this._collector.SrvId = (int)ParseSerialNumber(Result.data.serialNumber);
            }
        }

        /// <summary>
        ///     Analizza numero di serie e lo restituisce come intero.
        /// </summary>
        /// <param name="sSN">numero di serie in formato YY.DD.NNN</param>
        /// <returns>numero di serie in formato numerico, 0 in presenza di errori</returns>
        private static uint ParseSerialNumber(string sSN)
        {
            try
            {
                string[] sSplit = sSN.Split('.');

                uint iYear = uint.Parse(sSplit[0]);
                uint iDay = uint.Parse(sSplit[1]);
                uint iProg = uint.Parse(sSplit[2]);

                return (iYear << 24) | (iDay << 16) | iProg;
            }
            catch
            {
            }

            return 0;
        }

        #region Public Methods

        public void ClearCache()
        {
            try
            {
                if (File.Exists(this._fileDeviceStatusCache))
                {
                    File.Delete(this._fileDeviceStatusCache);
                }

                if (File.Exists(this._fileConfigCache))
                {
                    File.Delete(this._fileConfigCache);
                }
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on ClearCache:" + ex.Message);
                throw;
            }
        }

        public Guid SendConfigToCentral()
        {
            return this.SendConfigToCentral(false);
        }

        public Guid SendConfigToCentral(bool onlyChange)
        {
            Guid rc = Guid.Empty;
            try
            {
                dsConfig dsChange = new dsConfig();
                dsConfig dsConfigCache = new dsConfig();

                string[] tables = Settings.Default.SyncConfig.GetTablesNames();

                dsConfig dsConfigNew = dsConfig.GetData(tables);
                bool isValidCache = LoadFromCache(dsConfigCache, this._fileConfigCache);

                if (onlyChange && isValidCache)
                {
                    MergeDataSet(dsConfigNew, dsConfigCache, dsChange, Settings.Default.SyncConfig);
                }
                else
                {
                    LocalUtil.WriteDebugLogToFile("Initialize config cache");
                    dsConfigCache = dsConfigNew;
                    dsChange = dsConfigNew;
                }

                FilterSentColumns(dsChange, Settings.Default.SyncConfig);

                int nRows = GetDataSetRowsCount(dsChange);

                if (nRows > 0)
                {
                    if (dsChange.Servers.Rows.Count > 0 && ((dsConfig.ServersRow) dsChange.Servers.Rows[0]).SrvID == 0)
                    {
                        LocalUtil.WriteDebugLogToFile("SrvID = 0, Centralization Stopped");
                        this._isConfigValid = false; // invalido la configurazione
                    }
                    else
                    {
                        if (onlyChange)
                        {
                            byte[] cds = Util.CompressDataSet(dsChange);
                            rc = this._collector.UpdateConfig(cds, nRows);
                        }
                        else
                        {
                            byte[] cds = Util.CompressDataSet(dsChange);
                            rc = this._collector.ReplaceConfig(cds, nRows);
                        }
                        this._isConfigValid = SaveCache(dsConfigCache, this._fileConfigCache);
                    }
                }
                else
                {
                    LocalUtil.WriteDebugLogToFile("No config change");
                    this._isConfigValid = SaveCache(dsConfigCache, this._fileConfigCache);
                }

                return rc;
            }
            catch (Exception ex)
            {
                this._isConfigValid = false; // invalido da configurazione
                LocalUtil.WriteDebugLogToFile("Error on SendConfigToCentral:" + ex.Message);
                throw;
            }
        }

        public Guid SendDeviceStatusToCentral()
        {
            if (this._parkingServerStatus == ServerParkingData.ParkingKind.PreviouslyParkedServer)
                return this.SendDeviceStatusToCentral(false);
            else 
                return Guid.Empty;
        }

        public Guid SendDeviceStatusToCentral(bool onlyChange)
        {
            Guid rc = Guid.Empty;
            try
            {
                LocalUtil.WriteDebugLogToFile(string.Format("SendDeviceStatusToCentral {0}", (onlyChange==false)?"-SendAll-":"-OnlyChange-"));

                if (!this.IsConfigValid)
                {
                    throw new Exception("Can't send Device Status when Config is not valid, recall SendDeviceStatusToCentral");
                }

                dsDeviceStatus dsChange = new dsDeviceStatus();
                dsDeviceStatus dsDeviceStatusCache = new dsDeviceStatus();
                string[] tables = Settings.Default.SyncDeviceStatus.GetTablesNames();

                dsDeviceStatus dsDeviceStatusNew = dsDeviceStatus.GetData(tables);
                bool isCacheValid = LoadFromCache(dsDeviceStatusCache, this._fileDeviceStatusCache);

                if (onlyChange && isCacheValid)
                {
                    MergeDataSet(dsDeviceStatusNew, dsDeviceStatusCache, dsChange, Settings.Default.SyncDeviceStatus);
                }
                else
                {
                    LocalUtil.WriteDebugLogToFile("Initialize device status as empty");
                    dsDeviceStatusCache = dsDeviceStatusNew;
                    dsChange = dsDeviceStatusNew;
                }

                FilterSentColumns(dsChange, Settings.Default.SyncDeviceStatus);

                int nRows = GetDataSetRowsCount(dsChange);

                if (nRows > 0)
                {
                    // L'aggiornamento gestisce anche la cancellazione delle celle in centrale per le righe di dsChange che hanno
                    // la colonna IsDeleted = true. 
                    // Nel caso la chiamata non funzioni l'informazione di dover cancellare le righe va persa quindi pu� succedere che 
                    // STLCT1000 e Centrale diventino disallineati, in Centrale ci possono essere + righe.
                    // Per evitarlo si potrebbe memorizzare da parte i cambiamenti con cancellazioni non inviati correttamente
                    // e provare a rimandarli al giro successivo. Per ora si decide di non farlo 16.03.2007
                    if (onlyChange)
                    {
                        byte[] cds = Util.CompressDataSet(dsChange);
                        rc = this._collector.UpdateStatus(cds, nRows);
                    }
                    else
                    {
                        byte[] cds = Util.CompressDataSet(dsChange);
                        rc = this._collector.ReplaceStatus(cds, nRows);
                    }
                }
                else
                {
                    LocalUtil.WriteDebugLogToFile("No device status change");
                }
                this._isDeviceStatusValid = SaveCache(dsDeviceStatusCache, this._fileDeviceStatusCache);
                return rc;
            }
            catch (Exception ex)
            {
                this._isDeviceStatusValid = false;
                LocalUtil.WriteDebugLogToFile("Error on SendDeviceStatusToCentral:" + ex.Message);
                throw;
            }
        }

        public Guid SendIsAlive()
        {
            try
            {
                if (this._shouldSendParkedServer)
                {
                    this.SignalParkedServer();
                }

                return this._collector.IsAlive();
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on SendIsAlive:" + ex.Message);
                throw;
            }
        }

        public byte CheckLicense()
        {
            try
            {
                if (this._shouldSendParkedServer)
                {
                    this.SignalParkedServer();
                }

                return this._collector.IsServerLicensed();
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on CheckLicense:" + ex.Message);
                throw;
            }
        }

        public bool IsConfigValid
        {
            get { return this._isConfigValid; }
        }

        public bool IsDeviceStatusValid
        {
            get { return this._isDeviceStatusValid; }
        }

        public bool TrySqlConnect()
        {
            return Util.TrySqlConnection(10, 1000);
        }

        public ServerParkingData.ParkingKind GetParkingServerStatus()
        {
            return _parkingServerStatus;
        }

        internal dsConfig.ServersDataTable GetServerData()
        {
            this._shouldSendParkedServer = false;
            this._parkingServerStatus = ServerParkingData.ParkingKind.PreviouslyParkedServer;

            dsConfig.ServersDataTable servers;
            try
            {
                servers = new ServersTableAdapter().GetData(); // estratti solo i servers che abbiano periferiche non removed
            }
            catch (SqlException ex)
            {
                this._shouldSendParkedServer = true;
                this._parkingServerStatus = ServerParkingData.ParkingKind.MissingConfigurationServer;
                throw new ApplicationException("Error: unable to connect to SQL Server, Inner exception:" + ex);
            }

            if ((servers == null) || (servers.Count <= 0))
            {
                LocalUtil.WriteDebugLogToFile("No server present in DB, going to parking state...");

                this._shouldSendParkedServer = true;
                this._parkingServerStatus = ServerParkingData.ParkingKind.MissingConfigurationServer;
            }

            if ((servers != null) && (servers.Count > 1))
            {
                this._shouldSendParkedServer = true;
                this._parkingServerStatus = ServerParkingData.ParkingKind.ConfiguredServer;
                // per un corretto funzionamento di alcuni metodi di centralizzazione � necessaria la verifica che la tabella servers contenga sempre e solo un STLC operativo (con periferiche non rimosse)
                throw new ApplicationException("More than one valid row in 'servers' table.");
            }
            return servers;
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        public Guid SignalParkedServer()
        {
            return SignalParkedServer(this._parkingServerStatus);
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        public Guid SignalParkedServer(ServerParkingData.ParkingKind forcedParkingType)
        {
            try
            {
                Guid rc = this._collector.UpdateParkedServer(forcedParkingType);

                return rc;
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on UpdateParkedServer:" + ex.Message);
                throw;
            }
        }

        public bool UpdateServerFromEnvironment()
        {
            string fullhostname = Utility.GetFullHostName();
            string mac;
            string mac2;
            string ip = Util.GetNetworkInterfaceIP(Settings.Default.NetworkAdapterName1, out mac);
            string hardwareProfileString = LocalUtil.GetHardwareProfileStringFromConfig();
            // Sovrascriviamo il MAC address della scheda di rete con quello eventualmente forzato da config,
            // in modo che quello usato per la centralizzazione e sulla tabella server, siano coerenti
            mac = this.MACAddress;

            Util.GetNetworkInterfaceIP(Settings.Default.NetworkAdapterName2, out mac2);

            bool hasChange = false;

            dsConfig.ServersDataTable servers = this.GetServerData();

            foreach (dsConfig.ServersRow row in servers.Rows)
            {
                if (row.IsFullHostNameNull() || row.FullHostName != fullhostname)
                {
                    row.FullHostName = fullhostname;
                    hasChange = true;
                }

                if (row.IsIPNull() || row.IP != ip)
                {
                    row.IP = ip;
                    hasChange = true;
                }

                if (row.IsMACNull() || row.MAC != mac)
                {
                    row.MAC = mac;
                    hasChange = true;
                }
            }

            if (hasChange)
            {
                new ServersTableAdapter().Update(servers);
            }

            if (Util.ShrinkTransactionLog(Settings.Default.MBTranLogSizeLimit))
            {
                LocalUtil.WriteDebugLogToFile("Transaction Log shrink performed");
            }

            STLCParametersTableAdapter taParams = new STLCParametersTableAdapter();
            dsSTLCParameters dsParams = new dsSTLCParameters();

            taParams.Fill(dsParams.STLCParameters);

            #region aggiorna/inserisce MACAddress2 in STLCParameters

            DataRow[] rowParamMACAddress2 = dsParams.Tables[0].Select("ParameterName='MACAddress2'");
            if (rowParamMACAddress2.Length > 0)
            {
                dsSTLCParameters.STLCParametersRow row = rowParamMACAddress2[0] as dsSTLCParameters.STLCParametersRow;
                if (row != null)
                {
                    if (row.ParameterValue != mac2)
                    {
                        row.ParameterValue = mac2;
                        taParams.Update(row);
                        hasChange = true;
                    }
                }
            }
            else
            {
                dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                if (row != null)
                {
                    row.SrvID = 0;
                    row.ParameterName = "MACAddress2";
                    row.ParameterValue = mac2;
                    row.ParameterDescription = "MAC address della seconda scheda di rete";
                    dsParams.Tables[0].Rows.Add(row);
                }
                taParams.Update(dsParams);
                hasChange = true;
            }

            #endregion

            #region Recupero dati FBWF (File Base Write Filter)

            FileBasedWriteFilter fbwfCurrentSession = new FileBasedWriteFilter(FileBasedWriteFilter.Session.Current);
            FileBasedWriteFilter fbwfNextSession = new FileBasedWriteFilter(FileBasedWriteFilter.Session.Next);
            bool isFilterEnabledCurrentSession = false;
            string filterStateCurrentSession = "N/D";
            string filterStateNextSession = "N/D";
            string currentSessionCacheThreshold = "N/D";
            string directoryStructure = "N/D";
            string fileData = "N/D";
            string currentSessionAvailableMemory = "N/D";
            string currentSessionUsedMemory = "N/D";

            bool loggingFBWFEnabled = true;

            #region recupero dati FBWF sessione corrente

            try
            {
                isFilterEnabledCurrentSession = fbwfCurrentSession.IsFilterEnabled();

                filterStateCurrentSession = isFilterEnabledCurrentSession ? "Enabled" : "Disabled";
            }
            catch (FileBasedWriteFilterException ex)
            {
                if (!fbwfCurrentSession.IsDLLAvailable)
                {
                    loggingFBWFEnabled = false;
                }

                if (loggingFBWFEnabled)
                {
                    LocalUtil.WriteDebugLogToFile(ex.Message);
                }
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile(ex.Message);
            }

            #endregion

            #region recupero dati FBWF sessione successiva

            try
            {
                bool isFilterEnabledNextSession = fbwfNextSession.IsFilterEnabled();

                filterStateNextSession = isFilterEnabledNextSession ? "Enabled" : "Disabled";
            }
            catch (FileBasedWriteFilterException ex)
            {
                if (!fbwfNextSession.IsDLLAvailable)
                {
                    loggingFBWFEnabled = false;
                }

                if (loggingFBWFEnabled)
                {
                    LocalUtil.WriteDebugLogToFile(ex.Message);
                }
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile(ex.Message);
            }

            #endregion

            if (loggingFBWFEnabled)
            {
                #region aggiorna/inserisce FBWFCurrentSessionFilterState in STLCParameters

                DataRow[] rowParamFBWFCurrentSessionFilterState = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionFilterState'");
                if (rowParamFBWFCurrentSessionFilterState.Length > 0)
                {
                    dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionFilterState[0] as dsSTLCParameters.STLCParametersRow;
                    if (row != null)
                    {
                        if (row.ParameterValue != filterStateCurrentSession)
                        {
                            row.ParameterValue = filterStateCurrentSession;
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }
                }
                else
                {
                    dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                    if (row != null)
                    {
                        row.SrvID = 0;
                        row.ParameterName = "FBWFCurrentSessionFilterState";
                        row.ParameterValue = filterStateCurrentSession;
                        row.ParameterDescription = "Stato filtro sessione corrente FBWF";
                        dsParams.Tables[0].Rows.Add(row);
                    }
                    taParams.Update(dsParams);
                    hasChange = true;
                }

                #endregion

                #region aggiorna/inserisce FBWFNextSessionFilterState in STLCParameters

                DataRow[] rowParamFBWFFilterStateNextSession = dsParams.Tables[0].Select("ParameterName='FBWFNextSessionFilterState'");
                if (rowParamFBWFFilterStateNextSession.Length > 0)
                {
                    dsSTLCParameters.STLCParametersRow row = rowParamFBWFFilterStateNextSession[0] as dsSTLCParameters.STLCParametersRow;
                    if (row != null)
                    {
                        if (row.ParameterValue != filterStateNextSession)
                        {
                            row.ParameterValue = filterStateNextSession;
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }
                }
                else
                {
                    dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                    if (row != null)
                    {
                        row.SrvID = 0;
                        row.ParameterName = "FBWFNextSessionFilterState";
                        row.ParameterValue = filterStateNextSession;
                        row.ParameterDescription = "Stato filtro prossima sessione FBWF";
                        dsParams.Tables[0].Rows.Add(row);
                    }
                    taParams.Update(dsParams);
                    hasChange = true;
                }

                #endregion

                if (isFilterEnabledCurrentSession)
                {
                    #region Dati memoria del filtro

                    try
                    {
                        FileBasedWriteFilterMemoryUsage memoryUsage = fbwfCurrentSession.GetMemoryUsage();

                        currentSessionCacheThreshold = string.Format("{0:0,0} KB", memoryUsage.CurrentSessionCacheThreshold/1024);
                        directoryStructure = string.Format("{0:0,0} KB", memoryUsage.DirectoryStructure/1024);
                        fileData = string.Format("{0:0,0} KB", memoryUsage.FileData/1024);
                        currentSessionAvailableMemory = string.Format("{0:0,0} KB ({1:0.##}%)", memoryUsage.CurrentSessionAvailableMemory/1024,
                            memoryUsage.CurrentSessionAvailableMemoryPercentage);
                        currentSessionUsedMemory = string.Format("{0:0,0} KB ({1:0.##}%)", memoryUsage.CurrentSessionUsedMemory/1024,
                            memoryUsage.CurrentSessionUsedMemoryPercentage);
                    }
                    catch (FileBasedWriteFilterException ex)
                    {
                        LocalUtil.WriteDebugLogToFile(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        LocalUtil.WriteDebugLogToFile(ex.Message);
                    }

                    #endregion

                    #region aggiorna/inserisce FBWFCurrentSessionCacheThreshold in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionCacheThreshold = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionCacheThreshold'");
                    if (rowParamFBWFCurrentSessionCacheThreshold.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionCacheThreshold[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            if (row.ParameterValue != currentSessionCacheThreshold)
                            {
                                row.ParameterValue = currentSessionCacheThreshold;
                                taParams.Update(row);
                                hasChange = true;
                            }
                        }
                    }
                    else
                    {
                        dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.SrvID = 0;
                            row.ParameterName = "FBWFCurrentSessionCacheThreshold";
                            row.ParameterValue = currentSessionCacheThreshold;
                            row.ParameterDescription = "Limite cache attuale sessione corrente FBWF";
                            dsParams.Tables[0].Rows.Add(row);
                        }
                        taParams.Update(dsParams);
                        hasChange = true;
                    }

                    #endregion

                    #region aggiorna/inserisce FBWFCurrentSessionDirectoryStructure in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionDirectoryStructure =
                        dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionDirectoryStructure'");
                    if (rowParamFBWFCurrentSessionDirectoryStructure.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionDirectoryStructure[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            if (row.ParameterValue != directoryStructure)
                            {
                                row.ParameterValue = directoryStructure;
                                taParams.Update(row);
                                hasChange = true;
                            }
                        }
                    }
                    else
                    {
                        dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.SrvID = 0;
                            row.ParameterName = "FBWFCurrentSessionDirectoryStructure";
                            row.ParameterValue = directoryStructure;
                            row.ParameterDescription = "Uso memoria cartelle sessione corrente FBWF";
                            dsParams.Tables[0].Rows.Add(row);
                        }
                        taParams.Update(dsParams);
                        hasChange = true;
                    }

                    #endregion

                    #region aggiorna/inserisce FBWFCurrentSessionFileData in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionFileData = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionFileData'");
                    if (rowParamFBWFCurrentSessionFileData.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionFileData[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            if (row.ParameterValue != fileData)
                            {
                                row.ParameterValue = fileData;
                                taParams.Update(row);
                                hasChange = true;
                            }
                        }
                    }
                    else
                    {
                        dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.SrvID = 0;
                            row.ParameterName = "FBWFCurrentSessionFileData";
                            row.ParameterValue = fileData;
                            row.ParameterDescription = "Uso memoria file sessione corrente FBWF";
                            dsParams.Tables[0].Rows.Add(row);
                        }
                        taParams.Update(dsParams);
                        hasChange = true;
                    }

                    #endregion

                    #region aggiorna/inserisce FBWFCurrentSessionAvailableMemory in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionAvailableMemory =
                        dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionAvailableMemory'");
                    if (rowParamFBWFCurrentSessionAvailableMemory.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionAvailableMemory[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            if (row.ParameterValue != currentSessionAvailableMemory)
                            {
                                row.ParameterValue = currentSessionAvailableMemory;
                                taParams.Update(row);
                                hasChange = true;
                            }
                        }
                    }
                    else
                    {
                        dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.SrvID = 0;
                            row.ParameterName = "FBWFCurrentSessionAvailableMemory";
                            row.ParameterValue = currentSessionAvailableMemory;
                            row.ParameterDescription = "Memoria virtuale libera sessione corrente FBWF";
                            dsParams.Tables[0].Rows.Add(row);
                        }
                        taParams.Update(dsParams);
                        hasChange = true;
                    }

                    #endregion

                    #region aggiorna/inserisce FBWFCurrentSessionUsedMemory in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionUsedMemory = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionUsedMemory'");
                    if (rowParamFBWFCurrentSessionUsedMemory.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionUsedMemory[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            if (row.ParameterValue != currentSessionUsedMemory)
                            {
                                row.ParameterValue = currentSessionUsedMemory;
                                taParams.Update(row);
                                hasChange = true;
                            }
                        }
                    }
                    else
                    {
                        dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.SrvID = 0;
                            row.ParameterName = "FBWFCurrentSessionUsedMemory";
                            row.ParameterValue = currentSessionUsedMemory;
                            row.ParameterDescription = "Memoria virtuale occupata sessione corrente";
                            dsParams.Tables[0].Rows.Add(row);
                        }
                        taParams.Update(dsParams);
                        hasChange = true;
                    }

                    #endregion
                }
                else
                {
                    #region elimina FBWFCurrentSessionCacheThreshold in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionCacheThreshold = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionCacheThreshold'");
                    if (rowParamFBWFCurrentSessionCacheThreshold.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionCacheThreshold[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.Delete();
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }

                    #endregion

                    #region elimina FBWFCurrentSessionDirectoryStructure in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionDirectoryStructure =
                        dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionDirectoryStructure'");
                    if (rowParamFBWFCurrentSessionDirectoryStructure.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionDirectoryStructure[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.Delete();
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }

                    #endregion

                    #region elimina FBWFCurrentSessionFileData in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionFileData = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionFileData'");
                    if (rowParamFBWFCurrentSessionFileData.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionFileData[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.Delete();
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }

                    #endregion

                    #region elimina FBWFCurrentSessionAvailableMemory in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionAvailableMemory =
                        dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionAvailableMemory'");
                    if (rowParamFBWFCurrentSessionAvailableMemory.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionAvailableMemory[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.Delete();
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }

                    #endregion

                    #region elimina FBWFCurrentSessionUsedMemory in STLCParameters

                    DataRow[] rowParamFBWFCurrentSessionUsedMemory = dsParams.Tables[0].Select("ParameterName='FBWFCurrentSessionUsedMemory'");
                    if (rowParamFBWFCurrentSessionUsedMemory.Length > 0)
                    {
                        dsSTLCParameters.STLCParametersRow row = rowParamFBWFCurrentSessionUsedMemory[0] as dsSTLCParameters.STLCParametersRow;
                        if (row != null)
                        {
                            row.Delete();
                            taParams.Update(row);
                            hasChange = true;
                        }
                    }

                    #endregion
                }
            }

            #endregion

            #region aggiorna/inserisce HardwareProfile in STLCParameters

            DataRow[] rowHardwareProfile = dsParams.Tables[0].Select("ParameterName='HardwareProfile'");
            if (rowHardwareProfile.Length > 0)
            {
                dsSTLCParameters.STLCParametersRow row = rowHardwareProfile[0] as dsSTLCParameters.STLCParametersRow;
                if (row != null)
                {
                    if (row.ParameterValue != hardwareProfileString)
                    {
                        row.ParameterValue = hardwareProfileString;
                        taParams.Update(row);
                        hasChange = true;
                    }
                }
            }
            else
            {
                dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
                if (row != null)
                {
                    row.SrvID = 0;
                    row.ParameterName = "HardwareProfile";
                    row.ParameterValue = hardwareProfileString;
                    row.ParameterDescription = "Profilo hardware del server";
                    dsParams.Tables[0].Rows.Add(row);
                }
                taParams.Update(dsParams);
                hasChange = true;
            }

            #endregion

            return hasChange;
        }

        public void WriteDebugLog(string text)
        {
            LocalUtil.WriteDebugLogToFile(text);
        }

        public Guid SendEvents()
        {
            try
            {
                STLCEventsTableAdapter ta = new STLCEventsTableAdapter();
                dsEvents ds = new dsEvents();

                ta.FillEventsToSend(ds.STLCEvents);

                byte[] cds = Util.CompressDataSet(ds);
                Guid rc = this._collector.UpdateEvents(cds, ds.STLCEvents.Rows.Count);

                foreach (dsEvents.STLCEventsRow row in ds.STLCEvents.Rows)
                {
                    row.Delete();
                }

                ta.UpdateOnTransaction(ds);

                if (rc != Guid.Empty)
                {
                    // La centralizzazione degli eventi ha una schedulazione pi� bassa e non appesantisce il server
                    // Segnaliamo il server come correttamente funzionante, se inserito nei parcheggiati
                    // La segnalazione avviene solamente se siamo riusciti a centralizzare, quindi con Guid valorizzato
                    this.SignalParkedServer(this._parkingServerStatus);
                }

                return rc;
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on SendEvents:" + ex.Message);
                throw;
            }
        }

        public Guid SendAcks()
        {
            try
            {
                DeviceAckTableAdapter ta = new DeviceAckTableAdapter();
                dsDeviceAcks ds = new dsDeviceAcks();

                ta.Fill(ds.DeviceAck);

                byte[] cds = Util.CompressDataSet(ds);
                Guid rc = this._collector.UpdateAcks(cds, ds.DeviceAck.Rows.Count);

                return rc;
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on SendAcks:" + ex.Message);
                throw;
            }
        }

        public Guid SendSTLCParameters()
        {
            try
            {
                STLCParametersTableAdapter ta = new STLCParametersTableAdapter();
                dsSTLCParameters ds = new dsSTLCParameters();

                ta.Fill(ds.STLCParameters);

                byte[] cds = Util.CompressDataSet(ds);
                Guid rc = this._collector.ReplaceSTLCParameters(cds);

                return rc;
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on SendSTLCParameters:" + ex.Message);
                throw;
            }
        }

        public bool SyncronizeTime()
        {
            try
            {
                var ws = new Central();
                DateTime st = ws.GetTime();
                return SystemClock.Set(st);
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile("Error on SyncronizeTime:" + ex.Message);
                return false;
            }
        }

        #endregion

        #region Private Utility

        private static void FilterSentColumns(DataSet ds, SyncDataset syncDataset)
        {
            foreach (DataTable dt in ds.Tables)
            {
                string[] sColumnsNotSent = GetColumnsToNotSent(dt, syncDataset);
                if (sColumnsNotSent.Length > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (string colName in sColumnsNotSent)
                        {
                            row[colName] = DBNull.Value;
                        }
                    }
                }
            }
        }

        private static void MergeDataSet(DataSet dsSource, DataSet dsTarget, DataSet dsChange, SyncDataset syncDataset)
        {
            dsChange.Clear();

            foreach (DataTable dtSource in dsSource.Tables)
            {
                string[] sColumns = GetColumnsToCompare(dtSource, syncDataset);
                DataTable dtTarget = dsTarget.Tables[dtSource.TableName];
                DataTable dtChange = dsChange.Tables[dtSource.TableName];

                dtTarget.BeginLoadData();
                dtChange.BeginLoadData();

                // aggiungi e aggiorna le righe
                foreach (DataRow rowSource in dtSource.Rows)
                {
                    // find target row
                    DataRow rowTarget = dtTarget.Rows.Find(GetPKValues(rowSource));

                    if (rowTarget == null) // la riga non era presente nella cache: va aggiunta
                    {
                        LocalUtil.WriteDebugLogToFile(string.Format("NewRecord:{0}", dtSource.TableName));
                        LocalUtil.WriteLog(null, rowSource);
                        dtTarget.Rows.Add(rowSource.ItemArray);
                        dtChange.Rows.Add(rowSource.ItemArray);
                    }
                    else
                    {
                        foreach (string colName in sColumns) // verifica che non siano variati i valori delle colonne monitorate
                        {
                            if ((rowSource[colName] is byte[] && !BytesIsEqual((byte[]) rowSource[colName], (byte[]) rowTarget[colName])) ||
                                (!(rowSource[colName] is byte[]) && !rowSource[colName].Equals(rowTarget[colName])))
                            {
                                LocalUtil.WriteDebugLogToFile(string.Format("Column Changed:{0}.{1}", dtSource.TableName, colName));
                                LocalUtil.WriteLog(rowTarget, rowSource);
                                dtChange.Rows.Add(rowSource.ItemArray);
                                break;
                            }
                        }
                        rowTarget.ItemArray = rowSource.ItemArray;
                    }
                }

                // cancella le righe in + o le marca come da cancellare se la tabella ha la colonna "IsDeleted"
                for (int i = dtTarget.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow rowTarget = dtTarget.Rows[i];
                    if (rowTarget.RowState == DataRowState.Unchanged)
                    {
                        LocalUtil.WriteDebugLogToFile(string.Format("Deleted Record:{0}", rowTarget.Table.TableName));
                        DataRow deletedRow;
                        if (dtTarget.Columns.Contains("IsDeleted"))
                        {
                            deletedRow = dtChange.Rows.Add(rowTarget.ItemArray);
                            deletedRow["IsDeleted"] = true;
                        }
                        else
                        {
                            deletedRow = rowTarget;
                        }
                        LocalUtil.WriteLog(deletedRow, null);
                        dtTarget.Rows.Remove(rowTarget);
                    }
                }
                dtTarget.EndLoadData();
                dtChange.EndLoadData();
            }
            dsChange.AcceptChanges();
            dsTarget.AcceptChanges();
        }

        private static bool BytesIsEqual(byte[] a, byte[] b)
        {
            bool match = true;

            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] != b[i])
                    {
                        match = false;
                        break;
                    }
                }
            }
            else
            {
                match = false;
            }
            return match;
        }

        private static object[] GetPKValues(DataRow rowSource)
        {
            DataColumn[] pkColumns = rowSource.Table.PrimaryKey;
            object[] pkValues = new object[pkColumns.Length];

            for (int i = 0; i < pkColumns.Length; i++)
            {
                pkValues[i] = rowSource[pkColumns[i]];
            }

            return pkValues;
        }

        private static string[] GetColumnsToCompare(DataTable table, SyncDataset syncDataset)
        {
            SyncTable syncTable = syncDataset.Tables.Find(x => string.Compare(x.Name, table.TableName, StringComparison.OrdinalIgnoreCase) == 0);

            string s = string.Empty;
            if (syncTable == null || string.IsNullOrEmpty(syncTable.CompareColumns) || syncTable.CompareColumns == "*")
            {
                foreach (DataColumn col in table.Columns)
                {
                    s += col.ColumnName + ",";
                }
            }
            else
            {
                foreach (string colName in syncTable.CompareColumns.Split(','))
                {
                    if (table.Columns.Contains(colName.ToLower().Trim()))
                    {
                        s += colName.ToLower().Trim() + ",";
                    }
                }
            }

            return (s.Length > 0) ? s.Substring(0, s.Length - 1).Split(',') : new string[] {};
        }

        private static string[] GetColumnsToNotSent(DataTable table, SyncDataset syncDataset)
        {
            SyncTable syncTable = syncDataset.Tables.Find(x => string.Compare(x.Name, table.TableName, StringComparison.OrdinalIgnoreCase) == 0);

            if (syncTable == null || string.IsNullOrEmpty(syncTable.NotSentColumns))
            {
                return new string[] {};
            }

            string s = string.Empty;

            // considera solo i nomi di colonna che esistono nella tabella
            foreach (string colName in syncTable.NotSentColumns.Split(','))
            {
                if (table.Columns.Contains(colName.ToLower().Trim()))
                {
                    s += colName.ToLower().Trim() + ",";
                }
            }

            return (s.Length > 0) ? s.Substring(0, s.Length - 1).Split(',') : new string[] {};
        }

        private static int GetDataSetRowsCount(DataSet ds)
        {
            int count = 0;
            foreach (DataTable dt in ds.Tables)
            {
                count += dt.Rows.Count;
            }
            return count;
        }

        private static bool LoadFromCache(DataSet ds, string filename)
        {
            try
            {
                ds.Clear();
                if (File.Exists(filename))
                {
                    FileStream fs = new FileStream(filename, FileMode.Open);
                    ds.ReadXml(fs);
                    fs.Close();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("Error on LoadCache File={0} Error={1}", filename, ex.Message));
                return false;
            }
        }

        private static bool SaveCache(DataSet ds, string filename)
        {
            try
            {
                FileStream fs = new FileStream(filename, FileMode.Create);
                ds.WriteXml(fs);
                fs.Close();
                return true;
            }
            catch (Exception ex)
            {
                LocalUtil.WriteDebugLogToFile(string.Format("Error on SaveCache File={0} Error={1} ", filename, ex.Message));
                return false;
            }
        }

        private string MACAddress
        {
            get
            {
                // Il MAC address per la centralizzazione pu� essere forzato da configurazione
                // per permettere di centralizzare da due STLC differenti, sullo stesso server in centrale
                // In periferia i dati sulla tabella server e STLC Parameters (MAC2) restano quelli effettivi,
                // lo spoofing � effettivo solo in fase di centralizzazione
                // Tale valore � preso nell'ordine: file di configurazione -> Chiave di rgistro -> MAC address della scheda di rete.
                return AppCfg.AppCfg.Default.sETH1ForcedMAC;
            }
        }

        #endregion
    }
}
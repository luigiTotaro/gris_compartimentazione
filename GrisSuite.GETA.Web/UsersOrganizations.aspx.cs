﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using GrisSuite.GETA.Data;
using System.Collections.Generic;

public partial class UsersOrganizations : System.Web.UI.Page
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		if ( !Utility.IsUserInAdminGroup() )
		{
			this.Response.Redirect("~/Default.aspx");
		}

		( (IGetaMasterPage)this.Master ).GetaLayoutMode = GetaLayoutModeEnum.Administration;

		this.SetImageButtonOverBehavior(this.lvwOrganizations, "imgbOrgAdd", "lightadd", true);
		this.SetImageButtonOverBehavior(this.lvwUserGroups, "imgbUGAdd", "lightadd", true);
		this.SetImageButtonOverBehavior(this.lvwUsers, "imgbUserAdd", "lightadd", true);
	}

	# region Organizations

	# region lvwOrganizations
	protected void lvwOrganizations_ItemCreated ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.InsertItem )
		{
			ImageButton imgbAdd = this.lvwOrganizations.FindControl("imgbOrgAdd") as ImageButton;
			if ( imgbAdd != null )
			{
				imgbAdd.Visible = false;
			}
		}
		else if ( e.Item.ItemType == ListViewItemType.EmptyItem )
		{
			this.SetImageButtonOverBehavior(e.Item, "imgbOrgAdd", "lightadd", true);
		}
	}

	protected void lvwOrganizations_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			UserOrganization organization = ( (ListViewDataItem)e.Item ).DataItem as UserOrganization;

			this.SetImageButtonOverBehavior(e.Item, "imgbEdit", "edit", false);

			ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
			if ( imgbDelete != null )
			{
				if ( organization != null && organization.UsersCount > 0 )
				{
					imgbDelete.Visible = false;
				}
				else
				{
					imgbDelete.Visible = true;
					this.SetImageButtonOverBehavior(imgbDelete, "delete", false);
				}
			}
		}
	}

	protected void lvwOrganizations_ItemInserted ( object sender, ListViewInsertedEventArgs e )
	{
		this.lvwOrganizations.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwOrganizations, "imgbOrgAdd", true);
	}

	protected void lvwOrganizations_ItemEditing ( object sender, ListViewEditEventArgs e )
	{
		this.lvwOrganizations.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwOrganizations, "imgbOrgAdd", true);
	}

	protected void lvwOrganizations_ItemCanceling ( object sender, ListViewCancelEventArgs e )
	{
		if ( e.CancelMode == ListViewCancelMode.CancelingInsert )
		{
			this.lvwOrganizations.InsertItemPosition = InsertItemPosition.None;
			this.SetAddButtonVisible(this.lvwOrganizations, "imgbOrgAdd", true);
		}
	}
	
	protected void lvwOrganizations_SelectedIndexChanged ( object sender, EventArgs e )
	{
		lnkbOrgTitle.ForeColor = System.Drawing.Color.Black;
        updOrganizationsTitle.Update();

		this.lvwUsers.DataBind();
		this.updUsers.Update();
	}
	# endregion
	
	protected void lnkbOrgTitle_Click ( object sender, EventArgs e )
	{
		( (LinkButton)sender ).ForeColor = System.Drawing.Color.White;
		this.lvwOrganizations.SelectedIndex = -1;

		this.updOrganizations.Update();
		this.lvwUsers.DataBind();
		this.updUsers.Update();		
	}

	protected void imgbOrgAdd_Click ( object sender, EventArgs e )
	{
		this.lvwOrganizations.EditIndex = -1;
		this.lvwOrganizations.InsertItemPosition = InsertItemPosition.LastItem;
		((ImageButton)sender).Visible = false;
	}

	protected void linqOrganizations_Selecting ( object sender, LinqDataSourceSelectEventArgs e )
	{
		e.Result = new DBUsersDataContext().GetOrganizations().ToList();
	}
	# endregion

	# region User Groups

	# region lvwUserGroups
	protected void lvwUserGroups_ItemCreated ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.InsertItem )
		{
			ImageButton imgbAdd = this.lvwUserGroups.FindControl("imgbUGAdd") as ImageButton;
			if ( imgbAdd != null )
			{
				imgbAdd.Visible = false;
			}
		}
		else if ( e.Item.ItemType == ListViewItemType.EmptyItem )
		{
			this.SetImageButtonOverBehavior(e.Item, "imgbUGAdd", "lightadd", true);
		}
	}

	protected void lvwUserGroups_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			UserGroup userGroup = ( (ListViewDataItem)e.Item ).DataItem as UserGroup;
			
			this.SetImageButtonOverBehavior(e.Item, "imgbEdit", "edit", false);

			ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
			if ( imgbDelete != null )
			{
				if ( userGroup != null && (userGroup.UsersCount > 0 || userGroup.UserGroupID == 1 || userGroup.UserGroupID == 2) )
				{
					imgbDelete.Visible = false;
				}
				else
				{
					imgbDelete.Visible = true;
					this.SetImageButtonOverBehavior(imgbDelete, "delete", false);
				}
			}
		}
	}

    protected void lvwUserGroups_ItemInserted ( object sender, ListViewInsertedEventArgs e )
	{
		this.lvwUserGroups.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwUserGroups, "imgbUGAdd", true);
	}

	protected void lvwUserGroups_ItemEditing ( object sender, ListViewEditEventArgs e )
	{
		this.lvwUserGroups.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwUserGroups, "imgbUGAdd", true);
	}

	protected void lvwUserGroups_ItemCanceling ( object sender, ListViewCancelEventArgs e )
	{
		if ( e.CancelMode == ListViewCancelMode.CancelingInsert )
		{
			this.lvwUserGroups.InsertItemPosition = InsertItemPosition.None;
			this.SetAddButtonVisible(this.lvwUserGroups, "imgbUGAdd", true);
		}
	}

	protected void lvwUserGroups_SelectedIndexChanged ( object sender, EventArgs e )
	{
		lnkbUGTitle.ForeColor = System.Drawing.Color.Black;
        updUserGroupTitle.Update();

		this.lvwUsers.DataBind();
		this.updUsers.Update();
	}
	# endregion

	protected void lnkbUGTitle_Click ( object sender, EventArgs e )
	{
		( (LinkButton)sender ).ForeColor = System.Drawing.Color.White;
		this.lvwUserGroups.SelectedIndex = -1;
        this.updUserGroups.Update();

		this.lvwUsers.DataBind();
		this.updUsers.Update();
	}

	protected void imgbUGAdd_Click ( object sender, EventArgs e )
	{
		this.lvwUserGroups.EditIndex = -1;
		this.lvwUserGroups.InsertItemPosition = InsertItemPosition.LastItem;
		( (ImageButton)sender ).Visible = false;
	}

	protected void linqUserGroups_Selecting ( object sender, LinqDataSourceSelectEventArgs e )
	{
		e.Result = new DBUsersDataContext().GetUserGroups().ToList();
	}
	# endregion
	
	# region Users
	# region lvwUsers
	protected void lvwUsers_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
			User user = ( (ListViewDataItem)e.Item ).DataItem as User;

			this.SetImageButtonOverBehavior(e.Item, "imgbEdit", "edit", false);

            
			ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
            DropDownList ddlOrganizationID = e.Item.FindControl("OrganizationIDDropDown") as DropDownList;
            if (ddlOrganizationID != null && user.OrganizationID.HasValue) {
                ddlOrganizationID.SelectedValue = user.OrganizationID.Value.ToString();
            }
            DropDownList ddlUserGroupID = e.Item.FindControl("UserGroupIDDropDown") as DropDownList;
            if (ddlUserGroupID != null && user.UserGroupID.HasValue)
            {
                ddlUserGroupID.SelectedValue = user.UserGroupID.Value.ToString();
            }
            MultiIpuntControl mi = e.Item.FindControl("micEmail") as MultiIpuntControl;
            if (mi != null) mi.Text = user.Email;
            MultiIpuntControl ci = e.Item.FindControl("micMobilePhone") as MultiIpuntControl;
            if (ci != null) ci.Text = user.MobilePhone;

            
			//DropDownList ddl
			if ( imgbDelete != null )
			{
                if (user != null)
				{
                    DBIssuesDataContext db = new DBIssuesDataContext();
                    int count = (from isu in db.IssueUsers
                                 where isu.UserID == user.UserID
                                 select isu).Count();

					imgbDelete.Visible = count == 0;
				}
				else
				{
					imgbDelete.Visible = true;
					this.SetImageButtonOverBehavior(imgbDelete, "delete", false);
				}
			}
		}
	}

    protected void lvwUsers_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        ListViewItem item = lvwUsers.Items[e.ItemIndex];

        DropDownList ddl = item.FindControl("OrganizationIDDropDown") as DropDownList;
        e.NewValues.Add("OrganizationID", ddl.SelectedValue);

        ddl = item.FindControl("UserGroupIDDropDown") as DropDownList;
        e.NewValues.Add("UserGroupID", ddl.SelectedValue);

        MultiIpuntControl mic = item.FindControl("micEmail") as MultiIpuntControl;
        e.NewValues.Add("Email", mic.Text);

        MultiIpuntControl cic = item.FindControl("micMobilePhone") as MultiIpuntControl;
        e.NewValues.Add("MobilePhone", cic.Text);
    }

    protected void lvwUsers_ItemInserting(object sender, ListViewInsertEventArgs e)
    {
        TextBox txt = e.Item.FindControl("UsernameTextBox") as TextBox;
        e.Values.Add("UserName", txt.Text);

        txt = e.Item.FindControl("FirstNameTextBox") as TextBox;
        e.Values.Add("FirstName", txt.Text);

        txt = e.Item.FindControl("LastNameTextBox") as TextBox;
        e.Values.Add("LastName", txt.Text);

        DropDownList ddl = e.Item.FindControl("OrganizationIDDropDown") as DropDownList;
        e.Values.Add("OrganizationID", ddl.SelectedValue);

        ddl = e.Item.FindControl("UserGroupIDDropDown") as DropDownList;
        e.Values.Add("UserGroupID", ddl.SelectedValue);

        MultiIpuntControl mic = e.Item.FindControl("micEmail") as MultiIpuntControl;
        e.Values.Add("Email", mic.Text);

        MultiIpuntControl cic = e.Item.FindControl("micMobilePhone") as MultiIpuntControl;
        e.Values.Add("MobilePhone", cic.Text);
    }

	protected void lvwUsers_ItemInserted ( object sender, ListViewInsertedEventArgs e )
	{
		this.SetAddButtonVisible(this.lvwUsers, "imgbUserAdd", true);
        this.lvwUsers.InsertItemPosition = InsertItemPosition.None;
	}

	protected void lvwUsers_ItemEditing ( object sender, ListViewEditEventArgs e )
	{
        this.lvwUsers.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwUsers, "imgbUserAdd", true);
	}

	protected void lvwUsers_ItemCanceling ( object sender, ListViewCancelEventArgs e )
	{
		if ( e.CancelMode == ListViewCancelMode.CancelingInsert )
		{
			this.lvwUsers.InsertItemPosition = InsertItemPosition.None;
			this.SetAddButtonVisible(this.lvwUsers, "imgbUserAdd", true);
		}
	}

    protected void lvwUsers_OnPreRender(object sender, EventArgs e)
    {
        if (lvwUsers.InsertItem != null)
        {
            if (lvwOrganizations.SelectedValue != null)
            {
                DropDownList ddl = lvwUsers.InsertItem.FindControl("OrganizationIDDropDown") as DropDownList;
                ddl.SelectedValue = lvwOrganizations.SelectedValue.ToString();
            }
            if (lvwUserGroups.SelectedValue != null)
            {
                DropDownList ddl = lvwUsers.InsertItem.FindControl("UserGroupIDDropDown") as DropDownList;
                ddl.SelectedValue = lvwUserGroups.SelectedValue.ToString();
            }
        }
    }



	# endregion

	protected void OrganizationIDDropDown_Ondatabinding ( object sender, EventArgs e )
	{
		DropDownList ddl = null;
		if ( ( ddl = sender as DropDownList ) != null )
		{
			ddl.Items.Add(new ListItem("Nessuna Azienda", ""));
		}
	}

	protected void OrganizationIDDropDown_Ondatabound ( object sender, EventArgs e )
	{
		DropDownList ddl = null;
		if ( ( ddl = sender as DropDownList ) != null )
		{
			ddl.Items.Add(new ListItem("Nessuna Azienda", ""));
		}
	}
	
	protected void imgbUserAdd_Click ( object sender, EventArgs e )
	{
        this.lvwUsers.EditIndex = -1;
        this.lvwUsers.InsertItemPosition = InsertItemPosition.LastItem;
        ( (ImageButton)sender ).Visible = false;
	}

	protected void linqUsers_Selecting ( object sender, LinqDataSourceSelectEventArgs e )
	{
		int orgID;
		int ucID;

		if ( string.IsNullOrEmpty(( this.lvwOrganizations.SelectedValue ?? "" ).ToString()) || !int.TryParse(( this.lvwOrganizations.SelectedValue ?? "" ).ToString(), out orgID) ) orgID = -1;
		if ( string.IsNullOrEmpty(( this.lvwUserGroups.SelectedValue ?? "" ).ToString()) || !int.TryParse(( this.lvwUserGroups.SelectedValue ?? "" ).ToString(), out ucID) ) ucID = -1;

        e.Result = new DBUsersDataContext().GetUsers(ucID, orgID).ToList();
	}

    protected void OnFirstNameTextChanged(object sender, EventArgs e)
    {
        TextBox txtUserName = (sender as TextBox).Parent.FindControl("UsernameTextBox") as TextBox;
        TextBox txtFirstName = (sender as TextBox).Parent.FindControl("FirstNameTextBox") as TextBox;
        TextBox txtLastName = (sender as TextBox).Parent.FindControl("LastNameTextBox") as TextBox;

        txtUserName.Text = txtFirstName.Text + "." + txtLastName.Text;
        txtLastName.Focus();        
    }

    protected void OnLastNameTextChanged(object sender, EventArgs e)
    {
        TextBox txtUserName = (sender as TextBox).Parent.FindControl("UsernameTextBox") as TextBox;
        TextBox txtFirstName = (sender as TextBox).Parent.FindControl("FirstNameTextBox") as TextBox;
        TextBox txtLastName = (sender as TextBox).Parent.FindControl("LastNameTextBox") as TextBox;

        txtUserName.Focus();
        ((AjaxControlToolkit.TextBoxWatermarkExtender)(sender as TextBox).Parent.FindControl("wkLirstName")).Enabled  = false;
        txtUserName.Text = txtFirstName.Text + "." + txtLastName.Text;        
    }

	protected void linqUsers_Inserting ( object sender, LinqDataSourceInsertEventArgs e )
	{
		if ( this.lvwUsers.InsertItem != null )
		{
			DropDownList ddlO = this.lvwUsers.InsertItem.FindControl("OrganizationIDDropDown") as DropDownList;
			DropDownList ddlG = this.lvwUsers.InsertItem.FindControl("UserGroupIDDropDown") as DropDownList;
			MultiIpuntControl cloEm = this.lvwUsers.InsertItem.FindControl("micEmail") as MultiIpuntControl;
			MultiIpuntControl cloMp = this.lvwUsers.InsertItem.FindControl("micMobilePhone") as MultiIpuntControl;

			User user = e.NewObject as User;

			if ( user != null && ddlO != null && ddlG != null )
			{
				user.OrganizationID = Convert.ToInt32(ddlO.SelectedValue);
				user.UserGroupID = Convert.ToInt32(ddlG.SelectedValue);

				if ( cloEm != null ) user.Email = cloEm.Text;
				if ( cloMp != null ) user.MobilePhone = cloMp.Text;
			}
		}
	}

	protected void linqUsers_Updating ( object sender, LinqDataSourceUpdateEventArgs e )
	{
		if ( this.lvwUsers.EditIndex > -1 )
		{
			DropDownList ddlO = this.lvwUsers.Items[this.lvwUsers.EditIndex].FindControl("OrganizationIDDropDown") as DropDownList;
			DropDownList ddlG = this.lvwUsers.Items[this.lvwUsers.EditIndex].FindControl("UserGroupIDDropDown") as DropDownList;
			MultiIpuntControl cloEm = this.lvwUsers.Items[this.lvwUsers.EditIndex].FindControl("micEmail") as MultiIpuntControl;
			MultiIpuntControl cloMp = this.lvwUsers.Items[this.lvwUsers.EditIndex].FindControl("micMobilePhone") as MultiIpuntControl;

			User user = e.NewObject as User;

			if ( user != null && ddlO != null && ddlG != null )
			{
				user.OrganizationID = Convert.ToInt32(ddlO.SelectedValue);
				user.UserGroupID = Convert.ToInt32(ddlG.SelectedValue);

				if ( cloEm != null ) user.Email = cloEm.Text;
				if ( cloMp != null ) user.MobilePhone = cloMp.Text;
			}
		}
	}
	# endregion

	# region Utility Methods
	private void SetAddButtonVisible ( ListView lstView, string buttonName, bool visible )
	{
		ImageButton imgbAdd = lstView.FindControl(buttonName) as ImageButton;
		if ( imgbAdd != null ) 
		{
			imgbAdd.Visible = visible;
		}
	}

	private void SetImageButtonOverBehavior ( Control parent, string buttonName, string defaultImageName, bool pressedStatus )
	{
		ImageButton imgbutton = parent.FindControl(buttonName) as ImageButton;
		this.SetImageButtonOverBehavior(imgbutton, defaultImageName, pressedStatus);
	}

	private void SetImageButtonOverBehavior ( ImageButton imgbutton, string defaultImageName, bool pressedStatus )
	{
		if ( imgbutton != null )
		{
			imgbutton.Attributes.Add("OnMouseOver", string.Format("this.src = 'Images/{0}_over.gif'", defaultImageName));
			imgbutton.Attributes.Add("OnMouseOut", string.Format("this.src = 'Images/{0}.gif'", defaultImageName));
			if ( pressedStatus ) imgbutton.Attributes.Add("OnMouseDown", string.Format("this.src = 'Images/{0}_pressed.gif'", defaultImageName));
		}
	}
	# endregion	


    protected void btnNewEmail_OnClick(object sender, EventArgs e)
    {

        Control parent = (sender as Button).Parent;
        GridView gv = parent.FindControl("gvEmails") as GridView;
        TextBox txt = parent.FindControl("txtNewEmail") as TextBox;

        IList<string> ls = new List<string>();       
        foreach (GridViewRow gvr in gv.Rows)
        {
            ls.Add((gvr.Cells[0].FindControl("txtEmail") as TextBox).Text);
        }
        ls.Add(txt.Text);
        txt.Text = string.Empty;

        gv.DataSource = ls;
        gv.DataBind();
    }

    public void gvEmails_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gv = sender as GridView;
        IList<string> ls = new List<string>();
        foreach (GridViewRow gvr in gv.Rows)
        {
            if (gvr.RowIndex != e.RowIndex)
                ls.Add((gvr.Cells[0].FindControl("txtEmail") as TextBox).Text);
        }
        gv.DataSource = ls;
        gv.DataBind();
    }

    protected void FirstNameTextBox_OnLoad(object sender, EventArgs e)
    {
        TextBox txt = sender as TextBox;
        TextBox txtLastName = txt.Parent.FindControl("LastNameTextBox") as TextBox;
        TextBox txtUser = txt.Parent.FindControl("UsernameTextBox") as TextBox;
        txt.Attributes.Add("onchange", string.Format("FillUserName('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');", txt.ClientID, txtLastName.ClientID, txtUser.ClientID, txt.Parent.FindControl("wkFirstName").ClientID, txt.Parent.FindControl("wkLirstName").ClientID, txt.Parent.FindControl("wkUsername").ClientID));
    }

    protected void LastNameTextBox_OnLoad(object sender, EventArgs e)
    {
        TextBox txt = sender as TextBox;
        TextBox txtFirtName = txt.Parent.FindControl("FirstNameTextBox") as TextBox;
        TextBox txtUser = txt.Parent.FindControl("UsernameTextBox") as TextBox;
        txt.Attributes.Add("onchange", string.Format("FillUserName('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');", txtFirtName.ClientID, txt.ClientID, txtUser.ClientID, txt.Parent.FindControl("wkFirstName").ClientID, txt.Parent.FindControl("wkLirstName").ClientID, txt.Parent.FindControl("wkUsername").ClientID));
    }

}

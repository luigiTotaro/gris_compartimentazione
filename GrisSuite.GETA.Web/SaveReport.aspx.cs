﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using GrisSuite.GETA.Data;
using System.Text.RegularExpressions;

public partial class SaveReport : System.Web.UI.Page
{
	public string SelectedQueryName
	{
		get{ return (this.ViewState["SelectedQueryName"] ?? "").ToString(); }
		set { this.ViewState["SelectedQueryName"] = value; }
	}
	
	protected void Page_Load ( object sender, EventArgs e )
	{
		if ( !Utility.IsUserInAdminGroup() )
		{
			this.Response.Redirect("~/Default.aspx");
		}

		( (IGetaMasterPage)this.Master ).GetaLayoutMode = GetaLayoutModeEnum.Reports;

		this.SetImageButtonOverBehavior(this.lvwReports, "imgbQryAdd", "lightadd", true);
	}

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        lvwReports.DataBind();
        lvwReports.SelectedIndex = -1;
        updReports.Update();
    }

	protected void imgbQryAdd_Click ( object sender, EventArgs e )
	{
		this.SelectedQueryName = string.Empty;
		this.tblReport.Visible = true;
		this.txtQueryName.Text = string.Empty;
		this.txtQueryText.Text = string.Empty;

        this.lvwReports.SelectedIndex = -1;

		this.updReport.Update();
	}

	protected void lvwReports_SelectedIndexChanged ( object sender, EventArgs e )
	{
		string queryName = this.lvwReports.SelectedDataKey.Value.ToString();
		
		if ( !string.IsNullOrEmpty(queryName) )
		{			
			DBSavedReportQueriesDataContext db = new DBSavedReportQueriesDataContext();
			SavedReportQuery query = db.SavedReportQueries.SingleOrDefault(qry => qry.QueryName == queryName);
			
			if ( query != null )
			{
				this.SelectedQueryName = queryName;
				this.tblReport.Visible = true;
				this.txtQueryName.Text = query.QueryName;
				this.txtQueryText.Text = query.QueryText;

				this.updReport.Update();
			}
		}
	}

	protected void btnSaveQuery_Click ( object sender, EventArgs e )
	{
		if ( this.IsValid )
		{
			if ( string.IsNullOrEmpty(this.SelectedQueryName) )
			{
				//inserimento
				SavedReportQuery query = new SavedReportQuery();

				// validazione della query in input
				Regex regex = new Regex(@"insert\s+into", RegexOptions.Compiled | RegexOptions.IgnoreCase);
				query.QueryText = regex.Replace(this.txtQueryText.Text, "");

				regex = new Regex(@"update\s+\w+\s+set", RegexOptions.Compiled | RegexOptions.IgnoreCase);
				query.QueryText = regex.Replace(query.QueryText, "");

				regex = new Regex(@"delete\s+from", RegexOptions.Compiled | RegexOptions.IgnoreCase);
				query.QueryText = regex.Replace(query.QueryText, "");

				regex = new Regex(@"drop\s+", RegexOptions.Compiled | RegexOptions.IgnoreCase);
				query.QueryText = regex.Replace(query.QueryText, "");
				
				query.QueryName = this.txtQueryName.Text;
				
				DBSavedReportQueriesDataContext db = new DBSavedReportQueriesDataContext();
				db.SavedReportQueries.InsertOnSubmit(query);

				db.SubmitChanges();
				//try
				//{
				//    db.SubmitChanges();
				//}
				//catch ( Exception exc )
				//{
				//    //
				//}
			}
			else
			{
				//modifica
				DBSavedReportQueriesDataContext db = new DBSavedReportQueriesDataContext();
				SavedReportQuery query = db.SavedReportQueries.SingleOrDefault(qry => qry.QueryName == this.SelectedQueryName);
				
				if ( query != null )
				{
					// validazione della query in input
					Regex regex = new Regex(@"insert\s+into", RegexOptions.Compiled | RegexOptions.IgnoreCase);
					query.QueryText = regex.Replace(this.txtQueryText.Text, "");

					regex = new Regex(@"update\s+\w+\s+set", RegexOptions.Compiled | RegexOptions.IgnoreCase);
					query.QueryText = regex.Replace(query.QueryText, "");

					regex = new Regex(@"delete\s+from", RegexOptions.Compiled | RegexOptions.IgnoreCase);
					query.QueryText = regex.Replace(query.QueryText, "");

					regex = new Regex(@"drop\s+", RegexOptions.Compiled | RegexOptions.IgnoreCase);
					query.QueryText = regex.Replace(query.QueryText, "");

					query.QueryName = this.txtQueryName.Text;

					db.SubmitChanges();
					
					this.txtQueryText.Text = query.QueryText;
					this.lblMessage.Text = "Query modificata.";
				}
			}
			
			this.lvwReports.DataBind();
			this.updReports.Update();
		}
	}

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.tblReport.Visible = false;
        this.lvwReports.SelectedIndex = -1;
        this.updReports.Update();
    }

	private void SetImageButtonOverBehavior ( Control parent, string buttonName, string defaultImageName, bool pressedStatus )
	{
		ImageButton imgbutton = parent.FindControl(buttonName) as ImageButton;
		this.SetImageButtonOverBehavior(imgbutton, defaultImageName, pressedStatus);
	}

	private void SetImageButtonOverBehavior ( ImageButton imgbutton, string defaultImageName, bool pressedStatus )
	{
		if ( imgbutton != null )
		{
			imgbutton.Attributes.Add("OnMouseOver", string.Format("this.src = 'Images/{0}_over.gif'", defaultImageName));
			imgbutton.Attributes.Add("OnMouseOut", string.Format("this.src = 'Images/{0}.gif'", defaultImageName));
			if ( pressedStatus ) imgbutton.Attributes.Add("OnMouseDown", string.Format("this.src = 'Images/{0}_pressed.gif'", defaultImageName));
		}
	}
}

/*
This script was created by Visual Studio on 18/09/2008 at 18.12.
Run this script on crisdev.TelefinCentralPRO.dbo to make it the same as GrisSuite.DB.
Please back up your target database before running this script.
*/
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [geta].[issue_users]'
GO
ALTER TABLE [geta].[issue_users] DROP
CONSTRAINT [FK_issue_users_issues]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [geta].[issues]'
GO
ALTER TABLE [geta].[issues] DROP
CONSTRAINT [FK_issues_target_types],
CONSTRAINT [FK_issues_issue_activities],
CONSTRAINT [FK_issues_issue_states]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [geta].[target_values]'
GO
ALTER TABLE [geta].[target_values] DROP
CONSTRAINT [FK_target_values_target_attributes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [geta].[target_attributes]'
GO
ALTER TABLE [geta].[target_attributes] DROP
CONSTRAINT [FK_target_attributes_target_types]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [geta].[target_values_history]'
GO
ALTER TABLE [geta].[target_values_history] DROP
CONSTRAINT [FK_target_values_history_target_values]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[issues]'
GO
ALTER TABLE [geta].[issues] DROP CONSTRAINT [PK_issues]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[issues]'
GO
ALTER TABLE [geta].[issues] DROP CONSTRAINT [DF_issues_IssueActivityID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[issues]'
GO
ALTER TABLE [geta].[issues] DROP CONSTRAINT [DF_issues_DateCreation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[issues]'
GO
ALTER TABLE [geta].[issues] DROP CONSTRAINT [DF_issues_IssueStateID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[issue_activities]'
GO
ALTER TABLE [geta].[issue_activities] DROP CONSTRAINT [PK_issue_activities]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_attributes]'
GO
ALTER TABLE [geta].[target_attributes] DROP CONSTRAINT [PK_target_attributes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_attributes]'
GO
ALTER TABLE [geta].[target_attributes] DROP CONSTRAINT [DF_target_attributes_IsObsolete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_attributes]'
GO
ALTER TABLE [geta].[target_attributes] DROP CONSTRAINT [DF_target_attributes_TargetAttribute]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_attributes]'
GO
ALTER TABLE [geta].[target_attributes] DROP CONSTRAINT [DF_target_attributes_MainAttribute]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_values]'
GO
ALTER TABLE [geta].[target_values] DROP CONSTRAINT [PK_target_values]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_values_history]'
GO
ALTER TABLE [geta].[target_values_history] DROP CONSTRAINT [PK_target_values_history]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [geta].[target_values_history]'
GO
ALTER TABLE [geta].[target_values_history] DROP CONSTRAINT [DF_target_values_history_DateCreation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_U_target_attributes] from [geta].[target_attributes]'
GO
DROP INDEX [IX_U_target_attributes] ON [geta].[target_attributes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [geta].[IssuesByTargetID_Get]'
GO
DROP PROCEDURE [geta].[IssuesByTargetID_Get]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [geta].[GetHistoryValuesEvolutionByTargetID]'
GO
DROP FUNCTION [geta].[GetHistoryValuesEvolutionByTargetID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [geta].[target_values_history]'
GO
DROP TABLE [geta].[target_values_history]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [geta].[target_attributes]'
GO
DROP TABLE [geta].[target_attributes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [geta].[issue_activities]'
GO
DROP TABLE [geta].[issue_activities]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [geta].[target_values]'
GO
DROP TABLE [geta].[target_values]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [geta].[issue_activity_types]'
GO
CREATE TABLE [geta].[issue_activity_types]
(
[IssueActivityTypeID] [int] NOT NULL,
[IssueActivityTypeName] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_issue_activity_types] on [geta].[issue_activity_types]'
GO
ALTER TABLE [geta].[issue_activity_types] ADD CONSTRAINT [PK_issue_activity_types] PRIMARY KEY CLUSTERED  ([IssueActivityTypeID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [geta].[activity_groups]'
GO
CREATE TABLE [geta].[activity_groups]
(
[ActivityGroupID] [int] NOT NULL IDENTITY(1, 1),
[ActivityGroupName] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[Order] [tinyint] NOT NULL CONSTRAINT [DF_activity_groups_Order] DEFAULT ((0))
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_activity_groups] on [geta].[activity_groups]'
GO
ALTER TABLE [geta].[activity_groups] ADD CONSTRAINT [PK_activity_groups] PRIMARY KEY CLUSTERED  ([ActivityGroupID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[servers]'
GO
ALTER TABLE [dbo].[servers] ADD
[InExercise] [bit] NOT NULL CONSTRAINT [DF_servers_InExercise] DEFAULT ((0)),
[NodID] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [geta].[issues]'
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET XACT_ABORT ON
GO
BEGIN TRANSACTION
CREATE TABLE [geta].[tmp_ms_xx_issues]
(
[IssueID] [int] NOT NULL IDENTITY(1, 1),
[TargetID] [bigint] NOT NULL,
[TargetTypeID] [int] NULL,
[TargetActivityID] [int] NULL,
[IssueActivityTypeID] [int] NOT NULL CONSTRAINT [DF_issues_IssueActivityID] DEFAULT ((1)),
[TargetActivityValue] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[IssueComment] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[DateCreation] [datetime] NOT NULL CONSTRAINT [DF_issues_DateCreation] DEFAULT (getdate()),
[DateIssue] [datetime] NOT NULL,
[IssueStateID] [int] NOT NULL CONSTRAINT [DF_issues_IssueStateID] DEFAULT ((1)),
[IssueChildID] [int] NULL,
[IsClosed] [bit] NOT NULL CONSTRAINT [DF_issues_IsClosed] DEFAULT ((0)),
[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_issues_IsDeleted] DEFAULT ((0)),
[IssueElapsedTime] [int] NOT NULL CONSTRAINT [DF_issues_IssueElapsedTime] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET IDENTITY_INSERT [geta].[tmp_ms_xx_issues] ON
INSERT INTO [geta].[tmp_ms_xx_issues]([IssueID], [TargetID], [TargetTypeID], [IssueComment], [DateCreation], [DateIssue], [IssueStateID]) SELECT [IssueID], [TargetID], [TargetTypeID], [IssueComment], [DateCreation], [DateIssue], [IssueStateID] FROM [geta].[issues]
SET IDENTITY_INSERT [geta].[tmp_ms_xx_issues] OFF
DROP TABLE [geta].[issues]
EXEC sp_rename N'[geta].[tmp_ms_xx_issues]', N'issues'
COMMIT TRANSACTION
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
PRINT N'Creating primary key [PK_issues] on [geta].[issues]'
GO
ALTER TABLE [geta].[issues] ADD CONSTRAINT [PK_issues] PRIMARY KEY CLUSTERED  ([IssueID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [geta].[Users_Get]'
GO
-- =============================================
-- Author:		Cristian Storti
-- Create date: 26-06-2008
-- Description:	
-- =============================================
ALTER PROCEDURE [geta].[Users_Get]
(
	@UserGroupID int = -1,
	@OrganizationID int = -1
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		users.UserID, users.Username, users.FirstName, users.LastName, 
		(users.Username + ISNULL(' (' + users.LastName + ' ' + users.FirstName + ')', '')) as [Name], users.OrganizationID, 
		users.UserGroupID, users.MobilePhone, users.Email, COUNT(geta.issue_users.IssueUserID) as IssuesCount
	FROM users 
	LEFT JOIN geta.issue_users ON users.UserID = geta.issue_users.UserID
	WHERE (UserGroupID = @UserGroupID OR @UserGroupID = -1) AND (OrganizationID = @OrganizationID OR @OrganizationID = -1)
	GROUP BY 
		users.UserID, users.Username, users.FirstName, users.LastName, users.OrganizationID, 
		users.UserGroupID, users.MobilePhone, users.Email
	ORDER BY users.Username;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [geta].[target_activities]'
GO
CREATE TABLE [geta].[target_activities]
(
[TargetActivityID] [int] NOT NULL IDENTITY(1, 1),
[TargetActivityName] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[IsObsolete] [bit] NOT NULL CONSTRAINT [DF_target_activities_IsObsolete] DEFAULT ((0)),
[ControlTypeUI] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[TargetTypeID] [int] NOT NULL,
[EnumValues] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Order] [tinyint] NOT NULL,
[ActivityGroupsID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_target_activities] on [geta].[target_activities]'
GO
ALTER TABLE [geta].[target_activities] ADD CONSTRAINT [PK_target_activities] PRIMARY KEY CLUSTERED  ([TargetActivityID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[saved_report_queries]'
GO
CREATE TABLE [dbo].[saved_report_queries]
(
[QueryName] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[QueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_saved_report_queries] on [dbo].[saved_report_queries]'
GO
ALTER TABLE [dbo].[saved_report_queries] ADD CONSTRAINT [PK_saved_report_queries] PRIMARY KEY CLUSTERED  ([QueryName]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [geta].[TargetAttributes_Get]'
GO
-- =============================================
-- Author:		Cristian Storti
-- Create date: 05-08-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributes_Get] 
(
	@TargetTypeID int = -1,
	@Namespace varchar(256) = ''
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TargetAttributeID, TargetAttributeName, [Namespace], IsObsolete, ControlTypeUI, TargetTypeID, MainAttribute, EnumValues, [Order]
	FROM geta.target_attributes
	WHERE (TargetTypeID = @TargetTypeID OR @TargetTypeID = -1)
	AND ([Namespace] = @Namespace OR @Namespace = '')
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [geta].[TargetAttributeValuesByTargetID_Get]'
GO
-- =============================================
-- Author:		Cristian Storti
-- Create date: 06-08-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributeValuesByTargetID_Get] 
	@TargetID BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TargetValueID, TargetAttributeID, TargetID, [Value]
	FROM geta.target_values
	WHERE (TargetID = @TargetID)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [geta].[target_activities]'
GO
ALTER TABLE [geta].[target_activities] ADD
CONSTRAINT [FK_target_activities_activity_groups] FOREIGN KEY ([ActivityGroupsID]) REFERENCES [geta].[activity_groups] ([ActivityGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [geta].[target_activities] ADD
CONSTRAINT [FK_target_activities_target_types] FOREIGN KEY ([TargetTypeID]) REFERENCES [geta].[target_types] ([TargetTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [geta].[issues]'
GO
ALTER TABLE [geta].[issues] ADD
CONSTRAINT [FK_issues_issue_activity_types] FOREIGN KEY ([IssueActivityTypeID]) REFERENCES [geta].[issue_activity_types] ([IssueActivityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [geta].[issues] ADD
CONSTRAINT [FK_issues_issues] FOREIGN KEY ([IssueChildID]) REFERENCES [geta].[issues] ([IssueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [geta].[issues] ADD
CONSTRAINT [FK_issues_target_types] FOREIGN KEY ([TargetTypeID]) REFERENCES [geta].[target_types] ([TargetTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [geta].[issues] ADD
CONSTRAINT [FK_issues_target_activities] FOREIGN KEY ([TargetActivityID]) REFERENCES [geta].[target_activities] ([TargetActivityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [geta].[issues] ADD
CONSTRAINT [FK_issues_issue_states] FOREIGN KEY ([IssueStateID]) REFERENCES [geta].[issue_states] ([IssueStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [geta].[issue_users]'
GO
ALTER TABLE [geta].[issue_users] ADD
CONSTRAINT [FK_issue_users_issues] FOREIGN KEY ([IssueID]) REFERENCES [geta].[issues] ([IssueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[servers]'
GO
ALTER TABLE [dbo].[servers] ADD
CONSTRAINT [FK_servers_nodes] FOREIGN KEY ([NodID]) REFERENCES [dbo].[nodes] ([NodID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded.'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed.'
GO
DROP TABLE #tmpErrors
GO

---------------------------------------------------------

PRINT 'Inserting static data.'
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
BEGIN TRANSACTION

UPDATE [dbo].[parameters] SET [ParameterValue]='1.3.1.0 g' WHERE [ParameterName]='DBVersion'

DELETE FROM [geta].[issue_users]
DELETE FROM [geta].[issues]

SET IDENTITY_INSERT [geta].[organizations] ON
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (1, 'Telefin')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (2, 'Eurotel')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (3, 'Temmi')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (4, 'Effegi')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (5, 'Smiet')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (6, 'Sifel')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (7, 'Sie-fer')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (8, 'TSF')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (9, 'Computerway')
INSERT INTO [geta].[organizations] ([OrganizationID], [OrganizationName]) VALUES (10, 'N.P.')
SET IDENTITY_INSERT [geta].[organizations] OFF

SET IDENTITY_INSERT [geta].[activity_groups] ON
INSERT INTO [geta].[activity_groups] ([ActivityGroupID], [ActivityGroupName], [Order]) VALUES (1, 'Setup STLC1000', 0)
INSERT INTO [geta].[activity_groups] ([ActivityGroupID], [ActivityGroupName], [Order]) VALUES (3, 'Eprom PZ', 1)
INSERT INTO [geta].[activity_groups] ([ActivityGroupID], [ActivityGroupName], [Order]) VALUES (5, 'Diagnostica Monitors', 2)
INSERT INTO [geta].[activity_groups] ([ActivityGroupID], [ActivityGroupName], [Order]) VALUES (2, 'Sistema SARA', 3)
INSERT INTO [geta].[activity_groups] ([ActivityGroupID], [ActivityGroupName], [Order]) VALUES (4, 'Validazione STLC1000', 4)
SET IDENTITY_INSERT [geta].[activity_groups] OFF

INSERT INTO [geta].[issue_activity_types] ([IssueActivityTypeID], [IssueActivityTypeName]) VALUES (1, 'Intervento Generico')
INSERT INTO [geta].[issue_activity_types] ([IssueActivityTypeID], [IssueActivityTypeName]) VALUES (2, 'Da Resettare')
INSERT INTO [geta].[issue_activity_types] ([IssueActivityTypeID], [IssueActivityTypeName]) VALUES (3, 'Da Sostituire')
INSERT INTO [geta].[issue_activity_types] ([IssueActivityTypeID], [IssueActivityTypeName]) VALUES (4, 'Da Fare')

UPDATE [dbo].[users] SET [FirstName]='Damiano', [LastName]='Marchi', [OrganizationID]=1, [UserGroupID]=2, [MobilePhone]='0458100404', [Email]='marchi@telefin.it' WHERE [UserID]=1
UPDATE [dbo].[users] SET [FirstName]='Diego', [LastName]='Nalin', [OrganizationID]=1, [UserGroupID]=2, [MobilePhone]='0458100404', [Email]='nalin@telefin.it' WHERE [UserID]=14

INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Michael.Stiller', 'Michael', 'Stiller', 9, 1, '3299282699', 'stiller_m@msn.com')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Michele.Melchioni', 'Michele', 'Melchioni', 4, 1, '3494601082', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Pietro.Michea', 'Pietro', 'Michea', 4, 1, '3295342441|3925654421|3482735703', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Rosario', 'Rosario', '???', 4, 1, '3400543629|3494601082', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Severino.Cascitelli', 'Severino', 'Cascitelli', 4, 1, '3482559107', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Paolo.Castiglioni', 'Paolo', 'Castiglioni', 3, 1, '3299282639', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Mario.Sferra', 'Mario', 'Sferra', 5, 1, '3395839928|3932808424', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Pietro.Castronovo', 'Pietro', 'Castronovo', 6, 1, '3388637114', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Paolo.Renda', 'Paolo', 'Renda', 7, 1, '3899724981', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Giorgio', 'Giorgio', '???', 2, 1, '3138041715|3483180274', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Giordano.Baldassare', 'Giordano', 'Baldassare', 2, 1, '3334080095', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Alan.Formentin', 'Alan', 'Formentin', 9, 1, '3487814655', 'aaa@ciao.com')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Mirco.Venturi', 'mirco', 'Venturi', 8, 1, '3497843509', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Paolo.Barca', 'Paolo', 'Barca', 10, 1, '3204082086', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Massimo.Ragusa', 'Massimo', 'Ragusa', 10, 1, '3478496087|3204082089', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Salvatore.Barone', 'Salvatore', 'Barone', 10, 1, '3894215818', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Enzo.Sanmarco', 'Enzo', 'Sanmarco', 10, 1, '3316825893', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('MASTER-RETE\e.alborali', 'Enrico', 'Alborali', 1, 2, '0458100404', 'alborali@telefin.it')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('MASTER-RETE\l.ferrarini', 'Luigi', 'Ferrarini', 1, 2, '0458100404', 'ferrarini@telefin.it')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Andrea.Righetti', 'Andrea', 'Righetti', 1, 2, '0458100404', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Marco.Tronci', 'Marco', 'Tronci', 1, 2, '0458100404', '')
INSERT INTO [dbo].[users] ([Username], [FirstName], [LastName], [OrganizationID], [UserGroupID], [MobilePhone], [Email]) VALUES ('Antonio.Pacilio', 'Antonio', 'Pacilio', 5, 1, '3407249159', '')

SET IDENTITY_INSERT [geta].[target_activities] ON
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (1, 'Versione Software', 0, 'ComboBox', 2, '1.0.3.0|1.2.0.0|1.2.0.1|1.2.0.2|1.2.0.3|1.2.1.0|1.2.1.1|1.2.1.2|1.2.1.3|1.4.0.0', 0, 1)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (2, 'Funzionamento Periferiche Telefin', 0, 'CheckBox', 2, NULL, 1, 1)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (3, 'Posizione Periferiche', 0, 'CheckBox', 2, NULL, 2, 1)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (4, 'PZAutoConfigurator', 0, 'ComboBox', 2, '1.0.3.2|1.0.3.3', 3, 1)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (6, 'Eprom', 0, 'ComboBox', 2, 'Installata|Non Installata|Non Necessaria', 0, 2)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (9, 'Monitors', 0, 'ComboBox', 2, 'Presenti|Non Presenti', 0, 3)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (5, 'Sara', 0, 'ComboBox', 2, 'Installato|Non Installato|Non Necessario', 0, 4)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (7, 'Bollino Verde', 0, 'CheckBox', 2, NULL, 0, 5)
INSERT INTO [geta].[target_activities] ([TargetActivityID], [TargetActivityName], [IsObsolete], [ControlTypeUI], [TargetTypeID], [EnumValues], [Order], [ActivityGroupsID]) VALUES (8, 'Conferma Mail', 0, 'CheckBox', 2, NULL, 1, 5)
SET IDENTITY_INSERT [geta].[target_activities] OFF

UPDATE servers SET servers.NodID = Map.NodID
FROM servers
INNER JOIN 
(
	SELECT servers.SrvID, MIN(nodes.NodID) AS NodID
	FROM servers 
	INNER JOIN devices ON servers.SrvID = devices.SrvID 
	INNER JOIN nodes ON devices.NodID = nodes.NodID
	GROUP BY servers.SrvID
	HAVING COUNT(DISTINCT nodes.NodID) = 1
) AS Map ON Map.SrvID = servers.SrvID

UPDATE servers SET NodID = 1700938186762 WHERE SrvID = 87294108; -- STLC0216.rfi.it -> Albano Laziale
UPDATE servers SET NodID = 7108302012426 WHERE SrvID = 103612422; -- STLC0157.rfiservizi.corp -> Marino Laziale
UPDATE servers SET NodID = 4509846863882 WHERE SrvID = 103612425; -- STLC0167.rfiservizi.corp -> Cecchina
UPDATE servers SET NodID = 7078237372426 WHERE SrvID = 103612426; -- STLC0212.rfiservizi.corp -> Marechiaro
UPDATE servers SET NodID = 11764046954506 WHERE SrvID = 118292492; -- STLC0386.rfiservizi.corp -> Sipicciano
UPDATE servers SET NodID = 7228548710408 WHERE SrvID = 120782973; -- TELEFIN-APR9SZZ -> Medesano
UPDATE servers SET NodID = 8199211319304 WHERE SrvID = 120783024; -- TELEFIN-22JZ2N6 -> Noceto

COMMIT TRANSACTION
PRINT 'Static data inserted.'
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using GrisSuite.GETA.Data;

public partial class MasterPage : System.Web.UI.MasterPage, IGetaMasterPage
{
	# region Properties
	public GetaLayoutModeEnum GetaLayoutMode
	{
		get 
		{ 
			if ( this.imgAdmin.ImageUrl.IndexOf("selected") != -1 ) return GetaLayoutModeEnum.Administration;
			else if ( this.imgReports.ImageUrl.IndexOf("selected") != -1 ) return GetaLayoutModeEnum.Reports;
			else return GetaLayoutModeEnum.Issues;
		}
		
		set 
		{
			this.imgAdmin.ImageUrl.Replace("_selected", "");
			this.imgIssues.ImageUrl.Replace("_selected", "");
			this.imgReports.ImageUrl.Replace("_selected", "");
			
			switch (value)
			{
				case GetaLayoutModeEnum.Reports:
				{
                    if (!this.imgReports.ImageUrl.Contains("_selected")) this.imgReports.ImageUrl = this.imgReports.ImageUrl.Replace(".", "_selected.");
					//this.Response.Redirect("~/Reports.aspx");
					break;
				}
				case GetaLayoutModeEnum.Administration:
				{
                    if (!this.imgAdmin.ImageUrl.Contains("_selected")) this.imgAdmin.ImageUrl = this.imgAdmin.ImageUrl.Replace(".", "_selected.");
					//this.Response.Redirect("~/UsersOrganizations.aspx");
					break;
				}
				default:
				{
                    if (!this.imgIssues.ImageUrl.Contains("_selected")) this.imgIssues.ImageUrl = this.imgIssues.ImageUrl.Replace(".", "_selected.");
					//this.Response.Redirect("~/Default.aspx");
					break;
				}
			}
		}
	}
	# endregion
	
	protected void Page_Load ( object sender, EventArgs e )
	{
		this.imgAdmin.Style.Add(HtmlTextWriterStyle.VerticalAlign, "Bottom");
		this.imgIssues.Style.Add(HtmlTextWriterStyle.VerticalAlign, "Bottom");
		this.imgReports.Style.Add(HtmlTextWriterStyle.VerticalAlign, "Bottom");

		# region Gestione stato over dei pulsanti nel menu
		if ( this.imgAdmin.ImageUrl.IndexOf("selected") == -1 )
		{
			this.imgAdmin.Attributes.Add("OnMouseOver", "this.src='Images/head_admin_over.gif';");
			this.imgAdmin.Attributes.Add("OnMouseOut", "this.src='Images/head_admin.gif';");		
		}
		
		if ( this.imgIssues.ImageUrl.IndexOf("selected") == -1 )
		{
			this.imgIssues.Attributes.Add("OnMouseOver", "this.src='Images/head_issues_over.gif';");
			this.imgIssues.Attributes.Add("OnMouseOut", "this.src='Images/head_issues.gif';");
		}
		
		if ( this.imgReports.ImageUrl.IndexOf("selected") == -1 )
		{
			this.imgReports.Attributes.Add("OnMouseOver", "this.src='Images/head_report_over.gif';");
			this.imgReports.Attributes.Add("OnMouseOut", "this.src='Images/head_report.gif';");
		}
		# endregion
		
		if ( !this.IsPostBack )
		{
			this.imgIssues.PostBackUrl = string.Format("{0}?nod={1}", this.imgIssues.PostBackUrl, Utility.NodeID);
			
			DBSTLCDataContext dbCtx = new DBSTLCDataContext();
			ServerInfo server = dbCtx.ServerInfos.SingleOrDefault(srv => srv.SrvID == Utility.NodeID);
			if ( server != null ) this.lblTarget.Text = server.FullHostName;
		}
	}

	protected void smMain_AsyncPostBackError ( object sender, AsyncPostBackErrorEventArgs e )
	{
		string sMessage = "";
		string sStackTrace;

		//if ( e.Exception != null ) Tracer.TraceMessage(this.Context, e.Exception, true);

		if ( e.Exception.InnerException != null )
		{
			sMessage = e.Exception.InnerException.Message != null ? e.Exception.InnerException.Message : string.Empty;
			sStackTrace = e.Exception.InnerException.Message != null ? e.Exception.InnerException.StackTrace : string.Empty;
		}
		else
		{
			sMessage = e.Exception.Message != null ? e.Exception.Message : string.Empty;
			sStackTrace = e.Exception.Message != null ? e.Exception.StackTrace : string.Empty;
		}

		//System.Diagnostics.Trace.TraceError("Error -> " + sMessage + ", Stack Trace -> " + sStackTrace + ", User -> " + Utility.GetUserInfos(true));

		this.smMain.AsyncPostBackErrorMessage = sMessage;
	}
	
	# region Methods
	# endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiIpuntControl : System.Web.UI.UserControl
{

    public string Text
    {
        get 
        {
            string sEmail = string.Empty;
            foreach (GridViewRow gvr in gvItems.Rows)
            {
                TextBox txt = gvr.Cells[0].FindControl("txtItem") as TextBox;
                if (!string.IsNullOrEmpty(txt.Text)) sEmail += txt.Text + "|";
            }
            if (!string.IsNullOrEmpty(sEmail)) sEmail = sEmail.Remove(sEmail.Length - 1);
            return sEmail;
        }
        set 
        {
            if (! string.IsNullOrEmpty(value))
            {
                IList<string> ls = new List<string>();
                foreach (string sStr in value.Split('|'))
                    if (!string.IsNullOrEmpty(sStr)) ls.Add(sStr);                                       
                gvItems.DataSource = ls;
                gvItems.DataBind();
            }
        }
    }

    public string ButtonLabel 
    {
        get { return btnNewItem.Text; }
        set { btnNewItem.Text = value; } 
    }

    public string ToolTip
    {
        get { return txtNewItem.ToolTip; }
        set { txtNewItem.ToolTip = value; }
    }

    public int MaxLength 
    {
        get { return txtNewItem.MaxLength; }
        set { txtNewItem.MaxLength = value; } 
    }

    public string ValidationError 
    {
        get { return revNewItem.ErrorMessage; }
        set { revNewItem.ErrorMessage = value; } 
    }

    public string ValidationExpression 
    {
        get { return revNewItem.ValidationExpression; }
        set { revNewItem.ValidationExpression = value; } 
    }

    public string ValidationGroup 
    {
        get { return revNewItem.ValidationGroup; }
        set { revNewItem.ValidationGroup = value; } 
    }

    private IList<string> GetList()
    {
        IList<string> ls = new List<string>();
        foreach (GridViewRow gvr in gvItems.Rows)
        {
            ls.Add((gvr.Cells[0].FindControl("txtItem") as TextBox).Text);
        }
        return ls;
    }


    protected void btnNewItem_OnClick(object sender, EventArgs e)
    {
        IList<string> ls = GetList();
        ls.Add(txtNewItem.Text);
        gvItems.DataSource = ls;
        gvItems.DataBind();

        txtNewItem.Text = string.Empty;        
    }

    public void gvItems_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gv = sender as GridView;
        IList<string> ls = new List<string>();
        foreach (GridViewRow gvr in gv.Rows)
        {
            if (gvr.RowIndex != e.RowIndex)
                ls.Add((gvr.Cells[0].FindControl("txtItem") as TextBox).Text);
        }
        gv.DataSource = ls;
        gv.DataBind();
    }
}

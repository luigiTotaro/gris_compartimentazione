﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Data.Linq;

using GrisSuite.GETA.Data;

[WebService(Namespace = "http://rfi.it/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AttributeNamespaces : System.Web.Services.WebService
{

	public AttributeNamespaces ()
	{
	}

    [WebMethod]
    public string[] GetCompletionList(string prefixText, int count)
    {
        DBIssuesDataContext db = new DBIssuesDataContext();
        var result = from attribute in db.ActivityGroups
                      select attribute.ActivityGroupName;
        return result.ToArray<string>();
    }

}


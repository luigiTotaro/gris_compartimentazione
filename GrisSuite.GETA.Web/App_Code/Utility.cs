﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using GrisSuite.GETA.Data;

public class Utility
{
    public static bool IsUserInAdminGroup()
    {
        if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupAdmin"]))
        {
            return false;
        }

        return HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupAdmin"]);
    }

    public static bool IsUserInGETAGroup()
    {
        if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["groupGETA"]))
        {
            return false;
        }

        return HttpContext.Current.User.IsInRole(ConfigurationManager.AppSettings["groupGETA"]);
    }

    public static string GetAbsoluteUrl(string tildeUrl)
    {
        if (HttpContext.Current != null)
        {
            HttpRequest request = HttpContext.Current.Request;

            string appUrl = (request.Url.Scheme + Uri.SchemeDelimiter + request.Url.Authority +
                             (request.ApplicationPath.EndsWith("/") ? request.ApplicationPath : request.ApplicationPath + "/"));

            return Path.Combine(appUrl, new Control().ResolveClientUrl(tildeUrl));
        }

        return "";
    }

    public static long NodeID
    {
        get
        {
            long nodeId;
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["SrvID"] != null &&
                long.TryParse(HttpContext.Current.Session["SrvID"].ToString(), out nodeId))
            {
                return nodeId;
            }

            return -1;
        }

        set
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["SrvID"] = value;
            }
        }
    }

    public static string GetNodeNameFromNodeID(long nodeId, bool withTopography)
    {
        if (nodeId > 0)
        {
            DBSTLCDataContext db = new DBSTLCDataContext();

            string nodeName = (from node in db.Nodes where node.NodID == nodeId select node.Name).SingleOrDefault();

            if (string.IsNullOrEmpty(nodeName))
            {
                return string.Empty;
            }

            if (withTopography)
            {
                string zoneName = (from node in db.Nodes where node.NodID == nodeId select node.Zone.Name).SingleOrDefault();
                string regionName = (from node in db.Nodes where node.NodID == nodeId select node.Zone.Region.Name).SingleOrDefault();
                return string.Format("{0} ({1} > {2})", nodeName, (regionName ?? string.Empty).Replace("Compartimento di ", string.Empty),
                                     (zoneName ?? string.Empty).Replace("Linea ", string.Empty));
            }

            return nodeName;
        }

        return string.Empty;
    }

    public static string GetZoneNameFromNodeID(long nodeId, bool withTopography)
    {
        if (nodeId > 0)
        {
            DBSTLCDataContext db = new DBSTLCDataContext();

            string zoneName = (from node in db.Nodes where node.NodID == nodeId select node.Zone.Name).SingleOrDefault();

            if (string.IsNullOrEmpty(zoneName))
            {
                return string.Empty;
            }

            if (withTopography)
            {
                string regionName = (from node in db.Nodes where node.NodID == nodeId select node.Zone.Region.Name).SingleOrDefault();
                return string.Format("{0} ({1})", zoneName, (regionName ?? string.Empty).Replace("Compartimento di ", string.Empty));
            }

            return zoneName;
        }

        return string.Empty;
    }

    public static long GetZoneIDFromNodeID(long nodeId)
    {
        if (nodeId > 0)
        {
            DBSTLCDataContext db = new DBSTLCDataContext();

            long zoneId = (from node in db.Nodes where node.NodID == nodeId select node.Zone.ZonID).SingleOrDefault();

            return zoneId > 0 ? zoneId : -1;
        }

        return -1;
    }

    public static int UserID
    {
        get
        {
            int userId;
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["UserID"] != null &&
                int.TryParse(HttpContext.Current.Session["UserID"].ToString(), out userId))
            {
                return userId;
            }

            return -1;
        }

        set
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["UserID"] = value;
            }
        }
    }

    /// <summary>
    /// Ritorna la stringa con il numero di serie decodificato dal Server ID
    /// </summary>
    /// <param name="servId">Server ID</param>
    /// <returns>Stringa con il numero di serie dell'STLC, nel formato 00.00.000 (anno, mese, progressivo)</returns>
    public static string GetSNFromSrvID(ulong servId)
    {
        return string.Format("{0:00}.{1:00}.{2:000}", ((servId & 0xFF000000) >> 24), ((servId & 0xFF0000) >> 16), (servId & 0xFFFF));
    }
}
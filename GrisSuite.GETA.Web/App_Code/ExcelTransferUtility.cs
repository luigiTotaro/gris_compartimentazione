using System;
using System.Data;
using System.Web;

namespace Edulife.Presentation.Web.Components 
{
    public class ExcelTransferUtility 
    {
        private HttpServerUtility _server;
        private string _headerFieldsBlock = "";
        private string _fileName;
        private string[] _excludeColumns;
        private string _result = "";
        private bool showNullAsEmptyString;
        private bool showHoursOnDates;

        public ExcelTransferUtility(HttpServerUtility server) {
            if (server == null) throw new ArgumentException("Il parametro passato non pu� essere nullo", "server");

            this._server = server;
            this.showNullAsEmptyString = false;
            this.showHoursOnDates = true;
        }

        public enum TransformationType {
            HTML,
            Excel
        }

        public string HeaderFieldsBlock {
            get { return this._headerFieldsBlock; }
            set { this._headerFieldsBlock = value; }
        }

        public string FileName {
            get { return this._fileName; }
            set { this._fileName = value; }
        }

        public bool ShowNullAsEmptyString {
            get {
                return this.showNullAsEmptyString;
            }
            set {
                this.showNullAsEmptyString = value;
            }
        }

        public bool ShowHoursOnDates {
            get {
                return this.showHoursOnDates;
            }
            set {
                this.showHoursOnDates = value;
            }
        }

        public string ExcludeColumns {
            get {
                if (this._excludeColumns != null)
                    return string.Join("|", this._excludeColumns);
                else
                    return null;
            }

            set {
                if (value != null && value.Length > 0) {
                    this._excludeColumns = value.Split('|');
                }
            }
        }

        public void Transfer(DataSet ds) {
            this.Transfer(ds, TransformationType.HTML);
        }

        public void Transfer(DataSet ds, TransformationType transType) {
            if (_server != null) {
                switch (transType) {
                    case TransformationType.HTML:
                        this._result = this.TransformToHTML(ds);

                        break;
                    case TransformationType.Excel:
                        this._result = this.TransformToExcelXML(ds);

                        break;
                }

                //_server.Transfer("~/ExcelTransfer.aspx", false);
            }
        }

        private string TransformToHTML(DataSet source) {
            System.IO.StringWriter htmlDoc;

            htmlDoc = new System.IO.StringWriter();

            string startTable = "<Table border=\"1\">";
            string endTable = "</Table>";

            string startTableHeader = "<Table border=\"0\">";
            string startRowHeader = "<TR>";
            string startCellHeader = "<TD style=\"mso-number-format:\\@;font-weight:bold\">";
            string endRowHeader = "</TR>";
            string endCellHeader = "</TD>";
            string endTableHeader = "</Table>";

            string startRowBody = "<TR>";
            string startCellBody = "<TD {0} >";
            string endRowBody = "</TR>";
            string endCellBody = "</TD>";

            string styleString = "style=\"mso-number-format:\\@;\"";
            string styleInt = "style=\"mso-number-format:0;text-align:right;\"";
            string styleDouble = "style='mso-number-format:\\#\\.\\#\\#0,00\\#\\#;text-align:right;'";

            string styleDateTime;

            if (this.showHoursOnDates) {
                styleDateTime = "style='mso-number-format:\"d\\/m\\/yy\\\\ h\\:mm\";'";
            }
            else {
                styleDateTime = "style='mso-number-format:\"Short Date\";'";
            }

            string styleBoolean = "style='mso-number-format:\"Yes\\/No\";text-align:center;'";
            string styleDecimal = "style='mso-number-format:\"\\0022&euro;\\0022\\\\ \\#\\,\\#\\#0\\.00\";text-align:right;'";

            int rowCount = 0;

            //Campi intestazione ->   | = nuova cella	� = a capo
            if (this._headerFieldsBlock.Length > 0) {
                string[] rows = null;
                string[] cols = null;

                htmlDoc.Write(startTableHeader);

                rows = this._headerFieldsBlock.Split('�');
                foreach (string row in rows) {
                    htmlDoc.Write(startRowHeader);
                    cols = row.Split('|');

                    foreach (string col in cols) {
                        htmlDoc.Write(startCellHeader); //i campi intestazione sono esportati solo come stringa
                        htmlDoc.Write(col);
                        htmlDoc.Write(endCellHeader);
                    }

                    htmlDoc.Write(endRowHeader);
                }

                //riga di separazione
                htmlDoc.Write(startRowBody);
                htmlDoc.Write(startCellBody, "colspan=\"" + cols.Length.ToString() + "\"");
                htmlDoc.Write(endCellBody);
                htmlDoc.Write(endRowBody);
                htmlDoc.Write(endTableHeader);
            }

            if (source != null) {
                //tabella vera e propria
                htmlDoc.Write(startTable);
                //intestazioni
                htmlDoc.Write(startRowHeader);
                for (int i = 0; i < source.Tables[0].Columns.Count; i++) {
                    htmlDoc.Write(startCellHeader);
                    htmlDoc.Write(source.Tables[0].Columns[i].Caption);
                    htmlDoc.Write(endCellHeader);
                }
                htmlDoc.Write(endRowHeader);

                //body
                System.Collections.ArrayList colarr = null;
                if (this._excludeColumns != null)
                    colarr = new System.Collections.ArrayList(this._excludeColumns);

                foreach (DataRow row in source.Tables[0].Rows) {
                    rowCount++;
                    htmlDoc.Write(startRowBody);
                    for (int col = 0; col < source.Tables[0].Columns.Count; col++) {
                        System.Type cellType;

                        if (colarr != null && colarr.Contains(source.Tables[0].Columns[col].ColumnName)) continue;

                        cellType = row[col].GetType();
                        switch (cellType.ToString()) {
                            case "System.Int64":
                            case "System.Int32":
                            case "System.Int16":
                                htmlDoc.Write(startCellBody, styleInt);
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode(row[col].ToString()));
                                break;
                            case "System.String":
                                htmlDoc.Write(startCellBody, styleString);
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode(row[col].ToString()).Replace("�", "&euro;"));
                                break;
                            case "System.Double":
                                htmlDoc.Write(startCellBody, styleDouble);
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode(row[col].ToString()));
                                break;
                            case "System.DateTime":
                                htmlDoc.Write(startCellBody, styleDateTime);
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode(row[col].ToString()));
                                break;
                            case "System.Boolean":
                                htmlDoc.Write(startCellBody, styleBoolean);
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode(row[col].ToString()));
                                break;
                            case "System.Decimal":
                                htmlDoc.Write(startCellBody, styleDecimal);
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode(row[col].ToString()));
                                break;
                            case "System.DBNull":
                                htmlDoc.Write(startCellBody, "");
                                if (this.showNullAsEmptyString) {
                                    htmlDoc.Write(String.Empty);
                                }
                                else {
                                    htmlDoc.Write(System.Web.HttpUtility.HtmlEncode("null"));
                                }
                                break;
                            default:
                                htmlDoc.Write(startCellBody, "");
                                htmlDoc.Write(System.Web.HttpUtility.HtmlEncode("Type not handled:" + cellType.ToString()));
                                break;
                        }

                        htmlDoc.Write(endCellBody);
                    }
                    htmlDoc.Write(endRowBody);
                }
            }
            htmlDoc.Write(endTable);

            string ret = htmlDoc.ToString();

            htmlDoc.Close();
            return ret;
        }

        private string TransformToExcelXML(DataSet ds) {
            return null;
        }

        public void Transfer(DataSet ds, string excludeColumns) {
            this.ExcludeColumns = excludeColumns;

            this.Transfer(ds);
        }

        public void Transfer(DataSet ds, TransformationType transType, string excludeColumns) {
            this.ExcludeColumns = excludeColumns;

            this.Transfer(ds, transType);
        }

        public string Result {
            get { return this._result; }
        }
    }
}

﻿
using System;
using System.Linq;
using System.Web.Services;
using GrisSuite.GETA.Data;

[WebService(Namespace = "http://rfi.it/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class NavigationBarGoToNode : WebService {
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetCompletionList(string prefixText, int count) {
        if (prefixText.Length > 0) {
            int i;

            DBSTLCDataContext dbCtx = new DBSTLCDataContext();

            IQueryable<Node> nodes;

            if (int.TryParse(prefixText[0].ToString(), out i)) {
                if (prefixText.Split('.').Length < 4) {
                    return null;
                }

                nodes = from srv in dbCtx.ServerInfos where srv.IP.StartsWith(prefixText) && srv.NodID.HasValue select srv.Node;
            }
            else {
                nodes = from nd in dbCtx.Nodes where nd.Name.IndexOf(prefixText) != -1 select nd;
            }

            var selectedResults =
                nodes.Select(
                    nd =>
                    AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(
                        string.Format("{0} ({1}){2}", nd.Name, nd.ServerInfos.Count > 0 ? nd.ServerInfos.First().IP : "Nessun STLC", GetServerPhysicalLocation(nd.NodID)), nd.NodID.ToString())).Distinct().Take(count);

            return selectedResults.ToArray();
        }

        return null;
    }

    // metodo duplicato anche nella Default.aspx.cs
    private static string GetServerPhysicalLocation(long nodID) {
        DBSTLCDataContext dc = new DBSTLCDataContext();

        var relatedServer = (from d in dc.DeviceRels where d.NodID == nodID select d.ServerInfo).FirstOrDefault();

        if (relatedServer != null) {
            if (relatedServer.NodID == nodID) {
                // Nel caso la locazione fisica dell'STLC corrisponda a quella della stazione corrente, non c'è nulla da visualizzare
                return String.Empty;
            }

            if (relatedServer.NodID.HasValue) {
                // ritorna il nome della stazione fisica
                return " [" + relatedServer.Node.Name + "]";
            }

            return String.Empty;
        }

        // risalendo dai Devices, non si trova nessun server collegato
        return String.Empty;
    }
}
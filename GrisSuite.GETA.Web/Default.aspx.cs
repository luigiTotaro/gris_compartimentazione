﻿using System;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using GrisSuite.GETA.Data;

public partial class _Default : Page
{
    // Target type di default da usare nel caso di interventi generici - sono comunque associati ad un nodo (prima erano con tipo nullo)
    private const int TARGET_TYPE_ID_STLC_1000 = 2;

    int? CurrentTargetActivityID
    {
        get { return (int?) this.Session["CurrentTargetActivityID"]; }
        set { this.Session["CurrentTargetActivityID"] = value; }
    }

    int? CurrentTargetTypeID
    {
        get { return (int?) this.Session["CurrentTargetTypeID"]; }
        set { this.Session["CurrentTargetTypeID"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        long nodeId;

        if (!Utility.IsUserInGETAGroup() && !Utility.IsUserInAdminGroup())
        {
            this.Response.Redirect("~/NoPermissions.aspx");
        }

        ((IGetaMasterPage) this.Master).GetaLayoutMode = GetaLayoutModeEnum.Issues;

        if (!string.IsNullOrEmpty(this.Request.QueryString["nod"]) && long.TryParse(this.Request.QueryString["nod"], out nodeId))
        {
            Utility.NodeID = nodeId;
        }

        if (Utility.NodeID != -1)
        {
            this.tblIssues.Visible = true;

            # region Header

            if (!this.IsPostBack)
            {
                this.pnlStlcArrows.Visible = true;
                this.SetImageButtonOverBehavior(this.imgNextStlc, "next_stlc", false);
                this.SetImageButtonOverBehavior(this.imgPrevStlc, "prev_stlc", false);

                DBSTLCDataContext dbStlc = new DBSTLCDataContext();

                var nodes = from n in dbStlc.Nodes where n.NodID == Utility.NodeID select n;

                if (nodes.Count() > 0)
                {
                    this.InitHeader(nodes.First());
                }
            }

            # endregion
        }
        else
        {
            this.tblIssues.Visible = false;
        }
    }

    protected void ddlNodes_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.Response.Redirect("~/Default.aspx?nod=" + this.ddlNodes.SelectedValue);
    }

    private void InitHeader(Node node)
    {
        Zone zone = node.Zone;
        Region region = zone.Region;

        var nodes = zone.Nodes.OrderBy(o => o.Meters).ThenBy(o => o.Name).ToList();
        this.ddlNodes.DataSource = nodes;
        this.ddlNodes.DataBind();
        this.ddlNodes.SelectedValue = node.NodID.ToString();
        this.ddlNodes.Visible = true;

        var index = nodes.IndexOf(node);
        Node tmpNode;
        if (index < nodes.Count - 1)
        {
            tmpNode = nodes[index + 1];
            this.imgNextStlc.Enabled = true;
            this.imgNextStlc.CommandArgument = tmpNode.NodID.ToString();
        }
        else
        {
            this.imgNextStlc.Enabled = false;
        }

        if (index > 0)
        {
            tmpNode = nodes[index - 1];
            this.imgPrevStlc.Enabled = true;
            this.imgPrevStlc.CommandArgument = tmpNode.NodID.ToString();
        }
        else
        {
            this.imgPrevStlc.Enabled = false;
        }

        this.lblNodePath.Text = string.Format("{0} > {1} > ", region.Name, zone.Name);
    }

    # region Attributes

    protected void dlstAttributeNamespaces_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ActivityGroup group = e.Item.DataItem as ActivityGroup;
            DataList dlstAttribute = e.Item.FindControl("dlstAttribute") as DataList;

            Issue issue;
            bool flagClosed = true;

            foreach (TargetActivity activity in group.TargetActivities)
            {
                if (group.OnlyOpenActivityState && activity.GetServerIssues(Utility.NodeID).Count() <= 0)
                {
                    continue;
                }

                issue = activity.GetServerLastIssue(Utility.NodeID);
                if (issue == null || !issue.IsClosed)
                {
                    flagClosed = false;
                    break;
                }
            }

            e.Item.CssClass = flagClosed ? "group_close" : "group_open";

            if (dlstAttribute != null)
            {
                dlstAttribute.DataSource = group.TargetActivities.Where(s => s.IsObsolete == false).OrderBy(s => s.Order);
                dlstAttribute.DataBind();

                // Nel caso siano visualizzati più di 4 elementi, impostiamo l'altezza del DIV, in modo che appaiano le barre di scorrimento
                // Nel caso normale (fino a 4 elementi) l'altezza si adatta al contenuto
                // Su IE 7, è possibile supportare l'altezza massima via max-height, ma non funziona su IE 6 e precedenti
                if (dlstAttribute.Items.Count > 4)
                {
                    ((HtmlGenericControl) e.Item.FindControl("divAttributes")).Style.Add("height", "130px");
                }
            }
        }
    }

    protected void dlstAttribute_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TargetActivity activity = e.Item.DataItem as TargetActivity;
            LinkButton label = e.Item.FindControl("lbAttribute") as LinkButton;

            if (activity.GetServerIssues(Utility.NodeID).Count() > 0)
            {
                e.Item.CssClass = activity.GetServerLastIssue(Utility.NodeID).IsClosed ? "activity_close" : "activity_open";
            }
            else
            {
                e.Item.CssClass = "activity_none";
            }

            if (label != null && activity.ControlTypeUI != "CheckBox")
            {
                Issue issue = activity.GetServerLastIssue(Utility.NodeID);

                if (issue != null)
                {
                    label.Text += " " + issue.TargetActivityValue;
                }
            }

            label.ToolTip = label.Text;
            if (label.Text.Length > 30)
            {
                label.Text = label.Text.Substring(0, 30) + "...";
            }
        }
    }

    private void InitTargetActivityEditor(TargetActivity target, string attributeValue)
    {
        Label labelTitle = this.lblFieldTitle;
        Label labelText = string.IsNullOrEmpty(attributeValue) ? this.lblFieldName : this.lblEditFieldName;
        GetaCheckBox cb = string.IsNullOrEmpty(attributeValue) ? this.chkFieldValue : this.chEditFieldValue;
        DropDownList ddl = string.IsNullOrEmpty(attributeValue) ? this.ddlFieldValue : this.ddlEditFieldValue;
        TextBox txt = string.IsNullOrEmpty(attributeValue) ? this.txtFieldValue : this.txtEditFieldValue;

        labelText.Text = string.Format("{0}", target.TargetActivityName);
        labelTitle.Text = this.lblFieldName.Text;
        switch (target.ControlTypeUI)
        {
            case "CheckBox":
                if (cb != null)
                {
                    cb.Visible = true;
                    if (!string.IsNullOrEmpty(attributeValue))
                    {
                        cb.Checked = Convert.ToBoolean(attributeValue);
                    }
                    else
                    {
                        cb.Checked = true;
                    }
                }
                ddl.Visible = false;
                txt.Visible = false;
                break;
            case "ComboBox":
                ddl.Visible = true;
                ddl.Items.Clear();
                if (!string.IsNullOrEmpty(target.EnumValues))
                {
                    var cboValues = from cboValue in target.EnumValues.Split('|') select new ListItem(cboValue);
                    ddl.Items.AddRange(cboValues.ToArray());
                }
                if (!string.IsNullOrEmpty(attributeValue))
                {
                    try
                    {
                        ddl.SelectedValue = attributeValue;
                    }
                    catch
                    {
                    }
                }
                cb.Visible = false;
                txt.Visible = false;
                break;
            default:
                txt.Visible = true;
                if (!string.IsNullOrEmpty(attributeValue))
                {
                    txt.Text = attributeValue;
                }
                ddl.Visible = false;
                cb.Visible = false;
                break;
        }
    }

    private void BindEditBox(int issueID)
    {
        DBIssuesDataContext db = new DBIssuesDataContext();

        Issue issue = db.Issues.SingleOrDefault(iss => iss.IssueID == issueID);

        if (issue != null)
        {
            this.txtEditDate.Text = issue.DateIssue.ToString("dd-MM-yyyy");
            this.ddlEditIssueActivities.SelectedValue = issue.IssueActivityTypeID.ToString();
            this.ddlEditState.SelectedValue = issue.IssueStateID.ToString();
            this.txtEditNotes.Text = issue.IssueComment;
            if (issue.ServerID.HasValue)
            {
                try
                {
                    this.ddlEditServers.SelectedValue = issue.ServerID.ToString();
                }
                catch
                {
                }
            }
            else
            {
                this.ddlEditServers.SelectedIndex = 0;
            }

            if (issue.TargetActivity != null)
            {
                this.InitTargetActivityEditor(issue.TargetActivity, issue.TargetActivityValue);
                this.pnlEditActivityValue.Visible = true;
                this.trEditActivityValueSeparator.Visible = true;
            }
            else
            {
                this.pnlEditActivityValue.Visible = false;
                this.trEditActivityValueSeparator.Visible = false;
            }

            this.chklEditInstaller.DataBind();
            this.chklEditGrisOperator.DataBind();

            // ciclo gli utenti associati all'intervento con un dato ruolo e flaggo gli elementi della checklistbox corrispondente
            issue.IssueUsers.Where(role => role.IssueUserRoleID == 1 /* Tecnico di Riferimento */).ToList().ForEach(
                user => this.chklEditInstaller.Items.FindByValue(user.UserID.ToString()).Selected = true);

            issue.IssueUsers.Where(role => role.IssueUserRoleID == 2 /* Operatore Gris */).ToList().ForEach(
                user => this.chklEditGrisOperator.Items.FindByValue(user.UserID.ToString()).Selected = true);
        }
    }

    private string GetInsertTargetActivityValue()
    {
        if (this.chkFieldValue.Visible)
        {
            return this.chkFieldValue.Checked.ToString();
        }

        if (this.ddlFieldValue.Visible)
        {
            return this.ddlFieldValue.SelectedValue;
        }

        return this.txtFieldValue.Text;
    }

    private string GetEditTargetActivityValue()
    {
        if (this.chEditFieldValue.Visible)
        {
            return this.chEditFieldValue.Checked.ToString();
        }

        if (this.ddlEditFieldValue.Visible)
        {
            return this.ddlEditFieldValue.SelectedValue;
        }

        return this.txtEditFieldValue.Text;
    }

    protected void linqServers_OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        DBSTLCDataContext dc = new DBSTLCDataContext();

        // relazione diretta tra Nodes e Servers
        var stlc1 = from s in dc.ServerInfos
                    where s.NodID == Utility.NodeID
                    select
                        new
                            {
                                ID = s.SrvID,
                                Name = string.Format("{0} ({1}) - {2} ({3}){4}", s.SrvID, Utility.GetSNFromSrvID((ulong)s.SrvID), s.FullHostName, s.IP, GetServerPhysicalLocation(Utility.NodeID))
                            };

        // tutti i Servers che derivano dai Devices del Node selezionato
        var stlc2 = from s in dc.ServerInfos
                    where (from d in dc.DeviceRels where d.NodID == Utility.NodeID && d.SrvID != null select d.SrvID).Contains(s.SrvID)
                    select
                        new
                            {
                                ID = s.SrvID,
                                Name = string.Format("{0} ({1}) - {2} ({3}){4}", s.SrvID, Utility.GetSNFromSrvID((ulong)s.SrvID), s.FullHostName, s.IP, GetServerPhysicalLocation(Utility.NodeID))
                            };

        // tutti i Servers che derivano da issues (quindi attuali od obsoleti)
        var stlc3 = from s in dc.ServerInfos
                    where (from i in dc.IssuePerNodes where i.TargetID == Utility.NodeID && i.ServerID != null select i.ServerID).Contains(s.SrvID)
                    select
                        new
                            {
                                ID = s.SrvID,
                                Name = string.Format("{0} ({1}) - {2} ({3}){4}", s.SrvID, Utility.GetSNFromSrvID((ulong)s.SrvID), s.FullHostName, s.IP, GetServerPhysicalLocation(Utility.NodeID))
                            };

        e.Result = stlc1.ToList().Union(stlc2).ToList().Union(stlc3).ToList().OrderBy(s => s.Name);
    }

    // metodo duplicato anche nella NavigationBarGoToNode.cs
    private static string GetServerPhysicalLocation(long nodID)
    {
        DBSTLCDataContext dc = new DBSTLCDataContext();

        var relatedServer = (from d in dc.DeviceRels where d.NodID == nodID select d.ServerInfo).FirstOrDefault();

        if (relatedServer != null)
        {
            if (relatedServer.NodID == nodID)
            {
                // Nel caso la locazione fisica dell'STLC corrisponda a quella della stazione corrente, non c'è nulla da visualizzare
                return String.Empty;
            }

            if (relatedServer.NodID.HasValue)
            {
                // ritorna il nome della stazione fisica
                return " [" + relatedServer.Node.Name + "]";
            }

            return String.Empty;
        }

        // risalendo dai Devices, non si trova nessun server collegato
        return String.Empty;
    }

    protected void linqInstaller_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        DBUsersDataContext db = new DBUsersDataContext();
        e.Result = db.GetUsers(1, -1);
    }

    protected void linqGrisOperator_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        DBUsersDataContext db = new DBUsersDataContext();
        e.Result = db.GetUsers(2, -1);
    }

    public void lbAttribute_OnClick(object sender, EventArgs e)
    {
        this.CurrentTargetActivityID =
            (Int32) ((sender as LinkButton).Parent.Parent as DataList).DataKeys[((sender as LinkButton).Parent as DataListItem).ItemIndex];
        this.CurrentTargetTypeID = Int32.Parse((sender as LinkButton).CommandArgument);
        this.VisibilitySwitch("Insert");

        DBIssuesDataContext db = new DBIssuesDataContext();
        this.InitTargetActivityEditor(db.TargetActivities.SingleOrDefault(t => t.TargetActivityID == this.CurrentTargetActivityID), string.Empty);

        this.ddlServers.SelectedIndex = -1;
        int index = this.ddlGrisOperator.SelectedIndex;
        this.ddlGrisOperator.DataBind();
        if (index >= 0)
        {
            this.ddlGrisOperator.SelectedIndex = index;
        }
        else if (Utility.UserID > -1)
        {
            try
            {
                this.ddlGrisOperator.SelectedValue = Utility.UserID.ToString();
            }
            catch
            {
            }
        }
        if (string.IsNullOrEmpty(this.txtDate.Text))
        {
            this.txtDate.Text = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
        }
        this.pnlAddActivityValue.Visible = true;
        this.trAddActivityValueSeparator.Visible = true;
        this.updAddIssue.Update();
    }

    public void imgAddIssue_OnClick(object sender, ImageClickEventArgs e)
    {
        this.CurrentTargetActivityID = null;
        this.CurrentTargetTypeID = null;
        this.ddlServers.SelectedIndex = -1;
        this.lblFieldTitle.Text = "";

        this.pnlAddActivityValue.Visible = false;
        this.trAddActivityValueSeparator.Visible = false;
        int index = this.ddlGrisOperator.SelectedIndex;
        this.ddlGrisOperator.DataBind();
        if (index >= 0)
        {
            this.ddlGrisOperator.SelectedIndex = index;
        }
        else if (Utility.UserID > -1)
        {
            try
            {
                this.ddlGrisOperator.SelectedValue = Utility.UserID.ToString();
            }
            catch
            {
            }
        }

        if (string.IsNullOrEmpty(this.txtDate.Text))
        {
            this.txtDate.Text = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
        }
        this.VisibilitySwitch("Insert");
    }

    # endregion

    # region Issues

    # region lvwIssues

    protected void lvwIssues_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
            ImageButton imgbEdit = e.Item.FindControl("imgbEdit") as ImageButton;

            this.SetImageButtonOverBehavior(imgbDelete, "delete", false);

            if (imgbEdit.ImageUrl.IndexOf("editopen") > 0)
            {
                this.SetImageButtonOverBehavior(imgbEdit, "editopen", false);
            }
            else
            {
                this.SetImageButtonOverBehavior(imgbEdit, "edit", false);
            }
        }
    }

    protected void lvwIssues_LayoutCreated(object sender, EventArgs e)
    {
        ImageButton imgAddIssue = this.lvwIssues.FindControl("imgAddIssue") as ImageButton;
        this.SetImageButtonOverBehavior(imgAddIssue, "lightadd", true);
    }

    protected void lnkbIssue_Command(object sender, CommandEventArgs e)
    {
        if (this.IsValid)
        {
            switch (e.CommandName)
            {
                case "Insert":
                    {
                        DBIssuesDataContext dbIssues = new DBIssuesDataContext();

                        /************ Inserimento dati Intervento ************/
                        Issue issue = new Issue();
                        IssueUser user = null;

                        if (!string.IsNullOrEmpty(this.ddlInstaller.SelectedValue))
                        {
                            user = new IssueUser();
                            user.UserID = Convert.ToInt32(this.ddlInstaller.SelectedValue);
                            user.IssueUserRoleID = 1; //Tecnico di Riferimento
                            issue.IssueUsers.Add(user);
                        }

                        user = new IssueUser();
                        user.UserID = Convert.ToInt32(this.ddlGrisOperator.SelectedValue);
                        user.IssueUserRoleID = 2; //Operatore Gris
                        issue.IssueUsers.Add(user);

                        if (!string.IsNullOrEmpty(this.ddlServers.SelectedValue))
                        {
                            issue.ServerID = Convert.ToInt32(this.ddlServers.SelectedValue);
                        }

                        // Se è un intervento generico, lo consideriamo comunque per Nodo
                        int targetTypeID = (this.CurrentTargetTypeID ?? TARGET_TYPE_ID_STLC_1000);

                        switch (targetTypeID)
                        {
                            case 1: // Linea
                                issue.TargetID = Utility.GetZoneIDFromNodeID(Utility.NodeID);
                                issue.TargetData = Utility.GetZoneNameFromNodeID(Utility.NodeID, true);
                                break;
                            case 2: // STLC1000 - Nodo
                                issue.TargetID = Utility.NodeID;
                                issue.TargetData = Utility.GetNodeNameFromNodeID(Utility.NodeID, true);
                                break;
                        }

                        issue.TargetTypeID = targetTypeID;

                        issue.TargetActivityID = this.CurrentTargetActivityID;
                        issue.IssueStateID = Convert.ToInt32(this.ddlState.SelectedValue);
                        if (issue.IssueStateID == 2)
                        {
                            issue.IsClosed = true;
                        }
                        issue.IssueActivityTypeID = Convert.ToInt32(this.ddlIssueActivities.SelectedValue);
                        issue.TargetActivityValue = this.GetInsertTargetActivityValue();
                        issue.DateCreation = DateTime.Now;
                        issue.DateIssue = Convert.ToDateTime(this.txtDate.Text);

                        if (this.txtNotes.Text.Length > 0)
                        {
                            string comment = string.Format("{0}", this.txtNotes.Text);
                            if (!comment.EndsWith("."))
                            {
                                comment = string.Format("{0}.", comment);
                            }
                            issue.IssueComment = comment;
                        }

                        dbIssues.Issues.InsertOnSubmit(issue);
                        dbIssues.SubmitChanges();

                        this.lvwIssues.DataBind();
                        this.updIssues.Update();
                        this.dlstAttributeNamespaces.DataBind();
                        this.updHeader.Update();

                        this.VisibilitySwitch("");

                        break;
                    }
                case "Update":
                    {
                        DBIssuesDataContext dbIssues = new DBIssuesDataContext();

                        /************ Aggiornamento dati Intervento ************/
                        int issueID = Convert.ToInt32(this.lvwIssues.SelectedDataKey.Value);
                        Issue fatherIssue = dbIssues.Issues.SingleOrDefault(iss => iss.IssueID == issueID);
                        Issue issue = new Issue();

                        foreach (ListItem chkItem in this.chklEditInstaller.Items)
                        {
                            if (chkItem.Selected)
                            {
                                IssueUser issueUser = new IssueUser();
                                //issueUser.IssueID = issue.IssueID;
                                issueUser.IssueUserRoleID = 1; // Tecnico di Riferimento
                                issueUser.UserID = Convert.ToInt32(chkItem.Value);

                                issue.IssueUsers.Add(issueUser);
                            }
                        }

                        foreach (ListItem chkItem in this.chklEditGrisOperator.Items)
                        {
                            if (chkItem.Selected)
                            {
                                IssueUser issueUser = new IssueUser();
                                issueUser.IssueUserRoleID = 2; // Operatore Gris
                                issueUser.UserID = Convert.ToInt32(chkItem.Value);

                                issue.IssueUsers.Add(issueUser);
                            }
                        }

                        // Se è un intervento generico, lo consideriamo comunque per Nodo
                        int targetTypeID = (fatherIssue.TargetTypeID ?? TARGET_TYPE_ID_STLC_1000);

                        switch (targetTypeID)
                        {
                            case 1: // Linea
                                issue.TargetID = Utility.GetZoneIDFromNodeID(Utility.NodeID);
                                issue.TargetData = Utility.GetZoneNameFromNodeID(Utility.NodeID, true);
                                break;
                            case 2: // STLC1000 - Nodo
                                issue.TargetID = Utility.NodeID;
                                issue.TargetData = Utility.GetNodeNameFromNodeID(Utility.NodeID, true);
                                break;
                        }

                        issue.TargetTypeID = targetTypeID;

                        issue.ServerID = string.IsNullOrEmpty(this.ddlEditServers.SelectedValue)
                                             ? (int?) null
                                             : Convert.ToInt32(this.ddlEditServers.SelectedValue);
                        issue.TargetActivityID = fatherIssue.TargetActivityID;
                        issue.TargetActivityValue = this.GetEditTargetActivityValue();
                        issue.IssueStateID = Convert.ToInt32(this.ddlEditState.SelectedValue);
                        issue.IssueActivityTypeID = Convert.ToInt32(this.ddlEditIssueActivities.SelectedValue);
                        if (issue.IssueStateID == 2)
                        {
                            issue.IsClosed = true;
                        }
                        issue.DateIssue = Convert.ToDateTime(this.txtEditDate.Text);
                        issue.DateCreation = DateTime.Now;
                        issue.IssueElapsedTime = fatherIssue.IssueElapsedTime + issue.DateCreation.Subtract(fatherIssue.DateCreation).Minutes;

                        if (this.txtEditNotes.Text.Length > 0)
                        {
                            string comment = string.Format("{0}", this.txtEditNotes.Text);
                            if (!comment.EndsWith("."))
                            {
                                comment = string.Format("{0}.", comment);
                            }
                            issue.IssueComment = comment;
                        }

                        dbIssues.Issues.InsertOnSubmit(issue);
                        dbIssues.SubmitChanges();

                        fatherIssue.IssueChildID = issue.IssueID;
                        dbIssues.SubmitChanges();

                        this.lvwIssues.DataBind();
                        this.dlstAttributeNamespaces.DataBind();
                        this.updHeader.Update();

                        this.VisibilitySwitch("");

                        break;
                    }
                case "Cancel":
                    {
                        this.VisibilitySwitch("Cancel");

                        break;
                    }
                default:
                    {
                        this.VisibilitySwitch("");

                        break;
                    }
            }

            this.ddlState.SelectedIndex = -1;
            this.ddlIssueActivities.SelectedIndex = -1;
            this.txtNotes.Text = string.Empty;
            this.txtEditFieldValue.Text = string.Empty;
            this.txtFieldValue.Text = string.Empty;
        }
    }

    protected void lvwIssues_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        int issueID = Convert.ToInt32(e.Keys[0]);

        DBIssuesDataContext db = new DBIssuesDataContext();
        Issue issue = db.Issues.SingleOrDefault(iss => iss.IssueID == issueID);

        if (issue != null)
        {
            issue.IsDeleted = true;
            db.SubmitChanges();

            this.lvwIssues.DataBind();
            this.updIssues.Update();

            this.dlstAttributeNamespaces.DataBind();
            this.updHeader.Update();
        }

        e.Cancel = true;
    }

    protected void lvwIssues_Sorting(object sender, ListViewSortEventArgs e)
    {
        Image img = this.lvwIssues.FindControl("img" + this.lvwIssues.SortExpression) as Image;
        if (img != null)
        {
            img.Visible = false;
        }

        img = this.lvwIssues.FindControl("img" + e.SortExpression) as Image;
        if (img != null)
        {
            img.Visible = true;
            img.ImageUrl = "~/Images/" + (e.SortDirection == SortDirection.Ascending ? "up_arrow.gif" : "down_arrow.gif");
        }
    }

    protected void lvwIssues_SelectedIndexChanged(object sender, EventArgs e)
    {
        int issueID = Convert.ToInt32(this.lvwIssues.SelectedDataKey.Value);
        this.BindEditBox(issueID);

        this.VisibilitySwitch("Update");
    }

    # endregion

    public void imgPrevStlc_OnClick(object sender, ImageClickEventArgs e)
    {
        this.Response.Redirect("Default.aspx?nod=" + this.imgPrevStlc.CommandArgument);
    }

    public void imgNextStlc_OnClick(object sender, ImageClickEventArgs e)
    {
        this.Response.Redirect("Default.aspx?nod=" + this.imgNextStlc.CommandArgument);
    }

    protected void linqIssueList_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        DBIssuesDataContext db = new DBIssuesDataContext();

        var stlcIssues = (from issue in db.Issues
                          where issue.TargetID == Utility.NodeID && !issue.IssueChildID.HasValue && !issue.IsDeleted
                          group issue by issue
                          into issues
                          select
                              new
                                  {
                                      issues.Key.IssueID,
                                      issues.Key.DateIssue,
                                      DateCreated = issues.Key.DateCreation,
                                      issues.Key.TargetActivity.ActivityGroup.ActivityGroupName,
                                      issues.Key.TargetActivity.TargetActivityName,
                                      issues.Key.IssueState.IssueStateName,
                                      GrisOperators =
                              string.Join(", ",
                                          issues.Key.IssueUsers.Where(role => role.IssueUserRoleID == 2).Select(
                                              iu =>
                                              iu.UserInfo.FirstName == string.Empty || iu.UserInfo.FirstName == null ||
                                              iu.UserInfo.LastName == string.Empty || iu.UserInfo.LastName == null
                                                  ? iu.UserInfo.Username
                                                  : iu.UserInfo.FirstName + " " + iu.UserInfo.LastName).ToArray()),
                                      issues.Key.IssueComment,
                                      issues.Key.IssueStateID
                                  }).ToList();

        e.Result = stlcIssues;
    }

    # endregion

    # region Methods

    // metodo statico per il redirect lato client (Goto)
    // è da spostare sulla classe di base delle pagine quando + pagine useranno il Goto
    [WebMethod]
    public static string ClientGoToPage(string id)
    {
        long srvId;
        if (long.TryParse(id, out srvId))
        {
            return Utility.GetAbsoluteUrl(string.Format("~/Default.aspx?nod={0}", srvId));
        }
        return "";
    }

    private void VisibilitySwitch(string layoutMode)
    {
        switch (layoutMode)
        {
            case "Insert":
                {
                    if (this.tblEditIssue.Visible)
                    {
                        this.tblEditIssue.Visible = false;
                        this.updEditIssue.Update();
                    }

                    if (!this.tblAddIssue.Visible)
                    {
                        this.tblAddIssue.Visible = true;
                        this.updAddIssue.Update();
                    }

                    if (this.tblIssueList.Visible)
                    {
                        this.tblIssueList.Visible = false;
                        this.updIssues.Update();
                    }

                    if (this.ddlState.Items.Count == 0)
                    {
                        this.ddlState.DataBind();
                    }

                    foreach (ListItem status in this.ddlState.Items)
                    {
                        // Seleziona lo stato chiuso come default
                        if (status.Value == "2")
                        {
                            status.Selected = true;
                        }
                        else
                        {
                            status.Selected = false;
                        }
                    }

                    break;
                }
            case "Update":
                {
                    if (!this.tblEditIssue.Visible)
                    {
                        this.tblEditIssue.Visible = true;
                        this.updEditIssue.Update();
                    }

                    if (this.tblAddIssue.Visible)
                    {
                        this.tblAddIssue.Visible = false;
                        this.updAddIssue.Update();
                    }

                    if (this.tblIssueList.Visible)
                    {
                        this.tblIssueList.Visible = false;
                        this.updIssues.Update();
                    }

                    break;
                }
            case "Cancel":
                {
                    if (this.tblEditIssue.Visible)
                    {
                        this.tblEditIssue.Visible = false;
                        this.updEditIssue.Update();
                    }

                    if (this.tblAddIssue.Visible)
                    {
                        this.tblAddIssue.Visible = false;
                        this.updAddIssue.Update();
                    }

                    if (!this.tblIssueList.Visible)
                    {
                        this.tblIssueList.Visible = true;
                        this.updIssues.Update();
                    }

                    break;
                }
            default:
                {
                    if (this.tblEditIssue.Visible)
                    {
                        this.tblEditIssue.Visible = false;
                        this.updEditIssue.Update();
                    }

                    if (this.tblAddIssue.Visible)
                    {
                        this.tblAddIssue.Visible = false;
                        this.updAddIssue.Update();
                    }

                    if (!this.tblIssueList.Visible)
                    {
                        this.tblIssueList.Visible = true;
                        this.updIssues.Update();
                    }

                    break;
                }
        }
    }

    private void SetImageButtonOverBehavior(ImageButton imgbutton, string defaultImageName, bool pressedStatus)
    {
        if (imgbutton != null)
        {
            imgbutton.Attributes.Add("OnMouseOver", string.Format("this.src = 'Images/{0}_over.gif'", defaultImageName));
            imgbutton.Attributes.Add("OnMouseOut", string.Format("this.src = 'Images/{0}.gif'", defaultImageName));
            if (pressedStatus)
            {
                imgbutton.Attributes.Add("OnMouseDown", string.Format("this.src = 'Images/{0}_pressed.gif'", defaultImageName));
            }
        }
    }

    # endregion

    private static long GetPhysicalServerID(long nodID)
    {
        DBSTLCDataContext dc = new DBSTLCDataContext();

        var relatedServer = (from d in dc.DeviceRels where d.NodID == nodID select d.ServerInfo).FirstOrDefault();

        if (relatedServer != null)
        {
            if (relatedServer.NodID == nodID)
            {
                // Nel caso la locazione fisica dell'STLC corrisponda a quella della stazione corrente, non c'è nulla da visualizzare
                return relatedServer.SrvID;
            }

            if (relatedServer.NodID.HasValue)
            {
                // ritorna il nome della stazione fisica
                return relatedServer.SrvID;
            }

            return -1;
        }

        // risalendo dai Devices, non si trova nessun server collegato
        return -1;
    }

    private void ResetComboSelectedIndex(DropDownList cbo, bool state)
    {
        if (cbo != null)
        {
            foreach (ListItem item in cbo.Items)
            {
                item.Selected = state;
            }
        }
    }

    protected void ddlServers_PreRender(object sender, EventArgs e)
    {
        if ((this.ddlServers.SelectedIndex == -1) || (this.ddlServers.SelectedIndex == 0))
        {
            this.ResetComboSelectedIndex(this.ddlServers, false);

            long srvID = GetPhysicalServerID(Utility.NodeID);

            if (srvID > 0)
            {
                ListItem item = this.ddlServers.Items.FindByValue(srvID.ToString());
                if (item != null)
                {
                    item.Selected = true;
                }
            }
            else
            {
                if (this.ddlServers.Items.Count > 1)
                {
                    this.ddlServers.SelectedIndex = 1;
                }
            }
        }
    }
}
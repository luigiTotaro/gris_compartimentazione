﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class GoTo : System.Web.UI.UserControl
{
	# region Properties
	public bool GoToControlOpen
	{
		get { return this.tdCenterGoTo.Visible; }
		set
		{
			this.tdCenterGoTo.Visible = value;
			this.tdSxGoTo.Visible = value;

			if ( value )
			{
				this.imgbBoxGoToNode.ImageUrl = "~/Images/goto_dx_open.gif";
				this.imgbBoxGoToNode.Attributes["OnMouseOver"] = "this.src = 'Images/goto_dx_open_over.gif'";
				this.imgbBoxGoToNode.Attributes["OnMouseOut"] = "this.src = 'Images/goto_dx_open.gif'";
				this.imgbBoxGoToNode.ToolTip = "Chiudi";
			}
			else
			{
				this.imgbBoxGoToNode.ImageUrl = "~/Images/goto_dx_closed.gif";
				this.imgbBoxGoToNode.Attributes["OnMouseOver"] = "this.src = 'Images/goto_dx_closed_over.gif'";
				this.imgbBoxGoToNode.Attributes["OnMouseOut"] = "this.src = 'Images/goto_dx_closed.gif'";
				this.imgbBoxGoToNode.ToolTip = "Vai a";
			}
		}
	}

	public string WatermarkText
	{
		get { return this.txtBoxGoToNodeWM.WatermarkText; }
		set { this.txtBoxGoToNodeWM.WatermarkText = value; }
	}
	# endregion

	protected void Page_Load ( object sender, EventArgs e )
	{
		// le pagine che includono il controllo di navigazione avranno i PageMethods abilitati per permettere le chiamate ai metodi GoToPage statici.
		ScriptManager.GetCurrent(this.Page).EnablePageMethods = true;

		this.RegisterGoToNoResultsClientHandlers();

		this.autoCompBoxGoToNode.OnClientItemSelected = this.EnableClientNodeNavigation();
		this.autoCompBoxGoToNode.OnClientPopulated = "ShowDisplayNoRowsMessage";
		this.autoCompBoxGoToNode.OnClientHiding = "HideDisplayNoRowsMessage";

		if ( !this.IsPostBack )
		{
		}
	}

	protected void imgbBoxGoToNode_Click ( object sender, ImageClickEventArgs e )
	{
		this.GoToControlOpen = !this.GoToControlOpen;
		if ( this.GoToControlOpen )
		{
			this.txtBoxGoToNode.Text = "";
			this.txtBoxGoToNode.Focus();
		}
	}
	
	# region Methods

	# region Client Navigation
	private string EnableClientNodeNavigation ()
	{
		if ( ScriptManager.GetCurrent(this.Page).EnablePageMethods )
		{
			this.RegisterScriptNavigationFunctions();

			return "NodeSelected";
		}

		return "";
	}

	private void RegisterScriptNavigationFunctions ()
	{
		string scriptName = "DestinationSelected";
		string scriptFunctions = @"								
									function NodeSelected( source, eventArgs ) 
									{                                    
									   PageMethods.ClientGoToPage(eventArgs.get_value(), ExecuteRedirect);
									}
									
									function ExecuteRedirect(results, context, methodName)
									{                                        
										window.location.href = results;
									}
									";

		if ( !this.Page.ClientScript.IsClientScriptBlockRegistered(scriptName) ) this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptName, scriptFunctions, true);
	}
	# endregion

	private void RegisterGoToNoResultsClientHandlers ()
	{
		string scriptName = "GoToNoResultsClientHandlers";
		string scriptFunctions = @"
									function ShowDisplayNoRowsMessage ()
									{
										var autoCompEx = $find('AutoCompleteEx');
										
										if ( autoCompEx && autoCompEx._completionListElement.children.length > 0 )
										{
											$get('spnNoRowsMessage').style.display = 'none';
										}
										else
										{
											$get('spnNoRowsMessage').style.display = 'inline';
										}
									}
									
									function HideDisplayNoRowsMessage ()
									{
										var autoCompEx = $find('AutoCompleteEx');
										var waterMEx = $find('" + this.txtBoxGoToNodeWM.ClientID + @"');

										if ( !autoCompEx._textBoxHasFocus && autoCompEx._selectIndex < 0 )
										{
											waterMEx._applyWatermark();
											autoCompEx._currentPrefix = '';
											$get('spnNoRowsMessage').style.display = 'none';
											return;
										}
										
										if ( autoCompEx && autoCompEx._currentPrefix && 
											autoCompEx._currentPrefix.length > autoCompEx._minimumPrefixLength)
										{
											if ( autoCompEx._selectIndex >= 0 )
											{
												$get('spnNoRowsMessage').style.display = 'none';
											}
											else
											{
												$get('spnNoRowsMessage').style.display = 'inline';
											}
										}
										else
										{
											$get('spnNoRowsMessage').style.display = 'none';
										}		
									}
									";

		if ( !this.Page.ClientScript.IsClientScriptBlockRegistered(scriptName) ) this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), scriptName, scriptFunctions, true);
	}

	# endregion
}

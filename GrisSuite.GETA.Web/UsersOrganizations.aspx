<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UsersOrganizations.aspx.cs" Inherits="UsersOrganizations" Title="GETA - Utenti" %>

<%@ Register Src="~/MultiIpuntControl.ascx" TagName="MultiInputControl" TagPrefix="geta" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">

    <script language="javascript" type="text/javascript">
        function FillUserName(idFirst, idLast, idUserName, idWKFirst, idWKLast, idWKUser) {
            var txtFirst = document.getElementById(idFirst);
            var txtLast = document.getElementById(idLast);
            var txtUserName = document.getElementById(idUserName);
            var wkFirst = $find(idWKFirst); wkFirst.set_WatermarkText('');
            var wkLast = $find(idWKLast); wkLast.set_WatermarkText('');
            //var wkUser = $find(idWKUser); wkUser.set_WatermarkText('');
            //txtUserName.focus();
            txtUserName.value = txtFirst.value + '.' + txtLast.value;
        }
    </script>

    <table border="0" cellpadding="0" cellspacing="0" style="width: 1200px; text-align: center">
        <tr>
            <td align="left">
                <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
                    <tr>
                        <td id="tdUsersSelector" runat="server" class="ToolbarButtonToggleOn">
                            <asp:Button ID="btnUsersSelector" runat="server" Text="UTENTI" CssClass="ToolbarButton"
                                PostBackUrl="~/UsersOrganizations.aspx" />
                        </td>
                        <td id="tdAttributesSelector" runat="server" class="ToolbarButtonToggleOff">
                            <asp:Button ID="btnAttributesSelector" runat="server" Text="ATTIVITA'" CssClass="ToolbarButton"
                                PostBackUrl="~/Attributes.aspx" />
                        </td>
                        <td class="ToolbarTitle">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="SubToolbar" colspan="3">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td id="tdCategories" style="width: 20%; vertical-align: top; padding: 5px;" runat="server">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td>
                                        <!-- Pannello gestione AZIENDE -->
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                            <tr>
                                                <td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                                    text-align: center;">&nbsp;</td>
                                                <td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updOrganizationsTitle" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lnkbOrgTitle" runat="server" ForeColor="White" Font-Bold="true"
                                                                CssClass="noDecorationLink" OnClick="lnkbOrgTitle_Click">AZIENDE</asp:LinkButton></td>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updOrganizations" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:ListView ID="lvwOrganizations" runat="server" DataSourceID="linqOrganizations"
                                                                InsertItemPosition="None" DataKeyNames="OrganizationID" OnItemDataBound="lvwOrganizations_ItemDataBound"
                                                                OnItemInserted="lvwOrganizations_ItemInserted" OnItemCanceling="lvwOrganizations_ItemCanceling"
                                                                OnItemEditing="lvwOrganizations_ItemEditing" OnSelectedIndexChanged="lvwOrganizations_SelectedIndexChanged"
                                                                OnItemCreated="lvwOrganizations_ItemCreated">
                                                                <LayoutTemplate>
                                                                    <table id="Table1" runat="server" style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr id="Tr1" runat="server">
                                                                            <td id="Td1" runat="server">
                                                                                <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;">
                                                                                    <tr id="itemPlaceholder" runat="server">
                                                                                    </tr>
                                                                                    <tr id="OrgFooter">
                                                                                        <td colspan="3" style="text-align: left;">
                                                                                            <asp:ImageButton ID="imgbOrgAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                                                OnClick="imgbOrgAdd_Click" AlternateText="Inserimento Azienda" ToolTip="Aggiungi" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LayoutTemplate>
                                                                <InsertItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="OrganizationNameTextBox" runat="server" Text='<%# Bind("OrganizationName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqOrganizationName" runat="server" ControlToValidate="OrganizationNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertOrgGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqOrganizationNameE" TargetControlID="reqOrganizationName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                                            ValidationGroup="InsertOrgGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </InsertItemTemplate>
                                                                <EmptyDataTemplate>
                                                                    <table id="Table1" style="width: 100%;">
                                                                        <tr id="Tr1">
                                                                            <td id="Td1">
                                                                                <table border="0" style="width: 100%;">
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="OrgFooter">
                                                                                        <td style="text-align: left;">
                                                                                            <asp:ImageButton ID="imgbOrgAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                                                OnClick="imgbOrgAdd_Click" AlternateText="Inserimento Azienda" ToolTip="Aggiungi" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                                <EditItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="OrganizationNameTextBox" runat="server" Text='<%# Bind("OrganizationName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqOrganizationName" runat="server" ControlToValidate="OrganizationNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="EditOrgGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqOrganizationNameE" TargetControlID="reqOrganizationName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Aggiorna"
                                                                                            ValidationGroup="EditOrgGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:LinkButton ID="ItemButton" runat="server" CommandName="Select" Text='<%# Eval("OrganizationName") %>'
                                                                                CssClass="selectableField" />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare l\' azienda?');"
                                                                                CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <SelectedItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:Label ID="OrganizationNameLabel" runat="server" Font-Bold="true" ForeColor="White"
                                                                                Text='<%# Eval("OrganizationName") %>' />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare l\' azienda?');" />
                                                                        </td>
                                                                    </tr>
                                                                </SelectedItemTemplate>
                                                                <ItemSeparatorTemplate>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <hr class="hrStyle" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemSeparatorTemplate>
                                                            </asp:ListView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- Pannello gestione GRUPPI -->
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                            <tr>
                                                <td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                                    text-align: center;">&nbsp;</td>
                                                <td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updUserGroupTitle" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lnkbUGTitle" runat="server" ForeColor="White" Font-Bold="true"
                                                                CssClass="noDecorationLink" OnClick="lnkbUGTitle_Click">GRUPPI</asp:LinkButton>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updUserGroups" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:ListView ID="lvwUserGroups" runat="server" DataSourceID="linqUserGroups" InsertItemPosition="None"
                                                                DataKeyNames="UserGroupID" OnItemDataBound="lvwUserGroups_ItemDataBound" OnItemInserted="lvwUserGroups_ItemInserted"
                                                                OnItemCanceling="lvwUserGroups_ItemCanceling" OnItemEditing="lvwUserGroups_ItemEditing"
                                                                OnSelectedIndexChanged="lvwUserGroups_SelectedIndexChanged">
                                                                <LayoutTemplate>
                                                                    <table id="Table1" runat="server" style="width: 100%;">
                                                                        <tr id="Tr1" runat="server">
                                                                            <td id="Td1" runat="server">
                                                                                <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;">
                                                                                    <tr id="itemPlaceholder" runat="server">
                                                                                    </tr>
                                                                                    <tr id="UserGroupsFooter">
                                                                                        <td colspan="3" style="text-align: left;">
                                                                                            <asp:ImageButton ID="imgbUGAdd" runat="server" ImageUrl="~/Images/lightadd.gif" OnClick="imgbUGAdd_Click"
                                                                                                AlternateText="Inserimento Azienda" ToolTip="Aggiungi" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LayoutTemplate>
                                                                <InsertItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="UserGroupNameTextBox" runat="server" Text='<%# Bind("UserGroupName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqUserGroupName" runat="server" ControlToValidate="UserGroupNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertUGGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqUserGroupNameE" TargetControlID="reqUserGroupName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                                            ValidationGroup="InsertUGGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </InsertItemTemplate>
                                                                <EmptyDataTemplate>
                                                                    <table id="Table2" runat="server" style="">
                                                                        <tr>
                                                                            <td>
                                                                                Nessun gruppo presente.
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                                <EditItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="UserGroupNameTextBox" runat="server" Text='<%# Bind("UserGroupName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqUserGroupName" runat="server" ControlToValidate="UserGroupNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="EditUGGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqUserGroupNameE" TargetControlID="reqUserGroupName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Aggiorna"
                                                                                            ValidationGroup="EditUGGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:LinkButton ID="UserGroupNameButton" runat="server" CommandName="Select" Text='<%# Eval("UserGroupName") %>'
                                                                                CssClass="selectableField" />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare il gruppo?');"
                                                                                CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <SelectedItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:Label ID="UserGroupNameLabel" runat="server" Font-Bold="true" ForeColor="White"
                                                                                Text='<%# Eval("UserGroupName") %>' />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare il gruppo?');" />
                                                                        </td>
                                                                    </tr>
                                                                </SelectedItemTemplate>
                                                                <ItemSeparatorTemplate>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <hr class="hrStyle" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemSeparatorTemplate>
                                                            </asp:ListView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td id="tdUsers" runat="server" style="padding: 5px; vertical-align: top;">
                            <!-- Pannello gestione UTENTI -->
                            <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                <tr>
                                    <td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
                                    <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                        text-align: center; color: White; font-weight: bold;">UTENTI</td>
                                    <td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:UpdatePanel ID="updUsers" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:ListView ID="lvwUsers" runat="server" DataSourceID="linqUsers" InsertItemPosition="None"
                                                    OnItemInserting="lvwUsers_ItemInserting" OnItemUpdating="lvwUsers_ItemUpdating"  OnPreRender="lvwUsers_OnPreRender"
                                                    DataKeyNames="UserID" OnItemDataBound="lvwUsers_ItemDataBound" OnItemInserted="lvwUsers_ItemInserted"
                                                    OnItemCanceling="lvwUsers_ItemCanceling" OnItemEditing="lvwUsers_ItemEditing">
                                                    <LayoutTemplate>
                                                        <table id="Table1" runat="server" style="width: 100%;">
                                                            <tr id="Tr1" runat="server">
                                                                <td id="Td1" runat="server">
                                                                    <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;"
                                                                        cellpadding="1">
                                                                        <tr id="Tr2" runat="server" style="">
                                                                            <th id="Th1" runat="server" style="width: 1%;">
                                                                            </th>
                                                                            <th id="Th3" style="width: 48%;" class="gridHeader" runat="server">
                                                                                NOME
                                                                            </th>
                                                                            <th id="Th6" style="width: 25%;" class="gridHeader" runat="server">
                                                                                TELEFONO
                                                                            </th>
                                                                            <th id="Th5" style="width: 25%;" class="gridHeader" runat="server">
                                                                                E-MAIL
                                                                            </th>
                                                                            <th id="Th7" runat="server" style="width: 1%;">
                                                                            </th>
                                                                        </tr>
                                                                        <tr id="itemPlaceholder" runat="server">
                                                                        </tr>
                                                                        <tr id="UsersFooter">
                                                                            <td colspan="5" style="text-align: left;">
                                                                                <div style="text-align: left;">
                                                                                    <asp:ImageButton ID="imgbUserAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                                        OnClick="imgbUserAdd_Click" AlternateText="Inserimento Utente" ToolTip="Aggiungi" /></div>
                                                                                <div style="text-align: right;">
                                                                                    <asp:DataPager runat="server" ID="UsersPager" PageSize="10">
                                                                                        <Fields>
                                                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="true" ShowNextPageButton="false"
                                                                                                ButtonCssClass="menu_link2" ShowPreviousPageButton="false" ShowLastPageButton="false"
                                                                                                FirstPageText="Primo" />
                                                                                            <asp:NumericPagerField ButtonCount="10" NumericButtonCssClass="menu_link2" NextPreviousButtonCssClass="menu_link2" />
                                                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowNextPageButton="false"
                                                                                                ButtonCssClass="menu_link2" ShowPreviousPageButton="false" ShowLastPageButton="true"
                                                                                                LastPageText="Ultimo" />
                                                                                            <asp:NextPreviousPagerField ButtonType="Image" PreviousPageImageUrl="~/Images/prev_page.gif"
                                                                                                NextPageImageUrl="~/Images/next_page.gif" ShowFirstPageButton="false" ShowNextPageButton="true"
                                                                                                ShowPreviousPageButton="true" ShowLastPageButton="false" ButtonCssClass="pager_button" />
                                                                                        </Fields>
                                                                                    </asp:DataPager>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </LayoutTemplate>
                                                    <InsertItemTemplate>
                                                        <tr style="color: Black;">
                                                            <td>
                                                            </td>
                                                            <td colspan="4" style="text-align: left;">
                                                                <div style="display: inline;">
                                                                    <asp:TextBox ID="FirstNameTextBox" OnLoad="FirstNameTextBox_OnLoad" runat="server"
                                                                        MaxLength="256" />
                                                                    <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="FirstNameTextBox"
                                                                        ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertUserGroup">
                                                                    </asp:RequiredFieldValidator>
                                                                    <act:ValidatorCalloutExtender runat="Server" ID="reqFirstNameE" TargetControlID="reqFirstName"
                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                    <act:TextBoxWatermarkExtender ID="wkFirstName" runat="server" TargetControlID="FirstNameTextBox"
                                                                        WatermarkText="Nome" WatermarkCssClass="watermarked" />
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:TextBox ID="LastNameTextBox" OnLoad="LastNameTextBox_OnLoad" runat="server"
                                                                        MaxLength="256" />
                                                                    <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="LastNameTextBox"
                                                                        ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertUserGroup">
                                                                    </asp:RequiredFieldValidator>
                                                                    <act:ValidatorCalloutExtender runat="Server" ID="reqLastNameE" TargetControlID="reqLastName"
                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                    <act:TextBoxWatermarkExtender ID="wkLirstName" runat="server" TargetControlID="LastNameTextBox"
                                                                        WatermarkText="Cognome" WatermarkCssClass="watermarked" />
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:TextBox ID="UsernameTextBox" runat="server" MaxLength="256" />
                                                                    <asp:RequiredFieldValidator ID="reqUsername" runat="server" ControlToValidate="UsernameTextBox"
                                                                        ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertUserGroup">
                                                                    </asp:RequiredFieldValidator>
                                                                    <act:ValidatorCalloutExtender runat="Server" ID="reqUsernameE" TargetControlID="reqUsername"
                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                    <act:TextBoxWatermarkExtender ID="wkUsername" runat="server" TargetControlID="UsernameTextBox"
                                                                        WatermarkText="Username" WatermarkCssClass="watermarked" Enabled="false" />
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:DropDownList ID="OrganizationIDDropDown" runat="server" DataSourceID="linqOrganization"
                                                                        DataValueField="OrganizationID" DataTextField="OrganizationName">
                                                                    </asp:DropDownList>
                                                                    <asp:LinqDataSource ID="linqOrganization" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                                        TableName="UserOrganizations" EnableDelete="True" EnableInsert="True" EnableUpdate="True"
                                                                        OnSelecting="linqOrganizations_Selecting">
                                                                    </asp:LinqDataSource>
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:DropDownList ID="UserGroupIDDropDown" runat="server" DataSourceID="linqUserGroup"
                                                                        DataValueField="UserGroupID" DataTextField="UserGroupName">
                                                                    </asp:DropDownList>
                                                                    <asp:LinqDataSource ID="linqUserGroup" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                                        TableName="UserGroups" EnableDelete="True" EnableInsert="True" EnableUpdate="True"
                                                                        OnSelecting="linqUserGroups_Selecting">
                                                                    </asp:LinqDataSource>
                                                                </div>
                                                                <br />
                                                                <div>
                                                                    <div style="display: inline; float: left; margin-right: 10px;">
                                                                        <geta:MultiInputControl ID="micEmail" ButtonLabel="e-mail +" ToolTip="Indirizzo Email"
                                                                            MaxLength="128" ValidationError="Indirizzo E-mail non valido." ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                                                                            ValidationGroup="InsertEmail" runat="server" />
                                                                    </div>
                                                                    <div style="display: inline; float: left; margin-right: 10px;">
                                                                        <geta:MultiInputControl ID="micMobilePhone" ButtonLabel="telefono +" ToolTip="Numero di telefono"
                                                                            MaxLength="10" ValidationError="Numero di cellulare non valido." ValidationExpression="\d{10}"
                                                                            ValidationGroup="InsertPhone" runat="server"></geta:MultiInputControl>
                                                                    </div>
                                                                </div>
                                                                <br style="clear: both" />
                                                                <br style="clear: both" />
                                                                <center>
                                                                    <table cellpadding="4" cellspacing="4" style="text-align: left;">
                                                                        <tr>
                                                                            <td style="color: White; text-align: left;">
                                                                                <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                                    ValidationGroup="InsertUserGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                <span>| </span>
                                                                                <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                    ForeColor="White" CssClass="noDecorationLink" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </center>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </InsertItemTemplate>
                                                    <EmptyDataTemplate>
                                                        <table id="Table2" runat="server" style="">
                                                            <tr>
                                                                <td>
                                                                    Nessun utente presente.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </EmptyDataTemplate>
                                                    <EditItemTemplate>
                                                        <tr style="color: Black;">
                                                            <td>
                                                            </td>
                                                            <td colspan="4" style="text-align: left;">
                                                                <div style="display: inline;">
                                                                    <asp:TextBox ID="FirstNameTextBox" runat="server" MaxLength="256" Text='<%# Bind("FirstName") %>' />
                                                                    <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="FirstNameTextBox"
                                                                        ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="UpdateUserGroup">
                                                                    </asp:RequiredFieldValidator>
                                                                    <act:ValidatorCalloutExtender runat="Server" ID="reqFirstNameE" TargetControlID="reqFirstName"
                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                    <act:TextBoxWatermarkExtender ID="wkFirstName" runat="server" TargetControlID="FirstNameTextBox"
                                                                        WatermarkText="Nome" WatermarkCssClass="watermarked" />
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:TextBox ID="LastNameTextBox" runat="server" MaxLength="256" Text='<%# Bind("LastName") %>' />
                                                                    <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="LastNameTextBox"
                                                                        ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="UpdateUserGroup">
                                                                    </asp:RequiredFieldValidator>
                                                                    <act:ValidatorCalloutExtender runat="Server" ID="reqLastNameE" TargetControlID="reqLastName"
                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                    <act:TextBoxWatermarkExtender ID="wkLirstName" runat="server" TargetControlID="LastNameTextBox"
                                                                        WatermarkText="Cognome" WatermarkCssClass="watermarked" />
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:TextBox ID="UsernameTextBox" runat="server" MaxLength="256" Text='<%# Bind("Username") %>' />
                                                                    <asp:RequiredFieldValidator ID="reqUsername" runat="server" ControlToValidate="UsernameTextBox"
                                                                        ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="UpdateUserGroup">
                                                                    </asp:RequiredFieldValidator>
                                                                    <act:ValidatorCalloutExtender runat="Server" ID="reqUsernameE" TargetControlID="reqUsername"
                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                    <act:TextBoxWatermarkExtender ID="wkUsername" runat="server" TargetControlID="UsernameTextBox"
                                                                        WatermarkText="Username" WatermarkCssClass="watermarked" />
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:DropDownList ID="OrganizationIDDropDown" runat="server" DataSourceID="linqOrganization"
                                                                        DataValueField="OrganizationID" DataTextField="OrganizationName" OnDataBound="OrganizationIDDropDown_Ondatabound"
                                                                        OnDataBinding="OrganizationIDDropDown_Ondatabinding">
                                                                        <asp:ListItem Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:LinqDataSource ID="linqOrganization" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                                        TableName="UserOrganizations" OnSelecting="linqOrganizations_Selecting">
                                                                    </asp:LinqDataSource>
                                                                </div>
                                                                <div style="display: inline;">
                                                                    <asp:DropDownList ID="UserGroupIDDropDown" runat="server" DataSourceID="linqUserGroup"
                                                                        DataValueField="UserGroupID" DataTextField="UserGroupName">
                                                                        <asp:ListItem Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:LinqDataSource ID="linqUserGroup" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                                        TableName="UserGroups" EnableDelete="True" EnableInsert="True" EnableUpdate="True"
                                                                        OnSelecting="linqUserGroups_Selecting">
                                                                    </asp:LinqDataSource>
                                                                </div>
                                                                <br />
                                                                <div>
                                                                    <div style="display: block; float: left; margin-right: 10px">
                                                                        <geta:MultiInputControl ID="micEmail" ButtonLabel="e-mail +" MaxLength="128" ValidationError="Indirizzo E-mail non valido."
                                                                            ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                                                                            ValidationGroup="UpdateEmail" runat="server" />
                                                                    </div>
                                                                    <div style="display: block; float: left; margin-right: 10px">
                                                                        <geta:MultiInputControl ID="micMobilePhone" ButtonLabel="telefono +" MaxLength="10"
                                                                            ValidationError="Numero di cellulare non valido." ValidationExpression="\d{10}"
                                                                            ValidationGroup="UpdatePhone" runat="server" />
                                                                    </div>
                                                                </div>
                                                                <br style="clear: both" />
                                                                <br style="clear: both" />
                                                                <center>
                                                                    <table cellpadding="4" cellspacing="4" style="text-align: left;">
                                                                        <tr>
                                                                            <td style="color: White; text-align: left;">
                                                                                <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Aggiorna"
                                                                                    ValidationGroup="UpdateUserGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                <span>| </span>
                                                                                <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                    ForeColor="White" CssClass="noDecorationLink" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </center>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 1%;">
                                                                <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                    ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink" />
                                                            </td>
                                                            <td class="gridItem">
                                                                <asp:Label ID="UserGroupNameLabel" runat="server" Text='<%# Eval("Nominativo") %>'
                                                                    CssClass="selectableField" />
                                                            </td>
                                                            <td class="gridItem">
                                                                &nbsp;<asp:Label ID="Label3" runat="server" Text='<%# Eval("MobilePhone").ToString().Replace("|", "<br/>&nbsp;") %>' CssClass="selectableField" />
                                                            </td>
                                                            <td class="gridItem">
                                                                &nbsp;<asp:Label ID="Label2" runat="server" Text='<%# Eval("Email").ToString().Replace("|", "<br/>&nbsp;") %>' CssClass="selectableField" />
                                                            </td>
                                                            <td style="width: 1%;">
                                                                <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                    ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare l\'utente?');"
                                                                    CssClass="noDecorationLink" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <ItemSeparatorTemplate>
                                                        <tr>
                                                            <td colspan="6">
                                                                <hr class="hrStyle" />
                                                            </td>
                                                        </tr>
                                                    </ItemSeparatorTemplate>
                                                </asp:ListView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                    <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                    <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:LinqDataSource ID="linqOrganizations" runat="server" OrderBy="OrganizationName"
        ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext" EnableDelete="True"
        EnableInsert="True" EnableUpdate="True" TableName="UserOrganizations" OnSelecting="linqOrganizations_Selecting">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="linqUserGroups" runat="server" OrderBy="UserGroupName" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
        EnableDelete="True" EnableInsert="True" EnableUpdate="True" TableName="UserGroups"
        OnSelecting="linqUserGroups_Selecting">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="linqUsers" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
        EnableDelete="True" EnableInsert="True" EnableUpdate="True" TableName="Users"
        OnSelecting="linqUsers_Selecting" OnInserting="linqUsers_Inserting" OnUpdating="linqUsers_Updating">
        <SelectParameters>
            <asp:Parameter Name="UserGroupID" DefaultValue="-1" Type="Int32" />
            <asp:Parameter Name="OrganizationID" DefaultValue="-1" Type="Int32" />
        </SelectParameters>
    </asp:LinqDataSource>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="Reports.aspx.cs" Inherits="Reports" Title="GETA - Reports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Src="~/GetaCheckBox.ascx" TagName="GetaCheckBox" TagPrefix="geta" %>
<asp:Content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 1200px;">
        <tr>
            <td align="left">
                <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
                    <tr>
                        <td id="tdReportListSelector" runat="server" class="ToolbarButtonToggleOn">
                            <asp:Button ID="btnReportListSelector" runat="server" Text="ESEGUI REPORT" CssClass="ToolbarButton"
                                PostBackUrl="~/Reports.aspx" />
                        </td>
                        <td id="tdReportUpdateSelector" runat="server" class="ToolbarButtonToggleOff">
                            <asp:Button ID="btnReportUpdateSelector" runat="server" Text="GESTIONE REPORT" CssClass="ToolbarButton"
                                PostBackUrl="~/SaveReport.aspx" />
                        </td>
                        <td class="ToolbarTitle">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="SubToolbar" colspan="3">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="updReport" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:MultiView ID="mvReport" ActiveViewIndex="0" runat="server">
                            <asp:View runat="server">
                                <table cellpadding="4" cellspacing="4" style="width: 100%;">
                                    <tr>
                                        <td style="width: 35%; vertical-align: top;">
                                            <!-- Lista Reports Disponibili -->
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                                <tr>
                                                    <td>
                                                        <img src="Images/lightpanel_top_sx.gif" alt="" />
                                                    </td>
                                                    <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                                        text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <img src="Images/lightpanel_top_dx.gif" alt="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="left" style="padding-left: 10px;">
                                                        <asp:Label ID="lblReports" runat="server" ForeColor="White" Font-Bold="true" EnableViewState="false">RICERCA REPORT</asp:Label>
                                                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                                        <asp:Button ID="btnSearch" OnClick="btnSearch_OnClick" CausesValidation="false" Text="Cerca" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:UpdatePanel ID="updReports" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:ListView ID="lvwReports" runat="server" DataSourceID="linqReports" InsertItemPosition="None"
                                                                    DataKeyNames="QueryName" OnSelectedIndexChanged="lvwReports_SelectedIndexChanged">
                                                                    <LayoutTemplate>
                                                                        <div style="height: 500px; overflow: auto;">
                                                                        <table id="Table1" runat="server" style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                            <tr id="Tr1" runat="server">
                                                                                <td id="Td1" runat="server">
                                                                                    <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;">
                                                                                        <tr id="itemPlaceholder" runat="server">
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        </div>
                                                                    </LayoutTemplate>
                                                                    <EmptyDataTemplate>
                                                                        <table id="Table1" style="width: 100%;">
                                                                            <tr id="Tr1">
                                                                                <td id="Td1">
                                                                                    <table border="0" style="width: 100%;">
                                                                                        <tr>
                                                                                            <td>
                                                                                                Nessun Report.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 1%;">
                                                                                <asp:ImageButton ID="imgbEdit" runat="Server" CausesValidation="false" ImageUrl="~/Images/edit.gif"
                                                                                    AlternateText="Modifica" ToolTip="Modifica" CommandName="Select" CssClass="noDecorationLink" />
                                                                            </td>
                                                                            <td style="width: 98%; text-align: left; white-space: nowrap;" class="gridItem">
                                                                                <asp:LinkButton ID="ItemButton" runat="server" Font-Bold="true" CommandName="Select"
                                                                                    Text='<%# Eval("QueryName") %>' CssClass="selectableField" CausesValidation="false" />
                                                                            </td>
                                                                            <td style="width: 1%;">
                                                                                <asp:ImageButton ID="imgbDelete" runat="Server" CausesValidation="false" ImageUrl="~/Images/delete.gif"
                                                                                    AlternateText="Elimina" ToolTip="Elimina" CommandName="Delete" OnClientClick="return confirm('Confermi di voler cancellare l\' azienda?');"
                                                                                    CssClass="noDecorationLink" />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <SelectedItemTemplate>
                                                                        <tr>
                                                                            <td style="width: 1%;">
                                                                                <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                    ToolTip="Modifica" CommandName="Select" CausesValidation="false" />
                                                                            </td>
                                                                            <td style="width: 98%; text-align: left; white-space: nowrap;" class="gridItem">
                                                                                <asp:Label ID="QueryNameLabel" runat="server" Font-Bold="true" ForeColor="White"
                                                                                    Text='<%# Eval("QueryName") %>' />
                                                                            </td>
                                                                            <td style="width: 1%;">
                                                                                <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                    ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare l\' azienda?');" />
                                                                            </td>
                                                                        </tr>
                                                                    </SelectedItemTemplate>
                                                                    <ItemSeparatorTemplate>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <hr class="hrStyle" />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemSeparatorTemplate>
                                                                </asp:ListView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:LinqDataSource ID="linqReports" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBSavedReportQueriesDataContext"
                                                            Where="QueryName.Contains(@QueryName)" OrderBy="QueryName" Select="new (QueryName, QueryText)"
                                                            TableName="SavedReportQueries">
                                                            <WhereParameters>
                                                                <asp:ControlParameter ConvertEmptyStringToNull="false" ControlID="txtSearch" Name="QueryName"
                                                                    PropertyName="Text" Type="String" />
                                                            </WhereParameters>
                                                        </asp:LinqDataSource>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                                    <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                                    <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 70%; vertical-align: top;">
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                                <tr>
                                                    <td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
                                                    <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                                        text-align: center;">&nbsp;</td>
                                                    <td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblReportParameters" runat="server" ForeColor="White" Font-Bold="true"
                                                            EnableViewState="false">PARAMETRI REPORT</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Panel ID="pnlReportParameters" Visible="false" runat="server">
                                                            <asp:DataGrid ID="dgReportParameters" BorderWidth="0" runat="server" OnItemDataBound="dgReportParameters_OnItemDataBound"
                                                                Width="100%" AutoGenerateColumns="false" ShowHeader="false" DataKeyField="Value"
                                                                CellSpacing="10">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-ForeColor="White" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Right"
                                                                        HeaderStyle-Font-Bold="true" HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:Label ForeColor="White" ID="txtFieldName" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="" HeaderStyle-ForeColor="White" HeaderStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtFieldValue" Visible="false" runat="server" />
                                                                            <act:CalendarExtender ID="ceDate" Enabled="false" runat="server" TargetControlID="txtFieldValue"
                                                                                Format="dd-MM-yyyy">
                                                                            </act:CalendarExtender>
                                                                            <asp:RequiredFieldValidator ID="rfvFieldValue" runat="server" ControlToValidate="txtFieldValue"
                                                                                Display="Dynamic" EnableClientScript="true" ErrorMessage="Parametro obbligatorio"
                                                                                Enabled="false" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="cvFieldValue" runat="server" ControlToValidate="txtFieldValue"
                                                                                Display="Dynamic" EnableClientScript="true" ErrorMessage="Formato non valido"
                                                                                Enabled="false" Type="Date" Operator="DataTypeCheck" SetFocusOnError="true">
                                                                            </asp:CompareValidator>
                                                                            <asp:CheckBox ID="chkFieldValue" runat="server" Visible="false" />
                                                                            <asp:DropDownList ID="ddlFieldValue" runat="server" Visible="false" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:Panel>
                                                        <asp:Button ID="btnExecute" OnClick="btnExecute_OnClick" Text="Esegui" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                                    <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                                    <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View runat="server">
                                <!-- Visualizzazione Report -->
                                <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                    <tr>
                                        <td>
                                            <img src="Images/lightpanel_top_sx.gif" alt="" />
                                        </td>
                                        <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                            text-align: center;">
                                            <asp:Label Font-Bold="true" ForeColor="White" ID="lblRptName" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <img src="Images/lightpanel_top_dx.gif" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Button ID="btnBacktop" OnClick="btnBack_OnClick" Text="Indietro" runat="server" />
                                            <asp:Button ID="btnAggiornaTop" OnClick="btnExecute_OnClick" Text="Aggiorna" runat="server" />
                                            <asp:Button ID="btnExcelTop" OnClick="btnExcel_OnClick" PostBackUrl="~/Reports.aspx"
                                                Text="Esporta" runat="server" />
                                            <br />
                                            <asp:Panel ID="pnlReport" runat="server">
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label><br />
                                                <asp:DataGrid ID="dgReport" runat="server" Width="100%" BorderWidth="0" HeaderStyle-CssClass="gridHeader">
                                                </asp:DataGrid>
                                            </asp:Panel>
                                            <br />
                                            <asp:Button ID="btnBack" OnClick="btnBack_OnClick" Text="Indietro" runat="server" />
                                            <asp:Button ID="btnAggiorna" OnClick="btnExecute_OnClick" Text="Aggiorna" runat="server" />
                                            <asp:Button ID="btnExcel" OnClick="btnExcel_OnClick" PostBackUrl="~/Reports.aspx"
                                                Text="Esporta" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="Images/lightpanel_bot_sx.gif" alt="" />
                                        </td>
                                        <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <img src="Images/lightpanel_bot_dx.gif" alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                        </asp:MultiView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

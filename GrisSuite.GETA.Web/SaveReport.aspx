﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SaveReport.aspx.cs" Inherits="SaveReport" Title="Untitled Page" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="act" %>
<asp:content ID="cntMainArea" ContentPlaceHolderID="cphMainArea" Runat="Server">
	<table border="0" cellpadding="0" cellspacing="0" style="width: 1200px;">
		<tr>
			<td align="left">
				<table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
					<tr>
						<td id="tdReportListSelector" runat="server" class="ToolbarButtonToggleOff">
							<asp:button id="btnReportListSelector" runat="server" text="ESEGUI REPORT" cssclass="ToolbarButton" postbackurl="~/Reports.aspx" />
						</td>
						<td id="tdReportUpdateSelector" runat="server" class="ToolbarButtonToggleOn">
							<asp:button id="btnReportUpdateSelector" runat="server" text="GESTIONE REPORT" cssclass="ToolbarButton" postbackurl="~/SaveReport.aspx" />
						</td>
						<td class="ToolbarTitle">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="SubToolbar" colspan="3">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table cellpadding="4" cellspacing="4" style="width: 100%;">
					<tr>
						<td style="width: 35%; vertical-align: top;">
							<!-- Lista Reports -->
							<table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
								<tr>
									<td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
									<td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x; text-align: center;">&nbsp;</td>
									<td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
								</tr>
								<tr>
                                    <td colspan="3" align="left" style="padding-left: 10px;">
                                        <asp:Label ID="lblReports" runat="server" ForeColor="White" Font-Bold="true" EnableViewState="false">RICERCA REPORT</asp:Label>
                                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnSearch" OnClick="btnSearch_OnClick" CausesValidation="false" Text="Cerca" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
								<tr>
									<td colspan="3">
										<asp:updatepanel id="updReports" runat="server" updatemode="Conditional">
											<contenttemplate>
												<asp:listview id="lvwReports" runat="server" 
													datasourceid="linqReports" insertitemposition="None" 
													datakeynames="QueryName" 
													onselectedindexchanged="lvwReports_SelectedIndexChanged">
													<layouttemplate>
													<div style="height: 500px; overflow: auto;">
														<table id="Table1" runat="server" style="width: 100%;" cellpadding="0" cellspacing="0">
															<tr id="Tr1" runat="server">
																<td id="Td1" runat="server">
																	<table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;">
																		<tr id="itemPlaceholder" runat="server"></tr>
																		<tr id="OrgFooter"><td colspan="3" style="text-align: left;"><asp:imagebutton id="imgbQryAdd" runat="server" imageurl="~/Images/lightadd.gif" CausesValidation="false" onclick="imgbQryAdd_Click" alternatetext="Inserimento Query" tooltip="Aggiungi" /></td></tr>
																	</table>
																</td>
															</tr>
														</table>
														</div>
													</layouttemplate>
													<emptydatatemplate>
														<table id="Table1" style="width: 100%;">
															<tr id="Tr1">
																<td id="Td1">
																	<table border="0" style="width: 100%;">
																		<tr><td>Nessun Report.</td></tr>
																		<tr id="OrgFooter"><td style="text-align: left;"><asp:imagebutton id="imgbQryAdd" runat="server" imageurl="~/Images/lightadd.gif" onclick="imgbQryAdd_Click" CausesValidation="false" alternatetext="Inserimento Query" tooltip="Aggiungi" /></td></tr>
																	</table>
																</td>
															</tr>
														</table>
													</emptydatatemplate>
													<itemtemplate>
														<tr>
															<td style="width: 1%;">
																<asp:imagebutton ID="imgbEdit" runat="Server" CausesValidation="false" imageurl="~/Images/edit.gif" alternatetext="Modifica" tooltip="Modifica" CommandName="Select" cssclass="noDecorationLink" />
															</td>
															<td style="width: 98%; text-align: left; white-space: nowrap;" class="gridItem" >
																<asp:linkbutton id="ItemButton" runat="server" font-bold="true" commandname="Select"
																	text='<%# Eval("QueryName") %>' cssclass="selectableField" CausesValidation="false" />
															</td>
															<td style="width: 1%;">
																<asp:imagebutton ID="imgbDelete" runat="Server" CausesValidation="false" imageurl="~/Images/delete.gif" alternatetext="Elimina" tooltip="Elimina" CommandName="Delete" onclientclick="return confirm('Confermi di voler cancellare l\' azienda?');" cssclass="noDecorationLink" />
															</td>
														</tr>
													</itemtemplate>
													<selecteditemtemplate>
														<tr>
															<td style="width: 1%;">
																<asp:imagebutton ID="imgbEdit" runat="Server" imageurl="~/Images/edit.gif" alternatetext="Modifica" tooltip="Modifica" CommandName="Select" causesvalidation="false" />
															</td>
															<td style="width: 98%; text-align: left; white-space: nowrap;" class="gridItem">
																<asp:label id="QueryNameLabel" runat="server" font-bold="true" forecolor="White"
																	text='<%# Eval("QueryName") %>' />
															</td>
															<td style="width: 1%;">
																<asp:imagebutton ID="imgbDelete" runat="Server" imageurl="~/Images/delete.gif" alternatetext="Elimina" tooltip="Elimina" CommandName="Delete" causesvalidation="false" onclientclick="return confirm('Confermi di voler cancellare l\' azienda?');" />
															</td>
														</tr>																
													</selecteditemtemplate>
													<itemseparatortemplate>
														<tr><td colspan="3"><hr class="hrStyle" /></td></tr>
													</itemseparatortemplate>
												</asp:listview>
												<asp:label id="lblNoResults" runat="server" visible="false" text="Nessun Report."></asp:label>
											</contenttemplate>
										</asp:updatepanel>
										<asp:linqdatasource id="linqReports" runat="server" 
											contexttypename="GrisSuite.GETA.Data.DBSavedReportQueriesDataContext" Where="QueryName.Contains(@QueryName)"
											orderby="QueryName"	tablename="SavedReportQueries" enabledelete="true">
											<WhereParameters>
                                                <asp:ControlParameter ConvertEmptyStringToNull="false" ControlID="txtSearch" Name="QueryName"
                                                    PropertyName="Text" Type="String" />
                                            </WhereParameters>
										</asp:linqdatasource>
									</td>
								</tr>
								<tr><td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td><td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td><td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td></tr>
							</table>
						</td>
						<td style="width: 70%; vertical-align: top;">
							<!-- Input Query -->
							<table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
								<tr>
									<td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
									<td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x; text-align: center;">&nbsp;</td>
									<td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
								</tr>
								<tr>
									<td colspan="3"><asp:label id="lblTitle" runat="server" forecolor="White" 
											font-bold="true" enableviewstate="false">REPORT QUERY</asp:label></td>
								</tr>
								<tr>
									<td colspan="3">
										<asp:updatepanel id="updReport" runat="server" updatemode="Conditional">
											<contenttemplate>
												<table id="tblReport" runat="server" cellpadding="10" cellspacing="10" style="width: 100%;" visible="false">
													<tr>
														<td>
															<asp:textbox id="txtQueryName" runat="server" maxlength="256" width="100%"></asp:textbox>
															<asp:requiredfieldvalidator id="reqQueryName" runat="server" 
																controltovalidate="txtQueryName" errormessage="Campo Richiesto" 
																display="None"></asp:requiredfieldvalidator>
															<act:ValidatorCalloutExtender runat="Server" ID="reqQueryNameE"
																TargetControlID="reqQueryName"
																HighlightCssClass="validatorCalloutHighlight" />
														</td>
													</tr>
													<tr>
														<td>
															<asp:label id="lblSuggest" runat="server" enableviewstate="false" text="E' preferibile indicare i nomi dei campi tra parentesi quadre."></asp:label><br />
															<asp:textbox id="txtQueryText" runat="server" textmode="MultiLine" width="100%" rows="15"></asp:textbox>
															<asp:requiredfieldvalidator id="reqQueryText" runat="server" 
																controltovalidate="txtQueryText" errormessage="Campo Richiesto" 
																display="None"></asp:requiredfieldvalidator>
															<act:ValidatorCalloutExtender runat="Server" ID="reqQueryTextE"
																TargetControlID="reqQueryText"
																HighlightCssClass="validatorCalloutHighlight" />
														</td>
													</tr>
													<tr>
														<td style="text-align: left;">
															<asp:button id="btnSaveQuery" runat="server" enableviewstate="false" 
																text="Salva" onclick="btnSaveQuery_Click" />
                                                            <asp:button id="btnCancel" runat="server" enableviewstate="false" 
																text="Indietro" onclick="btnCancel_Click" CausesValidation="false" />
														</td>
													</tr>
													<tr>
														<td style="text-align: left;">
															<asp:label id="lblMessage" runat="server" enableviewstate="false"></asp:label>
														</td>
													</tr>
												</table>
											</contenttemplate>
										</asp:updatepanel>
									</td>
								</tr>
								<tr><td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td><td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td><td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td></tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:content>


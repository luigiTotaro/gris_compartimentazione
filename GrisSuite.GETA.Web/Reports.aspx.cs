﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using GrisSuite.GETA.Data;
using System.Text.RegularExpressions;
using Edulife.Presentation.Web.Components;

public partial class Reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Utility.IsUserInAdminGroup())
        {
            this.Response.Redirect("~/Default.aspx");
        }

        ((IGetaMasterPage)this.Master).GetaLayoutMode = GetaLayoutModeEnum.Reports;
    }

    protected void btnExecute_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string queryName = this.lvwReports.SelectedValue.ToString();

            if (!string.IsNullOrEmpty(queryName))
            {
                DBSavedReportQueriesDataContext db = new DBSavedReportQueriesDataContext();
                SavedReportQuery query = db.SavedReportQueries.SingleOrDefault(qry => qry.QueryName == queryName);
                if (query != null)
                {
                    SqlDataAdapter da = new SqlDataAdapter(query.QueryText, db.Connection.ConnectionString);
                    PrepareCommand(da.SelectCommand);
                    DataSet ds = CreateDataSet(da);

                    if (ds != null)
                    {
                        mvReport.ActiveViewIndex = 1;

                        this.pnlReport.Visible = true;
                        this.dgReport.DataSource = ds.Tables["Report"];
                        this.dgReport.DataBind();

                        this.lblMessage.Text = (dgReport.Items.Count == 0) ? "Nessun record estratto" : "";
                        this.dgReport.Visible = (dgReport.Items.Count > 0);
                        this.lblRptName.Text = queryName;

                        this.updReport.Update();
                    }
                }
            }
        }
    }

    protected void btnExcel_OnClick(object sender, EventArgs e)
    {
        string queryName = this.lvwReports.SelectedValue.ToString();

        if (!string.IsNullOrEmpty(queryName))
        {
            DBSavedReportQueriesDataContext db = new DBSavedReportQueriesDataContext();
            SavedReportQuery query = db.SavedReportQueries.SingleOrDefault(qry => qry.QueryName == queryName);

            if (query != null)
            {
                SqlDataAdapter da = new SqlDataAdapter(query.QueryText, db.Connection.ConnectionString);
                PrepareCommand(da.SelectCommand);    
                DataSet ds = CreateDataSet(da);
                
                ExcelTransferUtility excelUtil = new ExcelTransferUtility(Server);
                excelUtil.Transfer(ds);

                this.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}_{1}.xls", queryName, DateTime.Today.ToShortDateString()));
                this.Response.ContentType = "application/ms-excel";
                this.Response.Write(excelUtil.Result);
                this.Response.End();
            }
        }
    }
   
    protected void btnBack_OnClick(object sender, EventArgs e)
    {
        mvReport.ActiveViewIndex = 0;
        //lvwReports.SelectedIndex = -1;
        //pnlReportParameters.Visible = false;
        updReport.Update();
    }

    protected void lvwReports_SelectedIndexChanged(object sender, EventArgs e)
    {
        string queryName = this.lvwReports.SelectedValue.ToString();

        if (!string.IsNullOrEmpty(queryName))
        {
            DBSavedReportQueriesDataContext db = new DBSavedReportQueriesDataContext();

            SavedReportQuery query = db.SavedReportQueries.SingleOrDefault(qry => qry.QueryName == queryName);

            if (query != null)
            {

                Regex regex = new Regex(@"(@[\w:\[\]]*[\s-[\n\r\t]]?)");
                MatchCollection mc = regex.Matches(query.QueryText);
                if (mc.Count > 0)
                {
                    this.dgReportParameters.DataSource = mc;
                    this.dgReportParameters.DataBind();
                    this.pnlReportParameters.Visible = true;

                    updReports.Update();

                    this.pnlReport.Visible = false;
                    this.updReport.Update();

                    return;
                }
                else
                {
                    this.dgReportParameters.DataSource = null;
                    this.dgReportParameters.DataBind();
                    this.pnlReportParameters.Visible = true;
                    updReports.Update();
                    this.pnlReport.Visible = false;
                    this.updReport.Update();
                }

                SqlDataAdapter da = new SqlDataAdapter(query.QueryText, db.Connection.ConnectionString);
                CreateDataSet(da);
            }
        }
    }    

    protected void dgReportParameters_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item
            || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Match mc = e.Item.DataItem as Match;
            TextBox txt = e.Item.FindControl("txtFieldValue") as TextBox;
            AjaxControlToolkit.CalendarExtender cal = e.Item.FindControl("ceDate") as AjaxControlToolkit.CalendarExtender;
            CompareValidator cv = e.Item.FindControl("cvFieldValue") as CompareValidator;
            //CompareValidator cvInt = e.Item.FindControl("covalInt") as CompareValidator;
            RequiredFieldValidator rfv = e.Item.FindControl("rfvFieldValue") as RequiredFieldValidator;
            CheckBox ga = e.Item.FindControl("chkFieldValue") as CheckBox;
            DropDownList ddl = e.Item.FindControl("ddlFieldValue") as DropDownList;

            (e.Item.FindControl("txtFieldName") as Label).Text = GetParamName(mc.Value);

            string dataType = GetDataType(mc.Value);

            if (dataType.Equals("DateTime", StringComparison.InvariantCultureIgnoreCase))
            {
                txt.Visible = true;
                cal.Enabled = true;
                cv.Enabled = true;
                cv.Type = ValidationDataType.Date;
                rfv.Enabled = true;
            }
            else if (dataType.Equals("bit", StringComparison.InvariantCultureIgnoreCase))
            {
                ga.Visible = true;
            }
            else if (dataType.Equals("int", StringComparison.InvariantCultureIgnoreCase))
            {
                txt.Visible = true;
                cv.Enabled = true;
                cv.Type = ValidationDataType.Integer;
                rfv.Enabled = true;
            }
            else if (dataType.Equals("double", StringComparison.InvariantCultureIgnoreCase))
            {
                txt.Visible = true;
                cv.Enabled = true;
                cv.Type = ValidationDataType.Double;
                rfv.Enabled = true;
            }
            else if (dataType.StartsWith("Attribute", StringComparison.InvariantCultureIgnoreCase))
            {
                TargetActivity ta = GetTargetActivity(dataType);
                if (ta != null)
                {
                    switch (ta.ControlTypeUI)
                    {
                        case "CheckBox":
                            ga.Visible = true;
                            break;
                        case "ComboBox":
                            ddl.Visible = true;
                            ddl.Items.Clear();
                            if (!string.IsNullOrEmpty(ta.EnumValues))
                            {
                                var cboValues = from cboValue in ta.EnumValues.Split('|')
                                                select new ListItem(cboValue);
                                ddl.Items.AddRange(cboValues.ToArray());
                            }
                            break;
                        default:
                            txt.Visible = true;
                            break;
                    }
                }
            }
            else
            {
                txt.Visible = true;
            }
        }
    }

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        lvwReports.DataBind();
        lvwReports.SelectedIndex = -1;
        pnlReportParameters.Visible = false;
        updReport.Update();
    }


    #region Private Methods

    private TargetActivity GetTargetActivity(string dataType)
    {
        int bracketOpen = dataType.IndexOf('[');
        int bracketClose = dataType.IndexOf(']');
        string idAttribute = dataType.Substring(bracketOpen + 1, bracketClose - bracketOpen - 1);
        DBIssuesDataContext dbIssue = new DBIssuesDataContext();
        var atts = from att in dbIssue.TargetActivities
                   where att.TargetActivityID == Int32.Parse(idAttribute)
                   select att;
        TargetActivity ta = null;
        if (atts.Count() > 0) ta = atts.First();
        return ta;
    }

    private string GetParamName(string param)
    {
        int startIndex = param.IndexOf("@") + 1;
        int endIndex = param.IndexOf(":");
        return endIndex >= 0 ? param.Substring(startIndex, endIndex - startIndex) : param.Substring(startIndex);
    }

    private string GetDataType(string param)
    {
        int startIndex = param.IndexOf("@") + 1;
        int endIndex = param.IndexOf(":");
        string dataType = string.Empty;
        if (endIndex >= 0)
        {
            dataType = param.Substring(endIndex + 1);
        }
        return dataType;
    }

    private void PrepareCommand(SqlCommand sqlCmd)
    {
        foreach (DataGridItem item in dgReportParameters.Items)
        {
            SqlParameter sp = null;
            Label lb = item.Cells[0].FindControl("txtFieldName") as Label;
            TextBox txt = item.Cells[1].FindControl("txtFieldValue") as TextBox;
            CheckBox ga = (item.FindControl("chkFieldValue") as CheckBox);
            DropDownList ddl = (item.FindControl("ddlFieldValue") as DropDownList);                     

            string param = dgReportParameters.DataKeys[item.ItemIndex].ToString();
            sqlCmd.CommandText = sqlCmd.CommandText.Replace(param, "@" + lb.Text);

            string dataType = GetDataType(param);

            if (dataType.StartsWith("DateTime", StringComparison.InvariantCultureIgnoreCase))
            {
                sp = new SqlParameter("@" + lb.Text, txt.Text);
                sp.SqlDbType = SqlDbType.DateTime;
            }
            else if (dataType.StartsWith("Bit", StringComparison.InvariantCultureIgnoreCase))
            {
                sp = new SqlParameter("@" + lb.Text, ga.Checked);
                sp.SqlDbType = SqlDbType.Bit;
            }
            else if (dataType.StartsWith("int", StringComparison.InvariantCultureIgnoreCase))
            {
                sp = new SqlParameter("@" + lb.Text, Int32.Parse(txt.Text));
                sp.SqlDbType = SqlDbType.Int;
            }
            else if (dataType.StartsWith("double", StringComparison.InvariantCultureIgnoreCase))
            {
                sp = new SqlParameter("@" + lb.Text, Double.Parse(txt.Text));
                sp.SqlDbType = SqlDbType.Decimal;
            }
            else if (dataType.StartsWith("Attribute", StringComparison.InvariantCultureIgnoreCase))
            {
                TargetActivity ta = GetTargetActivity(dataType);

                if (ta != null)
                {
                    switch (ta.ControlTypeUI)
                    {
                        case "CheckBox":
                            sp = new SqlParameter("@" + lb.Text, ga.Checked.ToString());
                            break;
                        case "ComboBox":                            
                            sp = new SqlParameter("@" + lb.Text, ddl.SelectedValue);
                            break;
                        default:
                            sp = new SqlParameter("@" + lb.Text, txt.Text);
                            break;
                    }
                }
            }
            else
            {
                sp = new SqlParameter("@" + lb.Text, txt.Text);
            }

            sqlCmd.Parameters.Add(sp);
        }
    }

    private DataSet CreateDataSet(SqlDataAdapter da)
    {
        DataSet ds = null;
        try
        {
            ds = new DataSet();
            da.Fill(ds, "Report");
        }
        catch (Exception ex)
        {
            ds = null;
            this.lblMessage.Text = "Errore durante l'esecuzione della query." + ex.Message;
            mvReport.ActiveViewIndex = 1;
            this.pnlReport.Visible = true;
            this.dgReport.Visible = false;
            this.updReport.Update();
        }

        return ds;
    }

    #endregion
}



﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" Title="GETA - Interventi" %>

<%@ Register Src="~/GetaCheckBox.ascx" TagName="GetaCheckBox" TagPrefix="geta" %>
<%@ Register Src="~/GoTo.ascx" TagName="GoTo" TagPrefix="geta" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainArea" runat="Server">

    <script id="jsNoRowMessage" type="text/javascript" language="javascript"></script>

    <asp:UpdatePanel ID="updNavToolbar" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr style="background-image: url(Images/nav_bg.gif); background-repeat: repeat-x">
                    <td style="height: 30px; text-align: left;">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNodePath" style="line-height: 20px" runat="server" CssClass="target_infos_titlebar"></asp:Label>
                                    <asp:DropDownList ID="ddlNodes" Visible="false" OnSelectedIndexChanged="ddlNodes_OnSelectedIndexChanged"
                                        AutoPostBack="true" DataTextField="Name" DataValueField="NodId" runat="server"></asp:DropDownList>                                                                            				                                                                              
                                </td>
                                <td>
                                    <asp:Label ID="lblIP" runat="server" CssClass="target_infos_titlebar"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblHost" runat="server" CssClass="target_infos_titlebar"></asp:Label>                                    
                                </td>
                                <td>
                                    <table id="pnlStlcArrows" style="width: 40px; padding: 0px; margin: 0px" runat="server"
                                        border="0" cellpadding="0" cellspacing="0" visible="false">
                                        <tr>
                                            <td>
                                                <asp:ImageButton BorderWidth="0" ID="imgPrevStlc" OnClick="imgPrevStlc_OnClick" runat="server"
                                                    ImageUrl="~/Images/prev_stlc.gif" />
                                            </td>
                                            <td>
                                                <asp:ImageButton BorderWidth="0" ID="imgNextStlc" OnClick="imgNextStlc_OnClick" runat="server"
                                                    ImageUrl="~/Images/next_stlc.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 400px; text-align: right; padding: 0px; padding-right: 10px;">
                        <asp:UpdatePanel ID="updGoTo" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <geta:GoTo ID="gotoTarget" runat="server" WatermarkText=" -- Vai all' STLC1000">
                                </geta:GoTo>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="background-image: url(Images/nav_shadow_bg.gif); height: 21px;
                        background-repeat: repeat-x;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table id="tblIssues" runat="server" cellpadding="5" cellspacing="5" style="width: 100%;">
        <tr id="trHeader">
            <td style="vertical-align: top; text-align: left;">
                <asp:UpdatePanel ID="updHeader" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <asp:DataList ID="dlstAttributeNamespaces" runat="server" RepeatDirection="Horizontal"
                            DataKeyField="ActivityGroupID" DataSourceID="linqActivityGroups" RepeatColumns="4"
                            Width="100%" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center"
                            OnItemDataBound="dlstAttributeNamespaces_ItemDataBound">
                            <ItemTemplate>
                                <div>
                                    <asp:Label ID="lblNamespace" runat="server" Text='<%# Eval("ActivityGroupName")%>'></asp:Label>
                                </div>
                                <div style="width: 95%; padding-right: 15px; overflow-x: hidden; overflow-y: auto;" runat="server" id="divAttributes">
                                    <asp:DataList ID="dlstAttribute" runat="server" RepeatDirection="Vertical" DataKeyField="TargetActivityID"
                                        OnItemDataBound="dlstAttribute_ItemDataBound" Width="100%" CssClass="activity_none">
                                        <ItemTemplate>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="sx">
                                                        &nbsp;
                                                    </td>
                                                    <td class="body">
                                                        <asp:LinkButton OnClick="lbAttribute_OnClick" CommandArgument='<%# Eval("TargetTypeID")%>'
                                                            ID="lbAttribute" runat="server" CommandName="Select" Text='<%# Eval("TargetActivityName")%>' />
                                                    </td>
                                                    <td class="dx">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:LinqDataSource ID="linqActivityGroups" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                            TableName="ActivityGroups" Where="TargetActivities.Where(IsObsolete == false).Count() > 0"
                            OrderBy="Order">
                        </asp:LinqDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr id="trAddIssue">
            <td style="vertical-align: top;">
                <asp:UpdatePanel ID="updAddIssue" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblAddIssue" runat="server" visible="false" cellpadding="0" cellspacing="0"
                            style="width: 100%; background-color: #808080;">
                            <tr>
                                <td>
                                    <img src="Images/lightpanel_top_sx.gif" alt="" />
                                </td>
                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                    text-align: left; color: White;">
                                    Intervento
                                    <asp:Label ID="lblFieldTitle" runat="Server" />
                                </td>
                                <td>
                                    <img src="Images/lightpanel_top_dx.gif" alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <table cellpadding="4" cellspacing="4" style="width: 80%; text-align: left;">
                                        <tr>
                                            <td style="width: 25%;">
                                                Data<br />
                                                <asp:TextBox ID="txtDate" runat="server" Width="80%"></asp:TextBox>
                                                <act:CalendarExtender ID="Date" runat="server" TargetControlID="txtDate" Format="dd-MM-yyyy">
                                                </act:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="reqvalDate" runat="server" ControlToValidate="txtDate"
                                                    Display="None" ErrorMessage="Campo richiesto" SetFocusOnError="true" ValidationGroup="Insert">
                                                </asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="covalDate" runat="server" ControlToValidate="txtDate" Display="None"
                                                    ErrorMessage="Data non valida" Type="Date" Operator="DataTypeCheck" SetFocusOnError="true"
                                                    ValidationGroup="Insert">
                                                </asp:CompareValidator>
                                                <act:ValidatorCalloutExtender runat="Server" ID="covalDateE" TargetControlID="covalDate"
                                                    HighlightCssClass="validatorCalloutHighlight" />
                                                <act:ValidatorCalloutExtender runat="Server" ID="reqvalDateE" TargetControlID="reqvalDate"
                                                    HighlightCssClass="validatorCalloutHighlight" />
                                            </td>
                                            <td style="width: 25%;">
                                                Operatore GRIS<br />
                                                <asp:DropDownList ID="ddlGrisOperator" runat="server" DataSourceID="linqGrisOperator"
                                                    DataValueField="UserID" DataTextField="Name" Width="80%">
                                                </asp:DropDownList>
                                                <asp:LinqDataSource ID="linqGrisOperator" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                    TableName="Users" OnSelecting="linqGrisOperator_Selecting">
                                                </asp:LinqDataSource>
                                            </td>
                                            <td style="width: 25%;">
                                                Installatore<br />
                                                <asp:DropDownList ID="ddlInstaller" runat="server" DataSourceID="linqInstaller" DataValueField="UserID"
                                                    DataTextField="Name" AppendDataBoundItems="true" Width="80%">
                                                    <asp:ListItem Value="">Nessun Installatore</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:LinqDataSource ID="linqInstaller" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                    TableName="Users" OnSelecting="linqInstaller_Selecting">
                                                </asp:LinqDataSource>
                                            </td>
                                            <td style="width: 25%;">
                                                Tipo Intervento<br />
                                                <asp:DropDownList ID="ddlIssueActivities" runat="server" DataSourceID="linqIssueActivities"
                                                    DataValueField="IssueActivityTypeID" DataTextField="IssueActivityTypeName" Width="80%">
                                                </asp:DropDownList>
                                                <asp:LinqDataSource ID="linqIssueActivities" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                    Select="new (IssueActivityTypeID, IssueActivityTypeName)" TableName="IssueActivityTypes"
                                                    OrderBy="IssueActivityTypeID">
                                                </asp:LinqDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                Note<br />
                                                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="8" Columns="120" />
                                            </td>
                                        </tr>
                                        <tr id="trAddActivityValueSeparator" runat="server">
                                            <td colspan="4" style="width: 100%;">
                                                <hr class="hrStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                <asp:Panel ID="pnlAddActivityValue" Style="float: left; margin-right: 20px" runat="server">
                                                    <asp:Label ID="lblFieldName" runat="Server" />
                                                    <geta:GetaCheckBox ID="chkFieldValue" runat="server" Visible="false" Readonly="false" />
                                                    <asp:DropDownList ID="ddlFieldValue" runat="server" Visible="false" />
                                                    <asp:TextBox ID="txtFieldValue" runat="server" Visible="false" />
                                                </asp:Panel>
                                                STLC1000
                                                <asp:DropDownList ID="ddlServers" DataValueField="ID" DataTextField="Name"
                                                    DataSourceID="linqServers" runat="server"  AppendDataBoundItems="true" 
                                                    onprerender="ddlServers_PreRender">
                                                    <asp:ListItem Value="">Nessun STLC1000</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:LinqDataSource ID="linqServers" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBSTLCDataContext"
                                                    OnSelecting="linqServers_OnSelecting" TableName="ServerInfos">
                                                </asp:LinqDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                <hr class="hrStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 5%;">
                                                            Stato&nbsp;
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:DropDownList ID="ddlState" runat="server" DataSourceID="linqState" DataValueField="IssueStateID"
                                                                DataTextField="IssueStateName">
                                                            </asp:DropDownList>
                                                            <asp:LinqDataSource ID="linqState" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                                Select="new (IssueStateID, IssueStateName)" TableName="IssueStates">
                                                            </asp:LinqDataSource>
                                                        </td>
                                                        <td style="width: 30%;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 60%; color: White;">
                                                            <asp:LinkButton ID="lnkbInsertIssue" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                ValidationGroup="Insert" ForeColor="White" CssClass="noDecorationLink" OnCommand="lnkbIssue_Command" />
                                                            <span>| </span>
                                                            <asp:LinkButton ID="lnkbCancelInsertIssue" runat="server" CommandName="Cancel" Text="Annulla"
                                                                ForeColor="White" CssClass="noDecorationLink" OnCommand="lnkbIssue_Command" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr id="trEditIssue">
            <td style="vertical-align: top;">
                <asp:UpdatePanel ID="updEditIssue" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblEditIssue" runat="server" visible="false" cellpadding="0" cellspacing="0"
                            style="width: 100%; background-color: #808080;">
                            <tr>
                                <td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                    text-align: left; color: White;">Modifica Intervento</td>
                                <td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <table cellpadding="4" cellspacing="4" style="width: 90%; text-align: left;">
                                        <tr>
                                            <td style="width: 25%; vertical-align: top;">
                                                Data<br />
                                                <asp:TextBox ID="txtEditDate" runat="server" Width="80%"></asp:TextBox>
                                                <act:CalendarExtender ID="calEditDate" runat="server" TargetControlID="txtEditDate"
                                                    Format="dd-MM-yyyy">
                                                </act:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="reqvalEditDate" runat="server" ControlToValidate="txtEditDate"
                                                    Display="None" ErrorMessage="Campo richiesto" SetFocusOnError="true" ValidationGroup="Update">
                                                </asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="covalEditDate" runat="server" ControlToValidate="txtEditDate"
                                                    Display="None" ErrorMessage="Data non valida" Type="Date" Operator="DataTypeCheck"
                                                    SetFocusOnError="true" ValidationGroup="Update">
                                                </asp:CompareValidator>
                                                <act:ValidatorCalloutExtender runat="Server" ID="covalEditDateE" TargetControlID="covalEditDate"
                                                    HighlightCssClass="validatorCalloutHighlight" />
                                                <act:ValidatorCalloutExtender runat="Server" ID="reqvalEditDateE" TargetControlID="reqvalEditDate"
                                                    HighlightCssClass="validatorCalloutHighlight" />
                                            </td>
                                            <td style="width: 25%; vertical-align: top;">
                                                Operatore GRIS<br />
                                                <div id="divGrisOperator" style="height: 100px; overflow-y: scroll; background-color: White;
                                                    color: Black;">
                                                    <asp:CheckBoxList ID="chklEditGrisOperator" runat="server" DataSourceID="linqEditGrisOperator"
                                                        DataValueField="UserID" DataTextField="Name" Width="80%" />
                                                </div>
                                                <asp:LinqDataSource ID="linqEditGrisOperator" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                    TableName="Users" OnSelecting="linqGrisOperator_Selecting">
                                                </asp:LinqDataSource>
                                            </td>
                                            <td style="width: 25%; vertical-align: top;">
                                                Installatore<br />
                                                <div id="divInstaller" style="height: 100px; overflow-y: scroll; background-color: White;
                                                    color: Black;">
                                                    <asp:CheckBoxList ID="chklEditInstaller" runat="server" DataSourceID="linqEditInstaller"
                                                        DataValueField="UserID" DataTextField="Name" Width="80%" />
                                                </div>
                                                <asp:LinqDataSource ID="linqEditInstaller" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBUsersDataContext"
                                                    TableName="Users" OnSelecting="linqInstaller_Selecting">
                                                </asp:LinqDataSource>
                                            </td>
                                            <td style="width: 25%; vertical-align: top;">
                                                Tipo Intervento<br />
                                                <asp:DropDownList ID="ddlEditIssueActivities" runat="server" DataSourceID="linqEditIssueActivities"
                                                    DataValueField="IssueActivityTypeID" DataTextField="IssueActivityTypeName" Width="80%">
                                                </asp:DropDownList>
                                                <asp:LinqDataSource ID="linqEditIssueActivities" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                    Select="new (IssueActivityTypeID, IssueActivityTypeName)" TableName="IssueActivityTypes"
                                                    OrderBy="IssueActivityTypeID">
                                                </asp:LinqDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                Note<br />
                                                <asp:TextBox ID="txtEditNotes" runat="server" TextMode="MultiLine" Rows="8" Columns="120" />
                                            </td>
                                        </tr>
                                        <tr id="trEditActivityValueSeparator" runat="server">
                                            <td colspan="4" style="width: 100%;">
                                                <hr class="hrStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:Panel ID="pnlEditActivityValue" Style="float: left; margin-right: 20px;" runat="server">
                                                    <asp:Label ID="lblEditFieldName" runat="Server" />
                                                    <geta:GetaCheckBox ID="chEditFieldValue" runat="server" Visible="false" Readonly="false" />
                                                    <asp:DropDownList ID="ddlEditFieldValue" runat="server" Visible="false" />
                                                    <asp:TextBox ID="txtEditFieldValue" runat="server" Visible="false" />
                                                </asp:Panel>
                                                STLC1000
                                                <asp:DropDownList ID="ddlEditServers" DataValueField="ID" DataTextField="Name"
                                                    DataSourceID="linqServers" runat="server"  AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Nessun STLC1000</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:LinqDataSource ID="linqEditServers" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBSTLCDataContext"
                                                    OnSelecting="linqServers_OnSelecting" TableName="ServerInfos">
                                                </asp:LinqDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                <hr class="hrStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width: 100%;">
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 5%;">
                                                            Stato&nbsp;
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:DropDownList ID="ddlEditState" runat="server" DataSourceID="linqEditState" DataValueField="IssueStateID"
                                                                DataTextField="IssueStateName">
                                                            </asp:DropDownList>
                                                            <asp:LinqDataSource ID="linqEditState" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                                Select="new (IssueStateID, IssueStateName)" TableName="IssueStates">
                                                            </asp:LinqDataSource>
                                                        </td>
                                                        <td style="width: 30%;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 60%; color: White;">
                                                            <asp:LinkButton ID="lnkbUpdateIssue" runat="server" CommandName="Update" Text="Modifica"
                                                                ValidationGroup="Update" ForeColor="White" CssClass="noDecorationLink" OnCommand="lnkbIssue_Command" />
                                                            <span>| </span>
                                                            <asp:LinkButton ID="lnkbCancelUpdateIssue" runat="server" CommandName="Cancel" Text="Annulla"
                                                                ForeColor="White" CssClass="noDecorationLink" OnCommand="lnkbIssue_Command" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr id="trIssues">
            <td style="vertical-align: top;">
                <asp:UpdatePanel ID="updIssues" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblIssueList" runat="server" cellpadding="0" cellspacing="0" style="width: 100%;
                            background-color: #808080;">
                            <tr>
                                <td><img src="Images/lightpanel_top_sx.gif" alt="" /></td>
                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                    text-align: left; color: White;">&nbsp;</td>
                                <td><img src="Images/lightpanel_top_dx.gif" alt="" /></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:ListView ID="lvwIssues" runat="server" DataSourceID="linqIssueList" InsertItemPosition="None"
                                        DataKeyNames="IssueID" OnSelectedIndexChanged="lvwIssues_SelectedIndexChanged"
                                        OnLayoutCreated="lvwIssues_LayoutCreated" OnItemDataBound="lvwIssues_ItemDataBound"
                                        OnItemDeleting="lvwIssues_ItemDeleting" OnSorting="lvwIssues_Sorting">
                                        <LayoutTemplate>
                                            <table id="Table1" runat="server" style="width: 100%;" cellpadding="1" cellspacing="0">
                                                <tr id="Tr2" runat="server" style="">
                                                    <th id="Th1" runat="server" style="width: 34px;">
                                                        <asp:ImageButton OnClick="imgAddIssue_OnClick" ID="imgAddIssue" ImageUrl="~/Images/lightadd.gif"
                                                            runat="server" />
                                                    </th>
                                                    <th id="Th3" style="width: 100px;" class="grisIssueHeader" runat="server">
                                                        <asp:LinkButton ID="lnkDate" runat="server" Text="DATA" CommandName="Sort" CommandArgument="DateIssue"
                                                            CssClass="issueHeader"></asp:LinkButton>
                                                        <asp:Image ID="imgDateIssue" Visible="false" runat="server"></asp:Image>
                                                    </th>
                                                    <th id="Th6" style="width: 150px;" class="grisIssueHeader" runat="server">
                                                        <asp:LinkButton ID="lnkOpGrisHeader" runat="server" Text="OPERATORE GRIS" CommandName="Sort"
                                                            CommandArgument="GrisOperators" CssClass="issueHeader"></asp:LinkButton>
                                                        <asp:Image ID="imgGrisOperators" Visible="false" runat="server"></asp:Image>
                                                    </th>
                                                    <th id="Th4" style="width: 150px;" class="grisIssueHeader" runat="server">
                                                        <asp:LinkButton ID="lnkActivityGroup" runat="server" Text="GRUPPO" CommandName="Sort"
                                                            CommandArgument="ActivityGroupName" CssClass="issueHeader"></asp:LinkButton>
                                                        <asp:Image ID="imgActivityGroupName" Visible="false" runat="server"></asp:Image>
                                                    </th>
                                                    <th id="Th7" style="width: 150px;" class="grisIssueHeader" runat="server">
                                                        <asp:LinkButton ID="lnkTargetActivity" runat="server" Text="ATTIVITA'" CommandName="Sort"
                                                            CommandArgument="TargetActivityName" CssClass="issueHeader"></asp:LinkButton>
                                                        <asp:Image ID="imgTargetActivityName" Visible="false" runat="server"></asp:Image>
                                                    </th>
                                                    <th id="Th5" style="width: 100px;" class="grisIssueHeader" runat="server">
                                                        <asp:LinkButton ID="lnkState" runat="server" Text="STATO" CommandName="Sort" CommandArgument="IssueStateName"
                                                            CssClass="issueHeader"></asp:LinkButton>
                                                        <asp:Image ID="imgIssueStateName" Visible="false" runat="server"></asp:Image>
                                                    </th>
                                                    <th id="Th2" style="width: 350px;" class="grisIssueHeader" runat="server">
                                                        <asp:LinkButton ID="lnkStateComment" runat="server" Text="NOTE" CommandName="Sort"
                                                            CommandArgument="IssueComment" CssClass="issueHeader"></asp:LinkButton>
                                                        <asp:Image ID="imgIssueComment" Visible="false" runat="server"></asp:Image>
                                                    </th>
                                                    <th id="Th8" style="width: 34px;" runat="server">
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                                <tr id="Tr1" runat="server">
                                                    <td id="Td1" colspan="8" runat="server">
                                                        <div style="height: 250px; overflow: auto;">
                                                            <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 95%;"
                                                                cellpadding="1">
                                                                <tr id="itemPlaceholder" runat="server">
                                                                </tr>
                                                            </table>                                                        
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <EmptyDataTemplate>
                                            <table id="Table1" style="width: 100%;">
                                                <tr id="Tr1">
                                                    <td id="Td1">
                                                        <table border="0" style="width: 100%;">
                                                            <tr>
                                                                <td valign="middle">
                                                                    Nessun Intervento per questa stazione.<br />
                                                                    Aggiungi intervento generico
                                                                    <asp:ImageButton OnClick="imgAddIssue_OnClick" ID="imgAddIssue" ImageUrl="~/Images/lightadd.gif"
                                                                        runat="server" Style="position: relative; top: 5px;" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 34px;">
                                                    <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl='<%# (int)Eval("IssueStateID") == 1 ? "~/Images/editopen.gif" : "~/Images/edit.gif" %>' AlternateText="Modifica"
                                                        ToolTip="Modifica" CommandName="Select" CausesValidation="false" CssClass="noDecorationLink" />
                                                </td>
                                                <td class="gridItem" style="width: 100px;">
                                                    <asp:Label ID="lblDateIssue" runat="server" Text='<%# Eval("DateIssue", "{0:dd-MM-yyyy}") %>' />
                                                </td>
                                                <td class="gridItem" style="width: 150px;">
                                                    &nbsp;<asp:Label ID="lblGrisOperator" runat="server" Text='<%# Eval("GrisOperators") %>' />
                                                </td>
                                                <td class="gridItem" style="width: 150px;">
                                                    &nbsp;<asp:Label ID="lblActivityGroupName" runat="server" Text='<%# Eval("ActivityGroupName") %>' />
                                                </td>
                                                <td class="gridItem" style="width: 150px;">
                                                    &nbsp;<asp:Label ID="lblTargetActivityName" runat="server" Text='<%# Eval("TargetActivityName") %>' />
                                                </td>
                                                <td class="gridItem" style="width: 100px;">
                                                    <asp:Label ID="lbl" runat="server" Text='<%# Eval("IssueStateName") %>' />
                                                </td>
                                                <td class="gridItem" style="width: 350px;font-size:8pt;">
                                                    <asp:Label ID="lblComment" runat="server" Text='<%# Eval("IssueComment") %>' />
                                                </td>
                                                <td style="width: 34px;">
                                                    <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                        ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare l\'intervento?');"
                                                        CssClass="noDecorationLink" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <ItemSeparatorTemplate>
                                            <tr>
                                                <td colspan="8" style="padding: 0; margin: 0;">
                                                    <hr class="hrStyle" />
                                                </td>
                                            </tr>
                                        </ItemSeparatorTemplate>
                                    </asp:ListView>
                                    <asp:LinqDataSource ID="linqIssueList" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                        OrderBy="DateIssue desc, DateCreated desc" TableName="Issues" EnableDelete="true"
                                        OnSelecting="linqIssueList_Selecting">
                                    </asp:LinqDataSource>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">&nbsp;</td>
                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

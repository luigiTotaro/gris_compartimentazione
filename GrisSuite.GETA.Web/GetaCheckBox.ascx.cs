﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class GetaCheckBox : System.Web.UI.UserControl
{
	# region Properties
	public bool Checked
	{
		get { return this.imgCheck.ImageUrl == "~/Images/flag_on.gif"; }
		set 
		{
			if ( value )
			{
				this.imgCheck.ImageUrl = "~/Images/flag_on.gif";
			}
			else
			{
				this.imgCheck.ImageUrl = "~/Images/flag_off.gif";
			}
		}
	}
	
	public string Text
	{
		get { return this.lblDescription.Text; }
		set { this.lblDescription.Text = string.Format("{0} ", value); }
	}
	
	public string CssClass
	{
		get { return this.lblDescription.CssClass; }
		set { this.lblDescription.CssClass = value; }
	}
	
	public bool Readonly
	{
		get { return Convert.ToBoolean(this.ViewState["Readonly"] ?? true); }
		set { this.ViewState["Readonly"] = value; }
	}
	# endregion
	
	protected void Page_Load ( object sender, EventArgs e )
	{
		if ( this.Readonly )
		{
			this.imgCheck.Enabled = false;
		}
	}
	
	protected void imgCheck_Click ( object sender, ImageClickEventArgs e )
	{
		this.Checked = !this.Checked;
	}
}

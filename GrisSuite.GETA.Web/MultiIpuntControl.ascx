﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiIpuntControl.ascx.cs"
    Inherits="MultiIpuntControl" %>
<asp:TextBox ID="txtNewItem" runat="server"></asp:TextBox>
<asp:RegularExpressionValidator ID="revNewItem" ControlToValidate="txtNewItem" runat="server" Display="Dynamic" />
<asp:Button ID="btnNewItem" OnClick="btnNewItem_OnClick" runat="server" />
<br />
<asp:GridView ID="gvItems" BorderWidth="0" OnRowDeleting="gvItems_OnRowDeleting"
    ShowHeader="false" AutoGenerateColumns="false" runat="server">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:TextBox ID="txtItem" MaxLength="<%# MaxLength %>"  Text="<%# Container.DataItem %>" runat="server" />
                <asp:RegularExpressionValidator ID="revItem" ValidationExpression="<%# ValidationExpression %>" 
                    ErrorMessage="<%# ValidationError %>" ValidationGroup="<%# ValidationGroup %>"
                    ControlToValidate="txtItem" runat="server" Display="Dynamic" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="CancelButton" runat="server" CommandName="Delete" Text="Elimina"
                    ForeColor="White" CssClass="noDecorationLink" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

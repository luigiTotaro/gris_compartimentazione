﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoTo.ascx.cs" Inherits="GoTo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0px;
    height: 30px">
    <tr>
        <td>
            <span id="spnNoRowsMessage" class="autocomplete_norows">Nessun Risultato</span>
        </td>
        <td id="tdSxGoTo" runat="server" style="background-image: url(Images/goto_sx.gif);
            background-repeat: no-repeat; width: 20px;">
            &nbsp;&nbsp;
        </td>
        <td id="tdCenterGoTo" runat="server" style="background-image: url(Images/goto_bg.gif);
            height: 30px; background-repeat: repeat-x;">
            <asp:TextBox ID="txtBoxGoToNode" runat="server" Width="250px" Height="20px" CssClass="BoxGoToNode"
                BackColor="Transparent"></asp:TextBox>
            <act:TextBoxWatermarkExtender ID="txtBoxGoToNodeWM" runat="server" TargetControlID="txtBoxGoToNode"
                WatermarkCssClass="watermarked" />
            <act:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteEx" ID="autoCompBoxGoToNode"
                TargetControlID="txtBoxGoToNode" ServicePath="NavigationBarGoToNode.asmx" ServiceMethod="GetCompletionList"
                MinimumPrefixLength="2" CompletionInterval="500" EnableCaching="true" CompletionSetCount="20"
                CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem">
            </act:AutoCompleteExtender>
        </td>
        <td id="tdDxGoTo" runat="server" style="width: 20px; height: 30px; margin: 0; padding: 0;">
            <asp:ImageButton ID="imgbBoxGoToNode" runat="server" ImageUrl="~/Images/goto_dx_open.gif"
                OnClick="imgbBoxGoToNode_Click" style="display: block;" />
        </td>
    </tr>
</table>

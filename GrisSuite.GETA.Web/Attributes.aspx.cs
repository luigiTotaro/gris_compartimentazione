﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using GrisSuite.GETA.Data;
using System.Data.Linq;
using System.Drawing;

public partial class Attributes : System.Web.UI.Page
{
	protected void Page_Load ( object sender, EventArgs e )
	{
		if ( !Utility.IsUserInAdminGroup() )
		{
			this.Response.Redirect("~/Default.aspx");
		}

		( (IGetaMasterPage)this.Master ).GetaLayoutMode = GetaLayoutModeEnum.Administration;
		this.SetImageButtonOverBehavior(this.lvwAttributes, "imgbAttributesAdd", "lightadd", true);
        this.SetImageButtonOverBehavior(this.lvwTargetTypes, "imgbTargetTypesAdd", "lightadd", true);
        this.SetImageButtonOverBehavior(this.lvwNamespaces, "imgbGroupAdd", "lightadd", true);        
	}
	
	# region Target Types
	protected void lvwTargetTypes_SelectedIndexChanged ( object sender, EventArgs e )
	{
		LinkButton lnkb = this.lvwTargetTypes.FindControl("lnkbTargetTitle") as LinkButton;
		if ( lnkb != null ) lnkb.ForeColor = System.Drawing.Color.Black;
        lnkbTargetTitle.ForeColor = Color.Black;
        updTargetTitle.Update();

		this.lvwAttributes.DataBind();
		this.updAttributes.Update();
	}

	protected void lnkbTargetTitle_Click ( object sender, EventArgs e )
	{
		( (LinkButton)sender ).ForeColor = System.Drawing.Color.White;
        
		this.lvwTargetTypes.SelectedIndex = -1;
        this.updTargetTypes.Update();

		this.lvwAttributes.DataBind();
		this.updAttributes.Update();
	}
	# endregion

    protected void ControlTypeUI_OnSelectedIndexChanged (object sender, EventArgs e )
    {
        DropDownList ddl = sender as DropDownList;
        TextBox txt = ddl.Parent.FindControl("EnumValuesTextBox") as TextBox;
        txt.Text = string.Empty;
        txt.Enabled = "ComboBox".Equals(ddl.SelectedValue);
    }


	# region Namespaces
	protected void lvwNamespaces_SelectedIndexChanged ( object sender, EventArgs e )
	{
		LinkButton lnkb = this.lvwNamespaces.FindControl("lnkbNamespacesTitle") as LinkButton;
		if ( lnkb != null ) lnkb.ForeColor = System.Drawing.Color.Black;
        lnkbNamespacesTitle.ForeColor = Color.Black;
        updNamespacesTitle.Update();

		this.lvwAttributes.DataBind();
		this.updAttributes.Update();
	}

    protected void lvwNamespaces_ItemInserting(object sender, ListViewInsertEventArgs e)
    {
        DBIssuesDataContext db = new DBIssuesDataContext();
        byte order = db.ActivityGroups.Max(ag => ag.Order);
        e.Values.Add("Order", ++order);       
    }


	protected void lnkbNamespacesTitle_Click ( object sender, EventArgs e )
	{
		( (LinkButton)sender ).ForeColor = System.Drawing.Color.White;
		this.lvwNamespaces.SelectedIndex = -1;
        this.updNamespaces.Update();

		this.lvwAttributes.DataBind();
		this.updAttributes.Update();
	}
	# endregion
	
	# region Attributes
	# region linqAttributes

	protected void linqAttributes_Inserting ( object sender, LinqDataSourceInsertEventArgs e )
	{
		TargetActivity attribute = e.NewObject as TargetActivity;
		DropDownList ddlControlTypeUI = this.lvwAttributes.InsertItem.FindControl("ControlTypeUIDropdownlist") as DropDownList;
		DropDownList ddlTargetType = this.lvwAttributes.InsertItem.FindControl("TargetTypeIDDropDownList") as DropDownList;
		GetaCheckBox chkObsolete = this.lvwAttributes.InsertItem.FindControl("IsObsoleteCheckBox") as GetaCheckBox;

		if ( attribute != null && ddlControlTypeUI != null && ddlTargetType != null && chkObsolete != null )
		{
			attribute.ControlTypeUI = ddlControlTypeUI.SelectedValue;
			attribute.TargetTypeID = Convert.ToInt32(ddlTargetType.SelectedValue);
			attribute.IsObsolete = chkObsolete.Checked;
		}
	}

    protected void linqAttributes_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        int ttID;
        int ns;
        DBIssuesDataContext db = new DBIssuesDataContext();

        if (string.IsNullOrEmpty((this.lvwTargetTypes.SelectedValue ?? "").ToString()) || !int.TryParse((this.lvwTargetTypes.SelectedValue ?? "").ToString(), out ttID)) ttID = -1;
        if (string.IsNullOrEmpty((this.lvwNamespaces.SelectedValue ?? "").ToString()) || !int.TryParse((this.lvwNamespaces.SelectedValue.ToString() ?? "").ToString(), out ns)) ns = -1; 

        IQueryable<TargetActivity> activities = db.TargetActivities;
        if ( ttID != -1 )
            activities = activities.Where(a => a.TargetTypeID == ttID);
        if ( ns != -1 )
            activities = activities.Where(a => a.ActivityGroupsID == ns);

        e.Result = activities;  
    }

	
	protected void linqAttributes_Updating ( object sender, LinqDataSourceUpdateEventArgs e )
	{
		TargetActivity attribute = e.NewObject as TargetActivity;
		DropDownList ddlControlTypeUI = this.lvwAttributes.Items[this.lvwAttributes.EditIndex].FindControl("ControlTypeUIDropdownlist") as DropDownList;
		DropDownList ddlTargetType = this.lvwAttributes.Items[this.lvwAttributes.EditIndex].FindControl("TargetTypeIDDropDownList") as DropDownList;
		GetaCheckBox chkObsolete = this.lvwAttributes.Items[this.lvwAttributes.EditIndex].FindControl("IsObsoleteCheckBox") as GetaCheckBox;

		if ( attribute != null && ddlControlTypeUI != null && ddlTargetType != null )
		{
			attribute.ControlTypeUI = ddlControlTypeUI.SelectedValue;
			attribute.TargetTypeID = Convert.ToInt32(ddlTargetType.SelectedValue);
			attribute.IsObsolete = chkObsolete.Checked;
		}
	}
	# endregion

	# region lvwAttributes

	protected void lvwAttributes_ItemDataBound ( object sender, ListViewItemEventArgs e )
	{
		if ( e.Item.ItemType == ListViewItemType.DataItem )
		{
            TargetActivity ta = ((ListViewDataItem)e.Item).DataItem as TargetActivity;
			this.SetImageButtonOverBehavior(e.Item, "imgbEdit", "edit", false);

			Label lblEnumValues = e.Item.FindControl("EnumValuesLabel") as Label;
            ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
            if (imgbDelete != null)
            {
                if (ta != null && ta.Issues.Count > 0)
                {
                    imgbDelete.Visible = false;
                }
                else
                {
                    imgbDelete.Visible = true;
                    this.SetImageButtonOverBehavior(imgbDelete, "delete", false);
                }
            }

			if ( lblEnumValues != null && lblEnumValues.Text.Length > 70 ) lblEnumValues.Text = string.Format("{0}...", lblEnumValues.Text.Substring(0, 70));
		}        
	}

	protected void lvwAttributes_ItemInserted ( object sender, ListViewInsertedEventArgs e )
	{
		this.lvwAttributes.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwAttributes, "imgbAttributesAdd", true);
		this.lvwNamespaces.DataBind();
		this.updNamespaces.Update();
	}

    protected void lvwAttributes_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
        this.lvwNamespaces.DataBind();
        this.updNamespaces.Update();
    }

    protected void lvwAttributes_OnPreRender(object sender, EventArgs e)
    { 
        if (lvwAttributes.InsertItem != null)
        {
            if (lvwTargetTypes.SelectedValue != null)
            {
                DropDownList ddl = lvwAttributes.InsertItem.FindControl("TargetTypeIDDropDownList") as DropDownList;
                ddl.SelectedValue = lvwTargetTypes.SelectedValue.ToString();
            }
            if (lvwNamespaces.SelectedValue != null)
            {
                DropDownList ddl = lvwAttributes.InsertItem.FindControl("ActivityGroupIDDropDownList") as DropDownList;
                ddl.SelectedValue = lvwNamespaces.SelectedValue.ToString();
            }            
        }
    }

	protected void lvwAttributes_ItemEditing ( object sender, ListViewEditEventArgs e )
	{
		this.lvwAttributes.InsertItemPosition = InsertItemPosition.None;
		this.SetAddButtonVisible(this.lvwAttributes, "imgbAttributesAdd", true);
	}

	protected void lvwAttributes_ItemCanceling ( object sender, ListViewCancelEventArgs e )
	{
		if ( e.CancelMode == ListViewCancelMode.CancelingInsert )
		{
			this.lvwAttributes.InsertItemPosition = InsertItemPosition.None;
			this.SetAddButtonVisible(this.lvwAttributes, "imgbAttributesAdd", true);
		}
	}

	protected void lvwAttributes_ItemInserting ( object sender, ListViewInsertEventArgs e )
	{
        if (this.IsValid)
        {
            DropDownList ddlControlTypeUI = e.Item.FindControl("ControlTypeUIDropdownlist") as DropDownList;
            DropDownList ddlActivityGroupID = e.Item.FindControl("ActivityGroupIDDropDownList") as DropDownList;
            DropDownList ddlTargetTypeID = e.Item.FindControl("TargetTypeIDDropDownList") as DropDownList;

            e.Values.Add("TargetActivityID", ddlTargetTypeID.SelectedValue);
            e.Values.Add("ControlTypeUI", ddlControlTypeUI.SelectedValue);
            e.Values.Add("ActivityGroupsID", ddlActivityGroupID.SelectedValue);
            
            DBIssuesDataContext db = new DBIssuesDataContext();
            byte order = db.TargetActivities.Max(ag => ag.Order);
            e.Values.Add("Order", ++order);
        }
	}
	
	protected void lvwAttributes_ItemUpdated ( object sender, ListViewUpdatedEventArgs e )
	{
		this.lvwNamespaces.DataBind();
		this.updNamespaces.Update();
	}
	# endregion

	protected void imgbAttributesAdd_Click ( object sender, EventArgs e )
	{
		this.lvwAttributes.EditIndex = -1;
		this.lvwAttributes.InsertItemPosition = InsertItemPosition.LastItem;
		( (ImageButton)sender ).Visible = false;

	}
	# endregion
	
	# region Utility Methods
	private void SetAddButtonVisible ( ListView lstView, string buttonName, bool visible )
	{
		ImageButton imgbAdd = lstView.FindControl(buttonName) as ImageButton;
		if ( imgbAdd != null )
		{
			imgbAdd.Visible = visible;
		}
	}

	private void SetImageButtonOverBehavior ( Control parent, string buttonName, string defaultImageName, bool pressedStatus )
	{
		ImageButton imgbutton = parent.FindControl(buttonName) as ImageButton;
		this.SetImageButtonOverBehavior(imgbutton, defaultImageName, pressedStatus);
	}

	private void SetImageButtonOverBehavior ( ImageButton imgbutton, string defaultImageName, bool pressedStatus )
	{
		if ( imgbutton != null )
		{
			imgbutton.Attributes.Add("OnMouseOver", string.Format("this.src = 'Images/{0}_over.gif'", defaultImageName));
			imgbutton.Attributes.Add("OnMouseOut", string.Format("this.src = 'Images/{0}.gif'", defaultImageName));
			if ( pressedStatus ) imgbutton.Attributes.Add("OnMouseDown", string.Format("this.src = 'Images/{0}_pressed.gif'", defaultImageName));
		}
	}
	# endregion


    protected void imgbTargetTypesAdd_Click(object sender, EventArgs e)
    {
        this.lvwTargetTypes.EditIndex = -1;
        this.lvwTargetTypes.InsertItemPosition = InsertItemPosition.LastItem;
        ((ImageButton)sender).Visible = false;
    }

    protected void lvwTargetTypes_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        this.lvwTargetTypes.InsertItemPosition = InsertItemPosition.None;
        this.SetAddButtonVisible(this.lvwTargetTypes, "imgbTargetTypesAdd", true);
    }

    protected void lvwTargetTypes_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        if (e.CancelMode == ListViewCancelMode.CancelingInsert)
        {
            this.lvwTargetTypes.InsertItemPosition = InsertItemPosition.None;
            this.SetAddButtonVisible(this.lvwTargetTypes, "imgbTargetTypesAdd", true);
        }
    }

    protected void lvwTargetTypes_ItemCreated(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.InsertItem)
        {
            ImageButton imgbAdd = this.lvwTargetTypes.FindControl("imgbTargetTypesAdd") as ImageButton;
            if (imgbAdd != null)
            {
                imgbAdd.Visible = false;
            }
        }
        else if (e.Item.ItemType == ListViewItemType.EmptyItem)
        {
            this.SetImageButtonOverBehavior(e.Item, "imgbTargetTypesAdd", "lightadd", true);
        }
    }

    protected void lvwTargetTypes_ItemInserted(object sender, ListViewInsertedEventArgs e)
    {
        this.lvwTargetTypes.InsertItemPosition = InsertItemPosition.None;
        this.SetAddButtonVisible(this.lvwTargetTypes, "imgbTargetTypesAdd", true);
    }


    protected void imgbGroupAdd_Click(object sender, EventArgs e)
    {
        this.lvwNamespaces.EditIndex = -1;
        this.lvwNamespaces.InsertItemPosition = InsertItemPosition.LastItem;
        ((ImageButton)sender).Visible = false;
    }

    protected void lvwNamespaces_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        this.lvwNamespaces.InsertItemPosition = InsertItemPosition.None;
        this.SetAddButtonVisible(this.lvwNamespaces, "imgbGroupAdd", true);
    }

    protected void lvwNamespaces_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        if (e.CancelMode == ListViewCancelMode.CancelingInsert)
        {
            this.lvwNamespaces.InsertItemPosition = InsertItemPosition.None;
            this.SetAddButtonVisible(this.lvwNamespaces, "imgbGroupAdd", true);
        }
    }

    protected void lvwNamespaces_ItemCreated(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.InsertItem)
        {
            ImageButton imgbAdd = this.lvwNamespaces.FindControl("imgbGroupAdd") as ImageButton;
            if (imgbAdd != null)
            {
                imgbAdd.Visible = false;
            }
        }
        else if (e.Item.ItemType == ListViewItemType.EmptyItem)
        {
            this.SetImageButtonOverBehavior(e.Item, "imgbGroupAdd", "lightadd", true);
        }
    }

    protected void lvwNamespaces_ItemInserted(object sender, ListViewInsertedEventArgs e)
    {
        this.lvwNamespaces.InsertItemPosition = InsertItemPosition.None;
        this.SetAddButtonVisible(this.lvwNamespaces, "imgbGroupAdd", true);
    }

    protected void lvwTargetTypes_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            TargetType tt = ((ListViewDataItem)e.Item).DataItem as TargetType;

            //this.SetImageButtonOverBehavior(e.Item, "imgbEdit", "edit", false);

            //ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
            //if (imgbDelete != null)
            //{
            //    if (tt != null && tt.TargetActivities.Count > 0)
            //    {
            //        imgbDelete.Visible = false;
            //    }
            //    else
            //    {
            //        imgbDelete.Visible = true;
            //        this.SetImageButtonOverBehavior(imgbDelete, "delete", false);
            //    }
            //}
        }
    }

    protected void lvwNamespaces_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ActivityGroup ag = ((ListViewDataItem)e.Item).DataItem as ActivityGroup;

            this.SetImageButtonOverBehavior(e.Item, "imgbEdit", "edit", false);

            ImageButton imgbDelete = e.Item.FindControl("imgbDelete") as ImageButton;
            if (imgbDelete != null)
            {
                if (ag != null && ag.TargetActivities.Count > 0)
                {
                    imgbDelete.Visible = false;
                }
                else
                {
                    imgbDelete.Visible = true;
                    this.SetImageButtonOverBehavior(imgbDelete, "delete", false);
                }
            }
        }
    }

}

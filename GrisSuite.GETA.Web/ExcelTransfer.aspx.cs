using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Edulife.Presentation.Web.Components;
using GrisSuite.GETA.Data;

namespace Edulife.Presentation.Web.Pages {

    public partial class ExcelTransfer : System.Web.UI.Page {
        
        protected ExcelTransferUtility ExcelUtil = null;
        protected string Result = "";

        protected void Page_Load(object sender, System.EventArgs e) {

            this.ExcelUtil = new ExcelTransferUtility(Server);
           // if (this.ExcelUtil.FileName.Length > 0)
            this.Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            this.Response.Write(this.ExcelUtil.Result);
            this.Response.End();
            
       }

       

    }
}

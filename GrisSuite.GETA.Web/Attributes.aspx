﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Attributes.aspx.cs" Inherits="Attributes" Title="GETA Attributi" %>

<%@ Register Src="~/GetaCheckBox.ascx" TagName="GetaCheckBox" TagPrefix="geta" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainArea" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 1200px; text-align: center">
        <tr>
            <td align="left">
                <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%">
                    <tr>
                        <td id="tdUsersSelector" runat="server" class="ToolbarButtonToggleOff">
                            <asp:Button ID="btnUsersSelector" runat="server" Text="UTENTI" CssClass="ToolbarButton"
                                PostBackUrl="~/UsersOrganizations.aspx" />
                        </td>
                        <td id="tdAttributesSelector" runat="server" class="ToolbarButtonToggleOn">
                            <asp:Button ID="btnAttributesSelector" runat="server" Text="ATTIVITA'" CssClass="ToolbarButton"
                                PostBackUrl="~/Attributes.aspx" />
                        </td>
                        <td class="ToolbarTitle">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="SubToolbar" colspan="3">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td id="tdCategories" style="width: 20%; vertical-align: top; padding: 5px;" runat="server">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td>
                                        <!-- Pannello TARGET -->
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                            <tr>
                                                <td>
                                                    <img src="Images/lightpanel_top_sx.gif" alt="" />
                                                </td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                                    text-align: center;">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <img src="Images/lightpanel_top_dx.gif" alt="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updTargetTitle" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lnkbTargetTitle" runat="server" ForeColor="White" Font-Bold="true"
                                                                CssClass="noDecorationLink" OnClick="lnkbTargetTitle_Click">TARGET</asp:LinkButton>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updTargetTypes" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:ListView ID="lvwTargetTypes" runat="server" DataSourceID="linqTargetTypes" InsertItemPosition="None"
                                                                OnItemEditing="lvwTargetTypes_ItemEditing" OnItemCanceling="lvwTargetTypes_ItemCanceling"
                                                                OnItemCreated="lvwTargetTypes_ItemCreated" OnItemInserted="lvwTargetTypes_ItemInserted"
                                                                OnItemDataBound="lvwTargetTypes_ItemDataBound" DataKeyNames="TargetTypeID" OnSelectedIndexChanged="lvwTargetTypes_SelectedIndexChanged">
                                                                <LayoutTemplate>
                                                                    <table id="Table1" runat="server" style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr id="Tr1" runat="server">
                                                                            <td id="Td1" runat="server">
                                                                                <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;">
                                                                                    <tr id="itemPlaceholder" runat="server">
                                                                                    </tr>
                                                                                    <tr id="OrgFooter">
                                                                                        <td colspan="3" style="text-align: left;">
                                                                                            <asp:ImageButton ID="imgbTargetTypesAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                                                OnClick="imgbTargetTypesAdd_Click" Visible="false" AlternateText="Inserimento TargetType"
                                                                                                ToolTip="Aggiungi" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LayoutTemplate>
                                                                <EmptyDataTemplate>
                                                                    <table id="Table1" style="width: 100%;">
                                                                        <tr id="Tr1">
                                                                            <td id="Td1">
                                                                                <table border="0" style="width: 100%;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink"
                                                                                Visible="false" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:LinkButton ID="ItemButton" runat="server" CommandName="Select"
                                                                                Text='<%# Eval("TargetTypeName") %>' CssClass="selectableField" />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare il Target?');"
                                                                                CssClass="noDecorationLink" Visible="false" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TargetTypeNameTextBox" runat="server" Text='<%# Bind("TargetTypeName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqTargetTypeName" runat="server" ControlToValidate="TargetTypeNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="EditOrgGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqTargetTypeNameE" TargetControlID="reqTargetTypeName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Aggiorna"
                                                                                            ValidationGroup="EditOrgGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </EditItemTemplate>
                                                                <InsertItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TargetTypeNameTextBox" runat="server" Text='<%# Bind("TargetTypeName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqTargetTypeName" runat="server" ControlToValidate="TargetTypeNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertOrgGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqTargetTypeNameE" TargetControlID="reqTargetTypeName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                                            ValidationGroup="InsertOrgGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </InsertItemTemplate>
                                                                <SelectedItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" Visible="false" CommandName="Edit" CausesValidation="false"
                                                                                CssClass="noDecorationLink" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" class="gridItem" nowrap>
                                                                            <asp:Label ID="OrganizationNameLabel" runat="server" Font-Bold="true" ForeColor="White"
                                                                                Text='<%# Eval("TargetTypeName") %>' />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" Visible="false" CommandName="Delete" CausesValidation="false"
                                                                                OnClientClick="return confirm('Confermi di voler cancellare il Target?');" CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </SelectedItemTemplate>
                                                                <ItemSeparatorTemplate>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <hr class="hrStyle" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemSeparatorTemplate>
                                                            </asp:ListView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">
                                                    &nbsp;
                                                </td>
                                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- Pannello GRUPPI ATTIVITA' -->
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                            <tr>
                                                <td>
                                                    <img src="Images/lightpanel_top_sx.gif" alt="" />
                                                </td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                                    text-align: center;">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <img src="Images/lightpanel_top_dx.gif" alt="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updNamespacesTitle" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lnkbNamespacesTitle" runat="server" ForeColor="White" Font-Bold="true"
                                                                CssClass="noDecorationLink" OnClick="lnkbNamespacesTitle_Click">GRUPPI</asp:LinkButton>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:UpdatePanel ID="updNamespaces" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:ListView ID="lvwNamespaces" runat="server" DataSourceID="linqNamespaces" InsertItemPosition="None"
                                                                OnItemEditing="lvwNamespaces_ItemEditing" OnItemCanceling="lvwNamespaces_ItemCanceling"
                                                                OnItemCreated="lvwNamespaces_ItemCreated" OnItemInserted="lvwNamespaces_ItemInserted"
                                                                OnItemDataBound="lvwNamespaces_ItemDataBound" DataKeyNames="ActivityGroupID"
                                                                OnItemInserting="lvwNamespaces_ItemInserting"
                                                                OnSelectedIndexChanged="lvwNamespaces_SelectedIndexChanged">
                                                                <LayoutTemplate>
                                                                    <table id="Table1" runat="server" style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr id="Tr1" runat="server">
                                                                            <td id="Td1" runat="server">
                                                                                <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%;">
                                                                                    <tr id="itemPlaceholder" runat="server">
                                                                                    </tr>
                                                                                    <tr id="OrgFooter">
                                                                                        <td colspan="3" style="text-align: left;">
                                                                                            <asp:ImageButton ID="imgbGroupAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                                                OnClick="imgbGroupAdd_Click" AlternateText="Inserimento Gruppo" ToolTip="Aggiungi" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </LayoutTemplate>
                                                                <EmptyDataTemplate>
                                                                    <table id="Table2" runat="server" style="">
                                                                        <tr>
                                                                            <td>
                                                                                Nessun gruppo presente.
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:LinkButton ID="UserGroupNameButton" runat="server" CommandName="Select"
                                                                                Text='<%# Eval("ActivityGroupName") %>' CssClass="selectableField" />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare il Gruppo?');"
                                                                                CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <InsertItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="ActivityGroupNameTextBox" runat="server" Text='<%# Bind("ActivityGroupName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqActivityGroupName" runat="server" ControlToValidate="ActivityGroupNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="InsertOrgGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqActivityGroupNameE" TargetControlID="reqActivityGroupName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                                            ValidationGroup="InsertOrgGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </InsertItemTemplate>
                                                                <EditItemTemplate>
                                                                    <tr style="color: Black;">
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="ActivityGroupNameTextBox" runat="server" Text='<%# Bind("ActivityGroupName") %>'
                                                                                            MaxLength="256" Width="100%" />
                                                                                        <asp:RequiredFieldValidator ID="reqActivityGroupName" runat="server" ControlToValidate="ActivityGroupNameTextBox"
                                                                                            ErrorMessage="Campo Richiesto" Display="None" ValidationGroup="EditOrgGroup"></asp:RequiredFieldValidator>
                                                                                        <act:ValidatorCalloutExtender runat="Server" ID="reqActivityGroupNameE" TargetControlID="reqActivityGroupName"
                                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="color: White; text-align: center;">
                                                                                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Aggiorna"
                                                                                            ValidationGroup="EditOrgGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                                        <span>| </span>
                                                                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                            ForeColor="White" CssClass="noDecorationLink" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </EditItemTemplate>
                                                                <SelectedItemTemplate>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                                ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink" />
                                                                        </td>
                                                                        <td style="width: 98%; text-align: left;" class="gridItem" nowrap>
                                                                            <asp:Label ID="UserGroupNameLabel" runat="server" Font-Bold="true" ForeColor="White"
                                                                                Text='<%# Eval("ActivityGroupName") %>' />
                                                                        </td>
                                                                        <td style="width: 1%;">
                                                                            <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                                ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare il Gruppo?');"
                                                                                CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </SelectedItemTemplate>
                                                                <ItemSeparatorTemplate>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <hr class="hrStyle" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemSeparatorTemplate>
                                                            </asp:ListView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                                <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">
                                                    &nbsp;
                                                </td>
                                                <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td id="tdAttributes" runat="server" style="padding: 5px; vertical-align: top;">
                            <!-- Pannello gestione ATTIVITA' -->
                            <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #808080;">
                                <tr>
                                    <td>
                                        <img src="Images/lightpanel_top_sx.gif" alt="" />
                                    </td>
                                    <td style="width: 98%; background-image: url(Images/lightpanel_top_bg.gif); background-repeat: repeat-x;
                                        text-align: center; color: White; font-weight: bold;">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <img src="Images/lightpanel_top_dx.gif" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:UpdatePanel ID="updAttributes" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:ListView ID="lvwAttributes" runat="server" DataSourceID="linqAttributes" DataKeyNames="TargetActivityID"
                                                    InsertItemPosition="None" OnItemInserting="lvwAttributes_ItemInserting" OnItemCanceling="lvwAttributes_ItemCanceling"
                                                    OnItemDataBound="lvwAttributes_ItemDataBound" OnItemInserted="lvwAttributes_ItemInserted"
                                                    OnItemDeleted="lvwAttributes_ItemDeleted" OnPreRender="lvwAttributes_OnPreRender"
                                                    OnItemUpdated="lvwAttributes_ItemUpdated" OnItemEditing="lvwAttributes_ItemEditing">
                                                    <LayoutTemplate>
                                                        <table id="Table1" runat="server" style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr id="Tr1" runat="server">
                                                                <td id="Td1" runat="server">
                                                                    <table id="itemPlaceholderContainer" runat="server" border="0" style="width: 100%; text-align: left;" cellpadding="1">
                                                                        <tr id="Tr2" runat="server" style="">
                                                                            <th id="Th1" runat="server">
                                                                            </th>
                                                                            <th id="Th2" runat="server" style="width: 30%;" class="gridHeader">
                                                                                NOME
                                                                            </th>
                                                                            <th id="Th3" runat="server" style="width: 20%;" class="gridHeader">
                                                                                TIPO CONTROLLO
                                                                            </th>
                                                                            <th id="Th4" runat="server" style="width: 10%;" class="gridHeader">
                                                                                OBSOLETO
                                                                            </th>
                                                                            <th id="Th5" runat="server" style="width: 40%;" class="gridHeader">
                                                                                VALORI
                                                                            </th>
                                                                            <th>
                                                                                &nbsp;
                                                                            </th>
                                                                        </tr>
                                                                        <tr id="itemPlaceholder" runat="server">
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="AttributesFooter">
                                                                <td colspan="5" style="text-align: left;">
                                                                    <div style="text-align: left;">
                                                                        <asp:ImageButton ID="imgbAttributesAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                            OnClick="imgbAttributesAdd_Click" AlternateText="Inserimento Attributo" ToolTip="Aggiungi" /></div>
                                                                    <div style="text-align: right;">
                                                                        <asp:DataPager runat="server" ID="AttributesPager" PageSize="10">
                                                                            <Fields>
                                                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="true" ShowNextPageButton="false"
                                                                                    ShowPreviousPageButton="false" ShowLastPageButton="false"  ButtonCssClass="menu_link2" FirstPageText="Primo" />
                                                                                <asp:NumericPagerField ButtonCount="10"  NumericButtonCssClass="menu_link2" NextPreviousButtonCssClass="menu_link2" />
                                                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowNextPageButton="false"
                                                                                     ButtonCssClass="menu_link2"  ShowPreviousPageButton="false" ShowLastPageButton="true" LastPageText="Ultimo" />
                                                                                <asp:NextPreviousPagerField ButtonType="Image" PreviousPageImageUrl="~/Images/prev_page.gif"
                                                                                    NextPageImageUrl="~/Images/next_page.gif" ShowFirstPageButton="false" ShowNextPageButton="true"
                                                                                    ShowPreviousPageButton="true" ShowLastPageButton="false" ButtonCssClass="pager_button"/>
                                                                            </Fields>
                                                                        </asp:DataPager>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </LayoutTemplate>
                                                    <InsertItemTemplate>
                                                        <tr style="color: Black; text-align: left;">
                                                            <td colspan="5">
                                                                <table cellpadding="4" cellspacing="4" style="width: 100%; text-align: left;">
                                                                    <tr>
                                                                        <td style="width: 25%;">
                                                                            <asp:TextBox ID="TargetActivityNameTextBox" runat="server" Text='<%# Bind("TargetActivityName") %>'
                                                                                MaxLength="256" Width="100%" />
                                                                            <asp:RequiredFieldValidator ID="reqTargetActivityName" runat="server" ControlToValidate="TargetActivityNameTextBox"
                                                                                ValidationGroup="InsertAttributeGroup" ErrorMessage="Campo Richiesto" Display="None">
                                                                            </asp:RequiredFieldValidator>
                                                                            <act:ValidatorCalloutExtender runat="Server" ID="reqTargetActivityNameE" TargetControlID="reqTargetActivityName"
                                                                                HighlightCssClass="validatorCalloutHighlight" />
                                                                            <act:TextBoxWatermarkExtender ID="wkUsername" runat="server" TargetControlID="TargetActivityNameTextBox"
                                                                                WatermarkText="Nome Attributo" WatermarkCssClass="watermarked" />
                                                                        </td>
                                                                        <td style="width: 25%;">
                                                                            <asp:DropDownList ID="ControlTypeUIDropdownlist" AutoPostBack="true"
                                                                                OnSelectedIndexChanged="ControlTypeUI_OnSelectedIndexChanged" runat="server" Width="100%">
                                                                                <asp:ListItem Value="CheckBox" Text="CheckBox" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Value="ComboBox" Text="ComboBox"></asp:ListItem>
                                                                                <asp:ListItem Value="TextBox" Text="TextBox"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 50%;">
                                                                            <asp:TextBox ID="EnumValuesTextBox" runat="server" Text='<%# Bind("EnumValues") %>'
                                                                                Width="100%" Enabled="false" />
                                                                            <asp:RegularExpressionValidator ID="rexEnumValues" runat="server" ControlToValidate="EnumValuesTextBox"
                                                                                ValidationExpression=".+\|.+" ValidationGroup="InsertAttributeGroup" ErrorMessage="I valori introdotti devono essere divisi dal carattere pipe '|'"
                                                                                Display="None">
                                                                            </asp:RegularExpressionValidator>
                                                                            <act:ValidatorCalloutExtender runat="Server" ID="rexEnumValuesE" TargetControlID="rexEnumValues"
                                                                                HighlightCssClass="validatorCalloutHighlight" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="TargetTypeIDDropDownList" runat="server" DataTextField="TargetTypeName"
                                                                                DataValueField="TargetTypeID" DataSourceID="linqTargetTypeID" Width="100%">
                                                                            </asp:DropDownList>
                                                                            <asp:LinqDataSource ID="linqTargetTypeID" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                                                TableName="TargetTypes" OrderBy="TargetTypeName DESC" Select="new (TargetTypeID, TargetTypeName)">
                                                                            </asp:LinqDataSource>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ActivityGroupIDDropDownList" runat="server" DataTextField="ActivityGroupName"
                                                                                DataValueField="ActivityGroupID" DataSourceID="linqActivityGroup" Width="100%">
                                                                            </asp:DropDownList>
                                                                            <asp:LinqDataSource ID="linqActivityGroup" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                                                TableName="ActivityGroups" OrderBy="ActivityGroupName" Select="new (ActivityGroupID, ActivityGroupName)">
                                                                            </asp:LinqDataSource>
                                                                        </td>
                                                                        <td>
                                                                            <geta:GetaCheckBox ID="IsObsoleteCheckBox" runat="server" Checked='<%# Bind("IsObsolete") %>'
                                                                                Text="Obsoleto" Readonly="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:Label ID="lblInsertMessage" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table cellpadding="4" cellspacing="4" style="text-align: left;">
                                                                    <tr>
                                                                        <td style="color: White; text-align: left;">
                                                                            <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert" Text="Aggiungi"
                                                                                ValidationGroup="InsertAttributeGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                            <span>| </span>
                                                                            <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                ForeColor="White" CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </InsertItemTemplate>
                                                    <EmptyDataTemplate>
                                                        <table id="Table2" runat="server" style="">
                                                            <tr>
                                                                <td>
                                                                    Nessun dato restituito.
                                                                </td>
                                                            </tr>
                                                            <tr id="AttributesFooter">
                                                                <td style="text-align: left;">
                                                                    <asp:ImageButton ID="imgbAttributesAdd" runat="server" ImageUrl="~/Images/lightadd.gif"
                                                                        OnClick="imgbAttributesAdd_Click" AlternateText="Inserimento Attributo" ToolTip="Aggiungi" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </EmptyDataTemplate>
                                                    <EditItemTemplate>
                                                        <tr style="color: Black; text-align: left;">
                                                            <td colspan="5">
                                                                <table cellpadding="4" cellspacing="4" style="width: 100%; text-align: left;">
                                                                    <tr>
                                                                        <td style="width: 25%;">
                                                                            <asp:TextBox ID="TargetActivityNameTextBox" runat="server" Text='<%# Bind("TargetActivityName") %>'
                                                                                MaxLength="256" Width="100%" />
                                                                            <asp:RequiredFieldValidator ID="reqTargetActivityName" runat="server" ControlToValidate="TargetActivityNameTextBox"
                                                                                ValidationGroup="UpdateAttributeGroup" ErrorMessage="Campo Richiesto" Display="None">
                                                                            </asp:RequiredFieldValidator>
                                                                            <act:ValidatorCalloutExtender runat="Server" ID="reqTargetActivityNameE" TargetControlID="reqTargetActivityName"
                                                                                HighlightCssClass="validatorCalloutHighlight" />
                                                                            <act:TextBoxWatermarkExtender ID="wkUsername" runat="server" TargetControlID="TargetActivityNameTextBox"
                                                                                WatermarkText="Nome Attributo" WatermarkCssClass="watermarked" />
                                                                        </td>
                                                                        <td style="width: 25%;">
                                                                            <asp:DropDownList ID="ControlTypeUIDropdownlist" runat="server" Width="100%" SelectedValue='<%# Bind("ControlTypeUI") %>'>
                                                                                <asp:ListItem Value="CheckBox" Text="CheckBox" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Value="ComboBox" Text="ComboBox"></asp:ListItem>
                                                                                <asp:ListItem Value="TextBox" Text="TextBox"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 50%;">
                                                                            <asp:TextBox ID="EnumValuesTextBox" runat="server" Text='<%# Bind("EnumValues") %>'
                                                                                Width="100%" />
                                                                            <asp:RegularExpressionValidator ID="rexEnumValues" runat="server" ControlToValidate="EnumValuesTextBox"
                                                                                ValidationExpression=".+\|.+" ValidationGroup="UpdateAttributeGroup" ErrorMessage="I valori introdotti devono essere divisi dal carattere pipe '|'"
                                                                                Display="None">
                                                                            </asp:RegularExpressionValidator>
                                                                            <act:ValidatorCalloutExtender runat="Server" ID="rexEnumValuesE" TargetControlID="rexEnumValues"
                                                                                HighlightCssClass="validatorCalloutHighlight" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="TargetTypeIDDropDownList" runat="server" SelectedValue='<%# Bind("TargetTypeID") %>'
                                                                                DataTextField="TargetTypeName" DataValueField="TargetTypeID" DataSourceID="linqTargetTypeID"
                                                                                Width="100%">
                                                                            </asp:DropDownList>
                                                                            <asp:LinqDataSource ID="linqTargetTypeID" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                                                TableName="TargetTypes" OrderBy="TargetTypeName DESC" Select="new (TargetTypeID, TargetTypeName)">
                                                                            </asp:LinqDataSource>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ActivityGroupIDDropDownList" runat="server" SelectedValue='<%# Bind("ActivityGroupsID") %>'
                                                                                DataTextField="ActivityGroupName" DataValueField="ActivityGroupID" DataSourceID="linqActivityGroup"
                                                                                Width="100%">
                                                                            </asp:DropDownList>
                                                                            <asp:LinqDataSource ID="linqActivityGroup" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
                                                                                TableName="ActivityGroups" OrderBy="ActivityGroupName" Select="new (ActivityGroupID, ActivityGroupName)">
                                                                            </asp:LinqDataSource>
                                                                        </td>
                                                                        <td>
                                                                            <geta:GetaCheckBox ID="IsObsoleteCheckBox" runat="server" Checked='<%# Eval("IsObsolete") %>'
                                                                                Text="Obsoleto" Readonly="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:Label ID="lblInsertMessage" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table cellpadding="4" cellspacing="4" style="text-align: left;">
                                                                    <tr>
                                                                        <td style="color: White; text-align: left;">
                                                                            <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Aggiorna"
                                                                                ValidationGroup="UpdateAttributeGroup" ForeColor="White" CssClass="noDecorationLink" />
                                                                            <span>| </span>
                                                                            <asp:LinkButton ID="Linkbutton1" runat="server" CommandName="Cancel" Text="Annulla"
                                                                                ForeColor="White" CssClass="noDecorationLink" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <tr style="">
                                                            <td>
                                                                <asp:ImageButton ID="imgbEdit" runat="Server" ImageUrl="~/Images/edit.gif" AlternateText="Modifica"
                                                                    ToolTip="Modifica" CommandName="Edit" CausesValidation="false" CssClass="noDecorationLink" />
                                                            </td>
                                                            <td style="white-space: normal;" class="gridItem">
                                                                <asp:Label ID="TargetActivityNameLabel" runat="server" Text='<%# Eval("TargetActivityName") %>'
                                                                    CssClass="selectableField" />
                                                            </td>
                                                            <td style="text-align: center;" class="gridItem">
                                                                &nbsp;<asp:Label ID="ControlTypeUILabel" runat="server" Text='<%# Eval("ControlTypeUI") %>'
                                                                    CssClass="selectableField" />
                                                            </td>
                                                            <td style="text-align: center;" class="gridItem">
                                                                &nbsp;<geta:GetaCheckBox ID="IsObsoleteCheckBox" runat="server" Checked='<%# Eval("IsObsolete") %>'
                                                                    CssClass="selectableField" />
                                                            </td>
                                                            <td class="gridItem">
                                                                &nbsp;<asp:Label ID="EnumValuesLabel" runat="server" Text='<%# Eval("EnumValues") %>' tooltip='<%# Eval("EnumValues") %>' CssClass="selectableField" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgbDelete" runat="Server" ImageUrl="~/Images/delete.gif" AlternateText="Elimina"
                                                                    ToolTip="Elimina" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Confermi di voler cancellare l\' attributo?');"
                                                                    CssClass="noDecorationLink" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <ItemSeparatorTemplate>
                                                        <tr>
                                                            <td colspan="5">
                                                                <hr class="hrStyle" />
                                                            </td>
                                                        </tr>
                                                    </ItemSeparatorTemplate>
                                                </asp:ListView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td><img src="Images/lightpanel_bot_sx.gif" alt="" /></td>
                                    <td style="width: 98%; background-image: url(Images/lightpanel_bot_bg.gif); background-repeat: repeat-x;">
                                        &nbsp;
                                    </td>
                                    <td><img src="Images/lightpanel_bot_dx.gif" alt="" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:LinqDataSource ID="linqAttributes" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
        TableName="TargetActivities" OrderBy="TargetType.TargetTypeName,Order"
        EnableDelete="True" EnableInsert="True" EnableUpdate="True" OnSelecting="linqAttributes_Selecting"
        OnInserting="linqAttributes_Inserting" OnUpdating="linqAttributes_Updating">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="linqTargetTypes" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
        OrderBy="TargetTypeName" EnableInsert="true" EnableUpdate="true" EnableDelete="true"
        TableName="TargetTypes">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="linqNamespaces" runat="server" ContextTypeName="GrisSuite.GETA.Data.DBIssuesDataContext"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true" TableName="ActivityGroups" OrderBy="Order">
    </asp:LinqDataSource>
</asp:Content>

USE [master];
GO

-- Create my global vars
CREATE TABLE #myvars (OldServerName varchar(100), NewServerName varchar(100), ErrorCount int);
INSERT #myvars VALUES (CONVERT(varchar(100), @@SERVERNAME),CONVERT(varchar(100), '$(NEWSERVERNAME)\SQLEXPRESS'),0);


/*********************************************/
/*          Set Max Server Memory            */
/*********************************************/
EXEC sys.sp_configure N'show advanced options', N'1'  RECONFIGURE WITH OVERRIDE;
GO
EXEC sys.sp_configure N'max server memory (MB)', N'256';
EXEC sys.sp_configure N'user instances enabled', N'0';
GO
RECONFIGURE WITH OVERRIDE;
GO
EXEC sys.sp_configure N'show advanced options', N'0'  RECONFIGURE WITH OVERRIDE;
GO


/*****************************************/
/*          Adjust ServerName            */
/*****************************************/
DECLARE @oserver varchar(100), @nserver varchar(100);
DECLARE @ErrorVar int;
SET @oserver = (SELECT OldServerName FROM #myvars);
SET @nserver = (SELECT NewServerName FROM #myvars);
IF @oserver <> @nserver
	BEGIN
		-- Drop the OLD server name
		EXEC sp_dropserver @oserver;
		SELECT @ErrorVar = @@ERROR;
		IF @ErrorVar <> 0
			BEGIN
				UPDATE #myvars SET ErrorCount = ErrorCount + 1;
				PRINT 'ERROR in sp_dropserver ' + @oserver + ' Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
			END
		ELSE
			PRINT 'Servername ' + @oserver + ' is Dropped!';
		-- Add the correct server as a local server
		EXEC sp_addserver @nserver, @local='local';
		SELECT @ErrorVar = @@ERROR;
		IF @ErrorVar <> 0
			BEGIN
				UPDATE #myvars SET ErrorCount = ErrorCount + 1;
				PRINT 'ERROR in sp_addserver ' + @nserver + ' Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
			END
		ELSE
			PRINT 'Servername ' + @nserver + ' is Added!';
	END
GO


/*****************************************/
/*           Adjust Security             */
/*****************************************/
-- Drop the current Logins
DECLARE @gruppo varchar(100), @SQLString nvarchar(200), @ErrorVar int;
SELECT @gruppo = (SELECT name FROM sys.server_principals WHERE name like '%\StlcAppsAdmins')
IF (@gruppo != '')
 BEGIN
	SET @SQLString = 'DROP LOGIN [' + @gruppo + ']'
	EXEC sp_executesql @SQLString
	SET @gruppo = ''
	IF @ErrorVar <> 0
		BEGIN
			UPDATE #myvars SET ErrorCount = ErrorCount + 1;
			PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
		END
 END
GO

DECLARE @gruppo varchar(100), @SQLString nvarchar(200), @ErrorVar int;
SELECT @gruppo = (SELECT name FROM sys.server_principals WHERE name like '%\StlcAppsUsers')
IF (@gruppo != '')
 BEGIN
	SET @SQLString = 'DROP LOGIN [' + @gruppo + ']'
	EXEC sp_executesql @SQLString
	SET @gruppo = ''
	IF @ErrorVar <> 0
		BEGIN
			UPDATE #myvars SET ErrorCount = ErrorCount + 1;
			PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
		END
 END
GO

DECLARE @gruppo varchar(100), @SQLString nvarchar(200), @ErrorVar int;
SELECT @gruppo = (SELECT name FROM sys.server_principals WHERE name like '%\StlcAppsServices')
IF (@gruppo != '')
 BEGIN
	SET @SQLString = 'DROP LOGIN [' + @gruppo + ']'
	EXEC sp_executesql @SQLString
	SET @gruppo = ''
	IF @ErrorVar <> 0
		BEGIN
			UPDATE #myvars SET ErrorCount = ErrorCount + 1;
			PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
		END
 END
GO

-- Add the correct logins
IF NOT EXISTS(SELECT name FROM [sys].[syslogins] WHERE NAME = 'BUILTIN\Administrators')
	BEGIN 
		CREATE LOGIN [BUILTIN\Administrators] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	END
GO

DECLARE @SQLString nvarchar(200), @ErrorVar int;
SET @SQLString = 'CREATE LOGIN [$(NEWSERVERNAME)\StlcAppsAdmins] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
EXEC sp_executesql @SQLString
SELECT @ErrorVar = @@ERROR;
IF @ErrorVar <> 0
	BEGIN
		UPDATE #myvars SET ErrorCount = ErrorCount + 1;
		PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
	END
GO

DECLARE @SQLString nvarchar(200), @ErrorVar int;
SET @SQLString = 'CREATE LOGIN [$(NEWSERVERNAME)\StlcAppsServices] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
EXEC sp_executesql @SQLString
SELECT @ErrorVar = @@ERROR;
IF @ErrorVar <> 0
	BEGIN
		UPDATE #myvars SET ErrorCount = ErrorCount + 1;
		PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
	END
GO

DECLARE @SQLString nvarchar(200), @ErrorVar int;
SET @SQLString = 'CREATE LOGIN [$(NEWSERVERNAME)\StlcAppsUsers] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
EXEC sp_executesql @SQLString
SELECT @ErrorVar = @@ERROR;
IF @ErrorVar <> 0
	BEGIN
		UPDATE #myvars SET ErrorCount = ErrorCount + 1;
		PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
	END
GO

-- Set serverrole
EXEC master..sp_addsrvrolemember @loginame = N'BUILTIN\Administrators', @rolename = N'sysadmin'
GO

/*******************************************************************/
/*           Telefin DB Users Creation and Permissions             */
/*******************************************************************/
USE [Telefin]
GO

/****** Drop Users ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name like 'StlcAppsUsers')
	DROP USER [StlcAppsUsers]
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name like 'StlcAppsAdmins')
	DROP USER [StlcAppsAdmins]
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name like 'StlcAppsServices')
	DROP USER [StlcAppsServices]

/****** Create Users ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name like 'BUILTIN\Administrators')
	BEGIN
		CREATE USER [BUILTIN\Administrators] FOR LOGIN [BUILTIN\Administrators]
		ALTER AUTHORIZATION ON SCHEMA::[db_owner] TO [BUILTIN\Administrators]
		EXEC sp_addrolemember N'db_owner', N'BUILTIN\Administrators'
	END
GO

DECLARE @SQLString nvarchar(200), @ErrorVar int;
SET @SQLString = 'CREATE USER [StlcAppsAdmins] FOR LOGIN [$(NEWSERVERNAME)\StlcAppsAdmins]'
EXEC sp_executesql @SQLString
SELECT @ErrorVar = @@ERROR;
IF @ErrorVar <> 0
	BEGIN
		UPDATE #myvars SET ErrorCount = ErrorCount + 1;
		PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
	END
GO

DECLARE @SQLString nvarchar(200), @ErrorVar int;
SET @SQLString = 'CREATE USER [StlcAppsServices] FOR LOGIN [$(NEWSERVERNAME)\StlcAppsServices]'
EXEC sp_executesql @SQLString
SELECT @ErrorVar = @@ERROR;
IF @ErrorVar <> 0
	BEGIN
		UPDATE #myvars SET ErrorCount = ErrorCount + 1;
		PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
	END
GO

DECLARE @SQLString nvarchar(200), @ErrorVar int;
SET @SQLString = 'CREATE USER [StlcAppsUsers] FOR LOGIN [$(NEWSERVERNAME)\StlcAppsUsers]'
EXEC sp_executesql @SQLString
SELECT @ErrorVar = @@ERROR;
IF @ErrorVar <> 0
	BEGIN
		UPDATE #myvars SET ErrorCount = ErrorCount + 1;
		PRINT 'ERROR in [' + @SQLString + '] Error code: ' + CAST(@ErrorVar AS NVARCHAR(8));
	END
GO

GRANT CONNECT TO [StlcAppsServices]
GO
GRANT EXECUTE TO [StlcAppsServices]
GO
GRANT CONNECT TO [StlcAppsAdmins]
GO
GRANT EXECUTE TO [StlcAppsAdmins]
GO
GRANT CONNECT TO [StlcAppsUsers]
GO
GRANT EXECUTE TO [StlcAppsUsers]
GO

EXEC sp_addrolemember N'db_datawriter', N'StlcAppsServices'
GO
EXEC sp_addrolemember N'db_datareader', N'StlcAppsServices'
GO
EXEC sp_addrolemember N'db_datawriter', N'StlcAppsAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'StlcAppsAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'StlcAppsUsers'
GO

/*******************************************************************/
/*           END Users Creation and Permissions                    */
/*******************************************************************/
IF (SELECT ErrorCount FROM #myvars) = 0
	PRINT 'SQLEXPRESS is successfully adjusted!';
ELSE
	PRINT 'SQLEXPRESS Settings is terminated with Errors!';

-- Drop temp table
IF object_id('tempdb..#myvars') IS NOT NULL
    DROP TABLE #myvars
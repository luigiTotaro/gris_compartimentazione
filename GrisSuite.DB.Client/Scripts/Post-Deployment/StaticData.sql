﻿-- =============================================
-- Static Data
-- =============================================

INSERT INTO [dbo].[stlc_parameters] ([ParameterName], [ParameterValue], [ParameterDescription]) VALUES ('DBVersion', '2.1.0.0', 'La versione del DataBase')

INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (-255, 'Informazione')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (0, 'Informazione')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (1, 'Avviso')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (2, 'Errore')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (255, 'Sconosciuta')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (9, 'Da configurare')

INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription]) VALUES (1, 'Produttore di default', NULL)

INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription]) VALUES (1, 'Sistema di Default')


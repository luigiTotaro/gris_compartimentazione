ALTER PROCEDURE [dbo].[tf_GetDevices]
AS
	SET NOCOUNT ON;
	SELECT DevID, NodID, SrvID, Name, COALESCE(DiscoveredType, [Type]) AS [Type], SN, PortID, Addr, ProfileID, Active, Scheduled, Removed, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion, DiscoveredType, MonitoringDeviceID
	FROM devices
	WHERE ISNULL(Removed,0) = 0
GO
ALTER PROCEDURE [dbo].[sWEB_GetServer]

AS
SELECT 
		OutServers.SrvID, OutServers.Name, OutServers.Host, OutServers.FullHostName, OutServers.IP, OutServers.LastUpdate, 
		OutServers.LastMessageType, OutServers.SupervisorSystemXML, OutServers.ClientSupervisorSystemXMLValidated, 
		OutServers.ClientValidationSign, OutServers.ClientDateValidationRequested, OutServers.ClientDateValidationObtained, OutServers.ClientKey
	FROM servers OutServers
	WHERE EXISTS
	(
		SELECT *
		FROM servers InnServers
		INNER JOIN devices ON InnServers.SrvID = devices.SrvID
		WHERE InnServers.SrvID = OutServers.SrvID
	);
GO
ALTER PROCEDURE [dbo].[tf_GetServers]
AS
	SET NOCOUNT ON;

	SELECT
		SrvID, [Name], Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
	AND (ISNULL([servers].Removed, 0) = 0)
GO
ALTER PROCEDURE [dbo].[tf_GetDeviceStatus]
AS
SET NOCOUNT ON;
SELECT device_status.DevID, device_status.SevLevel, device_status.Description, device_status.Offline, 0 as IsDeleted, device_status.ShouldSendNotificationByEmail, devices.Name, devices.MonitoringDeviceID, devices.NodID
FROM device_status
INNER JOIN devices ON device_status.DevID = devices.DevID
WHERE device_status.DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO
ALTER PROCEDURE [dbo].[tf_GetStreams]
AS
	SET NOCOUNT ON;
	SELECT DevID, StrID, [Name], [DateTime], SevLevel, Visible, 0 as IsDeleted, Data	
	FROM streams
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO
ALTER PROCEDURE [dbo].[tf_GetStream_Fields]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID, ShouldSendNotificationByEmail
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO
ALTER PROCEDURE [dbo].[tf_GetReference]

AS
SET NOCOUNT ON;

	SELECT ReferenceID, Value, DateTime, Visible, DeltaDevID, DeltaStrID, DeltaFieldID, DeltaArrayID
	FROM reference
	WHERE ReferenceID IN 
		(SELECT DISTINCT ReferenceID 
			FROM Stream_Fields INNER JOIN Devices ON Stream_Fields.DevID = Devices.DevID
			WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO
CREATE PROCEDURE tf_GetDeviceDataByIP (@IP VARCHAR(15)) AS
SET NOCOUNT ON;

SELECT
devices.DevID,
devices.Name AS DeviceName,
devices.SN,
devices.Addr,
dbo.GetIPStringFromInt(devices.Addr) AS IP,
COALESCE(devices.DiscoveredType, devices.Type) AS [Type],
devices.MonitoringDeviceID,
nodes.NodID,
nodes.Name AS NodeName,
device_status.SevLevel,
device_status.Description,
device_status.Offline
FROM devices
INNER JOIN device_status ON devices.DevID = device_status.DevID
INNER JOIN nodes ON devices.NodID = nodes.NodID
WHERE (dbo.GetIPStringFromInt(devices.Addr) = @IP)
AND (ISNULL(devices.Removed, 0) = 0)
AND (devices.MonitoringDeviceID IS NOT NULL)
GO

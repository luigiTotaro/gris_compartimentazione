USE [master]
GO
IF EXISTS (SELECT name FROM sys.databases WHERE name = N'Telefin')
BEGIN
	DECLARE @ActiveConnections AS INT

	SELECT
	@ActiveConnections = (select count(*) from master.dbo.sysprocesses p where dtb.database_id=p.dbid)
	FROM
	master.sys.databases AS dtb
	WHERE
	(dtb.name=N'Telefin')

	IF (@ActiveConnections > 0)
	BEGIN
		ALTER DATABASE [Telefin] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
	END

	DROP DATABASE [Telefin]
END
CREATE DATABASE [Telefin] ON  PRIMARY 
( NAME = N'Telefin', FILENAME = N'C:\Program Files\Telefin\DB\Telefin.mdf' , SIZE = 5120KB , MAXSIZE = 50MB, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Telefin_log', FILENAME = N'C:\Program Files\Telefin\DB\Telefin_log.LDF' , SIZE = 560KB , MAXSIZE = 200MB , FILEGROWTH = 10%)
COLLATE Latin1_General_CI_AS
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'Telefin', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Telefin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Telefin] SET ANSI_NULL_DEFAULT ON
GO
ALTER DATABASE [Telefin] SET ANSI_NULLS ON
GO
ALTER DATABASE [Telefin] SET ANSI_PADDING ON
GO
ALTER DATABASE [Telefin] SET ANSI_WARNINGS ON
GO
ALTER DATABASE [Telefin] SET ARITHABORT ON
GO
ALTER DATABASE [Telefin] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Telefin] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Telefin] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Telefin] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Telefin] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Telefin] SET CURSOR_DEFAULT  LOCAL
GO
ALTER DATABASE [Telefin] SET CONCAT_NULL_YIELDS_NULL ON
GO
ALTER DATABASE [Telefin] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Telefin] SET QUOTED_IDENTIFIER ON
GO
ALTER DATABASE [Telefin] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Telefin] SET  DISABLE_BROKER
GO
ALTER DATABASE [Telefin] SET AUTO_UPDATE_STATISTICS_ASYNC ON
GO
ALTER DATABASE [Telefin] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Telefin] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Telefin] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Telefin] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Telefin] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Telefin] SET  READ_WRITE
GO
ALTER DATABASE [Telefin] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Telefin] SET  MULTI_USER
GO
ALTER DATABASE [Telefin] SET PAGE_VERIFY NONE
GO
ALTER DATABASE [Telefin] SET DB_CHAINING OFF
GO
USE [Telefin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_hexstrtovarbin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_hexstrtovarbin]
(@input VARCHAR (8000))
RETURNS VARBINARY (8000)
AS
begin 
declare @result varbinary(8000), @i int, @l int 

set @result = 0x 
set @l = len(@input)/2 
set @i = 2 

while @i <= @l 
begin 
set @result = @result + 
cast(cast(case lower(substring(@input, @i*2-1, 1)) 
when ''0'' then 0x00 
when ''1'' then 0x10 
when ''2'' then 0x20 
when ''3'' then 0x30 
when ''4'' then 0x40 
when ''5'' then 0x50 
when ''6'' then 0x60 
when ''7'' then 0x70 
when ''8'' then 0x80 
when ''9'' then 0x90 
when ''a'' then 0xa0 
when ''b'' then 0xb0 
when ''c'' then 0xc0 
when ''d'' then 0xd0 
when ''e'' then 0xe0 
when ''f'' then 0xf0 
end as tinyint) | 
cast(case lower(substring(@input, @i*2, 1)) 
when ''0'' then 0x00 
when ''1'' then 0x01 
when ''2'' then 0x02 
when ''3'' then 0x03 
when ''4'' then 0x04 
when ''5'' then 0x05 
when ''6'' then 0x06 
when ''7'' then 0x07 
when ''8'' then 0x08 
when ''9'' then 0x09 
when ''a'' then 0x0a 
when ''b'' then 0x0b 
when ''c'' then 0x0c 
when ''d'' then 0x0d 
when ''e'' then 0x0e 
when ''f'' then 0x0f 
end as tinyint) as binary(1)) 
set @i = @i + 1 
end 

return @result 
end

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[systems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[systems](
	[SystemID] [int] NOT NULL,
	[SystemDescription] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
 CONSTRAINT [PK_systems] PRIMARY KEY CLUSTERED 
(
	[SystemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vendors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vendors](
	[VendorID] [int] NOT NULL,
	[VendorName] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
	[VendorDescription] [varchar](512) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_vendors] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[util_ShrinkTransactionLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[util_ShrinkTransactionLog]
(
	@TriggerTranLogSize INT = 50
)
AS
	
	-- @TriggerTranLogSize è la grandezza massima in MB oltre la quale viene eseguito lo shrink del file
	
	DECLARE @RETURN BIT;
	DECLARE @LogFileName VARCHAR(500)
	DECLARE @PagesNumb INT

	SELECT @LogFileName = [name], @PagesNumb = [size] FROM sys.database_files WHERE type_desc = ''LOG''

	IF (''SIMPLE'' = (SELECT recovery_model_desc FROM sys.databases WHERE [name] = db_name()))
	BEGIN 
		BEGIN TRY
			IF ((@PagesNumb*8/1024) > @TriggerTranLogSize)
			BEGIN
				DBCC SHRINKFILE (@LogFileName) WITH NO_INFOMSGS;
				SET @RETURN = 1;
			END
			ELSE
			BEGIN
				SET @RETURN = 0;
			END
		END TRY
		BEGIN CATCH
			SET @RETURN = 0;
		END CATCH
	END
	ELSE 
	BEGIN
		--ALTER DATABASE Telefin
		--SET RECOVERY SIMPLE;
		--GO

		--DBCC SHRINKFILE (Telefin);
		--GO

		--ALTER DATABASE Telefin
		--SET RECOVERY FULL;
		SET @RETURN = 0;
	END;
	SELECT @RETURN;
RETURN 0;' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stlc_parameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[stlc_parameters](
	[ParameterName] [varchar](64) COLLATE Latin1_General_CI_AS NOT NULL,
	[ParameterValue] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
	[ParameterDescription] [varchar](1024) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_stlc_parameters] PRIMARY KEY CLUSTERED 
(
	[ParameterName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[station]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[station](
	[StationID] [uniqueidentifier] NOT NULL,
	[StationXMLID] [int] NULL,
	[StationName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_station] PRIMARY KEY CLUSTERED 
(
	[StationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[severity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[severity](
	[SevLevel] [int] NOT NULL,
	[Description] [varchar](128) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_severity] PRIMARY KEY CLUSTERED 
(
	[SevLevel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[regions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[regions](
	[RegID] [bigint] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_regions] PRIMARY KEY CLUSTERED 
(
	[RegID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reference]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[reference](
	[ReferenceID] [uniqueidentifier] NOT NULL,
	[Value] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[DateTime] [datetime] NOT NULL,
	[Visible] [tinyint] NULL,
	[DeltaDevID] [bigint] NULL,
	[DeltaStrID] [int] NULL,
	[DeltaFieldID] [int] NULL,
	[DeltaArrayID] [int] NULL,
 CONSTRAINT [PK_field_reference] PRIMARY KEY CLUSTERED 
(
	[ReferenceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[device_ack]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[device_ack](
	[DeviceAckID] [uniqueidentifier] NOT NULL,
	[DevID] [bigint] NOT NULL,
	[StrID] [int] NULL,
	[FieldID] [int] NULL,
	[AckDate] [datetime] NOT NULL,
	[AckDurationMinutes] [int] NOT NULL,
	[SupervisorID] [tinyint] NOT NULL,
	[Username] [varchar](256) COLLATE Latin1_General_CI_AS NOT NULL,
 CONSTRAINT [PK_device_ack_DeviceAckID] PRIMARY KEY CLUSTERED 
(
	[DeviceAckID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_device_ack_DevID_StrID_FieldID] UNIQUE NONCLUSTERED 
(
	[DevID] ASC,
	[StrID] ASC,
	[FieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[building]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[building](
	[BuildingID] [uniqueidentifier] NOT NULL,
	[BuildingXMLID] [int] NULL,
	[StationID] [uniqueidentifier] NULL,
	[BuildingName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[BuildingDescription] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_building] PRIMARY KEY CLUSTERED 
(
	[BuildingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[device_type]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[device_type](
	[DeviceTypeID] [varchar](16) COLLATE Latin1_General_CI_AS NOT NULL,
	[SystemID] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
	[DeviceTypeDescription] [varchar](512) COLLATE Latin1_General_CI_AS NULL,
	[WSUrlPattern] [varchar](512) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_device_type] PRIMARY KEY CLUSTERED 
(
	[DeviceTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdRegions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdRegions]    Script Date: 11/20/2006 18:19:09 ******/
CREATE PROCEDURE [dbo].[tf_UpdRegions]
(
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT RegID FROM regions WHERE (RegID = @RegID))
		UPDATE [regions] SET [RegID] = @RegID, [Name] = @Name, [Removed] = @Removed WHERE (([RegID] = @RegID));
	ELSE
		INSERT INTO [regions] ([RegID], [Name], [Removed]) VALUES (@RegID, @Name, @Removed);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdReference]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdReference]
(
	@ReferenceID uniqueidentifier,
	@Value varchar(256),
	@DateTime datetime,
	@Visible tinyint,
	@DeltaDevID bigint,
	@DeltaStrID int,
	@DeltaFieldID int,
	@DeltaArrayID int
)
AS
	SET NOCOUNT OFF;
	IF EXISTS (SELECT ReferenceID FROM reference WHERE (ReferenceID = @ReferenceID))
		UPDATE [reference] SET [Value] = @Value, [DateTime] = @DateTime, [Visible] = @Visible, [DeltaDevID] = @DeltaDevID, [DeltaStrID] = @DeltaStrID, [DeltaFieldID] = @DeltaFieldID, [DeltaArrayID] = @DeltaArrayID WHERE (([ReferenceID] = @ReferenceID));
	ELSE
		INSERT INTO [reference] ([ReferenceID], [Value], [DateTime], [Visible], [DeltaDevID], [DeltaStrID], [DeltaFieldID], [DeltaArrayID]) VALUES (@ReferenceID, @Value, @DateTime, @Visible, @DeltaDevID, @DeltaStrID, @DeltaFieldID, @DeltaArrayID);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_InsSTLCParameters]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_InsSTLCParameters]
(
	@SrvID int,
	@ParameterValue varchar(256),
	@ParameterName varchar(64),
	@ParameterDescription varchar(1024)
)
AS
	SET NOCOUNT OFF;

	IF EXISTS (SELECT * FROM [stlc_parameters] WHERE ([ParameterName] = @ParameterName))
	BEGIN
		UPDATE [stlc_parameters]
		SET [ParameterValue] = @ParameterValue,
		[ParameterDescription] = @ParameterDescription
		WHERE ([ParameterName] = @ParameterName);		
	END
	ELSE
	BEGIN
		INSERT INTO [stlc_parameters] ([ParameterValue], [ParameterName], [ParameterDescription])
		VALUES (@ParameterValue, @ParameterName, @ParameterDescription);
	END
RETURN 0;' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_InsDeviceAck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_InsDeviceAck]
	@DeviceAckID uniqueidentifier,
	@DevID bigint,
	@StrID int, 
	@FieldID int,
	@AckDate datetime, 
	@AckDurationMinutes int,
	@SupervisorID tinyint, 
	@Username nvarchar(255)
AS
	
	IF EXISTS (SELECT DeviceAckID FROM  device_ack WHERE (DeviceAckID = @DeviceAckID))
	BEGIN
		UPDATE device_ack
		SET DevID = @DevID,
		StrID = @StrID,
		FieldID = @FieldID,
		AckDate = @AckDate,
		AckDurationMinutes = @AckDurationMinutes,
		SupervisorID = @SupervisorID,
		Username = @Username
		WHERE (DeviceAckID = @DeviceAckID);
	END
	ELSE
	BEGIN
		INSERT INTO device_ack (DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username)
		VALUES (@DeviceAckID, @DevID, @StrID, @FieldID, @AckDate, @AckDurationMinutes, @SupervisorID, @Username);
	END

RETURN @@ROWCOUNT;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetRegions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetRegions]    Script Date: 11/20/2006 18:19:04 ******/
CREATE PROCEDURE [dbo].[tf_GetRegions]
AS
	SET NOCOUNT ON;
	SELECT RegID, Name, Removed
	FROM regions
	WHERE ISNULL(Removed,0) = 0' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdDeviceAck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdDeviceAck]
	@Original_DeviceAckID uniqueidentifier,
	@DevID bigint,
	@StrID int, 
	@FieldID int,
	@AckDate datetime, 
	@AckDurationMinutes int,
	@SupervisorID tinyint, 
	@Username nvarchar(255)
AS
UPDATE device_ack
SET DevID = @DevID,
StrID = @StrID,
FieldID = @FieldID,
AckDate = @AckDate,
AckDurationMinutes = @AckDurationMinutes,
SupervisorID = @SupervisorID,
Username = @Username
WHERE (DeviceAckID = @Original_DeviceAckID);

RETURN @@ROWCOUNT;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetStation]

AS
SET NOCOUNT ON;
	SELECT StationID, StationXMLID, StationName, Removed
	FROM station
	WHERE ISNULL(Removed,0) = 0

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelSTLCParameters]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelSTLCParameters]
(
	@Original_SrvID int,
	@Original_ParameterName varchar(64)
)
AS
	SET NOCOUNT OFF;
	DELETE FROM [stlc_parameters] WHERE ([ParameterName] = @Original_ParameterName)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDeviceAcksToSend]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDeviceAcksToSend] AS
SET NOCOUNT ON;
SELECT DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username
FROM device_ack
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDeviceAckByDevId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDeviceAckByDevId]
	@DevID bigint
AS
	SELECT DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username
	FROM device_ack
	WHERE (DevID = @DevID);
RETURN @@ROWCOUNT;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zones](
	[ZonID] [bigint] NOT NULL,
	[RegID] [bigint] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_zones] PRIMARY KEY CLUSTERED 
(
	[ZonID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdSTLCParameters]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdSTLCParameters]
(
	@SrvID int,
	@ParameterValue varchar(256),
	@ParameterName varchar(64),
	@ParameterDescription varchar(1024),
	@Original_SrvID int,
	@Original_ParameterName varchar(64)
)
AS
	SET NOCOUNT OFF;
	UPDATE [stlc_parameters]
	SET [ParameterValue] = @ParameterValue,
	[ParameterName] = @ParameterName,
	[ParameterDescription] = @ParameterDescription
	WHERE ([ParameterName] = @Original_ParameterName);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdStation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdStation]
(
	@StationID uniqueidentifier,
	@StationXMLID int,
	@StationName varchar(64), 
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT StationID FROM station WHERE (StationID = @StationID))
		UPDATE [station] SET [StationID] = @StationID, [StationXMLID] = @StationXMLID, [StationName] = @StationName, [Removed] = @Removed WHERE (([StationID] = @StationID));
	ELSE
		INSERT INTO [station] ([StationID], [StationXMLID], [StationName], [Removed]) VALUES (@StationID, @StationXMLID, @StationName, @Removed);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelDeviceAck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelDeviceAck] (@Original_DeviceAckID UNIQUEIDENTIFIER) AS
DELETE FROM device_ack WHERE (DeviceAckID = @Original_DeviceAckID);
RETURN @@ROWCOUNT;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetIPStringFromInt]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetIPStringFromInt]
(
	@IpBigIntRepresentation BIGINT
)
RETURNS VARCHAR(15)
AS
BEGIN
	DECLARE @IpVarbinRepresentation AS VARBINARY(16);
	DECLARE @Result AS VARCHAR(15);
	DECLARE @IpHexRepresentation AS CHAR(18); -- 16 + 2 (0x)

	SET @IpVarbinRepresentation = CAST(@IpBigIntRepresentation AS VARBINARY(16));
	EXECUTE @IpHexRepresentation = sys.fn_varbintohexstr @IpVarbinRepresentation;

	SET @Result =	CAST((CAST(dbo.fn_hexstrtovarbin(''0x'' + SUBSTRING(@IpHexRepresentation, 11, 2)) AS TINYINT)) AS VARCHAR(3)) + ''.'' + 
					CAST((CAST(dbo.fn_hexstrtovarbin(''0x'' + SUBSTRING(@IpHexRepresentation, 13, 2)) AS TINYINT)) AS VARCHAR(3)) + ''.'' + 
					CAST((CAST(dbo.fn_hexstrtovarbin(''0x'' + SUBSTRING(@IpHexRepresentation, 15, 2)) AS TINYINT)) AS VARCHAR(3)) + ''.'' + 
					CAST((CAST(dbo.fn_hexstrtovarbin(''0x'' + SUBSTRING(@IpHexRepresentation, 17, 2)) AS TINYINT)) AS VARCHAR(3));
	
	IF (@@ERROR <> 0)
	BEGIN
		SET @Result = ''0.0.0.0'';
	END

	RETURN @Result;
END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelRegions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelRegions]
(
	@Original_RegID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [regions] WHERE (([RegID] = @Original_RegID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[nodes](
	[NodID] [bigint] NOT NULL,
	[ZonID] [bigint] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
	[Meters] [int] NULL,
 CONSTRAINT [PK_nodes] PRIMARY KEY CLUSTERED 
(
	[NodID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdZones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdZones]    Script Date: 11/20/2006 18:19:11 ******/

CREATE PROCEDURE [dbo].[tf_UpdZones]
(
	@ZonID bigint,
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT ZonID FROM zones WHERE (ZonID = @ZonID))
		UPDATE [zones] SET [ZonID] = @ZonID, [RegID] = @RegID, [Name] = @Name, [Removed] = @Removed WHERE (([ZonID] = @ZonID));
	ELSE
		INSERT INTO [zones] ([ZonID], [RegID], [Name],[Removed]) VALUES (@ZonID, @RegID, @Name, @Removed);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetBuilding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetBuilding]

AS
SET NOCOUNT ON;
	SELECT BuildingID, BuildingXMLID, StationID, BuildingName, BuildingDescription, Removed
	FROM building
	WHERE ISNULL(Removed,0) = 0

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelZones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelZones]
(
	@Original_ZonID bigint
)
AS
	SET NOCOUNT ON;
	DELETE FROM [zones] WHERE (([ZonID] = @Original_ZonID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdBuilding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdBuilding]
(
	@BuildingID uniqueidentifier,
	@BuildingXMLID int,
	@StationID uniqueidentifier,
	@BuildingName varchar(64),
	@BuildingDescription varchar(256),
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT BuildingID FROM [building] WHERE ([BuildingID] = @BuildingID))
		UPDATE [building] SET [BuildingXMLID] = @BuildingXMLID, [StationID] = @StationID, [BuildingName] = @BuildingName, [BuildingDescription] = @BuildingDescription, Removed = @Removed WHERE [BuildingID] = @BuildingID;
	ELSE
		INSERT INTO [building] ([BuildingID], [BuildingXMLID], [StationID], [BuildingName], [BuildingDescription], [Removed]) VALUES (@BuildingID, @BuildingXMLID, @StationID, @BuildingName, @BuildingDescription, @Removed);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetZones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetZones]    Script Date: 11/20/2006 18:19:06 ******/
CREATE PROCEDURE [dbo].[tf_GetZones]
AS
	SET NOCOUNT ON;
	SELECT ZonID, RegID, Name, Removed
	FROM zones
	WHERE ISNULL(Removed,0) = 0' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rack]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rack](
	[RackID] [uniqueidentifier] NOT NULL,
	[RackXMLID] [int] NULL,
	[BuildingID] [uniqueidentifier] NULL,
	[RackName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[RackType] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[RackDescription] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_rack] PRIMARY KEY CLUSTERED 
(
	[RackID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[servers](
	[SrvID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Host] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[FullHostName] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[IP] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[LastUpdate] [datetime] NULL,
	[LastMessageType] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[SupervisorSystemXML] [xml] NULL,
	[ClientSupervisorSystemXMLValidated] [xml] NULL,
	[ClientValidationSign] [varbinary](128) NULL,
	[ClientDateValidationRequested] [datetime] NULL,
	[ClientDateValidationObtained] [datetime] NULL,
	[ClientKey] [varbinary](148) NULL,
	[NodID] [bigint] NULL,
	[ServerVersion] [varchar](32) COLLATE Latin1_General_CI_AS NULL,
	[MAC] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
 CONSTRAINT [PK_servers] PRIMARY KEY CLUSTERED 
(
	[SrvID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdNodes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_UpdNodes]    Script Date: 11/20/2006 18:19:08 ******/
CREATE PROCEDURE [dbo].[tf_UpdNodes]
(
	@NodID bigint,
	@ZonID bigint,
	@Name varchar(64),
	@Removed bit,
	@Meters int
)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT NodID FROM nodes WHERE (NodID = @NodID))
		UPDATE [nodes] SET [NodID] = @NodID, [ZonID] = @ZonID, [Name] = @Name, [Removed] = @Removed, Meters = @Meters WHERE (([NodID] = @NodID));
	ELSE
		INSERT INTO [nodes] ([NodID], [ZonID], [Name], [Removed], Meters) VALUES (@NodID, @ZonID, @Name, @Removed, @Meters);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdRack]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdRack]
(
	@RackID uniqueidentifier,
	@RackXMLID int,
	@BuildingID uniqueidentifier,
	@RackName varchar(64),
	@RackType varchar(64),
	@RackDescription varchar(256),
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS(SELECT RackID FROM rack WHERE (RackID = @RackID))
		UPDATE [rack] SET [RackXMLID] = @RackXMLID, [BuildingID] = @BuildingID, [RackName] = @RackName, [RackType] = @RackType, [RackDescription] = @RackDescription, [Removed] = @Removed WHERE (([RackID] = @RackID));
	ELSE
		INSERT INTO [rack] ([RackID], [RackXMLID], [BuildingID], [RackName], [RackType], [RackDescription], [Removed]) VALUES (@RackID, @RackXMLID, @BuildingID, @RackName, @RackType, @RackDescription, @Removed);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetNodes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_GetNodes]    Script Date: 11/20/2006 18:19:00 ******/
CREATE PROCEDURE [dbo].[tf_GetNodes]
AS
	SET NOCOUNT ON;
	SELECT NodID, ZonID, Name, Removed, Meters
	FROM nodes
	WHERE ISNULL(Removed,0) = 0' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetRack]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetRack]

AS
SET NOCOUNT ON;
	SELECT RackID, RackXMLID, BuildingID, RackName, RackType, RackDescription, Removed
	FROM rack
	WHERE ISNULL(Removed,0) = 0

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelNodes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelNodes]
(
	@Original_NodID bigint
)
AS
	SET NOCOUNT ON;
DELETE FROM [nodes] WHERE (([NodID] = @Original_NodID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_UpdServerAsValidated]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_UpdServerAsValidated]
(
	@SrvID int,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ClientDateValidationObtained datetime,
	@ClientKey varbinary(148)
	)
AS
	UPDATE servers
	SET ClientSupervisorSystemXMLValidated = @ClientSupervisorSystemXMLValidated, ClientValidationSign = @ClientValidationSign, ClientDateValidationObtained = @ClientDateValidationObtained, ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetSystemXMLInUseChanged]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetSystemXMLInUseChanged]
@SrvID INT
AS
SELECT 
		(
			CASE WHEN (
						SupervisorSystemXML IS NOT NULL 
						AND 
						ClientSupervisorSystemXMLValidated IS NOT NULL 
						AND 
						(CAST(SupervisorSystemXML AS NVARCHAR(MAX)) = CAST(ClientSupervisorSystemXMLValidated AS NVARCHAR(MAX)))
						) 
			THEN 1 
			ELSE 0 
			END
		) as SystemXMLInUseChanged
	FROM servers
	WHERE (SrvID = @SrvID);

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[port]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[port](
	[PortID] [uniqueidentifier] NOT NULL,
	[PortXMLID] [int] NULL,
	[PortName] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[PortType] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Parameters] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Status] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Removed] [tinyint] NULL,
	[SrvID] [int] NOT NULL,
 CONSTRAINT [PK_port] PRIMARY KEY CLUSTERED 
(
	[PortID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdServerStatusBySrvID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdServerStatusBySrvID]
(
	@SrvID int,
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64)
)
AS
	SET NOCOUNT OFF;

	DECLARE @ServerCount TINYINT;
	
	SET @ServerCount = (SELECT COUNT(*) FROM [servers] WHERE SrvID = @SrvID);
	
	IF ( @ServerCount = 1 )
	BEGIN
		UPDATE [servers] 
			SET [IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType
		WHERE SrvID = @SrvID;
	END
	
	SELECT @ServerCount;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdServerStatusByMAC]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdServerStatusByMAC]
(
	@MAC varchar(16),
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64)
)
AS
	SET NOCOUNT OFF;

	DECLARE @ServerCount TINYINT;
	
	SET @ServerCount = (SELECT COUNT(*) FROM [servers] WHERE MAC = @MAC);
	
	IF ( @ServerCount = 1 )
	BEGIN
		UPDATE [servers] 
			SET [IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType
		WHERE ISNULL(MAC,'''') = @MAC;
	END
	
	SELECT @ServerCount;' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdServers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdServers] (
@SrvID INT,
@Name VARCHAR (64),
@Host VARCHAR (64),
@FullHostName VARCHAR (256),
@IP VARCHAR (16),
@LastUpdate DATETIME,
@LastMessageType VARCHAR (64),
@SupervisorSystemXML XML,
@ClientSupervisorSystemXMLValidated XML,
@ClientValidationSign VARBINARY (128),
@ClientDateValidationRequested DATETIME,
@ClientDateValidationObtained DATETIME,
@ClientKey VARBINARY (148),
@NodID BIGINT = NULL,
@ServerVersion VARCHAR (32) = NULL,
@MAC VARCHAR (16) = NULL
) AS
	SET NOCOUNT OFF;

	SET @MAC = RTRIM(ISNULL(@MAC,''''))

	IF EXISTS (SELECT SrvID FROM servers WHERE ([SrvID] = @SrvID))
		UPDATE [servers] 
			SET [Name] = @Name, [Host] = @Host, [FullHostName] = @FullHostName, 
			[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
			[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
			[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
			[ClientKey] = @ClientKey, [NodID] = @NodID, [ServerVersion] = @ServerVersion, [MAC] = @MAC
		WHERE (([SrvID] = @SrvID));
	ELSE
		INSERT INTO [servers] ([SrvID], [Name], [Host], [FullHostName], [IP], [LastUpdate], [LastMessageType], [SupervisorSystemXML], [ClientSupervisorSystemXMLValidated], [ClientValidationSign], [ClientDateValidationRequested], [ClientDateValidationObtained], [ClientKey], [NodID], [ServerVersion], [MAC]) 
		VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetPort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetPort]

AS
SET NOCOUNT ON;
	SELECT PortID, PortXMLID, PortName, PortType, Parameters, Status, Removed, SrvID
	FROM port
	WHERE ISNULL(Removed,0) = 0

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdPort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdPort]
(
	@PortID uniqueidentifier,
	@PortXMLID int,
	@PortName varchar(64),
	@PortType varchar(64),
	@Parameters varchar(64),
	@Status varchar(64),
	@Removed tinyint,
	@SrvId int
)
AS
	SET NOCOUNT OFF;
	
	IF EXISTS (SELECT PortID FROM port WHERE (PortID = @PortID))
		UPDATE [port] SET [PortXMLID] = @PortXMLID, [PortName] = @PortName, [PortType] = @PortType, [Parameters] = @Parameters, [Status] = @Status, [Removed] = @Removed, [SrvId] = @SrvId WHERE ([PortID] = @PortID);
	ELSE
		INSERT INTO [port] ([PortID],[PortXMLID], [PortName], [PortType], [Parameters], [Status], [Removed], [SrvId]) VALUES (@PortID, @PortXMLID, @PortName, @PortType, @Parameters, @Status, @Removed, @SrvId);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[devices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[devices](
	[DevID] [bigint] NOT NULL,
	[NodID] [bigint] NULL,
	[SrvID] [int] NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Type] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[SN] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[Addr] [varchar](32) COLLATE Latin1_General_CI_AS NULL,
	[PortId] [uniqueidentifier] NULL,
	[ProfileID] [int] NULL,
	[Active] [tinyint] NULL,
	[Scheduled] [tinyint] NULL,
	[Removed] [tinyint] NULL,
	[RackID] [uniqueidentifier] NULL,
	[RackPositionRow] [int] NULL,
	[RackPositionCol] [int] NULL,
	[DefinitionVersion] [varchar](8) COLLATE Latin1_General_CI_AS NULL,
	[ProtocolDefinitionVersion] [varchar](8) COLLATE Latin1_General_CI_AS NULL,
    [DiscoveredType] [varchar](16) COLLATE Latin1_General_CI_AS NULL,
	[MonitoringDeviceID] [varchar](128) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_devices] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[device_status]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[device_status](
	[DevID] [bigint] NOT NULL,
	[SevLevel] [int] NULL,
	[Description] [varchar](256) COLLATE Latin1_General_CI_AS NULL,
	[Offline] [tinyint] NULL,
	[AckFlag] [bit] NOT NULL,
	[AckDate] [datetime] NULL,
	[ShouldSendNotificationByEmail] [tinyint] NOT NULL,
 CONSTRAINT [PK_device_status] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdDevices]
(
	@DevID bigint,
	@NodID bigint,
	@SrvID int,
	@Name varchar(64),
	@Type varchar(16),
	@SN varchar(16),
	@PortID uniqueidentifier,
	@Addr varchar(32),
	@ProfileID int,
	@Active tinyint,
	@Scheduled tinyint,
	@Removed bit,
	@RackID uniqueidentifier,
	@RackPositionRow int,
	@RackPositionCol int,
	@DefinitionVersion varchar(8),
	@ProtocolDefinitionVersion varchar(8)
	)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT DevID FROM devices WITH(UPDLOCK) WHERE (DevID = @DevID))
		UPDATE [devices] 
		SET [DevID] = @DevID, 
			[NodID] = @NodID, 
			[SrvID] = @SrvID, 
			[Name] = @Name, 
			[Type] = @Type, 
			[SN] = @SN, 
			[PortID] = @PortID, 
			[Addr] = @Addr, 
			[ProfileID] = @ProfileID, 
			[Active] = @Active, 
			[Scheduled] = @Scheduled, 
			Removed = @Removed,
			RackID= @RackID,
			RackPositionRow= @RackPositionRow,
			RackPositionCol= @RackPositionCol,
			DefinitionVersion = @DefinitionVersion,
			ProtocolDefinitionVersion = @ProtocolDefinitionVersion
		WHERE (([DevID] = @DevID));
	ELSE
		INSERT INTO [devices] ([DevID], 
								[NodID], 
								[SrvID], 
								[Name], 
								[Type], 
								[SN], 
								[PortID], 
								[Addr], 
								[ProfileID], 
								[Active], 
								[Scheduled], 
								Removed,
								RackID,
								RackPositionRow,
								RackPositionCol,
								DefinitionVersion,
								ProtocolDefinitionVersion) 
		VALUES (@DevID, 
				@NodID, 
				@SrvID, 
				@Name, 
				@Type, 
				@SN, 
				@PortID, 
				@Addr, 
				@ProfileID, 
				@Active, 
				@Scheduled, 
				@Removed,
				@RackID,
				@RackPositionRow,
				@RackPositionCol,
				@DefinitionVersion,
				@ProtocolDefinitionVersion);' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetSTLCParameters]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetSTLCParameters]
AS
	DECLARE @SrvID BIGINT;
	SET @SrvID = (SELECT SrvID FROM Servers WHERE (SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed, 0) = 0)) AND (ISNULL([servers].Removed, 0) = 0))

	IF NOT @SrvID IS NULL
		SELECT @SrvID AS SrvID, ParameterName, ParameterValue, ParameterDescription
		FROM stlc_parameters
	ELSE
		RAISERROR (''SrvID not found.'',16 /*Severity*/,1 /*State*/);

RETURN 0;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDevices_List]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDevices_List]
(
	@ServerName varchar(64),
	@ServerHost varchar(64),
	@NodeName varchar(64),	   -- Stazioni
	@ZoneName varchar(64),	   -- Linea
	@RegionName varchar(64),   -- Compartimenti
	@DeviceName varchar(64),
	@DevID bigint
)
AS
SET NOCOUNT ON;

SELECT     servers.Name AS ServerName, servers.Host AS ServerHost, nodes.Name AS NodeName, zones.Name AS ZoneName, regions.Name AS RegionName, 
                      devices.Name, devices.Type, devices.SN, devices.PortID, devices.Addr, devices.ProfileID, devices.Active, devices.Scheduled, devices.DevID, 
                      nodes.NodID, zones.ZonID, regions.RegID, servers.SrvID
FROM         devices INNER JOIN
                      nodes ON devices.NodID = nodes.NodID INNER JOIN
                      servers ON devices.SrvID = servers.SrvID INNER JOIN
                      zones ON nodes.ZonID = zones.ZonID INNER JOIN
                      regions ON zones.RegID = regions.RegID
WHERE (ISNULL(@ServerName,'''') = '''' OR  servers.Name LIKE @ServerName)
AND (ISNULL(@ServerHost,'''') = '''' OR servers.Host LIKE @ServerHost) 
AND (ISNULL(@NodeName,'''') = '''' OR nodes.Name LIKE @NodeName) 
AND (ISNULL(@ZoneName,'''')= '''' OR zones.Name LIKE @ZoneName) 
AND (ISNULL(@RegionName,'''') = '''' OR regions.Name LIKE @RegionName) 
AND (ISNULL(@DeviceName,'''') = '''' OR devices.Name LIKE @DeviceName) 
AND (ISNULL(@DevID,0) =0 OR devices.DevID = @DevID)
AND	ISNULL(devices.Removed,0) = 0' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDevices]
AS
	SET NOCOUNT ON;
	SELECT DevID, NodID, SrvID, Name, COALESCE(DiscoveredType, [Type]) AS [Type], SN, PortID, Addr, ProfileID, Active, Scheduled, Removed, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion
	FROM devices
	WHERE ISNULL(Removed,0) = 0' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelStation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelStation]
(
	@Original_StationID uniqueidentifier
)
AS
	SET NOCOUNT ON; -- se off e non c''è la riga ADO.NET da errore

	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE [RackID] IN (SELECT RackID FROM [rack] INNER JOIN [building] ON [rack].[BuildingID] = [building].[BuildingID]
					   WHERE ([StationID] = @Original_StationID))
	DELETE [rack] 
		FROM [rack] INNER JOIN [building] ON [rack].[BuildingID] = [building].[BuildingID] 
		WHERE ([StationID] = @Original_StationID)
	DELETE FROM [building] WHERE ([StationID] = @Original_StationID)
	DELETE FROM [station] WHERE ([StationID] = @Original_StationID)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServers]
AS
SET NOCOUNT ON;

	SELECT
		SrvID, [Name], Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0)
	AND (ISNULL([servers].Removed, 0) = 0)
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServerByMAC]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServerByMAC]
( @MAC varchar(16))
AS
	SET NOCOUNT ON;
	SELECT
		SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE ISNULL(MAC,'''') = @MAC
		AND (SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0))
		AND (ISNULL([servers].Removed, 0) = 0)
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServerByIP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServerByIP]
( @IP varchar(16))
AS
	SET NOCOUNT ON;
	SELECT
		SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE ISNULL(IP,'''') = @IP
		AND (SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0))
		AND (ISNULL([servers].Removed, 0) = 0)
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServerById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServerById]
( @SrvID int)
AS
	SET NOCOUNT ON;
	SELECT SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID = @SrvID
		AND SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0)
		AND (ISNULL([servers].Removed, 0) = 0)
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procedures]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[procedures](
	[DevID] [bigint] NOT NULL,
	[ProID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[ExeCount] [tinyint] NULL,
	[LastExecution] [datetime] NULL,
	[InProgress] [tinyint] NULL,
 CONSTRAINT [PK_procedures] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC,
	[ProID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[events](
	[EventID] [uniqueidentifier] NOT NULL,
	[DevID] [bigint] NOT NULL,
	[EventData] [varbinary](max) NOT NULL,
	[Created] [datetime] NULL,
	[Requested] [datetime] NOT NULL,
	[ToBeDeleted] [bit] NOT NULL,
	[EventCategory] [tinyint] NOT NULL,
 CONSTRAINT [PK_events] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetServer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetServer]

AS
SELECT 
		OutServers.SrvID, OutServers.Name, OutServers.Host, OutServers.FullHostName, OutServers.IP, OutServers.LastUpdate, 
		OutServers.LastMessageType, OutServers.SupervisorSystemXML, OutServers.ClientSupervisorSystemXMLValidated, 
		OutServers.ClientValidationSign, OutServers.ClientDateValidationRequested, OutServers.ClientDateValidationObtained, OutServers.ClientKey
	FROM servers OutServers
	WHERE EXISTS
	(
		SELECT *
		FROM servers InnServers
		INNER JOIN devices ON InnServers.SrvID = devices.SrvID
		WHERE InnServers.SrvID = OutServers.SrvID
	);

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelDeviceAckByServer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelDeviceAckByServer] (@Original_SrvID int, @Original_MAC varchar(16)) AS
SET NOCOUNT ON;
DECLARE @OriginalSrvID int

SET @Original_MAC = RTRIM(ISNULL(@Original_MAC,''''))

IF @Original_MAC <> '''' -- Se cancello via MAC cancello il SrvId associato al MAC selezionato ignorando il SrvID passato
BEGIN
	SET @Original_SrvID = NULL;
	SELECT @Original_SrvID = SrvID FROM servers WHERE (MAC = @Original_MAC);
END

DELETE FROM device_ack
WHERE (DevID IN (SELECT DevID FROM devices WHERE SrvID = @Original_SrvID));
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelBuilding]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelBuilding]
(
	@Original_BuildingID uniqueidentifier
)
AS
	SET NOCOUNT ON;

	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE [RackID] IN (SELECT RackID FROM [rack] WHERE ([BuildingID] = @Original_BuildingID))
	DELETE FROM [rack] WHERE (([BuildingID] = @Original_BuildingID))
	DELETE FROM [building] WHERE (([BuildingID] = @Original_BuildingID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelRack]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelRack]
(
	@Original_RackID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE ([RackID] = @Original_RackID)
	DELETE FROM [rack] WHERE ([RackID] = @Original_RackID)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelPort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelPort]
(
	@Original_PortID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	
	UPDATE Devices SET PortID = Null WHERE (([PortID] = @Original_PortID))
	DELETE FROM [port] WHERE (([PortID] = @Original_PortID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelEvents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelEvents]
@Original_EventID UNIQUEIDENTIFIER
AS
DELETE FROM [events] WHERE ([EventID] = @Original_EventID);
   RETURN @@ROWCOUNT;

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[streams]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[streams](
	[DevID] [bigint] NOT NULL,
	[StrID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[Visible] [tinyint] NULL,
	[Data] [image] NULL,
	[DateTime] [datetime] NOT NULL,
	[SevLevel] [int] NULL,
	[Description] [text] COLLATE Latin1_General_CI_AS NULL,
	[Processed] [tinyint] NULL,
 CONSTRAINT [PK_streams] PRIMARY KEY CLUSTERED 
(
	[DevID] ASC,
	[StrID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetEventsToSend]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- Stored procedure
CREATE PROCEDURE [dbo].[tf_GetEventsToSend]
AS
SET NOCOUNT ON;
SELECT TOP (1000) EventID, DevID, EventData, Created, Requested, ToBeDeleted, EventCategory
FROM events
ORDER BY Created' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDeviceStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDeviceStatus]
AS
SET NOCOUNT ON;
SELECT device_status.DevID, device_status.SevLevel, device_status.Description, device_status.Offline, 0 as IsDeleted, device_status.ShouldSendNotificationByEmail
FROM device_status
INNER JOIN devices ON device_status.DevID = devices.DevID
WHERE device_status.DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0))'
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStreams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetStreams]
AS
	SET NOCOUNT ON;
	SELECT DevID, StrID, [Name], [DateTime], SevLevel, Visible, 0 as IsDeleted, Data	
	FROM streams
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stream_fields]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[stream_fields](
	[DevID] [bigint] NOT NULL,
	[StrID] [int] NOT NULL,
	[FieldID] [int] NOT NULL,
	[ArrayID] [int] NOT NULL,
	[Name] [varchar](64) COLLATE Latin1_General_CI_AS NULL,
	[SevLevel] [int] NULL,
	[Value] [varchar](1024) COLLATE Latin1_General_CI_AS NULL,
	[Description] [text] COLLATE Latin1_General_CI_AS NULL,
	[Visible] [tinyint] NULL,
	[ReferenceID] [uniqueidentifier] NULL,
	[AckFlag] [bit] NOT NULL,
	[AckDate] [datetime] NULL,
    [ShouldSendNotificationByEmail] [tinyint] NOT NULL,
 CONSTRAINT [PK_stream_fields_1] PRIMARY KEY CLUSTERED 
(
	[FieldID] ASC,
	[ArrayID] ASC,
	[StrID] ASC,
	[DevID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelReference]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelReference]
(
	@Original_ReferenceID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	
	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE ReferenceId =@Original_ReferenceID;
	
	DELETE FROM [reference] WHERE (([ReferenceID] = @Original_ReferenceID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelDeviceStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_DelDeviceStatus]    Script Date: 11/20/2006 18:18:33 ******/
CREATE PROCEDURE [dbo].[tf_DelDeviceStatus]
(
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (	SELECT reference.ReferenceID FROM [reference] 
							INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
							WHERE ([DevID] = @Original_DevID) )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] = @Original_DevID)

	DELETE FROM [stream_fields] WHERE ([DevID] = @Original_DevID)
	DELETE FROM [streams] WHERE ([DevID] = @Original_DevID)
	DELETE FROM [device_status] WHERE ([DevID] = @Original_DevID)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelDevices]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelDevices]
(
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	UPDATE [devices] WITH (ROWLOCK, XLOCK) SET Removed = 1 WHERE [DevID] = @Original_DevID

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (	SELECT reference.ReferenceID FROM [reference] 
								INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE ([DevID] = @Original_DevID) )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] = @Original_DevID)
	
	DELETE FROM [stream_fields] WHERE [DevID] = @Original_DevID
	DELETE FROM [streams]		WHERE [DevID] = @Original_DevID
	DELETE FROM [device_status] WHERE [DevID] = @Original_DevID
	DELETE FROM [devices]		WHERE [DevID] = @Original_DevID' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelServers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelServers]
(
	@Original_SrvID int,
	@Original_MAC varchar(16)
)
AS
	SET NOCOUNT ON; -- se off e non c''è la riga ADO.NET da errore
	DECLARE @OriginalSrvID int

	SET @Original_MAC = RTRIM(ISNULL(@Original_MAC,''''))

	IF @Original_MAC <> '''' -- Se cancello via MAC cancello il SrvId associato al MAC selezionato ignorando il SrvID passato
		BEGIN
			SET @Original_SrvID = NULL
			SELECT @Original_SrvID = SrvID FROM servers WHERE (MAC = @Original_MAC)
		END

	UPDATE [reference] 
	SET [DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))
	
	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN ( 
		SELECT Reference.ReferenceID
		FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
		WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	)

	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))

	DELETE FROM [stream_fields] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [streams]		WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	
	DELETE FROM [device_status] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID AND [Type] NOT LIKE ''STLC1000'')
	DELETE FROM [devices]		WHERE [SrvID] = @Original_SrvID AND [Type] NOT LIKE ''STLC1000''	

	--DELETE FROM [stlc_parameters] WHERE [SrvID] = @Original_SrvID

	UPDATE [devices] SET PortId = null WHERE PortID in (SELECT PortId FROM [port] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [port]			WHERE [SrvID] = @Original_SrvID' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelStreams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  StoredProcedure [dbo].[tf_DelStreams]    Script Date: 11/20/2006 18:18:55 ******/
CREATE PROCEDURE [dbo].[tf_DelStreams]
(
	@Original_DevID bigint,
	@Original_StrID int
)
AS
	SET NOCOUNT ON;

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaStrID] = @Original_StrID) 
			AND ([DeltaDevID] = @Original_DevID)

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (SELECT reference.ReferenceID FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
							WHERE ([StrID] = @Original_StrID) 
							  AND ([DevID] = @Original_DevID)
	)
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([StrID] = @Original_StrID) 
	  AND ([DevID] = @Original_DevID)

	DELETE FROM [stream_fields] WHERE (([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID))
	DELETE FROM [streams] WHERE (([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_DelStream_Fields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_DelStream_Fields]
(
	@Original_FieldID int,
	@Original_ArrayID int,
	@Original_StrID int,
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	
	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE (([DeltaFieldID] = @Original_FieldID) 
			AND ([DeltaArrayID] = @Original_ArrayID) 
			AND ([DeltaStrID] = @Original_StrID) 
			AND ([DeltaDevID] = @Original_DevID))

	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE [ReferenceId] IN  (SELECT [reference].[ReferenceId]	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE (([FieldID] = @Original_FieldID) 
									AND ([ArrayID] = @Original_ArrayID) 
									AND ([StrID] = @Original_StrID) 
									AND ([DevID] = @Original_DevID))
							 )
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE (([FieldID] = @Original_FieldID) 
		AND ([ArrayID] = @Original_ArrayID) 
		AND ([StrID] = @Original_StrID) 
		AND ([DevID] = @Original_DevID))
	
	DELETE FROM [stream_fields] 
	WHERE (([FieldID] = @Original_FieldID) 
		AND ([ArrayID] = @Original_ArrayID) 
		AND ([StrID] = @Original_StrID) 
		AND ([DevID] = @Original_DevID))' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetReference]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetReference]

AS
SET NOCOUNT ON;

	SELECT ReferenceID, Value, DateTime, Visible, DeltaDevID, DeltaStrID, DeltaFieldID, DeltaArrayID
	FROM reference
	WHERE ReferenceID IN 
		(SELECT DISTINCT ReferenceID 
			FROM Stream_Fields INNER JOIN Devices ON Stream_Fields.DevID = Devices.DevID
			WHERE ISNULL(Removed,0) = 0)

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStream_Fields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetStream_Fields]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID, ShouldSendNotificationByEmail
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[util_EmptyDatabase]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[util_EmptyDatabase]
AS
	DELETE FROM reference;
	DELETE FROM stream_fields;
	DELETE FROM streams;
	DELETE FROM device_status;
	DELETE FROM [events];
	DELETE FROM [procedures];
	DELETE FROM devices;
	DELETE FROM port;
	DELETE FROM [servers];
	DELETE FROM nodes;
	DELETE FROM zones;
	DELETE FROM regions;
	DELETE FROM rack;
	DELETE FROM building;
	DELETE FROM station;
	DELETE FROM device_type;
	DELETE FROM vendors;
	DELETE FROM systems;
	DELETE FROM severity;

	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (-255, ''Informazione'')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (0, ''Informazione'')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (1, ''Avviso'')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (2, ''Errore'')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (255, ''Sconosciuta'')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (9, ''Da configurare'')

	INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription]) VALUES (1, ''Produttore di default'', NULL)

	INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription]) VALUES (1, ''Sistema di Default'')
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[util_DelUnusedRows]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[util_DelUnusedRows]
AS
	DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);
	DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM devices WHERE (devices.Removed = 1);

	DELETE FROM nodes WHERE Removed = 1;
	DELETE FROM zones WHERE Removed = 1;
	DELETE FROM regions WHERE Removed = 1;

	DELETE FROM rack WHERE Removed = 1;
	DELETE FROM building WHERE Removed = 1;
	DELETE FROM station WHERE Removed = 1;
	DELETE FROM port WHERE Removed = 1;

	-- Esiste il caso in cui ci siano periferiche non rimosse (perché gestite da Supervisor SNMP), agganciate ad un server rimosso
	-- Avviene quando si spostano le periferiche su System.xml da un ServerID ad un altro e si riavvia solamente il Supervisor Seriale:
	-- il Supervisor SNMP non avrà riletto il System.xml, quindi continuerà a salvarle con lo stesso server ID, che quindi non risulterà
	-- rimovibile per errori di integrità referenziale. In questo caso occorre cancellare le periferiche dall''alto, partendo dal server
	-- marcato come rimosso ed eliminando tutti gli oggetti che referenzia. Questo problema si manifesta solamente se si sovrappongono
	-- i due supervisori.

	DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);
	DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM devices WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM port WHERE SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1);

	DELETE FROM servers WHERE Removed = 1;
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdStream_Fields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdStream_Fields]
(
	@FieldID int,
	@ArrayID int,
	@StrID int,
	@DevID bigint,
	@Name varchar(64),
	@SevLevel int,
	@Value varchar(1024),
	@Description text,
	@Visible tinyint,
	@IsDeleted bit,
	@ReferenceID uniqueidentifier,
	@ShouldSendNotificationByEmail tinyint = 0
)
AS
SET NOCOUNT OFF;

IF EXISTS (SELECT FieldID FROM stream_fields WITH(UPDLOCK) WHERE (ArrayID = @ArrayID) AND (DevID = @DevID) AND (FieldID = @FieldID) AND (StrID = @StrID))
BEGIN
	IF @IsDeleted = 0
	BEGIN
		UPDATE [stream_fields]
		SET
		[Name] = @Name,
		[SevLevel] = @SevLevel,
		[Value] = @Value,
		[Description] = @Description,
		[Visible] = @Visible,
		[ReferenceID] = @ReferenceID,
		[ShouldSendNotificationByEmail] = ISNULL(@ShouldSendNotificationByEmail, 0)
		WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
	END
	ELSE
	BEGIN
		DELETE FROM [stream_fields]
		WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
	END
END
ELSE
BEGIN
	INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], [ReferenceID], [ShouldSendNotificationByEmail])
	VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, @Visible, @ReferenceID, ISNULL(@ShouldSendNotificationByEmail, 0));
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdStreams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdStreams]
(
	@DevID bigint,
	@StrID int,
	@Name varchar(64),
	@DateTime datetime,
	@SevLevel int,
	@Visible tinyint,
	@IsDeleted bit,
	@Data image = null
)
AS
	SET NOCOUNT OFF;
	
IF EXISTS( SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID))
	IF @IsDeleted = 0
		UPDATE [streams] SET [DevID] = @DevID, [StrID] = @StrID, [Name] = @Name, [DateTime] = @DateTime, [SevLevel] = @SevLevel, [Visible] = @Visible, [Data] = @Data  WHERE (([DevID] = @DevID) AND ([StrID] = @StrID));
	ELSE
		EXEC [tf_DelStreams] @DevID, @StrID 
ELSE
	INSERT INTO [streams] ([DevID], [StrID], [Name], [DateTime], [SevLevel], [Visible], [Data]) VALUES (@DevID, @StrID, @Name, @DateTime, @SevLevel, @Visible, @Data)' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_UpdDeviceStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_UpdDeviceStatus]
(
	@DevID bigint,
	@SevLevel int,
	@Description varchar(256),
	@Offline tinyint,
	@IsDeleted bit,
	@ShouldSendNotificationByEmail tinyint = 0
)
AS
SET NOCOUNT OFF;

IF EXISTS (SELECT [DevID] FROM [device_status] WHERE [DevID] = @DevID)
BEGIN
	IF @IsDeleted = 0
	BEGIN
		UPDATE [device_status] 
		SET
		[SevLevel] = @SevLevel,
		[Description] = @Description,
		[Offline] = @Offline,
		[ShouldSendNotificationByEmail] = ISNULL(@ShouldSendNotificationByEmail, 0)
		WHERE [DevID] = @DevID;
	END
	ELSE
	BEGIN
		EXEC [tf_DelDeviceStatus] @DevID;
	END
END
ELSE
BEGIN
	INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline], [ShouldSendNotificationByEmail]) 
	VALUES (@DevID, @SevLevel, @Description, @Offline, ISNULL(@ShouldSendNotificationByEmail, 0));
END' 
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sWEB_GetServer_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sWEB_GetServer_sa]
AS
SELECT 
		OutServers.SrvID, OutServers.Name, OutServers.Host, OutServers.FullHostName, OutServers.IP, OutServers.LastUpdate, 
		OutServers.LastMessageType, OutServers.SupervisorSystemXML, OutServers.ClientSupervisorSystemXMLValidated, 
		OutServers.ClientValidationSign, OutServers.ClientDateValidationRequested, OutServers.ClientDateValidationObtained, OutServers.ClientKey
	FROM servers OutServers
	WHERE EXISTS
	(
		SELECT *
		FROM servers InnServers
		INNER JOIN devices ON InnServers.SrvID = devices.SrvID
		WHERE InnServers.SrvID = OutServers.SrvID
	);'
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDeviceDataByIP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDeviceDataByIP] (@IP VARCHAR(15)) AS
SET NOCOUNT ON;

SELECT
devices.DevID,
devices.Name AS DeviceName,
devices.SN,
devices.Addr,
dbo.GetIPStringFromInt(devices.Addr) AS IP,
COALESCE(devices.DiscoveredType, devices.Type) AS [Type],
devices.MonitoringDeviceID,
nodes.NodID,
nodes.Name AS NodeName,
device_status.SevLevel,
device_status.Description,
device_status.Offline
FROM devices
INNER JOIN device_status ON devices.DevID = device_status.DevID
INNER JOIN nodes ON devices.NodID = nodes.NodID
WHERE (dbo.GetIPStringFromInt(devices.Addr) = @IP)
AND (ISNULL(devices.Removed, 0) = 0)
AND (devices.MonitoringDeviceID IS NOT NULL)'
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDevices_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDevices_sa]
AS
	SET NOCOUNT ON;
	SELECT DevID, NodID, SrvID, Name, COALESCE(DiscoveredType, [Type]) AS [Type], SN, PortID, Addr, ProfileID, Active, Scheduled, Removed, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion, DiscoveredType, MonitoringDeviceID
	FROM devices
	WHERE ISNULL(Removed,0) = 0'
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetDeviceStatus_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetDeviceStatus_sa]
AS
SET NOCOUNT ON;
SELECT device_status.DevID, device_status.SevLevel, device_status.Description, device_status.Offline, 0 as IsDeleted, device_status.ShouldSendNotificationByEmail, devices.Name, devices.MonitoringDeviceID, devices.NodID
FROM device_status
INNER JOIN devices ON device_status.DevID = devices.DevID
WHERE device_status.DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))'
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetReference_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetReference_sa]
AS
SET NOCOUNT ON;

	SELECT ReferenceID, Value, DateTime, Visible, DeltaDevID, DeltaStrID, DeltaFieldID, DeltaArrayID
	FROM reference
	WHERE ReferenceID IN 
		(SELECT DISTINCT ReferenceID 
			FROM Stream_Fields INNER JOIN Devices ON Stream_Fields.DevID = Devices.DevID
			WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))'
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetServers_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetServers_sa]
AS
	SET NOCOUNT ON;

	SELECT
		SrvID, [Name], Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
	AND (ISNULL([servers].Removed, 0) = 0)'
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStream_Fields_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetStream_Fields_sa]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID, ShouldSendNotificationByEmail
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))'
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tf_GetStreams_sa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[tf_GetStreams_sa]
AS
	SET NOCOUNT ON;
	SELECT DevID, StrID, [Name], [DateTime], SevLevel, Visible, 0 as IsDeleted, Data	
	FROM streams
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))'
END
GO

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_station_StationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[station]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_station_StationID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[station] ADD  CONSTRAINT [DF_station_StationID]  DEFAULT (newid()) FOR [StationID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_field_reference_ReferenceID]') AND parent_object_id = OBJECT_ID(N'[dbo].[reference]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_field_reference_ReferenceID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[reference] ADD  CONSTRAINT [DF_field_reference_ReferenceID]  DEFAULT (newid()) FOR [ReferenceID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_reference_DateTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[reference]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_reference_DateTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[reference] ADD  CONSTRAINT [DF_reference_DateTime]  DEFAULT (getdate()) FOR [DateTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_device_ack_DeviceAckID]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_ack]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_device_ack_DeviceAckID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[device_ack] ADD  CONSTRAINT [DF_device_ack_DeviceAckID]  DEFAULT (newid()) FOR [DeviceAckID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_device_ack_AckDurationMinutes]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_ack]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_device_ack_AckDurationMinutes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[device_ack] ADD  CONSTRAINT [DF_device_ack_AckDurationMinutes]  DEFAULT ((4320)) FOR [AckDurationMinutes]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_device_ack_SupervisorID]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_ack]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_device_ack_SupervisorID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[device_ack] ADD  CONSTRAINT [DF_device_ack_SupervisorID]  DEFAULT ((0)) FOR [SupervisorID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_building_BuildingID]') AND parent_object_id = OBJECT_ID(N'[dbo].[building]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_building_BuildingID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[building] ADD  CONSTRAINT [DF_building_BuildingID]  DEFAULT (newid()) FOR [BuildingID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_rack_RackID]') AND parent_object_id = OBJECT_ID(N'[dbo].[rack]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_rack_RackID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rack] ADD  CONSTRAINT [DF_rack_RackID]  DEFAULT (newid()) FOR [RackID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_port_PortID]') AND parent_object_id = OBJECT_ID(N'[dbo].[port]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_port_PortID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[port] ADD  CONSTRAINT [DF_port_PortID]  DEFAULT (newid()) FOR [PortID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_device_status_AckFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_device_status_AckFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[device_status] ADD  CONSTRAINT [DF_device_status_AckFlag]  DEFAULT ((0)) FOR [AckFlag]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_device_status_ShouldSendNotificationByEmail]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_device_status_ShouldSendNotificationByEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[device_status] ADD  CONSTRAINT [DF_device_status_ShouldSendNotificationByEmail]  DEFAULT ((0)) FOR [ShouldSendNotificationByEmail]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_events_EventID]') AND parent_object_id = OBJECT_ID(N'[dbo].[events]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_events_EventID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[events] ADD  CONSTRAINT [DF_events_EventID]  DEFAULT (newid()) FOR [EventID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_events_Created]') AND parent_object_id = OBJECT_ID(N'[dbo].[events]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_events_Created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[events] ADD  CONSTRAINT [DF_events_Created]  DEFAULT (getdate()) FOR [Created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_events_Requested]') AND parent_object_id = OBJECT_ID(N'[dbo].[events]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_events_Requested]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[events] ADD  CONSTRAINT [DF_events_Requested]  DEFAULT (getdate()) FOR [Requested]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_events_ToBeDeleted]') AND parent_object_id = OBJECT_ID(N'[dbo].[events]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_events_ToBeDeleted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[events] ADD  CONSTRAINT [DF_events_ToBeDeleted]  DEFAULT ((0)) FOR [ToBeDeleted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_streams_DateTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_streams_DateTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[streams] ADD  CONSTRAINT [DF_streams_DateTime]  DEFAULT (getdate()) FOR [DateTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stream_fields_AckFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stream_fields_AckFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stream_fields] ADD  CONSTRAINT [DF_stream_fields_AckFlag]  DEFAULT ((0)) FOR [AckFlag]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_stream_fields_ShouldSendNotificationByEmail]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_stream_fields_ShouldSendNotificationByEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stream_fields] ADD  CONSTRAINT [DF_stream_fields_ShouldSendNotificationByEmail]  DEFAULT ((0)) FOR [ShouldSendNotificationByEmail]
END


End
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_device_ack_SupervisorID]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_ack]'))
ALTER TABLE [dbo].[device_ack]  WITH CHECK ADD  CONSTRAINT [CK_device_ack_SupervisorID] CHECK  (([SupervisorID]=(1) OR [SupervisorID]=(0)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_device_ack_SupervisorID]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_ack]'))
ALTER TABLE [dbo].[device_ack] CHECK CONSTRAINT [CK_device_ack_SupervisorID]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'device_ack', N'CONSTRAINT',N'CK_device_ack_SupervisorID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SupervisorID = 0 - Supervisore Seriale
SupervisorID = 1 - Supervisore SNMP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'device_ack', @level2type=N'CONSTRAINT',@level2name=N'CK_device_ack_SupervisorID'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_building_station]') AND parent_object_id = OBJECT_ID(N'[dbo].[building]'))
ALTER TABLE [dbo].[building]  WITH CHECK ADD  CONSTRAINT [FK_building_station] FOREIGN KEY([StationID])
REFERENCES [dbo].[station] ([StationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_building_station]') AND parent_object_id = OBJECT_ID(N'[dbo].[building]'))
ALTER TABLE [dbo].[building] CHECK CONSTRAINT [FK_building_station]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_type_systems]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_type]'))
ALTER TABLE [dbo].[device_type]  WITH CHECK ADD  CONSTRAINT [FK_device_type_systems] FOREIGN KEY([SystemID])
REFERENCES [dbo].[systems] ([SystemID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_type_systems]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_type]'))
ALTER TABLE [dbo].[device_type] CHECK CONSTRAINT [FK_device_type_systems]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_type_vendors]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_type]'))
ALTER TABLE [dbo].[device_type]  WITH CHECK ADD  CONSTRAINT [FK_device_type_vendors] FOREIGN KEY([VendorID])
REFERENCES [dbo].[vendors] ([VendorID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_type_vendors]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_type]'))
ALTER TABLE [dbo].[device_type] CHECK CONSTRAINT [FK_device_type_vendors]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_zones_regions]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones]  WITH CHECK ADD  CONSTRAINT [FK_zones_regions] FOREIGN KEY([RegID])
REFERENCES [dbo].[regions] ([RegID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_zones_regions]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] CHECK CONSTRAINT [FK_zones_regions]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_nodes_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[nodes]'))
ALTER TABLE [dbo].[nodes]  WITH CHECK ADD  CONSTRAINT [FK_nodes_zones] FOREIGN KEY([ZonID])
REFERENCES [dbo].[zones] ([ZonID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_nodes_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[nodes]'))
ALTER TABLE [dbo].[nodes] CHECK CONSTRAINT [FK_nodes_zones]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_rack_building]') AND parent_object_id = OBJECT_ID(N'[dbo].[rack]'))
ALTER TABLE [dbo].[rack]  WITH CHECK ADD  CONSTRAINT [FK_rack_building] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[building] ([BuildingID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_rack_building]') AND parent_object_id = OBJECT_ID(N'[dbo].[rack]'))
ALTER TABLE [dbo].[rack] CHECK CONSTRAINT [FK_rack_building]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_servers_nodes]') AND parent_object_id = OBJECT_ID(N'[dbo].[servers]'))
ALTER TABLE [dbo].[servers]  WITH CHECK ADD  CONSTRAINT [FK_servers_nodes] FOREIGN KEY([NodID])
REFERENCES [dbo].[nodes] ([NodID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_servers_nodes]') AND parent_object_id = OBJECT_ID(N'[dbo].[servers]'))
ALTER TABLE [dbo].[servers] CHECK CONSTRAINT [FK_servers_nodes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_port_servers]') AND parent_object_id = OBJECT_ID(N'[dbo].[port]'))
ALTER TABLE [dbo].[port]  WITH CHECK ADD  CONSTRAINT [FK_port_servers] FOREIGN KEY([SrvID])
REFERENCES [dbo].[servers] ([SrvID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_port_servers]') AND parent_object_id = OBJECT_ID(N'[dbo].[port]'))
ALTER TABLE [dbo].[port] CHECK CONSTRAINT [FK_port_servers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_nodes]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_nodes] FOREIGN KEY([NodID])
REFERENCES [dbo].[nodes] ([NodID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_nodes]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_nodes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_port]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_port] FOREIGN KEY([PortId])
REFERENCES [dbo].[port] ([PortID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_port]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_port]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_rack]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_rack] FOREIGN KEY([RackID])
REFERENCES [dbo].[rack] ([RackID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_rack]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_rack]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_servers]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices]  WITH CHECK ADD  CONSTRAINT [FK_devices_servers] FOREIGN KEY([SrvID])
REFERENCES [dbo].[servers] ([SrvID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_devices_servers]') AND parent_object_id = OBJECT_ID(N'[dbo].[devices]'))
ALTER TABLE [dbo].[devices] CHECK CONSTRAINT [FK_devices_servers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_status_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
ALTER TABLE [dbo].[device_status]  WITH CHECK ADD  CONSTRAINT [FK_device_status_devices] FOREIGN KEY([DevID])
REFERENCES [dbo].[devices] ([DevID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_status_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
ALTER TABLE [dbo].[device_status] CHECK CONSTRAINT [FK_device_status_devices]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_status_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
ALTER TABLE [dbo].[device_status]  WITH CHECK ADD  CONSTRAINT [FK_device_status_severity] FOREIGN KEY([SevLevel])
REFERENCES [dbo].[severity] ([SevLevel])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_device_status_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[device_status]'))
ALTER TABLE [dbo].[device_status] CHECK CONSTRAINT [FK_device_status_severity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_procedures_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[procedures]'))
ALTER TABLE [dbo].[procedures]  WITH CHECK ADD  CONSTRAINT [FK_procedures_devices] FOREIGN KEY([DevID])
REFERENCES [dbo].[devices] ([DevID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_procedures_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[procedures]'))
ALTER TABLE [dbo].[procedures] CHECK CONSTRAINT [FK_procedures_devices]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_events_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[events]'))
ALTER TABLE [dbo].[events]  WITH CHECK ADD  CONSTRAINT [FK_events_devices] FOREIGN KEY([DevID])
REFERENCES [dbo].[devices] ([DevID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_events_devices]') AND parent_object_id = OBJECT_ID(N'[dbo].[events]'))
ALTER TABLE [dbo].[events] CHECK CONSTRAINT [FK_events_devices]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_streams_device_status]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
ALTER TABLE [dbo].[streams]  WITH CHECK ADD  CONSTRAINT [FK_streams_device_status] FOREIGN KEY([DevID])
REFERENCES [dbo].[device_status] ([DevID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_streams_device_status]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
ALTER TABLE [dbo].[streams] CHECK CONSTRAINT [FK_streams_device_status]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_streams_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
ALTER TABLE [dbo].[streams]  WITH CHECK ADD  CONSTRAINT [FK_streams_severity] FOREIGN KEY([SevLevel])
REFERENCES [dbo].[severity] ([SevLevel])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_streams_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[streams]'))
ALTER TABLE [dbo].[streams] CHECK CONSTRAINT [FK_streams_severity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_reference]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields]  WITH CHECK ADD  CONSTRAINT [FK_stream_fields_reference] FOREIGN KEY([ReferenceID])
REFERENCES [dbo].[reference] ([ReferenceID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_reference]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields] CHECK CONSTRAINT [FK_stream_fields_reference]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields]  WITH CHECK ADD  CONSTRAINT [FK_stream_fields_severity] FOREIGN KEY([SevLevel])
REFERENCES [dbo].[severity] ([SevLevel])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_severity]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields] CHECK CONSTRAINT [FK_stream_fields_severity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_streams]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields]  WITH CHECK ADD  CONSTRAINT [FK_stream_fields_streams] FOREIGN KEY([DevID], [StrID])
REFERENCES [dbo].[streams] ([DevID], [StrID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stream_fields_streams]') AND parent_object_id = OBJECT_ID(N'[dbo].[stream_fields]'))
ALTER TABLE [dbo].[stream_fields] CHECK CONSTRAINT [FK_stream_fields_streams]
GO

INSERT INTO [dbo].[stlc_parameters] ([ParameterName], [ParameterValue], [ParameterDescription]) VALUES ('DBVersion', '2.4.0.0', 'La versione del DataBase')
GO

----------------- severity -----------------------
IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = -255)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (-255, 'Informazione')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Informazione' WHERE [SevLevel] = -255


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = -1)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (-1, 'Non classificato')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Non classificato' WHERE [SevLevel] = -1


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 0)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (0, 'In servizio')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'In servizio' WHERE [SevLevel] = 0


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 1)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (1, 'Anomalia lieve')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Anomalia lieve' WHERE [SevLevel] = 1


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 2)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (2, 'Anomalia grave')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Anomalia grave' WHERE [SevLevel] = 2


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 3)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (3, 'Offline')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Offline' WHERE [SevLevel] = 3


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 9)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (9, 'Non attivo')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Non attivo' WHERE [SevLevel] = 9


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 255)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description])
     VALUES (255, 'Sconosciuto')
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Sconosciuto' WHERE [SevLevel] = 255
----------------- END severity -----------------------

PRINT 'Database created'
GO
﻿CREATE TABLE [dbo].[nodes]
(
[NodID] [bigint] NOT NULL,
[ZonID] [bigint] NOT NULL,
[Name] [varchar] (64) NULL,
[Removed] [tinyint] NULL,
[Meters] [int] NULL
)



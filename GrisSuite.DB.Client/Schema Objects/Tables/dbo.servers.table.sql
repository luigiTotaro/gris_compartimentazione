﻿CREATE TABLE [dbo].[servers]
(
[SrvID] [int] NOT NULL,
[Name] [varchar] (64)  NULL,
[Host] [varchar] (64)  NULL,
[FullHostName] [varchar] (256)  NULL,
[IP] [varchar] (16)  NULL,
[LastUpdate] [datetime] NULL,
[LastMessageType] [varchar] (64)  NULL,
[SupervisorSystemXML] [xml] NULL,
[ClientSupervisorSystemXMLValidated] [xml] NULL,
[ClientValidationSign] [varbinary] (128) NULL,
[ClientDateValidationRequested] [datetime] NULL,
[ClientDateValidationObtained] [datetime] NULL,
[ClientKey] [varbinary] (148) NULL,
[NodID] [bigint] NULL,
[ServerVersion] [varchar] (32)  NULL,
[MAC] [varchar] (16)  NULL,
[Removed] [tinyint] NULL
);



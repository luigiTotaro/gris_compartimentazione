﻿CREATE TABLE [dbo].[device_ack] (
    [DeviceAckID]        UNIQUEIDENTIFIER CONSTRAINT [DF_device_ack_DeviceAckID] DEFAULT (newid()) NOT NULL,
    [DevID]              BIGINT           NOT NULL,
    [StrID]              INT              NULL,
    [FieldID]            INT              NULL,
    [AckDate]            DATETIME         NOT NULL,
    [AckDurationMinutes] INT              CONSTRAINT [DF_device_ack_AckDurationMinutes] DEFAULT ((4320)) NOT NULL,
    [SupervisorID]       TINYINT          CONSTRAINT [DF_device_ack_SupervisorID] DEFAULT ((0)) NOT NULL,
    [Username]           VARCHAR (256)    NOT NULL,
    CONSTRAINT [PK_device_ack_DeviceAckID] PRIMARY KEY CLUSTERED ([DeviceAckID] ASC),
    CONSTRAINT [CK_device_ack_SupervisorID] CHECK ([SupervisorID]=(1) OR [SupervisorID]=(0)),
    CONSTRAINT [IX_device_ack_DevID_StrID_FieldID] UNIQUE NONCLUSTERED ([DevID] ASC, [StrID] ASC, [FieldID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SupervisorID = 0 - Supervisore Seriale
SupervisorID = 1 - Supervisore SNMP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'device_ack', @level2type = N'CONSTRAINT', @level2name = N'CK_device_ack_SupervisorID';


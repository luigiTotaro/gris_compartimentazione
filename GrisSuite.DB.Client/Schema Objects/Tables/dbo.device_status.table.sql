﻿CREATE TABLE [dbo].[device_status] (
    [DevID]                         BIGINT        NOT NULL,
    [SevLevel]                      INT           NULL,
    [Description]                   VARCHAR (256) NULL,
    [Offline]                       TINYINT       NULL,
    [AckFlag]                       BIT           CONSTRAINT [DF_device_status_AckFlag] DEFAULT ((0)) NOT NULL,
    [AckDate]                       DATETIME      NULL,
    [ShouldSendNotificationByEmail] TINYINT       CONSTRAINT [DF_device_status_ShouldSendNotificationByEmail] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_device_status] PRIMARY KEY CLUSTERED ([DevID] ASC),
    CONSTRAINT [FK_device_status_devices] FOREIGN KEY ([DevID]) REFERENCES [dbo].[devices] ([DevID]),
    CONSTRAINT [FK_device_status_severity] FOREIGN KEY ([SevLevel]) REFERENCES [dbo].[severity] ([SevLevel])
);





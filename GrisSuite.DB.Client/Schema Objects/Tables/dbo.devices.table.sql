﻿CREATE TABLE [dbo].[devices] (
    [DevID]                     BIGINT           NOT NULL,
    [NodID]                     BIGINT           NULL,
    [SrvID]                     INT              NULL,
    [Name]                      VARCHAR (64)     COLLATE Latin1_General_CI_AS NULL,
    [Type]                      VARCHAR (16)     COLLATE Latin1_General_CI_AS NULL,
    [SN]                        VARCHAR (16)     COLLATE Latin1_General_CI_AS NULL,
    [Addr]                      VARCHAR (32)     COLLATE Latin1_General_CI_AS NULL,
    [PortId]                    UNIQUEIDENTIFIER NULL,
    [ProfileID]                 INT              NULL,
    [Active]                    TINYINT          NULL,
    [Scheduled]                 TINYINT          NULL,
    [Removed]                   TINYINT          NULL,
    [RackID]                    UNIQUEIDENTIFIER NULL,
    [RackPositionRow]           INT              NULL,
    [RackPositionCol]           INT              NULL,
    [DefinitionVersion]         VARCHAR (8)      COLLATE Latin1_General_CI_AS NULL,
    [ProtocolDefinitionVersion] VARCHAR (8)      COLLATE Latin1_General_CI_AS NULL,
    [DiscoveredType]            VARCHAR (16)     COLLATE Latin1_General_CI_AS NULL,
    [MonitoringDeviceID]        VARCHAR (128)    COLLATE Latin1_General_CI_AS NULL,
    CONSTRAINT [PK_devices] PRIMARY KEY CLUSTERED ([DevID] ASC),
    CONSTRAINT [FK_devices_nodes] FOREIGN KEY ([NodID]) REFERENCES [dbo].[nodes] ([NodID]),
    CONSTRAINT [FK_devices_port] FOREIGN KEY ([PortId]) REFERENCES [dbo].[port] ([PortID]),
    CONSTRAINT [FK_devices_rack] FOREIGN KEY ([RackID]) REFERENCES [dbo].[rack] ([RackID]),
    CONSTRAINT [FK_devices_servers] FOREIGN KEY ([SrvID]) REFERENCES [dbo].[servers] ([SrvID])
);









﻿CREATE TABLE [dbo].[stream_fields] (
    [DevID]                         BIGINT           NOT NULL,
    [StrID]                         INT              NOT NULL,
    [FieldID]                       INT              NOT NULL,
    [ArrayID]                       INT              NOT NULL,
    [Name]                          VARCHAR (64)     NULL,
    [SevLevel]                      INT              NULL,
    [Value]                         VARCHAR (1024)   NULL,
    [Description]                   TEXT             NULL,
    [Visible]                       TINYINT          NULL,
    [ReferenceID]                   UNIQUEIDENTIFIER NULL,
    [AckFlag]                       BIT              CONSTRAINT [DF_stream_fields_AckFlag] DEFAULT ((0)) NOT NULL,
    [AckDate]                       DATETIME         NULL,
    [ShouldSendNotificationByEmail] TINYINT          CONSTRAINT [DF_stream_fields_ShouldSendNotificationByEmail] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_stream_fields_1] PRIMARY KEY CLUSTERED ([FieldID] ASC, [ArrayID] ASC, [StrID] ASC, [DevID] ASC),
    CONSTRAINT [FK_stream_fields_reference] FOREIGN KEY ([ReferenceID]) REFERENCES [dbo].[reference] ([ReferenceID]),
    CONSTRAINT [FK_stream_fields_severity] FOREIGN KEY ([SevLevel]) REFERENCES [dbo].[severity] ([SevLevel]),
    CONSTRAINT [FK_stream_fields_streams] FOREIGN KEY ([DevID], [StrID]) REFERENCES [dbo].[streams] ([DevID], [StrID])
);





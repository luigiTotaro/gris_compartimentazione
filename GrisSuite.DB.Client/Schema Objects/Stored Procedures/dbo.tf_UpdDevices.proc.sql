﻿CREATE PROCEDURE [dbo].[tf_UpdDevices]
(
	@DevID bigint,
	@NodID bigint,
	@SrvID int,
	@Name varchar(64),
	@Type varchar(16),
	@SN varchar(16),
	@PortID uniqueidentifier,
	@Addr varchar(32),
	@ProfileID int,
	@Active tinyint,
	@Scheduled tinyint,
	@Removed bit,
	@RackID uniqueidentifier,
	@RackPositionRow int,
	@RackPositionCol int,
	@DefinitionVersion varchar(8),
	@ProtocolDefinitionVersion varchar(8)
	)
AS
	SET NOCOUNT OFF;

	IF EXISTS(SELECT DevID FROM devices WITH(UPDLOCK) WHERE (DevID = @DevID))
		UPDATE [devices] 
		SET [DevID] = @DevID, 
			[NodID] = @NodID, 
			[SrvID] = @SrvID, 
			[Name] = @Name, 
			[Type] = @Type, 
			[SN] = @SN, 
			[PortID] = @PortID, 
			[Addr] = @Addr, 
			[ProfileID] = @ProfileID, 
			[Active] = @Active, 
			[Scheduled] = @Scheduled, 
			Removed = @Removed,
			RackID= @RackID,
			RackPositionRow= @RackPositionRow,
			RackPositionCol= @RackPositionCol,
			DefinitionVersion = @DefinitionVersion,
			ProtocolDefinitionVersion = @ProtocolDefinitionVersion
		WHERE (([DevID] = @DevID));
	ELSE
		INSERT INTO [devices] ([DevID], 
								[NodID], 
								[SrvID], 
								[Name], 
								[Type], 
								[SN], 
								[PortID], 
								[Addr], 
								[ProfileID], 
								[Active], 
								[Scheduled], 
								Removed,
								RackID,
								RackPositionRow,
								RackPositionCol,
								DefinitionVersion,
								ProtocolDefinitionVersion) 
		VALUES (@DevID, 
				@NodID, 
				@SrvID, 
				@Name, 
				@Type, 
				@SN, 
				@PortID, 
				@Addr, 
				@ProfileID, 
				@Active, 
				@Scheduled, 
				@Removed,
				@RackID,
				@RackPositionRow,
				@RackPositionCol,
				@DefinitionVersion,
				@ProtocolDefinitionVersion);



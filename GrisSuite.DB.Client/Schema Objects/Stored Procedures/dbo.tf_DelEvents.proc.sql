﻿CREATE PROCEDURE [dbo].[tf_DelEvents] 
	@Original_EventID uniqueidentifier 
AS
   DELETE FROM [events] WHERE ([EventID] = @Original_EventID);
   RETURN @@ROWCOUNT;
﻿CREATE PROCEDURE [dbo].[tf_GetServerById]
( @SrvID int)
AS
	SET NOCOUNT ON;
	SELECT SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID = @SrvID
		AND SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE ISNULL(Removed,0) = 0)
		AND (ISNULL([servers].Removed, 0) = 0)



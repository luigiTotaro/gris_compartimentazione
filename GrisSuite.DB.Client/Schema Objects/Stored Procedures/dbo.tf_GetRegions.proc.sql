﻿/****** Object:  StoredProcedure [dbo].[tf_GetRegions]    Script Date: 11/20/2006 18:19:04 ******/
CREATE PROCEDURE [dbo].[tf_GetRegions]
AS
	SET NOCOUNT ON;
	SELECT RegID, Name, Removed
	FROM regions
	WHERE ISNULL(Removed,0) = 0



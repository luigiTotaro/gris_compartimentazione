﻿CREATE PROCEDURE [dbo].[tf_UpdDeviceStatus]
(
	@DevID bigint,
	@SevLevel int,
	@Description varchar(256),
	@Offline tinyint,
	@IsDeleted bit,
	@ShouldSendNotificationByEmail tinyint = 0
)
AS
SET NOCOUNT OFF;

IF EXISTS (SELECT [DevID] FROM [device_status] WHERE [DevID] = @DevID)
BEGIN
	IF @IsDeleted = 0
	BEGIN
		UPDATE [device_status] 
		SET
		[SevLevel] = @SevLevel,
		[Description] = @Description,
		[Offline] = @Offline,
		[ShouldSendNotificationByEmail] = ISNULL(@ShouldSendNotificationByEmail, 0)
		WHERE [DevID] = @DevID;
	END
	ELSE
	BEGIN
		EXEC [tf_DelDeviceStatus] @DevID;
	END
END
ELSE
BEGIN
	INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline], [ShouldSendNotificationByEmail]) 
	VALUES (@DevID, @SevLevel, @Description, @Offline, ISNULL(@ShouldSendNotificationByEmail, 0));
END



﻿CREATE PROCEDURE dbo.tf_DelSTLCParameters
(
	@Original_SrvID int,
	@Original_ParameterName varchar(64)
)
AS
	SET NOCOUNT OFF;
	DELETE FROM [stlc_parameters] WHERE ([ParameterName] = @Original_ParameterName)

﻿CREATE PROCEDURE [dbo].[tf_InsSTLCParameters]
(
	@SrvID int,
	@ParameterValue varchar(256),
	@ParameterName varchar(64),
	@ParameterDescription varchar(1024)
)
AS
	SET NOCOUNT OFF;

	IF EXISTS (SELECT * FROM [stlc_parameters] WHERE ([ParameterName] = @ParameterName))
	BEGIN
		UPDATE [stlc_parameters]
		SET [ParameterValue] = @ParameterValue,
		[ParameterDescription] = @ParameterDescription
		WHERE ([ParameterName] = @ParameterName);		
	END
	ELSE
	BEGIN
		INSERT INTO [stlc_parameters] ([ParameterValue], [ParameterName], [ParameterDescription])
		VALUES (@ParameterValue, @ParameterName, @ParameterDescription);
	END
RETURN 0;
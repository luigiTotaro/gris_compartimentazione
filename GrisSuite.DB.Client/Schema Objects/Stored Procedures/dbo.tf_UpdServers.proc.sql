﻿CREATE PROCEDURE [dbo].[tf_UpdServers] (
@SrvID INT,
@Name VARCHAR (64),
@Host VARCHAR (64),
@FullHostName VARCHAR (256),
@IP VARCHAR (16),
@LastUpdate DATETIME,
@LastMessageType VARCHAR (64),
@SupervisorSystemXML XML,
@ClientSupervisorSystemXMLValidated XML,
@ClientValidationSign VARBINARY (128),
@ClientDateValidationRequested DATETIME,
@ClientDateValidationObtained DATETIME,
@ClientKey VARBINARY (148),
@NodID BIGINT = NULL,
@ServerVersion VARCHAR (32) = NULL,
@MAC VARCHAR (16) = NULL
) AS
	SET NOCOUNT OFF;

	SET @MAC = RTRIM(ISNULL(@MAC,''))

	IF EXISTS (SELECT SrvID FROM servers WHERE ([SrvID] = @SrvID))
		UPDATE [servers] 
			SET [Name] = @Name, [Host] = @Host, [FullHostName] = @FullHostName, 
			[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
			[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
			[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
			[ClientKey] = @ClientKey, [NodID] = @NodID, [ServerVersion] = @ServerVersion, [MAC] = @MAC
		WHERE (([SrvID] = @SrvID));
	ELSE
		INSERT INTO [servers] ([SrvID], [Name], [Host], [FullHostName], [IP], [LastUpdate], [LastMessageType], [SupervisorSystemXML], [ClientSupervisorSystemXMLValidated], [ClientValidationSign], [ClientDateValidationRequested], [ClientDateValidationObtained], [ClientKey], [NodID], [ServerVersion], [MAC]) 
		VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC);
GO
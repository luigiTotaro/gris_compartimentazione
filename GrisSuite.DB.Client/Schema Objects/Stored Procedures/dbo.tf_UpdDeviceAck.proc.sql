﻿CREATE PROCEDURE [dbo].[tf_UpdDeviceAck]
	@Original_DeviceAckID uniqueidentifier,
	@DevID bigint,
	@StrID int, 
	@FieldID int,
	@AckDate datetime, 
	@AckDurationMinutes int,
	@SupervisorID tinyint, 
	@Username nvarchar(255)
AS
UPDATE device_ack
SET DevID = @DevID,
StrID = @StrID,
FieldID = @FieldID,
AckDate = @AckDate,
AckDurationMinutes = @AckDurationMinutes,
SupervisorID = @SupervisorID,
Username = @Username
WHERE (DeviceAckID = @Original_DeviceAckID);

RETURN @@ROWCOUNT;
﻿CREATE PROCEDURE [dbo].[tf_GetDeviceStatus]
AS
SET NOCOUNT ON;
SELECT device_status.DevID, device_status.SevLevel, device_status.Description, device_status.Offline, 0 as IsDeleted, device_status.ShouldSendNotificationByEmail
FROM device_status
INNER JOIN devices ON device_status.DevID = devices.DevID
WHERE device_status.DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0))



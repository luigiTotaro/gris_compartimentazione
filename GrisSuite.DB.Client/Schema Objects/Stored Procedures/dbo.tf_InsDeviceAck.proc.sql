﻿CREATE PROCEDURE [dbo].[tf_InsDeviceAck]
	@DeviceAckID uniqueidentifier,
	@DevID bigint,
	@StrID int, 
	@FieldID int,
	@AckDate datetime, 
	@AckDurationMinutes int,
	@SupervisorID tinyint, 
	@Username nvarchar(255)
AS
	
	IF EXISTS (SELECT DeviceAckID FROM  device_ack WHERE (DeviceAckID = @DeviceAckID))
	BEGIN
		UPDATE device_ack
		SET DevID = @DevID,
		StrID = @StrID,
		FieldID = @FieldID,
		AckDate = @AckDate,
		AckDurationMinutes = @AckDurationMinutes,
		SupervisorID = @SupervisorID,
		Username = @Username
		WHERE (DeviceAckID = @DeviceAckID);
	END
	ELSE
	BEGIN
		INSERT INTO device_ack (DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username)
		VALUES (@DeviceAckID, @DevID, @StrID, @FieldID, @AckDate, @AckDurationMinutes, @SupervisorID, @Username);
	END

RETURN @@ROWCOUNT;
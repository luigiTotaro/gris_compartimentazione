﻿/*CREATE PROCEDURE [dbo].[sWEB_Report_GetConfigurationInUsePorts]
	@SrvID int
AS
	SELECT 
		ResultExt.SupervisorSystemXML.query('.') AS PortNode,
		ResultExt.SupervisorSystemXML.value('@name[1]', 'varchar(64)') AS [Name],
		ResultExt.SupervisorSystemXML.value('@type[1]', 'varchar(64)') AS [Type],
		ResultExt.SupervisorSystemXML.value('@com[1]', 'varchar(8)') AS [Com],
		ResultExt.SupervisorSystemXML.value('@baud[1]', 'varchar(64)') AS [Baud],
		ResultExt.SupervisorSystemXML.value('@echo[1]', 'bit') AS [Echo],
		ResultExt.SupervisorSystemXML.value('@data[1]', 'varchar(8)') AS [Data],
		ResultExt.SupervisorSystemXML.value('@stop[1]', 'varchar(8)') AS [Stop],
		ResultExt.SupervisorSystemXML.value('@parity[1]', 'varchar(8)') AS [Parity],
		ResultExt.SupervisorSystemXML.value('@timeout[1]', 'smallint') AS [Timeout],
		ResultExt.SupervisorSystemXML.value('@persistent[1]', 'bit') AS [Persistent],
		ResultExt.SupervisorSystemXML.value('@ip[1]', 'varchar(64)') AS [IP],
		ResultExt.SupervisorSystemXML.value('@port[1]', 'varchar(64)') AS [Port]
	FROM servers EXT CROSS APPLY SupervisorSystemXML.nodes('//port/item') AS ResultExt(SupervisorSystemXML)
	WHERE (SrvID = @SrvID);*/
﻿CREATE PROCEDURE [dbo].[tf_DelRack]
(
	@Original_RackID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE ([RackID] = @Original_RackID)
	DELETE FROM [rack] WHERE ([RackID] = @Original_RackID)



﻿CREATE PROCEDURE [dbo].[tf_DelReference]
(
	@Original_ReferenceID uniqueidentifier
)
AS
	SET NOCOUNT ON;
	
	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE ReferenceId =@Original_ReferenceID;
	
	DELETE FROM [reference] WHERE (([ReferenceID] = @Original_ReferenceID))



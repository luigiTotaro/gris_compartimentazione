﻿CREATE PROCEDURE [dbo].[tf_UpdServerStatusBySrvID]
(
	@SrvID int,
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64)
)
AS
	SET NOCOUNT OFF;

	DECLARE @ServerCount TINYINT;
	
	SET @ServerCount = (SELECT COUNT(*) FROM [servers] WHERE SrvID = @SrvID);
	
	IF ( @ServerCount = 1 )
	BEGIN
		UPDATE [servers] 
			SET [IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType
		WHERE SrvID = @SrvID;
	END
	
	SELECT @ServerCount;
GO
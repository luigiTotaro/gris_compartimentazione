﻿CREATE PROCEDURE [dbo].[util_DelUnusedRows]
AS
	DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);
	DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));
	DELETE FROM devices WHERE (devices.Removed = 1);

	DELETE FROM nodes WHERE Removed = 1;
	DELETE FROM zones WHERE Removed = 1;
	DELETE FROM regions WHERE Removed = 1;
	
	DELETE FROM rack WHERE Removed = 1;
	DELETE FROM building WHERE Removed = 1;
	DELETE FROM station WHERE Removed = 1;
	DELETE FROM port WHERE Removed = 1;
	
	-- Esiste il caso in cui ci siano periferiche non rimosse (perché gestite da Supervisor SNMP), agganciate ad un server rimosso
	-- Avviene quando si spostano le periferiche su System.xml da un ServerID ad un altro e si riavvia solamente il Supervisor Seriale:
	-- il Supervisor SNMP non avrà riletto il System.xml, quindi continuerà a salvarle con lo stesso server ID, che quindi non risulterà
	-- rimovibile per errori di integrità referenziale. In questo caso occorre cancellare le periferiche dall'alto, partendo dal server
	-- marcato come rimosso ed eliminando tutti gli oggetti che referenzia. Questo problema si manifesta solamente se si sovrappongono
	-- i due supervisori.
	
	DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);
	DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM devices WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));
	DELETE FROM port WHERE SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1);
	
	DELETE FROM servers WHERE Removed = 1;
GO
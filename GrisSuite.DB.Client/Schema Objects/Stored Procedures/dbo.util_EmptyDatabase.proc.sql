﻿CREATE PROCEDURE [dbo].[util_EmptyDatabase]
AS
	DELETE FROM reference;
	DELETE FROM stream_fields;
	DELETE FROM streams;
	DELETE FROM device_status;
	DELETE FROM [events];
	DELETE FROM [procedures];
	DELETE FROM devices;
	DELETE FROM port;
	DELETE FROM [servers];
	DELETE FROM nodes;
	DELETE FROM zones;
	DELETE FROM regions;
	DELETE FROM rack;
	DELETE FROM building;
	DELETE FROM station;
	DELETE FROM device_type;
	DELETE FROM vendors;
	DELETE FROM systems;
	DELETE FROM severity;

	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (-255, 'Informazione')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (0, 'Informazione')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (1, 'Avviso')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (2, 'Errore')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (255, 'Sconosciuta')
	INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (9, 'Da configurare')

	INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription]) VALUES (1, 'Produttore di default', NULL)

	INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription]) VALUES (1, 'Sistema di Default')
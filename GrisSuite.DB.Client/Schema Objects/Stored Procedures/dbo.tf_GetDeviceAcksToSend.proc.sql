﻿CREATE PROCEDURE dbo.tf_GetDeviceAcksToSend AS
SET NOCOUNT ON;
SELECT DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username
FROM device_ack
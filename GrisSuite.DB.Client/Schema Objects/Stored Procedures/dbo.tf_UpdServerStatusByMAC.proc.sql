﻿CREATE PROCEDURE [dbo].[tf_UpdServerStatusByMAC]
(
	@MAC varchar(16),
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64)
)
AS
	SET NOCOUNT OFF;

	DECLARE @ServerCount TINYINT;
	
	SET @ServerCount = (SELECT COUNT(*) FROM [servers] WHERE MAC = @MAC);
	
	IF ( @ServerCount = 1 )
	BEGIN
		UPDATE [servers] 
			SET [IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType
		WHERE ISNULL(MAC,'') = @MAC;
	END
	
	SELECT @ServerCount;



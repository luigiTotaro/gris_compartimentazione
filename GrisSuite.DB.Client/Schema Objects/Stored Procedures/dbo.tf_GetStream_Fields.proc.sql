﻿CREATE PROCEDURE [dbo].[tf_GetStream_Fields]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID, ShouldSendNotificationByEmail
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE ISNULL(Removed,0) = 0) 




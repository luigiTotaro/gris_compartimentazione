﻿CREATE PROCEDURE [dbo].[tf_DelDeviceAck] (@Original_DeviceAckID UNIQUEIDENTIFIER) AS
DELETE FROM device_ack WHERE (DeviceAckID = @Original_DeviceAckID);
RETURN @@ROWCOUNT;
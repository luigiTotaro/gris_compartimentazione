﻿CREATE PROCEDURE [dbo].[tf_GetDeviceAckByDevId]
	@DevID bigint
AS
	SELECT DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username
	FROM device_ack
	WHERE (DevID = @DevID);
RETURN @@ROWCOUNT;
﻿CREATE PROCEDURE [dbo].[tf_UpdReference]
(
	@ReferenceID uniqueidentifier,
	@Value varchar(256),
	@DateTime datetime,
	@Visible tinyint,
	@DeltaDevID bigint,
	@DeltaStrID int,
	@DeltaFieldID int,
	@DeltaArrayID int
)
AS
	SET NOCOUNT OFF;
	IF EXISTS (SELECT ReferenceID FROM reference WHERE (ReferenceID = @ReferenceID))
		UPDATE [reference] SET [Value] = @Value, [DateTime] = @DateTime, [Visible] = @Visible, [DeltaDevID] = @DeltaDevID, [DeltaStrID] = @DeltaStrID, [DeltaFieldID] = @DeltaFieldID, [DeltaArrayID] = @DeltaArrayID WHERE (([ReferenceID] = @ReferenceID));
	ELSE
		INSERT INTO [reference] ([ReferenceID], [Value], [DateTime], [Visible], [DeltaDevID], [DeltaStrID], [DeltaFieldID], [DeltaArrayID]) VALUES (@ReferenceID, @Value, @DateTime, @Visible, @DeltaDevID, @DeltaStrID, @DeltaFieldID, @DeltaArrayID);



﻿CREATE PROCEDURE [dbo].[tf_GetPort]
AS
	SET NOCOUNT ON;
	SELECT PortID, PortXMLID, PortName, PortType, Parameters, Status, Removed, SrvID
	FROM port
	WHERE ISNULL(Removed,0) = 0


﻿CREATE PROCEDURE [dbo].[tf_DelDeviceAckByServer] (@Original_SrvID int, @Original_MAC varchar(16)) AS
SET NOCOUNT ON;
DECLARE @OriginalSrvID int

SET @Original_MAC = RTRIM(ISNULL(@Original_MAC,''))

IF @Original_MAC <> '' -- Se cancello via MAC cancello il SrvId associato al MAC selezionato ignorando il SrvID passato
BEGIN
	SET @Original_SrvID = NULL;
	SELECT @Original_SrvID = SrvID FROM servers WHERE (MAC = @Original_MAC);
END

DELETE FROM device_ack
WHERE (DevID IN (SELECT DevID FROM devices WHERE SrvID = @Original_SrvID));
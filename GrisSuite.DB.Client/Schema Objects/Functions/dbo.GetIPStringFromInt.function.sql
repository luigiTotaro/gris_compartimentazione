﻿CREATE FUNCTION [dbo].[GetIPStringFromInt]
(
	@IpBigIntRepresentation BIGINT
)
RETURNS VARCHAR(15)
AS
BEGIN
	DECLARE @IpVarbinRepresentation AS VARBINARY(16);
	DECLARE @Result AS VARCHAR(15);
	DECLARE @IpHexRepresentation AS CHAR(18); -- 16 + 2 (0x)

	SET @IpVarbinRepresentation = CAST(@IpBigIntRepresentation AS VARBINARY(16));
	EXECUTE @IpHexRepresentation = sys.fn_varbintohexstr @IpVarbinRepresentation;

	SET @Result =	CAST((CAST(dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 11, 2)) AS TINYINT)) AS VARCHAR(3)) + '.' + 
					CAST((CAST(dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 13, 2)) AS TINYINT)) AS VARCHAR(3)) + '.' + 
					CAST((CAST(dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 15, 2)) AS TINYINT)) AS VARCHAR(3)) + '.' + 
					CAST((CAST(dbo.fn_hexstrtovarbin('0x' + SUBSTRING(@IpHexRepresentation, 17, 2)) AS TINYINT)) AS VARCHAR(3));
	
	IF (@@ERROR <> 0)
	BEGIN
		SET @Result = '0.0.0.0';
	END

	RETURN @Result;
END



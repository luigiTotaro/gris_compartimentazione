//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("src\MainFrm.cpp", frmMain);
USEFORM("src\GridLocPositionFrm.cpp", frmGridLocPosition);
USEFORM("src\SaveSplashFrm.cpp", frmSaveSplash);
USEFORM("src\OptionsFrm.cpp", frmOptions);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Telefin STLC1000 Configurator";
         Application->CreateForm(__classid(TfrmMain), &frmMain);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------

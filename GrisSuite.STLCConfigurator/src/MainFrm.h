//---------------------------------------------------------------------------

#ifndef MainFrmH
#define MainFrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>

#include "XMLInterface.h"
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <Graphics.hpp>
#include <Dialogs.hpp>
#include <Grids.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
//---------------------------------------------------------------------------

#define MAX_INTEGER 2147483647 // (2^31) - 1
#define MAX_NUM_TCPIP_PORT 65535 // (2^16) - 1 
#define MAX_LENGTH_NOME 64
#define MAX_LENGTH_NOTE 256
#define MAX_LENGTH_SERIAL_NUMBER 16
#define MAX_NUM_IP 255
//---------------------------------------------------------------------------

class TfrmMain : public TForm
{
__published:	// IDE-managed Components
    TTreeView *XmlTreeView;
    TImageList *ImageList;
    TPageControl *ClientPage;
    TTabSheet *tsItem;
    TTabSheet *tsServer;
    TPopupMenu *XMLTreePopupMenu;
    TMenuItem *miAddItem;
    TMenuItem *miUpdateItem;
    TMenuItem *miDelItem;
    TMenuItem *miUpdateServer;
    TMenuItem *miAddRegion;
    TMenuItem *miAddZone;
    TMenuItem *miUpdateRegion;
    TMenuItem *miDelRegion;
    TMenuItem *miUpdateZone;
    TMenuItem *miDelZone;
    TMenuItem *miAddNode;
    TMenuItem *miUpdateNode;
    TMenuItem *miDelNode;
    TMenuItem *miAddDevice;
    TMenuItem *miUpdateDevice;
    TMenuItem *miDelDevice;
    TTabSheet *tsRegion;
    TEdit *eNameItem;
    TLabel *Nome;
    TEdit *eTimeOutItem;
    TComboBox *cbTypeItem;
    TComboBox *cbCOMItem;
    TComboBox *cbBaudItem;
    TComboBox *cbPersistentItem;
    TComboBox *cbEchoItem;
    TComboBox *cbDataItem;
    TComboBox *cbStopItem;
    TComboBox *cbParityItem;
    TEdit *eIP1Item;
    TEdit *ePortItem;
    TLabel *lblTypeItem;
    TLabel *lblCOM;
    TLabel *lblBaud;
    TLabel *lblEcho;
    TLabel *lblData;
    TLabel *lblStop;
    TLabel *lblParity;
    TLabel *lblIP;
    TLabel *lblPort;
    TLabel *lblTimeOut;
    TLabel *lblPersistent;
    TTabSheet *tsZone;
    TTabSheet *tsNode;
    TTabSheet *tsDevice;
    TTabSheet *tsNull;
    TBitBtn *btnSaveItem;
    TBitBtn *btnCancelItem;
    TActionList *ActionList;
    TAction *actSaveItem;
    TMainMenu *MainMenu;
    TMenuItem *miFile;
    TMenuItem *miOpenXMLSource;
    TMenuItem *miSaveXMLOutputAndRestartService;
    TMenuItem *miClose;
    TMenuItem *miNew;
    TPanel *HeaderPanel;
    TLabel *lblHeader;
    TImage *ImageHeader;
    TLabel *Label2;
    TEdit *eNameServer;
    TLabel *Label3;
    TEdit *eHostServer;
    TBitBtn *btnSaveServer;
    TBitBtn *btnCancelServer;
    TAction *actSaveServer;
    TLabel *Label4;
    TLabel *Label6;
    TBitBtn *btnSaveRegion;
    TBitBtn *btnCancelRegion;
    TAction *actSaveRegion;
    TLabel *Label7;
    TBitBtn *btnSaveZone;
    TBitBtn *btnCancelZone;
    TLabel *Label9;
    TBitBtn *btnSaveNode;
    TBitBtn *btnCancelNode;
    TBitBtn *btnSaveDevice;
    TBitBtn *btnCancelDevice;
    TAction *actSaveZone;
    TAction *actSaveNode;
    TAction *actSaveDevice;
    TEdit *eIP2Item;
    TEdit *eIP3Item;
    TEdit *eIP4Item;
    TOpenDialog *OpenDialog;
    TMenuItem *miOpenXMLBackUp;
    TSaveDialog *SaveDialog;
    TMenuItem *miSaveXMLBackUp;
    TImageList *ImageList48;
    TTabSheet *tsStation;
    TTabSheet *tsBuilding;
    TTabSheet *tsLocation;
    TMenuItem *miUpdateStation;
    TMenuItem *miUpdateBuilding;
    TMenuItem *miAddLocation;
    TMenuItem *miUpdateLocation;
    TMenuItem *miDelLocation;
    TLabel *Label27;
    TEdit *eNameStation;
    TBitBtn *btnSaveStation;
    TBitBtn *btnCancelStation;
    TLabel *Label29;
    TEdit *eNameBuilding;
    TBitBtn *btnSaveBuilding;
    TBitBtn *btnCancelBuilding;
    TLabel *Label31;
    TEdit *eNameLocation;
    TBitBtn *btnSaveLocation;
    TBitBtn *btnCancelLocation;
    TComboBox *cbTypeLocation;
    TLabel *Label32;
    TAction *actSaveStation;
    TAction *actSaveBuilding;
    TAction *actSaveLocation;
    TMenuItem *miAddStation;
    TMenuItem *miDelStation;
    TMenuItem *miAddBuilding;
    TMenuItem *miDelBuilding;
    TComboBox *cbNameRegion;
    TComboBox *cbNameZone;
    TComboBox *cbNameNode;
    TTabSheet *tsCentralization;
    TCheckBox *cbActiveCentralizationService;
    TGroupBox *GroupBox1;
    TLabel *Label38;
    TEdit *eIP1Centralization;
    TEdit *eIP2Centralization;
    TEdit *eIP3Centralization;
    TEdit *eIP4Centralization;
    TGroupBox *GroupBox2;
	TLabel *lblId;
    TEdit *eIDDevice;
	TLabel *lblSerialNumber;
    TEdit *eSNDevice;
    TLabel *Label11;
    TEdit *eNameDevice;
    TLabel *Label17;
    TComboBox *cbTypeDevice;
    TGroupBox *GroupBox3;
    TLabel *Label35;
    TComboBox *cbStationDevice;
    TLabel *Label36;
    TComboBox *cbBuildingDevice;
    TComboBox *cbLocationDevice;
    TBitBtn *btnGridLocPosition;
    TLabel *Label12;
    TLabel *Label13;
    TLabel *Label22;
    TLabel *Label23;
    TGroupBox *GroupBox4;
    TComboBox *cbPortDevice;
    TEdit *eIP1Device;
    TComboBox *cbIPDevice;
    TEdit *eIP2Device;
    TEdit *eIP3Device;
    TEdit *eIP4Device;
    TLabel *Label15;
	TLabel *lbl_IP_COM_Address;
    TGroupBox *GroupBox5;
    TComboBox *cbScheduledDevice;
    TComboBox *cbActiveDevice;
    TLabel *Label14;
    TLabel *Label19;
    TLabel *Label20;
    TComboBox *cbProfileDevice;
    TLabel *Label21;
    TMenuItem *miDuplicateDevice;
    TGroupBox *GroupBox6;
    TEdit *eNoteBuilding;
    TLabel *Label33;
    TGroupBox *GroupBox7;
    TLabel *Label40;
    TEdit *eNoteLocation;
    TGroupBox *GroupBox8;
    TComboBox *cbAddrModemNode;
    TLabel *Label25;
    TLabel *Label24;
    TMenuItem *miSaveXMLOutputWhitoutRestartSevice;
	TLabel *lblIDRegion;
	TBitBtn *btnUpdateRegion;
	TBitBtn *btnUpdateItem;
	TBitBtn *btnUpdateServer;
	TBitBtn *btnUpdateZone;
	TBitBtn *btnUpdateNode;
	TBitBtn *btnUpdateDevice;
	TBitBtn *btnUpdateStation;
	TBitBtn *btnUpdateBuilding;
	TBitBtn *btnUpdateLocation;
	TLabel *Label41;
	TLabel *lblIDItem;
	TLabel *Label34;
	TLabel *lblIDServer;
	TLabel *lblIDZone;
	TLabel *Label42;
	TLabel *Label1;
	TLabel *lblIDNode;
	TLabel *Label5;
	TLabel *lblIDStation;
	TLabel *lblIDBuilding;
	TLabel *Label26;
	TLabel *Label8;
	TLabel *lblIDLocation;
	TLabel *lblSizeModemNode;
	TLabel *lblPosXDevice;
	TLabel *lblPosYDevice;
	TBitBtn *btnSaveCentralization;
	TBitBtn *btnUpdateCentralization;
	TBitBtn *btnCancelCentralization;
	TAction *actSaveCentralization;
	TMenuItem *miUpdateCentralization;
	TEdit *eHostNameCentralization;
	TTabSheet *tsSNMPManager;
	TLabel *Label28;
	TEdit *eCommunity;
	TLabel *Label30;
	TEdit *eTrapCommunity;
	TBitBtn *btnSaveSNMP;
	TBitBtn *btnUpdateSNMP;
	TBitBtn *btnCancelSNMP;
	TAction *actSaveSNMP;
	TMenuItem *miUpdateSNMP;
	TLabel *Label39;
	TEdit *eIntervalSeconds;
	TIdHTTP *IdHTTP_reg;
	TMenuItem *miRegionListDownload;
	TMenuItem *N2;
	TMenuItem *N3;
	TMenuItem *Opzioni1;
	TMenuItem *Opzioni;
	TCheckBox *cbUseSystemXMLCommunity;
	TEdit *eSNMPCommunity;
	TLabel *lblSNMPCommunity;
	TCheckBox *cbLocal;
	TLabel *lblSupervisorId;
	TComboBox *cbSupervisorId;
	TMenuItem *miDeviceTypeListDownload;
    void __fastcall TreeViewChange(TObject *Sender, TTreeNode *Node);
    void __fastcall actSaveItemUpdate(TObject *Sender);
    void __fastcall actSaveItemExecute(TObject *Sender);
    void __fastcall miAddItemClick(TObject *Sender);
    void __fastcall btnCancelItemClick(TObject *Sender);
    void __fastcall miDelItemClick(TObject *Sender);
    void __fastcall miUpdateItemClick(TObject *Sender);
    void __fastcall cbTypeItemChange(TObject *Sender);
    void __fastcall miCloseClick(TObject *Sender);
    void __fastcall miOpenXMLSourceClick(TObject *Sender);
    void __fastcall miSaveXMLOutputAndRestartServiceClick(TObject *Sender);
    void __fastcall miNewClick(TObject *Sender);
    void __fastcall actSaveServerExecute(TObject *Sender);
    void __fastcall actSaveServerUpdate(TObject *Sender);
    void __fastcall miUpdateServerClick(TObject *Sender);
    void __fastcall btnCancelServerClick(TObject *Sender);
    void __fastcall actSaveRegionExecute(TObject *Sender);
    void __fastcall actSaveRegionUpdate(TObject *Sender);
    void __fastcall btnCancelRegionClick(TObject *Sender);
    void __fastcall miAddRegionClick(TObject *Sender);
    void __fastcall miUpdateRegionClick(TObject *Sender);
    void __fastcall miDelRegionClick(TObject *Sender);
    void __fastcall actSaveZoneExecute(TObject *Sender);
    void __fastcall actSaveZoneUpdate(TObject *Sender);
    void __fastcall actSaveNodeExecute(TObject *Sender);
    void __fastcall actSaveNodeUpdate(TObject *Sender);
    void __fastcall actSaveDeviceExecute(TObject *Sender);
    void __fastcall actSaveDeviceUpdate(TObject *Sender);
    void __fastcall btnCancelZoneClick(TObject *Sender);
    void __fastcall btnCancelNodeClick(TObject *Sender);
    void __fastcall btnCancelDeviceClick(TObject *Sender);
    void __fastcall miAddZoneClick(TObject *Sender);
    void __fastcall miUpdateZoneClick(TObject *Sender);
    void __fastcall miDelZoneClick(TObject *Sender);
    void __fastcall miAddNodeClick(TObject *Sender);
    void __fastcall miUpdateNodeClick(TObject *Sender);
    void __fastcall miDelNodeClick(TObject *Sender);
    void __fastcall miAddDeviceClick(TObject *Sender);
    void __fastcall miUpdateDeviceClick(TObject *Sender);
    void __fastcall miDelDeviceClick(TObject *Sender);
    void __fastcall miOpenXMLBackUpClick(TObject *Sender);
    void __fastcall miSaveXMLBackUpClick(TObject *Sender);
    void __fastcall cbPortDeviceChange(TObject *Sender);
    void __fastcall XmlTreeViewChange(TObject *Sender, TTreeNode *Node);
    void __fastcall actSaveStationExecute(TObject *Sender);
    void __fastcall actSaveStationUpdate(TObject *Sender);
    void __fastcall actSaveBuildingExecute(TObject *Sender);
    void __fastcall actSaveBuildingUpdate(TObject *Sender);
    void __fastcall actSaveLocationExecute(TObject *Sender);
    void __fastcall actSaveLocationUpdate(TObject *Sender);
    void __fastcall btnCancelStationClick(TObject *Sender);
    void __fastcall btnCancelBuildingClick(TObject *Sender);
    void __fastcall btnCancelLocationClick(TObject *Sender);
    void __fastcall miUpdateStationClick(TObject *Sender);
    void __fastcall miUpdateBuildingClick(TObject *Sender);
    void __fastcall miAddLocationClick(TObject *Sender);
    void __fastcall miUpdateLocationClick(TObject *Sender);
    void __fastcall miDelLocationClick(TObject *Sender);
    void __fastcall FormActivate(TObject *Sender);
    void __fastcall XmlTreeViewMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall miAddBuildingClick(TObject *Sender);
    void __fastcall miAddStationClick(TObject *Sender);
    void __fastcall miDelStationClick(TObject *Sender);
    void __fastcall miDelBuildingClick(TObject *Sender);
    void __fastcall cbStationDeviceChange(TObject *Sender);
    void __fastcall cbBuildingDeviceChange(TObject *Sender);
    void __fastcall btnGridLocPositionClick(TObject *Sender);
    void __fastcall cbLocationDeviceChange(TObject *Sender);
    void __fastcall cbTypeDeviceChange(TObject *Sender);
    void __fastcall cbProfileDeviceChange(TObject *Sender);
    void __fastcall cbTypeLocationChange(TObject *Sender);
    void __fastcall cbAddrModemNodeChange(TObject *Sender);
    void __fastcall cbIPDeviceChange(TObject *Sender);
    void __fastcall cbActiveDeviceChange(TObject *Sender);
    void __fastcall cbSupervisorIdChange(TObject *Sender);
    void __fastcall cbScheduledDeviceChange(TObject *Sender);
    void __fastcall cbEchoItemChange(TObject *Sender);
    void __fastcall cbDataItemChange(TObject *Sender);
    void __fastcall cbStopItemChange(TObject *Sender);
    void __fastcall cbParityItemChange(TObject *Sender);
    void __fastcall cbPersistentItemChange(TObject *Sender);
    void __fastcall cbCOMItemChange(TObject *Sender);
    void __fastcall cbBaudItemChange(TObject *Sender);
    void __fastcall cbNameRegionChange(TObject *Sender);
    void __fastcall cbNameZoneChange(TObject *Sender);
    void __fastcall cbNameNodeChange(TObject *Sender);
	void __fastcall miDuplicateDeviceClick(TObject *Sender);
    void __fastcall miSaveXMLOutputWhitoutRestartSeviceClick(
          TObject *Sender);
	void __fastcall eNameItemKeyPress(TObject *Sender, char &Key);
	void __fastcall btnUpdateRegionClick(TObject *Sender);
	void __fastcall btnUpdateItemClick(TObject *Sender);
	void __fastcall btnUpdateServerClick(TObject *Sender);
	void __fastcall btnUpdateZoneClick(TObject *Sender);
	void __fastcall btnUpdateNodeClick(TObject *Sender);
	void __fastcall btnUpdateDeviceClick(TObject *Sender);
	void __fastcall btnUpdateStationClick(TObject *Sender);
	void __fastcall btnUpdateBuildingClick(TObject *Sender);
	void __fastcall btnUpdateLocationClick(TObject *Sender);
	void __fastcall eIP1ItemKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP2ItemKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP3ItemKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP4ItemKeyPress(TObject *Sender, char &Key);
	void __fastcall ePortItemKeyPress(TObject *Sender, char &Key);
	void __fastcall eTimeOutItemKeyPress(TObject *Sender, char &Key);
	void __fastcall eHostServerKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameServerKeyPress(TObject *Sender, char &Key);
	void __fastcall eIDDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eSNDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP1DeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP2DeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP3DeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eIP4DeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameStationKeyPress(TObject *Sender, char &Key);
	void __fastcall eNoteBuildingKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameBuildingKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameLocationKeyPress(TObject *Sender, char &Key);
	void __fastcall eNoteLocationKeyPress(TObject *Sender, char &Key);
	void __fastcall cbTypeItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbCOMItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbBaudItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbEchoItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbDataItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbStopItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbParityItemKeyPress(TObject *Sender, char &Key);
	void __fastcall cbPersistentItemKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameItemChange(TObject *Sender);
	void __fastcall eIP1ItemChange(TObject *Sender);
	void __fastcall eIP2ItemChange(TObject *Sender);
	void __fastcall eIP3ItemChange(TObject *Sender);
	void __fastcall eIP4ItemChange(TObject *Sender);
	void __fastcall ePortItemChange(TObject *Sender);
	void __fastcall eTimeOutItemChange(TObject *Sender);
	void __fastcall eHostServerChange(TObject *Sender);
	void __fastcall eNameServerChange(TObject *Sender);
	void __fastcall cbNameRegionKeyPress(TObject *Sender, char &Key);
	void __fastcall cbNameZoneKeyPress(TObject *Sender, char &Key);
	void __fastcall cbNameNodeKeyPress(TObject *Sender, char &Key);
	void __fastcall cbAddrModemNodeKeyPress(TObject *Sender, char &Key);
	void __fastcall eIDDeviceChange(TObject *Sender);
	void __fastcall eSNDeviceChange(TObject *Sender);
	void __fastcall eNameDeviceChange(TObject *Sender);
	void __fastcall eIP1DeviceChange(TObject *Sender);
	void __fastcall eIP2DeviceChange(TObject *Sender);
	void __fastcall eIP3DeviceChange(TObject *Sender);
	void __fastcall eIP4DeviceChange(TObject *Sender);
	void __fastcall cbTypeDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbStationDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbBuildingDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbLocationDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbPortDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbIPDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbActiveDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbSupervisorIdKeyPress(TObject *Sender, char &Key);    
	void __fastcall cbScheduledDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbProfileDeviceKeyPress(TObject *Sender, char &Key);
	void __fastcall cbTypeLocationKeyPress(TObject *Sender, char &Key);
	void __fastcall eNameStationChange(TObject *Sender);
	void __fastcall eNameBuildingChange(TObject *Sender);
	void __fastcall eNoteBuildingChange(TObject *Sender);
	void __fastcall eNameLocationChange(TObject *Sender);
	void __fastcall eNoteLocationChange(TObject *Sender);
	void __fastcall actSaveCentralizationExecute(TObject *Sender);
	void __fastcall actSaveCentralizationUpdate(TObject *Sender);
	void __fastcall btnUpdateCentralizationClick(TObject *Sender);
	void __fastcall btnCancelCentralizationClick(TObject *Sender);
	void __fastcall miUpdateCentralizationClick(TObject *Sender);
	void __fastcall actSaveSNMPExecute(TObject *Sender);
	void __fastcall actSaveSNMPUpdate(TObject *Sender);
	void __fastcall btnUpdateSNMPClick(TObject *Sender);
	void __fastcall btnCancelSNMPClick(TObject *Sender);
	void __fastcall miUpdateSNMPClick(TObject *Sender);
	void __fastcall miRegionListDownloadClick(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall OpzioniClick(TObject *Sender);
	void __fastcall eSNMPCommunityChange(TObject *Sender);
	void __fastcall eSNMPCommunityKeyPress(TObject *Sender, char &Key);
	void __fastcall IdHTTP_regWork(TObject *Sender, TWorkMode AWorkMode,
          const int AWorkCount);
	void __fastcall IdHTTP_regWorkBegin(TObject *Sender, TWorkMode AWorkMode,
          const int AWorkCountMax);
	void __fastcall miDeviceTypeListDownloadClick(TObject *Sender);
private:	// User declarations
    void __fastcall TfrmMain :: ReadElementData();
    int __fastcall TfrmMain :: OpenXMLSource(AnsiString path);
    int __fastcall TfrmMain :: OpenXMLCentralization(AnsiString path);
    void __fastcall TfrmMain :: LoadTreeView();
    void __fastcall TfrmMain :: LoadPopUpMenu();
    void __fastcall TfrmMain :: ClearPopUpMenu();
    void __fastcall TfrmMain :: LoadCurrentTabSheet();
    void __fastcall TfrmMain :: PageCancel();
    void __fastcall TfrmMain :: RemoveNode();
    void __fastcall TfrmMain :: MakeItemPageEnabled();
    void __fastcall TfrmMain :: MakeServerPageEnabled();
    void __fastcall TfrmMain :: MakeRegionPageEnabled();
    void __fastcall TfrmMain :: MakeZonePageEnabled();
    void __fastcall TfrmMain :: MakeNodePageEnabled();
    void __fastcall TfrmMain :: MakeStationPageEnabled();
    void __fastcall TfrmMain :: MakeBuildingPageEnabled();
    void __fastcall TfrmMain :: MakeLocationPageEnabled();
    void __fastcall TfrmMain :: MakeDevicePageEnabled();
    void __fastcall TfrmMain :: SetControlByTypeItem();
    void __fastcall TfrmMain :: SetDefaultValueByTypeItem();
    void __fastcall TfrmMain :: EnabledContextForm(bool enabledValue);
    int  __fastcall TfrmMain :: LoadTypeDeviceComboFromXML();
    int  __fastcall TfrmMain :: LoadRegionZoneNodeFromXML();
    void __fastcall TfrmMain :: LoadNameRegionCombo();
    void __fastcall TfrmMain :: LoadNameZoneCombo();
    void __fastcall TfrmMain :: LoadNameNodeCombo();
    void __fastcall TfrmMain :: LoadPortDeviceCombo();
    void __fastcall TfrmMain :: LoadProfileDeviceCombo();
    void __fastcall TfrmMain :: LoadStationDeviceCombo();
    void __fastcall TfrmMain :: LoadBuildingDeviceCombo();
    void __fastcall TfrmMain :: LoadLocationDeviceCombo();
	AnsiString __fastcall TfrmMain :: getNamePort(int port);
	AnsiString __fastcall TfrmMain :: getNamePort(AnsiString type_port);
    AnsiString __fastcall TfrmMain :: getNameStation(int idStation);
    AnsiString __fastcall TfrmMain :: getNameBuilding(int idBuilding);
    AnsiString __fastcall TfrmMain :: getNameLocation(int idLocation);
    AnsiString __fastcall TfrmMain :: getNameProfile(int profile);
    AnsiString __fastcall TfrmMain :: getIDPort(AnsiString portName);
	AnsiString __fastcall TfrmMain :: getTypePort(int port);
	AnsiString __fastcall TfrmMain :: getCodeTypeDevice(AnsiString typeName);
	AnsiString __fastcall TfrmMain :: getPortTypeDevice(AnsiString typeName);
	AnsiString __fastcall TfrmMain :: getNameTypeDevice(AnsiString typeCode);
	AnsiString __fastcall TfrmMain :: getSNMPCommunityDevice(AnsiString typeCode);
   	AnsiString __fastcall TfrmMain :: getSupervisorIdDevice(AnsiString typeCode);
    AnsiString __fastcall TfrmMain :: getDefaultNameDevice(AnsiString typeCode);		

    AnsiString __fastcall TfrmMain :: getIDRegionByName(AnsiString regionName);
    AnsiString __fastcall TfrmMain :: getIDZoneByName(AnsiString zoneName);
	AnsiString __fastcall TfrmMain :: getIDNodeByName(AnsiString nodeName);
	bool __fastcall TfrmMain :: getExistLocalNode( void );
	void __fastcall TfrmMain :: setNoLocalNodes(AnsiString idNewLocal);
    bool __fastcall TfrmMain :: RegionExist(AnsiString regionID);
    bool __fastcall TfrmMain :: ZoneExist(AnsiString zoneID);
    bool __fastcall TfrmMain :: NodeExist(AnsiString nodeID);

    int __fastcall TfrmMain :: getMaxIDPort();
    int __fastcall TfrmMain :: getMaxIDStation();
    int __fastcall TfrmMain :: getMaxIDBuilding();
    int __fastcall TfrmMain :: getMaxIDLocation();
    int __fastcall TfrmMain :: getMaxIDdevice();
    bool __fastcall TfrmMain :: IsIDdeviceUsed(AnsiString idValue);
    bool __fastcall TfrmMain :: IsDeviceAddressUsed(AnsiString idPortValue, int decDeviceAddressValue);

    AnsiString __fastcall TfrmMain :: GetTypeLocationFromDevice(AnsiString stationIDValue,
                                    AnsiString buildingIDValue, AnsiString locationIDValue);

    bool __fastcall TfrmMain :: IsCOMUsed(AnsiString comValue);
    bool __fastcall TfrmMain :: IsIDPortUsed(AnsiString idPortValue);
    void __fastcall TfrmMain :: DecPortNumberOnDevices(AnsiString PortNumberValue);
    void __fastcall TfrmMain :: DecPortNumberOnPort(AnsiString PortNumberValue);

    bool __fastcall TfrmMain :: loadIDServer(void);
	int __fastcall TfrmMain :: getIDServer(AnsiString * IDServer);
    void __fastcall TfrmMain :: loadHostNameServer(void);
    AnsiString __fastcall TfrmMain :: getHostNameServer();

    AnsiString __fastcall TfrmMain :: getIDProfile(AnsiString profileName);
    AnsiString __fastcall TfrmMain :: getIDStation(AnsiString stationName);
    AnsiString __fastcall TfrmMain :: getIDBuildingByDevice(AnsiString buildingName);
    AnsiString __fastcall TfrmMain :: getIDLocationByDevice(AnsiString locationName);

    bool __fastcall TfrmMain :: StationNameExist(AnsiString stationName);
    bool __fastcall TfrmMain :: BuildingNameExist(AnsiString buildingName);
    bool __fastcall TfrmMain :: LocationNameExist(AnsiString locationName);

    bool __fastcall TfrmMain :: IsIDLocationUsed();
    bool __fastcall TfrmMain :: IsTypeLocationUsed(AnsiString typeLocation);
    void __fastcall TfrmMain :: DecLocationIDOnDevices(AnsiString LocationNumberValue);
    void __fastcall TfrmMain :: DecLocationIDOnLocation(AnsiString LocationIDValue);
    void __fastcall TfrmMain :: DecBuildingIDOnBuilding(AnsiString BuildingIDValue);
    void __fastcall TfrmMain :: DecStationIDOnStation(AnsiString StationIDValue);

    void __fastcall TfrmMain :: DecDeviceIDOnDevice();

    char * __fastcall TfrmMain::CodificaDeviceAddress(char * hexModemAddressValue, int decDeviceAddressValue);
    int __fastcall TfrmMain::DecodificaDeviceAddress(char * hexModemAddressValue, char * hexCodifyDeviceAddressValue);
    void __fastcall TfrmMain :: RicodificaDeviceAddress(char * hexModemAddressNewValue);

	void __fastcall TfrmMain :: MakeCentralizationPageEnabled();
	void __fastcall TfrmMain :: EnabledContextFormByAddServices(bool enabledValue);
	void __fastcall TfrmMain::CentralizationPageCancel();
	XML_ELEMENT * __fastcall TfrmMain :: GetCentralizationNodeByName(AnsiString nodeNameValue);

	int __fastcall TfrmMain :: ManageSCAgentService();

	int __fastcall TfrmMain ::ManageService(DWORD dwStartTypeValue, char * service );
	int __fastcall TfrmMain ::GetServiceStartType( char * service );
	int __fastcall TfrmMain ::RestartService(char * service );
	int __fastcall TfrmMain ::StopService(char * service );
	int __fastcall TfrmMain ::StartService(char * service );

	bool __fastcall TfrmMain::checkInt(AnsiString textValue);
	bool __fastcall TfrmMain::checkTextComboBoxInItemList(TComboBox *cb);
	bool __fastcall TfrmMain::checkTextComboBoxIsValid(TComboBox *cb);
	bool __fastcall TfrmMain::checkTextEditIsValid(TEdit *e);
	void __fastcall TfrmMain :: CheckKey(char &KeyValue);

	void __fastcall TfrmMain :: SetcbActiveCentralizationService();
	void __fastcall TfrmMain :: loadEditsCentralization();
	void __fastcall TfrmMain :: setHostCentralizationNode(AnsiString nodeNameValue, AnsiString sHostCentralizationValue);
	bool __fastcall TfrmMain :: CheckSCAgent();


	char * __fastcall TfrmMain::GetXMLSystemPathFileName( void );
//	char * __fastcall TfrmMain::GetXMLAgentPathFileName( void );
  //	char * __fastcall TfrmMain::GetCharPointer(const char * Str);

	int __fastcall TfrmMain :: OpenIniSNMPManagerConfig(AnsiString pathValue);
	int __fastcall TfrmMain :: SaveIniSNMPManagerConfig(AnsiString pathValue);
//	char * __fastcall TfrmMain::GetSNMPManagerPathFileName( void );
	void __fastcall TfrmMain :: loadControlSNMPManager();
	void __fastcall TfrmMain :: MakeSNMPManagerPageEnabled();
	void __fastcall TfrmMain :: SNMPManagerPageCancel();
	void __fastcall TfrmMain :: RegionListDownload();
	void __fastcall TfrmMain :: DeviceTypeListDownload();
	bool __fastcall TfrmMain :: ExistsLocations();


public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

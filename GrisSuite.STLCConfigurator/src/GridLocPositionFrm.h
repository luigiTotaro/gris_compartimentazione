//---------------------------------------------------------------------------

#ifndef GridLocPositionFrmH
#define GridLocPositionFrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmGridLocPosition : public TForm
{
__published:	// IDE-managed Components
    TStringGrid *sgLocPosition;
    TLabel *lblTypeLocation;
    TLabel *lblColRow;
    TPanel *Panel1;
    TBitBtn *btnSaveGrid;
    TBitBtn *btnCancelGrid;
    void __fastcall sgLocPositionSelectCell(TObject *Sender, int ACol,
          int ARow, bool &CanSelect);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall btnCancelGridClick(TObject *Sender);
private:	// User declarations
    bool isOnShow;
    int locXPosTemp;
    int locYPosTemp;

public:		// User declarations
    __fastcall TfrmGridLocPosition(TComponent* Owner);
    int locXPosition;
    int locYPosition;
    AnsiString typeLocation;
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmGridLocPosition *frmGridLocPosition;
//---------------------------------------------------------------------------
#endif

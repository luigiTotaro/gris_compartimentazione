//---------------------------------------------------------------------------

#ifndef OptionsFrmH
#define OptionsFrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TfrmOptions : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *gpUrl;
	TEdit *eRegionListURL;
	TButton *btnConfirm;
	TButton *btnCancel;
	TLabel *lRegionListURL;
	TLabel *lDeviceTypeListURL;
	TEdit *eDeviceTypeListURL;
	void __fastcall btnCancelClick(TObject *Sender);
	void __fastcall btnConfirmClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfrmOptions(TComponent* Owner);
	AnsiString s_RL_URL;
    AnsiString s_DTL_URL;
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmOptions *frmOptions;
//---------------------------------------------------------------------------
#endif

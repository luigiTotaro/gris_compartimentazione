object frmSaveSplash: TfrmSaveSplash
  Left = 445
  Top = 328
  BorderIcons = []
  Caption = 'Salvataggio configurazione e riavvio servizi in corso ...'
  ClientHeight = 66
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lCurrentTask: TLabel
    Left = 21
    Top = 40
    Width = 320
    Height = 13
    AutoSize = False
  end
  object pbSave: TProgressBar
    Left = 21
    Top = 11
    Width = 320
    Height = 17
    Smooth = True
    TabOrder = 0
  end
end

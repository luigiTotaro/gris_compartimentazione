object frmOptions: TfrmOptions
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Opzioni'
  ClientHeight = 176
  ClientWidth = 749
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gpUrl: TGroupBox
    Left = 8
    Top = 14
    Width = 733
    Height = 123
    Caption = 'URL per aggiornamento'
    TabOrder = 0
    object lRegionListURL: TLabel
      Left = 13
      Top = 22
      Width = 123
      Height = 13
      Caption = 'URL lista compartimentale'
    end
    object lDeviceTypeListURL: TLabel
      Left = 13
      Top = 68
      Width = 112
      Height = 13
      Caption = 'URL lista tipi periferiche'
    end
    object eRegionListURL: TEdit
      Left = 13
      Top = 41
      Width = 707
      Height = 21
      TabOrder = 0
    end
    object eDeviceTypeListURL: TEdit
      Left = 13
      Top = 87
      Width = 707
      Height = 21
      TabOrder = 1
    end
  end
  object btnConfirm: TButton
    Left = 299
    Top = 143
    Width = 75
    Height = 25
    Caption = 'Conferma'
    TabOrder = 1
    OnClick = btnConfirmClick
  end
  object btnCancel: TButton
    Left = 388
    Top = 143
    Width = 75
    Height = 25
    Caption = 'Annulla'
    TabOrder = 2
    OnClick = btnCancelClick
  end
end

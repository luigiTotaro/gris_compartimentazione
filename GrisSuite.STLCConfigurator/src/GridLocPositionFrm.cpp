//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GridLocPositionFrm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmGridLocPosition *frmGridLocPosition;
//---------------------------------------------------------------------------
__fastcall TfrmGridLocPosition::TfrmGridLocPosition(TComponent* Owner)
    : TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TfrmGridLocPosition::sgLocPositionSelectCell(TObject *Sender,
      int ACol, int ARow, bool &CanSelect)
{
    if (!isOnShow)
    {
        locXPosition = ACol + 1;
        locYPosition = ARow + 1;

        if (typeLocation.Trim() == "RACK24U")
        {
            lblColRow->Caption = "Riga: " + IntToStr(locYPosition);
        }
        else
        {
			lblColRow->Caption = "Colonna: " + IntToStr(locXPosition) + " Riga: " + IntToStr(locYPosition);
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TfrmGridLocPosition::FormShow(TObject *Sender)
{
   isOnShow = true;

   sgLocPosition->Color = 0x0086DFF9;

   if (typeLocation.Trim() == "ATPS24")
   {
	  this->Height = 370;
	  this->Width = 250;
	  sgLocPosition->ColCount = 6;
	  sgLocPosition->RowCount = 4;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 16;
	  sgLocPosition->Width = 211;
	  sgLocPosition->Height = 259;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 24";
//      sgLocPosition->Canvas->Brush->Color = clLime;
   }
   else if (typeLocation.Trim() == "ATPS24S")
   {
	  this->Height = 439;
	  this->Width = 250;
	  sgLocPosition->ColCount = 6;
	  sgLocPosition->RowCount = 5;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 16;
	  sgLocPosition->Width = 211;
	  sgLocPosition->Height = 324;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 24 con sopralzo";
//      sgLocPosition->Canvas->Brush->Color = clLime;
   }
   else if (typeLocation.Trim() == "ATPS20")
   {
	  this->Height = 370;
	  this->Width = 240;
	  sgLocPosition->ColCount = 5;
	  sgLocPosition->RowCount = 4;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 30;
	  sgLocPosition->Width = 174;
	  sgLocPosition->Height = 259;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 20";
   }
   else if (typeLocation.Trim() == "ATPS20S")
   {
	  this->Height = 439;
	  this->Width = 240;
	  sgLocPosition->ColCount = 5;
	  sgLocPosition->RowCount = 5;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 30;
	  sgLocPosition->Width = 174;
	  sgLocPosition->Height = 324;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 20 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS16")
   {
	  this->Height = 370;
	  this->Width = 205;
	  sgLocPosition->ColCount = 4;
	  sgLocPosition->RowCount = 4;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 29;
	  sgLocPosition->Width = 140;
	  sgLocPosition->Height = 259;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 16";
   }
   else if (typeLocation.Trim() == "ATPS16S")
   {
	  this->Height = 439;
	  this->Width = 221;
	  sgLocPosition->ColCount = 4;
	  sgLocPosition->RowCount = 5;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 38;
	  sgLocPosition->Width = 140;
	  sgLocPosition->Height = 324;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 16 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS12")
   {
	  this->Height = 301;
	  this->Width = 205;
	  sgLocPosition->ColCount = 4;
	  sgLocPosition->RowCount = 3;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 29;
	  sgLocPosition->Width = 140;
	  sgLocPosition->Height = 194;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 12";
   }
   else if (typeLocation.Trim() == "ATPS12S")
   {
	  this->Height = 370;
	  this->Width = 221;
	  sgLocPosition->ColCount = 4;
	  sgLocPosition->RowCount = 4;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 38;
	  sgLocPosition->Width = 140;
	  sgLocPosition->Height = 259;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 12 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS9")
   {
	  this->Height = 301;
	  this->Width = 166;
	  sgLocPosition->ColCount = 3;
	  sgLocPosition->RowCount = 3;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 27;
	  sgLocPosition->Width = 105;
	  sgLocPosition->Height = 194;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 9";
   }
   else if (typeLocation.Trim() == "ATPS9S")
   {
	  this->Height = 370;
	  this->Width = 221;
	  sgLocPosition->ColCount = 3;
	  sgLocPosition->RowCount = 4;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 54;
	  sgLocPosition->Width = 105;
	  sgLocPosition->Height = 259;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 9 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS8")
   {
	  this->Height = 239;
	  this->Width = 205;
	  sgLocPosition->ColCount = 4;
	  sgLocPosition->RowCount = 2;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 29;
	  sgLocPosition->Width = 140;
	  sgLocPosition->Height = 129;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 8";
   }
   else if (typeLocation.Trim() == "ATPS8S")
   {
	  this->Height = 301;
	  this->Width = 221;
	  sgLocPosition->ColCount = 4;
	  sgLocPosition->RowCount = 3;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 37;
	  sgLocPosition->Width = 140;
	  sgLocPosition->Height = 259;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 8 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS6")
   {
	  this->Height = 239;
	  this->Width = 166;
	  sgLocPosition->ColCount = 3;
	  sgLocPosition->RowCount = 2;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 27;
	  sgLocPosition->Width = 105;
	  sgLocPosition->Height = 129;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 6";
   }
   else if (typeLocation.Trim() == "ATPS6S")
   {
	  this->Height = 301;
	  this->Width = 221;
	  sgLocPosition->ColCount = 3;
	  sgLocPosition->RowCount = 3;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 54;
	  sgLocPosition->Width = 105;
	  sgLocPosition->Height = 194;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 6 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS4")
   {
	  this->Height = 239;
	  this->Width = 166;
	  sgLocPosition->ColCount = 2;
	  sgLocPosition->RowCount = 2;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 45;
	  sgLocPosition->Width = 70;
	  sgLocPosition->Height = 129;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 4";
   }
   else if (typeLocation.Trim() == "ATPS4S")
   {
	  this->Height = 301;
	  this->Width = 221;
	  sgLocPosition->ColCount = 2;
	  sgLocPosition->RowCount = 3;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 72;
	  sgLocPosition->Width = 70;
	  sgLocPosition->Height = 194;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 4 con sopralzo";
   }
   else if (typeLocation.Trim() == "ATPS3")
   {
	  this->Height = 179;
	  this->Width = 166;
	  sgLocPosition->ColCount = 3;
	  sgLocPosition->RowCount = 1;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 27;
	  sgLocPosition->Width = 105;
	  sgLocPosition->Height = 60;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Caption = "Armadio ATPS 3";
   }
   else if (typeLocation.Trim() == "ATPS3S")
   {
	  this->Height = 239;
	  this->Width = 221;
	  sgLocPosition->ColCount = 3;
	  sgLocPosition->RowCount = 2;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 54;
	  sgLocPosition->Width = 105;
	  sgLocPosition->Height = 129;
	  sgLocPosition->DefaultColWidth = 30;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Armadio ATPS 3 con sopralzo";
   }
   else if (typeLocation.Trim() == "SUPPTELEIND")
   {
	  this->Height = 137;
	  this->Width = 174;
	  sgLocPosition->ColCount = 1;
	  sgLocPosition->RowCount = 1;
	  sgLocPosition->Top = 21;
	  sgLocPosition->Left = 38;
	  sgLocPosition->Width = 96;
	  sgLocPosition->Height = 64;
	  sgLocPosition->DefaultColWidth = 90;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblColRow->Visible = false;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Supporto Monitor";
	  btnCancelGrid->Visible = false;
	  btnSaveGrid->Left = btnSaveGrid->Left + 20;
   }
   else if (typeLocation.Trim() == "SUPPDIFF")
   {
	  this->Height = 137;
	  this->Width = 174;
	  sgLocPosition->ColCount = 1;
	  sgLocPosition->RowCount = 1;
	  sgLocPosition->Top = 21;
	  sgLocPosition->Left = 38;
	  sgLocPosition->Width = 96;
	  sgLocPosition->Height = 64;
	  sgLocPosition->DefaultColWidth = 90;
	  sgLocPosition->DefaultRowHeight = 60;
	  lblColRow->Visible = false;
	  lblTypeLocation->Font->Size = 10;
	  lblTypeLocation->Caption = "Supporto Diffusore";
	  btnCancelGrid->Visible = false;
	  btnSaveGrid->Left = btnSaveGrid->Left + 20;
   }
   else if (typeLocation.Trim() == "RACK24U")
   {
	  this->Height = 477;
	  this->Width = 143;
	  sgLocPosition->ColCount = 1;
	  sgLocPosition->RowCount = 24;
	  sgLocPosition->Top = 51;
	  sgLocPosition->Left = 25;
	  sgLocPosition->Width = 86;
	  sgLocPosition->Height = 359;
	  sgLocPosition->DefaultColWidth = 80;
	  sgLocPosition->DefaultRowHeight = 10;
	  lblTypeLocation->Caption = "Armadio Rack";
   }



   locXPosTemp = locXPosition;
   locYPosTemp = locYPosition;

   sgLocPosition->Col = locXPosition - 1;
   sgLocPosition->Row = locYPosition - 1;

   if (typeLocation.Trim() == "RACK24U")
   {
		lblColRow->Caption = "Riga: " + IntToStr(sgLocPosition->Row + 1);
   }
   else
   {
		lblColRow->Caption = "Colonna: " + IntToStr(sgLocPosition->Col + 1) + " Riga: " + IntToStr(sgLocPosition->Row + 1);
   }
   isOnShow = false;
}
//---------------------------------------------------------------------------


void __fastcall TfrmGridLocPosition::btnCancelGridClick(TObject *Sender)
{
	locXPosition = locXPosTemp;
	locYPosition = locYPosTemp;
	this->Close();
}
//---------------------------------------------------------------------------




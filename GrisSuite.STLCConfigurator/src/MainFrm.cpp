//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainFrm.h"
#include "XMLView.h"
#include "SPVSTLC1000.h"
#include "SYSKernel.h"
#include "GridLocPositionFrm.h"
#include "SaveSplashFrm.h"
#include "OptionsFrm.h"
#include "SYSSocket.h"
#include "cnv_lib.h"
#include <Inifiles.hpp>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SCM_ACCESS_CODE 1
#define CONFIGURATOR_OPEN_CODE 55

#define CFG_KEY                        				"SOFTWARE\\Telefin\\Configurator"
#define CFG_REGION_LIST_URL_VALUE       			"RegionListUpdateURL"
#define CFG_REGION_LIST_URL_DEFAULT_VALUE			"http://griscollector.rfiservizi.corp/centralws/central.asmx/GetRegionList"
#define CFG_DEVICE_TYPE_LIST_URL_VALUE       		"DeviceTypeListUpdateURL"
#define CFG_DEVICE_TYPE_LIST_URL_DEFAULT_VALUE		"http://griscollector.rfiservizi.corp/centralws/central.asmx/GetDeviceTypeList"

#define SPV_CFG_SERVER_KEY                      	"SYSTEM\\CurrentControlSet\\Services\\SPVServerDaemon"
#define SPV_CFG_XML_SYSTEM_VALUE_NAME       		"XMLSystem"
#define SPV_CFG_XML_SYSTEM_DEFAULT_VALUE_PATH		"C:\\Program Files\\Telefin\\SupervisorServer\\system.xml"
#define SCA_CFG_TEM_SYSTEM_DEFAULT_VALUE_PATH 		"system.tem"

#define CONFIGURATION_MODIFIED 						2 // System.xml modificato
#define CONFIGURATION_SAVED							1 // configurazione salvata e servizi non riavviati
#define SERVICES_RESTARTED							0 // configurazione salvata e servizi riavviati

TfrmMain *frmMain;

XML_SOURCE * SystemSource;
XML_SOURCE * OutputSource;
XML_SOURCE * DeviceTypeSource;
XML_SOURCE * RegionZoneNodeSource;
XML_SOURCE * CentralizationSource;
XML_SOURCE * DownloadedRegionListSource;
XML_SOURCE * DownloadedDeviceTypeListSource;

AnsiString communityValue = "";
AnsiString trapCommunityValue = "";
bool useSystemXMLCommunity = false;

AnsiString mRegionListUpdateURL = "";
AnsiString mDeviceTypeListUpdateURL = "";

XML_ELEMENT * selectedElement = NULL;
XML_ELEMENT * originElement = NULL;

XML_ELEMENT * addServicesElement = NULL;

TTreeNode * selectedParentNode = NULL;
TTreeNode * selectedNode = NULL;

bool isNewNode = false;
bool isNodeInInsertUpdate = false;
bool isCentralizzationNodeInInsertUpdate = false;
bool isTreeViewOnLoading = false;
int config_status = SERVICES_RESTARTED;

int ViewChildNode(XML_ELEMENT * element,TTreeView * tree, AnsiString terminator);
int ViewNodeEntity(XML_ENTITY * xENTITY,TTreeView * pTree,TTreeNode * ParentNode);
void CreateAddServiceElement();
XML_ENTITY * GetPrevEntity(XML_SOURCE * XMLSource, XML_ELEMENT * element);
void AddElement(XML_SOURCE * XMLSource, XML_ELEMENT * selElement,XML_ELEMENT * newElement);

//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
		: TForm(Owner)
{

}


int __fastcall TfrmMain :: OpenXMLSource(AnsiString path)
{
    int ret_code = -1;

    try
    {
		SystemSource = XMLCreate(path.c_str());

		if (SystemSource != NULL)
        {
            ret_code = XMLOpen(SystemSource,NULL, 'R');
			if (ret_code == 0)
			{
                ret_code = XMLRead(SystemSource,NULL);
                if (ret_code == 0)
                {
					XMLCheck(SystemSource);
                }
            }
            XMLClose(SystemSource);
        }
        return ret_code;
    }
	catch(...)
    {
        return -1;
    }
}


void __fastcall TfrmMain :: LoadTreeView()
{
    int ret_code = 0;
    XML_ELEMENT * portElement;
	XML_ELEMENT * topographyElement;
    XML_ELEMENT * systemElement;

	isTreeViewOnLoading = true;

	XmlTreeView->Items->Clear();

	if (SystemSource != NULL)
    {
		portElement = XMLResolveElement(SystemSource->root_element,"port",NULL);
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		if (portElement != NULL)
        {
			ViewChildNode(portElement,XmlTreeView,"topography");
        }

        if (topographyElement != NULL)
        {
            ViewChildNode(topographyElement,XmlTreeView,"system");
        }

        if (systemElement != NULL)
		{
			ViewChildNode(systemElement,XmlTreeView,"scheduler");
		}

		ViewChildNode(addServicesElement,XmlTreeView,"");

		for(int i=0; XmlTreeView->Items->Count; i++)
		{
			if (XmlTreeView->Items->Item[i]->Text == "Porte di comunicazione" )
			{
				XmlTreeView->Items->Item[i]->Expand(true);
				break;
			}
		}

		for(int i=0; XmlTreeView->Items->Count; i++)
		{
			if (XmlTreeView->Items->Item[i]->Text == "Topografia" )
			{
				XmlTreeView->Items->Item[i]->Expand(true);
				break;
			}
		}

		for(int i=0; XmlTreeView->Items->Count; i++)
		{
			if (XmlTreeView->Items->Item[i]->Text == "Sistema" )
			{
				XmlTreeView->Items->Item[i]->Expand(true);
				break;
			}
		}

		/* Promemmoria
		XmlTreeView->Items->Item[1]->Expand(true);
		XmlTreeView->Items->Item[2]->Expand(true);
		XmlTreeView->Items->Item[3]->Expand(true);
		*/

		//Da togliere a regime
		ClearPopUpMenu();
	}
	isTreeViewOnLoading = false;
}
//---------------------------------------------------------------------------




void __fastcall TfrmMain :: LoadPortDeviceCombo()
{
	XML_ELEMENT * element;

    cbPortDevice->Items->Clear();

    if (SystemSource != NULL)
    {
        element = XMLResolveElement(SystemSource->root_element,"port",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
            {
                cbPortDevice->Items->Add(AnsiString(XMLGetValue(element,"name")));
				// Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
}

void __fastcall TfrmMain :: LoadProfileDeviceCombo()
{
    XML_ELEMENT * element;

	cbProfileDevice->Items->Clear();

    if (SystemSource != NULL)
    {
		element = XMLResolveElement(SystemSource->root_element,"scheduler",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
			 // Cerco gli elementi XML figli

			element = element->child;
            while (element)
            {
                cbProfileDevice->Items->Add(AnsiString(XMLGetValue(element,"name")));
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
}

void __fastcall TfrmMain :: LoadStationDeviceCombo()
{
    XML_ELEMENT * topographyElement;
	XML_ELEMENT * stationElement;

    cbStationDevice->Items->Clear();

    if (SystemSource != NULL)
    {
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
			{
                cbStationDevice->Items->Add(AnsiString(XMLGetValue(stationElement,"name")));

                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
}

void __fastcall TfrmMain :: LoadBuildingDeviceCombo()
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;


    cbBuildingDevice->Items->Clear();

    if (SystemSource != NULL)
    {
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ( (stationElement != NULL) && (stationElement->child != NULL) &&
                     (AnsiString(XMLGetValue(stationElement,"id")) == getIDStation(cbStationDevice->Text.Trim().c_str())) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        cbBuildingDevice->Items->Add(AnsiString(XMLGetValue(buildingElement,"name")));
						// Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
	}
}

void __fastcall TfrmMain :: LoadLocationDeviceCombo()
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ELEMENT * locationElement;

    cbLocationDevice->Items->Clear();

	if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL) &&
                     (AnsiString(XMLGetValue(stationElement,"id")) == getIDStation(cbStationDevice->Text.Trim().c_str())) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
						if ((buildingElement != NULL) && (buildingElement->child != NULL) &&
                            (AnsiString(XMLGetValue(buildingElement,"id")) == getIDBuildingByDevice(cbBuildingDevice->Text.Trim().c_str())) )
                        {
                            // Cerco gli elementi XML figli
                            locationElement = buildingElement->child;
                            while (locationElement)
                            {
								cbLocationDevice->Items->Add(AnsiString(XMLGetValue(locationElement,"name")));
                                // Passo all'elemento successivo (se esiste)
                                locationElement = locationElement->next;
                            }
						}
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
			}
        }
    }
}

AnsiString __fastcall TfrmMain :: getIDStation(AnsiString stationName)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
			{
                if (  AnsiString(XMLGetValue(stationElement,"name"))  == stationName)
                {
                    return AnsiString(XMLGetValue(stationElement,"id"));
                }
                // Passo all'elemento successivo (se esiste)
				stationElement = stationElement->next;
            }
        }
    }
    return "";
}

AnsiString __fastcall TfrmMain :: getIDBuildingByDevice(AnsiString buildingName)
{
    XML_ELEMENT * topographyElement;
	XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
                    (AnsiString(XMLGetValue(stationElement,"id")) == getIDStation(cbStationDevice->Text.Trim().c_str())) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        if (  AnsiString(XMLGetValue(buildingElement,"name"))  == buildingName)
						{
                            return AnsiString(XMLGetValue(buildingElement,"id"));
                        }
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
					}
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
    return "";
}


AnsiString __fastcall TfrmMain :: getIDLocationByDevice(AnsiString locationName)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ELEMENT * locationElement;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
                    (AnsiString(XMLGetValue(stationElement,"id")) == getIDStation(cbStationDevice->Text.Trim().c_str())) )
                {
                    // Cerco gli elementi XML figli
					buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        if ((buildingElement != NULL) && (buildingElement->child != NULL)&&
							(AnsiString(XMLGetValue(buildingElement,"id")) == getIDBuildingByDevice(cbBuildingDevice->Text.Trim().c_str())) )
                        {
                            // Cerco gli elementi XML figli
                            locationElement = buildingElement->child;
                            while (locationElement)
                            {
                                if (  AnsiString(XMLGetValue(locationElement,"name"))  == locationName)
                                {
									return AnsiString(XMLGetValue(locationElement,"id"));
                                }

                                // Passo all'elemento successivo (se esiste)
                                locationElement = locationElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
    return "";
}

AnsiString __fastcall TfrmMain :: getNameStation(int idStation)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;

    if (SystemSource != NULL)
	{
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

		if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if (  StrToInt(AnsiString(XMLGetValue(stationElement,"id")))  == idStation)
				{
                    return AnsiString(XMLGetValue(stationElement,"name"));
                }

                // Passo all'elemento successivo (se esiste)
				stationElement = stationElement->next;
            }
        }
    }
    return "";
}

AnsiString __fastcall TfrmMain :: getNameBuilding(int idBuilding)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;


	if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
			while (stationElement)
            {
				if ((stationElement != NULL) && (stationElement->child != NULL)&&
                    (AnsiString(XMLGetValue(stationElement,"id")) == getIDStation(cbStationDevice->Text.Trim().c_str())) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
					{
                        if (  StrToInt(AnsiString(XMLGetValue(buildingElement,"id")))  == idBuilding)
                        {
                            return AnsiString(XMLGetValue(buildingElement,"name"));
                        }
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
				}
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
    return "";
}

AnsiString __fastcall TfrmMain :: getNameLocation(int idLocation)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ELEMENT * locationElement;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
					(AnsiString(XMLGetValue(stationElement,"id")) == getIDStation(cbStationDevice->Text.Trim().c_str())) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        if ((buildingElement != NULL) && (buildingElement->child != NULL)&&
                            (AnsiString(XMLGetValue(buildingElement,"id")) == getIDBuildingByDevice(cbBuildingDevice->Text.Trim().c_str())) )
                        {
                            // Cerco gli elementi XML figli
                            locationElement = buildingElement->child;
							while (locationElement)
                            {
                                if (  StrToInt(AnsiString(XMLGetValue(locationElement,"id")))  == idLocation)
                                {
                                    return AnsiString(XMLGetValue(locationElement,"name"));
                                }

                                // Passo all'elemento successivo (se esiste)
                                locationElement = locationElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
	return "";
}


AnsiString __fastcall TfrmMain :: getTypePort(int port)
{
	XML_ELEMENT * element;

	if (SystemSource != NULL)
	{
		element = XMLResolveElement(SystemSource->root_element,"port",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
			{
                if (  StrToInt(AnsiString(XMLGetValue(element,"id")))  == port)
                {
                    return AnsiString(XMLGetValue(element,"type"));
                }
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
    return "";
}

AnsiString __fastcall TfrmMain :: getNamePort(int port)
{
	XML_ELEMENT * element;

	if (SystemSource != NULL)
	{
		element = XMLResolveElement(SystemSource->root_element,"port",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli

			element = element->child;
			while (element)
			{
				if (  StrToInt(AnsiString(XMLGetValue(element,"id")))  == port)
				{
					return AnsiString(XMLGetValue(element,"name"));
				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}

AnsiString __fastcall TfrmMain :: getNamePort(AnsiString type_port)
{
	XML_ELEMENT * element;

	if (SystemSource != NULL)
	{
		element = XMLResolveElement(SystemSource->root_element,"port",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli

			element = element->child;
			while (element)
			{
				if (  AnsiString(XMLGetValue(element,"type")) == type_port)
				{
					return AnsiString(XMLGetValue(element,"name"));
				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}


AnsiString __fastcall TfrmMain :: getIDPort(AnsiString portName)
{
    XML_ELEMENT * element;

    if (SystemSource != NULL)
    {
        element = XMLResolveElement(SystemSource->root_element,"port",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
			{
                if (  AnsiString(XMLGetValue(element,"name"))  == portName)
				{
                    return AnsiString(XMLGetValue(element,"id"));

                }
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
    return "";
}

int __fastcall TfrmMain :: getMaxIDPort()
{
    XML_ELEMENT * element;
    XML_ELEMENT * lastChild;

    if (SystemSource != NULL)
    {
        element = XMLResolveElement(SystemSource->root_element,"port",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
            {
                lastChild = element;
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
            return StrToInt(AnsiString(XMLGetValue(lastChild,"id")));
        }
		else return -1;
	}
    else return -1;
}

bool __fastcall TfrmMain :: IsCOMUsed(AnsiString comValue)
{
    XML_ELEMENT * element;
    XML_ELEMENT * lastChild;

    if (SystemSource != NULL)
    {
        element = XMLResolveElement(SystemSource->root_element,"port",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
            {
                lastChild = element;
                if (AnsiString(XMLGetValue(lastChild,"com")) != "")
                {
					if ( AnsiString(XMLGetValue(lastChild,"com")) == comValue )
                        return true;
                }
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
    return false;
}


bool __fastcall TfrmMain :: IsIDPortUsed(AnsiString idPortValue)
{
	XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
	XML_ELEMENT * nodeElement = NULL;
	XML_ELEMENT * deviceElement = NULL;

	if (SystemSource != NULL)
	{
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

		if ((serverElement != NULL) && (serverElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
			regionElement = serverElement->child;
			while (regionElement)
			{
				if ((regionElement != NULL) && (regionElement->child != NULL))
				{
					// Cerco gli elementi XML figli
					zoneElement = regionElement->child;
					while (zoneElement)
					{
						if ((zoneElement != NULL) && (zoneElement->child != NULL))
						{
							// Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
							{
								if ((nodeElement != NULL) && (nodeElement->child != NULL))
								{
									// Cerco gli elementi XML figli
									deviceElement = nodeElement->child;
									while (deviceElement)
									{
										if (  AnsiString(XMLGetValue(deviceElement,"port"))  == idPortValue)
										{
											return true;
										}

										// Passo all'elemento successivo (se esiste)
										deviceElement = deviceElement->next;
									}

								}
								// Passo all'elemento successivo (se esiste)
								nodeElement = nodeElement->next;
							}
						}
						// Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
					}
				}
				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
			}
		}
	}
	return false;
}

bool __fastcall TfrmMain :: IsIDLocationUsed()
{
	XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
	XML_ELEMENT * nodeElement = NULL;
	XML_ELEMENT * deviceElement = NULL;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
            {
                if ((regionElement != NULL) && (regionElement->child != NULL))
                {
                    // Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL))
                        {
                            // Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
                                if ((nodeElement != NULL) && (nodeElement->child != NULL))
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
									while (deviceElement)
                                    {
                                        if (  (AnsiString(XMLGetValue(deviceElement,"station"))  == AnsiString(XMLGetValue(selectedElement->parent->parent,"id"))) &&
											  (AnsiString(XMLGetValue(deviceElement,"building")) == AnsiString(XMLGetValue(selectedElement->parent,"id"))) &&
                                              (AnsiString(XMLGetValue(deviceElement,"location")) == AnsiString(XMLGetValue(selectedElement,"id"))) )
                                        {
											return true;
                                        }

                                        // Passo all'elemento successivo (se esiste)
                                        deviceElement = deviceElement->next;
                                    }

								}
                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
    return false;
}

bool __fastcall TfrmMain :: IsTypeLocationUsed(AnsiString typeLocation)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
			{
                if ((regionElement != NULL) && (regionElement->child != NULL))
                {
                    // Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL))
                        {
                            // Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
                                if ((nodeElement != NULL) && (nodeElement->child != NULL))
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
                                    while (deviceElement)
                                    {
                                        if (  (AnsiString(XMLGetValue(deviceElement,"station"))  == AnsiString(XMLGetValue(selectedElement->parent->parent,"id"))) &&
                                              (AnsiString(XMLGetValue(deviceElement,"building")) == AnsiString(XMLGetValue(selectedElement->parent,"id"))) &&
                                              (AnsiString(XMLGetValue(deviceElement,"location")) == AnsiString(XMLGetValue(selectedElement,"id"))) &&
                                              (typeLocation == AnsiString(XMLGetValue(selectedElement,"type")))   )
										{
                                            return true;
                                        }

                                        // Passo all'elemento successivo (se esiste)
										deviceElement = deviceElement->next;
                                    }

                                }
                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
    return false;
}

void __fastcall TfrmMain :: DecPortNumberOnDevices(AnsiString PortNumberValue)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;
    XML_ATTRIBUTE * attribute = NULL;
    int iPortValue;
	AnsiString sNewPortValue;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
            {
                if ((regionElement != NULL) && (regionElement->child != NULL))
                {
					// Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL))
                        {
                            // Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
								if ((nodeElement != NULL) && (nodeElement->child != NULL))
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
                                    while (deviceElement)
                                    {
                                        iPortValue = StrToInt(AnsiString(XMLGetValue(deviceElement,"port")));
                                        if (iPortValue >= StrToInt(PortNumberValue) )
                                        {
											attribute = XMLGetAttrib(deviceElement,"port");
                                            if (attribute  != NULL)
                                                {
                                                    sNewPortValue = IntToStr(iPortValue-1);
													XMLSetValue(attribute,sNewPortValue.c_str());
                                                }

										}

										// Passo all'elemento successivo (se esiste)
                                        deviceElement = deviceElement->next;
                                    }

                                }
                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
						// Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
}

void __fastcall TfrmMain :: DecLocationIDOnDevices(AnsiString LocationIDValue)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
	XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;
    XML_ATTRIBUTE * attribute = NULL;
    int staIDValue,buiIDValue,locIDValue;
    AnsiString sNewLocationValue;

    if (SystemSource != NULL)
    {
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

		if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
            {
                if ((regionElement != NULL) && (regionElement->child != NULL))
                {
					// Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
						if ((zoneElement != NULL) && (zoneElement->child != NULL))
                        {
                            // Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
								if ((nodeElement != NULL) && (nodeElement->child != NULL))
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
                                    while (deviceElement)
									{
                                        staIDValue = StrToInt(AnsiString(XMLGetValue(deviceElement,"station")));
                                        buiIDValue = StrToInt(AnsiString(XMLGetValue(deviceElement,"building")));
                                        locIDValue = StrToInt(AnsiString(XMLGetValue(deviceElement,"location")));

                                        if ( (AnsiString(staIDValue) == AnsiString(XMLGetValue(selectedElement->parent->parent,"id")) ) &&
                                             (AnsiString(buiIDValue) == AnsiString(XMLGetValue(selectedElement->parent,"id"))) &&
                                             (locIDValue >= StrToInt(LocationIDValue)) )
                                        {
											attribute = XMLGetAttrib(deviceElement,"location");
                                            if (attribute  != NULL)
                                                {
                                                    sNewLocationValue = IntToStr(locIDValue-1);
                                                    XMLSetValue(attribute,sNewLocationValue.c_str());
                                                }

                                        }

                                        // Passo all'elemento successivo (se esiste)
                                        deviceElement = deviceElement->next;
                                    }

								}
                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
	}
}


void __fastcall TfrmMain :: DecPortNumberOnPort(AnsiString PortNumberValue)
{
    XML_ELEMENT * element;
    XML_ATTRIBUTE * attribute = NULL;
    int iPortValue;
    AnsiString sNewPortValue;

    if (SystemSource != NULL)
    {
        element = XMLResolveElement(SystemSource->root_element,"port",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
            {
                iPortValue = StrToInt(AnsiString(XMLGetValue(element,"id")));
                if (iPortValue >= StrToInt(PortNumberValue) )
				{
                    attribute = XMLGetAttrib(element,"id");
                    if (attribute  != NULL)
                    {
                        sNewPortValue = IntToStr(iPortValue-1);
                        XMLSetValue(attribute,sNewPortValue.c_str());
                    }
                }

                // Passo all'elemento successivo (se esiste)
                element = element->next;
			}
        }
    }
}

void __fastcall TfrmMain :: DecLocationIDOnLocation(AnsiString LocationIDValue)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ELEMENT * locationElement;
	XML_ATTRIBUTE * attribute = NULL;
    int iLocationValue;
    AnsiString sNewLocationValue;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
			 // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
					(XMLGetValue(stationElement,"id") == XMLGetValue(selectedElement->parent->parent,"id")))
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        if ((buildingElement != NULL) && (buildingElement->child != NULL)&&
                            (XMLGetValue(buildingElement,"id") == XMLGetValue(selectedElement->parent,"id")))
                        {
							// Cerco gli elementi XML figli
                            locationElement = buildingElement->child;
                            while (locationElement)
                            {
                                iLocationValue = StrToInt(AnsiString(XMLGetValue(locationElement,"id")));
                                if (iLocationValue >= StrToInt(LocationIDValue) )
                                {
                                    attribute = XMLGetAttrib(locationElement,"id");
                                    if (attribute  != NULL)
                                    {
                                        sNewLocationValue = IntToStr(iLocationValue-1);
                                        XMLSetValue(attribute,sNewLocationValue.c_str());
									}
                                }

                                // Passo all'elemento successivo (se esiste)
                                locationElement = locationElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
				stationElement = stationElement->next;
            }
        }
    }
}

AnsiString __fastcall TfrmMain :: getIDProfile(AnsiString profileName)
{
    XML_ELEMENT * element;

    if (SystemSource != NULL)
	{
        element = XMLResolveElement(SystemSource->root_element,"scheduler",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
            while (element)
            {
                if (  AnsiString(XMLGetValue(element,"name"))  == profileName)
                {
                    return AnsiString(XMLGetValue(element,"id"));

                }
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
}

AnsiString __fastcall TfrmMain :: getNameProfile(int profile)
{
    XML_ELEMENT * element;

    if (SystemSource != NULL)
	{
        element = XMLResolveElement(SystemSource->root_element,"scheduler",NULL);

		if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli

            element = element->child;
			while (element)
            {
                if (  StrToInt(AnsiString(XMLGetValue(element,"id")))  == profile)
                {
                    return AnsiString(XMLGetValue(element,"name"));

                }
                // Passo all'elemento successivo (se esiste)
                element = element->next;
            }
        }
    }
}


void __fastcall TfrmMain::TreeViewChange(TObject *Sender, TTreeNode *Node)
{
    XMLTreeHint(Sender,Node);
}

int ViewChildNode(XML_ELEMENT * element,TTreeView * tree, AnsiString terminator)
{
  int ret_code;
  int level;
  bool finished                       = false;
  bool next_found                     = false;
  XML_ELEMENT           * xFAKE       = NULL;
  XML_ELEMENT           * xHEADER     = NULL;
  XML_ELEMENT           * xELEMENT    = NULL;
  XML_ELEMENT           * xCHILD      = NULL;
  XML_ATTRIBUTE         * xATTRIB     = NULL;
  XML_TEXT              * xTEXT       = NULL;
  XML_COMMENT           * xCOMMENT    = NULL;
  XML_CDATA_SECTION     * xCDATA      = NULL;
  XML_PROC_INSTRUCTION  * xINSTR      = NULL;
  XML_ENTITY            * xENTITY     = NULL;
  XML_ENTITY            * xlENTITY    = NULL; // Lista di sub-entit� per elemento
  int                   * cENTITY     = NULL;
  int                     entity_count;       // Numero di sub-entit� per elemento

  if (true) // Controllo la sorgente
  {
    // Alloco la memoria per una struttura entit� XML di appoggio
    xENTITY = (XML_ENTITY*) malloc(sizeof(XML_ENTITY));


    if (element) // Controllo l'elemento root
    {
      level = 0; // Comincio dal livello 0

      // Creo un elemento fasullo come root virtuale dell'albero
      xFAKE                   = XMLCreateElement("Prova",NULL,NULL);
	  xELEMENT                = xFAKE;
      xELEMENT->child         = element;

      // Numero massimo di livelli in una struttura XML: 1024
	  cENTITY = (int *) malloc(sizeof(int)*1024);
      // Inizializzazioni
      cENTITY[0] = 0; // Salvo il contatore per il livello
      // Numero massimo di sub entit� per elemento: 1024*100 = 102400
      xlENTITY = (XML_ENTITY *) malloc(sizeof(XML_ENTITY*)*1024*100); // Alloco la memoria per le sub-entit�

      while (!finished)
      {
        entity_count = cENTITY[level]; // Recupero il contatore di entit� per il livello dell'albero XML

        // Cerco gli elementi XML figli
        xCHILD = xELEMENT->child;
        while ((xCHILD) && (AnsiString(xCHILD->name) != terminator))
        {
          xlENTITY[entity_count].pos = xCHILD->pos;
          xlENTITY[entity_count].type = XML_ENTITY_ELEMENT;
		  xlENTITY[entity_count].structure = (void *) xCHILD;
          entity_count++;
          // Passo all'elemento successivo (se esiste)
          xCHILD = xCHILD->next;
        }

        cENTITY[level] = entity_count; // Salvo il nuovo contatore per il livello


        // Ciclo di visualizzazione delle entit� XML appena ordinate
        for (int e=0; e<entity_count; e++)
        {
          // Visualizzo il nodo dell'entit� XML e-esima
          ViewNodeEntity(&xlENTITY[e],tree,(TTreeNode*) xELEMENT->tag);
        }

        // Cerco l'elemento successivo da trattare
		if (xELEMENT->child)// Comincio dagli elementi figli
        {
		  xELEMENT = xELEMENT->child;
		  level++; // Vado al livello successivo
          cENTITY[level] = 0; // Azzero il contatore di entit� per il nuovo livello
        }
        else // Altrimenti...
        {
          next_found = false;

          while(!next_found)
          {
            if (xELEMENT->next)// ... cerco tra gli elementi fratelli ...
            {
              if (AnsiString(xELEMENT->next->name) != terminator)
              {
                  cENTITY[level] = 0;
                  xELEMENT = xELEMENT->next; // Passo all'elemento successivo
                  next_found = true; // Asserisco il flag di elemento trovato
			  }
              else // ... oppure mi fermo
              {
                xELEMENT = NULL;
                next_found = true; // Asserisco il flag (ma solo per uscire)
              }
            }
            else // ... ed eventualmente ...
            {

              if ((xELEMENT->parent) && (xELEMENT->parent != element) )// ... torno all'elemento genitore ...
              {
                // Passo al livello superiore
                level--;
                cENTITY[level] = 0;
                xELEMENT = xELEMENT->parent;
              }
              else // ... oppure mi fermo
			  {
                xELEMENT = NULL;
                next_found = true; // Asserisco il flag (ma solo per uscire)
              }
			}
          }
        }
        finished = (xELEMENT)? false:true;
      }
      free(xFAKE);
      free(cENTITY);
      free(xlENTITY);
    }
    else
    {
      ret_code = XML_ERROR_INVALID_ROOT;
    }
  }
  else
  {
    ret_code = XML_ERROR_INVALID_SOURCE;
  }

  free(xENTITY);

  return ret_code;
}

int ViewNodeEntity(XML_ENTITY * xENTITY,TTreeView * pTree,TTreeNode * ParentNode)
{
  int                     ret_code          ;
  int                     icon              ;
  AnsiString              name              ;
  void                  * tag               ;
  TTreeNode             * node              ;
  XML_ELEMENT           * xELEMENT    = NULL;

	if (xENTITY) // Controllo entit�
    {
        if (xENTITY->structure) // Controllo struttura
        {
            name = "<XML_ENTITY>";
			tag = XMLCreateNode(pTree,ParentNode,xENTITY->structure,name,icon);
            node = (TTreeNode*) tag;
            xELEMENT = (XML_ELEMENT*) xENTITY->structure;
            xELEMENT->tag = tag;

            name = xELEMENT->name;

            node->Text = XMLGetValue(xELEMENT,"name");

            if (name == "port")
            {
                icon = 0;
				node->Text = "Porte di comunicazione";

            }
            else if (name == "item")
            {
                if (AnsiString(XMLGetValue(xELEMENT,"type")) == "serial")
                    icon = 2;
                else if (AnsiString(XMLGetValue(xELEMENT,"type")) == "RS232")
                    icon = 8;
				else if (AnsiString(XMLGetValue(xELEMENT,"type")) == "STLC1000_RS485")
                    icon = 9;
                else if (AnsiString(XMLGetValue(xELEMENT,"type")) == "STLC1000_RS422")
                    icon = 10;
                else if (AnsiString(XMLGetValue(xELEMENT,"type")) == "TCP_Client")
                    icon = 11;
            }
			else if (name == "system")
            {
                icon = 1;
                node->Text = "Sistema";
			}
            else if (name == "server")
            {
                icon = 3;
                node->Text = "(" + AnsiString(XMLGetValue(xELEMENT,"SrvID"))+ ") " + node->Text;
            }
			else if (name == "region")
            {
                icon = 4;
                node->Text = "(" + AnsiString(XMLGetValue(xELEMENT,"RegID"))+ ") " + node->Text;
            }
            else if (name == "zone")
            {
                icon = 5;
                node->Text = "(" + AnsiString(XMLGetValue(xELEMENT,"ZonID"))+ ") " + node->Text;
            }
			else if (name == "node")
            {
                icon = 6;
                node->Text = "(" + AnsiString(XMLGetValue(xELEMENT,"NodID"))+ ") " + node->Text;
            }
            else if (name == "device")
            {
                icon = 7;
                node->Text = "(" + AnsiString(XMLGetValue(xELEMENT,"DevID"))+ ") " + node->Text;
            }
            else if (name == "topography")
            {
                icon = 12;
                node->Text = "Topografia";
            }
			else if (name == "station")
            {
                icon = 13;
            }
            else if (name == "building")
            {
				icon = 14;
            }
            else if (name == "location")
            {
                icon = 15;
            }
            else if (name == "addServices")
			{
                icon = 16;
            }
			else if (name == "centralization")
            {
                icon = 17;
            }
			else if (name == "snmp")
            {
				icon = 18;
			}

			node->ImageIndex = icon;
            node->SelectedIndex  = icon;
        }
        else
        {
            ret_code = XML_ERROR_INVALID_ENTITY;
        }
    }
    else
    {
        ret_code = XML_ERROR_INVALID_ENTITY;
    }

    return ret_code;
}


void __fastcall TfrmMain :: ReadElementData()
{

    if ( XmlTreeView->Selected == NULL) return;

    selectedElement = (XML_ELEMENT*) (XmlTreeView->Selected->Data);
	LoadPopUpMenu();
	LoadCurrentTabSheet();
}



XML_ENTITY * GetPrevEntity(XML_SOURCE * XMLSource,XML_ELEMENT * element)
{
     XML_ENTITY * prevEntity = NULL;
     XML_ELEMENT * lastChild = NULL;
     bool notFound = true;
     int level = 0;

	if (element->child != NULL)
    {
        lastChild = element->child;
        while (notFound)
        {
            if (lastChild->next != NULL)
            {
                lastChild = lastChild->next;
            }
            else
            {
                notFound = false;
            }
		}
        prevEntity = XMLGetEntity(XMLSource->first_entity,(void*)lastChild);

        if (lastChild->child != NULL)
        {
            notFound = true;
			level = prevEntity->level;
            prevEntity = prevEntity->next;
            while ((notFound) && (prevEntity != NULL))
            {
               if ((prevEntity->type == XML_ENTITY_ELEMENT_CLOSURE) && (level == prevEntity->level))
               {
                   notFound = false;
               }
               else
               {
                   prevEntity = prevEntity->next;
               }
            }
		}


    }
	else
    {
        prevEntity = XMLGetEntity(XMLSource->first_entity,(void*)element);
       // level = prevEntity->level + 1;
    }

    return prevEntity;
}



void AddElement(XML_SOURCE * XMLSource, XML_ELEMENT * selElement,XML_ELEMENT * newElement)
{
     XML_ENTITY * newEntity = NULL;
     XML_ENTITY * prevEntity = NULL;
     XML_ATTRIBUTE * newAttribute = NULL;
     XML_ENTITY * closureEntity = NULL;
     int level = 0;
     AnsiString closureName = "";

	 prevEntity = GetPrevEntity(XMLSource,selElement);

     if (selElement->child != NULL)
     {
        level = prevEntity->level;
     }
     else
     {
        level = prevEntity->level + 1;
        closureName = "</" + AnsiString(selElement->name) + ">";
        char * tag = XMLStrCpy(NULL,closureName.c_str());
        closureEntity = XMLCreateEntity(XMLSource, NULL, (void*)tag, XML_ENTITY_ELEMENT_CLOSURE, -1);
        closureEntity->level = prevEntity->level;
        XMLAddEntity(prevEntity,closureEntity);
	 }

	 XMLAddElement(selElement,newElement);
     newEntity = XMLCreateEntity(XMLSource, NULL, (void*)newElement, XML_ENTITY_ELEMENT, 0);
     newEntity->level = level;
     XMLAddEntity(prevEntity,newEntity);

}


void __fastcall TfrmMain :: ClearPopUpMenu()
{
    miAddItem->Visible = false;
	miUpdateItem->Visible = false;
    miDelItem->Visible = false;
    miUpdateServer->Visible = false;
    miAddRegion->Visible = false;
    miUpdateRegion->Visible = false;
    miDelRegion->Visible = false;
    miAddZone->Visible = false;
    miUpdateZone->Visible = false;
    miDelZone->Visible = false;
    miAddNode->Visible = false;
	miUpdateNode->Visible = false;
    miDelNode->Visible = false;
    miAddDevice->Visible = false;
    miUpdateDevice->Visible = false;
    miDuplicateDevice->Visible = false;
    miDelDevice->Visible = false;
    miAddStation->Visible = false;
    miUpdateStation->Visible = false;
	miDelStation->Visible = false;
	miAddBuilding->Visible = false;
	miUpdateBuilding->Visible = false;
	miDelBuilding->Visible = false;
	miAddLocation->Visible = false;
	miDelLocation->Visible = false;
	miUpdateLocation->Visible = false;
	miUpdateCentralization->Visible = false;
	miUpdateSNMP->Visible = false;
}


void __fastcall TfrmMain :: LoadPopUpMenu()
{
	AnsiString value;

	ClearPopUpMenu();

	if ( (!isNewNode) && (selectedElement != NULL) && (selectedElement->name != "") )
	{
		value = selectedElement->name ;

		if (value == "port")
		{
			miAddItem->Visible = true;
		}
		else if (value == "item")
		{
			if (AnsiString(XMLGetValue(selectedElement,"type")) == "serial")
			{
				miUpdateItem->ImageIndex = 2;
				miDelItem->ImageIndex = 2;
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "RS232")
			{
				miUpdateItem->ImageIndex = 8;
				miDelItem->ImageIndex = 8;
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "STLC1000_RS485")
			{
				miUpdateItem->ImageIndex = 9;
				miDelItem->ImageIndex = 9;
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "STLC1000_RS422")
			{
				miUpdateItem->ImageIndex = 10;
				miDelItem->ImageIndex = 10;
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "TCP_Client")
			{
				miUpdateItem->ImageIndex = 11;
				miDelItem->ImageIndex = 11;
			}

			miUpdateItem->Visible = true;
			miDelItem->Visible = true;
		}
		else if (value == "server")
		{
			miUpdateServer->Visible = true;
			miAddRegion->Visible = true;
		}
		else if (value == "region")
		{
			if (selectedElement->child == NULL)
			{
				miUpdateRegion->Visible = true;
				miDelRegion->Visible = true;
			}
			miAddZone->Visible = true;
		}
		else if (value == "zone")
		{
			if (selectedElement->child == NULL)
			{
				miUpdateZone->Visible = true;
				miDelZone->Visible = true;
			}
			miAddNode->Visible = true;
		}
		else if (value == "node")
		{
			miUpdateNode->Visible = true;
			if (selectedElement->child == NULL)
				miDelNode->Visible = true;
			miAddDevice->Visible = true;
		}
		else if (value == "device")
		{
			miDuplicateDevice->Visible = true;
			miUpdateDevice->Visible = true;
			if (selectedElement->child == NULL)
				miDelDevice->Visible = true;
		}
		else if (value == "topography")
		{
			miAddStation->Visible = true;
		}
		else if (value == "station")
		{
			miUpdateStation->Visible = true;
			if (selectedElement->child == NULL)
				miDelStation->Visible = true;
			miAddBuilding->Visible = true;
		}
		else if (value == "building")
		{
			miUpdateBuilding->Visible = true;
			if (selectedElement->child == NULL)
				miDelBuilding->Visible = true;
			miAddLocation->Visible = true;
		}
		else if (value == "location")
		{
			miUpdateLocation->Visible = true;
			if (selectedElement->child == NULL)
				miDelLocation->Visible = true;
		}
		else if (value == "centralization")
		{
			miUpdateCentralization->Visible = true;
		}
		else if (value == "snmp")
		{
			miUpdateSNMP->Visible = true;
		}
	}
}

void __fastcall TfrmMain :: LoadCurrentTabSheet()
{
	AnsiString value;
	AnsiString sIPItemValue,sIPDeviceValue,sAddrNodeValue;
	AnsiString sPosDeviceValue;
	AnsiString sSNMPCommunity = "";
	AnsiString sSNMPCommunityDefault = "";
	int i=0;
	int iTempValue = 0;

	if (selectedElement != NULL)
	{
		value = selectedElement->name ;

		if ((value == "port") || (value == "system") || (value == "topography") || (value == "addServices"))
		{
			ClientPage->ActivePage = tsNull;
			ImageHeader->Picture = NULL;
			lblHeader->Caption = "";
		}
		else if (value == "item")
		{
			ClientPage->ActivePage = tsItem;

			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(2,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Porta";


			eNameItem->Enabled = false;
			cbTypeItem->Enabled = false;
			cbCOMItem->Enabled = false;
			cbBaudItem->Enabled = false;
			cbEchoItem->Enabled = false;
			cbDataItem->Enabled = false;
			cbStopItem->Enabled = false;
			cbParityItem->Enabled = false;
			eIP1Item->Enabled = false;
			eIP2Item->Enabled = false;
			eIP3Item->Enabled = false;
			eIP4Item->Enabled = false;
			ePortItem->Enabled = false;
			eTimeOutItem->Enabled = false;
			cbPersistentItem->Enabled = false;


			lblIDItem->Caption = AnsiString(XMLGetValue(selectedElement,"id"));
			eNameItem->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			if (AnsiString(XMLGetValue(selectedElement,"type")) == "serial")
			{
				cbTypeItem->ItemIndex = 0;
				ImageHeader->Picture = NULL;
				ImageList48->GetBitmap(2,ImageHeader->Picture->Bitmap);
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "RS232")
			{
				cbTypeItem->ItemIndex = 1;
				ImageHeader->Picture = NULL;
				ImageList48->GetBitmap(8,ImageHeader->Picture->Bitmap);
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "STLC1000_RS485")
			{
				cbTypeItem->ItemIndex = 2;
				ImageHeader->Picture = NULL;
				ImageList48->GetBitmap(9,ImageHeader->Picture->Bitmap);
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "STLC1000_RS422")
			{
				cbTypeItem->ItemIndex = 3;
				ImageHeader->Picture = NULL;
				ImageList48->GetBitmap(10,ImageHeader->Picture->Bitmap);
			}
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "TCP_Client")
			{
				cbTypeItem->ItemIndex = 4;
				ImageHeader->Picture = NULL;
				ImageList48->GetBitmap(11,ImageHeader->Picture->Bitmap);
			}

			eTimeOutItem->Text = AnsiString(XMLGetValue(selectedElement,"timeout"));
			if (AnsiString(XMLGetValue(selectedElement,"persistent")) == "true")
				cbPersistentItem->ItemIndex = 0;
			else
				cbPersistentItem->ItemIndex = 1;

			SetControlByTypeItem();
			if (cbTypeItem->ItemIndex != 4)
			{
				if (AnsiString(XMLGetValue(selectedElement,"com")) != "")
					cbCOMItem->ItemIndex = StrToInt(AnsiString(XMLGetValue(selectedElement,"com")))-1;
				else
					cbCOMItem->ItemIndex = 0;

				cbBaudItem->Text = AnsiString(XMLGetValue(selectedElement,"baud"));

				if (AnsiString(XMLGetValue(selectedElement,"echo")) == "true")
					cbEchoItem->ItemIndex = 0;
				else
					cbEchoItem->ItemIndex = 1;

				if (AnsiString(XMLGetValue(selectedElement,"data")) != "")
					cbDataItem->ItemIndex = StrToInt(AnsiString(XMLGetValue(selectedElement,"data")))-5;
				else
					cbDataItem->ItemIndex = 0;

				if (AnsiString(XMLGetValue(selectedElement,"stop")) != "")
					cbStopItem->ItemIndex = StrToInt(AnsiString(XMLGetValue(selectedElement,"stop")));
				else
					cbStopItem->ItemIndex = 0;

				if (AnsiString(XMLGetValue(selectedElement,"parity")) == "N")
					cbParityItem->ItemIndex = 0;
				else if (AnsiString(XMLGetValue(selectedElement,"parity")) == "O")
					cbParityItem->ItemIndex = 1;
				else if (AnsiString(XMLGetValue(selectedElement,"parity")) == "E")
					cbParityItem->ItemIndex = 2;
				else if (AnsiString(XMLGetValue(selectedElement,"parity")) == "M")
					cbParityItem->ItemIndex = 3;
				else if (AnsiString(XMLGetValue(selectedElement,"parity")) == "S")
					cbParityItem->ItemIndex = 4;
			 }
			else
			{
				sIPItemValue = AnsiString(XMLGetValue(selectedElement,"ip"));
				char * ipTokenValue = NULL;
				ipTokenValue = XMLGetToken(sIPItemValue.c_str(),".",0);
				eIP1Item->Text = ipTokenValue;
				free(ipTokenValue);

				ipTokenValue = XMLGetToken(sIPItemValue.c_str(),".",1);
				eIP2Item->Text = ipTokenValue;
				free(ipTokenValue);

				ipTokenValue = XMLGetToken(sIPItemValue.c_str(),".",2);
				eIP3Item->Text = ipTokenValue;
				free(ipTokenValue);

				ipTokenValue = XMLGetToken(sIPItemValue.c_str(),".",3);
				eIP4Item->Text = ipTokenValue;
				free(ipTokenValue);

				ePortItem->Text = AnsiString(XMLGetValue(selectedElement,"port"));
			}

			checkTextEditIsValid(eNameItem);
			checkTextComboBoxInItemList(cbTypeItem);
			checkTextComboBoxInItemList(cbCOMItem);
			checkTextComboBoxInItemList(cbBaudItem);
			checkTextComboBoxInItemList(cbEchoItem);
			checkTextComboBoxInItemList(cbDataItem);
			checkTextComboBoxInItemList(cbStopItem);
			checkTextComboBoxInItemList(cbParityItem);
			checkTextEditIsValid(eIP1Item);
			checkTextEditIsValid(eIP2Item);
			checkTextEditIsValid(eIP3Item);
			checkTextEditIsValid(eIP4Item);
			checkTextEditIsValid(ePortItem);
			checkTextEditIsValid(eTimeOutItem);
			checkTextComboBoxInItemList(cbPersistentItem);
		}
		else if (value == "server")
		{
			ClientPage->ActivePage = tsServer;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(3,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Server";

			eNameServer->Enabled = false;
			eHostServer->Enabled = false;

			lblIDServer->Caption = AnsiString(XMLGetValue(selectedElement,"SrvID"));
			eHostServer->Text = AnsiString(XMLGetValue(selectedElement,"host"));
			eNameServer->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			checkTextEditIsValid(eNameServer);
			checkTextEditIsValid(eHostServer);
		}
		else if (value == "region")
		{
			ClientPage->ActivePage = tsRegion;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(4,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Compartimento";

			cbNameRegion->Enabled = false;

			lblIDRegion->Caption = AnsiString(XMLGetValue(selectedElement,"RegID"));
			cbNameRegion->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			LoadNameRegionCombo();

			checkTextComboBoxInItemList(cbNameRegion);
		}
		else if (value == "zone")
		{
			ClientPage->ActivePage = tsZone;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(5,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Linea";

			cbNameZone->Enabled = false;

			lblIDZone->Caption = AnsiString(XMLGetValue(selectedElement,"ZonID"));
			cbNameZone->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			LoadNameZoneCombo();

			checkTextComboBoxInItemList(cbNameZone);
		}
		else if (value == "node")
		{
			ClientPage->ActivePage = tsNode;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(6,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Stazione";

			cbNameNode->Enabled = false;
			cbAddrModemNode->Enabled = false;
	   //     cbAddrModemNode->Text = "";

			cbLocal->Enabled = false;

			lblIDNode->Caption = AnsiString(XMLGetValue(selectedElement,"NodID"));
			cbNameNode->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			lblSizeModemNode->Caption = AnsiString(XMLGetValue(selectedElement,"modem_size"));

			if (XMLGetValue(selectedElement,"local") != NULL)
			{
				if (AnsiString(XMLGetValue(selectedElement,"local")) == "true")
					cbLocal->Checked = true;
				else
					cbLocal->Checked = false;
			}
			else
			{
				cbLocal->Checked = false;
			}


			sAddrNodeValue = AnsiString(XMLGetValue(selectedElement,"modem_addr"));

			if (sAddrNodeValue == "Nessun Modem")
			{
				cbAddrModemNode->ItemIndex = 0;
			}
			else
			{
				char * ipTokenValue = XMLGetToken(sAddrNodeValue.c_str()," ",0);
				if ((ipTokenValue != NULL))
				{
					unsigned __int8* pValue8 =  (unsigned __int8*)(XMLASCII2Type(ipTokenValue,t_u_hex_8));
					int modemAddr = * pValue8;
					if (modemAddr == 255)              // 0xFF = 255
					   modemAddr = -1;
					cbAddrModemNode->ItemIndex = modemAddr + 1; //StrToInt(ipTokenValue);
					free(pValue8);
					free(ipTokenValue);
				}
				else
				{
					cbAddrModemNode->Text = "";
				}
			}

			LoadNameNodeCombo();

			checkTextComboBoxInItemList(cbNameNode);
			checkTextComboBoxInItemList(cbAddrModemNode);

		}
		else if (value == "device")
		{
			ClientPage->ActivePage = tsDevice;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(7,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Periferica";

			eIDDevice->Enabled = false;
			btnGridLocPosition->Enabled = false;
			eNameDevice->Enabled = false;
			eIP1Device->Enabled = false;
			eIP2Device->Enabled = false;
			eIP3Device->Enabled = false;
			eIP4Device->Enabled = false;
			cbIPDevice->Enabled = false;
			eSNDevice->Enabled = false;
			cbPortDevice->Enabled = false;
			eSNMPCommunity->Enabled = false;
			cbTypeDevice->Enabled = false;
			cbStationDevice->Enabled = false;
			cbBuildingDevice->Enabled = false;
			cbLocationDevice->Enabled = false;
			cbProfileDevice->Enabled = false;
			cbActiveDevice->Enabled = false;
			cbScheduledDevice->Enabled = false;
            cbSupervisorId->Enabled = false;

			eIDDevice->Text = AnsiString(XMLGetValue(selectedElement,"DevID"));
			eNameDevice->Text = AnsiString(XMLGetValue(selectedElement,"name"));

			sPosDeviceValue = AnsiString(XMLGetValue(selectedElement,"position"));
			char * posTokenValue = NULL;
			posTokenValue = XMLGetToken(sPosDeviceValue.c_str(),",",0);
			lblPosXDevice->Caption = posTokenValue;
			free(posTokenValue);

			posTokenValue = XMLGetToken(sPosDeviceValue.c_str(),",",1);
			lblPosYDevice->Caption = posTokenValue;
			free(posTokenValue);

			if (lblPosXDevice->Caption != "")
			{
				lblPosXDevice->Font->Color = clWindowText;
				lblPosYDevice->Font->Color = clWindowText;
			}
			else
			{
				lblPosXDevice->Caption = "0";
				lblPosYDevice->Caption = "0";
				lblPosXDevice->Font->Color = clRed;
				lblPosYDevice->Font->Color = clRed;
			}

			eSNDevice->Text = AnsiString(XMLGetValue(selectedElement,"SN"));

			if (AnsiString(XMLGetValue(selectedElement,"active")) == "true")
				cbActiveDevice->ItemIndex = 0;
			else
				cbActiveDevice->ItemIndex = 1;

			if (AnsiString(XMLGetValue(selectedElement,"scheduled")) == "true")
				cbScheduledDevice->ItemIndex = 0;
			else
				cbScheduledDevice->ItemIndex = 1;

			LoadPortDeviceCombo();
			if (AnsiString(XMLGetValue(selectedElement,"port")) != "")
			{
				lbl_IP_COM_Address->Visible = true;
				cbPortDevice->Text = getNamePort(StrToInt(AnsiString(XMLGetValue(selectedElement,"port"))));
				sIPDeviceValue = AnsiString(XMLGetValue(selectedElement,"addr"));
				char * ipTokenValue = NULL;

				if (getTypePort(StrToInt(AnsiString(XMLGetValue(selectedElement,"port")))) == "TCP_Client")
				{
					cbIPDevice->Visible = false;

					cbIPDevice->Text = "Seleziona ->";

					eIP1Device->Visible = true;
					eIP2Device->Visible = true;
					eIP3Device->Visible = true;
					eIP4Device->Visible = true;


					ipTokenValue = XMLGetToken(sIPDeviceValue.c_str(),".",0);
					eIP1Device->Text = ipTokenValue;
					free(ipTokenValue);

					ipTokenValue = XMLGetToken(sIPDeviceValue.c_str(),".",1);
					eIP2Device->Text = ipTokenValue;
					free(ipTokenValue);

					ipTokenValue = XMLGetToken(sIPDeviceValue.c_str(),".",2);
					eIP3Device->Text = ipTokenValue;
					free(ipTokenValue);

					ipTokenValue = XMLGetToken(sIPDeviceValue.c_str(),".",3);
					eIP4Device->Text = ipTokenValue;
					free(ipTokenValue);

                    cbSupervisorId->Visible = true;
            		lblSupervisorId->Visible = true;

                    if (AnsiString(XMLGetValue(selectedElement,"supervisor_id")) == "1") {
                        cbSupervisorId->ItemIndex = 1;
                    }
                    else {
                        cbSupervisorId->ItemIndex = 0;
	                }
				}
				else
				{

					eIP1Device->Visible = false;
					eIP2Device->Visible = false;
					eIP3Device->Visible = false;
					eIP4Device->Visible = false;

					eIP1Device->Text = "";
					eIP2Device->Text = "";
					eIP3Device->Text = "";
					eIP4Device->Text = "";

					cbIPDevice->Visible = true;

					cbIPDevice->ItemIndex = DecodificaDeviceAddress(XMLGetValue(selectedElement->parent,"modem_addr"),XMLGetValue(selectedElement,"addr"));
					cbIPDevice->Text = cbIPDevice->Items->Strings[cbIPDevice->ItemIndex];

       	            cbSupervisorId->Visible = false;
		            lblSupervisorId->Visible = false;
				}
			}
			else
			{
				cbPortDevice->Text = "";

				lbl_IP_COM_Address->Visible = false;

				cbIPDevice->Visible = false;

				cbIPDevice->Text = "Seleziona ->";

				eIP1Device->Visible = false;
				eIP2Device->Visible = false;
				eIP3Device->Visible = false;
				eIP4Device->Visible = false;

				eIP1Device->Text = "";
				eIP2Device->Text = "";
				eIP3Device->Text = "";
				eIP4Device->Text = "";

   	            cbSupervisorId->Visible = false;
	            lblSupervisorId->Visible = false;
			}

			LoadStationDeviceCombo();
			if (AnsiString(XMLGetValue(selectedElement,"station")) != "")
			{
				cbStationDevice->Text = getNameStation(StrToInt(AnsiString(XMLGetValue(selectedElement,"station"))));
			}
			else
			{
				cbStationDevice->Text = "";
			}


			LoadBuildingDeviceCombo();
			if (AnsiString(XMLGetValue(selectedElement,"building")) != "")
			{
				cbBuildingDevice->Text = getNameBuilding(StrToInt(AnsiString(XMLGetValue(selectedElement,"building"))));
			}
			else
			{
				cbBuildingDevice->Text = "";
			}

			LoadLocationDeviceCombo();
			if (AnsiString(XMLGetValue(selectedElement,"location")) != "")
			{
				cbLocationDevice->Text = getNameLocation(StrToInt(AnsiString(XMLGetValue(selectedElement,"location"))));
			}
			else
			{
				cbLocationDevice->Text = "";
			}

			LoadProfileDeviceCombo();
			if (AnsiString(XMLGetValue(selectedElement,"profile")) != "")
			{
				cbProfileDevice->Text = getNameProfile(StrToInt(AnsiString(XMLGetValue(selectedElement,"profile"))));
			}
			else
			{
				cbProfileDevice->Text = "";
			}

			if (AnsiString(XMLGetValue(selectedElement,"type")) != "")
			{
				cbTypeDevice->Text = getNameTypeDevice(AnsiString(XMLGetValue(selectedElement,"type")));
			}
			else
			{
				cbTypeDevice->Text = "";
			}

			sSNMPCommunityDefault = getSNMPCommunityDevice(AnsiString(XMLGetValue(selectedElement,"type")));
			if (sSNMPCommunityDefault != "")
			{
				lblSNMPCommunity->Visible = true;
				eSNMPCommunity->Visible = true;
				sSNMPCommunity = AnsiString(XMLGetValue(selectedElement,"snmp_community"));

			   /*	if (sSNMPCommunity != "" )
				{*/
					eSNMPCommunity->Text = sSNMPCommunity;
			   /*	}
				else
				{
					eSNMPCommunity->Text = sSNMPCommunityDefault;
				}
				 */
				checkTextEditIsValid(eSNMPCommunity);
			}
			else
			{
				lblSNMPCommunity->Visible = false;
				eSNMPCommunity->Visible = false;
			}

			checkTextEditIsValid(eIDDevice);
			checkTextEditIsValid(eNameDevice);
			checkTextEditIsValid(eIP1Device);
			checkTextEditIsValid(eIP2Device);
			checkTextEditIsValid(eIP3Device);
			checkTextEditIsValid(eIP4Device);
			checkTextComboBoxInItemList(cbIPDevice);
			checkTextEditIsValid(eSNDevice);
			checkTextComboBoxInItemList(cbPortDevice);
			checkTextComboBoxInItemList(cbTypeDevice);
			checkTextComboBoxInItemList(cbStationDevice);
			checkTextComboBoxInItemList(cbBuildingDevice);
			checkTextComboBoxInItemList(cbLocationDevice);
			checkTextComboBoxInItemList(cbProfileDevice);
			checkTextComboBoxInItemList(cbActiveDevice);
			checkTextComboBoxInItemList(cbScheduledDevice);
			checkTextComboBoxInItemList(cbSupervisorId);
		}
		else if (value == "station")
		{
			ClientPage->ActivePage = tsStation;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(13,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Stazione";

			eNameStation->Enabled = false;

			lblIDStation->Caption = AnsiString(XMLGetValue(selectedElement,"id"));
			eNameStation->Text = AnsiString(XMLGetValue(selectedElement,"name"));

			checkTextEditIsValid(eNameStation);

		}
		else if (value == "building")
		{
			ClientPage->ActivePage = tsBuilding;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(14,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Edificio";

			eNameBuilding->Enabled = false;
			eNoteBuilding->Enabled = false;

			lblIDBuilding->Caption = AnsiString(XMLGetValue(selectedElement,"id"));
			eNameBuilding->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			eNoteBuilding->Text = AnsiString(XMLGetValue(selectedElement,"note"));

			checkTextEditIsValid(eNameBuilding);
			checkTextEditIsValid(eNoteBuilding);

		}
		else if (value == "location")
		{
			ClientPage->ActivePage = tsLocation;
			ImageHeader->Picture = NULL;
			ImageList48->GetBitmap(15,ImageHeader->Picture->Bitmap);
			lblHeader->Caption = "Armadio";

			eNameLocation->Enabled = false;
			eNoteLocation->Enabled = false;
			cbTypeLocation->Enabled = false;

			lblIDLocation->Caption = AnsiString(XMLGetValue(selectedElement,"id"));
			eNameLocation->Text = AnsiString(XMLGetValue(selectedElement,"name"));
			eNoteLocation->Text = AnsiString(XMLGetValue(selectedElement,"note"));

			if (isNewNode) cbTypeLocation->Text = "";

			if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS24")
				cbTypeLocation->ItemIndex = 0;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS24S")
				cbTypeLocation->ItemIndex = 1;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS20")
				cbTypeLocation->ItemIndex = 2;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS20S")
				cbTypeLocation->ItemIndex = 3;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS16")
				cbTypeLocation->ItemIndex = 4;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS16S")
				cbTypeLocation->ItemIndex = 5;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS12")
				cbTypeLocation->ItemIndex = 6;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS12S")
				cbTypeLocation->ItemIndex = 7;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS9")
				cbTypeLocation->ItemIndex = 8;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS9S")
				cbTypeLocation->ItemIndex = 9;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS8")
				cbTypeLocation->ItemIndex = 10;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS8S")
				cbTypeLocation->ItemIndex = 11;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS6")
				cbTypeLocation->ItemIndex = 12;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS6S")
				cbTypeLocation->ItemIndex = 13;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS4")
				cbTypeLocation->ItemIndex = 14;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS4S")
				cbTypeLocation->ItemIndex = 15;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS3")
				cbTypeLocation->ItemIndex = 16;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "ATPS3S")
				cbTypeLocation->ItemIndex = 17;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "SUPPTELEIND")
				cbTypeLocation->ItemIndex = 18;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "SUPPDIFF")
				cbTypeLocation->ItemIndex = 19;
			else if (AnsiString(XMLGetValue(selectedElement,"type")) == "RACK24U")
				cbTypeLocation->ItemIndex = 20;


			checkTextEditIsValid(eNameLocation);
			checkTextEditIsValid(eNoteLocation);
			checkTextComboBoxInItemList(cbTypeLocation);

		}
		else if (value == "centralization")
		{
			try
			{
				if (ClientPage->ActivePage != tsCentralization)
				{
					ClientPage->ActivePage = tsCentralization;
					ImageHeader->Picture = NULL;
					ImageList48->GetBitmap(17,ImageHeader->Picture->Bitmap);
					lblHeader->Caption = "Servizio di Centralizzazione";

					eHostNameCentralization->Enabled = false;
					eIntervalSeconds->Enabled = false;
					cbActiveCentralizationService->Enabled = false;

					//SetcbActiveCentralizationService();
					//centralizationElement = GetHostCentralizationNode();
					loadEditsCentralization();

				}
			}
			catch(...)
			{}
		}
		else if (value == "snmp")
		{
			try
			{
				if (ClientPage->ActivePage != tsSNMPManager)
				{
					ClientPage->ActivePage = tsSNMPManager;
					ImageHeader->Picture = NULL;
					ImageList48->GetBitmap(18,ImageHeader->Picture->Bitmap);
					lblHeader->Caption = "SNMP Manager";

					eCommunity->Enabled = false;
					eTrapCommunity->Enabled = false;
					cbUseSystemXMLCommunity->Enabled = false;
					loadControlSNMPManager();
				}
			}
			catch(...)
			{}
		}

	}
}


void __fastcall TfrmMain::actSaveItemUpdate(TObject *Sender)
{
	if ((cbTypeItem->Text.Trim().Length() == 0) ||
	   (!checkTextComboBoxIsValid(cbTypeItem)) )
	{
		actSaveItem->Enabled = false;
		return;
	}

	if (cbTypeItem->ItemIndex != 4)
	{
		if ((lblIDItem->Caption.Trim().Length() > 0) &&
			(eNameItem->Text.Trim().Length() > 0) &&
			(checkTextComboBoxIsValid(cbCOMItem)) &&
			(checkTextComboBoxIsValid(cbBaudItem)) &&
			(checkTextComboBoxIsValid(cbEchoItem)) &&
			(checkTextComboBoxIsValid(cbDataItem)) &&
			(checkTextComboBoxIsValid(cbStopItem)) &&
			(checkTextComboBoxIsValid(cbParityItem)) &&
			(eTimeOutItem->Text.Trim().Length() > 0) &&
			(checkTextComboBoxIsValid(cbPersistentItem)) )
		{
		   actSaveItem->Enabled = true;
		}
		else
		{
		   actSaveItem->Enabled = false;
		}
	}
	else
	{
		if ((lblIDItem->Caption.Trim().Length() > 0) &&
			(eNameItem->Text.Trim().Length() > 0) &&
			(eIP1Item->Text.Trim().Length() > 0) &&
			(eIP2Item->Text.Trim().Length() > 0) &&
			(eIP3Item->Text.Trim().Length() > 0) &&
			(eIP4Item->Text.Trim().Length() > 0) &&
			(ePortItem->Text.Trim().Length() > 0) &&
			(eTimeOutItem->Text.Trim().Length() > 0) &&
			(checkTextComboBoxIsValid(cbPersistentItem)))
		{
		   actSaveItem->Enabled = true;
		}
		else
		{
		   actSaveItem->Enabled = false;
		}

	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveItemExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

    XML_ATTRIBUTE * newAttribute = NULL;
    XML_ATTRIBUTE * attribute;
    XML_ATTRIBUTE * typeAttribute;
    int iTempValue=0;
    AnsiString sTempCOM = "";
    AnsiString sIPValue = "";
    AnsiString errMsg = "";

    if  (eNameItem->Text.Length() > MAX_LENGTH_NOME)
    {
		errMsg = " Errore inserimento Nome.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

    try{
		if  (!(checkInt(eTimeOutItem->Text)))
        {
            MessageBox(NULL, " Errore inserimento valore di TimeOut.\r\n Inserire un valore numerico intero positivo.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
            return;
        }

        if (((StrToInt(eTimeOutItem->Text)) < 0) || ((StrToInt(eTimeOutItem->Text)) > MAX_INTEGER )) // (2^31) - 1 = 2147483647
        {
			errMsg = " Errore inserimento valore di TimeOut.\r\n Inserire un valore compreso tra 0 e " + (IntToStr(MAX_INTEGER))+".";
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
            return;
        }

		if (ePortItem->Visible)
        {
            if (!(checkInt(ePortItem->Text)))
			{
                MessageBox(NULL, " Errore inserimento numero Port.\r\n Inserire un valore numerico intero positivo.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
				return;
            }
        }

        if (ePortItem->Visible)
        {
            if (((StrToInt(ePortItem->Text)) < 1) || ((StrToInt(ePortItem->Text)) > MAX_NUM_TCPIP_PORT)) // (2^16) - 1 = 65535
            {
                errMsg = " Errore inserimento numero Port.\r\n Inserire un valore compreso tra 1 e " + (IntToStr(MAX_NUM_TCPIP_PORT))+".";
                MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
                return;
            }
        }

        if (eIP1Item->Visible)
        {
            if ( !(checkInt(eIP1Item->Text)) || !(checkInt(eIP2Item->Text)) ||
                 !(checkInt(eIP3Item->Text)) || !(checkInt(eIP4Item->Text)) )
            {
                MessageBox(NULL, " Errore inserimento Indirizzo IP.\r\n Inserire 4 valori numerici interi positivi.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
                return;
            }
        }

        if (eIP1Item->Visible)
        {
			if ( ((StrToInt(eIP1Item->Text)) < 0) || ((StrToInt(eIP1Item->Text)) > MAX_NUM_IP) ||
				 ((StrToInt(eIP2Item->Text)) < 0) || ((StrToInt(eIP2Item->Text)) > MAX_NUM_IP) ||
                 ((StrToInt(eIP3Item->Text)) < 0) || ((StrToInt(eIP3Item->Text)) > MAX_NUM_IP) ||
                 ((StrToInt(eIP4Item->Text)) < 0) || ((StrToInt(eIP4Item->Text)) > MAX_NUM_IP) )
            {
                errMsg = " Errore inserimento Indirizzo IP.\r\n Inserire 4 valori compresi tra 0 e " + (IntToStr(MAX_NUM_IP))+".";
				MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
                return;
            }
        }

    }
	catch (EConvertError &E)
	{
		errMsg = " Errore inserimento valore intero.\r\n Inserire valori compresi tra 0 e " + (IntToStr(MAX_INTEGER))+".";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

    if (cbCOMItem->Visible)
    {
        attribute = XMLGetAttrib(selectedElement,"com");
        if (attribute  != NULL)
        {
            sTempCOM = AnsiString(XMLGetValue(selectedElement,"com"));
            XMLRemoveAttrib(selectedElement,attribute);
        }

        iTempValue = cbCOMItem->ItemIndex + 1;
        if (IsCOMUsed(IntToStr(iTempValue)))
        {
            if (sTempCOM != "")
            {
                newAttribute = XMLNewAttrib(selectedElement,"com",sTempCOM.c_str());
                XMLAddAttrib(selectedElement,newAttribute);
			}
            MessageBox(NULL, "COM gi� associata ad una porta.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
            return;
		}
    }

    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
	{
        XMLSetValue(attribute,eNameItem->Text.Trim().c_str());
    }
	else
    {
		newAttribute = XMLNewAttrib(selectedElement,"name",eNameItem->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }


    typeAttribute = XMLGetAttrib(selectedElement,"type");
    if (typeAttribute == NULL)
    {
        typeAttribute = XMLNewAttrib(selectedElement,"type","");
        XMLAddAttrib(selectedElement,typeAttribute);
    }

    if (typeAttribute  != NULL)
    {
        if (cbTypeItem->ItemIndex != 4)
		{
            if ((cbTypeItem->ItemIndex == 0))
                XMLSetValue(typeAttribute,"serial");
            else if ((cbTypeItem->ItemIndex == 1))
                XMLSetValue(typeAttribute,"RS232");
            else if ((cbTypeItem->ItemIndex == 2))
				XMLSetValue(typeAttribute,"STLC1000_RS485");
            else if ((cbTypeItem->ItemIndex == 3))
                XMLSetValue(typeAttribute,"STLC1000_RS422");

            attribute = XMLGetAttrib(selectedElement,"ip");
			if (attribute  != NULL)
            {
                XMLRemoveAttrib(selectedElement,attribute);
            }
            attribute = XMLGetAttrib(selectedElement,"port");
			if (attribute  != NULL)
            {
				XMLRemoveAttrib(selectedElement,attribute);
            }

            attribute = XMLGetAttrib(selectedElement,"com");
			iTempValue = cbCOMItem->ItemIndex + 1;
            if (attribute  != NULL)
            {
                XMLSetValue(attribute,IntToStr(iTempValue).c_str());
            }
            else
            {
                newAttribute = XMLNewAttrib(selectedElement,"com",IntToStr(iTempValue).c_str());
                XMLAddAttrib(selectedElement,newAttribute);
            }

            attribute = XMLGetAttrib(selectedElement,"baud");
            if (attribute  != NULL)
            {
                XMLSetValue(attribute,cbBaudItem->Text.Trim().c_str());
            }
            else
            {
                newAttribute = XMLNewAttrib(selectedElement,"baud",cbBaudItem->Text.Trim().c_str());
				XMLAddAttrib(selectedElement,newAttribute);
            }

            attribute = XMLGetAttrib(selectedElement,"echo");
            if (attribute  != NULL)
            {
                if (cbEchoItem->ItemIndex == 0)
				{
                    XMLSetValue(attribute,"true");
                }
                else
                {
					XMLSetValue(attribute,"false");
				}
            }
            else
            {
                if (cbEchoItem->ItemIndex == 0)
                {
					newAttribute = XMLNewAttrib(selectedElement,"echo","true");
                }
                else
                {
                    newAttribute = XMLNewAttrib(selectedElement,"echo","false");
                }
                XMLAddAttrib(selectedElement,newAttribute);
            }

            attribute = XMLGetAttrib(selectedElement,"data");
            iTempValue = cbDataItem->ItemIndex + 5;
            if (attribute  != NULL)
            {
                XMLSetValue(attribute,IntToStr(iTempValue).c_str());
            }
            else
			{
                newAttribute = XMLNewAttrib(selectedElement,"data",IntToStr(iTempValue).c_str());
                XMLAddAttrib(selectedElement,newAttribute);
            }

            attribute = XMLGetAttrib(selectedElement,"stop");
            iTempValue = cbStopItem->ItemIndex;
            if (attribute  != NULL)
            {
				XMLSetValue(attribute,IntToStr(iTempValue).c_str());
            }
            else
            {
                newAttribute = XMLNewAttrib(selectedElement,"stop",IntToStr(iTempValue).c_str());
				XMLAddAttrib(selectedElement,newAttribute);
            }

            attribute = XMLGetAttrib(selectedElement,"parity");
            if (attribute  != NULL)
            {
                if (cbParityItem->ItemIndex == 0)
                    XMLSetValue(attribute,"N");
				else if (cbParityItem->ItemIndex == 1)
                    XMLSetValue(attribute,"O");
                else if (cbParityItem->ItemIndex == 2)
                    XMLSetValue(attribute,"E");
                else if (cbParityItem->ItemIndex == 3)
                    XMLSetValue(attribute,"M");
                else if (cbParityItem->ItemIndex == 4)
                    XMLSetValue(attribute,"S");
            }
            else
            {
                if (cbParityItem->ItemIndex == 0)
                    newAttribute = XMLNewAttrib(selectedElement,"parity","N");
				else if (cbParityItem->ItemIndex == 1)
                    newAttribute = XMLNewAttrib(selectedElement,"parity","O");
                else if (cbParityItem->ItemIndex == 2)
                    newAttribute = XMLNewAttrib(selectedElement,"parity","E");
                else if (cbParityItem->ItemIndex == 3)
                    newAttribute = XMLNewAttrib(selectedElement,"parity","M");
                else if (cbParityItem->ItemIndex == 4)
                    newAttribute = XMLNewAttrib(selectedElement,"parity","S");

                XMLAddAttrib(selectedElement,newAttribute);
            }
		}
        else
        {
            XMLSetValue(typeAttribute,"TCP_Client");

			attribute = XMLGetAttrib(selectedElement,"com");
            if (attribute  != NULL)
            {
                XMLRemoveAttrib(selectedElement,attribute);
            }

            attribute = XMLGetAttrib(selectedElement,"baud");
            if (attribute  != NULL)
            {
				XMLRemoveAttrib(selectedElement,attribute);
            }

            attribute = XMLGetAttrib(selectedElement,"echo");
            if (attribute  != NULL)
            {
                XMLRemoveAttrib(selectedElement,attribute);
            }
            attribute = XMLGetAttrib(selectedElement,"data");
            if (attribute  != NULL)
			{
                XMLRemoveAttrib(selectedElement,attribute);
            }

            attribute = XMLGetAttrib(selectedElement,"stop");
            if (attribute  != NULL)
            {
                XMLRemoveAttrib(selectedElement,attribute);
            }
            attribute = XMLGetAttrib(selectedElement,"parity");
            if (attribute  != NULL)
            {
                XMLRemoveAttrib(selectedElement,attribute);
			}

            attribute = XMLGetAttrib(selectedElement,"ip");
			sIPValue = eIP1Item->Text.Trim() + "." + eIP2Item->Text.Trim() + "." +
                       eIP3Item->Text.Trim() + "." + eIP4Item->Text.Trim();
			if (attribute  != NULL)
            {
                XMLSetValue(attribute,sIPValue.c_str());
            }
            else
            {
                newAttribute = XMLNewAttrib(selectedElement,"ip",sIPValue.c_str());
                XMLAddAttrib(selectedElement,newAttribute);
            }

			attribute = XMLGetAttrib(selectedElement,"port");
            if (attribute  != NULL)
            {
                XMLSetValue(attribute,ePortItem->Text.Trim().c_str());
            }
            else
            {
				newAttribute = XMLNewAttrib(selectedElement,"port",ePortItem->Text.Trim().c_str());
                XMLAddAttrib(selectedElement,newAttribute);
            }
        }
    }

    attribute = XMLGetAttrib(selectedElement,"timeout");
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,eTimeOutItem->Text.Trim().c_str());
    }
    else
    {
        newAttribute = XMLNewAttrib(selectedElement,"timeout",eTimeOutItem->Text.Trim().c_str());
		XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"persistent");
    if (attribute  != NULL)
	{
        if (cbPersistentItem->ItemIndex == 0)
        {
            XMLSetValue(attribute,"true");
        }
        else
        {
            XMLSetValue(attribute,"false");
        }
    }
    else
	{
        if (cbPersistentItem->ItemIndex == 0)
        {
            newAttribute = XMLNewAttrib(selectedElement,"persistent","true");
		}
        else
        {
            newAttribute = XMLNewAttrib(selectedElement,"persistent","false");
        }
        XMLAddAttrib(selectedElement,newAttribute);
    }

    EnabledContextForm(true);

    btnSaveItem->Visible = false;
    btnCancelItem->Visible = false;

	if (isNewNode)
    {
        LoadTreeView();
        LoadPopUpMenu();
	}
	else
    {
        XmlTreeView->Selected->Text = eNameItem->Text;
    }

	isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;
	
	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddItemClick(TObject *Sender)
{
    XML_ELEMENT * newElement = NULL;
    XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString sID = "";
	int i = 0;
	AnsiString sCOMValue = "";
	isNewNode = true;

    newElement = XMLCreateElement("item",NULL,NULL);
    sID = IntToStr(getMaxIDPort()+1);
    newAttribute = XMLNewAttrib(newElement,"id",sID.c_str());
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"name","");
    XMLAddAttrib(newElement,newAttribute);
//    newAttribute = XMLNewAttrib(newElement,"type","TCP_Client");
    newAttribute = XMLNewAttrib(newElement,"type","serial");
    XMLAddAttrib(newElement,newAttribute);

    for (i=1;32;i++)
    {
        if (!(IsCOMUsed(IntToStr(i))))
        {
            sCOMValue = IntToStr(i);
			break;
		}
    }
    newAttribute = XMLNewAttrib(newElement,"com",sCOMValue.c_str());
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"baud","9600");
	XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"echo","true");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"data","8");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"stop","0");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"parity","N");
    XMLAddAttrib(newElement,newAttribute);
/*    newAttribute = XMLNewAttrib(newElement,"ip","");
    XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"port","");
    XMLAddAttrib(newElement,newAttribute);*/
	newAttribute = XMLNewAttrib(newElement,"timeout","500");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"persistent","true");
    XMLAddAttrib(newElement,newAttribute);

    AddElement(SystemSource,selectedElement,newElement);

    selectedElement = newElement;

    LoadPopUpMenu();
    LoadCurrentTabSheet();
	MakeItemPageEnabled();

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain :: MakeItemPageEnabled()
{
    EnabledContextForm(false);
    btnSaveItem->Visible = true;
    btnCancelItem->Visible = true;
    //eIDItem->Enabled = true;
    eNameItem->Enabled = true;
    eNameItem->SetFocus();// = true;
    cbTypeItem->Enabled = true;
    cbCOMItem->Enabled = true;
    cbBaudItem->Enabled = true;
    cbEchoItem->Enabled = true;
    cbDataItem->Enabled = true;
    cbStopItem->Enabled = true;
    cbParityItem->Enabled = true;
    eIP1Item->Enabled = true;
    eIP2Item->Enabled = true;
    eIP3Item->Enabled = true;
    eIP4Item->Enabled = true;
    ePortItem->Enabled = true;
    eTimeOutItem->Enabled = true;
    cbPersistentItem->Enabled = true;
}

void __fastcall TfrmMain :: MakeRegionPageEnabled()
{
    EnabledContextForm(false);

    cbNameRegion->Enabled = true;
}

void __fastcall TfrmMain :: MakeZonePageEnabled()
{
    EnabledContextForm(false);

    cbNameZone->Enabled = true;
}

void __fastcall TfrmMain :: MakeNodePageEnabled()
{
    EnabledContextForm(false);

	cbNameNode->Enabled = true;
	cbAddrModemNode->Enabled = true;
	cbLocal->Enabled = true;
/*    if (selectedElement->child == NULL)
	   cbAddrModemNode->Enabled = true;
    else
	   MessageBox(NULL, "           Attenzione: impossibile modificare Indirizzo Modem. \r\nPer modificarlo bisogna eliminare le Periferiche assegnate alla Stazione.", "STLC1000 Configurator", MB_OK + MB_ICONWARNING + MB_TASKMODAL + MB_TOPMOST );
*/
}

void __fastcall TfrmMain :: MakeDevicePageEnabled()
{
	AnsiString sSNMPCommunityDefault = "";

	EnabledContextForm(false);

	eIDDevice->Enabled = true;
	btnGridLocPosition->Enabled = true;
	eNameDevice->Enabled = true;
	cbIPDevice->Enabled = true;
	eIP1Device->Enabled = true;
	eIP2Device->Enabled = true;
	eIP3Device->Enabled = true;
	eIP4Device->Enabled = true;
	eSNDevice->Enabled = true;
	cbPortDevice->Enabled = true;
	cbTypeDevice->Enabled = true;
	cbStationDevice->Enabled = true;
	cbBuildingDevice->Enabled = true;
	cbLocationDevice->Enabled = true;
	cbProfileDevice->Enabled = true;
	cbActiveDevice->Enabled = true;
	cbScheduledDevice->Enabled = true;
    cbSupervisorId->Enabled = true;
	if (useSystemXMLCommunity)
	{
		eSNMPCommunity->Enabled = true;
	}

	sSNMPCommunityDefault = getSNMPCommunityDevice(getCodeTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());

	if (sSNMPCommunityDefault != "")
	{
		if (eSNMPCommunity->Text.Trim() == "")
		{
			eSNMPCommunity->Text = sSNMPCommunityDefault;
		}
	}
}

void __fastcall TfrmMain :: MakeServerPageEnabled()
{
	EnabledContextForm(false);

    eNameServer->Enabled = true;
    eHostServer->Enabled = true;
}

void __fastcall TfrmMain :: MakeLocationPageEnabled()
{
	AnsiString typeSel = "";

	EnabledContextForm(false);

	eNameLocation->Enabled = true;
	eNoteLocation->Enabled = true;

	if ((cbTypeLocation->ItemIndex == 0))
		typeSel = "ATPS24";
	else if ((cbTypeLocation->ItemIndex == 1))
		typeSel = "ATPS24S";
	else if ((cbTypeLocation->ItemIndex == 2))
		typeSel = "ATPS20";
	else if ((cbTypeLocation->ItemIndex == 3))
		typeSel = "ATPS20S";
	else if ((cbTypeLocation->ItemIndex == 4))
		typeSel = "ATPS16";
	else if ((cbTypeLocation->ItemIndex == 5))
		typeSel = "ATPS16S";
	else if ((cbTypeLocation->ItemIndex == 6))
		typeSel = "ATPS12";
	else if ((cbTypeLocation->ItemIndex == 7))
		typeSel = "ATPS12S";
	else if ((cbTypeLocation->ItemIndex == 8))
		typeSel = "ATPS9";
	else if ((cbTypeLocation->ItemIndex == 9))
		typeSel = "ATPS9S";
	else if ((cbTypeLocation->ItemIndex == 10))
		typeSel = "ATPS8";
	else if ((cbTypeLocation->ItemIndex == 11))
		typeSel = "ATPS8S";
	else if ((cbTypeLocation->ItemIndex == 12))
		typeSel = "ATPS6";
	else if ((cbTypeLocation->ItemIndex == 13))
		typeSel = "ATPS6S";
	else if ((cbTypeLocation->ItemIndex == 14))
		typeSel = "ATPS4";
	else if ((cbTypeLocation->ItemIndex == 15))
		typeSel = "ATPS4S";
	else if ((cbTypeLocation->ItemIndex == 16))
		typeSel = "ATPS3";
	else if ((cbTypeLocation->ItemIndex == 17))
		typeSel = "ATPS3S";
	else if ((cbTypeLocation->ItemIndex == 18))
		typeSel = "SUPPTELEIND";
	else if ((cbTypeLocation->ItemIndex == 19))
		typeSel = "SUPPDIFF";
	else if ((cbTypeLocation->ItemIndex == 20))
		typeSel = "RACK24U";


	if (!IsTypeLocationUsed(typeSel))
	 cbTypeLocation->Enabled = true;
/*	else
	   MessageBox(NULL, " Attenzione: impossibile modificare il Tipo di Armadio. \r\n Per modificarlo bisogna sostituirlo con un Armadio di un'altro tipo nelle Periferiche che vi fanno riferimento oppure eliminare le Periferiche.", "STLC1000 Configurator", MB_OK + MB_ICONWARNING + MB_TASKMODAL + MB_TOPMOST );
*/

}

void __fastcall TfrmMain :: MakeStationPageEnabled()
{
	EnabledContextForm(false);

	eNameStation->Enabled = true;
}

void __fastcall TfrmMain :: MakeBuildingPageEnabled()
{
	EnabledContextForm(false);

	eNameBuilding->Enabled = true;
	eNoteBuilding->Enabled = true;
}


void __fastcall TfrmMain :: PageCancel()
{
	XML_ELEMENT * parentElement;

	EnabledContextForm(true);
	if (isNewNode)
	{
		isNewNode = false;
		if (originElement != NULL)
		{
			RemoveNode();
			selectedElement = originElement;
			originElement = NULL;
		}
		else
		{
			parentElement = selectedElement->parent;
			RemoveNode();
			selectedElement = parentElement;
		}
	}
	LoadPopUpMenu();
	LoadCurrentTabSheet();
}


void __fastcall TfrmMain :: MakeCentralizationPageEnabled()
{
	EnabledContextFormByAddServices(false);
	eHostNameCentralization->Enabled = true;
	eIntervalSeconds->Enabled = true;
	cbActiveCentralizationService->Enabled = true;

	/*Forse da fare in secondo momento
	eIP1Centralization->Enabled = !enabledValue;
	eIP2Centralization->Enabled = !enabledValue;
	eIP3Centralization->Enabled = !enabledValue;
	eIP4Centralization->Enabled = !enabledValue;
	*/

}

void __fastcall TfrmMain :: MakeSNMPManagerPageEnabled()
{
	EnabledContextFormByAddServices(false);
	eCommunity->Enabled = true;
	eTrapCommunity->Enabled = true;
	cbUseSystemXMLCommunity->Enabled = true;
}

void __fastcall TfrmMain::btnCancelItemClick(TObject *Sender)
{
	PageCancel();
}

void __fastcall TfrmMain :: RemoveNode()
{
	XML_ENTITY * delEntity = NULL;
	XML_ELEMENT * parentElement = NULL;
	XML_ENTITY * parentEntity = NULL;

    parentElement = selectedElement->parent;

    delEntity = XMLGetEntity(SystemSource->first_entity,(void*)selectedElement);

    XMLRemoveEntity(delEntity);

    XMLRemoveElement(selectedElement);

	XMLFreeEntity(delEntity);

    if (parentElement->child == NULL)
    {
        parentEntity = XMLGetEntity(SystemSource->first_entity,(void*)parentElement);
		XML_ENTITY * closureEntity = parentEntity->next;
        XMLRemoveEntity(closureEntity);
        XMLFreeEntity(closureEntity);
	}

    selectedElement = parentElement;
}

//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelItemClick(TObject *Sender)
{
   AnsiString portValue;

   portValue = AnsiString(XMLGetValue(selectedElement,"id"));
   if (IsIDPortUsed(portValue))
   {
       MessageBox(NULL, "Porta utilizzata da una periferica. Impossibile eliminarla.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
       return;
   }

   RemoveNode();
   DecPortNumberOnPort(portValue);
   DecPortNumberOnDevices(portValue);
   LoadTreeView();
   LoadPopUpMenu();
   /* ClientPage->ActivePage = tsNull;
    ImageHeader->Picture = NULL;
    lblHeader->Caption = "";*/
   LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateItemClick(TObject *Sender)
{
	MakeItemPageEnabled();
}
//---------------------------------------------------------------------------

 void __fastcall TfrmMain :: SetControlByTypeItem()
{
            lblCOM->Visible = false;
            cbCOMItem->Visible = false;
			lblBaud->Visible = false;
            cbBaudItem->Visible = false;
            lblEcho->Visible = false;
            cbEchoItem->Visible = false;
            lblData->Visible = false;
			cbDataItem->Visible = false;
            lblStop->Visible = false;
            cbStopItem->Visible = false;
            lblParity->Visible = false;
            cbParityItem->Visible = false;
            lblIP->Visible = false;
            eIP1Item->Visible = false;
            eIP2Item->Visible = false;
            eIP3Item->Visible = false;
            eIP4Item->Visible = false;
            lblPort->Visible = false;
            ePortItem->Visible = false;

            if (cbTypeItem->ItemIndex != 4)
            {
                lblCOM->Visible = true;
                cbCOMItem->Visible = true;
                lblBaud->Visible = true;
                cbBaudItem->Visible = true;
                lblEcho->Visible = true;
                cbEchoItem->Visible = true;
                lblData->Visible = true;
                cbDataItem->Visible = true;
				lblStop->Visible = true;
				cbStopItem->Visible = true;
                lblParity->Visible = true;
                cbParityItem->Visible = true;
			   }
            else
            {
                lblIP->Visible = true;
                eIP1Item->Visible = true;
				eIP2Item->Visible = true;
                eIP3Item->Visible = true;
                eIP4Item->Visible = true;
				lblPort->Visible = true;
                ePortItem->Visible = true;
            }
}



void __fastcall TfrmMain :: SetDefaultValueByTypeItem()
{
    int i = 0;

            for (i=1;32;i++)
            {
                if (!(IsCOMUsed(IntToStr(i))))
                {
                    cbCOMItem->ItemIndex = i-1;
                    break;
                }
            }

            if (cbTypeItem->ItemIndex == 0)
            {
                cbBaudItem->Text = "9600";
                cbEchoItem->ItemIndex = 0;
                cbDataItem->ItemIndex = 3;
				cbStopItem->ItemIndex = 0;
				cbParityItem->ItemIndex = 0;
                eTimeOutItem->Text = 500;
				cbPersistentItem->ItemIndex = 0;

                ImageHeader->Picture = NULL;
                ImageList48->GetBitmap(2,ImageHeader->Picture->Bitmap);
            }
            else if (cbTypeItem->ItemIndex == 1)
			{
				cbBaudItem->Text = "9600";
                cbEchoItem->ItemIndex = 1;
                cbDataItem->ItemIndex = 3;
                cbStopItem->ItemIndex = 0;
                cbParityItem->ItemIndex = 0;
                eTimeOutItem->Text = 500;
                cbPersistentItem->ItemIndex = 0;

                ImageHeader->Picture = NULL;
                ImageList48->GetBitmap(8,ImageHeader->Picture->Bitmap);
            }
            else if (cbTypeItem->ItemIndex == 2)
            {
                cbBaudItem->Text = "9600";
                cbEchoItem->ItemIndex = 0;
                cbDataItem->ItemIndex = 3;
                cbStopItem->ItemIndex = 0;
                cbParityItem->ItemIndex = 0;
                eTimeOutItem->Text = 500;
                cbPersistentItem->ItemIndex = 0;

                ImageHeader->Picture = NULL;
                ImageList48->GetBitmap(9,ImageHeader->Picture->Bitmap);
            }
            else if (cbTypeItem->ItemIndex == 3)
            {
				cbBaudItem->Text = "1200";
                cbEchoItem->ItemIndex = 0;
                cbDataItem->ItemIndex = 3;
				cbStopItem->ItemIndex = 0;
                cbParityItem->ItemIndex = 0;
                eTimeOutItem->Text = 450;
                cbPersistentItem->ItemIndex = 0;

                ImageHeader->Picture = NULL;
				ImageList48->GetBitmap(10,ImageHeader->Picture->Bitmap);
			}
            else if (cbTypeItem->ItemIndex == 4)
            {
                eIP1Item->Text = "127";
                eIP2Item->Text = "0";
                eIP3Item->Text = "0";
                eIP4Item->Text = "1";
                ePortItem->Text = "5220";
                eTimeOutItem->Text = 450;
                cbPersistentItem->ItemIndex = 0;

                ImageHeader->Picture = NULL;
                ImageList48->GetBitmap(11,ImageHeader->Picture->Bitmap);
            }
}

void __fastcall TfrmMain::cbTypeItemChange(TObject *Sender)
{
	if (checkTextComboBoxInItemList(cbTypeItem))
    {
        SetControlByTypeItem();
		SetDefaultValueByTypeItem();
	}
}

void __fastcall TfrmMain::miCloseClick(TObject *Sender)
{
	this->Close();
}

void __fastcall TfrmMain::miOpenXMLSourceClick(TObject *Sender)
{
	if ( config_status == CONFIGURATION_MODIFIED)
	{
		if (MessageBox(NULL, "Attenzione: sono state effettuate delle modifiche. \r\nProcedendo, senza aver salvato, la nuova configurazione andr� persa. \r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			return;
		}
		else
		{
			config_status = SERVICES_RESTARTED;
		}
	}

	if (OpenXMLSource(AnsiString(GetXMLSystemPathFileName())) != 0)
	{
		MessageBox(NULL, "Errore in apertura del file system.xml.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	loadIDServer();
	loadHostNameServer();

	/*
	if (IDServer_Changed)
	{
		MessageBox(NULL, "Un nuovo codice identificativo della macchina STLC1000 � stato prelevato.", "STLC1000 Configurator", MB_OK + MB_ICONINFORMATION + MB_TASKMODAL + MB_TOPMOST );
	}
	*/

	SetcbActiveCentralizationService();
	ClientPage->ActivePage = tsNull;
	ImageHeader->Picture = NULL;
	lblHeader->Caption = "";

	LoadTreeView();
	miSaveXMLOutputWhitoutRestartSevice->Enabled = true;
	miSaveXMLOutputAndRestartService->Enabled = true;
    // Abilito l'esportazione configurazione di backup
    this->miSaveXMLBackUp->Enabled = true;
}

void __fastcall TfrmMain::miSaveXMLOutputWhitoutRestartSeviceClick(
	  TObject *Sender)
{
	AnsiString msgErr;
	int ret_code = 0;

	if (MessageBox(NULL, "Salvando si sovrascrive la vecchia configurazione.\r\nProcedere al salvataggio della nuova configurazione ?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
	{
		return;
	}

	if (SystemSource == NULL)
	{
		MessageBox(NULL, " Configurazione non caricata.\r\n Impossibile salvare", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	frmSaveSplash = new TfrmSaveSplash(Application);
	try
	{
		frmSaveSplash->Caption = "Salvataggio configurazione in corso ...";
		frmSaveSplash->pbSave->Max = 75;
		frmSaveSplash->Show();
		frmSaveSplash->Update();

		// Salvo system.xml
		OutputSource = XMLCreate(GetXMLSystemPathFileName());

		if (OutputSource != NULL)
		{
			ret_code = XMLOpen(OutputSource,NULL, 'W'); //Cos� XMLOpen apre in 'w+'
			if ((ret_code == 0) && (SystemSource != NULL))
			{
				OutputSource->root_header = SystemSource->root_header;
				OutputSource->root_element = SystemSource->root_element;
				OutputSource->root_text = SystemSource->root_text;
				OutputSource->root_comment = SystemSource->root_comment;
				OutputSource->root_cdata = SystemSource->root_cdata;
				OutputSource->root_instruction = SystemSource->root_instruction;
				OutputSource->first_entity = SystemSource->first_entity;

				ret_code = XMLWrite(OutputSource);
			}
			if (ret_code == 0)
			{
				ret_code = XMLClose(OutputSource);
			}
			else
			{
				XMLClose(OutputSource);
			}
		}

		if (ret_code != 0)
		{
			frmSaveSplash->Close();
			msgErr = "Si � verificato un errore in fase di salvataggio del file ""System.xml"" ! \r\nCodice errore = " + IntToStr(ret_code);
			MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		frmSaveSplash->pbSave->StepBy(25);
		frmSaveSplash->pbSave->StepBy(25);

		config_status = CONFIGURATION_SAVED;

		frmSaveSplash->pbSave->StepBy(25);
		Sleep(500);

		frmSaveSplash->Close();
		MessageBox(NULL, "Operazioni effettuate con successo.", "STLC1000 Configurator", MB_OK + MB_OK + MB_TASKMODAL + MB_TOPMOST );

	}
	__finally
	{
		frmSaveSplash->Free();
		if (!isTreeViewOnLoading)
		{
			ReadElementData();
		}
	}
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::miSaveXMLOutputAndRestartServiceClick(TObject *Sender)
{
	AnsiString msgErr = "";
	int ret_code = 0;
	int numErrori = 0;
	bool allFileSaved = true;

	if (MessageBox(NULL, "Salvando si sovrascrive la vecchia configurazione.\r\nProcedere al salvataggio della nuova configurazione ?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
	{
		return;
	}

	if (SystemSource == NULL)
	{
		MessageBox(NULL, "Configurazione non caricata.\r\nImpossibile salvare", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	frmSaveSplash = new TfrmSaveSplash(Application);
	try
	{
		frmSaveSplash->pbSave->Max = 80;
        frmSaveSplash->pbSave->Step = 0;
		frmSaveSplash->Show();

        frmSaveSplash->lCurrentTask->Caption = "Arresto servizio STLC Manager...";
		frmSaveSplash->Update();

		// Stoppo STLCManager
		ret_code = StopService("STLCManagerService");
		if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			msgErr = msgErr + "\r\n in fase di arresto del servizio ""STLCManager""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Salvataggio file system.xml...";
        frmSaveSplash->Update();
		Sleep(100);

		// Salvo system.xml
		OutputSource = XMLCreate(GetXMLSystemPathFileName());

		if (OutputSource != NULL)
		{
			ret_code = XMLOpen(OutputSource,NULL, 'W');  //Cos� XMLOpen apre in 'w+'
			if ((ret_code == 0) && (SystemSource != NULL))
			{
				OutputSource->root_header = SystemSource->root_header;
				OutputSource->root_element = SystemSource->root_element;
				OutputSource->root_text = SystemSource->root_text;
				OutputSource->root_comment = SystemSource->root_comment;
				OutputSource->root_cdata = SystemSource->root_cdata;
				OutputSource->root_instruction = SystemSource->root_instruction;
				OutputSource->first_entity = SystemSource->first_entity;

				ret_code = XMLWrite(OutputSource);
			}
			if (ret_code == 0)
			{
				ret_code = XMLClose(OutputSource);
			}
			else
			{
				XMLClose(OutputSource);
			}
		}

		if (ret_code != 0)
		{
			allFileSaved = false;
			frmSaveSplash->Color = clRed;
			if (numErrori > 0)
			{
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di salvataggio del file ""System.xml""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Arresto servizio Supervisore Standard...";
        frmSaveSplash->Update();
		Sleep(100);

        // Stop Servizio Supervisore SNMP
		ret_code = StopService("SnmpSupervisorService");
        if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			if (numErrori > 0)
			{
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di arresto del servizio ""SnmpSupervisorService""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Arresto servizio Supervisore SNMP...";
        frmSaveSplash->Update();
		Sleep(100);

        // Stop servizio Supervisore Standard
		ret_code = StopService("SPVServerDaemon");
		if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			if (numErrori > 0)
			{
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di arresto del servizio ""SPVServerDaemon""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Avvio servizio Supervisore Standard...";
        frmSaveSplash->Update();
		Sleep(100);

        // Avvio servizio Supervisore Standard
		ret_code = StartService("SPVServerDaemon");
        if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			if (numErrori > 0) {
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di riavvio del servizio ""SPVServerDaemon""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Avvio servizio Supervisore SNMP...";
        frmSaveSplash->Update();
		Sleep(100);

        // Avvio servizio Supervisore SNMP
		ret_code = StartService("SnmpSupervisorService");
        if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			if (numErrori > 0) {
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di riavvio del servizio ""SnmpSupervisorService""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Avvio servizio Agente Centralizzazione...";
        frmSaveSplash->Update();
		Sleep(100);

		ret_code = ManageSCAgentService();

		if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			if (numErrori > 0) {
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di riavvio del servizio ""SCAgentService""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "Avvio servizio STLC Manager...";
        frmSaveSplash->Update();
		Sleep(100);

		// Start STLCManager
		ret_code = StartService("STLCManagerService");
		if (ret_code != SYS_KERNEL_NO_ERROR)
		{
			frmSaveSplash->Color = clRed;
			if (numErrori > 0) {
				msgErr = msgErr + ";";
			}
			msgErr = msgErr + "\r\n in fase di riavvio del servizio ""STLCManager""! Codice errore = " + IntToStr(ret_code);
			numErrori++;
		}

		frmSaveSplash->pbSave->StepBy(10);
        frmSaveSplash->lCurrentTask->Caption = "";
        frmSaveSplash->Update();
		Sleep(200);

		frmSaveSplash->Close();

		if ( allFileSaved )
		{
			 config_status = SERVICES_RESTARTED;
		}

		if (numErrori > 1)
		{
			msgErr = "Si sono verificati i seguenti errori:" + msgErr + ".";
			MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		}
		else if (numErrori == 1)
		{
			msgErr = "Si � verificato un errore" + msgErr + "." + " NumErrori: " + AnsiString(numErrori);
			MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		}
		else
		{
			MessageBox(NULL, "Operazioni effettuate con successo.", "STLC1000 Configurator", MB_OK + MB_OK + MB_TASKMODAL + MB_TOPMOST );
		}
	}
	__finally
	{
		frmSaveSplash->Free();
		if (!isTreeViewOnLoading)
		{
			ReadElementData();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miNewClick(TObject *Sender)
{
	if ( config_status == CONFIGURATION_MODIFIED)
	{
		if (MessageBox(NULL, "Attenzione: sono state effettuate delle modifiche. \r\nProcedendo, senza aver salvato, la nuova configurazione andr� persa. \r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			return;
		}
	}

	if (OpenXMLSource(ExtractFilePath(Application->ExeName) + "\\" + SCA_CFG_TEM_SYSTEM_DEFAULT_VALUE_PATH) != 0)
	{
		MessageBox(NULL, "Errore in apertura del file system.tem.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}
	loadIDServer();
	loadHostNameServer();

	SetcbActiveCentralizationService();
	
	ClientPage->ActivePage = tsNull;
	ImageHeader->Picture = NULL;
	lblHeader->Caption = "";

	LoadTreeView();

	miSaveXMLOutputWhitoutRestartSevice->Enabled = true;
	miSaveXMLOutputAndRestartService->Enabled = true;
    // Abilito l'esportazione configurazione di backup
    this->miSaveXMLBackUp->Enabled = true;
}

void __fastcall TfrmMain :: EnabledContextFormByAddServices(bool enabledValue)
{
//    XmlTreeView->Enabled = enabledValue;
	isCentralizzationNodeInInsertUpdate = !enabledValue;
    miNew->Enabled = enabledValue;
    miOpenXMLSource->Enabled = enabledValue;
    if (SystemSource != NULL)
	{
        miSaveXMLOutputWhitoutRestartSevice->Enabled = enabledValue;
        miSaveXMLOutputAndRestartService->Enabled = enabledValue;
    }
	miOpenXMLBackUp->Enabled = enabledValue;
	//miSaveXMLBackUp->Enabled = enabledValue;

	btnSaveCentralization->Visible = !enabledValue;
	btnCancelCentralization->Visible = !enabledValue;
	btnUpdateCentralization->Visible = enabledValue;

	btnSaveSNMP->Visible = !enabledValue;
	btnCancelSNMP->Visible = !enabledValue;
	btnUpdateSNMP->Visible = enabledValue;
}

void __fastcall TfrmMain :: EnabledContextForm(bool enabledValue)
{
	isNodeInInsertUpdate = !enabledValue;
   	// XmlTreeView->Enabled = enabledValue;
    miNew->Enabled = enabledValue;
    miOpenXMLSource->Enabled = enabledValue;
    miSaveXMLOutputWhitoutRestartSevice->Enabled = enabledValue;
    miSaveXMLOutputAndRestartService->Enabled = enabledValue;
    miOpenXMLBackUp->Enabled = enabledValue;
	//miSaveXMLBackUp->Enabled = enabledValue;

	btnSaveItem->Visible = !enabledValue;
	btnCancelItem->Visible = !enabledValue;
	btnSaveServer->Visible = !enabledValue;
	btnCancelServer->Visible = !enabledValue;
	btnSaveRegion->Visible = !enabledValue;
	btnCancelRegion->Visible = !enabledValue;
	btnSaveZone->Visible = !enabledValue;
	btnCancelZone->Visible = !enabledValue;
	btnSaveNode->Visible = !enabledValue;
	btnCancelNode->Visible = !enabledValue;
	btnSaveDevice->Visible = !enabledValue;
	btnCancelDevice->Visible = !enabledValue;
	btnSaveStation->Visible = !enabledValue;
	btnCancelStation->Visible = !enabledValue;
	btnSaveBuilding->Visible = !enabledValue;
	btnCancelBuilding->Visible = !enabledValue;
	btnSaveLocation->Visible = !enabledValue;
	btnCancelLocation->Visible = !enabledValue;

	btnUpdateItem->Visible = enabledValue;
	btnUpdateServer->Visible = enabledValue;
	btnUpdateRegion->Visible = enabledValue;
	btnUpdateZone->Visible = enabledValue;
	btnUpdateNode->Visible = enabledValue;
	btnUpdateDevice->Visible = enabledValue;
	btnUpdateStation->Visible = enabledValue;
	btnUpdateBuilding->Visible = enabledValue;
	btnUpdateLocation->Visible = enabledValue;
}

void __fastcall TfrmMain::actSaveServerExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	XML_ATTRIBUTE * attribute;
    XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString errMsg = "";

    if  (eHostServer->Text.Length() > MAX_LENGTH_NOME)
    {
        errMsg = " Errore inserimento Host.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

    if  (eNameServer->Text.Length() > MAX_LENGTH_NOME)
    {
        errMsg = " Errore inserimento Nome.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
    }

    attribute = XMLGetAttrib(selectedElement,"SrvID");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,lblIDServer->Caption.Trim().c_str());
    }
	else
    {
		newAttribute = XMLNewAttrib(selectedElement,"SrvID",lblIDServer->Caption.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }


    attribute = XMLGetAttrib(selectedElement,"host");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,eHostServer->Text.Trim().c_str());
    }
    else
    {
        newAttribute = XMLNewAttrib(selectedElement,"host",eHostServer->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

	attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,eNameServer->Text.Trim().c_str());
    }
    else
    {
        newAttribute = XMLNewAttrib(selectedElement,"name",eNameServer->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }


	EnabledContextForm(true);


	if (isNewNode)
    {
        LoadTreeView();
        LoadPopUpMenu();
	}
    else
    {
		XmlTreeView->Selected->Text = "(" + lblIDServer->Caption + ") " + eNameServer->Text;
    }

    isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;

	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::actSaveServerUpdate(TObject *Sender)
{

	if ((lblIDServer->Caption.Trim().Length() > 0) &&
       (eNameServer->Text.Trim().Length() > 0) &&
       (eHostServer->Text.Trim().Length() > 0))
		{
           actSaveServer->Enabled = true;
        }
        else
        {
           actSaveServer->Enabled = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateServerClick(TObject *Sender)
{
	MakeServerPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelServerClick(TObject *Sender)
{
    PageCancel();
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::actSaveRegionExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	XML_ATTRIBUTE * attribute;
    XML_ATTRIBUTE * newAttribute = NULL;

	if (!(checkInt(lblIDRegion->Caption)))
    {
        MessageBox(NULL, "Errore inserimento ID Compartimento.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	if  (RegionExist(lblIDRegion->Caption.Trim()))
    {
        MessageBox(NULL, "Compartimento gi� inserito. ", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
    }

    attribute = XMLGetAttrib(selectedElement,"RegID");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,lblIDRegion->Caption.Trim().c_str());
    }
    else
    {
		newAttribute = XMLNewAttrib(selectedElement,"RegID",lblIDRegion->Caption.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
	{
		XMLSetValue(attribute,cbNameRegion->Text.Trim().c_str());
    }
    else
    {
		newAttribute = XMLNewAttrib(selectedElement,"name",cbNameRegion->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }


    EnabledContextForm(true);


    if (isNewNode)
    {
        LoadTreeView();
        LoadPopUpMenu();
    }
    else
    {
		XmlTreeView->Selected->Text = "(" + lblIDRegion->Caption + ") " + cbNameRegion->Text;
    }

    isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;

	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveRegionUpdate(TObject *Sender)
{
	if ((lblIDRegion->Caption.Trim().Length() > 0) &&
		checkTextComboBoxIsValid(cbNameRegion))
		{
		   actSaveRegion->Enabled = true;
		}
		else
        {
           actSaveRegion->Enabled = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelRegionClick(TObject *Sender)
{
    PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddRegionClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
    XML_ATTRIBUTE * newAttribute = NULL;

	isNewNode = true;

	newElement = XMLCreateElement("region",NULL,NULL);
	newAttribute = XMLNewAttrib(newElement,"RegID","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"name","");
	XMLAddAttrib(newElement,newAttribute);

	AddElement(SystemSource,selectedElement,newElement);

	selectedElement = newElement;

	LoadPopUpMenu();
	LoadCurrentTabSheet();
	MakeRegionPageEnabled();
	cbNameRegion->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbNameRegion);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateRegionClick(TObject *Sender)
{
	MakeRegionPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelRegionClick(TObject *Sender)
{
	RemoveNode();
	LoadTreeView();
	LoadPopUpMenu();
	LoadCurrentTabSheet();
 /*
   ClientPage->ActivePage = tsServer;
   ImageHeader->Picture->Bitmap = NULL;
   ImageList48->GetBitmap(3,ImageHeader->Picture->Bitmap);
   lblHeader->Caption = "Server";*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveZoneExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	XML_ATTRIBUTE * attribute;
	XML_ATTRIBUTE * newAttribute = NULL;

	if (!(checkInt(lblIDZone->Caption)))
	{
		MessageBox(NULL, "Errore inserimento ID Linea.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	if  (ZoneExist(lblIDZone->Caption.Trim()))
    {
        MessageBox(NULL, "Linea gi� inserita. ", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	attribute = XMLGetAttrib(selectedElement,"ZonID");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,lblIDZone->Caption.Trim().c_str());
    }
    else
    {
		newAttribute = XMLNewAttrib(selectedElement,"ZonID",lblIDZone->Caption.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,cbNameZone->Text.Trim().c_str());
    }
    else
    {
        newAttribute = XMLNewAttrib(selectedElement,"name",cbNameZone->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }


	EnabledContextForm(true);


    if (isNewNode)
	{
		LoadTreeView();
        LoadPopUpMenu();
    }
        else
    {
		XmlTreeView->Selected->Text = "(" + lblIDZone->Caption + ") " +cbNameZone->Text;
    }

	isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;

    LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveZoneUpdate(TObject *Sender)
{
   if ((lblIDZone->Caption.Trim().Length() > 0) &&
		checkTextComboBoxIsValid(cbNameZone))
		{
           actSaveZone->Enabled = true;
		}
        else
        {
           actSaveZone->Enabled = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveNodeExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	XML_ATTRIBUTE * attribute;
	XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString sIPValue;

	if (!(checkInt(lblIDNode->Caption)))
    {
        MessageBox(NULL, "Errore inserimento ID Stazione.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
    }

	if  (NodeExist(lblIDNode->Caption.Trim()))
    {
		MessageBox(NULL, "Stazione gi� inserita. ", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
	}

    attribute = XMLGetAttrib(selectedElement,"NodID");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,lblIDNode->Caption.Trim().c_str());
    }
    else
    {
		newAttribute = XMLNewAttrib(selectedElement,"NodID",lblIDNode->Caption.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,cbNameNode->Text.Trim().c_str());
    }
    else
    {
		newAttribute = XMLNewAttrib(selectedElement,"name",cbNameNode->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

	attribute = XMLGetAttrib(selectedElement,"local");
	if (attribute  != NULL)
	{
		if (cbLocal->Checked)
		{
			XMLSetValue(attribute,"true");
		}
		else
		{
			XMLSetValue(attribute,"false");
		}
	}
	else
	{
		if (cbLocal->Checked)
		{
			newAttribute = XMLNewAttrib(selectedElement,"local","true");
		}
		else
		{
			newAttribute = XMLNewAttrib(selectedElement,"local","false");
		}
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"modem_size");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute,"16");
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"modem_size","16");
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"modem_addr");
	if (attribute == NULL)
	{
		attribute = XMLNewAttrib(selectedElement,"modem_addr","");
		XMLAddAttrib(selectedElement,attribute);
	}

	if (attribute != NULL)
	{
		if (cbAddrModemNode->ItemIndex == 0)
			sIPValue = "FF";
		else if (cbAddrModemNode->ItemIndex == 1)
			sIPValue = "00";
		else if (cbAddrModemNode->ItemIndex == 2)
			sIPValue = "01";
		else if (cbAddrModemNode->ItemIndex == 3)
			sIPValue = "02";
		else if (cbAddrModemNode->ItemIndex == 4)
			sIPValue = "03";
		else if (cbAddrModemNode->ItemIndex == 5)
			sIPValue = "04";
		else if (cbAddrModemNode->ItemIndex == 6)
			sIPValue = "05";
		else if (cbAddrModemNode->ItemIndex == 7)
			sIPValue = "06";
		else if (cbAddrModemNode->ItemIndex == 8)
			sIPValue = "07";
		else if (cbAddrModemNode->ItemIndex == 9)
			sIPValue = "08";
		else if (cbAddrModemNode->ItemIndex == 10)
			sIPValue = "09";
		else if (cbAddrModemNode->ItemIndex == 11)
			sIPValue = "0A";
		else if (cbAddrModemNode->ItemIndex == 12)
			sIPValue = "0B";
		else if (cbAddrModemNode->ItemIndex == 13)
			sIPValue = "0C";
		else if (cbAddrModemNode->ItemIndex == 14)
			sIPValue = "0D";
		else if (cbAddrModemNode->ItemIndex == 15)
			sIPValue = "0E";
		else if (cbAddrModemNode->ItemIndex == 16)
			sIPValue = "0F";

		if (AnsiString(XMLGetValue(selectedElement,"modem_addr")) != sIPValue)
		{
			RicodificaDeviceAddress(sIPValue.c_str());
		}

		XMLSetValue(attribute,sIPValue.c_str());
	}

	EnabledContextForm(true);

	if (cbLocal->Checked)
	{
		setNoLocalNodes(lblIDNode->Caption.Trim().c_str());
	}


	if (isNewNode)
	{
		LoadTreeView();
		LoadPopUpMenu();
	}
	else
	{
		XmlTreeView->Selected->Text = "(" + lblIDNode->Caption + ") " + cbNameNode->Text;
	}

	isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;

	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveNodeUpdate(TObject *Sender)
{
   if ((lblIDNode->Caption.Trim().Length() > 0) &&
	   (checkTextComboBoxIsValid(cbNameNode)) &&
	   (lblSizeModemNode->Caption.Trim().Length() > 0) &&
	   (checkTextComboBoxIsValid(cbAddrModemNode)) )
        {
           actSaveNode->Enabled = true;
        }
        else
        {
           actSaveNode->Enabled = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveDeviceExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	XML_ATTRIBUTE * attribute;
    XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString sPosValue,sIPValue,errMsg;

    if (!(checkInt(eIDDevice->Text)))
    {
		MessageBox(NULL, "Errore inserimento ID Dispositivo.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

    if (IsIDdeviceUsed(eIDDevice->Text))
    {
		MessageBox(NULL, " ID Dispositivo gi� presente.\r\n Inserire un ID non presente per la stazione.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
    }

	if  (eSNDevice->Text.Length() > MAX_LENGTH_SERIAL_NUMBER)
    {
        errMsg = " Errore inserimento Serial Number.\r\n Inserire un valore con al massimo "+ (IntToStr(MAX_LENGTH_SERIAL_NUMBER)) +" caratteri.";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
    }

	if  (eNameDevice->Text.Length() > MAX_LENGTH_NOME)
	{
		errMsg = " Errore inserimento Nome.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

    if (eIP1Device->Visible)
    {
        try{
            if ( !(checkInt(eIP1Device->Text)) || !(checkInt(eIP2Device->Text)) ||
                 !(checkInt(eIP3Device->Text)) || !(checkInt(eIP4Device->Text)) )
            {
                MessageBox(NULL, " Errore inserimento Indirizzo IP.\r\n Inserire 4 valori numerici interi positivi.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
                return;
            }

            if ( ((StrToInt(eIP1Device->Text)) < 0) || ((StrToInt(eIP1Device->Text)) > MAX_NUM_IP) ||
                 ((StrToInt(eIP2Device->Text)) < 0) || ((StrToInt(eIP2Device->Text)) > MAX_NUM_IP) ||
				 ((StrToInt(eIP3Device->Text)) < 0) || ((StrToInt(eIP3Device->Text)) > MAX_NUM_IP) ||
                 ((StrToInt(eIP4Device->Text)) < 0) || ((StrToInt(eIP4Device->Text)) > MAX_NUM_IP) )
            {
                errMsg = " Errore inserimento Indirizzo IP.\r\n Inserire 4 valori compresi tra 0 e " + (IntToStr(MAX_NUM_IP))+".";
                MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
                return;
			}
        }
		catch (EConvertError &E)
        {
            errMsg = " Errore inserimento valore intero.\r\n Inserire valori compresi tra 0 e " + (IntToStr(MAX_INTEGER))+".";
            MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
            return;
        }
	}
    else
	{
		if (IsDeviceAddressUsed(getIDPort(cbPortDevice->Text.Trim().c_str()),cbIPDevice->ItemIndex))
        {
			MessageBox(NULL, " Indirizzo Dispositivo gi� presente per lo stesso tipo di porta.\r\n Inserire un indirizzo non presente per la stazione.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
            return;
		}
	}

	if  (eSNMPCommunity->Text.Length() > MAX_LENGTH_NOME)
	{
		errMsg = " Errore inserimento Community.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}


	if (!(checkInt(lblPosXDevice->Caption)))
    {
        MessageBox(NULL, "Errore inserimento Colonna.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	if ( !(checkInt(lblPosYDevice->Caption)))
    {
        MessageBox(NULL, "Errore inserimento Riga.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	attribute = XMLGetAttrib(selectedElement,"DevID");
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,eIDDevice->Text.Trim().c_str());
    }
	else
    {
        newAttribute = XMLNewAttrib(selectedElement,"DevID",eIDDevice->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }


    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,eNameDevice->Text.Trim().c_str());
    }
    else
	{
        newAttribute = XMLNewAttrib(selectedElement,"name",eNameDevice->Text.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"position");
	sPosValue = lblPosXDevice->Caption.Trim() + "," + lblPosYDevice->Caption.Trim();
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,sPosValue.Trim().c_str());
    }
    else
    {
        newAttribute = XMLNewAttrib(selectedElement,"position",sPosValue.Trim().c_str());
        XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"addr");
	if (attribute == NULL)
    {
        attribute = XMLNewAttrib(selectedElement,"addr","");
        XMLAddAttrib(selectedElement,attribute);
	}

    if (attribute != NULL)
    {
		if (eIP1Device->Visible)
        {
            sIPValue = eIP1Device->Text.Trim() + "." + eIP2Device->Text.Trim() + "." +
                       eIP3Device->Text.Trim() + "." + eIP4Device->Text.Trim();
            XMLSetValue(attribute,sIPValue.c_str());
        }
        else
        {
            // Codifica dell'indirizzo del dispositivo in funzione dell'indirizzo del modem della stazione in cui � presente
			char *  sCodifyDevAddres = CodificaDeviceAddress(XMLGetValue(selectedElement->parent,"modem_addr"),cbIPDevice->ItemIndex);
            XMLSetValue(attribute,sCodifyDevAddres);
			free(sCodifyDevAddres);
		}
    }

	attribute = XMLGetAttrib(selectedElement,"SN");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute,eSNDevice->Text.Trim().c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"SN",eSNDevice->Text.Trim().c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"active");
	if (attribute  != NULL)
	{
		if (cbActiveDevice->ItemIndex == 0)
		{
			XMLSetValue(attribute,"true");
		}
		else
		{
			XMLSetValue(attribute,"false");
		}
	}
	else
	{
		if (cbActiveDevice->ItemIndex == 0)
		{
			newAttribute = XMLNewAttrib(selectedElement,"active","true");
		}
		else
		{
			newAttribute = XMLNewAttrib(selectedElement,"active","false");
		}
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"scheduled");
	if (attribute != NULL)
	{
		if (cbScheduledDevice->ItemIndex == 0)
		{
			XMLSetValue(attribute,"true");
		}
		else
		{
			XMLSetValue(attribute,"false");
		}
	}
	else
	{
		if (cbScheduledDevice->ItemIndex == 0)
		{
			newAttribute = XMLNewAttrib(selectedElement,"scheduled","true");
		}
		else
		{
			newAttribute = XMLNewAttrib(selectedElement,"scheduled","false");
		}
		XMLAddAttrib(selectedElement,newAttribute);
	}

	if (cbSupervisorId->Visible)
	{
        attribute = XMLGetAttrib(selectedElement,"supervisor_id");
        if (attribute != NULL)
        {
            if (cbSupervisorId->ItemIndex == 0)
            {
                XMLSetValue(attribute,"0");
            }
            else
            {
                XMLSetValue(attribute,"1");
            }
        }
        else
        {
            if (cbSupervisorId->ItemIndex == 0)
            {
                newAttribute = XMLNewAttrib(selectedElement,"supervisor_id","0");
            }
            else
            {
                newAttribute = XMLNewAttrib(selectedElement,"supervisor_id","1");
            }
            XMLAddAttrib(selectedElement,newAttribute);
        }
    }
    else {
        attribute = XMLGetAttrib(selectedElement,"supervisor_id");
        if (attribute != NULL)
        {
    		XMLRemoveAttrib(selectedElement, attribute);
        }
	}

	attribute = XMLGetAttrib(selectedElement,"type");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute, getCodeTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"type", getCodeTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"port");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute, getIDPort(cbPortDevice->Text.Trim().c_str()).c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"port", getIDPort(cbPortDevice->Text.Trim().c_str()).c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	if (eSNMPCommunity->Visible)
	{
		attribute = XMLGetAttrib(selectedElement,"snmp_community");
		if (attribute != NULL)
		{
			XMLSetValue(attribute,eSNMPCommunity->Text.Trim().c_str());
		}
		else
		{
			newAttribute = XMLNewAttrib(selectedElement,"snmp_community",eSNMPCommunity->Text.Trim().c_str());
			XMLAddAttrib(selectedElement,newAttribute);
		}
	}
    else
    {
        attribute = XMLGetAttrib(selectedElement,"snmp_community");
        if (attribute != NULL)
        {
    		XMLRemoveAttrib(selectedElement, attribute);
        }
	}

	attribute = XMLGetAttrib(selectedElement,"station");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute, getIDStation(cbStationDevice->Text.Trim().c_str()).c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"station", getIDStation(cbStationDevice->Text.Trim().c_str()).c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"building");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute, getIDBuildingByDevice(cbBuildingDevice->Text.Trim().c_str()).c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"building", getIDBuildingByDevice(cbBuildingDevice->Text.Trim().c_str()).c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"location");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute, getIDLocationByDevice(cbLocationDevice->Text.Trim().c_str()).c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"location", getIDLocationByDevice(cbLocationDevice->Text.Trim().c_str()).c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"profile");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute, getIDProfile(cbProfileDevice->Text.Trim().c_str()).c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"profile", getIDProfile(cbProfileDevice->Text.Trim().c_str()).c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}

	EnabledContextForm(true);


	if (isNewNode)
	{
		LoadTreeView();
		LoadPopUpMenu();

		if (originElement != NULL)
			originElement = NULL;
	}
	else
	{
		XmlTreeView->Selected->Text = "(" + eIDDevice->Text + ") " + eNameDevice->Text;
	}

	isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;

	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveDeviceUpdate(TObject *Sender)
{
	if (eIP1Device->Visible)
	{
		if(! ((eIP1Device->Text.Trim().Length() > 0) &&
			  (eIP2Device->Text.Trim().Length() > 0) &&
			  (eIP3Device->Text.Trim().Length() > 0) &&
			  (eIP4Device->Text.Trim().Length() > 0))   )
		{
		   actSaveDevice->Enabled = false;
		   return;
		}
	}
	else
	{
		if(! (checkTextComboBoxIsValid(cbIPDevice)) )
		{
		   actSaveDevice->Enabled = false;
		   return;
		}
	}

	if (eSNMPCommunity->Visible)
	{
		if(!(eSNMPCommunity->Text.Trim().Length() > 0))
		{
		   actSaveDevice->Enabled = false;
		   return;
		}
	}

	if ((eIDDevice->Text.Trim().Length() > 0) &&
		(eNameDevice->Text.Trim().Length() > 0) &&
		(checkTextComboBoxIsValid(cbStationDevice)) &&
		(checkTextComboBoxIsValid(cbBuildingDevice)) &&
		(checkTextComboBoxIsValid(cbLocationDevice)) &&
		(lblPosXDevice->Caption != "0") &&
		(lblPosYDevice->Caption != "0") &&
		(eSNDevice->Text.Trim().Length() > 0) &&
		(checkTextComboBoxIsValid(cbPortDevice)) &&
		(checkTextComboBoxIsValid(cbTypeDevice)) &&
		(checkTextComboBoxIsValid(cbProfileDevice)) &&
		(checkTextComboBoxIsValid(cbActiveDevice)) &&
		(checkTextComboBoxIsValid(cbScheduledDevice)) &&
        ((!(cbSupervisorId->Visible)) || (((cbSupervisorId->Visible) && (checkTextComboBoxIsValid(cbSupervisorId))))))
		{
		   actSaveDevice->Enabled = true;
		}
		else
		{
		   actSaveDevice->Enabled = false;
		}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelZoneClick(TObject *Sender)
{
	PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelNodeClick(TObject *Sender)
{
	PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelDeviceClick(TObject *Sender)
{
    PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddZoneClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
	XML_ATTRIBUTE * newAttribute = NULL;

	isNewNode = true;

	newElement = XMLCreateElement("zone",NULL,NULL);
	newAttribute = XMLNewAttrib(newElement,"ZonID","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"name","");
	XMLAddAttrib(newElement,newAttribute);

	AddElement(SystemSource,selectedElement,newElement);

	selectedElement = newElement;

	LoadPopUpMenu();
	LoadCurrentTabSheet();
	MakeZonePageEnabled();
	cbNameZone->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbNameZone);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateZoneClick(TObject *Sender)
{
    MakeZonePageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelZoneClick(TObject *Sender)
{
    RemoveNode();
    LoadTreeView();
    LoadPopUpMenu();
    LoadCurrentTabSheet();

/*   ClientPage->ActivePage = tsRegion;
   ImageHeader->Picture->Bitmap = NULL;
   ImageList48->GetBitmap(4,ImageHeader->Picture->Bitmap);
   lblHeader->Caption = "Compartimento";*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddNodeClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
    XML_ATTRIBUTE * newAttribute = NULL;

	isNewNode = true;

	newElement = XMLCreateElement("node",NULL,NULL);
	newAttribute = XMLNewAttrib(newElement,"NodID","");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"name","");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"modem_size","16");
    XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"modem_addr","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"local","false");
	XMLAddAttrib(newElement,newAttribute);

	AddElement(SystemSource,selectedElement,newElement);

	selectedElement = newElement;

	LoadPopUpMenu();
	LoadCurrentTabSheet();
	MakeNodePageEnabled();
	cbNameNode->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbNameNode);
	cbAddrModemNode->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbAddrModemNode);
	if (!getExistLocalNode())
	{
		cbLocal->Checked = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateNodeClick(TObject *Sender)
{
	MakeNodePageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelNodeClick(TObject *Sender)
{
    RemoveNode();
    LoadTreeView();
    LoadPopUpMenu();
    LoadCurrentTabSheet();
/*
   ClientPage->ActivePage = tsZone;
   ImageHeader->Picture->Bitmap = NULL;
   ImageList48->GetBitmap(5,ImageHeader->Picture->Bitmap);
   lblHeader->Caption = "Linea";*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddDeviceClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
	XML_ATTRIBUTE * newAttribute = NULL;
	AnsiString errMsg = "";
	AnsiString sID = "";

	if (!ExistsLocations())
	{
		errMsg = " Prima di aggiungere una periferica � necessario aggiungere almeno un armadio.";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	isNewNode = true;

	newElement = XMLCreateElement("device",NULL,NULL);
	sID = IntToStr(getMaxIDdevice()+1);
	newAttribute = XMLNewAttrib(newElement,"DevID",sID.c_str());
	XMLAddAttrib(newElement,newAttribute);

//    newAttribute = XMLNewAttrib(newElement,"DevID","");
//    XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"name","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"station","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"building","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"location","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"position","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"addr","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"SN","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"port","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"type","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"profile","");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"active","true");
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"scheduled","true");
	XMLAddAttrib(newElement,newAttribute);

	AddElement(SystemSource,selectedElement,newElement);

	selectedElement = newElement;

	LoadPopUpMenu();
	LoadCurrentTabSheet();

	cbStationDevice->Text = "";
	cbBuildingDevice->Text = "";
	cbLocationDevice->Text = "";

	cbTypeDevice->Text = "";
	cbProfileDevice->ItemIndex = 0;

	MakeDevicePageEnabled();

	cbTypeDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbTypeDevice);
	cbStationDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbStationDevice);
	cbBuildingDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbBuildingDevice);
	cbLocationDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbLocationDevice);
	cbPortDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbPortDevice);
	checkTextComboBoxInItemList(cbProfileDevice);

	lbl_IP_COM_Address->Visible = false;

	cbIPDevice->Visible = false;
	cbIPDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbIPDevice);

	eIP1Device->Visible = false;
	eIP2Device->Visible = false;
	eIP3Device->Visible = false;
	eIP4Device->Visible = false;
	eIP1Device->Text = "";
	eIP2Device->Text = "";
	eIP3Device->Text = "";
	eIP4Device->Text = "";

	lblSNMPCommunity->Visible = false;
	eSNMPCommunity->Visible = false;
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::miDuplicateDeviceClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
	XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString sID = "";

    originElement = selectedElement;
    selectedElement = selectedElement->parent;

    isNewNode = true;

    newElement = XMLCreateElement("device",NULL,NULL);
    sID = IntToStr(getMaxIDdevice()+1);
    newAttribute = XMLNewAttrib(newElement,"DevID",sID.c_str());
	XMLAddAttrib(newElement,newAttribute);

    newAttribute = XMLNewAttrib(newElement,"name","");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"station",XMLGetValue(originElement,"station"));
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"building",XMLGetValue(originElement,"building"));
	XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"location",XMLGetValue(originElement,"location"));
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"position","");
    XMLAddAttrib(newElement,newAttribute);

    if (getTypePort(StrToInt(AnsiString(XMLGetValue(originElement,"port")))) == "TCP_Client")
	{
        newAttribute = XMLNewAttrib(newElement,"addr",XMLGetValue(originElement,"addr"));
    }
    else
	{
        newAttribute = XMLNewAttrib(newElement,"addr","");
    }
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"SN","");
    XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"port",XMLGetValue(originElement,"port"));
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"type",XMLGetValue(originElement,"type"));
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"profile",XMLGetValue(originElement,"profile"));
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"active",XMLGetValue(originElement,"active"));
    XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"scheduled",XMLGetValue(originElement,"scheduled"));
    XMLAddAttrib(newElement,newAttribute);


    AddElement(SystemSource,selectedElement,newElement);

	selectedElement = newElement;

    LoadPopUpMenu();
    LoadCurrentTabSheet();

	eIP4Device->Text = "";

	cbIPDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbIPDevice);

	MakeDevicePageEnabled();

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateDeviceClick(TObject *Sender)
{
	MakeDevicePageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelDeviceClick(TObject *Sender)
{
    if (MessageBox(NULL, "Procedere all'eliminazione della periferica selezionata ?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
    {
        return;
    }

    RemoveNode();
    LoadTreeView();
    LoadPopUpMenu();
    LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

bool __fastcall TfrmMain::checkInt(AnsiString textValue)
{
    int count = 0;
    int i = 0;

    textValue = textValue.TrimRight();

	count = textValue.Length();
	for (i = 0; i < count; i++)
	{
		if ( (textValue.operator [](i+1)<'0')||(textValue.operator [](i+1)>'9') )
		{
			return false;
		}
	}
	return true;
}

void __fastcall TfrmMain::miOpenXMLBackUpClick(TObject *Sender)
{
	AnsiString filename;

	if ( config_status == CONFIGURATION_MODIFIED)
	{
		if (MessageBox(NULL, "Attenzione: sono state effettuate delle modifiche. \r\nProcedendo, senza aver salvato, la nuova configurazione andr� persa. \r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			return;
		}
	}

	if(OpenDialog->Execute())
	{
		filename = OpenDialog->FileName;
	}

	if (filename != "")
	{
		if (OpenXMLSource(filename) != 0)
		{
			AnsiString msg = "Errore in apertura del file " + filename + ".";
			MessageBox(NULL, msg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		loadIDServer();
		loadHostNameServer();
		/*
		if (IDServer_Changed)
		{
			MessageBox(NULL, "Un nuovo codice identificativo della macchina STLC1000 � stato prelevato.", "STLC1000 Configurator", MB_OK + MB_ICONINFORMATION + MB_TASKMODAL + MB_TOPMOST );
		}
		*/
		SetcbActiveCentralizationService();
		ClientPage->ActivePage = tsNull;
        ImageHeader->Picture = NULL;
        lblHeader->Caption = "";

		LoadTreeView();
        miSaveXMLOutputWhitoutRestartSevice->Enabled = true;
        miSaveXMLOutputAndRestartService->Enabled = true;
	    // Abilito l'esportazione configurazione di backup
    	this->miSaveXMLBackUp->Enabled = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miSaveXMLBackUpClick(TObject *Sender)
{
	int ret_code = 0;
	AnsiString msgErr;
	AnsiString filename;

	if(SaveDialog->Execute())
	{
		filename = SaveDialog->FileName;
	}

	if (filename == AnsiString(GetXMLSystemPathFileName()))
	{
		MessageBox(NULL, "Impossibile sovrascrivere il file ""'system.xml'"".", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

   OutputSource = XMLCreate(filename.c_str());

   if (OutputSource != NULL)
   {
	   ret_code = XMLOpen(OutputSource,NULL, 'W');
	   if ((ret_code == 0) && (SystemSource != NULL))
	   {
			OutputSource->root_header = SystemSource->root_header;
			OutputSource->root_element = SystemSource->root_element;
			OutputSource->root_text = SystemSource->root_text;
			OutputSource->root_comment = SystemSource->root_comment;
			OutputSource->root_cdata = SystemSource->root_cdata;
			OutputSource->root_instruction = SystemSource->root_instruction;
			OutputSource->first_entity = SystemSource->first_entity;

			ret_code = XMLWrite(OutputSource);
	   }


	   if (ret_code == 0)
	   {
		   ret_code = XMLClose(OutputSource);
	   }
	   else
	   {
		   XMLClose(OutputSource);
	   }

   }

   if (ret_code != 0)
   {
	   msgErr = "Si � verificato un errore in fase di salvataggio del file " + filename + " ! \r\nCodice errore = " + IntToStr(ret_code);
	   MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
   }
   else
   {
	   MessageBox(NULL, "Operazione effettuata con successo.", "STLC1000 Configurator", MB_OK + MB_OK + MB_TASKMODAL + MB_TOPMOST );
   }

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbPortDeviceChange(TObject *Sender)
{
	if (checkTextComboBoxInItemList(cbPortDevice))
	{
		lbl_IP_COM_Address->Visible = true;
		if (getTypePort(StrToInt(getIDPort(cbPortDevice->Text))) == "TCP_Client")
		{
			cbIPDevice->Visible = false;
			eIP1Device->Visible = true;
			eIP2Device->Visible = true;
			eIP3Device->Visible = true;
			eIP4Device->Visible = true;
		}
		else
		{
			cbIPDevice->Visible = true;
			eIP1Device->Visible = false;
			eIP2Device->Visible = false;
			eIP3Device->Visible = false;
			eIP4Device->Visible = false;
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::XmlTreeViewChange(TObject *Sender,
	  TTreeNode *Node)
{
	if (isNodeInInsertUpdate)
	{
		PageCancel();
	}
	if (isCentralizzationNodeInInsertUpdate)
	{
	   CentralizationPageCancel();
	}
	if (!isTreeViewOnLoading)
	{
		ReadElementData();
	}
}
//---------------------------------------------------------------------------

int __fastcall TfrmMain :: ManageSCAgentService()
{
	if (cbActiveCentralizationService->Checked)
	{
		return ManageService(SERVICE_AUTO_START,"SCAgentService");
	}
	else
	{
		return ManageService(SERVICE_DEMAND_START,"SCAgentService");
	}
}

void __fastcall TfrmMain::actSaveStationExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }
    
	XML_ATTRIBUTE * attribute;
	XML_ATTRIBUTE * newAttribute = NULL;
	AnsiString errMsg = "";

	if (!(checkInt(lblIDStation->Caption)))
	{
		MessageBox(NULL, "Errore inserimento ID Stazione.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

    if  (eNameStation->Text.Length() > MAX_LENGTH_NOME)
    {
        errMsg = " Errore inserimento Nome.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
    }

    if  (StationNameExist(eNameStation->Text))
    {
        errMsg = "Nome Stazione gi� presente. ";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

    attribute = XMLGetAttrib(selectedElement,"id");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,lblIDStation->Caption.Trim().c_str());
	}
	else
    {
		 newAttribute = XMLNewAttrib(selectedElement,"id",lblIDStation->Caption.Trim().c_str());
         XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
    {
        XMLSetValue(attribute,eNameStation->Text.Trim().c_str());
    }
    else
    {
         newAttribute = XMLNewAttrib(selectedElement,"name",eNameStation->Text.Trim().c_str());
		 XMLAddAttrib(selectedElement,newAttribute);
    }

    EnabledContextForm(true);

    if (isNewNode)
    {
        LoadTreeView();
		LoadPopUpMenu();
    }
        else
    {
        XmlTreeView->Selected->Text = eNameStation->Text;
    }

    isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;
	
    LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveStationUpdate(TObject *Sender)
{
   if ((lblIDStation->Caption.Trim().Length() > 0) &&
       (eNameStation->Text.Trim().Length() > 0))
        {
           actSaveStation->Enabled = true;
        }
        else
        {
           actSaveStation->Enabled = false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveBuildingExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }
    
	XML_ATTRIBUTE * attribute;
	XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString errMsg = "";

	if (!(checkInt(lblIDBuilding->Caption)))
    {
        MessageBox(NULL, "Errore inserimento ID Edificio.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
	}

    if  (eNameBuilding->Text.Length() > MAX_LENGTH_NOME)
    {
        errMsg = " Errore inserimento Nome.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

    if  (BuildingNameExist(eNameBuilding->Text))
    {
        errMsg = "Nome Edificio gi� presente nella Stazione. ";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
	}

    if  (eNoteBuilding->Text.Length() > MAX_LENGTH_NOTE)
    {
        errMsg = " Errore inserimento Nota.\r\n Inserire una nota con al massimo "+ (IntToStr(MAX_LENGTH_NOTE)) +" caratteri.";
        MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

    attribute = XMLGetAttrib(selectedElement,"id");
    if (attribute  != NULL)
	{
		XMLSetValue(attribute,lblIDBuilding->Caption.Trim().c_str());
    }
    else
    {
		 newAttribute = XMLNewAttrib(selectedElement,"id",lblIDBuilding->Caption.Trim().c_str());
		 XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"name");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,eNameBuilding->Text.Trim().c_str());
    }
    else
    {
         newAttribute = XMLNewAttrib(selectedElement,"name",eNameBuilding->Text.Trim().c_str());
         XMLAddAttrib(selectedElement,newAttribute);
    }

    attribute = XMLGetAttrib(selectedElement,"note");
    if (attribute  != NULL)
    {
		XMLSetValue(attribute,eNoteBuilding->Text.Trim().c_str());
    }
    else
	{
         newAttribute = XMLNewAttrib(selectedElement,"note",eNoteBuilding->Text.Trim().c_str());
         XMLAddAttrib(selectedElement,newAttribute);
    }

    EnabledContextForm(true);


    if (isNewNode)
	{
        LoadTreeView();
        LoadPopUpMenu();
    }
    else
    {
        XmlTreeView->Selected->Text = eNameBuilding->Text;
    }

	isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;

    LoadCurrentTabSheet();

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveBuildingUpdate(TObject *Sender)
{
   if ((lblIDBuilding->Caption.Trim().Length() > 0) &&
       (eNameBuilding->Text.Trim().Length() > 0) &&
       (eNoteBuilding->Text.Trim().Length() > 0))
        {
		   actSaveBuilding->Enabled = true;
		}
		else
		{
		   actSaveBuilding->Enabled = false;
		}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveLocationExecute(TObject *Sender)
{
    if (XmlTreeView->Selected == NULL) {
        MessageBox(NULL, "Occorre selezionare un nodo nell'albero prima di proseguire.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
        return;
    }

	XML_ATTRIBUTE * attribute;
	XML_ATTRIBUTE * newAttribute = NULL;
	AnsiString errMsg = "";

	if (!(checkInt(lblIDLocation->Caption)))
	{
		MessageBox(NULL, "Errore inserimento ID Armadio.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	if  (eNameLocation->Text.Length() > MAX_LENGTH_NOME)
	{
		errMsg = " Errore inserimento Nome.\r\n Inserire un nome con al massimo "+ (IntToStr(MAX_LENGTH_NOME)) +" caratteri.";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	if  (LocationNameExist(eNameLocation->Text))
	{
		errMsg = "Nome Armadio gi� presente nell'Edificio. ";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	if  (eNoteLocation->Text.Length() > MAX_LENGTH_NOTE)
	{
		errMsg = " Errore inserimento Nota.\r\n Inserire una nota con al massimo "+ (IntToStr(MAX_LENGTH_NOTE)) +" caratteri.";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	attribute = XMLGetAttrib(selectedElement,"id");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute,lblIDLocation->Caption.Trim().c_str());
	}
	else
	{
		 newAttribute = XMLNewAttrib(selectedElement,"id",lblIDLocation->Caption.Trim().c_str());
		 XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"name");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute,eNameLocation->Text.Trim().c_str());
	}
	else
	{
		 newAttribute = XMLNewAttrib(selectedElement,"name",eNameLocation->Text.Trim().c_str());
		 XMLAddAttrib(selectedElement,newAttribute);
	}

	attribute = XMLGetAttrib(selectedElement,"note");
	if (attribute  != NULL)
	{
		XMLSetValue(attribute,eNoteLocation->Text.Trim().c_str());
	}
	else
	{
		 newAttribute = XMLNewAttrib(selectedElement,"note",eNoteLocation->Text.Trim().c_str());
		 XMLAddAttrib(selectedElement,newAttribute);
	}


	attribute = XMLGetAttrib(selectedElement,"type");
	if (attribute == NULL)
	{
		 attribute = XMLNewAttrib(selectedElement,"type","");
		 XMLAddAttrib(selectedElement,attribute);
	}

	if (attribute != NULL)
	{
		if ((cbTypeLocation->ItemIndex == 0))
			XMLSetValue(attribute,"ATPS24");
		else if ((cbTypeLocation->ItemIndex == 1))
			XMLSetValue(attribute,"ATPS24S");
		else if ((cbTypeLocation->ItemIndex == 2))
			XMLSetValue(attribute,"ATPS20");
		else if ((cbTypeLocation->ItemIndex == 3))
			XMLSetValue(attribute,"ATPS20S");
		else if ((cbTypeLocation->ItemIndex == 4))
			XMLSetValue(attribute,"ATPS16");
		else if ((cbTypeLocation->ItemIndex == 5))
			XMLSetValue(attribute,"ATPS16S");
		else if ((cbTypeLocation->ItemIndex == 6))
			XMLSetValue(attribute,"ATPS12");
		else if ((cbTypeLocation->ItemIndex == 7))
			XMLSetValue(attribute,"ATPS12S");
		else if ((cbTypeLocation->ItemIndex == 8))
			XMLSetValue(attribute,"ATPS9");
		else if ((cbTypeLocation->ItemIndex == 9))
			XMLSetValue(attribute,"ATPS9S");
		else if ((cbTypeLocation->ItemIndex == 10))
			XMLSetValue(attribute,"ATPS8");
		else if ((cbTypeLocation->ItemIndex == 11))
			XMLSetValue(attribute,"ATPS8S");
		else if ((cbTypeLocation->ItemIndex == 12))
			XMLSetValue(attribute,"ATPS6");
		else if ((cbTypeLocation->ItemIndex == 13))
			XMLSetValue(attribute,"ATPS6S");
		else if ((cbTypeLocation->ItemIndex == 14))
			XMLSetValue(attribute,"ATPS4");
		else if ((cbTypeLocation->ItemIndex == 15))
			XMLSetValue(attribute,"ATPS4S");
		else if ((cbTypeLocation->ItemIndex == 16))
			XMLSetValue(attribute,"ATPS3");
		else if ((cbTypeLocation->ItemIndex == 17))
			XMLSetValue(attribute,"ATPS3S");
		else if ((cbTypeLocation->ItemIndex == 18))
			XMLSetValue(attribute,"SUPPTELEIND");
		else if ((cbTypeLocation->ItemIndex == 19))
			XMLSetValue(attribute,"SUPPDIFF");
		else if ((cbTypeLocation->ItemIndex == 20))
			XMLSetValue(attribute,"RACK24U");
	}

	EnabledContextForm(true);

	if (isNewNode)
	{
		LoadTreeView();
		LoadPopUpMenu();
	}
	else
	{
        if (XmlTreeView->Selected != NULL) {
            XmlTreeView->Selected->Text = eNameLocation->Text;
        }
	}

	isNewNode = false;
	config_status = CONFIGURATION_MODIFIED;
	
	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveLocationUpdate(TObject *Sender)
{
   if ((lblIDLocation->Caption.Trim().Length() > 0) &&
       (eNameLocation->Text.Trim().Length() > 0) &&
	   (eNoteLocation->Text.Trim().Length() > 0) &&
	   (checkTextComboBoxIsValid(cbTypeLocation))  )
		{
           actSaveLocation->Enabled = true;
        }
        else
        {
           actSaveLocation->Enabled = false;
		}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelStationClick(TObject *Sender)
{
	PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelBuildingClick(TObject *Sender)
{
    PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelLocationClick(TObject *Sender)
{
	PageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateStationClick(TObject *Sender)
{
    MakeStationPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateBuildingClick(TObject *Sender)
{
    MakeBuildingPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddLocationClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
    XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString sID = "";
    
    isNewNode = true;

    newElement = XMLCreateElement("location",NULL,NULL);
	sID = IntToStr(getMaxIDLocation()+1);
    newAttribute = XMLNewAttrib(newElement,"id",sID.c_str());
	XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"name","");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"type","");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"note","Nessuna");
    XMLAddAttrib(newElement,newAttribute);

    AddElement(SystemSource,selectedElement,newElement);

	selectedElement = newElement;

	LoadPopUpMenu();
	LoadCurrentTabSheet();
	MakeLocationPageEnabled();
	cbTypeLocation->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbTypeLocation);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateLocationClick(TObject *Sender)
{
	MakeLocationPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelLocationClick(TObject *Sender)
{
	AnsiString locValue;

	locValue = AnsiString(XMLGetValue(selectedElement,"id"));
	if (IsIDLocationUsed())
	{
		MessageBox(NULL, "Armadio utilizzato da una periferica. Impossibile eliminarlo.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}


	DecLocationIDOnLocation(locValue);
	DecLocationIDOnDevices(locValue);

	RemoveNode();
	LoadTreeView();
	LoadPopUpMenu();
	LoadCurrentTabSheet();
}
//---------------------------------------------------------------------------


//==============================================================================
/// Funzione per ottenere un puntatore alla Costante di tipo stringa
/// che si passa come parametro
//==============================================================================
char * GetCharPointer( const char * Str )
{
	char  * value_buffer  = NULL;

	int sizeStr = StrLen(Str);

	value_buffer = (char*)malloc( sizeof(char) * sizeStr +1 ); // -MALLOC
	if ( value_buffer != NULL )
	{
		memcpy( value_buffer, Str, sizeof(char) * sizeStr +1 );
	}
	return value_buffer;
}


//==============================================================================
/// Funzione per ottenere l'URL per il download della Region List
//------------------------------------------------------------------------------
char * GetRegionListUpdateURL( void )
{
	void * 	key               			 = NULL;
	char * 	region_list_update_URL_value = NULL;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, CFG_KEY, false );
	if ( key != NULL )
	{
		region_list_update_URL_value = (char*) SYSGetRegValue( key, CFG_REGION_LIST_URL_VALUE );
		SYSCloseRegKey( key );
	}

	if ((region_list_update_URL_value == NULL) || ((AnsiString(region_list_update_URL_value))== "") ){
		region_list_update_URL_value = GetCharPointer(CFG_REGION_LIST_URL_DEFAULT_VALUE);
	}

	return region_list_update_URL_value;
}

//==============================================================================
/// Funzione per ottenere l'URL per il download della Device Type List
//------------------------------------------------------------------------------
char * GetDeviceTypeListUpdateURL( void )
{
	void * 	key               			 		= NULL;
	char * 	devie_type_list_update_URL_value 	= NULL;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, CFG_KEY, false );
	if ( key != NULL )
	{
		devie_type_list_update_URL_value = (char*) SYSGetRegValue( key, CFG_DEVICE_TYPE_LIST_URL_VALUE );
		SYSCloseRegKey( key );
	}

	if ((devie_type_list_update_URL_value == NULL) || ((AnsiString(devie_type_list_update_URL_value))== "") ){
		devie_type_list_update_URL_value = GetCharPointer(CFG_DEVICE_TYPE_LIST_URL_DEFAULT_VALUE);
	}

	return devie_type_list_update_URL_value;
}

void __fastcall TfrmMain::FormActivate(TObject *Sender)
{
	HeaderPanel->Top = 0;
	ClientPage->ActivePage = tsNull;

	LoadTypeDeviceComboFromXML();
	LoadRegionZoneNodeFromXML();

	mRegionListUpdateURL = GetRegionListUpdateURL();
    mDeviceTypeListUpdateURL = GetDeviceTypeListUpdateURL();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::XmlTreeViewMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    TTreeNode * node = NULL;

	if ((ClientPage->ActivePage == tsCentralization) || (ClientPage->ActivePage == tsSNMPManager))
	{
        node = this->XmlTreeView->GetNodeAt(X,Y);
		if (node != NULL)
        {
            this->XmlTreeView->Selected = node->GetPrev();
            this->XmlTreeView->Selected = node;
        }

	}

    if (Button == mbRight)
    {
        node = this->XmlTreeView->GetNodeAt(X,Y);
		if (node != NULL)
        {
			this->XmlTreeView->Selected = node;
		}
    }
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::miAddBuildingClick(TObject *Sender)
{
	XML_ELEMENT * newElement = NULL;
	XML_ATTRIBUTE * newAttribute = NULL;
	AnsiString sID = "";

    isNewNode = true;

    newElement = XMLCreateElement("building",NULL,NULL);
    sID = IntToStr(getMaxIDBuilding()+1);
    newAttribute = XMLNewAttrib(newElement,"id",sID.c_str());
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"name","");
    XMLAddAttrib(newElement,newAttribute);
    newAttribute = XMLNewAttrib(newElement,"note","Nessuna");
	XMLAddAttrib(newElement,newAttribute);

    AddElement(SystemSource,selectedElement,newElement);

    selectedElement = newElement;

    LoadPopUpMenu();
	LoadCurrentTabSheet();
    MakeBuildingPageEnabled();

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miAddStationClick(TObject *Sender)
{
    XML_ELEMENT * newElement = NULL;
    XML_ATTRIBUTE * newAttribute = NULL;
    AnsiString sID = "";

    isNewNode = true;

    newElement = XMLCreateElement("station",NULL,NULL);
    sID = IntToStr(getMaxIDStation()+1);
    newAttribute = XMLNewAttrib(newElement,"id",sID.c_str());
	XMLAddAttrib(newElement,newAttribute);
	newAttribute = XMLNewAttrib(newElement,"name","");
	XMLAddAttrib(newElement,newAttribute);

    AddElement(SystemSource,selectedElement,newElement);

    selectedElement = newElement;

    LoadPopUpMenu();
    LoadCurrentTabSheet();
    MakeStationPageEnabled();

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelStationClick(TObject *Sender)
{
    AnsiString idValue;

    idValue = AnsiString(XMLGetValue(selectedElement,"id"));

	DecStationIDOnStation(idValue);
	RemoveNode();
	LoadTreeView();
    LoadPopUpMenu();
    LoadCurrentTabSheet();

/*   ClientPage->ActivePage = tsNull;
   ImageHeader->Picture = NULL;
   lblHeader->Caption = "";*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miDelBuildingClick(TObject *Sender)
{
    AnsiString idValue;

	idValue = AnsiString(XMLGetValue(selectedElement,"id"));

    DecBuildingIDOnBuilding(idValue);
    RemoveNode();
    LoadTreeView();
    LoadPopUpMenu();
    LoadCurrentTabSheet();


/*   ClientPage->ActivePage = tsStation;
   ImageHeader->Picture->Bitmap = NULL;
   ImageList48->GetBitmap(13,ImageHeader->Picture->Bitmap);
   lblHeader->Caption = "Stazione";
*/
}
//---------------------------------------------------------------------------


int __fastcall TfrmMain :: getMaxIDStation()
{
	XML_ELEMENT * element;
	XML_ELEMENT * lastChild;

    if (SystemSource != NULL)
    {
        element = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((element != NULL) && (element->child != NULL))
        {
             // Cerco gli elementi XML figli
            element = element->child;
            while (element)
			{
                lastChild = element;
                // Passo all'elemento successivo (se esiste)
                element = element->next;
			}
			return StrToInt(AnsiString(XMLGetValue(lastChild,"id")));
        }
        else return -1;
    }
    else return -1;
}

int __fastcall TfrmMain :: getMaxIDBuilding()
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
	XML_ELEMENT * lastChild;
    int retValue = -1;

	if (SystemSource != NULL)
    {
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
			stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL) &&
                    (XMLGetValue(stationElement,"id") == XMLGetValue(selectedElement,"id")) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
						lastChild = buildingElement;
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
					}
					retValue = StrToInt(AnsiString(XMLGetValue(lastChild,"id")));
				}
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
    return retValue;
}


int __fastcall TfrmMain :: getMaxIDLocation()
{
	XML_ELEMENT * topographyElement;
	XML_ELEMENT * stationElement;
	XML_ELEMENT * buildingElement;
	XML_ELEMENT * locationElement;
	XML_ELEMENT * lastChild;
    int retValue = -1;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

		if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
			while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
                    (XMLGetValue(stationElement,"id") == XMLGetValue(selectedElement->parent,"id")))
                {
					// Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
					while (buildingElement)
                    {
						if ((buildingElement != NULL) && (buildingElement->child != NULL)&&
							(XMLGetValue(buildingElement,"id") == XMLGetValue(selectedElement,"id")) )
                        {
                            // Cerco gli elementi XML figli
                            locationElement = buildingElement->child;
							while (locationElement)
							{
                                lastChild = locationElement;
                                locationElement = locationElement->next;
                            }
                            retValue = StrToInt(AnsiString(XMLGetValue(lastChild,"id")));
                        }
						// Passo all'elemento successivo (se esiste)
						buildingElement = buildingElement->next;
					}
				}
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
	return retValue;
}

void __fastcall TfrmMain :: DecBuildingIDOnBuilding(AnsiString BuildingIDValue)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ATTRIBUTE * attribute = NULL;
    int iBuildingIDValue;
    AnsiString sNewBuildingIDValue;

	if (SystemSource != NULL)
	{
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

		if ((topographyElement != NULL) && (topographyElement->child != NULL))
		{
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
                    (XMLGetValue(stationElement,"id") == XMLGetValue(selectedElement->parent,"id")))
                {
                    // Cerco gli elementi XML figli
					buildingElement = stationElement->child;
                    while (buildingElement)
					{
						iBuildingIDValue = StrToInt(AnsiString(XMLGetValue(buildingElement,"id")));
                        if (iBuildingIDValue >= StrToInt(BuildingIDValue) )
                        {
							attribute = XMLGetAttrib(buildingElement,"id");
                            if (attribute  != NULL)
                            {
                                sNewBuildingIDValue = IntToStr(iBuildingIDValue-1);
                                XMLSetValue(attribute,sNewBuildingIDValue.c_str());
                            }
                        }

                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
					}
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
}

void __fastcall TfrmMain :: DecStationIDOnStation(AnsiString StationIDValue)
{
	XML_ELEMENT * topographyElement;
	XML_ELEMENT * stationElement;
    XML_ATTRIBUTE * attribute = NULL;
    int iStationIDValue;
    AnsiString sNewStationIDValue;

    if (SystemSource != NULL)
    {
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

		if ((topographyElement != NULL) && (topographyElement->child != NULL))
		{
             // Cerco gli elementi XML figli
			stationElement = topographyElement->child;
			while (stationElement)
            {
                iStationIDValue = StrToInt(AnsiString(XMLGetValue(stationElement,"id")));
                if (iStationIDValue >= StrToInt(StationIDValue) )
                {
                    attribute = XMLGetAttrib(stationElement,"id");
                    if (attribute  != NULL)
                    {
                        sNewStationIDValue = IntToStr(iStationIDValue-1);
                        XMLSetValue(attribute,sNewStationIDValue.c_str());
                    }
                }

				// Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
}


void __fastcall TfrmMain::cbStationDeviceChange(TObject *Sender)
{
/*    bool isInList = false;
	for (int i=0; i < cbStationDevice->Items->Count;i++)
    {
		if (cbStationDevice->Text == cbStationDevice->Items->Strings[i])
            isInList = true;
    }
*/

	cbBuildingDevice->Text = "";
	cbLocationDevice->Text = "";
	cbLocationDevice->Clear();

	lblPosXDevice->Caption = "0";
	lblPosYDevice->Caption = "0";
	lblPosXDevice->Font->Color = clRed;
	lblPosYDevice->Font->Color = clRed;

	checkTextComboBoxInItemList(cbStationDevice);
/*    if (!checkTextComboBoxInItemList(cbStationDevice))
	{
		cbStationDevice->Text = "";
	   // return;
	}
  */
	LoadBuildingDeviceCombo();
	cbBuildingDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbBuildingDevice);
	cbLocationDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbLocationDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbBuildingDeviceChange(TObject *Sender)
{
	cbLocationDevice->Text = "";
	lblPosXDevice->Caption = "0";
	lblPosYDevice->Caption = "0";
	lblPosXDevice->Font->Color = clRed;
	lblPosYDevice->Font->Color = clRed;

	checkTextComboBoxInItemList(cbBuildingDevice);
	LoadLocationDeviceCombo();
	cbLocationDevice->Text = "Seleziona ->";
	checkTextComboBoxInItemList(cbLocationDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbLocationDeviceChange(TObject *Sender)
{
	AnsiString typeLocation, stationIDValue, buildingIDValue, locationIDValue;

	checkTextComboBoxInItemList(cbLocationDevice);

	stationIDValue = getIDStation(cbStationDevice->Text.Trim());
	buildingIDValue = getIDBuildingByDevice(cbBuildingDevice->Text.Trim());
	locationIDValue = getIDLocationByDevice(cbLocationDevice->Text.Trim());

	typeLocation = GetTypeLocationFromDevice(stationIDValue,buildingIDValue,locationIDValue);

	if (typeLocation != "SUPPTELEIND" && typeLocation != "SUPPDIFF")
	{
		lblPosXDevice->Caption = "0";
		lblPosYDevice->Caption = "0";
		lblPosXDevice->Font->Color = clRed;
		lblPosYDevice->Font->Color = clRed;
	}
	else
	{
		lblPosXDevice->Caption = "1";
		lblPosYDevice->Caption = "1";
		lblPosXDevice->Font->Color = clWindowText;
		lblPosYDevice->Font->Color = clWindowText;
	}
}
//---------------------------------------------------------------------------

int __fastcall TfrmMain :: LoadTypeDeviceComboFromXML()
{
	XML_ELEMENT * deviceTypeElement;
	XML_ELEMENT * typeElement;
	AnsiString path;
	int ret_code = 0;

    path = ExtractFilePath(Application->ExeName) + "\device_type_list.xml";
    DeviceTypeSource = XMLCreate(path.c_str());

	if (DeviceTypeSource != NULL)
    {
        ret_code = XMLOpen(DeviceTypeSource,NULL, 'R');
		if (ret_code == 0)
        {
            ret_code = XMLRead(DeviceTypeSource,NULL);
            if (ret_code == 0)
			{
				 XMLCheck(DeviceTypeSource);
            }
        }
		XMLClose(DeviceTypeSource);
    }

    if (ret_code == 0) // Procedo solo se la lettura del file e' andata a buon fine
    {
		cbTypeDevice->Items->Clear();

    	if (DeviceTypeSource != NULL)
	    {
    	    deviceTypeElement = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

	        if ((deviceTypeElement != NULL) && (deviceTypeElement->child != NULL))
			{
        		// Cerco gli elementi XML figli
	            typeElement = deviceTypeElement->child;
    	        while (typeElement)
				{
					cbTypeDevice->Items->Add(AnsiString(XMLGetValue(typeElement,"name")));

					// Passo all'elemento successivo (se esiste)
					typeElement = typeElement->next;
				}
			}
		}
    }

    return ret_code;
}


AnsiString __fastcall TfrmMain :: getCodeTypeDevice(AnsiString typeName)
{
	XML_ELEMENT * element;

	if (DeviceTypeSource != NULL)
	{
		element = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli
			element = element->child;
			while (element)
			{
				if (  AnsiString(XMLGetValue(element,"name"))  == typeName)
				{
					return AnsiString(XMLGetValue(element,"code"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}

AnsiString __fastcall TfrmMain :: getPortTypeDevice(AnsiString typeName)
{
	XML_ELEMENT * element;

	if (DeviceTypeSource != NULL)
	{
		element = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli
			element = element->child;
			while (element)
			{
				if (  AnsiString(XMLGetValue(element,"name"))  == typeName)
				{
					return AnsiString(XMLGetValue(element,"port_type"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}

AnsiString __fastcall TfrmMain :: getNameTypeDevice(AnsiString typeCode)
{
	XML_ELEMENT * element;

	if (DeviceTypeSource != NULL)
	{
		element = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli
			element = element->child;
			while (element)
			{
				if (  AnsiString(XMLGetValue(element,"code"))  == typeCode)
				{
					return AnsiString(XMLGetValue(element,"name"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}

AnsiString __fastcall TfrmMain :: getDefaultNameDevice(AnsiString typeCode)
{
	XML_ELEMENT * element;

	if (DeviceTypeSource != NULL)
	{
		element = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli
			element = element->child;
			while (element)
			{
				if (  AnsiString(XMLGetValue(element,"code"))  == typeCode)
				{
					return AnsiString(XMLGetValue(element,"default_name"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}

AnsiString __fastcall TfrmMain :: getSNMPCommunityDevice(AnsiString typeCode)
{
	XML_ELEMENT * element;

	if (DeviceTypeSource != NULL)
	{
		element = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli
			element = element->child;
			while (element)
			{
				if (  AnsiString(XMLGetValue(element,"code"))  == typeCode)
				{
					return AnsiString(XMLGetValue(element,"snmp_community"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}


AnsiString __fastcall TfrmMain :: getSupervisorIdDevice(AnsiString typeCode)
{
	XML_ELEMENT * element;

	if (DeviceTypeSource != NULL)
	{
		element = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli
			element = element->child;
			while (element)
			{
				if ( AnsiString(XMLGetValue(element,"code"))  == typeCode)
				{
					return AnsiString(XMLGetValue(element,"supervisor_id"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}


bool __fastcall TfrmMain :: IsDeviceAddressUsed(AnsiString idPortValue, int decDeviceAddressValue)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;

    bool retValue = false;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

		if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
			while (regionElement)
            {
				if ((regionElement != NULL) && (regionElement->child != NULL)&&
                    (XMLGetValue(regionElement,"RegID") == XMLGetValue(selectedElement->parent->parent->parent,"RegID")) )
				{
					// Cerco gli elementi XML figli
					zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL)&&
                            (XMLGetValue(zoneElement,"ZonID") == XMLGetValue(selectedElement->parent->parent,"ZonID")) )
						{
                            // Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
                                if ((nodeElement != NULL) && (nodeElement->child != NULL)&&
									(XMLGetValue(nodeElement,"NodID") == XMLGetValue(selectedElement->parent,"NodID")) )
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
                                    while (deviceElement)
                                    {
                                        if ( (deviceElement->tag != selectedElement->tag) &&
                                             (getTypePort(StrToInt(AnsiString(XMLGetValue(deviceElement,"port")))) != "TCP_Client") &&
                                             (AnsiString(XMLGetValue(deviceElement,"port")) == idPortValue) &&
											 (decDeviceAddressValue == DecodificaDeviceAddress(XMLGetValue(selectedElement->parent,"modem_addr"),XMLGetValue(deviceElement,"addr"))) )
                                        {
                                           retValue = true;
                                        }
										deviceElement = deviceElement->next;
                                    }
                                }
                                // Passo all'elemento successivo (se esiste)
								nodeElement = nodeElement->next;
                            }
                        }
						// Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
					}
                }
				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
            }
        }
    }
	return retValue;
}


bool __fastcall TfrmMain :: IsIDdeviceUsed(AnsiString idValue)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;

    bool retValue = false;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
			{
                if ((regionElement != NULL) && (regionElement->child != NULL)&&
					(XMLGetValue(regionElement,"RegID") == XMLGetValue(selectedElement->parent->parent->parent,"RegID")) )
                {
					// Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
					while (zoneElement)
					{
                        if ((zoneElement != NULL) && (zoneElement->child != NULL)&&
							(XMLGetValue(zoneElement,"ZonID") == XMLGetValue(selectedElement->parent->parent,"ZonID")) )
                        {
							// Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
                                if ((nodeElement != NULL) && (nodeElement->child != NULL)&&
                                    (XMLGetValue(nodeElement,"NodID") == XMLGetValue(selectedElement->parent,"NodID")) )
                                {
                                    // Cerco gli elementi XML figli
									deviceElement = nodeElement->child;
                                    while (deviceElement)
                                    {
                                        if ( (deviceElement->tag != selectedElement->tag) &&
                                             ((AnsiString(XMLGetValue(deviceElement,"DevID"))) == idValue) )
                                        {
                                           retValue = true;
                                        }
                                        deviceElement = deviceElement->next;
                                    }
                                }
								// Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
				}
				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
            }
        }
    }
	return retValue;
}



int __fastcall TfrmMain :: getMaxIDdevice()
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;
//    XML_ELEMENT * lastChild;
    int retValue = -1;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
		{
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
			{
                if ((regionElement != NULL) && (regionElement->child != NULL)&&
					(XMLGetValue(regionElement,"RegID") == XMLGetValue(selectedElement->parent->parent,"RegID")) )
                {
					// Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
					while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL)&&
							(XMLGetValue(zoneElement,"ZonID") == XMLGetValue(selectedElement->parent,"ZonID")) )
						{
							// Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
                            while (nodeElement)
                            {
                                if ((nodeElement != NULL) && (nodeElement->child != NULL)&&
                                    (XMLGetValue(nodeElement,"NodID") == XMLGetValue(selectedElement,"NodID")) )
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
                                    while (deviceElement)
                                    {
                                        //lastChild = deviceElement;
										if (retValue < StrToInt(AnsiString(XMLGetValue(deviceElement,"DevID"))) )
                                        {
                                           retValue = StrToInt(AnsiString(XMLGetValue(deviceElement,"DevID")));
                                        }
                                        deviceElement = deviceElement->next;
                                    }
                                    //retValue = StrToInt(AnsiString(XMLGetValue(lastChild,"DevID")));

                                }
								// Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
						}
                        // Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
				}
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
		}
	}
    return retValue;
}


void __fastcall TfrmMain :: DecDeviceIDOnDevice()
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;
    XML_ATTRIBUTE * attribute = NULL;
    int idDeviceValue;
    AnsiString sNewIdDeviceValue;

    if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
		{
             // Cerco gli elementi XML figli
			regionElement = serverElement->child;
            while (regionElement)
			{
                if ((regionElement != NULL) && (regionElement->child != NULL)&&
					(XMLGetValue(regionElement,"RegID") == XMLGetValue(selectedElement->parent->parent->parent,"RegID")) )
                {
                    // Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
					{
						if ((zoneElement != NULL) && (zoneElement->child != NULL)&&
							(XMLGetValue(zoneElement,"ZonID") == XMLGetValue(selectedElement->parent->parent,"ZonID")) )
						{
                            // Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
                            {
                                if ((nodeElement != NULL) && (nodeElement->child != NULL)&&
                                    (XMLGetValue(nodeElement,"NodID") == XMLGetValue(selectedElement->parent,"NodID")) )
                                {
                                    // Cerco gli elementi XML figli
                                    deviceElement = nodeElement->child;
                                    while (deviceElement)
                                    {
                                        idDeviceValue = StrToInt(AnsiString(XMLGetValue(deviceElement,"DevID")));
                                        if (idDeviceValue >= StrToInt(XMLGetValue(selectedElement,"DevID")) )
                                        {
											attribute = XMLGetAttrib(deviceElement,"DevID");
                                            if (attribute  != NULL)
                                            {
                                                sNewIdDeviceValue = IntToStr(idDeviceValue-1);
                                                XMLSetValue(attribute,sNewIdDeviceValue.c_str());
                                            }
                                        }

										// Passo all'elemento successivo (se esiste)
                                        deviceElement = deviceElement->next;
									}

								}
                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
						// Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
					}
				}
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
			}
		}
	}
}

void __fastcall TfrmMain::btnGridLocPositionClick(TObject *Sender)
{
	if (!checkTextComboBoxIsValid(cbLocationDevice))
	{
		MessageBox(NULL, "Scegliere l'armadio.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}

	AnsiString stationIDValue, buildingIDValue, locationIDValue;
	int XPos = 1;
	int YPos = 1;

	stationIDValue = getIDStation(cbStationDevice->Text.Trim());
	buildingIDValue = getIDBuildingByDevice(cbBuildingDevice->Text.Trim());
	locationIDValue = getIDLocationByDevice(cbLocationDevice->Text.Trim());

	if (lblPosXDevice->Caption != "0")
		XPos = StrToInt(lblPosXDevice->Caption.Trim());
	if (lblPosYDevice->Caption != "0")
		YPos = StrToInt(lblPosYDevice->Caption.Trim());

	frmGridLocPosition = new TfrmGridLocPosition(Application);
	try
	{
		frmGridLocPosition->typeLocation = GetTypeLocationFromDevice(stationIDValue,buildingIDValue,locationIDValue);

		frmGridLocPosition->locXPosition = XPos;
		frmGridLocPosition->locYPosition = YPos;
		frmGridLocPosition->ShowModal();
		lblPosXDevice->Caption = IntToStr(frmGridLocPosition->locXPosition);
		lblPosYDevice->Caption = IntToStr(frmGridLocPosition->locYPosition);
		lblPosXDevice->Font->Color = clWindowText;
		lblPosYDevice->Font->Color = clWindowText;

	}
	__finally
	{
		frmGridLocPosition->Free();
	}
}
//---------------------------------------------------------------------------


AnsiString __fastcall TfrmMain :: GetTypeLocationFromDevice(AnsiString stationIDValue,
                                  AnsiString buildingIDValue, AnsiString locationIDValue)
{
	XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ELEMENT * locationElement;
	AnsiString retTypeLocation = "";

    if (SystemSource != NULL)
    {
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
			{
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
					(AnsiString(XMLGetValue(stationElement,"id")) == stationIDValue) )
                {
                    // Cerco gli elementi XML figli
					buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        if ((buildingElement != NULL) && (buildingElement->child != NULL)&&
							(AnsiString(XMLGetValue(buildingElement,"id")) == buildingIDValue) )
						{
                            // Cerco gli elementi XML figli
							locationElement = buildingElement->child;
                            while (locationElement)
                            {
                                if (AnsiString(XMLGetValue(locationElement,"id")) == locationIDValue)
                                {
                                    retTypeLocation = AnsiString(XMLGetValue(locationElement,"type"));
                                    return retTypeLocation;
                                }

                                // Passo all'elemento successivo (se esiste)
								locationElement = locationElement->next;
                            }
						}
                        // Passo all'elemento successivo (se esiste)
						buildingElement = buildingElement->next;
                    }
				}
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
    return retTypeLocation;
}




void __fastcall TfrmMain::cbTypeDeviceChange(TObject *Sender)
{
	AnsiString sSNMPCommunityDefault = "";
	AnsiString sDefaultNameDevice = "";
    AnsiString sSupervisorId = "";

	checkTextComboBoxInItemList(cbTypeDevice);
	cbPortDevice->Text = getNamePort(getPortTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());
	cbPortDeviceChange(Sender);

	sDefaultNameDevice = getDefaultNameDevice(getCodeTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());

	if (sDefaultNameDevice != "")
	{
		if (eNameDevice->Text.Trim() == "")
		{
			eNameDevice->Text = sDefaultNameDevice;
		}
	}

	sSNMPCommunityDefault = getSNMPCommunityDevice(getCodeTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());
   	sSupervisorId = getSupervisorIdDevice(getCodeTypeDevice(cbTypeDevice->Text.Trim().c_str()).c_str());

	if (sSNMPCommunityDefault != "")
	{
		lblSNMPCommunity->Visible = true;
		eSNMPCommunity->Visible = true;
		eSNMPCommunity->Text = sSNMPCommunityDefault;

        lblSupervisorId->Visible = true;
        cbSupervisorId->Visible = true;
	}
	else
	{
		lblSNMPCommunity->Visible = false;
		eSNMPCommunity->Visible = false;

        lblSupervisorId->Visible = false;
        cbSupervisorId->Visible = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbProfileDeviceChange(TObject *Sender)
{
    checkTextComboBoxInItemList(cbProfileDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbTypeLocationChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbTypeLocation);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbAddrModemNodeChange(TObject *Sender)
{
    checkTextComboBoxInItemList(cbAddrModemNode);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbIPDeviceChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbIPDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbActiveDeviceChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbActiveDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbScheduledDeviceChange(TObject *Sender)
{
    checkTextComboBoxInItemList(cbScheduledDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbSupervisorIdChange(TObject *Sender)
{
    checkTextComboBoxInItemList(cbSupervisorId);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbEchoItemChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbEchoItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbDataItemChange(TObject *Sender)
{
    checkTextComboBoxInItemList(cbDataItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbStopItemChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbStopItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbParityItemChange(TObject *Sender)
{
    checkTextComboBoxInItemList(cbParityItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbPersistentItemChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbPersistentItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbCOMItemChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbCOMItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbBaudItemChange(TObject *Sender)
{
	checkTextComboBoxInItemList(cbBaudItem);
}
//---------------------------------------------------------------------------


bool __fastcall TfrmMain :: StationNameExist(AnsiString stationName)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

		if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
            {
				if ( AnsiString(XMLGetValue(stationElement,"name")).Trim().LowerCase()  == stationName.Trim().LowerCase() &&
                     (XMLGetValue(stationElement,"id") != XMLGetValue(selectedElement,"id")) )
				{
                    return true;
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
    return false;
}

bool __fastcall TfrmMain :: BuildingNameExist(AnsiString buildingName)
{
    XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
	XML_ELEMENT * buildingElement;

    if (SystemSource != NULL)
    {
        topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
		{
             // Cerco gli elementi XML figli
            stationElement = topographyElement->child;
            while (stationElement)
			{
                if ((stationElement != NULL) && (stationElement->child != NULL)&&
				   ( XMLGetValue(stationElement,"id") == XMLGetValue(selectedElement->parent,"id")) )
                {
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
					{
                        if ( AnsiString(XMLGetValue(buildingElement,"name")).Trim().LowerCase()  == buildingName.Trim().LowerCase() &&
							(XMLGetValue(buildingElement,"id") != XMLGetValue(selectedElement,"id")) )
                        {
                            return true;
                        }
                        // Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
                // Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
            }
        }
    }
	return false;
}

bool __fastcall TfrmMain :: LocationNameExist(AnsiString locationName)
{
	XML_ELEMENT * topographyElement;
    XML_ELEMENT * stationElement;
    XML_ELEMENT * buildingElement;
    XML_ELEMENT * locationElement;

    if (SystemSource != NULL)
    {
		topographyElement = XMLResolveElement(SystemSource->root_element,"topography",NULL);

        if ((topographyElement != NULL) && (topographyElement->child != NULL))
        {
			 // Cerco gli elementi XML figli
			stationElement = topographyElement->child;
            while (stationElement)
            {
				if ((stationElement != NULL) && (stationElement->child != NULL)&&
                   ( XMLGetValue(stationElement,"id") == XMLGetValue(selectedElement->parent->parent,"id")) )
				{
                    // Cerco gli elementi XML figli
                    buildingElement = stationElement->child;
                    while (buildingElement)
                    {
                        if ((buildingElement != NULL) && (buildingElement->child != NULL)&&
                            ( XMLGetValue(buildingElement,"id") == XMLGetValue(selectedElement->parent,"id")) )
                        {
                            // Cerco gli elementi XML figli
                            locationElement = buildingElement->child;
                            while (locationElement)
                            {
                                if ( AnsiString(XMLGetValue(locationElement,"name")).Trim().LowerCase()  == locationName.Trim().LowerCase() &&
                                     (XMLGetValue(locationElement,"id") != XMLGetValue(selectedElement,"id")) )
                                {
									return true;
                                }

                                // Passo all'elemento successivo (se esiste)
								locationElement = locationElement->next;
							}
                        }
						// Passo all'elemento successivo (se esiste)
                        buildingElement = buildingElement->next;
                    }
                }
				// Passo all'elemento successivo (se esiste)
                stationElement = stationElement->next;
			}
        }
	}
    return false;
}


int __fastcall TfrmMain :: LoadRegionZoneNodeFromXML()
{
	AnsiString path;
	int ret_code = 0;

	path = ExtractFilePath(Application->ExeName) + "region_list.xml";
	RegionZoneNodeSource = XMLCreate(path.c_str());

	if (RegionZoneNodeSource != NULL)
	{
		ret_code = XMLOpen(RegionZoneNodeSource,NULL, 'R');
		if (ret_code == 0)
		{
			ret_code = XMLRead(RegionZoneNodeSource,NULL);
			if (ret_code == 0)
			{
				 ret_code = XMLCheck(RegionZoneNodeSource);
			}
		}
		XMLClose(RegionZoneNodeSource);
	}
	return ret_code;
}

void __fastcall TfrmMain :: LoadNameRegionCombo()
{
    XML_ELEMENT * regionZoneNodeElement;
	XML_ELEMENT * regionElement;

	cbNameRegion->Items->Clear();

	if (RegionZoneNodeSource != NULL)
	{
		regionZoneNodeElement = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);

        if ((regionZoneNodeElement != NULL) && (regionZoneNodeElement->child != NULL))
		{
             // Cerco gli elementi XML figli
            regionElement = regionZoneNodeElement->child;
			while (regionElement)
            {
				cbNameRegion->Items->Add(AnsiString(XMLGetValue(regionElement,"name")));

                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
}

void __fastcall TfrmMain::cbNameRegionChange(TObject *Sender)
{
	if (checkTextComboBoxInItemList(cbNameRegion))
		lblIDRegion->Caption = getIDRegionByName(cbNameRegion->Text.Trim());
	else
		lblIDRegion->Caption = "";
}
//---------------------------------------------------------------------------

AnsiString __fastcall TfrmMain :: getIDRegionByName(AnsiString regionName)
{
	XML_ELEMENT * element;

	if (RegionZoneNodeSource != NULL)
	{
		element = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);

		if ((element != NULL) && (element->child != NULL))
		{
			 // Cerco gli elementi XML figli

			element = element->child;
			while (element)
			{
				if (AnsiString(XMLGetValue(element,"name"))  == regionName)
				{
					return AnsiString(XMLGetValue(element,"id"));

				}
				// Passo all'elemento successivo (se esiste)
				element = element->next;
			}
		}
	}
	return "";
}

bool __fastcall TfrmMain :: RegionExist(AnsiString regionID)
{
    XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;

	if (SystemSource != NULL)
    {
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
			{
                if ( (AnsiString(XMLGetValue(regionElement,"RegID")) == regionID) &&
					(regionElement->pos != selectedElement->pos) )
                {
					return true;
                }
				// Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
			}
		}
	}
	return false;
}

void __fastcall TfrmMain :: LoadNameZoneCombo()
{
	XML_ELEMENT * regionZoneNodeElement = NULL;
	XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;

	cbNameZone->Items->Clear();

	if (RegionZoneNodeSource != NULL)
	{
		regionZoneNodeElement = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);

		if ((regionZoneNodeElement != NULL) && (regionZoneNodeElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
			regionElement = regionZoneNodeElement->child;
			while (regionElement)
			{
				if ((regionElement != NULL) && (regionElement->child != NULL) &&
					(AnsiString(XMLGetValue(regionElement,"id")) == AnsiString(XMLGetValue(selectedElement->parent,"RegID"))) )
				{
					// Cerco gli elementi XML figli
					zoneElement = regionElement->child;
					while (zoneElement)
					{
						cbNameZone->Items->Add(AnsiString(XMLGetValue(zoneElement,"name")));

						// Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
				}

                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
}

AnsiString __fastcall TfrmMain :: getIDZoneByName(AnsiString zoneName)
{
    XML_ELEMENT * regionZoneNodeElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;

	if (RegionZoneNodeSource != NULL)
    {
		regionZoneNodeElement = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);

		if ((regionZoneNodeElement != NULL) && (regionZoneNodeElement->child != NULL))
        {
			 // Cerco gli elementi XML figli
			regionElement = regionZoneNodeElement->child;
			while (regionElement)
			{
                if ((regionElement != NULL) && (regionElement->child != NULL) &&
					(AnsiString(XMLGetValue(regionElement,"id")) == AnsiString(XMLGetValue(selectedElement->parent,"RegID"))) )
                {
					// Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if (AnsiString(XMLGetValue(zoneElement,"name"))  == zoneName)
                        {
							return AnsiString(XMLGetValue(zoneElement,"id"));
                        }

                        // Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
                }

                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
    return "";
}

void __fastcall TfrmMain::cbNameZoneChange(TObject *Sender)
{
    if (checkTextComboBoxInItemList(cbNameZone))
		lblIDZone->Caption = getIDZoneByName(cbNameZone->Text.Trim());
}
//---------------------------------------------------------------------------

bool __fastcall TfrmMain :: ZoneExist(AnsiString zoneID)
{
	XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;

	if (SystemSource != NULL)
    {
        systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
		{
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
            {
				if ((regionElement != NULL) && (regionElement->child != NULL)&&
                   ( AnsiString(XMLGetValue(regionElement,"RegID")) == AnsiString(XMLGetValue(selectedElement->parent,"RegID"))) )
                {
                    // Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ( (AnsiString(XMLGetValue(zoneElement,"ZonID")) == zoneID) &&
                            (zoneElement->pos != selectedElement->pos) )
						{
                            return true;
                        }

						// Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
					}
                }
                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
			}
		}
	}
	return false;
}

void __fastcall TfrmMain :: LoadNameNodeCombo()
{
    XML_ELEMENT * regionZoneNodeElement = NULL;
    XML_ELEMENT * regionElement = NULL;
    XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;

	cbNameNode->Items->Clear();

    if (RegionZoneNodeSource != NULL)
    {
        regionZoneNodeElement = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);

		if ((regionZoneNodeElement != NULL) && (regionZoneNodeElement->child != NULL))
        {
             // Cerco gli elementi XML figli
            regionElement = regionZoneNodeElement->child;
            while (regionElement)
            {
				if ((regionElement != NULL) && (regionElement->child != NULL) &&
                    (AnsiString(XMLGetValue(regionElement,"id")) == AnsiString(XMLGetValue(selectedElement->parent->parent,"RegID"))) )
                {
                    // Cerco gli elementi XML figli
					zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL) &&
						(AnsiString(XMLGetValue(zoneElement,"id")) == AnsiString(XMLGetValue(selectedElement->parent,"ZonID"))) )
                        {
                            // Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
							{
								cbNameNode->Items->Add(AnsiString(XMLGetValue(nodeElement,"name")));

								// Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }

                        // Passo all'elemento successivo (se esiste)
                        zoneElement = zoneElement->next;
                    }
                }

                // Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
            }
        }
    }
}

AnsiString __fastcall TfrmMain :: getIDNodeByName(AnsiString nodeName)
{
	XML_ELEMENT * regionZoneNodeElement = NULL;
	XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
	XML_ELEMENT * nodeElement = NULL;

	if (RegionZoneNodeSource != NULL)
	{
		regionZoneNodeElement = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);

		if ((regionZoneNodeElement != NULL) && (regionZoneNodeElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
			regionElement = regionZoneNodeElement->child;
			while (regionElement)
			{
				if ((regionElement != NULL) && (regionElement->child != NULL) &&
					(AnsiString(XMLGetValue(regionElement,"id")) == AnsiString(XMLGetValue(selectedElement->parent->parent,"RegID"))) )
				{
					// Cerco gli elementi XML figli
					zoneElement = regionElement->child;
					while (zoneElement)
					{
						if ((zoneElement != NULL) && (zoneElement->child != NULL) &&
							(AnsiString(XMLGetValue(zoneElement,"id")) == AnsiString(XMLGetValue(selectedElement->parent,"ZonID"))) )
						{
							// Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
							{
								if (AnsiString(XMLGetValue(nodeElement,"name"))  == nodeName)
								{
									return AnsiString(XMLGetValue(nodeElement,"id"));
								}

								// Passo all'elemento successivo (se esiste)
								nodeElement = nodeElement->next;
							}
						}

						// Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
					}
				}

				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
			}
		}
	}
	return "";
}

void __fastcall TfrmMain :: setNoLocalNodes(AnsiString idNewLocal)
{
	XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
	XML_ELEMENT * nodeElement = NULL;
	XML_ATTRIBUTE * nodeAttribute = NULL;

	if (SystemSource != NULL)
	{
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

		if ((serverElement != NULL) && (serverElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
			regionElement = serverElement->child;
			while (regionElement)
			{
				if ((regionElement != NULL) && (regionElement->child != NULL))
				{
					// Cerco gli elementi XML figli
					zoneElement = regionElement->child;
					while (zoneElement)
					{
						if ((zoneElement != NULL) && (zoneElement->child != NULL))
						{
							// Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
							{
								if (AnsiString(XMLGetValue(nodeElement,"NodID"))  != idNewLocal)
								{
									if (XMLGetValue(nodeElement,"local") != NULL)
									{
										nodeAttribute = XMLGetAttrib(nodeElement,"local");
										if (nodeAttribute  != NULL)
										{
											XMLSetValue(nodeAttribute,"false");
										}
									}
								}

								// Passo all'elemento successivo (se esiste)
								nodeElement = nodeElement->next;
							}
						}

						// Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
					}
				}

				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
			}
		}
	}
}


bool __fastcall TfrmMain :: getExistLocalNode()
{
	XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
	XML_ELEMENT * nodeElement = NULL;

	if (SystemSource != NULL)
	{
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

		if ((serverElement != NULL) && (serverElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
			regionElement = serverElement->child;
			while (regionElement)
			{
				if ((regionElement != NULL) && (regionElement->child != NULL))
				{
					// Cerco gli elementi XML figli
					zoneElement = regionElement->child;
					while (zoneElement)
					{
						if ((zoneElement != NULL) && (zoneElement->child != NULL))
						{
							// Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
							{
								if (XMLGetValue(nodeElement,"local") != NULL)
								{
									if (AnsiString(XMLGetValue(nodeElement,"local"))  == "true")
									{
										return true;
									}
								}

								// Passo all'elemento successivo (se esiste)
								nodeElement = nodeElement->next;
							}
						}
						// Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
					}
				}
				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
			}
		}
	}
	return false;
}





void __fastcall TfrmMain::cbNameNodeChange(TObject *Sender)
{
	if (checkTextComboBoxInItemList(cbNameNode))
		lblIDNode->Caption = getIDNodeByName(cbNameNode->Text.Trim());
}
//---------------------------------------------------------------------------

bool __fastcall TfrmMain :: NodeExist(AnsiString nodeID)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;

    if (SystemSource != NULL)
    {
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

		if ((serverElement != NULL) && (serverElement->child != NULL))
		{
             // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
            {
                if ((regionElement != NULL) && (regionElement->child != NULL)&&
                   ( AnsiString(XMLGetValue(regionElement,"RegID")) == AnsiString(XMLGetValue(selectedElement->parent->parent,"RegID"))) )
				{
                    // Cerco gli elementi XML figli
					zoneElement = regionElement->child;
					while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL)&&
                           ( AnsiString(XMLGetValue(zoneElement,"ZonID")) == AnsiString(XMLGetValue(selectedElement->parent,"ZonID"))) )
						{
							// Cerco gli elementi XML figli
							nodeElement = zoneElement->child;
							while (nodeElement)
                            {
                                if ( (AnsiString(XMLGetValue(nodeElement,"NodID")) == nodeID) &&
                                (nodeElement->pos != selectedElement->pos) )
                                {
                                    return true;
                                }

                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
							}
                        }

                        // Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
                }

				// Passo all'elemento successivo (se esiste)
                regionElement = regionElement->next;
			}
        }
    }
    return false;
}


int __fastcall TfrmMain :: getIDServer(AnsiString * IDServer)
{
	unsigned long serialNumber;
	unsigned long ret_code = SPV_STLC1000_NO_ERROR;

	* IDServer = "0";

	ret_code = STLC1KInitJidaDll();
	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		ret_code = STLC1KJidaDllInitialize();
		if (ret_code == SPV_STLC1000_NO_ERROR)
		{
			ret_code = STLC1KOpenCPUBoard();
			if (ret_code == SPV_STLC1000_NO_ERROR)
			{
				ret_code = STLC1KReadSN(&serialNumber);
				if (ret_code == SPV_STLC1000_NO_ERROR)
				{
					* IDServer =  AnsiString(serialNumber);
				}
				STLC1KCloseCPUBoard();
			}
			STLC1KJidaDllUninitialize();
		}
		STLC1KClearJidaDll();
	}
	return ret_code;
}

bool __fastcall TfrmMain :: loadIDServer( void )
{
	XML_ELEMENT * systemElement = NULL;
	XML_ELEMENT * serverElement = NULL;
	XML_ATTRIBUTE * attribute;
	bool IDServer_changed = false;
	AnsiString SystemIdServer = "";
	AnsiString STLCIdServer = "";

	if (SystemSource != NULL)
	{
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

		serverElement = systemElement->child;

		SystemIdServer = (AnsiString(XMLGetValue(serverElement,"SrvID"))).Trim();

		if (getIDServer(&STLCIdServer) == SPV_STLC1000_NO_ERROR)
		{
			STLCIdServer = STLCIdServer.Trim();

			if (SystemIdServer != STLCIdServer)
			{
				if (MessageBox(NULL, "L'ID dell'STLC1000 non corrisponde a quello configurato. Vuoi Aggiornarlo?.", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_TASKMODAL + MB_TOPMOST ) == IDYES)
				{
					try
					{
						attribute = XMLGetAttrib(serverElement,"SrvID");
						if (attribute  != NULL)
						{
							XMLSetValue(attribute,STLCIdServer.c_str());
							IDServer_changed = true;
						}
					}
					catch(...)
					{
						MessageBox(NULL, "Fallito aggiornamento dell'ID dell'STLC1000.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
					}
				}
			}
		}
	}
	return IDServer_changed;
}

AnsiString __fastcall TfrmMain :: getHostNameServer()
{
	char *bHostName = NULL;
	AnsiString sHostName = "";

	try
	{
		SYSSocketInit();

		bHostName = SYSGetHostName();

		SYSSocketClear();

		for (int i=0; i<64; i++)
			sHostName = sHostName + AnsiString(bHostName[i]);
	}
	__finally
	{
		free(bHostName);
		return sHostName;
	}
}

void __fastcall TfrmMain :: loadHostNameServer(void)
{

	XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
	XML_ATTRIBUTE * attribute;

	if (SystemSource != NULL)
	{
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        attribute = XMLGetAttrib(serverElement,"host");
        if (attribute  != NULL)
        {
            XMLSetValue(attribute,getHostNameServer().Trim().c_str());
		}
    }
}

char * __fastcall TfrmMain::CodificaDeviceAddress(char * hexModemAddressValue, int decDeviceAddressValue)
{
	int iResult = 0;
    if (hexModemAddressValue != NULL)
    {
        unsigned __int8* pma_Value8 =  (unsigned __int8*)(XMLASCII2Type(hexModemAddressValue,t_u_hex_8));
		int modemAddr = *pma_Value8; // Indirizzo modem
        if (modemAddr == 255)              // 0xFF = 255
            modemAddr = 0;
        iResult = (modemAddr * 16) + decDeviceAddressValue;
        free(pma_Value8);
    }
    char * sResult = XMLType2ASCII(&iResult,t_u_hex_8);
	return sResult;
}


int __fastcall TfrmMain::DecodificaDeviceAddress(char * hexModemAddressValue, char * hexCodifyDeviceAddressValue)
{
	int iResult = 0;
	if ((AnsiString(hexCodifyDeviceAddressValue) != NULL) && (Trim(AnsiString(hexCodifyDeviceAddressValue)) != ""))
	{
        unsigned __int8* pValue8 =  (unsigned __int8*)(XMLASCII2Type(hexCodifyDeviceAddressValue,t_u_hex_8));
		int devAddr = *pValue8; // Indirizzo modem (4 bit) + periferica (4 bit)
		if (hexModemAddressValue != NULL)
		{
			unsigned __int8* pma_Value8 =  (unsigned __int8*)(XMLASCII2Type(hexModemAddressValue,t_u_hex_8));
            int modemAddr = *pma_Value8; // Indirizzo modem
            if (modemAddr == 255)              // 0xFF = 255
                modemAddr = 0;
			iResult = devAddr - (modemAddr * 16); // Tolgo l'indirizzo modem
        }
    }
    return iResult; // Indirizzo periferica
}

void __fastcall TfrmMain :: RicodificaDeviceAddress(char * hexModemAddressNewValue)
{
    XML_ELEMENT * systemElement = NULL;
    XML_ELEMENT * serverElement = NULL;
    XML_ELEMENT * regionElement = NULL;
	XML_ELEMENT * zoneElement = NULL;
    XML_ELEMENT * nodeElement = NULL;
    XML_ELEMENT * deviceElement = NULL;
    XML_ATTRIBUTE * attribute;
    int decDeviceAddressValue = 0;
    char *  sCodifyDevAddres = NULL;

	if (SystemSource != NULL)
    {
		systemElement = XMLResolveElement(SystemSource->root_element,"system",NULL);

        serverElement = systemElement->child;

        if ((serverElement != NULL) && (serverElement->child != NULL))
		{
			 // Cerco gli elementi XML figli
            regionElement = serverElement->child;
            while (regionElement)
            {
				if ((regionElement != NULL) && (regionElement->child != NULL)&&
					(XMLGetValue(regionElement,"RegID") == XMLGetValue(selectedElement->parent->parent,"RegID")) )
				{
					// Cerco gli elementi XML figli
                    zoneElement = regionElement->child;
                    while (zoneElement)
                    {
                        if ((zoneElement != NULL) && (zoneElement->child != NULL)&&
                            (XMLGetValue(zoneElement,"ZonID") == XMLGetValue(selectedElement->parent,"ZonID")) )
                        {
                            // Cerco gli elementi XML figli
                            nodeElement = zoneElement->child;
							while (nodeElement)
							{
                                if ((nodeElement != NULL) && (nodeElement->child != NULL)&&
                                    (XMLGetValue(nodeElement,"NodID") == XMLGetValue(selectedElement,"NodID")) )
								{
                                    if ( XMLGetValue(selectedElement,"modem_addr") != hexModemAddressNewValue )
                                    {
                                        // Cerco gli elementi XML figli
                                        deviceElement = nodeElement->child;
                                        while (deviceElement)
                                        {
											if (getTypePort(StrToInt(AnsiString(XMLGetValue(deviceElement,"port")))) != "TCP_Client")
                                            {
												//decodificazione dell'indirizzo della periferica
												decDeviceAddressValue = DecodificaDeviceAddress(XMLGetValue(selectedElement,"modem_addr"),XMLGetValue(deviceElement,"addr"));

                                                //ricodificazione dell'indirizzo della periferica
                                                attribute = XMLGetAttrib(deviceElement,"addr");
                                                if (attribute  != NULL)
                                                {
													sCodifyDevAddres = CodificaDeviceAddress(hexModemAddressNewValue,decDeviceAddressValue);
													XMLSetValue(attribute,sCodifyDevAddres);
                                                    free(sCodifyDevAddres);
                                                }
                                            }
											// Passo all'elemento successivo (se esiste)
											deviceElement = deviceElement->next;
										}
									}
                                }
                                // Passo all'elemento successivo (se esiste)
                                nodeElement = nodeElement->next;
                            }
                        }
                        // Passo all'elemento successivo (se esiste)
						zoneElement = zoneElement->next;
                    }
                }
				// Passo all'elemento successivo (se esiste)
				regionElement = regionElement->next;
            }
        }
    }
}





int __fastcall TfrmMain :: OpenXMLCentralization(AnsiString path)
{
	int ret_code = -1;

	try
	{
		CentralizationSource = XMLCreate(path.c_str());
		if (CentralizationSource != NULL)
		{
			ret_code = XMLOpen(CentralizationSource,NULL, 'R');
			if (ret_code == 0)
			{
				ret_code = XMLRead(CentralizationSource,NULL);
				if (ret_code == 0)
				{
					 XMLCheck(CentralizationSource);
				}
			}
			XMLClose(CentralizationSource);
		}

		/*if (ret_code == 0)
		{
			if (!CheckSCAgent())
			{
				MessageBox(NULL, "File SCAgentService corrotto.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
				ret_code = -1;
			}
		}     */

		return ret_code;
	}
	catch(...)
	{
		return -1;
	}
}

void __fastcall TfrmMain :: SetcbActiveCentralizationService()
{
	if (GetServiceStartType("SCAgentService") == SYS_KERNEL_QSC_SERVICE_AUTO_START)
	{
		cbActiveCentralizationService->Checked = true;
	}
	else
	{
		cbActiveCentralizationService->Checked = false;
	}
}


bool __fastcall TfrmMain :: CheckSCAgent()
{

	XML_ELEMENT * ConfigElement;
	XML_ELEMENT * ConfigChildsElement;
	AnsiString nodeName = "";
	AnsiString boolDBRead = "" ;
	AnsiString boolDBWrite = "";
	AnsiString boolUpLink = "";
	AnsiString boolDownLink = "";

	bool retValue = false;

	try
	{
		if (CentralizationSource != NULL)
		{
			ConfigElement = XMLResolveElement(CentralizationSource->root_element,"config",NULL);

			if ((ConfigElement != NULL) && (ConfigElement->child != NULL))
			{
				// Cerco gli elementi XML figli
				ConfigChildsElement = ConfigElement->child;
				while (ConfigChildsElement)
				{
					nodeName = ConfigChildsElement->name;
					if (nodeName == "dbread")
					{
						if (XMLGetValue(ConfigChildsElement,"enabled") != NULL)
						{
							boolDBRead = AnsiString(XMLGetValue(ConfigChildsElement,"enabled"));
						}
					}
					else
					if (nodeName == "dbwrite")
					{
						if (XMLGetValue(ConfigChildsElement,"enabled") != NULL)
						{
							boolDBWrite = AnsiString(XMLGetValue(ConfigChildsElement,"enabled"));
						}
					}
					else
					if (nodeName == "uplink")
					{
						if (XMLGetValue(ConfigChildsElement,"enabled") != NULL)
						{
							boolUpLink = AnsiString(XMLGetValue(ConfigChildsElement,"enabled"));
						}
					}
					else
					if (nodeName == "downlink")
					{
						if (XMLGetValue(ConfigChildsElement,"enabled") != NULL)
						{
							boolDownLink = AnsiString(XMLGetValue(ConfigChildsElement,"enabled"));
						}
					}
					// Passo all'elemento successivo (se esiste)
					ConfigChildsElement = ConfigChildsElement->next;
				}

				if ( ((boolDBRead == "true") || (boolDBRead == "false")) &&
					 ((boolDBWrite == "true") || (boolDBWrite == "false")) &&
					 ((boolUpLink == "true") || (boolUpLink == "false")) &&
					 ((boolDownLink == "true") || (boolDownLink == "false")))
				{
					retValue = true;
				}
			}
		}
	}catch(...)
	{
		return false;
	}
	return retValue;
}


//==============================================================================
/// Funzione per arrestare un servizio, settare la modalilt� di un servizio NT
/// (Automatico,Manuale,Disabilitato) e farlo ripartire o meno di conseguenza
///
/// \date [12.10.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfrmMain ::ManageService(DWORD dwStartTypeValue, char * service )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	SC_HANDLE schSetService = NULL;
	bool serviceIsStopped;

	ret_code = SYSOpenSCManager("",NULL,1); // SCM_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,55);
		if (schService != NULL)
		{

			ret_code = SYSServiceIsStopped(schService,&serviceIsStopped); //	if (! SYSServiceIsStopped(schService))

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (! serviceIsStopped)
				{
					ret_code = SYSStopService(schService,5000);
				}

				if (ret_code == SYS_KERNEL_NO_ERROR)
				{
					ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);

					if (ret_code == SYS_KERNEL_NO_ERROR)
					{
						if (serviceIsStopped)
						{
							schSetService = SYSOpenService(service, SERVICE_CHANGE_CONFIG );
							ret_code = SYSSetServiceStartType(dwStartTypeValue,schSetService);
							SYSCloseService(schSetService);
						}
						else
						{
							ret_code = SYS_KERNEL_SERVICE_NOT_STOPPED;
						}
					}
				}
			}

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (dwStartTypeValue == SERVICE_AUTO_START)
				{
					ret_code = SYSStartService(schService);
				}
				else
				if ((dwStartTypeValue != SERVICE_DEMAND_START) && (dwStartTypeValue != SERVICE_DISABLED))
				{
					ret_code = SYS_KERNEL_INVALID_START_TYPE;
				}
			}

			SYSCloseService(schService);
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}


		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{
			SYSCloseSCManager();
		}
	}

	return ret_code;

	/*int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	SC_HANDLE schSetService = NULL;

	ret_code = SYSOpenSCManager("",NULL,SCM_ACCESS_CODE); // SCM_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,CONFIGURATOR_OPEN_CODE);
		if (schService != NULL)
		{

			if (! SYSServiceIsStopped(schService))
			{
				ret_code = SYSStopService(schService,5000);
			}

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (SYSServiceIsStopped(schService))
				{
					schSetService = SYSOpenService(service, SERVICE_CHANGE_CONFIG );
					ret_code = SYSSetServiceStartType(dwStartTypeValue,schSetService);
					SYSCloseService(schSetService);
				}
				else
				{
					ret_code = SYS_KERNEL_SERVICE_NOT_STOPPED;
				}
			}

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (dwStartTypeValue == SERVICE_AUTO_START)
				{
					ret_code = SYSStartService(schService);
				}
				else
				if ((dwStartTypeValue != SERVICE_DEMAND_START) && (dwStartTypeValue != SERVICE_DISABLED))
				{
					ret_code = SYS_KERNEL_INVALID_START_TYPE;
				}
			}

			SYSCloseService(schService);
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{
			SYSCloseSCManager();
		}
	}

	return ret_code;*/
}


//==============================================================================
/// Funzione per arrestare e far ripartire un Servizio NT
///
/// \date [12.10.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfrmMain ::RestartService(char * service )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;

	ret_code = SYSOpenSCManager("",NULL,SCM_ACCESS_CODE); // SCM_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,CONFIGURATOR_OPEN_CODE);
		if (schService != NULL)
		{
			ret_code = SYSRestartService(schService,5000);
			SYSCloseService(schService);
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{
			SYSCloseSCManager();
		}
	}
	return ret_code;
}


//==============================================================================
/// Funzione per arrestare un Servizio NT
///
/// \date [06.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfrmMain ::StopService(char * service )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	bool serviceIsStopped;

	ret_code = SYSOpenSCManager("",NULL,SCM_ACCESS_CODE); // SCM_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,CONFIGURATOR_OPEN_CODE);
		if (schService != NULL)
		{
			ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (! serviceIsStopped)
				{
					ret_code = SYSStopService(schService,5000);
				}
			}
			SYSCloseService(schService);
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{
			SYSCloseSCManager();
		}
	}
	return ret_code;
}


//==============================================================================
/// Funzione per far partire un Servizio NT
///
/// \date [06.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfrmMain ::StartService(char * service )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	bool serviceIsStopped;

	ret_code = SYSOpenSCManager("",NULL,SCM_ACCESS_CODE); // SCM_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,CONFIGURATOR_OPEN_CODE);
		if (schService != NULL)
		{
			ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (serviceIsStopped)
				{
					ret_code = SYSStartService(schService);
				}
			}
			SYSCloseService(schService);
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{
			SYSCloseSCManager();
		}
	}
	return ret_code;
}

//==============================================================================
/// Funzione per recuperare la modalit� di partenza di un Servizio NT
/// (Automtico,Manuale,Disabilitato)
/// con gestione dell'handle del servizio
///
/// \date [15.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int __fastcall TfrmMain :: GetServiceStartType( char * service )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;

	ret_code = SYSOpenSCManager("",NULL,SCM_ACCESS_CODE);// SCM_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,CONFIGURATOR_OPEN_CODE);
		if (schService != NULL) {
			ret_code = SYSGetServiceStartType(schService);
			SYSCloseService(schService);
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}
		SYSCloseSCManager();
	}
	return ret_code;
}

bool __fastcall TfrmMain::checkTextEditIsValid(TEdit *e)
{
	bool isValid = false;

	if (e->Text.Trim() != "")
	{
		e->Color = clWindow;
		isValid = true;
	}
	else
	{
		e->Color = 0x00BBDDFF;
	}

	return isValid;
}

bool __fastcall TfrmMain::checkTextComboBoxInItemList(TComboBox *cb)
{
	bool isInList = false;
	for (int i=0; i < cb->Items->Count;i++)
	{
		if (cb->Text == cb->Items->Strings[i])
		{
			cb->Color = clWindow;
			isInList = true;
			break;
		}
	}

	if (!isInList)
	{
	    cb->ItemIndex = -1;
		cb->Color = 0x00BBDDFF;
		if (cb->Width > 65)
		{
			cb->Text = "Seleziona ->";
		}
		else
		{
			cb->Text = "Sel. ->";
		}
	}

	return isInList;
}

bool __fastcall TfrmMain::checkTextComboBoxIsValid(TComboBox *cb)
{
	bool isValid = false;

	if((cb->Text.Trim().Length() > 0) &&
	   ((cb->Text.Trim() != "Seleziona ->") && (cb->Text.Trim() != "Sel. ->")))
	{
		isValid = true;
	}
	return  isValid;
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::btnUpdateItemClick(TObject *Sender)
{
	MakeItemPageEnabled();
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::btnUpdateServerClick(TObject *Sender)
{
	MakeServerPageEnabled();
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::btnUpdateRegionClick(TObject *Sender)
{
	MakeRegionPageEnabled();
}

void __fastcall TfrmMain::btnUpdateZoneClick(TObject *Sender)
{
	MakeZonePageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateNodeClick(TObject *Sender)
{
	MakeNodePageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateDeviceClick(TObject *Sender)
{
	MakeDevicePageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateStationClick(TObject *Sender)
{
	MakeStationPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateBuildingClick(TObject *Sender)
{
	MakeBuildingPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateLocationClick(TObject *Sender)
{
	MakeLocationPageEnabled();
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain :: CheckKey(char &KeyValue)
{
	if ((KeyValue == '"') || (KeyValue == char(39)))
	{
		KeyValue = '`';
	}
	else if ((KeyValue == '>') || (KeyValue == '<'))
	{
		KeyValue = NULL;
	}

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}

//------------------------------------------------------------------------------
void __fastcall TfrmMain::eIP1ItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP2ItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP3ItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP4ItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ePortItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eTimeOutItemKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eHostServerKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameServerKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIDDeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eSNDeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::eNameDeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP1DeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP2DeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP3DeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP4DeviceKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameStationKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNoteBuildingKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameBuildingKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameLocationKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNoteLocationKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::cbTypeItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbCOMItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbBaudItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbEchoItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbDataItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbStopItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbParityItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbPersistentItemKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameItemChange(TObject *Sender)
{
	checkTextEditIsValid(eNameItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP1ItemChange(TObject *Sender)
{
	checkTextEditIsValid(eIP1Item);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP2ItemChange(TObject *Sender)
{
	checkTextEditIsValid(eIP2Item);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP3ItemChange(TObject *Sender)
{
	checkTextEditIsValid(eIP3Item);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP4ItemChange(TObject *Sender)
{
	checkTextEditIsValid(eIP4Item);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ePortItemChange(TObject *Sender)
{
	checkTextEditIsValid(ePortItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eTimeOutItemChange(TObject *Sender)
{
	checkTextEditIsValid(eTimeOutItem);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eHostServerChange(TObject *Sender)
{
	checkTextEditIsValid(eHostServer);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameServerChange(TObject *Sender)
{
	checkTextEditIsValid(eNameServer);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbNameRegionKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbNameZoneKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbNameNodeKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbAddrModemNodeKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIDDeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eIDDevice);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eSNDeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eSNDevice);		
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameDeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eNameDevice);		
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP1DeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eIP1Device);		
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP2DeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eIP2Device);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP3DeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eIP3Device);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eIP4DeviceChange(TObject *Sender)
{
	checkTextEditIsValid(eIP4Device);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbTypeDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbStationDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbBuildingDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbLocationDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbPortDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbIPDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbActiveDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbScheduledDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbSupervisorIdKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbProfileDeviceKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cbTypeLocationKeyPress(TObject *Sender, char &Key)
{
	Key = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameStationChange(TObject *Sender)
{
	checkTextEditIsValid(eNameStation);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameBuildingChange(TObject *Sender)
{
	checkTextEditIsValid(eNameBuilding );
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNoteBuildingChange(TObject *Sender)
{
	checkTextEditIsValid(eNoteBuilding);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNameLocationChange(TObject *Sender)
{
	checkTextEditIsValid(eNameLocation);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eNoteLocationChange(TObject *Sender)
{
	checkTextEditIsValid(eNoteLocation);
}
//---------------------------------------------------------------------------

//==============================================================================
/// Funzione per ottenere il percorso e il nome file della configurazione sistema
//------------------------------------------------------------------------------
char * __fastcall TfrmMain::GetXMLSystemPathFileName( void )
{
	void        * key               = NULL;
	char        * xml_system_value  = NULL;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SPV_CFG_SERVER_KEY, false );
	if ( key != NULL )
	{
		xml_system_value = (char*) SYSGetRegValue( key, SPV_CFG_XML_SYSTEM_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	if ((xml_system_value == NULL) || ((AnsiString(xml_system_value))== "") ){
		xml_system_value = GetCharPointer(SPV_CFG_XML_SYSTEM_DEFAULT_VALUE_PATH);
	}

	return xml_system_value;
}

void __fastcall TfrmMain::actSaveCentralizationExecute(TObject *Sender)
{
   //	XML_ATTRIBUTE * attribute;
   //	XML_ATTRIBUTE * newAttribute = NULL;
   //	AnsiString sIPValue = "";
   //	AnsiString errMsg = "";

	/*  Forse da fare in secondo momento
	try{

		if ( !(checkInt(eIP1Centralization->Text)) || !(checkInt(eIP2Centralization->Text)) ||
			 !(checkInt(eIP3Centralization->Text)) || !(checkInt(eIP4Centralization->Text)) )
		{
			MessageBox(NULL, " Errore inserimento Indirizzo Host.\r\n Inserire 4 valori numerici interi positivi.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		if ( ((StrToInt(eIP1Centralization->Text)) < 0) || ((StrToInt(eIP1Centralization->Text)) > MAX_NUM_IP) ||
			 ((StrToInt(eIP2Centralization->Text)) < 0) || ((StrToInt(eIP2Centralization->Text)) > MAX_NUM_IP) ||
			 ((StrToInt(eIP3Centralization->Text)) < 0) || ((StrToInt(eIP3Centralization->Text)) > MAX_NUM_IP) ||
			 ((StrToInt(eIP4Centralization->Text)) < 0) || ((StrToInt(eIP4Centralization->Text)) > MAX_NUM_IP) )
		{
			errMsg = " Errore inserimento Indirizzo Host.\r\n Inserire 4 valori compresi tra 0 e " + (IntToStr(MAX_NUM_IP))+".";
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}


	}
	catch (EConvertError &E)
	{
		errMsg = " Errore inserimento valore intero.\r\n Inserire valori compresi tra 0 e " + (IntToStr(MAX_INTEGER))+".";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}
	*/
	///attribute = XMLGetAttrib(centralizationElement,"host");

	/*   Forse da fare in secondo momento
	sIPValue = eIP1Centralization->Text.Trim() + "." + eIP2Centralization->Text.Trim() + "." +
			   eIP3Centralization->Text.Trim() + "." + eIP4Centralization->Text.Trim();
	*/


	/*if (attribute  != NULL)
	{
		XMLSetValue(attribute,sIPValue.c_str());
	}
	else
	{
		newAttribute = XMLNewAttrib(selectedElement,"host",sIPValue.c_str());
		XMLAddAttrib(selectedElement,newAttribute);
	}*/
	AnsiString errMsg = "";
	XML_ELEMENT * intervalSecondsElement = NULL;

	try
	{
		if  (!(checkInt(eIntervalSeconds->Text)))
		{
			MessageBox(NULL, " Errore inserimento valore di IntervalSeconds.\r\n Inserire un valore numerico intero positivo.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		if (((StrToInt(eIntervalSeconds->Text)) < 300) || ((StrToInt(eIntervalSeconds->Text)) > MAX_INTEGER )) // (2^31) - 1 = 2147483647
		{
			errMsg = " Errore inserimento valore di IntervalSeconds.\r\n Inserire un valore compreso tra 300 e " + (IntToStr(MAX_INTEGER))+".";
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}
	}
	catch (EConvertError &E)
	{
		errMsg = " Errore inserimento valore intero di IntervalSeconds.\r\n Inserire valori compresi tra 300 e " + (IntToStr(MAX_INTEGER))+".";
		MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
		return;
	}


	setHostCentralizationNode("Telefin_SCAgent_TelefinCentralWS_SCACollector",eHostNameCentralization->Text);
	setHostCentralizationNode("SCACollectorAddressBackup",eHostNameCentralization->Text);


	intervalSecondsElement = GetCentralizationNodeByName("IntervalSeconds");
	if ((intervalSecondsElement != NULL) &&
		(intervalSecondsElement->text != NULL) &&
		(intervalSecondsElement->text_count > 0))
	{

		intervalSecondsElement->text->length = eIntervalSeconds->Text.Length();

		intervalSecondsElement->text->value = XMLStrCpy(intervalSecondsElement->text->value,eIntervalSeconds->Text.c_str());
	}


	EnabledContextFormByAddServices(true);
	eHostNameCentralization->Enabled = false;
	eIntervalSeconds->Enabled = false;
	cbActiveCentralizationService->Enabled = false;

	config_status = CONFIGURATION_MODIFIED;	
  ///	isNewNode = false;


}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::actSaveCentralizationUpdate(TObject *Sender)
{
	/* Forse da fare in secondo momento
	if ((eIP1Centralization->Text.Trim().Length() > 0) &&
		(eIP2Centralization->Text.Trim().Length() > 0) &&
		(eIP3Centralization->Text.Trim().Length() > 0) &&
		(eIP4Centralization->Text.Trim().Length() > 0) &&
		(eIP1Centralization->Enabled) )  // eIP1Centralization->Enabled serve per capirese si � in insertMode od in editMode
	*/
	if ((eHostNameCentralization->Text.Trim().Length() > 0) &&
		(eIntervalSeconds->Text.Trim().Length() > 0) )
	{
		actSaveCentralization->Enabled = true;
	}
	else
	{
		actSaveCentralization->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateCentralizationClick(TObject *Sender)
{
	MakeCentralizationPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelCentralizationClick(TObject *Sender)
{
	CentralizationPageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::CentralizationPageCancel()
{
	EnabledContextFormByAddServices(true);
	eHostNameCentralization->Enabled = false;
	eIntervalSeconds->Enabled = false;
	cbActiveCentralizationService->Enabled = false;

	loadEditsCentralization();
}

void __fastcall TfrmMain::SNMPManagerPageCancel()
{
	EnabledContextFormByAddServices(true);
	eCommunity->Enabled = false;
	eTrapCommunity->Enabled = false;
	cbUseSystemXMLCommunity->Enabled = false;
	loadControlSNMPManager();
}

void __fastcall TfrmMain::miUpdateCentralizationClick(TObject *Sender)
{
	MakeCentralizationPageEnabled();
}
//---------------------------------------------------------------------------


XML_ELEMENT * __fastcall TfrmMain :: GetCentralizationNodeByName(AnsiString nodeNameValue)
{
	XML_ELEMENT * ConfigElement;
	XML_ELEMENT * ConfigChildsElement;
	XML_ELEMENT * ApplicationChildsElement;
	XML_ELEMENT * TPSChildsElement;
	XML_ELEMENT * ValueChildsElement;
	AnsiString nodeName = "";

	if (CentralizationSource != NULL)
	{
		ConfigElement = CentralizationSource->root_element; //XMLResolveElement(CentralizationSource->root_element,"configuration",NULL);

		if ((ConfigElement != NULL) && (ConfigElement->child != NULL))
		{
			// Cerco gli elementi XML figli
			ConfigChildsElement = ConfigElement->child;
			while (ConfigChildsElement)
			{
				nodeName = ConfigChildsElement->name;
				if (nodeName == "applicationSettings")
				{
					if ((ConfigChildsElement != NULL) && (ConfigChildsElement->child != NULL))
					{
						// Cerco gli elementi XML figli
						ApplicationChildsElement = ConfigChildsElement->child;
						while (ApplicationChildsElement)
						{
							nodeName = ApplicationChildsElement->name;
							if (nodeName == "Telefin.Properties.Settings")
							{
								if ((ApplicationChildsElement != NULL) && (ApplicationChildsElement->child != NULL))
								{
									// Cerco gli elementi XML figli
									TPSChildsElement = ApplicationChildsElement->child;
									while (TPSChildsElement)
									{
										nodeName = TPSChildsElement->name;
										if ((nodeName == "setting") &&
										   (AnsiString(XMLGetValue(TPSChildsElement,"name")) == nodeNameValue))
											//(AnsiString(XMLGetValue(TPSChildsElement,"name")) == "Telefin_SCAgent_TelefinCentralWS_SCACollector"))
										{
											if ((TPSChildsElement != NULL) && (TPSChildsElement->child != NULL))
											{
												// Cerco gli elementi XML figli
												ValueChildsElement = TPSChildsElement->child;
												while (ValueChildsElement)
												{
													nodeName = ValueChildsElement->name;
													if (nodeName == "value")
													{
														return ValueChildsElement;
													}
													// Passo all'elemento successivo (se esiste)
													ValueChildsElement = ValueChildsElement->next;
												}
											}
										}
										// Passo all'elemento successivo (se esiste)
										TPSChildsElement = TPSChildsElement->next;
									}
								}
							}
							// Passo all'elemento successivo (se esiste)
							ApplicationChildsElement = ApplicationChildsElement->next;
						}
					}
				}
				// Passo all'elemento successivo (se esiste)
				ConfigChildsElement = ConfigChildsElement->next;
			}
		}
	}
	return NULL;
}

void __fastcall TfrmMain :: loadEditsCentralization()
{
	XML_ELEMENT * hostElement = NULL;
	XML_ELEMENT * backupHostElement = NULL;
	XML_ELEMENT * intervalSecElement = NULL;

	hostElement = GetCentralizationNodeByName("Telefin_SCAgent_TelefinCentralWS_SCACollector");

	if ((hostElement != NULL) &&
		(hostElement->text != NULL) &&
		(hostElement->text_count > 0))
	{

		char * hostPathTokenValue_Partial = NULL;
		char * hostPathTokenValue_Final = NULL;

		char * hostPathTokenValue_Total = hostElement->text->value;
		hostPathTokenValue_Partial = XMLGetToken(hostPathTokenValue_Total,"//",1);
		hostPathTokenValue_Final = XMLGetToken(hostPathTokenValue_Partial,"/",0);

		AnsiString sIPCentralizationValue = AnsiString(hostPathTokenValue_Final);

		// non fare il free di hostPathTokenValue_Total perch�
		// punta alla stessa menoria di centralizationElement->text->value
		free(hostPathTokenValue_Partial);
		free(hostPathTokenValue_Final);

		eHostNameCentralization->Text = sIPCentralizationValue;
		/*  Forse da fare in secondo momento
		char * ipTokenValue = NULL;
		ipTokenValue = XMLGetToken(sIPCentralizationValue.c_str(),".",0);
		eIP1Centralization->Text = ipTokenValue;
		free(ipTokenValue);

		ipTokenValue = XMLGetToken(sIPCentralizationValue.c_str(),".",1);
		eIP2Centralization->Text = ipTokenValue;
		free(ipTokenValue);

		ipTokenValue = XMLGetToken(sIPCentralizationValue.c_str(),".",2);
		eIP3Centralization->Text = ipTokenValue;
		free(ipTokenValue);

		ipTokenValue = XMLGetToken(sIPCentralizationValue.c_str(),".",3);
		eIP4Centralization->Text = ipTokenValue;
		free(ipTokenValue);
		*/
	}

	backupHostElement = GetCentralizationNodeByName("SCACollectorAddressBackup");

	if ((backupHostElement != NULL) &&
		(backupHostElement->text != NULL) &&
		(backupHostElement->text_count > 0))
	{

		char * backupHostPathTokenValue_Partial = NULL;
		char * backupHostPathTokenValue_Final = NULL;

		char * backupHostPathTokenValue_Total = backupHostElement->text->value;
		backupHostPathTokenValue_Partial = XMLGetToken(backupHostPathTokenValue_Total,"//",1);
		backupHostPathTokenValue_Final = XMLGetToken(backupHostPathTokenValue_Partial,"/",0);

		AnsiString sbackupHostCentralizationValue = AnsiString(backupHostPathTokenValue_Final);

		// non fare il free di backupHostPathTokenValue_Total perch�
		// punta alla stessa menoria di centralizationElement->text->value
		free(backupHostPathTokenValue_Partial);
		free(backupHostPathTokenValue_Final);

	   //	eBackupHostNameCentralization->Text = sbackupHostCentralizationValue;
	}

	intervalSecElement = GetCentralizationNodeByName("IntervalSeconds");

	if ((intervalSecElement != NULL) &&
		(intervalSecElement->text != NULL) &&
		(intervalSecElement->text_count > 0))
	{

		AnsiString sIntSecValue = AnsiString(intervalSecElement->text->value);
		eIntervalSeconds->Text = sIntSecValue;
	}


}

void __fastcall TfrmMain :: setHostCentralizationNode(AnsiString nodeNameValue, AnsiString sHostCentralizationValue)
{
	XML_ELEMENT * centralizationElement = NULL;
	centralizationElement = GetCentralizationNodeByName(nodeNameValue);

	if ((centralizationElement != NULL) &&
		(centralizationElement->text != NULL) &&
		(centralizationElement->text_count > 0))
	{

		char * hostPathTokenValue_First = NULL;
		char * hostPathTokenValue_Second = NULL;
		char * hostPathTokenValue_OldHost = NULL;

		char * hostPathTokenValue_Total = centralizationElement->text->value;
		hostPathTokenValue_First = XMLGetToken(hostPathTokenValue_Total,"//",0);
		hostPathTokenValue_Second = XMLGetToken(hostPathTokenValue_Total,"//",1);
		hostPathTokenValue_OldHost = XMLGetToken(hostPathTokenValue_Second,"/",0);

		// QUI fare il free di hostPathTokenValue_Total perch� dopo
		// centralizationElement->text->value punter� ad un'altra parte di menoria
		//free(hostPathTokenValue_Total);

		AnsiString sOldHost = AnsiString(hostPathTokenValue_OldHost);
		AnsiString sFinal = AnsiString(hostPathTokenValue_Second);

		sFinal = sFinal.SubString((sOldHost.Length()+1),(sFinal.Length()-sOldHost.Length()));

		AnsiString newHostValue = AnsiString(hostPathTokenValue_First) + "//" +
								  sHostCentralizationValue +
								  sFinal;

		centralizationElement->text->length = newHostValue.Length();

		centralizationElement->text->value = XMLStrCpy(centralizationElement->text->value,newHostValue.c_str());


		free(hostPathTokenValue_First);
		free(hostPathTokenValue_Second);
		free(hostPathTokenValue_OldHost);
	}

}


int __fastcall TfrmMain :: OpenIniSNMPManagerConfig(AnsiString pathValue)
{
	TIniFile * IniSNMPManagerConfig = NULL;
	AnsiString sUseSystemXMLCommunity = "";
	int ret_code = -1;

	try
	{
		try{
			IniSNMPManagerConfig = new TIniFile(pathValue);
			communityValue = IniSNMPManagerConfig->ReadString( "SNMP", "community", "public" );
			trapCommunityValue = IniSNMPManagerConfig->ReadString( "SNMP", "trap_community_filter", "public" );
			sUseSystemXMLCommunity = IniSNMPManagerConfig->ReadString( "APPLICATION", "use_system_xml", "False");
			useSystemXMLCommunity = cnv_CharPToBool(sUseSystemXMLCommunity.c_str());
			ret_code = 0;
		}//try
		catch(...)
		{
			ret_code = -1;
		}//catch
	}
	__finally
	{
		 delete IniSNMPManagerConfig;
		 return ret_code;
	}
}

int __fastcall TfrmMain :: SaveIniSNMPManagerConfig(AnsiString pathValue)
{
	TIniFile * IniSNMPManagerConfig = NULL;
	int ret_code = -1;

	try
	{
		try{
			IniSNMPManagerConfig = new TIniFile(pathValue);
			IniSNMPManagerConfig->WriteString ( "SNMP", "community", communityValue);
			IniSNMPManagerConfig->WriteString ( "SNMP", "trap_community_filter", trapCommunityValue);
			IniSNMPManagerConfig->WriteString ( "APPLICATION", "use_system_xml", cnv_BoolToCharP(useSystemXMLCommunity));
			ret_code = 0;
		}//try
		catch(...)
		{
			ret_code = -1;
		}//catch
	}
	__finally
	{
		 delete IniSNMPManagerConfig;
		 return ret_code;
	}
}

void __fastcall TfrmMain :: loadControlSNMPManager()
{
	eCommunity->Text = communityValue;
	eTrapCommunity->Text = trapCommunityValue;
	cbUseSystemXMLCommunity->Checked = useSystemXMLCommunity;
}

void __fastcall TfrmMain::actSaveSNMPExecute(TObject *Sender)
{
	communityValue = eCommunity->Text;
	trapCommunityValue = eTrapCommunity->Text;
	useSystemXMLCommunity = cbUseSystemXMLCommunity->Checked;
	EnabledContextFormByAddServices(true);
	eCommunity->Enabled = false;
	eTrapCommunity->Enabled = false;
	cbUseSystemXMLCommunity->Enabled = false;
	config_status = CONFIGURATION_MODIFIED;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::actSaveSNMPUpdate(TObject *Sender)
{
	if ((eCommunity->Text.Trim().Length() > 0) && (eTrapCommunity->Text.Trim().Length() > 0))
	{
		actSaveSNMP->Enabled = true;
	}
	else
	{
		actSaveSNMP->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnUpdateSNMPClick(TObject *Sender)
{
	MakeSNMPManagerPageEnabled();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnCancelSNMPClick(TObject *Sender)
{
	SNMPManagerPageCancel();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::miUpdateSNMPClick(TObject *Sender)
{
	MakeSNMPManagerPageEnabled();
}
//---------------------------------------------------------------------------


int SaveNewRegionListXMLFile( void )
{
	int ret_code = 0;
	AnsiString msgErr;

   OutputSource = XMLCreate("region_list.xml");

   if (OutputSource != NULL)
   {
	   ret_code = XMLOpen(OutputSource,NULL, 'W');
	   if ((ret_code == 0) && (DownloadedRegionListSource != NULL))
	   {
			OutputSource->root_header 		= DownloadedRegionListSource->root_header;
			OutputSource->root_element 		= DownloadedRegionListSource->root_element;
			OutputSource->root_text 		= DownloadedRegionListSource->root_text;
			OutputSource->root_comment 		= DownloadedRegionListSource->root_comment;
			OutputSource->root_cdata 		= DownloadedRegionListSource->root_cdata;
			OutputSource->root_instruction 	= DownloadedRegionListSource->root_instruction;
			OutputSource->first_entity 		= DownloadedRegionListSource->first_entity;

			ret_code = XMLWrite(OutputSource);
	   }


	   if (ret_code == 0)
	   {
		   ret_code = XMLClose(OutputSource);
	   }
	   else
	   {
		   XMLClose(OutputSource);
	   }

   }

   if (ret_code != 0)
   {
	   msgErr = "Si � verificato un errore in fase di creazione del nuovo file della lista compartimentale! \r\nCodice errore = " + IntToStr(ret_code);
	   MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
   }


   return ret_code;

}
//---------------------------------------------------------------------------

int SaveNewDeviceTypeListXMLFile( void )
{
	int ret_code = 0;
	AnsiString msgErr;

   OutputSource = XMLCreate("device_type_list.xml");

   if (OutputSource != NULL)
   {
	   ret_code = XMLOpen(OutputSource,NULL, 'W');
	   if ((ret_code == 0) && (DownloadedDeviceTypeListSource != NULL))
	   {
			OutputSource->root_header 		= DownloadedDeviceTypeListSource->root_header;
			OutputSource->root_element 		= DownloadedDeviceTypeListSource->root_element;
			OutputSource->root_text 		= DownloadedDeviceTypeListSource->root_text;
			OutputSource->root_comment 		= DownloadedDeviceTypeListSource->root_comment;
			OutputSource->root_cdata 		= DownloadedDeviceTypeListSource->root_cdata;
			OutputSource->root_instruction 	= DownloadedDeviceTypeListSource->root_instruction;
			OutputSource->first_entity 		= DownloadedDeviceTypeListSource->first_entity;

			ret_code = XMLWrite(OutputSource);
	   }

	   if (ret_code == 0)
	   {
		   ret_code = XMLClose(OutputSource);
	   }
	   else
	   {
		   XMLClose(OutputSource);
	   }

   }

   if (ret_code != 0)
   {
	   msgErr = "Si � verificato un errore in fase di creazione del nuovo file della lista compartimentale! \r\nCodice errore = " + IntToStr(ret_code);
	   MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
   }


   return ret_code;

}
//---------------------------------------------------------------------------

int BackupRegionListXMLFile( void )
{
	int ret_code = 0;
	AnsiString msgErr;

	OutputSource = XMLCreate("region_list_backup.xml");

   if (OutputSource != NULL)
   {
	   ret_code = XMLOpen(OutputSource,NULL, 'W');
	   if ((ret_code == 0) && (RegionZoneNodeSource != NULL))
	   {
			OutputSource->root_header = RegionZoneNodeSource->root_header;
			OutputSource->root_element = RegionZoneNodeSource->root_element;
			OutputSource->root_text = RegionZoneNodeSource->root_text;
			OutputSource->root_comment = RegionZoneNodeSource->root_comment;
			OutputSource->root_cdata = RegionZoneNodeSource->root_cdata;
			OutputSource->root_instruction = RegionZoneNodeSource->root_instruction;
			OutputSource->first_entity = RegionZoneNodeSource->first_entity;

			ret_code = XMLWrite(OutputSource);
	   }


	   if (ret_code == 0)
	   {
		   ret_code = XMLClose(OutputSource);
	   }
	   else
	   {
		   XMLClose(OutputSource);
	   }

   }

   if (ret_code != 0)
   {
	   msgErr = "Si � verificato un errore in fase di creazione del file di backup della lista compartimentale! \r\nCodice errore = " + IntToStr(ret_code);
	   MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
   }


   return ret_code;
  /* else
   {
	   MessageBox(NULL, "Operazione effettuata con successo.", "STLC1000 Configurator", MB_OK + MB_OK + MB_TASKMODAL + MB_TOPMOST );
   } */

}
//---------------------------------------------------------------------------

int BackupDeviceTypeListXMLFile( void )
{
	int ret_code = 0;
	AnsiString msgErr;

	OutputSource = XMLCreate("device_type_list_backup.xml");

   if (OutputSource != NULL)
   {
	   ret_code = XMLOpen(OutputSource,NULL, 'W');
	   if ((ret_code == 0) && (DeviceTypeSource != NULL))
	   {
			OutputSource->root_header = DeviceTypeSource->root_header;
			OutputSource->root_element = DeviceTypeSource->root_element;
			OutputSource->root_text = DeviceTypeSource->root_text;
			OutputSource->root_comment = DeviceTypeSource->root_comment;
			OutputSource->root_cdata = DeviceTypeSource->root_cdata;
			OutputSource->root_instruction = DeviceTypeSource->root_instruction;
			OutputSource->first_entity = DeviceTypeSource->first_entity;

			ret_code = XMLWrite(OutputSource);
	   }

	   if (ret_code == 0)
	   {
		   ret_code = XMLClose(OutputSource);
	   }
	   else
	   {
		   XMLClose(OutputSource);
	   }
   }

   if (ret_code != 0)
   {
	   msgErr = "Si � verificato un errore in fase di creazione del file di backup della lista tipi periferiche! \r\nCodice errore = " + IntToStr(ret_code);
	   MessageBox(NULL, msgErr.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
   }

   return ret_code;
}
//---------------------------------------------------------------------------

bool CheckRegionListVersion( void )
{
	XML_ELEMENT * regElement;
	XML_ELEMENT * newRegElement;
	bool bAggiornamento = false;

	if ( (RegionZoneNodeSource != NULL) && (DownloadedRegionListSource != NULL))
	{
		regElement = XMLResolveElement(RegionZoneNodeSource->root_element,"system",NULL);
		newRegElement = XMLResolveElement(DownloadedRegionListSource->root_element,"system",NULL);

		if ((regElement != NULL) && (newRegElement != NULL))
		{
			 if ( ( AnsiString(XMLGetValue(regElement,"version")) == AnsiString(XMLGetValue(newRegElement,"version")) ) &&
				  ( AnsiString(XMLGetValue(regElement,"date")) == AnsiString(XMLGetValue(newRegElement,"date")) )
				)
			 {
				if (MessageBox(NULL, "Il file della lista compartimentale scaricato ha stessa versione e data di quello presente. \r\nProcedere comunque all'aggiornamento ?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) == IDYES)
				{
					bAggiornamento = true;
				}
			 }
			 else
			 {
				 bAggiornamento = true;
			 }
		}
	}
	return bAggiornamento;
}

bool CheckDeviceTypeListVersion( void )
{
	XML_ELEMENT * regElement;
	XML_ELEMENT * newRegElement;
	bool bAggiornamento = false;

	if ( (DeviceTypeSource != NULL) && (DownloadedDeviceTypeListSource != NULL))
	{
		regElement = XMLResolveElement(DeviceTypeSource->root_element,"device_type",NULL);
		newRegElement = XMLResolveElement(DownloadedDeviceTypeListSource->root_element,"device_type",NULL);

		if ((regElement != NULL) && (newRegElement != NULL))
		{
			 if ( ( AnsiString(XMLGetValue(regElement,"version")) == AnsiString(XMLGetValue(newRegElement,"version")) ) &&
				  ( AnsiString(XMLGetValue(regElement,"date")) == AnsiString(XMLGetValue(newRegElement,"date")) )
				)
			 {
				if (MessageBox(NULL, "Il file della lista tipi periferiche scaricato ha stessa versione e data di quello presente. \r\nProcedere comunque all'aggiornamento ?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) == IDYES)
				{
					bAggiornamento = true;
				}
			 }
			 else
			 {
				 bAggiornamento = true;
			 }
		}
	}
	return bAggiornamento;
}

int CheckNewRegionList( void )
{
	AnsiString path;
	int ret_code = 0;

	path = ExtractFilePath(Application->ExeName) + "region_list.tmp";
	DownloadedRegionListSource = XMLCreate(path.c_str());

	if (DownloadedRegionListSource != NULL)
	{
		ret_code = XMLOpen(DownloadedRegionListSource,NULL, 'R');
		if (ret_code == 0)
		{
			ret_code = XMLRead(DownloadedRegionListSource,NULL);
			if (ret_code == 0)
			{
				 ret_code = XMLCheck(DownloadedRegionListSource);
			}
		}
		XMLClose(DownloadedRegionListSource);
	}
	else
	{
		ret_code = -1;
	}

	return ret_code;
}

int CheckNewDeviceTypeList( void )
{
	AnsiString path;
	int ret_code = 0;

	path = ExtractFilePath(Application->ExeName) + "device_type_list.tmp";
	DownloadedDeviceTypeListSource = XMLCreate(path.c_str());

	if (DownloadedDeviceTypeListSource != NULL)
	{
		ret_code = XMLOpen(DownloadedDeviceTypeListSource,NULL, 'R');
		if (ret_code == 0)
		{
			ret_code = XMLRead(DownloadedDeviceTypeListSource,NULL);
			if (ret_code == 0)
			{
				 ret_code = XMLCheck(DownloadedDeviceTypeListSource);
			}
		}
		XMLClose(DownloadedDeviceTypeListSource);
	}
	else
	{
		ret_code = -1;
	}

	return ret_code;
}

void __fastcall TfrmMain :: RegionListDownload()
{
	AnsiString errMsg, request_result, path;
	int ret_code = 0;
	request_result = NULL;

	if ( config_status == CONFIGURATION_MODIFIED)
	{
		if (MessageBox(NULL, "Attenzione: sono state effettuate delle modifiche. \r\nProcedendo, senza aver salvato, la nuova configurazione andr� persa. \r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			return;
		}
	}


	XmlTreeView->Items->Clear();

	frmSaveSplash = new TfrmSaveSplash(Application);
	try
	{
		frmSaveSplash->Caption = "Scaricamento lista compartimentale in corso ...";
		frmSaveSplash->pbSave->Max = 100;
		frmSaveSplash->Show();
		frmSaveSplash->Update();


		try
		{
			request_result = IdHTTP_reg->Get(mRegionListUpdateURL);
		}
		catch (Exception &E)
		{
			frmSaveSplash->Close();
			errMsg = "Errore durante lo scaricamento della lista compartimentale.\r\n" + E.Message ;
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		TStringList * list = new TStringList;
		try
		{
			path = ExtractFilePath(Application->ExeName) + "region_list.tmp";
			list->Add(request_result);
			list->SaveToFile("region_list.tmp");
		}
		catch (Exception &E)
		{
			frmSaveSplash->Close();
			errMsg = "Errore durante il salvataggo della nuova lista compartimentale.\r\n" + E.Message ;
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			delete list;
			return;
		}
		delete list;

		try
		{
			ret_code = CheckNewRegionList();
			if (ret_code == 0)
			{
				if (!CheckRegionListVersion())
				{
					frmSaveSplash->Close();
					return;
				}

				ret_code = BackupRegionListXMLFile();

				if (ret_code == 0)
				{
					ret_code = SaveNewRegionListXMLFile();
				}
			}
			else
			{
				frmSaveSplash->Close();
				errMsg = "La lista compartimentale scaricata non � valida! Aggiornamento Fallito \r\nCodice errore = " + IntToStr(ret_code);
				MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
				return;
			}

			if (ret_code == 0)
			{
				MessageBox(NULL, "Operazione di aggiornamento della lista compartimentale effettuata con successo.", "STLC1000 Configurator", MB_OK + MB_OK + MB_TASKMODAL + MB_TOPMOST );

				// ricarico la nuova Region List
				ret_code = LoadRegionZoneNodeFromXML();
				if (ret_code != 0)
				{
					MessageBox(NULL, "Operazione di caricamento della nuova lista compartimentale fallita.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
				}
			}
			else
			{
				MessageBox(NULL, "Operazione di aggiornamento della lista compartimentale fallita.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			}
		}
		catch (Exception &E)
		{
			frmSaveSplash->Close();
			errMsg = "Errore durante operazione di aggiornamento della lista compartimentale. Aggiornamento Fallito.\r\n" + E.Message ;
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		frmSaveSplash->Close();
	}
	__finally
	{
		frmSaveSplash->Free();
	}

}

void __fastcall TfrmMain :: DeviceTypeListDownload()
{
	AnsiString errMsg, request_result, path;
	int ret_code = 0;
	request_result = NULL;

	if ( config_status == CONFIGURATION_MODIFIED)
	{
		if (MessageBox(NULL, "Attenzione: sono state effettuate delle modifiche. \r\nProcedendo, senza aver salvato, la nuova configurazione andr� persa. \r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			return;
		}
	}

	XmlTreeView->Items->Clear();

	frmSaveSplash = new TfrmSaveSplash(Application);
	try
	{
		frmSaveSplash->Caption = "Scaricamento lista tipi periferiche in corso ...";
		frmSaveSplash->pbSave->Max = 100;
		frmSaveSplash->Show();
		frmSaveSplash->Update();


		try
		{
			request_result = IdHTTP_reg->Get(mDeviceTypeListUpdateURL);
		}
		catch (Exception &E)
		{
			frmSaveSplash->Close();
			errMsg = "Errore durante lo scaricamento della lista tipi periferiche.\r\n" + E.Message ;
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		TStringList * list = new TStringList;
		try
		{
			path = ExtractFilePath(Application->ExeName) + "device_type_list.tmp";
			list->Add(request_result);
			list->SaveToFile("device_type_list.tmp");
		}
		catch (Exception &E)
		{
			frmSaveSplash->Close();
			errMsg = "Errore durante il salvataggo della nuova lista tipi periferiche.\r\n" + E.Message ;
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			delete list;
			return;
		}
		delete list;

		try
		{
			ret_code = CheckNewDeviceTypeList();
			if (ret_code == 0)
			{
				if (!CheckDeviceTypeListVersion())
				{
					frmSaveSplash->Close();
					return;
				}

				ret_code = BackupDeviceTypeListXMLFile();

				if (ret_code == 0)
				{
					ret_code = SaveNewDeviceTypeListXMLFile();
				}
			}
			else
			{
				frmSaveSplash->Close();
				errMsg = "La lista tipi periferiche scaricata non � valida! Aggiornamento Fallito \r\nCodice errore = " + IntToStr(ret_code);
				MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
				return;
			}

			if (ret_code == 0)
			{
				MessageBox(NULL, "Operazione di aggiornamento della lista tipi periferiche effettuata con successo.", "STLC1000 Configurator", MB_OK + MB_OK + MB_TASKMODAL + MB_TOPMOST );

				// ricarico la nuova Device Type List
				ret_code = LoadTypeDeviceComboFromXML();
				if (ret_code != 0)
				{
					MessageBox(NULL, "Operazione di caricamento della nuova lista tipi periferiche fallita.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
				}
			}
			else
			{
				MessageBox(NULL, "Operazione di aggiornamento della lista tipi periferiche fallita.", "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			}
		}
		catch (Exception &E)
		{
			frmSaveSplash->Close();
			errMsg = "Errore durante operazione di aggiornamento della lista tipi periferiche. Aggiornamento Fallito.\r\n" + E.Message ;
			MessageBox(NULL, errMsg.c_str(), "STLC1000 Configurator", MB_OK + MB_ICONERROR + MB_TASKMODAL + MB_TOPMOST );
			return;
		}

		frmSaveSplash->Close();
	}
	__finally
	{
		frmSaveSplash->Free();
	}

}

void __fastcall TfrmMain::miRegionListDownloadClick(TObject *Sender)
{
	RegionListDownload();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	CanClose = true;
	if ( config_status == CONFIGURATION_MODIFIED )
	{
		if (MessageBox(NULL, "Attenzione: sono state effettuate delle modifiche. \r\nUscendo senza salvare la nuova configurazione andr� persa.\r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			CanClose = false;
		}
	}
	else
	if ( config_status == CONFIGURATION_SAVED )
	{
		if (MessageBox(NULL, "Attenzione: sono state salvate le modifiche senza riavviare i servizi.\r\nUscendo le nuove modifiche non avranno effetto.\r\nProcedere comunque?", "STLC1000 Configurator", MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2 + MB_TASKMODAL + MB_TOPMOST ) != IDYES)
		{
			CanClose = false;
		}
	}
}
//---------------------------------------------------------------------------


int SaveNewRegionListUpdateURL( void )
{
	void *	key      = 	NULL;
	int		ret_code = 	0;
	char *	cRegionListUpdateURLValue = NULL;

	try
	{
		// Apro la chiave in scrittura
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, CFG_KEY, true );

		if (key == NULL)  // la chiave non esiste
		{
			// creo la chiave
			ret_code = SYSCreateRegKey( HKEY_LOCAL_MACHINE, CFG_KEY );
			if (ret_code == 0)
			{
				key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, CFG_KEY, true );
			}
		}

		if ((ret_code == 0) && ( key != NULL ))
		{
			cRegionListUpdateURLValue = mRegionListUpdateURL.c_str();
			ret_code = SYSSetRegValue( key, CFG_REGION_LIST_URL_VALUE, (void*)cRegionListUpdateURLValue, REG_SZ, sizeof(char)*(strlen(cRegionListUpdateURLValue)+1)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = -20;
		}

	}
	catch(...)
	{
		ret_code = -10;
	}

	return ret_code;
}

int SaveNewDeviceTypeListUpdateURL( void )
{
	void *	key      = 	NULL;
	int		ret_code = 	0;
	char *	cDeviceTypeListUpdateURLValue = NULL;

	try
	{
		// Apro la chiave in scrittura
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, CFG_KEY, true );

		if (key == NULL)  // la chiave non esiste
		{
			// creo la chiave
			ret_code = SYSCreateRegKey( HKEY_LOCAL_MACHINE, CFG_KEY );
			if (ret_code == 0)
			{
				key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, CFG_KEY, true );
			}
		}

		if ((ret_code == 0) && ( key != NULL ))
		{
			cDeviceTypeListUpdateURLValue = mDeviceTypeListUpdateURL.c_str();
			ret_code = SYSSetRegValue( key, CFG_DEVICE_TYPE_LIST_URL_VALUE, (void*)cDeviceTypeListUpdateURLValue, REG_SZ, sizeof(char)*(strlen(cDeviceTypeListUpdateURLValue)+1)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = -20;
		}

	}
	catch(...)
	{
		ret_code = -10;
	}

	return ret_code;
}

void __fastcall TfrmMain::OpzioniClick(TObject *Sender)
{
	AnsiString tempRegionURL = "";
    AnsiString tempDeviceURL = "";
	frmOptions = new TfrmOptions(Application);
	try
	{
		tempRegionURL = mRegionListUpdateURL;
        tempDeviceURL = mDeviceTypeListUpdateURL;
		frmOptions->s_RL_URL =  mRegionListUpdateURL;
        frmOptions->s_DTL_URL = mDeviceTypeListUpdateURL;

		frmOptions->ShowModal();

		mRegionListUpdateURL = frmOptions->s_RL_URL;
        mDeviceTypeListUpdateURL = frmOptions->s_DTL_URL;
		if (mRegionListUpdateURL != tempRegionURL)
		{
			SaveNewRegionListUpdateURL();
		}
        if (mDeviceTypeListUpdateURL != tempDeviceURL)
        {
            SaveNewDeviceTypeListUpdateURL();
        }
	}
	__finally
	{
		frmGridLocPosition->Free();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eSNMPCommunityChange(TObject *Sender)
{
	checkTextEditIsValid(eSNMPCommunity);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::eSNMPCommunityKeyPress(TObject *Sender, char &Key)
{
	CheckKey(Key);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::IdHTTP_regWork(TObject *Sender, TWorkMode AWorkMode,
	  const int AWorkCount)
{
	frmSaveSplash->pbSave->StepBy(AWorkCount);
	Application->ProcessMessages();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::IdHTTP_regWorkBegin(TObject *Sender,
	  TWorkMode AWorkMode, const int AWorkCountMax)
{
	frmSaveSplash->pbSave->Max = AWorkCountMax;
}
//---------------------------------------------------------------------------

bool __fastcall TfrmMain :: ExistsLocations()
{
	XML_ELEMENT * locationElement;
	bool retValue = true;

	if (SystemSource != NULL)
	{
		 locationElement = XMLGetFirst(SystemSource,"location");
		 if (locationElement == NULL)
		 {
			retValue = false;
		 }
	}
	return retValue;
}


void __fastcall TfrmMain::miDeviceTypeListDownloadClick(TObject *Sender)
{
	DeviceTypeListDownload();	
}
//---------------------------------------------------------------------------


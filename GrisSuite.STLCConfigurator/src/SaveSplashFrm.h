//---------------------------------------------------------------------------

#ifndef SaveSplashFrmH
#define SaveSplashFrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmSaveSplash : public TForm
{
__published:	// IDE-managed Components
    TProgressBar *pbSave;
	TLabel *lCurrentTask;
private:	// User declarations
public:		// User declarations
    __fastcall TfrmSaveSplash(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmSaveSplash *frmSaveSplash;
//---------------------------------------------------------------------------
#endif

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OptionsFrm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmOptions *frmOptions;
//---------------------------------------------------------------------------
__fastcall TfrmOptions::TfrmOptions(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmOptions::btnCancelClick(TObject *Sender)
{
	this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TfrmOptions::btnConfirmClick(TObject *Sender)
{
	s_RL_URL = this->eRegionListURL->Text;
    s_DTL_URL = this->eDeviceTypeListURL->Text;
	this->Close();
}
//---------------------------------------------------------------------------
void __fastcall TfrmOptions::FormShow(TObject *Sender)
{
	this->eRegionListURL->Text = s_RL_URL;
    this->eDeviceTypeListURL->Text = s_DTL_URL;
}
//---------------------------------------------------------------------------


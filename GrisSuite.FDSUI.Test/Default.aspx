﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GrisiSuite.FDS.UI.Test.Default" %>
<%@ Register Assembly="System.Web.Silverlight" Namespace="System.Web.UI.SilverlightControls"
    TagPrefix="asp" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body style="margin-left:auto; margin-right:auto;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>FDS Test Page</div>
        <div style="vertical-align:top;">
            <asp:DropDownList ID="DropDownList1" runat="server" 
                DataSourceID="SqlDataSource1" DataTextField="Description" 
                DataValueField="DevID" Height="28px" Width="360px" 
                ondatabound="DropDownList1_DataBound">
            </asp:DropDownList>
            
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TelefinConnectionString %>" 
                SelectCommand="SELECT devices.DevID, nodes.Name + ' - ' + devices.Name AS Description FROM devices INNER JOIN nodes ON devices.NodID = nodes.NodID WHERE (devices.Type LIKE 'FD%')">
            </asp:SqlDataSource>
            
            <asp:Button ID="btnLoad" Text="Load" runat="server" onclick="btnLoad_Click" style="vertical-align:top;" />
            
        </div>
        <div style="border:solid 2px;">
            <asp:Silverlight ID="Xaml1" runat="server" Source="~/ClientBin/GrisSuite.FDS.UI.xap" MinimumVersion="3.0.40624.0" Width="100%" Height="508px"/>
        </div>    
    </form>
</body>
</html>

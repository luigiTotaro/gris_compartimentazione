﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrisiSuite.FDS.UI.Test
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Xaml1.InitParameters = String.Format("DevID={0},FdsServiceUrl={1},HtmlControlHostId={2}", 0, Properties.Settings.Default.FdsServiceUrl, this.Xaml1.ClientID); 
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            this.Xaml1.InitParameters = String.Format("DevID={0},FdsServiceUrl={1},HtmlControlHostId={2}", DropDownList1.SelectedValue, Properties.Settings.Default.FdsServiceUrl, this.Xaml1.ClientID); 
        }

        protected void DropDownList1_DataBound(object sender, EventArgs e)
        {
            DropDownList1.Items.Insert(0, new ListItem("Dati Simulati", "0"));
        }
    }
}

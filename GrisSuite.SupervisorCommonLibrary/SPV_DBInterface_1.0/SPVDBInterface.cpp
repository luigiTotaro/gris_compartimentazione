//==============================================================================
// Telefin Supervisor DataBase Interface Module 1.0
//------------------------------------------------------------------------------
// Modulo di interafaccia verso i database (SPVDBInterface.cpp)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:   2.08 (02.09.2004 -> 12.02.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      richiede SPVDBInterface.h
//------------------------------------------------------------------------------
// Version history: vedere in SPVDBInterface.h
//==============================================================================
#pragma hdrstop
#include "SPVDBInterface.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
// --- Inclusione di SQLAPI++ ver. 3.7.16 ---
#include <SQLAPI.h>
#include "UtilityLibrary.h"

//------------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
/// Funzione per inizializzare il modulo
///
/// \date [02.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
void SPVDBInit( void )
{

}

//==============================================================================
/// Funzione per ottenere il codice di un tipo database in base alla stringa
///
/// \date [12.10.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBGetTypeCode( char * type_str )
{
  int type_code = SPV_DBI_CONN_TYPE_UNKNOWN;

  if ( strcmpi( type_str, "MySQL" ) == 0 ) type_code = SPV_DBI_CONN_TYPE_mySQL;
  if ( strcmpi( type_str, "ODBC"  ) == 0 ) type_code = SPV_DBI_CONN_TYPE_ODBC;
  if ( strcmpi( type_str, "MSSQL" ) == 0 ) type_code = SPV_DBI_CONN_TYPE_MS_SQL_SERVER;

  return type_code;
}

//==============================================================================
/// Funzione per ottenere il nome di un tipo database in base al codice
///
/// \date [15.02.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SPVDBGetTypeName( int type )
{
  char * type_name = NULL;

  switch ( type )
  {
    case SPV_DBI_CONN_TYPE_mySQL:
      type_name = SPV_DBI_CONN_TYPE_NAME_MYSQL;
    break;
    case SPV_DBI_CONN_TYPE_ODBC:
      type_name = SPV_DBI_CONN_TYPE_NAME_ODBC;
    break;
    case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
      type_name = SPV_DBI_CONN_TYPE_NAME_MSSQL;
    break;
    default:
      type_name = SPV_DBI_CONN_TYPE_NAME_UNKNOWN;
  }

  return type_name;
}



//==============================================================================
/// Funzione per creare una nuova struttura campo
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DBI_FIELD * SPVDBNewField( int ID, int type, char * data, int len )
{
  SPV_DBI_FIELD * field = NULL; // Ptr alla nuova struttura campo

  field = (SPV_DBI_FIELD *)malloc(sizeof(SPV_DBI_FIELD));

  if (field != NULL)
  {
    field->ID       = ID;
    field->Type     = type;
	field->Data     = ULStrCpy(NULL,data);
    field->DataLen  = len;
  }

  return field;
}

//==============================================================================
/// Funzione per eliminare una struttura campo
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBDeleteField( SPV_DBI_FIELD * field )
{
  int ret_code = 0; // Codice di ritorno

  if ( field != NULL )
  {
    if ( field->Data != NULL ) free( field->Data );
    free( field );
  }
  else
  {
    ret_code = SPV_DBI_INVALID_FIELD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una lista di campi
///
/// \date [12.05.2008]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SPV_DBI_FIELD * SPVDBNewFields( int num )
{
	SPV_DBI_FIELD * fields = NULL; // Ptr ad una nuova lista di campi

	try
	{
	    if ( num > 0 )
		{
	    	try
	    	{
	    		fields = (SPV_DBI_FIELD *)malloc(sizeof(SPV_DBI_FIELD)*num);
	    	}
	    	catch(...)
	    	{
	    		fields = NULL;
	    	}

	    	if ( fields != NULL )
	    	{
	    		for ( int i=0; i<num; i++ )
	    		{
	    			fields[i].ID      = i;
	    			fields[i].Type    = 0;
	    			fields[i].Data    = NULL;
	    			fields[i].DataLen = 0;
	    		}
	    	}
		}
	}
	catch(...)
	{
        fields = NULL;
    }

	return fields;
}

//==============================================================================
/// Funzione per eliminare una lista di campi
///
/// \date [09.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBDeleteFields( SPV_DBI_FIELD * fields, int num )
{
  int ret_code = 0; // Codice di ritorno

  if ( fields != NULL )
  {
    if ( num > 0 )
    {
      for ( int i=0; i< num; i++ )
      {
        if ( fields[i].Data != NULL )
        try
        {
          free( fields[i].Data );
        }
        catch(...)
        {
        }
      }
      try
      {
        free( fields );
      }
      catch(...)
      {
      }
    }
    else
    {
      ret_code = SPV_DBI_INVALID_FIELD_NUM;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_FIELD_LIST;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova struttura riga
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DBI_ROW * SPVDBNewRow( int ID, int field_num, SPV_DBI_FIELD * fields )
{
  SPV_DBI_ROW * row = NULL; // Ptr alla nuova struttura riga

  row = (SPV_DBI_ROW *)malloc(sizeof(SPV_DBI_ROW));

  if ( row != NULL )
  {
    row->ID         = ID;
    row->FieldNum   = field_num;
    row->Fields     = fields;
  }

  return row;
}

//==============================================================================
/// Funzione per eliminare una struttura riga
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBDeleteRow( SPV_DBI_ROW * row )
{
  int ret_code = 0; // Codice di ritorno

  if ( row != NULL )
  {
    SPVDBDeleteFields( row->Fields, row->FieldNum );
    free( row );
  }
  else
  {
    ret_code = SPV_DBI_INVALID_ROW;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova lista di righe
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DBI_ROW * SPVDBNewRows( int num, int field_num )
{
  SPV_DBI_ROW * rows = NULL; // Ptr alla nuova lista di righe

  if ( num > 0 )
  {
    rows = (SPV_DBI_ROW *)malloc(sizeof(SPV_DBI_ROW)*num);

    if ( rows != NULL ) // Se l'allocazione � andata a buon fine
    {
      for ( int r=0; r<num; r++ )
      {
        rows[r].ID        = 0;
        if ( field_num > 0 )
        {
          rows[r].Fields    = SPVDBNewFields(field_num);
          rows[r].FieldNum  = field_num;
        }
        else
        {
          rows[r].Fields    = NULL;
          rows[r].FieldNum  = 0;
        }
      }
    }
  }

  return rows;
}

//==============================================================================
/// Funzione per eliminare una lista di righe
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBDeleteRows( SPV_DBI_ROW * rows, int num )
{
  int ret_code = 0; // Codice di ritorno

  if ( rows != NULL ) // Controllo il ptr alla lista
  {
    if ( num > 0 )
    {
      for ( int r=0; r<num; r++ )
      {
        SPVDBDeleteFields( rows[r].Fields, rows[r].FieldNum );
      }
      free( rows );
    }
    else
    {
      ret_code = SPV_DBI_INVALID_ROW_NUM;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_ROW_LIST;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova struttura risultato query
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DBI_RES * SPVDBNewRes( int ID, int row_num, SPV_DBI_ROW * rows )
{
  SPV_DBI_RES * res = NULL; // Ptr alla nuova struttura risultato

  res = (SPV_DBI_RES *)malloc(sizeof(SPV_DBI_RES));

  if ( res != NULL )
  {
    res->ID     = ID;
    res->RowNum = row_num;
    res->Rows   = rows;
  }

  return res;
}

//==============================================================================
/// Funzione per creare una nuova struttura risultato query con tutte le liste
/// sottostanti
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
SPV_DBI_RES * SPVDBNewRes( int ID, int row_num, int field_num )
{
  SPV_DBI_RES * res = NULL; // Ptr alla nuova struttura risultato

  res = (SPV_DBI_RES *)malloc(sizeof(SPV_DBI_RES));

  if ( res != NULL )
  {
    res->ID     = ID;
    res->RowNum = 0;
    res->Rows   = NULL;
    if (row_num > 0)
    {
      res->Rows = SPVDBNewRows( row_num, field_num );
      if ( res->Rows != NULL )
      {
        res->RowNum = row_num;
      }
    }
  }

  return res;
}

//==============================================================================
/// Funzione per eliminare una struttura risultato query
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBDeleteRes( SPV_DBI_RES * res )
{
  int ret_code = 0; // Codice di ritorno

  if ( res != NULL )
  {
    SPVDBDeleteRows(res->Rows,res->RowNum);
    free ( res );
  }
  else
  {
    ret_code = SPV_DBI_INVALID_RESULT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova connessione a database
///
/// \date [09.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.07
//------------------------------------------------------------------------------
SPV_DBI_CONN * SPVDBNewConn( int ID, char * name, int type, char * host, char * user, char * pass, char * database )
{
  SPV_DBI_CONN  * dbconn  = NULL; // Ptr alla nuova struttura connessione db
  //MYSQL         * mySQL   = NULL; // Ptr ad un eventuale handler mySQL
  SAConnection  * SQLSrv  = NULL; // Ptr ad un eventuale handler MS SQL Server

  dbconn = (SPV_DBI_CONN *)malloc( sizeof(SPV_DBI_CONN) ); // Allocazione

  if ( dbconn != NULL ) // Se l'allocazione � andata a buon fine
  {
	memset( (void*)dbconn, 0, sizeof(SPV_DBI_CONN) );
	// --- Copio i valori passati come parametro alla funzione ---
	dbconn->ID        = ID  ;
	dbconn->Type      = type;
	dbconn->Name      = ULStrCpy( dbconn->Name     , name     );
	dbconn->Host      = ULStrCpy( dbconn->Host     , host     );
	dbconn->UserName  = ULStrCpy( dbconn->UserName , user     );
	dbconn->Password  = ULStrCpy( dbconn->Password , pass     );
	dbconn->Database  = ULStrCpy( dbconn->Database , database );
    // --- Inserisco i valori di default ---
    dbconn->Params        = NULL  ;
    dbconn->Active        = false ;
    dbconn->Status        = NULL  ;
    dbconn->LastErrorCode = 0     ;
    dbconn->LastError     = NULL  ;
	dbconn->LastResult    = NULL  ;

	//InitializeCriticalSection( &dbconn->CriticalSection );
	char * db_conn_name = NULL;
	db_conn_name = ULAddText(db_conn_name,dbconn->Name);
	db_conn_name = ULAddText(db_conn_name," - dbconn->Lock");
	SYSInitLock(&dbconn->Lock,db_conn_name);// Inizializzo il lock della connessione a DB
	try
	{
		if ( db_conn_name != NULL ) free( db_conn_name );
	}
	catch(...)
	{
		// Eccezione non loggata
	}
    dbconn->Persistent    = false ;
    dbconn->Def           = NULL  ;
    switch ( dbconn->Type ) // Switcho sui tipi di connessione database
    {
      // --- Caso mySQL ---
      /*
      case SPV_DBI_CONN_TYPE_mySQL:
        mySQL = mysql_init(NULL);
        dbconn->Handler = (void*) mySQL;
      break;
      //*/
      // --- Caso Microsoft SQL Server / MSDE ---
      case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
		SQLSrv = new SAConnection;
        SQLSrv->setClient( SA_SQLServer_Client );
        dbconn->Handler = (void*) SQLSrv;
      break;                                               
      // --- Caso ODBC ---
      case SPV_DBI_CONN_TYPE_ODBC:
        dbconn->Handler = NULL;
      break;          
      // --- Casi non supportati ---
      default:
        dbconn->Handler = NULL;
    }
  }

  return dbconn;
}

//==============================================================================
/// Funzione per eliminare una connessione a database
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.10
//------------------------------------------------------------------------------
int SPVDBDeleteConn( SPV_DBI_CONN * dbconn )
{
  int             ret_code  = 0   ; // Codice di ritorno
  //MYSQL         * mySQL     = NULL; // Ptr ad un eventuale handler mySQL
  SAConnection  * SQLSrv    = NULL; // Ptr ad un eventuale handler MS SQL Server

  if ( dbconn != NULL ) // Controllo il ptr della connessione
  {
	//EnterCriticalSection( &dbconn->CriticalSection ); // Entro nella sezione critica
	SYSLock(&dbconn->Lock,"SPVDBDeleteConn");
	try
	{
	  switch ( dbconn->Type ) // Switcho sui tipi di connessione database
	  {
		// --- Caso Microsoft SQL Server / MSDE ---
		case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
		  SQLSrv = (SAConnection*)dbconn->Handler;
		  try
		  {
			//* DA VERIFICARE
			delete SQLSrv;
			//*/
		  }
		  catch( SAException &ex )
		  {
			dbconn->LastErrorCode = ex.ErrNativeCode();
			dbconn->LastError     = ULStrCpy( dbconn->LastError, ex.ErrText() );
			ret_code = SPV_DBI_DELETE_FAILURE;
		  }
		  catch(...)
		  {
			ret_code = SPV_DBI_DELETE_FAILURE;
		  }
		  //*/
		break;
		// --- Caso MySQL ---
		case SPV_DBI_CONN_TYPE_mySQL:
		  //mySQL = (MYSQL*)dbconn->Handler;
		break;
		// --- Casi non supportati ---
		default:
		  ret_code = SPV_DBI_UNKNOWN_CONNECTION_TYPE;
	  }
	  if ( dbconn->Name        != NULL ) free( dbconn->Name      );
	  if ( dbconn->Host        != NULL ) free( dbconn->Host      );
	  if ( dbconn->UserName    != NULL ) free( dbconn->UserName  );
	  if ( dbconn->Password    != NULL ) free( dbconn->Password  );
	  if ( dbconn->Database    != NULL ) free( dbconn->Database  );
	  if ( dbconn->LastError   != NULL ) free( dbconn->LastError );
	  if ( dbconn->LastResult  != NULL ) SPVDBDeleteRes( dbconn->LastResult );
	}
	catch(...)
	{
	  ret_code = SPV_DBI_CRITICAL_ERROR;
	}
	//LeaveCriticalSection( &dbconn->CriticalSection ); // Esco dalla sezione critica
	SYSUnLock(&dbconn->Lock);
	//DeleteCriticalSection( &dbconn->CriticalSection );
	SYSClearLock(&dbconn->Lock);

	try
	{
	  free( dbconn );
	}
	catch(...)
	{
	  ret_code = SPV_DBI_CRITICAL_ERROR;
	}
  }
  else
  {
	ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per connettersi ad un database server
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.12
//------------------------------------------------------------------------------
int SPVDBOpenConn( SPV_DBI_CONN * dbconn )
{
  int             ret_code  = SPV_DBI_NO_ERROR; // Codice di ritorno
  //MYSQL         * mySQL     = NULL; // Ptr ad un eventuale handler mySQL
  SAConnection  * SQLSrv    = NULL; // Ptr ad un eventuale handler MS SQL Server
  SAString        DB_str    = ""  ;
  int			  isNotConnected = true;
  int			  count			 = 0;

  if ( dbconn != NULL ) // Controllo il ptr della connessione
  {
	//EnterCriticalSection( &dbconn->CriticalSection ); // Entro nella sezione critica
	//SYSLock(&dbconn->Lock,"SPVDBOpenConn");
	try
	{
	  switch ( dbconn->Type ) // Switcho sui tipi di connessione database
	  {
		// --- Caso Microsoft SQL Server / MSDE ---
		case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
		  SQLSrv = (SAConnection*)dbconn->Handler;
		  DB_str += SAString( dbconn->Host );
		  DB_str += "@";
		  DB_str += SAString( dbconn->Database );
		  // --- Autenticazione integrata di Windows ---
		  if ( strcmpi( dbconn->UserName, "WindowsAuthentication" ) == 0 )
		  {
			try
			{
			  SQLSrv->Connect( DB_str, "", "", SA_SQLServer_Client );
			}
			catch( SAException &ex )
			{
			  dbconn->LastErrorCode = ex.ErrNativeCode();
			  dbconn->LastError     = ULStrCpy( dbconn->LastError, ex.ErrText() );
			  ret_code = SPV_DBI_CONNECT_FAILURE;
			}
		  }
		  // --- Autenticazione SQL ---
		  else
		  {
			try
			{
			  SQLSrv->Connect( DB_str, dbconn->UserName, dbconn->Password, SA_SQLServer_Client );
			}
			catch( SAException &ex )
			{
			  dbconn->LastErrorCode = ex.ErrNativeCode();
			  dbconn->LastError     = ULStrCpy( dbconn->LastError, ex.ErrText() );
			  ret_code = SPV_DBI_CONNECT_FAILURE;
			}
		  }

		  while ((isNotConnected) && (count < 100))
		  {
			try
			{
			  isNotConnected = !SQLSrv->isConnected();
			}
			catch( ... )
			{
			  isNotConnected = true;
			}
			count++;
			Sleep(10);
		  }

		  //if ( SQLSrv->isConnected() )
		  if(!isNotConnected)
		  {
			if ( ret_code != SPV_DBI_CONNECT_FAILURE )
			{
			  dbconn->Active = true;
			}
			else
			{
			  SQLSrv->Disconnect();
			  dbconn->Active = false;
			}
		  }
		  else
		  {
			dbconn->Active = false;
			ret_code = SPV_DBI_CONNECT_FAILURE;
		  }
		break;
		/*
		case SPV_DBI_CONN_TYPE_mySQL:
		  mySQL = (MYSQL*)dbconn->Handler;
		  if ( mysql_real_connect( mySQL, dbconn->Host, dbconn->UserName, dbconn->Password, dbconn->Database, 0, NULL, 0 ) != NULL )
		  {
			dbconn->Handler = (void*) mySQL;
			dbconn->Active  = true;
		  }
		  else
		  {
			dbconn->Active        = false;
			dbconn->LastErrorCode = mysql_errno(mySQL);
			dbconn->LastError     = SPVStrCpy( dbconn->LastError, mysql_error( mySQL ) );
			if ( dbconn->LastErrorCode == 1049 )
			{
			  if ( mysql_real_connect( mySQL, dbconn->Host, dbconn->UserName, dbconn->Password, NULL, 0, NULL, 0 ) != NULL )
			  {
				dbconn->Handler = (void*) mySQL;
				dbconn->Active  = true;
				ret_code = SPV_DBI_WARNING_DATABASE_NOT_FOUND;
			  }
			  else
			  {
				dbconn->LastErrorCode = mysql_errno(mySQL);
				dbconn->LastError     = SPVStrCpy( dbconn->LastError, mysql_error( mySQL ) );
				ret_code = SPV_DBI_CONNECT_FAILURE;
			  }
			}
			else
			{
			  ret_code = SPV_DBI_CONNECT_FAILURE;
			}
		  }
		break;
		//*/
		default: /* Casi non supportati */
		  dbconn->Active  = false;
		  ret_code = SPV_DBI_UNKNOWN_CONNECTION_TYPE;
	  }
	}
	catch(...)
	{
	  ret_code = SPV_DBI_CRITICAL_ERROR;
	}
	//LeaveCriticalSection( &dbconn->CriticalSection ); // Esco dalla sezione critica
	///SYSUnLock(&dbconn->Lock);
  }
  else
  {
	ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per disconnettersi da un database server
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
int SPVDBCloseConn( SPV_DBI_CONN * dbconn )
{
	int             ret_code  	= 0   ; // Codice di ritorno
	SAConnection  * SQLSrv    	= NULL; // Ptr ad un eventuale handler MS SQL Server
	bool 			isConnected = true;
	int				count		= 0;

	if ( dbconn != NULL ) // Controllo il ptr della connessione
	{
		//EnterCriticalSection(&dbconn->CriticalSection); // Entro nella sezione critica
		///SYSLock(&dbconn->Lock,"SPVDBCloseConn");
		switch ( dbconn->Type ) // Switcho sui tipi di connessione database
		{
			// --- Caso Microsoft SQL Server / MSDE ---
			case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
				SQLSrv = (SAConnection*)dbconn->Handler;
				try
				{
					SQLSrv->Disconnect();
				}
				catch( SAException &ex )
				{
					dbconn->LastErrorCode = ex.ErrNativeCode();
					dbconn->LastError     = ULStrCpy( dbconn->LastError, ex.ErrText() );
					ret_code = SPV_DBI_DISCONNECT_FAILURE;
				}
				catch(...)
				{
					ret_code = SPV_DBI_FUNCTION_EXCEPTION;
				}

				while ((isConnected) && (count < 100))
				{
					try
					{
						isConnected = SQLSrv->isConnected();
					}
					catch( ... )
					{
						isConnected = true;
					}
					count++;
					Sleep(10);
				}

				if (isConnected)
				{
					ret_code = SPV_DBI_DISCONNECT_FAILURE;
				}

				dbconn->Active = isConnected;
			break;
			default: /* Casi non supportati */
				ret_code = SPV_DBI_UNKNOWN_CONNECTION_TYPE;
		}
		//LeaveCriticalSection(&dbconn->CriticalSection); // Esco dalla sezione critica
		///SYSUnLock(&dbconn->Lock);
	}
	else
	{
		ret_code = SPV_DBI_INVALID_CONNECTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eseguire una query su un database
///
/// La lunghezza della query (che pu� contenere dati binari) viene specificata
/// dal parametro <len>. Se <len> � posto a 0 la lunghezza viene caloclata
/// supponendo che la query contenga solo caratteri ASCII (e nessuno '\0').
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.17
//------------------------------------------------------------------------------
int SPVDBQuery( SPV_DBI_CONN * dbconn, char * query, long len, void * param, long param_size )
{
  int             ret_code    = SPV_DBI_NO_ERROR; // Codice di ritorno
  /*
  MYSQL         * mySQL       = NULL; // Ptr ad un eventuale handler mySQL
  MYSQL_RES     * myRES       = NULL; // Ptr ad un descrittore di risultato mySQL
  MYSQL_ROW       myROW             ; // Descrittore di righa mySQL
  MYSQL_FIELD   * myFIELD     = NULL; // Descrittore di un campo mySQL
  //*/
  int             row_num     = 0   ; // Numero di righe
  int             field_num   = 0   ; // Numero di campi per riga
  SPV_DBI_RES   * res         = NULL; // Ptr ad una struttura risultato
  long            query_len   = NULL; // Lunghezza della query
  unsigned long * field_len   = NULL; // Lunghezze dei campi
  char          * data        = NULL; // Ptr a dato del campo
  SAConnection  * SQLSrv      = NULL; // Ptr ad un eventuale handler MS SQL Server
  SACommand       SQLCmd            ; // Oggeto comando SQL
  SPV_DBI_ROW   * temp_rows   = NULL; // Ptr a lista temporanea di righe
  unsigned long   field_size  = 0   ; // Dimensione di un singolo campo
  SAString        blob_str    = ""  ; // Stringa del BLOB
  int             retries     = 0   ;

  #define _LOCAL_DB_QUERY_RETRIES 20

  if ( dbconn != NULL ) // Controllo il ptr della connessione
  {
	//EnterCriticalSection(&dbconn->CriticalSection); // Entro nella sezione critica
	///SYSLock(&dbconn->Lock,"SPVDBQuery");
    if ( len > 0 ) // Se ho specificato la lunghezza della query (pu� contenere anche dati binari)
    {
      query_len = (long)(len <= SPV_DBI_QUERY_MAX_LENGTH)?len:SPV_DBI_QUERY_MAX_LENGTH;
    }
    else // Se la lunghezza non � specificata viene calcolata (solo per query in formato ASCII)
    {
      query_len = strlen(query);
    }
	switch ( dbconn->Type ) // Switcho sui tipi di connessione database
    {
      // --- Caso Microsoft SQL Server / MSDE ---
	  case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
        SQLSrv = (SAConnection*)dbconn->Handler;
        if ( query != NULL )
		{
          try
          {
			// --- Imposto la connessione al DB ---
            SQLCmd.setConnection( SQLSrv );
            // --- Imposto la query ---
            SQLCmd.setCommandText( SAString( query, query_len ) );
            // --- Faccio il binding di un eventuale parametro blob ---
            if ( param != NULL )
            {
              // --- Recupero il blob ---
              blob_str += SAString( (char*)param, param_size );
              // --- Aggiungo il paramtro blob ---
              SQLCmd.Param("blob").setAsBLob() = blob_str;
            }
            // --- Eseguo la query ---
			SQLCmd.Execute();
			Sleep( 50 );
			//Sleep( 250 + ( query_len / 10 ) );
          }
          catch( SAException &ex )
          {
			// --- Gestione dell'errore ---
			dbconn->LastErrorCode = ex.ErrNativeCode();
			dbconn->LastError     = ULStrCpy( dbconn->LastError, ex.ErrText() );
			ret_code = SPV_DBI_QUERY_FAILURE;
		  }
		  catch(...)
		  {
			// --- Gestione dell'errore ---
			dbconn->LastErrorCode = SPV_DBI_QUERY_FAILURE;
			dbconn->LastError     = ULStrCpy( dbconn->LastError, "Query fallita" );
			ret_code = SPV_DBI_QUERY_FAILURE;
		  }
          retries = 0;
          // --- ALTRI TENTATIVI ---
          while ( ret_code == SPV_DBI_QUERY_FAILURE && dbconn->LastErrorCode == -2147467259 && retries < _LOCAL_DB_QUERY_RETRIES )
		  {
            retries++;
            ret_code = SPV_DBI_NO_ERROR;
            Sleep( SPV_DBI_QUERY_RETRY_DELAY );
            try
            {
              // --- Eseguo la query ---
			  SQLCmd.Execute();
			  Sleep( 250 + ( query_len / 10 ) + ( retries * 50 ) );
            }
            catch( SAException &ex )
            {
              // --- Gestione dell'errore ---
              dbconn->LastErrorCode = ex.ErrNativeCode();
			  dbconn->LastError     = ULStrCpy( dbconn->LastError, ex.ErrText() );
              ret_code = SPV_DBI_QUERY_FAILURE;
            }
          }
          if ( ret_code == SPV_DBI_NO_ERROR )
          {
            // --- Se la query ha prodotto un risultato ---
            if ( SQLCmd.isResultSet() )
            {
              field_num = SQLCmd.FieldCount();
              row_num = 0;
              if ( field_num > 0 )
              {
                // --- Alloco 10000 righe temporanee ---
                temp_rows = SPVDBNewRows( SPV_DBI_QUERY_MAX_RES_ROWS, field_num ); // -MALLOC
                if ( temp_rows != NULL )
                {
                  // --- Ciclo di fetching delle righe ---
                  while ( SQLCmd.FetchNext() )
                  {
                    // --- Ciclo sui campi della riga ---
                    for( unsigned int j = 0; j < field_num; j++ )
					{
					  // --- BUGIFX 20060309-01 ---
					  /*
					  field_size = SQLCmd.Field(j+1).FieldSize();
					  //*/
					  // --- BUGFIX 20060428-01 ---
					  if ( SQLCmd.Field(j+1).isNull() ) {
						  data = NULL;
						  field_size = 0;
					  }
					  else
					  {
						  if ( SQLCmd.Field(j+1).FieldType() == SA_dtLongBinary )
						  {
							field_size = SQLCmd.Field(j+1).asBytes().GetBinaryLength() + 1;
							data = (char*)malloc( sizeof(char) * ( field_size ) );
							memset( data, 0, sizeof(char) * field_size ) ;
							memcpy( data, SQLCmd.Field(j+1).asBytes().GetBinaryBuffer(0), sizeof(char) * field_size );
						  }
						  else
						  {
							field_size = strlen( SQLCmd.Field(j+1).asString() );
							data = (char*)malloc( sizeof(char) * ( field_size + 1 ) );
							strcpy( data, SQLCmd.Field(j+1).asString() );
						  }
					  }
					  temp_rows[row_num].Fields[j].Data     = data;
					  temp_rows[row_num].Fields[j].DataLen  = field_size;
					  temp_rows[row_num].Fields[j].ID       = j;
					  temp_rows[row_num].Fields[j].Type     = SQLCmd.Field(j+1).FieldType();
					}
					// --- Incremento il numero della riga ---
					row_num++;
				  }
				  // --- Alloco la struttura definitiva per il risultato ---
				  res = SPVDBNewRes( 0, row_num, field_num );
				  if ( res != NULL )
				  {
					// --- Ciclo sulle righe del risultato ---
                    for( unsigned int i = 0; i < row_num; i++ )
                    {
                      // --- Ciclo sulle colonne del risultato ---
                      for( unsigned int j = 0; j < field_num; j++ )
                      {
                        // --- Copio un singolo campo ---
                        res->Rows[i].Fields[j] = temp_rows[i].Fields[j];
                        temp_rows[i].Fields[j].Data = NULL;
                      }
                    }
                  }
                  // --- Cancello le righe temporanee ---
                  SPVDBDeleteRows( temp_rows, SPV_DBI_QUERY_MAX_RES_ROWS );
                  //SPVDBDeleteRows( temp_rows, row_num );
                  // --- Se c'� un risultato precedente lo cancello ---
                  if ( dbconn->LastResult != NULL ) SPVDBDeleteRes( dbconn->LastResult );
                  // --- Salvo il nuovo risultato ---
                  dbconn->LastResult = res;
                }
                else
                {
                  ret_code = SPV_DBI_ALLOCATION_FAILURE;
                }
              }
            }
          }
        }
        else
        {
          ret_code = SPV_DBI_INVALID_QUERY;
        }
      break;
      // --- Caso MySQL (API C) ---
      /*
      case SPV_DBI_CONN_TYPE_mySQL:
        mySQL = (MYSQL*)dbconn->Handler;
        if ( query != NULL ) // Allocazione andata a buon fine
        {
          if ( mysql_real_query( mySQL, query, query_len ) == 0 )
          {
            myRES = mysql_store_result( mySQL );
            if ( myRES != NULL )
            {
              row_num   = mysql_num_rows  ( myRES );
              field_num = mysql_num_fields( myRES );
              res = SPVDBNewRes( 0, row_num, field_num );
              if ( res != NULL )
              {
                myFIELD = mysql_fetch_fields( myRES );
                for( unsigned int i = 0; i < row_num; i++ )
                {
                  myROW     = mysql_fetch_row( myRES );
                  field_len = mysql_fetch_lengths( myRES );
                  for( unsigned int j = 0; j < field_num; j++ )
                  {
                    // --- Carico un campo ---
                    data = (char*)malloc( sizeof(char) * ( field_len[j] + 1 ) );
                    memcpy( data, myROW[j], field_len[j] );
                    data[field_len[j]] = '\0';
                    res->Rows[i].Fields[j].Data     = data;
                    res->Rows[i].Fields[j].DataLen  = field_len[j];
                    res->Rows[i].Fields[j].ID       = j;
                    res->Rows[i].Fields[j].Type     = myFIELD[j].type;
                  }
                }
              }
              // --- Se c'� un risultato precedente lo cancello ---
              if ( dbconn->LastResult != NULL ) SPVDBDeleteRes( dbconn->LastResult );
              // --- Salvo il nuovo risultato ---
              dbconn->LastResult = res;
              mysql_free_result( myRES );
            }
          }
          else
          {
            dbconn->LastErrorCode = mysql_errno(mySQL);
            dbconn->LastError     = SPVStrCpy(dbconn->LastError,mysql_error(mySQL));
            ret_code = SPV_DBI_QUERY_FAILURE;
          }
        }
        else
        {
          ret_code = SPV_DBI_INVALID_QUERY;
        }
      break;
      //*/
      default: /* Casi non supportati */
        ret_code = SPV_DBI_UNKNOWN_CONNECTION_TYPE;
	}
	//LeaveCriticalSection(&dbconn->CriticalSection); // Esco dalla sezione critica
	///SYSUnLock(&dbconn->Lock);
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
// Funzione per ripulire una stringa per una query
///
/// \date [22.08.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBClearQuery( char * query, int query_len )
{
  int new_len = 0;
  int zero_len = (query_len>0)?query_len:1;

  if ( query != NULL )
  {
    try
    {
      memset( query, 0, sizeof( char ) * zero_len );
    }
    catch(...)
    {
      new_len = 0;
    }
  }

  return new_len;
}

//==============================================================================
/// Funzione per appendere una stringa a una query passando la lunghezza
///
/// \date [24.10.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBAddToQuery( char * query, int query_len, char * str, int str_len )
{
  int     new_len   = 0   ; // Lunghezza nuova query

  if ( str != NULL && str_len > 0 ) // Se la nuova stringa da appendere � valida
  {
    if ( query != NULL )
    {
      memcpy( &query[query_len], str, str_len );
      new_len = query_len + str_len;
      query[new_len] = '\0';
    }
  }
  else
  {
    new_len = query_len;
  }

  return new_len;
}

//==============================================================================
/// Funzione per appendere una stringa a una query
///
/// \date [03.10.2006]
/// \author Enrico Alborali
/// \version 0.05
//------------------------------------------------------------------------------
int SPVDBAddToQuery( char * query, int query_len, char * str )
{
  int     str_len   = 0   ; // Lunghezza stringa
  int     new_len   = 0   ; // Lunghezza nuova query

  if ( str != NULL ) // Se la nuova stringa da appendere � valida
  {
    if ( query != NULL )
    {
      try
      {
        str_len = strlen( str );
      }
      catch(...)
      {
        str_len = 0;
      }
      if ( str_len > 0 )
      {
        new_len = SPVDBAddToQuery( query, query_len, str, str_len );
      }
      else
      {
        new_len = query_len;
      }
    }
  }
  else
  {
    new_len = query_len;
  }

  return new_len;
}

//==============================================================================
/// Funzione per selezionare un database
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SPVDBUse( SPV_DBI_CONN * dbconn, char * database )
{
  int     ret_code  = 0   ; // Codice di ritorno
  char    query_str[SPV_DBI_QUERY_MAX_LENGTH];
  char  * query     = &query_str[0];

  if ( dbconn != NULL ) // Controllo il ptr della connessione
  {
	//EnterCriticalSection(&dbconn->CriticalSection); // Entro nella sezione critica
	SYSLock(&dbconn-> Lock,"SPVDBUse");
    switch ( dbconn->Type ) // Switcho sui tipi di connessione database
    {
      // --- Caso Microsoft SQL Server / MSDE ---
      case SPV_DBI_CONN_TYPE_MS_SQL_SERVER:
        query[0] = 0;
        strcat( query, "USE " );
        strcat( query, database );
        if ( SPVDBQuery( dbconn, query, 0, NULL, 0 ) == 0 )
        {
		  dbconn->Database = ULStrCpy( dbconn->Database, database );
        }
        else
        {
          ret_code = SPV_DBI_QUERY_FAILURE;
        }
      break;
      // --- Caso MySQL (API C) ---
      /*
      case SPV_DBI_CONN_TYPE_mySQL:
        query[0] = 0;
        strcat( query, "USE " );
        strcat( query, database );
        if ( SPVDBQuery( dbconn, query, 0, NULL, 0 ) == 0)
        {
          dbconn->Database = SPVStrCpy( dbconn->Database, database );
        }
        else
        {
          ret_code = SPV_DBI_QUERY_FAILURE;
        }
      break;
      //*/
      default: /* Casi non supportati */
        ret_code = SPV_DBI_UNKNOWN_CONNECTION_TYPE;
    }
	//LeaveCriticalSection(&dbconn->CriticalSection); // Esco dalla sezione critica
	SYSUnLock(&dbconn->Lock);
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per scrivere una riga
///
/// \date [12.10.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBWriteRow( SPV_DBI_CONN * dbconn, char * table, char * fields, char * values, long values_len )
{
  int     ret_code    = 0; // Codice di ritorno
  char    query[SPV_DBI_QUERY_MAX_LENGTH];
  long    query_len   = 0; //

  if ( dbconn != NULL )
  {
    if ( table != NULL )
    {
      strcpy(&query[0],"INSERT INTO ");
      query_len += strlen("INSERT INTO ");
      strcpy(&query[query_len],table);
      query_len += strlen(table);
      query[query_len] = ' ';
      query_len += 1;
      if (fields != NULL)
      {
        query[query_len] = '(';
        query_len += 1;
        strcpy(&query[query_len],fields);
        query_len += strlen(fields);
        query[query_len] = ')';
        query_len += 1;
        query[query_len] = ' ';
        query_len += 1;
      }
      strcpy(&query[query_len],"VALUES (");
      query_len += strlen("VALUES (");
      for (int i=0; i<values_len; i++)
      {
        query[query_len+i] = values[i];
      }
      query_len += values_len;
      query[query_len] = ')';
      query_len += 1;
      ret_code = SPVDBQuery( dbconn, &query[0], query_len, NULL, 0 );
    }
    else
    {
      ret_code = SPV_DBI_INVALID_TABLE;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per leggere una riga
///
/// \date [12.10.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDBReadRow( SPV_DBI_CONN * dbconn, char * table, char * fields, int ID )
{
  int     ret_code    = 0; // Codice di ritorno
  long    query_len   = 0; // Lunghezza della query
  char    query[SPV_DBI_QUERY_MAX_LENGTH];

  if ( dbconn != NULL ) // Controllo la validit� del ptr a connessione
  {
	if ( table != NULL ) // Controllo la validit� della stringa tabella
    {
      strcpy(&query[query_len],"SELECT ");
      query_len += strlen("SELECT ");
      if (fields != NULL)
      {
        strcpy(&query[query_len],fields);
        query_len += strlen(fields);
        query[query_len] = ' ';
        query_len += 1;
      }
      else
      {
        strcpy(&query[query_len],"* ");
        query_len += strlen("* ");
      }
      strcpy(&query[query_len],"FROM ");
      query_len += strlen("FROM ");
      strcpy(&query[query_len],table);
      query_len += strlen(table);
      strcpy(&query[query_len]," WHERE ID = ");
      query_len += strlen(" WHERE ID = ");
      query_len += sprintf(&query[query_len],"%i",ID);
      ret_code = SPVDBQuery( dbconn, &query[0], query_len, NULL, 0 );
    }
    else
    {
      ret_code = SPV_DBI_INVALID_TABLE;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una riga
///
/// \date [12.10.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBDeleteRow( SPV_DBI_CONN * dbconn, char * table, int ID )
{
  int   ret_code    = 0; // Codice di ritorno
  long  query_len   = 0; // Lunghezza della query
  char  query[SPV_DBI_QUERY_MAX_LENGTH];

  if ( dbconn != NULL ) // Controllo la validit� del ptr a connessione
  {
    if ( table != NULL ) // Controllo la validit� della stringa tabella
    {
      strcpy(&query[query_len],"DELETE FROM ");
      query_len += strlen("DELETE FROM ");
      strcpy(&query[query_len],table);
      query_len += strlen(table);
      strcpy(&query[query_len]," WHERE ID = ");
      query_len += strlen(" WHERE ID = ");
      query_len += sprintf(&query[query_len],"%i",ID);
      ret_code = SPVDBQuery( dbconn, &query[0], query_len, NULL, 0);
    }
    else
    {
      ret_code = SPV_DBI_INVALID_TABLE;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per modificare un campo
///
/// \date [12.10.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SPVDBModifyField( SPV_DBI_CONN * dbconn, char * table, char * field, char * value, long value_len, int ID )
{
  int   ret_code    = 0; // Codice di ritorno
  long  query_len   = 0; // Lunghezza della query
  char  query[SPV_DBI_QUERY_MAX_LENGTH];

  if ( dbconn != NULL ) // Controllo la validit� del ptr a connessione
  {
    if ( table != NULL ) // Controllo la validit� della stringa tabella
    {
      if ( field != NULL ) // Controllo la validit� del ptr stringa campo
      {
        if ( value != NULL ) // Controllo la validit� del ptr stringa valore
        {
          strcpy(&query[query_len],"UPDATE ");
          query_len += strlen("UPDATE ");
          strcpy(&query[query_len],table);
          query_len += strlen(table);
		  strcpy(&query[query_len]," SET ");
          query_len += strlen(" SET ");
          strcpy(&query[query_len],field);
          query_len += strlen(field);
          query[query_len] = '=';
          query_len += 1;
          for (int i=0; i<value_len; i++)
          {
            query[query_len+i] = value[i];
          }
          query_len += value_len;
          strcpy(&query[query_len]," WHERE ID = ");
          query_len += strlen(" WHERE ID = ");
          query_len += sprintf(&query[query_len],"%i",ID);
          ret_code = SPVDBQuery( dbconn, &query[0], query_len, NULL, 0 );
        }
        else
        {
          ret_code = SPV_DBI_INVALID_VALUE;
        }
      }
      else
      {
        ret_code = SPV_DBI_INVALID_FIELD;
      }
    }
    else
    {
      ret_code = SPV_DBI_INVALID_TABLE;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiornare colonne di una tabella
///
/// \date [21.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBUpdateTable( SPV_DBI_CONN * dbconn, char * table, char * values )
{
  int   ret_code    = 0; // Codice di ritorno
  long  query_len   = 0; // Lunghezza della query
  char  query[SPV_DBI_QUERY_MAX_LENGTH];

  if ( dbconn != NULL ) // Controllo la validit� del ptr a connessione
  {
    if ( table != NULL ) // Controllo la validit� della stringa tabella
    {
      if ( values != NULL ) // Controllo la validit� del ptr stringa valore
      {
        // --- Preparo la query per verificare se riga esiste ---
        query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
        query_len = SPVDBAddToQuery( query, query_len, "UPDATE " );
        query_len = SPVDBAddToQuery( query, query_len, table );
        query_len = SPVDBAddToQuery( query, query_len, " SET " );
        query_len = SPVDBAddToQuery( query, query_len, values );
        // --- Lancio la query ---
        ret_code = SPVDBQuery( dbconn, query, query_len, NULL, 0 );
      }
      else
	  {
        ret_code = SPV_DBI_INVALID_VALUE;
      }
    }
    else
    {
      ret_code = SPV_DBI_INVALID_TABLE;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per svuotare completamente una tabella
///
/// \date [24.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SPVDBTruncateTable( SPV_DBI_CONN * dbconn, char * table )
{
  int   ret_code    = 0; // Codice di ritorno
  long  query_len   = 0; // Lunghezza della query
  char  query[SPV_DBI_QUERY_MAX_LENGTH];

  if ( dbconn != NULL ) // Controllo la validit� del ptr a connessione
  {
    if ( table != NULL ) // Controllo la validit� della stringa tabella
    {
	  // --- Preparo la query ---
      query_len = SPVDBClearQuery( query, SPV_DBI_QUERY_MAX_LENGTH );
      query_len = SPVDBAddToQuery( query, query_len, "TRUNCATE TABLE " );
      query_len = SPVDBAddToQuery( query, query_len, table );
      // --- Lancio la query ---
      ret_code = SPVDBQuery( dbconn, query, query_len, NULL, 0 );
    }
    else
    {
      ret_code = SPV_DBI_INVALID_TABLE;
    }
  }
  else
  {
    ret_code = SPV_DBI_INVALID_CONNECTION;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere il numero di righe dei risultati di una query
///
/// \date [12.10.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SPVDBGetRowNum( SPV_DBI_CONN * dbconn )
{
  int row_num = 0; // Numero di righe nell'ultimo result set

  try
  {
    if ( dbconn != NULL )
	{
      if ( dbconn->LastResult != NULL )
      {
        row_num = dbconn->LastResult->RowNum;
      }
    }
  }
  catch(...)
  {
    row_num = 0;
  }

  return row_num;
}

//==============================================================================
/// Funzione per ottenere un campo dai risultati di una query
///
/// \date [12.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
SPV_DBI_FIELD * SPVDBGetField( SPV_DBI_CONN * dbconn, int row, int col )
{
  SPV_DBI_RES   * res   = NULL; // Ptr al risultato di una query
  SPV_DBI_FIELD * field = NULL; // Ptr al campo
  SPV_DBI_ROW   * rrow  = NULL; // Ptr alla riga

  if ( dbconn != NULL ) // Controllo il ptr alla struttura di connessione
  {
	//EnterCriticalSection(&dbconn->CriticalSection); // Entro nella sezione critica
	///SYSLock(&dbconn->Lock,"SPVDBGetField");
    if ( row >= 0 && col >= 0 )
    {
      res = dbconn->LastResult;
	  if ( res != NULL )
      {
        if ( row < res->RowNum )
        {
          rrow = &res->Rows[row];
          if ( col < rrow->FieldNum )
          {
            field = &(rrow->Fields[col]);
          }
        }
      }
    }
	//LeaveCriticalSection(&dbconn->CriticalSection); // Esco dalla sezione critica
	///SYSUnLock(&dbconn->Lock);
  }

  return field;
}

//==============================================================================
/// Funzione per formattare una data/ora
///
/// In caso di errore restituisce NULL.
/// NOTA: Se date_time = 0 restituisce una nuova stringa N.T. allocata con la
///       data ora attuale.
///
/// \date [18.10.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
char * SPVDBGetTime( long date_time )
{
	struct tm   * time_now  = NULL; // Data ora formato tm
	time_t        secs_now        ; // Data ora formato time_t
	char        * str       = NULL; // Stringa per la data ora

	if ( date_time == 0 )
	{
		time( &secs_now );
	}
	else
	{
		secs_now = (time_t)date_time;
	}
	time_now = localtime( &secs_now );
	if ( time_now != NULL )
	{
		str = (char*)malloc( sizeof(char) * 24 );
		if ( str != NULL )
		{
			strftime( str, 24, "%Y-%m-%d %H:%M:%S.000", time_now );
		}
	}

	return str;
}

//==============================================================================
/// Funzione per ottenere un formato uniqueid da una stringa
///
/// \date [16.04.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * SPVDBGetUniqueID( char * hexstring )
{
	char base[37];
	char * uniqueid = NULL;
	int hexlen = 0;

	if ( hexstring != NULL ) {
		hexlen = strlen( hexstring );
		if ( hexlen >= 32 ) {
			memcpy( &base[0], &hexstring[6], sizeof(char) );
			memcpy( &base[1], &hexstring[7], sizeof(char) );
			memcpy( &base[2], &hexstring[4], sizeof(char) );
			memcpy( &base[3], &hexstring[5], sizeof(char) );
			memcpy( &base[4], &hexstring[2], sizeof(char) );
			memcpy( &base[5], &hexstring[3], sizeof(char) );
			memcpy( &base[6], &hexstring[0], sizeof(char) );
			memcpy( &base[7], &hexstring[1], sizeof(char) );
			base[8]= '-';
			memcpy( &base[9], &hexstring[10], sizeof(char) );
			memcpy( &base[10], &hexstring[11], sizeof(char) );
			memcpy( &base[11], &hexstring[8], sizeof(char) );
			memcpy( &base[12], &hexstring[9], sizeof(char) );
			base[13]= '-';
			memcpy( &base[14], &hexstring[14], sizeof(char) );
			memcpy( &base[15], &hexstring[15], sizeof(char) );
			memcpy( &base[16], &hexstring[12], sizeof(char) );
			memcpy( &base[17], &hexstring[13], sizeof(char) );
			base[18]= '-';
			memcpy( &base[19], &hexstring[16], sizeof(char) );
			memcpy( &base[20], &hexstring[17], sizeof(char) );
			memcpy( &base[21], &hexstring[18], sizeof(char) );
			memcpy( &base[22], &hexstring[19], sizeof(char) );
			base[23]= '-';
			memcpy( &base[24], &hexstring[20], sizeof(char) );
			memcpy( &base[25], &hexstring[21], sizeof(char) );
			memcpy( &base[26], &hexstring[22], sizeof(char) );
			memcpy( &base[27], &hexstring[23], sizeof(char) );
			memcpy( &base[28], &hexstring[24], sizeof(char) );
			memcpy( &base[29], &hexstring[25], sizeof(char) );
			memcpy( &base[30], &hexstring[26], sizeof(char) );
			memcpy( &base[31], &hexstring[27], sizeof(char) );
			memcpy( &base[32], &hexstring[28], sizeof(char) );
			memcpy( &base[33], &hexstring[29], sizeof(char) );
			memcpy( &base[34], &hexstring[30], sizeof(char) );
			memcpy( &base[35], &hexstring[31], sizeof(char) );
			base[36]= 0;
			uniqueid = ULStrCpy( uniqueid, base );
		}
	}

	return uniqueid;
}


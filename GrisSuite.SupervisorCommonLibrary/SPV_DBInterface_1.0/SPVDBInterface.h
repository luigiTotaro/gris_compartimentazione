//==============================================================================
// Telefin Supervisor DataBase Interface Module 1.0
//------------------------------------------------------------------------------
// Header modulo di interafaccia verso i database (SPVDBInterface.h)
// Progetto:  Telefin Supervisor Server 1.0
//
// Revisione:  2.08 (02.09.2004 -> 12.02.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, 2006 e 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      richiede SPVDBInterface.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [02.09.2004]:
// - Prima versione de modulo.
// 0.02 [03.09.2004]:
// - Aggiunta la definizione del tipo SPV_DBI_FIELD.
// - Aggiunta la definizione del tipo SPV_DBI_ROW.
// - Aggiunta la definizione del tipo SPV_DBI_RES.
// - Modificata la funzione SPVDBQuery.
// - Aggiunto il codice di errore SPV_DBI_INVALID_FIELD.
// - Aggiunto il codice di errore SPV_DBI_INVALID_ROW.
// - Aggiunto il codice di errore SPV_DBI_INVALID_FIELD_LIST.
// - Aggiunto il codice di errore SPV_DBI_INVALID_FIELD_NUM.
// - Aggiunto il codice di errore SPV_DBI_INVALID_ROW_LIST.
// - Aggiunto il codice di errore SPV_DBI_INVALID_ROW_NUM.
// - Aggiunto il codice di errore SPV_DBI_INVALID_RESULT.
// - Aggiunta la funzione SPVDBNewField.
// - Aggiunta la funzione SPVDBDeleteField.
// - Aggiunta la funzione SPVDBNewFields.
// - Aggiunta la funzione SPVDBDeleteFields.
// - Aggiunta la funzione SPVDBNewRow.
// - Aggiunta la funzione SPVDBDeleteRow.
// - Aggiunta la funzione SPVDBNewRows.
// - Aggiunta la funzione SPVDBDeleteRows.
// - Aggiunta la funzione SPVDBNewRes (due implementazioni).
// - Aggiunta la funzione SPVDBDeleteRes.
// - Modificata la funzione SPVDBDeleteConn.
// - Modificata la funzione SPVDBNewConn.
// 0.03 [06.09.2004]:
// - Modificata la funzione SPVDBQuery.
// - Modificata la funzione SPVDBUse.
// - Aggiunto il codice di errore SPV_DBI_ALLOCATION_FAILURE.
// - Aggiunto il codice di errore SPV_DBI_INVALID_TABLE.
// - Modificata la definizione del tipo SPV_DBI_FIELD.
// - Aggiunta la funzione SPVDBWriteRow.
// - Aggiunta la funzione SPVDBReadRow.
// - Aggiunta la funzione SPVDBGetField.
// 0.04 [07.09.2004]:
// - Modificata la funzione SPVDBReadRow.
// - Aggiunto il codice di errore SPV_DBI_INVALID_VALUE.
// - Aggiunta la funzione SPVDBDeleteRow.
// - Aggiunta la funzione SPVDBModifyField.
// 0.05 [08.09.2004]:
// - Aggiunta la funzione SPVDBGetTime.
// 0.06 [19.05.2005]:
// - Aggiunto campo <Persistent> nella struttura SPV_DBI_CONN.
// 0.07 [24.05.2005]:
// - Modificata la funzione SPVDBNewConn.
// - Aggiunto il campo <Def> alla struttura SPV_DBI_CONN.
// 0.08 [25.05.2005]:
// - Incluso winsock.h per supportare le API di MySQL 4.1.12.
// - Modificata la funzione SPVDBOpenConn.
// - Modificata la funzione SPVDBUse.
// 0.09 [15.07.2005]:
// - Modificata la funzione SPVDBDeleteConn.
// - Modificata la funzione SPVDBOpenConn.
// 0.10 [29.07.2005]:
// - Corretta la funzione SPVDBQuery.
// 0.11 [03.08.2005]:
// - Modificata la funzione SPVDBGetField.
// 1.12 [10.10.2005]:
// - Inclusa la libreria SQLAPI++ versione 3.7.12.
// - Aggiunto il nuovo tipo di database SPV_DBI_CONN_TYPE_MS_SQL_SERVER.
// - Modificata la funzione SPVDBNewConn.
// - Modificata la funzione SPVDBDeleteConn.
// - Modificata la funzione SPVDBOpenConn.
// - Modificata la funzione SPVDBCloseConn.
// 1.13 [11.10.2005]:
// - Modificata la funzione SPVDBOpenConn.
// - Modificata la funzione SPVDBQuery.
// 1.14 [12.10.2005]:
// - Aggiunta la funzione SPVDBGetTypeCode.
// - Modificata la funzione SPVDBDeleteConn.
// - Modificata la funzione SPVDBOpenConn.
// - Modificata la funzione SPVDBQuery.
// - Modificata la funzione SPVDBUse.
// - Modificata la funzione SPVDBWriteRow.
// - Modificata la funzione SPVDBReadRow.
// - Modificata la funzione SPVDBDeleteRow.
// - Modificata la funzione SPVDBModifyField.
// - Modificata la funzione SPVDBGetField.
// - Modificata la funzione SPVDBGetTime.
// 1.15 [17.10.2005]:
// - Modificata la funzione SPVDBAddToQuery.
// 1.16 [18.10.2005]:
// - Modificata la funzione SPVDBAddToQuery.
// - Modificata la funzione SPVDBGetTime.
// 1.17 [19.10.2005]:
// - Inclusa la libreria SQLAPI++ versione 3.7.14 Pro.
// - Modificata la funzione SPVDBDeleteConn.
// 1.18 [24.10.2005]:
// - Modificata la funzione SPVDBAddToQuery.
// - Aggiunta una nuova implementazione di SPVDBAddToQuery.
// 1.19 [19.12.2005]:
// - Inclusa la libreria SQLAPI++ versione 3.7.16 Pro.
// - Modificata la funzione SPVDBDeleteConn.
// - Modificata la funzione SPVDBQuery.
// 1.20 [15.02.2006]:
// - Aggiunta SPVDBGetTypeName().
// 1.21 [09.03.2006]:
// - Modificata SPVDBDeleteFields().
// - Aggiunta SPVDBClearQuery().
// 1.22 [21.03.2006]:
// - Aggiunta SPVDBUpdateTable().
// 2.00 [30.03.2006]:
// - Tolta la libreria MySQL API C.
// 2.01 [28.04.2006]:
// - Corretta la funzione SPVDBQuery.
// 2.02 [01.06.2006]:
// - Modificata la funzione SPVDBOpenConn (Connessione SQL Server Express SP1)
// 2.03 [22.08.2006]:
// - Aggiunta SPVDBGetRowNum().
// - Modificata SPVDBClearQuery().
// 2.04 [24.03.2006]:
// - Aggiunta SPVDBTruncateTable().
// 2.05 [25.08.2006]:
// - Modificata SPVDBQuery() (tempo di ritardo tentativi incrementale).
// [13.04.2007]:
// - Aggiunta SPVDBGetUniqueID().
// 2.06 [19.10.2007]:
// - Modificata la funzione SPVDBOpenConn
// - Modificata la funzione SPVDBCloseConn
// 2.07 [24.10.2007]:
// - Modificata la funzione SPVDBDeleteConn per eliminare il memory leak relativo ad una SAConnection
// 2.08 [12.02.2008]:
// - Modificato il tipo SPV_DBI_CONN  ai fini della nuova gestione delle eccezioni;
// - Introdotta nuova gestione delle eccezioni nelle seguenti funzioni:
//		- SPVDBNewConn;
//		- SPVDBDeleteConn;
//		- SPVDBOpenConn;
//		- SPVDBCloseConn;
//		- SPVDBQuery;
//		- SPVDBUse;
//		- SPVDBGetField;
//==============================================================================

#ifndef SPVDBInterfaceH
#define SPVDBInterfaceH

#include <windows.h>
#include "SYSKernel.h"

//==============================================================================
/// Tipi di connessione a database server supportati
//------------------------------------------------------------------------------
#define SPV_DBI_CONN_TYPE_UNKNOWN       0
#define SPV_DBI_CONN_TYPE_mySQL         76219
#define SPV_DBI_CONN_TYPE_ODBC          79353
#define SPV_DBI_CONN_TYPE_MS_SQL_SERVER 72219

//==============================================================================
/// Tipi di connessione a database server supportati
//------------------------------------------------------------------------------
#define SPV_DBI_CONN_TYPE_NAME_UNKNOWN  "Database sconosciuto"
#define SPV_DBI_CONN_TYPE_NAME_MYSQL    "MySQL Server"
#define SPV_DBI_CONN_TYPE_NAME_ODBC     "Generico ODBC"
#define SPV_DBI_CONN_TYPE_NAME_MSSQL    "MS SQL Server"

#define SPV_DBI_QUERY_MAX_LENGTH        512000
#define SPV_DBI_QUERY_MAX_RES_ROWS      10000
#define SPV_DBI_QUERY_RETRY_DELAY       1000

//==============================================================================
/// Codici di errore
//------------------------------------------------------------------------------
#define SPV_DBI_NO_ERROR                    0
#define SPV_DBI_INVALID_CONNECTION          11000
#define SPV_DBI_UNKNOWN_CONNECTION_TYPE     11001
#define SPV_DBI_CONNECT_FAILURE             11002
#define SPV_DBI_QUERY_FAILURE               11003
#define SPV_DBI_SELECT_DB_FAILURE           11004
#define SPV_DBI_ERROR_CONNECTION_ACTIVE     11005
#define SPV_DBI_INVALID_FIELD               11006
#define SPV_DBI_INVALID_ROW                 11007
#define SPV_DBI_INVALID_FIELD_LIST          11008
#define SPV_DBI_INVALID_FIELD_NUM           11009
#define SPV_DBI_INVALID_ROW_LIST            11010
#define SPV_DBI_INVALID_ROW_NUM             11011
#define SPV_DBI_INVALID_RESULT              11012
#define SPV_DBI_ALLOCATION_FAILURE          11013
#define SPV_DBI_INVALID_TABLE               11014
#define SPV_DBI_INVALID_VALUE               11015
#define SPV_DBI_WARNING_DATABASE_NOT_FOUND  11016
#define SPV_DBI_CRITICAL_ERROR              11017
#define SPV_DBI_INVALID_QUERY               11018
#define SPV_DBI_DELETE_FAILURE              11019
#define SPV_DBI_DISCONNECT_FAILURE			11020
#define SPV_DBI_FUNCTION_EXCEPTION			11021

//==============================================================================
/// Struttura dati per un campo
///
/// \date [06.09.2004]
/// \author Enrico Alborali.
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SPV_DBI_FIELD
{
  int                 ID              ; // Codice numerico identificativo
  int                 Type            ; // Tipo di campo
  char              * Data            ; // Ptr al dato
  unsigned long       DataLen         ; // Lunghezza del dato
}
SPV_DBI_FIELD;

//==============================================================================
/// Struttura dati per una righa
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DBI_ROW
{
  int                 ID              ; // Codice numerico identificativo
  int                 FieldNum        ; // Numero di campi nella riga
  SPV_DBI_FIELD     * Fields          ; // Ptr alla lista di campi
}
SPV_DBI_ROW;

//==============================================================================
/// Struttura dati per un risultato di una query.
///
/// \date [03.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SPV_DBI_RES
{
  int                 ID              ; // Codice numerico identificativo
  int                 RowNum          ; // Numero di righe
  SPV_DBI_ROW       * Rows            ; // Ptr alla lista delle righe
}
SPV_DBI_RES;

//==============================================================================
/// Struttura dati per una connessione database.
///
/// \date [24.05.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SPV_DBI_CONN
{
  int                 ID              ; // Codice numerico identificatico
  char              * Name            ; // Nome della connessione
  int                 Type            ; // Tipo di connessione (tipo di database)
  char              * Host            ; // Host del database server
  char              * UserName        ; // Nome utente
  char              * Password        ; // Password
  char              * Database        ; // Nome del database di default (opzionale)
  bool                Persistent      ; // Flag di connessione persistente
  void              * Handler         ; // Ptr a struttura specifica di gestione connessione
  void              * Params          ; // Paramatri (opzionale)
  bool                Active          ; // Flag di connessione attiva
  void              * Status          ; // Stato (opzionale)
  int                 LastErrorCode   ; // Ultimo codice di errore
  char              * LastError       ; // Ultima stringa di errore
  SPV_DBI_RES       * LastResult      ; // Ptr all'ultimo risultato della query
  void              * Def             ; // Ptr alla definizione XML della connessione
  //_CRITICAL_SECTION    CriticalSection ; // Handler di sezione critica per la connessione
  SYS_LOCK    		Lock		  	  ; // Descrittore Lock per la connessione
}
SPV_DBI_CONN;

//==============================================================================
/// Funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo
void            SPVDBInit         ( void );
// Funzione per ottenere il codice di un tipo database in base alla stringa
int             SPVDBGetTypeCode  ( char * type_str );
// Funzione per ottenere il nome di un tipo database in base al codice
char          * SPVDBGetTypeName  ( int type );
// Funzione per creare una nuova struttura campo
SPV_DBI_FIELD * SPVDBNewField     ( int ID, int type, char * data, int len );
// Funzione per eliminare una struttura campo
int             SPVDBDeleteField  ( SPV_DBI_FIELD * field );
// Funzione per creare una lista di campi
SPV_DBI_FIELD * SPVDBNewFields    ( int num );
// Funzione per eliminare una lista di campi
int             SPVDBDeleteFields ( SPV_DBI_FIELD * fields, int num );
// Funzione per creare una nuova struttura riga
SPV_DBI_ROW   * SPVDBNewRow       ( int ID, int field_num, SPV_DBI_FIELD * fields );
// Funzione per eliminare una struttura riga
int             SPVDBDeleteRow    ( SPV_DBI_ROW * row );
// Funzione per creare una nuova lista di righe
SPV_DBI_ROW   * SPVDBNewRows      ( int num, int field_num );
// Funzione per eliminare una lista di righe
int             SPVDBDeleteRows   ( SPV_DBI_ROW * rows, int num );
// Funzione per creare una nuova struttura risultato query
SPV_DBI_RES   * SPVDBNewRes       ( int ID, int row_num, SPV_DBI_ROW * rows );
// Funzione per creare una nuova struttura risultato query con tutte le liste sottostanti
SPV_DBI_RES   * SPVDBNewRes       ( int ID, int row_num, int field_num );
// Funzione per eliminare una struttura risultato query
int             SPVDBDeleteRes    ( SPV_DBI_RES * res );
// Funzione per creare una nuova connessione a database
SPV_DBI_CONN  * SPVDBNewConn      ( int ID, char * name, int type, char * host, char * user, char * pass, char * database );
// Funzione per eliminare una connessione a database
int             SPVDBDeleteConn   ( SPV_DBI_CONN * dbconn );
// Funzione per connettersi ad un database server
int             SPVDBOpenConn     ( SPV_DBI_CONN * dbconn );
// Funzione per disconnettersi da un database server
int             SPVDBCloseConn    ( SPV_DBI_CONN * dbconn );
// Funzione per eseguire una query su un database
int             SPVDBQuery        ( SPV_DBI_CONN * dbconn, char * query, long len, void * param, long param_size );
// Funzione per ripulire una stringa per una query
int             SPVDBClearQuery     ( char * query, int query_len );
// Funzione per appendere una stringa a una query passando la lunghezza
int             SPVDBAddToQuery   ( char * query, int query_len, char * str, int str_len );
// Funzione per appendere una stringa a una query
int             SPVDBAddToQuery   ( char * query, int query_len, char * str );
// Funzione per selezionare un database
int             SPVDBUse          ( SPV_DBI_CONN * dbconn, char * database );
// Funzione per scrivere una riga
int             SPVDBWriteRow     ( SPV_DBI_CONN * dbconn, char * table, char * fields, char * values, long values_len );
// Funzione per leggere una riga
int             SPVDBReadRow      ( SPV_DBI_CONN * dbconn, char * table, char * fields, int ID );
// Funzione per eliminare una riga
int             SPVDBDeleteRow    ( SPV_DBI_CONN * dbconn, char * table, int ID );
// Funzione per modificare una riga
int             SPVDBModifyField  ( SPV_DBI_CONN * dbconn, char * table, char * field, char * value, long value_len, int ID );
// Funzione per aggiornare colonne di una tabella
int             SPVDBUpdateTable  ( SPV_DBI_CONN * dbconn, char * table, char * values );
// Funzione per svuotare completamente una tabella
int             SPVDBTruncateTable( SPV_DBI_CONN * dbconn, char * table );
// Funzione per ottenere il numero di righe dei risultati di una query
int             SPVDBGetRowNum    ( SPV_DBI_CONN * dbconn );
// Funzione per ottenere un campo dai risultati di una query
SPV_DBI_FIELD 	* SPVDBGetField     ( SPV_DBI_CONN * dbconn, int row, int col );
// Funzione per formattare una data/ora
char          	* SPVDBGetTime      ( long date_time );
// Funzione per ottenere un formato uniqueid da una stringa
char			* SPVDBGetUniqueID	( char * hexstring );

//------------------------------------------------------------------------------
#endif

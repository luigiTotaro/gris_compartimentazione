//---------------------------------------------------------------------------
#ifndef XPortH
#define XPortH

#include <vcl.h>
#include <Classes.hpp>
#include <Grids.hpp>
#include <stdio.h>

// DEFINIZIONE LUNGHEZZE MASSIME
#define X_MAX_CHAR  1024
#define X_MAX_LINE  1000
#define X_MAX_TOKEN 50

// DEFINIZIONE STRINGHE DI IDENTIFICAZIONE
#define X_SUP_ID  "SupID"
#define X_DIR_ID  "DirID"
#define X_NOD_ID  "NodID"
#define X_DEV_ID  "DevID"

// DEFINIZIONE VALORI DI DEFAULT
#define X_DEF_ID_FORMAT "DirID:2;NodID:3;DevID:2"
#define X_DEF_LINE_FORMAT "9999996|StatusLevel|DevName|Status"
#define X_DEF_DT_FORMAT "yyyy/mm/dd hh:nn:ss"

// DEFINIZIONE ALIAS FUNZIONI
#define XPlode  XExplode
#define XTract  XExtract

// DEFINIZIONE STATI
#define AS_STAT_SUP_UNKNOWN 0
#define AS_STAT_SUP_OK 1
#define AS_STAT_SUP_ANYWARNING 2
#define AS_STAT_SUP_ANYALARM 3
#define AS_STAT_SUP_OFFLINE 4
#define AS_STAT_SUP_DISABLED 5


//==============================================================================
/// Struttura dati per una periferica
///
/// \date [07.11.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct XDevice{
  AnsiString  ID                ;
  AnsiString  SupID             ;
  AnsiString  SupName           ;
  AnsiString  DirID             ;
  AnsiString  DirName           ;
  AnsiString  NodID             ;
  AnsiString  NodName           ;
  AnsiString  DevID             ;
  AnsiString  DevName           ;
  int         StatusLevel       ;
  AnsiString  Status            ;
  bool        Registered        ;
  // CAMPI DI CONFIGURAZIONE
  int         cActivaLevel      ;
  int         cActivaDuration   ;
  int         cActivaFrequency  ;
  int         cFrequencyInterval;
  bool        cMultiSendEnabled ;
  int         cSendInterval     ;
  bool        cSendReturnMsg    ;
  int         cAlarmGroupID     ; // ID del gruppo di riferimento per inviare allarmi
  //AnsiString  cNameDestination  ; // DEPRECATED
  bool        cFAXEnabled       ; // Flag di creazione allarme via FAX
  //AnsiString  cFAXDestination   ; // DEPRECATED
  bool        cPrinterEnabled   ; // Flag di creazione allarme via Stampante
  bool        cSMSEnabled       ; // Flag di creazione allarme via SMS
  bool        cSendStartStop    ;
  AnsiString  cSendStart        ;
  AnsiString  cSendStop         ;
  // CAMPI RUNTIME
  bool        Active            ;
  bool        Alarm             ;
  bool        TrueAlarm         ;
  bool        Warning           ;
  TDateTime   LastAlarm         ;
  TDateTime   LastAlarmEvent    ;
  TDateTime   LastAlarmMessage  ; // Data_ora dell'ultimo messaggio allarme inviato
  TDateTime   LastReturn        ;
  TDateTime   LastStatus        ;
  TDateTime   RemoteStatusDT    ; // Data_ora relativa allo stato remoto ricevuto
  int         Duration          ;
  int         Frequency         ;
  TList     * AlarmEventFifo    ;
  void      * TreeNode          ;
} XDevice;

typedef struct XSource{
  // Campi di identificazione
  AnsiString            ID                  ;
  AnsiString            Name                ;
  AnsiString            FileName            ;
  char                  Type                ;
  // Campi di configurazione
  bool                  cAlarmEnabled       ;
  bool                  cRemoteStatusCheck  ; // Abilita controllo Data/Ora stato remoto
  int                   cRemoteStatusTimeout; // Specifica il timeout dello stato remoto
  bool                  cMultiSendEnabled   ;
  int                   cSendInterval       ;
  bool                  cSendReturnMsg      ;
  int                   cAlarmGroupID       ;
  bool                  cFAXEnabled         ;
  bool                  cSMSEnabled         ;
  bool                  cPrinterEnabled     ;
  bool                  cSendStartStop      ;
  AnsiString            cSendStart          ;
  AnsiString            cSendStop           ;
  // Campi run-time
  bool                  Active              ;
  int                   Status              ;
  bool                  Alarm               ;
  TDateTime             LastAlarm           ;
  TDateTime             LastAlarmEvent      ;
  TDateTime             LastAlarmMessage    ; // Data_ora ultimo invio di messaggio allarme
  TDateTime             LastReturn          ;
  TDateTime             LastStatus          ;
  TDateTime             RemoteStatusDT      ; // Data_ora dello stato (remoto)
  FILE                * XFile               ;
  XDevice             * Device              ;
  TList               * lDevice             ;
  int                   DeviceNum           ;
  void                * TreeNode            ;
} XSource;

typedef struct XProgram{
  // CAMPI RUNTIME
  int         Status          ; // Stato attuale del server allarmi
  int         StatusNodeIndex ; // Indice assoluto del nodo di stato
  int         AlarmDBNodeIndex; // Indice assoluto del nodo di allarmi
  void      * pStatusNode     ;
	void      * pAlarmDBNode    ;
	/*
	void      * pAlarmCodeNode  ;
	void      * pAlarmSentNode  ;
	void      * pAlarmFailNode  ;
	*/
	void			* pAlarmFAXNode		; // Nodo che contiene i nodi allarme FAX
	void			* pAlarmSMSNode		; // Nodo che contiene i nodi allarme SMS
  // CAMPI DI CONFIGURAZIONE
  AnsiString  ServerID        ; // ID del Server Allarmi
  AnsiString  ServerName      ; // Nome del Server Allarmi
  AnsiString  ServerFAX       ; // Numero FAX del Server Allarmi
  int         ThreadDelay     ; // Durata di un ciclo del Thread di scansione
  bool        AutoStart       ; // Flag di avvio automatico del server
  bool        AutoSave        ; // Salva automaticamente le variazioni di configurazione
  bool        IconEnabled     ; // Flag di abilitazione IconTray
  bool        FAXEnabled      ; // Flag di abilitazione del servizio FAX
  bool        SMSEnabled      ; // Flag di abilitazione del servizio SMS
  bool        EMailEnabled    ; // Flag di abilitazione del servizio EMAIL
  bool        PrinterEnabled  ; // Flag di abilitazione del servizio PRINTER
  bool        AutoExpand      ; // Flag di espansione automatica delle TreeView
  bool        AutoPurge       ; // Flag di abilitazione cancellazione automatica allarmi
  int         PurgeInterval   ; // Soglia di cancellazione di allarmi (minuti)
  bool        EventLog        ; // Flag di abilitazione salvataggio eventi server
  AnsiString  EventLogName    ; // Nome del file di log eventi del server
  XSource   * Source          ; // Puntatore alle strutture delle sorgenti (supervisori)
  TList     * lSource         ;
  int         SourceNum       ; // Numero di sorgenti (supervisori) configurate
  // CAMPI DI INTERFACCIA
  int         MainLeft        ; // Ultima posizione X della finestra principale
  int         MainTop         ; // Ultima posizione Y della finestra principale
  int         MainHeight      ; // Ultima altezza della finestra principale
  int         MainWidth       ; // Ultima larghezza della finestra principale
  // CAMPI DI BACKUP
  int         LastAlarmID     ; // ID dell'ultimo allarme
  int         LastHour        ; // Orario del controllo precedente
  void      * pFaxConf        ; // Puntatore alla struttura di configurazione del Fax
  void      * pComConf        ; // Puntatore alla struttura di configurazione della Com
  void      * pPrinterConf    ; // Puntatore alla struttura di configurazione della Stampante
  void      * pSMSConf        ; //  "            "          sms modem
  void      * pSMSComConf     ; //   "          "           sms COM port
} XProgram;

  bool __fastcall StrToBool(AnsiString Input);
  int __fastcall CharPToInt(char * Input);

XSource   XCreate   (AnsiString SourceName,char Service,TComponent *AOwner);
XSource * XOpen     (XSource * SourceHandler);
int       XClose    (XSource SourceHandler);
int       XRead     (XSource SourceHandler,int BufferSize,char *Buffer);
int       XWrite    (XSource SourceHandler,int BufferSize,char *Buffer);
char    * XGets     (char *Buffer,int BufferSize,XSource SourceHandler);
int       XPuts     (XSource SourceHandler,int BufferSize,char *Buffer);
char    * XChop     (char *Line);
char   ** XExplode  (char *Separator,char *Line,int *Count);
int       XImplode  (char **ExplodedList,int ListCount);
char   ** XDuplicate(char **ExplodedList,int ListCount);
XDevice   XIDDecode (char *Line,char *IDFormat);
XDevice * XDecode   (XDevice *Device,char *Line,char *IDFormat,char *LineFormat,char *Header,AnsiString &sRemoteStatusDT);
char    * XEncode   (XDevice Device,char *IDFormat);
char    * XExtract  (char *Line,int TokenOffset,int TokenLength);
char    * XToken    (char *Separator,char *Line,int Token);
XDevice * XImport   (XSource *Source,int *Count);
XDevice * XDevScan  (XDevice *Device,XSource Source);
XSource * XSupScan  (XSource *Source);
int       XsgView   (XSource *Source,TStringGrid *sgXPort);

//---------------------------------------------------------------------------
#endif

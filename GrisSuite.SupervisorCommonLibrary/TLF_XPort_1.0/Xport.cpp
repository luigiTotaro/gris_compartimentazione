//------------------------------------------------------------------------------
// Telefin Xport Library 1.0
//------------------------------------------------------------------------------
// LIBRERIA ACCESSO A FILE DI ESPORTAZIONE (Xport.cpp)
// Libreria di funzioni per l'accesso alle informazioni contenute all'interno
// dei file di esportazione generati dal server di diagnostica Telefin.
// Questa libreria supporta i file di esportazione nel formato "Supervisor
// Monitor" avanzato (arricchito con formato ID e nomi campi).
//
// Versione:	1.09 (01.01.2003 -> 07.11.2005)
//
// Copyright: 2003-2205 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede Xport.h
//------------------------------------------------------------------------------
// Version history:
// 1.00 [01.01.2003]:
// - Prima versione della libreria di accesso ai file di esportazione.
// 1.01 [01.03.2003]:
// - Migliorata la gestione della memoria per le funzioni di importazione
//   degli stati.
// 1.02 [10.04.2003]:
// - Aggiunti nuovi parametri di configurazione per il server.
// 1.03 [23.04.2003]:
// - Aggiunta funzione di controllo sorgente.
// - Aggiunti gli stati delle sorgenti.
// 1.04 [30.04.2003]:
// - Eliminato il campo cPrinterDestination dalla struttura periferica.
// - Aggiunto il campo LastStatus nella struttura periferica.
// - Aggiunti campi di configurazione per allarme nella struttura sorgente.
// 1.05 [07.05.2003]:
// - Eliminato campo AlarmEventCount dalla struttura periferica.
// - Aggiunto alla strutture periferica e sorgente campo di data_ora ultimo
//   invio di messaggio allarme.
// 1.06 [09.05.2003]:
// - Eliminato campo DefaultStatus e sostituito con AutoStart nella struttura
//   Program.
// - Aggiunti campi EventLog e EventLogFile nella struttura Program.
// 1.07 [25.06.2003]:
// - Aggiunta la funzionalit� di estrazione Data/Ora stato remoto nelle
//   funzioni XDecode e XImport.
// 1.08 [26.06.2003]:
// - Aggiunto il supporto della vecchia versione dei file di esportazione
//   XPort v.1.0 (tipo "Genova") privi di Header formato ID e Header formato
//   riga.
// - Aggiunti i campi cRemoteStatusCheck e cRemoteStatusTimeout per il
//   controllo di data e ora del file di esportazione.
// 1.09 [07.11.2005]:
// - Modificata la definizione del tipo XDevice,XProgram,XSource
//------------------------------------------------------------------------------
#pragma hdrstop
#include "XPort.h"
#include <string.h>
#include <stdlib.h>
#include <DateUtils.hpp>
#include <share.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
//static TUdpSocket *Socket;
//  static TIdIPWatch *XIP;


bool __fastcall StrToBool(AnsiString Input)
{
  bool Output = false;

  if (Input.LowerCase() == "true") Output = true;

  return Output;
}

int __fastcall CharPToInt(char * Input)
{
  int Output = NULL;
  int Length = strlen(Input);
  bool Numeric = true;

  if (Length)
  {
    for (int i=0;i<Length;i++)
    {
      if (!isdigit(Input[i])) Numeric = false;
    }

    if (Numeric)
    {
      Output = StrToInt(AnsiString(Input));
    }
  }

  return Output;
}


XSource XCreate(AnsiString SourceName,char Service,TComponent *AOwner)
{
  XSource SourceHandler;
  int ErrorCode = 0;
	// NEW //TIdIPWatch *XIP;


  switch (Service)
  {
    case 'F': // Servizio Filesystem
      SourceHandler.Type     = Service;
      SourceHandler.FileName = SourceName;
      SourceHandler.Status   = 1; // @@@

      // NEW //SourceHandler.XSocket  = NULL;
		break;
    case 'S': // Servizio Socket
      SourceHandler.Type       = Service;
      SourceHandler.Name       = SourceName;
      SourceHandler.Status     = 0; // @@@

      if (SourceHandler.XFile != NULL)
      {
        delete SourceHandler.XFile;
        SourceHandler.XFile  = NULL;
      }

      // NEW //XIP = new TIdIPWatch(AOwner);
      // NEW //XIP->Active = true;

      // NEW //SourceHandler.XSocket    = new TUdpSocket(AOwner);
			// NEW //SourceHandler.XSocket->LocalHost = XIP->CurrentIP;
      // NEW //SourceHandler.XSocket->LocalPort = 4444;
      // NEW //SourceHandler.XSocket->RemoteHost = SourceHandler.Name;
      // NEW //SourceHandler.XSocket->RemotePort = "4444";
      // NEW //SourceHandler.XSocket->BlockMode = bmBlocking;
      // NEW //SourceHandler.XSocket->Open();



      // NEW //delete XIP;

    break;
    default:
      SourceHandler.Type     = NULL;
      SourceHandler.Name     = NULL;
      SourceHandler.Status   = NULL; // @@@
  }

  return SourceHandler;
}

XSource * XOpen(XSource *SourceHandler)
{
  int ErrorCode = 0;
  // NEW //TIdIPWatch *XIP;


  switch (SourceHandler->Type)
  {
    case 'F': // Servizio Filesystem
      //SourceHandler->Name     = SourceName;
      ////SourceHandler->Status   = 1; // @@@
      SourceHandler->Status = AS_STAT_SUP_UNKNOWN;

      SourceHandler->XFile    = _fsopen( SourceHandler->FileName.c_str(), "r", SH_DENYNO );
      if (SourceHandler->XFile == NULL)
      {
        int ERRORE = 1;
        // Se entra qui significa che il file non � stato aperto
      }

      //SourceHandler->XSocket  = NULL;
    break;
    case 'S': // Servizio Socket
      //SourceHandler->Name       = SourceName;
      SourceHandler->Status     = 0; // @@@

      if (SourceHandler->XFile != NULL)
      {
        delete SourceHandler->XFile;
        SourceHandler->XFile  = NULL;
      }

      //XIP = new TIdIPWatch(AOwner);
      //XIP->Active = true;

      /*
      SourceHandler.XSocket    = new TUdpSocket(AOwner);
      SourceHandler.XSocket->LocalHost = XIP->CurrentIP;
      SourceHandler.XSocket->LocalPort = 4444;
      SourceHandler.XSocket->RemoteHost = SourceHandler.Name;
      SourceHandler.XSocket->RemotePort = "4444";
      SourceHandler.XSocket->BlockMode = bmBlocking;
      SourceHandler.XSocket->Open();
      */


      //delete XIP;

    break;
    default:
      SourceHandler->Name     = NULL;
      SourceHandler->Status   = NULL; // @@@
  }

  return SourceHandler;
}


int XClose( XSource SourceHandler )
{
	int ErrorCode = 0;

	switch ( SourceHandler.Type )
	{
		case 'F': // Servizio File
			SourceHandler.Status   = 0; // @@@
			fclose( SourceHandler.XFile );
			// NEW //SourceHandler.XSocket  = NULL;
		break;
		case 'S': // Servizio Socket
			;
		break;
		default: ;
	}

	return ErrorCode;
}

int XRead(XSource SourceHandler,int BufferSize,char *Buffer)
{
  int ErrorCode;
	int errors        = 0;
	int warnings      = 0;
	int disabled      = 0;
	int nodownload    = 0;
  int offline       = 0;
	char * disabled_msg = "FUORI SERVIZIO";
  bool FileFinito = false;
  char * riga_log;


  switch (SourceHandler.Type)
  {
    case 'F': // Servizio File
      // @@@ Da portare in XRead
      riga_log = new char[X_MAX_CHAR];
      riga_log = fgets(riga_log, X_MAX_CHAR, SourceHandler.XFile);
    break;
    case 'S': // Servizio Socket
    ;
    break;
    default: ;
  }
  return ErrorCode;
}

int XWrite(XSource SourceHandler,int BufferSize,char *Buffer)
{
  int ErrorCode;

  return ErrorCode;
}



char * XGets(char *Buffer,int BufferSize,XSource SourceHandler)
{
  int ErrorCode;

  char * Line;
  char * a_Line;

  switch (SourceHandler.Type)
  {
    case 'F': // Servizio File
      Line = (char *) malloc(X_MAX_CHAR+1);
      a_Line = fgets(Line, X_MAX_CHAR, SourceHandler.XFile);
      if (a_Line == NULL)
      {
        free (Line);
        Line = NULL;
      }

    break;
    case 'S': // Servizio Socket
    ;
    break;
    default: ;
  }

  //Buffer = Line;

  return Line;
}


char * XChop (char *Line)
{
  int len;
  int EOLCount = 0;
  int CharCount = 0;
  char * a_Line;

  if (Line != NULL)
  {
    len = strlen(Line);

    for (int i=0;i<(len);i++)
    {
      if (Line[i] == '\n') EOLCount++;
    }
  }
  

  if (EOLCount)
  {
    a_Line = (char *) malloc (len-EOLCount+1);

    for (int i=0;i<(len);i++)
    {
      if (Line[i] != '\n')
      {
        a_Line[CharCount] = Line[i];
        CharCount++;
      }
    }

    a_Line[CharCount] = NULL;

    free(Line);
    Line = a_Line;
  }

  return Line;
}


char ** XExplode(char *Separator,char *oLine,int *Count)
{
  char **Table;
  char *Line;
  char *LineToFree;
  char *Token;
  int TokenCount = 0;
  int Length;

  Length = strlen(oLine)+1;
  LineToFree = Line = (char *) malloc(Length);
  strcpy(Line,oLine);
  Table = (char **) malloc(X_MAX_LINE*sizeof(char *));

  while ( ( Token = strtok( Line, Separator ) ) != NULL )
  {
    Line = NULL;
    // Cerco il primo/prossimo token
    Table[TokenCount] = (char *) malloc(StrLen(Token)+1);
    // Copio il token trovato
    strcpy( Table[TokenCount], Token );
    // Incremento il contatore dei token
    TokenCount++;
    //Line = Token + StrLen(Token) + 1;
  }
  Count[0] = TokenCount;

  free(LineToFree);

  return Table;
}

int XImplode(char **ExplodedList,int ListCount)
{
  int ErrorCode = 0;

  if (ExplodedList == NULL)
  {
    ErrorCode = 1; // Impossibile liberare la memoria della lista
  }
  else
  {
    for (int f=0;f<ListCount;f++)
    {
      if (ExplodedList[f] == NULL)
      {
        ErrorCode = 2; // Impossibile liberare la memoria di alcuni elementi
      }
      else
      {
        free(ExplodedList[f]);
      }
    }
    free(ExplodedList);
  }

  return ErrorCode;
}

char ** XDuplicate(char **ExplodedList,int ListCount)
{
  int StringLength;
  char ** DuplicatedList;
  char *  DuplicatedString;
  char *  ExplodedString;

  if (ExplodedList != NULL)
  {
    DuplicatedList = (char **) malloc(ListCount*sizeof(char *));
    for (int l=0;l<ListCount;l++)
    {
      ExplodedString = ExplodedList[l];
      StringLength = strlen(ExplodedString)+1;
      DuplicatedString = (char *) malloc(StringLength);
      strcpy(DuplicatedString,ExplodedString);
      DuplicatedList[l]=DuplicatedString;
    }
  }
  else
  {
    DuplicatedList = NULL;
  }

  return DuplicatedList;
}


XDevice XIDDecode(char *Line,char *IDF)
{
  XDevice Device;
  char ** Formati;
  char ** Lunghezze;
  char *IDFormat;
  int IDNum;
  int LNum;
  int IDOffset = 0;
  int IDLength;

  if (IDF == NULL)
  {
    // Prendo il formato ID di default
    IDFormat = X_DEF_ID_FORMAT;
  }
  else
  {
    IDFormat = IDF;
  }

  Formati = XExplode(";",IDFormat,&IDNum);
  for (int i=0;i<IDNum;i++)
  {
    Lunghezze = XPlode(":",Formati[i],&LNum);
    IDLength =  StrToInt(AnsiString(Lunghezze[1]));
    if (AnsiString(Lunghezze[0]) == "SupID")
    {
      Device.SupID = XTract(Line,IDOffset,IDLength);
    }
    if (AnsiString(Lunghezze[0]) == "DirID")
    {
      Device.DirID = AnsiString(XTract(Line,IDOffset,IDLength));
    }
    if (AnsiString(Lunghezze[0]) == "NodID")
    {
      Device.NodID = AnsiString(XTract(Line,IDOffset,IDLength));
    }
    if (AnsiString(Lunghezze[0]) == "DevID")
    {
      Device.DevID = AnsiString(XTract(Line,IDOffset,IDLength));
    }
    IDOffset += IDLength;
    // CANCELLAZIONE MEMORIA
    XImplode(Lunghezze,LNum);
  }

  // CANCELLAZIONE MEMORIA
  XImplode(Formati,IDNum);

  return Device;
}




XDevice * XDecode(XDevice *Device,char *Line,char *IDFormat,char *LineFormat,char *Header,AnsiString &sRemoteStatusDT)
//---------------------------------------------------------------------------
// [25.06.2003]
//---------------------------------------------------------------------------
{
  char ** Fields            ; // Lista dei campi
  int     FieldsNum         ; // Numero di campi
  char ** HeaderList        ; // Lista campi dell'header
  int     HeaderNum         ; // Numero campi dell'header
  char ** NameList          ; // Lista delle assegnazioni nomi
  int     NameNum           ; // Numero di assegnazioni nomi
  char ** DirList           ; // Nomi delle direttrici
  int     DirNum            ; // Numero di direttrici
  char ** NodList           ; // Nomi dei nodi
  int     NodNum            ; // Numero di nodi
  char ** FieldsPosition    ; // Posizione dei campi
  int     FieldsPositionNum ; // Numero di posizioni di campi
  char  * IDLine            ; // Riga contenente l'ID
  char ** IDCouples         ; // Lista di coppie ID:lunghezza
  char ** IDValues          ; // Coppia ID,lunghezza
  int     IDNum             ; // Numero di coppie ID:lunghezza
  int     IDOffset          ; // Offset dell'ID
  int     IDLength          ; // Lunghezza dell'ID
  int     LNum              ; // Numero di campi di lunghezza
  char  * a_ID              ; // Buffer temporaneo per ID

  /* RESET DEVICE */
  Device->SupID = "N/A";
  Device->SupName = "Sconosciuto";
  Device->DirID = "N/A";
  Device->DirName = "Sconosciuto";
  Device->NodID = "N/A";
  Device->NodName = "Sconosciuto";
  Device->DevID = "N/A";
  Device->DevName = "Sconosciuto";
  Device->Registered = false;

  /* ESTRAZIONE DEI CAMPI */
  Fields = XPlode("|",Line,&FieldsNum);

  /* ESTRAZIONE CAMPI HEADER */
  HeaderList = XPlode("|",Header,&HeaderNum);
  Device->SupName = HeaderList[0];
  // Estraggo la data_ora stato remota
  sRemoteStatusDT = AnsiString(HeaderList[3]);

  /* DECODIFICA ID */
  // Caso assenza Formato ID (XPort v.1.0 tipo "Genova")
  if (IDFormat == NULL)
  {
    //IDFormat = X_DEF_ID_FORMAT; // Prendo il formato ID di default
    ////Device->SupID = a_ID = XTract(Fields[0],0,IDLength);free(a_ID);
    Device->DirID = a_ID = XTract(Fields[0],0,2);free(a_ID);
    Device->NodID = a_ID = XTract(Fields[0],2,3);free(a_ID);
    Device->DevID = a_ID = XTract(Fields[0],5,2);free(a_ID);
  }
  else
  {
    IDLine = Fields[0]; // Il codice � sempre il primo campo
    IDCouples = XPlode(";",IDFormat,&IDNum);
    IDOffset = 0;
    for (int i=0;i<IDNum;i++)
    {
      IDValues = XPlode(":",IDCouples[i],&LNum);
      IDLength =  StrToInt(AnsiString(IDValues[1])); // Estraggo la lunghezza dell'ID
      if (AnsiString(IDValues[0]) == "SupID") Device->SupID = a_ID = XTract(IDLine,IDOffset,IDLength);
      if (AnsiString(IDValues[0]) == "DirID") Device->DirID = a_ID = XTract(IDLine,IDOffset,IDLength);
      if (AnsiString(IDValues[0]) == "NodID") Device->NodID = a_ID = XTract(IDLine,IDOffset,IDLength);
      if (AnsiString(IDValues[0]) == "DevID") Device->DevID = a_ID = XTract(IDLine,IDOffset,IDLength);
      IDOffset += IDLength;
      // Cancellazione memoria
      XImplode(IDValues,LNum);
      free(a_ID);
    }
    XImplode(IDCouples,IDNum);
  }

  /* DECODIFICA CAMPI */
  // Caso assenza LineFormat (XPort v.1.0 tipo "Genova")
  if (LineFormat == NULL)
  {
    Device->StatusLevel   = CharPToInt(Fields[1]);
    Device->DevName       = Fields[2];
    Device->Status        = Fields[3];
  }
  else
  {
    FieldsPosition = XPlode("|",LineFormat,&FieldsPositionNum);
    for (int i=1;i<FieldsNum;i++)
    {
      if (AnsiString(FieldsPosition[i]) == "StatusLevel") Device->StatusLevel  = CharPToInt(Fields[i]);
      if (AnsiString(FieldsPosition[i]) == "DevName")     Device->DevName      = Fields[i];
      if (AnsiString(FieldsPosition[i]) == "Status")      Device->Status       = Fields[i];
    }
    // Cancello la memoria
    XImplode(FieldsPosition,FieldsPositionNum);
  }

  /* ESTRAZIONE NOMI DIRETTRICI */
  NameList = XPlode(";",HeaderList[1],&NameNum);
  for (int i=0;i<NameNum;i++)
  {
    DirList = XPlode(":",NameList[i],&DirNum);
    if (AnsiString(DirList[1]) == Device->DirID) Device->DirName = DirList[0];
    XImplode(DirList,DirNum);
  }
  XImplode(NameList,NameNum);

  /* ESTRAZIONE NOMI NODI */
  NameList = XPlode(";",HeaderList[2],&NameNum);
  for (int i=0;i<NameNum;i++)
  {
    NodList = XPlode(":",NameList[i],&NodNum);
    if (AnsiString(NodList[1]) == Device->NodID) Device->NodName = NodList[0];
    XImplode(NodList,NodNum);
  }
  XImplode(NameList,NameNum);

  // CANCELLAZIONE MEMORIA
  XImplode(Fields,FieldsNum);
  XImplode(HeaderList,HeaderNum);

  return Device;
}

char * XEncode(XDevice Device,char *IDFormat)
{
  char * Line;

  return Line;
}

char * XExtract(char *Line,int TokenOffset,int TokenLength)
{
  char * Token;

  Token = (char *) malloc(TokenLength+1);
  for (int i=0;i<TokenLength;i++)
  {
    Token[i] = Line[TokenOffset+i];
  }
  Token[TokenLength] = NULL;

  return Token;
}

char * XToken(char *Separator,char *Line,int Token)
{
  char * OutToken;
  int Count;
  int Length;

  char ** Tokens = XExplode(Separator,Line,&Count);

  if (Token > Count)
  {
    OutToken = NULL;
  }
  else
  {
    Length = strlen(Tokens[Token])+1;
    OutToken = (char *) malloc(Length);
    strcpy(OutToken,Tokens[Token]);
  }

  XImplode(Tokens,Count);

  return OutToken;
}

XDevice * XImport( XSource * Source, int * Count )
{
    // 0. VARIABILI TEMPORANEE
    XDevice       * Device              ; // Lista di strutture periferiche
    int             DeviceNum           ; // Numero di periferiche
    int             DeviceCount         ; // Contatore periferiche
    char          * Header              ; // Header Principale
    //char       ** HeaderList          ; // Lista elementi dell'header
    int             HeaderNum           ; // Numero di elementi dell'header
    char          * LineList[X_MAX_LINE]; // Lista delle righe del file di esportazione
    char          * Line                ; // Riga del file di esportazione
    int             LineNum             ; // Numero di righe lette
    char         ** LineToken           ; // Token della riga del file di esportazione
    int             LineTokenNum        ; // Numero di token della riga del file di esportazione
    char         ** TokenFormato        ; // Token dell'header dei campi
    int             TokenFormatoNum     ; // Numero di token dell'header dei campi
    bool            H96,H97,H98,H99     ; // Indicatori della presenza Header 9x
    int             HNum                ; // Numero di righe header secondario o controllo
    char          * Header96            ; // Header secondario 9999996
    char          * Header97            ; // Header secondario 9999997
    char          * DefHeader96 = X_DEF_ID_FORMAT   ; // Header secondario 9999996 di dafault (usato se assente)
    char          * DefHeader97 = X_DEF_LINE_FORMAT ; // Header secondario 9999997 di dafault (usato se assente)
    AnsiString      sRemoteStatusDT     ; // Stringa Data_Ora stato remoto
    XDevice         Dev                 ; // Struttura periferica

    // LETTURA HEADER PRINCIPALE
    Header        = XChop( XGets( Header, X_MAX_CHAR, *Source ) );

    //HeaderList  = XExplode("|",Header,&HeaderNum);

    // LETTURA DELLE RIGHE DEL FILE DI ESPORTAZIONE
    LineNum = 0; // Azzero il contatore delle righe
    Line = (char *) 1;
    while ( Line != NULL )
    {
        Line = XChop( XGets( Line, X_MAX_CHAR, *Source ) );
        if ( Line != NULL )
        {
            LineList[LineNum] = Line;
            LineNum++; // Incremento il contatore delle righe
        }
        else
        {
			free( Line );
		}
	}

    // RICERCA DELLE RIGHE DI HEADER SECONDARI O DI CONTROLLO
    H96 = H97 = H98 = H99 = false;
    HNum = 0; //Azzero il contatore di Header secondari o controllo;
    for ( int l = 0; l < LineNum; l++ )
    {
        Line      = LineList[l];
        LineToken = XExplode("|",Line,&LineTokenNum);
        if ( Line[0] == '9' ) HNum++;
        if ( AnsiString( LineToken[0] ) == "9999996" )
        {
			H96 = true;
			Header96 = Line;
		}
		if ( AnsiString( LineToken[0] ) == "9999997" )
		{
			H97 = true;
			Header97 = Line;
			TokenFormato = XDuplicate( LineToken, LineTokenNum );
		}
		if ( AnsiString( LineToken[0] ) == "9999998" )
		{
			H98 = true;
			Source->Status = AS_STAT_SUP_OFFLINE;
		}
		if ( AnsiString( LineToken[0] ) == "9999999" )
		{
			H99 = true;
		}
		XImplode( LineToken, LineTokenNum );
	}
	// ALLOCAZIONE LISTA STRUTTURE PERIFERICA
	DeviceNum = LineNum - HNum;
	Device = new XDevice[DeviceNum];

	/*
  // In caso di assenza di Header 9999996 es: File XPort v.1.0 (tipo "Genova")
  if (!H96)
  {
    Header96  = (char *) malloc (X_MAX_CHAR+1);
    strcpy(Header96,DefHeader96);
  }

  // In caso di assenza di Header 9999997 es: File XPort v.1.0 (tipo "Genova")
  if (!H97)
  {
    Header97  = (char *) malloc (X_MAX_CHAR+1);
    strcpy(Header97,DefHeader97);
    TokenFormato = XExplode("|",Header97,&LineTokenNum);
  }
  //*/

  // ELABORAZIONE DELLE RIGHE DEL FILE DI ESPORTAZIONE
  DeviceCount = 0;
  sRemoteStatusDT = "";
  for (int l=0;l<LineNum;l++)
  {
    Line      = LineList[l]; // Recupero la righa l-esima
    //LineToken = XExplode("|",LineList[l],&LineTokenNum);
    // Forse questa riga non serve

    if (Line[0] != '9') // Controllo che non sia una riga di Header secondario o di controllo
    {
      if (H96 && H97)
      {
        XDecode(&Device[DeviceCount],Line,TokenFormato[3],Header96,Header,sRemoteStatusDT);
      }
      else
      {
        XDecode(&Device[DeviceCount],Line,NULL,NULL,Header,sRemoteStatusDT);
      }
      if (Device[DeviceCount].SupID == "N/A") Device[DeviceCount].SupID = Source->ID;
      Device[DeviceCount].ID = Device[DeviceCount].SupID + "." +
                               Device[DeviceCount].DirID + "." +
                               Device[DeviceCount].NodID + "." +
                               Device[DeviceCount].DevID;
      DeviceCount++;
    }
  }

  if (sRemoteStatusDT != "")
  {
    Source->RemoteStatusDT = StrToDateTime(sRemoteStatusDT);
    // Controllo timeout offline
    if (Source->cRemoteStatusCheck)
    {
      int RemoteStatusInterval;
      TDateTime NowDT = Now();
      RemoteStatusInterval = (int) MinutesBetween(NowDT,Source->RemoteStatusDT);
      if (RemoteStatusInterval > Source->cRemoteStatusTimeout)
      {
        Source->Status = AS_STAT_SUP_OFFLINE;
      }
    }
  }
  else
  {
    Source->RemoteStatusDT = (TDateTime) 0;
  }

  // CANCELLAZIONE MEMORIA
  free(Header);
  if (H97)
  {
    XImplode(TokenFormato,LineTokenNum);
  }
  for (int f=0;f<LineNum;f++)
  {
    Line = LineList[f];
    free(Line);
  }
  *Count = DeviceNum;

  /*
  if (!H96)
  {
    free (Header96);
  }
  if (!H97)
  {
    free (Header97);
  }
  //*/

  return Device;
}

XDevice * XDevScan (XDevice *Device,XSource Source)
{

}


XSource * XSupScan (XSource *Source)
{
  XDevice * Device;             // Lista di strutture periferiche
  int   DeviceNum;              // Numero di periferiche
  int   DeviceCount;            // Contatore periferiche
  char  *Header;                // Header Principale
  //char  **HeaderList;           // Lista elementi dell'header
  int   HeaderNum;              // Numero di elementi dell'header
  char  *LineList[X_MAX_LINE];  // Lista delle righe del file di esportazione
  char  *Line;                  // Riga del file di esportazione
  int   LineNum;                // Numero di righe lette
  char  **LineToken;            // Token della riga del file di esportazione
  int   LineTokenNum;           // Numero di token della riga del file di esportazione
  char  **TokenFormato;         // Token dell'header dei campi
  int   TokenFormatoNum;        // Numero di token dell'header dei campi
  bool  H96,H97,H98,H99;        // Indicatori della presenza Header 9x
  int   HNum;                   // Numero di righe header secondario o controllo
  char  *Header96;              // Header secondario 9999996
  char  *Header97;              // Header secondario 9999997

  XDevice Dev;

  // LETTURA HEADER PRINCIPALE
  Header      = XChop(XGets(Header,X_MAX_CHAR,*Source));
  //HeaderList  = XExplode("|",Header,&HeaderNum);

  // LETTURA DELLE RIGHE DEL FILE DI ESPORTAZIONE
  LineNum = 0; // Azzero il contatore delle righe
  Line = (char *) 1;
  while (Line != NULL)
  {
    Line = XChop(XGets(Line,X_MAX_CHAR,*Source));
    if (Line != NULL)
    {
      LineList[LineNum] = Line;
      LineNum++; // Incremento il contatore delle righe
    }
    else
    {
       free(Line);
    }
  }

  Source->Status = AS_STAT_SUP_OK;

  // RICERCA DELLE RIGHE DI HEADER SECONDARI O DI CONTROLLO
  H96 = H97 = H98 = H99 = false;
  HNum = 0; //Azzero il contatore di Header secondari o controllo;
  for (int l=0;l<LineNum;l++)
  {
    Line      = LineList[l];
    LineToken = XExplode("|",Line,&LineTokenNum);
    if (Line[0] == '9') HNum++;
    if (AnsiString(LineToken[0]) == "9999996")
    {
      H96 = true;
      Header96 = Line;
    }
    if (AnsiString(LineToken[0]) == "9999997")
    {
      H97 = true;
      Header97 = Line;
      TokenFormato = XDuplicate(LineToken,LineTokenNum);
    }
    if (AnsiString(LineToken[0]) == "9999998")
    {
      H98 = true;
      Source->Status = AS_STAT_SUP_OFFLINE;
    }
    if (AnsiString(LineToken[0]) == "9999999")
    {
      H99 = true;
    }
    XImplode(LineToken,LineTokenNum);
  }

  // CANCELLAZIONE MEMORIA
  free(Header);
  XImplode(TokenFormato,LineTokenNum);
  for (int f=0;f<LineNum;f++)
  {
    Line = LineList[f];
    free(Line);
  }
  //*Count = DeviceNum;
  return Source;
}

int XsgView(XSource *Source,TStringGrid *sgXPort)
{
  int ErrorCode;

  for (int d=0;d<Source->DeviceNum;d++)
  {
    sgXPort->RowCount = d+2;
    sgXPort->Cells[0][d+1] = Source->Device[d].SupID + "." + Source->Device[d].DirID + "." + Source->Device[d].NodID + "." + Source->Device[d].DevID;
    sgXPort->Cells[1][d+1] = "[" + Source->Device[d].SupID + "] " + Source->Device[d].SupName;
    sgXPort->Cells[2][d+1] = "[" + Source->Device[d].DirID + "] " + Source->Device[d].DirName;
    sgXPort->Cells[3][d+1] = "[" + Source->Device[d].NodID + "] " + Source->Device[d].NodName;
    sgXPort->Cells[4][d+1] = "[" + Source->Device[d].DevID + "] " + Source->Device[d].DevName;
    sgXPort->Cells[5][d+1] = Source->Device[d].StatusLevel;
    sgXPort->Cells[6][d+1] = Source->Device[d].Status;
  }

  return ErrorCode;
}


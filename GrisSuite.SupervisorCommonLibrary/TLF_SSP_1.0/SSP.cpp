//==============================================================================
// Telefin SSP Protocol Module 1.0
//------------------------------------------------------------------------------
// MODULO PROTOCOLLO SSP (SSP.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.07 (10.02.2006 -> 09.05.2006)
//
// Copyright: 2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SSP.h
//------------------------------------------------------------------------------
// Version history: vedere in SSP.h
//==============================================================================

#pragma hdrstop
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "SSP.h"
#include <SysUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
// Funzione per allocare un nuovo messaggio SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SSP_MESSAGE * SSPNewMessage( void )
{
  SSP_MESSAGE           * message   = NULL;

  message = (SSP_MESSAGE*)malloc(sizeof(SSP_MESSAGE));
  if ( message != NULL )
  {
    try
    {
      memset( message, 0, sizeof( SSP_MESSAGE ) );
      message->VCC = SSP_MESSAGE_VALIDITY_CHECK_CODE;
    }
    catch(...)
    {
      message = NULL;
    }
  }

  return message;
}

//==============================================================================
/// Funzione per eliminare un messaggio SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SSPDeleteMessage( SSP_MESSAGE * message )
{
  int ret_code = SSP_NO_ERROR;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      memset( message, 0, sizeof( SSP_MESSAGE ) );
      free( message );
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }

  return ret_code;
}

//==============================================================================
// Funzione per verificare la validita' di una struttura messaggio SSP
///
/// \date [31.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SSPValidMessage( SSP_MESSAGE * message )
{
  bool valid = false;

  if ( message != NULL )
  {
    try
    {
      if ( message->VCC == SSP_MESSAGE_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per copiare una struttura messaggio SSP
///
/// \date [09.05.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SSP_MESSAGE * SSPCopyMessage( SSP_MESSAGE * message )
{
  SSP_MESSAGE * mcopy = NULL;

  try
  {
    if ( SSPValidMessage( message ) )
    {
      mcopy = SSPNewMessage( ); // -MALLOC
      if ( SSPValidMessage( mcopy ) )
      {
        // --- Copio la memoria occupata dalla struttura messaggio ---
        memcpy( (void*)mcopy, (void*)message, sizeof(SSP_MESSAGE) );
      }
    }
  }
  catch(...)
  {
    mcopy = NULL;
  }

  return mcopy;
}

//==============================================================================
/// Funzione per recuperare una struttura di definizione tipo messaggio
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SSP_MESSAGE_TYPE_DEF * SSPGetMessageTypeDef( unsigned __int8 msg_type )
{
  SSP_MESSAGE_TYPE_DEF  * type_def                = NULL;
  int                     SSPMessageTypeDefCount  = sizeof(SSPMessageTypeDefList) / sizeof(SSP_MESSAGE_TYPE_DEF);

  for ( int d = 0; d < SSPMessageTypeDefCount; d++ )
  {
    type_def = &SSPMessageTypeDefList[d];
    try
    {
      if ( msg_type == type_def->MsgType )
      {
        break;
      }
      else
      {
        type_def = NULL;
      }
    }
    catch(...)
    {
      type_def = NULL;
    }
  }

  return type_def;
}

//==============================================================================
/// Funzione per settare i parametri di un messaggio SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SSPSetMessage( SSP_MESSAGE * message, SSP_HEADER header, SSP_DATABLOCK * datablock_list )
{
  int ret_code = SSP_NO_ERROR;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      ret_code = SSPSetMessageHeader( message, header );
      if ( ret_code != SSP_NO_ERROR )
      {
        for ( int d = 0; d < message->Header.DataNum; d++ )
        {
          ret_code = SSPSetMessageDataBlock( message, d, datablock_list[d] );
        }
      }
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_MESSAGE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per costruire il buffer di un messaggio SSP
///
/// \date [05.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SSPBuildMessage( SSP_MESSAGE * message )
{
  int ret_code = SSP_NO_ERROR;
  int data_num = 0;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      // --- Costruisco l'header ---
      SSPBuildMessageHeader( &message->Header );
      // --- Costruisco i datablock ---
      data_num = message->Header.DataNum;
      for ( int d = 0; d < data_num; d++ )
      {
        SSPBuildDataBlock( &message->DataBlockList[d] );
      }

      message->BufferLen = 0;
      // --- Copio l'Header ---
      memcpy( &message->Buffer[message->BufferLen], &message->Header.Buffer[0], message->Header.BufferLen );
      message->BufferLen += message->Header.BufferLen;
      // --- Copio i DataBlock ---
      for ( int d = 0; d < data_num; d++ )
      {
        memcpy( &message->Buffer[message->BufferLen], &message->DataBlockList[d].Buffer[0], message->DataBlockList[d].BufferLen );
        message->BufferLen += message->DataBlockList[d].BufferLen;
      }
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_MESSAGE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per settare i parametri di un messaggio SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SSPSetMessageHeader( SSP_MESSAGE * message, SSP_HEADER header )
{
  int ret_code = SSP_NO_ERROR;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      message->Header = header;
      message->Header.VCC = SSP_HEADER_VALIDITY_CHECK_CODE;
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una struttura header SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SSPValidHeader( SSP_HEADER * header )
{
  bool valid = false;

  if ( header != NULL )
  {
    try
    {
      if ( header->VCC == SSP_HEADER_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per costruire il buffer di un header SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SSPBuildMessageHeader( SSP_HEADER * header )
{
  int ret_code = SSP_NO_ERROR;

  if ( SSPValidHeader( header ) )
  {
    try
    {
      header->BufferLen = 0;
      // --- Copio MsgType ---
      memcpy( &header->Buffer[header->BufferLen], &header->MsgType, sizeof(unsigned __int8) );
      header->BufferLen += sizeof(unsigned __int8);
      // --- Copio UniID ---
      for ( int i = 0; i < header->UniIDLen; i++ )
      {
        memcpy( &header->Buffer[header->BufferLen], &header->UniID[i], sizeof(unsigned __int16) );
        header->BufferLen += sizeof(unsigned __int16);
      }
      // --- Copio DataNum ---
      memcpy( &header->Buffer[header->BufferLen], &header->DataNum, sizeof(unsigned __int8) );
      header->BufferLen += sizeof(unsigned __int8);
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_HEADER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per settare un datablock in un messaggio SSP
///
/// \date [27.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SSPSetMessageDataBlock( SSP_MESSAGE * message, int num, SSP_DATABLOCK datablock )
{
  int ret_code = SSP_NO_ERROR;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      if ( num < message->Header.DataNum )
      {
        message->DataBlockList[num] = datablock;
        message->DataBlockList[num].VCC = SSP_DATABLOCK_VALIDITY_CHECK_CODE;
      }
      else
      {
        ret_code = SSP_INVALID_DATABLOCK_NUM;
      }
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_MESSAGE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di un datablock SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SSPValidDataBlock( SSP_DATABLOCK * datablock )
{
  bool valid = false;

  if ( datablock != NULL )
  {
    try
    {
      if ( datablock->VCC == SSP_DATABLOCK_VALIDITY_CHECK_CODE )
      {
        valid = true;
      }
    }
    catch(...)
    {
      valid = false;
    }
  }

  return valid;
}

//==============================================================================
/// Funzione per costruire il buffer di un datablock SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SSPBuildDataBlock( SSP_DATABLOCK * datablock )
{
  int ret_code = SSP_NO_ERROR;

  if ( SSPValidDataBlock( datablock ) )
  {
    try
    {
      datablock->BufferLen = 0;
      // --- Copio DataID ---
      memcpy( &datablock->Buffer[datablock->BufferLen], &datablock->DataID, sizeof(unsigned __int16) );
      datablock->BufferLen += sizeof(unsigned __int16);
      // --- Copio DataLen ---
      memcpy( &datablock->Buffer[datablock->BufferLen], &datablock->DataLen, sizeof(unsigned __int16) );
      datablock->BufferLen += sizeof(unsigned __int16);
      // --- Copio Data ---
      memcpy( &datablock->Buffer[datablock->BufferLen], &datablock->Data[0], sizeof(unsigned char)*datablock->DataLen );
      datablock->BufferLen += sizeof(unsigned char)*datablock->DataLen;
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_DATABLOCK;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per estrarre un messaggio SSP da un buffer
///
/// \date [03.05.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SSPExtractMessage( SSP_MESSAGE * message, SSP_MESSAGE * res_message )
{
  int                     ret_code      = SSP_NO_ERROR;
  int                     offset        = 0;
  unsigned __int8         data_num      = 0;
  int                     res_len       = 0;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      if ( message->BufferLen > 0 )
      {
        ret_code = SSPExtractHeader( message, &offset );
        if ( ret_code == SSP_NO_ERROR )
        {
          data_num = message->Header.DataNum;
          for ( int d = 0; d < data_num; d++ )
          {
            ret_code = SSPExtractDataBlock( message, d, &offset );
            if ( ret_code != SSP_NO_ERROR )
            {
              break;
            }
            else
            {
            }
          }
          // --- Se ho estratto correttamente il messaggio ---
          if ( ret_code == SSP_NO_ERROR )
          {
            res_len = message->BufferLen - offset;
            // --- Se avanzano byte nel buffer ---
            if ( res_len > 0 )
            {
              // --- Se si richiede di salvare il residuo ---
              if ( SSPValidMessage( res_message ) )
              {
                memcpy( &res_message->Buffer[0], &message->Buffer[offset], res_len );
                res_message->BufferLen = res_len;
              }
              // --- Tronco il buffer togliendo il residuo ---
              memset( &message->Buffer[offset], 0, SSP_DATABLOCK_BUFFER_MAX_LEN - offset );
              message->BufferLen = offset;
            }
          }
        }
      }
      else
      {
        ret_code = SSP_INVALID_MESSAGE_BUFFER;
      }
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_MESSAGE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per estrarre un header SSP da un buffer
///
/// \date [02.05.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SSPExtractHeader( SSP_MESSAGE * message, int * poffset )
{
  int                     ret_code      = SSP_NO_ERROR;
  int                     offset        = 0;
  int                     header_len    = 0;
  unsigned __int8         msg_type      = 0;
  SSP_MESSAGE_TYPE_DEF  * msg_type_def  = NULL;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      if ( poffset != NULL ) offset = *poffset;
      if ( message->BufferLen > offset )
      {
        msg_type = message->Buffer[offset];
        memcpy( &msg_type, &message->Buffer[offset], sizeof(unsigned __int8) );
        msg_type_def = SSPGetMessageTypeDef( msg_type );
        if ( msg_type_def != NULL )
        {
          // --- Estraggo MsgType ---
          message->Header.MsgType = msg_type;
          offset += sizeof(unsigned __int8);
          // --- Estraggo UniID ---
          message->Header.UniIDLen = msg_type_def->UniIDLen;
          if ( message->BufferLen >= offset + ( sizeof(unsigned __int16) * msg_type_def->UniIDLen ) )
          {
            memcpy( &message->Header.UniID[0], &message->Buffer[offset], sizeof(unsigned __int16) * msg_type_def->UniIDLen );
            offset += sizeof(unsigned __int16) * msg_type_def->UniIDLen;
          }
          else
          {
            ret_code = SSP_HEADER_CHECK_FAILURE;
          }
          // --- Estraggo DataNum ---
          if ( ret_code == SSP_NO_ERROR && message->BufferLen >= offset + sizeof(unsigned __int8) )
          {
            memcpy( &message->Header.DataNum, &message->Buffer[offset], sizeof(unsigned __int8) );
            offset += sizeof(unsigned __int8);
          }
          else
          {
            ret_code = SSP_HEADER_CHECK_FAILURE;
          }
          // --- Copio il buffer e aggiorno l'offset ---
          if ( ret_code == SSP_NO_ERROR )
          {
            header_len = *poffset;
            header_len = offset - header_len;
            memcpy( message->Header.Buffer, &message->Buffer[offset], header_len );
            message->Header.BufferLen = header_len;
            *poffset = offset;
          }
        }
      }
      else
      {
        ret_code = SSP_INVALID_MESSAGE_BUFFER;
      }
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_MESSAGE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per estrarre un datablock SSP da un buffer
///
/// \date [05.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SSPExtractDataBlock( SSP_MESSAGE * message, int num, int * poffset )
{
  int                     ret_code      = SSP_NO_ERROR;
  int                     offset        = 0;
  int                     datablock_len = 0;
  SSP_DATABLOCK         * datablock     = NULL;

  if ( SSPValidMessage( message ) )
  {
    try
    {
      if ( poffset != NULL ) offset = *poffset;
      if ( num < message->Header.DataNum )
      {
        datablock = &message->DataBlockList[num];
        // --- Estraggo DataID ---
        if ( message->BufferLen >= offset + sizeof(unsigned __int16 ) )
        {
          memcpy( &datablock->DataID, &message->Buffer[offset], sizeof(unsigned __int16) );
          offset += sizeof(unsigned __int16);
        }
        else
        {
          ret_code = SSP_DATABLOCK_CHECK_FAILURE;
        }
        // --- Estraggo DataLen ---
        if ( ret_code == SSP_NO_ERROR && message->BufferLen >= offset + sizeof(unsigned __int16 ) )
        {
          memcpy( &datablock->DataLen, &message->Buffer[offset], sizeof(unsigned __int16) );
          offset += sizeof(unsigned __int16);
        }
        else
        {
          ret_code = SSP_DATABLOCK_CHECK_FAILURE;
        }
        // --- Estraggo Data ---
        if ( ret_code == SSP_NO_ERROR && message->BufferLen >= offset + ( sizeof(unsigned __int8 ) * datablock->DataLen ) )
        {
          memcpy( &datablock->Data[0], &message->Buffer[offset], ( sizeof(unsigned __int8 ) * datablock->DataLen ) );
          offset += ( sizeof(unsigned __int8 ) * datablock->DataLen );
        }
        else
        {
          ret_code = SSP_DATABLOCK_CHECK_FAILURE;
        }
        // --- Copio il buffer e aggiorno l'offset ---
        if ( ret_code == SSP_NO_ERROR )
        {
          datablock_len = *poffset;
          datablock_len = offset - datablock_len;
          memcpy( datablock->Buffer, &message->Buffer[offset], datablock_len );
          datablock->BufferLen = datablock_len;
          *poffset = offset;
        }
      }
      else
      {
        ret_code = SSP_DATABLOCK_CHECK_FAILURE;
      }
    }
    catch(...)
    {
      ret_code = SSP_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SSP_INVALID_MESSAGE;
  }

  return ret_code;
}


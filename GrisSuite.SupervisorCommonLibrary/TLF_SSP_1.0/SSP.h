//==============================================================================
// Telefin SSP Protocol Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO PROTOCOLLO SSP (SSP.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.07 (10.02.2006 -> 09.05.2006)
//
// Copyright: 2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SSP.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [10.02.2006]:
// - Prima versione del modulo.
// 0.01 [31.03.2006]:
// - Aggiunta la funzione SSPValidMessage.
// 0.02 [03.04.2006]:
// - Aggiunta la funzione SSPNewMessage.
// - Aggiunta la funzione SSPDeleteMessage.
// - Aggiunta la funzione SSPGetMessageTypeDef.
// - Aggiunta la funzione SSPSetMessage.
// - Aggiunta la funzione SSPBuildMessage.
// - Aggiunta la funzione SSPSetMessageHeader.
// - Aggiunta la funzione SSPValidHeader.
// - Aggiunta la funzione SSPBuildMessageHeader.
// - Aggiunta la funzione SSPValidDataBlock.
// - Aggiunta la funzione SSPBuildDataBlock.
// 0.03 [05.04.2006]:
// - Modificata la funzione SSPBuildMessage.
// - Aggiunta la funzione SSPExtractDataBlock. 
// 0.04 [27.04.2006]:
// - Modificata la funzione SSPSetMessageDataBlock.
// 0.05 [02.05.2006]:
// - Corretta la funzione SSPExtractHeader.
// 0.06 [03.05.2006]:
// - Corretta la funzione SSPExtractMessage.
// 0.07 [09.05.2006]:
// - Aggiunta la funzione SSPCopyMessage.
//==============================================================================

#ifndef SSPH
#define SSPH

//==============================================================================
// Definizione valori
//------------------------------------------------------------------------------
#define SSP_HEADER_BUFFER_MAX_LEN         12
#define SSP_HEADER_UNIID_MAX_LEN          5
#define SSP_DATABLOCK_DATA_MAX_LEN        1436
#define SSP_DATABLOCK_BUFFER_MAX_LEN      1440
#define SSP_MESSAGE_DATABLOCK_MAX_NUM     256
#define SSP_MESSAGE_BUFFER_MAX_LEN        1452
#define SSP_MESSAGE_TYPE_NAME_MAX_LEN     64

#define SSP_DATAID_NOT_USED               20053
#define SSP_DATAID_NOT_AVAILABLE          30821

//==============================================================================
// Codici per la verifica della validita'
//------------------------------------------------------------------------------
#define SSP_HEADER_VALIDITY_CHECK_CODE    2200631334
#define SSP_DATABLOCK_VALIDITY_CHECK_CODE 2203151599
#define SSP_MESSAGE_VALIDITY_CHECK_CODE   2207322153

//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define SSP_NO_ERROR                                  0
#define SSP_CRITICAL_ERROR                            2407001
#define SSP_INVALID_DATABLOCK_NUM                     2407002
#define SSP_INVALID_DATABLOCK                         2407003
#define SSP_INVALID_HEADER                            2407004
#define SSP_INVALID_MESSAGE                           2407005
#define SSP_INVALID_MESSAGE_BUFFER                    2407006
#define SSP_HEADER_CHECK_FAILURE                      2407007
#define SSP_DATABLOCK_CHECK_FAILURE                   2407008

//==============================================================================
/// Struttura di un Header SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SSP_HEADER
{
  unsigned __int32    VCC                               ; // Validity Check Code
  unsigned __int8     UniIDLen                          ; // Lunghezza in word dell'UniID
  unsigned __int8     MsgType                           ; // Tipo di messaggio SSP
  unsigned __int16    UniID[SSP_HEADER_UNIID_MAX_LEN]   ; // ID univoco
  unsigned __int8     DataNum                           ; // Numero di DataBlock
  unsigned char       Buffer[SSP_HEADER_BUFFER_MAX_LEN] ; // Buffer dell'header
  unsigned __int16    BufferLen                         ; // Lunghezza del buffer
}
SSP_HEADER;

//==============================================================================
/// Struttura di un DataBlock SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SSP_DATABLOCK
{
  unsigned __int32    VCC                                   ; // Validity Check Code
  unsigned __int16    DataID                                ; // ID del dato
  unsigned __int16    DataLen                               ; // Lunghezza in byte di Data
  unsigned char       Data[SSP_DATABLOCK_DATA_MAX_LEN]      ; // Buffer Data
  unsigned char       Buffer[SSP_DATABLOCK_BUFFER_MAX_LEN]  ; // Buffer dell'header
  unsigned __int16    BufferLen                             ; // Lunghezza del buffer
}
SSP_DATABLOCK;

//==============================================================================
/// Struttura di un Messaggio SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SSP_MESSAGE
{
  unsigned __int32    VCC                                         ; // Validity Check Code
  SSP_HEADER          Header                                      ; // Header SSP
  SSP_DATABLOCK       DataBlockList[SSP_MESSAGE_DATABLOCK_MAX_NUM]; // Array dei DataBlock
  unsigned char       Buffer[SSP_DATABLOCK_BUFFER_MAX_LEN]        ; // Buffer dell'header
  unsigned __int16    BufferLen                                   ; // Lunghezza del buffer
}
SSP_MESSAGE;

//==============================================================================
/// Struttura di una Definizione Tipo Messaggio SSP
///
/// \date [03.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SSP_MESSAGE_TYPE_DEF
{
  int                 ID          ; // ID dell'elemento in un array
  unsigned __int8     MsgType     ; // Codice del tipo di messaggio
  char                MsgName[64] ; // Nome del tipo di messaggio
  unsigned __int8     UniIDLen    ; // Lunghezza in word dell'UniID
  unsigned __int8     DataMinNum  ; // Numero di DataBlock
  unsigned __int8     DataMaxNum  ; // Numero di DataBlock
}
SSP_MESSAGE_TYPE_DEF;

//==============================================================================
// Definizione dei tipi di messaggio
//------------------------------------------------------------------------------
static SSP_MESSAGE_TYPE_DEF SSPMessageTypeDefList[11] = {
{ 0 , 2   , "SetDeviceObjectRequest"        , 5, 2, 2 },
{ 1 , 3   , "ExecuteDeviceCommandRequest"   , 5, 1, 1 },
{ 2 , 5   , "GetDeviceObjectRequest"        , 5, 1, 1 },
{ 3 , 6   , "GetDeviceEventRequest"         , 5, 0, 1 },
{ 4 , 130 , "SetDeviceObjectResponse"       , 5, 2, 2 },
{ 5 , 131 , "ExecuteDeviceCommandResponse"  , 5, 2, 2 },
{ 6 , 133 , "GetDeviceObjectResponse"       , 5, 2, 3 },
{ 7 , 134 , "GetDeviceEventResponse"        , 5, 2, 4 },
{ 8 , 253 , "SendAliveStatus"               , 0, 0, 0 },
{ 9 , 254 , "SendDeviceStatus"              , 5, 3, 3 },
{ 10, 255 , "SendDeviceObject"              , 5, 6, 6 }
};

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

// Funzione per allocare un nuovo messaggio SSP
SSP_MESSAGE           * SSPNewMessage           ( void );
// Funzione per eliminare un messaggio SSP
int                     SSPDeleteMessage        ( SSP_MESSAGE * message );
// Funzione per verificare la validita' di una struttura messaggio SSP
bool                    SSPValidMessage         ( SSP_MESSAGE * message );
// Funzione per copiare una struttura messaggio SSP
SSP_MESSAGE           * SSPCopyMessage          ( SSP_MESSAGE * message );
// Funzione per recuperare una struttura di definizione tipo messaggio SSP
SSP_MESSAGE_TYPE_DEF  * SSPGetMessageTypeDef    ( unsigned __int8 msg_type );
// Funzione per settare i parametri di un messaggio SSP
int                     SSPSetMessage           ( SSP_MESSAGE * message, SSP_HEADER header, SSP_DATABLOCK * datablock_list );
// Funzione per costruire il buffer di un messaggio SSP
int                     SSPBuildMessage         ( SSP_MESSAGE * message );
// Funzione per settare un header in un messaggio SSP
int                     SSPSetMessageHeader     ( SSP_MESSAGE * message, SSP_HEADER header );
// Funzione per verificare la validita' di una struttura header SSP
bool                    SSPValidHeader          ( SSP_HEADER * header );
// Funzione per costruire il buffer di un header SSP
int                     SSPBuildMessageHeader   ( SSP_HEADER * header );
// Funzione per settare un datablock in un messaggio SSP
int                     SSPSetMessageDataBlock  ( SSP_MESSAGE * message, int num, SSP_DATABLOCK datablock );
// Funzione per verificare la validita' di un datablock SSP
bool                    SSPValidDataBlock       ( SSP_DATABLOCK * datablock );
// Funzione per costruire il buffer di un datablock SSP
int                     SSPBuildDataBlock       ( SSP_DATABLOCK * datablock );
// Funzione per estrarre un messaggio SSP da un buffer
int                     SSPExtractMessage       ( SSP_MESSAGE * message, SSP_MESSAGE * res_message );
// Funzione per estrarre un header SSP da un buffer
int                     SSPExtractHeader        ( SSP_MESSAGE * message, int * poffset );
// Funzione per estrarre un datablock SSP da un buffer
int                     SSPExtractDataBlock     ( SSP_MESSAGE * message, int num, int * poffset );
#endif

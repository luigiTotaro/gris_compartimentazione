//==============================================================================
// Telefin Conversion Library 1.0
//------------------------------------------------------------------------------
// LIBRERIA DI CONVERSIONE (cnv_lib.cpp)
//
// Versione:	0.20 (16.03.2004 -> 21.08.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:		richiede cnv_lib.h,stdio.h,string.h,stdlib.h,alloc.h,math.h
//------------------------------------------------------------------------------
// Version history: vedere in cnv_lib.h
//==============================================================================
#pragma hdrstop

#include "cnv_lib.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#pragma package(smart_init)
#endif
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione di conversione da stringa di caratteri a bool.
///
/// \date [16.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
bool cnv_CharPToBool(char * str)
{
  bool value = false; // Di default il valore � falso

  if (str) // Controllo la stringa
  {
	if (strcmpi(str,"true")==0) value = true;
	if (strcmpi(str,"1")==0) value = true;
  }

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa a intero con segna a 8 bit
///
/// \date [23.06.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
__int8 cnv_CharPToInt8(char * str)
{
  __int8            value_sign  = 1 ;
  __int8            value       = 0 ;
  int               string_len      ;
  char              c               ;
  int               cval            ;

  if ( str != NULL )
  {
    string_len = strlen(str);
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c     = str[i];
        if ( c == '-' )
        {
          value_sign = -1;
        }
        else
        {
          cval  = (int) c;
          cval  = (cval>=48 && cval<=57)?cval-48:0;
          value *= 10;
          value += (__int8)cval;
        }
      }
    }
  }
  value *= value_sign; // Applico il segno

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa a intero con segna a 16 bit
///
/// \date [23.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
__int16 cnv_CharPToInt16( char * str )
{
  __int16           value_sign  = 1 ;
  __int16           value       = 0 ;
  int               string_len      ;
  char              c               ;
  int               cval            ;

  if ( str != NULL )
  {
    string_len = strlen(str);
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c     = str[i];
        if ( c == '-' )
        {
          value_sign = -1;
        }
        else
        {
          cval  = (int) c;
          cval  = (cval>=48 && cval<=57)?cval-48:0;
          value *= 10;
          value += (__int16)cval;
        }
      }
    }
  }
  value *= value_sign; // Applico il segno

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa a intero con segna a 32 bit
///
/// \date [23.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
__int32 cnv_CharPToInt32( char * str )
{
  __int32           value_sign  = 1 ;
  __int32           value       = 0 ;
  int               string_len      ;
  char              c               ;
  int               cval            ;

  if ( str != NULL )
  {
    string_len = strlen(str);
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c     = str[i];
        if ( c == '-' )
        {
          value_sign = -1;
        }
        else
        {
          cval  = (int) c;
          cval  = (cval>=48 && cval<=57)?cval-48:0;
          value *= 10;
          value += (__int32)cval;
        }
      }
    }
  }
  value *= value_sign; // Applico il segno

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa a intero con segna a 64 bit
///
/// \date [23.06.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
__int64 cnv_CharPToInt64( char * str )
{
  __int64           value_sign  = 1 ;
  __int64           value       = 0 ;
  int               string_len      ;
  char              c               ;
  int               cval            ;

  if ( str != NULL )
  {
    string_len = strlen(str);
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c     = str[i];
        if ( c == '-' )
        {
          value_sign = -1;
        }
        else
        {
          cval  = (int) c;
          cval  = (cval>=48 && cval<=57)?cval-48:0;
          value *= 10;
          value += (__int64)cval;
        }
      }
    }
  }
  value *= value_sign; // Applico il segno

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned int a 8 bit
///
/// \date [26.03.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
unsigned __int8 cnv_CharPToUInt8(char * str)
{
  unsigned __int8 value           ;
  int             string_len      ;
  int             value_int   = 0 ;
  char            c               ;
  int             cval            ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)?cval-48:((cval<48)?0:9);
        value_int *= 10;
        value_int += cval;
      }
    }
  }
  value_int   = (value_int>255)?255:value_int;
  value       = (unsigned __int8) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned int a 16 bit
///
/// \date [26.03.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
unsigned __int16 cnv_CharPToUInt16(char * str)
{
  unsigned __int16 value           ;
  int             string_len      ;
  int             value_int   = 0 ;
  char            c               ;
  int             cval            ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)?cval-48:((cval<48)?0:9);
        value_int *= 10;
        value_int += cval;
      }
    }
  }
  value_int = (value_int>65535)?65535:value_int;
  value = (unsigned __int16) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned int a 32 bit
///
/// \date [04.08.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int32 cnv_CharPToUInt32( char * str )
{
  unsigned __int32  value           ;
  int               string_len      ;
  unsigned __int32  value_int   = 0 ;
  char              c               ;
  int               cval            ;

  if ( str != NULL )
  {
    string_len = strlen( str );
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)?cval-48:((cval<48)?0:9);
        value_int *= 10;
        value_int += cval;
      }
    }
  }
  value_int = (value_int>4294967295)?4294967295:value_int;
  value = (unsigned __int32) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned int a 64 bit
///
/// \date [24.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
unsigned __int64 cnv_CharPToUInt64( char * str )
{
  unsigned __int64  value           ;
  int               string_len      ;
  unsigned __int64  value_int   = 0 ;
  char              c               ;
  int               cval            ;

  if ( str != NULL )
  {
    string_len = strlen( str );
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)?cval-48:((cval<48)?0:9);
        value_int *= 10;
        value_int += cval;
      }
    }
  }

  value_int = (value_int>18446744073709551615)?18446744073709551615:value_int;
  value = (unsigned __int64) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned hex a 8 bit
///
/// \date [21.08.2012]
/// \author Enrico Alborali, Paolo Colli
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int8 cnv_CharPToUHex8(char * str)
{
  unsigned __int8 value           ;
  int             string_len      ;
  int             value_int   = 0 ;
  char            c               ;
  int             cval            ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)  ? cval-48 :
                    (cval>=65 && cval<=70)  ? cval-55 :
                    (cval>=97 && cval<=102) ? cval-87 :
                    (cval<48)               ? 0       :
                    (cval>57&&cval<65)      ? 9       :
                    (cval>70&&cval<97)      ? 0x0A    : 0x0F;
        value_int *= 16;
        value_int += cval;
      }
    }
  }
  value_int = (value_int>255)?255:value_int;
  value     = (unsigned __int8) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned hex a 16 bit
///
/// \date [26.03.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
unsigned __int16 cnv_CharPToUHex16(char * str)
{
  unsigned __int16 value          ;
  int             string_len      ;
  int             value_int   = 0 ;
  char            c               ;
  int             cval            ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)  ? cval-48 :
                    (cval>=65 && cval<=70)  ? cval-55 :
                    (cval>=97 && cval<=102) ? cval-87 :
                    (cval<48)               ? 0       :
                    (cval>57&&cval<65)      ? 9       :
                    (cval>70&&cval<97)      ? 0x0A    : 0x0F;
        value_int *= 16;
        value_int += cval;
      }
    }
  }
  value_int = (value_int>65535)?65535:value_int;
  value = (unsigned __int16) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a unsigned hex a 32 bit
///
/// \date [19.10.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
unsigned __int32 cnv_CharPToUHex32( char * str )
{
  unsigned __int32  value           ;
  int               string_len      ;
  unsigned __int32  value_int   = 0 ;
  char              c               ;
  unsigned __int32  cval            ;

  if ( str != NULL )
  {
    string_len = strlen( str );
    if ( string_len > 0 )
    {
      for ( int i = 0; i < string_len; i++ )
      {
        c         = str[i];
        cval      = (unsigned __int32) c;
        cval      = ( cval >= 48 && cval <= 57 )  ? cval-48 :
                    ( cval >= 65 && cval <= 70 )  ? cval-55 :
                    ( cval >= 97 && cval <= 102 ) ? cval-87 :
                    ( cval < 48)                  ? 0       :
                    ( cval > 57 && cval < 65 )    ? 9       :
                    ( cval > 70 && cval < 97 )    ? 0x0A    : 0x0F;
        value_int *= 16;
        value_int += cval;
      }
    }
  }
  value_int = ( value_int > 4294967295 ) ? 4294967295 : value_int;
  value     = (unsigned __int32) value_int;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a signed hex a 8 bit
///
/// \date [13.02.2009]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
__int8 cnv_CharPToSHex8(char * str)
{
	unsigned __int8 value           ;
	__int8			svalue		= 0	;
	int             string_len      ;
	int             value_int   = 0 ;
	char            c               ;
	int             cval            ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)  ? cval-48 :
                    (cval>=65 && cval<=70)  ? cval-55 :
                    (cval>=97 && cval<=102) ? cval-87 :
                    (cval<48)               ? 0       :
                    (cval>57&&cval<65)      ? 9       :
                    (cval>70&&cval<97)      ? 0x0A    : 0x0F;
        value_int *= 16;
        value_int += cval;
      }
    }
  }
	value_int = (value_int>255)?255:value_int;
	value     = (unsigned __int8) value_int;
	// Coversione in 8 bit con segno
	svalue = (__int8)value;

	return svalue;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a signed hex a 16 bit
///
/// \date [12.02.2009]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
__int16 cnv_CharPToSHex16(char * str)
{
	unsigned __int16	value			;
	__int16				svalue		= 0	;
	int             	string_len      ;
	int					value_int   = 0 ;
	char            	c               ;
	int             	cval            ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c         = str[i];
        cval      = (int) c;
        cval      = (cval>=48 && cval<=57)  ? cval-48 :
                    (cval>=65 && cval<=70)  ? cval-55 :
                    (cval>=97 && cval<=102) ? cval-87 :
                    (cval<48)               ? 0       :
                    (cval>57&&cval<65)      ? 9       :
                    (cval>70&&cval<97)      ? 0x0A    : 0x0F;
        value_int *= 16;
        value_int += cval;
      }
    }
  }
	value_int = (value_int>65535)?65535:value_int;
	value = (unsigned __int16) value_int;
	// Coversione in 16 bit con segno
	svalue = (__int16)value;

	return svalue;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a float
///
/// \date [29.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
float cnv_CharPToFloat(char * str)
{
  float           value               ;
  int             string_len          ;
  float           value_int   = 0     ;
  float           value_fraz  = 0     ;
  float           sign        = 1     ; // Il segno del valore finale
  char            c                   ;
  int             cval                ;
  bool            fraz        = false ;
  int             offset              ;

  if (str)
  {
    string_len = strlen(str);
    if (string_len)
    {
      for (int i=0; i<string_len; i++)
      {
        c = str[i];
        if (i==0 && c=='-')
        {
          sign = -1;
        }
        else
        {
          if (c == '.')
          {
            fraz = true;
            offset = i; // Salvo l'offset delle cifre decimali
          }
          else
          {
            if (fraz)
            {
              c         = str[string_len+offset-i];
              cval      =   (int) c;
              cval      =   (cval>=48 && cval<=57)?cval-48  :
                            (cval<48)?0                     : 9;
              value_fraz +=  (float) cval;
              value_fraz /=  10;
            }
            else
            {
              cval      =   (int) c;
              cval      =   (cval>=48 && cval<=57)?cval-48  :
                            (cval<48)?0                     : 9;
              value_int *=  10;
              value_int +=  (float) cval;
            }
          }
        }
      }
    }
  }
  value = (float) (value_int+value_fraz) * sign;

  return value;
}

//==============================================================================
/// Funzione di conversione da stringa di caratteri a float
///
/// \date [29.06.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
long cnv_CharPToTime( char * str )
{
  long      value;
  time_t    dt_time;
  char      s_hour[3] ;
  char      s_mins[3] ;
  char      s_secs[3] ;
  int       i_hour    ;
  int       i_mins    ;
  int       i_secs    ;

  // --- Converto l'ora ---
  s_hour[0] = str[0];
  s_hour[1] = str[1];
  s_hour[2] = NULL;
  i_hour = (int)cnv_CharPToUInt8( s_hour );
  // --- Converto i minuti ---
  s_mins[0] = str[3];
  s_mins[1] = str[4];
  s_mins[2] = NULL;
  i_mins = (int)cnv_CharPToUInt8( s_mins );
  // --- Converto i secondi ---
  s_secs[0] = str[6];
  s_secs[1] = str[7];
  s_secs[2] = NULL;
  i_secs = (int)cnv_CharPToUInt8( s_secs );

  // --- Converto l'ora in format time_t ---
  dt_time = ( i_hour * 3600 ) + ( i_mins * 60 ) + i_secs;

  value = (long)dt_time;

  return value;
}

//==============================================================================
/// Funzione di conversione da unsigned int a 8 bit a stringa di caratteri.
///
/// \date [19.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_UInt8ToCharP( unsigned __int8 input )
{
  char  * output   = NULL;
  int     value          ;
  int     divider  = 1   ;
  int     result         ;
  int     dig      = 3   ;
  char    d              ;

  value = (int) input;

  if (value < 100)  dig = 2; // Caso di due cifre
  if (value < 10)   dig = 1; // Caso di una cifra

  output = (char*) malloc(sizeof(char)*(dig+1));

  for (int i=0; i<dig; i++)
  {
	divider *= 10;
	result = (int)floor((float)((value%divider)/(divider/10)))+48;
	d = (char) result;
    output[dig-i-1] = d;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da unsigned hex a 8 bit a stringa di caratteri.
///
/// \date [03.08.2004]
/// \author Enrico Alborali.
/// \version 0.03
//------------------------------------------------------------------------------
char * cnv_UHex8ToCharP(unsigned __int8 input)
{
  char  * output   = NULL;
  int     value          ;
  int     divider  = 1   ;
  int     result         ;
  int     dig      = 2   ; // Ho due cifre (ognuna contiene 4 bit di info)
  char    d              ;

  value = (int) input;

  output = (char*) malloc(sizeof(char)*(dig+1)); // Alloco la memoria

  for (int i=0; i<dig; i++)
  {
	divider *= 16;
	result = (int)floor((float)((value%divider)/(divider/16)));
	result = (result<=9)?result+48:result+55;
    d = (char) result;
    output[dig-i-1] = d;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da unsigned int a 16 bit a stringa di caratteri.
///
/// \date [19.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_UInt16ToCharP (unsigned __int16 input)
{
  char  * output   = NULL;
  int     value          ;
  int     divider  = 1   ;
  int     result         ;
  int     dig      = 5   ;
  char    d              ;

  value = (int) input;

  if (value < 10000)  dig = 4; // Caso di quattro cifre
  if (value < 1000)   dig = 3; // Caso di tre cifre
  if (value < 100)    dig = 2; // Caso di due cifre
  if (value < 10)     dig = 1; // Caso di una cifra

  output = (char*) malloc(sizeof(char)*(dig+1));

  for (int i=0; i<dig; i++)
  {
	divider *= 10;
	result = (int)floor((float)((value%divider)/(divider/10)))+48;
    d = (char) result;
    output[dig-i-1] = d;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da unsigned int a 16 bit a stringa di caratteri.
///
/// \date [09.03.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_UHex16ToCharP (unsigned __int16 input)
{
  char  * output   = NULL;
  int     value          ;
  int     divider  = 1   ;
  int     result         ;
  int     dig      = 4   ;
  char    d              ;

  value = (int) input;

  output = (char*) malloc(sizeof(char)*(dig+1));

  for (int i=0; i<dig; i++)
  {
	divider         *= 16;
	result          = (int)floor((float)((value%divider)/(divider/16)));
    result          = (result<=9)?result+48:result+55;
    d               = (char) result;
    output[dig-i-1] = d;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da unsigned int a 32 bit a stringa di caratteri.
///
/// \date [10.10.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * cnv_UInt32ToCharP( unsigned __int32 input )
{
  char              * output    = NULL;
  unsigned __int32    value           ;
  unsigned __int32    value_app       ;
  unsigned __int32    result          ;
  int                 dig       = 0   ; // Massimo 20 cifre
  char                d               ;

  value = (unsigned __int32) input;

  if ( value == 0 )
  {
    dig = 1;
  }
  else
  {
    // --- Calcolo il numero di cifre ---
    value_app = value;
    while( value_app != 0 )
    {
      value_app = value_app / 10L;
      dig++;
    }
  }

  output = (char*) malloc( sizeof(char) * ( dig + 1 ) );

  for ( int i = 0; i < dig; i++ )
  {
    result = ( value % 10L ) + 48L;
    d = (char) result;
    output[dig-i-1] = d;
    value = value / 10L;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da unsigned int a 64 bit a stringa di caratteri.
///
/// \date [08.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_UInt64ToCharP( unsigned __int64 input )
{
  char    * output   = NULL;
  unsigned __int64   value          ;
  unsigned __int64   value_app      ;
  unsigned __int64   result         ;
  int       dig      = 0   ; // Massimo 20 cifre
  char      d              ;

  value = (unsigned __int64) input;

  if ( value == 0 )
  {
    dig = 1;
  }
  else
  {
    // --- Calcolo il numero di cifre ---
    value_app = value;
    while( value_app != 0 )
    {
      value_app = value_app / 10L;
      dig++;
    }
  }  

  output = (char*) malloc( sizeof(char) * ( dig + 1 ) );

  for ( int i = 0; i < dig; i++ )
  {
    result = ( value % 10L ) + 48L;
    d = (char) result;
    output[dig-i-1] = d;
    value = value / 10L;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da unsigned int a 32 bit a stringa di caratteri.
///
/// \date [25.09.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
char * cnv_UHex32ToCharP (unsigned __int32 input)
{
  char * 				output   = NULL;
  unsigned __int32     	value          ;
  unsigned __int64     	divider  = 1   ;
  unsigned __int32     	result         ;
  int     				dig      = 8   ;
  char    				d              ;

  value = (int) input;

  output = (char*) malloc( sizeof(char) * ( dig + 1 ) );

  for ( int i = 0; i < dig; i++ )
  {
	divider         *= 16;
	result          = (int)floor( (float)( ( value % divider ) / ( divider / 16 ) ) );
	result          = ( result <= 9 ) ? result + 48 : result + 55;
    d               = (char) result;
    output[dig-i-1] = d;
  }

  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione da boolean a stringa di caratteri.
///
/// \date [19.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_BoolToCharP (bool input)
{
  char * output = NULL;

  if (input == true)
  {
    output = (char*) malloc(sizeof(char)*5);
    output[0] = 't';
    output[1] = 'r';
    output[2] = 'u';
    output[3] = 'e';
    output[4] = NULL;
  }
  else
  {
    output = (char*) malloc(sizeof(char)*6);
    output[0] = 'f';
    output[1] = 'a';
    output[2] = 'l';
    output[3] = 's';
    output[4] = 'e';
    output[5] = NULL;
  }

  return output;
}

//==============================================================================
/// Funzione di conversione da float a stringa di caratteri.
///
///
/// \date [29.03.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_FloatToCharP(float input)
{
  char  * output    = NULL  ;
  float   fapp              ; // Float di appoggio
  int     iapp              ; // Int di appoggio
  int     f_int             ; // Parte intera
  float   f_fraz            ; // Parte frazionaria
  int     divider   = 1     ;
  int     result            ;
  int     dig       = 16    ; // Imposto 15 cifre significative (+1 per il punto)
  int     offset    = 0     ;
  char    d                 ;
  int     start     = 0     ;

  fapp    = input;
  if (input < 0) // Se l'input � negativo
  {
    dig++; // Aggiungo lo spazio per il '-'
    output  = (char*) malloc(sizeof(char)*(dig+1));
    output[0] = '-';
    offset++;
    fapp *= -1; // Tolgo il segno negativo
    start = 1;
  }
  else
  {
    output  = (char*) malloc(sizeof(char)*(dig+1));
  }
  iapp    = (int) floor(fapp);
  f_int   = iapp; // Salvo la parte intera
  f_fraz  = fapp - (float) iapp; // Salvo la parte frazionaria
  // --- Estraggo le cifre intere ---
  while (fapp >= 1)
  {
    fapp    /= 10;
	divider *= 10;
	result = (int)floor((float)((f_int%divider)/(divider/10)))+48;
    d = (char) result;
    if (offset)
    {
      for (int i=offset; i>0; i--)
      {
        output[i] = output[i-1];
      }
    }
    output[start] = d; // Copio la nuova cifra nella prima posizione.
    offset++;
  }
  output[offset] = '.'; // Aggiungo il punto
  offset++;
  // --- Estraggo le cifre decimali ---
  for (int i=offset; i<dig; i++)
  {
	f_fraz *= 10;
	result = (int)floor(f_fraz)+48;
    f_fraz -= (float) floor(f_fraz);
    d = (char) result;
    output[i] = d;
  }
  output[dig] = NULL; // Aggiungo il terminatore di stringa

  return output;
}

//==============================================================================
/// Funzione di conversione ora da time_t a stringa di caratteri.
///

/// \date [21.08.2012]
/// \author Enrico Alborali, Paolo Colli
/// \version 0.02
//------------------------------------------------------------------------------
char * cnv_TimeToCharP( long input )
{
  time_t      time          = input;
  struct tm * dt_struct     = NULL;
  char        dt_string[80]       ;
  char      * output        = NULL;
  int         output_len    = 0   ;

  dt_struct = localtime( &time );
  strftime( dt_string, 80, "%H:%M:%S", dt_struct );
  output_len = strlen( dt_string );
  if ( output_len > 0 )
  {
    output = (char*)malloc( sizeof(char) * ( output_len + 1 ) ); // -MALLOC
    strcpy( output, dt_string );
  }

  return output;
}

//==============================================================================
/// Funzione di conversione data/ora da time_t a stringa di caratteri.
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_DateTimeToCharP( long input )
{
  time_t      time          = input;
  struct tm * dt_struct     = NULL;
  char        dt_string[80]       ;
  char      * output        = NULL;
  int         output_len    = 0   ;

  dt_struct = localtime( &time );
  strftime( dt_string, 80, "%d/%m/%Y %H:%M:%S", dt_struct );
  output_len = strlen( dt_string );
  if ( output_len > 0 )
  {
    output = (char*)malloc( sizeof(char) * ( output_len + 1 ) ); // -MALLOC
    strcpy( output, dt_string );
  }

  return output;
}

//==============================================================================
/// Funzione di conversione data/ora da time_t a stringa di caratteri.
///
/// \date [30.06.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * cnv_SwapBytes( char * input, int size, int len, bool ascii )
{
	char * output = NULL;
	int byte_size;
	int input_offset = 0;
	int output_offset = 0;
	int offset = 0;

	try
	{
		if ( input != NULL && size > 0 && len > 0 )
		{
			// Caso ASCII
			if (ascii) {
				byte_size = 2;
			}
			// Caso non ASCII
			else
			{
				byte_size = 1;
			}
			output = (char*)malloc(sizeof(char)*((size*byte_size*len)+1));
			memset(output,0,sizeof(char)*((size*byte_size*len)+1));
			if (output != NULL) {
				// ASCII = true, len = 2, size = 2
				// [BB][AA][DD][CC]
				// diventa
				// [AA][BB][CC][DD]

				// ASCII = false, len = 1, size = 4
				// [D][C][B][A]
				// diventa
				// [A][B][C][D]

				// Ciclo
				//*
				for (int l=0; l < len; l++) {
					offset = byte_size*size*l;
					for (int s=0; s < size; s++) {
						input_offset = offset + (byte_size*(size-1-s));
						output_offset = offset + (byte_size*s);
						for (int b=0; b < byte_size; b++) {
							output[output_offset+b] = input[input_offset+b];
						}
					}
				}
				//*/
				//memset(output,'8',sizeof(char)*(size*byte_size*len));
			}
		}
		else
		{
			output = NULL;
		}
	}
	catch(...)
	{
		output = NULL;
	}

	return output;
}

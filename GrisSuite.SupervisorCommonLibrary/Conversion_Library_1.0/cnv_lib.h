//==============================================================================
// Telefin Conversion Library 1.0
//------------------------------------------------------------------------------
// HEADER LIBRERIA DI CONVERSIONE (cnv_lib.h)
//
// Versione:	0.20 (16.03.2004 -> 21.08.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:		richiede cnv_lib.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [16.03.2004]:
// - Prima versione prototipo della libreria.
// 0.01 [19.03.2004]:
// - Aggiunta la funzione cnv_UInt8ToCharP.
// - Aggiunta la funzione cnv_UInt16ToCharP.
// - Aggiunta la funzione cnv_CharPToUInt16.
// 0.02 [22.03.2004]:
// - Corretta funzione cnv_CharPToUInt8.
// - Corretta funzione cnv_CharPToUInt16.
// 0.03 [26.03.2004]:
// - Aggiunta la funzione cnv_CharPToUHex8.
// - Corretta la funzione cnv_CharPToInt8.
// - Corretta la funzione cnv_CharPToUInt8.
// - Corretta la funzione cnv_CharPToUInt16.
// - Aggiunta la funzione cnv_CharPToUHex16.
// 0.04 [29.03.2004]:
// - Aggiunta la funzione cnv_CharPToFloat.
// - Aggiunta la funzione cnv_FloatToCharP.
// 0.05 [01.06.2004]:
// - Aggiunta la funzione cnv_UHex8ToCharP.
// 0.06 [15.06.2004]:
// - Modificata la funzione cnv_UHex8ToCharP.
// 0.07 [03.08.2004]:
// - Corretta la funzione cnv_UHex8ToCharP.
// 0.08 [09.03.2005]:
// - Aggiunta la funzione cnv_UHex16ToCharP.
// 0.09 [10.06.2005]:
// - Aggiunta la funzione cnv_CharPToUInt32.
// - Aggiunta la funzione cnv_CharPToUHex32.
// - Aggiunta la funzione cnv_UInt32ToCharP.
// - Aggiunta la funzione cnv_UHex32ToCharP.
// 0.10 [20.06.2005]:
// - Inclusa la libreria <time.h>
// - Modificata la funzione cnv_CharPToTime.
// - Modificata la funzione cnv_TimeToCharP.
// 0.11 [24.06.2005]:
// - Modificata la funzione cnv_CharPToTime.
// 0.12 [29.06.2005]:
// - Modificata la funzione cnv_CharPToTime.
// 0.13 [30.06.2005]:
// - Modificata la funzione cnv_DateTimeToCharP.
// 0.14 [04.08.2005]:
// - Corretta la funzione cnv_CharPToUInt32.
// 0.15 [19.10.2005]:
// - Corretta la funzione cnv_CharPToUHex32.
// 0.16 [08.03.2006]:
// - Aggiunta la funzione cnv_UInt64ToCharP.
// 0.17 [24.04.2006]:
// - Aggiunta la funzione cnv_CharPToUInt64.
// 0.18 [10.10.2006]:
// - Corretta la funzione cnv_UInt32ToCharP.
// 0.19 [25.09.2007]:
// - Corretta la funzione cnv_UHex32ToCharP.
//==============================================================================
#ifndef cnv_libH
#define cnv_libH
//---------------------------------------------------------------------------

// --- Funzioni di conversione da stringa di caratteri a tipo specifico ---
         bool     cnv_CharPToBool   (             char  * str );
// Funzione di conversione da stringa a intero con segna a 8 bit
         __int8   cnv_CharPToInt8   (             char  * str );
// Funzione di conversione da stringa a intero con segna a 16 bit
         __int16  cnv_CharPToInt16  (             char  * str );
// Funzione di conversione da stringa a intero con segna a 32 bit
         __int32  cnv_CharPToInt32  (             char  * str );
// Funzione di conversione da stringa a intero con segna a 64 bit
         __int64  cnv_CharPToInt64  (             char  * str );
unsigned __int8   cnv_CharPToUInt8  (             char  * str );
unsigned __int16  cnv_CharPToUInt16 (             char  * str );
unsigned __int32  cnv_CharPToUInt32 (             char  * str );
unsigned __int64  cnv_CharPToUInt64 (             char  * str );
unsigned __int8   cnv_CharPToUHex8  (             char  * str );
unsigned __int16  cnv_CharPToUHex16 (             char  * str );
unsigned __int32  cnv_CharPToUHex32 (             char  * str );
		 __int8   cnv_CharPToSHex8  (             char  * str );
		 __int16  cnv_CharPToSHex16 (             char  * str );
		 float    cnv_CharPToFloat  (             char  * str );
         long     cnv_CharPToTime   (             char  * str );

// --- Funzioni di conversione da tipo specifico a stringa di caratteri ---
char          * cnv_UInt8ToCharP    ( unsigned  __int8  input );
char          * cnv_UInt16ToCharP   ( unsigned  __int16 input );
char          * cnv_UInt32ToCharP   ( unsigned  __int32 input );
char          * cnv_UInt64ToCharP   ( unsigned  __int64 input );
char          * cnv_UHex8ToCharP    ( unsigned  __int8  input );
char          * cnv_UHex16ToCharP   ( unsigned  __int16 input );
char          * cnv_UHex32ToCharP   ( unsigned  __int32 input );
char          * cnv_BoolToCharP     (             bool  input );
char          * cnv_FloatToCharP    (             float input );
char          * cnv_TimeToCharP     (             long  input );
char          * cnv_DateTimeToCharP (             long  input );

// --- Funzioni di conversione riguardo a Endianess ---
char *			cnv_SwapBytes( char * input, int size, int len, bool ascii );

#endif

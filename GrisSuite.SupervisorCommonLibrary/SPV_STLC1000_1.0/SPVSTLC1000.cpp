//==============================================================================
// Telefin Supervisor STLC1000 Module 1.0
//------------------------------------------------------------------------------
// Modulo di gestione dispositivi STLC1000 (SPVSTLC1000.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Revisione:	0.09 (13.09.2005 -> 28.03.2008)
//
// Copyright: 2004-2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, C++ Builder 2006
// Autori:	  Enrico Alborali (alborali@telefin.it)
//			  Mario Ferro	  (mferro@deltasistemi-vr.it)
// Note:      richiede SPVSTLC1000.h
//------------------------------------------------------------------------------
// Version history: vedere SPVSTLC1000.h
//==============================================================================

#pragma hdrstop
#include <windows.h>
#include "SPVSTLC1000.h"
#include "gpioctl.h"
#include "Jida.h"
#pragma package(smart_init)

typedef BOOL WINAPI ( * STLC1K_JidaDllInitialize_PTR         ) ( void                               );
typedef BOOL WINAPI ( * STLC1K_JidaDllUninitialize_PTR       ) ( void                               );
typedef BOOL WINAPI ( * STLC1K_JidaBoardOpen_PTR             ) ( LPCSTR, DWORD, DWORD, PHJIDA       );
typedef BOOL WINAPI ( * STLC1K_JidaWDogSetConfig_PTR         ) ( HJIDA, DWORD, DWORD, DWORD, DWORD  );
typedef BOOL WINAPI ( * STLC1K_JidaWDogTrigger_PTR           ) ( HJIDA, DWORD                       );
typedef BOOL WINAPI ( * STLC1K_JidaI2CReadRegister_PTR       ) ( HJIDA, DWORD, BYTE, WORD, LPBYTE   );
typedef BOOL WINAPI ( * STLC1K_JidaI2CWriteRegister_PTR      ) ( HJIDA, DWORD, BYTE, WORD, BYTE     );
typedef BOOL WINAPI ( * STLC1K_JidaTemperatureGetInfo_PTR    ) ( HJIDA, DWORD, PJIDATEMPERATUREINFO );
typedef BOOL WINAPI ( * STLC1K_JidaTemperatureGetCurrent_PTR ) ( HJIDA, DWORD, LPDWORD, LPDWORD 	);
typedef BOOL WINAPI ( * STLC1K_JidaBoardClose_PTR            ) ( HJIDA                              );

static STLC1K_JidaDllInitialize_PTR          _JidaDllInitialize    ;
static STLC1K_JidaDllUninitialize_PTR        _JidaDllUninitialize  ;
static STLC1K_JidaBoardOpen_PTR              _JidaBoardOpen        ;
static STLC1K_JidaBoardClose_PTR             _JidaBoardClose       ;
static STLC1K_JidaWDogSetConfig_PTR          _JidaWDogSetConfig    ;
static STLC1K_JidaWDogTrigger_PTR            _JidaWDogTrigger      ;
static STLC1K_JidaI2CReadRegister_PTR        _JidaI2CReadRegister  ;
static STLC1K_JidaI2CWriteRegister_PTR       _JidaI2CWriteRegister ;
static STLC1K_JidaTemperatureGetInfo_PTR     _JidaTemperatureGetInfo  ;
static STLC1K_JidaTemperatureGetCurrent_PTR  _JidaTemperatureGetCurrent ;

static HANDLE     IOCHandle       ;
static BYTE       COMBitMask      ;
static BYTE       IOBitMask       ;
static HINSTANCE  Jida32Library   = NULL;
static HJIDA      CPUBoardHandler ;
static JIDATEMPERATUREINFO JidaTempInfo;


//==============================================================================
/// Funzione per aprire l'accesso agli IO dell'STLC1000
///
/// \date [14.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KOpenIOC( void )
{
	int ret_code = SPV_STLC1000_NO_ERROR;

	STLC1KCloseIOC();

	IOCHandle = CreateFile(
				SPV_STLC1000_COM_GPDDEVPATH,
				GENERIC_READ | GENERIC_WRITE,
				0,
				NULL,
				OPEN_EXISTING,
				0,
				NULL
				);
	if ( IOCHandle == INVALID_HANDLE_VALUE )
	{
		ret_code = SPV_STLC1000_IOC_OPEN_FAILURE;
	}

	return ret_code;
}
//==============================================================================
/// Funzione per chiudere l'accesso agli IO dell'STLC1000
///
/// \date [14.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KCloseIOC( void )
{
	int ret_code = SPV_STLC1000_NO_ERROR;

	if ( IOCHandle != INVALID_HANDLE_VALUE )
	{
		CloseHandle( IOCHandle );
	}
	else
	{
		ret_code = SPV_STLC1000_IOC_NOT_OPENED;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per calcolare il codice di velocit� per le COM dell'STLC1000
///
/// \date [14.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
WORD STLC1KGetBaudCode( int com_baud )
{
	WORD baud_code = 0;

	switch( com_baud )
	{
		case 300:
			baud_code = SPV_STLC1000_COM_SPEED_300;
			break;
		case 600:
			baud_code = SPV_STLC1000_COM_SPEED_600;
			break;
		case 1200:
			baud_code = SPV_STLC1000_COM_SPEED_1200;
			break;
		case 2400:
			baud_code = SPV_STLC1000_COM_SPEED_2400;
			break;
		case 4800:
			baud_code = SPV_STLC1000_COM_SPEED_4800;
			break;
		case 9600:
			baud_code = SPV_STLC1000_COM_SPEED_9600;
			break;
		case 14400:
			baud_code = SPV_STLC1000_COM_SPEED_14400;
			break;
		case 19200:
			baud_code = SPV_STLC1000_COM_SPEED_19200;
			break;
		case 38400:
			baud_code = SPV_STLC1000_COM_SPEED_38400;
			break;
		case 56000:
			baud_code = SPV_STLC1000_COM_SPEED_56000;
			break;
		case 57600:
			baud_code = SPV_STLC1000_COM_SPEED_57600;
			break;
		case 115200:
			baud_code = SPV_STLC1000_COM_SPEED_115200; // BYTE_PER_BAUD_115200;
			break;
		default:
			baud_code = 0;
			break;
	}

	return baud_code;
}

//==============================================================================
// Funzione per scrivere in un registro degli IO dell'STLC1000
///
/// \date [13.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int STLC1KWriteIOC( BYTE data, HANDLE dev_handle, int offset )
{
	int                 ret_code = SPV_STLC1000_NO_ERROR;
	GENPORT_WRITE_INPUT input_buffer;    // Input buffer for DeviceIoControl
	LONG				        IOCTL_code;
	LONG				        data_length;
	BOOL				        result;
	DWORD				        ret_length;     // Number of bytes returned

	if ( dev_handle != INVALID_HANDLE_VALUE )
	{
		// --- Imposto la modalit� in scrittura ---
		IOCTL_code = IOCTL_GPD_WRITE_PORT_UCHAR;
		// --- Imposto il giusto offset dove andare a scrivere ---
		input_buffer.PortNumber = offset;
		// --- Imposto il byte da scrivere ---
		input_buffer.CharData = (UCHAR)data;
		// --- Calcolo la lunghezza del dato ---
		data_length = offsetof( GENPORT_WRITE_INPUT, CharData ) + sizeof(input_buffer.CharData);
		// --- Eseguo il comando sul device ---
		result = DeviceIoControl(
			dev_handle    , // Handle to device
			IOCTL_code    , // IO Control code for Write
			&input_buffer , // Buffer to driver.  Holds port & data.
			data_length   , // Length of buffer in bytes.
			NULL          , // Buffer from driver.   Not used.
			0             , // Length of buffer in bytes.
			&ret_length   , // Bytes placed in outbuf.  Should be 0.
			NULL            // NULL means wait till I/O completes.
		);
		// --- Controllo il risultato ---
		if ( !result )
		{
			ret_code = SPV_STLC1000_IOC_WRITE_FAILURE;
		}
	}
	else
	{
		ret_code = SPV_STLC1000_IOC_NOT_OPENED;
	}

	return ret_code;
}
//==============================================================================
/// Funzione per leggere da un registro degli IO dell'STLC1000
///
/// \date [13.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int STLC1KReadIOC( void )
{
	int ret_code = SPV_STLC1000_NO_ERROR;

	return ret_code;
}

//==============================================================================
/// Funzione per impostare la maschera di bit dei parametri delle COM
///
/// \date [14.09.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int STLC1KSetCOMBitMask( int com_num, int type, bool echo )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( com_num == 3 || com_num == 4 )
  {
    if ( com_num == 3 )
    {
      if ( type == SPV_STLC1000_COM_MODE_RS485 )
      {
		COMBitMask &= 0xFB;
      }
	  if ( type == SPV_STLC1000_COM_MODE_RS422 )
      {
        COMBitMask |= 0x04;
      }
      if ( echo )
      {
        COMBitMask &= 0xFE;
      }
	  else
      {
        COMBitMask |= 0x01;
      }
    }
    if ( com_num == 4 )
    {
      if ( type == SPV_STLC1000_COM_MODE_RS485 )
      {
        COMBitMask &= 0xF7;
      }
      if ( type == SPV_STLC1000_COM_MODE_RS422 )
      {
        COMBitMask |= 0x08;
      }
      if ( echo )
	  {
        COMBitMask &= 0xFD;
      }
      else
      {
        COMBitMask |= 0x02;
      }             
    }
  }
  else
  {
    ret_code = SPV_STLC1000_INVALID_COM_NUMBER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per impostare i parametri di COM RS485/422 dell'STLC1000
///
/// \date [28.03.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int STLC1KSetCOMParams( int com_num, int baud, int type, bool echo )
{
  int   ret_code    = SPV_STLC1000_NO_ERROR;
  WORD  speed_code  = 0;
  int   offset      = 0;

  // --- Imposto il codice della velocit� COM ---
  if ( com_num == 3 || com_num == 4 )
  {
	offset = (com_num == 3)? 2:4;
	ret_code =  STLC1KOpenIOC( );
    if ( ret_code == SPV_STLC1000_NO_ERROR )
    {
      speed_code = STLC1KGetBaudCode( baud );
      // --- Scrivo il primo byte del codice velocit� ---
	  ret_code = STLC1KWriteIOC( (BYTE) speed_code, IOCHandle, offset ); // COM3: 2 - COM4: 4
      if ( ret_code == SPV_STLC1000_NO_ERROR )
	  {
        // --- Scrivo il secondo byte del codice velocit� ---
		ret_code = STLC1KWriteIOC( (BYTE) ( speed_code >> 8 ), IOCHandle, offset + 1 ); // COM3: 3 - COM4: 5
      }
      // --- Preparo la maschera di bit per la COM ---
      ret_code = STLC1KSetCOMBitMask( com_num, type, echo );
      if ( ret_code == SPV_STLC1000_NO_ERROR )
      {
        // --- Imposto i parametri delle COM ---
		  ret_code = STLC1KWriteIOC( COMBitMask, IOCHandle, SPV_STLC1000_COM_REG_OFFSET );
      }
	  STLC1KCloseIOC( );
    }
  }
  else
  {
    ret_code = SPV_STLC1000_INVALID_COM_NUMBER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per accendere/spegnere uno dei due led di sistema
///
/// \date [28.03.2008]
/// \author Enrico Alborali. Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int STLC1KSetSystemLED( int led, bool on )
{
  int   ret_code    = SPV_STLC1000_NO_ERROR;
//  UCHAR BitMask = 0x00;

  if ( led == SPV_STLC1000_SYSTEM_RED_LED || led == SPV_STLC1000_SYSTEM_GREEN_LED )
  {

   //	ret_code ==  STLC1KOpenIOC( );

	if ( ret_code == SPV_STLC1000_NO_ERROR )
	{
	  // --- LED Rosso ---
	  if ( led == SPV_STLC1000_SYSTEM_RED_LED )
	  {
		if ( on )
		{
		  IOBitMask |= 0x02;
		}
		else
		{
		  IOBitMask &= 0xFD;
		}
	  }
	  // --- LED Verde ---
	  if ( led == SPV_STLC1000_SYSTEM_GREEN_LED )
	  {
		if ( on )
		{
		  IOBitMask |= 0x01;
		}
		else
		{
		  IOBitMask &= 0xFE;
		}
	  }

   //	STLC1KOutPort(0x300,BitMask);
	  // --- Imposto i parametri dei LED ---
		ret_code = STLC1KWriteIOC( IOBitMask, IOCHandle, SPV_STLC1000_IO_REG_OFFSET );
   //	  STLC1KCloseIOC( );
	}
  }
  else
  {
	ret_code = SPV_STLC1000_INVALID_SYSTEM_LED;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per caricare e inizializzare la libreria Jida.dll
///
/// \date [08.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KInitJidaDll( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  // --- Recupero le funzioni dalla DLL ---
  try
  {
    Jida32Library = LoadLibrary( "Jida.dll" );
  }
  catch(...)
  {
    Jida32Library = NULL;
  }
  if ( Jida32Library == NULL )
  {
    ret_code = SPV_STLC1000_JIDA_INIT_FAILURE;
  }
  else
  {
    try
	{
	  _JidaDllInitialize         = ( STLC1K_JidaDllInitialize_PTR         ) GetProcAddress( Jida32Library, "JidaDllInitialize"         );
	  _JidaDllUninitialize       = ( STLC1K_JidaDllUninitialize_PTR       ) GetProcAddress( Jida32Library, "JidaDllUninitialize"       );
	  _JidaBoardOpen             = ( STLC1K_JidaBoardOpen_PTR             ) GetProcAddress( Jida32Library, "JidaBoardOpenA"            );
	  _JidaBoardClose            = ( STLC1K_JidaBoardClose_PTR            ) GetProcAddress( Jida32Library, "JidaBoardClose"            );
	  _JidaWDogSetConfig         = ( STLC1K_JidaWDogSetConfig_PTR         ) GetProcAddress( Jida32Library, "JidaWDogSetConfig"         );
	  _JidaWDogTrigger           = ( STLC1K_JidaWDogTrigger_PTR           ) GetProcAddress( Jida32Library, "JidaWDogTrigger"           );
	  _JidaI2CReadRegister       = ( STLC1K_JidaI2CReadRegister_PTR       ) GetProcAddress( Jida32Library, "JidaI2CReadRegister"       );
	  _JidaI2CWriteRegister      = ( STLC1K_JidaI2CWriteRegister_PTR      ) GetProcAddress( Jida32Library, "JidaI2CWriteRegister"      );
	  _JidaTemperatureGetInfo    = ( STLC1K_JidaTemperatureGetInfo_PTR    ) GetProcAddress( Jida32Library, "JidaTemperatureGetInfo"    );
	  _JidaTemperatureGetCurrent = ( STLC1K_JidaTemperatureGetCurrent_PTR ) GetProcAddress( Jida32Library, "JidaTemperatureGetCurrent" );
	}
	catch(...)
	{
      ret_code = SPV_STLC1000_JIDA_GET_FUNCTION_FAILURE;
    }
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere la libreria Jida.dll
///
/// \date [08.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KClearJidaDll( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
    try
    {
      // --- Chiudo la DLL ---
      if( FreeLibrary( Jida32Library ) == true )
      {
        Jida32Library = NULL;
        // --- Metto a NULL tutti i puntatori alle funzione Jida32 ---
        _JidaDllInitialize    = NULL;
        _JidaDllUninitialize  = NULL;
        _JidaBoardOpen        = NULL;
        _JidaBoardClose       = NULL;
        _JidaWDogSetConfig    = NULL;
        _JidaWDogTrigger      = NULL;
        _JidaI2CReadRegister  = NULL;
      }
      else
      {
        ret_code = SPV_STLC1000_JIDA_CLEAR_FAILURE;
      }
	}
	catch(...)
    {
      ret_code = SPV_STLC1000_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per accedere alla scheda CPU dell'STLC1000
///
/// \date [08.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KOpenCPUBoard( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
    if ( _JidaBoardOpen != NULL )
    {
      try
      {
        if ( _JidaBoardOpen( JIDA_BOARD_CLASS_CPU, 0, 0, &CPUBoardHandler ) == false )
        {
          ret_code = SPV_STLC1000_JIDA_BOARD_OPEN_FAILURE;
        }
	  }
	  catch(...)
      {
        ret_code = SPV_STLC1000_CRITICAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
    }
  }
  else
  {
    ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per terminare l'accesso alla scheda CPU dell'STLC1000
///
/// \date [08.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KCloseCPUBoard( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
    if ( _JidaBoardClose != NULL )
    {
      try
	  {
		if ( _JidaBoardClose( CPUBoardHandler ) == false )
        {
          ret_code = SPV_STLC1000_JIDA_BOARD_CLOSE_FAILURE;
        }
      }
      catch(...)
      {
        ret_code = SPV_STLC1000_CRITICAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
    }
  }
  else
  {
    ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per impostare via software i parametri del Watch Dog
///
/// \date [08.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KSetWatchDog( unsigned int timeout, unsigned int delay )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
    if ( _JidaWDogSetConfig != NULL )
    {
      try
      {
        if ( _JidaWDogSetConfig( CPUBoardHandler, 0, (DWORD)timeout, (DWORD)delay, JWDModeRebootPC ) == false )
        {
          ret_code = SPV_STLC1000_JIDA_WATCHDOG_SET_FAILURE;
        }
      }
      catch(...)
      {
        ret_code = SPV_STLC1000_CRITICAL_ERROR;
      }
    }
    else
    {
      ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
    }
  }
  else
  {
    ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}

//==============================================================================
/// Funzione di trigger Watch Dog
///
/// \date [08.06.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int STLC1KTriggerWatchDog( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
	if ( _JidaWDogTrigger != NULL )
	{
	  try
	  {
		if ( _JidaWDogTrigger( CPUBoardHandler, 0 ) == false )
		{
		  ret_code = SPV_STLC1000_JIDA_WATCHDOG_TRIGGER_FAILURE;
		}
	  }
	  catch(...)
	  {
		ret_code = SPV_STLC1000_CRITICAL_ERROR;
	  }
	}
	else
	{
	  ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
	}
  }
  else
  {
	ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}



//==============================================================================
/// Funzione per recuperare il Serial Number dell'STLC1000
///
/// \date [06.04.2007]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
unsigned long  STLC1KReadSN ( unsigned long * serialNumber )
{
	// ret_code:
	//1 Byte per l'anno
	//1 Byte per la settimana
	//2 Byte per il progressivo
	unsigned long ret_code = SPV_STLC1000_NO_ERROR;

	int iReg;
	BYTE byTemp;
	BYTE byAddr = 0xac&0xFE;  //DEVICE_I2C_UNKNOW_ADDRESS;
	DWORD dwNum = 0; //JidaTypeUnknow

	DWORD dwTemp = 0;


	if ( Jida32Library != NULL )
	{
		if ( _JidaI2CReadRegister != NULL )
		{
			try
			{
				for(iReg = 0; iReg < sizeof(DWORD); iReg++)
				{
					// bit 0 of address should be always 0
					if (!_JidaI2CReadRegister(CPUBoardHandler, dwNum, byAddr, (WORD)iReg, &byTemp)) {
						dwTemp = 0l;
						break;
					}
					dwTemp |= (DWORD)(byTemp << (24 - 8*iReg));
				}
				* serialNumber = dwTemp;
			}
			catch(...)
			{
				ret_code = SPV_STLC1000_CRITICAL_ERROR;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
		}
	}
	else
	{
		ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
	}

	return ret_code;
}



//==============================================================================
/// Funzione per inizializzare la Dll Jida
///
/// \date [28.08.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int STLC1KJidaDllInitialize( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
	if ( _JidaDllInitialize != NULL )
	{
	  try
	  {
		if ( _JidaDllInitialize() == false )
		{
			 ret_code = SPV_STLC1000_JIDA_INITIALIZED_FAILURE;
		}
	  }
	  catch(...)
	  {
		ret_code = SPV_STLC1000_CRITICAL_ERROR;
	  }
	}
	else
	{
	  ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
	}
  }
  else
  {
	ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per deinizializzare la Dll Jida
///
/// \date [28.08.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int STLC1KJidaDllUninitialize( void )
{
  int ret_code = SPV_STLC1000_NO_ERROR;

  if ( Jida32Library != NULL )
  {
	if ( _JidaDllUninitialize != NULL )
	{
	  try
	  {
		if ( _JidaDllUninitialize() == false )
		{
			 ret_code = SPV_STLC1000_JIDA_UNINITIALIZED_FAILURE;
		}
	  }
	  catch(...)
	  {
		ret_code = SPV_STLC1000_CRITICAL_ERROR;
	  }
	}
	else
	{
	  ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
	}
  }
  else
  {
	ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
  }

  return ret_code;
}


//==============================================================================
/// Funzione per regolare la velocit� di rotazione della ventola destra
/// dell'STLC1000
///
/// \date [26.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
unsigned long STLC1KSetPWMRightFan(int pwmPercent)
{
	unsigned long ret_code = SPV_STLC1000_NO_ERROR;

	int		iPWMDxFunReg   		= 0x5B;     // il registro iPWMDxFunReg imposta la velocit� di rotazione della ventola in funzione del valore che va da
											// 0x00 (ventola spenta) a 0xFF (ventola accesa alla massima veloct�)
	BYTE	byAddr				= 0x54;		//DEVICE_I2C_SMB_ADDRESS;
	DWORD	dwNum				= 1;		//JidaTypeSMB
	BYTE	PWMValue;                		//PWMValue: valore che va da 0x00 (ventola spenta) a 0xFF (ventola accesa alla massima veloct�)


	if ((pwmPercent < 0) || (pwmPercent > 100))
	{
		ret_code = SPV_STLC1000_PWM_OUT_OF_RANGE;
	}

	PWMValue = ((255 * pwmPercent) / 100);

	if (ret_code == SPV_STLC1000_NO_ERROR)
	{

		if ( Jida32Library != NULL )
		{
			if ( _JidaI2CWriteRegister != NULL )
			{
				try
				{
					if (!_JidaI2CWriteRegister(CPUBoardHandler, dwNum, byAddr, (WORD)iPWMDxFunReg, PWMValue)) {
						ret_code = SPV_STLC1000_JIDA_WRITE_PWMDXFUN_REGISTER_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_STLC1000_CRITICAL_ERROR;
				}
			}
			else
			{
				ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
		}
	}
	return ret_code;
}


//==============================================================================
/// Funzione per regolare la velocit� di rotazione della ventola sinistra
/// dell'STLC1000
///
/// \date [26.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
unsigned long STLC1KSetPWMLeftFan(int pwmPercent)
{
	unsigned long ret_code = SPV_STLC1000_NO_ERROR;

	int		iPinReg   		= 0x4D; // iPinReg = 0x25 (00100101) =>  Pin come output e ventola accesa con velocit� data dal valore PWM,
									// iPinReg = 0x05 (00000101) =>  Pin come output e ventola spenta

	int 	iPWMEnabledReg	= 0x5C; // iPWMEnabledReg = 0x19 (00011001) => PWM � abilitato e la velocit� � determinata dal valore del registro 0x5A
									// iPWMEnabledReg = 0x11 (00010001) => PWM � disabilitato e la velocit� � massima (default)

	int 	iPWMSxFunReg	= 0x5A; // il registro iPWMSxFunReg imposta la velocit� di rotazione della ventola in funzione del valore che va da
									// 0x00 (ventola spenta) a 0xFF (ventola accesa alla massima veloct�)

	BYTE	byAddr	   		= 0x54;	//DEVICE_I2C_SMB_ADDRESS;
	DWORD	dwNum	   		= 1;   	//JidaTypeSMB

	BYTE	PinValue		= 0x25;
	BYTE	PWMEnabledValue = 0x19;

	BYTE	PWMValue;                //PWMValue: valore che va da 0x00 (ventola spenta) a 0xFF (ventola accesa alla massima veloct�)


	if ((pwmPercent < 0) || (pwmPercent > 100))
	{
		ret_code = SPV_STLC1000_PWM_OUT_OF_RANGE;
	}

	PWMValue = ((255 * pwmPercent) / 100);

	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		if ( Jida32Library != NULL )
		{
			if ( _JidaI2CWriteRegister != NULL )
			{
				try
				{
					if (!_JidaI2CWriteRegister(CPUBoardHandler, dwNum, byAddr, (WORD)iPinReg, PinValue))
					{
						ret_code = SPV_STLC1000_JIDA_WRITE_PINSXFUN_REGISTER_FAILURE;
					}
					if (ret_code == SPV_STLC1000_NO_ERROR)
					{
						if (!_JidaI2CWriteRegister(CPUBoardHandler, dwNum, byAddr, (WORD)iPWMEnabledReg, PWMEnabledValue))
						{
							ret_code = SPV_STLC1000_JIDA_WRITE_PWMENSXFUN_REGISTER_FAILURE;
						}
					}
					if (ret_code == SPV_STLC1000_NO_ERROR)
					{
						if (!_JidaI2CWriteRegister(CPUBoardHandler, dwNum, byAddr, (WORD)iPWMSxFunReg, PWMValue))
						{
							ret_code = SPV_STLC1000_JIDA_WRITE_PWMSXFUN_REGISTER_FAILURE;
						}
					}
				}
				catch(...)
				{
					ret_code = SPV_STLC1000_CRITICAL_ERROR;
				}
			}
			else
			{
				ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
		}
	}
	return ret_code;
}

//==============================================================================
/// Funzione per rilevare la temperature in gradi Celsius della MotherBoard
/// dell'STLC1000
///
/// \date [28.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
unsigned long STLC1KGetMBTemperature(BYTE * temperature)
{
	unsigned long ret_code = SPV_STLC1000_NO_ERROR;

	WORD iReg = 0x27;
	BYTE byTemp;
	BYTE byAddr = 0x54&0xFE;  //DEVICE_I2C_UNKNOW_ADDRESS;
	DWORD dwNum = 1;
	DWORD dwTemp = 0;


	if ( Jida32Library != NULL )
	{
		if ( _JidaI2CReadRegister != NULL )
		{
			try
			{
				// bit 0 of address should be always 0
				if (_JidaI2CReadRegister(CPUBoardHandler, dwNum, byAddr, iReg, &byTemp))
				{
					* temperature = byTemp;
				}
				else
				{
					* temperature = 0;
				}
			}
			catch(...)
			{
				ret_code = SPV_STLC1000_CRITICAL_ERROR;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
		}
	}
	else
	{
		ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per settare le info della Jida sulla temperatura dell'STLC1000
///
/// \date [05.05.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
unsigned long STLC1KSetInfoTemperature( void )
{
	unsigned long ret_code =  SPV_STLC1000_NO_ERROR;

	DWORD dwNum = 0;
	BOOL bLettoInfo=TRUE;


	JidaTempInfo.dwSize = sizeof(JIDATEMPERATUREINFO);

	try
	{
	if ( Jida32Library != NULL )
	{
		if ( _JidaTemperatureGetInfo != NULL )
		{
			try
			{
				bLettoInfo=_JidaTemperatureGetInfo(CPUBoardHandler, dwNum, &JidaTempInfo);

				if (!bLettoInfo)
				{
					ret_code = SPV_STLC1000_JIDA_FUNCTION_FAILURE;
				}
			}
			catch(...)
			{
				ret_code = SPV_STLC1000_CRITICAL_ERROR;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
		}
	}
	else
	{
		ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
	}
	}
	catch(...)
	{
		ret_code = SPV_STLC1000_CRITICAL_ERROR;
	}
	return ret_code;
}


//==============================================================================
/// Funzione per rilevare la temperature in gradi Celsius della CPU dell'STLC1000
///
/// \date [05.05.2008]
/// \author Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
unsigned long STLC1KGetCPUTemperature(double * temperature)
{
	unsigned long ret_code =  SPV_STLC1000_NO_ERROR;

	double dbTemperature;
	DWORD dwNum = 0;
	DWORD dwSetting, dwStatus;

	BOOL bLettoInfo=TRUE;
	BOOL bLettoCurrent=TRUE;

	try
	{
		if ( Jida32Library != NULL )
		{
			if ( _JidaTemperatureGetCurrent != NULL )
			{
				try
				{
					bLettoCurrent=_JidaTemperatureGetCurrent(CPUBoardHandler,dwNum, &dwSetting, &dwStatus);
					if (bLettoCurrent)
					{
						if(JidaTempInfo.dwType == SPV_STLC1000_JIDA_TEMP_CPU)
						{
							dbTemperature =(double)(signed long) dwSetting;
							* temperature =  dbTemperature/1000.0;
						}
					}
					else
					{
						ret_code = SPV_STLC1000_JIDA_FUNCTION_FAILURE;
					}
				}
				catch(...)
				{
					ret_code = SPV_STLC1000_CRITICAL_ERROR;
				}
			}
			else
			{
				ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
		}
	}
	catch(...)
	{
		ret_code = SPV_STLC1000_CRITICAL_ERROR;
	}
	return ret_code;
}


//==============================================================================
/// Funzione per leggere il valore su un registro dell'STLC1000 il cui indirizzo
/// viene passato come parametro
///
/// \date [04.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
unsigned long STLC1KReadRegister (BYTE regAddr,WORD iReg,BYTE * regValue)
{
	unsigned long ret_code = SPV_STLC1000_NO_ERROR;

	BYTE byTemp;
	DWORD dwNum = 1;

	if ( Jida32Library != NULL )
	{
		if ( _JidaI2CReadRegister != NULL )
		{
			try
			{
				// bit 0 of address should be always 0
				if (!_JidaI2CReadRegister(CPUBoardHandler, dwNum, regAddr&0xFE, iReg, &byTemp)) {
					ret_code = SPV_STLC1000_JIDA_READ_REGISTER_FAILURE;
				}
				else
				{
					* regValue = byTemp;
				}
			}
			catch(...)
			{
				ret_code = SPV_STLC1000_CRITICAL_ERROR;
			}
		}
		else
		{
			ret_code = SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND;
		}
	}
	else
	{
		ret_code = SPV_STLC1000_JIDA_NOT_INITIALIZED;
	}

	return ret_code;
}

int STLC1KGetFBWFVirtualSize( char diskLetter )
{
	int ret_code = SPV_STLC1000_NO_ERROR;

	try
	{

	}
	catch (...)
	{
		ret_code = SPV_STLC1000_FBWF_GENERIC_FAILURE;
    }

	return SPV_STLC1000_NO_ERROR;
}

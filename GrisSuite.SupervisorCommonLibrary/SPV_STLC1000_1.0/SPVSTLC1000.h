//==============================================================================
// Telefin Supervisor STLC1000 Module 1.0
//------------------------------------------------------------------------------
// Header Modulo di gestione dispositivi STLC1000 (SPVSTLC1000.h)
// Progetto:  Telefin Supervisor 3.0
//
// Revisione:	0.09 (13.09.2005 -> 28.03.2008)
//
// Copyright: 2004-2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, C++ Builder 2006
// Autori:	  Enrico Alborali (alborali@telefin.it)
//			  Mario Ferro	  (mferro@deltasistemi-vr.it)
// Note:      richiede SPVSTLC1000.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [13.09.2005]:
// - Prima versione del modulo.
// 0.02 [14.09.2005]:
// - Modificata la funzione STLC1KOpenIOC.
// - Modificata la funzione STLC1KCloseIOC.
// - Modificata la funzione STLC1KGetBaudCode.
// - Aggiunta la funzione STLC1KSetCOMBitMask.
// - Modificata la funzione STLC1KSetCOMParams.
// - Aggiunta la funzione STLC1KSetSystemLED.
// 0.03 [15.09.2005]:
// - Modificata la funzione STLC1KSetCOMParams.
// - Modificata la funzione STLC1KSetSystemLED.
// 0.04 [05.06.2006]:
// - Aggiunta la funzione STLC1KInitJidaDll.
// - Aggiunta la funzione STLC1KClearJidaDll.
// - Aggiunta la funzione STLC1KOpenCPUBoard.
// - Aggiunta la funzione STLC1KCloseCPUBoard.
// - Aggiunta la funzione STLC1KSetWatchDog.
// - Aggiunta la funzione STLC1KTriggerWatchDog.
// 0.05 [08.06.2006]:
// - Modificata la funzione STLC1KInitJidaDll.
// - Modificata la funzione STLC1KClearJidaDll.
// - Modificata la funzione STLC1KOpenCPUBoard.
// - Modificata la funzione STLC1KCloseCPUBoard.
// - Modificata la funzione STLC1KSetWatchDog.
// - Modificata la funzione STLC1KTriggerWatchDog.
// 0.06 [02.08.2006]:
// - Corretto bug nella funzione STLC1KSetCOMParams.
// 0.07 [22.11.2006]:
// - Aggiunta la funzione STLC1KReadSN.
// - Aggiunta la funzione STLC1KJidaDllInitialize.
// - Aggiunta la funzione STLC1KJidaDllUninitialize.
// 0.08 [06.04.2007]:
// - Eliminata la funzione STLC1KSetOnOffRightFan.
// - Eliminata la funzione STLC1KSetOnOffLeftFan.
// - Aggiunta la funzione STLC1KGetCPUTemperature.
// - Aggiunta la funzione STLC1KSetPWMRightFan.
// - Aggiunta la funzione STLC1KSetPWMLeftFan.
// - Aggiunta la funzione STLC1KReadRegister.
// - Modificata la funzione STLC1KReadSN.
// 0.09 [23.03.2008]:
// - Aggiunta la funzione STLC1KGetCPUTemperature.
// - Aggiunta la funzione STLC1KGetMBTemperature.
// - Modificata la funzione STLC1KSetSystemLED.
// - Modificata la funzione STLC1KSetCOMParams.




//==============================================================================
#ifndef SPVSTLC1000H
#define SPVSTLC1000H
//------------------------------------------------------------------------------

#include <windows.h>
#include "Jida.h"

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SPV_STLC1000_COM_GPDDEVPATH			"\\\\.\\GpdDev"
#define SPV_STLC1000_SYSTEM_RED_LED     1
#define SPV_STLC1000_SYSTEM_GREEN_LED   0
#define SPV_STLC1000_IO_REG_OFFSET      0
#define SPV_STLC1000_COM_REG_OFFSET     1
#define SPV_STLC1000_JIDA_TEMP_CPU		0

//==============================================================================
// Codici di velocit� COM
//------------------------------------------------------------------------------
#define SPV_STLC1000_COM_SPEED_300		0x81b0
#define SPV_STLC1000_COM_SPEED_600		0x40d8
#define SPV_STLC1000_COM_SPEED_1200	 	0x206c
#define SPV_STLC1000_COM_SPEED_2400	 	0x1036
#define SPV_STLC1000_COM_SPEED_4800		0x081b
#define SPV_STLC1000_COM_SPEED_9600	 	0x040d
#define SPV_STLC1000_COM_SPEED_14400	0x02b4
#define SPV_STLC1000_COM_SPEED_19200	0x0207
#define SPV_STLC1000_COM_SPEED_38400	0x0103
#define SPV_STLC1000_COM_SPEED_56000	0x00b0
#define SPV_STLC1000_COM_SPEED_57600	0x00ad
#define SPV_STLC1000_COM_SPEED_115200 	0x0056

//==============================================================================
// Codici di modalit� COM
//------------------------------------------------------------------------------
#define SPV_STLC1000_COM_MODE_RS485     42485
#define SPV_STLC1000_COM_MODE_RS422     42422

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SPV_STLC1000_NO_ERROR                       		0
#define SPV_STLC1000_CRITICAL_ERROR                 		259000
#define SPV_STLC1000_IOC_OPEN_FAILURE               		259001
#define SPV_STLC1000_IOC_NOT_OPENED                 		259002
#define SPV_STLC1000_INVALID_COM_NUMBER             		259003
#define SPV_STLC1000_INVALID_SYSTEM_LED             		259004
#define SPV_STLC1000_IOC_WRITE_FAILURE              		259005
#define SPV_STLC1000_JIDA_INIT_FAILURE              		259006
#define SPV_STLC1000_JIDA_NOT_INITIALIZED           		259007
#define SPV_STLC1000_JIDA_CLEAR_FAILURE             		259008
#define SPV_STLC1000_JIDA_GET_FUNCTION_FAILURE      		259009
#define SPV_STLC1000_JIDA_BOARD_OPEN_FAILURE        		259010
#define SPV_STLC1000_JIDA_FUNCTION_NOT_FOUND        		259011
#define SPV_STLC1000_JIDA_BOARD_CLOSE_FAILURE       		259012
#define SPV_STLC1000_JIDA_WATCHDOG_SET_FAILURE      		259013
#define SPV_STLC1000_JIDA_WATCHDOG_TRIGGER_FAILURE  		259014
#define SPV_STLC1000_JIDA_INITIALIZED_FAILURE       		259015
#define SPV_STLC1000_JIDA_UNINITIALIZED_FAILURE     		259016
#define SPV_STLC1000_JIDA_WRITE_PWMDXFUN_REGISTER_FAILURE	259017
#define SPV_STLC1000_JIDA_WRITE_PINSXFUN_REGISTER_FAILURE	259018
#define SPV_STLC1000_JIDA_WRITE_PWMENSXFUN_REGISTER_FAILURE	259019
#define SPV_STLC1000_JIDA_WRITE_PWMSXFUN_REGISTER_FAILURE	259020
#define SPV_STLC1000_JIDA_FUNCTION_FAILURE					259021
#define SPV_STLC1000_JIDA_READ_REGISTER_FAILURE				259022
#define SPV_STLC1000_PWM_OUT_OF_RANGE						259023
#define SPV_STLC1000_FBWF_GENERIC_FAILURE					259024
//------------------------------------------------------------------------------

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

// Funzione per aprire l'accesso agli IO dell'STLC1000
int   STLC1KOpenIOC         ( void );
// Funzione per chiudere l'accesso agli IO dell'STLC1000
int   STLC1KCloseIOC        ( void );
// Funzione per calcolare il codice di velocit� per le COM dell'STLC1000
WORD  STLC1KGetBaudCode     ( int com_baud );
// Funzione per scrivere in un registro degli IO dell'STLC1000
int   STLC1KWriteIOC        ( BYTE data, HANDLE dev_handle, int offset );
// Funzione per leggere da un registro degli IO dell'STLC1000
int   STLC1KReadIOC         ( void );
// Funzione per impostare la maschera di bit dei parametri delle COM
int   STLC1KSetCOMBitMask   ( int com_num, int type, bool echo );
// Funzione per impostare i parametri di COM RS485/422 dell'STLC1000
int   STLC1KSetCOMParams    ( int com_num, int baud, int type, bool echo );
// Funzione per accendere/spegnere uno dei due led di sistema
int   STLC1KSetSystemLED    ( int led, bool on );
// Funzione per caricare e inizializzare la libreria Jida.dll
int   STLC1KInitJidaDll     ( void );
// Funzione per chiudere la libreria Jida.dll
int   STLC1KClearJidaDll    ( void );
// Funzione per accedere alla scheda CPU dell'STLC1000
int   STLC1KOpenCPUBoard    ( void );
// Funzione per terminare l'accesso alla scheda CPU dell'STLC1000
int   STLC1KCloseCPUBoard   ( void );
// Funzione per impostare via software i parametri del Watch Dog
int   STLC1KSetWatchDog     ( unsigned int timeout, unsigned int delay );
// Funzione di trigger Watch Dog
int   STLC1KTriggerWatchDog ( void );
// Funzione per recuperare il Serial Number dell'STLC1000
unsigned long  STLC1KReadSN ( unsigned long * serialNumber );
// Funzione per inizializzare la Dll Jida
int STLC1KJidaDllInitialize( void );
// Funzione per deinizializzare la Dll Jida
int STLC1KJidaDllUninitialize( void );
// Funzione per regolare la velocit� di rotazione della ventola destra dell'STLC1000
unsigned long STLC1KSetPWMRightFan(int pwmPercent);
// Funzione per regolare la velocit� di rotazione della ventola sinistra dell'STLC1000
unsigned long STLC1KSetPWMLeftFan(int pwmPercent);
// Funzione per settare le info della Jida sulla temperatura dell'STLC1000
unsigned long STLC1KSetInfoTemperature( void );
// Funzione per rilevare la temperature in gradi Celsius della CPU dell'STLC1000
unsigned long STLC1KGetCPUTemperature(double * temperature);
// Funzione per leggere il valore su un registro dell'STLC1000
unsigned long STLC1KReadRegister(BYTE regAddr,WORD iReg,BYTE * regValue);
// Funzione per rilevare la temperature in gradi Celsius della MotherBoard dell'STLC1000
unsigned long STLC1KGetMBTemperature(BYTE * temperature);



#endif


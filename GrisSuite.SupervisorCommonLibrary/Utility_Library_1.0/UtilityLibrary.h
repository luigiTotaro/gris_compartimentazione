//==============================================================================
// Telefin Utility Library 1.0
//------------------------------------------------------------------------------
// Header Libreria di utlit�  (UtilityLibrary.h)
// Progetto:	Telefin Common Library 1.0
//
// Versione:	0.02 (21.09.2007 -> 22.08.2012)
//
// Copyright:	2007-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//==============================================================================
#ifndef UtilityLibraryH
#define UtilityLibraryH

//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define UL_NO_ERROR 	                0
#define	UL_FUNCTION_EXCEPTION        7900
#define UL_INVALID_LOG_MANAGER		 7901
#define UL_INVALID_STRING			 7902

// Funzione per ottenere lo Unix Time
long			ULGetUnixTime			( void );
// Funzione per accodare una stringa ad un testo
char * 			ULAddText				( char * oldtxt, char * newtxt );
// Funzione che fa il free di una stringa e la mette a NULL
int 			ULFreeAndNullString		( char ** stringAddress );
// Funzione per copiare stringhe (costanti)
char * 			ULStrCpy         		( char * dest, const char * src );
// Funzione per copiare stringhe
char * 			ULStrCpy         		( char * dest, char * src );
// Funzione per mettere il contenuto di un file ASCII in una stringa
char * 			ULReadASCIIFile			( char * file_path );
// Funzione per sostituire un carattere in una stringa
int 			ULStrSubstitute			( char * str, char search, char subst );
#endif

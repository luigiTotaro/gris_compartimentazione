//==============================================================================
// Telefin Utility Library 1.0
//------------------------------------------------------------------------------
// Libreria di utlit�  (UtilityLibrary.cpp)
// Progetto:	Telefin Common Library 1.0
//
// Versione:	0.02 (21.09.2007 -> 22.08.2012)
//
// Copyright:	2007-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//==============================================================================
#pragma hdrstop
#include "UtilityLibrary.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "cnv_lib.h"
//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#include <system.hpp>
#pragma package(smart_init)
#else
#include <stdlib.h>
#endif
//------------------------------------------------------------------------------

#define MAX_ASCII_LINE	1024

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
//SPV_EVENTLOG_MANAGER  * log_manager; 	// --- Definizione di un EventLog manager ---

//==============================================================================
/// Funzione per ottenere lo UNIX TIME.
/// Lo UNIX TIME � il numero di secondi trascorsi dal 1 gennaio 1970 alle ore
/// 00:00:00 GMT.
/// In caso di errore restituisce 0.
///
/// \date [14.04.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
long ULGetUnixTime( void )
{
	long  lUnixTime = 0; // Unix Time long

	lUnixTime = (long)time( NULL ); // Ricavo l'UNIX TIME

	return lUnixTime;
}

//==============================================================================
/// Funzione per accodare una stringa ad un testo
///
/// \date [20.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char * ULAddText( char * oldtxt, char * newtxt )
{
  char * text         = NULL;
  int    old_len      = 0   ;
  int    new_len      = 0   ;
  int    offset       = 0   ;

  if ( newtxt != NULL )
  {
	new_len = strlen( newtxt );
  }
  else
  {
	new_len = 0;
  }
  if ( oldtxt != NULL )
  {
	old_len = strlen( oldtxt );
  }
  else
  {
	old_len = 0;
  }
  if ( ( old_len + new_len ) > 0 )
  {
	text = (char*)malloc( sizeof(char) * ( old_len + new_len + 1 ) ); // -MALLOC
  }
  if ( old_len > 0 )
  {
	memcpy( text, oldtxt, sizeof(char) * old_len );
	text[ old_len ]  = '\0';
	offset = old_len;
	try
	{
	  free( oldtxt ); // -FREE
	}
	catch(...)
	{
	  /* TODO -oEnrico -cError : Gestione dell'errore critico */
	}
  }
  if ( new_len > 0 )
  {
	memcpy( &text[offset], newtxt, new_len );
	text[ offset + new_len ] = '\0';
  }

  return text;
}

//==============================================================================
/// Funzione che fa il free di una stringa e la mette a NULL
///
/// \date [25.09.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int ULFreeAndNullString(char ** stringAddress)
{
	int ret_code = UL_NO_ERROR;

	try
	{
		if ((stringAddress != NULL)  && (*stringAddress != NULL))
		{
			free(*stringAddress);
			*stringAddress = NULL;
		}
	}
	catch(...)
	{
		ret_code = UL_FUNCTION_EXCEPTION;
	}
	return ret_code;
}

//==============================================================================
/// Funzione per copiare stringhe (costanti)
///
/// \date [17.10.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * ULStrCpy( char * dest, const char * src )
{
  char * res = NULL;
  int src_len = 0;

  if ( dest != NULL ) free( dest );
  if ( src != NULL )
  {
	src_len = strlen( src );
	res = (char*)malloc( sizeof(char) * ( src_len + 1 ) );
	strcpy( res, src );
  }
  else
  {
	res = NULL;
  }

  return res;
}

//==============================================================================
/// Funzione per copiare stringhe
///
/// \date [02.09.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
char * ULStrCpy( char * dest, char * src )
{
  char * res = NULL;
  int src_len = 0;

  if (dest != NULL) free(dest);
  if (src != NULL)
  {
	src_len = strlen(src);
	res = (char*)malloc(sizeof(char)*(src_len+1));
	strcpy(res,src);
  }
  else
  {
	res = NULL;
  }

  return res;
}

//==============================================================================
/// Funzione per mettere il contenuto di un file ASCII in una stringa
///
/// \date [26.11.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char * ULReadASCIIFile ( char * file_path )
{
	FILE * 		source_file = NULL;
	char *      text = NULL;
	char * 		line = NULL;

	try
	{
		if ( file_path != NULL )
		{
			if ( ( source_file = fopen( file_path, "rt" ) ) != NULL )
			{
				line = (char *) malloc( sizeof(char)*(MAX_ASCII_LINE+1)); // Alloco la memoria
				while(fgets(line, MAX_ASCII_LINE, source_file))
				{
					text = ULAddText(text,line);
				}
				free(line);
				// Chiudo il file
				fclose(source_file);
			}
		}
	}
	catch (...)
	{
		try
		{
			if (text != NULL)
			{
				free(text);
			}

			if (line != NULL)
			{
				free(line);
			}
		}
		catch(...)
		{

		}
		text = NULL;
	}
	return text;
}


//==============================================================================
/// Funzione per sostituire un carattere in una stringa
///
/// \date [26.11.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int ULStrSubstitute ( char * str, char search, char subst )
{
	int ret_code = UL_NO_ERROR;
	int str_len = 0;
	int i = 0;

	try
	{
		if (str != NULL)
		{
			str_len = strlen(str);
			for (i = 0; i < (str_len); i++)
			{
				if (str[i] == search)
				{
					str[i] = subst;
				}
			}
		}
		else
		{
        	ret_code = UL_INVALID_STRING;
        }
	}
	catch(...)
	{
		ret_code = UL_FUNCTION_EXCEPTION;
	}

	return ret_code;
}


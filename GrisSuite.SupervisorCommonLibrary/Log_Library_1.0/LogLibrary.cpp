//==============================================================================
// Telefin Log Library 1.0
//------------------------------------------------------------------------------
// Modulo di gestione Log (LogLibrary.cpp)
// Progetto:	Telefin Common Library 1.0
//
// Versione:	0.01 (22.08.2012 -> 22.08.2012)
//
// Copyright:	2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//==============================================================================
#pragma hdrstop
#include "LogLibrary.h"
#include "UtilityLibrary.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "SPVEventLog.h"
#include "SPVConfig.h"
#include "cnv_lib.h"
//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#include <system.hpp>
#pragma package(smart_init)
#else
#include <stdlib.h>
#endif
//------------------------------------------------------------------------------

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
SPV_EVENTLOG_MANAGER  * log_manager; 	// --- Definizione di un EventLog manager ---

//==============================================================================
/// Funzione per creare un file di log di utilizzo comune
///
/// \date [31.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int LLCreateLogFile( void )
{
	int						ret_code = UL_NO_ERROR;
	char *					logPath = NULL;
	char * 					logFileName = NULL;
	char * 					logTime = NULL;

	try
	{


//		logFileName = ULAddText(logFileName,"SPVServer_Log_Manager_");
//		logTime = cnv_UInt32ToCharP(ULGetUnixTime());
//		logFileName = ULAddText(logFileName,logTime);
//		logFileName = ULAddText(logFileName,".log");

        	logFileName = ULAddText(logFileName,"SPVServer_Log_Manager");
//		logTime = cnv_UInt32ToCharP(ULGetUnixTime());
//		logFileName = ULAddText(logFileName,logTime);
		logFileName = ULAddText(logFileName,".log");


                logPath = SPVGetPath( logFileName );

		// --- Creazione del nuovo EventLog manager ---
		log_manager = SPVNewEventLog( SPV_EVENTLOG_TYPE_ASCII_FILE, SPV_EVENTLOG_TEMPLATE_DATETIME, logPath );

		ULFreeAndNullString(&logFileName);
		ULFreeAndNullString(&logTime);
		ULFreeAndNullString(&logPath);

		if ( log_manager != NULL )
		{
			// --- Apertura l'EventLog Manager creato ---
			ret_code = SPVOpenEventLog( log_manager->ID );
		}
		else
		{
			ret_code = UL_INVALID_LOG_MANAGER;
		}
	}
	catch(...)
	{
		ret_code = UL_FUNCTION_EXCEPTION;
	}

	return ret_code;

}

//==============================================================================
/// Funzione per eliminare il file di log di utilizzo comune
///
/// \date [31.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int LLDestroyLogFile( void )
{
	int	ret_code = UL_NO_ERROR;

	try
	{
		if ( log_manager != NULL )
		{
			if (log_manager->Opened)
			{
				ret_code = SPVCloseEventLog( log_manager->ID );
			}

			if ( (ret_code == UL_NO_ERROR) && ( log_manager != NULL ) )
			{
				ret_code = SPVDeleteEventLog( log_manager->ID );
			}
		}
		else
		{
			ret_code = UL_INVALID_LOG_MANAGER;
		}
	}
	catch(...)
	{
		ret_code = UL_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aggiungere una riga al file di log di utilizzo comune
///
/// \date [31.10.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int LLAddLogFile( char * msg )
{
	int	ret_code = UL_NO_ERROR;

	try
	{
		if ( log_manager != NULL )
		{

			if (log_manager->Opened)
			{
				SPVPrintEventLog( log_manager->ID, msg, strlen(msg), 0 );
			}
		}
		else
		{
			ret_code = UL_INVALID_LOG_MANAGER;
		}
	}
	catch(...)
	{
		ret_code = UL_FUNCTION_EXCEPTION;
	}

	return ret_code;
}
//==============================================================================
// Telefin Log Library 1.0
//------------------------------------------------------------------------------
// Header Modulo di gestione Log (LogLibrary.h)
// Progetto:	Telefin Common Library 1.0
//
// Versione:	0.01 (22.08.2012 -> 22.08.2012)
//
// Copyright:	2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//==============================================================================


#ifndef LogLibraryH
#define LogLibraryH


//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define LL_NO_ERROR 	                0
#define	LL_FUNCTION_EXCEPTION        9900
#define LL_INVALID_LOG_MANAGER		 9901
#define LL_INVALID_STRING			 9902

// Funzione per creare un file di log di utilizzo comune
int 			LLCreateLogFile			( void	);
// Funzione per eliminare il file di log di utilizzo comune
int 			LLDestroyLogFile		( void	);
// Funzione per aggiungere una riga al file di log di utilizzo comune
int 			LLAddLogFile			( char * msg );

#endif

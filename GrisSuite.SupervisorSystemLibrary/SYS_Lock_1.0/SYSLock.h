//==============================================================================
// Telefin System Lock Module 1.0
//------------------------------------------------------------------------------
// Header Modulo Lock (per S.O. Microsoft Windows 2000/XP/XPe/WePoS (SYSLock.h)
// Progetto:	Telefin System Library 1.0
//
// Revisione:	0.01 (21.08.2012 -> 21.08.2012)
//
// Copyright:	2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//				Paolo Colli (paolo.colli@telefin.it)
//==============================================================================
#ifndef SYSLockH
#define SYSLockH
#include <windows.h>

//==============================================================================
// Codici per la verifica della validita'
//------------------------------------------------------------------------------
#define SYS_LOCK_VALIDITY_CHECK_CODE		2620009938

//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define SYS_LOCK_NO_ERROR					0
#define SYS_LOCK_FUNCTION_EXCEPTION			4157
#define SYS_LOCK_ALLOCATION_FAILURE			4159
#define SYS_LOCK_FREE_FAILURE				4160
#define SYS_LOCK_POSITION_OUT_OF_RANGE		4162
#define SYS_LOCK_INVALID_LOCK				4168
#define SYS_LOCK_INVALID_LOCK_PTR			4169
#define SYS_LOCK_UNINTIALIZED_LOCK			4170
#define SYS_LOCK_LOCKED_LOCK				4171
#define SYS_LOCK_UNLOCKED_LOCK				4172
#define SYS_LOCK_LIST_FULL					4173
#define SYS_LOCK_LIST_REMOVE_FAILURE		4174
#define SYS_LOCK_LIST_EMPTY					4175

//==============================================================================
/// Struttura dati per un lock
///
/// \date [04.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SYS_LOCK
{
	unsigned __int32        VCC					; // Validity Check Code
	CRITICAL_SECTION		CriticalSection		; // Descrittore sezione critica
	char 					Name[65]			; // Nome del lock
	bool					Initialized			; // Flag inizalizzazione
	bool					Locked				; // Flag lockato
	char					Label[65]			; // Etichetta del locker
	long					LockDateTime		; // DataOra del lock
	int						LockCounter			; // Contatore del numero dei lock effettuati
	bool					PreLock				; // Flag indicante fase precedente al lock
}
SYS_LOCK;

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
extern	SYS_LOCK **			LockList;
extern	unsigned int		LockListMax;
extern	unsigned int		LockListCount;

//==============================================================================
// Funzioni per la gestione dei lock
//------------------------------------------------------------------------------
// Funzione per verificare la validita' di un Lock
bool			SYSIsValidLock			( SYS_LOCK * lock );
// Funzione per inizializzare un lock
int				SYSInitLock				( SYS_LOCK * lock, char * lockName );
// Funzione per cancellare un lock
int				SYSClearLock			( SYS_LOCK * lock );
// Funzione per chiudere un lock
int				SYSLock					( SYS_LOCK * lock, char * label );
// Funzione per aprire un lock
int				SYSUnLock				( SYS_LOCK * lock );
// Funzione per sapere se un lock e' inizializzato
bool			SYSIsLockInitialized	( SYS_LOCK * lock );
// Funzione per sapere se un lock e' chiuso
bool			SYSIsLocked				( SYS_LOCK * lock );
// Funzione per estrarre (malloc) l'etichetta di un lock
char *			SYSExtractLockLabel		( SYS_LOCK * lock );
// Funzione per estrarre (malloc) il nome di un lock
char * 			SYSExtractLockName		( SYS_LOCK * lock );
// Funzione per sapere il numero di chiusure attive su un lock
int 			SYSGetLockCounter		( SYS_LOCK * lock );
// Funzione per sapere se si � in fase di PreLock
bool 			SYSIsPreLock			( SYS_LOCK * lock );
// Funzione per accedere alla sezione critica della lista dei lock
// Se LockList non � inizializzata la funzione viene ignorata
int				SYSLockLockList			( char * label );
// Funzione per uscire dalla sezione critica della lista dei lock
// Se LockList non � inizializzata la funzione viene ignorata
int				SYSUnlockLockList		( void );
// Funzione per inizializzare la variabile globale SYSLockNess e la lista
// globale delle variabili lock di tipo SYS_LOCK
int				SYSInitLockList			( unsigned int max_lock );
// Funzione per cancellare la variabile globale SYSLockNess e la lista
// globale dei delle variabili "lock" di tipo SYS_LOCK
int 			SYSClearLockList		( void );

#endif

//==============================================================================
// Telefin System Lock Module 1.0
//------------------------------------------------------------------------------
// Modulo Lock (per S.O. Microsoft Windows 2000/XP/XPe/WePoS (SYSLock.cpp)
// Progetto:	Telefin System Library 1.0
//
// Revisione:	0.01 (21.08.2012 -> 21.08.2012)
//
// Copyright:	2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007,
//				Microsoft Visual C++ 2008
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//				Paolo Colli (paolo.colli@telefin.it)
//==============================================================================
#pragma hdrstop
#include "SYSLock.h"
#include "UtilityLibrary.h"
#include <stdio.h>
//------------------------------------------------------------------------------
#ifdef __BORLANDC__
#pragma package(smart_init)
#endif
//------------------------------------------------------------------------------

//==============================================================================
// Variabili globali
//------------------------------------------------------------------------------
static	SYS_LOCK	SYSLockNess;
SYS_LOCK **			LockList;
unsigned int		LockListMax;
unsigned int		LockListCount;

//==============================================================================
/// Funzione per inizializzare la variabile globale SYSLockNess e la lista
/// globale delle variabili lock di tipo SYS_LOCK
/// Opzionalemente si pu� passare il numero massimo di lock per la lista
/// altrimenti il valore default e' 100. Il valore di default e' selezionabile
/// passando max_lock = 0
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSInitLockList( unsigned int max_lock )
{
	int ret_code = SYS_LOCK_NO_ERROR;

	try
	{
		LockListMax = ( max_lock > 0 )? max_lock : 100; // Se passo 0 uso il valore di default (100)
		// --- Alloco la lista ---
		LockList = (SYS_LOCK**)malloc( sizeof(SYS_LOCK*) * LockListMax ); // -MALLOC
		// --- Resetto la lista appena allocata ---
		memset( LockList, 0, sizeof(SYS_LOCK*) * LockListMax );

		if ( !(LockList != NULL ) ) {
			ret_code = SYS_LOCK_ALLOCATION_FAILURE;
		}

		if ( !SYSIsLockInitialized( &SYSLockNess ) )
		{
			ret_code = SYSInitLock(&SYSLockNess, "SYSLockNess");
		}

	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per accedere alla sezione critica della lista dei lock
/// Se LockList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSLockLockList( char * label )
{
	int ret_code = SYS_LOCK_NO_ERROR;

	if (LockList != NULL)
	{
		SYSLock( &SYSLockNess, label);
	}

	return ret_code;
}

//==============================================================================
/// Funzione per uscire dalla sezione critica della lista dei lock
/// Se LockList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSUnlockLockList( void )
{
	int ret_code = SYS_LOCK_NO_ERROR;

	if (LockList != NULL)
	{
		SYSUnLock( &SYSLockNess );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per cancellare la variabile globale SYSLockNess e la lista globale
/// dei delle variabili "lock" di tipo SYS_LOCK
/// Se LockList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSClearLockList( void )
{
	int ret_code = SYS_LOCK_NO_ERROR;

	// --- Accedo alla lista dei lock ---
	SYSLockLockList("SYSClearLockList" );


	try
	{
		if ( LockList != NULL ) {
			try
			{
				// --- Resetto la lista appena allocata ---
				memset( LockList, 0, sizeof(SYS_LOCK*) * LockListMax );
				// --- Elimino la lista ---
				free( LockList );
			}
			catch(...)
			{
				ret_code = SYS_LOCK_FREE_FAILURE;
			}
			// --- Azzero i parametri della lista ---
			LockListMax	= 0;
			LockListCount = 0;
			LockList = NULL;
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	// --- Rilascio la lista dei lock ---
   /*	if (LockList != NULL)
	{*/
		SYSUnLock( &SYSLockNess );
   //	}
	// --- Elimino la sezione critica della lista dei lock ---
/*	if ( LockList != NULL )
	{ */
		if ( SYSIsLockInitialized( &SYSLockNess ) )
		{
			ret_code = SYSClearLock(&SYSLockNess);
		}
//	}

	return ret_code;
}


//==============================================================================
/// Funzione per ottenere il primo posto libero nella lista dei lock
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSGetFirstFreeLockListPosition( void )
{
	int position = -1;
	unsigned int i;
	SYS_LOCK * lock = NULL;

	try
	{
			if ( LockList != NULL ) {
				for ( i = 0; i < LockListMax; i++ ) {
					try
					{
						lock = LockList[i];
						if ( !SYSIsValidLock( lock ) ) {
							position = i; // salvo la posizione
							break;
						}
					}
					catch(...)
					{

					}
				}
			}
			else
			{
				// Lista lock non valida
			}
	}
	catch(...)
	{
		position = -1;
	}

	return position;
}


//==============================================================================
/// Funzione per aggiungere un lock alla lista globale
/// Se LockList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSAddLockToList( SYS_LOCK * lock )
{
	int ret_code = SYS_LOCK_NO_ERROR;
	int free_position;

	// --- Accedo alla lista dei thread ---
	SYSLockLockList("SYSAddLockToList");
	try
	{
		if ( LockList != NULL ) {
			if ( SYSIsValidLock( lock ) ) {
				if ( LockListCount < LockListMax ) {
					free_position = SYSGetFirstFreeLockListPosition( );
					if ( free_position >= 0 && free_position < (int)LockListMax ) {
						// --- Aggiungo alla lista ---
						LockList[free_position] = lock;
						// --- Incremento il contatore lista ---
						LockListCount++;
					}
					else
					{
						ret_code = SYS_LOCK_LIST_FULL;
					}
				}
				else
				{
					ret_code = SYS_LOCK_LIST_FULL;
				}
			}
			else
			{
				ret_code = SYS_LOCK_INVALID_LOCK;
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	// --- Rilascio la lista dei thread ---
	SYSUnlockLockList( );

	return ret_code;
}

//==============================================================================
// Funzione per ottenere la posizione di un lock nella lista
///
/// \date [07.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSGetLockListPosition( SYS_LOCK * lock )
{
	int position = -1;
	unsigned int i;
	SYS_LOCK * current = NULL;

	try
	{
		if ( LockList != NULL ) {
			for ( i = 0; i < LockListMax; i++ ) {
				try
				{
					current = LockList[i];
					if ( SYSIsValidLock( current ) ) {
						if ( lock == current ) {
							position = i; // salvo la posizione
							break;
						}
					}
				}
				catch(...)
				{

				}
			}
		}
		else
		{
			// Lista lock non valida
		}
	}
	catch(...)
	{
		position = -1;
	}

	return position;
}


//==============================================================================
/// Funzione per togliere un lock dalla lista globale
/// Se LockList non � inizializzata la funzione viene ignorata
///
/// \date [07.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSRemoveLockFromList( SYS_LOCK * lock )
{
	int ret_code = SYS_LOCK_NO_ERROR;
	int position = 0;

	// --- Accedo alla lista dei thread ---
	SYSLockLockList("SYSRemoveLockFromList");
	try
	{
		if ( LockList != NULL )
		{
			if ( SYSIsValidLock( lock ) ) {
				if ( LockListCount > 0 ) {
					position = SYSGetLockListPosition( lock );
					if ( position >= 0 && position < (int)LockListMax ) {
						try
						{
							// --- Rimuovo dalla lista ---
							LockList[position] = NULL;
							// --- Decremento il contatore ---
							LockListCount--;
						}
						catch(...)
						{
							ret_code = SYS_LOCK_LIST_REMOVE_FAILURE;
						}
					}
					else
					{
						ret_code = SYS_LOCK_POSITION_OUT_OF_RANGE;
					}
				}
				else
				{
					ret_code = SYS_LOCK_LIST_EMPTY;
				}
			}
			else
			{
				ret_code = SYS_LOCK_INVALID_LOCK;
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}
	// --- Rilascio la lista dei lock---
	SYSUnlockLockList( );

	return ret_code;
}
//==============================================================================
/// Funzione per verificare la validita' di un Lock
///
/// \date [07.11.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SYSIsValidLock( SYS_LOCK * lock )
{
	bool valid = false;

	try
	{
		if ( lock != NULL ) {
			if ( lock->VCC == SYS_LOCK_VALIDITY_CHECK_CODE ) {
				valid = true;
			}
		}
	}
	catch(...)
	{
		valid = false;
	}

	return valid;
}

//==============================================================================
/// Funzione per inizializzare un lock
///
/// \date [04.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	SYSInitLock( SYS_LOCK * lock, char * lockName )
{
	int	ret_code = SYS_LOCK_NO_ERROR;
	int	lockName_len = 0;

	try
	{
		if ( lock != NULL ) {
			memset( (void*)lock, 0, sizeof(SYS_LOCK) );

			lock->VCC = SYS_LOCK_VALIDITY_CHECK_CODE;
			lock->Initialized = true;
			lock->Locked = false;
			lock->PreLock = false;
			lock->LockCounter = 0;

			// --- Salvo il nome del lock ---
			memset( &lock->Name[0], 0, 65 * sizeof( char ) );
			if ( lockName != NULL ) {
				// --- Copio la label ---
				lockName_len = strlen( lockName );
				lockName_len = (lockName_len>64)?64:lockName_len;
				memcpy( &lock->Name[0], lockName, lockName_len * sizeof(char) );
			}

			InitializeCriticalSection( &lock->CriticalSection );

			// aggiungo il lock alla lista dei lock
			ret_code = SYSAddLockToList(lock);

		}
		else
		{
			ret_code = SYS_LOCK_INVALID_LOCK_PTR;
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per cancellare un lock
///
/// \date [04.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	SYSClearLock( SYS_LOCK * lock )
{
	int	ret_code = SYS_LOCK_NO_ERROR;

	try
	{
		if ( SYSIsValidLock( lock ) )
		{
			DeleteCriticalSection( &lock->CriticalSection );

			memset( (void*)lock, 0, sizeof(SYS_LOCK) );

			lock->VCC = SYS_LOCK_VALIDITY_CHECK_CODE;
			lock->Initialized = false;
			lock->Locked = false;
			lock->PreLock = false;
			lock->LockCounter = 0;
		   /*	try
				{
				if (lock->Name != NULL)
				{
					free(lock->Name);      ///?????////
				}
			}
			catch(...)
			{
				ret_code = SYS_KERNEL_CRITICAL_ERROR;
			}*/
			// elimino il lock dalla lista dei thread
			ret_code = SYSRemoveLockFromList(lock);
		}
		else
		{
			ret_code = SYS_LOCK_INVALID_LOCK;
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere (bloccare) un lock
///
/// \date [04.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	SYSLock( SYS_LOCK * lock, char * label )
{
	int		ret_code = SYS_LOCK_NO_ERROR;
	int		label_len = 0;

	CRITICAL_SECTION *		pCriticalSection; // Ptr a Sezione Critica

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSLock Sup" );
		}
		// --- Effettuo il blocco ---
		if ( SYSIsValidLock( lock ) )
		{
			if ( lock->Initialized == true )
			{
				lock->PreLock = true;
				pCriticalSection = &lock->CriticalSection;
			}
			else
			{
				ret_code = SYS_LOCK_UNINTIALIZED_LOCK;
			}
		}
		else
		{
			ret_code = SYS_LOCK_INVALID_LOCK;
		}

		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}

		if (ret_code == SYS_LOCK_NO_ERROR)
		{
			//EnterCriticalSection( &lock->CriticalSection );
			EnterCriticalSection( pCriticalSection );

			// --- Blocco la gestione dei lock ---
			if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
				SYSLock( &SYSLockNess, "SYSLock Inf" );
			}

			lock->PreLock = false;
			// --- Salvo i parametri del lock ---
			memset( &lock->Label[0], 0, 65 * sizeof( char ) );
			if ( label != NULL ) {
				// --- Copio la label ---
				label_len = strlen( label );
				label_len = (label_len>64)?64:label_len;
				memcpy( &lock->Label[0], label, label_len * sizeof(char) );
			}
			lock->Locked = true;
			lock->LockDateTime = ULGetUnixTime();
			lock->LockCounter++;

			// --- Sblocco la gestione dei lock ---
			if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
				SYSUnLock( &SYSLockNess );
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aprire (sbloccare) un lock
///
/// \date [04.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	SYSUnLock( SYS_LOCK * lock )
{
	int		ret_code = SYS_LOCK_NO_ERROR;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSUnLock" );
		}
		// --- Effettuo il blocco ---
		if ( SYSIsValidLock( lock ) ) {
			if ( lock->Initialized == true ) {
				LeaveCriticalSection( &lock->CriticalSection );
				lock->Locked = false;
				lock->LockCounter--;
				// lock->LockDateTime = ULGetUnixTime();
			}
			else
			{
				ret_code = SYS_LOCK_UNINTIALIZED_LOCK;
			}
		}
		else
		{
			ret_code = SYS_LOCK_INVALID_LOCK;
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		ret_code = SYS_LOCK_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per sapere se un lock e' inizializzato
///
/// \date [07.11.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SYSIsLockInitialized( SYS_LOCK * lock )
{
	bool	initialized = false;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSIsInitialized" );
		}
		// --- Recupero il flag ---
		if ( SYSIsValidLock( lock ) ) {
			initialized = lock->Initialized;
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		initialized = false;
	}

	return initialized;
}

//==============================================================================
/// Funzione per sapere se un lock e' chiuso
///
/// \date [07.11.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SYSIsLocked( SYS_LOCK * lock )
{
	bool	locked = false;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSIsLocked" );
		}
		// --- Recupero il flag ---
		if ( SYSIsValidLock( lock ) ) {
			locked = lock->Locked;
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		locked = false;
	}

	return locked;
}

//==============================================================================
/// Funzione per estrarre (malloc) l'etichetta di un lock
///
/// \date [07.11.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
char * SYSExtractLockLabel( SYS_LOCK * lock )
{
	char *	label = NULL;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSExtractLabel" );
		}
		// --- Copio la stringa ---
		if ( SYSIsValidLock( lock ) ) {
			label = ULStrCpy( NULL, &lock->Label[0] ); // -MALLOC
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		label = NULL;
	}

	return label;

}

//==============================================================================
/// Funzione per estrarre (malloc) il nome di un lock
///
/// \date [07.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char * SYSExtractLockName( SYS_LOCK * lock )
{
	char *	name = NULL;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSExtractName" );
		}
		// --- Copio la stringa ---
		if ( SYSIsValidLock( lock ) ) {
			name = ULStrCpy( NULL, &lock->Name[0] ); // -MALLOC
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		name = NULL;
	}

	return name;

}

//==============================================================================
/// Funzione per sapere il numero di chiusure attive su un lock
///
/// \date [04.02.2008]
/// \author Ferro Mario
/// \version 0.01
//------------------------------------------------------------------------------
int SYSGetLockCounter( SYS_LOCK * lock )
{
	int	lockCounter = 0;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSGetLockCounter" );
		}
		// --- Recupero il flag ---
		if ( SYSIsValidLock( lock ) ) {
			lockCounter = lock->LockCounter;
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		lockCounter = -100000;
	}

	return lockCounter;
}

//==============================================================================
/// Funzione per sapere se si � in fase di PreLock
///
/// \date [04.02.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SYSIsPreLock( SYS_LOCK * lock )
{
	bool	preLock = false;

	try
	{
		// --- Blocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSLock( &SYSLockNess, "SYSIsLocked" );
		}
		// --- Recupero il flag ---
		if ( SYSIsValidLock( lock ) ) {
			preLock = lock->PreLock;
		}
		// --- Sblocco la gestione dei lock ---
		if ( lock != &SYSLockNess && SYSIsValidLock( &SYSLockNess ) ) {
			SYSUnLock( &SYSLockNess );
		}
	}
	catch(...)
	{
		preLock = false;
	}

	return preLock;
}



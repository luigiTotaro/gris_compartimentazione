//==============================================================================
// Telefin System Kernel Module 1.0
//------------------------------------------------------------------------------
// Modulo Kernel (per S.O. Microsoft Windows 2000/XP/XPe/WePoS (SYSKernel.cpp)
// Progetto:	Telefin System Library 1.0
//
// Revisione:	0.55 (05.04.2004 -> 21.08.2012)
//
// Copyright:	2004-2012 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (enrico.alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
//				Paolo Colli (paolo.colli@telefin.it)
// Note:		richiede SYSKernel.h, SYSSerial.h, SYSSocket.h e
//				SPVPortMonitorForm.h (GUI).
//------------------------------------------------------------------------------
// Version history: vedere in SYSKernel.h
//==============================================================================
#pragma hdrstop
#include "SYSKernel.h"
#include "SYSSerial.h"
#include "SYSSocket.h"
#include "SPVSTLC1000.h"
#include "XMLPort.h"
#include "SPVEventLog.h"
#include "cnv_lib.h"
#include "UtilityLibrary.h"
#pragma package(smart_init)
//------------------------------------------------------------------------------


//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SYS_THREAD_LIST_EXPORT_KEY_PATH			"SYSTEM\\CurrentControlSet\\Services\\StlcSPVService"
#define SYS_ENABLE_THREAD_LIST_EXPORT_VALUE   	"EnableThreadListExport"
#define SYS_LAST_THREAD_LIST_EXPORT_NAME_VALUE  "LastThreadListExportName"
#define SYS_LAST_LOCK_LIST_EXPORT_NAME_VALUE  	"LastLockListExportName"
#define SYS_KERNEL_EXPORT_ENABLED				1L
#define SYS_KERNEL_EXPORT_DISABLED	            0L

#define SYS_CFG_SERVER_KEY                  "SYSTEM\\CurrentControlSet\\Services\\StlcSPVService"
#define SYS_CFG_DEBUG_LOG_ENABLED	    "DebugLogEnabled"	

//------------------------------------------------------------------------------

static	SC_HANDLE			SYSSCManager = NULL;

//static	_CRITICAL_SECTION	ThreadListCriticalSection;
static	SYS_LOCK			ThreadListLock;
static	SYS_THREAD **		ThreadList = NULL;
static	unsigned int		ThreadListMax;
static	unsigned int		ThreadListCount;

//==============================================================================
/// Funzione privata per codificare l'eventuale errore ritornato dalla funzione
/// QueryServiceStatusEx.
///
/// \date [12.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSCodifyQSSELastError( DWORD pLastError )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	switch (pLastError) {
		case ERROR_INVALID_HANDLE:
			ret_code = SYS_KERNEL_QSSE_INVALID_HANDLE;
			break;
		case ERROR_ACCESS_DENIED:
			ret_code = SYS_KERNEL_QSSE_ACCESS_DENIED;
			break;
		case ERROR_INSUFFICIENT_BUFFER:
			ret_code = SYS_KERNEL_QSSE_INSUFFICIENT_BUFFER;
			break;
		case ERROR_INVALID_PARAMETER:
			ret_code = SYS_KERNEL_QSSE_INVALID_PARAMETER;
			break;
		case ERROR_INVALID_LEVEL:
			ret_code = SYS_KERNEL_QSSE_INVALID_LEVEL;
			break;
		case ERROR_SHUTDOWN_IN_PROGRESS:
			ret_code = SYS_KERNEL_QSSE_SHUTDOWN_IN_PROGRESS;
			break;
		default: ret_code = SYS_KERNEL_SERVICE_QUERY_FAILURE;
	}
	return ret_code;
}


//==============================================================================
/// Funzione per inizializzare la lista globale dei thread
/// Opzionalemente si pu� passare il numero massimo di thread per la lista
/// altrimenti il valore default e' 1000. Il valore di default e' selezionabile
/// passando max_thread = 0
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int	SYSInitThreadList( unsigned int max_thread )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	try
	{
//		InitializeCriticalSection( &ThreadListCriticalSection );
		SYSInitLock(&ThreadListLock,"ThreadListLock");

		ThreadListMax = ( max_thread > 0 )? max_thread : 1000; // Se passo 0 uso il valore di default (1000)
		// --- Alloco la lista ---
		ThreadList = (SYS_THREAD**)malloc( sizeof(SYS_THREAD*) * ThreadListMax ); // -MALLOC
		// --- Resetto la lista appena allocata ---
		memset( ThreadList, 0, sizeof(SYS_THREAD*) * ThreadListMax );

		if ( !(ThreadList != NULL ) ) {
			ret_code = SYS_LOCK_ALLOCATION_FAILURE;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per accedere alla sezione critica della lista dei thread
/// Se ThreadList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SYSLockThreadList( char * label )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	if (ThreadList != NULL)
	{
		//EnterCriticalSection( &ThreadListCriticalSection );
		SYSLock( &ThreadListLock, label);
	}

	return ret_code;
}

//==============================================================================
/// Funzione per uscire dalla sezione critica della lista dei thread
/// Se ThreadList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int	SYSUnlockThreadList( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	if (ThreadList != NULL)
	{
//		LeaveCriticalSection( &ThreadListCriticalSection );
		SYSUnLock( &ThreadListLock );
	}

	return ret_code;
}

//==============================================================================
/// Funzione per cancellare la lista globale dei thread
/// Se ThreadList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int	SYSClearThreadList( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// --- Accedo alla lista dei thread ---
	SYSLockThreadList("SYSClearThreadList");
	try
	{
		if ( ThreadList != NULL ) {
			try
			{
				// --- Resetto la lista appena allocata ---
				memset( ThreadList, 0, sizeof(SYS_THREAD*) * ThreadListMax );
				// --- Elimino la lista ---
				free( ThreadList );
			}
			catch(...)
			{
				ret_code = SYS_KERNEL_FREE_FAILURE;
			}
			// --- Azzero i parametri della lista ---
			ThreadListMax	= 0;
			ThreadListCount	= 0;
			ThreadList		= NULL;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}
	// --- Rilascio la lista dei thread ---
	SYSUnlockThreadList( );
	// --- Elimino la sezione critica della lista dei thread ---
	if ( ThreadList != NULL ) {
//		DeleteCriticalSection( &ThreadListCriticalSection );
		SYSClearLock(&ThreadListLock);
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere il primo posto libero nella lista thread
///
/// \date [10.09.2007]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSGetFirstFreeThreadListPosition( void )
{
	int position = -1;
	unsigned int i;
	SYS_THREAD * thread = NULL;

	try
	{
			if ( ThreadList != NULL ) {
				for ( i = 0; i < ThreadListMax; i++ ) {
					try
					{
						thread = ThreadList[i];
						if ( !SYSValidThread( thread ) ) {
							position = i; // salvo la posizione
							break;
						}
					}
					catch(...)
					{

					}
				}
			}
			else
			{
				// Lista thread non valida
			}
	}
	catch(...)
	{
		position = -1;
	}

	return position;
}
//------------------------------------------------------------------------------

//==============================================================================
// Funzione per ottenere la posizione di un thread nella lista
///
/// \date [19.09.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int	SYSGetThreadListPosition( SYS_THREAD * thread )
{
	int position = -1;
	unsigned int i;
	SYS_THREAD * current = NULL;

	try
	{
		if ( ThreadList != NULL ) {
			for ( i = 0; i < ThreadListMax; i++ ) {
				try
				{
					current = ThreadList[i];
					if ( SYSValidThread( current ) ) {
						if ( thread == current ) {
							position = i; // salvo la posizione
							break;
						}
					}
				}
				catch(...)
				{

				}
			}
		}
		else
		{
			// Lista thread non valida
		}
	}
	catch(...)
	{
		position = -1;
	}

	return position;
}
//------------------------------------------------------------------------------

//==============================================================================
/// Funzione per aggiungere un thread alla lista globale
/// Se ThreadList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SYSAddThreadToList( SYS_THREAD * thread )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	int free_position;

	// --- Accedo alla lista dei thread ---
	SYSLockThreadList("SYSAddThreadToList");
	try
	{
		if ( ThreadList != NULL ) {
			if ( SYSValidThread( thread ) ) {
				if ( ThreadListCount < ThreadListMax ) {
					free_position = SYSGetFirstFreeThreadListPosition( );
					if ( free_position >= 0 && free_position < (int)ThreadListMax ) {
						// --- Aggiungo alla lista ---
						ThreadList[free_position] = thread;
						// --- Incremento il contatore lista ---
						ThreadListCount++;
					}
					else
					{
						ret_code = SYS_KERNEL_THREAD_LIST_FULL;
					}
				}
				else
				{
					ret_code = SYS_KERNEL_THREAD_LIST_FULL;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD;
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}
	// --- Rilascio la lista dei thread ---
	SYSUnlockThreadList( );

	return ret_code;
}

//==============================================================================
/// Funzione per togliere un thread dalla lista globale
/// Se ThreadList non � inizializzata la funzione viene ignorata
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int	SYSRemoveThreadFromList( SYS_THREAD * thread )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	int position = 0;

	// --- Accedo alla lista dei thread ---
	SYSLockThreadList("SYSRemoveThreadFromList");
	try
	{
		if ( ThreadList != NULL )
		{
			if ( SYSValidThread( thread ) ) {
				if ( ThreadListCount > 0 ) {
					position = SYSGetThreadListPosition( thread );
					if ( position >= 0 && position < (int)ThreadListMax ) {
						try
						{
							// --- Rimuovo dalla lista ---
							ThreadList[position] = NULL;
							// --- Decremento il contatore ---
							ThreadListCount--;
						}
						catch(...)
						{
							ret_code = SYS_KERNEL_THREAD_LIST_REMOVE_FAILURE;
						}
					}
					else
					{
						ret_code = SYS_KERNEL_POSITION_OUT_OF_RANGE;
					}
				}
				else
				{
					ret_code = SYS_KERNEL_THREAD_LIST_EMPTY;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD;
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}
	// --- Rilascio la lista dei thread ---
	SYSUnlockThreadList( );

	return ret_code;
}


//==============================================================================
/// Funzione che ritorna in un DWORD i valori 0 o 1 scritti nella chiave di
/// registro "EnableThreadListExport"
/// 0 : esportazione della ThreadList disabilitata
/// 1 : esportazione della ThreadList abilitata
/// In caso di errore nella lettura della chiave restituisce 0l
///
/// \date [27.09.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
DWORD SYSGetKeyEnableThreadListExportValue()
{
	void  *			key      	    = 	NULL;
	DWORD * 		enable_value   	= 	NULL;
	DWORD 			enableValue 	= 	SYS_KERNEL_EXPORT_DISABLED	;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SYS_THREAD_LIST_EXPORT_KEY_PATH, false );
		if ( key != NULL )
		{
			enable_value = (DWORD *) SYSGetRegValue( key, SYS_ENABLE_THREAD_LIST_EXPORT_VALUE);
			SYSCloseRegKey( key );
		}

		if (enable_value != NULL)
		{
			enableValue = *enable_value ;
		}
	}
	catch(...)
	{
		enableValue = SYS_KERNEL_EXPORT_DISABLED;
	}
	return enableValue;
}

//==============================================================================
/// Funzione che ritorna in un DWORD i valori 0 o 1 scritti nella chiave di
/// registro "DebugLogEnabled"
/// 0 : log di debug disabilitati
/// 1 : log di debug abilitati
/// In caso di errore nella lettura della chiave restituisce 0
///
/// \date [27.09.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
DWORD SYSSetKeyDebugLogEnabled()
{
   	void        * key               = NULL;
	DWORD       * log_enabled_value = NULL;
        DWORD       enableValue 	= 0;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SYS_CFG_SERVER_KEY, false );
	if ( key != NULL ){
		log_enabled_value = (DWORD *)  SYSGetRegValue( key, SYS_CFG_DEBUG_LOG_ENABLED );
		SYSCloseRegKey( key );

                if (log_enabled_value != NULL)
		{
			enableValue = *log_enabled_value ;
		}
	}
	return enableValue;
}

//==============================================================================
/// Funzione che scrive sulla chiave di registro "EnableThreadListExport"
/// i valori 0 o 1.
/// 0 : esportazione della ThreadList disabilitata
/// 1 : esportazione della ThreadList abilitata
/// Se non c'� errore nella scrittura della chiave restituisce 0
///
/// \date [08.11.2007]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SYSSetKeyEnableThreadListExportValue(bool enableValue)
{
	void  *			key      	    = 	NULL;
	int				ret_code		= 	SYS_KERNEL_NO_ERROR;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SYS_THREAD_LIST_EXPORT_KEY_PATH, true );
		if ( key != NULL )
		{
			ret_code = SYSSetRegValue( key, SYS_ENABLE_THREAD_LIST_EXPORT_VALUE, (void*)&enableValue, REG_DWORD, sizeof(DWORD)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_REGKEY_PATH;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}
	return ret_code;
}

//==============================================================================
/// Funzione che scrive sulla chiave di registro "LastThreadListExportName"
/// il nome del file di esportazione della thredList
/// Se non c'� errore nella scrittura della chiave restituisce 0
///
/// \date [27.09.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSSetKeyLastThreadListExportNameValue(char* fileNameValue)
{
	void  *			key      	    = 	NULL;
	int				ret_code		= 	SYS_KERNEL_NO_ERROR;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SYS_THREAD_LIST_EXPORT_KEY_PATH, true );
		if ( key != NULL )
		{
			ret_code = SYSSetRegValue( key, SYS_LAST_THREAD_LIST_EXPORT_NAME_VALUE, (void*)fileNameValue, REG_SZ, sizeof(char)*(strlen(fileNameValue)+1)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_REGKEY_PATH;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}
	return ret_code;
}

//==============================================================================
/// Funzione che scrive sulla chiave di registro "LastLockListExportName"
/// il nome del file di esportazione della lockList
/// Se non c'� errore nella scrittura della chiave restituisce 0
///
/// \date [06.05.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSSetKeyLastLockListExportNameValue(char* fileNameValue)
{
	void  *			key      	    = 	NULL;
	int				ret_code		= 	SYS_KERNEL_NO_ERROR;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SYS_THREAD_LIST_EXPORT_KEY_PATH, true );
		if ( key != NULL )
		{
			ret_code = SYSSetRegValue( key, SYS_LAST_LOCK_LIST_EXPORT_NAME_VALUE, (void*)fileNameValue, REG_SZ, sizeof(char)*(strlen(fileNameValue)+1)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_REGKEY_PATH;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}
	return ret_code;
}
//==============================================================================
/// Funzione per verificare che non vi siano dei thread bloccati
///	Si controllano i thread dello Scheduler e quelli di InuptBuffer e OutputBuffer
///
/// \date [06.02.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
bool SYSCheckThreads( void )
{
	//int 			ret_code 		= SYS_KERNEL_NO_ERROR;
	unsigned int	i;
	SYS_THREAD * 	thread 			= NULL;
	bool			threadIsLLocked = false;


	// --- Accedo alla lista dei thread ---
	SYSLockThreadList("SYSCheckThreads");

	try
	{
		if ( ThreadList != NULL )
		{
			for ( i = 0; i < ThreadListMax; i++ )
			{
				try
				{
					thread = ThreadList[i];
					if ( (SYSValidThread( thread )) && (thread->Name != NULL) )
					{
						// --- Verifico che si tratti di uno dei 3 thread seguenti
						if ((strcmp(thread->Name,"Scheduler") == 0) ||
							(strstr(thread->Name,"- InputBuffer") != NULL) ||
							(strstr(thread->Name,"- OutputBuffer") != NULL) )
						{
							// --- Verifico che il thread sia stato eseguito entro SYS_KERNEL_MAX_THREAD_TIMEOUT
							if ((ULGetUnixTime() - thread->LastExecDateTime) > SYS_KERNEL_MAX_THREAD_TIMEOUT)
							{
								threadIsLLocked = true;
								break;
							}
						}
					}
				}
				catch(...)
				{
					threadIsLLocked = true;
					//ret_code = SYS_KERNEL_EXPORT_THREAD_LIST_FAILURE;
					break;
				}
			}
		}
	}
	catch(...)
	{
		threadIsLLocked = true;
		//ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio la lista dei thread ---
	SYSUnlockThreadList( );

	return (!threadIsLLocked);
}


//==============================================================================
/// Funzione per ottenere l'Export su file di testo della lista thread
///
/// \date [06.02.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int	SYSExportThreadList( void )
{
	int 			ret_code 	= SYS_KERNEL_NO_ERROR;
	unsigned int	i;
	char * 			exportPath 	= NULL;
	SYS_THREAD * 	thread 		= NULL;
	char * 			cnv_value 	= NULL;

	// --- Accedo alla lista dei thread ---
	SYSLockThreadList("SYSExportThreadList");

	try
	{
		if ( ThreadList != NULL )
		{
			// --- Definizione di un EventLog manager per fare il Export della lista---
			SPV_EVENTLOG_MANAGER  * export_manager;
			char * exportFileName = NULL;
			char * exportTime = cnv_UInt32ToCharP(ULGetUnixTime());
			exportFileName = ULAddText(exportFileName,"SPVServer_ThreadList_");
			exportFileName = ULAddText(exportFileName,exportTime);
			exportFileName = ULAddText(exportFileName,".log");
			exportPath = ULAddText(exportPath,"C:\\Program Files\\Telefin\\SupervisorServer\\");
			exportPath = ULAddText(exportPath,exportFileName);
			// --- Creazione del nuovo EventLog manager ---
			export_manager = SPVNewEventLog( SPV_EVENTLOG_TYPE_ASCII_FILE, SPV_EVENTLOG_TEMPLATE_NONE, exportPath );

			ULFreeAndNullString(&exportFileName);
			ULFreeAndNullString(&exportTime);

			if ( export_manager != NULL )
			{
				// --- Apertura dell'EventLog Manager creato ---
				SPVOpenEventLog( export_manager->ID );

				if (export_manager->Opened)
				{
					// Intestazione del file
					SPVPrintEventLog( export_manager->ID, "[ThreadList]", strlen("[ThreadList]"), 0 );
					SPVPrintEventLog( export_manager->ID, " ", strlen(" "), 0 );
					int num = 0;
					char * numThread = NULL;
					char * descThread = NULL;
					char * valueThread = cnv_UInt32ToCharP(ThreadListCount);

					//Numero di thread attivi
					descThread = ULAddText(descThread,"ThreadCount=");
					descThread = ULAddText(descThread,valueThread);
					SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
					SPVPrintEventLog( export_manager->ID, " ", strlen(" "), 0 );
					ULFreeAndNullString(&valueThread);
					ULFreeAndNullString(&descThread);

					for ( i = 0; i < ThreadListMax; i++ )
					{
						try
						{
							thread = ThreadList[i];
							if ( SYSValidThread( thread ) )
							{
								// Numero del thread
								/*valueThread = cnv_UInt32ToCharP(i);
								descThread = ULAddText(descThread,"Thread n� ");
								descThread = ULAddText(descThread,valueThread);
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&valueThread);
								ULFreeAndNullString(&descThread);
								*/
								num++;
								numThread = cnv_UInt32ToCharP(num);

								// ID del thread
								valueThread = cnv_UInt32ToCharP(thread->ThreadId);
								descThread = ULAddText(descThread,"ID"); // = ");
								descThread = ULAddText(descThread,numThread);
								descThread = ULAddText(descThread,"=");
								descThread = ULAddText(descThread,valueThread);
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&valueThread);
								ULFreeAndNullString(&descThread);

								// Nome del thread
								valueThread = thread->Name;
								descThread = ULAddText(descThread,"Name");
								descThread = ULAddText(descThread,numThread);
								descThread = ULAddText(descThread,"=");
								descThread = ULAddText(descThread,valueThread);
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&descThread);

								// Numero cicli del thread
								valueThread = cnv_UInt32ToCharP(thread->ExecCounter);
								descThread = ULAddText(descThread,"ExecCounter");
								descThread = ULAddText(descThread,numThread);
								descThread = ULAddText(descThread,"=");
								descThread = ULAddText(descThread,valueThread);
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&valueThread);
								ULFreeAndNullString(&descThread);

								// Ultima esecuzione del thread
								valueThread = cnv_UInt32ToCharP(thread->LastExecDateTime);
								descThread = ULAddText(descThread,"LastExecDateTime");
								descThread = ULAddText(descThread,numThread);
								descThread = ULAddText(descThread,"=");
								descThread = ULAddText(descThread,valueThread);
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&valueThread);
								ULFreeAndNullString(&descThread);

								// --- ID dello step in cui si trova il thread ---
								cnv_value = cnv_UInt32ToCharP( thread->StepID );
								descThread = ULAddText( descThread, "StepID" );
								descThread = ULAddText( descThread, numThread );
								descThread = ULAddText( descThread, "=" );
								descThread = ULAddText( descThread, cnv_value );
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descThread);

								// --- Flag Terminated del Thread ---
								cnv_value = cnv_BoolToCharP( thread->Terminated );
								descThread = ULAddText( descThread, "Terminated" );
								descThread = ULAddText( descThread, numThread );
								descThread = ULAddText( descThread, "=" );
								descThread = ULAddText( descThread, cnv_value );
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descThread);

								// --- Flag Suspended del Thread ---
								cnv_value = cnv_BoolToCharP( thread->Suspended );
								descThread = ULAddText( descThread, "Suspended" );
								descThread = ULAddText( descThread, numThread );
								descThread = ULAddText( descThread, "=" );
								descThread = ULAddText( descThread, cnv_value );
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descThread);

								// --- SuspendCount del thread ---
								cnv_value = cnv_UInt32ToCharP( thread->SuspendCount );
								descThread = ULAddText( descThread, "SuspendCount" );
								descThread = ULAddText( descThread, numThread );
								descThread = ULAddText( descThread, "=" );
								descThread = ULAddText( descThread, cnv_value );
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descThread);

								// --- LastErrorCode del thread ---
								cnv_value = cnv_UInt32ToCharP( thread->LastErrorCode );
								descThread = ULAddText( descThread, "LastErrorCode" );
								descThread = ULAddText( descThread, numThread );
								descThread = ULAddText( descThread, "=" );
								descThread = ULAddText( descThread, cnv_value );
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descThread);

								// --- ExitCode thread ---
								cnv_value = cnv_UInt32ToCharP( thread->ExitCode );
								descThread = ULAddText( descThread, "ExitCode" );
								descThread = ULAddText( descThread, numThread );
								descThread = ULAddText( descThread, "=" );
								descThread = ULAddText( descThread, cnv_value );
								SPVPrintEventLog( export_manager->ID, descThread, strlen(descThread), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descThread);

								ULFreeAndNullString(&numThread);
								SPVPrintEventLog( export_manager->ID, " ", strlen(" "), 0 );
							}
						}
						catch(...)
						{
							ret_code = SYS_KERNEL_EXPORT_THREAD_LIST_FAILURE;
						}
					}
					ret_code = SPVCloseEventLog( export_manager->ID );
				}

				if ( export_manager != NULL )
				{
					ret_code = SPVDeleteEventLog( export_manager->ID );
				}
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio la lista dei thread ---
	SYSUnlockThreadList( );

	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		try
		{
			ret_code = SYSSetKeyLastThreadListExportNameValue(exportPath);
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				ret_code = SYSSetKeyEnableThreadListExportValue(false);
			}
		}
		catch(...)
		{
			ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
		}
	}


	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		try
		{
			ret_code = SYSSetKeyLastThreadListExportNameValue("");
			ret_code = SYSSetKeyEnableThreadListExportValue(false);
		}
		catch(...)
		{
			ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
		}
	}

	ULFreeAndNullString(&exportPath);

	return ret_code;
}
//------------------------------------------------------------------------------



//==============================================================================
/// Funzione per creare una struttura per thread.
/// Alloca la memoria per una nuova struttura thread e setta tre parametri
/// fondamentali e lascia i rimanenti a NULL.
/// Restituisce il puntatore alla nuova struttura.
/// In caso di errore restituisce NULL.
///
/// \date [17.09.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
SYS_THREAD * SYSNewThread( unsigned long __stdcall start(void *), unsigned long stack, unsigned long flags )
{
	SYS_THREAD * thread = NULL;

	thread = (SYS_THREAD*) malloc( sizeof(SYS_THREAD) );

	if ( thread != NULL ) // Controllo se la struttura � stata allocata
	{
		// --- Azzero la memoria ppena allocata ---
		memset( thread, 0, sizeof(SYS_THREAD) );
		// --- Imposto i valori di default e specificati ---
		thread->VCC					= SYS_KERNEL_THREAD_VALIDITY_CHECK_CODE;
		thread->Terminated			= false ;
		thread->Suspended			= false ;
		thread->SuspendCount		= 0     ;
		thread->LastErrorCode		= 0     ;
		thread->ExitCode			= 0     ;
		thread->Handle				= NULL  ;
		thread->ThreadAttributes	= NULL  ;
		thread->StackSize			= stack ; // Imposto la dimensione dello stack
		thread->StartAddress		= start ; // Imposto l'indirizzo della funzione di partenza
		thread->Parameter			= NULL  ;
		thread->CreationFlags		= flags ; // Imposto i flag di creazione
		thread->ThreadId			= NULL  ;
		thread->Name				= NULL  ;
	}

	return thread;
}

//==============================================================================
/// Funzione per verificare la validitia' di una struttura thread
///
/// \date [07.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
bool SYSValidThread( SYS_THREAD  * thread )
{
	bool  valid     = true;

	if ( thread != NULL )
	{
		try
		{
			if ( thread->VCC != SYS_KERNEL_THREAD_VALIDITY_CHECK_CODE )
			{
				valid = false;
			}
		}
		catch(...)
		{
			valid = false;
		}
	}
	else
	{
		valid = false;
	}

	return valid;
}

//==============================================================================
/// Funzione per settare gli attributi di un thread.
/// Se la struttura di thread <thread> passata � valida imposta il puntatore
/// alla struttura <security> che contiene gli attributi di sicurezza per il
/// thread.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSSetThreadAttributes( SYS_THREAD * thread, void * security )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSSetThreadAttributes");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			thread->ThreadAttributes = (LPSECURITY_ATTRIBUTES) security;
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per settare il parametro di un thread.
/// Se la struttura di thread <thread> passata � valida imposta il puntatore
/// alla struttura <parameter> che contiene il parametro per il thread.
/// In caso di errore la funzione restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSSetThreadParameter( SYS_THREAD * thread, void * parameter )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSSetThreadParameter");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			thread->Parameter = (LPVOID) parameter;
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per valorizzare l'attributo Name del thread
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int	SYSSetThreadName( SYS_THREAD * thread, char * name )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSSetThreadName");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			thread->Name = name;
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per incrementare la variabile contatore di esecuzioni del thread
///  e memorizzare la data e l'ora dell'ultima esecuzione
///
/// \date [06.02.2008]
/// \author Mario Ferro, Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int	SYSUpdateThreadDebugInfo( SYS_THREAD * thread )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSUpdateThreadDebugInfo");

	try
	{
		thread->ExecCounter++;
		thread->LastExecDateTime = ULGetUnixTime();
	}
	catch(...)
	{
	  ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

    return ret_code;
}

//==============================================================================
/// Funzione per istanziare thread.
/// Istanzia il thread specificato dalla struttura <thread>.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [07.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSCreateThread( SYS_THREAD * thread )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
	{
		thread->Handle = CreateThread( 	thread->ThreadAttributes,
										thread->StackSize       ,
										thread->StartAddress    ,
										thread->Parameter       ,
										thread->CreationFlags	,
										&thread->ThreadId       );
		if ( thread->Handle == NULL ) // Se la creazione � fallita
		{
			thread->LastErrorCode = GetLastError();
			ret_code = (int)thread->LastErrorCode;
		}
		else // altrimenti se � andata a buon fine
		{
			if ( thread->CreationFlags == CREATE_SUSPENDED )
			{
				thread->Suspended     = true;
				thread->SuspendCount  = 1;
			}
			else
			{
				thread->Suspended     = false;
				thread->SuspendCount  = 0;
			}
			thread->Terminated = false;

			// aggiungo il thread alla lista dei thread
			ret_code = SYSAddThreadToList(thread);

		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ripristinare thread.
/// Questa funzione decrementa il contatore delle sospensioni di un thread.
/// Quando il contatore va a zero il thread viene ripristinato.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSResumeThread( SYS_THREAD * thread )
{
	int   ret_code = SYS_KERNEL_NO_ERROR;
	DWORD code;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSResumeThread");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo il ptr alla struttura del thread
		{
			if ( thread->Handle != NULL ) // Controllo l'handle del thread
			{
				code = ResumeThread( thread->Handle ); // Ripristino il thread
				if ( code == 0xFFFFFFFF ) // Rispristino fallito
				{
					thread->LastErrorCode = GetLastError();
					ret_code = (int)thread->LastErrorCode;
				}
				else // Rispristino eseguito con successo
				{
					thread->SuspendCount = (code <= 0)?0 :(code-1);
					if ( thread->SuspendCount == 0 ) // Se si � sospeso davvero
					{
						thread->Suspended = false;
					}
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per sospendere thread.
/// Questa funzione sospende il thread specificato nella struttura <thread>.
/// Se il thread � gi� sospeso incrementa il contatore delle sospensioni.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSSuspendThread( SYS_THREAD * thread )
{
	int   ret_code = SYS_KERNEL_NO_ERROR;
	DWORD code        ;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSSuspendThread");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			if ( thread->Handle != NULL )
			{
				code = SuspendThread( thread->Handle );
				if ( code == 0xFFFFFFFF ) // Sospensione fallita
				{
					thread->LastErrorCode = GetLastError();
					ret_code = (int)thread->LastErrorCode;
				}
				else
				{
					thread->SuspendCount  = code+1;
					thread->Suspended     = true;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per terminare thread.
/// Questa funzione termina il thread specificato nella struttura <thread>.
/// Questa funzione deve essere usata solo in casi estremi perch� lo stack non
/// viene disallocato ed eventuali DLL non vengono avvisate.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSTerminateThread( SYS_THREAD * thread )
{
	int           ret_code  = SYS_KERNEL_NO_ERROR;
	bool          code          ;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSTerminateThread");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			if ( thread->Handle != NULL ) // Controllo l'handle del thread
			{
				code = GetExitCodeThread( thread->Handle, &(thread->ExitCode) ); // Recupero l'exitcode
				if ( code == 0 ) // Funzione fallita
				{
					thread->LastErrorCode = GetLastError();
					ret_code = (int)thread->LastErrorCode;
				}
				else
				{
					code = TerminateThread( thread->Handle, (DWORD)thread->ExitCode ); // Termino il thread
					if ( code == 0 ) // Terminazione fallita
					{
						thread->LastErrorCode = GetLastError();
						ret_code = (int)thread->LastErrorCode;
					}
					else // Terminazione esguita con successo
					{
						thread->Terminated  = true;
						thread->Suspended   = true;
					}
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per attendere la fine di un thread (con timeout specificato)
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [07.05.2008]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SYSWaitForThread( SYS_THREAD * thread, int timeout )
{
	int           ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno
	unsigned long wait_code = 0;

	if ( SYSValidThread( thread ) )
	{
		if ( thread->Handle != NULL )
		{
			wait_code = WaitForSingleObject( thread->Handle, timeout );
			if ( wait_code == WAIT_FAILED )
			{
				ret_code = SYS_KERNEL_THREAD_WAIT_FAILED;
			}
			else if ( wait_code == WAIT_TIMEOUT )
			{
				ret_code = SYS_KERNEL_THREAD_WAIT_TIMEOUT;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per attendere la fine di un thread (con timeout infinito)
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [07.05.2008]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SYSWaitForThread( SYS_THREAD * thread )
{
	int           ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno

	ret_code = SYSWaitForThread( thread, INFINITE );

	return ret_code;
}

//==============================================================================
/// Funzione per uscire da thread.
/// Questa funzione termina il thread specificato nella struttura <thread>.
/// L'uso di questa funzione � preferibile rispetto a SYSTermiateThread perch�
/// esegue la disallocazione dello stack del thread e avvisa eventuali DLL.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.06
//------------------------------------------------------------------------------
int SYSExitThread( SYS_THREAD * thread )
{
	int           ret_code  = SYS_KERNEL_NO_ERROR;
	bool          code          ;
	DWORD		  exit_code		;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSExitThread");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			if ( thread->Handle != NULL )
			{
				code = GetExitCodeThread( thread->Handle, &(thread->ExitCode) ); // Recupero l'exitcode
				if ( code == 0 )
				{
					thread->LastErrorCode = GetLastError();
					ret_code = (int)thread->LastErrorCode;
				}
				else
				{
					exit_code = (DWORD)thread->ExitCode;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	try
	{
		ExitThread(exit_code);
		//thread->Terminated = true;
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_EXIT_THREAD_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per impostare la priorit� a thread.
/// Questa funzione imposta al thread specificato nella struttura <thread> la
/// priorit� <priority> specificata.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [06.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSThreadPriority( SYS_THREAD * thread, int priority )
{
	int             ret_code = SYS_KERNEL_NO_ERROR;
	bool            code        ;

	// --- Blocco l'accesso alla lista dei thread ---
	SYSLockThreadList("SYSThreadPriority");

	try
	{
		if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
		{
			if ( thread->Handle != NULL ) // Controllo l'handle del Thread
			{
				code = SetThreadPriority( thread->Handle, priority );
				if ( code == 0 )
				{
					thread->LastErrorCode = GetLastError();
					ret_code = (int)thread->LastErrorCode;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio l'accesso alla lista dei thread ---
	SYSUnlockThreadList();

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare strutture di thread.
/// Dato il puntatore <thread> alla struttura di un thread chiude l'eventuale
/// handle, se valido, e libera la memoria occupata da tale struttura.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [20.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.05
//------------------------------------------------------------------------------
int SYSDeleteThread( SYS_THREAD * thread )
{
	int		ret_code = SYS_KERNEL_NO_ERROR;
	bool 	code        ;

	if ( SYSValidThread( thread ) ) // Controllo la struttura del thread
	{
		// elimino il thread dalla lista dei thread
		ret_code = SYSRemoveThreadFromList(thread);

		if ( thread->Handle != NULL ) // Controllo l'handle del Thread
		{
			code = CloseHandle( thread->Handle );
			if ( code == 0 )
			{
				thread->LastErrorCode = GetLastError();
				ret_code = (int)thread->LastErrorCode;
			}
			else // Disalloco la struttura del thread
			{
				try
				{
					if (thread->Name != NULL)
					{
						free(thread->Name);
					}
					free( thread );
				}
				catch(...)
				{
					ret_code = SYS_KERNEL_CRITICAL_ERROR;
				}
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_THREAD_HANDLE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_THREAD_STRUCT;
	}

	return ret_code;
}



//==============================================================================
/// Funzione per ottenere l'Export su file di testo della lista lock
///
/// \date [05.05.2008]
/// \author Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SYSExportLockList( void )
{
	int 			ret_code 	= SYS_KERNEL_NO_ERROR;
	unsigned int	i;
	char * 			exportPath 	= NULL;
	SYS_LOCK * 		lock 		= NULL;
	char * 			cnv_value 	= NULL;

	// --- Accedo alla lista dei lock ---
	SYSLockLockList("SYSExportLockList");

	try
	{
		if ( LockList != NULL )
		{
			// --- Definizione di un EventLog manager per fare il Export della lista---
			SPV_EVENTLOG_MANAGER  * export_manager;
			char * exportFileName = NULL;
			char * exportTime = cnv_UInt32ToCharP(ULGetUnixTime());
			exportFileName = ULAddText(exportFileName,"SPVServer_LockList_");
			exportFileName = ULAddText(exportFileName,exportTime);
			exportFileName = ULAddText(exportFileName,".log");
			exportPath = ULAddText(exportPath,"C:\\Program Files\\Telefin\\SupervisorServer\\");
			exportPath = ULAddText(exportPath,exportFileName);
			// --- Creazione del nuovo EventLog manager ---
			export_manager = SPVNewEventLog( SPV_EVENTLOG_TYPE_ASCII_FILE, SPV_EVENTLOG_TEMPLATE_NONE, exportPath );

			ULFreeAndNullString(&exportFileName);
			ULFreeAndNullString(&exportTime);

			if ( export_manager != NULL )
			{
				// --- Apertura dell'EventLog Manager creato ---
				SPVOpenEventLog( export_manager->ID );

				if (export_manager->Opened)
				{
					// Intestazione del file
					SPVPrintEventLog( export_manager->ID, "[LockList]", strlen("[LockList]"), 0 );
					SPVPrintEventLog( export_manager->ID, " ", strlen(" "), 0 );
					int num = 0;
					char * numLock = NULL;
					char * descLock = NULL;
					char * valueLock = cnv_UInt32ToCharP(LockListCount);

					//Numero di lock attivi
					descLock = ULAddText(descLock,"LockCount=");
					descLock = ULAddText(descLock,valueLock);
					SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
					SPVPrintEventLog( export_manager->ID, " ", strlen(" "), 0 );
					ULFreeAndNullString(&valueLock);
					ULFreeAndNullString(&descLock);

					for ( i = 0; i < LockListMax; i++ )
					{
						try
						{
							lock = LockList[i];
							if ( SYSIsValidLock( lock ) )
							{
								// Numero del lock
								num++;
								numLock = cnv_UInt32ToCharP(num);
								descLock = ULAddText(descLock,"NumLock");
								descLock = ULAddText(descLock,numLock);
								descLock = ULAddText(descLock,"=");
								descLock = ULAddText(descLock,numLock);

								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&descLock);

								// Nome del lock
								valueLock = ULStrCpy( NULL, &lock->Name[0] ); // -MALLOC SYSExtractLockName(lock);
								descLock = ULAddText(descLock,"Name");
								descLock = ULAddText(descLock,numLock);
								descLock = ULAddText(descLock,"=");
								descLock = ULAddText(descLock,valueLock);
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&valueLock);
								ULFreeAndNullString(&descLock);

								// Nome del locker
								valueLock = ULStrCpy( NULL, &lock->Label[0] ); // -MALLOC  SYSExtractLockLabel(lock);
								descLock = ULAddText(descLock,"Locker");
								descLock = ULAddText(descLock,numLock);
								descLock = ULAddText(descLock,"=");
								descLock = ULAddText(descLock,valueLock);
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&valueLock);
								ULFreeAndNullString(&descLock);

								// --- Flag Initialized del Lock ---
							   /*	cnv_value = cnv_BoolToCharP( lock->Initialized );
								descLock = ULAddText( descLock, "Initialized" );
								//descLock = ULAddText( descLock, numLock );
								descLock = ULAddText( descLock, " = " );
								descLock = ULAddText( descLock, cnv_value );
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descLock);*/

								// --- Flag Locked del Lock ---
								cnv_value = cnv_BoolToCharP( lock->Locked );
								descLock = ULAddText( descLock, "Locked" );
								descLock = ULAddText( descLock, numLock );
								descLock = ULAddText( descLock, "=" );
								descLock = ULAddText( descLock, cnv_value );
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descLock);


								// Numero cicli del lock
								valueLock = cnv_UInt32ToCharP(lock->LockCounter);
								descLock = ULAddText(descLock,"LockCounter");
								descLock = ULAddText(descLock,numLock);
								descLock = ULAddText(descLock,"=");
								descLock = ULAddText(descLock,valueLock);
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&valueLock);
								ULFreeAndNullString(&descLock);

								// Ultima esecuzione del lock
								//valueLock = cnv_UInt32ToCharP(lock->LockDateTime);
								valueLock = cnv_DateTimeToCharP( lock->LockDateTime );
								descLock = ULAddText(descLock,"LockDateTime");
								descLock = ULAddText(descLock,numLock);
								descLock = ULAddText(descLock,"=");
								descLock = ULAddText(descLock,valueLock);
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&valueLock);
								ULFreeAndNullString(&descLock);

								// --- Flag PreLock del Lock ---
								cnv_value = cnv_BoolToCharP( lock->PreLock );
								descLock = ULAddText( descLock, "PreLock" );
								descLock = ULAddText( descLock, numLock );
								descLock = ULAddText( descLock, "=" );
								descLock = ULAddText( descLock, cnv_value );
								SPVPrintEventLog( export_manager->ID, descLock, strlen(descLock), 0 );
								ULFreeAndNullString(&cnv_value);
								ULFreeAndNullString(&descLock);

								ULFreeAndNullString(&numLock);
								SPVPrintEventLog( export_manager->ID, " ", strlen(" "), 0 );
							}
						}
						catch(...)
						{
							ret_code = SYS_KERNEL_EXPORT_LOCK_LIST_FAILURE;
						}
					}
					ret_code = SPVCloseEventLog( export_manager->ID );
				}

				if ( export_manager != NULL )
				{
					ret_code = SPVDeleteEventLog( export_manager->ID );
				}
			}
		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	// --- Rilascio la lista dei lock ---
	SYSUnlockLockList( );

	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		try
		{
			ret_code = SYSSetKeyLastLockListExportNameValue(exportPath);
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
			  //	ret_code = SYSSetKeyEnableLockListExportValue(false);
			}
		}
		catch(...)
		{
			ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
		}
	}


	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		try
		{
			ret_code = SYSSetKeyLastLockListExportNameValue("");
			//ret_code = SYSSetKeyEnableLockListExportValue(false);
		}
		catch(...)
		{
			ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
		}
	}

	ULFreeAndNullString(&exportPath);

	return ret_code;
}
//------------------------------------------------------------------------------


//==============================================================================
/// Funzione per gestire L'Export su file di testo della lista thread
///
/// \date [27.09.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int	SYSExportThreadListManager( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	try
	{
		if (SYSGetKeyEnableThreadListExportValue() == SYS_KERNEL_EXPORT_ENABLED)
		{
			ret_code = SYSExportThreadList();

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				ret_code = SYSExportLockList();
			}

		}
	}
	catch(...)
	{
		ret_code = SYS_KERNEL_FUNCTION_EXCEPTION;
	}

	return ret_code;
}
//------------------------------------------------------------------------------


//==============================================================================
/// Funzione per aprire un SCManager database
///
/// \date [24.11.2006]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SYSOpenSCManager( char * host, char * database, DWORD access_type )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// --- Open a handle to the SC Manager database. ---
	SYSSCManager = OpenSCManager( host, database, access_type );
	if ( SYSSCManager == NULL )
	{
		ret_code = SYS_KERNEL_SCM_OPEN_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere un SCManager Database
///
/// \date [29.05.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSCloseSCManager( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	if ( CloseServiceHandle( SYSSCManager ) == false )
	{
		ret_code = SYS_KERNEL_SCM_CLOSE_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per aprire un Servizio Win NT
///
/// \date [24.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
SC_HANDLE SYSOpenService( char * service, DWORD open_type )
{
	SC_HANDLE  schService = NULL;

	if ( service != NULL )
	{
		schService = OpenService( SYSSCManager, service, open_type );
	}

	return schService;
}

//==============================================================================
/// Funzione per chiudere un Servizio Win NT
///
/// \date [24.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SYSCloseService( SC_HANDLE schService )
{
	bool closeValue;

	if ( schService != NULL )
	{
		closeValue = CloseServiceHandle(schService);
	}
	return closeValue;
}

//==============================================================================
/// Funzione per verificare se un Servizio NT e' in esecuzione
///
/// Note:
///	La funzione ora torna un codice di ritorno diverso da 0 in caso di errore
///	Il valore booleano indicante il risultato della funzione viene passato
///	come il parametro "bool * isRunning"
///
/// \date [08.11.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int	SYSServiceIsRunning( SC_HANDLE schService, bool * isRunning )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	bool lIsRunning = false;

	SERVICE_STATUS_PROCESS  ssStatus          ;
	DWORD                   dwBytesNeeded     ;

	if ( schService != NULL )
	{
		if ( QueryServiceStatusEx(  schService,                      // handle to service
									SC_STATUS_PROCESS_INFO,          // info level
									(char*)&ssStatus,                // address of structure
									sizeof(SERVICE_STATUS_PROCESS),  // size of structure
									&dwBytesNeeded ) != 0 )          // if buffer too small
		{

			if ( ssStatus.dwCurrentState == SERVICE_RUNNING )
			{
				lIsRunning = true;
			}
		}
		else
		{

			ret_code = SYSCodifyQSSELastError(GetLastError());
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_SERVICE_HANDLE;
	}

	* isRunning = lIsRunning;

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se un Servizio NT e' arrestato
///
/// Note:
///	La funzione ora torna un codice di ritorno diverso da 0 in caso di errore
///	Il valore booleano indicante il risultato della funzione viene passato
///	come il parametro "bool * isStopped"
///
/// \date [08.11.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SYSServiceIsStopped( SC_HANDLE schService, bool * isStopped )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	bool lIsStopped = false;

	SERVICE_STATUS_PROCESS  ssStatus          ;
	DWORD                   dwBytesNeeded     ;

	if ( schService != NULL )
	{
		if ( QueryServiceStatusEx(  schService,                      // handle to service
									SC_STATUS_PROCESS_INFO,          // info level
									(char*)&ssStatus,                // address of structure
									sizeof(SERVICE_STATUS_PROCESS),  // size of structure
									&dwBytesNeeded ) != 0 )          // if buffer too small
		{
			if ( ssStatus.dwCurrentState == SERVICE_STOPPED )
			{
				lIsStopped = true;
			}
		}
		else
		{

			ret_code = SYSCodifyQSSELastError(GetLastError());
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_SERVICE_HANDLE;
	}

	* isStopped = lIsStopped;

	return ret_code;
}


//==============================================================================
/// Funzione per verificare se un Servizio NT e' in pausa
///
/// Note:
///	La funzione ora torna un codice di ritorno diverso da 0 in caso di errore
///	Il valore booleano indicante il risultato della funzione viene passato
///	come il parametro "bool * isPaused"
///
/// \date [08.11.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SYSServiceIsPaused( SC_HANDLE schService, bool * isPaused )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	bool lIsPaused = false;

	SERVICE_STATUS_PROCESS  ssStatus          ;
	DWORD                   dwBytesNeeded     ;

	if ( schService != NULL )
	{
		if ( QueryServiceStatusEx( 	schService,                      // handle to service
									SC_STATUS_PROCESS_INFO,          // info level
									(char*)&ssStatus,                // address of structure
									sizeof(SERVICE_STATUS_PROCESS),  // size of structure
									&dwBytesNeeded ) != 0 )          // if buffer too small
		{
			if ( ssStatus.dwCurrentState == SERVICE_PAUSED )
			{
				lIsPaused = true;
			}
		}
		else
		{

			ret_code = SYSCodifyQSSELastError(GetLastError());
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_SERVICE_HANDLE;
	}

	* isPaused = lIsPaused;

	return ret_code;
}

//==============================================================================
/// Funzione per verificare se un Servizio NT e' in uno stato pendente
///
/// Note:
///	La funzione ora torna un codice di ritorno diverso da 0 in caso di errore
///	Il valore booleano indicante il risultato della funzione viene passato
///	come il parametro "bool * isPending"
///
/// \date [08.11.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SYSServiceIsPending( SC_HANDLE schService, bool * isPending )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	bool lIsPending = false;

	SERVICE_STATUS_PROCESS  ssStatus          ;
	DWORD                   dwBytesNeeded     ;

	if ( schService != NULL )
	{
		if ( QueryServiceStatusEx( 	schService,                      // handle to service
									SC_STATUS_PROCESS_INFO,          // info level
									(char*)&ssStatus,                // address of structure
									sizeof(SERVICE_STATUS_PROCESS),  // size of structure
									&dwBytesNeeded ) != 0 )          // if buffer too small
		{
			if (   ssStatus.dwCurrentState == SERVICE_START_PENDING
				|| ssStatus.dwCurrentState == SERVICE_STOP_PENDING
				|| ssStatus.dwCurrentState == SERVICE_CONTINUE_PENDING
				|| ssStatus.dwCurrentState == SERVICE_PAUSE_PENDING )
			{
				lIsPending = true;
			}
		}
		else
		{

			ret_code = SYSCodifyQSSELastError(GetLastError());
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_SERVICE_HANDLE;
	}

	* isPending = lIsPending;

	return ret_code;
}

//==============================================================================
/// Funzione per avviare un Servizio NT
///
/// \date [08.11.2006]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SYSStartService( SC_HANDLE schService )
{
	int                     ret_code = SYS_KERNEL_NO_ERROR;

	SERVICE_STATUS_PROCESS  ssStatus          ;
	DWORD                   dwOldCheckPoint   ;
	DWORD                   dwWaitTime        ;
	DWORD                   dwBytesNeeded     ;
	int                     tryCount          ;

	if ( schService != NULL)
	{
		if ( StartService( schService, 0, NULL ) != 0 )
		{
			// Check the status until the service is no longer start pending.
			if ( QueryServiceStatusEx(
					schService,             			// handle to service
					SC_STATUS_PROCESS_INFO, 			// info level
					(char*)&ssStatus,              		// address of structure
					sizeof(SERVICE_STATUS_PROCESS), 	// size of structure
					&dwBytesNeeded ) != 0 )             // if buffer too small
			{
				dwOldCheckPoint = ssStatus.dwCheckPoint;

				tryCount = 0;
				while ( ssStatus.dwCurrentState == SERVICE_START_PENDING )
				{
					// Do not wait longer than the wait hint. A good interval is
					// one tenth the wait hint, but no less than 1 second and no
					// more than 10 seconds.
					///dwWaitTime = ssStatus.dwWaitHint / 10;
					dwWaitTime = 1000;

					Sleep( dwWaitTime );

					// Check the status again.
					if ( QueryServiceStatusEx(
							schService,                     // handle to service
							SC_STATUS_PROCESS_INFO,         // info level
							(char*)&ssStatus,               // address of structure
							sizeof(SERVICE_STATUS_PROCESS), // size of structure
							&dwBytesNeeded ) != 0 )         // if buffer too small
					{
						if ( ssStatus.dwCheckPoint > dwOldCheckPoint )
						{
							// The service is making progress.
							dwOldCheckPoint = ssStatus.dwCheckPoint;
							tryCount++;
						}
						else
						{
							if (tryCount > 30)
							{
								// No progress made within the wait hint
								break;
							}
						}
					}
					else
					{
						break;
					}
				}


				if ( ssStatus.dwCurrentState != SERVICE_RUNNING )
				{
					ret_code = SYS_KERNEL_SERVICE_NOT_RUNNING;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_SERVICE_QUERY_FAILURE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_START_FAILURE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per arrestare un Servizio NT
///
/// \date [08.11.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SYSStopService( SC_HANDLE schService, DWORD dwTimeout )
{
	int             ret_code          = SYS_KERNEL_NO_ERROR;

	SERVICE_STATUS_PROCESS  ssStatusProcess   ;
	SERVICE_STATUS			ssStatus          ;
	DWORD           		dwWaitTime        ;
	DWORD           		dwStartTime       = GetTickCount();
	DWORD           		dwBytesNeeded     ;
	//DWORD           dwTimeout         = 5000;

	if ( schService != NULL )
	{
		// Make sure the service is not already stopped
		if ( QueryServiceStatusEx( schService, SC_STATUS_PROCESS_INFO, (LPBYTE)&ssStatusProcess,
		   sizeof(SERVICE_STATUS_PROCESS), &dwBytesNeeded ) != 0 )
		{
			if ( ssStatusProcess.dwCurrentState != SERVICE_STOPPED )
			{
				// If a stop is pending, just wait for it
				while ( ssStatusProcess.dwCurrentState == SERVICE_STOP_PENDING )
				{
					Sleep( ssStatusProcess.dwWaitHint );
					if ( QueryServiceStatusEx(
						schService,
						SC_STATUS_PROCESS_INFO,
						(LPBYTE)&ssStatusProcess,
						sizeof(SERVICE_STATUS_PROCESS),
						&dwBytesNeeded ) != 0 )
					{
						if ( ssStatusProcess.dwCurrentState == SERVICE_STOPPED )
						{
							ret_code = SYS_KERNEL_NO_ERROR;
							break;
						}
						else
						{
							if ( GetTickCount() - dwStartTime > dwTimeout )
							{
								ret_code = SYS_KERNEL_SERVICE_CONTROL_TIMEOUT;
								break;
							}
						}
					}
					else
					{
						ret_code = SYS_KERNEL_SERVICE_QUERY_FAILURE;
						break;
					}
				}

				if ( ControlService( schService, SERVICE_CONTROL_STOP, &ssStatus ) != 0 )
				{

					// Wait for the service to stop
					while ( ssStatus.dwCurrentState != SERVICE_STOPPED )
					{
						Sleep( ssStatus.dwWaitHint );
						if ( QueryServiceStatusEx(
								schService,
								SC_STATUS_PROCESS_INFO,
								(LPBYTE)&ssStatusProcess,
								sizeof(SERVICE_STATUS_PROCESS),
								&dwBytesNeeded ) != 0 )
						{
							if ( ssStatusProcess.dwCurrentState == SERVICE_STOPPED )
							{
								ret_code = SYS_KERNEL_NO_ERROR;
								break;
							}
							else
							{
								if ( GetTickCount() - dwStartTime > dwTimeout )
								{
									ret_code = SYS_KERNEL_SERVICE_CONTROL_TIMEOUT;
									break;
								}
							}
						}
						else
						{
							ret_code = SYS_KERNEL_SERVICE_QUERY_FAILURE;
							break;
						}
					}
				}
				else
				{
					ret_code = SYS_KERNEL_SERVICE_CONTROL_FAILURE;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_SERVICE_ALREADY_STOPPED;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_QUERY_FAILURE;
		}

	}
	else
	{
		ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per arrestare e far ripartire un Servizio NT
///
/// \date [24.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSRestartService(SC_HANDLE schService, DWORD dwTimeout )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	bool serviceIsStopped;

	if ( schService != NULL )
	{

		ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);
		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			if (! serviceIsStopped)
			{
				ret_code = SYSStopService(schService,dwTimeout);
			}
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSStartService(schService);
		}

		/*
		if( ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (serviceIsStopped)
				{
					ret_code = SYSStartService(schService);
				}
				else
				{
					ret_code = SYS_KERNEL_SERVICE_NOT_STOPPED;
				}
			}
		}*/
	}
	else
	{
		ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per recuperare la modalit� di partenza di un Servizio NT
/// (Automtico,Manuale,Disabilitato)
/// con gestione dell'handle del servizio
///
/// \date [24.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSGetServiceStartType(SC_HANDLE schService)
{
	int                     ret_code = SYS_KERNEL_NO_ERROR;

	LPQUERY_SERVICE_CONFIG  lpqscBuf          ;
	DWORD                   dwBytesNeeded     ;

	if ( schService != NULL)
	{
		// Allocate a buffer for the configuration information.
		lpqscBuf = (LPQUERY_SERVICE_CONFIG) LocalAlloc(LPTR, 4096);
		if (lpqscBuf != NULL)
		{
			if ( QueryServiceConfig(
				 schService,                      // handle to service
				 lpqscBuf,                        // buffer of structure
				 4096,                            // size of structure
				 &dwBytesNeeded ) != 0 )          // if buffer too small
			{
				if (lpqscBuf->dwStartType == SERVICE_AUTO_START)
				{
					ret_code = SYS_KERNEL_QSC_SERVICE_AUTO_START;
				}
				else
				if (lpqscBuf->dwStartType == SERVICE_DEMAND_START)
				{
					ret_code = SYS_KERNEL_QSC_SERVICE_DEMAND_START;
				}
				else
				if (lpqscBuf->dwStartType == SERVICE_DISABLED)
				{
					ret_code = SYS_KERNEL_QSC_SERVICE_DISABLED;
				}
				else
				{
					ret_code = SYS_KERNEL_QSC_ERROR;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_QSC_ERROR;
			}

			LocalFree(lpqscBuf);
		}
		else
		{
			ret_code = SYS_KERNEL_QSC_ERROR;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
	}

	return ret_code;
}



//==============================================================================
/// Funzione per settare la modalit� di partenza di un Servizio NT
/// (Automtico,Manuale,Disabilitato)
/// Richiede apertura del servizio con modalit� "SERVICE_CHANGE_CONFIG"
///
/// \date [24.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SYSSetServiceStartType(DWORD dwStartTypeValue, SC_HANDLE schService )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	if ( schService != NULL)
	{

		// Make the changes.
		if (ChangeServiceConfig(
				schService,        // handle of service
				SERVICE_NO_CHANGE, // service type: no change
				dwStartTypeValue,       // change service start type
				SERVICE_NO_CHANGE, // error control: no change
				NULL,              // binary path: no change
				NULL,              // load order group: no change
				NULL,              // tag ID: no change
				NULL,              // dependencies: no change
				NULL,              // account name: no change
				NULL,              // password: no change
				NULL)              // display name: no change
				!= 0)
		{
			ret_code = SYS_KERNEL_NO_ERROR;
		}
		else
		{
			ret_code = SYS_KERNEL_CSC_ERROR;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova struttura buffer binario
///
/// \date [24.03.2006]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
SYS_BUFFER * SYSNewBuffer( unsigned char * buffer, int len, long datetime )
{
  // --- Alloco la memoria per la nuova struttura buffer ---
  SYS_BUFFER * bbuffer = (SYS_BUFFER*)malloc(sizeof(SYS_BUFFER)); // -MALLOC

  if ( bbuffer != NULL )
  {
    // --- Azzero l'area di memoria allocata ---
    memset( bbuffer, 0, sizeof(SYS_BUFFER) );
    // --- Marchio l'area di memoria con il VCC ---
    bbuffer->VCC = SYS_KERNEL_BUFFER_VALIDITY_CHECK_CODE;
    // --- Alloco memoria per un eventuale buffer ---
    if ( len > 0 && len <= SYS_KERNEL_PORT_MAX_BUFFER_LENGTH )
    {
      bbuffer->BufferLen  = len;
      try
	  {
        memcpy( bbuffer->Buffer, buffer, sizeof(unsigned char) * len );
      }
      catch(...)
      {
        bbuffer->BufferLen  = len;
      }
    }
    // --- Marchio la struttura con la data/ora specificata ---
    bbuffer->Time = datetime;
  }

  return bbuffer;
}

//==============================================================================
/// Funzione per eliminare una struttura buffer binario
///
/// \date [24.03.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSDeleteBuffer( SYS_BUFFER * bbuffer )
{
  int ret_code = SYS_KERNEL_NO_ERROR;

  if ( SYSValidBuffer( bbuffer ) ) // Controllo della validita'
  {
    try
    {
      free( bbuffer );
    }
    catch(...)
    {
      ret_code = SYS_KERNEL_CRITICAL_ERROR;
	}
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_BUFFER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per verificare la validita' di una struttura buffer binario
///
/// \date [08.11.2007]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
bool SYSValidBuffer( SYS_BUFFER * bbuffer )
{
  bool  valid     = true;
  bool  is_buffer = false;
  bool  is_buflen = false;

  if ( bbuffer != NULL )
  {
	try
    {
      if ( bbuffer->VCC == SYS_KERNEL_BUFFER_VALIDITY_CHECK_CODE )
      {
        if ( bbuffer->Buffer != NULL )
        {
          is_buffer = true;
        }
        if ( bbuffer->BufferLen > 0 )
        {
		  is_buflen = true;
        }
        if ( is_buflen == !is_buffer )
        {
          valid = false;
        }
      }
      else
      {
        valid = false;
      }
    }
	catch(...)
    {
      valid = false;
    }
  }
  else
  {
    valid = false;
  }

  return valid;
}

//==============================================================================
/// Funzione di attivazione per un thread di gestione buffer in ingresso
///
/// \date [09.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.10
//------------------------------------------------------------------------------
unsigned long __stdcall SYSInputBufferThread( void * parameter )
{
	unsigned long         ret_code    = SYS_KERNEL_NO_ERROR   ; // Codice di ritorno
	int                   fun_code    = SYS_KERNEL_NO_ERROR   ;
	SYS_PORT *            port        = NULL; // Puntatore alla porta
	SYS_THREAD *          thread      = NULL; // Puntatore al thread
	long                  time        = 0   ; // Unix Time
	unsigned char         tbuffer[SYS_KERNEL_PORT_MAX_BUFFER_LENGTH]; // Buffer di ricezione
	int                   bytes_rcvd  = 0   ; // Numero effettivo di byte ricevuti
	SYS_BUFFER *          bbuffer     = NULL  ;
	char *         		  thread_name = NULL  ; // Stringa del nome del thread
	char *				  lock_name	  = NULL;

	try
	{
		port = (SYS_PORT*) parameter;
		if ( port != NULL )
		{
			thread = port->InBufferThread; // Recupero il thread
			if ( SYSValidThread( thread ) )
			{
				// assegno il nome al thread
				try
				{
					thread_name = ULAddText(thread_name,port->Name);
					thread_name = ULAddText(thread_name," - InputBuffer");
				}
				catch(...)
				{
					fun_code = SYS_KERNEL_FUNCTION_EXCEPTION;
				}

				if (fun_code == SYS_KERNEL_NO_ERROR)
				{
					fun_code = SYSSetThreadName(thread,thread_name);
				}
				/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */
				thread->StepID = 1;

				while ( !thread->Terminated ) // Ciclo del thread di protocollo
				{
					// --- Gestione Half-Duplex ---
					/*
					if ( port->Type == SYS_KERNEL_PORT_TYPE_STLC1000_RS485 )
					{
						EnterCriticalSection( &port->OutCriticalSection );
						Sleep( 50 );
					}
					*/
					// --- Entro nella sezione critica ---
					//EnterCriticalSection( &port->InCriticalSection );
					lock_name = ULAddText(NULL,"SYSInputBufferThread: ");
					lock_name = ULAddText(lock_name,thread_name);
					SYSLock(&port->InLock,lock_name);
					// --- Cancello la stringa del nome lock ---
					try
					{
						if ( lock_name != NULL ) free( lock_name );
					}
					catch(...)
					{
						// Eccezione non loggata
						thread->StepID = 11;
					}
					try
					{
						if ( port->Active ) // Se la porta � attiva
						{
							if (thread->StepID == 3) thread->StepID = 2;
							// --- Prendo il tempo ---
							time = ULGetUnixTime();
							// --- Provo a leggere qualcosa ---
							fun_code = SYSReadPort( port, (char*)&tbuffer[0], SYS_KERNEL_PORT_MAX_BUFFER_LENGTH, &bytes_rcvd );
							if ( fun_code == 0 ) // Se la lettura � andata a buon fine
							{
								if ( bytes_rcvd > 0 ) // Se ho ricevuto un buffer
								{
									// --- Creo una struttura buffer binario ---
									bbuffer = SYSNewBuffer( &tbuffer[0], bytes_rcvd, time );
									// --- Aggiungo il nuovo buffer alla FIFO ---
									SYSWriteFIFOItem( port->InBuffer, (void*)bbuffer ); // Aggiungo il buffer binario alla FIFO di input
								}
								else
								{
								}
							}
							else
							{
								if ( fun_code == SYS_SERIAL_READ_FAILURE || fun_code == SYS_SOCKET_RECV_FAILURE )
								{
									port->Active = false;
									if (thread->StepID < 12) thread->StepID = 12;
								}
							}
						}
						else
						{
							if (thread->StepID == 2) thread->StepID = 3;
						}
					}
					catch(...)
					{
						ret_code = (unsigned long)SYS_KERNEL_CRITICAL_ERROR;
						if (thread->StepID < 14) thread->StepID = 14;
					}
					// --- Esco nella sezione critica ---
					//LeaveCriticalSection( &port->InCriticalSection );
					SYSUnLock(&port->InLock);
					// --- Aspetto un tempo di ritardo standard ---
					Sleep( SYS_KERNEL_BUFFER_THREAD_SLEEP );
					// --- Gestione Half-Duplex ---
					/*
					if ( port->Type == SYS_KERNEL_PORT_TYPE_STLC1000_RS485 )
					{
						Sleep( 50 );
						LeaveCriticalSection( &port->OutCriticalSection );
					}
					*/
					try
					{
						// Aggiornamento delle informazioni di debug del thread
						SYSUpdateThreadDebugInfo(thread);
					}
					catch(...)
					{
                    	if (thread->StepID < 15) thread->StepID = 15;
                    }
				}
				thread->StepID = 3;
				// --- Cancello la stringa del nome thread ---
				try
				{
					if ( thread_name != NULL ) free( thread_name );
				}
				catch(...)
				{
					// Eccezione non loggata
				}
			}
			else
			{

			}
		}
		else
		{

		}
	}
	catch (...)
	{
		ret_code = (unsigned long)SYS_KERNEL_CRITICAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione di attivazione per un thread di gestione buffer in uscita
///
/// \date [09.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.09
//------------------------------------------------------------------------------
unsigned long __stdcall SYSOutputBufferThread( void * parameter )
{
	unsigned long         ret_code    = SYS_KERNEL_NO_ERROR   ; // Codice di ritorno
	int                   fun_code    = SYS_KERNEL_NO_ERROR   ;
	SYS_PORT *            port        = NULL; // Puntatore alla porta
	SYS_THREAD *          thread            ; // Puntatore al thread
	SYS_BUFFER *          bbuffer     = NULL;
	int                   bytes_sent  = 0   ; // Numero effettivo di byte ricevuti
	char *				  thread_name = NULL;
	char *				  lock_name   = NULL;
	try
	{
		port = (SYS_PORT*) parameter; // Recupero la porta
		if ( port != NULL )
		{
			thread = port->OutBufferThread; // Recupero il thread
			if ( thread != NULL )
			{
				// assegno il nome al thread
				try
				{
					thread_name = ULAddText(thread_name,port->Name);
					thread_name = ULAddText(thread_name," - OutputBuffer");
				}
				catch(...)
				{
					fun_code = SYS_KERNEL_FUNCTION_EXCEPTION;
				}
				if (fun_code == SYS_KERNEL_NO_ERROR)
				{
					fun_code = SYSSetThreadName(thread,thread_name);
				}
				/* TODO -oMario : Esportare fun_code su EventLog in caso di errore o eccezione */
				thread->StepID = 1;

				while ( !thread->Terminated ) // Ciclo del thread di protocollo
				{
					// --- Entro nella sezione critica ---
					// --- Gestione Half-Duplex ---
					/*
					if ( port->Type == SYS_KERNEL_PORT_TYPE_STLC1000_RS485 )
					{
						EnterCriticalSection( &port->InCriticalSection );
						Sleep( 50 );
					}
					*/
					//EnterCriticalSection( &port->OutCriticalSection );
					lock_name = ULAddText(NULL,"SYSOutputBufferThread: ");
					lock_name = ULAddText(lock_name,thread_name);
					SYSLock(&port->OutLock,lock_name);
					// --- Cancello la stringa del nome lock ---
					try
					{
						if ( lock_name != NULL ) free( lock_name );
					}
					catch(...)
					{
						// Eccezione non loggata
					}
					///EnterCriticalSection( &port->InCriticalSection );
					try
					{
	                	thread->StepID = 2;
						if ( port->Active )
						{
							// --- Recupero il primo elemento dalla FIFO ---
							bbuffer = (SYS_BUFFER*) SYSReadFIFOItem( port->OutBuffer );
							if ( bbuffer != NULL )
							{
								if ( SYSValidBuffer( bbuffer ) )
								{
									ret_code = SYSWritePort( port, bbuffer->Buffer, bbuffer->BufferLen, &bytes_sent );
									if ( bytes_sent > 0 )
									{
									}
									SYSDeleteBuffer( bbuffer );
								}
							}
						}
						else
						{
						}
					}
					catch(...)
					{
						ret_code = (unsigned long)SYS_KERNEL_CRITICAL_ERROR;
					}
					//--- Esco nella sezione critica ---
					//LeaveCriticalSection( &port->OutCriticalSection );
					SYSUnLock(&port->OutLock);
					// --- Aspetto un tempo di ritardo predefinito ---
					Sleep( SYS_KERNEL_BUFFER_THREAD_SLEEP );
					// --- Gestione Half-Duplex ---
					/*
					if ( port->Type == SYS_KERNEL_PORT_TYPE_STLC1000_RS485 )
					{
						Sleep( 50 );
						LeaveCriticalSection( &port->InCriticalSection );
					}
					*/
					///LeaveCriticalSection( &port->InCriticalSection );

					// Aggiornamento delle informazioni di debug del thread
					SYSUpdateThreadDebugInfo(thread);
				}
				thread->StepID = 3;
				// --- Cancello la stringa del nome thread ---
				try
				{
					if ( thread_name != NULL ) free( thread_name );
				}
				catch(...)
				{
					// Eccezione non loggata
				}
			}
			else
			{

			}
		}
		else
		{

		}
	}
	catch (...)
	{
		ret_code = (unsigned long)SYS_KERNEL_CRITICAL_ERROR;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova struttura porta di comunicazione
/// Alloca una nuova struttura per contenere le propriet� di una porta di
/// comunicazione quali tipo, parametri e stato.
/// Restituisce il puntatore della nuova struttura creata.
/// In caso di errore restituisce NULL.
///
/// \date [12.05.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.18
//------------------------------------------------------------------------------
SYS_PORT * SYSNewPort( int type )
{
	SYS_PORT *			port        = NULL  ; // Ptr alla nuova struttura porta com
	SYS_SERIAL_PARAMS *	p_serial    = NULL  ; // Parametri della porta seriale
	SYS_SERIAL_STATUS * s_serial    = NULL  ; // Stato della porta seriale
	SYS_SOCKET *		p_socket    = NULL  ; // Parametri della porta socket
	SYS_SOCKET_STATUS * s_socket    = NULL  ; // Stato della porta socket
	XML_PORT *			p_xml_port	= NULL	; // Parametri della porta XML
	XML_PORT_STATUS *	s_xml_port	= NULL	; // Stato della porta XML
	void *				params      = NULL  ; // Parametri della porta di com.
	void *				status      = NULL  ; // Stato della porta di com.
	SYS_FIFO *			fifo_in     = NULL  ; // Ptr ad una FIFO per buffer in ingresso
	SYS_FIFO *			fifo_out    = NULL  ; // Ptr ad una FIFO per buffer in uscita
	SYS_THREAD *		thread_in   = NULL  ; // Ptr ad un thread per buffer in ingresso
	SYS_THREAD *		thread_out  = NULL  ; // Ptr ad un thread per buffer in uscita


	port = (SYS_PORT*) malloc( sizeof(SYS_PORT) );

	if ( port != NULL ) // Se l'allocazione � andata a buon fine
	{
		port->ID              = 0     ;
		port->Type            = type  ;
		port->Name            = NULL  ;
		port->Active          = false ;
		port->Shared          = false ;
		port->InBufferLen     = 0     ;
		port->InBuffer        = NULL  ;
		port->InBufferThread  = NULL  ;
		port->OutBufferLen    = 0     ;
		port->OutBuffer       = NULL  ;
		port->OutBufferThread = NULL  ;
		port->ThreadSleep			= SYS_KERNEL_BUFFER_THREAD_SLEEP;

		//InitializeCriticalSection( &port->CriticalSection    ); // Inizializzo la sezione critica generale della porta
		char * gen_port_name = NULL;
		gen_port_name = ULAddText(gen_port_name,port->Name);
		gen_port_name = ULAddText(gen_port_name," - port->GeneralLock");
		SYSInitLock(&port->GeneralLock,gen_port_name); // Inizializzo il lock generale della porta

		try
		{
			if ( gen_port_name != NULL ) free( gen_port_name );
		}
		catch(...)
		{
			// Eccezione non loggata
		}

		//InitializeCriticalSection( &port->InCriticalSection  ); // Inizializzo la sezione critica in ingresso della porta
		char * in_port_name = NULL;
		in_port_name = ULAddText(in_port_name,port->Name);
		in_port_name = ULAddText(in_port_name," - port->InLock");
		SYSInitLock(&port->InLock,in_port_name); // Inizializzo il lock di Input della porta

		try
		{
			if ( in_port_name != NULL ) free( in_port_name );
		}
		catch(...)
		{
			// Eccezione non loggata
		}

		//InitializeCriticalSection( &port->OutCriticalSection ); // Inizializzo la sezione critica in uscita della porta
		char * out_port_name = NULL;
		out_port_name = ULAddText(out_port_name,port->Name);
		out_port_name = ULAddText(out_port_name," - port->OutLock");
		SYSInitLock(&port->OutLock,out_port_name); // Inizializzo il lock di Output della porta

		try
		{
			if ( out_port_name != NULL ) free( out_port_name );
		}
		catch(...)
		{
			// Eccezione non loggata
		}

		switch ( port->Type )
		{
			case SYS_KERNEL_PORT_TYPE_SERIAL: // Caso seriale generica
			case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
				// --- Da sostituire con una funzione del modulo SYSSerial ---
				p_serial      = (SYS_SERIAL_PARAMS*) malloc( sizeof(SYS_SERIAL_PARAMS) );
				s_serial      = (SYS_SERIAL_STATUS*) malloc( sizeof(SYS_SERIAL_STATUS) );
				params        = (void*) p_serial;
				status        = (void*) s_serial;
				fifo_in       = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				fifo_out      = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				// --- Creo un thread di gestione del buffer in ingresso ---
				thread_in     = SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
				// --- Creo un thread di gestione del buffer in uscita ---
				thread_out     = SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			case SYS_KERNEL_PORT_TYPE_SOCKET: // Caso socket (sottinteso TCP client)
			case SYS_KERNEL_PORT_TYPE_TCP_CLIENT: // Caso socket TCP Client
				// --- Da sostituire con una funzione del modulo SYSSocket ---
				port->Shared	= true; // La porta � condivisibile
				p_socket		= SYSNewSocket( SYS_SOCKET_TYPE_TCP_CLIENT );
				s_socket		= &p_socket->Status;
				params			= (void*) p_socket;
				status			= (void*) s_socket;
				fifo_in			= SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				fifo_out		= SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				// --- Creo un thread di gestione del buffer in ingresso ---
				thread_in		= SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
				// --- Creo un thread di gestione del buffer in uscita ---
				thread_out		= SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
				// --- Da sostituire con una funzione del modulo SYSSocket ---
				port->Shared	= true; // La porta � condivisibile
				p_socket		= SYSNewSocket( SYS_SOCKET_TYPE_UDP_CLIENT );
				s_socket		= &p_socket->Status;
				params			= (void*) p_socket;
				status			= (void*) s_socket;
				fifo_in			= SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				fifo_out		= SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				// --- Creo un thread di gestione del buffer in ingresso ---
				thread_in		= SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
				// --- Creo un thread di gestione del buffer in uscita ---
				thread_out		= SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
				// --- Da sostituire con una funzione del modulo SYSSocket ---
				port->Shared  = true; // La porta � condivisibile
				p_socket      = SYSNewSocket( SYS_SOCKET_TYPE_TCP_SERVER );
				s_socket      = &p_socket->Status;
				params        = (void*) p_socket;
				status        = (void*) s_socket;
				fifo_in       = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				fifo_out      = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				// --- Creo un thread di gestione del buffer in ingresso ---
				thread_in     = SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
				// --- Creo un thread di gestione del buffer in uscita ---
				thread_out    = SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				// --- Da sostituire con una funzione del modulo SYSSocket ---
				port->Shared  = true; // La porta � condivisibile
				p_socket      = SYSNewSocket( SYS_SOCKET_TYPE_UDP_SERVER );
				s_socket      = &p_socket->Status;
				params        = (void*) p_socket;
				status        = (void*) s_socket;
				fifo_in       = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				fifo_out      = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				// --- Creo un thread di gestione del buffer in ingresso ---
				thread_in     = SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
				// --- Creo un thread di gestione del buffer in uscita ---
				thread_out    = SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			// --- Caso SNMP Manager (via SSP) ---
			case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
				port->Shared  = true;
				p_socket      = SYSNewSocket( SYS_SOCKET_TYPE_TCP_CLIENT );
				s_socket      = &p_socket->Status;
				params        = (void*) p_socket;
				status        = (void*) s_socket;
				fifo_in       = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				fifo_out      = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
				// --- Creo un thread di gestione del buffer in ingresso ---
				thread_in     = SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
				// --- Creo un thread di gestione del buffer in uscita ---
				thread_out    = SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			// --- Caso XML Port (lettura file XML) ---
			case SYS_KERNEL_PORT_TYPE_XML_PORT:
			    p_xml_port		= XMLNewPort( "default_port.xml" );
				s_xml_port		= &p_xml_port->Status;
				params				= (void*) p_xml_port;
			    status				= (void*) s_xml_port;
			    fifo_in       = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
			    fifo_out      = SYSNewFIFO( SYS_KERNEL_FIFO_MAX_ITEMS );
			    // --- Creo un thread di gestione del buffer in ingresso ---
			    thread_in     = SYSNewThread( SYSInputBufferThread, 0, CREATE_SUSPENDED );
			    // --- Creo un thread di gestione del buffer in uscita ---
				thread_out    = SYSNewThread( SYSOutputBufferThread, 0, CREATE_SUSPENDED );
			break;
			// --- Caso default ---
			default:
			    params = NULL;
				status = NULL;
		}

		// --- Impostazione e creazione del thread input ---
		if ( thread_in != NULL )
		{
			SYSSetThreadParameter( thread_in, (void*) port ); // Gli passo come parametro la str. porta
			SYSCreateThread( thread_in );
		}
		// --- Impostazione e creazione del thread output ---
		if ( thread_out != NULL )
		{
			SYSSetThreadParameter( thread_out, (void*) port ); // Gli passo come parametro la str. porta
			SYSCreateThread( thread_out );
		}

		// --- Inizializzazione delle FIFO dei buffer ---
		port->InBuffer        = fifo_in   ;
		port->InBufferLen     = 0         ;
		port->InBufferCount   = 0         ;
		port->InBufferThread  = thread_in ;
		port->OutBuffer       = fifo_out  ;
		port->OutBufferLen    = 0         ;
		port->OutBufferCount  = 0         ;
		port->OutBufferThread = thread_out;
		// --- Inizializzazione delle strutture parametri e stato ---
		port->Params          = params    ;
		port->Status          = status    ;
		// --- Inizializzazione ptr strutture aggiuntive
		port->Monitor         = NULL      ;
		port->EventLog        = NULL      ;
	}

	return port;
}

//==============================================================================
/// Funzione per aprire una porta di comunicazione
///
/// \date [16.07.2007]
/// \author Enrico Alborali
/// \version 0.12
//------------------------------------------------------------------------------
int SYSOpenPort( SYS_PORT * port )
{
	int         ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno
	int         fun_code  = SYS_KERNEL_NO_ERROR;

	if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
	{
		switch ( port->Type )
		{
			case SYS_KERNEL_PORT_TYPE_SERIAL:
            case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
            case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
            case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
				ret_code = SYSSerialOpen( (SYS_SERIAL_PARAMS*) port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_SOCKET:
			case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
				ret_code = SYSOpenSocket( (SYS_SOCKET*) port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
				ret_code = SYSOpenSocket( (SYS_SOCKET*) port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
				ret_code = SYSOpenSocket( (SYS_SOCKET*) port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				ret_code = SYSOpenSocket( (SYS_SOCKET*) port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
				ret_code = SYSOpenSocket( (SYS_SOCKET*) port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_XML_PORT:
				ret_code = XMLOpenPort( (XML_PORT*) port->Params );
			break;
			default:
				ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
		}

		///Sleep( 100 );

		// --- Imposto i parametri della porta ---
		if ( ret_code == SYS_KERNEL_NO_ERROR )
		{
			ret_code = SYSPortSetParams( port );
		}

		///Sleep( 100 );

		// --- Faccio partire il thread di gestione buffer in ingresso ---
		if ( port->InBufferThread != NULL )
		{
			fun_code = SYSResumeThread( port->InBufferThread );
			if ( fun_code != SYS_KERNEL_NO_ERROR )
			{
				ret_code = SYS_KERNEL_THREAD_RESUME_FAILURE;
			}
		}

		///Sleep( 100 );

		// --- Faccio partire il thread di gestione buffer in uscita ---
		if ( port->OutBufferThread != NULL )
		{
			fun_code = SYSResumeThread( port->OutBufferThread );
			if ( fun_code != SYS_KERNEL_NO_ERROR )
			{
				ret_code = SYS_KERNEL_THREAD_RESUME_FAILURE;
			}
		}

		if ( ret_code == 0 )
		{
			port->Active = true;
		}
		else
		{
			port->Active = false;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_PORT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere una porta di comunicazione
///
/// \date [30.04.2008]
/// \author Enrico Alborali
/// \version 0.10
//------------------------------------------------------------------------------
int SYSClosePort( SYS_PORT * port )
{
	int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

	if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
	{
		if ( port->Active ) {
		    switch ( port->Type )
		    {
		    	case SYS_KERNEL_PORT_TYPE_SERIAL:
		    	case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
		    	case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
		    	case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
		    		ret_code = SYSSerialClose( (SYS_SERIAL_PARAMS*) port->Params );
		    	break;
		    	case SYS_KERNEL_PORT_TYPE_SOCKET:
		    	case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
		    		ret_code = SYSCloseSocket( (SYS_SOCKET*) port->Params );
		    	break;
		    	case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
		    		ret_code = SYSCloseSocket( (SYS_SOCKET*) port->Params );
		    	break;
		    	case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
		    		ret_code = SYSCloseSocket( (SYS_SOCKET*) port->Params );
		    	break;
		    	case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
		    		ret_code = SYSCloseSocket( (SYS_SOCKET*) port->Params );
		    	break;
		    	case SYS_KERNEL_PORT_TYPE_XML_PORT:
		    		ret_code = XMLClosePort( (XML_PORT*) port->Params );
		    	break;
		    	default:
		    		ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
		    }
		    if ( ret_code == 0 )
			{
		    	port->Active = false;
			}
		}
		else
		{
        	ret_code = SYS_KERNEL_PORT_NOT_ACTIVE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_PORT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per scrivere su una porta di comunicazione
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int SYSWritePort( SYS_PORT * port, char * buffer, int len, int * bytes_sent )
{
	int	ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno

	if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
	{
		// --- Scrittura sulla porta ---
		switch (port->Type)
		{
			case SYS_KERNEL_PORT_TYPE_SERIAL:
			case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
				ret_code = SYSSerialWrite((SYS_SERIAL_PARAMS*) port->Params,  buffer, len, (unsigned long *) bytes_sent);
			break;
			case SYS_KERNEL_PORT_TYPE_SOCKET:
			case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
				ret_code = SYSWriteSocket( (SYS_SOCKET*) port->Params,  buffer, len, bytes_sent, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
				ret_code = SYSWriteSocket( (SYS_SOCKET*) port->Params,  buffer, len, bytes_sent, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
				ret_code = SYSWriteSocket( (SYS_SOCKET*) port->Params,  buffer, len, bytes_sent, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				ret_code = SYSWriteSocket( (SYS_SOCKET*) port->Params,  buffer, len, bytes_sent, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_XML_PORT:
				ret_code = XMLWritePort( (XML_PORT*) port->Params, buffer, len, bytes_sent );
			break;
			default:
				ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_PORT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per leggere da una porta di comunicazione
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int SYSReadPort( SYS_PORT * port, char * buffer, int len, int * bytes_rcvd )
{
	int                 ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno

	if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
	{
		// --- Leggo dalla porta di comunicazione ---
		switch ( port->Type )
		{
			case SYS_KERNEL_PORT_TYPE_SERIAL:
			case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
				ret_code = SYSSerialRead( (SYS_SERIAL_PARAMS*)port->Params, buffer, len, (unsigned long *)bytes_rcvd );
			break;
			case SYS_KERNEL_PORT_TYPE_SOCKET:
			case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
				ret_code = SYSReadSocket( (SYS_SOCKET*)port->Params, buffer, len, bytes_rcvd, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
				ret_code = SYSReadSocket( (SYS_SOCKET*)port->Params, buffer, len, bytes_rcvd, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
				ret_code = SYSReadSocket( (SYS_SOCKET*)port->Params, buffer, len, bytes_rcvd, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				ret_code = SYSReadSocket( (SYS_SOCKET*)port->Params, buffer, len, bytes_rcvd, 0 );
			break;
			case SYS_KERNEL_PORT_TYPE_XML_PORT:
				ret_code = XMLReadPort( (XML_PORT*)port->Params, buffer, len, bytes_rcvd );
				Sleep( 100 );
			break;
			default:
				ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_PORT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ricavare i parametri di una porta da una definizione XML
///
/// In caso di errore restituisce un valore non nullo
///
/// \date [12.01.2010]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int SYSExtractPortParams( SYS_PORT * port )
{
	int                 ret_code  		= SYS_KERNEL_NO_ERROR; // Codice di ritorno
	char *				parity    		= NULL; // Parametro parit�
	XML_ELEMENT *		element_p 		= NULL; // Ptr all'elemento XML di definizione porta
	SYS_SERIAL_PARAMS * serial_p  		= NULL; // Ptr a struttura parametri a porta seriale
	SYS_SOCKET *		socket_p  		= NULL; // Ptr a struttura parametri a porta socket
	XML_PORT *			xml_port		= NULL; // Ptr a struttura parametri a porta XML
	XML_SOURCE *		xml_source		= NULL;
	char *				xml_filename	= NULL;

	if ( port != NULL ) // Controllo il ptr alla porta di comunicazione
	{
		element_p = port->Def;
		if ( element_p != NULL ) // Controllo il ptr alla definizione XML della porta
		{
			port->ID			= XMLGetValueInt( element_p, "id" ); // Setto l'ID della porta
			port->Name			= XMLExtractValue( element_p, "name" ); // Setto il nome della porta
			port->Persistent	= XMLGetValueBool( element_p, "persistent" ); // Flag porta persistente
			switch ( port->Type ) // Switcho sui tipi di porta
			{
				/* Caso porta SERIALE o RS232 */
				case SYS_KERNEL_PORT_TYPE_SERIAL:
				case SYS_KERNEL_PORT_TYPE_RS232:
					serial_p              = (SYS_SERIAL_PARAMS*) port->Params;
					if ( serial_p != NULL )
					{
						serial_p->Com         = XMLGetValueInt  ( element_p, "com"      );
						parity                = XMLGetValue     ( element_p, "parity"   );
						serial_p->ChParity    = parity[0];
						serial_p->Baud        = XMLGetValueUInt32  ( element_p, "baud"     );
						serial_p->DataBits    = XMLGetValueInt  ( element_p, "data"     );
						serial_p->StopBits    = XMLGetValueInt  ( element_p, "stop"     );
						serial_p->Echo        = XMLGetValueBool ( element_p, "echo"     );
						serial_p->Timeout     = XMLGetValueInt  ( element_p, "timeout"  );
					}
					else
					{
						ret_code = SYS_KERNEL_INVALID_PORT_PARAMS;
					}
				break;
				// --- Caso porta SOCKET o SNMP Manager (via SSP) ---
				case SYS_KERNEL_PORT_TYPE_SOCKET:
				case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
				case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
				case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
				case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
					socket_p              = (SYS_SOCKET*) port->Params;
					if ( socket_p != NULL )
					{
						socket_p->Params.IPAddr = XMLExtractValue ( element_p, "ip"   );
						socket_p->Params.Port   = XMLGetValueInt  ( element_p, "port" );
					}
					else
					{
						ret_code = SYS_KERNEL_INVALID_PORT_PARAMS;
					}
				break;
				// --- Caso porta STLC1000 in modalit� RS485 o RS422 ---
				case SYS_KERNEL_PORT_TYPE_STLC1000_RS485:
				case SYS_KERNEL_PORT_TYPE_STLC1000_RS422:
					serial_p              = (SYS_SERIAL_PARAMS*) port->Params;
					if ( serial_p != NULL )
					{
						serial_p->Com         = XMLGetValueInt  ( element_p, "com"      );
						parity                = XMLGetValue     ( element_p, "parity"   );
						serial_p->ChParity    = parity[0];
						serial_p->Baud        = XMLGetValueInt  ( element_p, "baud"     );
						serial_p->DataBits    = XMLGetValueInt  ( element_p, "data"     );
						serial_p->StopBits    = XMLGetValueInt  ( element_p, "stop"     );
						serial_p->Echo        = XMLGetValueBool ( element_p, "echo"     );
						serial_p->Timeout     = XMLGetValueInt  ( element_p, "timeout"  );
					}
					else
					{
						ret_code = SYS_KERNEL_INVALID_PORT_PARAMS;
					}
				break;
				case SYS_KERNEL_PORT_TYPE_XML_PORT:
					xml_port	= (XML_PORT*) port->Params;
					if ( XMLValidPort( xml_port ) )
					{
						xml_source = (XML_SOURCE*)xml_port->Source;
						if ( xml_source != NULL ) {
							xml_filename = XMLGetValue( element_p, "src" );
							if ( xml_filename != NULL ) {
								xml_source->name = XMLStrCpy( xml_source->name, xml_filename );
							}
						}
					}
				break;
				default:
					ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_DEFINITION;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_PORT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per impostare i parametri di una porta di comunicazione
///
/// \date [25.01.2007]
/// \author Enrico Alborali
/// \version 0.09
//------------------------------------------------------------------------------
int SYSPortSetParams( SYS_PORT * port )
{
	int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

	if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
	{
		switch ( port->Type )
		{
			case SYS_KERNEL_PORT_TYPE_SERIAL:
				ret_code = SYSSerialSetParams( (SYS_SERIAL_PARAMS*)port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_SOCKET:
			case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
			case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
			case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
			case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
			case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
			case SYS_KERNEL_PORT_TYPE_XML_PORT:
				ret_code = SYS_KERNEL_NO_ERROR;
				//ret_code = SYSSocketSetParams( (SYS_SOCKET_PARAMS*)port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_MODEM:
				ret_code = SYS_KERNEL_NOT_IMPLEMENTED;
			break;
			case SYS_KERNEL_PORT_TYPE_RS232:
				ret_code = SYSSerialSetParams( (SYS_SERIAL_PARAMS*)port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS485:
				ret_code = SYSSerialSetParams( (SYS_SERIAL_PARAMS*)port->Params );
				/*
				if ( ret_code == SYS_KERNEL_NO_ERROR )
				{
					ret_code = STLC1KSetCOMParams( ((SYS_SERIAL_PARAMS*)port->Params)->Com  ,
										 ((SYS_SERIAL_PARAMS*)port->Params)->Baud ,
										 SPV_STLC1000_COM_MODE_RS485              ,
										 ((SYS_SERIAL_PARAMS*)port->Params)->Com  );
				}
				*/
			break;
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS422:
				ret_code = SYSSerialSetParams( (SYS_SERIAL_PARAMS*)port->Params );
				/*
				if ( ret_code == SYS_KERNEL_NO_ERROR )
				{
					ret_code = STLC1KSetCOMParams( ((SYS_SERIAL_PARAMS*)port->Params)->Com  ,
										 ((SYS_SERIAL_PARAMS*)port->Params)->Baud ,
										 SPV_STLC1000_COM_MODE_RS422              ,
										 ((SYS_SERIAL_PARAMS*)port->Params)->Com  );
				}
				*/
			break;
			default:
				ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_PORT;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere i parametri di una porta di comunicazione
///
/// \date [19.06.2006]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SYSPortGetParams( SYS_PORT * port )
{
  int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

  if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
  {
		switch ( port->Type )
    {
      case SYS_KERNEL_PORT_TYPE_SERIAL:
      case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
      case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
      case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
        ret_code = SYSSerialGetParams( (SYS_SERIAL_PARAMS*)port->Params );
      break;
	  case SYS_KERNEL_PORT_TYPE_SOCKET:
      case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
      case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
      case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
        ret_code = SYSSocketGetParams( (SYS_SOCKET_PARAMS*)port->Params );
	  break;
	  default:
        ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
    }
  }
  else
  {
	ret_code = SYS_KERNEL_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere lo stato di una porta di comunicazione
///
/// \date [22.02.2006]
/// \author Enrico Alborali
/// \version 0.06
//------------------------------------------------------------------------------
int SYSPortGetStatus( SYS_PORT * port )
{
	int                 ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno
  SYS_SERIAL_PARAMS * p_serial = NULL ; // Parametri della porta seriale

  if ( port != NULL ) // Controllo la validit� della struttura porta di comunicazione
  {
	switch ( port->Type )
    {
      case SYS_KERNEL_PORT_TYPE_SERIAL:
      case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
      case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
      case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
        p_serial = (SYS_SERIAL_PARAMS*) port->Params;
        ret_code = SYSSerialGetStatus( p_serial );
        if ( ret_code == SYS_SERIAL_NOT_IMPLEMENTED ) ret_code = 0; // Perch� SYSSerialGetStatus non e' implementata
		if ( ret_code == 0 ) // Se � andata a buon fine
        {
          port->InBufferLen = p_serial->Status.ComStatus.cbInQue;
          port->Active    = true;
        }
        else // Se la lettura dello stato � fallita
		{
          port->InBufferLen = 0;
          port->Active    = false;
        }
      break;
      case SYS_KERNEL_PORT_TYPE_SOCKET:
      case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
      case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
        ret_code = SYSSocketGetStatus( (SYS_SOCKET_PARAMS*)port->Params );
	  break;
	  default:
        ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
    }
  }
  else
	{
    ret_code = SYS_KERNEL_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una struttura porta di comunicazione
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [11.02.2008]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.15
//------------------------------------------------------------------------------
int SYSDeletePort( SYS_PORT * port )
{
  int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

  if ( port != NULL ) // Controllo la validit� del ptr a porta di com.
  {
    // --- Elimino la stringa del nome ---
    if ( port->Name != NULL ) free( port->Name );
    // --- Termino il thread di gestione buffer in ingresso ---
    if ( port->InBufferThread != NULL )
    {
			SYSTerminateThread( port->InBufferThread );
		}
    // --- Termino il thread di gestione buffer in uscita ---
	if ( port->OutBufferThread != NULL )
    {
      SYSTerminateThread( port->OutBufferThread );
		}
		SYSWaitForThread( port->InBufferThread );
		SYSWaitForThread( port->OutBufferThread );
		// --- Elimino i descrittori delle sezioni critiche della porta ---
	//DeleteCriticalSection( &port->CriticalSection );
	SYSClearLock(&port->GeneralLock);
	//DeleteCriticalSection( &port->InCriticalSection );
	SYSClearLock(&port->InLock);
	//DeleteCriticalSection( &port->OutCriticalSection );
    SYSClearLock(&port->OutLock);
    // --- Elimino le FIFO per la gestione dei buffer di I/O ---
    SYSDeleteFIFO( port->InBuffer );
    SYSDeleteFIFO( port->OutBuffer );
    // --- Elimino le strutture dei parametri ---
    switch ( port->Type )
    {
      case SYS_KERNEL_PORT_TYPE_SERIAL:
      case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
      case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
			case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
        /* TODO 1 -oEnrico -cSYS : Aggiungere in SYSSerial una funzione per cancellare strutture di parametri */
        // --- Cancello la struttura status ---
        if ( port->Params != NULL )
		{
          free( port->Params );
        }
        // --- Cancello la struttura status ---
				if ( port->Status != NULL )
        {
          free( port->Status );
        }
      break;
      case SYS_KERNEL_PORT_TYPE_SOCKET:
      case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
      case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
	  case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
      case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
			case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
				ret_code = SYSDeleteSocket( (SYS_SOCKET*)port->Params );
			break;
			case SYS_KERNEL_PORT_TYPE_XML_PORT:
				ret_code = XMLDeletePort( (XML_PORT*)port->Params );
			break;
      default:
        ret_code = SYS_KERNEL_INVALID_PORT_TYPE;
    }
		// --- Elimino il thread di gestione buffer in ingresso ---
    if ( port->InBufferThread != NULL )
    {
      SYSDeleteThread( port->InBufferThread );
    }
    // --- Elimino il thread di gestione buffer in uscita ---
    if ( port->OutBufferThread != NULL )
    {
      SYSDeleteThread( port->OutBufferThread );
	}
    free( port );
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aggiornare il monitor della porta
///
/// In caso di errore restituisce valore non nullo.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSUpdatePortMonitor( SYS_PORT * port )
{
  int             ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno
  //TfPortMonitor * monitor   = NULL;

  if ( port != NULL )
  {
    /*
		monitor = (TfPortMonitor*)port->Monitor;
    if ( monitor != NULL )
    {
      monitor->UpdateInfo( port );
    }
    else
    {
      ret_code = SYS_KERNEL_INVALID_PORT_MONITOR;
	}
	*/
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per calcolare il numero di byte nella FIFO di ingresso
///
///
/// \date [22.12.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
long SYSGetInFIFOByteCount( SYS_PORT * port )
{
  long          count = -1  ; // Numero di byte nella FIFO di ingresso
  SYS_FIFO    * fifo  = NULL; // Ptr alla FIFO in ingresso della porta
  SYS_BUFFER  * item  = NULL; // Ptr ad un elemento della FIFO

  if ( port != NULL ) // Controllo la validit� del ptr a porta di com
  {
    fifo = port->InBuffer;
    if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
		{
      count = 0; // Azzero il contatore dei byte
      for ( int i = 0; i < fifo->Count; i++ ) // Ciclo sugli elementi della FIFO
      {
        item = (SYS_BUFFER*) SYSGetFIFOItem( fifo, i );
        if ( item != NULL ) // Controllo la validit� del ptr all'elemento i-esimo
        {
		  try
		  {
            count += item->BufferLen; // Aggiorno il conteggio dei byte
          }
          catch(...)
          {
            /* TODO -oEnrico -cError : Gestire l'errore critico */
          }
        }
      }
    }
  }

  return count;
}

//==============================================================================
/// Funzione per impostare parametri HW speciali di una porta
///
/// \date [10.07.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSSetPortHardware( SYS_PORT * port )
{
  int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

  if ( port != NULL )
  {
    try
		{
      if ( ret_code == SYS_KERNEL_NO_ERROR )
      {
        if ( port->Type == SYS_KERNEL_PORT_TYPE_STLC1000_RS485 )
        {
		  ret_code = STLC1KSetCOMParams( ((SYS_SERIAL_PARAMS*)port->Params)->Com  ,
										 ((SYS_SERIAL_PARAMS*)port->Params)->Baud ,
                                         SPV_STLC1000_COM_MODE_RS485              ,
                                         ((SYS_SERIAL_PARAMS*)port->Params)->Com  );
        }
        if ( port->Type == SYS_KERNEL_PORT_TYPE_STLC1000_RS422 )
        {
          ret_code = STLC1KSetCOMParams( ((SYS_SERIAL_PARAMS*)port->Params)->Com  ,
                                         ((SYS_SERIAL_PARAMS*)port->Params)->Baud ,
                                         SPV_STLC1000_COM_MODE_RS422              ,
                                         ((SYS_SERIAL_PARAMS*)port->Params)->Com  );
        }
      }
	}
	catch(...)
    {
      ret_code = SYS_KERNEL_CRITICAL_ERROR;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_PORT;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere la valocita' di una porta di comunicazione
/// In caso di errore restituisce 0.
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SYSGetPortBaud( SYS_PORT * port )
{
  unsigned __int32 baud = 0;
  SYS_SERIAL_PARAMS * params = NULL;

  if ( port != NULL )
  {
    try
    {
      switch ( port->Type )
      {
        case SYS_KERNEL_PORT_TYPE_SERIAL:
        case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
		case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
        case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
          params = (SYS_SERIAL_PARAMS *)port->Params;
          baud = params->Baud;
        break;
        case SYS_KERNEL_PORT_TYPE_SOCKET:
        case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
        case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
        case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
        case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
				case SYS_KERNEL_PORT_TYPE_XML_PORT:
          baud = 128000;
        break;
      }
    }
    catch(...)
    {
      baud = 9600;
    }
  }

  return baud;
}

//==============================================================================
/// Funzione per ottenere il timeout di una porta di comunicazione
///
/// \date [16.01.2007]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSGetPortTimeout( SYS_PORT * port )
{
  int timeout = 0;
  SYS_SERIAL_PARAMS * params = NULL;

  if ( port != NULL )
  {
    try
    {
      switch ( port->Type )
      {
        case SYS_KERNEL_PORT_TYPE_SERIAL:
        case SYS_KERNEL_PORT_TYPE_RS232: // Caso RS232
        case SYS_KERNEL_PORT_TYPE_STLC1000_RS485: // Caso STLC1000 RS485
        case SYS_KERNEL_PORT_TYPE_STLC1000_RS422: // Caso STLC1000 RS422
          params = (SYS_SERIAL_PARAMS *)port->Params;
          timeout = params->Timeout;
        break;
        case SYS_KERNEL_PORT_TYPE_SOCKET:
        case SYS_KERNEL_PORT_TYPE_TCP_CLIENT:
        case SYS_KERNEL_PORT_TYPE_UDP_CLIENT:
        case SYS_KERNEL_PORT_TYPE_TCP_SERVER:
        case SYS_KERNEL_PORT_TYPE_UDP_SERVER:
				case SYS_KERNEL_PORT_TYPE_SNMP_MANAGER:
				case SYS_KERNEL_PORT_TYPE_XML_PORT:
					timeout = 500;
				break;
	  }
    }
	catch(...)
    {
      timeout = 500;
    }
  }

  return timeout;
}

//==============================================================================
/// Funzione per creare una nuova FIFO
/// Alloca una nuova struttura per contenenre i parametri di una FIFO e imposta
/// la stessa in modo che sia di dimensione <dim>.
/// In caso di errore restituisce NULL.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SYS_FIFO * SYSNewFIFO( int dim )
{
  SYS_FIFO * fifo = NULL; // Ptr ad una struttura FIFO

  fifo = new SYS_FIFO;

  if ( fifo != NULL ) // Se ho allocato la struttura per la FIFO
  {
    fifo->TimeOut = SYS_KERNEL_FIFO_DEFAULT_TIMEOUT ;
    fifo->Dim     = ( dim < SYS_KERNEL_FIFO_MAX_ITEMS )?dim:SYS_KERNEL_FIFO_MAX_ITEMS;
    SYSResetFIFO( fifo ); // Resetto la nuova struttura della FIFO
  }

  return fifo;
}

//==============================================================================
/// Funzione per eliminare una FIFO
/// Disalloca la struttura dei parametri della FIFO. Non vengono cancellati i
/// suoi elementi.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSDeleteFIFO( SYS_FIFO * fifo )
{
  int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    delete fifo;
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_FIFO;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per scrivere un nuovo elemento nella FIFO
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSWriteFIFOItem( SYS_FIFO * fifo, void * item )
{
  int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    if ( item != NULL) // Controllo la validit� del ptr a Elemento FIFO
    {
      if ( fifo->Count == fifo->Dim ) // Controllo che la FIFO non sia piena
	  {
        ret_code = SYS_KERNEL_FIFO_FULL;
      }
      else // La FIFO non � piena
      {
        fifo->Item[fifo->InPos] = item;
        fifo->TotCounter++;
        fifo->Count++;
        fifo->InPos = ( fifo->InPos < (fifo->Dim-1) )?( fifo->InPos + 1 ):0;
      }
    }
    else
    {
      ret_code = SYS_KERNEL_INVALID_ITEM;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_FIFO;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per leggere il primo elemento dalla FIFO
/// In caso di errore restituisce NULL.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void * SYSReadFIFOItem( SYS_FIFO * fifo )
{
  void * item = NULL; // Ptr a elemento da leggere

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    if ( fifo->Count > 0 ) // Se la FIFO non � vuota
    {
      item = fifo->Item[fifo->OutPos];
      fifo->Item[fifo->OutPos] = NULL;
      fifo->Count--;
      fifo->OutPos  = ( fifo->OutPos < ( fifo->Dim - 1 ) )?( fifo->OutPos + 1 ):0;
    }
  }

  return item;
}

//==============================================================================
/// Funzione per resettare il contenuto di una FIFO
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SYSResetFIFO( SYS_FIFO * fifo )
{
  int ret_code = SYS_KERNEL_NO_ERROR; // Codice di ritorno

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    fifo->Count   = 0;
	fifo->OutPos  = 0;
	fifo->InPos   = 0;
		for ( int i = 0; i < fifo->Dim; i++ ) // Resetto i singoli elementi
    {
      fifo->Item[i] = NULL;
    }
	fifo->TotCounter = 0;
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_FIFO;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per svuotare una FIFO per buffer
///
/// \date [24.05.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSClearBufferFIFO( SYS_FIFO * fifo )
{
	int     ret_code  = SYS_KERNEL_NO_ERROR; // Codice di ritorno
	void  * item      = NULL;

	if ( fifo != NULL )
	{
		try
		{
			for ( int i = 0; i < fifo->Dim; i++ ) // Resetto i singoli elementi
			{
				try
				{
					item = SYSExtractFIFOItem( fifo, 0 );
					SYSDeleteBuffer( (SYS_BUFFER*)item );
				}
				catch(...)
				{
				}
			}
		}
		catch(...)
		{
			ret_code = SYS_KERNEL_CRITICAL_ERROR;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_FIFO;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ottenere la posizione di un elemento
/// In caso di errore restituisce -1
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSGetFIFOItemPos( SYS_FIFO * fifo, int ID )
{
  int pos           = -1; // Posizione effettiva nell'array dell'elemento della FIFO
  int out_to_bottom = 0 ; // Numero di elementi dalla posizione di uscita alla fine della FIFO

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    if ( fifo->Count > 0 && ID < fifo->Count ) // Se la FIFO non � vuota e l'ID non supera la dim
	{
      out_to_bottom = fifo->Dim - fifo->OutPos;

			// Ricavo quale elemento devo prendere dall'array
      if ( ID < out_to_bottom )
      {
        pos = fifo->OutPos + ID;
      }
      else
      {
        pos = fifo->OutPos - out_to_bottom;
      }
    }
  }

  return pos;
}

//==============================================================================
/// Funzione per ottenere un elemento di una FIFO
/// Restituisce il ptr all'elemento originale della FIFO (non una copia).
/// Nota: usare questa funzione con attenzione, solo per lettura.
/// In caso di errore restituisce NULL.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
void * SYSGetFIFOItem( SYS_FIFO * fifo, int ID )
{
  void  * item          = NULL  ; // Ptr a elemento da leggere
  int     new_id        = 0     ;

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    if ( fifo->Count > 0 && ID < fifo->Count ) // Se la FIFO non � vuota e l'ID non supera la dim
	{
      new_id = SYSGetFIFOItemPos( fifo, ID );
	  item = fifo->Item[new_id];
    }
	}

  return item;
}

//==============================================================================
/// Funzione per 'editare' un elemento di una FIFO
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSEditFIFOItem( SYS_FIFO * fifo, int ID, void * item )
{
  int     ret_code      = 0; // Codice di ritorno
  int     new_id        = 0;

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    if ( item != NULL ) // Controllo la validit� del ptr a Elemento FIFO
    {
	  if ( fifo->Count > 0 && ID < fifo->Count ) // Se la FIFO non � vuota e l'ID non supera la dim
	  {
		new_id = SYSGetFIFOItemPos( fifo, ID );
        fifo->Item[new_id] = item;
      }
      else
      {
        ret_code = SYS_KERNEL_INVALID_ID;
      }
    }
    else
	{
	  ret_code = SYS_KERNEL_INVALID_ITEM;
	}
  }
  else
	{
    ret_code = SYS_KERNEL_INVALID_FIFO;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per estrarre un elemento da una FIFO
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void * SYSExtractFIFOItem( SYS_FIFO * fifo, int ID )
{
  void  * item            = NULL; // Ptr a elemento da estrarre
  int     pos             = -1  ; // Posizione nell'array della FIFO
  int     this_to_end     = 0   ; // Numero di elementi fino all'ultimo

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
  {
    if ( fifo->Count > 0 ) // Controllo che la FIFO non sia vuota
    {
      if ( ID >=0 && ID < fifo->Count ) // Controllo che l'ID sia valido
      {
        if ( ID == 0 ) // Se � il primo elemento della FIFO
        {
          item = SYSReadFIFOItem( fifo );
        }
        else // Se non � il primo elemento della FIFO
		{
          item = SYSGetFIFOItem( fifo, ID );
          fifo->Item[ID] = NULL;
          fifo->Count--;
          fifo->InPos = ( fifo->InPos > 0)?( fifo->InPos - 1 ):( fifo->Dim - 1 );
          if ( ID < ( fifo->Count - 1 ) ) // Se � un elemento intermedio della FIFO
					{
            this_to_end = fifo->Count - ID - 1; // Elementi successivi a quello estratto
			for ( int i = 1; i <= this_to_end; i++ ) // Sposto gli elementi successivi
            {
              pos = SYSGetFIFOItemPos( fifo, ID + i );
              if ( pos == 0 ) // Se � nella prima posizione dell'array
              {
                fifo->Item[fifo->Dim-1] = fifo->Item[pos];
              }
              else
              {
                fifo->Item[pos-1] = fifo->Item[pos];
              }
            }
          }
        }
      }
	}
  }

  return item;
}

//==============================================================================
/// Funzione per inserire un elemento in una FIFO
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSInsertFIFOItem( SYS_FIFO * fifo, int ID, void * item )
{
  int     ret_code        = SYS_KERNEL_NO_ERROR; // Codice di ritorno
  int     pos             = -1  ; // Posizione nell'array della FIFO
  int     this_to_end     = 0   ; // Numero di elementi fino all'ultimo

  if ( fifo != NULL ) // Controllo la validit� del ptr a FIFO
	{
    if ( fifo->Count < fifo->Dim ) // Controllo che la FIFO non sia piena
    {
      if ( ID >=0 && ID <= fifo->Count ) // Controllo che l'ID sia valido
      {
        if ( ID == fifo->Count ) // Se � subito dopo l'ultimo elemento della FIFO
        {
          ret_code = SYSWriteFIFOItem( fifo, item );
        }
        else // Se non � subito dopo l'ultimo elemento della FIFO
        {
          this_to_end = fifo->Count - ID; // Elementi successivi a quello da inserire
          for ( int i = this_to_end; i >= 1; i-- ) // Sposto gli elementi successivi
          {
			pos = SYSGetFIFOItemPos( fifo, ID + i );
			if ( pos == 0 ) // Se � nella prima posizione dell'array
			{
              fifo->Item[pos] = fifo->Item[fifo->Dim-1];
            }
            else
            {
              fifo->Item[pos] = fifo->Item[pos-1];
            }
          }
          ret_code = SYSEditFIFOItem( fifo, ID, item );
          if ( ret_code == 0 )
          {
			fifo->TotCounter++;
            fifo->Count++;
			fifo->InPos = ( fifo->InPos > 0 )?( fifo->InPos - 1 ):( fifo->Dim - 1 );
          }
        }
      }
      else
      {
        ret_code = SYS_KERNEL_INVALID_ID;
      }
		}
    else
    {
      ret_code = SYS_KERNEL_FIFO_FULL;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_FIFO;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova chiave di registro
/// <keybase> specifica l'handler di una delle chiavi aperte da cui partire. Se
/// il parametro � nullo viene automaticamente utilizzato HKEY_LOCAL_MACHINE.
/// <kaypath> specifica il percorso e il nome della nuova chiave di registro che
/// si vuole creare (es: "SYSTEM\\CurrentControlSet\\Control\\ProductOptions").
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSCreateRegKey( void * keybase, char * keypath )
{
  int   ret_code = SYS_KERNEL_NO_ERROR;
  HKEY  hk      ; // Handler della chiave di registro
  DWORD dwDisp  ;
  TCHAR szBuf[MAX_PATH];

  if ( keypath != NULL )
  {
    // --- Preparo la stringa del percorso e nome chiave di registro ---
		if ( keybase == NULL )
    {
      keybase = (void*)HKEY_LOCAL_MACHINE;
    }
    wsprintf( szBuf, keypath );
    // --- Provo a scrivere la chiave ---
    if ( RegCreateKeyEx( (HKEY)keybase, szBuf,
         0, NULL, REG_OPTION_NON_VOLATILE,
		 KEY_WRITE, NULL, &hk, &dwDisp ) == ERROR_SUCCESS )
    {
      ret_code = SYSCloseRegKey( (void*)hk );
	}
	else
    {
      ret_code = SYS_KERNEL_REGKEY_WRITE_FAILURE;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_REGKEY_PATH;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare una chiave di registro
/// <keybase> specifica l'handler di una delle chiavi aperte da cui partire. Se
/// il parametro � nullo viene automaticamente utilizzato HKEY_LOCAL_MACHINE.
/// <kaypath> specifica il percorso e il nome della chiave di registro che si
/// vuole eliminare (es: "SYSTEM\\CurrentControlSet\\Control\\ProductOptions").
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSDeleteRegKey( void * keybase, char * keypath )
{
  int ret_code = SYS_KERNEL_NO_ERROR;

  if ( keypath != NULL )
  {
    if ( keybase == NULL )
    {
	  keybase = (void*)HKEY_LOCAL_MACHINE;
	}
    if ( RegDeleteKey( (HKEY)keybase, keypath ) != ERROR_SUCCESS )
    {
      ret_code = SYS_KERNEL_REGKEY_DELETE_FAILURE;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_REGKEY_PATH;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere l'handler di una chiave di registro
/// <keybase> specifica l'handler di una delle chiavi aperte da cui partire. Se
/// il parametro � nullo viene automaticamente utilizzato HKEY_LOCAL_MACHINE.
/// <kaypath> specifica il percorso e il nome della chiave di registro che si
/// vuole ottenere (es: "SYSTEM\\CurrentControlSet\\Control\\ProductOptions").
/// <write_enable> indica se aprire l'handle in scrittura o in lettura
///
/// In caso di errore restituisce NULL.
///
/// \date [27.09.2007]
/// \author Enrico Alborali, Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void * SYSOpenRegKey( void * keybase, char * keypath, bool write_enable )
{
	void  * keyhandler = NULL ;
	HKEY    hk                ; // Handler della chiave di registro
	TCHAR   szBuf[MAX_PATH]   ;
	unsigned long samDesired  ;

	if ( keypath != NULL )
	{
		// --- Preparo la stringa del percorso e nome chiave di registro ---
		if ( keybase == NULL )
		{
			keybase = (void*)HKEY_LOCAL_MACHINE;
		}
		wsprintf( szBuf, keypath );
		// --- Provo a scrivere la chiave ---
		if (write_enable)
		{
			samDesired = KEY_WRITE;
		}
		else
		{
			samDesired = KEY_QUERY_VALUE;
		}
		if ( RegOpenKeyEx ( (HKEY)keybase, keypath, 0, samDesired, &hk ) == ERROR_SUCCESS )
		{
			keyhandler = (void*)hk;
		}
	}
  return keyhandler;
}

//==============================================================================
/// Funzione per chiudere un handler di una chiave di registro
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSCloseRegKey( void * keyhandler )
{
  int ret_code = SYS_KERNEL_NO_ERROR;

  if ( keyhandler != NULL )
  {
    if ( RegCloseKey( (HKEY)keyhandler ) != ERROR_SUCCESS )
    {
      ret_code = SYS_KERNEL_REGKEY_CLOSE_FAILURE;
	}
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_REGKEY_HANDLER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per impostare un valore in una chiave di registro
/// <type> � il tipo di valore della chiave di registro e pu� essere:
/// REG_BINARY: qualsiaisi tipo di dato binario
/// REG_DWORD: un numero a 32 bit
/// REG_DWORD_LITTLE_ENDIAN: un numero a 32 bit formato little-endian
/// REG_DWORD_BIG_ENDIAN: un numero a 32 bit formato big-endian
/// REG_EXPAND_SZ: una stringa NULL-terminata che contiene riferimenti non
///                espansi a variabili di ambiente (es: "%PATH%")
/// REG_LINK: un link simbolico unicode.
/// REG_MULTI_SZ: un array di stringhe NULL-terminate terminato da due NULL
/// REG_NONE: tipo non definito
/// REG_RESOURCE_LIST: una lista di risorse per un driver
/// REG_SZ: una stringa NULL-terminata
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [21.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSSetRegValue( void * keyhandler, char * name, void * value, int type, int size )
{
  int   ret_code = SYS_KERNEL_NO_ERROR;
  long  fun_code = 0;

  if ( keyhandler != NULL )
	{                                        
    if ( name != NULL )
    {
      if ( value != NULL && size > 0 )
      {
        fun_code = RegSetValueEx( (HKEY)keyhandler, (LPCTSTR)name, (DWORD)0,
                                  (DWORD)type, (CONST BYTE*)value, (DWORD)size );
        if ( fun_code != ERROR_SUCCESS )
        {
          ret_code = SYS_KERNEL_REGKEY_SETVALUE_FAILURE;
		}
      }
      else
      {
        ret_code = SYS_KERNEL_INVALID_REGKEY_VALUE;
      }
    }
    else
    {
      ret_code = SYS_KERNEL_INVALID_REGKEY_VALUENAME;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_REGKEY_HANDLER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere un valore di una chiave di registro
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [19.07.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
void * SYSGetRegValue( void * keyhandler, char * name )
{
  void  * value         = NULL;
  DWORD   buffer_size   = 1024;
  char  * buffer[1024]        ;
  char  * value_buffer  = NULL;

  if ( keyhandler != NULL )
  {
    if ( name != NULL )
    {
      if ( RegQueryValueEx( (HKEY)keyhandler, (LPCTSTR)name, NULL, NULL, (LPBYTE)buffer, &buffer_size ) == ERROR_SUCCESS )
      {
        value_buffer = (char*)malloc( sizeof(char) * buffer_size ); // -MALLOC
        if ( value_buffer != NULL )
        {
          memcpy( value_buffer, buffer, sizeof(char) * buffer_size );
          value = (void*)value_buffer;
        }
      }
	}
  }

  return value;
}

//==============================================================================
/// Funzione per creare un  nuovo file di log
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSEventLogCreateFile( char * logname )
{
  int     ret_code        = SYS_KERNEL_NO_ERROR ;
  TCHAR   path[MAX_PATH]                        ;

  if ( logname != NULL )
  {
    wsprintf( path, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s", logname );
	ret_code = SYSCreateRegKey( HKEY_LOCAL_MACHINE, path );
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_LOGNAME;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per eliminare un file di log
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSEventLogDeleteFile( char * logname )
{
  int     ret_code        = SYS_KERNEL_NO_ERROR ;
  TCHAR   path[MAX_PATH]                        ;

  if ( logname != NULL )
  {
	wsprintf( path, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s", logname );
    ret_code = SYSDeleteRegKey( HKEY_LOCAL_MACHINE, path );
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_LOGNAME;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare una nuova sorgente per il file di log
///
/// \date [21.09.2005]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSEventLogCreateSource( char * logname, char * sourcename )
{
	int     ret_code        = SYS_KERNEL_NO_ERROR ;
	TCHAR   path[MAX_PATH]                        ;
	void  * keyhandler      = NULL                ;
	DWORD   dwData                                ;

	if ( logname != NULL )
	{
		if ( sourcename != NULL )
		{
			wsprintf( path, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s\\%s\\", logname, sourcename );
			ret_code = SYSCreateRegKey( HKEY_LOCAL_MACHINE, path );
			if ( ret_code == SYS_KERNEL_NO_ERROR )
			{
				keyhandler = SYSOpenRegKey( HKEY_LOCAL_MACHINE, path, true );
				if ( keyhandler != NULL )
				{
					dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;
					ret_code = SYSSetRegValue( keyhandler, "TypeSupported", (void*)&dwData, REG_DWORD, sizeof(DWORD) );
					SYSCloseRegKey( keyhandler );
				}
				else
				{
					ret_code = SYS_KERNEL_REGKEY_DOESNT_EXISTS;
				}
			}
			else
			{
				ret_code = SYS_KERNEL_REGKEY_CREATE_FAILURE;
			}
		}
		else
		{
			ret_code = SYS_KERNEL_INVALID_SOURCENAME;
		}
	}
	else
	{
		ret_code = SYS_KERNEL_INVALID_LOGNAME;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per eliminare una sorgente per il file di log
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSEventLogDeleteSource( char * logname, char * sourcename )
{
  int     ret_code        = SYS_KERNEL_NO_ERROR ;
  TCHAR   path[MAX_PATH]                        ;

  if ( logname != NULL )
  {
    if ( sourcename != NULL )
    {
      wsprintf( path, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s\\%s", logname, sourcename );
      ret_code = SYSDeleteRegKey( HKEY_LOCAL_MACHINE, path );
	}
    else
    {
      ret_code = SYS_KERNEL_INVALID_SOURCENAME;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_LOGNAME;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aprire una sorgente per il file di log
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
void * SYSEventLogRegisterSource( char * sourcename )
{
  void * sourcehandler = NULL;

  if ( sourcename != NULL )
  {
    sourcehandler = (void*)RegisterEventSource( NULL, (LPCTSTR)sourcename );
  }

  return sourcehandler;
}

//==============================================================================
/// Funzione per chiudere una sorgente per il file di log
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSEventLogDeregisterSource( void * source )
{
  int   ret_code = SYS_KERNEL_NO_ERROR;
  bool  fun_code = true;

  if ( source != NULL )
  {
    fun_code = DeregisterEventSource( (HANDLE)source );
	if ( fun_code == false )
    {
      ret_code = SYS_KERNEL_SOURCE_DEREG_FAILURE;
    }
  }
  else
  {
    ret_code = SYS_KERNEL_INVALID_SOURCE_HANDLER;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per scrivere una riga nel file di log
///
/// \date [28.07.2005]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSEventLogReport( void * source, int type, int cat, int ID, int str_num, void * str, int data_size, void * data )
{
  int   ret_code = SYS_KERNEL_NO_ERROR;
  bool  fun_code = true;

  if ( source != NULL )
  {
	fun_code = ReportEvent( (HANDLE)source    , (WORD)type    , (WORD)cat     ,
							(DWORD)ID         , NULL          , (WORD)str_num ,
							(DWORD)data_size  , (LPCTSTR*)str , (LPVOID)data  );
	if ( fun_code == false )
	{
	  ret_code = SYS_KERNEL_EVENT_REPORT_FAILURE;
	}
  }
  else
  {
	ret_code = SYS_KERNEL_INVALID_SOURCE_HANDLER;
  }

  return ret_code;
}





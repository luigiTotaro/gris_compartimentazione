//==============================================================================
// Telefin SSP Protocol Module 1.0
//------------------------------------------------------------------------------
// MODULO PROTOCOLLO SSP (SYSSSP.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.00 (10.02.2006 -> 10.02.2006)
//
// Copyright: 2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SYSSSP.h
//------------------------------------------------------------------------------
// Version history: vedere in SYSSSP.h
//==============================================================================

#pragma hdrstop
#include "SYSSSP.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

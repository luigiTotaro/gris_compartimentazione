//==============================================================================
// Telefin SSP Protocol Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO PROTOCOLLO SSP (SYSSSP.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:  0.00 (10.02.2006 -> 10.02.2006)
//
// Copyright: 2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:		Enrico Alborali (alborali@telefin.it)
// Note:      richiede SYSSSP.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [10.02.2006]:
// - Prima versione del modulo.
//==============================================================================

#ifndef SYSSSPH
#define SYSSSPH
//---------------------------------------------------------------------------
#endif

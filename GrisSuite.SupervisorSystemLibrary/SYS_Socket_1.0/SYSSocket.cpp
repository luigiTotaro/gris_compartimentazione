//==============================================================================
// Telefin Supervisor Socket Module 1.0
//------------------------------------------------------------------------------
// MODULO GESTIONE SOCKET PER S.O. MS WINDOWS 2000/XP (SYSSocket.cpp)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	1.0.0.9 (28.05.2004 -> 13.11.2006)
//
// Copyright: 2004-2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland C++ Builder 2006
// Autori:	  Enrico Alborali (alborali@telefin.it)
//            Mario Ferro (mferro@deltasistemi-vr.it)
//------------------------------------------------------------------------------
// Version history: vedere in SYSSocket.h
//==============================================================================
#pragma hdrstop
#pragma package(smart_init)
#include "SYSSocket.h"
#include "winsock2.h"
#include <string.h>
//------------------------------------------------------------------------------

static WSADATA SysSocketData; // Struttura dati utilizzata da WinSocket

//==============================================================================
/// Funzione per inizializzare il modulo.
/// Esegue il caricamento e l'inizializzazione della DLL del Socket di Windows
/// per il processo corrente.
/// La funzione necessita di un WinSocket versione 2.2 altrimenti lo chiude.
/// In caso di errore restituisce una valore non nullo.
///
/// \date [29.06.2004]
/// \author Enrico Alborali.
/// \version 0.01
//------------------------------------------------------------------------------
int SYSSocketInit( void )
{
  int ret_code = SYS_SOCKET_NO_ERROR; // Codice di ritorno

  ret_code = WSAStartup( MAKEWORD( 2, 2 ), &SysSocketData ); // Carico la DLL del Socket di Windows - Versione 2.2

  if ( ret_code == NO_ERROR ) // Se non ci sono stati errori
  {
    if ( LOBYTE( SysSocketData.wVersion ) != 2 || // Controllo se la versione del Socket richiesta � supportata
         HIBYTE( SysSocketData.wVersion ) != 2 )
    {
      WSACleanup();
      ret_code = SYS_SOCKET_VERSION_NOT_SUPPORTED;
    }
    else
    {
      ret_code = SYS_SOCKET_NO_ERROR;
    }
  }
  else // Startup del socket fallito
  {
    ret_code = SYS_SOCKET_STARTUP_FAILURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per creare un nuovo socket
///
/// In caso di errore restituisce NULL.
///
/// \date [20.09.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
SYS_SOCKET * SYSNewSocket( int type )
{
  SYS_SOCKET      * sock          = NULL            ; // Ptr alla nuova struttura socket
  SYS_SOCKET_PARAMS sock_params   = { NULL  ,5001 , false } ; // Struttura dei parametri del socket
  SYS_SOCKET_STATUS sock_status   = { false ,0    } ; // Struttura dello stato del socket
  int               sock_type     = type            ; // Tipo di socket

  sock = (SYS_SOCKET*)malloc( sizeof(SYS_SOCKET) );

  if ( sock != NULL ) // Se l'allocazione � andata a buon fine
  {
    switch ( type ) // Switcho sul tipo di socket
    {
      /* Socket Client TCP*/
      case SYS_SOCKET_TYPE_TCP_CLIENT:
        sock_params.Server = false;
      break;
      /* Socket Server TCP */
      case SYS_SOCKET_TYPE_TCP_SERVER:
        sock_params.Server = true;
	  break;
      /* Socket Client UDP */
      case SYS_SOCKET_TYPE_UDP_CLIENT:
        sock_params.Server = false;
      break;
      /* Socket Server UDP */
      case SYS_SOCKET_TYPE_UDP_SERVER:
        sock_params.Server = true;
      break;
      /* Prendo il socket di default: Client TCP */
      default:
        sock_type = SYS_SOCKET_TYPE_TCP_CLIENT;
    }
    sock->Type    = sock_type;    // Assegno il tipo di socket
    sock->Params  = sock_params;  // Assegno i parametri
    sock->Status  = sock_status;   // Azzero lo stato
    /* Inizializzo la lista delle connessione del server (se applicabile) */
    if ( sock_params.Server )
    {
      sock->ConnCount = 0;
      memset( &sock->ConnList, 0, sizeof(SYS_SOCKET_CONNECTION) * SYS_SOCKET_MAX_CONNECTIONS );
      for ( int c = 0; c < SYS_SOCKET_MAX_CONNECTIONS; c++ )
      {
        sock->ConnList[c].Addr = malloc( sizeof(sockaddr_in) );
      }
    }
  }

  return sock;
}

//==============================================================================
/// Funzione per cancellare un socket
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSDeleteSocket( SYS_SOCKET * sock )
{
  int ret_code = SYS_SOCKET_NO_ERROR; // Codice di ritorno

  if ( sock != NULL )
  {
    if ( sock->Status.Opened == false ) // Se il socket � gi� stato chiuso
    {
      if ( sock->Params.IPAddr != NULL ) free ( sock->Params.IPAddr );
      /* Elimino la lista delle connessioni in ingresso se � un server*/
      if ( sock->Params.Server )
      {
        for ( int c=0; c<SYS_SOCKET_MAX_CONNECTIONS; c++ )
        {
          if ( sock->ConnList[c].Addr != NULL ) free( sock->ConnList[c].Addr );
        }
      }
      free ( sock );
    }
    else
    {
      ret_code = SYS_SOCKET_NOT_CLOSED;
    }
  }
  else
  {
    ret_code = SYS_SOCKET_INVALID_SOCKET;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per aprire un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSOpenSocket( SYS_SOCKET * sock )
{
  int               ret_code      = 0       ; // Codice di ritorno
  SOCKET            sock_handler  = 0       ; // Hanlder del socket
  SYS_SOCKET_PARAMS sock_params             ; // Struttura dei parametri del socket
  SYS_SOCKET_STATUS sock_status             ; // Struttura dello stato del socket
  int               sock_family   = AF_INET ; // Famiglia degli indirizzi
  int               sock_type     = 0       ; // Tipo di socket
  int               sock_protocol = 0       ; // Protocollo del socket
  sockaddr_in       sock_addr               ; // Indirizzo in ingresso del socket

  if ( sock != NULL ) // Controllo il ptr alla struttura socket
  {
    switch ( sock->Type ) // Switcho sul tipo di socket
    {
      /* Socket Client TCP*/
      case SYS_SOCKET_TYPE_TCP_CLIENT:
        sock_type = SOCK_STREAM;
        /* Impostazioni indirizzo socket */
        memset( &sock_addr, 0, sizeof(sock_addr) );
        sock_addr.sin_family      = AF_INET;
        sock_addr.sin_addr.s_addr = (sock->Params.IPAddr == NULL)?INADDR_ANY:inet_addr(sock->Params.IPAddr);
        sock_addr.sin_port        = htons(sock->Params.Port);
      break;
      /* Socket Server TCP */
      case SYS_SOCKET_TYPE_TCP_SERVER:
        sock_type = SOCK_STREAM;
		/* Impostazioni indirizzo socket */
        memset( &sock_addr, 0, sizeof(sock_addr) );
        sock_addr.sin_family      = AF_INET;
        sock_addr.sin_addr.s_addr = (sock->Params.IPAddr == NULL)?INADDR_ANY:inet_addr(sock->Params.IPAddr);
        sock_addr.sin_port        = htons(sock->Params.Port);
      break;
      /* Socket Client UDP */
      case SYS_SOCKET_TYPE_UDP_CLIENT:
        sock_type = SOCK_DGRAM;
        /* Impostazioni indirizzo socket */
        memset( &sock_addr, 0, sizeof(sock_addr) );
        sock_addr.sin_family      = AF_INET;
        sock_addr.sin_addr.s_addr = (sock->Params.IPAddr == NULL)?INADDR_ANY:inet_addr(sock->Params.IPAddr);
        sock_addr.sin_port        = htons(sock->Params.Port);
      break;
      /* Socket Server UDP */
      case SYS_SOCKET_TYPE_UDP_SERVER:
        sock_type = SOCK_DGRAM;
        /* Impostazioni indirizzo socket */
        memset( &sock_addr, 0, sizeof(sock_addr) );
        sock_addr.sin_family      = AF_INET;
        sock_addr.sin_addr.s_addr = (sock->Params.IPAddr == NULL)?INADDR_ANY:inet_addr(sock->Params.IPAddr);
        sock_addr.sin_port        = htons(sock->Params.Port);
      break;
      /* Tipo di socket non sconosciuto */
      default:
        ret_code = SYS_SOCKET_UNKNOWN_TYPE;
    }
    if ( ret_code == 0 )
    {
      /* Apertura del socket */
      sock_handler = socket( sock_family, sock_type, sock_protocol ); // Creo il socket di windows
      if ( sock_handler != INVALID_SOCKET )
      {
        sock->Handler = sock_handler;
        /* Caso Server TCP/UDP */
		if ( sock->Params.Server == true )
        {
          /* Bind del socket */
          ret_code = bind( sock_handler, (struct sockaddr*)&sock_addr, sizeof(sock_addr) );
          if ( ret_code == 0 )
          {
            /* Solo per Server TCP */
            if ( sock->Type == SYS_SOCKET_TYPE_TCP_SERVER )
            {
              /* Metto il socket in attesa di connessioni */
              ret_code = listen( sock_handler, SYS_SOCKET_MAX_CONNECTIONS );
              if ( ret_code == 0 )
              {
                sock->Status.Opened = true;
              }
              else
              {
                sock->Status.LastError = WSAGetLastError( );
                ret_code = SYS_SOCKET_LISTEN_FAILURE;
              }
            }
            else
            {
              sock->Status.Opened = true;
            }
          }
          else
          {
            sock->Status.LastError = WSAGetLastError( );
            ret_code = SYS_SOCKET_BIND_FAILURE;
          }
        }
        /* Caso Client TCP/UDP */
        else
        {
          ret_code = connect( sock_handler, (struct sockaddr*)&sock_addr,sizeof(sock_addr) );
		  if ( ret_code == 0 )
          {
            sock->Status.Opened = true;
          }
          else
          {
            sock->Status.LastError = WSAGetLastError( );
            ret_code = SYS_SOCKET_CONNECT_FAILURE;
          }
        }
      }
      else
      {
        sock->Status.LastError = WSAGetLastError( );
        ret_code = SYS_SOCKET_OPEN_FAILURE;
      }
    }
  }
  else
  {
    ret_code = SYS_SOCKET_INVALID_SOCKET;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSCloseSocket( SYS_SOCKET * sock )
{
  int       ret_code      = SYS_SOCKET_NO_ERROR; // Codice di ritorno
  SOCKET    sock_handler  = 0 ; // Handler del socket

  if ( sock != NULL ) // Controllo il ptr alla struttura del socket
  {
    if ( sock->Status.Opened == true ) // Controllo che il socket sia stato aperto
    {
      sock_handler = (SOCKET)sock->Handler;
      ret_code = closesocket(sock_handler); // Chiudo il socket
      if ( ret_code != 0 ) // C'� stato un errore nella chiusura
      {
        sock->Status.LastError = WSAGetLastError( );
        ret_code = SYS_SOCKET_CLOSE_FAILURE;
      }
      else
      {
        sock->Status.Opened = false;
      }
    }
    else
    {
      ret_code = SYS_SOCKET_NOT_OPENED;
    }
  }
  else // Il ptr alla struttura dei parametri non � valido
  {
    ret_code = SYS_SOCKET_INVALID_PARAMS;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per accettare connesioni in ingresso sul socket server.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [17.09.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSAcceptSocketConn( SYS_SOCKET * sock, int * ID )
{
  int                     ret_code      = SYS_SOCKET_NO_ERROR ; // Codice di ritorno
  SOCKET                  sock_handler  = 0                   ; // Descrittore del socket
  SOCKET                  conn_handler  = 0                   ; // Descrittore del socket di connessione
  sockaddr_in             conn_addr     = { 0, 0, 0, NULL }   ; // Indirizzo del socket di connessione in ingresso
  int                     conn_ID       = 0                   ; // Identificatore del socket di connessione
  SYS_SOCKET_CONNECTION * conn          = NULL                ; // Struttura connessione a socket

  if ( sock != NULL ) // Controllo il ptr alla struttura socket
  {
    if ( sock->Params.Server == true )
    {
      sock_handler = (SOCKET) sock->Handler;
      /* Caso server TCP */
      if ( sock->Type == SYS_SOCKET_TYPE_TCP_SERVER )
      {
        conn_handler = accept( sock_handler, (struct sockaddr*)&conn_addr, NULL );
      }
      /* Caso server UDP */
      else
      {
        conn_handler = (SOCKET) sock->Handler;
      }
      if ( conn_handler != INVALID_SOCKET )
      {
        randomize( );
        conn_ID = random( 1000000 );
        for ( int i = 0; i < SYS_SOCKET_MAX_CONNECTIONS; i++ )
        {
		  if ( sock->ConnList[i].ID == 0 )
          {
            conn = &sock->ConnList[i];
            i = SYS_SOCKET_MAX_CONNECTIONS;
          }
        }
        if ( conn != NULL ) // Se ho trovato una cella libera nell'array
        {
          conn->ID       = conn_ID;
          conn->Handler  = conn_handler;
          memcpy(conn->Addr,&conn_addr,sizeof(sockaddr_in));
          sock->ConnCount++;
        }
        else
        {
          ret_code = SYS_SOCKET_ARRAY_FULL;
        }
      }
      else
      {
        sock->Status.LastError = WSAGetLastError( );
        ret_code = SYS_SOCKET_ACCEPT_FAILURE;
      }
    }
    else
    {
      ret_code = SYS_SOCKET_NOT_SERVER;
    }
  }
  else
  {
    ret_code = SYS_SOCKET_INVALID_SOCKET;
  }

  *ID = conn_ID;
  return ret_code;
}

//==============================================================================
/// Funzione per rilasciare connesioni in ingresso sul socket server.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSReleaseSocketConn( SYS_SOCKET * sock, int ID )
{
  int                     ret_code      = SYS_SOCKET_NO_ERROR ; // Codice di ritorno
  SYS_SOCKET_CONNECTION * conn          = NULL                ; // Struttura connessione a socket
  SOCKET                  conn_handler  = 0                   ; // Descrittore del socket di connessione

  if ( sock != NULL ) // Controllo il puntatore
  {
    if ( sock->Params.Server == true ) // Controllo che il socket sia server
    {
      conn = SYSGetSocketConn( sock, ID );
      if ( conn != NULL ) // Se ho trovato la connessione nell'array
      {
        conn_handler = (SOCKET) conn->Handler;
        ret_code = closesocket( conn_handler );
        if ( ret_code == 0 )
        {
          conn->ID = 0; // Libero l'elemento
        }
        else
        {
          sock->Status.LastError = WSAGetLastError( );
          ret_code = SYS_SOCKET_ACCEPT_FAILURE;
        }
      }
	  else
      {
        ret_code = SYS_SOCKET_CONNECTION_NOT_FOUND;
      }
    }
    else
    {
      ret_code = SYS_SOCKET_NOT_SERVER;
    }
  }
  else
  {
    ret_code = SYS_SOCKET_INVALID_SOCKET;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere una connessione in ingresso su socket
///
/// In caso di errore restituisce NULL.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SYS_SOCKET_CONNECTION * SYSGetSocketConn( SYS_SOCKET * sock, int ID )
{
  SYS_SOCKET_CONNECTION * conn = NULL; // Ptr alla struttura connessione a socket

  if ( sock != NULL && ID > 0 ) // Controllo il ptr della struttura socket
  {
    if ( sock->Params.Server && sock->ConnCount > 0 )
    {
      for ( int c = 0; c < SYS_SOCKET_MAX_CONNECTIONS; c++ )
	  {
        if ( sock->ConnList[c].ID == ID )
        {
          conn = &sock->ConnList[c];
          c = SYS_SOCKET_MAX_CONNECTIONS;
        }
      }
    }
  }

  return conn;
}

//==============================================================================
/// Funzione per ottenere una connessione in ingresso su socket
///
/// In caso di errore restituisce NULL.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
SYS_SOCKET_CONNECTION * SYSGetSocketConn( SYS_SOCKET * sock, void * addr )
{
  SYS_SOCKET_CONNECTION * conn      = NULL; // Ptr alla struttura connessione a socket
  sockaddr_in           * pthe_addr = NULL; // Ptr all'indirizzo della connessione
  sockaddr_in           * pany_addr = NULL; // Ptr all'indirizzo della connessione
  sockaddr_in             the_addr        ; // Struttura indirizzo della connessione
  sockaddr_in             any_addr        ; // Struttura indirizzo della connessione

  if ( sock != NULL && addr != NULL ) // Controllo il ptr della struttura socket e il ptr a indirizzo
  {
    if ( sock->Params.Server && sock->ConnCount > 0 )
    {
      pthe_addr = (sockaddr_in*) addr;
      the_addr = *pthe_addr;
	  for ( int c = 0; c < SYS_SOCKET_MAX_CONNECTIONS; c++ )
      {
        pany_addr = (sockaddr_in*) sock->ConnList[c].Addr;
        any_addr = *pany_addr;
        if ( any_addr.sin_port == the_addr.sin_port
          && any_addr.sin_addr.s_addr == the_addr.sin_addr.s_addr )
        {
          conn = &sock->ConnList[c];
          c = SYS_SOCKET_MAX_CONNECTIONS;
        }
      }
    }
  }

  return conn;
}

//==============================================================================
/// Funzione per scrivere su un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [17.09.2004]
/// \author Enrico Alborali
/// \version 0.04
//------------------------------------------------------------------------------
int SYSWriteSocket( SYS_SOCKET * sock, char* buffer, int len, int * bytes_sent, int connID )
{
  int                     ret_code      = SYS_SOCKET_NO_ERROR ; // Codice di ritorno
  int                     ret_val       = 0   ; // Valore di ritorno
  SOCKET                  sock_handler  = 0   ; // Descrittore del socket
  SOCKET                  conn_handler  = 0   ; // Descrittore del socket di connessione
  SYS_SOCKET_CONNECTION * conn          = NULL; // Ptr alla connessione al socket
  sockaddr_in           * addr          = NULL; // Ptr alla struttura indirizzo

  if ( sock != NULL ) // Controllo la validit� del ptr a struttura socket
  {
    if ( sock->Status.Opened ) // Controllo che il socket sia aperto
    {
      if ( buffer != NULL ) // Controllo il ptr al buffer
      {
        if ( len > 0 ) // Controllo la lunghezza del buffer
        {
          /* Caso Server TCP/UDP */
          if ( sock->Params.Server )
          {
            conn = SYSGetSocketConn( sock, connID ); // Recupero la struttura di connessione
            if ( conn != NULL ) // Se il ptr alla struttura connessione � valido
            {
              conn_handler = (SOCKET) conn->Handler;
              /* Caso Server TCP */
              if ( sock->Type == SYS_SOCKET_TYPE_TCP_SERVER )
              {
                ret_val = send( conn_handler, buffer, len, 0 );
              }
              /* Caso Server UDP */
              else
              {
                addr = (sockaddr_in*) conn->Addr;
                ret_val = sendto( conn_handler, buffer, len, 0, (struct sockaddr*)addr, sizeof(addr) );
              }
            }
            else
            {
              ret_code = SYS_SOCKET_CONNECTION_NOT_FOUND;
            }
          }
          /* Caso Client TCP/UDP */
          else
          {
            sock_handler = (SOCKET)sock->Handler; // Prendo il descrittore del socket
            ret_val = send( sock_handler, buffer, len, 0 );
		  }
          if ( ret_val == SOCKET_ERROR && ret_code == 0 ) // Errore in fase di invio
          {
            sock->Status.LastError = WSAGetLastError( );
            ret_code = SYS_SOCKET_SEND_FAILURE;
          }
        }
        else
        {
          ret_code = SYS_SOCKET_INVALID_BUFFER_LEN;
        }
      }
      else
      {
        ret_code = SYS_SOCKET_INVALID_BUFFER;
      }
    }
    else
    {
      ret_code = SYS_SOCKET_NOT_OPENED;
    }
  }
  else // Il ptr alla struttura dei parametri non � valido
  {
    ret_code = SYS_SOCKET_INVALID_PARAMS;
  }

  *bytes_sent = ret_val;
  return ret_code;
}

//==============================================================================
/// Funzione per leggere da un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [09.05.2006]
/// \author Enrico Alborali
/// \version 0.07
//------------------------------------------------------------------------------
int SYSReadSocket( SYS_SOCKET * sock, char* buffer, int len, int * bytes_rcvd, int connID )
{
  int                     ret_code      = SYS_SOCKET_NO_ERROR ; // Codice di ritorno
  int                     ret_val       = 0       ; // Valore di ritorno
  SOCKET                  sock_handler  = 0       ; // Descrittore del socket
  SOCKET                  conn_handler  = 0       ; // Descrittore del socket di connessione
  SYS_SOCKET_CONNECTION * conn          = NULL    ; // Ptr alla connessione al socket
  sockaddr_in           * addr          = NULL    ; // Ptr alla struttura indirizzo
  sockaddr_in             addr_in                 ; //
  fd_set                  read_fd                 ; //
  timeval                 timeout       = { 0, 0 }; //

  if ( sock != NULL ) // Controllo la validit� della struttura dei parametri
  {
    if ( sock->Status.Opened ) // Controllo che il socket sia aperto
    {
      if ( buffer != NULL ) // Controllo il ptr al buffer
      {
        if ( len > 0 ) // Controllo la lunghezza del buffer
        {
          /* Caso Server TCP/UDP */
          if ( sock->Params.Server )
          {
            conn = SYSGetSocketConn( sock, connID ); // Recupero la struttura di connessione
            if ( conn != NULL ) // Se il ptr alla struttura connessione � valido
            {
              conn_handler = (SOCKET) conn->Handler;
              /* Preparo il set di descrittori */
              FD_ZERO(&read_fd);
              FD_SET( conn_handler,&read_fd );
              /* Prima di leggere controllo che ci sia qualcosa da leggere altrimenti recv blocca l'esecuzione */
              if ( select(0, &read_fd, NULL, NULL, &timeout) > 0 )
			  {
                /* Caso Server TCP */
                if ( sock->Type == SYS_SOCKET_TYPE_TCP_SERVER )
                {
                  ret_val = recv( conn_handler, buffer, len, 0 );
                }
                /* Caso Server UDP */
                else
                {
                  addr = (sockaddr_in*)conn->Addr;
                  ret_val = recvfrom( conn_handler, buffer, len, 0, (struct sockaddr*)addr, NULL );
                }
              }
              else
              {
                ret_code = SYS_SOCKET_BUFFER_EMPTY;
              }
            }
            else
            {
              ret_code = SYS_SOCKET_CONNECTION_NOT_FOUND;
            }
          }
          /* Caso Client TCP/UDP */
          else
          {
            sock_handler = (SOCKET)sock->Handler;
            /* Preparo il set di descrittori */
            FD_ZERO(&read_fd);
            FD_SET( sock_handler,&read_fd );
            /* Prima di leggere controllo che ci sia qualcosa da leggere altrimenti recv blocca l'esecuzione */
            if ( select(0, &read_fd, NULL, NULL, &timeout) > 0 )
            {
              ret_val = recv( sock_handler, buffer, len, 0 );
            }
            else
			{
              ret_code = SYS_SOCKET_BUFFER_EMPTY;
            }
          }
          if ( ret_val == SOCKET_ERROR && ret_code == 0 )
          {
            sock->Status.LastError = WSAGetLastError( );
            ret_code = SYS_SOCKET_RECV_FAILURE;
          }
          else
          {
            if ( ret_val == 0 && ret_code != SYS_SOCKET_BUFFER_EMPTY )
            {
              ret_code = SYS_SOCKET_CLIENT_DISCONNECTED;
            }
          }
        }
        else
        {
          ret_code = SYS_SOCKET_INVALID_BUFFER_LEN;
        }
      }
      else
      {
        ret_code = SYS_SOCKET_INVALID_BUFFER;
      }
    }
    else
    {
      ret_code = SYS_SOCKET_NOT_OPENED;
    }
  }
  else // Il ptr alla struttura dei parametri non � valido
  {
    ret_code = SYS_SOCKET_INVALID_PARAMS;
  }

  *bytes_rcvd = ret_val;
  return ret_code;
}

//==============================================================================
/// Funzione per impostare i parametri di un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [04.04.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int SYSSetSocketParams( SYS_SOCKET * sock, char * IP, int port )
{
  int ret_code = 0 ; // Codice di ritorno

  if ( sock != NULL )
  {
    if ( IP != NULL )
    {
      // --- Se c'era gi� un indirizzo IP lo cancello ---
      if ( sock->Params.IPAddr != NULL ) free( sock->Params.IPAddr );
      // --- Alloco la memoria per la nuova stringa che conterr� l'IP --
      sock->Params.IPAddr = (char*)malloc(sizeof(char)*(strlen(IP)+1));
      if ( sock->Params.IPAddr != NULL )
      {
        strcpy(sock->Params.IPAddr,IP);
        sock->Params.Port = port;
      }
      else
	  {
		ret_code = SYS_SOCKET_INVALID_ALLOCATION;
	  }
	}
	else
	{
	  ret_code = SYS_SOCKET_INVALID_IP_ADDRESS;
	}
  }
  else
  {
	ret_code = SYS_SOCKET_INVALID_SOCKET;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per impostare i parametri di un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSSocketSetParams( SYS_SOCKET_PARAMS * params )
{
  int ret_code = SYS_SOCKET_NO_ERROR; // Codice di ritorno

  if ( params != NULL ) // Controllo la validit� della struttura dei parametri
  {
	/* TODO -oNon specificato -cSocket : Implementare un eventuale settaggio dei parametri di un socket */
	ret_code = SYS_SOCKET_NOT_IMPLEMENTED;
  }
  else // Il ptr alla struttura dei parametri non � valido
  {
	ret_code = SYS_SOCKET_INVALID_PARAMS;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere i parametri di un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSSocketGetParams( SYS_SOCKET_PARAMS * params )
{
  int ret_code = SYS_SOCKET_NO_ERROR; // Codice di ritorno

  if ( params != NULL ) // Controllo la validit� della struttura dei parametri
  {
	/* TODO -oNon specificato -cSocket : Implementare un eventuale recupero dei parametri di un socket */
	ret_code = SYS_SOCKET_NOT_IMPLEMENTED;
  }
  else // Il ptr alla struttura dei parametri non � valido
  {
	ret_code = SYS_SOCKET_INVALID_PARAMS;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per ottenere lo stato di un socket.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [12.04.2005]
/// \author Enrico Alborali
/// \version 0.03
//------------------------------------------------------------------------------
int SYSSocketGetStatus( SYS_SOCKET_PARAMS * params )
{
  int ret_code = SYS_SOCKET_NO_ERROR; // Codice di ritorno

  if ( params != NULL ) // Controllo la validit� della struttura dei parametri
  {
	/* TODO -oNon specificato -cSocket : Implementare un eventuale recupero dello stato di un socket */
	ret_code = SYS_SOCKET_NOT_IMPLEMENTED;
  }
  else // Il ptr alla struttura dei parametri non � valido
  {
	ret_code = SYS_SOCKET_INVALID_PARAMS;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo.
///
/// In caso di errore restituisce una valore non nullo.
///
/// \date [29.06.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int SYSSocketClear( void )
{
  int ret_code = SYS_SOCKET_NO_ERROR; // Codice di ritorno

  ret_code = WSACleanup(); // Chiudo la DLL del Socket di Windows

  if ( ret_code != 0 )
  {
	ret_code = SYS_SOCKET_CLEANUP_FAILURE;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per recuperare il Nome Host
///
/// In caso di errore restituisce NULL.
///
/// \date [13.11.2006]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char* SYSGetHostName ( void )
{
	char *HostName = NULL;
	char Buffer [256];
	int len = 0;

	try
	{
		try
		{
			gethostname(Buffer,256);
			len = strlen(Buffer);
			HostName = (char *)malloc(len+1);
			strcpy(HostName,Buffer);
		}
		catch(...)
		{
			HostName = NULL;
		}
	}
	__finally
	{
		return HostName;
	}
}

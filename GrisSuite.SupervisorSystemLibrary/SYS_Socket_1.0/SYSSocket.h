//==============================================================================
// Telefin Supervisor Socket Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO GESTIONE SOCKET PER S.O. MS WINDOWS 2000/XP (SYSSocket.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	1.0.0.9 (28.05.2004 -> 13.11.2006)
//
// Copyright: 2004-2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6, Borland C++ Builder 2006
// Autori:	  Enrico Alborali (alborali@telefin.it)
//            Mario Ferro (mferro@deltasistemi-vr.it)
// Note:      richiede SYSSocket.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [28.05.2004]:
// - Prima versione del modulo.
// 0.01 [29.06.2004]:
// - Aggiunta la funzione SYSSocketInit.
// - Aggiunti alcuni codici di errore.
// - Modificata la definizione del tipo SYS_SOCKET_STATUS.
// - Modificata la definizione del tipo SYS_SOCKET_PARAMS.
// - Modificata la funzione SYSSocketCloseAll.
// - Modificata la funzione SYSSocketOpen.
// - Modificata la funzione SYSSocketSetEcho.
// - Modificata la funzione SYSSocketClose.
// - Modificata la funzione SYSSocketWrite.
// - Modificata la funzione SYSSocketRead.
// - Modificata la funzione SYSSocketSetParams.
// - Modificata la funzione SYSSocketGetParams.
// - Modificata la funzione SYSSocketGetStatus.
// 0.02 [13.09.2004]:
// - Aggiunti i codici dei tipi di socket.
// 0.03 [16.09.2004]:
// - Modificata la definizione del tipo SYS_SOCKET_PARAMS.
// - Modificata la definizione del tipo SYS_SOCKET_STATUS.
// - Aggiunta la definizione del tipo SYS_SOCKET.
// - Aggiunta la funzione SYSNewSocket.
// - Aggiunta la funzione SYSDeleteSocket.
// - Rinominata la funzione SYSSocketOpen in SYSOpenSocket.
// - Modificata la funzione SYSOpenSocket.
// - Rinominata la funzione SYSSocketClose in SYSCloseSocket.
// - Modificata la funzione SYSCloseSocket.
// - Aggiunto il codice di errore SYS_SOCKET_INVALID_SOCKET.
// - Aggiunto il codice di errore SYS_SOCKET_NOT_CLOSED.
// - Aggiunto il codice di errore SYS_SOCKET_INVALID_SOCKET.
// - Aggiunto il codice di errore SYS_SOCKET_LISTEN_FAILURE.
// - Aggiunto il codice di errore SYS_SOCKET_BIND_FAILURE.
// - Aggiunto il codice di errore SYS_SOCKET_OPEN_FAILURE.
// - Aggiunto il codice di errore SYS_SOCKET_NOT_SERVER.
// - Aggiunta la funzione SYSAcceptSocketConn.
// - Aggiunto il codice di errore SYS_SOCKET_ARRAY_FULL.
// - Aggiunto il codice di errore SYS_SOCKET_CONNECTION_NOT_FOUND.
// - Modificata la funzione SYSSocketCloseAll in SYSSocketClear.
// - Aggiunto il codice di errore SYS_SOCKET_UNKNOWN_TYPE.
// - Aggiunto il codice di errore SYS_SOCKET_ACCEPT_FAILURE.
// - Aggiunta la funzione SYSReleaseSocketConn.
// - Rinominata la funzione SYSSocketWrite in SYSWriteSocket.
// - Rinominata la funzione SYSSocketRead in SYSReadSocket.
// - Modificata la funzione SYSWriteSocket.
// - Modificata la funzione SYSReadSocket.
// - Aggiunto il codice di errore SYS_SOCKET_CLIENT_DISCONNECTED.
// - Aggiunta la funzione SYSGetSocketConn.
// 0.04 [17.09.2004]:
// - Aggiunto il codice di errore SYS_SOCKET_BUFFER_EMPTY.
// - Modificata la funzione SYSWriteSocket.
// - Modificata la funzione SYSReadSocket.
// - Modificata la funzione SYSAcceptSocketConn.
// 0.05 [20.09.2004]:
// - Corretta la funzione SYSReadSocket.
// - Modificata la funzione SYSNewSocket.
// 0.06 [12.04.2005]:
// - Ottimizzazione del codice.
// - Modificata la funzione SYSSocketSetParams.
// - Modificata la funzione SYSSocketGetParams.
// - Modificata la funzione SYSSocketGetStatus.
// 0.07 [04.04.2006]:
// - Aggiunta la funzione SYSSetSocketParams.
// 0.08 [09.05.2006]:
// - Modificata la funzione SYSReadSocket.
// 0.09 [13.11.2006]:
// - Aggiunta la funzione SYSGetHostName.

//==============================================================================
#ifndef SYSSocketH
#define SYSSocketH
//==============================================================================
// Definizione valori e tipo
//------------------------------------------------------------------------------
#define SYS_SOCKET_TYPE_TCP_CLIENT        5303
#define SYS_SOCKET_TYPE_TCP_SERVER        5302
#define SYS_SOCKET_TYPE_UDP_CLIENT        7303
#define SYS_SOCKET_TYPE_UDP_SERVER        7302
#define SYS_SOCKET_MAX_CONNECTIONS        128
//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SYS_SOCKET_NO_ERROR               0
#define SYS_SOCKET_STARTUP_FAILURE        7000
#define SYS_SOCKET_VERSION_NOT_SUPPORTED  7001
#define SYS_SOCKET_CREATE_FAILURE         7002
#define SYS_SOCKET_CONNECT_FAILURE        7003
#define SYS_SOCKET_INVALID_PARAMS         7004
#define SYS_SOCKET_CLEANUP_FAILURE        7005
#define SYS_SOCKET_CLOSE_FAILURE          7006
#define SYS_SOCKET_INVALID_BUFFER         7007
#define SYS_SOCKET_INVALID_BUFFER_LEN     7008
#define SYS_SOCKET_NOT_OPENED             7009
#define SYS_SOCKET_SEND_FAILURE           7010
#define SYS_SOCKET_RECV_FAILURE           7011
#define SYS_SOCKET_INVALID_SOCKET         7012
#define SYS_SOCKET_NOT_CLOSED             7013
#define SYS_SOCKET_LISTEN_FAILURE         7014
#define SYS_SOCKET_BIND_FAILURE           7015
#define SYS_SOCKET_OPEN_FAILURE           7016
#define SYS_SOCKET_NOT_SERVER             7017
#define SYS_SOCKET_ARRAY_FULL             7018
#define SYS_SOCKET_CONNECTION_NOT_FOUND   7019
#define SYS_SOCKET_UNKNOWN_TYPE           7020
#define SYS_SOCKET_ACCEPT_FAILURE         7021
#define SYS_SOCKET_CLIENT_DISCONNECTED    7022
#define SYS_SOCKET_BUFFER_EMPTY           7023
#define SYS_SOCKET_INVALID_ALLOCATION     7024
#define SYS_SOCKET_INVALID_IP_ADDRESS     7025
#define SYS_SOCKET_NOT_IMPLEMENTED        7099
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura dati per per lo stato del socket.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SYS_SOCKET_STATUS
{
  bool                Opened      ; // Flag di socket aperto
  int                 LastError   ; // Codice dell'ultimo errore rilevato
}
SYS_SOCKET_STATUS;

//==============================================================================
/// Struttura dati per per i parametri del socket.
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
typedef struct _SYS_SOCKET_PARAMS
{
  char              * IPAddr      ; // Indirizzo IP del socket
  int                 Port        ; // Porta del socket
  bool                Server      ; // Flag di socket server TCP/UDP
}
SYS_SOCKET_PARAMS;

//==============================================================================
/// Struttura dati per una connessione a server
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SYS_SOCKET_CONNECTION
{
  int                 ID          ; // Codice identificativo del client
  void              * Addr        ; // Ptr all'indirizzo del client connesso
  unsigned int        Handler     ; // Ptr al descrittore del socket di connessione
}
SYS_SOCKET_CONNECTION;

//==============================================================================
/// Struttura dati per un socket
///
/// \date [16.09.2004]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SYS_SOCKET
{
  int                   Type        ; // Tipo di socket
  unsigned int          Handler     ; // Ptr all'handler di sistema del socket
  SYS_SOCKET_PARAMS     Params      ; // Parametri del socket
  SYS_SOCKET_STATUS     Status      ; // Stato del socket
  SYS_SOCKET_CONNECTION ConnList[SYS_SOCKET_MAX_CONNECTIONS]; // Lista di connessioni in ingresso (solo server)
  int                   ConnCount   ; // Numero di connessioni in ingresso in lista (solo server)
}
SYS_SOCKET;

//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

// Funzione per inizializzare il modulo (MSWin)
int                     SYSSocketInit       ( void );
// Funzione per creare un nuovo socket
SYS_SOCKET            * SYSNewSocket        ( int type );
// Funzione per cancellare un socket
int                     SYSDeleteSocket     ( SYS_SOCKET * sock );
// Funzione per aprire un socket (MSWin)
int                     SYSOpenSocket       ( SYS_SOCKET * sock );
// Funzione per chiudere un socket (MSWin)
int                     SYSCloseSocket      ( SYS_SOCKET * sock );
// Funzione per accettare connesioni in ingresso sul socket server (MSWin)
int                     SYSAcceptSocketConn ( SYS_SOCKET * sock, int * ID );
// Funzione per rilasciare connesioni in ingresso sul socket server (MSWin)
int                     SYSReleaseSocketConn( SYS_SOCKET * sock, int ID );
// Funzione per ottenere l'indirizzo di una connessione in ingresso su socket
SYS_SOCKET_CONNECTION * SYSGetSocketConn    ( SYS_SOCKET * sock, int ID );
// Funzione per ottenere l'indirizzo di una connessione in ingresso su socket (MSWin)
SYS_SOCKET_CONNECTION * SYSGetSocketConn    ( SYS_SOCKET * sock, void * addr );
// Funzione per scrivere su un socket (MSWin)
int                     SYSWriteSocket      ( SYS_SOCKET * sock, char* buffer, int len, int * bytes_sent, int connID );
// Funzione per leggere da un socket (MSWin)
int                     SYSReadSocket       ( SYS_SOCKET * sock, char* buffer, int len, int * bytes_rcvd, int connID );
// Funzione per impostare i parametri di un socket
int                     SYSSetSocketParams  ( SYS_SOCKET * sock, char * IP, int port );
// Funzione per impostare i parametri di un socket
int                     SYSSocketSetParams  ( SYS_SOCKET_PARAMS * params );
// Funzione per ottenere i parametri di un socket
int                     SYSSocketGetParams  ( SYS_SOCKET_PARAMS * params );
// Funzione per ottenere lo stato di un socket
int                     SYSSocketGetStatus  ( SYS_SOCKET_PARAMS * params );
// Funzione per chiudere il modulo (MSWin)
int                     SYSSocketClear      ( void );
// Funzione per recuperare il Nome Host
char*                   SYSGetHostName      ( void );

#endif



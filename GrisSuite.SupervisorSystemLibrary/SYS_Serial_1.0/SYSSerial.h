//==============================================================================
// Telefin Supervisor Serial Module 1.0
//------------------------------------------------------------------------------
// HEADER MODULO GESTIONE SERIALI PER S.O. MS WINDOWS 2000/XP (SYSSerial.h)
// Progetto:  Telefin Supervisor 3.0
//
// Versione:	1.0.1.04 (05.04.2004 -> 04.08.2005)
//
// Copyright: 2004-2005 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:	  Enrico Alborali (alborali@telefin.it), Olix (olivieri@telefin.it)
// Note:      richiede SYSSerial.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.01 [14.05.2004]:
// - Prima versione del modulo.
// 0.02 [20.05.2004]:
// - Aggiunte define di alcuni codici di controllo seriale.
// 0.03 [28.05.2004]:
// - Aggiunti alcuni codici di errore per le funzioni.
// - Escluso "General.h"
// - Aggiunta la definizione del tipo SYS_SERIAL_PARAMS.
// - Aggiunta la definizione del tipo SYS_SERIAL_STATUS.
// - Modificata la funzione SYSSerialLoadLib.
// - Modificata la funzione SYSSerialOpen.
// - Modificata la funzione SYSSerialSetEcho.
// - Modificata la funzione SYSSerialClose.
// - Modificata la funzione SYSSerialWrite.
// - Modificata la funzione SYSSerialRead.
// - Modificata la funzione SYSSerialSetParams.
// - Modificata la funzione SYSSerialGetParams.
// - Modificata la funzione SYSSerialGetStatus.
// - Modificata la funzione SYSSerialCloseAll.
// 1.00 [06.07.2004]:
// - Eliminato il supporto della libreria TLF_COM.dll
// - Aggiunto il flag <Echo> nella definizione del tipo SYS_SERIAL_PARAMS.
// - Aggiunto il campo <Hanlder> nella definizione del tipo SYS_SERIAL_PARAMS.
// - Aggiunto il campo <ComStatus> nella definizione del tipo SYS_SERIAL_STATUS.
// - Modificato il campo <Hanlder> nella definizione del tipo SYS_SERIAL_PARAMS.
// - Aggiunto il codice di errore SYS_SERIAL_INVALID_COM_NUMBER.
// - Reimplementata la funzione SYSSerialOpen.
// - Aggiunto il campo <LastError> nella definizione del tipo SYS_SERIAL_STATUS.
// - Reimplementata la funzione SYSSerialSetParams.
// - Reimplementata la funzione SYSSerialGetParams.
// 1.01 [07.07.2004]:
// - Corretta la funzione SYSSerialSetParams.
// - Reimplementata la funzione SYSSerialRead.
// - Modificata la funzione SYSSerialOpen.
// - Reimplementata la funzione SYSSerialClose.
// - Reimplementata la funzione SYSSerialWrite.
// 1.02 [12.04.2005]:
// - Ottimizzazione del codice.
// - Eliminata la funzione SYSSerialLoadLib.
// - Eliminata la funzione SYSSerialCloseAll.
// - Modificata la funzione SYSSerialOpen.
// - Modificata la funzione SYSSerialSetEcho.
// - Modificata la funzione SYSSerialClose.
// - Modificata la funzione SYSSerialWrite.
// - Modificata la funzione SYSSerialRead.
// - Modificata la funzione SYSSerialSetParams.
// - Modificata la funzione SYSSerialGetParams.
// - Modificata la funzione SYSSerialGetStatus.
// 1.03 [02.08.2005]:
// - Modificata la funzione SYSSerialWrite.
// 1.04 [04.08.2005]:
// - Modificata la funzione SYSSerialWrite.
//==============================================================================
#ifndef SYSSerialH
#define SYSSerialH
//------------------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SYS_SERIAL_TIMEOUT_DEFAULT        6000
#define SYS_SERIAL_DEFAULT_BUFFER_LENGTH  1024

//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SYS_SERIAL_NO_ERROR               0
#define SYS_SERIAL_INVALID_DLL_ADDRESS    6001
#define SYS_SERIAL_OPEN_FAILURE           6002
#define SYS_SERIAL_INVALID_PARAMS         6003
#define SYS_SERIAL_SET_ECHO_FAILURE       6004
#define SYS_SERIAL_CLOSE_FAILURE          6005
#define SYS_SERIAL_INVALID_BUFFER         6006
#define SYS_SERIAL_INVALID_BUFFER_LENGTH  6007
#define SYS_SERIAL_WRITE_FAILURE          6008
#define SYS_SERIAL_READ_FAILURE           6009
#define SYS_SERIAL_SET_PARAMS_FAILURE     6010
#define SYS_SERIAL_GET_PARAMS_FAILURE     6011
#define SYS_SERIAL_GET_STATUS_FAILURE     6012
#define SYS_SERIAL_CLOSE_ALL_FAILURE      6013
#define SYS_SERIAL_INVALID_COM_NUMBER     6014
#define SYS_SERIAL_SET_STOP_BITS_FAILURE  6015
#define SYS_SERIAL_SET_BAUD_RATE_FAILURE  6016
#define SYS_SERIAL_SET_DATA_BITS_FAILURE  6017
#define SYS_SERIAL_SET_PARITY_FAILURE     6018
#define SYS_SERIAL_SET_TIMEOUTS_FAILURE   6019
#define SYS_SERIAL_GET_TIMEOUTS_FAILURE   6020
#define SYS_SERIAL_PORT_NOT_OPENED        6021
#define SYS_SERIAL_ECHO_ERROR             6022
#define SYS_SERIAL_NO_ECHO                6023
#define SYS_SERIAL_WRITE_TIMEOUT_FAILURE  6024
#define SYS_SERIAL_PORT_CLOSE_EXCEPTION		6025
#define SYS_SERIAL_NOT_IMPLEMENTED        6099
//------------------------------------------------------------------------------

//==============================================================================
/// Struttura dati per il thread.
///
/// \date [06.07.2004]
/// \author Enrico Alborali, Olix
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SYS_SERIAL_STATUS
{
  COMSTAT	          ComStatus ;
  int               LastError ;
  int               BufferDim ;
  bool              Opened    ;
}
SYS_SERIAL_STATUS;

//==============================================================================
/// Struttura dati per i parametri della seriale.
///
/// \date [12.01.2010]
/// \author Enrico Alborali, Olix
/// \version 0.03
//------------------------------------------------------------------------------
typedef struct _SYS_SERIAL_PARAMS
{
  int				Com       ;
  void            * Handler   ;
  char				ChParity  ;
  unsigned __int32	Baud      ;
  int				DataBits  ;
  int				StopBits  ;
  bool              Echo      ;
  DWORD             Timeout   ;
  SYS_SERIAL_STATUS Status    ;
}
SYS_SERIAL_PARAMS;


//==============================================================================
// Funzioni
//------------------------------------------------------------------------------

/// Funzione per aprire una porta seriale (MSWin)
int SYSSerialOpen      ( SYS_SERIAL_PARAMS * params );
/// Funzione per impostare l'echo di una porta seriale
int SYSSerialSetEcho   ( SYS_SERIAL_PARAMS * params , bool echo );
/// Funzione per chiudere una porta seriale (MSWin)
int SYSSerialClose     ( SYS_SERIAL_PARAMS * params );
/// Funzione per scrivere su una porta seriale (MSWin)
int SYSSerialWrite     ( SYS_SERIAL_PARAMS * params , char * buffer, unsigned long len, unsigned long * written );
/// Funzione per leggere da una porta seriale (MSWin)
int SYSSerialRead      ( SYS_SERIAL_PARAMS * params , char * buffer, unsigned long len, unsigned long * read );
/// Funzione per impostare i parametri di una porta seriale (MSWin)
int SYSSerialSetParams ( SYS_SERIAL_PARAMS * params );
/// Funzione per ottenere i parametri di una porta seriale (MSWin)
int SYSSerialGetParams ( SYS_SERIAL_PARAMS * params );
/// Funzione per ottenere lo stato di una porta seriale
int SYSSerialGetStatus ( SYS_SERIAL_PARAMS * params );

#endif

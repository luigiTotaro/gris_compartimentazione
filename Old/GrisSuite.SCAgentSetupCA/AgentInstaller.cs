using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Configuration;
using System.Reflection;
using System.IO;

namespace GrisSuite
{
    [RunInstaller(true)]
    public partial class AgentInstaller : Installer 
    {
        public AgentInstaller()
        {
            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);

            string sConfig = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SCAgentService.exe.config";            

            ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();
            configFile.ExeConfigFilename = sConfig;
            Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
            ConfigurationSection confSection = configuration.SectionGroups["applicationSettings"].Sections["GrisSuite.WebServiceAccount"];
            if (!confSection.SectionInformation.IsProtected)
            {
                confSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
            }
            confSection.SectionInformation.ForceSave = true;
            configuration.Save(ConfigurationSaveMode.Modified);

           
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            base.Uninstall(savedState);
        }

    }
}
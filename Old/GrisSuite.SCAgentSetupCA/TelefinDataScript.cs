using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using System.IO;
using System.Data.SqlClient;

namespace GrisSuite.SCAgentSetupCA
{
    [RunInstaller(true)]
    public partial class TelefinDataScript : Installer
    {
        public TelefinDataScript()
        {
            InitializeComponent();
        }


        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);

            ExecScript();
        }

        public override void Commit(System.Collections.IDictionary savedState)
        {
            base.Commit(savedState);

            ExecScript();
        }

        public override void Rollback(System.Collections.IDictionary savedState)
        {
            base.Rollback(savedState);
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            base.Uninstall(savedState);
        }

        private string[] GetSql(string Name)
        {
            //  Gets the current assembly.
            Assembly Asm = Assembly.GetExecutingAssembly();
            //  Resources are named using a fully qualified name.
            Stream strm = Asm.GetManifestResourceStream((Asm.GetName().Name + ("." + Name)));
            //  Reads the contents of the embedded file.
            StreamReader reader = new StreamReader(strm);
            string s = reader.ReadToEnd();
            reader.Close();

            s = s.Replace("{HOST}", System.Net.Dns.GetHostName());
            return s.Split(new string[] { "\nGO" }, StringSplitOptions.RemoveEmptyEntries);
        }


        private void ExecScript()
        { 
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(global::GrisSuite.SCAgentSetupCA.Properties.Settings.Default.TelefinConnectionString);

            
            if (this.Context.Parameters.ContainsKey("datasource"))
                sb.DataSource = this.Context.Parameters["datasource"];

            if (this.Context.Parameters.ContainsKey("initialcatalog"))
                sb.InitialCatalog = this.Context.Parameters["initialcatalog"];

            string[] commands = GetSql("TelefinDataScript.SQL");

            //try
            //{
            using (SqlConnection cn = new SqlConnection(sb.ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                foreach (string sCmd in commands)
                {
                    this.Context.LogMessage("Execute:" + sCmd);
                    cmd.CommandText = sCmd;
                    cmd.ExecuteNonQuery();
                }
            }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(string.Format("Error on db access connection string =[{0}]", sb.ConnectionString), ex);
            //}
            

        }


    }

}
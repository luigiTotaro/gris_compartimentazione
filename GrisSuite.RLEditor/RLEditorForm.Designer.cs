namespace GrisSuite.RLEditor
{
    partial class RLEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RLEditorForm));
            this.bdsZones = new System.Windows.Forms.BindingSource(this.components);
            this.bdsRegions = new System.Windows.Forms.BindingSource(this.components);
            this.dsConfig1 = new GrisSuite.Data.dsConfig();
            this.bdsNodes = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgRegions = new System.Windows.Forms.DataGridView();
            this.regIDRegionsCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameRegionsCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgZones = new System.Windows.Forms.DataGridView();
            this.RegID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Removed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zonIDZonesCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameZonesCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgNodes = new System.Windows.Forms.DataGridView();
            this.zonIDNodesCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nodIDNodesCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameNodesCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metersNodesCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.removedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.importToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.exportToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.viewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.clearToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripVersion = new System.Windows.Forms.ToolStripTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDBName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripRegionList = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dsParameters1 = new GrisSuite.Data.dsParameters();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRegions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfig1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsNodes)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRegions)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgNodes)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsParameters1)).BeginInit();
            this.SuspendLayout();
            // 
            // bdsZones
            // 
            this.bdsZones.DataMember = "Regions_Zones";
            this.bdsZones.DataSource = this.bdsRegions;
            this.bdsZones.Filter = "";
            // 
            // bdsRegions
            // 
            this.bdsRegions.DataMember = "Regions";
            this.bdsRegions.DataSource = this.dsConfig1;
            // 
            // dsConfig1
            // 
            this.dsConfig1.DataSetName = "dsConfig";
            this.dsConfig1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bdsNodes
            // 
            this.bdsNodes.DataMember = "Zones_Nodes";
            this.bdsNodes.DataSource = this.bdsZones;
            this.bdsNodes.Filter = "";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgRegions);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(985, 499);
            this.splitContainer1.SplitterDistance = 255;
            this.splitContainer1.TabIndex = 4;
            this.splitContainer1.TabStop = false;
            // 
            // dgRegions
            // 
            this.dgRegions.AutoGenerateColumns = false;
            this.dgRegions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgRegions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRegions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.regIDRegionsCol,
            this.nameRegionsCol});
            this.dgRegions.DataSource = this.bdsRegions;
            this.dgRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgRegions.Location = new System.Drawing.Point(0, 0);
            this.dgRegions.MultiSelect = false;
            this.dgRegions.Name = "dgRegions";
            this.dgRegions.Size = new System.Drawing.Size(255, 499);
            this.dgRegions.TabIndex = 0;
            this.dgRegions.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.dgRegions_CellParsing);
            this.dgRegions.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgRegions_CellFormatting);
            this.dgRegions.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgRegions_DataError);
            // 
            // regIDRegionsCol
            // 
            this.regIDRegionsCol.DataPropertyName = "RegID";
            dataGridViewCellStyle1.Format = "#####";
            dataGridViewCellStyle1.NullValue = null;
            this.regIDRegionsCol.DefaultCellStyle = dataGridViewCellStyle1;
            this.regIDRegionsCol.HeaderText = "Id";
            this.regIDRegionsCol.Name = "regIDRegionsCol";
            this.regIDRegionsCol.Width = 41;
            // 
            // nameRegionsCol
            // 
            this.nameRegionsCol.DataPropertyName = "Name";
            this.nameRegionsCol.HeaderText = "Name";
            this.nameRegionsCol.Name = "nameRegionsCol";
            this.nameRegionsCol.Width = 60;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgZones);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgNodes);
            this.splitContainer2.Size = new System.Drawing.Size(726, 499);
            this.splitContainer2.SplitterDistance = 311;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // dgZones
            // 
            this.dgZones.AutoGenerateColumns = false;
            this.dgZones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgZones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgZones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RegID,
            this.Removed,
            this.zonIDZonesCol,
            this.nameZonesCol});
            this.dgZones.DataSource = this.bdsZones;
            this.dgZones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgZones.Location = new System.Drawing.Point(0, 0);
            this.dgZones.MultiSelect = false;
            this.dgZones.Name = "dgZones";
            this.dgZones.Size = new System.Drawing.Size(311, 499);
            this.dgZones.TabIndex = 1;
            this.dgZones.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.dgZones_CellParsing);
            this.dgZones.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgZones_CellFormatting);
            this.dgZones.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgZones_DataError);
            // 
            // RegID
            // 
            this.RegID.DataPropertyName = "RegID";
            this.RegID.HeaderText = "RegID";
            this.RegID.Name = "RegID";
            this.RegID.Visible = false;
            this.RegID.Width = 63;
            // 
            // Removed
            // 
            this.Removed.DataPropertyName = "Removed";
            this.Removed.HeaderText = "Removed";
            this.Removed.Name = "Removed";
            this.Removed.Visible = false;
            this.Removed.Width = 78;
            // 
            // zonIDZonesCol
            // 
            this.zonIDZonesCol.DataPropertyName = "ZonID";
            this.zonIDZonesCol.HeaderText = "Id";
            this.zonIDZonesCol.Name = "zonIDZonesCol";
            this.zonIDZonesCol.Width = 41;
            // 
            // nameZonesCol
            // 
            this.nameZonesCol.DataPropertyName = "Name";
            this.nameZonesCol.HeaderText = "Name";
            this.nameZonesCol.Name = "nameZonesCol";
            this.nameZonesCol.Width = 60;
            // 
            // dgNodes
            // 
            this.dgNodes.AutoGenerateColumns = false;
            this.dgNodes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgNodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgNodes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.zonIDNodesCol,
            this.nodIDNodesCol,
            this.nameNodesCol,
            this.metersNodesCol,
            this.removedDataGridViewTextBoxColumn});
            this.dgNodes.DataSource = this.bdsNodes;
            this.dgNodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNodes.Location = new System.Drawing.Point(0, 0);
            this.dgNodes.MultiSelect = false;
            this.dgNodes.Name = "dgNodes";
            this.dgNodes.Size = new System.Drawing.Size(411, 499);
            this.dgNodes.TabIndex = 2;
            this.dgNodes.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.dgNodes_CellParsing);
            this.dgNodes.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgNodes_CellFormatting);
            this.dgNodes.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgNodes_DataError);
            // 
            // zonIDNodesCol
            // 
            this.zonIDNodesCol.DataPropertyName = "ZonID";
            this.zonIDNodesCol.HeaderText = "ZonID";
            this.zonIDNodesCol.Name = "zonIDNodesCol";
            this.zonIDNodesCol.Visible = false;
            this.zonIDNodesCol.Width = 62;
            // 
            // nodIDNodesCol
            // 
            this.nodIDNodesCol.DataPropertyName = "NodId";
            this.nodIDNodesCol.HeaderText = "Id";
            this.nodIDNodesCol.Name = "nodIDNodesCol";
            this.nodIDNodesCol.Width = 41;
            // 
            // nameNodesCol
            // 
            this.nameNodesCol.DataPropertyName = "Name";
            this.nameNodesCol.HeaderText = "Name";
            this.nameNodesCol.Name = "nameNodesCol";
            this.nameNodesCol.Width = 60;
            // 
            // metersNodesCol
            // 
            this.metersNodesCol.DataPropertyName = "Meters";
            this.metersNodesCol.HeaderText = "Meters";
            this.metersNodesCol.Name = "metersNodesCol";
            this.metersNodesCol.Width = 64;
            // 
            // removedDataGridViewTextBoxColumn
            // 
            this.removedDataGridViewTextBoxColumn.DataPropertyName = "Removed";
            this.removedDataGridViewTextBoxColumn.HeaderText = "Removed";
            this.removedDataGridViewTextBoxColumn.Name = "removedDataGridViewTextBoxColumn";
            this.removedDataGridViewTextBoxColumn.Visible = false;
            this.removedDataGridViewTextBoxColumn.Width = 78;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton,
            this.importToolStripButton,
            this.exportToolStripButton,
            this.viewToolStripButton,
            this.clearToolStripButton,
            this.toolStripLabel1,
            this.toolStripVersion});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(985, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Load data from DB";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save data to DB";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // importToolStripButton
            // 
            this.importToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.importToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("importToolStripButton.Image")));
            this.importToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.importToolStripButton.Name = "importToolStripButton";
            this.importToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.importToolStripButton.Text = "Import from XML file";
            this.importToolStripButton.Click += new System.EventHandler(this.importToolStripButton_Click);
            // 
            // exportToolStripButton
            // 
            this.exportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.exportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("exportToolStripButton.Image")));
            this.exportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exportToolStripButton.Name = "exportToolStripButton";
            this.exportToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.exportToolStripButton.Text = "Export to Xml file";
            this.exportToolStripButton.Click += new System.EventHandler(this.exportToolStripButton_Click);
            // 
            // viewToolStripButton
            // 
            this.viewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.viewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("viewToolStripButton.Image")));
            this.viewToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.viewToolStripButton.Name = "viewToolStripButton";
            this.viewToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.viewToolStripButton.Text = "View Xml";
            this.viewToolStripButton.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // clearToolStripButton
            // 
            this.clearToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("clearToolStripButton.Image")));
            this.clearToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearToolStripButton.Name = "clearToolStripButton";
            this.clearToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearToolStripButton.Text = "Clear grids";
            this.clearToolStripButton.Click += new System.EventHandler(this.clearToolStripButton_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(49, 22);
            this.toolStripLabel1.Text = "Version:";
            // 
            // toolStripVersion
            // 
            this.toolStripVersion.Name = "toolStripVersion";
            this.toolStripVersion.Size = new System.Drawing.Size(100, 25);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDBName,
            this.toolStripRegionList,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 528);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(985, 24);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDBName
            // 
            this.toolStripDBName.Name = "toolStripDBName";
            this.toolStripDBName.Size = new System.Drawing.Size(99, 19);
            this.toolStripDBName.Text = "toolStripDBName";
            // 
            // toolStripRegionList
            // 
            this.toolStripRegionList.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripRegionList.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.toolStripRegionList.IsLink = true;
            this.toolStripRegionList.Name = "toolStripRegionList";
            this.toolStripRegionList.Size = new System.Drawing.Size(111, 19);
            this.toolStripRegionList.Text = "toolStripRegionList";
            this.toolStripRegionList.Click += new System.EventHandler(this.toolStripRegionList_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(122, 19);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // dsParameters1
            // 
            this.dsParameters1.DataSetName = "dsParameters";
            this.dsParameters1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RLEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 552);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RLEditorForm";
            this.Text = "GRIS Region List Editor";
            this.Load += new System.EventHandler(this.RLEditorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bdsZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRegions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfig1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsNodes)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgRegions)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgNodes)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsParameters1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GrisSuite.Data.dsConfig dsConfig1;
        private System.Windows.Forms.BindingSource bdsRegions;
        private System.Windows.Forms.BindingSource bdsZones;
        private System.Windows.Forms.BindingSource bdsNodes;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgRegions;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridViewTextBoxColumn regIDRegionsCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameRegionsCol;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.DataGridView dgZones;
        private System.Windows.Forms.DataGridView dgNodes;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Removed;
        private System.Windows.Forms.DataGridViewTextBoxColumn zonIDZonesCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameZonesCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn zonIDNodesCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nodIDNodesCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameNodesCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn metersNodesCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn removedDataGridViewTextBoxColumn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripDBName;
        private System.Windows.Forms.ToolStripButton importToolStripButton;
        private System.Windows.Forms.ToolStripButton exportToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel toolStripRegionList;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripButton viewToolStripButton;
        private System.Windows.Forms.ToolStripButton clearToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStripVersion;
        private GrisSuite.Data.dsParameters dsParameters1;
    }
}
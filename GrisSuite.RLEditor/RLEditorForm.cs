using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using GrisSuite.Data;

namespace GrisSuite.RLEditor
{
    public partial class RLEditorForm : Form
    {
        private string _regionListXML = string.Empty;
        private bool _isImported;

        public RLEditorForm()
        {
            _isImported = false;
            InitializeComponent();
        }

        private string RegionListXML
        {
            get
            {
                return _regionListXML;
            }
            set
            {
                if (System.IO.Path.GetDirectoryName(value) == string.Empty)
                {
                    _regionListXML = AppDomain.CurrentDomain.BaseDirectory + value;
                }
                else
                {
                    _regionListXML = value;
                }
                
                toolStripRegionList.Text = "File:" + _regionListXML;
                this.IsImported = false;
            }
        }

        private void RLEditorForm_Load(object sender, EventArgs e)
        {
            this.Text += string.Format(" - ver. {0}", Application.ProductVersion);

            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(Data.Properties.Settings.Default.TelefinConnectionString))
            {
                toolStripDBName.Text = "Server:" + cn.DataSource + " Database:" + cn.Database;
            }
            this.RegionListXML = Properties.Settings.Default.RegionListXML;

            dsConfig.Fill(dsConfig1);
            LoadRegionListVersion();

        }

        private void LoadRegionListVersion()
        {
            Data.dsParametersTableAdapters.ParametersTableAdapter pa = new Data.dsParametersTableAdapters.ParametersTableAdapter();
            dsParameters1.Clear();
            int rc = pa.FillByName(dsParameters1.Parameters, "RegionListVersion");
            if (rc == 0)
            {
                dsParameters1.Parameters.AddParametersRow("RegionListVersion", "1.0.0.0", "");
            }
            toolStripVersion.Text = dsParameters1.Parameters[0].ParameterValue;        
        }


        private void dgRegions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgRegions.Columns[e.ColumnIndex].Name == "regIDRegionsCol")
            {
                if (e.Value != null)
                {
                    DataRowView rv = this.dgRegions.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (rv != null)
                    {
                        dsConfig.RegionsRow rr = (dsConfig.RegionsRow)rv.Row;
                        e.Value = rr.GetDecodedRegId().ToString();
                        e.FormattingApplied = true;
                    }
                }
            }

        }

        private void dgRegions_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (this.dgRegions.Columns[e.ColumnIndex].Name == "regIDRegionsCol")
            {
                if (e.Value != null)
                {
                    DataRowView rv = this.dgRegions.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (rv != null)
                    {
                        dsConfig.RegionsRow rr = (dsConfig.RegionsRow)rv.Row;

                        try
                        {
                            e.Value = rr.SetDecodedRegId(ushort.Parse(e.Value.ToString()));
                            e.ParsingApplied = true;
                        }
                        catch (Exception)
                        {
                            // Set to false in case another CellParsing handler
                            // wants to try to parse this DataGridViewCellParsingEventArgs instance.
                            e.ParsingApplied = false;
                        }

                    }
                }
            }
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                dsParameters1.Parameters[0].ParameterValue = toolStripVersion.Text;
                Data.dsParametersTableAdapters.ParametersTableAdapter pa = new Data.dsParametersTableAdapters.ParametersTableAdapter();
                pa.Update(dsParameters1.Parameters);
                dsConfig.Update(dsConfig1, new[] {"Regions","Zones","Nodes" },this.IsImported);
                this.OpenFromDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on update DB:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgZones_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgZones.Columns[e.ColumnIndex].Name == "zonIDZonesCol")
            {
                if (e.Value != null)
                {
                    DataRowView rv = this.dgZones.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (rv != null)
                    {
                        dsConfig.ZonesRow r = (dsConfig.ZonesRow)rv.Row;
                        e.Value = r.GetDecodedZonId().ToString();
                        e.FormattingApplied = true;
                    }
                }
            }
        }

        private void dgZones_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (this.dgZones.Columns[e.ColumnIndex].Name == "zonIDZonesCol")
            {
                if (e.Value != null)
                {
                    DataRowView rv = this.dgZones.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (rv != null)
                    {
                        dsConfig.ZonesRow r = (dsConfig.ZonesRow)rv.Row;

                        try
                        {
                            e.Value = r.SetDecodedZonId(r.RegionsRow.GetDecodedRegId(),ushort.Parse(e.Value.ToString()));
                            e.ParsingApplied = true;
                        }
                        catch (Exception)
                        {
                            // Set to false in case another CellParsing handler
                            // wants to try to parse this DataGridViewCellParsingEventArgs instance.
                            e.ParsingApplied = false;
                        }

                    }
                }
            }
        }

        private void dgNodes_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            if (this.dgNodes.Columns[e.ColumnIndex].Name == "nodIDNodesCol")
            {
                if (e.Value != null)
                {
                    DataRowView rv = this.dgNodes.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (rv != null)
                    {
                        dsConfig.NodesRow r = (dsConfig.NodesRow)rv.Row;

                        try
                        {
                            e.Value = r.SetDecodedNodId(r.ZonesRow.RegionsRow.GetDecodedRegId(), r.ZonesRow.GetDecodedZonId(), ushort.Parse(e.Value.ToString()));
                            e.ParsingApplied = true;
                        }
                        catch (Exception)
                        {
                            // Set to false in case another CellParsing handler
                            // wants to try to parse this DataGridViewCellParsingEventArgs instance.
                            e.ParsingApplied = false;
                        }

                    }
                }
            }
        }

        private void dgNodes_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgNodes.Columns[e.ColumnIndex].Name == "nodIDNodesCol")
            {
                if (e.Value != null)
                {
                    DataRowView rv = this.dgNodes.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (rv != null)
                    {
                        dsConfig.NodesRow r = (dsConfig.NodesRow)rv.Row;
                        e.Value = r.GetDecodedNodId().ToString();
                        e.FormattingApplied = true;
                    }
                }
            }
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.OpenFromDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on read DB data:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OpenFromDB()
        {
            this.IsImported = false;
            dsConfig.Fill(dsConfig1);
            LoadRegionListVersion();
        }

        private void importToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                ImpExpRegionList ieRL = new ImpExpRegionList();
                DataSet ds = ieRL.ImportFileToDataSet(this.RegionListXML);
                dsConfig1.Clear();
                dsConfig1.Merge(ds);
                MessageBox.Show(this, "File imported from :" + this.RegionListXML, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.IsImported = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on import Xml file:" + ex.Message,this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void toolStripRegionList_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = "File XML|*.xml";
                if (System.IO.File.Exists(_regionListXML))
                {
                    openFileDialog1.FileName = this.RegionListXML ;
                }
                if (DialogResult.OK == openFileDialog1.ShowDialog(this))
                {
                    this.RegionListXML = openFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on change Xml file:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                ImpExpRegionList ieRL = new ImpExpRegionList();
                ieRL.ExportToFile(this.RegionListXML);

                MessageBox.Show(this, "File exported to :" + this.RegionListXML, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on export Xml file:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartInfo pi = new ProcessStartInfo
                                          {
                                              Arguments = this.RegionListXML,
                                              FileName = Properties.Settings.Default.XmlViewer,
                                              WorkingDirectory =
                                                  AppDomain.CurrentDomain.BaseDirectory
                                          };


                Process.Start(pi);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error on show Xml file:" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearToolStripButton_Click(object sender, EventArgs e)
        {
            this.IsImported = false;
            dsConfig1.Clear();
        }

        private void dgZones_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message,this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
            e.Cancel = true;
        }

        private void dgNodes_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
        }

        private void dgRegions_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
        }

        private bool IsImported
        {
            get
            {
                return _isImported;
            }
            set
            {
                _isImported = value;
                toolStripStatusLabel1.Text = _isImported ? "Imported from XML" : "";
            }
        }


    }
}
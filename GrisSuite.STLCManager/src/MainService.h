//---------------------------------------------------------------------------
#ifndef MainServiceH
#define MainServiceH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <SvcMgr.hpp>
#include <vcl.h>
//---------------------------------------------------------------------------
class TSTLCManagerService : public TService
{
__published:    // IDE-managed Components
	void __fastcall ServiceStart(TService *Sender, bool &Started);
	void __fastcall ServiceStop(TService *Sender, bool &Stopped);
private:        // User declarations
public:         // User declarations
	__fastcall TSTLCManagerService(TComponent* Owner);
	TServiceController __fastcall GetServiceController(void);

	friend void __stdcall ServiceController(unsigned CtrlCode);
};
//---------------------------------------------------------------------------
extern PACKAGE TSTLCManagerService *STLCManagerService;
//---------------------------------------------------------------------------
#endif

//==============================================================================
// Telefin SCAgent Main Thread Module 1.0
//------------------------------------------------------------------------------
// MODULO THREAD PRINCIPALE (MainThread.cpp)
// Progetto:  Telefin STLCManager 1.0
//
// Versione:  0.01 (24.01.2007 -> 24.01.2007)
//
// Copyright: 2006 Telefin S.p.A.
// Ambiente:  Borland C++ Builder 6
// Autore:	  Mario Ferro (mferro@deltasistemi.it)
// Note:      richiede MainThread.h
//------------------------------------------------------------------------------
// Version history: vedere in MainThread.h
//==============================================================================


#pragma hdrstop
#include "SYSKernel.h"

#include "MainThread.h"
#include "MainService.h"
#include "ServiceFunctions.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
static SYS_THREAD * MainThread;
DWORD lastCheckDebugTime = 0l ; // Unix Time


//==============================================================================
/// Funzione per inizializzare il modulo del thread
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [04.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int MTInit( void )
{
	int ret_code = SRV_MAIN_THREAD_NO_ERROR;

	MainThread = SYSNewThread( MThreadActiveFunction, 0, CREATE_SUSPENDED ); // Alloco il thread principale
	if ( MainThread != NULL )
	{
		ret_code = SYSSetThreadParameter( MainThread, (void*) MainThread ); // Gli passo come parametro la procedura
		if ( ret_code == SRV_MAIN_THREAD_NO_ERROR )
		{
			ret_code = SYSCreateThread( MainThread );
			if ( ret_code == SRV_MAIN_THREAD_NO_ERROR )
			{
				IsDebugActive = SFIsDebugActive();
				lastCheckDebugTime = SYSGetUnixTime();
				ret_code = SFConfigInit();
			}
		}
	}
	else
	{
		ret_code = SRV_MAIN_THREAD_ALLOCATION_FAILURE;
	}

	if (ret_code != SRV_MAIN_THREAD_NO_ERROR)
	{
		AnsiString msg  = "ERRORE " + AnsiString(ret_code) + " in fase di Inizializzazione del modulo Gestione Servizi";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo del thread
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [04.04.2006]
/// \author Enrico Alborali
/// \version 0.02
//------------------------------------------------------------------------------
int MTClear( void )
{
  int ret_code = SRV_MAIN_THREAD_NO_ERROR;

  if ( SYSValidThread( MainThread ) )
  {
	ret_code = SYSTerminateThread( MainThread );
	// --- Aspetto la fine del thread ---
	if ( SYSWaitForThread( MainThread ) != 0 )
	{
	  // --- Aspetto 250 ms ---
	  Sleep( 250 );



	  ///ret_code = SCAConfigClear( );  vedi sopra
	}
	// --- Elimino il thread terminato ---
	ret_code = SYSDeleteThread( MainThread );
  }
  else
  {
	ret_code = SRV_MAIN_THREAD_INVALID_THREAD;
  }

  return ret_code;
}

//==============================================================================
/// Funzione per avviare il thread principale
///
/// \date [31.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int MTStart( void )
{
  int ret_code = SRV_MAIN_THREAD_NO_ERROR;

  ret_code = SYSResumeThread( MainThread );

  return ret_code;
}

//==============================================================================
/// Funzione per arrestare il thread principale
///
/// \date [31.03.2006]
/// \author Enrico Alborali
/// \version 0.01
//------------------------------------------------------------------------------
int MTStop( void )
{
  int ret_code = SRV_MAIN_THREAD_NO_ERROR;
  int fun_code = SRV_MAIN_THREAD_NO_ERROR;

  SFMessage("ARRESTO SERVIZIO",MT_INFO,1100);

  ret_code = SYSSuspendThread( MainThread );

  return ret_code;
}

//==============================================================================
/// Funzione di attivazione per il thread principale
///
/// \date [05.03.2007]
/// \author Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
unsigned long __stdcall MThreadActiveFunction( void * parameter )
{
	unsigned long           ret_code        = SRV_MAIN_THREAD_NO_ERROR; // Codice di ritorno
	SYS_THREAD            * thread          = NULL                    ; // Puntatore al thread
	int i = 0;
	AnsiString msg;

	if ( parameter != NULL ) // Controllo il parametro passato
	{
		thread = (SYS_THREAD*) parameter; // Recupero il thread
		if ( thread != NULL ) // Se la procedura � valida
		{
			try
			{
				SFMessage("AVVIO THREAD CONTROLLO SERVIZI",MT_INFO,1000);

				while ( thread->Terminated == false ) // Ciclo del thread
				{
					//Ad intervalli di 5 minuti verifico se la modalit� di debug � attiva o meno
					if ((SYSGetUnixTime() -  lastCheckDebugTime) >= SRV_CHECK_DEBUG_TIMEOUT)
					{
						IsDebugActive = SFIsDebugActive();
						lastCheckDebugTime = SYSGetUnixTime();
					}

					for(i = 0; i < NumServices; i++)
					{

						// se per il servizio � previsto il controllo che non sia Stopped
						if (Services[i].CheckStartTimeout > 0)
						{
							if ((SYSGetUnixTime() - Services[i].LastCheckStartTime) >= Services[i].CheckStartTimeout)
							{
								ret_code = SFCheckServiceIsStoppedAndRestart(Services[i].Name);

								Services[i].LastCheckStartTime = SYSGetUnixTime();
							}
						}

						// se per il servizio � previsto il controllo che non sia bloccato
						if (Services[i].AutocheckRestartTimeout > 0)
						{
							if ((SYSGetUnixTime() - Services[i].LastAutocheckRestartTime) >= Services[i].AutocheckRestartTimeout)
							{
								ret_code = SFCheckServiceIsBlockedAndRestart(Services[i].Name,Services[i].AutocheckRestartTimeout);

								Services[i].LastAutocheckRestartTime = SYSGetUnixTime();
							}
						}

						// se per il servizio � previsto riavvio forzato
						if (Services[i].ForcedRestartTimeout > 0)
						{
							if ((SYSGetUnixTime() - Services[i].LastForcedRestartTime) >= Services[i].ForcedRestartTimeout)
							{
								ret_code = SFServiceForcedRestart(Services[i].Name);

								Services[i].LastForcedRestartTime = SYSGetUnixTime();
							}
						}

					} // end for

					Sleep(5000); // Rilascio le risorse.

				} // end while
			}
			catch (...)
			{
				ret_code = SRV_MAIN_THREAD_CRITICAL_ERROR;
			}
		}
	}
	return ret_code;
}
/*

//==============================================================================
/// Funzione di attivazione per il thread principale
///
/// \date [05.03.2007]
/// \author Mario Ferro
/// \version 0.08
//------------------------------------------------------------------------------
unsigned long __stdcall MThreadActiveFunction( void * parameter )
{
	unsigned long           ret_code        = SRV_MAIN_THREAD_NO_ERROR; // Codice di ritorno
	int                     fun_code        = SRV_MAIN_THREAD_NO_ERROR; // Codice di ritorno
	SYS_THREAD            * thread          = NULL                    ; // Puntatore al thread
	int serviceState;
	AnsiString sName;
	int checkTime = SRV_CHECK_TIME;

	if ( parameter != NULL ) // Controllo il parametro passato
	{
		thread = (SYS_THREAD*) parameter; // Recupero il thread
		if ( thread != NULL ) // Se la procedura � valida
		{
			try
			{
				SFMessage("AVVIO THREAD CHECK AND RESTART SERVICES",MT_INFO,1000);

				int msgType = MT_ERROR;
				AnsiString msg  = "LETTURA CHECK_TIME FALLITA";
				if (getServiceConfigSource() != NULL)
				{
					serviceElement = XMLResolveElement(getServiceConfigSource()->root_element,"manager[0]:service[0]",NULL);
					if (serviceElement != NULL) {
						char * value = XMLGetValue(serviceElement,"check_time");
						if (value != NULL)
						{
							checkTime = StrToInt(AnsiString(value));
							msg  = "CHECK_TIME = " + AnsiString(value) + " secondi";
							msgType = MT_INFO;
						}
					}
				}

				SFMessage(msg,msgType,1001);

				while ( thread->Terminated == false ) // Ciclo del thread
				{

					if ((SYSGetUnixTime() - lastCheckTime) >= checkTime)
					{
						if ((serviceElement != NULL) && (serviceElement->child != NULL))
						{
							// Cerco gli elementi XML figli
							itemElement = serviceElement->child;
							while (itemElement)
							{
								sName = AnsiString(XMLGetValue(itemElement,"name"));

								 ///DEBUG
								 //	msg  = "SERVIZIO " + sName + " IN TEST";
								 //	STLCManagerService->LogMessage( msg, EVENTLOG_INFORMATION_TYPE, 0, fun_code );
								///						AnsiString sName = "SPVServerDaemon";

								ret_code = SFCheckAndRestartService(sName.c_str(),&serviceState);


								if (serviceState == SYS_KERNEL_SERVICE_RESTARTED)
								{

									msg = "SERVIZIO " + sName + " � arrestato !!!";
									SFMessage(msg,MT_WARNING,1010);

									msg = "SERVIZIO " + sName + " riavviato correttamente !!!";
									SFMessage(msg,MT_INFO,1050);

									if (ret_code != SYS_KERNEL_NO_ERROR)
									{
										msg  = "ERRORE " + AnsiString(ret_code) + " in SFCheckAndRestartService";
										SFMessage(msg,MT_ERROR,ret_code);
									}
								}
								else if (serviceState == SYS_KERNEL_SERVICE_NOT_RUNNING)
								{
									msg = "SERVIZIO " + sName + " � arrestato !!!";
									SFMessage(msg,MT_WARNING,1010);

									msg  = "SERVIZIO " + sName + ". ERRORE : " + AnsiString(ret_code) + " in fase di riavvio";
									SFMessage(msg,MT_ERROR,ret_code);
								}
								else if (ret_code != SYS_KERNEL_NO_ERROR)
								{
									msg  = "ERRORE " + AnsiString(ret_code) + " in SFCheckAndRestartService";
									SFMessage(msg,MT_ERROR,ret_code);
								}

								// Passo all'elemento successivo (se esiste)
								itemElement = itemElement->next;
							}

						}

						lastCheckTime = SYSGetUnixTime();

					}
					// Rilascio le risorse.
					Sleep(5000);
				}
			}
			catch (...)
			{
				ret_code = SRV_MAIN_THREAD_CRITICAL_ERROR;
			}
		}
	}
	return ret_code;
}
*/

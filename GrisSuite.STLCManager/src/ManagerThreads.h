//==============================================================================
// Telefin ManagerThreads Module 1.0
//------------------------------------------------------------------------------
// MODULO DEI THREAD (ManagerThreads.cpp)
// Progetto:  	Telefin STLCManager 1.0
//
// Revisione:  	0.03 (29.03.2007 -> 27.03.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      	richiede ManagerThreads.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [29.03.2007]:
// - Prima versione del modulo
// 0.01 [05.03.2008]:
// - Aggiunta la funzione WatchDogThreadInit
// - Aggiunta la funzione WatchDogThreadClear
// - Aggiunta la funzione WatchDogThreadActiveFunction (NON IMPLEMENTATA)
// - Modificata la funzione MTInit
// - Modificata la funzione MTStart
// - Modificata la funzione MTStop
// - Modificata la funzione MTClear
// - Modificata la funzione ServiceThreadActiveFunction
// 0.02 [07.03.2008]:
// - Modificata la funzione ServiceThreadActiveFunction
// 0.03 [27.03.2008]:
// - Modificata la funzione MTStop
// - Modificata la funzione ServiceThreadActiveFunction
// - Modificata la funzione MTInit
//==============================================================================


#ifndef ManagerThreadsH
#define ManagerThreadsH
//---------------------------------------------------------------------------

#define SRV_CHECK_DEBUG_TIMEOUT			   			300


//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SRV_MANAGER_THREADS_NO_ERROR                   	0
#define SRV_FUNCTION_NOT_IMPLEMENTED					715800

#define SRV_SERVICE_THREAD_NO_ERROR                    	0
#define SRV_SERVICE_THREAD_CRITICAL_ERROR              	715801
#define SRV_SERVICE_THREAD_ALLOCATION_FAILURE          	715802
#define SRV_SERVICE_THREAD_INVALID_THREAD              	715803

#define SRV_FAN_THREAD_NO_ERROR 	                   	0
#define SRV_FAN_THREAD_CRITICAL_ERROR              		715811
#define SRV_FAN_THREAD_ALLOCATION_FAILURE          		715812
#define SRV_FAN_THREAD_INVALID_THREAD              		715813

#define SRV_WATCH_DOG_THREAD_NO_ERROR                  	0
#define SRV_WATCH_DOG_THREAD_CRITICAL_ERROR        		715821
#define SRV_WATCH_DOG_THREAD_ALLOCATION_FAILURE         715822
#define SRV_WATCH_DOG_THREAD_INVALID_THREAD        		715823

#define SRV_SERVICE_FAILURE								715831
#define SRV_SYSTEM_FAILURE								715832
#define SRV_SYSTEM_REBOOT								715833
#define SRV_FBWFAPI_NOT_AVAILABLE						715834
#define SRV_FBWFAPI_FAILURE								715835

// Funzione per inizializzare il modulo dei thread
void MTInit( void );
// Funzione per chiudere il modulo dei thread
void MTClear( void );
// Funzione per avviare i thread
void MTStart( void );
// Funzione per arrestare i thread
void MTStop( void );
// Funzione di attivazione per il thread per la gestione del WatchDog
unsigned long __stdcall WatchDogThreadActiveFunction( void * parameter );
// Funzione di attivazione per il thread per la gestione dei servizi
unsigned long __stdcall ServiceThreadActiveFunction( void * parameter );
// Funzione di attivazione per il thread per la gestione delle ventole
unsigned long __stdcall FanThreadActiveFunction( void * parameter );


#endif
//---------------------------------------------------------------------------

#ifndef MainThreadH
#define MainThreadH
//---------------------------------------------------------------------------

#define SRV_CHECK_DEBUG_TIMEOUT			   			300


//==============================================================================
// Codici di errore
//------------------------------------------------------------------------------
#define SRV_MAIN_THREAD_NO_ERROR                    0
#define SRV_MAIN_THREAD_CRITICAL_ERROR              715801
#define SRV_MAIN_THREAD_ALLOCATION_FAILURE          715802
#define SRV_MAIN_THREAD_INVALID_THREAD              715803



// Funzione per inizializzare il modulo del thread principale
int MTInit( void );
// Funzione per chiudere il modulo del thread principale
int MTClear( void );
// Funzione per avviare il thread principale
int MTStart( void );
// Funzione per arrestare il thread principale
int MTStop( void );
// Funzione di attivazione per il thread principale
unsigned long __stdcall MThreadActiveFunction( void * parameter );


#endif

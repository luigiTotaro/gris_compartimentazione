//==============================================================================
// Telefin Service Function Module 1.0
//------------------------------------------------------------------------------
// MODULO FUNZIONI RICHIAMATE DAL SERVIZIO (ServiceFunction.cpp)
// Progetto:  Telefin STLCManager 1.0
//
// Revisione:  	0.05 (28.02.2007 -> 30.05.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      	richiede ServiceFunction.cpp
//------------------------------------------------------------------------------
// Version history:
// 0.00 [28.02.2007]:
// - Prima versione del modulo
// 0.01 [05.03.2008]:
// - Aggiunta la funzione SFSTLCSetWatchDog
// - Aggiunta la funzione SFSTLCTriggerWatchDog
// - Aggiunta la funzione SFSTLCSetSystemLED
// - Aggiunta la funzione SFSTLCSetPWMFans
// - Modificata la funzione SFLoadJIDA
// - Modificata la funzione SFUnloadJIDA
// - Modificata la funzione SFConfigInit
// - Modificata la funzione SFCheckServiceIsBlockedAndRestart
// - Modificata la funzione SFCheckServiceIsStoppedAndRestart
// - Modificata la funzione SFServiceForcedRestart
// - Modificata la funzione SFCheckTemperatureAndManageFans
// - Modificata la funzione SFStopService
// 0.02 [07.03.2008]:
// - Aggiunta la funzione SFIsMaintenanceActive
// - Aggiunta la funzione SFSetKeyMaintenanceValue
// - Modificata la funzione SFConfigInit
// 0.03 [27.03.2008]:
// - Aggiunta la funzione SFCreateWatchDogLogPath
// - Aggiunta la funzione SFCreateWatchDogLogManager
// - Aggiunta la funzione SFCloseWatchDogLogManager
// - Aggiunta la funzione SFAddLogTriggerTime
// - Modificata la funzione SFConfigInit
// 0.04 [28.03.2008]:
// - Modificata la funzione SFCheckTemperatureAndManageFans
// 0.05 [30.05.2008]:
// - Modificata la funzione SFLoadJIDA
//==============================================================================


#ifndef ServiceFunctionsH
#define ServiceFunctionsH
//---------------------------------------------------------------------------
#include "System.hpp"
#include "XMLInterface.h"

//==============================================================================
// Codici del tipo dei messaggi dell'EventLog
//------------------------------------------------------------------------------
#define MT_INFO			1
#define	MT_WARNING		2
#define	MT_ERROR		3

//==============================================================================
// Codici dei messaggi dell'EventLog
//------------------------------------------------------------------------------
#define COD_SERVICE_MANAGEMENT_STARTED					1000
#define COD_SERVICE_MANAGEMENT_NOT_STARTED				1001
#define COD_SERVICE_BLOCKED								1010
#define COD_SERVICE_BLOCKED_RESTART_SUCCESS				1015
#define COD_SERVICE_STOPPED								1020
#define COD_SERVICE_STOPPED_RESTART_SUCCESS				1025
#define COD_SERVICE_FORCED_RESTART_SUCCESS				1035

#define COD_FAN_THREAD_STARTED                   		1100
#define COD_FAN_THREAD_NOT_STARTED                 		1101
#define COD_FAN_THRESHOLD_SORTED						1105
#define COD_CPU_SENSOR_TEMPERATURE_ACTIVATED			1111
#define COD_MB_SENSOR_TEMPERATURE_ACTIVATED				1112

#define COD_SERVICE_ALREADY_STOPPED						1210
#define COD_SERVICE_STOPPED_FOR_UPDATE					1220

#define COD_MANAGER_SERVICE_STOPPED	               		1300

#define COD_JIDA_LOADING_SUCCESS						1400

#define COD_WATCHDOG_ACTIVACTION_SUCCESS				1500
#define COD_WATCHDOG_MAINTENANCE_ACTIVACTION_SUCCESS	1501
#define COD_WATCHDOG_MANAGEMENT_STARTED					1502
#define COD_WATCHDOG_MANAGEMENT_NOT_STARTED				1503

#define COD_WATCHDOG_MAINTENANCE_STARTED				1601
#define COD_SERVICE_WATCHDOG_MANAGEMENT_NOT_STARTED		1701
//==============================================================================
// Definizione codici di errore
//------------------------------------------------------------------------------
#define SF_OPEN_XML_CONFIG_SOURCE_CRITICAL_ERROR         73001
#define SF_OPEN_XML_INDEX_SOURCE_CRITICAL_ERROR        	 73002
#define SF_FUNCTION_EXCEPTION							 73003
#define SF_SERVICE_CONFIG_SOURCE_NULL_VALUE              73004
#define SF_INVALID_REGKEY_PATH							 73005

#define MAX_SERVICES 	20
#define MAX_THRESHOLDS  20
#define MAX_COMPONENTS  20

#define SF_MAINTENANCE_ENABLED				1L
#define SF_MAINTENANCE_DISABLED	            0L

#define SF_WATCH_DOG_PROG_ERROR				-1L



//==============================================================================
/// Struttura dati per conrollo dei servizi.
///
/// \date [20.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SF_SERVICE
{
	char		* Name             		   	; // Nome del servizio da controllare
	int         AutocheckRestartTimeout		; // Timeout di controllo per ripartenza se servizio � in blocco
	DWORD		LastAutocheckRestartTime    ; // Unix Time dove si memorizza la Data/Ora dell'ultimo Autochech
	int         CheckStartTimeout       	; // Timeout di controllo per partenza se servizio � Stopped
	DWORD		LastCheckStartTime          ; // Unix Time dove si memorizza la Data/Ora dell'ultimo Chech
	int         ForcedRestartTimeout       	; // Timeout per ripartenza forzata
	DWORD		LastForcedRestartTime       ; // Unix Time dove si memorizza la Data/Ora dell'ultimo riavvio forzato
}
SF_SERVICE;

//==============================================================================
/// Strutture dati per gestione delle ventole.
///
/// \date [30.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
typedef struct _SF_THRESHOLD
{
	int			Temperature        		; // temperatura di soglia
	int     	Range					; // range della soglia
	int			PWMRightFan				; // valore percentuale di PWM per la ventola destra
	int         PWMLeftFan     			; // valore percentuale di PWM per la ventola sinistra
}
SF_THRESHOLD;

typedef struct _SF_STATUS
{
	int			ThresholdTemperature	; // temperatura di soglia corrente
	int			ThresholdRange			; // range di soglia corrente
}
SF_STATUS;

//==============================================================================
/// Strutture dati per gestione dell'aggiornamento.
///
/// \date [16.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
enum CompTypes {CTUndefined,CTApplication,CTService};

typedef struct _SF_COMPONENT
{
	char		* Name             		   	; // Nome del servizio o del processo interssato all'aggiornamento
	int         ComponentType				; // discrimina se il componente � un Servizio o un Processo
	bool		Restart						; // indica se bisogna procedere al riavvio dopo che � avvenuto l'aggiornamento del file
}
SF_COMPONENT;


extern int NumServices;
extern SF_SERVICE Services[];

extern int NumThresholds;
extern SF_THRESHOLD Thresholds[];
extern SF_STATUS CurrentStatus;

extern int NumComponents;
extern SF_COMPONENT Components[];

extern bool RebootOnFailure;

extern bool IsServiceDebugActive;
extern bool IsFanDebugActive;
extern bool IsUpdateDebugActive;

extern bool IsFanActive;
extern bool IsWatchDogActive;
extern bool IsWatchDogLEDActive;
extern bool IsMaintenanceActive;
extern bool IsFBWFActive;

extern unsigned int ServiceFailureCount;
extern bool IsSystemFailure;
extern bool IsFBWFFailure;

extern unsigned int WatchDogDelay;
extern unsigned int WatchDogTimeOut;
extern unsigned int WatchDogMaintenance;

extern unsigned int FBWFMaxMemoryPerc;

extern unsigned int TemperatureCheckDelay;
extern bool IsCPUSensor;

// Funzione che ritorna un booleano indicante se la modalit� di debug della gestione servizi � attiva
bool SFIsServiceDebugActive( void );
// Funzione che ritorna un booleano indicante se la modalit� di debug della gestione ventole � attiva
bool SFIsFanDebugActive( void );
// Funzione per scrivere un messaggio di Debug relativo alla gestione del riavvio dei servizi nell'Application Event Log
void SFServiceDebug(AnsiString msg,int eventCode);
// Funzione per scrivere un messaggio di Debug relativo alla gestione delle ventole nell'Application Event Log
void SFFanDebug(AnsiString msg,int eventCode);
// Funzione per scrivere un messaggio di Debug relativo alla gestione dell'aggiornamento delle applicazioni dell'STLC1000
void SFUpdateDebug(AnsiString msg,int eventCode);
// Funzione per scrivere un messaggio nell'Application Event Log
void SFMessage(AnsiString msg, int msgType, int eventCode);
// Funzione per caricare la configurazione del servizio
int SFConfigInit();
// Funzione per caricare la JIDA
int SFLoadJIDA();
// Funzione per scaricare la JIDA
int SFUnloadJIDA();
// Funzione per far ripartire un servizio nel caso verifichi che � bloccato
int SFCheckServiceIsBlockedAndRestart(char * service, int blockedTimeout);
// Funzione per far ripartire un servizio nel caso verifichi che non � attivo
int SFCheckServiceIsStoppedAndRestart(char * service);
// Funzione per riavviare forzatamente un servizio
int SFServiceForcedRestart(char * service);
// Funzione per la gestione delle ventole in funzione della temperatura rilevata
int SFCheckTemperatureAndManageFans();
// Funzione per caricare i file da aggiornare da un file XML
int SFLoadXMLIndexFile();
// Funzione per la gestione dell'aggiornamento dei file dell'STLC1000
int SFSTLCFileUpdate();
// Funzione per il Set del WatchDog
int SFSTLCSetWatchDog( unsigned int timeout, unsigned int delay );
// Funzione per il Trigger del WatchDog
int SFSTLCTriggerWatchDog( void );
// Funzione per accendere/spegnere uno dei due led di sistema
int SFSTLCSetSystemLED( int led, bool on );
// Funzione per regolare ugualmente la velocit� di rotazione di entrambe le ventole dell'STLC1000
int SFSTLCSetPWMFans( int pwmPercent );

// Funzione per Chiudere e cancellare il WhatchDogLog_manager
int SFCloseWatchDogLogManager( void );
// Funzione per scrivere su log la data ora dell'ultimo trigger del WatchDog
int SFAddLogTriggerTime( void );

#endif

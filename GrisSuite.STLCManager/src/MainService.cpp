//---------------------------------------------------------------------------
#include "MainService.h"
#include "ManagerThreads.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TSTLCManagerService *STLCManagerService;
//---------------------------------------------------------------------------
__fastcall TSTLCManagerService::TSTLCManagerService(TComponent* Owner)
	: TService(Owner)
{
}

TServiceController __fastcall TSTLCManagerService::GetServiceController(void)
{
	return (TServiceController) ServiceController;
}

void __stdcall ServiceController(unsigned CtrlCode)
{
	STLCManagerService->Controller(CtrlCode);
}
//---------------------------------------------------------------------------
void __fastcall TSTLCManagerService::ServiceStart(TService *Sender,
      bool &Started)
{
  MTInit();

  MTStart();
}
//---------------------------------------------------------------------------
void __fastcall TSTLCManagerService::ServiceStop(TService *Sender,
	  bool &Stopped)
{
  MTStop();

  MTClear();
}
//---------------------------------------------------------------------------

//==============================================================================
// Telefin Service Function Module 1.0
//------------------------------------------------------------------------------
// MODULO FUNZIONI RICHIAMATE DAL SERVIZIO (ServiceFunction.cpp)
// Progetto:  Telefin STLCManager 1.0
//
// Revisione:  	0.05 (28.02.2007 -> 30.05.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      	richiede ServiceFunction.h
//------------------------------------------------------------------------------
// Version history: vedere in ServiceFunction.h
//==============================================================================


#pragma hdrstop

#include "ServiceFunctions.h"
#include "SYSKernel.h"
#include "SPVSTLC1000.h"
#include "MainService.h"
#include "UtilityLibrary.h"
#include "cnv_lib.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//==============================================================================
// Definizioni
//------------------------------------------------------------------------------
#define SF_ACCESS_CODE 										1
#define SF_OPEN_CODE 										55
#define SF_LAST_AUTO_CHECK_SERVICES_KEY           			"SYSTEM\\CurrentControlSet\\Services\\"
#define SF_LAST_AUTO_CHECK_VALUE		           			"LastAutoCheck"
#define SF_CFG_SERVICE_KEY                        			"SOFTWARE\\Telefin\\STLCManagerService"
#define SF_CFG_XML_SERVICE_VALUE_NAME       				"XMLConfig"
#define SF_CFG_XML_SERVICE_DEFAULT_VALUE_PATH				"C:\\Program Files\\Telefin\\STLCManager\\STLCManagerConfig.xml"
//#define SF_CFG_TEM_SERVICE_DEFAULT_VALUE_PATH 			"STLCManagerConfig.tem"  ???
#define SF_INDEX_XML_VALUE_NAME 		      				"XMLIndex"
#define SF_INDEX_XML_DEFAULT_VALUE_PATH						"C:\\Program Files\\Telefin\\Update\\Index.xml"
#define SF_CFG_DEBUG_SERVICE_VALUE				       		"DebugService"
#define SF_CFG_DEBUG_FAN_VALUE					       		"DebugFan"
#define SF_CFG_DEBUG_UPDATE_VALUE                           "DebugUpdate"
#define SF_CFG_MAINTENANCE_VALUE					       	"TriggeredMaintenance"
#define SF_CFG_WATCH_DOG_PROG_VALUE                         "WDProg"

static XML_SOURCE * ServiceConfigSource;
static XML_SOURCE * IndexSource;

int NumServices = 0;
SF_SERVICE Services[MAX_SERVICES];

int NumThresholds = 0;
SF_THRESHOLD Thresholds[MAX_THRESHOLDS];
SF_STATUS CurrentStatus;

int NumComponents = 0;
SF_COMPONENT Components[MAX_COMPONENTS];

bool RebootOnFailure;

bool IsServiceDebugActive;
bool IsFanDebugActive;
bool IsUpdateDebugActive;

bool IsFanActive;
bool IsWatchDogActive;
bool IsWatchDogLEDActive;
bool IsMaintenanceActive;
bool IsFBWFActive;

// Gestione del caso System Failure
unsigned int ServiceFailureCount = 0;
bool IsSystemFailure = false; // viene inizializzato a false
bool IsFBWFFailure = false;

unsigned int WatchDogDelay;
unsigned int WatchDogTimeOut;
unsigned int WatchDogMaintenance;
unsigned int WatchDogMaxProg;

unsigned int FBWFMaxMemoryPerc;

unsigned int TemperatureCheckDelay;
bool IsCPUSensor;

FILE * pWDLog_file;

//==============================================================================
/// Funzione che ritorna un intero indicante il progressivo dell'ultimo file
/// del WatchDog

/// In caso di errore nella lettura della chiave restituisce -1L
///
/// \date [17.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
DWORD SFGetWatchDogProgValue( void )
{
	void  *			key      	    	= 	NULL;
	DWORD * 		WDProg_value   = 	NULL;
	DWORD 			WDProgValue 	= 	SF_WATCH_DOG_PROG_ERROR	;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
		if ( key != NULL )
		{
			WDProg_value = (DWORD *) SYSGetRegValue( key, SF_CFG_WATCH_DOG_PROG_VALUE);
			SYSCloseRegKey( key );
		}

		if (WDProg_value != NULL)
		{
			WDProgValue = *WDProg_value ;
		}
	}
	catch(...)
	{
		WDProgValue = SF_WATCH_DOG_PROG_ERROR;
	}


	return WDProgValue;
}

//==============================================================================
/// Funzione che scrive sulla chiave di registro "WDProg" un intero
///
/// Se non c'� errore nella scrittura della chiave restituisce 0
///
/// \date [17.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSetKeyWatchDogProgValue(int prog_value)
{
	void  *			key      	    = 	NULL;
	int				ret_code		= 	SYS_KERNEL_NO_ERROR;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, true );
		if ( key != NULL )
		{
			ret_code = SYSSetRegValue( key, SF_CFG_WATCH_DOG_PROG_VALUE, (void*)&prog_value, REG_DWORD, sizeof(DWORD)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = SF_INVALID_REGKEY_PATH;
		}
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
	}
	return ret_code;
}

//==============================================================================
/// Funzione per istanziare ed aprire il WhatchDogLog_manager
///
/// \date [12.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFCreateWatchDogLogManager( AnsiString logPath  )
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	char * WDLogPath 	 = NULL;

	char * WDLogFileName = NULL;
	//char * WDLogTime 	 = NULL;
	char * WDLogProg 	 = NULL;
	char * desc = NULL;
	char * timeValue = NULL;
	int prog = 0;

	try
	{
		prog = SFGetWatchDogProgValue();
		if (prog < (WatchDogMaxProg))
		{
			prog++;
		}
		else
		{
			prog = 1;
		}
		SFSetKeyWatchDogProgValue(prog);


		WDLogProg = ULAddText(WDLogProg,cnv_UInt32ToCharP(prog));
		WDLogFileName = ULAddText(WDLogFileName,"WatchDog_");
		WDLogFileName = ULAddText(WDLogFileName,WDLogProg);
		WDLogFileName = ULAddText(WDLogFileName,".log");
		WDLogPath = ULAddText(WDLogPath,logPath.c_str());
		WDLogPath = ULAddText(WDLogPath,WDLogFileName);

		timeValue = cnv_DateTimeToCharP(ULGetUnixTime());
		desc = ULAddText(desc,"WatchDogStartTime");
		desc = ULAddText(desc," = ");
		desc = ULAddText(desc,timeValue);
		desc = ULAddText(desc,"\n");
		desc = ULAddText(desc,"\n");

		pWDLog_file = fopen ( WDLogPath , "w" );
		fputs ( desc , pWDLog_file );
		fflush(pWDLog_file);
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFCreateWatchDogLogManager";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	ULFreeAndNullString(&timeValue);
	ULFreeAndNullString(&desc);
	ULFreeAndNullString(&WDLogProg);
	ULFreeAndNullString(&WDLogFileName);
	ULFreeAndNullString(&WDLogPath);

	return ret_code;
}

//==============================================================================
/// Funzione per Chiudere e cancellare il WhatchDogLog_manager
///
/// \date [12.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFCloseWatchDogLogManager( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	try
	{
		if (pWDLog_file != NULL)
		{
			fputs ( "\n" , pWDLog_file );
			fputs ( "\n" , pWDLog_file );
			fputs ( "Chiusura WatchDog" , pWDLog_file );
			fflush(pWDLog_file);
			fclose ( pWDLog_file );
		}
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFCloseWatchDogLogManager";
		SFMessage(msg,MT_ERROR,ret_code);
	}
	return ret_code;
}

//==============================================================================
/// Funzione per scrivere su log la data ora dell'ultimo trigger del WatchDog
/// Gestisce anche il caso di System Failure.
/// In caso di errore restituisce un valore non nullo.
///
/// \date [22.05.2010]
/// \author Mario Ferro, Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
int SFAddLogTriggerTime( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	char * desc = NULL;
	char * timeValue = NULL;

	try
	{
		if ( pWDLog_file != NULL )
		{
				fseek ( pWDLog_file , 43 , SEEK_SET );
				// Ultima esecuzione del trigger
				timeValue = cnv_DateTimeToCharP(ULGetUnixTime());
				
				if (IsSystemFailure) {
					desc = ULAddText(desc,"SystemFailure");
					desc = ULAddText(desc," = ");
					desc = ULAddText(desc,timeValue);
				}
				else
				{
					desc = ULAddText(desc,"LastTriggerTime");
					desc = ULAddText(desc," = ");
					desc = ULAddText(desc,timeValue);
				}
				
				fputs ( desc , pWDLog_file );

				fflush(pWDLog_file);
		}
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFAddLogTriggerTime";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	ULFreeAndNullString(&timeValue);
	ULFreeAndNullString(&desc);

	return ret_code;
}

//==============================================================================
/// Funzione che ritorna un booleano indicante se la modalit� di debug della
/// gestione servizi � attiva
/// In caso di errore nella lettura della chiave restituisce false
///
/// \date [20.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SFIsServiceDebugActive( void )
{
	void        * key           = NULL;
	char        * debug_value   = NULL;
	bool		  isDebugActive = false;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
	if ( key != NULL )
	{
		debug_value = (char*) SYSGetRegValue( key, SF_CFG_DEBUG_SERVICE_VALUE );
		SYSCloseRegKey( key );
	}

	if (debug_value != NULL)
	{
		if ((AnsiString(debug_value)) == "1")
		{
			isDebugActive = true;
		}
	}

	return isDebugActive;
}

//==============================================================================
/// Funzione che ritorna un booleano indicante se la modalit� di debug della
/// gestione ventole � attiva
/// In caso di errore nella lettura della chiave restituisce false
///
/// \date [02.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SFIsFanDebugActive( void )
{
	void        * key           = NULL;
	char        * debug_value   = NULL;
	bool		  isDebugActive = false;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
	if ( key != NULL )
	{
		debug_value = (char*) SYSGetRegValue( key, SF_CFG_DEBUG_FAN_VALUE );
		SYSCloseRegKey( key );
	}

	if (debug_value != NULL)
	{
		if ((AnsiString(debug_value)) == "1")
		{
			isDebugActive = true;
		}
	}

	return isDebugActive;
}

//==============================================================================
/// Funzione che ritorna un booleano indicante se la modalit� di manutenzione
/// � attiva
/// 0 : manutenzione disabilitata
/// 1 : manutenzione abilitata
/// In caso di errore nella lettura della chiave restituisce false
///
/// \date [07.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SFIsMaintenanceActive( void )
{
	void  *			key      	    	= 	NULL;
	DWORD * 		maintenance_value   = 	NULL;
	DWORD 			maintenanceValue 	= 	SF_MAINTENANCE_DISABLED	;
	bool		    isMaintenanceActive = false;


	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
		if ( key != NULL )
		{
			maintenance_value = (DWORD *) SYSGetRegValue( key, SF_CFG_MAINTENANCE_VALUE);
			SYSCloseRegKey( key );
		}

		if (maintenance_value != NULL)
		{
			maintenanceValue = *maintenance_value ;
		}
	}
	catch(...)
	{
		maintenanceValue = SF_MAINTENANCE_DISABLED;
	}

	if (maintenanceValue == SF_MAINTENANCE_ENABLED)
	{
		isMaintenanceActive = true;
	}

	return isMaintenanceActive;
}

//==============================================================================
/// Funzione che scrive sulla chiave di registro "Maintenance"
/// i valori 0 o 1.
/// 0 : manutenzione disabilitata
/// 1 : manutenzione abilitata
/// Se non c'� errore nella scrittura della chiave restituisce 0
///
/// \date [07.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSetKeyMaintenanceValue(bool enableValue)
{
	void  *			key      	    = 	NULL;
	int				ret_code		= 	SYS_KERNEL_NO_ERROR;

	try
	{
		key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, true );
		if ( key != NULL )
		{
			ret_code = SYSSetRegValue( key, SF_CFG_MAINTENANCE_VALUE, (void*)&enableValue, REG_DWORD, sizeof(DWORD)) ;
			SYSCloseRegKey( key );
		}
		else
		{
			ret_code = SF_INVALID_REGKEY_PATH;
		}
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
	}
	return ret_code;
}

//==============================================================================
/// Funzione che ritorna un booleano indicante se la modalit� di debug della
/// gestione dell'aggiornamento � attiva.
/// In caso di errore nella lettura della chiave restituisce false
///
/// \date [02.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SFIsUpdateDebugActive( void )
{
	void        * key           = NULL;
	char        * debug_value   = NULL;
	bool		  isDebugActive = false;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
	if ( key != NULL )
	{
		debug_value = (char*) SYSGetRegValue( key, SF_CFG_DEBUG_UPDATE_VALUE );
		SYSCloseRegKey( key );
	}

	if (debug_value != NULL)
	{
		if ((AnsiString(debug_value)) == "1")
		{
			isDebugActive = true;
		}
	}

	return isDebugActive;
}

//==============================================================================
/// Funzione per scrivere un messaggio di Debug relativo alla gestione del
//  riavvio dei servizi nell'Application Event Log.
/// Scrive il messaggio solo se � abilitata la modalit� di debug
///
/// \date [20.03.2007]
/// \author Mario Ferro
/// \version 0.01
//==============================================================================
void SFServiceDebug(AnsiString msg,int eventCode)
{
	if (STLCManagerService != NULL)
	{
		if (IsServiceDebugActive)
		{
			STLCManagerService->LogMessage("DEBUG SERVICES: " + msg, EVENTLOG_INFORMATION_TYPE, 2, eventCode );
		}
	}
}

//==============================================================================
/// Funzione per scrivere un messaggio di Debug relativo alla gestione delle
//  ventole nell'Application Event Log.
/// Scrive il messaggio solo se � abilitata la modalit� di debug
///
/// \date [20.03.2007]
/// \author Mario Ferro
/// \version 0.01
//==============================================================================
void SFFanDebug(AnsiString msg,int eventCode)
{
	if (STLCManagerService != NULL)
	{
		if (IsFanDebugActive)
		{
			STLCManagerService->LogMessage("DEBUG FANS: " + msg, EVENTLOG_INFORMATION_TYPE, 2, eventCode );
		}
	}
}

//==============================================================================
/// Funzione per scrivere un messaggio di Debug relativo alla gestione
/// dell'aggiornamento delle applicazioni dell'STLC1000
/// Scrive il messaggio solo se � abilitata la modalit� di debug
///
/// \date [13.04.2007]
/// \author Mario Ferro
/// \version 0.01
//==============================================================================
void SFUpdateDebug(AnsiString msg,int eventCode)
{
	if (STLCManagerService != NULL)
	{
		if (IsUpdateDebugActive)
		{
			STLCManagerService->LogMessage("DEBUG UPDATE: " + msg, EVENTLOG_INFORMATION_TYPE, 2, eventCode );
		}
	}
}

//==============================================================================
/// Funzione per scrivere un messaggio nell'Application Event Log
///
/// \date [20.03.2007]
/// \author Mario Ferro
/// \version 0.01
//==============================================================================
void SFMessage(AnsiString msg, int msgType, int eventCode)
{
	if (STLCManagerService != NULL)
	{
		if (msgType == MT_INFO)
		{
			STLCManagerService->LogMessage( msg, EVENTLOG_INFORMATION_TYPE, 1, eventCode );
		}
		else if (msgType == MT_WARNING)
		{
			STLCManagerService->LogMessage( msg, EVENTLOG_WARNING_TYPE, 1, eventCode );
		}
		else
		{
			STLCManagerService->LogMessage( msg, EVENTLOG_ERROR_TYPE, 3, eventCode );
		}
	}
}

//==============================================================================
/// Funzione che ritorna un DWORD riportante l'ultima Data/Ora scritta
/// nella chiave di registro dal servizio da controllare
/// In caso di errore nella lettura della chiave restituisce 0l
///
/// \date [21.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
DWORD SFGetKeyCheckTimeValue(AnsiString serviceName)
{
	void           * key      	    = 	NULL;
	DWORD		   * time_value   	= 	NULL;
	DWORD			 timeValue 		= 	0l	;

	AnsiString lastAutoCheckServicesKeyPath = SF_LAST_AUTO_CHECK_SERVICES_KEY + serviceName;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, lastAutoCheckServicesKeyPath.c_str(), false );
	if ( key != NULL )
	{
		time_value = (DWORD *) SYSGetRegValue( key, SF_LAST_AUTO_CHECK_VALUE);
		SYSCloseRegKey( key );
	}

	if (time_value != NULL)
	{
		timeValue = *time_value ;
	}

	return timeValue;
}

//==============================================================================
/// Funzione per ottenere un puntatore alla Costante di tipo stringa
/// che si passa come parametro
///
/// \date [09.03.2007]
/// \author Mario Ferro
/// \version 0.01
//==============================================================================
char * SFGetCharPointer( const char * Str )
{
	char  * value_buffer  = NULL;

	int sizeStr = StrLen(Str);

	value_buffer = (char*)malloc( sizeof(char) * sizeStr +1 ); // -MALLOC
	if ( value_buffer != NULL )
	{
		memcpy( value_buffer, Str, sizeof(char) * sizeStr +1 );
	}
	return value_buffer;
}

//==============================================================================
/// Funzione per ottenere il percorso e il nome del file di configurazione
/// del servizio
/// In caso di errore restituisce un puntatore a NULL.
///
/// \date [09.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char * SFGetXMLConfigPathFileName( void )
{
	void        * key               = NULL;
	char        * xml_system_value  = NULL;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
	if ( key != NULL )
	{
		xml_system_value = (char*) SYSGetRegValue( key, SF_CFG_XML_SERVICE_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	if ((xml_system_value == NULL) || ((AnsiString(xml_system_value))== "") ){
		xml_system_value = SFGetCharPointer(SF_CFG_XML_SERVICE_DEFAULT_VALUE_PATH);
	}

	return xml_system_value;
}

//==============================================================================
/// Funzione per caricare in un file XML di configurazione dato il path per ricercarlo
///
/// In caso di errore restituisce un valore diverso da 0
///
/// \date [09.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFOpenXMLConfigSource(AnsiString path)
{
	int ret_code = XML_NO_ERROR;

	try
	{
		ServiceConfigSource = XMLCreate(path.c_str());

		if (ServiceConfigSource != NULL)
		{
			ret_code = XMLOpen(ServiceConfigSource,NULL, 'R');
			if (ret_code == XML_NO_ERROR)
			{
				ret_code = XMLRead(ServiceConfigSource,NULL);
				if (ret_code == XML_NO_ERROR)
				{
					XMLCheck(ServiceConfigSource);
				}
			}
			XMLClose(ServiceConfigSource);
		}

		return ret_code;
	}
	catch(...)
	{
		return SF_OPEN_XML_CONFIG_SOURCE_CRITICAL_ERROR;
	}
}


//==============================================================================
/// Funzione per ottenere il percorso e il nome del file indice
/// per l'aggiornamento
/// In caso di errore restituisce un puntatore a NULL.
///
/// \date [12.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
char * SFGetXMLIndexPathFileName( void )
{
	void        * key               = NULL;
	char        * xml_system_value  = NULL;

	key = SYSOpenRegKey( HKEY_LOCAL_MACHINE, SF_CFG_SERVICE_KEY, false );
	if ( key != NULL )
	{
		xml_system_value = (char*) SYSGetRegValue( key, SF_INDEX_XML_VALUE_NAME );
		SYSCloseRegKey( key );
	}

	if ((xml_system_value == NULL) || ((AnsiString(xml_system_value))== "") ){
		xml_system_value = SFGetCharPointer(SF_INDEX_XML_DEFAULT_VALUE_PATH);
	}

	return xml_system_value;
}

//==============================================================================
/// Funzione per caricare un file XML di indice dato il path per ricercarlo
///
/// In caso di errore restituisce un valore diverso da 0
///
/// \date [12.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFOpenXMLIndexSource(AnsiString path)
{
	int ret_code = XML_NO_ERROR;

	try
	{
		IndexSource = XMLCreate(path.c_str());

		if (IndexSource != NULL)
		{
			ret_code = XMLOpen(IndexSource,NULL, 'R');
			if (ret_code == XML_NO_ERROR)
			{
				ret_code = XMLRead(IndexSource,NULL);
				if (ret_code == XML_NO_ERROR)
				{
					XMLCheck(IndexSource);
				}
			}
			XMLClose(IndexSource);
		}

		return ret_code;
	}
	catch(...)
	{
		return SF_OPEN_XML_INDEX_SOURCE_CRITICAL_ERROR;
	}
}


//==============================================================================
/// Funzione per caricare la JIDA
///
/// In caso di errore restituisce un valore diverso da 0
///
/// \date [30.05.2008]
/// \author Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SFLoadJIDA()
{
	int ret_code = SPV_STLC1000_NO_ERROR;

	try
	{
		ret_code = STLC1KInitJidaDll();
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFLoadJIDA dovuta a chiamata di STLC1KInitJidaDll";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		try
		{
			ret_code = STLC1KJidaDllInitialize();
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFLoadJIDA dovuta a chiamata di STLC1KJidaDllInitialize";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		try
		{
			ret_code = STLC1KOpenCPUBoard();
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFLoadJIDA dovuta a chiamata di STLC1KOpenCPUBoard";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}


	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		try
		{
			ret_code = STLC1KSetInfoTemperature();
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFLoadJIDA dovuta a chiamata di STLC1KSetInfoTemperature";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}


	return ret_code;
}

//==============================================================================
/// Funzione per scaricare la JIDA
///
/// In caso di errore restituisce un valore diverso da 0
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SFUnloadJIDA()
{
	int ret_code = SPV_STLC1000_NO_ERROR;

	try
	{
		ret_code = STLC1KCloseCPUBoard();
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFUnloadJIDA dovuta a chiamata di STLC1KCloseCPUBoard";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		try
		{
			ret_code = STLC1KJidaDllUninitialize();
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFUnloadJIDA dovuta a chiamata di STLC1KJidaDllUninitialize";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		try
		{
			ret_code = STLC1KClearJidaDll();
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFUnloadJIDA dovuta a chiamata di STLC1KClearJidaDll";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	return ret_code;
}

//==============================================================================
/// Funzione per ordinare l'array delle soglie di temperatura
///
/// Utilizza l'algoritmo di "Shell Sort" standard
///
/// In caso di errore restituisce un valore diverso da 0
///
/// \date [05.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
void SFSortThresholds()
{
	int gap,n,m;
	SF_THRESHOLD TempThreshold;
	bool go, sorted;

	sorted = false;
	gap = NumThresholds;
	while(gap > 1)
	{
		gap = gap / 2;
		go = true;
		while (go)
		{
			for (m = 0; m <(NumThresholds - gap); m++)
			{
				n = m + gap;
				if (Thresholds[m].Temperature > Thresholds[n].Temperature)
				{
					TempThreshold.Temperature 	= Thresholds[n].Temperature;
					TempThreshold.Range 		= Thresholds[n].Range;
					TempThreshold.PWMRightFan 	= Thresholds[n].PWMRightFan;
					TempThreshold.PWMLeftFan 	= Thresholds[n].PWMLeftFan;

					Thresholds[n].Temperature 	= Thresholds[m].Temperature;
					Thresholds[n].Range 		= Thresholds[m].Range;
					Thresholds[n].PWMRightFan 	= Thresholds[m].PWMRightFan;
					Thresholds[n].PWMLeftFan 	= Thresholds[m].PWMLeftFan;

					Thresholds[m].Temperature 	= TempThreshold.Temperature;
					Thresholds[m].Range 		= TempThreshold.Range;
					Thresholds[m].PWMRightFan 	= TempThreshold.PWMRightFan;
					Thresholds[m].PWMLeftFan 	= TempThreshold.PWMLeftFan;

					sorted = true;
					SFFanDebug("Sorted", 2170);
					go = true;
				}
				else
				{
					go = false;
				}
			} // end for
		}
	}

	if (sorted)
	{
		AnsiString msg  = "Effettuato ordinamento delle soglie di tempertura. Le soglie caricate non erano in ordine crescente";
		SFMessage(msg,MT_INFO,COD_FAN_THRESHOLD_SORTED);
	}

}


//==============================================================================
/// Funzione per caricare la configurazione del servizio da un file XML
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.03.2008]
/// \author Mario Ferro
/// \version 0.04
//------------------------------------------------------------------------------
int SFConfigInit()
{
	XML_ELEMENT * managerElement;
	XML_ELEMENT * watchdogElement;
	XML_ELEMENT * serviceElement;
	XML_ELEMENT * fanElement;
	XML_ELEMENT * itemElement;
	XML_ELEMENT * fbwfElement;
	
	char * xmlValue;
	DWORD currentTime;
	int i;
	AnsiString msg;

	int ret_code = SYS_KERNEL_NO_ERROR;

	RebootOnFailure = false;
	IsFanActive = false;
	IsWatchDogActive = false;
	IsWatchDogLEDActive = false;
	IsMaintenanceActive = false;
	IsFBWFActive = false;

	IsMaintenanceActive = SFIsMaintenanceActive();

	if (IsMaintenanceActive)
	{
		SFSetKeyMaintenanceValue(false);
	}

	ret_code = SFOpenXMLConfigSource(AnsiString(SFGetXMLConfigPathFileName()));

	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		if (ServiceConfigSource != NULL)
		{
			managerElement = XMLResolveElement(ServiceConfigSource->root_element,"manager[0]",NULL);

			if ((managerElement != NULL) &&
				
				 ((XMLGetValue(managerElement,"reboot_on_failure") != NULL) &&
				  (AnsiString(XMLGetValue(managerElement,"reboot_on_failure")) == "true")))
			{
				RebootOnFailure = true; // 
			}

			// Caricamento dell'elemento relativo alla gestione del watchdog
			watchdogElement = XMLResolveElement(ServiceConfigSource->root_element,"manager[0]:watchdog[0]",NULL);

			if ((watchdogElement != NULL) &&
				((XMLGetValue(watchdogElement,"enabled") == NULL) ||
				 ((XMLGetValue(watchdogElement,"enabled") != NULL) &&
				  (AnsiString(XMLGetValue(watchdogElement,"enabled")) == "true"))))
			{
				IsWatchDogActive = true; // se la gestione del watchdog � configurata
			}

			if (!IsMaintenanceActive)
			{
				// Caricamento dell'elemento relativo alla gestione delle ventole
				fanElement = XMLResolveElement(ServiceConfigSource->root_element,"manager[0]:fan[0]",NULL);

				if ((fanElement != NULL) &&
					(fanElement->child != NULL) &&
					((XMLGetValue(fanElement,"enabled") == NULL) ||
					 ((XMLGetValue(fanElement,"enabled") != NULL) &&
					  (AnsiString(XMLGetValue(fanElement,"enabled")) == "true"))))
				{
					IsFanActive = true; // se la gestione delle ventole � configurata
				}
			}

			if (IsWatchDogActive || IsFanActive || IsMaintenanceActive)
			{
				// Carico la Jida
				ret_code = SFLoadJIDA();
				if (ret_code == SPV_STLC1000_NO_ERROR)
				{
					msg = "Libreria JIDA caricata.";
					SFMessage(msg,MT_INFO,COD_JIDA_LOADING_SUCCESS);
				}
				else
				{
					msg  = "Caricamento libreria JIDA  fallito. Errore : " + AnsiString(ret_code);
					SFMessage(msg,MT_ERROR,ret_code);
				}

			}

			if (IsWatchDogActive)
			{
				// Inizializzo le variabili del watchdog
				WatchDogDelay = 0;
				WatchDogTimeOut = 600000;
				WatchDogMaintenance = 3600000;
				WatchDogMaxProg = 100;
				IsWatchDogLEDActive = true;
				AnsiString WatchDogLogPath = "C:\\Program Files\\Telefin\\Log\\";

				// Caricamento dei parametri per la gestione del watchdog
				xmlValue = XMLGetValue(watchdogElement,"timeout");
				if (xmlValue != NULL)
				{
					WatchDogTimeOut = (StrToInt(AnsiString(xmlValue)) * 1000);
				}

				xmlValue = XMLGetValue(watchdogElement,"maintenance");
				if (xmlValue != NULL)
				{
					WatchDogMaintenance = (StrToInt(AnsiString(xmlValue)) * 1000);
				}

				xmlValue = XMLGetValue(watchdogElement,"led");
				if (xmlValue != NULL)
				{
					IsWatchDogLEDActive = (StrToBool(AnsiString(xmlValue)));
				}

				xmlValue = XMLGetValue(watchdogElement,"log_path");
				if (xmlValue != NULL)
				{
					WatchDogLogPath = AnsiString(xmlValue);
				}

				xmlValue = XMLGetValue(watchdogElement,"max_log");
				if (xmlValue != NULL)
				{
					WatchDogMaxProg = StrToInt(AnsiString(xmlValue));
				}

				// Attivazione del WatchDog
				try
				{
					if (IsWatchDogLEDActive)
					{
						STLC1KOpenIOC();
					}

					SFCreateWatchDogLogManager(WatchDogLogPath);

					ret_code = SFSTLCSetWatchDog(WatchDogTimeOut,WatchDogDelay) ;

					msg = "WatchDog ";
					if (IsMaintenanceActive)
					{
						msg = msg + "di manutenzione";
					}

					if (ret_code == SPV_STLC1000_NO_ERROR)
					{
						msg = msg + " attivato. Timeout = " + AnsiString(WatchDogTimeOut) + " ms";
						SFMessage(msg,MT_INFO,COD_WATCHDOG_ACTIVACTION_SUCCESS);
					}
					else
					{
						msg  = "Attivazione del " + msg + " fallita. Errore : " + AnsiString(ret_code);
						SFMessage(msg,MT_ERROR,ret_code);
					}
				}
				catch(...)
				{
					ret_code = SF_FUNCTION_EXCEPTION;
					AnsiString msg  = "Eccezione in SFConfigInit dovuta a chiamata di SFSTLCSetWatchDog";
					SFMessage(msg,MT_ERROR,ret_code);
				}
			}

			// Caricamento dei servizi da controllare
			serviceElement = XMLResolveElement(ServiceConfigSource->root_element,"manager[0]:service[0]",NULL);

			if ((serviceElement != NULL) &&
				(serviceElement->child != NULL) &&
				((XMLGetValue(serviceElement,"enabled") == NULL) ||
				 ((XMLGetValue(serviceElement,"enabled") != NULL) &&
				  (AnsiString(XMLGetValue(serviceElement,"enabled")) == "true"))))
			{
				currentTime = ULGetUnixTime();
				NumServices = serviceElement->child_count;
				// Cerco gli elementi XML figli
				itemElement = serviceElement->child;
				i=0;

				while (itemElement)
				{
					Services[i].Name = "Sconosciuto";
					Services[i].AutocheckRestartTimeout = 0;
					Services[i].LastAutocheckRestartTime = currentTime;
					Services[i].CheckStartTimeout = 0;
					Services[i].LastCheckStartTime = currentTime;
					Services[i].ForcedRestartTimeout = 0;
					Services[i].LastForcedRestartTime = currentTime;

					xmlValue = XMLGetValue(itemElement,"name");
					if (xmlValue != NULL)
					{
						Services[i].Name = xmlValue;
					}

					xmlValue = XMLGetValue(itemElement,"forced_restart_timeout");
					if (xmlValue != NULL)
					{
						Services[i].ForcedRestartTimeout = StrToInt(AnsiString(xmlValue));
					}

					xmlValue = XMLGetValue(itemElement,"autocheck_restart_timeout");
					if (xmlValue != NULL)
					{
						Services[i].AutocheckRestartTimeout = StrToInt(AnsiString(xmlValue));
					}

					xmlValue = XMLGetValue(itemElement,"check_start_timeout");
					if (xmlValue != NULL)
					{
						Services[i].CheckStartTimeout = StrToInt(AnsiString(xmlValue));
					}
					// Passo all'elemento successivo (se esiste)
					itemElement = itemElement->next;
					i++;
				}
			}

			

			if (IsFanActive)
			{
				// Caricamento dell'intervallo di check della temperatura
				TemperatureCheckDelay = 60000;
				xmlValue = XMLGetValue(fanElement,"delay");
				if (xmlValue != NULL)
				{
					TemperatureCheckDelay = (StrToInt(AnsiString(xmlValue)));
				}

				// Caricamento del sensore di rilevamento della temperatura (CPU/MotherBoard)
				IsCPUSensor = true;
				AnsiString sensor = "CPU";
				xmlValue = XMLGetValue(fanElement,"sensor");
				if (xmlValue != NULL)
				{
					sensor = AnsiString(xmlValue);
				}
				if (sensor == "MB")
				{
					IsCPUSensor = false;
					SFMessage("Sensore MotherBoard attivato",MT_INFO,COD_MB_SENSOR_TEMPERATURE_ACTIVATED);
				}
				else
				{
					SFMessage("Sensore CPU attivato",MT_INFO,COD_CPU_SENSOR_TEMPERATURE_ACTIVATED);
                }

				// Caricamento delle soglie di temperatura per la gestione delle ventole
				NumThresholds = fanElement->child_count;

				// Cerco gli elementi XML figli
				itemElement = fanElement->child;
				i=0;
				while (itemElement)
				{
					Thresholds[i].Temperature = 0;
					Thresholds[i].Range = 0;
					Thresholds[i].PWMRightFan = 0;
					Thresholds[i].PWMLeftFan = 0;

					xmlValue = XMLGetValue(itemElement,"temperature");
					if (xmlValue != NULL)
					{
						Thresholds[i].Temperature = StrToInt(AnsiString(xmlValue));
					}

					xmlValue = XMLGetValue(itemElement,"range");
					if (xmlValue != NULL)
					{
						Thresholds[i].Range = StrToInt(AnsiString(xmlValue));
					}

					xmlValue = XMLGetValue(itemElement,"pwm_right_fan");
					if (xmlValue != NULL)
					{
						Thresholds[i].PWMRightFan = StrToInt(AnsiString(xmlValue));
					}

					xmlValue = XMLGetValue(itemElement,"pwm_left_fan");
					if (xmlValue != NULL)
					{
						Thresholds[i].PWMLeftFan = StrToInt(AnsiString(xmlValue));
					}

					// Passo all'elemento successivo (se esiste)
					itemElement = itemElement->next;
					i++;
				}

				SFSortThresholds();
				// Inizializzo lo stato corrente
				CurrentStatus.ThresholdTemperature = 0;
				CurrentStatus.ThresholdRange = 0;

				// All'inizio spengo le ventole
				try
				{
					SFSTLCSetPWMFans(0);
				}
				catch(...)
				{
					ret_code = SF_FUNCTION_EXCEPTION;
					AnsiString msg  = "SFConfigInit: Eccezione in SFSTLCSetPWMFans";
					SFMessage(msg,MT_ERROR,ret_code);
				}

			}

			fbwfElement = XMLResolveElement(ServiceConfigSource->root_element,"manager[0]:fbwf[0]",NULL);

			if ((fbwfElement != NULL) &&
				((XMLGetValue(fbwfElement,"enabled") == NULL) ||
				 ((XMLGetValue(fbwfElement,"enabled") != NULL) &&
				  (AnsiString(XMLGetValue(fbwfElement,"enabled")) == "true"))))
			{
				IsFBWFActive = true; // se la gestione del watchdog � configurata

				xmlValue = XMLGetValue(fbwfElement,"max_mem_perc");
				if (xmlValue != NULL)
				{
					FBWFMaxMemoryPerc = (StrToInt(AnsiString(xmlValue)) );
				}
			}
			
			XMLFree(ServiceConfigSource);  //libero memoria che non mi serve pi�
		}
		else
		{
			ret_code = SF_SERVICE_CONFIG_SOURCE_NULL_VALUE;
		}
	}

	if(IsMaintenanceActive)
	{
		// Accensione precauzionale delle ventole
		try
		{
			SFSTLCSetPWMFans(100);
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "SFConfigInit: Eccezione in SFSTLCSetPWMFans";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	return ret_code;
}


//==============================================================================
/// Funzione per far ripartire un servizio nel caso verifichi che � bloccato
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [04.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SFCheckServiceIsBlockedAndRestart(char * service, int blockedTimeout)
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	bool serviceIsBlocked = false;
	AnsiString msg;


	//DEBUG
	msg  = "Chiamata funzione ""SFCheckServiceIsBlockedAndRestart"" per servizio " + AnsiString(service);
	SFServiceDebug(msg,2010);
	//

	ret_code = SYSOpenSCManager("",NULL,SF_ACCESS_CODE); // SF_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,SF_OPEN_CODE);
		if (schService != NULL)
		{
			//ret_code = SYSServiceIsStopped(schService,&serviceIsBlocked);
			if ((ULGetUnixTime() - SFGetKeyCheckTimeValue(AnsiString(service))) >= blockedTimeout)
			{
				 serviceIsBlocked = true;
			}


			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (serviceIsBlocked)
				{
					msg = "Servizio " + AnsiString(service) + " � bloccato.";
					SFMessage(msg,MT_WARNING,COD_SERVICE_BLOCKED);

					ret_code = SYSRestartService(schService,5000);
					if (ret_code == SYS_KERNEL_NO_ERROR)
					{
						msg = "Servizio " + AnsiString(service) + " bloccato riavviato con successo.";
						SFMessage(msg,MT_INFO,COD_SERVICE_BLOCKED_RESTART_SUCCESS);
					}
					else
					{
						msg  = "Riavvio del Servizio " + AnsiString(service) + " bloccato fallito. Errore : " + AnsiString(ret_code);
						SFMessage(msg,MT_ERROR,ret_code);
					}
				}
			}
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (!SYSCloseService(schService))
				{
					ret_code = SYS_KERNEL_SERVICE_CLOSE_FAILURE;
				}
			}
			else
			{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
				SYSCloseService(schService);
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
			SYSCloseSCManager();
		}
	}

	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		msg  = "Errore " + AnsiString(ret_code) + " in SFCheckServiceIsBlockedAndRestart";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}

//==============================================================================
/// Funzione per far ripartire un servizio nel caso verifichi che non � attivo
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SFCheckServiceIsStoppedAndRestart(char * service)
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	bool serviceIsStopped;
	AnsiString msg;


	//DEBUG
	msg  = "Chiamata funzione ""SFCheckServiceIsStoppedAndRestart"" per servizio " + AnsiString(service);
	SFServiceDebug(msg,2020);
	//

	ret_code = SYSOpenSCManager("",NULL,SF_ACCESS_CODE); // SF_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,SF_OPEN_CODE);
		if (schService != NULL)
		{
			ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (serviceIsStopped)
				{
					msg = "Servizio " + AnsiString(service) + " � arrestato";
					SFMessage(msg,MT_WARNING,COD_SERVICE_STOPPED);

					ret_code = SYSStartService(schService);
					if (ret_code == SYS_KERNEL_NO_ERROR)
					{
						msg = "Servizio " + AnsiString(service) + " riavviato correttamente";
						SFMessage(msg,MT_INFO,COD_SERVICE_STOPPED_RESTART_SUCCESS);
					}
					else
					{
						msg  = "Servizio " + AnsiString(service) + " non riavviato. Errore : " + AnsiString(ret_code) + " in fase di riavvio";
						SFMessage(msg,MT_ERROR,ret_code);
					}
				}
			}
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (!SYSCloseService(schService))
				{
					ret_code = SYS_KERNEL_SERVICE_CLOSE_FAILURE;
				}
			}
			else
			{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
				SYSCloseService(schService);
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
			SYSCloseSCManager();
		}
	}

	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		msg  = "Errore " + AnsiString(ret_code) + " in SFCheckServiceIsStoppedAndRestart";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}


//==============================================================================
/// Funzione per riavviare forzatamente un servizio
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SFServiceForcedRestart(char * service)
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	AnsiString msg;


	//DEBUG
	msg  = "Chiamata funzione ""SFServiceForcedRestart"" per servizio " + AnsiString(service);
	SFServiceDebug(msg,2030);
	//

	ret_code = SYSOpenSCManager("",NULL,SF_ACCESS_CODE); // SF_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,SF_OPEN_CODE);
		if (schService != NULL)
		{
			ret_code = SYSRestartService(schService,5000);

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				msg = "Servizio " + AnsiString(service) + " riavviato forzatamente con successo";
				SFMessage(msg,MT_INFO,COD_SERVICE_FORCED_RESTART_SUCCESS);
			}
			else
			{
				msg  = "Riavvio forzato del Servizio " + AnsiString(service) + " fallito. Errore : " + AnsiString(ret_code);
				SFMessage(msg,MT_ERROR,ret_code);
			}

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (!SYSCloseService(schService))
				{
					ret_code = SYS_KERNEL_SERVICE_CLOSE_FAILURE;
				}
			}
			else
			{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
				SYSCloseService(schService);
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
			SYSCloseSCManager();
		}
	}

	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		msg  = "Errore " + AnsiString(ret_code) + " in SFServiceForcedRestart";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}

//==============================================================================
/// Funzione per la gestione delle ventole in funzione della temperatura rilevata
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [28.03.2008]
/// \author Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
int SFCheckTemperatureAndManageFans()
{
	AnsiString msg;
	int ret_code = SPV_STLC1000_NO_ERROR;
	int i = 0;
	double temperature = 10000000;
	BYTE MBTemperature = 0;

	//DEBUG
	msg  = "Chiamata funzione ""SFCheckTemperatureAndManageFans"" ";
	SFFanDebug(msg,2110);
	//

	if (IsCPUSensor)
	{
		try
		{
			ret_code = STLC1KGetCPUTemperature(&temperature);
			//DEBUG
			msg  = "Chiamata funzione ""STLC1KGetCPUTemperature"" ";
			SFFanDebug(msg,2111);
			//
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a chiamata di STLC1KGetCPUTemperature";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}
	else
	{
		try
		{
			ret_code = STLC1KGetMBTemperature(&MBTemperature);
			temperature = (double)MBTemperature;
			//DEBUG
			msg  = "Chiamata funzione ""STLC1KGetMBTemperature"" ";
			SFFanDebug(msg,2112);
			//
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a chiamata di STLC1KGetMBTemperature";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}


	if (ret_code == SPV_STLC1000_NO_ERROR)
	{
		if (!((temperature >= (CurrentStatus.ThresholdTemperature - CurrentStatus.ThresholdRange)) &&
			(temperature <= (CurrentStatus.ThresholdTemperature + CurrentStatus.ThresholdRange))))
		{	//la temperatura rilevata � uscita dall'intervallo di soglia

			if (temperature > CurrentStatus.ThresholdTemperature)  //la temperatura sta salendo
			{
				for(i= NumThresholds; i>0; i--)
				{
					if (temperature >= Thresholds[i-1].Temperature)
					{
						if (CurrentStatus.ThresholdTemperature != Thresholds[i-1].Temperature)
						{
							//DEBUG
							msg  = "La temperatura CRESCENTE rilevata (" + AnsiString(temperature) +" �C) � uscita dall'intervallo di soglia (" +
							AnsiString(CurrentStatus.ThresholdTemperature) + "�C +- " + AnsiString(CurrentStatus.ThresholdRange) + "�C).";
							//

							CurrentStatus.ThresholdTemperature = Thresholds[i-1].Temperature;
							CurrentStatus.ThresholdRange = Thresholds[i-1].Range;

							try
							{
								ret_code = STLC1KSetPWMRightFan(Thresholds[i-1].PWMRightFan);
							}
							catch(...)
							{
								ret_code = SF_FUNCTION_EXCEPTION;
								AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a chiamata di STLC1KSetPWMRightFan per temperatura crescente";
								SFMessage(msg,MT_ERROR,ret_code);
							}

							if (ret_code == SPV_STLC1000_NO_ERROR) // riporto eventualmente l'errore che si verifica sulla ventola destra
							{
								try
								{
									ret_code = STLC1KSetPWMLeftFan(Thresholds[i-1].PWMLeftFan);
								}
								catch(...)
								{
									ret_code = SF_FUNCTION_EXCEPTION;
									AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a prima chiamata di STLC1KSetPWMLeftFan per temperatura crescente";
									SFMessage(msg,MT_ERROR,ret_code);
								}
							}
							else
							{
								try
								{
									STLC1KSetPWMLeftFan(Thresholds[i-1].PWMLeftFan);
								}
								catch(...)
								{
									ret_code = SF_FUNCTION_EXCEPTION;
									AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a seconda chiamata di STLC1KSetPWMLeftFan per temperatura crescente";
									SFMessage(msg,MT_ERROR,ret_code);
								}

							}

							//DEBUG
							msg = msg + " Nuova Soglia (" + AnsiString(CurrentStatus.ThresholdTemperature) +" �C); velocit� Ventola Dx � " +
							AnsiString(Thresholds[i-1].PWMRightFan) + "%; velocit� Ventola Sx � " + AnsiString(Thresholds[i-1].PWMLeftFan) + "%";
							SFFanDebug(msg,2120);
							//
						}
						i=0; //esco da for
					}
				} // end for
			}
			else // la temperatura sta scendendo
			{
				for(i=0; i<NumThresholds; i++)
				{
					if (temperature <= Thresholds[i].Temperature)
					{
						if (CurrentStatus.ThresholdTemperature != Thresholds[i].Temperature)
						{
							//DEBUG
							msg  = "La temperatura CALANTE rilevata (" + AnsiString(temperature) +" �C) � uscita dall'intervallo di soglia (" +
							AnsiString(CurrentStatus.ThresholdTemperature) + "�C +- " + AnsiString(CurrentStatus.ThresholdRange) + "�C).";
							//

							CurrentStatus.ThresholdTemperature = Thresholds[i].Temperature;
							CurrentStatus.ThresholdRange = Thresholds[i].Range;

							try
							{
								ret_code = STLC1KSetPWMRightFan(Thresholds[i].PWMRightFan);
							}
							catch(...)
							{
								ret_code = SF_FUNCTION_EXCEPTION;
								AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a chiamata di STLC1KSetPWMRightFan per temperatura calante";
								SFMessage(msg,MT_ERROR,ret_code);
							}

							if (ret_code == SPV_STLC1000_NO_ERROR) // riporto eventualmente l'errore che si verifica sulla ventola destra
							{
								try
								{
									ret_code = STLC1KSetPWMLeftFan(Thresholds[i].PWMLeftFan);
								}
								catch(...)
								{
									ret_code = SF_FUNCTION_EXCEPTION;
									AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a prima chiamata di STLC1KSetPWMLeftFan per temperatura calante";
									SFMessage(msg,MT_ERROR,ret_code);
								}
							}
							else
							{
								try
								{
									STLC1KSetPWMLeftFan(Thresholds[i].PWMLeftFan);
								}
								catch(...)
								{
									ret_code = SF_FUNCTION_EXCEPTION;
									AnsiString msg  = "Eccezione in SFCheckTemperatureAndManageFans dovuta a seconda chiamata di STLC1KSetPWMLeftFan per temperatura calante";
									SFMessage(msg,MT_ERROR,ret_code);
								}
							}

							//DEBUG
							msg = msg + "Nuova soglia (" + AnsiString(CurrentStatus.ThresholdTemperature) +" �C); velocit� Ventola Dx � " +
							AnsiString(Thresholds[i].PWMRightFan) + "%; velocit� Ventola Sx � " + AnsiString(Thresholds[i].PWMLeftFan) + "%";
							SFFanDebug(msg,2130);
							//
						}
						i=NumThresholds;   // esco da for
					}
				} // end for
			}
		}
	}




	if (ret_code != SPV_STLC1000_NO_ERROR)
	{
		msg  = "Errore " + AnsiString(ret_code) + " in SFCheckTemperatureAndManageFans";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}


//==============================================================================
/// Funzione per sapere se bisogna riavviare un servizio
///
/// In caso di errore restituisce un valore non nullo. ?!?!?!?  da RIVEDERE
///
/// \date [16.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
bool SFMustRestartService(char * service)
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	AnsiString msg;
	int iServiceType;
	bool serviceIsRunning;
	bool mustRestartService = true;
	//DEBUG
	msg  = "Chiamata funzione ""SFMustRestartService"" per servizio " + AnsiString(service);
	SFUpdateDebug(msg,2230);
	//

	ret_code = SYSOpenSCManager("",NULL,SF_ACCESS_CODE); // SF_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,SF_OPEN_CODE);
		if (schService != NULL)
		{
			iServiceType = SYSGetServiceStartType(schService);

			ret_code = SYSServiceIsRunning(schService,&serviceIsRunning);

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if ((!serviceIsRunning) && (iServiceType != SYS_KERNEL_QSC_SERVICE_AUTO_START))
				{
					//DEBUG
					msg  = "Servizio " + AnsiString(service) + " non deve essere riavviato";
					SFUpdateDebug(msg,2240);
					//
					mustRestartService = false;
				}
				else
				{
					//DEBUG
					msg  = "Servizio " + AnsiString(service) + " deve essere riavviato";
					SFUpdateDebug(msg,2250);
					//
				}
			}

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (!SYSCloseService(schService))
				{
					ret_code = SYS_KERNEL_SERVICE_CLOSE_FAILURE;
				}
			}
			else
			{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
				SYSCloseService(schService);
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
			SYSCloseSCManager();
		}
	}

	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		msg  = "Errore " + AnsiString(ret_code) + " in SFMustRestartService";
		SFMessage(msg,MT_ERROR,ret_code);
	}

   //	return ret_code;
   return mustRestartService;
}



//==============================================================================
/// Funzione per caricare i file da aggiornare da un file XML
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [12.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFLoadXMLIndexFile()
{
	int i;
	bool addComponent;
	int compType;

	XML_ELEMENT * mainElement;
	XML_ELEMENT * componentElement;
	char * xmlValue;
	int ret_code = XML_NO_ERROR;

//	int ret_code = SYS_KERNEL_NO_ERROR;


	ret_code = SFOpenXMLIndexSource(AnsiString(SFGetXMLIndexPathFileName()));

	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		if (IndexSource != NULL)
		{
			mainElement = XMLResolveElement(IndexSource->root_element,"components[0]",NULL);

			if ((mainElement != NULL) && (mainElement->child != NULL))
			{
				NumComponents = 0;
				// Cerco gli elementi XML figli
				componentElement = mainElement->child;
				while (componentElement)
				{
					xmlValue = XMLGetValue(componentElement,"service");
					if (xmlValue != NULL)
					{
						compType = CTService;
					}
					else
					{
						xmlValue = XMLGetValue(componentElement,"application");
						if (xmlValue != NULL)
						{
							compType = CTApplication;
						}
						else
						{
							compType = CTUndefined;
						}
					}

					if (compType != CTUndefined)
					{
						addComponent = true;
						for (i = 0; i < NumComponents; i++)
						{
							if (Components[i].Name == xmlValue)
							{
								addComponent = false;
								i = NumComponents;
							}
						}

						if (addComponent)
						{
							NumComponents++;
							Components[NumComponents-1].Name = xmlValue;
							Components[NumComponents-1].ComponentType = compType;
							if (compType == CTService)
							{
								Components[NumComponents-1].Restart = SFMustRestartService(xmlValue);
							}
							else
							{
								Components[NumComponents-1].Restart = false;
							}
						}
					}

					// Passo all'elemento successivo (se esiste)
					componentElement = componentElement->next;
				}
			}
		}
	}
	return ret_code;
}

//==============================================================================
/// Funzione per arrestare un servizio
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
int SFStopService(char * service)
{
	int ret_code = SYS_KERNEL_NO_ERROR;
	SC_HANDLE schService = NULL;
	bool serviceIsStopped;
	AnsiString msg;


	//DEBUG
	msg  = "Chiamata funzione ""SFStopService"" per servizio " + AnsiString(service);
	SFUpdateDebug(msg,2220);
	//

	ret_code = SYSOpenSCManager("",NULL,SF_ACCESS_CODE); // SF_ACCESS_CODE: accesso con diverse restrizioni
	if (ret_code == SYS_KERNEL_NO_ERROR)
	{
		schService = SYSOpenService(service,SF_OPEN_CODE);
		if (schService != NULL)
		{
			ret_code = SYSServiceIsStopped(schService,&serviceIsStopped);

			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (serviceIsStopped)
				{
					msg = "Servizio " + AnsiString(service) + " � stato trovato gi� arrestato";
					SFMessage(msg,MT_INFO,COD_SERVICE_ALREADY_STOPPED);
				}
				else
				{
					ret_code = SYSStopService(schService,5000);
					if (ret_code == SYS_KERNEL_NO_ERROR)
					{
						msg = "Servizio " + AnsiString(service) + " arrestato per aggiornamento";
						SFMessage(msg,MT_INFO,COD_SERVICE_STOPPED_FOR_UPDATE);
					}
					else
					{
						msg  = "Servizio " + AnsiString(service) + " non arrestato. Errore : " + AnsiString(ret_code) + " in fase di arresto per aggiornamento";
						SFMessage(msg,MT_ERROR,ret_code);
					}
				}
			}
			if (ret_code == SYS_KERNEL_NO_ERROR)
			{
				if (!SYSCloseService(schService))
				{
					ret_code = SYS_KERNEL_SERVICE_CLOSE_FAILURE;
				}
			}
			else
			{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
				SYSCloseService(schService);
			}
		}
		else
		{
			ret_code = SYS_KERNEL_SERVICE_OPEN_FAILURE;
		}

		if (ret_code == SYS_KERNEL_NO_ERROR)
		{
			ret_code = SYSCloseSCManager();
		}
		else
		{   // se anche fallisse devo riportare prima l'errore che gi� c'� a monte
			SYSCloseSCManager();
		}
	}

	if (ret_code != SYS_KERNEL_NO_ERROR)
	{
		msg  = "Errore " + AnsiString(ret_code) + " in SFStopService";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}



//==============================================================================
/// Funzione per la gestione dell'aggiornamento dei file dell'STLC1000
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [13.04.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSTLCFileUpdate()
{
	XML_ELEMENT * mainElement;
	XML_ELEMENT * componentElement;
	char * xmlValue;
	int i;
	int ret_code = XML_NO_ERROR;

	//Arresto dei servizi e dei processi interessati all'aggiornamento
	for(i=0; i<NumComponents; i++)
	{
		if (Components[i].ComponentType == CTService)
		{
			ret_code = SFStopService(Components[i].Name);
		}
		else
		{
			// ret_code = SFStopProcess(Components[i].Name); DA IMPLEMENTARE
		}
	} // end for


	if (IndexSource != NULL)
	{
		mainElement = XMLResolveElement(IndexSource->root_element,"components[0]",NULL);

		if ((mainElement != NULL) && (mainElement->child != NULL))
		{
			// Cerco gli elementi XML figli
			componentElement = mainElement->child;
			while (componentElement)
			{
				xmlValue = XMLGetValue(componentElement,"application");
				if (xmlValue != NULL)
				{

				}

	  //			ret_code = SFCheckAndRestartService(XMLGetValue(itemElement,"name"),&serviceState);

				// Passo all'elemento successivo (se esiste)
				componentElement = componentElement->next;
			}
		}

	}
}

//==============================================================================
/// Funzione per il Set del WatchDog
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSTLCSetWatchDog( unsigned int timeout, unsigned int delay )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	try
	{
		ret_code = STLC1KSetWatchDog(timeout,0);
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFSTLCSetWatchDog";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	return ret_code;
}


//==============================================================================
/// Funzione per il Trigger del WatchDog
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSTLCTriggerWatchDog( void )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	try
	{
		ret_code = STLC1KTriggerWatchDog();
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFSTLCTriggerWatchDog";
		SFMessage(msg,MT_ERROR,ret_code);
	}


	return ret_code;
}


//==============================================================================
/// Funzione per accendere/spegnere uno dei due led di sistema
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSTLCSetSystemLED( int led, bool on )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	if (IsWatchDogLEDActive)
	{
		try
		{
			ret_code = STLC1KSetSystemLED(led,on);
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFSTLCSetSystemLED";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	return ret_code;
}


//==============================================================================
/// Funzione per regolare ugualmente la velocit� di rotazione di entrambe le
///ventole dell'STLC1000
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [05.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int SFSTLCSetPWMFans( int pwmPercent )
{
	int ret_code = SYS_KERNEL_NO_ERROR;

	// Regolazione ventola destra
	try
	{
		ret_code = STLC1KSetPWMRightFan(pwmPercent);
	}
	catch(...)
	{
		ret_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "Eccezione in SFSTLCSetPWMFans dovuta a chiamata di STLC1KSetPWMRightFan";
		SFMessage(msg,MT_ERROR,ret_code);
	}

	// Regolazione ventola sinistra
	if (ret_code == SPV_STLC1000_NO_ERROR) // riporto eventualmente l'errore che si verifica sulla ventola destra
	{
		try
		{
			ret_code = STLC1KSetPWMLeftFan(pwmPercent);
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFSTLCSetPWMFans dovuta a prima chiamata di STLC1KSetPWMLeftFan";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}
	else
	{
		try
		{
			STLC1KSetPWMLeftFan(pwmPercent);
		}
		catch(...)
		{
			ret_code = SF_FUNCTION_EXCEPTION;
			AnsiString msg  = "Eccezione in SFSTLCSetPWMFans dovuta a seconda chiamata di STLC1KSetPWMLeftFan";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	return ret_code;
}




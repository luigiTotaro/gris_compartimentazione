//==============================================================================
// Telefin ManagerThreads Module 1.0
//------------------------------------------------------------------------------
// MODULO DEI THREAD (ManagerThreads.cpp)
// Progetto:  	Telefin STLCManager 1.0
//
// Revisione:  	0.03 (29.03.2007 -> 27.03.2008)
//
// Copyright:	2004-2008 Telefin S.p.A.
// Ambiente:	Borland C++ Builder 6, C++ Builder 2006, C++ Builder 2007
// Autore:		Enrico Alborali (alborali@telefin.it)
//				Mario Ferro (mferro@deltasistemi.it)
// Note:      	richiede ManagerThreads.h
//------------------------------------------------------------------------------
// Version history: vedere in ManagerThreads.h
//==============================================================================


#pragma hdrstop
#include "SYSKernel.h"
#include "ManagerThreads.h"
#include "MainService.h"
#include "ServiceFunctions.h"
#include "UtilityLibrary.h"
#include "SPVSTLC1000.h"
#include "fbwfapi.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//==============================================================================
// Variabili Globali
//------------------------------------------------------------------------------
static SYS_THREAD * WatchDogThread;
static SYS_THREAD * ServiceThread;
static SYS_THREAD * FanThread;
DWORD lastCheckServiceDebugTime = 0l ; // Unix Time
DWORD lastCheckFanDebugTime = 0l ; // Unix Time
DWORD lastCheckTemperatureTime = 0l ; // Unix Time

//==============================================================================
/// Funzione per inizializzare il thread per la gestione del WatchDog
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [03.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int WatchDogThreadInit( void )
{
	int ret_code = SRV_WATCH_DOG_THREAD_NO_ERROR;

	WatchDogThread = SYSNewThread( WatchDogThreadActiveFunction, 0, CREATE_SUSPENDED ); // Alloco il thread principale
	if ( WatchDogThread != NULL )
	{
		ret_code = SYSSetThreadParameter( WatchDogThread, (void*) WatchDogThread ); // Gli passo come parametro la procedura
		if ( ret_code == SRV_WATCH_DOG_THREAD_NO_ERROR )
		{
			ret_code = SYSCreateThread( WatchDogThread );
		}
	}
	else
	{
		ret_code = SRV_WATCH_DOG_THREAD_ALLOCATION_FAILURE;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per inizializzare il thread per la gestione dei servizi
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [29.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int ServiceThreadInit( void )
{
	int ret_code = SRV_SERVICE_THREAD_NO_ERROR;

	ServiceThread = SYSNewThread( ServiceThreadActiveFunction, 0, CREATE_SUSPENDED ); // Alloco il thread principale
	if ( ServiceThread != NULL )
	{
		ret_code = SYSSetThreadParameter( ServiceThread, (void*) ServiceThread ); // Gli passo come parametro la procedura
		if ( ret_code == SRV_SERVICE_THREAD_NO_ERROR )
		{
			ret_code = SYSCreateThread( ServiceThread );
		}
	}
	else
	{
		ret_code = SRV_SERVICE_THREAD_ALLOCATION_FAILURE;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per inizializzare il thread per la gestione delle ventole
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [29.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int FanThreadInit( void )
{
	int ret_code = SRV_FAN_THREAD_NO_ERROR;

	FanThread = SYSNewThread( FanThreadActiveFunction, 0, CREATE_SUSPENDED ); // Alloco il thread principale
	if ( FanThread != NULL )
	{
		ret_code = SYSSetThreadParameter( FanThread, (void*) FanThread ); // Gli passo come parametro la procedura
		if ( ret_code == SRV_FAN_THREAD_NO_ERROR )
		{
			ret_code = SYSCreateThread( FanThread );
		}
	}
	else
	{
		ret_code = SRV_FAN_THREAD_ALLOCATION_FAILURE;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per inizializzare il modulo dei thread
///
/// \date [27.03.2008]
/// \author Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
void MTInit( void )
{
	int fun_code = SRV_MANAGER_THREADS_NO_ERROR;

	IsServiceDebugActive = SFIsServiceDebugActive();
	IsFanDebugActive = SFIsFanDebugActive();
	lastCheckServiceDebugTime = ULGetUnixTime();
	lastCheckFanDebugTime = ULGetUnixTime();
	lastCheckTemperatureTime = 0l;

	//Spegnimento dei led
	IsWatchDogLEDActive = true;
	STLC1KOpenIOC();
	try
	{
		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,false);
		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,false);
	}
	catch(...)
	{
		fun_code = SF_FUNCTION_EXCEPTION;
		AnsiString msg  = "MTInit: Eccezione in Fase di spegnimento dei LED";
		SFMessage(msg,MT_ERROR,fun_code);
	}
	STLC1KCloseIOC();
	IsWatchDogLEDActive = false;

	fun_code = SFConfigInit();

	if (fun_code != SRV_MANAGER_THREADS_NO_ERROR)
	{
		AnsiString msg  = "Errore " + AnsiString(fun_code) + " in fase di Inizializzazione del modulo dei thread del servizio";
		SFMessage(msg,MT_ERROR,fun_code);
	}

	if ((NumServices > 0) || (IsWatchDogActive))
	{
		fun_code = ServiceThreadInit();
		if (fun_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(fun_code) + " in fase di Inizializzazione del thread per la gestione dei Servizi";
			SFMessage(msg,MT_ERROR,fun_code);
		}
	}
	else
	{
		SFMessage("Gestione Servizi e WatchDog non avviati",MT_INFO,COD_SERVICE_WATCHDOG_MANAGEMENT_NOT_STARTED);
	}

	if (NumThresholds > 0)
	{
		fun_code = FanThreadInit();
		if (fun_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(fun_code) + " in fase di Inizializzazione del thread per la gestione delle Ventole";
			SFMessage(msg,MT_ERROR,fun_code);
		}
	}
	else
	{
		SFMessage("Gestione Ventole non avviata",MT_INFO,COD_FAN_THREAD_NOT_STARTED);
	}
}

//==============================================================================
/// Funzione per avviare i thread
///
/// \date [03.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void MTStart( void )
{
	int ret_code = SRV_MANAGER_THREADS_NO_ERROR;

	if (ServiceThread != NULL)
	{
		ret_code = SYSResumeThread( ServiceThread );
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di avvio del thread della gestione dei Servizi";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	if (FanThread != NULL)
	{
		ret_code = SYSResumeThread( FanThread );
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di avvio del thread della gestione delle Ventole";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}
}

//==============================================================================
/// Funzione per arrestare i thread
///
/// \date [12.03.2008]
/// \author Mario Ferro
/// \version 0.03
//------------------------------------------------------------------------------
void MTStop( void )
{
	int ret_code = SRV_MANAGER_THREADS_NO_ERROR;
	AnsiString msg;

	if (ServiceThread != NULL)
	{
		ret_code = SYSSuspendThread( ServiceThread );
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di arresto del thread per la gestione dei Servizi";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	if (FanThread != NULL)
	{
		ret_code = SYSSuspendThread( FanThread );
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di arresto del thread per la gestione delle Ventole";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	if (IsFanActive || IsWatchDogActive)
	{
		if (IsWatchDogActive)
		{
			// Impostazione del timeout di manutenzione per il WatchDog
			ret_code = SFSTLCSetWatchDog(WatchDogMaintenance,WatchDogDelay) ;
			if (ret_code == SRV_MANAGER_THREADS_NO_ERROR)
			{
				//In fase di chiusura il Led di Sistema viene impostato di color rosso
				SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
				SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,false);
				msg = "Timeout di manutenzione del WatchDog impostato. Timeout = " + AnsiString(WatchDogMaintenance) + " ms";

				SFCloseWatchDogLogManager();
				if (IsWatchDogLEDActive)
				{
					STLC1KCloseIOC();
				}
				SFMessage(msg,MT_INFO,COD_WATCHDOG_MAINTENANCE_ACTIVACTION_SUCCESS);
			}
			else
			{
				msg  = "Impostazione timeout di manutenzione del WatchDog fallita. Errore : " + AnsiString(ret_code);
				SFMessage(msg,MT_ERROR,ret_code);
			}
		}

		if (IsFanActive)
		{
			// Accensione precauzionale delle ventole
			try
			{
				SFSTLCSetPWMFans(100);
			}
			catch(...)
			{
				ret_code = SF_FUNCTION_EXCEPTION;
				AnsiString msg  = "MTStop: Eccezione in SFSTLCSetPWMFans";
				SFMessage(msg,MT_ERROR,ret_code);
			}
		}

		ret_code = SFUnloadJIDA();
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di scaricamento della libreria JIDA";
			SFMessage(msg,MT_ERROR,ret_code);
		}
		else
		{
			SFFanDebug("Scaricamento della libreria JIDA OK.",2180);
		}
	}

	SFMessage("Arresto Servizio",MT_INFO,COD_MANAGER_SERVICE_STOPPED);
}


//==============================================================================
/// Funzione per chiudere il thread per la gestione del WatchDog
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [03.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int WatchDogThreadClear( void )
{
	int ret_code = SRV_WATCH_DOG_THREAD_NO_ERROR;

	if ( SYSValidThread( WatchDogThread ) )
	{
		ret_code = SYSTerminateThread( WatchDogThread );
	  // --- Aspetto la fine del thread ---
	  if ( SYSWaitForThread( WatchDogThread ) != 0 )
	  {
		// --- Aspetto 250 ms ---
		Sleep( 250 );
		//ret_code = SCAConfigClear( );  vedi sopra
	  }
	  // --- Elimino il thread terminato ---
	  ret_code = SYSDeleteThread( WatchDogThread );
	}
	else
	{
	  ret_code = SRV_WATCH_DOG_THREAD_INVALID_THREAD;
	}

	return ret_code;
}


//==============================================================================
/// Funzione per chiudere il thread per la gestione dei servizi
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [29.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int ServiceThreadClear( void )
{
	int ret_code = SRV_SERVICE_THREAD_NO_ERROR;

	if ( SYSValidThread( ServiceThread ) )
	{
		ret_code = SYSTerminateThread( ServiceThread );
	  // --- Aspetto la fine del thread ---
	  if ( SYSWaitForThread( ServiceThread ) != 0 )
	  {
		// --- Aspetto 250 ms ---
		Sleep( 250 );
		//ret_code = SCAConfigClear( );  vedi sopra
	  }
	  // --- Elimino il thread terminato ---
	  ret_code = SYSDeleteThread( ServiceThread );
	}
	else
	{
	  ret_code = SRV_SERVICE_THREAD_INVALID_THREAD;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere il thread per la gestione delle ventole
///
/// In caso di errore restituisce un valore non nullo.
///
/// \date [29.03.2007]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
int FanThreadClear( void )
{
	int ret_code = SRV_FAN_THREAD_NO_ERROR;

	if ( SYSValidThread( FanThread ) )
	{
	  ret_code = SYSTerminateThread( FanThread );
	  // --- Aspetto la fine del thread ---
	  if ( SYSWaitForThread( FanThread ) != 0 )
	  {
		// --- Aspetto 250 ms ---
		Sleep( 250 );
		//ret_code = SCAConfigClear( );  vedi sopra
	  }
	  // --- Elimino il thread terminato ---
	  ret_code = SYSDeleteThread( FanThread );
	}
	else
	{
	  ret_code = SRV_FAN_THREAD_INVALID_THREAD;
	}

	return ret_code;
}

//==============================================================================
/// Funzione per chiudere il modulo dei thread
///
/// \date [03.03.2008]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
void MTClear( void )
{
	int ret_code = SRV_MANAGER_THREADS_NO_ERROR;

	if (ServiceThread != NULL)
	{
		ret_code = ServiceThreadClear();
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di chiusura del thread per la gestione dei Servizi";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

	if (FanThread != NULL)
	{
		ret_code = FanThreadClear();
		if (ret_code != SRV_MANAGER_THREADS_NO_ERROR)
		{
			AnsiString msg  = "Errore " + AnsiString(ret_code) + " in fase di chiusura del thread per la gestione delle Ventole";
			SFMessage(msg,MT_ERROR,ret_code);
		}
	}

}


//==============================================================================
/// Funzione di attivazione per il thread per la gestione del WatchDog
///
/// \date [04.03.2008]
/// \author Mario Ferro
/// \version 0.01
//------------------------------------------------------------------------------
unsigned long __stdcall WatchDogThreadActiveFunction( void * parameter )
{
	unsigned long ret_code = SRV_FUNCTION_NOT_IMPLEMENTED; // Codice di ritorno
	return ret_code;
}

unsigned long _FBWFCheckMemoryUsage(ULONG maxMemPerc)
{
	ULONG           ret_code;
	FbwfMemoryUsage usage;
	AnsiString msg;
	ULONG	fbwf_mem_perc;

	ret_code = FbwfGetMemoryUsage(&usage);

	if (ret_code != NO_ERROR)
	{
		// Impossibile ottenere informazioni
		msg = "FBWF Memory Usage:\nImpossibile ottenere informazioni";
		SFMessage(msg,MT_WARNING,SRV_FBWFAPI_NOT_AVAILABLE);
	}
	else
	{
		// Valuto le informazioni ricevute (usage)
		fbwf_mem_perc = (ULONG)(((double)usage.fileData/(double)usage.currCacheThreshold)*100);

		if (fbwf_mem_perc > maxMemPerc) { // FBWF Failure
			IsFBWFFailure = true;
			msg = "Superata soglia massima di memoria utilizzata ("+ String(fbwf_mem_perc) +"% rispetto a "+ String(maxMemPerc) +"%)"
				+ "\n\nFBWF Memory Usage:"
				+ String("\ncurrCacheThreshold: ") + String(usage.currCacheThreshold)
				+ String("\nnextCacheThreshold: ") + String(usage.nextCacheThreshold)
				+ String("\ndirStructure: ") + String(usage.dirStructure)
				+ String("\nfileData: ") + String(usage.fileData);
			SFMessage(msg,MT_ERROR,SRV_FBWFAPI_FAILURE);
		}
		else
		{
			/*
			msg = "Soglia massima di memoria utilizzata non superata ("+ String(fbwf_mem_perc) +"% rispetto a "+ String(maxMemPerc) +"%)"
				+ "\n\nFBWF Memory Usage:"
				+ String("\ncurrCacheThreshold: ") + String(usage.currCacheThreshold)
				+ String("\nnextCacheThreshold: ") + String(usage.nextCacheThreshold)
				+ String("\ndirStructure: ") + String(usage.dirStructure)
				+ String("\nfileData: ") + String(usage.fileData);
			SFMessage(msg,MT_WARNING,SRV_FBWFAPI_FAILURE);
			*/
		}
	}

	return ret_code;
}

//==============================================================================
/// Funzione di attivazione per il thread per la gestione dei servizi
///
/// \date [22.04.2010]
/// \author Mario Ferro, Enrico Alborali
/// \version 1.01
//------------------------------------------------------------------------------
unsigned long __stdcall ServiceThreadActiveFunction( void * parameter )
{
	unsigned long           ret_code        = SRV_SERVICE_THREAD_NO_ERROR; // Codice di ritorno
	SYS_THREAD            * thread          = NULL                    ; // Puntatore al thread
	int i = 0;
	AnsiString msg;
	long startThreadTime = 0l;

	if ( parameter != NULL ) // Controllo il parametro passato
	{
		thread = (SYS_THREAD*) parameter; // Recupero il thread
		if ( thread != NULL ) // Se la procedura � valida
		{
			try
			{
				// memorizzazione ora di partenza del thread
				startThreadTime = ULGetUnixTime();

				//In fase di avvio del thread il Led di Sistema viene impostato di color giallo
				SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,true);
				SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
				Sleep(1000); // per vedere il led giallo

				if (!IsMaintenanceActive)
				{
					if (NumServices > 0)
					{
						SFMessage("Attivata gestione Servizi",MT_INFO,COD_SERVICE_MANAGEMENT_STARTED);
					}
					else
					{
						SFMessage("Gestione Servizi non attivata",MT_INFO,COD_SERVICE_MANAGEMENT_NOT_STARTED);
					}

					if (IsWatchDogActive)
					{
						SFMessage("Attivata gestione WatchDog",MT_INFO,COD_WATCHDOG_MANAGEMENT_STARTED);
					}
					else
					{
						SFMessage("Gestione WatchDog non attivata",MT_INFO,COD_WATCHDOG_MANAGEMENT_NOT_STARTED);
					}
				}
				else
				{
					SFMessage("Avvio manutenzione triggerata. Tempo massimo prima del riavvio = " + AnsiString(WatchDogMaintenance+WatchDogTimeOut) + " ms",MT_INFO,COD_WATCHDOG_MAINTENANCE_STARTED);
				}

				while ( thread->Terminated == false ) // Ciclo del thread
				{
					//Ad intervalli di 5 minuti verifico se la modalit� di debug � attiva o meno
					if ((ULGetUnixTime() -  lastCheckServiceDebugTime) >= SRV_CHECK_DEBUG_TIMEOUT)
					{
						IsServiceDebugActive = SFIsServiceDebugActive();
						lastCheckServiceDebugTime = ULGetUnixTime();
					}

					if (!IsMaintenanceActive)
					{
						// Ciclo di controllo dei servizi configurati
						for(i = 0; i < NumServices; i++)
						{
							// se per il servizio � previsto il controllo che non sia Stopped
							if (Services[i].CheckStartTimeout > 0)
							{
								if ((ULGetUnixTime() - Services[i].LastCheckStartTime) >= Services[i].CheckStartTimeout)
								{
									ret_code = SFCheckServiceIsStoppedAndRestart(Services[i].Name);
									// Service Failure
									if (ret_code != SYS_KERNEL_NO_ERROR)
									{
										ServiceFailureCount++;
										msg  = "Service Failure. Impossibile avviare il servizio " + String(Services[i].Name);
										SFMessage(msg,MT_WARNING,SRV_SERVICE_FAILURE);
									}
									Services[i].LastCheckStartTime = ULGetUnixTime();
								}
							}

							// se per il servizio � previsto il controllo che non sia bloccato
							if (Services[i].AutocheckRestartTimeout > 0)
							{
								if ((ULGetUnixTime() - Services[i].LastAutocheckRestartTime) >= Services[i].AutocheckRestartTimeout)
								{
									ret_code = SFCheckServiceIsBlockedAndRestart(Services[i].Name,Services[i].AutocheckRestartTimeout);
									// Service Failure
									if (ret_code != SYS_KERNEL_NO_ERROR)
									{
										ServiceFailureCount++;
										msg  = "Service Failure. Impossibile riavviare il servizio " + String(Services[i].Name);
										SFMessage(msg,MT_WARNING,SRV_SERVICE_FAILURE);
									}
									Services[i].LastAutocheckRestartTime = ULGetUnixTime();
								}
							}

							// se per il servizio � previsto riavvio forzato
							if (Services[i].ForcedRestartTimeout > 0)
							{
								if ((ULGetUnixTime() - Services[i].LastForcedRestartTime) >= Services[i].ForcedRestartTimeout)
								{
									ret_code = SFServiceForcedRestart(Services[i].Name);
									// Service Failure
									if (ret_code != SYS_KERNEL_NO_ERROR)
									{
										ServiceFailureCount++;
										msg  = "Service Failure. Impossibile riavviare in modo forzato il servizio " + String(Services[i].Name);
										SFMessage(msg,MT_WARNING,SRV_SERVICE_FAILURE);
									}
									Services[i].LastForcedRestartTime = ULGetUnixTime();
								}
							}

						} // end for

						// FBWF Check
						if (IsFBWFActive)
						{
							_FBWFCheckMemoryUsage(FBWFMaxMemoryPerc);
							Sleep(250);
						}

							// Controllo System Failure
							if (ServiceFailureCount > 0 || IsFBWFFailure) {// Attualmente la soglia minima � 1 perci� al primo service failure sono in system failure
					    		IsSystemFailure = true; // Il sistema sara' riavviato perche' il watch dog non verra' triggerato

					    		// Imposto il LED di sistema a rosso
					    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
					    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,false);

								msg  = "System Failure. Trigger del WatchDog non effettuato.";
					    		SFMessage(msg,MT_ERROR,SRV_SYSTEM_FAILURE);

								if (RebootOnFailure) {
									system("shutdown -r -f -t 5");
									msg  = "Arresto del sistema tra 5 secondi.";
									SFMessage(msg,MT_WARNING,SRV_SYSTEM_REBOOT);

									Sleep( 10000 );
								}
					    	}
					}

					if ((!IsMaintenanceActive) ||
						((IsMaintenanceActive) && (((ULGetUnixTime()-startThreadTime) * 1000) < WatchDogMaintenance)))
					{
						if (IsWatchDogActive)
						{
							if (IsSystemFailure) // Gestisco il caso System Failure
							{
								// Non effettuo il trigger del watch dog perche' il sistema deve riavviarsi!
								Sleep(250);
							}
							else
							{
								// Trigger del WatchDog
								ret_code = SFSTLCTriggerWatchDog();
								if (ret_code == SRV_SERVICE_THREAD_NO_ERROR)
								{
									SFAddLogTriggerTime();

									//In fase di trigger corretto il Led di Sistema viene impostato di color verde
									SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,false);
									SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,true);

									Sleep(250);

							    	if (!IsMaintenanceActive)
							    	{
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,false);
							    	}
							    	else
							    	{
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,true);
							    	}
							    }
							    else
							    {
							    	if (!IsMaintenanceActive)
							    	{
							    		//Se il trigger fallisce il Led di Sistema viene impostato di color giallo
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,true);
							    	}
							    	else
							    	{
							    		//Se il trigger fallisce in manutenzione il Led di Sistema viene impostato di color rosso
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,false);
							    	}

							    	Sleep(250);
							    	if (!IsMaintenanceActive)
							    	{
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,false);
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,false);
							    	}
							    	else
							    	{
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_RED_LED,true);
							    		SFSTLCSetSystemLED(SPV_STLC1000_SYSTEM_GREEN_LED,true);
							    	}

							    	msg =  "WatchDog ";
							    	if (IsMaintenanceActive)
							    	{
							    		msg = msg + "di manutenzione ";
							    	}
							    	msg  = "Trigger del" + msg + "fallito. Errore : " + AnsiString(ret_code);
							    	SFMessage(msg,MT_ERROR,ret_code);
							    }
							}
						}
					}

					Sleep(5000); // Rilascio le risorse.
			
				} // end while
			}
			catch (...)
			{
				ret_code = SRV_SERVICE_THREAD_CRITICAL_ERROR;
			}
		}
	}
	return ret_code;
}


//==============================================================================
/// Funzione di attivazione per il thread per la gestione delle ventole
///
/// \date [03.03.2007]
/// \author Mario Ferro
/// \version 0.02
//------------------------------------------------------------------------------
unsigned long __stdcall FanThreadActiveFunction( void * parameter )
{
	unsigned long           ret_code        = SRV_FAN_THREAD_NO_ERROR ; // Codice di ritorno
	SYS_THREAD            * thread          = NULL                    ; // Puntatore al thread
	int i = 0;
	AnsiString msg;

	if ( parameter != NULL ) // Controllo il parametro passato
	{
		thread = (SYS_THREAD*) parameter; // Recupero il thread
		if ( thread != NULL ) // Se la procedura � valida
		{
			try
			{
				SFMessage("Avvio thread gestione Ventole",MT_INFO,COD_FAN_THREAD_STARTED);

				while ( thread->Terminated == false ) // Ciclo del thread
				{
					//Ad intervalli di 5 minuti verifico se la modalit� di debug � attiva o meno
					if ((ULGetUnixTime() -  lastCheckFanDebugTime) >= SRV_CHECK_DEBUG_TIMEOUT)
					{
						IsFanDebugActive = SFIsFanDebugActive();
						lastCheckFanDebugTime = ULGetUnixTime();
					}

					//Ad intervalli di un numero di minuti configurabili attivo la gestione delle ventole
					if ((ULGetUnixTime() -  lastCheckTemperatureTime) >= TemperatureCheckDelay)
					{
						SFCheckTemperatureAndManageFans();
						lastCheckTemperatureTime = ULGetUnixTime();
					}


					Sleep(1000); // Rilascio le risorse.

				} // end while
			}
			catch (...)
			{
				ret_code = SRV_FAN_THREAD_CRITICAL_ERROR;
			}
		}
	}
	return ret_code;
}


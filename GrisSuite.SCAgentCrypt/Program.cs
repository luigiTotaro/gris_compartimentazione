using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Reflection;
using System.IO;

using System.Text;

namespace GrisSuite.SCAgentCrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                WriteSyntax();
            }
            else if (args[0].ToLower() == "crypt")
            {
                try
                {
                    string sConfig = args[1];
                    if (File.Exists(sConfig))
                    {
                        ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();
                        configFile.ExeConfigFilename = sConfig;
                        Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
                        ConfigurationSection confSection = configuration.SectionGroups["applicationSettings"].Sections["GrisSuite.WebServiceAccount"];
                        if (!confSection.SectionInformation.IsProtected)
                        {
                            confSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                        }
                        confSection.SectionInformation.ForceSave = true;
                        configuration.Save(ConfigurationSaveMode.Modified);
                        Console.WriteLine("Session crypted");
                    }
                    else
                    {
                        Console.WriteLine("File not found:" + sConfig);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unaspected error:" + ex.Message);    
                }
                
            }
            else if (args[0].ToLower() == "decrypt")
            {
                try
                {
                    string sConfig = args[1];
                    if (File.Exists(sConfig))
                    {
                        ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();
                        configFile.ExeConfigFilename = sConfig;
                        Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
                        ConfigurationSection confSection = configuration.SectionGroups["applicationSettings"].Sections["GrisSuite.WebServiceAccount"];
                        if (confSection.SectionInformation.IsProtected)
                        {
                            confSection.SectionInformation.UnprotectSection();
                        }
                        confSection.SectionInformation.ForceSave = true;
                        configuration.Save(ConfigurationSaveMode.Modified);
                        Console.WriteLine("Session decrypted");
                    }
                    else
                    {
                        Console.WriteLine("File not found:" + sConfig);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unaspected error:" + ex.Message);    
                }
            }
            else
            {
                WriteSyntax();
            }
        }

        private static void  WriteSyntax()
        {
            Console.WriteLine("Telefin SCAgent Section Crypt");
            Console.WriteLine("Syntax: SCACRYPT crypt file.config");
            Console.WriteLine("Syntax: SCACRYPT decrypt file.config");

        }
    }
}

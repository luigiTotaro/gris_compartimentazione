using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GrisSuite;

namespace GrisSuite.LogViewer
{
    public partial class MsgViewer : UserControl
    {
        private DataSet _message = new DataSet();

        public DataSet Message
        {
            get { return _message; }
            set 
            { 
                _message = value;
                Rebind();
            }
        }

        public MsgViewer()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_message != null && listBox1.SelectedIndex >= 0)
            {
                binding.DataSource = _message.Tables[listBox1.SelectedItem.ToString()];
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.AutoGenerateColumns = true;
                for(int j = dataGridView1.Columns.Count -1; j >= 0; j--)
                {
                    DataGridViewColumn col = dataGridView1.Columns[j];
                    if (col.ValueType == typeof(byte[]))
                        dataGridView1.Columns.RemoveAt(j);
                    
                }
                dataGridView1.Refresh();
            }
        }

        private void Rebind()
        {
            try
            {
                listBox1.Items.Clear();
                if (_message != null)
                {
                    foreach (DataTable dt in _message.Tables)
                    {
                        listBox1.Items.Add(dt.TableName);
                    }
                    listBox1.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error:{0}", ex.Message));
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

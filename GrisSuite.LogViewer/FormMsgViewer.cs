using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GrisSuite
{
    public partial class FormMsgViewer : Form
    {
        public FormMsgViewer()
        {
            InitializeComponent();
        }

        public DataSet Message
        {
            get 
            {
                return msgViewer1.Message;
            }
            set 
            {
                msgViewer1.Message = value;
            }
        }
    }
}
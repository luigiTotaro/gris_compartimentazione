namespace GrisSuite.LogViewer
{
    partial class LogViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogViewer));
            this.panButtons = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkIsAlive = new System.Windows.Forms.CheckBox();
            this.chkSTLCParameters = new System.Windows.Forms.CheckBox();
            this.chkConfig = new System.Windows.Forms.CheckBox();
            this.chkEvents = new System.Windows.Forms.CheckBox();
            this.chkDeviceStatus = new System.Windows.Forms.CheckBox();
            this.txtMAC = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHostReceiver = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMinMessageSize = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTop = new System.Windows.Forms.TextBox();
            this.txtHostSender = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLogId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.datTo = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.datFrom = new System.Windows.Forms.DateTimePicker();
            this.btnApplyFilter = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.navigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.binding = new System.Windows.Forms.BindingSource(this.components);
            this.dsLog1 = new GrisSuite.Data.dsLog();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.panContent = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.logIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MACdataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hostReceiverDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hostSenderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbnViewMessage = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panButtons.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navigator)).BeginInit();
            this.navigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.binding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsLog1)).BeginInit();
            this.panContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panButtons
            // 
            this.panButtons.Controls.Add(this.groupBox1);
            this.panButtons.Controls.Add(this.txtMAC);
            this.panButtons.Controls.Add(this.label8);
            this.panButtons.Controls.Add(this.txtHostReceiver);
            this.panButtons.Controls.Add(this.label7);
            this.panButtons.Controls.Add(this.txtMinMessageSize);
            this.panButtons.Controls.Add(this.label6);
            this.panButtons.Controls.Add(this.txtTop);
            this.panButtons.Controls.Add(this.txtHostSender);
            this.panButtons.Controls.Add(this.label5);
            this.panButtons.Controls.Add(this.label4);
            this.panButtons.Controls.Add(this.txtLogId);
            this.panButtons.Controls.Add(this.label3);
            this.panButtons.Controls.Add(this.label2);
            this.panButtons.Controls.Add(this.datTo);
            this.panButtons.Controls.Add(this.label1);
            this.panButtons.Controls.Add(this.datFrom);
            this.panButtons.Controls.Add(this.btnApplyFilter);
            this.panButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panButtons.Location = new System.Drawing.Point(0, 0);
            this.panButtons.Name = "panButtons";
            this.panButtons.Size = new System.Drawing.Size(780, 140);
            this.panButtons.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkIsAlive);
            this.groupBox1.Controls.Add(this.chkSTLCParameters);
            this.groupBox1.Controls.Add(this.chkConfig);
            this.groupBox1.Controls.Add(this.chkEvents);
            this.groupBox1.Controls.Add(this.chkDeviceStatus);
            this.groupBox1.Location = new System.Drawing.Point(560, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(132, 123);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MessageTypes";
            // 
            // chkIsAlive
            // 
            this.chkIsAlive.AutoSize = true;
            this.chkIsAlive.Location = new System.Drawing.Point(6, 19);
            this.chkIsAlive.Name = "chkIsAlive";
            this.chkIsAlive.Size = new System.Drawing.Size(57, 17);
            this.chkIsAlive.TabIndex = 7;
            this.chkIsAlive.Tag = "IsAlive";
            this.chkIsAlive.Text = "IsAlive";
            this.chkIsAlive.UseVisualStyleBackColor = true;
            // 
            // chkSTLCParameters
            // 
            this.chkSTLCParameters.AutoSize = true;
            this.chkSTLCParameters.Location = new System.Drawing.Point(6, 99);
            this.chkSTLCParameters.Name = "chkSTLCParameters";
            this.chkSTLCParameters.Size = new System.Drawing.Size(117, 17);
            this.chkSTLCParameters.TabIndex = 11;
            this.chkSTLCParameters.Tag = "GrisSuite.Data.dsSTLCParameters";
            this.chkSTLCParameters.Text = "dsSTLCParameters";
            this.chkSTLCParameters.UseVisualStyleBackColor = true;
            // 
            // chkConfig
            // 
            this.chkConfig.AutoSize = true;
            this.chkConfig.Location = new System.Drawing.Point(6, 39);
            this.chkConfig.Name = "chkConfig";
            this.chkConfig.Size = new System.Drawing.Size(67, 17);
            this.chkConfig.TabIndex = 8;
            this.chkConfig.Tag = "GrisSuite.Data.dsConfig";
            this.chkConfig.Text = "dsConfig";
            this.chkConfig.UseVisualStyleBackColor = true;
            // 
            // chkEvents
            // 
            this.chkEvents.AutoSize = true;
            this.chkEvents.Location = new System.Drawing.Point(6, 79);
            this.chkEvents.Name = "chkEvents";
            this.chkEvents.Size = new System.Drawing.Size(70, 17);
            this.chkEvents.TabIndex = 9;
            this.chkEvents.Tag = "GrisSuite.Data.dsEvents";
            this.chkEvents.Text = "dsEvents";
            this.chkEvents.UseVisualStyleBackColor = true;
            // 
            // chkDeviceStatus
            // 
            this.chkDeviceStatus.AutoSize = true;
            this.chkDeviceStatus.Location = new System.Drawing.Point(6, 59);
            this.chkDeviceStatus.Name = "chkDeviceStatus";
            this.chkDeviceStatus.Size = new System.Drawing.Size(101, 17);
            this.chkDeviceStatus.TabIndex = 10;
            this.chkDeviceStatus.Tag = "GrisSuite.Data.dsDeviceStatus";
            this.chkDeviceStatus.Text = "dsDeviceStatus";
            this.chkDeviceStatus.UseVisualStyleBackColor = true;
            // 
            // txtMAC
            // 
            this.txtMAC.Location = new System.Drawing.Point(57, 73);
            this.txtMAC.Name = "txtMAC";
            this.txtMAC.Size = new System.Drawing.Size(200, 20);
            this.txtMAC.TabIndex = 21;
            this.txtMAC.TextChanged += new System.EventHandler(this.txtMAC_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "MAC:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtHostReceiver
            // 
            this.txtHostReceiver.Location = new System.Drawing.Point(348, 11);
            this.txtHostReceiver.Name = "txtHostReceiver";
            this.txtHostReceiver.Size = new System.Drawing.Size(200, 20);
            this.txtHostReceiver.TabIndex = 19;
            this.txtHostReceiver.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(271, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Host Receiver:";
            // 
            // txtMinMessageSize
            // 
            this.txtMinMessageSize.Location = new System.Drawing.Point(348, 73);
            this.txtMinMessageSize.Name = "txtMinMessageSize";
            this.txtMinMessageSize.Size = new System.Drawing.Size(200, 20);
            this.txtMinMessageSize.TabIndex = 17;
            this.txtMinMessageSize.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(271, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Min MsgSize:";
            // 
            // txtTop
            // 
            this.txtTop.Location = new System.Drawing.Point(348, 104);
            this.txtTop.Name = "txtTop";
            this.txtTop.Size = new System.Drawing.Size(200, 20);
            this.txtTop.TabIndex = 15;
            this.txtTop.Text = "1000";
            // 
            // txtHostSender
            // 
            this.txtHostSender.Location = new System.Drawing.Point(348, 42);
            this.txtHostSender.Name = "txtHostSender";
            this.txtHostSender.Size = new System.Drawing.Size(200, 20);
            this.txtHostSender.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(271, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Max Records:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(271, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Host Sender:";
            // 
            // txtLogId
            // 
            this.txtLogId.Location = new System.Drawing.Point(57, 104);
            this.txtLogId.Name = "txtLogId";
            this.txtLogId.Size = new System.Drawing.Size(200, 20);
            this.txtLogId.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Log ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Data a:";
            // 
            // datTo
            // 
            this.datTo.Location = new System.Drawing.Point(57, 42);
            this.datTo.Name = "datTo";
            this.datTo.Size = new System.Drawing.Size(200, 20);
            this.datTo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Data da:";
            // 
            // datFrom
            // 
            this.datFrom.CustomFormat = "dd/mm/yyyy hh:nn:ss";
            this.datFrom.Location = new System.Drawing.Point(57, 11);
            this.datFrom.Name = "datFrom";
            this.datFrom.Size = new System.Drawing.Size(200, 20);
            this.datFrom.TabIndex = 1;
            // 
            // btnApplyFilter
            // 
            this.btnApplyFilter.Location = new System.Drawing.Point(698, 10);
            this.btnApplyFilter.Name = "btnApplyFilter";
            this.btnApplyFilter.Size = new System.Drawing.Size(79, 116);
            this.btnApplyFilter.TabIndex = 0;
            this.btnApplyFilter.Text = "Apply Filter";
            this.btnApplyFilter.UseVisualStyleBackColor = true;
            this.btnApplyFilter.Click += new System.EventHandler(this.btnApplyFilter_Click);
            // 
            // splitter1
            // 
            this.splitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 140);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(780, 3);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // navigator
            // 
            this.navigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.navigator.BindingSource = this.binding;
            this.navigator.CountItem = this.bindingNavigatorCountItem;
            this.navigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.navigator.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.navigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.navigator.Location = new System.Drawing.Point(0, 555);
            this.navigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.navigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.navigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.navigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.navigator.Name = "navigator";
            this.navigator.PositionItem = this.bindingNavigatorPositionItem;
            this.navigator.Size = new System.Drawing.Size(780, 25);
            this.navigator.TabIndex = 3;
            this.navigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // binding
            // 
            this.binding.DataMember = "LogMessage";
            this.binding.DataSource = this.dsLog1;
            // 
            // dsLog1
            // 
            this.dsLog1.DataSetName = "dsLog";
            this.dsLog1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // panContent
            // 
            this.panContent.Controls.Add(this.dataGridView1);
            this.panContent.Controls.Add(this.splitter2);
            this.panContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panContent.Location = new System.Drawing.Point(0, 143);
            this.panContent.Name = "panContent";
            this.panContent.Size = new System.Drawing.Size(780, 412);
            this.panContent.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.logIDDataGridViewTextBoxColumn,
            this.MACdataGridViewTextBoxColumn,
            this.hostReceiverDataGridViewTextBoxColumn,
            this.hostSenderDataGridViewTextBoxColumn,
            this.createdDataGridViewTextBoxColumn,
            this.messageTypeDataGridViewTextBoxColumn,
            this.cbnViewMessage});
            this.dataGridView1.DataSource = this.binding;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(777, 412);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(0, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 412);
            this.splitter2.TabIndex = 1;
            this.splitter2.TabStop = false;
            // 
            // logIDDataGridViewTextBoxColumn
            // 
            this.logIDDataGridViewTextBoxColumn.DataPropertyName = "LogID";
            this.logIDDataGridViewTextBoxColumn.FillWeight = 98.77586F;
            this.logIDDataGridViewTextBoxColumn.HeaderText = "LogID";
            this.logIDDataGridViewTextBoxColumn.Name = "logIDDataGridViewTextBoxColumn";
            // 
            // MACdataGridViewTextBoxColumn
            // 
            this.MACdataGridViewTextBoxColumn.DataPropertyName = "MAC";
            this.MACdataGridViewTextBoxColumn.FillWeight = 98.77586F;
            this.MACdataGridViewTextBoxColumn.HeaderText = "MAC";
            this.MACdataGridViewTextBoxColumn.Name = "MACdataGridViewTextBoxColumn";
            // 
            // hostReceiverDataGridViewTextBoxColumn
            // 
            this.hostReceiverDataGridViewTextBoxColumn.DataPropertyName = "HostReceiver";
            this.hostReceiverDataGridViewTextBoxColumn.FillWeight = 98.03369F;
            this.hostReceiverDataGridViewTextBoxColumn.HeaderText = "HostReceiver";
            this.hostReceiverDataGridViewTextBoxColumn.Name = "hostReceiverDataGridViewTextBoxColumn";
            // 
            // hostSenderDataGridViewTextBoxColumn
            // 
            this.hostSenderDataGridViewTextBoxColumn.DataPropertyName = "HostSender";
            this.hostSenderDataGridViewTextBoxColumn.FillWeight = 97.90037F;
            this.hostSenderDataGridViewTextBoxColumn.HeaderText = "HostSender";
            this.hostSenderDataGridViewTextBoxColumn.Name = "hostSenderDataGridViewTextBoxColumn";
            // 
            // createdDataGridViewTextBoxColumn
            // 
            this.createdDataGridViewTextBoxColumn.DataPropertyName = "Created";
            this.createdDataGridViewTextBoxColumn.FillWeight = 94.41625F;
            this.createdDataGridViewTextBoxColumn.HeaderText = "Created";
            this.createdDataGridViewTextBoxColumn.Name = "createdDataGridViewTextBoxColumn";
            // 
            // messageTypeDataGridViewTextBoxColumn
            // 
            this.messageTypeDataGridViewTextBoxColumn.DataPropertyName = "MessageType";
            this.messageTypeDataGridViewTextBoxColumn.FillWeight = 98.77586F;
            this.messageTypeDataGridViewTextBoxColumn.HeaderText = "MessageType";
            this.messageTypeDataGridViewTextBoxColumn.Name = "messageTypeDataGridViewTextBoxColumn";
            // 
            // cbnViewMessage
            // 
            this.cbnViewMessage.FillWeight = 33.32214F;
            this.cbnViewMessage.HeaderText = "View";
            this.cbnViewMessage.Name = "cbnViewMessage";
            this.cbnViewMessage.Text = "...";
            this.cbnViewMessage.UseColumnTextForButtonValue = true;
            // 
            // LogViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panContent);
            this.Controls.Add(this.navigator);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panButtons);
            this.Name = "LogViewer";
            this.Size = new System.Drawing.Size(780, 580);
            this.panButtons.ResumeLayout(false);
            this.panButtons.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navigator)).EndInit();
            this.navigator.ResumeLayout(false);
            this.navigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.binding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsLog1)).EndInit();
            this.panContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panButtons;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingNavigator navigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.Button btnApplyFilter;
        private System.Windows.Forms.BindingSource binding;
        private System.Windows.Forms.Panel panContent;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TextBox txtLogId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker datTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datFrom;
        private GrisSuite.Data.dsLog dsLog1;
        private System.Windows.Forms.CheckBox chkSTLCParameters;
        private System.Windows.Forms.CheckBox chkDeviceStatus;
        private System.Windows.Forms.CheckBox chkEvents;
        private System.Windows.Forms.CheckBox chkConfig;
        private System.Windows.Forms.CheckBox chkIsAlive;
        private System.Windows.Forms.TextBox txtHostSender;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTop;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMinMessageSize;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtHostReceiver;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMAC;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn logIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MACdataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hostReceiverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hostSenderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn cbnViewMessage;
    }
}

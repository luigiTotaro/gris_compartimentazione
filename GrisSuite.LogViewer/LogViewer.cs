using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GrisSuite;
using GrisSuite.Data.dsLogTableAdapters;
using GrisSuite.Data;

namespace GrisSuite.LogViewer
{
    public partial class LogViewer : UserControl
    {
        dsConfig _dsConfig = new dsConfig();

        public LogViewer()
        {
            InitializeComponent();
            this.datTo.Value = DateTime.Now.AddDays(1);
        }

          private void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                LogMessageTableAdapter ta = new LogMessageTableAdapter();

                Guid? logId = null;
                if (!string.IsNullOrEmpty(txtLogId.Text))
                    logId = new Guid(txtLogId.Text);

                string messageTypes = string.Empty;
                if (chkIsAlive.Checked)
                    messageTypes += "|" + chkIsAlive.Tag.ToString();
                if (chkConfig.Checked)
                    messageTypes += "|" + chkConfig.Tag.ToString();
                if (chkDeviceStatus.Checked)
                    messageTypes += "|" + chkDeviceStatus.Tag.ToString();
                if (chkEvents.Checked)
                    messageTypes += "|" + chkEvents.Tag.ToString();
                if (chkSTLCParameters.Checked)
                    messageTypes += "|" + chkSTLCParameters.Tag.ToString();
                if (messageTypes == string.Empty)
                    messageTypes = null;

                string hostSender = null;
                if (txtHostSender.Text.Trim() != string.Empty)
                    hostSender = txtHostSender.Text.Trim();

                string hostReceiver = null;
                if (txtHostReceiver.Text.Trim() != string.Empty)
                    hostReceiver = txtHostReceiver.Text.Trim();

                string mac = null;
                if (txtMAC.Text.Trim() != string.Empty)
                    mac = txtMAC.Text.Trim();

                int top = 1000;
                if (!int.TryParse(txtTop.Text, out top))
                {
                    top = 1000;
                    txtTop.Text = top.ToString();
                }

                int minMessageSize = 1000;
                if (!int.TryParse(txtMinMessageSize.Text, out minMessageSize))
                {
                    minMessageSize = 0;
                    txtMinMessageSize.Text = minMessageSize.ToString();
                }
                ta.CommandTimeout = 180;
                ta.Fill(dsLog1.LogMessage, logId, datFrom.Value, datTo.Value, messageTypes, hostSender, minMessageSize, top,mac,hostReceiver);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error:{0}", ex.Message));
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.Columns[e.ColumnIndex].Name == "cbnViewMessage")
                {
                    dsLog.LogMessageRow row = (dsLog.LogMessageRow)((DataRowView)dataGridView1.CurrentRow.DataBoundItem).Row;

                    FormMsgViewer mv = new FormMsgViewer();
                    DataSet ds = row.DecodeMessage();
                    if (ds.IsInitialized && ds.Tables.Count > 0)
                    {
                        mv.Message = ds;                       
                    }
                    mv.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error:{0}", ex.Message));
            }
        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void txtMAC_TextChanged(object sender, EventArgs e)
        {

        }


    }
}

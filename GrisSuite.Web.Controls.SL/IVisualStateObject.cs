﻿
using System.Windows;

namespace GrisSuite.Web.Controls.SL
{
	public interface IVisualStateObject
	{
		Point Location { get; set; }

		Point ScaleCenter { get; set; }

        GrisVisualState Status { get; set; }

		bool EditMode { get; set; }

		bool ShowContent { get; set; }

		object DataItem { get; set; }
		
		FrameworkElement ToolTip { get; set; }
	}
}

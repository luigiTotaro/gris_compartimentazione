﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.ComponentModel;

namespace GrisSuite.Web.Controls.SL.Converters
{
	public class AllowedDropElementTypeConverter : TypeConverter
	{
		public override bool CanConvertFrom ( ITypeDescriptorContext context, Type sourceType )
		{
			return true;
		}
		
		public override object ConvertFrom ( ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value )
		{
			if ( value is string && !string.IsNullOrEmpty(value.ToString()) && Application.Current != null && Application.Current.RootVisual != null )
			{
				var root = (FrameworkElement)Application.Current.RootVisual;
				var allowedElements = new List<UIElement>();
				string[] elmNames = value.ToString().Split(',');
				Array.ForEach(elmNames, s =>
				{
					if ( root.FindName(s) != null ) allowedElements.Add((UIElement)root.FindName(s));
				});

				if ( allowedElements.Count > 0 ) return allowedElements;
			}

			return null;
		}
	}
}

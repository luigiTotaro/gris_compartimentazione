﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace GrisSuite.Web.Controls.SL.Converters
{
    public class GrisSystemsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return GrisSystems.AltrePeriferiche;

            GrisSystems gs = (GrisSystems)Enum.Parse(typeof(GrisSystems), value.ToString(), true);
            return gs;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return ((int)GrisSystems.AltrePeriferiche).ToString();

            return ((int)((GrisSystems)value)).ToString();
        }
    }
}

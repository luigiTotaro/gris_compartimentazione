﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Controls;
using Devcorp.Controls.Design;

namespace GrisSuite.Web.Controls.SL
{
    public class VisualStateObject : DraggableUserControl, IVisualStateObject
    {
        private const string STATE_COLOR_ERROR = "#FF0000";
        private const string STATE_COLOR_WARNING = "#BA9200";
        private const string STATE_COLOR_OK = "#0D7400";
        private const string STATE_COLOR_MAINTENANCE_MODE = "#D0D0D0";
        private const string STATE_COLOR_OFFLINE = "#434343";

        # region Properties

        public Point Location { get; set; }

        public Point ScaleCenter { get; set; }

        # region Dependency Properties

        #region State

        public static readonly DependencyProperty StateProperty = DependencyProperty.Register("State", typeof(GrisVisualState),
                                                                                              typeof(VisualStateObject),
                                                                                              new PropertyMetadata(
                                                                                                  new GrisVisualState(),
                                                                                                  StatePropertyChangedCallback
                                                                                                  ));

        public GrisVisualState Status
        {
            get { return (GrisVisualState)this.GetValue(StateProperty); }
            set { this.SetValue(StateProperty, value); }
        }

        private static void StatePropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            if (eventArgs.Property == StateProperty && dependencyObject is VisualStateObject)
            {
                ((VisualStateObject)dependencyObject).SetVisualState((GrisVisualState)eventArgs.NewValue);
            }
        }

        #endregion

        #region EditMode

        public static readonly DependencyProperty EditModeProperty = DependencyProperty.Register("EditMode", typeof(bool),
                                                                                                 typeof(VisualStateObject),
                                                                                                 new PropertyMetadata(false,
                                                                                                                      EditModePropertyChangedCallback));

        public bool EditMode
        {
            get { return (bool)this.GetValue(EditModeProperty); }
            set { this.SetValue(EditModeProperty, value); }
        }

        private static void EditModePropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            if (eventArgs.Property == EditModeProperty && dependencyObject is DraggableUserControl)
            {
                ((DraggableUserControl)dependencyObject).DragAndDropEnabled = (bool)eventArgs.NewValue;
            }
        }

        public static readonly DependencyProperty ShowContentProperty = DependencyProperty.Register("ShowContent", typeof(bool),
                                                                                                    typeof(VisualStateObject),
                                                                                                    new PropertyMetadata(false, null));

        #endregion

        public bool ShowContent
        {
            get { return (bool)this.GetValue(ShowContentProperty); }
            set { this.SetValue(ShowContentProperty, value); }
        }

        public object DataItem { get; set; }

        public bool Relocated { get; set; }

        public FrameworkElement ToolTip { get; set; }

        # endregion

        # endregion

        public event EventHandler Click;

        public VisualStateObject()
        {
            this.DefaultStyleKey = typeof(VisualStateObject);

            this.EditMode = false;

            this.MouseEnter += this.StateObject_MouseEnter;
            this.MouseLeave += this.StateObject_MouseLeave;
            this.MouseLeftButtonDown += this.StateObject_MouseLeftButtonDown;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.SetVisualState(this.Status);

            UIElement contentEl = this.GetTemplateChild("ContentElement") as UIElement;
            Path xEl = this.GetTemplateChild("X") as Path;

            if (this.ShowContent)
            {
                if (contentEl != null)
                {
                    contentEl.Visibility = Visibility.Visible;
                }
                if (xEl != null)
                {
                    xEl.Fill.Opacity = 0.9;
                }
            }
            else
            {
                if (contentEl != null)
                {
                    contentEl.Visibility = Visibility.Collapsed;
                }
                if (xEl != null)
                {
                    xEl.Fill.Opacity = 0;
                }
            }
        }

        # region State Management

        private Color GetColorFromGrisVisualState(GrisVisualState visualState)
        {
            if (visualState.CustomColor != Colors.Transparent)
            {
                return visualState.CustomColor;
            }

            switch (visualState.Status)
            {
                case GrisStatus.Error:
                    return ColorSpaceHelper.GetColorFromHexString(STATE_COLOR_ERROR);
                case GrisStatus.Warning:
                    return ColorSpaceHelper.GetColorFromHexString(STATE_COLOR_WARNING);
                case GrisStatus.Ok:
                    return ColorSpaceHelper.GetColorFromHexString(STATE_COLOR_OK);
                case GrisStatus.Maintenance:
                    return ColorSpaceHelper.GetColorFromHexString(STATE_COLOR_MAINTENANCE_MODE);
                default:
                    return ColorSpaceHelper.GetColorFromHexString(STATE_COLOR_OFFLINE);
            }
        }

        private void SetVisualState(GrisVisualState visualState)
        {
            VisualStateManager.GoToState(this, "Empty", true);
            Border externalBorder = this.GetTemplateChild("ExternalBorder") as Border;
            Grid layoutPanel = this.GetTemplateChild("LayoutPanel") as Grid;
            Path xSymbol = this.GetTemplateChild("XSymbol") as Path;

            if ((externalBorder != null) && (layoutPanel != null) && (xSymbol != null))
            {
                switch (visualState.Status)
                {
                    case GrisStatus.Error:
                    {
                        Storyboard storyboard = new Storyboard();

                        #region Animazione colore, con diminuzione luminosità del 33% rispetto a colore passato

                        ColorAnimationUsingKeyFrames cAnimKeyFrames = new ColorAnimationUsingKeyFrames();
                        cAnimKeyFrames.BeginTime = TimeSpan.FromMilliseconds(0);
                        cAnimKeyFrames.AutoReverse = true;
                        cAnimKeyFrames.RepeatBehavior = RepeatBehavior.Forever;

                        HSB customColorHSB = ColorSpaceHelper.RGBtoHSB(this.GetColorFromGrisVisualState(visualState));

                        SplineColorKeyFrame keyFrame1 = new SplineColorKeyFrame();
                        keyFrame1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        RGB customColorRGB = ColorSpaceHelper.HSBtoRGB(customColorHSB.Hue, customColorHSB.Saturation,
                                                                       customColorHSB.Brightness - (customColorHSB.Brightness / 3));
                        keyFrame1.Value = Color.FromArgb(0xFF, customColorRGB.Red, customColorRGB.Green, customColorRGB.Blue);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame1);

                        SplineColorKeyFrame keyFrame2 = new SplineColorKeyFrame();
                        keyFrame2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(200));
                        RGB customColorRGB2 = ColorSpaceHelper.HSBtoRGB(customColorHSB.Hue, customColorHSB.Saturation,
                                                                        customColorHSB.Brightness - (customColorHSB.Brightness / 3));
                        keyFrame2.Value = Color.FromArgb(0xFE, customColorRGB2.Red, customColorRGB2.Green, customColorRGB2.Blue);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame2);

                        SplineColorKeyFrame keyFrame3 = new SplineColorKeyFrame();
                        keyFrame3.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(400));
                        keyFrame3.Value = this.GetColorFromGrisVisualState(visualState);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame3);

                        storyboard.Children.Add(cAnimKeyFrames);

                        Storyboard.SetTarget(storyboard.Children[0], externalBorder);
                        Storyboard.SetTargetProperty(storyboard.Children[0], new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));

                        #endregion

                        #region Animazione dimensione X

                        DoubleAnimationUsingKeyFrames dAnimKeyFramesX = new DoubleAnimationUsingKeyFrames();
                        dAnimKeyFramesX.BeginTime = TimeSpan.FromMilliseconds(0);
                        dAnimKeyFramesX.AutoReverse = true;
                        dAnimKeyFramesX.RepeatBehavior = RepeatBehavior.Forever;

                        SplineDoubleKeyFrame doubleKeyFrameX1 = new SplineDoubleKeyFrame();
                        doubleKeyFrameX1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        doubleKeyFrameX1.Value = 1;
                        dAnimKeyFramesX.KeyFrames.Add(doubleKeyFrameX1);

                        SplineDoubleKeyFrame doubleKeyFrameX2 = new SplineDoubleKeyFrame();
                        doubleKeyFrameX2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(200));
                        doubleKeyFrameX2.Value = 1;
                        dAnimKeyFramesX.KeyFrames.Add(doubleKeyFrameX2);

                        SplineDoubleKeyFrame doubleKeyFrameX3 = new SplineDoubleKeyFrame();
                        doubleKeyFrameX3.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(400));
                        doubleKeyFrameX3.Value = 1.2;
                        dAnimKeyFramesX.KeyFrames.Add(doubleKeyFrameX3);

                        storyboard.Children.Add(dAnimKeyFramesX);

                        Storyboard.SetTarget(storyboard.Children[1], layoutPanel);
                        Storyboard.SetTargetProperty(storyboard.Children[1],
                                                     new PropertyPath(
                                                         "(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleX)"));

                        #endregion

                        #region Animazione dimensione Y

                        DoubleAnimationUsingKeyFrames dAnimKeyFramesY = new DoubleAnimationUsingKeyFrames();
                        dAnimKeyFramesY.BeginTime = TimeSpan.FromMilliseconds(0);
                        dAnimKeyFramesY.AutoReverse = true;
                        dAnimKeyFramesY.RepeatBehavior = RepeatBehavior.Forever;

                        SplineDoubleKeyFrame doubleKeyFrameY1 = new SplineDoubleKeyFrame();
                        doubleKeyFrameY1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        doubleKeyFrameY1.Value = 1;
                        dAnimKeyFramesY.KeyFrames.Add(doubleKeyFrameY1);

                        SplineDoubleKeyFrame doubleKeyFrameY2 = new SplineDoubleKeyFrame();
                        doubleKeyFrameY2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(200));
                        doubleKeyFrameY2.Value = 1;
                        dAnimKeyFramesY.KeyFrames.Add(doubleKeyFrameY2);

                        SplineDoubleKeyFrame doubleKeyFrameY3 = new SplineDoubleKeyFrame();
                        doubleKeyFrameY3.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(400));
                        doubleKeyFrameY3.Value = 1.2;
                        dAnimKeyFramesY.KeyFrames.Add(doubleKeyFrameY3);

                        storyboard.Children.Add(dAnimKeyFramesY);

                        Storyboard.SetTarget(storyboard.Children[2], layoutPanel);
                        Storyboard.SetTargetProperty(storyboard.Children[2],
                                                     new PropertyPath(
                                                         "(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleY)"));

                        #endregion

                        storyboard.Begin();

                        break;
                    }
                    case GrisStatus.Warning:
                    {
                        Storyboard storyboard = new Storyboard();

                        #region Animazione colore

                        ColorAnimationUsingKeyFrames cAnimKeyFrames = new ColorAnimationUsingKeyFrames();
                        cAnimKeyFrames.BeginTime = TimeSpan.FromMilliseconds(0);
                        cAnimKeyFrames.Duration = new Duration(TimeSpan.FromMilliseconds(10));

                        SplineColorKeyFrame keyFrame = new SplineColorKeyFrame();
                        keyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        keyFrame.Value = this.GetColorFromGrisVisualState(visualState);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame);
                        storyboard.Children.Add(cAnimKeyFrames);

                        Storyboard.SetTarget(storyboard.Children[0], externalBorder);
                        Storyboard.SetTargetProperty(storyboard.Children[0], new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));

                        #endregion

                        storyboard.Begin();
                        break;
                    }
                    case GrisStatus.Ok:
                    {
                        Storyboard storyboard = new Storyboard();

                        #region Animazione colore

                        ColorAnimationUsingKeyFrames cAnimKeyFrames = new ColorAnimationUsingKeyFrames();
                        cAnimKeyFrames.BeginTime = TimeSpan.FromMilliseconds(0);
                        cAnimKeyFrames.Duration = new Duration(TimeSpan.FromMilliseconds(10));

                        SplineColorKeyFrame keyFrame = new SplineColorKeyFrame();
                        keyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        keyFrame.Value = this.GetColorFromGrisVisualState(visualState);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame);
                        storyboard.Children.Add(cAnimKeyFrames);

                        Storyboard.SetTarget(storyboard.Children[0], externalBorder);
                        Storyboard.SetTargetProperty(storyboard.Children[0], new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));

                        #endregion

                        storyboard.Begin();

                        break;
                    }
                    case GrisStatus.Maintenance:
                    {
                        Storyboard storyboard = new Storyboard();

                        #region Animazione colore

                        ColorAnimationUsingKeyFrames cAnimKeyFrames = new ColorAnimationUsingKeyFrames();
                        cAnimKeyFrames.BeginTime = TimeSpan.FromMilliseconds(0);
                        cAnimKeyFrames.Duration = new Duration(TimeSpan.FromMilliseconds(10));

                        SplineColorKeyFrame keyFrame = new SplineColorKeyFrame();
                        keyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        keyFrame.Value = this.GetColorFromGrisVisualState(visualState);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame);
                        storyboard.Children.Add(cAnimKeyFrames);

                        Storyboard.SetTarget(storyboard.Children[0], externalBorder);
                        Storyboard.SetTargetProperty(storyboard.Children[0], new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));

                        #endregion

                        storyboard.Begin();

                        break;
                    }
                    default:
                    {
                        Storyboard storyboard = new Storyboard();

                        #region Animazione colore

                        ColorAnimationUsingKeyFrames cAnimKeyFrames = new ColorAnimationUsingKeyFrames();
                        cAnimKeyFrames.BeginTime = TimeSpan.FromMilliseconds(0);
                        cAnimKeyFrames.Duration = new Duration(TimeSpan.FromMilliseconds(10));

                        SplineColorKeyFrame keyFrame = new SplineColorKeyFrame();
                        keyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0));
                        keyFrame.Value = this.GetColorFromGrisVisualState(visualState);
                        cAnimKeyFrames.KeyFrames.Add(keyFrame);
                        storyboard.Children.Add(cAnimKeyFrames);

                        Storyboard.SetTarget(storyboard.Children[0], externalBorder);
                        Storyboard.SetTargetProperty(storyboard.Children[0], new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));

                        #endregion

                        #region Animazione X nera lampeggiante

                        ObjectAnimationUsingKeyFrames oAnimKeyFrames = new ObjectAnimationUsingKeyFrames();
                        oAnimKeyFrames.BeginTime = TimeSpan.FromMilliseconds(0);
                        oAnimKeyFrames.AutoReverse = true;
                        oAnimKeyFrames.RepeatBehavior = RepeatBehavior.Forever;

                        DiscreteObjectKeyFrame keyFrame1 = new DiscreteObjectKeyFrame();
                        keyFrame1.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(400));
                        keyFrame1.Value = Visibility.Collapsed;
                        oAnimKeyFrames.KeyFrames.Add(keyFrame1);

                        DiscreteObjectKeyFrame keyFrame2 = new DiscreteObjectKeyFrame();
                        keyFrame2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(500));
                        keyFrame2.Value = Visibility.Visible;
                        oAnimKeyFrames.KeyFrames.Add(keyFrame2);

                        DiscreteObjectKeyFrame keyFrame3 = new DiscreteObjectKeyFrame();
                        keyFrame3.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(1000));
                        keyFrame3.Value = Visibility.Visible;
                        oAnimKeyFrames.KeyFrames.Add(keyFrame3);

                        storyboard.Children.Add(oAnimKeyFrames);

                        Storyboard.SetTarget(storyboard.Children[1], xSymbol);
                        Storyboard.SetTargetProperty(storyboard.Children[1], new PropertyPath("(UIElement.Visibility)"));

                        #endregion

                        storyboard.Begin();

                        break;
                    }
                }
            }
        }

        # endregion

        # region Event Handlers

        private void StateObject_MouseLeave(object sender, MouseEventArgs e)
        {
            VisualStateManager.GoToState(this, "Normal", true);
        }

        private void StateObject_MouseEnter(object sender, MouseEventArgs e)
        {
            VisualStateManager.GoToState(this, "MouseOver", true);
        }

        private void StateObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.Click != null)
            {
                e.Handled = true;
                this.Click(this, null);
            }
        }

        # endregion
    }
}
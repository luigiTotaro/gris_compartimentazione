﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GrisSuite.Web.Controls.SL
{
    public class GrisVisualState
    {
        public GrisStatus Status { get; set; }
        public Color CustomColor { get; set; }

        public GrisVisualState(GrisStatus status, Color customColor)
        {
            this.Status = status;
            this.CustomColor = customColor;
        }

        public GrisVisualState(Color customColor)
        {
            this.Status = GrisStatus.Maintenance;
            this.CustomColor = customColor;
        }

        public GrisVisualState(GrisStatus status)
        {
            this.Status = status;
            this.CustomColor = Colors.Transparent;
        }

        public GrisVisualState()
        {
            this.Status = GrisStatus.Maintenance;
            this.CustomColor = Colors.Transparent;
        }
    }
}
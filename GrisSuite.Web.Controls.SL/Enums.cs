﻿
namespace GrisSuite.Web.Controls.SL
{
    public enum GrisStatus
    {
        None = -1,
        Ok = 0,
        Warning = 1,
        Error = 2,
        Offline = 3,
        Maintenance = 9,
        Unknown = 255,
    }

    public enum GrisSystems
    {
        DiffusioneSonora = 1,
        InformazioneVisiva = 2,
        Illuminazione = 3,
        Telefonia = 4,
        Rete  = 5,
        ErogazioneEnergia= 6,
        FDS = 8,
        Diagnostica = 10,
		Facilities = 11,
		MonitoraggioVPNVerde = 12,
        WebRadio = 13,
        Orologi = 14,
        Ascensori = 15,
        AltrePeriferiche = 99
    }
}

﻿
using System.Windows.Controls;

namespace GrisSuite.Web.Controls.SL
{
	public class GrisPanel : ContentControl
	{
		public GrisPanel()
		{
			DefaultStyleKey = typeof(GrisPanel);
		}
	}
}

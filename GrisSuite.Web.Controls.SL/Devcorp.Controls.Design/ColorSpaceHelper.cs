using System;
using System.Text.RegularExpressions;
using System.Windows.Media;

namespace Devcorp.Controls.Design
{
    /// <summary>
    /// Provides methods to convert from a color space to an other.
    /// </summary>
    public static class ColorSpaceHelper
    {
        private static readonly Regex htmlColorRegex = new Regex("^#([a-f]|[A-F]|[0-9]){3}([a-f]|[A-F]|[0-9]){3}$", RegexOptions.Singleline);

        public static Color GetColorFromHexString(string s)
        {
            if (!string.IsNullOrEmpty(s) && (htmlColorRegex.IsMatch(s)))
            {
                byte r = Convert.ToByte(s.Substring(1, 2), 16);
                byte g = Convert.ToByte(s.Substring(3, 2), 16);
                byte b = Convert.ToByte(s.Substring(5, 2), 16);
                return Color.FromArgb(0xFF, r, g, b);
            }

            return Colors.Transparent;
        }

        #region HSB convert

        /// <summary>
        /// Converts HSB to RGB.
        /// </summary>
        /// <param name="hsb">The HSB structure to convert.</param>
        public static RGB HSBtoRGB(HSB hsb)
        {
            double r = 0;
            double g = 0;
            double b = 0;

            if (hsb.Saturation == 0)
            {
                r = g = b = hsb.Brightness;
            }
            else
            {
                // the color wheel consists of 6 sectors. Figure out which sector you're in.
                double sectorPos = hsb.Hue / 60.0;
                int sectorNumber = (int)(Math.Floor(sectorPos));
                // get the fractional part of the sector
                double fractionalSector = sectorPos - sectorNumber;

                // calculate values for the three axes of the color. 
                double p = hsb.Brightness * (1.0 - hsb.Saturation);
                double q = hsb.Brightness * (1.0 - (hsb.Saturation * fractionalSector));
                double t = hsb.Brightness * (1.0 - (hsb.Saturation * (1 - fractionalSector)));

                // assign the fractional colors to r, g, and b based on the sector the angle is in.
                switch (sectorNumber)
                {
                    case 0:
                        r = hsb.Brightness;
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = hsb.Brightness;
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = hsb.Brightness;
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = hsb.Brightness;
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = hsb.Brightness;
                        break;
                    case 5:
                        r = hsb.Brightness;
                        g = p;
                        b = q;
                        break;
                }
            }

            return new RGB(
                Convert.ToByte(Double.Parse(String.Format("{0:0.00}", r * 255.0))),
                Convert.ToByte(Double.Parse(String.Format("{0:0.00}", g * 255.0))),
                Convert.ToByte(Double.Parse(String.Format("{0:0.00}", b * 255.0)))
                );
        }

        /// <summary>
        /// Converts HSB to RGB.
        /// </summary>
        /// <param name="H">Hue value.</param>
        /// <param name="S">Saturation value.</param>
        /// <param name="V">Brigthness value.</param>
        public static RGB HSBtoRGB(double h, double s, double b)
        {
            return HSBtoRGB(new HSB(h, s, b));
        }

        /// <summary>
        /// Converts HSB to Color.
        /// </summary>
        /// <param name="hsv">the HSB structure to convert.</param>
        public static Color HSBtoColor(HSB hsb)
        {
            RGB rgb = HSBtoRGB(hsb);

            return Color.FromArgb(0xFF, rgb.Red, rgb.Green, rgb.Blue);
        }

        /// <summary> 
        /// Converts HSB to a .net Color.
        /// </summary>
        /// <param name="h">Hue value (must be between 0 and 360).</param>
        /// <param name="s">Saturation value (must be between 0 and 1).</param>
        /// <param name="b">Brightness value (must be between 0 and 1).</param>
        public static Color HSBtoColor(double h, double s, double b)
        {
            return HSBtoColor(new HSB(h, s, b));
        }

        /// <summary>
        /// Converts HSB to Color.
        /// </summary>
        /// <param name="h">Hue value.</param>
        /// <param name="s">Saturation value.</param>
        /// <param name="b">Brightness value.</param>
        public static Color HSBtoColor(int h, int s, int b)
        {
            double hue = 0, sat = 0, val = 0;

            // Scale Hue to be between 0 and 360. Saturation and value scale to be between 0 and 1.
            if (h > 360 || s > 1 || b > 1)
            {
                hue = (h / 255.0 * 360.0) % 360.0;
                sat = s / 255.0;
                val = b / 255.0;
            }

            return HSBtoColor(new HSB(hue, sat, val));
        }

        #endregion

        #region RGB convert

        /// <summary> 
        /// Converts RGB to HSB.
        /// </summary> 
        public static HSB RGBtoHSB(int red, int green, int blue)
        {
            double r = (red / 255.0);
            double g = (green / 255.0);
            double b = (blue / 255.0);

            double max = Math.Max(r, Math.Max(g, b));
            double min = Math.Min(r, Math.Min(g, b));

            double h = 0.0;
            if (max == r && g >= b)
            {
                if (max - min == 0)
                {
                    h = 0.0;
                }
                else
                {
                    h = 60 * (g - b) / (max - min);
                }
            }
            else if (max == r && g < b)
            {
                h = 60 * (g - b) / (max - min) + 360;
            }
            else if (max == g)
            {
                h = 60 * (b - r) / (max - min) + 120;
            }
            else if (max == b)
            {
                h = 60 * (r - g) / (max - min) + 240;
            }

            double s = (max == 0) ? 0.0 : (1.0 - (min / max));

            return new HSB(h, s, max);
        }

        /// <summary> 
        /// Converts RGB to HSB.
        /// </summary> 
        public static HSB RGBtoHSB(RGB rgb)
        {
            return RGBtoHSB(rgb.Red, rgb.Green, rgb.Blue);
        }

        /// <summary> 
        /// Converts RGB to HSB.
        /// </summary> 
        public static HSB RGBtoHSB(Color c)
        {
            return RGBtoHSB(c.R, c.G, c.B);
        }

        #endregion
    }
}
using System;
using System.ComponentModel;

namespace Devcorp.Controls.Design
{
    /// <summary>
    /// Structure to define RGB.
    /// </summary>
    public struct RGB
    {
        /// <summary>
        /// Gets an empty RGB structure;
        /// </summary>
        public static readonly RGB Empty;

        #region Fields

        private byte red;
        private byte green;
        private byte blue;

        #endregion

        #region Operators

        public static bool operator ==(RGB item1, RGB item2)
        {
            return (
                       item1.Red == item2.Red
                       && item1.Green == item2.Green
                       && item1.Blue == item2.Blue
                   );
        }

        public static bool operator !=(RGB item1, RGB item2)
        {
            return (
                       item1.Red != item2.Red
                       || item1.Green != item2.Green
                       || item1.Blue != item2.Blue
                   );
        }

        #endregion

        #region Accessors

        [Description("Red component."),]
        public byte Red
        {
            get { return this.red; }
            set { this.red = value; }
        }

        [Description("Green component."),]
        public byte Green
        {
            get { return this.green; }
            set { this.green = value; }
        }

        [Description("Blue component."),]
        public byte Blue
        {
            get { return this.blue; }
            set { this.blue = value; }
        }

        #endregion

        public RGB(byte R, byte G, byte B)
        {
            this.red = R;
            this.green = G;
            this.blue = B;
        }

        #region Methods

        public override bool Equals(Object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }

            return (this == (RGB)obj);
        }

        public override int GetHashCode()
        {
            return this.Red.GetHashCode() ^ this.Green.GetHashCode() ^ this.Blue.GetHashCode();
        }

        #endregion
    }
}
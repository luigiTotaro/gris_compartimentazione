﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.ComponentModel;
using System.Diagnostics;
using GrisSuite.Web.Controls.SL.Converters;

namespace GrisSuite.Web.Controls.SL
{
    public class DraggableUserControl : ContentControl
    {
        # region Fields

        private bool _dragging;
        private int _origZIndex;
        private Point _origAbsMousePosition;
        private Point _origRelMousePosition;
        private Point _lastMousePosition;
        private UIElement _lastAllowedContainerEntered;
        private UIElement _myCopy;

        public static readonly DependencyProperty DragAndDropEnabledProperty =
            DependencyProperty.Register("DragAndDropEnabled", typeof(bool), typeof(DraggableUserControl), new PropertyMetadata(false));

        public static readonly DependencyProperty MoveFromOriginalPositionProperty =
            DependencyProperty.Register("MoveFromOriginalPosition", typeof(bool), typeof(DraggableUserControl), new PropertyMetadata(false));

        public static readonly DependencyProperty NeededZIndexProperty =
            DependencyProperty.Register("NeededZIndex", typeof(int), typeof(DraggableUserControl), new PropertyMetadata(1000));

        # endregion

        # region Properties

        public double X { get; set; }
        public double Y { get; set; }

        [TypeConverter(typeof(AllowedDropElementTypeConverter))]
        public IList<UIElement> AllowedElements { get; set; }

        public bool DragAndDropEnabled
        {
            get { return (bool)this.GetValue(DragAndDropEnabledProperty); }
            set { this.SetValue(DragAndDropEnabledProperty, value); }
        }

        public bool MoveFromOriginalPosition
        {
            get { return (bool)this.GetValue(MoveFromOriginalPositionProperty); }
            set { this.SetValue(MoveFromOriginalPositionProperty, value); }
        }

        public int NeededZIndex
        {
            get { return (int)this.GetValue(NeededZIndexProperty); }
            set { this.SetValue(NeededZIndexProperty, value); }
        }

        # endregion

        # region Events

        public delegate void DroppedHandler(object sender, DraggableUserControlDropEventArgs e);

        public event DroppedHandler Dropped;

        public delegate void AllowedElementEnterHandler(object sender, DraggableUCElementEnterEventArgs e);

        public event AllowedElementEnterHandler AllowedElementEnter;

        # endregion

        public DraggableUserControl()
        {
            this.AllowedElements = new List<UIElement>(10);
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (this.DragAndDropEnabled)
            {
                base.OnMouseLeftButtonDown(e);
                e.Handled = true;
                this.StartDragHandler(e);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (this.DragAndDropEnabled)
            {
                base.OnMouseMove(e);
                this.DragHandler(e);
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (this.DragAndDropEnabled)
            {
                base.OnMouseLeftButtonUp(e);
                e.Handled = true;
                this.DropHandler(e);
            }
        }

        # region Methods

        private void StartDragHandler(MouseEventArgs e)
        {
            this._dragging = true;

            # region Mouse Position

            this._lastMousePosition = e.GetPosition(null);
            this._origAbsMousePosition = this._lastMousePosition;
            this._origRelMousePosition = e.GetPosition((UIElement)this.Parent);

            # endregion

            UIElement elToMove = this.GetElementToMove();

            # region Z-Index

            this._origZIndex = Canvas.GetZIndex(elToMove);
            Canvas.SetZIndex(elToMove, this.NeededZIndex);

            # endregion

            if (this.MoveFromOriginalPosition)
            {
                this.X = this._lastMousePosition.X;
                this.Y = this._lastMousePosition.Y;
            }
            else
            {
                TranslateTransform transform = this.GetTranslateTransform(elToMove);
                transform.X = this._lastMousePosition.X;
                transform.Y = this._lastMousePosition.Y;
            }

            elToMove.CaptureMouse();
        }

        private void DropHandler(MouseEventArgs e)
        {
            if (this._dragging)
            {
                this._dragging = false;

                UIElement elToMove = this.GetElementToMove();

                elToMove.ReleaseMouseCapture();

                # region Z-Index

                Canvas.SetZIndex(elToMove, this._origZIndex);

                # endregion

                //Determinazione della posizione finale

                # region Mouse Position

                if (this._lastAllowedContainerEntered != null)
                {
                    # region debug section

                    Debug.WriteLine("Dropped Internal");
                    string elems = string.Join(", ",
                                               (from el in
                                                    VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(null), Application.Current.RootVisual)
                                                select el.GetValue(NameProperty).ToString()).ToArray());
                    Debug.WriteLine(elems);

                    # endregion

                    this.OnDropped(this, new DraggableUserControlDropEventArgs
                                         {
                                             OriginContainer = (UIElement)this.Parent,
                                             OriginalAbsolutePosition = this._origAbsMousePosition,
                                             OriginalRelativePosition = this._origRelMousePosition,
                                             DestinationContainer = this._lastAllowedContainerEntered,
                                             FinalAbsolutePosition = e.GetPosition(null),
                                             FinalRelativePosition = e.GetPosition(this._lastAllowedContainerEntered)
                                         });
                    this._lastMousePosition = e.GetPosition(null);
                    this._origAbsMousePosition = this._lastMousePosition;
                    this._origRelMousePosition = e.GetPosition((UIElement)this.Parent);
                }

                # endregion

                this.FinalizeObjectToMove();
            }
        }

        private void DragHandler(MouseEventArgs e)
        {
            if (this._dragging)
            {
                UIElement enteredContainer = this.GetLocalValidDestinationContainer(e);

                if (enteredContainer != null)
                {
                    TranslateTransform transform = this.GetTranslateTransform(this.GetElementToMove());
                    Point currentMousePosition = e.GetPosition(null);
                    this.X = currentMousePosition.X - this._lastMousePosition.X;
                    this.Y = currentMousePosition.Y - this._lastMousePosition.Y;
                    transform.X += this.X;
                    transform.Y += this.Y;
                    this._lastMousePosition = currentMousePosition;
                }
            }
        }

        private TranslateTransform GetTranslateTransform(UIElement element)
        {
            TranslateTransform translateTransform = null;
            if (element.RenderTransform is TranslateTransform)
            {
                translateTransform = element.RenderTransform as TranslateTransform;
            }
            else if (element.RenderTransform is TransformGroup)
            {
                TransformGroup group = element.RenderTransform as TransformGroup;
                foreach (Transform transform in group.Children)
                {
                    if (transform is TranslateTransform)
                    {
                        translateTransform = transform as TranslateTransform;
                    }
                }
            }
            else
            {
                translateTransform = new TranslateTransform();
                element.RenderTransform = translateTransform;
            }
            return translateTransform;
        }

        private UIElement GetLocalValidDestinationContainer(MouseEventArgs e)
        {
            // Ottengo tutti gli elementi che contengono il punto su cui è posizionato il mouse
            List<UIElement> collidedElements =
                VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(null), Application.Current.RootVisual).ToList();

            UIElement topmostElement;
            this.RemoveUnrelevantElements(collidedElements, out topmostElement);

            if (topmostElement != null && this.CheckValidDestinations(collidedElements, this.AllowedElements) &&
                topmostElement != this._lastAllowedContainerEntered)
            {
                this._lastAllowedContainerEntered = topmostElement;
                this.OnAllowedElementEnter(this, new DraggableUCElementEnterEventArgs {Container = topmostElement});
            }

            return this._lastAllowedContainerEntered;
        }

        private void RemoveUnrelevantElements(IList<UIElement> collidedElements, out UIElement topmostElement)
        {
            topmostElement = null;

            if (collidedElements != null)
            {
                collidedElements.Remove(Application.Current.RootVisual); // elimino lo usercontrol radice

                var victims = from c in collidedElements
                              where collidedElements.IndexOf(this) >= collidedElements.IndexOf(c)
                              select c;

                victims.ToList().ForEach(sur => collidedElements.Remove(sur));

                if (collidedElements.Count > 0)
                {
                    topmostElement = collidedElements[0];
                }
                if (topmostElement is MultiScaleImage)
                {
                    topmostElement = (UIElement)((Grid)((MultiScaleImage)topmostElement).Parent).FindName("canvasPinnedObjects");
                }
            }
        }

        private bool CheckValidDestinations(IEnumerable<UIElement> collidedElements, IEnumerable<UIElement> allowedElements)
        {
            if (collidedElements != null && allowedElements != null)
            {
                // verifico se almeno uno degli elementi restituiti è tra quelli in cui il drop è consentito
                var mouseOverElements = from c in collidedElements
                                        join a in allowedElements on c.GetValue(NameProperty) equals
                                            a.GetValue(NameProperty)
                                        select a;

                return (mouseOverElements.Any());
            }

            return false;
        }

        /// <summary>
        /// Restituisce l'elemento a cui va applicata la traslazione.
        /// </summary>
        /// <returns>Elemento da traslare.</returns>
        private UIElement GetElementToMove()
        {
            if (!this.MoveFromOriginalPosition)
            {
                Panel pnl = this.GetRootPanel();

                this._myCopy = (UIElement)this.MemberwiseClone();
                this._myCopy.SetValue(NameProperty, string.Format("{0}_Copy", this.Name));

                this._myCopy.SetValue(MarginProperty, new Thickness(0));
                this._myCopy.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
                this._myCopy.SetValue(VerticalAlignmentProperty, VerticalAlignment.Top);

                ((Panel)this.Parent).Children.Remove(this._myCopy);

                object tmp = this.Tag;
                GrisVisualState state = ((VisualStateObject)this).Status;
                pnl.Children.Add(this._myCopy);
                this.Tag = tmp;
                ((VisualStateObject)this).Status = state;

                return this._myCopy;
            }

            return this;
        }

        /// <summary>
        /// Rimuove l'elemento clone dalla collezione dei figli del root container e lo distrugge.
        /// </summary>
        private void FinalizeObjectToMove()
        {
            if (!this.MoveFromOriginalPosition)
            {
                Panel pnl = this.GetRootPanel();

                pnl.Children.Remove(this._myCopy);
                this._myCopy = null;
            }
        }

        private Panel GetRootPanel()
        {
            Panel pnl;
            if ((Application.Current.RootVisual is UserControl) &&
                (pnl = ((UserControl)Application.Current.RootVisual).FindName("LayoutRoot") as Panel) != null)
            {
                return pnl;
            }

            throw new ClientGrisException(
                string.Format("Impossibile identificare il root container per il DraggableControl '{0}'", this.Name));
        }

        # endregion

        # region Class Plumbing

        protected virtual void OnDropped(object sender, DraggableUserControlDropEventArgs e)
        {
            if (this.Dropped != null)
            {
                this.Dropped(sender, e);
            }
        }

        protected virtual void OnAllowedElementEnter(object sender, DraggableUCElementEnterEventArgs e)
        {
            if (this.AllowedElementEnter != null)
            {
                this.AllowedElementEnter(sender, e);
            }
        }

        # endregion
    }

    public class DraggableUserControlDropEventArgs
    {
        public Point OriginalAbsolutePosition { get; set; }
        public Point FinalAbsolutePosition { get; set; }
        public Point OriginalRelativePosition { get; set; }
        public Point FinalRelativePosition { get; set; }
        public UIElement OriginContainer { get; set; }
        public UIElement DestinationContainer { get; set; }
    }

    public class DraggableUCElementEnterEventArgs
    {
        public UIElement Container { get; set; }
    }
}
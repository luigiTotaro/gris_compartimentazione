﻿using System;

namespace GrisSuite.Web.Controls.SL
{
	public class ClientGrisException : Exception
	{
		public ClientGrisException ( string message ) : base(message) { }
		public ClientGrisException ( string message, Exception innerException) : base(message, innerException) { }
	}
}

﻿using System.Collections;
using System.IO;

namespace GrisSuite.SystemXML.Console
{
    public class FilesDateComparer: IComparer
    {
        FilesDateComparerOrderDirectionEnum direction;
        
        public FilesDateComparer()
        {
            this.direction = FilesDateComparerOrderDirectionEnum.ASC;
        }

        public FilesDateComparer(FilesDateComparerOrderDirectionEnum orderDirection)
        {
            this.direction = orderDirection;    
        }

        #region IComparer Members

        public int Compare(object x, object y)
        {
            int iResult;

            FileInfo oFileX = (FileInfo)x;
            FileInfo oFileY = (FileInfo)y;

            if (oFileX.CreationTime == oFileY.CreationTime)
            {
                iResult = 0;
            }
            else
                if (oFileX.CreationTime > oFileY.CreationTime)
                {
                    iResult = direction == FilesDateComparerOrderDirectionEnum.ASC ? 1 : -1;
                }
                else
                {
                    iResult = direction == FilesDateComparerOrderDirectionEnum.ASC ? -1 : 1;
                }


            return iResult;
        } 

        #endregion
    }

    public enum FilesDateComparerOrderDirectionEnum
    { 
        ASC,
        DESC
    }
}

﻿using System;
using System.IO;
using System.Xml;

namespace GrisSuite.SystemXML.Console {
    public class SystemFileDocument : XmlDocument {
        private FileInfo fileInfo = null;
        private XmlNode telefinNode = null;
        private XmlNode xmlNode = null;

        public SystemFileDocument(string filePath) : base() {
            this.fileInfo = new FileInfo(filePath);

            if (!fileInfo.Exists) {
                throw new ApplicationException(string.Format("Impossibile trovare il file {0}. Controllare impostazioni nel file app.cofig", fileInfo.FullName));
            }
        }

        public XmlNode TelefinNode {
            get { return this.telefinNode; }
        }

        public XmlNode XmlNode {
            get { return this.xmlNode; }
        }

        public void Load() {
            try {
                base.Load(fileInfo.FullName);
                xmlNode = this.ChildNodes[0];
                telefinNode = this.SelectSingleNode("telefin");
                if (telefinNode == null) {
                    throw new ApplicationException(string.Format("Il file {0} non ha un elemento root Telefin.", fileInfo.FullName));
                }
            }
            catch (Exception ex) {
                throw new ApplicationException(String.Format("Impossibile caricare XMLDocument dal file {0}: " + ex.Message, fileInfo.FullName));
            }
        }

        public void Replace(string nodeName, string value) {
            XmlNode node = SelectOrCreateNode(nodeName);
            node.InnerXml = value;
        }

        public void ReplaceRoot(string value) {
            FileStream textFile = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.ReadWrite);
            StreamReader srFile = new StreamReader(textFile);
            string line = srFile.ReadLine();
            string sXml = String.Format(@"{0}{1}", value, "\r\n");
            sXml += srFile.ReadToEnd();
            srFile.Close();
            srFile.Dispose();

            StreamWriter fs = new StreamWriter(fileInfo.FullName);
            fs.Write(sXml);
            fs.Close();
            fs.Dispose();
        }

        public XmlNode SelectOrCreateNode(string nodeName) {
            XmlNode xmlNode = telefinNode.SelectSingleNode(nodeName);

            if (xmlNode == null) {
                xmlNode = this.CreateElement(nodeName);
                telefinNode.AppendChild(xmlNode);
            }

            return xmlNode;
        }

        public void Merge(string nodeName, XmlNode source, bool recursive) {
            Merge(nodeName, source, recursive, false);
        }

        public void Merge(string nodeName, XmlNode source, bool recursive, bool replace) {
            XmlNode destination = telefinNode.SelectSingleNode(nodeName);

            if (destination == null) {
                destination = this.CreateElement(nodeName);
                telefinNode.AppendChild(destination);
            }

            Merge(destination, source, recursive, replace);
        }

        public void Merge(XmlNode destination, XmlNode source, bool recursive) {
            Merge(destination, source, recursive, false);
        }

        public void Merge(XmlNode destination, XmlNode source, bool recursive, bool replace) {
            foreach (XmlAttribute attribute in source.Attributes) {
                if (destination.Attributes[attribute.Name] == null) {
                    XmlAttribute newAttribute = destination.OwnerDocument.CreateAttribute(attribute.Name);
                    newAttribute.Value = attribute.Value;
                    destination.Attributes.Append(newAttribute);
                }
                else {
                    if (replace) {
                        destination.Attributes[attribute.Name].Value = attribute.Value;
                    }
                }
            }

            for (int index = destination.Attributes.Count - 1; index >= 0; index--) {
                XmlAttribute attribute = destination.Attributes[index];
                if (source.Attributes[attribute.Name] == null) {
                    destination.Attributes.Remove(attribute);
                }
            }

            if (recursive) {
                foreach (XmlNode child in source.ChildNodes) {
                    XmlNode node = destination.SelectSingleNode(child.Name);
                    if (node == null) {
                        node = destination.OwnerDocument.CreateElement(child.Name);
                        destination.AppendChild(node);
                    }
                    Merge(node, child, recursive);
                }
            }
        }


        public void DoBackup(string path) {
            string backupName = String.Format("{0} {1:yyyy-MM-dd HH.mm.ss}{2}", Path.GetFileNameWithoutExtension(fileInfo.Name), DateTime.Now, Path.GetExtension(fileInfo.Name));
            fileInfo.CopyTo(Path.Combine(path, backupName), true);
            //FileSystemInfo[] files = diBackup.GetFileSystemInfos("system*.xml");
            //Array.Sort(files, new FilesDateComparer(FilesDateComparerOrderDirectionEnum.DESC));
        }

        public void Save() {
            base.Save(this.fileInfo.FullName);
        }
    }
}

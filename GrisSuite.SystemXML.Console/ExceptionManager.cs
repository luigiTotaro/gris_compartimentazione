﻿using System;

namespace GrisSuite.SystemXML.Console
{
    public class ExceptionManager
    {
        public static void HandleException(Exception ex)
        {
            LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, ex.Message);

            System.Console.WriteLine(String.Format("Si è verificato un errore. Controllare file {0} per maggiori dettagli.", LogUtility.LogFileName));
        }
    }
}

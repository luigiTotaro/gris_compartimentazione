﻿using System;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Xml.XPath;

namespace GrisSuite.SystemXML.Console {
    internal static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args) {
            //CONTROLLO PARAMETRI CONFIGURAZIONE
            string SystemTemFilePath = ConfigurationManager.AppSettings["SystemTemFilePath"];
            string SystemXmlFilePath = ConfigurationManager.AppSettings["SystemXmlFilePath"];
            string BackupDir = ConfigurationManager.AppSettings["BackupDir"];

            if (string.IsNullOrEmpty(SystemTemFilePath)) {
                ExceptionManager.HandleException(new ApplicationException(string.Format("AppSetting SystemTemFilePath mancante nel file app.cofig")));
                return;
            }
            if (string.IsNullOrEmpty(SystemXmlFilePath)) {
                ExceptionManager.HandleException(new ApplicationException(string.Format("AppSetting SystemXmlFilePath mancante nel file app.cofig")));
                return;
            }
            if (string.IsNullOrEmpty(BackupDir)) {
                ExceptionManager.HandleException(new ApplicationException(string.Format("AppSetting BackupDir mancante nel file app.cofig")));
                return;
            }

            SystemFileDocument fiTEM = null;
            SystemFileDocument fiXML = null;

            try {
                fiTEM = new SystemFileDocument(SystemTemFilePath);
                fiXML = new SystemFileDocument(SystemXmlFilePath);
            }
            catch (Exception ex) {
                ExceptionManager.HandleException(ex);
                return;
            }

            try {
                fiTEM.Load();
                fiXML.ReplaceRoot(fiTEM.XmlNode.OuterXml);
                fiXML.Load();
            }
            catch (Exception ex) {
                ExceptionManager.HandleException(ex);
                return;
            }

            DirectoryInfo diBackup = new DirectoryInfo(BackupDir);
            if (!diBackup.Exists) {
                try {
                    diBackup.Create();
                }
                catch (Exception ex) {
                    ExceptionManager.HandleException(ex);
                    return;
                }
            }


            //BACKUP DEL FILE
            try {
                fiXML.DoBackup(diBackup.FullName);
            }
            catch (Exception ex) {
                ExceptionManager.HandleException(ex);
                return;
            }

            XmlNode temNode = null;

            fiXML.XmlNode.Value = fiTEM.XmlNode.Value;
            fiXML.Merge(fiXML.TelefinNode, fiTEM.TelefinNode, false, true);

            string[] nodes = ConfigurationManager.AppSettings["Replace"].Split(',');
            foreach (string node in nodes) {
                temNode = fiTEM.TelefinNode.SelectSingleNode(node);

                if (temNode == null) {
                    ExceptionManager.HandleException(new ApplicationException(string.Format("Il nodo {0} non è presente nel file System.tem", node)));
                    continue;
                }

                fiXML.Replace(node, temNode.InnerXml);
            }

            nodes = ConfigurationManager.AppSettings["Merge"].Split(',');
            foreach (string node in nodes) {
                temNode = fiTEM.TelefinNode.SelectSingleNode(node);

                if (temNode == null) {
                    ExceptionManager.HandleException(new ApplicationException(string.Format("Il nodo {0} non è presente nel file System.tem", node)));
                    continue;
                }

                fiXML.Merge(node, temNode, true);
            }

            fiXML.Save();
        }
    }
}

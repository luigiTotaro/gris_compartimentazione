﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace GrisSuite.TelnetSupervisorService {
    static class Program {
        static void Main() {
#if (!DEBUG)
            // Se compilato in modalità Release è un servizio windows
            ServiceBase[] ServicesToRun = new ServiceBase[] 
                                          { 
                                              new TelnetSupervisorService() 
                                          };
            ServiceBase.Run(ServicesToRun);
#else
            // Se compilato in modalità Debug è una applicazione console (lo stop corrisponde con la chiusura dell'applicazione)
            TelnetSupervisorService service = new TelnetSupervisorService();
            service.Start();
#endif
        }
    }
}

﻿namespace GrisSuite.TelnetSupervisorService {
    partial class ProjectInstaller {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TelnetSupervisorServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.TelnetSupervisorServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // TelnetSupervisorServiceProcessInstaller
            // 
            this.TelnetSupervisorServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.TelnetSupervisorServiceProcessInstaller.Password = null;
            this.TelnetSupervisorServiceProcessInstaller.Username = null;
            // 
            // TelnetSupervisorServiceInstaller
            // 
            this.TelnetSupervisorServiceInstaller.Description = "GrisSuite Telnet Supervisor Service";
            this.TelnetSupervisorServiceInstaller.DisplayName = "STLC Telnet Supervisor Service";
            this.TelnetSupervisorServiceInstaller.ServiceName = "TelnetSupervisorService";
            this.TelnetSupervisorServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.TelnetSupervisorServiceProcessInstaller,
            this.TelnetSupervisorServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller TelnetSupervisorServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller TelnetSupervisorServiceInstaller;
    }
}
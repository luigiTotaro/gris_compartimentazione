﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using GrisSuite.TelnetSupervisorLibrary;
using GrisSuite.TelnetSupervisorLibrary.Database;
using GrisSuite.TelnetSupervisorLibrary.Devices.Prase;
using GrisSuite.TelnetSupervisorLibrary.Telnet;
using GrisSuite.TelnetSupervisorService.Properties;
using Timer = System.Timers.Timer;

namespace GrisSuite.TelnetSupervisorService {
    public partial class TelnetSupervisorService : ServiceBase {
        #region Costanti

        /// <summary>
        /// Indica il nome dell'assembly che contiene le classi con le definizioni
        /// </summary>
        private const string LIBRARY_ASSEMBLY_NAME = "GrisSuite.TelnetSupervisorLibrary";

        /// <summary>
        /// Indica il namespace delle classi dei dispositivi
        /// </summary>
        private const string DEFINITIONS_NAMESPACE = "GrisSuite.TelnetSupervisorLibrary.Devices.";

        /// <summary>
        /// Indica la versione dell'assembly che contiene le classi con le definizioni dei dispositivi
        /// </summary>
        private static readonly Version LIBRARY_ASSEMBLY_VERSION = new Version(1, 0, 0, 0);

        #endregion

        #region Campi Privati

        private static SqlConnection dbConnection;
        private static SqlConnection dbEventsConnection;
        private static List<DeviceObject> devices;
        private static Timer pollingTimer;
        private static bool processingInProgress;

        #endregion

        #region Costruttore

        public TelnetSupervisorService() {
            InitializeComponent();
        }

        #endregion

        #region Start del servizio

        protected override void OnStart(string[] args) {
            ConnectionStringSettings localDatabaseConnectionString = null;
            ConnectionStringSettings localDatabaseEventsConnectionString = null;

            try {
                localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
                localDatabaseEventsConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabaseEvents"];
            }
            catch (ConfigurationErrorsException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, "Configurazione non corretta. " + ex.Message);
            }

            if (localDatabaseConnectionString == null) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida.");
            }
            else {
                try {
                    dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                }
            }

            if (localDatabaseEventsConnectionString == null) {
                if (dbConnection != null) {
                    dbEventsConnection = dbConnection;
                }
            }
            else {
                try {
                    dbEventsConnection = new SqlConnection(localDatabaseEventsConnectionString.ConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database degli eventi non è valida. " + ex.Message);
                }
            }

            if ((dbConnection != null) && (dbEventsConnection != null)) {
                if (LogUtility.LogLevel >= LogUtility.LoggingLevel.Info) {
                    int maxWorkerThreads;
                    int maxCompletionPortThreads;

                    ThreadPool.GetMaxThreads(out maxWorkerThreads, out maxCompletionPortThreads);
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Info,
                                                  String.Format("Numero massimo worker threads: {0}, Numero massimo I/O completion threads: {1}", maxWorkerThreads,
                                                                maxCompletionPortThreads));
                }

                devices = DataBaseUtility.GetAVDigitalDomDeviceList(dbConnection.ConnectionString, false);

                if (devices.Count > 0) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Info,
                                                  String.Format("Caricati {0} dispositivi da base dati per il monitoraggio. Il polling impostato è di {1} secondi.", devices.Count,
                                                                Settings.Default.PollingIntervalSeconds));

                    pollingTimer = new Timer {Interval = (Settings.Default.PollingIntervalSeconds*1000)};
                    pollingTimer.Elapsed += OnPollingTimerElapsed;
                }
                else {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Warning, "Non sono disponibili dispositivi da monitorare nel database indicato.");
                }

                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Info, "Avvio monitoraggio.");

#if (!DEBUG)
                if (pollingTimer != null) {
                    pollingTimer.Start();
                }
#else
                if (devices.Count > 0) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Warning, "Esecuzione recupero singolo di dati (modalità debug).");

                    PollDevices();

                    // In fase di debug lasciamo vari secondi per il completamento dei thread, da aumentare all'occorrenza
                    Thread.Sleep(((int) Settings.Default.PollingIntervalSeconds - 1)*1000);
                }

                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Info, "Fine monitoraggio.");
#endif
            }
        }

        #endregion

        #region Gestione eventi tick del timer

        private static void OnPollingTimerElapsed(object source, ElapsedEventArgs e) {
            PollDevices();
        }

        #endregion

        #region Elaborazione eventi tick del timer e accodamento dei thread

        private static void PollDevices() {
            // Evita le chiamate nidificate
            if (processingInProgress) {
                return;
            }

            processingInProgress = true;

            pollingTimer.Stop();

            if (LogUtility.LogLevel >= LogUtility.LoggingLevel.Verbose) {
                LogUtility.RecycleLogFile();
            }

            if ((dbConnection != null) && (dbEventsConnection != null)) {
                lock (devices) {
                    devices = DataBaseUtility.GetAVDigitalDomDeviceList(dbConnection.ConnectionString);
                }

                if ((devices != null) && (devices.Count > 0)) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Info,
                                                  String.Format("Caricati {0} dispositivi da base dati per il monitoraggio. Il polling impostato è di {1} secondi.", devices.Count,
                                                                Settings.Default.PollingIntervalSeconds));

                    foreach (DeviceObject device in devices) {
                        LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Verbose,
                                                      String.Format("Nome dispositivo: {0}, Device ID: {1}, IP Address: {2}, Type: {3}", device.Name, device.DeviceID,
                                                                    device.Address, device.Type));

                        device.LastEvent = DataBaseUtility.GetLastEventByDevice(dbEventsConnection.ConnectionString, device.DeviceID);

                        ThreadPool.QueueUserWorkItem(ProcessAVDigitalDevice, device);
                    }

                    if (LogUtility.LogLevel >= LogUtility.LoggingLevel.Verbose) {
                        int workerThreads;
                        int completionPortThreads;

                        ThreadPool.GetAvailableThreads(out workerThreads, out completionPortThreads);
                        LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Verbose,
                                                      String.Format("Worker threads liberi: {0}, I/O completion threads liberi: {1}", workerThreads, completionPortThreads));
                    }
                }
                else {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Warning, "Non sono disponibili dispositivi da monitorare nel database indicato.");
                }
            }

            processingInProgress = false;

            pollingTimer.Start();
        }

        #endregion

        #region Creazione istanze dispositivi in base a lettura da DB, recupero dati ed inserimento in base dati - Metodo lanciato dai thread

        private static void ProcessAVDigitalDevice(object state) {
            var deviceObject = state as DeviceObject;
            IDigitalOutputModule device = null;

            if (deviceObject != null) {
                try {
                    // Il caricamento delle classi dei dispositivi si basa sui namespace e quindi dipende in modo statico da come è scritto il codice
                    // Da adeguare in caso di modifiche a nomi classi o namespace
                    Type deviceInstance =
                        Type.GetType(
                            DEFINITIONS_NAMESPACE + deviceObject.VendorName + "." + deviceObject.Type + ", " + LIBRARY_ASSEMBLY_NAME + ", Version=" + LIBRARY_ASSEMBLY_VERSION,
                            false, true);

                    if (deviceInstance != null) {
                        device =
                            (IDigitalOutputModule)
                            Activator.CreateInstance(deviceInstance,
                                                     new object[] {
                                                                      deviceObject.Name, deviceObject.Type, deviceObject.EndPoint, Settings.Default.DomUsername, Settings.Default.DomPassword,
                                                                      Settings.Default.TelnetLoginMarker, Settings.Default.TelnetPasswordMarker, Settings.Default.TelnetPromptMarker,
                                                                      deviceObject.LastEvent
                                                                  });
                        if (device.LastError != null) {
                            if (device.LastError is DomConfigurationHelperException) {
                                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, device.LastError.Message);
                            }
                            else if (device.LastError is TelnetException) {
                                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, device.LastError.Message);
                            }
                            else if (device.LastError is DomParserException) {
                                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, device.LastError.Message);
                            }
                        }
                        else {
                            device.Populate();

                            if (device.LastError != null) {
                                if (device.LastError is DomConfigurationHelperException) {
                                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, device.LastError.Message);
                                }
                                else if (device.LastError is TelnetException) {
                                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, device.LastError.Message);
                                }
                                else if (device.LastError is DomParserException) {
                                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Critical, device.LastError.Message);
                                }
                            }
                        }
                    }
                    else {
                        LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Warning,
                                                      String.Format(
                                                          "Impossibile caricare una classe che implementi il dispositivo di tipo {0}. Verificare il codice del dispositivo o le definizioni di classi.",
                                                          DEFINITIONS_NAMESPACE + deviceObject.VendorName + "." + deviceObject.Type));
                    }
                }
                catch (ArgumentNullException) {
                }
                catch (ArgumentException) {
                }
                catch (TargetInvocationException) {
                }
                catch (TypeLoadException) {
                }
                catch (FileNotFoundException) {
                }
                catch (FileLoadException) {
                }
                catch (BadImageFormatException) {
                }
                catch (NotSupportedException) {
                }
                catch (MethodAccessException) {
                }
                catch (MemberAccessException) {
                }
                catch (InvalidComObjectException) {
                }
                catch (COMException) {
                }
                catch (InvalidCastException) {
                }

                if ((device != null) && (device.LastError == null)) {
                    DataBaseUtility.UpdateDeviceStatus(dbConnection.ConnectionString, deviceObject.DeviceID, (int) device.SeverityLevel, device.ValueDescriptionComplete,
                                                       device.Offline);
                    foreach (DBStream stream in device.StreamData) {
                        DataBaseUtility.UpdateStream(dbConnection.ConnectionString, deviceObject.DeviceID, stream.StreamID, stream.Name, (int) stream.SeverityLevel,
                                                     stream.ValueDescriptionComplete);

                        foreach (DBStreamField field in stream.FieldData) {
                            DataBaseUtility.UpdateStreamField(dbConnection.ConnectionString, deviceObject.DeviceID, stream.StreamID, field.FieldID, field.ArrayID, field.Name,
                                                              (int) field.SeverityLevel, field.Value, field.ValueDescriptionComplete);
                        }
                    }

                    if (device.EventsData != null) {
                        foreach (EventObject evnt in device.EventsData) {
                            DataBaseUtility.UpdateEvent(dbEventsConnection.ConnectionString, deviceObject.DeviceID, evnt.EventDataBytes, evnt.Created);
                        }
                    }
                }
            }
        }

        #endregion

        #region Stop del servizio

        protected override void OnStop() {
            if (pollingTimer != null) {
                pollingTimer.Stop();
            }

            LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Info, "Fine monitoraggio.");
        }

        #endregion

        #region Metodo di avvio forzato del servizio da usare per debug

#if (DEBUG)
        public void Start() {
            OnStart(null);
        }
#endif

        #endregion
    }
}
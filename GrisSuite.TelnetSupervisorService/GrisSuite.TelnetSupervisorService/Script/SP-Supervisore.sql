CREATE SCHEMA supervisor AUTHORIZATION dbo
GO
IF OBJECT_ID('[supervisor].[DeviceStatus_Upd]') IS NOT NULL
BEGIN 
    DROP PROCEDURE [supervisor].[DeviceStatus_Upd]
END 
GO
CREATE PROCEDURE [supervisor].[DeviceStatus_Upd]
(
	@DevID bigint,
	@SevLevel int,
	@Description varchar(256),
	@Offline tinyint
)
AS
SET NOCOUNT ON; 
SET XACT_ABORT ON;

BEGIN TRAN;

IF EXISTS (SELECT [DevID] FROM [device_status] WHERE [DevID] = @DevID)
BEGIN
	UPDATE [device_status] 
	SET
	[SevLevel] = @SevLevel,
	[Description] = @Description,
	[Offline] = @Offline 
	WHERE [DevID] = @DevID;
END
ELSE
BEGIN
	INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline]) 
	VALUES (@DevID, @SevLevel, @Description, @Offline);
END

COMMIT;
GO
IF OBJECT_ID('[supervisor].[Stream_Upd]') IS NOT NULL
BEGIN 
    DROP PROCEDURE [supervisor].[Stream_Upd]
END 
GO
CREATE PROCEDURE [supervisor].[Stream_Upd]
(
	@DevID bigint,
	@StrID int,
	@Name varchar(64),
	@SevLevel int,
	@Description text
)
AS
SET NOCOUNT ON; 
SET XACT_ABORT ON;

BEGIN TRAN;
	
IF EXISTS (SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID))
BEGIN
	UPDATE [streams]
	SET
	[Name] = @Name,
	[Data] = NULL,
	[DateTime] = GetDate(),
	[SevLevel] = @SevLevel,
	[Description] = @Description
	WHERE (([DevID] = @DevID) AND ([StrID] = @StrID));
END
ELSE
BEGIN
	INSERT INTO [streams] ([DevID], [StrID], [Name], [Data], [DateTime], [SevLevel], [Description], [Visible])
	VALUES (@DevID, @StrID, @Name, NULL, GetDate(), @SevLevel, @Description, 1)
END
COMMIT;
GO
IF OBJECT_ID('[supervisor].[StreamFields_Upd]') IS NOT NULL
BEGIN 
    DROP PROCEDURE [supervisor].[StreamFields_Upd]
END 
GO
CREATE PROCEDURE [supervisor].[StreamFields_Upd]
(
	@FieldID int,
	@ArrayID int,
	@StrID int,
	@DevID bigint,
	@Name varchar(64),
	@SevLevel int,
	@Value varchar(1024),
	@Description text
)
AS
SET NOCOUNT ON; 
SET XACT_ABORT ON;

BEGIN TRAN;

IF EXISTS (SELECT FieldID FROM stream_fields WHERE (DevID = @DevID) AND (StrID = @StrID) AND (FieldID = @FieldID) AND (ArrayID = @ArrayID))
BEGIN
	UPDATE [stream_fields]
	SET
	[Name] = @Name,
	[SevLevel] = @SevLevel,
	[Value] = @Value,
	[Description] = @Description
	WHERE (([DevID] = @DevID) AND ([StrID] = @StrID) AND ([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID));
END
ELSE
BEGIN
	INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], ReferenceID)
	VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, 1, NULL);
END

COMMIT;
GO
IF OBJECT_ID('[supervisor].[PraseDomDevices_Get]') IS NOT NULL
BEGIN 
    DROP PROCEDURE [supervisor].[PraseDomDevices_Get]
END 
GO
CREATE PROCEDURE [supervisor].[PraseDomDevices_Get]
AS
SET NOCOUNT ON;

SELECT devices.DevID, devices.[Name], devices.[TYPE], devices.Addr, rack.RackName, building.BuildingName, building.BuildingDescription, station.StationName,
regions.RegID, regions.Name AS RegionName, zones.ZonID, zones.Name AS ZoneName, nodes.NodID, nodes.Name AS NodeName, vendors.VendorName
FROM devices
INNER JOIN rack ON devices.RackID = rack.RackID
INNER JOIN building ON rack.BuildingID = building.BuildingID
INNER JOIN station ON building.StationID = station.StationID
INNER JOIN device_type ON devices.TYPE = device_type.DeviceTypeID
INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
INNER JOIN nodes ON devices.NodID = nodes.NodID
INNER JOIN zones ON nodes.ZonID = zones.ZonID
INNER JOIN regions ON zones.RegID = regions.RegID
WHERE (device_type.VendorID = 5 /* Prase */)
-- AND (device_type.TechnologyID = 7 /* AV-DIGITAL */)
AND (device_type.DeviceTypeID LIKE 'PEAVDOM')
GO
/*
-- Creazione tipo periferica e dati di test
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[device_type]([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID])
SELECT N'PEAVDOM', 1, 5, N'Prase PEAVDOM - Diffusione Sonora AV Digital - DOM', NULL, 13, 7

INSERT INTO [dbo].[devices]([DevID], [NodID], [SrvID], [Name], [Type], [SN], [Addr], [PortId], [ProfileID], [Active], [Scheduled], [RackID], [RackPositionRow], [RackPositionCol], [DefinitionVersion], [ProtocolDefinitionVersion])
SELECT 12106357572698120, 2933574139911, 87228468, N'Unit� DOM 1 Rack 3', N'PEAVDOM', N'00.00.000', N'3232235621', NULL, 1, 1, 0, N'9b8086d7-2782-4327-8991-f426841202b1', 1, 1, NULL, NULL

COMMIT;
GO
*/
IF OBJECT_ID('[supervisor].[PraseDomEvents_Ins]') IS NOT NULL
BEGIN 
    DROP PROCEDURE [supervisor].[PraseDomEvents_Ins]
END 
GO
CREATE PROCEDURE [supervisor].[PraseDomEvents_Ins]
(
	@DevID bigint,
	@EventData varbinary(max),
	@Created datetime,
	@EventCategory tinyint
)
AS
SET NOCOUNT ON; 
SET XACT_ABORT ON;

BEGIN TRAN;

INSERT INTO [events] ([DevID], [EventData], [Created], [EventCategory])
VALUES (@DevID, @EventData, @Created, @EventCategory)

COMMIT;
GO
IF OBJECT_ID('[supervisor].[PraseDomEvents_Get]') IS NOT NULL
BEGIN 
    DROP PROCEDURE [supervisor].[PraseDomEvents_Get]
END 
GO
CREATE PROCEDURE [supervisor].[PraseDomEvents_Get] (@DevID BIGINT, @EventCategory TINYINT)
AS
SET NOCOUNT ON;

SELECT TOP 1
[EventID]
,[EventData]
,[Created]
FROM [events]
WHERE (DevID = @DevID)
AND ([EventCategory] = @EventCategory)
ORDER BY [Created] DESC
GO

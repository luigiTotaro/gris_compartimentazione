﻿using System.Collections.Generic;
using System.Text;

namespace GrisSuite.TelnetSupervisorLibrary.Database {
    /// <summary>
    /// Rappresenta il singolo Stream da persistere su database
    /// </summary>
    public class DBStream {
        #region Variabili private

        private readonly string name;
        private readonly int streamID;
        private List<DBStreamField> streamData;

        #endregion

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="streamID">ID dello Stream</param>
        /// <param name="name">Nome dello Stream</param>
        public DBStream(int streamID, string name) {
            this.streamID = streamID;
            this.name = name;
            streamData = new List<DBStreamField>();
        }

        #region Proprietà pubbliche

        /// <summary>
        /// Nome dello Stream
        /// </summary>
        public string Name {
            get { return name; }
        }

        /// <summary>
        /// Lista dei Field contenuti nello Stream
        /// </summary>
        public List<DBStreamField> FieldData {
            get { return streamData; }
            set { streamData = value; }
        }

        /// <summary>
        /// ID dello Stream
        /// </summary>
        public int StreamID {
            get { return streamID; }
        }

        /// <summary>
        /// Severità relativa allo Stream (come aggregata dei Field contenuti)
        /// </summary>
        public Severity SeverityLevel {
            get {
                Severity streamSeverity = Severity.Unknown;

                foreach (DBStreamField field in streamData) {
                    if ((field.SeverityLevel != Severity.Unknown) && ((streamSeverity == Severity.Unknown) || ((byte) field.SeverityLevel > (byte) streamSeverity))) {
                        streamSeverity = field.SeverityLevel;
                    }
                }

                return streamSeverity;
            }
        }

        /// <summary>
        /// Decodifica descrittiva del valore dello Stream, come concatenazione delle descrizioni dei singoli Field, se la loro severità è non sconosciuta
        /// </summary>
        public string ValueDescriptionComplete {
            get {
                var deviceDescription = new StringBuilder();

                int fieldCounter = 0;

                foreach (DBStreamField field in streamData) {
                    if (field.SeverityLevel != Severity.Unknown) {
                        if (fieldCounter > 0) {
                            deviceDescription.Append(";" + field.ValueDescriptionComplete);
                        }
                        else {
                            deviceDescription.Append(field.ValueDescriptionComplete);
                        }
                        fieldCounter++;
                    }
                }

                return deviceDescription.ToString();
            }
        }

        #endregion
    }
}
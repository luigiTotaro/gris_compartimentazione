﻿namespace GrisSuite.TelnetSupervisorLibrary.Database {
    /// <summary>
    /// Rappresenta il tipo di valori ad intervallo Minimo-Massimo
    /// </summary>
    public class DBStreamFieldValueRange {
        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="minValue">Valore minimo</param>
        /// <param name="maxValue">Valore massimo</param>
        public DBStreamFieldValueRange(int? minValue, int? maxValue) {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="value">Valore singolo (inteso come minimo e massimo uguali)</param>
        public DBStreamFieldValueRange(int value) {
            MinValue = value;
            MaxValue = value;
        }

        /// <summary>
        /// Valore minimo
        /// </summary>
        public int? MinValue { get; private set; }

        /// <summary>
        /// Valore massimo
        /// </summary>
        public int? MaxValue { get; private set; }
    }
}
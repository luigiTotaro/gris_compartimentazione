﻿namespace GrisSuite.TelnetSupervisorLibrary.Database {
    /// <summary>
    /// Rappresenta gli oggetti indicati per la decodifica dei valori
    /// </summary>
    public class DBStreamFieldValueDescription {
        /// <summary>
        /// Costruttore privato
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="isDefault">Indica se è il valore predefinito</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="exactStringValueToCompare">Valore di tipo stringa da confrontare con quello recuperato</param>
        /// <param name="exactIntValueToCompare">Valore di tipo intero da confrontare con quello recuperato</param>
        /// <param name="rangeValueToCompare">Valore di tipo intervallo da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, bool isDefault, string descriptionIfMatch, Severity severityLevelIfMatch, string exactStringValueToCompare,
                                             int? exactIntValueToCompare, DBStreamFieldValueRange rangeValueToCompare) {
            Field = field;
            IsDefault = isDefault;
            DescriptionIfMatch = descriptionIfMatch;
            SeverityLevelIfMatch = severityLevelIfMatch;
            ExactStringValueToCompare = exactStringValueToCompare;
            ExactIntValueToCompare = exactIntValueToCompare;

            this.RangeValueToCompare = rangeValueToCompare ?? new DBStreamFieldValueRange(null, null);
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="isDefault">Indica se è il valore predefinito</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="exactStringValueToCompare">Valore di tipo stringa da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, bool isDefault, string descriptionIfMatch, Severity severityLevelIfMatch, string exactStringValueToCompare)
            : this(field, isDefault, descriptionIfMatch, severityLevelIfMatch, exactStringValueToCompare, null, null) {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="exactStringValueToCompare">Valore di tipo stringa da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, string descriptionIfMatch, Severity severityLevelIfMatch, string exactStringValueToCompare)
            : this(field, false, descriptionIfMatch, severityLevelIfMatch, exactStringValueToCompare, null, null) {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="isDefault">Indica se è il valore predefinito</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="exactIntValueToCompare">Valore di tipo intero da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, bool isDefault, string descriptionIfMatch, Severity severityLevelIfMatch, int? exactIntValueToCompare)
            : this(field, isDefault, descriptionIfMatch, severityLevelIfMatch, null, exactIntValueToCompare, null) {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="exactIntValueToCompare">Valore di tipo intero da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, string descriptionIfMatch, Severity severityLevelIfMatch, int? exactIntValueToCompare)
            : this(field, false, descriptionIfMatch, severityLevelIfMatch, null, exactIntValueToCompare, null) {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="isDefault">Indica se è il valore predefinito</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="rangeValueToCompare">Valore di tipo intervallo da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, bool isDefault, string descriptionIfMatch, Severity severityLevelIfMatch,
                                             DBStreamFieldValueRange rangeValueToCompare)
            : this(field, isDefault, descriptionIfMatch, severityLevelIfMatch, null, null, rangeValueToCompare) {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="rangeValueToCompare">Valore di tipo intervallo da confrontare con quello recuperato</param>
        public DBStreamFieldValueDescription(DBStreamField field, string descriptionIfMatch, Severity severityLevelIfMatch, DBStreamFieldValueRange rangeValueToCompare)
            : this(field, false, descriptionIfMatch, severityLevelIfMatch, null, null, rangeValueToCompare) {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="field">DBStreamField da valutare</param>
        /// <param name="descriptionIfMatch">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        /// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
        public DBStreamFieldValueDescription(DBStreamField field, string descriptionIfMatch, Severity severityLevelIfMatch)
            : this(field, true, descriptionIfMatch, severityLevelIfMatch, null, null, null) {
        }

        /// <summary>
        /// Field relativo alla decodifica dei valori
        /// </summary>
        public DBStreamField Field { get; private set; }

        /// <summary>
        /// Indica se il valore è quello predefinito
        /// </summary>
        public bool IsDefault { get; private set; }

        /// <summary>
        /// Descrizione assegnata al valore del Field nel caso di corrispondenza con le regole indicate
        /// </summary>
        public string DescriptionIfMatch { get; private set; }

        /// <summary>
        /// Severità assegnata al valore del Field nel caso di corrispondenza con le regole indicate
        /// </summary>
        public Severity SeverityLevelIfMatch { get; private set; }

        /// <summary>
        /// Valore statico di tipo stringa da confrontare con il valore recuperato
        /// </summary>
        public string ExactStringValueToCompare { get; private set; }

        /// <summary>
        /// Valore statico di tipo intero da confrontare con il valore recuperato
        /// </summary>
        public int? ExactIntValueToCompare { get; private set; }

        /// <summary>
        /// Valore statico di tipo intervallo da confrontare con il valore recuperato
        /// </summary>
        public DBStreamFieldValueRange RangeValueToCompare { get; private set; }
    }
}
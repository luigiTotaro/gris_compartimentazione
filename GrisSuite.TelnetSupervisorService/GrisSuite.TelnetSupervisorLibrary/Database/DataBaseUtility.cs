﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Threading;
using GrisSuite.TelnetSupervisorLibrary.Properties;

namespace GrisSuite.TelnetSupervisorLibrary.Database {
    /// <summary>
    /// Rappresenta la classe di utility per l'accesso al database
    /// </summary>
    public class DataBaseUtility {
        #region Metodi pubblici

        #region UpdateDeviceStatus - Aggiorna lo stato del dispositivo

        /// <summary>
        /// Aggiorna lo stato del dispositivo
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="devID">ID del Device</param>
        /// <param name="sevLevel">Livello di severità del Device</param>
        /// <param name="descriptionStatus">Stato del Device</param>
        /// <param name="offline">Indica se il Device è fuori linea o non raggiungibile</param>
        public static void UpdateDeviceStatus(string dbConnectionString, long devID, int sevLevel, string descriptionStatus, byte offline) {
            SqlConnection dbConnection = null;
            var cmd = new SqlCommand();

            try {
                try {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "[supervisor].[DeviceStatus_Upd]";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, devID));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, sevLevel));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, descriptionStatus));
                cmd.Parameters.Add(new SqlParameter("@Offline", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null, DataRowVersion.Current, offline));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (ThreadAbortException) {
                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
            finally {
                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
        }

        #endregion

        #region UpdateStream - Aggiorna lo Stream

        /// <summary>
        /// Aggiorna lo Stream
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="devID">ID del Device</param>
        /// <param name="strID">ID dello Stream</param>
        /// <param name="name">Nome dello Stream</param>
        /// <param name="sevLevel">Livello di severità dello Stream</param>
        /// <param name="description">Descrizione del valore dello Stream</param>
        public static void UpdateStream(string dbConnectionString, long devID, int strID, string name, int sevLevel, string description) {
            SqlConnection dbConnection = null;
            var cmd = new SqlCommand();

            try {
                try {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "[supervisor].[Stream_Upd]";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, devID));
                cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, strID));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, name));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, sevLevel));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, description));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (ThreadAbortException) {
                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
            finally {
                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
        }

        #endregion

        #region UpdateStreamField - Aggiorna il Field

        /// <summary>
        /// Aggiorna il Field
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="devID">ID del Device</param>
        /// <param name="strID">ID dello Stream</param>
        /// <param name="fieldID">ID del Field</param>
        /// <param name="arrayID">ID del vettore dei valori del Field</param>
        /// <param name="name">Nome del Field</param>
        /// <param name="sevLevel">Livello di severità del Field</param>
        /// <param name="value">Valore del Field</param>
        /// <param name="description">Descrizione del valore del Field</param>
        public static void UpdateStreamField(string dbConnectionString, long devID, int strID, int fieldID, int arrayID, string name, int sevLevel, string value, string description) {
            SqlConnection dbConnection = null;
            var cmd = new SqlCommand();

            try {
                try {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "[supervisor].[StreamFields_Upd]";
                cmd.Parameters.Add(new SqlParameter("@FieldID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, fieldID));
                cmd.Parameters.Add(new SqlParameter("@ArrayID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, arrayID));
                cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, strID));
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, devID));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, name));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current, sevLevel));
                cmd.Parameters.Add(new SqlParameter("@Value", SqlDbType.VarChar, 1024, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, value));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, description));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (ThreadAbortException) {
                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
            finally {
                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
        }

        #endregion

        #region GetMonitorDeviceList - Recupera la lista dei dispositivi da monitorare

        /// <summary>
        /// Recupera la lista dei dispositivi da monitorare
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <returns>Lista di DeviceObject da monitorare</returns>
        public static List<DeviceObject> GetMonitorDeviceList(string dbConnectionString) {
            SqlConnection dbConnection = null;
            var cmd = new SqlCommand();
            SqlDataReader drDevices = null;
            var devices = new List<DeviceObject>();

            try {
                try {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return null;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[supervisor].[MonitorDevices_Get]";
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevices = cmd.ExecuteReader();

                if ((drDevices != null) && (drDevices.Read())) {
                    int colDevID = drDevices.GetOrdinal("DevID");
                    int colName = drDevices.GetOrdinal("Name");
                    int colType = drDevices.GetOrdinal("Type");
                    int colAddr = drDevices.GetOrdinal("Addr");
                    int colRackName = drDevices.GetOrdinal("RackName");
                    int colBuildingName = drDevices.GetOrdinal("BuildingName");
                    int colBuildingDescription = drDevices.GetOrdinal("BuildingDescription");
                    int colStationName = drDevices.GetOrdinal("StationName");
                    int colRegID = drDevices.GetOrdinal("RegID");
                    int colRegionName = drDevices.GetOrdinal("RegionName");
                    int colZonID = drDevices.GetOrdinal("ZonID");
                    int colZoneName = drDevices.GetOrdinal("ZoneName");
                    int colNodID = drDevices.GetOrdinal("NodID");
                    int colNodeName = drDevices.GetOrdinal("NodeName");
                    int colVendorName = drDevices.GetOrdinal("VendorName");

                    do {
                        IPAddress ip = DecodeIPAddress(drDevices.GetSqlString(colAddr).Value);

                        if (ip != null) {
                            devices.Add(new DeviceObject(drDevices.GetSqlInt64(colDevID).Value, drDevices.GetSqlString(colName).Value, drDevices.GetSqlString(colType).Value, ip,
                                                         drDevices.GetSqlString(colRackName).Value, drDevices.GetSqlString(colBuildingName).Value,
                                                         drDevices.GetSqlString(colBuildingDescription).Value, drDevices.GetSqlString(colStationName).Value,
                                                         drDevices.GetSqlInt64(colRegID).Value, drDevices.GetSqlString(colRegionName).Value, drDevices.GetSqlInt64(colZonID).Value,
                                                         drDevices.GetSqlString(colZoneName).Value, drDevices.GetSqlInt64(colNodID).Value, drDevices.GetSqlString(colNodeName).Value,
                                                         drDevices.GetSqlString(colVendorName).Value));

                            LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Verbose,
                                                          String.Format(
                                                              "Caricato Dispositivo: {0}, Device ID: {1}, Address: {2}, Tipo: {3}, Nome Rack: {4}, Nome Fabbricato: {5}, Descrizione Fabbricato: {6}, Nome Stazione: {7}, " +
                                                              "Region ID: {8}, Region Name: {9}, Zone ID: {10}, Zone Name: {11}, Node ID: {12}, Node Name: {13}, Produttore: {14}",
                                                              drDevices.GetSqlString(colName).Value, drDevices.GetSqlInt64(colDevID).Value, ip,
                                                              drDevices.GetSqlString(colType).Value, drDevices.GetSqlString(colRackName).Value,
                                                              drDevices.GetSqlString(colBuildingName).Value, drDevices.GetSqlString(colBuildingDescription).Value,
                                                              drDevices.GetSqlString(colStationName).Value, drDevices.GetSqlInt64(colRegID).Value,
                                                              drDevices.GetSqlString(colRegionName).Value, drDevices.GetSqlInt64(colZonID).Value,
                                                              drDevices.GetSqlString(colZoneName).Value, drDevices.GetSqlInt64(colNodID).Value,
                                                              drDevices.GetSqlString(colNodeName).Value, drDevices.GetSqlString(colVendorName).Value));
                        }
                        else {
                            LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Warning,
                                                          String.Format("Errore durante la decodifica dell'Address {0} del Dispositivo {1} (Device ID: {2})",
                                                                        drDevices.GetSqlString(colAddr).Value, drDevices.GetSqlString(colName).Value,
                                                                        drDevices.GetSqlInt64(colDevID).Value));
                        }
                    } while (drDevices.Read());
                }
            }
            catch (SqlException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (IndexOutOfRangeException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error,
                                              "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione della stored procedure MonitorDevices_Get. " +
                                              ex.Message);
            }
            catch (ThreadAbortException) {
                if (drDevices != null && !drDevices.IsClosed) {
                    drDevices.Close();
                }

                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
            finally {
                if (drDevices != null && !drDevices.IsClosed) {
                    drDevices.Close();
                }

                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }

            return devices;
        }

        #endregion

        #region GetAVDigitalDomDeviceList - Recupera la lista dei dispositivi da monitorare

        /// <summary>
        /// Recupera la lista dei dispositivi da monitorare
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <returns>Lista di DeviceObject da monitorare</returns>
        public static List<DeviceObject> GetAVDigitalDomDeviceList(string dbConnectionString) {
            return GetAVDigitalDomDeviceList(dbConnectionString, true);
        }

        /// <summary>
        /// Recupera la lista dei dispositivi da monitorare
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="enableLog">Indica se attivare il log su file dei dispositivi caricati</param>
        /// <returns>Lista di DeviceObject da monitorare</returns>
        public static List<DeviceObject> GetAVDigitalDomDeviceList(string dbConnectionString, bool enableLog) {
            SqlConnection dbConnection = null;
            var cmd = new SqlCommand();
            SqlDataReader drDevices = null;
            var devices = new List<DeviceObject>();

            try {
                try {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return null;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[supervisor].[PraseDomDevices_Get]";
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevices = cmd.ExecuteReader();

                if ((drDevices != null) && (drDevices.Read())) {
                    int colDevID = drDevices.GetOrdinal("DevID");
                    int colName = drDevices.GetOrdinal("Name");
                    int colType = drDevices.GetOrdinal("Type");
                    int colAddr = drDevices.GetOrdinal("Addr");
                    int colRackName = drDevices.GetOrdinal("RackName");
                    int colBuildingName = drDevices.GetOrdinal("BuildingName");
                    int colBuildingDescription = drDevices.GetOrdinal("BuildingDescription");
                    int colStationName = drDevices.GetOrdinal("StationName");
                    int colRegID = drDevices.GetOrdinal("RegID");
                    int colRegionName = drDevices.GetOrdinal("RegionName");
                    int colZonID = drDevices.GetOrdinal("ZonID");
                    int colZoneName = drDevices.GetOrdinal("ZoneName");
                    int colNodID = drDevices.GetOrdinal("NodID");
                    int colNodeName = drDevices.GetOrdinal("NodeName");
                    int colVendorName = drDevices.GetOrdinal("VendorName");

                    do {
                        IPAddress ip = DecodeIPAddress(drDevices.GetSqlString(colAddr).Value);

                        if (ip != null) {
                            devices.Add(new DeviceObject(drDevices.GetSqlInt64(colDevID).Value, drDevices.GetSqlString(colName).Value, drDevices.GetSqlString(colType).Value, ip,
                                                         drDevices.GetSqlString(colRackName).Value, drDevices.GetSqlString(colBuildingName).Value,
                                                         drDevices.GetSqlString(colBuildingDescription).Value, drDevices.GetSqlString(colStationName).Value,
                                                         drDevices.GetSqlInt64(colRegID).Value, drDevices.GetSqlString(colRegionName).Value, drDevices.GetSqlInt64(colZonID).Value,
                                                         drDevices.GetSqlString(colZoneName).Value, drDevices.GetSqlInt64(colNodID).Value, drDevices.GetSqlString(colNodeName).Value,
                                                         drDevices.GetSqlString(colVendorName).Value));

                            if (enableLog) {
                                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Verbose,
                                                              String.Format(
                                                                  "Caricato Dispositivo: {0}, Device ID: {1}, Address: {2}, Tipo: {3}, Nome Rack: {4}, Nome Fabbricato: {5}, Descrizione Fabbricato: {6}, Nome Stazione: {7}, " +
                                                                  "Region ID: {8}, Region Name: {9}, Zone ID: {10}, Zone Name: {11}, Node ID: {12}, Node Name: {13}, Produttore: {14}",
                                                                  drDevices.GetSqlString(colName).Value, drDevices.GetSqlInt64(colDevID).Value, ip,
                                                                  drDevices.GetSqlString(colType).Value, drDevices.GetSqlString(colRackName).Value,
                                                                  drDevices.GetSqlString(colBuildingName).Value, drDevices.GetSqlString(colBuildingDescription).Value,
                                                                  drDevices.GetSqlString(colStationName).Value, drDevices.GetSqlInt64(colRegID).Value,
                                                                  drDevices.GetSqlString(colRegionName).Value, drDevices.GetSqlInt64(colZonID).Value,
                                                                  drDevices.GetSqlString(colZoneName).Value, drDevices.GetSqlInt64(colNodID).Value,
                                                                  drDevices.GetSqlString(colNodeName).Value, drDevices.GetSqlString(colVendorName).Value));
                            }
                        }
                        else {
                            LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Warning,
                                                          String.Format("Errore durante la decodifica dell'Address {0} del Dispositivo {1} (Device ID: {2})",
                                                                        drDevices.GetSqlString(colAddr).Value, drDevices.GetSqlString(colName).Value,
                                                                        drDevices.GetSqlInt64(colDevID).Value));
                        }
                    } while (drDevices.Read());
                }
            }
            catch (SqlException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (IndexOutOfRangeException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error,
                                              "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione della stored procedure PraseDomDevices_Get. " +
                                              ex.Message);
            }
            catch (ThreadAbortException) {
                if (drDevices != null && !drDevices.IsClosed) {
                    drDevices.Close();
                }

                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
            finally {
                if (drDevices != null && !drDevices.IsClosed) {
                    drDevices.Close();
                }

                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }

            return devices;
        }

        #endregion

        #region GetLastEventByDevice - Recupera l'ultimo evento in ordine temporale di un dispositivo

        /// <summary>
        /// Recupera l'ultimo evento in ordine temporale di un dispositivo
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="devID">ID del Device</param>
        /// <returns>Ultimo EventObject disponibile</returns>
        public static EventObject GetLastEventByDevice(string dbConnectionString, long devID) {
            SqlConnection dbConnection = null;
            var cmd = new SqlCommand();
            SqlDataReader drLastEvent = null;
            EventObject evnt = null;

            try {
                try {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                    return null;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[supervisor].[PraseDomEvents_Get]";
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, devID));
                cmd.Parameters.Add(new SqlParameter("@EventCategory", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current,
                                                    Settings.Default.DomEventCategoryID));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drLastEvent = cmd.ExecuteReader();

                if ((drLastEvent != null) && (drLastEvent.Read())) {
                    int colEventID = drLastEvent.GetOrdinal("EventID");
                    int colEventData = drLastEvent.GetOrdinal("EventData");
                    int colCreated = drLastEvent.GetOrdinal("Created");

                    do {
                        evnt = new EventObject(devID, drLastEvent.GetSqlGuid(colEventID).Value, Encoding.ASCII.GetString(drLastEvent.GetSqlBinary(colEventData).Value),
                                               drLastEvent.GetSqlDateTime(colCreated).Value);

                        LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Verbose,
                                                      String.Format(
                                                          "Caricato ultimo evento disponibile per Device ID: {0}, Evento ID: {1}, Data di Creazione {2}, Dati Evento: {3}", devID,
                                                          drLastEvent.GetSqlGuid(colEventID).Value, drLastEvent.GetSqlDateTime(colCreated).Value,
                                                          Encoding.ASCII.GetString(drLastEvent.GetSqlBinary(colEventData).Value)));
                    } while (drLastEvent.Read());
                }
            }
            catch (SqlException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (IndexOutOfRangeException ex) {
                LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error,
                                              "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione della stored procedure PraseDomEvents_Get. " +
                                              ex.Message);
            }
            catch (ThreadAbortException) {
                if (drLastEvent != null && !drLastEvent.IsClosed) {
                    drLastEvent.Close();
                }

                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }
            finally {
                if (drLastEvent != null && !drLastEvent.IsClosed) {
                    drLastEvent.Close();
                }

                if (dbConnection != null) {
                    dbConnection.Close();
                }
            }

            return evnt;
        }

        #endregion

        #region UpdateEvent - Aggiorna / inserisce l'Evento

        /// <summary>
        ///  Aggiorna / inserisce l'Evento
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="devID">ID del Device</param>
        /// <param name="eventData">Dati dell'evento</param>
        /// <param name="created">Data di registrazione dell'evento</param>
        public static void UpdateEvent(string dbConnectionString, long devID, byte[] eventData, DateTime created) {
            if ((eventData != null) && (eventData.Length > 0)) {
                SqlConnection dbConnection = null;
                var cmd = new SqlCommand();

                try {
                    try {
                        dbConnection = new SqlConnection(dbConnectionString);
                    }
                    catch (ArgumentException ex) {
                        LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "La stringa di connessione al database non è valida. " + ex.Message);
                        return;
                    }

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                    cmd.CommandText = "[supervisor].[PraseDomEvents_Ins]";
                    cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null, DataRowVersion.Current, devID));
                    cmd.Parameters.Add(new SqlParameter("@EventData", SqlDbType.VarBinary, eventData.Length, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current,
                                                        eventData));
                    cmd.Parameters.Add(new SqlParameter("@Created", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, null, DataRowVersion.Current, created));
                    cmd.Parameters.Add(new SqlParameter("@EventCategory", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 1, null, DataRowVersion.Current,
                                                        Settings.Default.DomEventCategoryID));

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex) {
                    LogUtility.AppendStringToFile(LogUtility.LoggingLevel.Error, "Errore durante l'accesso alla base dati. " + ex.Message);
                }
                catch (ThreadAbortException) {
                    if (dbConnection != null) {
                        dbConnection.Close();
                    }
                }
                finally {
                    if (dbConnection != null) {
                        dbConnection.Close();
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Metodi privati

        /// <summary>
        /// Decodifica una stringa che contiene un valore a 64 bit in un indirizzo IP
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private static IPAddress DecodeIPAddress(string address) {
            long ip;
            if (long.TryParse(address, out ip)) {
                if (ip == 0) {
                    return null;
                }

                return
                    new IPAddress(new[]
                                  {(byte) ((uint) ip >> 24), (byte) (((uint) ip & 0x00FF0000) >> 16), (byte) (((uint) ip & 0x0000FF00) >> 8), (byte) (((uint) ip & 0x000000FF))});
            }

            return null;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GrisSuite.TelnetSupervisorLibrary.Devices.Prase;
using GrisSuite.TelnetSupervisorLibrary.Telnet;

namespace GrisSuite.TelnetSupervisorLibrary.Database {
    /// <summary>
    /// Rappresenta il singolo Field da persistere su database
    /// </summary>
    public class DBStreamField {
        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="fieldID">ID del Field</param>
        /// <param name="arrayID">Posizione del valore nel vettore</param>
        /// <param name="fieldName">Nome del Field</param>
        /// <param name="telnetCommand">Comando telnet su cui effettuare il parsing per la ricerca dei valori</param>
        /// <param name="domDataValue">Nome del possibile valore per il parsing dei dati</param>
        /// <param name="seekRowRegex">Espressione regolare che permette di rintracciare in modo univoco una riga nei log</param>
        public DBStreamField(int fieldID, int arrayID, string fieldName, string telnetCommand, PEAVDOM.DomDataValues domDataValue, Regex seekRowRegex) {
            this.FieldID = fieldID;
            this.ArrayID = arrayID;
            this.Name = fieldName;
            this.TelnetCommand = telnetCommand;
            DomDataValue = domDataValue;
            SeekRowRegex = seekRowRegex;
            DBStreamFieldValueDescriptions = new List<DBStreamFieldValueDescription>();
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="fieldID">ID del Field</param>
        /// <param name="arrayID">Posizione del valore nel vettore</param>
        /// <param name="fieldName">Nome del Field</param>
        /// <param name="telnetCommand">Comando telnet su cui effettuare il parsing per la ricerca dei valori</param>
        /// <param name="domDataValue">Nome del possibile valore per il parsing dei dati</param>
        public DBStreamField(int fieldID, int arrayID, string fieldName, string telnetCommand, PEAVDOM.DomDataValues domDataValue)
            : this(fieldID, arrayID, fieldName, telnetCommand, domDataValue, null) {
        }

        #region Proprietà pubbliche

        /// <summary>
        /// Severità relativa al Field
        /// </summary>
        public Severity SeverityLevel { get; set; }

        /// <summary>
        /// Decodifica descrittiva del valore del Field
        /// </summary>
        public string ValueDescription { get; set; }

        /// <summary>
        /// Comando telnet su cui effettuare il parsing per la ricerca dei valori
        /// </summary>
        public string TelnetCommand { get; set; }

        /// <summary>
        /// Nome del possibile valore per il parsing dei dati
        /// </summary>
        public PEAVDOM.DomDataValues DomDataValue { get; set; }

        /// <summary>
        /// Espressione regolare che permette di rintracciare in modo univoco una riga nei log
        /// </summary>
        public Regex SeekRowRegex { get; set; }

        /// <summary>
        /// Decodifica descrittiva del valore del Field, concatenata alla severità (solo nel caso non sia sconosciuta)
        /// </summary>
        public string ValueDescriptionComplete {
            get {
                if (SeverityLevel == Severity.Unknown) {
                    return ValueDescription;
                }

                return ValueDescription + "=" + (byte) SeverityLevel;
            }
        }

        /// <summary>
        /// ID del Field
        /// </summary>
        public int FieldID { get; private set; }

        /// <summary>
        /// Valore del Field
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Lista di valori
        /// </summary>
        public IList<String> RawData { get; set; }

        /// <summary>
        /// Posizione del valore nel vettore
        /// </summary>
        public int ArrayID { get; private set; }

        /// <summary>
        /// Nome del Field
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Lista di descrizioni per la decodifica del valore e della severità
        /// </summary>
        public List<DBStreamFieldValueDescription> DBStreamFieldValueDescriptions { get; set; }

        #endregion

        private enum EvaluationNeeded {
            Unknown,
            StringComparison,
            IntComparison,
            RangeComparison
        }

        private EvaluationNeeded DecideEvaluationMethod() {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((this.DBStreamFieldValueDescriptions != null) && (this.DBStreamFieldValueDescriptions.Count > 0)) {
                if (this.DBStreamFieldValueDescriptions.Count == 1) {
                    evaluationNeeded = GetEvaluationNeeded(this.DBStreamFieldValueDescriptions[0]);
                }
                else {
                    foreach (DBStreamFieldValueDescription dbStreamFieldValueDescription in this.DBStreamFieldValueDescriptions) {
                        if (!dbStreamFieldValueDescription.IsDefault) {
                            // Consideriamo solo il primo tipo - casi di tipi di valori etereogenei non sono supportati
                            evaluationNeeded = GetEvaluationNeeded(dbStreamFieldValueDescription);
                            break;
                        }
                    }
                }
            }

            return evaluationNeeded;
        }

        private static EvaluationNeeded GetEvaluationNeeded(DBStreamFieldValueDescription dbStreamFieldValueDescription) {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((dbStreamFieldValueDescription.RangeValueToCompare != null) &&
                ((dbStreamFieldValueDescription.RangeValueToCompare.MinValue.HasValue) || (dbStreamFieldValueDescription.RangeValueToCompare.MaxValue.HasValue))) {
                evaluationNeeded = EvaluationNeeded.RangeComparison;
            }
            else if (dbStreamFieldValueDescription.ExactIntValueToCompare.HasValue) {
                evaluationNeeded = EvaluationNeeded.IntComparison;
            }
            else if (!String.IsNullOrEmpty(dbStreamFieldValueDescription.ExactStringValueToCompare)) {
                evaluationNeeded = EvaluationNeeded.StringComparison;
            }
            return evaluationNeeded;
        }

        public void Evaluate() {
            if ((DBStreamFieldValueDescriptions != null) && (DBStreamFieldValueDescriptions.Count > 0)) {
                if (DBStreamFieldValueDescriptions.Count == 1) {
                    this.ParseDefaultSingleValue(DBStreamFieldValueDescriptions[0].DescriptionIfMatch);
                }
                else {
                    switch (this.DecideEvaluationMethod()) {
                        case EvaluationNeeded.Unknown:
                            ParseStringValue();
                            break;
                        case EvaluationNeeded.StringComparison:
                            ParseStringValue();
                            break;
                        case EvaluationNeeded.IntComparison:
                            this.ParseIntValue();
                            break;
                        case EvaluationNeeded.RangeComparison:
                            this.ParseRangeValue();
                            break;
                    }
                }
            }
        }

        #region Metodi privati per l'elaborazione dei dati relativi ai vari Fields degli Stream, con decodifica di descrizione e severità

        /// <summary>
        /// Elabora i dati di tipo stringa relativi ai vari Fields degli Streams
        /// </summary>
        private void ParseStringValue() {
            if ((this.RawData != null) && (this.RawData.Count > 0)) {
                if (this.ArrayID >= this.RawData.Count) {
                    SetFieldStateToInvalidArrayIndex();
                }
                else {
                    string value = this.RawData[this.ArrayID];

                    if (value == null) {
                        SetFieldStateToUnknown();
                    }
                    else {
                        DBStreamFieldValueDescription defaultDescription = null;
                        bool fieldValueSet = false;

                        foreach (DBStreamFieldValueDescription description in this.DBStreamFieldValueDescriptions) {
                            if (String.Equals(value, description.ExactStringValueToCompare, StringComparison.OrdinalIgnoreCase)) {
                                SetFieldData(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                fieldValueSet = true;
                                break;
                            }

                            if (description.IsDefault) {
                                // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                defaultDescription = description;
                            }
                        }

                        if (!fieldValueSet) {
                            if (defaultDescription != null) {
                                SetFieldData(defaultDescription.DescriptionIfMatch, value, defaultDescription.SeverityLevelIfMatch);
                            }
                            else {
                                SetFieldStateToUnknown();
                            }
                        }
                    }
                }
            }
            else {
                SetFieldStateToUnknown();
            }
        }

        /// <summary>
        /// Elabora i dati di tipo intero relativi ai vari Fields degli Streams
        /// </summary>
        private void ParseIntValue() {
            if ((this.RawData != null) && (this.RawData.Count > 0)) {
                if (this.ArrayID >= this.RawData.Count) {
                    SetFieldStateToInvalidArrayIndex();
                }
                else {
                    string valueString = this.RawData[this.ArrayID];

                    if (valueString == null) {
                        SetFieldStateToUnknown();
                    }
                    else {
                        int value;
                        if (Int32.TryParse(valueString, out value)) {
                            DBStreamFieldValueDescription defaultDescription = null;
                            bool fieldValueSet = false;

                            foreach (DBStreamFieldValueDescription description in this.DBStreamFieldValueDescriptions) {
                                if (value == description.ExactIntValueToCompare) {
                                    SetFieldData(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                    fieldValueSet = true;
                                    break;
                                }

                                if (description.IsDefault) {
                                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                    defaultDescription = description;
                                }
                            }

                            if (!fieldValueSet) {
                                if (defaultDescription != null) {
                                    SetFieldData(defaultDescription.DescriptionIfMatch, value, defaultDescription.SeverityLevelIfMatch);
                                }
                                else {
                                    SetFieldStateToUnknown();
                                }
                            }
                        }
                        else {
                            SetFieldStateToUnknown();
                        }
                    }
                }
            }
            else {
                SetFieldStateToUnknown();
            }
        }

        /// <summary>
        /// Elabora i dati di tipo intervallo relativi ai vari Fields degli Streams
        /// </summary>
        private void ParseRangeValue() {
            if ((this.RawData != null) && (this.RawData.Count > 0)) {
                if (this.ArrayID >= this.RawData.Count) {
                    SetFieldStateToInvalidArrayIndex();
                }
                else {
                    string valueString = this.RawData[this.ArrayID];

                    if (valueString == null) {
                        SetFieldStateToUnknown();
                    }
                    else {
                        int value;
                        if (Int32.TryParse(valueString, out value)) {
                            DBStreamFieldValueDescription defaultDescription = null;
                            bool fieldValueSet = false;

                            foreach (DBStreamFieldValueDescription description in this.DBStreamFieldValueDescriptions) {
                                if ((description.RangeValueToCompare.MinValue != null) && (description.RangeValueToCompare.MaxValue != null) &&
                                    (description.RangeValueToCompare.MinValue.HasValue) && (description.RangeValueToCompare.MaxValue.HasValue)) {
                                    if ((value >= description.RangeValueToCompare.MinValue.Value) && (value <= description.RangeValueToCompare.MaxValue.Value)) {
                                        SetFieldData(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                        fieldValueSet = true;
                                        break;
                                    }
                                }
                                else if ((description.RangeValueToCompare.MinValue.HasValue) && (!description.RangeValueToCompare.MaxValue.HasValue)) {
                                    if (value >= description.RangeValueToCompare.MinValue.Value) {
                                        SetFieldData(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                        fieldValueSet = true;
                                        break;
                                    }
                                }
                                else if ((!description.RangeValueToCompare.MinValue.HasValue) && (description.RangeValueToCompare.MaxValue.HasValue)) {
                                    if (value <= description.RangeValueToCompare.MaxValue.Value) {
                                        SetFieldData(description.DescriptionIfMatch, value, description.SeverityLevelIfMatch);
                                        fieldValueSet = true;
                                        break;
                                    }
                                }

                                if (description.IsDefault) {
                                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                    defaultDescription = description;
                                }
                            }

                            if (!fieldValueSet) {
                                if (defaultDescription != null) {
                                    SetFieldData(defaultDescription.DescriptionIfMatch, value, defaultDescription.SeverityLevelIfMatch);
                                }
                                else {
                                    SetFieldStateToUnknown();
                                }
                            }
                        }
                        else {
                            SetFieldStateToUnknown();
                        }
                    }
                }
            }
            else {
                SetFieldStateToUnknown();
            }
        }

        /// <summary>
        /// Elabora un dato unico relativo al Field degli Streams
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore passato</param>
        private void ParseDefaultSingleValue(string valueDescription) {
            if ((this.RawData != null) && (this.RawData.Count > 0)) {
                if (this.ArrayID >= this.RawData.Count) {
                    SetFieldStateToInvalidArrayIndex();
                }
                else {
                    string value = this.RawData[this.ArrayID];
                    if ((value != null) && (value.Trim().Length > 0)) {
                        SetFieldData(valueDescription, value);
                    }
                    else {
                        SetFieldStateToUnknown();
                    }
                }
            }
            else {
                SetFieldStateToUnknown();
            }
        }

        /// <summary>
        /// Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        private void SetFieldData(string valueDescription, string value) {
            SetFieldData(valueDescription, value, Severity.OK);
        }

        /// <summary>
        /// Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="severity">Severità da impostare</param>
        private void SetFieldData(string valueDescription, int value, Severity severity) {
            SetFieldData(valueDescription, value.ToString(), severity);
        }

        /// <summary>
        /// Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="severity">Severità da impostare</param>
        private void SetFieldData(string valueDescription, string value, Severity severity) {
            this.SeverityLevel = severity;
            this.ValueDescription = valueDescription;
            this.Value = value;
        }

        /// <summary>
        /// Imposta il valore del Field nel caso il valore sia nullo
        /// </summary>
        private void SetFieldStateToUnknown() {
            SetFieldData("Valore non ricevuto", "n/d", Severity.Unknown);
        }

        /// <summary>
        /// Imposta il valore del Field nel caso si sia richiesto un valore con indice non disponibile
        /// </summary>
        private void SetFieldStateToInvalidArrayIndex() {
            SetFieldData(String.Format("L'indice dell'array {0} non è valido per la dimensione dei dati disponibili {1}", this.ArrayID, this.RawData.Count), "n/d", Severity.Unknown);
        }

        #endregion
    }
}
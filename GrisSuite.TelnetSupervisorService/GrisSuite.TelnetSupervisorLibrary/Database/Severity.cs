﻿namespace GrisSuite.TelnetSupervisorLibrary.Database {
    /// <summary>
    /// Rappresenta la severità dei valori ricevuti
    /// </summary>
    public enum Severity : byte {
        /// <summary>
        /// Stato normale
        /// </summary>
        OK = 0,
        /// <summary>
        /// Stato di allerta
        /// </summary>
        Warning = 1,
        /// <summary>
        /// Stato di errore
        /// </summary>
        Error = 2,
        /// <summary>
        /// Stato da configurare
        /// </summary>
        MaintenanceMode = 9,
        /// <summary>
        /// Stato sconosciuto o nessuna risposta
        /// </summary>
        Unknown = 255
    }
}
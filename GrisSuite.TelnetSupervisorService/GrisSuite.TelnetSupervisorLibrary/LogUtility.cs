﻿using System;
using System.IO;
using System.Reflection;
using System.Security;
using System.Text;
using System.Web;
using GrisSuite.TelnetSupervisorLibrary.Properties;

namespace GrisSuite.TelnetSupervisorLibrary {
    public static class LogUtility {
        #region LoggingLevel enum

        /// <summary>
        /// Indica il livello di logging
        /// </summary>
        public enum LoggingLevel : ushort {
            /// <summary>
            /// Logging disattivato
            /// </summary>
            None = 0,
            /// <summary>
            /// Log solo degli eventi critici (eccezioni non gestite)
            /// </summary>
            Critical = 1,
            /// <summary>
            /// Log degli errori (eccezioni gestite)
            /// </summary>
            Error = 2,
            /// <summary>
            /// Log delle condizioni di allerta
            /// </summary>
            Warning = 3,
            /// <summary>
            /// Log informativo
            /// </summary>
            Info = 4,
            /// <summary>
            /// Log dettagliato
            /// </summary>
            Verbose = 5
        }

        #endregion

        #region Costruttore

        static LogUtility() {
            logFilePath = Assembly.GetEntryAssembly().Location + ".log.htm";

            if (Enum.IsDefined(typeof (LoggingLevel), Settings.Default.LogLevel)) {
                LogLevel = (LoggingLevel) Settings.Default.LogLevel;
            }
            else {
                LogLevel = LoggingLevel.Warning;
                AppendStringToFile(LoggingLevel.Warning,
                                   "Il valore per LogLevel indicato nella configurazione non è nel range previsto (0-5). Il log è stato impostato su 3 - Livello condizioni di allerta");
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Aggiunge una messaggio di log ad un file di testo su disco. Il messaggio è preceduto dalla data/ora completa. In caso di errore nell'accesso al file, non fa nulla.
        /// </summary>
        /// <param name="level">Livello di logging</param>
        /// <param name="text">Testo del messaggio</param>
        public static void AppendStringToFile(LoggingLevel level, string text) {
            if ((LogLevel > LoggingLevel.None) && (level <= LogLevel) && (!String.IsNullOrEmpty(text)) && (!String.IsNullOrEmpty(logFilePath))) {
                TextWriter fileWriter = null;
                try {
                    fileWriter = new StreamWriter(logFilePath, true, Encoding.GetEncoding(1252));

                    fileWriter.Write("<div style=\"font-family:Consolas,Courier;font-size:8pt\">[" + DateTime.Now.ToString(@"yyyy/MM/dd HH\:mm\:ss") + "] " + HTMLNewLine(text) +
                                     "</div><hr/>");
                    fileWriter.Flush();
                }
                catch (UnauthorizedAccessException) {
                }
                catch (IOException) {
                }
                catch (ObjectDisposedException) {
                }
                catch (ArgumentException) {
                }
                catch (SecurityException) {
                }
                finally {
                    if (fileWriter != null) {
                        fileWriter.Flush();
                        fileWriter.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Controlla la dimensione del file di log e, se eccede la dimensione massima configurata, ne crea uno nuovo, rinominando il corrente
        /// </summary>
        /// <returns></returns>
        public static void RecycleLogFile() {
            if (File.Exists(logFilePath)) {
                try {
                    var logFileInfo = new FileInfo(logFilePath);
                    long debugLogMaxSize = Settings.Default.DebugLogMaxByteSize;

                    if ((logFileInfo.Length > debugLogMaxSize) && (debugLogMaxSize > 0)) {
                        string logFileInfoOld = logFilePath.Replace(".log.htm", "_old.log.htm");

                        if (File.Exists(logFileInfoOld)) {
                            if ((File.GetAttributes(logFileInfoOld) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                                File.SetAttributes(logFileInfoOld, FileAttributes.Normal);
                            }

                            File.Delete(logFileInfoOld);
                        }

                        logFileInfo.MoveTo(logFileInfoOld);
                    }
                }
                catch (UnauthorizedAccessException) {
                    logFilePath = null;
                }
                catch (IOException) {
                    logFilePath = null;
                }
                catch (ObjectDisposedException) {
                    logFilePath = null;
                }
                catch (ArgumentException) {
                    logFilePath = null;
                }
                catch (SecurityException) {
                    logFilePath = null;
                }
                catch (NotSupportedException) {
                    logFilePath = null;
                }
            }
        }

        /// <summary>
        /// Crea una stringa da usare come separatore nei log
        /// </summary>
        /// <param name="separator">Carattere da usare come separatore</param>
        /// <param name="separatorLength">Lunghezza della riga separatrice</param>
        /// <returns></returns>
        public static string CreateSeparator(char separator, int separatorLength) {
            return CreateSeparator(separator, separatorLength, String.Empty);
        }

        /// <summary>
        /// Crea una stringa da usare come separatore nei log
        /// </summary>
        /// <param name="separator">Carattere da usare come separatore</param>
        /// <param name="separatorLength">Lunghezza della riga separatrice</param>
        /// <param name="header">Testo da inserire a fronte del separatore</param>
        /// <returns></returns>
        public static string CreateSeparator(char separator, int separatorLength, string header) {
            if (!String.IsNullOrEmpty(header)) {
                return header + " " + new string(separator, separatorLength - (header + " ").Length);
            }

            return new string(separator, separatorLength);
        }

        #endregion

        #region Metodi privati

        private static string HTMLNewLine(string input) {
            if (!String.IsNullOrEmpty(input)) {
                return HttpUtility.HtmlEncode(input).Replace("\n", "<br/>");
            }
            return String.Empty;
        }

        #endregion

        #region Variabili private

        private static string logFilePath;

        #endregion

        #region Prorietà pubbliche

        public static LoggingLevel LogLevel { get; private set; }

        #endregion
    }
}
﻿namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    public partial class PEAVDOM {
        public enum DomDataValues {
            Version,
            DeviceType,
            SerialNumber,
            DomIP,
            DomStatus,
            DomErrorState,
            DomDowntimeSeconds,
            DomDeviceState,
            DomDeviceDevAmp,
            DomDeviceConnection,
            DomImpedanceSupervisionAction,
            DomImpedanceSupervisionPowerNominal,
            DomImpedanceSupervisionTolerance,
            DomImpedanceSupervisionSetpoint,
            DomImpedanceSupervisionValue,
            DomPAReplacementStatus,
            DomEarthLeakageSupervisionAction,
            DomEarthLeakageSupervisionWireA,
            DomEarthLeakageSupervisionWireB
        }
    }
}
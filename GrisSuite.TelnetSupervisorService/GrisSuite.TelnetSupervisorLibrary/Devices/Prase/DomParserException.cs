﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    public class DomParserException : Exception {
        public DomParserException() {
        }

        public DomParserException(string message)
            : base(message) {
        }

        public DomParserException(string message, Exception inner)
            : base(message, inner) {
        }

        public DomParserException(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}

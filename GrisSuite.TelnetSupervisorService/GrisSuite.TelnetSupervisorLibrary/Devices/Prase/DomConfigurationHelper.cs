﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using GrisSuite.TelnetSupervisorLibrary.Database;

namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    internal class DomConfigurationHelper {
        public string ConfigurationFileName { get; private set; }

        /// <summary>
        /// Lista dei comandi telnet necessari al parsing dei dati
        /// </summary>
        public List<string> TelnetCommands {
            get {
                if (this.telnetCommands == null) {
                    this.LoadTelnetCommands();
                }

                return this.telnetCommands;
            }
        }

        public string TelnetCommandEvents { get; private set; }

        private List<string> telnetCommands;

        public DomConfigurationHelper(string configurationFileName) {
            this.ConfigurationFileName = configurationFileName;
        }

        private void LoadTelnetCommands() {
            this.telnetCommands = new List<string>();

            if (String.IsNullOrEmpty(this.ConfigurationFileName)) {
                throw new DomConfigurationHelperException("Nome del file di configurazione per la periferica non definito");
            }

            if (!CheckFileCanRead(this.ConfigurationFileName)) {
                throw new DomConfigurationHelperException(String.Format("Impossibile leggere il file di configurazione: {0}", this.ConfigurationFileName));
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(this.ConfigurationFileName);

            XmlNode root = doc.DocumentElement;

            if (root == null) {
                throw new DomConfigurationHelperException(String.Format("Configurazione non valida nel file {0}, con la definizione XML del dispositivo", this.ConfigurationFileName));
            }

            if (root.Name.Equals("Device", StringComparison.OrdinalIgnoreCase)) {
                if (root.Attributes["TelnetCommandEvents"] == null) {
                    throw new DomConfigurationHelperException(
                        String.Format(
                            "Impossibile trovare l'attributo 'TelnetCommandEvents' sul nodo 'Device' nel file {0}.", this.ConfigurationFileName));
                }
            }
            else {
                throw new DomConfigurationHelperException(String.Format("Impossibile trovare l'elemento 'Device' come nodo radice del file di configurazione {0}.",
                                                                        this.ConfigurationFileName));
            }

            this.TelnetCommandEvents = root.Attributes["TelnetCommandEvents"].Value.Trim();

            bool streamsFound = false;

            foreach (XmlNode streams in root.ChildNodes) {
                if (streams.Name.Equals("streams", StringComparison.OrdinalIgnoreCase)) {
                    #region Stream

                    bool streamFound = false;

                    foreach (XmlNode stream in streams.ChildNodes) {
                        if (stream.Name.Equals("stream", StringComparison.OrdinalIgnoreCase)) {
                            if ((stream.Attributes["ID"] == null) || (stream.Attributes["Name"] == null) || (stream.Attributes["Name"].Value.Trim().Length == 0)) {
                                throw new DomConfigurationHelperException(
                                    String.Format(
                                        "Impossibile trovare l'attributo 'ID' o 'Name' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                        this.ConfigurationFileName, stream.OuterXml));
                            }

                            #region StreamField

                            bool streamFieldFound = false;

                            foreach (XmlNode streamField in stream.ChildNodes) {
                                if (streamField.Name.Equals("StreamField", StringComparison.OrdinalIgnoreCase)) {
                                    if ((streamField.Attributes["ID"] == null) || (streamField.Attributes["Name"] == null) ||
                                        (streamField.Attributes["Name"].Value.Trim().Length == 0) || (streamField.Attributes["TelnetCommand"] == null) ||
                                        (streamField.Attributes["DomDataValues"] == null) || (streamField.Attributes["DomDataValues"].Value.Trim().Length == 0)) {
                                        throw new DomConfigurationHelperException(
                                            String.Format(
                                                "Impossibile trovare l'attributo 'ID', 'Name', 'TelnetCommand' o 'DomDataValues' sul nodo 'StreamField' nel file {0}, oppure nome dello StreamField nullo. Il contenuto del nodo è:\r\n{1}",
                                                this.ConfigurationFileName, streamField.OuterXml));
                                    }

                                    #region TelnetCommand

                                    string telnetCommand = streamField.Attributes["TelnetCommand"].Value.Trim();

                                    if (!telnetCommands.Contains(telnetCommand)) {
                                        telnetCommands.Add(telnetCommand);
                                    }

                                    #endregion

                                    streamFieldFound = true;
                                }
                            }

                            if (!streamFieldFound) {
                                throw new DomConfigurationHelperException(
                                    String.Format(
                                        "Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare almeno uno stream field definito per lo stream '{1}'.",
                                        this.ConfigurationFileName, stream.Attributes["Name"].Value.Trim()));
                            }

                            #endregion
                        }

                        streamFound = true;
                    }

                    if (!streamFound) {
                        throw new DomConfigurationHelperException(
                            String.Format("Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare almeno uno Stream definito.",
                                          this.ConfigurationFileName));
                    }

                    #endregion

                    streamsFound = true;
                    break;
                }
            }

            if (!streamsFound) {
                throw new DomConfigurationHelperException(
                    String.Format("Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare il nodo 'Streams' al primo livello.",
                                  this.ConfigurationFileName));
            }
        }

        public void LoadConfiguration(List<DBStream> streamData) {
            if (String.IsNullOrEmpty(this.ConfigurationFileName)) {
                throw new DomConfigurationHelperException("Nome del file di configurazione per la periferica non definito");
            }

            if (!CheckFileCanRead(this.ConfigurationFileName)) {
                throw new DomConfigurationHelperException(String.Format("Impossibile leggere il file di configurazione: {0}", this.ConfigurationFileName));
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(this.ConfigurationFileName);

            XmlNode root = doc.DocumentElement;

            if (root == null) {
                throw new DomConfigurationHelperException(String.Format("Configurazione non valida nel file {0}, con la definizione XML del dispositivo", this.ConfigurationFileName));
            }

            bool streamsFound = false;

            foreach (XmlNode streams in root.ChildNodes) {
                if (streams.Name.Equals("streams", StringComparison.OrdinalIgnoreCase)) {
                    #region Stream

                    bool streamFound = false;

                    foreach (XmlNode stream in streams.ChildNodes) {
                        if (stream.Name.Equals("stream", StringComparison.OrdinalIgnoreCase)) {
                            if ((stream.Attributes["ID"] == null) || (stream.Attributes["Name"] == null) || (stream.Attributes["Name"].Value.Trim().Length == 0)) {
                                throw new DomConfigurationHelperException(
                                    String.Format(
                                        "Impossibile trovare l'attributo 'ID' o 'Name' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                        this.ConfigurationFileName, stream.OuterXml));
                            }

                            #region ID

                            int streamId;

                            if (!Int32.TryParse(stream.Attributes["ID"].Value, out streamId)) {
                                throw new DomConfigurationHelperException(
                                    String.Format("L'attributo 'ID' del nodo 'Stream' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                                  this.ConfigurationFileName, stream.Attributes["ID"].Value));
                            }

                            #endregion

                            DBStream str = new DBStream(streamId, stream.Attributes["Name"].Value.Trim());

                            #region StreamField

                            bool streamFieldFound = false;

                            foreach (XmlNode streamField in stream.ChildNodes) {
                                if (streamField.Name.Equals("StreamField", StringComparison.OrdinalIgnoreCase)) {
                                    if ((streamField.Attributes["ID"] == null) || (streamField.Attributes["Name"] == null) ||
                                        (streamField.Attributes["Name"].Value.Trim().Length == 0) || (streamField.Attributes["TelnetCommand"] == null) ||
                                        (streamField.Attributes["DomDataValues"] == null) || (streamField.Attributes["DomDataValues"].Value.Trim().Length == 0)) {
                                        throw new DomConfigurationHelperException(
                                            String.Format(
                                                "Impossibile trovare l'attributo 'ID', 'Name', 'TelnetCommand' o 'DomDataValues' sul nodo 'StreamField' nel file {0}, oppure nome dello StreamField nullo. Il contenuto del nodo è:\r\n{1}",
                                                this.ConfigurationFileName, streamField.OuterXml));
                                    }

                                    #region ID

                                    int fieldId;

                                    if (!Int32.TryParse(streamField.Attributes["ID"].Value, out fieldId)) {
                                        throw new DomConfigurationHelperException(
                                            String.Format("L'attributo 'ID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                                          this.ConfigurationFileName, streamField.Attributes["ID"].Value));
                                    }

                                    #endregion

                                    #region ArrayID

                                    int arrayId = 0;

                                    if (streamField.Attributes["ArrayID"] != null) {
                                        if (!Int32.TryParse(streamField.Attributes["ArrayID"].Value, out arrayId)) {
                                            throw new DomConfigurationHelperException(
                                                String.Format(
                                                    "L'attributo 'ArrayID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                                    this.ConfigurationFileName, streamField.Attributes["ArrayID"].Value));
                                        }
                                    }

                                    #endregion

                                    #region TelnetCommand

                                    string telnetCommand = streamField.Attributes["TelnetCommand"].Value.Trim();

                                    #endregion

                                    #region DomDataValues

                                    object domDataValue;
                                    try {
                                        string domDataValuesString = streamField.Attributes["DomDataValues"].Value.Trim();
                                        domDataValue = Enum.Parse(typeof (PEAVDOM.DomDataValues), domDataValuesString, true);
                                        if (domDataValue == null) {
                                            throw new DomConfigurationHelperException(
                                                String.Format(
                                                    "L'attributo 'DomDataValues' del nodo 'StreamField' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: {1}",
                                                    this.ConfigurationFileName, streamField.Attributes["DomDataValues"].Value.Trim()));
                                        }
                                    }
                                    catch (ArgumentException) {
                                        throw new DomConfigurationHelperException(
                                            String.Format(
                                                "L'attributo 'DomDataValues' del nodo 'StreamField' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: {1}",
                                                this.ConfigurationFileName, streamField.Attributes["DomDataValues"].Value.Trim()));
                                    }

                                    #endregion

                                    #region SeekRowRegex

                                    Regex seekRowRegex = null;

                                    if (streamField.Attributes["SeekRowRegex"] != null) {
                                        string seekRowRegexString = streamField.Attributes["SeekRowRegex"].Value;

                                        try {
                                            seekRowRegex = new Regex(seekRowRegexString, RegexOptions.Singleline | RegexOptions.IgnoreCase);
                                        }
                                        catch (ArgumentException) {
                                            throw new DomConfigurationHelperException(
                                                String.Format(
                                                    "L'attributo 'SeekRowRegex' del nodo 'StreamField' nel file {0}, non contiene una espressione regolare valida. Valore contenuto: {1}",
                                                    this.ConfigurationFileName, streamField.Attributes["DomDataValues"].Value.Trim()));
                                        }
                                    }

                                    #endregion

                                    DBStreamField strField;

                                    if (seekRowRegex != null) {
                                        strField = new DBStreamField(fieldId, arrayId, streamField.Attributes["Name"].Value.Trim(), telnetCommand,
                                                                     (PEAVDOM.DomDataValues) domDataValue, seekRowRegex);
                                    }
                                    else {
                                        strField = new DBStreamField(fieldId, arrayId, streamField.Attributes["Name"].Value.Trim(), telnetCommand,
                                                                     (PEAVDOM.DomDataValues) domDataValue);
                                    }

                                    #region FieldValueDescription

                                    bool streamFieldValueDescriptionFound = false;

                                    foreach (XmlNode fieldValueDescription in streamField.ChildNodes) {
                                        if (fieldValueDescription.Name.Equals("FieldValueDescription", StringComparison.OrdinalIgnoreCase)) {
                                            if ((fieldValueDescription.Attributes["ValueDescription"] == null) ||
                                                (fieldValueDescription.Attributes["ValueDescription"].Value.Trim().Length == 0)) {
                                                throw new DomConfigurationHelperException(
                                                    String.Format(
                                                        "Impossibile trovare l'attributo 'ValueDescription' sul nodo 'FieldValueDescription', oppure descrizione del valore nulla nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                        this.ConfigurationFileName, fieldValueDescription.OuterXml));
                                            }

                                            #region Severity

                                            object severity = null;

                                            if (fieldValueDescription.Attributes["Severity"] != null) {
                                                try {
                                                    string severityString = fieldValueDescription.Attributes["Severity"].Value.Trim();
                                                    severity = Enum.Parse(typeof (Severity), severityString, true);
                                                    if (severity == null) {
                                                        throw new DomConfigurationHelperException(
                                                            String.Format(
                                                                "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: {1}",
                                                                this.ConfigurationFileName, fieldValueDescription.Attributes["Severity"].Value.Trim()));
                                                    }
                                                }
                                                catch (ArgumentException) {
                                                    throw new DomConfigurationHelperException(
                                                        String.Format(
                                                            "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: {1}",
                                                            this.ConfigurationFileName, fieldValueDescription.Attributes["Severity"].Value.Trim()));
                                                }
                                            }

                                            if (severity == null) {
                                                severity = Severity.OK;
                                            }

                                            #endregion

                                            #region ExactValueString

                                            string exactValueString = null;

                                            if (fieldValueDescription.Attributes["ExactValueString"] != null) {
                                                exactValueString = fieldValueDescription.Attributes["ExactValueString"].Value;
                                            }

                                            #endregion

                                            #region ExactValueInt

                                            int? exactValueIntNull = null;

                                            if (fieldValueDescription.Attributes["ExactValueInt"] != null) {
                                                string exactValueIntString = fieldValueDescription.Attributes["ExactValueInt"].Value.Trim();
                                                int exactValueInt;

                                                if (!Int32.TryParse(exactValueIntString, out exactValueInt)) {
                                                    throw new DomConfigurationHelperException(
                                                        String.Format(
                                                            "L'attributo 'ExactValueInt' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                                            this.ConfigurationFileName, fieldValueDescription.Attributes["ExactValueInt"].Value.Trim()));
                                                }

                                                exactValueIntNull = exactValueInt;
                                            }

                                            #endregion

                                            #region DBStreamFieldValueRange

                                            #region RangeValueMin

                                            int? rangeValueMinNull = null;

                                            if (fieldValueDescription.Attributes["RangeValueMin"] != null) {
                                                string rangeValueMinString = fieldValueDescription.Attributes["RangeValueMin"].Value.Trim();
                                                int rangeValueMin;

                                                if (!Int32.TryParse(rangeValueMinString, out rangeValueMin)) {
                                                    throw new DomConfigurationHelperException(
                                                        String.Format(
                                                            "L'attributo 'RangeValueMin' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                                            this.ConfigurationFileName, fieldValueDescription.Attributes["RangeValueMin"].Value.Trim()));
                                                }

                                                rangeValueMinNull = rangeValueMin;
                                            }

                                            #endregion

                                            #region RangeValueMax

                                            int? rangeValueMaxNull = null;

                                            if (fieldValueDescription.Attributes["RangeValueMax"] != null) {
                                                string rangeValueMaxString = fieldValueDescription.Attributes["RangeValueMax"].Value.Trim();
                                                int rangeValueMax;

                                                if (!Int32.TryParse(rangeValueMaxString, out rangeValueMax)) {
                                                    throw new DomConfigurationHelperException(
                                                        String.Format(
                                                            "L'attributo 'RangeValueMax' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
                                                            this.ConfigurationFileName, fieldValueDescription.Attributes["RangeValueMax"].Value.Trim()));
                                                }

                                                rangeValueMaxNull = rangeValueMax;
                                            }

                                            #endregion

                                            DBStreamFieldValueRange dbStreamFieldValueRange = null;

                                            if (rangeValueMinNull.HasValue && !rangeValueMaxNull.HasValue) {
                                                dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMinNull.Value);
                                            }
                                            else if (!rangeValueMinNull.HasValue && rangeValueMaxNull.HasValue) {
                                                dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMaxNull.Value);
                                            }
                                            else if (rangeValueMinNull.HasValue && rangeValueMaxNull.HasValue) {
                                                dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMinNull, rangeValueMaxNull);
                                            }

                                            #endregion

                                            #region IsDefault

                                            bool isDefault = false;

                                            if (fieldValueDescription.Attributes["IsDefault"] != null) {
                                                string isDefaultString = fieldValueDescription.Attributes["IsDefault"].Value.Trim().ToLower();
                                                if (!Boolean.TryParse(isDefaultString, out isDefault)) {
                                                    throw new DomConfigurationHelperException(
                                                        String.Format(
                                                            "L'attributo 'IsDefault' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: {1}",
                                                            this.ConfigurationFileName, fieldValueDescription.Attributes["IsDefault"].Value.Trim()));
                                                }
                                            }

                                            #endregion

                                            strField.DBStreamFieldValueDescriptions.Add(new DBStreamFieldValueDescription(strField, isDefault,
                                                                                                                          fieldValueDescription.Attributes["ValueDescription"].Value
                                                                                                                              .Trim(), (Severity) severity, exactValueString,
                                                                                                                          exactValueIntNull, dbStreamFieldValueRange));

                                            streamFieldValueDescriptionFound = true;
                                        }
                                    }

                                    if (!streamFieldValueDescriptionFound) {
                                        throw new DomConfigurationHelperException(
                                            String.Format(
                                                "Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare almeno una descrizione di decodifica dei valori dello stream field '{1}'.",
                                                this.ConfigurationFileName, streamField.Attributes["Name"].Value.Trim()));
                                    }

                                    #endregion

                                    str.FieldData.Add(strField);

                                    streamFieldFound = true;
                                }
                            }

                            if (!streamFieldFound) {
                                throw new DomConfigurationHelperException(
                                    String.Format(
                                        "Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare almeno uno stream field definito per lo stream '{1}'.",
                                        this.ConfigurationFileName, stream.Attributes["Name"].Value.Trim()));
                            }

                            #endregion

                            streamData.Add(str);
                        }

                        streamFound = true;
                    }

                    if (!streamFound) {
                        throw new DomConfigurationHelperException(
                            String.Format("Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare almeno uno Stream definito.",
                                          this.ConfigurationFileName));
                    }

                    #endregion

                    streamsFound = true;
                    break;
                }
            }

            if (!streamsFound) {
                throw new DomConfigurationHelperException(
                    String.Format("Impossibile caricare il file {0}, con la definizione XML del dispositivo. Impossibile trovare il nodo 'Streams' al primo livello.",
                                  this.ConfigurationFileName));
            }
        }

        public static bool CheckFileCanRead(string fileName) {
            bool returnValue;

            FileStream fs = null;
            try {
                if (!File.Exists(fileName)) {
                    returnValue = false;
                }
                else {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    if (!fs.CanRead) {
                        returnValue = false;
                    }
                    else {
                        returnValue = true;
                    }
                }
            }
            catch (UnauthorizedAccessException) {
                returnValue = false;
            }
            catch (IOException) {
                returnValue = false;
            }
            finally {
                if (fs != null) {
                    fs.Close();
                }
            }

            return returnValue;
        }
    }
}
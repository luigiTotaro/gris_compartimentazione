﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    public class DomConfigurationHelperException : Exception {
        public DomConfigurationHelperException() {
        }

        public DomConfigurationHelperException(string message)
            : base(message) {
        }

        public DomConfigurationHelperException(string message, Exception inner)
            : base(message, inner) {
        }

        public DomConfigurationHelperException(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}

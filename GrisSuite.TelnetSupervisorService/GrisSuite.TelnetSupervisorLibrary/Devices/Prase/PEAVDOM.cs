﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using GrisSuite.TelnetSupervisorLibrary.Database;
using GrisSuite.TelnetSupervisorLibrary.Properties;
using GrisSuite.TelnetSupervisorLibrary.Telnet;

namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    public partial class PEAVDOM : IDigitalOutputModule {
        #region Variabili private

        private readonly string code;
        private readonly IPEndPoint ipEndPoint;
        private readonly string name;
        private readonly string telnetUsername;
        private readonly string telnetPassword;
        private readonly string loginMarker;
        private readonly string passwordMarker;
        private readonly string promptMarker;
        protected bool isPopulated;
        private long pingRoundtripTime = -1;
        private List<DBStream> streamData;
        private List<EventObject> eventsData;
        private readonly bool errorOnTelnet;
        private readonly DomParser domParser;

        #endregion

        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="deviceName">Nome del dispositivo</param>
        /// <param name="deviceCode">Codice univoco del dispositivo</param>
        /// <param name="deviceIP">Indirizzo IP e porta del dispositivo</param>
        /// <param name="username">Username per l'autenticazione Telnet</param>
        /// <param name="password">Password per l'autenticazione Telnet</param>
        /// <param name="loginMarker">Stringa che permette di identificare una richiesta di username per l'autenticazione</param>
        /// <param name="passwordMarker">Stringa che permette di identificare una richiesta di password per l'autenticazione</param>
        /// <param name="promptMarker">Stringa che permette di identificare il prompt di richiesta comandi Telnet</param>
        /// <param name="lastEvent">Informazioni dell'ultimo evento in ordine temporale associato al dispositivo</param>
        public PEAVDOM(string deviceName, string deviceCode, IPEndPoint deviceIP, string username, string password, string loginMarker, string passwordMarker, string promptMarker, EventObject lastEvent) {
            this.name = deviceName;
            this.code = deviceCode;
            this.streamData = new List<DBStream>();
            this.eventsData = new List<EventObject>();
            this.ipEndPoint = deviceIP;
            this.telnetUsername = username;
            this.telnetPassword = password;
            this.loginMarker = loginMarker;
            this.passwordMarker = passwordMarker;
            this.promptMarker = promptMarker;
            this.errorOnTelnet = false;
            this.isPopulated = false;
            this.LastEvent = lastEvent;
            this.LastError = null;

            try {
                this.CheckStatus();
            }
            catch (TelnetException ex) {
                this.errorOnTelnet = true;
                this.LastError = ex;
            }

            if (this.IsAliveTelnet) {
                try {
#if (!LOCAL)

                    ITelnet tc = new TelnetClient(Settings.Default.TelnetSendReceiveTimeoutMilliseconds);
#else
                    ITelnet tc = new TelnetClientFake();
#endif
                    this.domParser = new DomParser();

                    DomConfigurationHelper domConfigurationHelper =
                        new DomConfigurationHelper(AppDomain.CurrentDomain.BaseDirectory + "Config\\DomDeviceConfig" + deviceIP.Address.ToString().Replace(".", String.Empty) +
                                                   ".xml");

                    foreach (string telnetCommand in domConfigurationHelper.TelnetCommands) {
                        this.domParser.AddTelnetResponse(telnetCommand,
                                                         tc.LoginAndExecute(this.ipEndPoint, this.TelnetUsername, this.TelnetPassword, this.TelnetLoginMarker,
                                                                            this.TelnetPasswordMarker, this.TelnetPromptMarker, telnetCommand));
                    }

                    // Lasciando l'attributo TelnetCommandEvents del nodo Device della configurazione nullo, si evita la gestione degli Eventi
                    if (!String.IsNullOrEmpty(domConfigurationHelper.TelnetCommandEvents)) {
                        this.TelnetCommandEvents = domConfigurationHelper.TelnetCommandEvents;

                        this.domParser.AddTelnetResponse(domConfigurationHelper.TelnetCommandEvents,
                                 tc.LoginAndExecute(this.ipEndPoint, this.TelnetUsername, this.TelnetPassword, this.TelnetLoginMarker,
                                                    this.TelnetPasswordMarker, this.TelnetPromptMarker, domConfigurationHelper.TelnetCommandEvents));
                    }
                    else {
                        this.TelnetCommandEvents = null;
                    }

                    domConfigurationHelper.LoadConfiguration(this.streamData);
                }
                catch (TelnetException ex) {
                    this.errorOnTelnet = true;
                    this.LastError = ex;
                }
                catch (DomConfigurationHelperException ex) {
                    this.LastError = ex;
                }
                catch (DomParserException ex) {
                    this.LastError = ex;
                }
            }
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Nome del dispositivo
        /// </summary>
        public string DeviceName {
            get { return name; }
        }

        /// <summary>
        /// Codice univoco del dispositivo
        /// </summary>
        public string DeviceCode {
            get { return code; }
        }

        /// <summary>
        /// Lista degli Stream relativi al dispositivo
        /// </summary>
        public List<DBStream> StreamData {
            get { return streamData; }
        }

        /// <summary>
        /// Lista degli Eventi relativi al dispositivo
        /// </summary>
        public List<EventObject> EventsData {
            get { return this.eventsData; }
        }

        /// <summary>
        /// Username per l'accesso Telnet
        /// </summary>
        public string TelnetUsername {
            get { return this.telnetUsername; }
        }

        /// <summary>
        /// Password per l'accesso Telnet
        /// </summary>
        public string TelnetPassword {
            get { return this.telnetPassword; }
        }

        /// <summary>
        /// Stringa che permette di identificare una richiesta di username per l'autenticazione
        /// </summary>
        public string TelnetLoginMarker {
            get { return this.loginMarker; }
        }

        /// <summary>
        /// Stringa che permette di identificare una richiesta di password per l'autenticazione
        /// </summary>
        public string TelnetPasswordMarker {
            get { return this.passwordMarker; }
        }

        /// <summary>
        /// Stringa che permette di identificare il prompt di richiesta comandi Telnet
        /// </summary>
        public string TelnetPromptMarker {
            get { return this.promptMarker; }
        }

        /// <summary>
        /// Comando Telnet usato per il recupero dei dati degli Eventi / SysLog
        /// </summary>
        public string TelnetCommandEvents {
            get;
            private set;
        }

        /// <summary>
        /// <para>Severità relativa al Device.</para>
        /// <para>Nel caso il Device sia visibile come ICMP, ma non come Telnet, la severità è sconosciuta.</para>
        /// </summary>
        public Severity SeverityLevel {
            get {
                if (this.IsAliveICMP && !this.IsAliveTelnet) {
                    return Severity.Unknown;
                }

                Severity streamSeverity = Severity.Unknown;

                if (streamData.Count > 0) {
                    foreach (DBStream stream in streamData) {
                        if ((stream.SeverityLevel != Severity.Unknown) && ((streamSeverity == Severity.Unknown) || ((byte)stream.SeverityLevel > (byte)streamSeverity))) {
                            streamSeverity = stream.SeverityLevel;
                        }
                    }
                }

                return streamSeverity;
            }
        }

        /// <summary>
        /// Stato del Device, inteso come Online (raggiungibile almeno via Telnet) oppure Offline (non raggiungibile e non monitorabile)
        /// </summary>
        public byte Offline {
            get {
                if (IsAliveICMP && IsAliveTelnet) {
                    return 0;
                }

                if (!IsAliveICMP && IsAliveTelnet) {
                    return 0;
                }

                return 1;
            }
        }

        /// <summary>
        /// <para>Decodifica descrittiva dello stato del dispositivo</para>
        /// <para>0 = "Periferica in servizio" - Colore verde, significa che la periferica è in servizio e non presenta alcuna anomalia</para>
        /// <para>1 = "Periferica in stato di attenzione" - Colore giallo, significa che ci sono delle anomalie al funzionamento ma non pregiudicano il servizio del sistema</para>
        /// <para>2 = "Periferica in stato di errore" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
        /// <para>9 = "Periferica in stato da configurare" - Colore blu, la periferica risponde sullo stato base </para>
        /// <para>255 = "Stato sconosciuto" - Non è stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
        /// </summary>
        public string ValueDescriptionComplete {
            get {
                if ((this.IsAliveICMP && this.IsAliveTelnet) || (!this.IsAliveICMP && this.IsAliveTelnet)) {
                    switch (SeverityLevel) {
                        case Severity.OK:
                            return Settings.Default.DeviceStatusMessageOK;
                        case Severity.Warning:
                            return Settings.Default.DeviceStatusMessageWarning;
                        case Severity.Error:
                            return Settings.Default.DeviceStatusMessageError;
                        case Severity.MaintenanceMode:
                            return Settings.Default.DeviceStatusMessageMaintenanceMode;
                        case Severity.Unknown:
                            return Settings.Default.DeviceStatusMessageUnknown;
                    }
                }

                if (IsAliveICMP && !IsAliveTelnet) {
                    return Settings.Default.DeviceStatusMessageUnknown;
                }

                if (!IsAliveICMP && !IsAliveTelnet) {
                    return Settings.Default.DeviceStatusMessageUnreachable;
                }

                return Settings.Default.DeviceStatusMessageUnknown;
            }
        }

        /// <summary>
        /// Indica se il dispositivo è raggiungibile via ICMP (ping)
        /// </summary>
        public bool IsAliveICMP { get; private set; }

        /// <summary>
        /// Indica se il dispositivo è raggiungibile via Telnet
        /// </summary>
        public bool IsAliveTelnet { get; private set; }

        /// <summary>
        /// Indica se c'è stata una eccezione durante il caricamento del dispositivo
        /// </summary>
        public Exception LastError { get; private set; }

        /// <summary>
        /// Informazioni dell'ultimo evento in ordine temporale associato al dispositivo
        /// </summary>
        public EventObject LastEvent { get; private set; }

        #endregion

        #region Metodi privati

        private void RetrieveData() {
            foreach (DBStream stream in streamData) {
                foreach (DBStreamField field in stream.FieldData) {
                    if (this.errorOnTelnet) {
                        field.SeverityLevel = Severity.Unknown;
                        field.ValueDescription = "Valore non ricevuto";
                        field.Value = "n/d";
                    }
                    else {
                        if (this.domParser != null) {
                            List<string> value = null;
                            try {
                                value = this.domParser.Get(field.TelnetCommand, field.DomDataValue, field.SeekRowRegex);
                            }
                            catch (TelnetException ex) {
                                this.LastError = ex;
                            }
                            catch (DomConfigurationHelperException ex) {
                                this.LastError = ex;
                            }
                            catch (DomParserException ex) {
                                this.LastError = ex;
                            }

                            if ((value == null) || (value.Count == 0)) {
                                field.SeverityLevel = Severity.Unknown;
                                field.ValueDescription = "Valore non ricevuto";
                                field.Value = "n/d";
                            }
                            else {
                                field.RawData = value;
                            }
                        }
                        else {
                            field.SeverityLevel = Severity.Unknown;
                            field.ValueDescription = "Valore non ricevuto";
                            field.Value = "n/d";
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(this.TelnetCommandEvents)) {
                if (this.domParser != null) {
                    try {
                        this.eventsData = this.domParser.GetEvents(this.TelnetCommandEvents, this.LastEvent);
                    }
                    catch (TelnetException ex) {
                        this.LastError = ex;
                    }
                    catch (DomConfigurationHelperException ex) {
                        this.LastError = ex;
                    }
                    catch (DomParserException ex) {
                        this.LastError = ex;
                    }
                }
            }
        }

        private void CheckStatus() {
            GetIsAliveICMP();
            GetIsAliveTelnet();
        }

        private void GetIsAliveICMP() {
            pingRoundtripTime = -1;
            bool returnValue = false;

            var pingSender = new Ping();
            PingReply reply = null;

            try {
                reply = pingSender.Send(ipEndPoint.Address, Settings.Default.IcmpTimeoutMilliseconds);
            }
            catch (ArgumentNullException) {
            }
            catch (ArgumentOutOfRangeException) {
            }
            catch (PingException) {
            }
            catch (ObjectDisposedException) {
            }
            catch (InvalidOperationException) {
            }
            catch (NotSupportedException) {
            }
            catch (SocketException) {
            }

            if ((reply != null) && (reply.Status == IPStatus.Success)) {
                pingRoundtripTime = reply.RoundtripTime;
                returnValue = true;
            }

            IsAliveICMP = returnValue;
        }

        private void GetIsAliveTelnet() {
#if (!LOCAL)
            ITelnet tc = new TelnetClient(Settings.Default.TelnetSendReceiveTimeoutMilliseconds);
#else
            ITelnet tc = new TelnetClientFake();
#endif

            this.IsAliveTelnet = tc.TestConnectivity(this.ipEndPoint);
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Produce una rappresentazione completa e descrittiva dell'oggetto a fini diagnostici
        /// </summary>
        /// <returns></returns>
        public string Dump() {
            if (!isPopulated) {
                Populate();
            }

            var dump = new StringBuilder();

            dump.AppendFormat("Device Name: {0}, Code: {1}, Offline: {2}, Severity: {3}, ReplyICMP: {4} ({5}ms), ReplyTelnet: {6}" + Environment.NewLine, DeviceName, DeviceCode,
                              Offline, (byte)SeverityLevel, IsAliveICMP, pingRoundtripTime, IsAliveTelnet);

            foreach (DBStream stream in StreamData) {
                dump.AppendFormat("Stream ID: {0}, Name: {1}, Severity: {2}, Description: {3}" + Environment.NewLine, stream.StreamID, stream.Name, (byte)stream.SeverityLevel,
                                  stream.ValueDescriptionComplete);

                foreach (DBStreamField field in stream.FieldData) {
                    dump.AppendFormat("Field ID: {0}, Array ID: {1}, Name: {2}, Severity: {3}, Value: {4}, Description: {5}" + Environment.NewLine, field.FieldID, field.ArrayID,
                                      field.Name, (byte)field.SeverityLevel, field.Value, field.ValueDescriptionComplete);
                }
            }

            return dump.ToString();
        }

        /// <summary>
        /// Stampa tutti gli stream Telnet letti dalla periferica
        /// </summary>
        /// <returns></returns>
        public string DumpTelnetResponses() {
            if (this.domParser != null) {
                return this.domParser.TelnetResponsesDumper();
            }

            return String.Empty;
        }

        /// <summary>
        /// Popola i dati del dispositivo, in base agli Stream configurati
        /// </summary>
        public void Populate() {
            this.LastError = null;

            if (IsAliveTelnet) {
                RetrieveData();
            }

            if (this.LastError == null) {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData) {
                    foreach (DBStreamField field in stream.FieldData) {
                        field.Evaluate();
                    }
                }
            }
            else {
                this.isPopulated = false;
            }
        }

        #endregion
    }
}
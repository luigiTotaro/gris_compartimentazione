﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    public class DomParser {
        private const string NEWLINE = "\r\n";
        private const string NO_VALUE_AVAILABLE_STRING = "---";
        private readonly bool serverEchoActive;
        private const bool IS_SERVER_ECHO_ACTIVE = true;
        private const string UNKNOWN_TELNET_COMMAND_RESPONSE = "*** UNKNOWN COMMAND  \"";
        private const int MLST_COMMAND_HEADER_ROW_INDEX = 2;
        private const string MLST_COMMAND_HEADER_NUM_COLUMN = "Num  ";
        private const string MLST_COMMAND_HEADER_DATE_COLUMN = "Date      ";
        private const string MLST_COMMAND_HEADER_TIME_COLUMN = "Time    ";
        private const string MLST_COMMAND_HEADER_SYSTEM_COLUMN = "System";
        private const string MLST_COMMAND_HEADER_MESSAGE_COLUMN = "Message";
        private readonly Regex MLST_COMMAND_DATA_ROW_PATTERN;

        private Dictionary<string, string> telnetResponses;

        internal DomParser() {
            this.telnetResponses = null;
            this.serverEchoActive = IS_SERVER_ECHO_ACTIVE;
            this.MLST_COMMAND_DATA_ROW_PATTERN = new Regex("\\d{1,6}\\s+\\d{4}/\\d{2}/\\d{2}\\s+-\\s+\\d{2}:\\d{2}:\\d{2}\\s+SYS", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        }

        public void AddTelnetResponse(string telnetCommand, string telnetCommandResponse) {
            if ((telnetCommand == null) || (telnetCommandResponse == null)) {
                throw new DomParserException("Impossibile aggiungere un comando oppure una risposta Telnet nulla.");
            }

            if (telnetCommandResponse.IndexOf(UNKNOWN_TELNET_COMMAND_RESPONSE, StringComparison.OrdinalIgnoreCase) >= 0) {
                throw new DomParserException(String.Format("Il comando '{0}' non è stato correttamente interpretato dal server Telnet (i comandi sono case sensitive).",
                                                           telnetCommand));
            }

            if (this.telnetResponses == null) {
                this.telnetResponses = new Dictionary<string, string>();
            }

            System.Diagnostics.Debug.WriteLine(telnetCommand);

            if (!this.telnetResponses.ContainsKey(telnetCommand)) {
                this.telnetResponses.Add(telnetCommand, telnetCommandResponse);
            }
        }

        public List<string> Get(string telnetCommand, PEAVDOM.DomDataValues domDataValue) {
            return this.Get(telnetCommand, domDataValue, null);
        }

        public List<string> Get(string telnetCommand, PEAVDOM.DomDataValues domDataValue, Regex seekRowRegex) {
            List<string> returnValue;

            if ((this.telnetResponses != null) && (this.telnetResponses.Count > 0)) {
                if (this.telnetResponses.ContainsKey(telnetCommand)) {
                    string response = this.telnetResponses[telnetCommand];
                    returnValue = new List<string>();

                    switch (domDataValue) {
                        case PEAVDOM.DomDataValues.Version:
                            returnValue.Add(this.ParseValueFromRegexWithGroup(response, "version:\\s*(?<version>.*?)\\r\\n", "version"));
                            break;
                        case PEAVDOM.DomDataValues.DeviceType:
                            returnValue.Add(this.ParseValueFromRegexWithGroup(response, "device type:\\s*(?<devicetype>.*?)\\r\\n", "devicetype"));
                            break;
                        case PEAVDOM.DomDataValues.SerialNumber:
                            returnValue.Add(this.ParseValueFromRegexWithGroup(response, "serial number:\\s*(?<serialnumber>.*?)\\r\\n", "serialnumber"));
                            break;
                        case PEAVDOM.DomDataValues.DomStatus:
                            returnValue.Add(this.ParseValueFromResponse(response, "status", seekRowRegex, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomErrorState:
                            returnValue.Add(this.ParseValueFromResponse(response, "errorstate", seekRowRegex, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomDowntimeSeconds:
                            returnValue.Add(this.ParseValueFromResponse(response, "downtime[seconds]", seekRowRegex, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomIP:
                            returnValue.Add(this.ParseValueFromResponse(response, "       found ip", seekRowRegex, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomDeviceState:
                            returnValue.Add(this.ParseValueFromResponse(response, "State      ", seekRowRegex, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomDeviceDevAmp:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "DevAmp", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomDeviceConnection:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "Connection", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomImpedanceSupervisionAction:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "Action  ", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2));
                            break;
                        case PEAVDOM.DomDataValues.DomImpedanceSupervisionPowerNominal:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "Power(nominal)", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2));
                            break;
                        case PEAVDOM.DomDataValues.DomImpedanceSupervisionTolerance:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "  Tolerance", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2));
                            break;
                        case PEAVDOM.DomDataValues.DomImpedanceSupervisionSetpoint:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "       Setpoint", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2));
                            break;
                        case PEAVDOM.DomDataValues.DomImpedanceSupervisionValue:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "           Value", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2));
                            break;
                        case PEAVDOM.DomDataValues.DomPAReplacementStatus:
                            returnValue.Add(this.ParseValueFromResponse(response, "Replacement", seekRowRegex, 1));
                            break;
                        case PEAVDOM.DomDataValues.DomEarthLeakageSupervisionAction:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "Action  ", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2));
                            break;
                        case PEAVDOM.DomDataValues.DomEarthLeakageSupervisionWireA:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "       Ohm", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2, 0));
                            break;
                        case PEAVDOM.DomDataValues.DomEarthLeakageSupervisionWireB:
                            returnValue.Add(this.ParseValueFromLogWithNoValue(response, "       Ohm", seekRowRegex, NO_VALUE_AVAILABLE_STRING, 2, 30));
                            break;
                    }
                }
                else {
                    throw new DomParserException(
                        String.Format(
                            "Il comando Telnet su cui effettuare il parsing, non è stato caricato e quindi non sono disponibili dati da valutare. Comando richiesto '{0}'.",
                            telnetCommand));
                }
            }
            else {
                throw new DomParserException(
                    String.Format(
                        "Non sono disponibili risposte Telnet su cui effettuare il parsing, caricate in base all'attributo TelnetCommand sui vari Stream della configurazione."));
            }

            return returnValue;
        }

        public List<EventObject> GetEvents(string telnetCommand, EventObject lastEvent) {
            List<EventObject> returnValue = null;
            List<EventObject> eventList;

            if ((this.telnetResponses != null) && (this.telnetResponses.Count > 0)) {
                if (this.telnetResponses.ContainsKey(telnetCommand)) {
                    string response = this.telnetResponses[telnetCommand];
                    eventList = new List<EventObject>();

                    if (!String.IsNullOrEmpty(response)) {
                        string[] lines = Regex.Split(response, NEWLINE);

                        // nella prima riga c'è sempre il comando stesso inviato via telnet - il server fa echo
                        // la seconda riga contiene le intestazioni di colonna
                        // quindi, se ci sono almeno 3 righe, ci sono dati presenti
                        if ((lines != null) && (lines.Length > (MLST_COMMAND_HEADER_ROW_INDEX + (this.serverEchoActive ? 1 : 0)))) {
                            int columnNumPosition = lines[MLST_COMMAND_HEADER_ROW_INDEX].IndexOf(MLST_COMMAND_HEADER_NUM_COLUMN, StringComparison.OrdinalIgnoreCase);
                            int columnDatePosition = lines[MLST_COMMAND_HEADER_ROW_INDEX].IndexOf(MLST_COMMAND_HEADER_DATE_COLUMN, StringComparison.OrdinalIgnoreCase);
                            int columnTimePosition = lines[MLST_COMMAND_HEADER_ROW_INDEX].IndexOf(MLST_COMMAND_HEADER_TIME_COLUMN, StringComparison.OrdinalIgnoreCase);
                            int columnSystemPosition = lines[MLST_COMMAND_HEADER_ROW_INDEX].IndexOf(MLST_COMMAND_HEADER_SYSTEM_COLUMN, StringComparison.OrdinalIgnoreCase);
                            int columnMessagePosition = lines[MLST_COMMAND_HEADER_ROW_INDEX].IndexOf(MLST_COMMAND_HEADER_MESSAGE_COLUMN, StringComparison.OrdinalIgnoreCase);

                            if ((columnNumPosition >= 0) && (columnDatePosition >= 0) && (columnTimePosition >= 0) && (columnSystemPosition >= 0) && (columnMessagePosition >= 0)) {
                                CultureInfo provider = CultureInfo.InvariantCulture;

                                for (int lineCounter = (MLST_COMMAND_HEADER_ROW_INDEX + (this.serverEchoActive ? 1 : 0)); lineCounter < lines.Length; lineCounter++) {
                                    if (this.MLST_COMMAND_DATA_ROW_PATTERN.IsMatch(lines[lineCounter])) {
                                        string row = lines[lineCounter];

                                        if (!String.IsNullOrEmpty(row)) {
                                            string eventData = row.Trim();

                                            string dateString = row.Substring(columnDatePosition, (MLST_COMMAND_HEADER_DATE_COLUMN).Length).Trim();
                                            string timeString = row.Substring(columnTimePosition, (MLST_COMMAND_HEADER_TIME_COLUMN).Length).Trim();
                                            DateTime? eventCreated;
                                            try {
                                                eventCreated = DateTime.ParseExact(dateString + " " + timeString, "yyyy/MM/dd HH:mm:ss", provider);
                                            }
                                            catch (FormatException) {
                                                throw new DomParserException(String.Format("Impossibile interpretare la data/ora dell'evento. Data letta '{0}'. Riga completa '{1}'",
                                                                                           dateString + " " + timeString, row));
                                            }
                                            catch (ArgumentException) {
                                                throw new DomParserException(String.Format("Impossibile interpretare la data/ora dell'evento. Data letta '{0}'. Riga completa '{1}'",
                                                                                           dateString + " " + timeString, row));
                                            }

                                            if (!String.IsNullOrEmpty(eventData)) {
                                                EventObject evnt = new EventObject(null, null, eventData, eventCreated.Value);
                                                eventList.Add(evnt);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    throw new DomParserException(
                        String.Format(
                            "Il comando Telnet su cui effettuare il parsing, non è stato caricato e quindi non sono disponibili dati da valutare. Comando richiesto '{0}'.",
                            telnetCommand));
                }
            }
            else {
                throw new DomParserException(
                    String.Format(
                        "Non sono disponibili risposte Telnet su cui effettuare il parsing, caricate in base all'attributo 'TelnetCommandEvents' del nodo 'Device' della configurazione."));
            }

            if (eventList.Count > 0) {
                returnValue = new List<EventObject>();

                if (lastEvent == null) {
                    // Non esiste un ultimo evento per questo dispositivo, quindi sono da caricare tutti gli eventi disponibili
                    returnValue = eventList;
                }
                else {
                    int newEventIndex = -1;

                    for (int eventCounter = 0; eventCounter < eventList.Count; eventCounter++) {
                        // Gli eventi che arrivano dalla lista del syslog, sono sempre in ordine crescente, quindi, appena troviamo quello che cerchiamo, iniziamo a salvare
                        if (String.Equals(lastEvent.EventData, eventList[eventCounter].EventData, StringComparison.OrdinalIgnoreCase)) {
                            newEventIndex = eventCounter + 1;
                            break;
                        }
                    }

                    if (newEventIndex > 0) {
                        if (newEventIndex >= eventList.Count) {
                            // L'ultimo evento disponibile è ancora l'ultimo della lista, non c'è nulla da salvare
                            returnValue = null;
                        }
                        else {
                            for (int eventCounter = newEventIndex; eventCounter < eventList.Count; eventCounter++) {
                                returnValue.Add(eventList[eventCounter]);
                            }
                        }
                    }
                    else {
                        // Non avendo trovato l'ultimo evento, tutti quelli presenti sono da salvare
                        returnValue = eventList;
                    }
                }
            }

            return returnValue;
        }

        public string TelnetResponsesDumper() {
            var dump = new StringBuilder();

            foreach (var response in this.telnetResponses) {
                dump.AppendFormat(LogUtility.CreateSeparator('-', 100, response.Key) + Environment.NewLine);
                dump.AppendFormat(response.Value ?? String.Empty);
                dump.AppendFormat(Environment.NewLine + LogUtility.CreateSeparator('=', 100) + Environment.NewLine + Environment.NewLine);
            }

            return dump.ToString();
        }

        private string ParseValueFromRegexWithGroup(string text, string regularExpression, string groupName) {
            string returnValue = null;

            if (!String.IsNullOrEmpty(text)) {
                Regex regex = new Regex(regularExpression, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                Match match = regex.Match(text);

                if ((match != null) && (match.Groups.Count > 0)) {
                    if (match.Groups[groupName] != null) {
                        returnValue = match.Groups[groupName].Value;
                    }
                }
            }

            return returnValue;
        }

        private string ParseValueFromResponse(string text, string columnName, Regex seekRowRegex, int headerRowIndex) {
            return this.ParseValueFromResponse(text, columnName, seekRowRegex, headerRowIndex, 0);
        }

        private string ParseValueFromResponse(string text, string columnName, Regex seekRowRegex, int headerRowIndex, int searchColumnStartIndex) {
            string returnValue = null;

            if (!String.IsNullOrEmpty(text) && (seekRowRegex != null)) {
                string[] lines = Regex.Split(text, NEWLINE);

                // nella prima riga c'è sempre il comando stesso inviato via telnet - il server fa echo
                // la seconda riga contiene le intestazioni di colonna
                // quindi, se ci sono almeno 3 righe, ci sono dati presenti
                if ((lines != null) && (lines.Length > (headerRowIndex + (this.serverEchoActive ? 1 : 0)))) {
                    int columnPosition = lines[headerRowIndex].IndexOf(columnName, searchColumnStartIndex, StringComparison.OrdinalIgnoreCase);

                    if (columnPosition >= 0) {
                        // la riga delle intestazioni contiene il nome di colonna
                        for (int lineCounter = (headerRowIndex + (this.serverEchoActive ? 1 : 0)); lineCounter < lines.Length; lineCounter++) {
                            if (seekRowRegex.IsMatch(lines[lineCounter])) {
                                string row = lines[lineCounter];

                                if (!String.IsNullOrEmpty(row)) {
                                    if (row.Length >= (columnPosition + columnName.Length)) {
                                        // colonna interna ai risultati
                                        returnValue = row.Substring(columnPosition, columnName.Length).Trim();
                                        break;
                                    }

                                    if (row.Length >= columnPosition) {
                                        // ultima colonna dei risultati
                                        returnValue = row.Substring(columnPosition, row.Length - columnPosition).Trim();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return returnValue;
        }

        private string ParseValueFromLogWithNoValue(string text, string columnName, Regex seekRowRegex, string noValueString, int headerRowIndex) {
            return ParseValueFromLogWithNoValue(text, columnName, seekRowRegex, noValueString, headerRowIndex, 0);
        }

        private string ParseValueFromLogWithNoValue(string text, string columnName, Regex seekRowRegex, string noValueString, int headerRowIndex, int searchColumnStartIndex) {
            string returnValue = ParseValueFromResponse(text, columnName, seekRowRegex, headerRowIndex, searchColumnStartIndex);

            if ((!String.IsNullOrEmpty(returnValue)) && (!String.IsNullOrEmpty(noValueString))) {
                if (String.Equals(returnValue, noValueString)) {
                    // Il valore letto equivale alla stringa che indica "nessun valore disponibile"
                    returnValue = null;
                }
            }

            return returnValue;
        }
    }
}
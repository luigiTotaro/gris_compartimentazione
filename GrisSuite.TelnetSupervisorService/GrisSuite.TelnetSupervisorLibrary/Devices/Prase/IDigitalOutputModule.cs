using System;
using System.Collections.Generic;
using GrisSuite.TelnetSupervisorLibrary.Database;

namespace GrisSuite.TelnetSupervisorLibrary.Devices.Prase {
    public interface IDigitalOutputModule {
        /// <summary>
        /// Nome del dispositivo
        /// </summary>
        string DeviceName { get; }

        /// <summary>
        /// Codice univoco del dispositivo
        /// </summary>
        string DeviceCode { get; }

        /// <summary>
        /// Lista degli Stream relativi al dispositivo
        /// </summary>
        List<DBStream> StreamData { get; }

        /// <summary>
        /// Lista degli Eventi relativi al dispositivo
        /// </summary>
        List<EventObject> EventsData { get; }

        /// <summary>
        /// Username per l'accesso Telnet
        /// </summary>
        string TelnetUsername { get; }

        /// <summary>
        /// Password per l'accesso Telnet
        /// </summary>
        string TelnetPassword { get; }

        /// <summary>
        /// Stringa che permette di identificare una richiesta di username per l'autenticazione
        /// </summary>
        string TelnetLoginMarker { get; }

        /// <summary>
        /// Stringa che permette di identificare una richiesta di password per l'autenticazione
        /// </summary>
        string TelnetPasswordMarker { get; }

        /// <summary>
        /// Stringa che permette di identificare il prompt di richiesta comandi Telnet
        /// </summary>
        string TelnetPromptMarker { get; }

        /// <summary>
        /// <para>Severit� relativa al Device.</para>
        /// <para>Nel caso il Device sia visibile come ICMP, ma non come Telnet, la severit� � sconosciuta.</para>
        /// </summary>
        Severity SeverityLevel { get; }

        /// <summary>
        /// Stato del Device, inteso come Online (raggiungibile almeno via Telnet) oppure Offline (non raggiungibile e non monitorabile)
        /// </summary>
        byte Offline { get; }

        /// <summary>
        /// <para>Decodifica descrittiva dello stato del dispositivo</para>
        /// <para>0 = "Periferica in servizio" - Colore verde, significa che la periferica � in servizio e non presenta alcuna anomalia</para>
        /// <para>1 = "Periferica in stato di attenzione" - Colore giallo, significa che ci sono delle anomalie al funzionamento ma non pregiudicano il servizio del sistema</para>
        /// <para>2 = "Periferica in stato di errore" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
        /// <para>9 = "Periferica in stato da configurare" - Colore blu, la periferica risponde sullo stato base </para>
        /// <para>255 = "Stato sconosciuto" - Non � stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
        /// </summary>
        string ValueDescriptionComplete { get; }

        /// <summary>
        /// Indica se il dispositivo � raggiungibile via ICMP (ping)
        /// </summary>
        bool IsAliveICMP { get; }

        /// <summary>
        /// Indica se il dispositivo � raggiungibile via Telnet
        /// </summary>
        bool IsAliveTelnet { get; }

        /// <summary>
        /// Indica se c'� stata una eccezione durante il caricamento del dispositivo
        /// </summary>
        Exception LastError { get; }

        /// <summary>
        /// Produce una rappresentazione completa e descrittiva dell'oggetto a fini diagnostici
        /// </summary>
        /// <returns></returns>
        string Dump();

        /// <summary>
        /// Stampa tutti gli stream Telnet letti dalla periferica
        /// </summary>
        /// <returns></returns>
        string DumpTelnetResponses();

        /// <summary>
        /// Popola i dati del dispositivo, in base agli Stream configurati
        /// </summary>
        void Populate();
    }
}
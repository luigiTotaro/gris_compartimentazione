﻿using System.Net;
using GrisSuite.TelnetSupervisorLibrary.Properties;

namespace GrisSuite.TelnetSupervisorLibrary {
    /// <summary>
    /// Rappresenta l'oggetto di stato per il passaggio di informazioni relative al device
    /// </summary>
    public class DeviceObject {
        #region Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="DeviceID">ID del dispositivo</param>
        /// <param name="Name">Nome del dispositivo</param>
        /// <param name="Type">Tipo del dispositivo</param>
        /// <param name="Address">Indirizzo IP del dispositivo</param>
        /// <param name="RackName">Nome del Rack</param>
        /// <param name="BuildingName">Nome del Fabbricato</param>
        /// <param name="BuildingDescription">Descrizione del Fabbricato</param>
        /// <param name="StationName">Nome della Stazione</param>
        /// <param name="RegionID">ID del Compartimento</param>
        /// <param name="RegionName">Nome del Compartimento</param>
        /// <param name="ZoneID">ID della Zona</param>
        /// <param name="ZoneName">Nome della Zona</param>
        /// <param name="NodeID">ID del Nodo</param>
        /// <param name="NodeName">Nome del Nodo</param>
        /// <param name="VendorName">Nome del Produttore</param>
        public DeviceObject(long DeviceID, string Name, string Type, IPAddress Address, string RackName, string BuildingName, string BuildingDescription, string StationName,
                            long RegionID, string RegionName, long ZoneID, string ZoneName, long NodeID, string NodeName, string VendorName) {
            this.DeviceID = DeviceID;
            this.Name = Name;
            this.Type = Type;
            this.Address = Address;
            this.RackName = RackName;
            this.BuildingName = BuildingName;
            this.BuildingDescription = BuildingDescription;
            this.StationName = StationName;
            this.RegionID = RegionID;
            this.RegionName = RegionName;
            this.ZoneID = ZoneID;
            this.ZoneName = ZoneName;
            this.NodeID = NodeID;
            this.NodeName = NodeName;
            this.VendorName = VendorName;
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// ID del dispositivo
        /// </summary>
        public long DeviceID { get; private set; }

        /// <summary>
        /// Nome del dispositivo
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Tipo del dispositivo
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// Indirizzo IP del dispositivo
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Endpoint completo del dispositivo (indirizzo IP e porta Telnet)
        /// </summary>
        public IPEndPoint EndPoint {
            get { return new IPEndPoint(Address, Settings.Default.TelnetPort); }
        }

        /// <summary>
        /// Nome del Rack
        /// </summary>
        public string RackName { get; private set; }

        /// <summary>
        /// Nome del Fabbricato
        /// </summary>
        public string BuildingName { get; private set; }

        /// <summary>
        /// Descrizione del Fabbricato
        /// </summary>
        public string BuildingDescription { get; private set; }

        /// <summary>
        /// Nome della Stazione
        /// </summary>
        public string StationName { get; private set; }

        /// <summary>
        /// ID del Compartimento
        /// </summary>
        public long RegionID { get; private set; }

        /// <summary>
        /// Nome del Compartimento
        /// </summary>
        public string RegionName { get; private set; }

        /// <summary>
        /// ID della Zona
        /// </summary>
        public long ZoneID { get; private set; }

        /// <summary>
        /// Nome della Zona
        /// </summary>
        public string ZoneName { get; private set; }

        /// <summary>
        /// ID del Nodo
        /// </summary>
        public long NodeID { get; private set; }

        /// <summary>
        /// Nome del Nodo
        /// </summary>
        public string NodeName { get; private set; }

        /// <summary>
        /// Nome del Produttore
        /// </summary>
        public string VendorName { get; private set; }

        /// <summary>
        /// Informazioni dell'ultimo evento in ordine temporale associato al dispositivo
        /// </summary>
        public EventObject LastEvent { get; set; }

        #endregion
    }
}
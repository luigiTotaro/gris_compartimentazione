﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GrisSuite.TelnetSupervisorLibrary.Telnet {
    internal class TelnetClient : ITelnet {
        private bool isConnected;
        private int connectionTimeout;
        private TcpClient tcpClient;
        private NetworkStream netStream;

        #region Telnet Control Codes and Options

        private const byte IAC = 255;
        private const byte DONT = 254;
        private const byte DO = 253;
        private const byte WONT = 252;
        private const byte WILL = 251;

        /// <summary>
        /// END OF FILE
        /// </summary>
        private const byte EOF = 236;

        /// <summary>
        /// SUSPEND
        /// </summary>
        private const byte SUSP = 237;

        /// <summary>
        /// ABORT PROCESS
        /// </summary>
        private const byte ABORT = 238;

        /// <summary>
        /// END OF RECORD
        /// </summary>
        private const byte EOR = 239;

        /// <summary>
        /// START OF SUBNEGOTIATION PARAMETERS
        /// </summary>
        private const byte SB = 250;

        /// <summary>
        /// GO AHEAD
        /// </summary>
        private const byte GA = 249;

        /// <summary>
        /// ERASE LINE
        /// </summary>
        private const byte EL = 248;

        /// <summary>
        /// ERASE CHARACTER
        /// </summary>
        private const byte EC = 247;

        /// <summary>
        /// ARE YOU THERE
        /// </summary>
        private const byte AYT = 246;

        /// <summary>
        /// ABORT OUTPUT
        /// </summary>
        private const byte AO = 245;

        /// <summary>
        /// INTERRUPT PROCESS
        /// </summary>
        private const byte INTERRUPT_PROCESS = 244;

        /// <summary>
        /// NVT BREAK CHARACTER
        /// </summary>
        private const byte BRK = 243;

        /// <summary>
        /// DATA MARK
        /// </summary>
        private const byte DM = 242;

        /// <summary>
        /// NO OPERATION
        /// </summary>
        private const byte NOP = 241;

        /// <summary>
        /// END OF SUBNEGOTIATION PARAMETERS
        /// </summary>
        private const byte SE = 240;

        #endregion

        public int ConnectionTimeout {
            get { return this.connectionTimeout; }
            set { this.connectionTimeout = value; }
        }

        public TelnetClient(int sendReceiveTimeout) {
            this.isConnected = false;
            this.connectionTimeout = sendReceiveTimeout;
        }

        public IPEndPoint DeviceEndPoint { get; private set; }

        public bool TestConnectivity(IPEndPoint deviceIP) {
            bool returnValue = false;

            if (deviceIP == null) {
                throw new TelnetException("Informazioni per la connessione Telnet mancanti (indirizzo IP e porta del dispositivo).");
            }

            this.DeviceEndPoint = deviceIP;

            this.tcpClient = new TcpClient {ReceiveTimeout = this.connectionTimeout, SendTimeout = this.connectionTimeout};

            this.Connect(deviceIP);

            if (tcpClient.Connected) {
                netStream = tcpClient.GetStream();

                if ((netStream.CanRead) && (netStream.CanWrite)) {
                    returnValue = true;
                }
            }

            this.Disconnect();

            return returnValue;
        }

        public string LoginAndExecute(IPEndPoint deviceIP, string username, string password, string loginMarker, string passwordMarker, string promptMarker, string command) {
            if (deviceIP == null) {
                throw new TelnetException("Informazioni per la connessione Telnet mancanti (indirizzo IP e porta del dispositivo).");
            }

            this.tcpClient = new TcpClient {ReceiveTimeout = this.connectionTimeout, SendTimeout = this.connectionTimeout};

            this.Connect(deviceIP);

            if (tcpClient.Connected) {
                netStream = tcpClient.GetStream();

                if ((netStream.CanRead) && (netStream.CanWrite)) {
                    string returnData = this.ReceiveData();

                    if (!String.IsNullOrEmpty(command)) {
                        if ((returnData.Length > 0) && (returnData.IndexOf(loginMarker, StringComparison.OrdinalIgnoreCase) >= 0)) {
                            returnData = this.DoCommand(username, true);

                            if ((returnData.Length > 0) && (returnData.IndexOf(passwordMarker, StringComparison.OrdinalIgnoreCase) >= 0)) {
                                returnData = this.DoCommand(password, true);
                            }

                            if (returnData.EndsWith(promptMarker)) {
                                returnData = this.DoCommand(command, true);

                                this.Disconnect();

                                return returnData;
                            }
                        }
                    }
                    else {
                        this.Disconnect();

                        return returnData;
                    }
                }
                else {
                    this.Disconnect();

                    throw new TelnetException(String.Format("Impossibile leggere e scrivere nello stream dati della connessione Telnet, dispositivo {0}", deviceIP));
                }
            }
            else {
                this.Disconnect();

                throw new TelnetException(String.Format("Impossibile stabilire una connessione Telnet, dispositivo {0}", deviceIP));
            }

            return null;
        }

        private void SendCommand(string command) {
            Byte[] sendBytes = Encoding.ASCII.GetBytes(command + Environment.NewLine);
            netStream.Write(sendBytes, 0, sendBytes.Length);
        }

        private string DoCommand(string command, bool expectResponse) {
            if (command.Length > 0) {
                this.SendCommand(command);
            }

            if (expectResponse) {
                byte[] returnDataBytes;

                System.Diagnostics.Debug.WriteLine(String.Format("{0} Inizio recupero dati Telnet, comando {1}", DateTime.Now, command));

                int bytesRead = this.ReceiveBytes(out returnDataBytes);

                int dataOffset;

                RemoveControlChars(returnDataBytes, bytesRead, out dataOffset);

                System.Diagnostics.Debug.WriteLine(String.Format("{0} Fine recupero dati Telnet, comando {1}", DateTime.Now, command));

                return Encoding.ASCII.GetString(returnDataBytes, dataOffset, bytesRead - dataOffset);
            }

            return String.Empty;
        }

        private string ReceiveData() {
            byte[] returnDataBytes;

            int bytesRead = this.ReceiveBytes(out returnDataBytes);

            int dataOffset;

            RemoveControlChars(returnDataBytes, bytesRead, out dataOffset);

            return Encoding.UTF8.GetString(returnDataBytes, dataOffset, bytesRead - dataOffset);
        }

        private static void RemoveControlChars(byte[] response, int bytesToDescribe, out int dataOffset) {
            dataOffset = 0;

            for (int byteCounter = 0; byteCounter < bytesToDescribe; byteCounter++) {
                switch (response[byteCounter]) {
                    case IAC:
                        dataOffset++;

                        switch (response[byteCounter + 1]) {
                            case DONT:
                            case DO:
                            case WONT:
                            case WILL:
                                dataOffset += 2;
                                byteCounter++;
                                break;
                            case SB:
                            case GA:
                            case EL:
                            case EC:
                            case AYT:
                            case AO:
                            case INTERRUPT_PROCESS:
                            case BRK:
                            case DM:
                            case NOP:
                            case EOR:
                            case ABORT:
                            case SUSP:
                            case EOF:
                            case SE:
                                dataOffset++;
                                break;
                        }
                        byteCounter++;
                        break;
                }
            }
        }

        private int ReceiveBytes(out byte[] returnDataBytes) {
            byte[] bytes = new byte[tcpClient.ReceiveBufferSize];

            List<byte> data = new List<byte>();

            Thread.Sleep(this.connectionTimeout);

            do {
                int bytesRead = this.netStream.Read(bytes, 0, this.tcpClient.ReceiveBufferSize);

                for (int byteCounter = 0; byteCounter < bytesRead; byteCounter++) {
                    data.Add(bytes[byteCounter]);
                }

                Thread.Sleep(this.connectionTimeout);
            } while (this.tcpClient.Available > 0);

            returnDataBytes = data.ToArray();

            return returnDataBytes.Length;
        }

        private void Connect(IPEndPoint deviceIP) {
            try {
                this.tcpClient.Connect(deviceIP);

                this.isConnected = this.tcpClient.Connected;
            }
            catch (SocketException) {
                this.Disconnect();

                throw new TelnetException(String.Format("Impossibile stabilire una connessione Telnet, dispositivo {0}", deviceIP));
            }
        }

        private void Disconnect() {
            if (this.isConnected) {
                if (netStream != null) {
                    netStream.Close();
                }

                if (tcpClient != null) {
                    tcpClient.Close();
                }

                this.isConnected = false;
            }
        }
    }
}
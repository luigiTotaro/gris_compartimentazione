﻿using System;
using System.IO;
using System.Text;

namespace GrisSuite.TelnetSupervisorLibrary.Telnet {
    internal class TelnetClientFake : ITelnet {
        public int ConnectionTimeout { get; set; }

        public bool TestConnectivity(System.Net.IPEndPoint deviceIP) {
            return true;
        }

        public string LoginAndExecute(System.Net.IPEndPoint deviceIP, string username, string password, string loginMarker, string passwordMarker, string promptMarker,
                                      string command) {
            string response = null;
            string fileName;

            if (command.StartsWith("mlst")) {
                fileName = AppDomain.CurrentDomain.BaseDirectory + "FakeResponses\\mlst.txt";

                if (CheckFileCanRead(fileName)) {
                    response = ReadFile(fileName);
                }
            }
            else {
                switch (command) {
                    case "": // Welcome
                        fileName = AppDomain.CurrentDomain.BaseDirectory + "FakeResponses\\Welcome.txt";

                        if (CheckFileCanRead(fileName)) {
                            response = ReadFile(fileName);
                        }
                        break;
                    case "sysstat":
                    case "dvstat":
                    case "earthlst":
                    case "implst":
                    case "repl":
                        fileName = AppDomain.CurrentDomain.BaseDirectory + "FakeResponses\\" + command + ".txt";

                        if (CheckFileCanRead(fileName)) {
                            response = ReadFile(fileName);
                        }
                        break;
                    default:
                        response = "*** UNKNOWN COMMAND  \"" + command + Environment.NewLine;
                        break;
                }
            }

            return response;
        }

        private static bool CheckFileCanRead(string fileName) {
            bool returnValue;

            FileStream fs = null;
            try {
                if (!File.Exists(fileName)) {
                    returnValue = false;
                }
                else {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    returnValue = fs.CanRead;
                }
            }
            catch (UnauthorizedAccessException) {
                returnValue = false;
            }
            catch (IOException) {
                returnValue = false;
            }
            finally {
                if (fs != null) {
                    fs.Close();
                }
            }

            return returnValue;
        }

        private static string ReadFile(string fileName) {
            string file = null;

            FileStream fs = null;
            try {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                byte[] fileData = new byte[fs.Length];

                if (fs.Read(fileData, 0, (int) fs.Length) > 0) {
                    file = Encoding.ASCII.GetString(fileData);
                }
            }
            catch (UnauthorizedAccessException) {
            }
            catch (IOException) {
            }
            finally {
                if (fs != null) {
                    fs.Close();
                }
            }

            return file;
        }
    }
}
﻿using System.Net;

namespace GrisSuite.TelnetSupervisorLibrary.Telnet {
    internal interface ITelnet {
        int ConnectionTimeout { get; set; }
        bool TestConnectivity(IPEndPoint deviceIP);
        string LoginAndExecute(IPEndPoint deviceIP, string username, string password, string loginMarker, string passwordMarker, string promptMarker, string command);
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.TelnetSupervisorLibrary.Telnet {
    public class TelnetException : Exception {
        public TelnetException() {
        }

        public TelnetException(string message) : base(message) {
        }

        public TelnetException(string message, Exception inner) : base(message, inner) {
        }

        public TelnetException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }
}
<%@ page language="C#" masterpagefile="~/MasterPage.master" autoeventwireup="true"
	codefile="Default.aspx.cs" inherits="_Default" meta:resourcekey="DefaultPage"%>

<asp:content id="cntMainArea" contentplaceholderid="cphMainArea" runat="Server">
	<table cellpadding="0" cellspacing="0" style="width: 800px; text-align: left;">
		<tr style="height: 30px">
			<td colspan="3" class="nav_bg">
				<table border="0" cellpadding="0" cellspacing="0" style="width: 800px; height: 30px">
					<tr>
						<td style="width: 400px; height: 30px;">
							<table id="tblMenu" style="width: 100%" cellpadding="0" cellspacing="0">
								<tr>
									<td id="tdDevices" runat="server" class="MenuItem" style="text-align: center;">
										<asp:linkbutton id="lnkbDevices" runat="server" onclick="lnkbDevices_Click" width="200px"
											cssclass="MenuItem" meta:resourcekey="lnkbDevices">Esplora periferiche</asp:linkbutton>
									</td>
									<td id="tdProblematicDevices" runat="server" class="MenuItem">
										<asp:linkbutton id="lnkbProblematicDevices" runat="server" onclick="lnkbProblematicDevices_Click"
											width="200px" cssclass="MenuItem" meta:resourcekey="lnkbProblematicDevices">Periferiche in anomalia</asp:linkbutton>
									</td>
									<td>
										<asp:imagebutton id="imgUploadDownload" runat="server" height="25px"
										imageurl="~/Images/up-down-load.gif" tooltip="Upload / download" 
										alternatetext="Upload / download" onclick="imgUploadDownload_Click" />
									</td>
								</tr>
							</table>
						</td>
						<td align="right" valign="top" style="width: 330px;">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 250px; height: 30px">
								<tr>
									<td style="background-image: url(Images/nav_search_btnsx_active.gif); background-repeat: no-repeat;">
										<asp:button id="btnRicercaOK" runat="server" onclick="Button1_Click" width="15px"
											height="30px" cssclass="box_ricerca_btn" meta:resourcekey="btnRicercaOK" />
									</td>
									<td style="background-image: url(Images/nav_search_bg_active.gif); background-repeat: repeat-x;">
										<asp:textbox id="txtboxricerca" runat="server" width="200px" height="20px" ontextchanged="TextBox1_TextChanged"
											cssclass="box_ricerca"></asp:textbox>
									</td>
									<td style="background-image: url(Images/nav_search_btndx_active.gif); background-repeat: no-repeat;
										width: 20px">
										<asp:button id="btnRicercaCancel" runat="server" cssclass="box_ricerca_btn" onclick="btnRicercaCancel_Click"
											width="20px" meta:resourcekey="btnRicercaCancel" />
									</td>
								</tr>
							</table>
						</td>
						<td style="width: 70px; text-align: right;">
							<asp:imagebutton id="imgbRefreshPage" runat="server" style="display: block; float: right;"
								imageurl="~/Images/btnAggiorna_default.gif" alternatetext="Aggiorna Pagina" tooltip="Aggiorna Pagina"
								onclick="imgbRefreshPage_Click" meta:resourcekey="imgbRefreshPage" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="height: 20px">
			<td colspan="3" class="nav_shadow_bg">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table border="0" cellpadding="5" cellspacing="0" class="panel_bg">
					<tr>
						<td class="panel_bg_sx">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td class="lblBox">
										<asp:label id="lblBoxSx" runat="server" text="Sistema" meta:resourcekey="lblBoxSx"></asp:label>
									</td>
								</tr>
								<tr>
									<td>
										<img alt="" src="Images/boxSX_top.gif" style="vertical-align: bottom;" />
									</td>
								</tr>
								<tr>
									<td class="box_bg">
										<asp:updatepanel id="updTreeView" runat="server" updatemode="Conditional">
											<contenttemplate>
												<asp:treeview id="tvwDevices" runat="server" imageset="Arrows" onselectednodechanged="tvwDevices_SelectedNodeChanged"
													width="250px" nodeindent="10" meta:resourcekey="tvwDevices">
													<levelstyles>
														<asp:treenodestyle font-names="Arial" font-size="10pt" forecolor="#A8A8A8" horizontalpadding="2px"
															nodespacing="0px" verticalpadding="0px" font-underline="False" />
														<asp:treenodestyle font-names="Arial" font-size="10pt" forecolor="#A8A8A8" horizontalpadding="2px"
															nodespacing="0px" verticalpadding="0px" font-underline="False" />
														<asp:treenodestyle forecolor="White" font-underline="False" />
													</levelstyles>
													<parentnodestyle font-bold="False" />
													<hovernodestyle font-bold="False" font-underline="False" forecolor="#0885CC" />
													<selectednodestyle font-underline="False" forecolor="White" />
													<nodestyle font-names="Arial" font-size="10pt" forecolor="#A8A8A8" horizontalpadding="2px"
														nodespacing="0px" verticalpadding="0px" />
												</asp:treeview>
											</contenttemplate>
											<triggers>
												<asp:asyncpostbacktrigger controlid="imgbRefreshPage" eventname="Click" />
											</triggers>
										</asp:updatepanel>
									</td>
								</tr>
								<tr>
									<td>
										<img alt="" src="Images/boxSX_bot.gif" style="vertical-align: top;" />
									</td>
								</tr>
							</table>
						</td>
						<td class="panel_bg_dx">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td class="lblBox">
										<asp:label id="lblBoxDx" runat="server"></asp:label>
									</td>
								</tr>
								<tr>
									<td>
										<img alt="" src="Images/boxDX_top.gif" style="vertical-align: bottom;" />
									</td>
								</tr>
								<tr>
									<td class="box_bg">
										<asp:updatepanel id="updDetail" runat="server" updatemode="Conditional">
											<contenttemplate>
												<asp:multiview id="MultiView1" runat="server" activeviewindex="0">
													<asp:view id="View1" runat="server" onactivate="View1_Activate" ondeactivate="View1_Deactivate">
														<asp:gridview id="GridViewDevOff" runat="server" autogeneratecolumns="False" datakeynames="DevID"
															datasourceid="SqlDataSourceGetOffline" width="100%" borderwidth="0px" onrowcommand="GridViewDevOff_RowCommand"
															onrowdatabound="GridViewDevOff_RowDataBound" font-names="Arial" enablemodelvalidation="True"
															meta:resourcekey="GridViewDevOff">
															<columns>
																<asp:templatefield>
																	<itemtemplate>
																		<asp:image id="imgOff" runat="server" imageurl="Images/stato_3.gif" meta:resourcekey="imgOff" />
																	</itemtemplate>
																	<itemstyle horizontalalign="Left" verticalalign="Middle" width="30px" />
																</asp:templatefield>
																<asp:boundfield datafield="Status" headertext="Status" readonly="True" sortexpression="Status"
																	visible="False" meta:resourcekey="BoundFieldStatus" />
																<asp:boundfield datafield="DevID" headertext="DevID" readonly="True" sortexpression="DevID"
																	visible="False" />
																<asp:buttonfield datatextfield="Name" headertext="<%$ CapResources:GrismResources, Name %>" text="<%$ CapResources:GrismResources, Name %>">
																	<controlstyle cssclass="GridViewButton" />
																</asp:buttonfield>
																<asp:boundfield datafield="Name" headertext="Nome" sortexpression="Name" visible="False">
																	<itemstyle width="170px" />
																</asp:boundfield>
																<asp:boundfield datafield="Description" headertext="<%$ CapResources:GrismResources, Description %>" sortexpression="Description"
																	meta:resourcekey="BoundFieldDescription">
																	<itemstyle width="270px" cssclass="GridViewRow" />
																</asp:boundfield>
																<asp:boundfield datafield="SevLevel" headertext="SevLevel" readonly="True" sortexpression="SevLevel"
																	visible="False" meta:resourcekey="BoundFieldSevLevel" />
																<asp:boundfield datafield="DeviceType" headertext="<%$ CapResources:GrismResources, Type %>" sortexpression="DeviceType">
																	<itemstyle width="100px" cssclass="GridViewRow" />
																</asp:boundfield>
															</columns>
															<headerstyle backcolor="Transparent" forecolor="Black" borderstyle="None" />
														</asp:gridview>
														<asp:sqldatasource id="SqlDataSourceGetOffline" runat="server" connectionstring="<%$ ConnectionStrings:Local %>"
															selectcommand="sWEB_GetOffDevList" selectcommandtype="StoredProcedure"></asp:sqldatasource>
														<asp:gridview id="GridViewDev" runat="server" autogeneratecolumns="False" borderwidth="0px"
															datakeynames="DevID" datasourceid="SqlDataSource4" onrowdatabound="GridViewDev_RowDataBound"
															width="100%" onrowcommand="GridViewDev_RowCommand" showheader="False" font-names="Arial"
															enablemodelvalidation="True" meta:resourcekey="GridViewDev">
															<columns>
																<asp:templatefield>
																	<itemtemplate>
																		<asp:image id="imgStatus" runat="server" />
																	</itemtemplate>
																	<itemstyle horizontalalign="Left" verticalalign="Middle" width="30px" />
																</asp:templatefield>
																<asp:boundfield datafield="DevID" headertext="DevID" visible="False" />
																<asp:boundfield datafield="Name" headertext="Nome" sortexpression="Name" visible="False">
																	<headerstyle horizontalalign="Left" />
																	<itemstyle width="170px" />
																</asp:boundfield>
																<asp:buttonfield datatextfield="Name" headertext="<%$ CapResources:GrismResources, Name %>" text="<%$ CapResources:GrismResources, Name %>">
																	<controlstyle cssclass="GridViewButton" />
																</asp:buttonfield>
																<asp:boundfield datafield="Description" headertext="<%$ CapResources:GrismResources, Description %>" sortexpression="Description">
																	<headerstyle horizontalalign="Left" />
																	<itemstyle width="270px" cssclass="GridViewRow" />
																</asp:boundfield>
																<asp:boundfield datafield="Status" headertext="Stato" sortexpression="Status" visible="False"
																	meta:resourcekey="BoundFieldStatus" />
																<asp:boundfield datafield="SevLevel" headertext="SevLevel" sortexpression="SevLevel"
																	visible="False" meta:resourcekey="BoundFieldSevLevel" />
																<asp:boundfield datafield="DeviceType" headertext="<%$ CapResources:GrismResources, Type %>" sortexpression="DeviceType"
																	meta:resourcekey="BoundFieldDeviceType">
																	<itemstyle width="100px" cssclass="GridViewRow" />
																</asp:boundfield>
															</columns>
															<headerstyle backcolor="Transparent" forecolor="Black" borderstyle="None" />
														</asp:gridview>
														<asp:sqldatasource id="SqlDataSource4" runat="server" connectionstring="<%$ ConnectionStrings:Local %>"
															selectcommand="sWEB_GetBadDevList" selectcommandtype="StoredProcedure"></asp:sqldatasource>
														<asp:label id="lblNoData" runat="server" cssclass="lblNoData" text="Nessuna periferica in anomalia"
															width="100%" visible="False" meta:resourcekey="lblNoData"></asp:label></asp:view>
													<asp:view id="View2" runat="server" onactivate="View2_Activate" ondeactivate="View2_Deactivate">
														<asp:formview id="FormDettaglio" runat="server" datasourceid="SqlDataSource3" width="100%"
															borderwidth="0px" cellpadding="0" onprerender="FormDettaglio_PreRender" enablemodelvalidation="True">
															<itemtemplate>
																<div style="width: 520px; padding-left: 5px; padding-right: 5px;">
																	<table width="100%" cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td colspan="2" style="height: 23px; text-align: right;">
																				<asp:label id="Label7" runat="server" text='<%# chkOffline(Eval("Offline")) %>' cssclass="OfflineStyle"></asp:label>
																			</td>
																		</tr>
																		<tr>
																			<td colspan="2" class="FormDettaglioHeader" style="text-align: center;">
																				<asp:label id="Label2" runat="server" text='<%# Eval("Name") + " - " + Eval("Type") %>'></asp:label>
																			</td>
																		</tr>
																		<tr class="FormDettaglioRiga">
																			<td class="colonnaEtichetta">
																				<asp:localize id="locInterface" runat="server" meta:resourcekey="locInterface"></asp:localize>:
																			</td>
																			<td class="colonnaDato">
																				<asp:label id="lblInterface" runat="server" text='<%# GetInterfaceDescription(Eval("PortName").ToString(), Eval("Addr").ToString(), Eval("PortType").ToString()) %>'></asp:label>
																			</td>
																		</tr>
																		<tr class="FormDettaglioRiga">
																			<td class="colonnaEtichetta">
																				<asp:localize id="locSerialNumber" runat="server" meta:resourcekey="locSerialNumber"></asp:localize>:
																			</td>
																			<td class="colonnaDato">
																				<asp:label id="lblSerialNumber" runat="server" text='<%# Eval("SN") %>'></asp:label>
																			</td>
																		</tr>
																		<tr class="FormDettaglioRiga">
																			<td class="colonnaEtichetta">
																				<asp:localize id="locLocation" runat="server" meta:resourcekey="locLocation"></asp:localize>:
																			</td>
																			<td class="colonnaDato">
																				<asp:label id="lblLocation" runat="server" text='<%# Eval("BuildingName") + " > " + Eval("RackName") %>'></asp:label>
																			</td>
																		</tr>
																		<tr>
																			<td colspan="2">
																				<asp:textbox id="txtSevLevel" runat="server" text='<%# Eval("Stato") %>' width="100%"
																					cssclass="FormDettaglioFooter" forecolor='<%# getColorCode((int)Eval("SevLevel")) %>'></asp:textbox>
																			</td>
																		</tr>
																	</table>
																</div>
															</itemtemplate>
														</asp:formview>
														<asp:sqldatasource id="SqlDataSource3" runat="server" connectionstring="<%$ ConnectionStrings:Local %>"
															selectcommand="sWEB_GetDevDetailsbyDevID" selectcommandtype="StoredProcedure">
															<selectparameters>
																<asp:sessionparameter defaultvalue="" name="DevID" sessionfield="DevID" type="Int64" />
															</selectparameters>
														</asp:sqldatasource>
														<table width="100%" class="tabellaCampi" cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td colspan="2" rowspan="">
																	<asp:label id="lblStrList" runat="server" cssclass="lblBox" text="Informazioni ricevute"
																		width="100%" visible="False" meta:resourcekey="lblStrList"></asp:label>
																</td>
															</tr>
															<tr>
																<td valign="top" colspan="2">
																	<asp:gridview id="GridViewStreams" runat="server" autogeneratecolumns="False" datakeynames="DevID,StrID"
																		datasourceid="SqlDataSource1" width="100%" onrowdatabound="GridViewStreams_RowDataBound"
																		onrowcommand="GridViewStreams_RowCommand" cellpadding="4" gridlines="None" onprerender="GridViewStreams_PreRender"
																		showheader="False" enablemodelvalidation="True" meta:resourcekey="GridViewStreams">
																		<columns>
																			<asp:templatefield>
																				<itemtemplate>
																					<asp:image id="imgSevLevel" runat="server" imageurl='<%# chkStatus((int)Eval("SevLevel")) %>'
																						meta:resourcekey="imgSevLevel" />
																				</itemtemplate>
																				<itemstyle horizontalalign="Left" verticalalign="Middle" />
																			</asp:templatefield>
																			<asp:boundfield datafield="DevID" headertext="DevID" visible="False" />
																			<asp:boundfield datafield="StrID" headertext="StrID" visible="False" />
																			<asp:buttonfield datatextfield="Name" headertext="<%$ CapResources:GrismResources, Name %>" text="<%$ CapResources:GrismResources, Name %>">
																				<controlstyle cssclass="GridViewButton" />
																			</asp:buttonfield>
																			<asp:boundfield datafield="Name" sortexpression="Name" visible="False" />
																			<asp:boundfield datafield="Data" headertext="<%$ CapResources:GrismResources, Date %>" sortexpression="Data">
																				<itemstyle cssclass="GridViewRow" />
																			</asp:boundfield>
																			<asp:boundfield datafield="Ora" headertext="Ora" sortexpression="Ora" meta:resourcekey="BoundFieldHour">
																				<itemstyle cssclass="GridViewRow" />
																			</asp:boundfield>
																		</columns>
																		<selectedrowstyle forecolor="White" font-bold="True" font-names="Arial" font-size="10pt" />
																		<headerstyle backcolor="Transparent" forecolor="Black" borderstyle="None" />
																	</asp:gridview>
																	<asp:sqldatasource id="SqlDataSource1" runat="server" connectionstring="<%$ ConnectionStrings:Local %>"
																		selectcommand="sWEB_GetStreamsbyDevID" selectcommandtype="StoredProcedure">
																		<selectparameters>
																			<asp:sessionparameter name="DevID" sessionfield="DevID" type="Int64" />
																		</selectparameters>
																	</asp:sqldatasource>
																	<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
																		<tr>
																			<td class="btn_espansione">
																				<asp:imagebutton id="imgAutomaticExpansion" runat="server" alternatetext="Attiva Espansione automatica"
																					onclick="imgAutomaticExpansion_Click" imageurl="~/Images/box2_btn_espandi.gif" />
																			</td>
																		</tr>
																	</table>
															</tr>
															<tr>
																<td colspan="2" valign="top">
																	<asp:label id="lblFields" runat="server" cssclass="lblBox" text="Elenco Campi" visible="False"
																		width="100%" meta:resourcekey="lblFields"></asp:label>
																</td>
															</tr>
															<tr>
																<td colspan="2" valign="top">
																	&nbsp;
																	<asp:datalist id="DataListFields" runat="server" datakeyfield="FieldID" datasourceid="SqlDataSource7"
																		repeatcolumns="1" width="530px" borderwidth="0px" cellpadding="0" showfooter="False"
																		showheader="False">
																		<itemtemplate>
																			<asp:panel id="Panel2" runat="server" cssclass="collapsePanelHeader" width="520px">
																				<table width="100%">
																					<tr>
																						<td>
																							<asp:image id="imgSevLevel" runat="server" imageurl='<%# chkStatus((int)Eval("SevLevel")) %>'
																								meta:resourcekey="imgSevLevel" />
																							<%# formatName(Eval("Name"), Eval("DevID"), Eval("StrID"), Eval("FieldID"), Eval("ArrayID"))%>
																						</td>
																						<td class="collapsePanelHeaderValue">
																							<%# formatValue(Eval("Value"),30) %>
																						</td>
																					</tr>
																				</table>
																			</asp:panel>
																			<asp:panel id="Panel1" runat="server" cssclass="collapsePanel" height="0px" width="520px" Wrap="true">
																				<div style="font-size: 10pt"><%# testoAcapo(Eval("Description")) %></div>
																			</asp:panel>
																			<ajaxToolkit:CollapsiblePanelExtender ID="cpe" runat="Server"
																				TargetControlID="Panel1"
																				ExpandControlID="Panel2"
																				CollapseControlID="Panel2" 
																				Collapsed="True"
																				TextLabelID=""
																				CollapsedSize="0"
																				ExpandedSize="-1"
																				AutoCollapse="false"
																				ExpandedText=""
																				CollapsedText=""
																				ExpandedImage=""
																				CollapsedImage=""
																				SuppressPostBack="true"
																				SkinID="" />
																		</itemtemplate>
																	</asp:datalist>
																	<asp:sqldatasource id="SqlDataSource7" runat="server" connectionstring="<%$ ConnectionStrings:Local %>"
																		selectcommand="sWEB_GetFieldsbyDevID_StrID" selectcommandtype="StoredProcedure">
																		<selectparameters>
																			<asp:sessionparameter name="DevID" sessionfield="DevID" type="Int64" />
																			<asp:sessionparameter name="StrID" sessionfield="StrID" type="Int32" />
																		</selectparameters>
																	</asp:sqldatasource>
																</td>
															</tr>
														</table>
													</asp:view>
													<asp:view id="View3" runat="server" onactivate="View3_Activate" ondeactivate="View3_Deactivate">
														<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
															<tr>
																<td style="text-align: center;">
																	<asp:label id="lblHideSearch" runat="server" visible="False" cssclass="erroreRicerca"></asp:label><br />
																	<asp:gridview id="GridViewSearch" runat="server" autogeneratecolumns="False" datakeynames="DevID"
																		datasourceid="SqlDataSource5" borderstyle="None" borderwidth="0px" cellpadding="4"
																		width="100%" onrowcommand="GridViewSearch_RowCommand" onrowdatabound="GridViewSearch_RowDataBound"
																		showheader="False" enablemodelvalidation="True" meta:resourcekey="GridViewSearch">
																		<columns>
																			<asp:templatefield sortexpression="Status">
																				<itemtemplate>
																					<asp:image id="Image1" runat="server" imageurl='<%# chkStatus((int)Eval("SevLevel")) %>' />
																				</itemtemplate>
																				<itemstyle horizontalalign="Left" verticalalign="Middle" />
																			</asp:templatefield>
																			<asp:boundfield datafield="DevID" headertext="DevID" visible="False" />
																			<asp:boundfield datafield="Name" headertext="Nome" sortexpression="Name" visible="False">
																				<itemstyle horizontalalign="Left" />
																				<headerstyle horizontalalign="Left" />
																			</asp:boundfield>
																			<asp:buttonfield datatextfield="Name" headertext="<%$ CapResources:GrismResources, Name %>" text="<%$ CapResources:GrismResources, Name %>">
																				<headerstyle horizontalalign="Left" />
																				<itemstyle horizontalalign="Left" />
																				<controlstyle cssclass="GridViewButton" />
																			</asp:buttonfield>
																			<asp:boundfield datafield="Description" headertext="<%$ CapResources:GrismResources, Description %>" sortexpression="Description">
																				<itemstyle horizontalalign="Left" cssclass="GridViewRow" />
																				<headerstyle horizontalalign="Left" />
																			</asp:boundfield>
																			<asp:boundfield datafield="SevLevel" headertext="<%$ CapResources:GrismResources, SevLevel %>" sortexpression="SevLevel" visible="False" />
																		</columns>
																		<emptydatatemplate>
																			<div class="RicercaNoData"><asp:localize id="locNoDeviceFound" runat="server" text="Nessua Periferica Trovata" meta:resourcekey="locNoDeviceFound"></asp:localize></div>
																		</emptydatatemplate>
																		<selectedrowstyle backcolor="#FFCC66" font-bold="True" forecolor="#663399" />
																		<headerstyle backcolor="Transparent" forecolor="Black" borderstyle="None" />
																	</asp:gridview>
																	<asp:sqldatasource id="SqlDataSource5" runat="server" connectionstring="<%$ ConnectionStrings:Local %>"
																		selectcommand="sWEB_FindDev" selectcommandtype="StoredProcedure">
																		<selectparameters>
																			<asp:controlparameter controlid="lblHideSearch" name="Chiave" propertyname="Text"
																				type="String" />
																		</selectparameters>
																	</asp:sqldatasource>
																</td>
															</tr>
														</table>
													</asp:view>
												</asp:multiview>
											</contenttemplate>
											<triggers>
												<asp:postbacktrigger controlid="GridViewDevOff" />
												<asp:postbacktrigger controlid="GridViewDev" />
												<asp:postbacktrigger controlid="GridViewSearch" />
												<asp:postbacktrigger controlid="tvwDevices" />
												<asp:asyncpostbacktrigger controlid="imgbRefreshPage" eventname="Click" />
											</triggers>
										</asp:updatepanel>
									</td>
								</tr>
								<tr>
									<td>
										<img alt="" src="Images/boxDX_bot.gif" style="vertical-align: top;" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:content>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.Globalization;

public partial class ReportsMasterPage : ReportsMaster
{
	public override bool FiltersButtonEnabled
	{
		get { return this.btnReportOptions.Enabled; }
		set { this.btnReportOptions.Enabled = value; }
	}

	public override bool ReportListPanelVisible
	{
		get
		{
			return Convert.ToBoolean(this.ViewState["ReportListPanelVisible"] ?? false);
		}
		set
		{
			this.ViewState["ReportListPanelVisible"] = value;
			this.SetSectionVisibility(!value);
		}
	}

	public override bool FiltersPanelVisible
	{
		get
		{
			return Convert.ToBoolean(this.ViewState["FiltersPanelVisible"] ?? false);
		}
		set
		{
			this.ViewState["FiltersPanelVisible"] = value;
			this.SetSectionVisibility(value);
		}
	}

	private void SetSectionVisibility ( bool report )
	{
		this.btnSelectReport.Enabled = report;
		this.btnReportOptions.Enabled = !report;

		if ( report )
		{
			this.tdSelectReport.Attributes["class"] = "ToolbarButtonToggleOff";
			this.tdReportList.Style[HtmlTextWriterStyle.Display] = "none";
			this.tdReportOptions.Attributes["class"] = "ToolbarButtonToggleOn";
			this.tdReportFilters.Style[HtmlTextWriterStyle.Display] = "block";
		}
		else
		{
			this.tdSelectReport.Attributes["class"] = "ToolbarButtonToggleOn";
			this.tdReportList.Style[HtmlTextWriterStyle.Display] = "block";
			this.tdReportOptions.Attributes["class"] = "ToolbarButtonToggleOff";
			this.tdReportFilters.Style[HtmlTextWriterStyle.Display] = "none";
		}

		this.tdReport.Style[HtmlTextWriterStyle.Display] = "none";
	}

	public override bool ReportPanelVisible
	{
		get
		{
			return Convert.ToBoolean(this.ViewState["ReportPanelVisible"] ?? false);
		}
		set
		{
			this.ViewState["ReportPanelVisible"] = value;
			if ( value )
			{
				this.tdReport.Style[HtmlTextWriterStyle.Display] = "block";
				this.tdReportFilters.Style[HtmlTextWriterStyle.Display] = "none";
			}
			else
			{
				this.tdReport.Style[HtmlTextWriterStyle.Display] = "none";
			}
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		this.lblReportTitle.Text = this.GetSelectedReportTitle().ToUpper();
		this.SetupReportViewer();
    }

	protected void ReportLink_Command ( Object sender, CommandEventArgs e )
	{
		this.SelectedReport = e.CommandName;

		// selezione della pagina di report
		this.Response.Redirect(string.Format("~/Reports/{0}.aspx?selRep={0}", e.CommandName));
	}

	protected void btnSelectReport_Click ( object sender, EventArgs e )
	{
		this.ReportListPanelVisible = true;
	}

	protected void btnReportOptions_Click ( object sender, EventArgs e )
	{
		this.FiltersPanelVisible = true;
	}

	protected void btnGenerate_Click ( object sender, EventArgs e )
	{
		this.ReportPanelVisible = true;
		this.PageReportViewer.LocalReport.Refresh();

		//this.imgbAdvancedOptions.Visible = true;
	}

	protected void imgbAdvancedOptions_Click ( object sender, EventArgs e )
	{
		this.PageReportViewer.ShowToolBar = !this.PageReportViewer.ShowToolBar;

		if ( this.PageReportViewer.ShowToolBar )
		{
			this.imgbAdvancedOptions.ImageUrl = "~/Images/opzioni_nascondi.gif";
		}
		else
		{
			this.imgbAdvancedOptions.ImageUrl = "~/Images/opzioni_mostra.gif";
		}
	}

	public override string GetSelectedReportTitle ()
	{
		if ( this.SelectedReport.Length > 0 )
		{
			foreach ( AjaxControlToolkit.AccordionPane pane in this.accReportList.Panes )
			{
				if ( pane.ContentContainer != null )
				{
					foreach ( Control ctrl in pane.ContentContainer.Controls )
					{
						if ( ( ( ctrl as LinkButton ) != null ) && ( ( (LinkButton)ctrl ).CommandName == this.SelectedReport ) )
						{
							return ( (LinkButton)ctrl ).Text;
						}
					}
				}
			}
		}

		return "";
	}

	public void SetupReportViewer ()
	{ 
		if ( this.PageReportViewer != null )
		{
			this.PageReportViewer.ShowToolBar = true;
			this.PageReportViewer.ShowFindControls = false;
			this.PageReportViewer.ShowPageNavigationControls = true;
			this.PageReportViewer.ShowPrintButton = true;
			this.PageReportViewer.ShowRefreshButton = false;
			this.PageReportViewer.BackColor = System.Drawing.Color.FromArgb(76, 76, 76); //#4c4c4c
			this.PageReportViewer.CssClass = "ReportViewerStyle";

			this.PageReportViewer.LocalReport.AddTrustedCodeModuleInCurrentAppDomain("GrisSuite.Common, Version=2.0.3.0, Culture=neutral, PublicKeyToken=32a178cd185c24c1");

			( (ReportPublisher) this.Page ).SetupReportParameters();
			// showfindcontrols="false" showpagenavigationcontrols="false" showprintbutton="true" showrefreshbutton="false" backcolor="#4c4c4c" cssclass="ReportViewerStyle"
		}
	}
}

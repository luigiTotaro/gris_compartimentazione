<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" meta:resourcekey="ErrorPage" %>
<asp:content ID="cntError" ContentPlaceHolderID="cphMainArea" Runat="Server">
	<meta http-equiv="refresh" content="4;url=default.aspx">
	<table cellpadding="0" cellspacing="0" style="width: 800px;">
		<tr style="height: 20px"><td class="nav_shadow_bg"></td></tr>
		<tr>
			<td>
				<table style="width: 100%;">
					<tr><td style="height:100px;">&nbsp;</td></tr>
					<tr><td><asp:label ID="lblError" runat="server" EnableViewState="False" 
							ForeColor="White" Font-Bold="True" Font-Size="Large" 
							meta:resourcekey="lblError"></asp:label></td></tr>
					<tr><td style="height:100px;">&nbsp;</td></tr>
				</table>
			</td>
		</tr>
	</table>
</asp:content>


using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports : ReportPublisher
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

	protected void Page_Prerender ( object sender, EventArgs e )
	{
		this.ReportsMaster.ReportListPanelVisible = true;
		this.ReportsMaster.FiltersButtonEnabled = false;		
	}

	public override Microsoft.Reporting.WebForms.ReportViewer ReportViewer
	{
		get { return null; }
	}
}

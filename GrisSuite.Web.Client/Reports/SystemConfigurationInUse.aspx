<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="SystemConfigurationInUse.aspx.cs" Inherits="Reports_SystemConfigurationInUse" meta:resourcekey="SystemConfigurationInUsePage" %>
<asp:content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" 
	Runat="Server" visible="false">
</asp:content>
<asp:content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" 
	Runat="Server">
	<rsweb:reportviewer id="rptvSelectedReport" runat="server" height="500px" width="98%" font-names="Verdana" font-size="8pt">
		<localreport reportpath="Reports\SystemConfigurationInUse.rdlc">
			<datasources>
				<rsweb:reportdatasource name="ServersDS" datasourceid="odsServers" />
				<rsweb:reportdatasource name="ReportDS" datasourceid="odsSystemConfigurationInUse" />
				<rsweb:reportdatasource name="ReportDSPorts" datasourceid="odsSystemConfigurationInUsePorts" />
			</datasources>
		</localreport>
	</rsweb:reportviewer>
	<asp:objectdatasource id="odsServers" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="ServerDSTableAdapters.serversTableAdapter" onselected="odsServers_Selected">
	</asp:objectdatasource>
	<asp:objectdatasource id="odsSystemConfigurationInUse" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="SystemConfigurationInUseTableAdapters.sWEB_Report_GetConfigurationInUseTableAdapter" onselecting="odsSystemConfigurationInUse_Selecting">
		<selectparameters>
			<asp:parameter name="SrvID" type="Int32" />
		</selectparameters>
	</asp:objectdatasource>
	<asp:objectdatasource id="odsSystemConfigurationInUsePorts" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="SystemConfigurationInUsePortsTableAdapters.sWEB_Report_GetConfigurationInUsePortsTableAdapter" onselecting="odsSystemConfigurationInUse_Selecting">
		<selectparameters>
			<asp:parameter name="SrvID" type="Int32" />
		</selectparameters>
	</asp:objectdatasource>
</asp:content>


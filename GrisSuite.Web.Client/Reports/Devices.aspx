<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="Devices.aspx.cs" Inherits="Reports_Devices" meta:resourcekey="DevicesReportPage" %>
<asp:content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" 
	Runat="Server">
</asp:content>
<asp:content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" 
	Runat="Server">
	<rsweb:reportviewer id="rptvSelectedReport" runat="server" height="500px" width="98%" font-names="Verdana" font-size="8pt">
		<localreport reportpath="Reports\Devices.rdlc">
			<datasources>
				<rsweb:reportdatasource name="ReportDS" datasourceid="odsDevices" />
			</datasources>
		</localreport>
	</rsweb:reportviewer>
	<asp:objectdatasource id="odsDevices" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="DevicesTableAdapters.sWEB_Report_GetDevicesTableAdapter">
	</asp:objectdatasource>
</asp:content>


<%@ Page Language="C#" MasterPageFile="~/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="SystemConfigurationValidated.aspx.cs" Inherits="Reports_SystemConfigurationValidated" meta:resourcekey="SystemConfigurationValidatedPage" %>
<asp:content ID="cntReportFilters" ContentPlaceHolderID="cphReportFilters" 
	Runat="Server">
</asp:content>
<asp:content ID="cntReportBody" ContentPlaceHolderID="cphReportBody" 
	Runat="Server">
	<rsweb:reportviewer id="rptvSelectedReport" runat="server" height="500px" width="98%" font-names="Verdana" font-size="8pt">
		<localreport reportpath="Reports\SystemConfigurationValidated.rdlc">
			<datasources>
				<rsweb:reportdatasource name="ServersDS" datasourceid="odsServers" />
				<rsweb:reportdatasource name="ReportDS" datasourceid="odsSystemConfigurationValidated" />
				<rsweb:reportdatasource name="ReportDSPorts" datasourceid="odsSystemConfigurationValidatedPorts" />
			</datasources>
		</localreport>
	</rsweb:reportviewer>
	<asp:objectdatasource id="odsServers" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="ServerDSTableAdapters.serversTableAdapter" onselected="odsServers_Selected">
	</asp:objectdatasource>
	<asp:objectdatasource id="odsSystemConfigurationValidated" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="SystemConfigurationValidatedTableAdapters.sWEB_Report_GetConfigurationValidatedTableAdapter" onselecting="odsSystemConfigurationValidated_Selecting">
		<selectparameters>
			<asp:parameter name="SrvID" type="Int32" />
		</selectparameters>
	</asp:objectdatasource>
	<asp:objectdatasource id="odsSystemConfigurationValidatedPorts" runat="server" oldvaluesparameterformatstring="original_{0}"
		selectmethod="GetData" typename="SystemConfigurationValidatedPortsTableAdapters.sWEB_Report_GetConfigurationValidatedPortsTableAdapter" onselecting="odsSystemConfigurationValidated_Selecting">
		<selectparameters>
			<asp:parameter name="SrvID" type="Int32" />
		</selectparameters>
	</asp:objectdatasource>
</asp:content>


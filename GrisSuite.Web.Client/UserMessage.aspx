﻿<%@ page language="C#" autoeventwireup="true" codefile="UserMessage.aspx.cs" inherits="UserMessage" meta:resourcekey="UserMessagePage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Autorizzazione Utilizzo sWEB</title>
	<link href="Css/main.css" rel="stylesheet" type="text/css" />
</head>
<body style="text-align: center; background-color: #4c4c4c;">
	<form id="form1" runat="server">
	<table border="0" cellpadding="0" cellspacing="0" style="width: 530px; text-align: center;">
		<tr>
			<td>
				<img alt="" src="Images/boxDX_top.gif" style="vertical-align: bottom;" />
			</td>
		</tr>
		<tr>
			<td class="FasciaTop">
				<asp:localize id="locAccess" runat="server" text="Accesso non consentito" meta:resourcekey="locAccess" />
			</td>
		</tr>
		<tr>
			<td class="FasciaBottom">
				<asp:localize id="locContactAdmin" runat="server" text="Contattare l'Amministratore di sistema per utilizzare l'applicativo." meta:resourcekey="locContactAdmin" />
			</td>
		</tr>
		<tr>
			<td>
				<img alt="" src="Images/boxDX_bot.gif" style="vertical-align: top;" />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>

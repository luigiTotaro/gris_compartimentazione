﻿using System;
using System.Collections.Generic;
using System.Text;

public class GrisReportViewerCustomMessages : Microsoft.Reporting.WebForms.IReportViewerMessages
{
	#region IReportViewerMessages Members

	public string BackButtonToolTip
	{
		get { return Resources.ReportViewerResources.BackButtonToolTip; /*Back*/ }
	}

	public string ChangeCredentialsText
	{
		get { return null; /*Change Credentials*/ }
	}

	public string ChangeCredentialsToolTip
	{
		get { return null; /*Change Credentials*/ }
	}

	public string CurrentPageTextBoxToolTip
	{
		get { return Resources.ReportViewerResources.CurrentPageTextBoxToolTip; /*Current Page*/ }
	}

	public string DocumentMap
	{
		get { return Resources.ReportViewerResources.DocumentMap; /*Document Map*/ }
	}

	public string DocumentMapButtonToolTip
	{
		get { return Resources.ReportViewerResources.DocumentMapButtonToolTip; /*Document Map*/ }
	}

	public string ExportButtonText
	{
		get { return Resources.ReportViewerResources.ExportButtonText; /*Export*/ }
	}

	public string ExportButtonToolTip
	{
		get { return Resources.ReportViewerResources.ExportButtonToolTip; /*Export*/ }
	}

	public string ExportFormatsToolTip
	{
		get { return Resources.ReportViewerResources.ExportFormatsToolTip; /*Export*/ }
	}

	public string FalseValueText
	{
		get { return null; /*False*/ }
	}

	public string FindButtonText
	{
		get { return Resources.ReportViewerResources.FindButtonText; /*Find*/ }
	}

	public string FindButtonToolTip
	{
		get { return Resources.ReportViewerResources.FindButtonToolTip; /*Find*/ }
	}

	public string FindNextButtonText
	{
		get { return Resources.ReportViewerResources.FindNextButtonText; /*Find Next*/ }
	}

	public string FindNextButtonToolTip
	{
		get { return Resources.ReportViewerResources.FindNextButtonToolTip; /*Find Next*/ }
	}

	public string FirstPageButtonToolTip
	{
		get { return Resources.ReportViewerResources.FirstPageButtonToolTip; /*First Page*/ }
	}

	public string InvalidPageNumber
	{
		get { return Resources.ReportViewerResources.InvalidPageNumber; /*Invalid Page Number*/ }
	}

	public string LastPageButtonToolTip
	{
		get { return Resources.ReportViewerResources.LastPageButtonToolTip; /*Last Page*/ }
	}

	public string NextPageButtonToolTip
	{
		get { return Resources.ReportViewerResources.NextPageButtonToolTip; /*Next Page*/ }
	}

	public string NoMoreMatches
	{
		get { return Resources.ReportViewerResources.NoMoreMatches; /*No More Matches*/}
	}

	public string NullCheckBoxText
	{
		get { return null; /*Null*/}
	}

	public string NullValueText
	{
		get { return null; /*Null*/}
	}

	public string PageOf
	{
		get { return Resources.ReportViewerResources.PageOf; /*Page Of*/}
	}

	public string ParameterAreaButtonToolTip
	{
		get { return null; /*Parameter Area*/}
	}

	public string PasswordPrompt
	{
		get { return null; /*Password*/}
	}

	public string PreviousPageButtonToolTip
	{
		get { return Resources.ReportViewerResources.PreviousPageButtonToolTip; /*Previous Page*/}
	}

	public string PrintButtonToolTip
	{
		get { return Resources.ReportViewerResources.PrintButtonToolTip; /*Print*/}
	}

	public string ProgressText
	{
		get { return Resources.ReportViewerResources.ProgressText; /*Progress*/}
	}

	public string RefreshButtonToolTip
	{
		get { return Resources.ReportViewerResources.RefreshButtonToolTip; /*Refresh*/}
	}

	public string SearchTextBoxToolTip
	{
		get { return Resources.ReportViewerResources.SearchTextBoxToolTip; /*Search*/}
	}

	public string SelectAValue
	{
		get { return Resources.ReportViewerResources.SelectAValue; /*Select a Value*/}
	}

	public string SelectAll
	{
		get { return Resources.ReportViewerResources.SelectAll; /*Select All*/}
	}

	public string SelectFormat
	{
		get { return Resources.ReportViewerResources.SelectFormat; /*Select Format*/}
	}

	public string TextNotFound
	{
		get { return Resources.ReportViewerResources.TextNotFound; /*Text Not Found*/}
	}

	public string TodayIs
	{
		get { return Resources.ReportViewerResources.TodayIs; /*Today is*/}
	}

	public string TrueValueText
	{
		get { return null; /*True*/}
	}

	public string UserNamePrompt
	{
		get { return null; /*User Name*/}
	}

	public string ViewReportButtonText
	{
		get { return Resources.ReportViewerResources.ViewReportButtonText; /*View Report*/}
	}

	public string ZoomControlToolTip
	{
		get { return Resources.ReportViewerResources.ZoomControlToolTip; /*Zoom*/}
	}

	public string ZoomToPageWidth
	{
		get { return Resources.ReportViewerResources.ZoomToPageWidth; /*Zoom to Page Width*/}
	}

	public string ZoomToWholePage
	{
		get { return Resources.ReportViewerResources.ZoomToWholePage; /*Zoom to Whole Page*/}
	}

	#endregion
}

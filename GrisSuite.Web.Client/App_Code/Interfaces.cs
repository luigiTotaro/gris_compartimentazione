using System;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;
using System.Globalization;

public abstract class ReportPublisher : Page
{
	protected ReportParameter CurrentCultureParameter = new ReportParameter("currentCulture", CultureInfo.CurrentCulture.Name);

	public ReportsMaster ReportsMaster
	{
		get 
		{
			if ( this.Master != null )
			{
				return (ReportsMaster)this.Master;
			}

			return null;
		}
	}

	public abstract Microsoft.Reporting.WebForms.ReportViewer ReportViewer
	{
		get;
	}

	protected override void OnLoad ( EventArgs e )
	{
		base.OnLoad(e);

		if ( this.ReportViewer != null )
		{
			this.ReportViewer.Style[HtmlTextWriterStyle.TextAlign] = "right";
			this.ReportViewer.Style[HtmlTextWriterStyle.PaddingBottom] = "30px";
		}

		if ( this.ReportsMaster != null )
		{
			// visibilitÓ dei pannelli
			if ( !this.IsPostBack )
			{
				this.ReportsMaster.FiltersPanelVisible = true;
				this.ReportsMaster.ReportPanelVisible = false;

				if ( HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.QueryString["selRep"] != null )
				{
					this.ReportsMaster.SelectedReport = HttpContext.Current.Request.QueryString["selRep"];
				}
			}
		}
	}

	public virtual void SetupReportParameters ()
	{
		if ( this.ReportViewer != null )
		{
			// passo la cultura assegnata al thread corrente, i reports risultano sempre en-US
			IList<ReportParameter> paramsList = new List<ReportParameter>();
			paramsList.Add(this.CurrentCultureParameter);
			this.ReportViewer.LocalReport.SetParameters(paramsList);
		}
	}
}

public abstract class ReportsMaster : MasterPage
{
	public Microsoft.Reporting.WebForms.ReportViewer PageReportViewer;

	public string SelectedReport
	{
		get { return ( this.ViewState["SelReport"] ?? "" ).ToString(); }
		set { this.ViewState["SelReport"] = value; }
	}

	public abstract bool FiltersButtonEnabled
	{
		get;
		set;
	}

	public abstract bool ReportListPanelVisible
	{
		get;
		set;
	}

	public abstract bool FiltersPanelVisible
	{
		get;
		set;
	}

	public abstract bool ReportPanelVisible
	{
		get;
		set;
	}

	protected override void OnLoad ( EventArgs e )
	{
		if ( this.Page is ReportPublisher )
		{
			this.PageReportViewer = ( (ReportPublisher)this.Page ).ReportViewer;
		}

		base.OnLoad(e);
	}

	public abstract string GetSelectedReportTitle ();
}

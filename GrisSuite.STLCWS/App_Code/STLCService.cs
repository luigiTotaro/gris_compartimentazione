using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ServiceProcess;
using System.Diagnostics;
using System.Security.Principal;

using GrisSuite;
using GrisSuite.Common;

[WebService(Namespace = "http://Telefin.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class STLCService : System.Web.Services.WebService
{
	private ServiceController _scagentService = null;
	private static string scagentServiceName = "SCAgentService"; // va sostituito con l'eventsource unificato per tutti i software Telefin

	public STLCService ()
	{
		this._scagentService = new ServiceController(scagentServiceName);
    }

	[WebMethod]
	public bool SendConfigToCentralComplete ()
	{
		try 
		{
			this._scagentService.ExecuteCommand((int)ScagentActionEnum.SendConfigToCentralComplete);
			return true;
		}
		catch ( InvalidOperationException exc )
		{
			EventLog.WriteEntry(scagentServiceName, "Error on 'SendConfigToCentralComplete' of the web service 'STLCService': " + exc.Message + " | Stack Trace: " + ( exc.StackTrace ?? "" ) + " | Windows User: " + WindowsIdentity.GetCurrent().Name, EventLogEntryType.Warning);
		}
		finally
		{
			this._scagentService.Dispose();
		}

		return false;
	}

	[WebMethod]
	public bool SendConfigToCentralOnlyChanges ()
	{
		try
		{
			this._scagentService.ExecuteCommand((int)ScagentActionEnum.SendConfigToCentralOnlyChanges);
			return true;
		}
		catch ( InvalidOperationException exc )
		{
			EventLog.WriteEntry(scagentServiceName, "Error on 'SendConfigToCentralOnlyChanges' of the web service 'STLCService': " + exc.Message + " | Stack Trace: " + ( exc.StackTrace ?? "" ) + " | Windows User: " + WindowsIdentity.GetCurrent().Name, EventLogEntryType.Warning);
		}
		finally
		{
			this._scagentService.Dispose();
		}

		return false;
	}

	[WebMethod]
	public bool SendDeviceStatusToCentralComplete ()
	{
		try
		{
			this._scagentService.ExecuteCommand((int)ScagentActionEnum.SendDeviceStatusToCentralComplete);
			return true;
		}
		catch ( InvalidOperationException exc )
		{
			EventLog.WriteEntry(scagentServiceName, "Error on 'SendDeviceStatusToCentralComplete' of the web service 'STLCService': " + exc.Message + " | Stack Trace: " + ( exc.StackTrace ?? "" ) + " | Windows User: " + WindowsIdentity.GetCurrent().Name, EventLogEntryType.Warning);
		}
		finally
		{
			this._scagentService.Dispose();
		}

		return false;
	}

	[WebMethod]
	public bool SendDeviceStatusToCentralOnlyChanges ()
	{
		try
		{
			this._scagentService.ExecuteCommand((int)ScagentActionEnum.SendDeviceStatusToCentralOnlyChanges);
			return true;
		}
		catch ( InvalidOperationException exc )
		{
			EventLog.WriteEntry(scagentServiceName, "Error on 'SendDeviceStatusToCentralOnlyChanges' of the web service 'STLCService': " + exc.Message + " | Stack Trace: " + ( exc.StackTrace ?? "" ) + " | Windows User: " + WindowsIdentity.GetCurrent().Name, EventLogEntryType.Warning);
		}
		finally
		{
			this._scagentService.Dispose();
		}

		return false;
	}

	[WebMethod]
	public bool SendDeviceStatusAndConfigToCentral ()
	{
		try
		{
			//EventLog.WriteEntry(scagentServiceName, "User: " + this.User.Identity.Name);
			this._scagentService.ExecuteCommand((int)ScagentActionEnum.SendDeviceStatusAndConfigToCentral);
			return true;
		}
		catch ( InvalidOperationException exc )
		{
			EventLog.WriteEntry(scagentServiceName, "Error on 'SendDeviceStatusAndConfigToCentral' of the web service 'STLCService': " + exc.Message + " | Stack Trace: " + ( exc.StackTrace ?? "" ) + " | Windows User: " + WindowsIdentity.GetCurrent().Name, EventLogEntryType.Warning);
		}
		finally
		{
			this._scagentService.Dispose();
		}

		return false;
	}

	[WebMethod]
	public short SetConfigurationAsValidated ( int srvID, string signedData, byte[] sign, byte[] publicKey )
	{
		try
		{
			DateTime dateValidationObtained = DateTime.Now;
            GrisSuite.Data.dsSTLCServersTableAdapters.serversTableAdapter ta = new GrisSuite.Data.dsSTLCServersTableAdapters.serversTableAdapter();
			ta.UpdateServerAsValidated(srvID, signedData, sign, dateValidationObtained, publicKey);
			return (short)SystemValidationEnum.RequestCorrectlyReceived;
		}
		catch ( System.Data.SqlClient.SqlException exc )
		{
			EventLog.WriteEntry(scagentServiceName, "Error on 'SetConfigurationAsValidated' of the web service 'STLCService': " + exc.Message + " | Stack Trace: " + ( exc.StackTrace ?? "" ) + " | Windows User: " + WindowsIdentity.GetCurrent().Name, EventLogEntryType.Warning);
			return (short)SystemValidationEnum.SqlError;
		}
	}
}

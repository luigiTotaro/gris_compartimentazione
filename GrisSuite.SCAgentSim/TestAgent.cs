using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Text;
using GrisSuite.Data;
using Telefin.TelefinCentralWS;
using System.IO;
using System.Net;
using GrisSuite;
using Telefin;


namespace GrisSuite
{
    public class TestAgent
    {
        private string _fileDeviceStatusCache = string.Empty;
        private string _fileConfigCache = string.Empty;       
        private SCACollector _collector = new SCACollector();
        private dsConfig.ServersRow _server = null;
        private TestAgentParameters _testAgentParameters = null;
        private TestServerLog _testServerLog = null;
        private bool _isConfigValid = false;
        private bool _isDeviceStatusValid = false;
        private readonly string _prefixStressTest = "STRESSTEST:";


        DefinitionDS _dsDefinition = new DefinitionDS();

        public TestAgent(TestAgentParameters testAgentParameters)
        {
            _testAgentParameters = testAgentParameters;
            _testServerLog = new TestServerLog(_testAgentParameters.ServerId);

            string absCacheFolder = AppDomain.CurrentDomain.BaseDirectory + global::Telefin.Properties.Settings.Default.CacheFolder;

            if (!System.IO.Directory.Exists(absCacheFolder))
                System.IO.Directory.CreateDirectory(absCacheFolder);

            _fileDeviceStatusCache = String.Format("{0}\\DeviceStatusCache{1}.xml", absCacheFolder, testAgentParameters.ServerId);
            _fileConfigCache = String.Format("{0}\\ConfigCache{1}.xml", absCacheFolder, testAgentParameters.ServerId);

            dsConfig dsc = new dsConfig();
            _server = dsc.Servers.NewServersRow();
            _server.SrvID = testAgentParameters.ServerId;
            _server.Name = String.Format("{0} Server{1:00000}", _prefixStressTest, testAgentParameters.ServerId);
            _server.Host = _server.Name;

            _collector.Credentials = this.GetCredentials();
            _collector.Timeout = global::Telefin.Properties.Settings.Default.WSCallTimeout * 1000;

            DefinitionDS.Fill(_dsDefinition);
        }

        #region Public Methods

        public void ClearCache()
        {
            try
            {
                if (File.Exists(_fileDeviceStatusCache))
                    File.Delete(_fileDeviceStatusCache);

                if (File.Exists(_fileConfigCache))
                    File.Delete(_fileConfigCache);
            }
            catch (Exception ex)
            {
                _testServerLog.WriteLog("Error on ClearCache:" + ex.Message, Guid.Empty);
            }
        }

        public Guid SendConfigToCentral()
        {
            return SendConfigToCentral(false);
        }

        public Guid SendConfigToCentral(bool onlyChange)
        {
            Guid rc = Guid.Empty;
            try
            {
                //LocalUtil.WriteLog("SendConfigToCentral:CreateDataset:Start");
                dsConfig dsChange = new dsConfig();
                dsConfig dsConfigCache = new dsConfig();
                bool bValidCache = false;

                string[] tables = global::Telefin.Properties.Settings.Default.Tables_Config.Split(',');
                
                bValidCache = this.LoadFromCache(dsConfigCache,_fileConfigCache);

                if (onlyChange && bValidCache)
                {
                    dsChange = this.CloneConfig(this.UpdateTestConfig(dsConfigCache), tables);
                }
                else
                {
                    dsConfigCache = this.CreateTestConfig();
                    dsChange = this.CloneConfig(dsConfigCache, tables);
                }

                //LocalUtil.WriteLog("SendConfigToCentral:CreateDataset:End");
                
                int nRows = GetDataSetRowsCount(dsChange);
                if (nRows > 0)
                {
                    //LocalUtil.WriteLog("Updated config rows:" + nRows.ToString());
                    byte[] cds = Util.CompressDataSet(dsChange);


                    if (onlyChange)
                    {
                        Guid guidLog = _testServerLog.WriteLog("SendConfigToCentral:UpdateConfig:Start", cds.Length,nRows, Guid.Empty);
                        rc = _collector.UpdateConfig(cds);
                        _testServerLog.WriteLog("SendConfigToCentral:UpdateConfig:End", cds.Length, nRows, guidLog);
                    }
                    else
                    {
                        Guid guidLog = _testServerLog.WriteLog("SendConfigToCentral:ReplaceConfig:Start", cds.Length, nRows, Guid.Empty);
                        rc = _collector.ReplaceConfig(cds);
                        _testServerLog.WriteLog("SendConfigToCentral:ReplaceConfig:End", cds.Length, nRows, guidLog);
                    }
                }
                else
                {
                    //LocalUtil.WriteLog("No config change");
                }
                _isConfigValid = SaveCache(dsConfigCache, _fileConfigCache);
                //LocalUtil.WriteLog("SendConfigToCentral:End");
                return rc;
            }
            catch (Exception ex)
            {
                _isConfigValid = false; // invalido da configurazione
                _testServerLog.WriteLog("Error on SendConfigToCentral:" + ex.Message, Guid.Empty);
                return rc;
            }
        }

        public Guid SendDeviceStatusToCentral()
        {
            return SendDeviceStatusToCentral(false);
        }

        public Guid SendDeviceStatusToCentral(bool onlyChange)
        {
            Guid rc = Guid.Empty;
            try
            {
                if (!this.IsConfigValid)
                    throw new Exception("Can't send Device Status becouse Config is not valid, recall SendConfigToCentral");

                dsDeviceStatus dsChange = new dsDeviceStatus();
                dsDeviceStatus dsDeviceStatusCache = new dsDeviceStatus();
                bool bValidCache = false;

                string[] tables = global::Telefin.Properties.Settings.Default.Tables_DeviceStatus.Split(',');

                bValidCache = LoadFromCache(dsDeviceStatusCache, _fileDeviceStatusCache);

                if (onlyChange && bValidCache)
                {
                    dsChange = this.CloneDeviceStatus(this.UpdateTestDeviceStatus(dsDeviceStatusCache),tables); 
                }
                else
                {
                    //LocalUtil.WriteLog("SendDeviceStatusToCentral:Cache device status empty");
                    dsDeviceStatusCache = this.CreateTestDeviceStatus();
                    dsChange = this.CloneDeviceStatus(dsDeviceStatusCache, tables) ;
                }
                //LocalUtil.WriteLog("SendDeviceStatusToCentral:CreateDataset:End");

                int nRows = GetDataSetRowsCount(dsChange);
                if (nRows > 0)
                {
                    //LocalUtil.WriteLog("Updated device status rows:" + nRows.ToString());
                    byte[] cds = Util.CompressDataSet(dsChange);

                    if (onlyChange)
                    {
                        Guid logGuid = _testServerLog.WriteLog("SendDeviceStatusToCentral:UpdateStatus:Start", cds.Length, nRows, Guid.Empty);
                        rc = _collector.UpdateStatus(cds);
                        _testServerLog.WriteLog("SendDeviceStatusToCentral:UpdateStatus:End", cds.Length, nRows, logGuid);
                    }
                    else
                    {
                        Guid logGuid = _testServerLog.WriteLog("SendDeviceStatusToCentral:ReplaceStatus:Start", cds.Length, nRows, Guid.Empty);
                        rc = _collector.ReplaceStatus(cds);
                        _testServerLog.WriteLog("SendDeviceStatusToCentral:ReplaceStatus:End", cds.Length, nRows, logGuid);
                    }
                }
                else if (global::Telefin.Properties.Settings.Default.SendIsAliveIfNoChange)
                {
                    rc = this.SendIsAlive();
                }
                else
                {
                    _testServerLog.WriteLog("SendDeviceStatusToCentral:No device status change", 0,0, Guid.Empty);
                }
                _isDeviceStatusValid = SaveCache(dsDeviceStatusCache, _fileDeviceStatusCache);
                return rc;
            }
            catch (Exception ex)
            {
                _isDeviceStatusValid = false; // invalido lo status
                _testServerLog.WriteLog("Error on SendDeviceStatusToCentral:" + ex.Message, Guid.Empty);
                return rc;
            }
        }

        public Guid SendIsAlive()
        {
            try
            {
                Guid logGuid = _testServerLog.WriteLog("SendDeviceStatusToCentral:SendIsAlive:Start", 0,0, Guid.Empty);
                Guid rc = _collector.IsAlive();
                _testServerLog.WriteLog("SendDeviceStatusToCentral:SendIsAlive:End", 0,0, logGuid);
                return rc;
            }
            catch (Exception ex)
            {
                _testServerLog.WriteLog("Error on SendIsAlive:" + ex.Message, Guid.Empty);
                return Guid.Empty;
            }
        }

        public bool IsConfigValid
        {
            get 
            {
                return _isConfigValid;
            }
                
        }

        #endregion

        #region Private Utility

        private dsConfig CreateTestConfig()
        {
            string[] guidPort = new string[] {"DB63005F-D362-4caf-A870-099766FD757B", 
                                         "ADAAB2C3-B710-4a56-8694-87E4CA8414F5",
                                         "D01C5BF6-35DF-4d93-BE9F-9435B843447B",
                                         "FA5C852F-F35B-44c3-A078-FCBD63ECCB9A",
                                         "CC0C309F-F432-4c16-A73D-E1764BF856FB",
                                         "6EDDF4B5-0B59-4b0c-B222-20A36CCF05CA",
                                         "D842E0AD-7CB1-4a09-9F0B-D057F83AAB7A",
                                         "B24D1514-C093-4575-BEFF-217B0E585AA7",
                                         "BF838F3E-3016-45e3-A7F3-8F8C3D693AFE",
                                         "055A7036-4FA2-4754-915A-4ECB010BDA33",
                                         "0A09AD33-88DE-45e7-8C37-0789AC9B2BA1"};

            string[] guidPos = new string[] {"33F9E603-CE5E-4b07-AF38-000000111111", 
                                         "B5FE1D0C-57D0-4168-830A-000000111111",
                                         "A8E87FA0-9203-46cf-AD60-000000111111",
                                         "41AE3733-FB68-4639-9F1C-000000111111",
                                         "7F789671-FB82-4569-9D44-000000111111",
                                         "F0A92D88-3B87-485d-8AA4-000000111111",
                                         "3DD87AAC-9C85-4a06-9A5C-000000111111",
                                         "FD4037CD-A7FA-4c4e-BC52-000000111111",
                                         "6FDADACE-01D4-4673-ADE1-000000111111",
                                         "46830B05-F159-4db0-97FF-000000111111"};

            
            dsConfig config = new dsConfig();
            Random randomIdx = new Random();

            RandomRegionList rrl = new RandomRegionList();

            // Create Servers
            config.Servers.AddServersRow(_server.SrvID, 
                                            _server.Name, 
                                            _server.Host,
                                            string.Empty,
                                            string.Empty,
                                            DateTime.Now,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            DateTime.Now,
                                            DateTime.Now,
                                            null,
                                            0,
                                            string.Empty,
                                            string.Empty);

            // Create Port
            dsConfig.PortRow rPort = config.Port.NewPortRow();
            rPort.PortXMLID = randomIdx.Next(9);
            rPort.PortID = new Guid(guidPort[rPort.PortXMLID]);
            rPort.PortName = "Port " + rPort.PortID.ToString();
            rPort.PortType = "PortType";
            rPort.Status = "none";
            rPort.Removed = 0;
            rPort.SrvID = _server.SrvID;
            config.Port.AddPortRow(rPort);

            // Create Station
            dsConfig.StationRow rStation = config.Station.NewStationRow();
            rStation.StationXMLID = randomIdx.Next(9);
            rStation.StationID = new Guid(guidPos[rStation.StationXMLID]);
            rStation.StationName = _prefixStressTest + rrl.NodeName;
            rStation.Removed = 0;    
            config.Station.AddStationRow(rStation);

            // Create Bulding
            dsConfig.BuildingRow rBuilding = config.Building.NewBuildingRow();
            rBuilding.BuildingXMLID = randomIdx.Next(10);
            rBuilding.StationID = rStation.StationID;
            rBuilding.BuildingID = GetChildGuid(rBuilding.StationID, rBuilding.BuildingXMLID, "000000");
            rBuilding.BuildingName = "Building Name " + rBuilding.BuildingID;
            rBuilding.BuildingDescription = "Building Description " + rBuilding.BuildingID;
            rBuilding.Removed = 0;
            config.Building.AddBuildingRow(rBuilding);
            
            // Create Rack
            dsConfig.RackRow rRack = config.Rack.NewRackRow();
            rRack.RackXMLID = randomIdx.Next(10);
            rRack.BuildingID = rBuilding.BuildingID;
            rRack.RackID = GetChildGuid(rRack.BuildingID, rRack.RackXMLID, "111111");
            rRack.RackName = "Rack " + rRack.RackXMLID.ToString();
            rRack.RackDescription = "Rack Description " + rRack.RackXMLID.ToString();
            config.Rack.AddRackRow(rRack);

            // Create Devices
            int deviceTypeCount = _dsDefinition.DeviceType.Rows.Count;
            Random random = new Random(DateTime.Now.Millisecond); 
            for (int i = 1; i <= this._testAgentParameters.NumDevices; i++)
            {
                DefinitionDS.DeviceTypeRow rDevType = (DefinitionDS.DeviceTypeRow)_dsDefinition.DeviceType.Rows[random.Next(0, deviceTypeCount - 1)];
                dsConfig.DevicesRow rDev = config.Devices.NewDevicesRow();

                rDev.DevID = ((_server.SrvID << 16) | i );
                rDev.Name = rDevType.DeviceTypeCode + i.ToString();
                rDev.SN = string.Format("{0:00000}-{1:000000000}", _server.SrvID, rDev.DevID);
                rDev.Type = rDevType.DeviceTypeCode;
                rDev.SrvID = _server.SrvID;
                rDev.NodID = rrl.NodeIdComplete;
                rDev.PortID = rPort.PortID;
                rDev.ProfileID = 1;
                rDev.Scheduled = 0;
                rDev.Removed = 0;
                rDev.Active = 1;
                rDev.RackID = rRack.RackID;
                rDev.RackPositionCol = 1;
                rDev.RackPositionRow = 1;
                rDev.Addr = "address";

                config.Devices.AddDevicesRow(rDev);
            }

            return config;
        }

        private Guid GetChildGuid(Guid parentGuid, int childId, string childToken)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            string parentGuidString = parentGuid.ToString();
            return new Guid(parentGuidString.Replace(childToken,childId.ToString().PadLeft(childToken.Length,'0')));
        }

        private dsConfig UpdateTestConfig(dsConfig dsConfigBase)
        {
            dsConfig dsChanged = new dsConfig();

            int count = dsConfigBase.Devices.Rows.Count;
            int countUpdate = (int)Math.Round(count * (this._testAgentParameters.PercUpdateDevices / (decimal)100));

            RandomSequence r = new RandomSequence(0, count - 1);

            for (int i = 1; i <= countUpdate; i++)
            {
                dsConfig.DevicesRow rDev = (dsConfig.DevicesRow)dsConfigBase.Devices.Rows[r.Next()];
                rDev.Addr = (rDev.Addr == "address" ? "addressUpdated" :"address");
                dsConfigBase.AcceptChanges();

                dsChanged.Devices.ImportRow(rDev);
            }

            return dsChanged;

        }

        private int GetRandomSevLevel()
        {
            int i = new Random().Next(100);
 
            if (i < 60)
                return 0; // ok
            else if (i >= 60 && i < 90 )
                return 1; // Warning
            else //if (i >= 90)
                return 2; // Error
        }

        private dsDeviceStatus CreateTestDeviceStatus()
        {
            dsDeviceStatus deviceStatus = new dsDeviceStatus();

            dsConfig dsConfigCache = new dsConfig();
            LoadFromCache(dsConfigCache,_fileConfigCache);

            // Create Device Status
            for (int i = 0; i < dsConfigCache.Devices.Rows.Count; i++)
            {
                dsDeviceStatus.DeviceStatusRow rDevStatus = deviceStatus.DeviceStatus.NewDeviceStatusRow();
                dsConfig.DevicesRow rDev = (dsConfig.DevicesRow)dsConfigCache.Devices.Rows[i];

                rDevStatus.DevID = rDev.DevID;
                rDevStatus.Description = string.Format("Description Device {0}", rDev.DevID);
                rDevStatus.IsDeleted = 0;
                rDevStatus.Offline = 0;
                rDevStatus.SevLevel = GetRandomSevLevel();

                deviceStatus.DeviceStatus.Rows.Add(rDevStatus);
                
            }
            // Create Streams
            for (int i = 0; i < dsConfigCache.Devices.Rows.Count; i++)
            {
                dsConfig.DevicesRow rDev = (dsConfig.DevicesRow)dsConfigCache.Devices.Rows[i];
                DefinitionDS.DeviceTypeRow rDevType = _dsDefinition.DeviceType.FindByDeviceTypeCode(rDev.Type);

                if (rDevType != null)
                {
                    DataRow[] rowsStreamType = rDevType.GetChildRows("DeviceType_StreamType");

                    for (int j = 0; j < rowsStreamType.Length; j++)
                    {
                        DefinitionDS.StreamTypeRow rStreamType = (DefinitionDS.StreamTypeRow)rowsStreamType[j];
                        dsDeviceStatus.StreamsRow rStream = deviceStatus.Streams.NewStreamsRow();

                        rStream.DateTime = DateTime.Now;
                        //rStream.Data = null;
                        //rStream.Description = ""; 
                        rStream.DevID = rDev.DevID;
                        rStream.IsDeleted = 0;
                        rStream.Name = rStreamType.Name;
                        rStream.SevLevel = GetRandomSevLevel();
                        rStream.StrID = rStreamType.StreamTypeId;
                        rStream.Visible = 1;

                        deviceStatus.Streams.AddStreamsRow(rStream);
                    }

                }
                else 
                {
                    throw new Exception("Device Type not Found");
                }
            }


            // Create Streams Fields
            for (int i = 0; i < deviceStatus.Streams.Rows.Count; i++)
            {
                dsDeviceStatus.StreamsRow rStream = (dsDeviceStatus.StreamsRow)deviceStatus.Streams.Rows[i];
                DefinitionDS.StreamTypeRow rStreamType = _dsDefinition.StreamType.FindByStreamTypeId(rStream.StrID);
                if (rStreamType != null)
                {
                    DataRow[] rowsFieldType = rStreamType.GetChildRows("StreamType_FieldType");

                    for (int j = 0; j < rowsFieldType.Length; j++)
                    {
                        DefinitionDS.FieldTypeRow rFieldType = (DefinitionDS.FieldTypeRow)rowsFieldType[j];

                        int capacity = rFieldType.Capacity;

                        for (int z = 0; z < capacity; z++)
                        {
                            dsDeviceStatus.StreamFieldsRow rField = deviceStatus.StreamFields.NewStreamFieldsRow();
                            rField.DevID = rStream.DevID;
                            rField.FieldID = rFieldType.FieldTypeId;
                            rField.ArrayID = z;
                            rField.IsDeleted = 0;
                            rField.Name = rFieldType.Name;
                            rField.SevLevel = GetRandomSevLevel();
                            rField.StrID = rStream.StrID;
                            rField.Value = GetRandomFieldValue(rFieldType);
                            rField.Description = GetRandomFieldMessage(rFieldType);
                            rField.Visible = 1;

                            // Create reference
                            if (new Random().Next(5) == 1)
                            {
                                dsDeviceStatus.ReferenceRow rReference = deviceStatus.Reference.NewReferenceRow();
                                rReference.ReferenceID = Guid.NewGuid();
                                rReference.Value = rField.Value;
                                rReference.DateTime = DateTime.Now;
                                rReference.SetDeltaArrayIDNull();
                                rReference.SetDeltaDevIDNull();
                                rReference.SetDeltaFieldIDNull();
                                rReference.SetDeltaStrIDNull();
                                deviceStatus.Reference.Rows.Add(rReference);

                                rField.ReferenceID = rReference.ReferenceID;
                            }
                            else
                            {
                                rField.SetReferenceIDNull();
                            }

                            deviceStatus.StreamFields.Rows.Add(rField);
                        }

                    }
                }
                else
                {
                    throw new Exception("Stream Type not Found");
                }
            }

            //// Update Streams Data & Description with fields
            //foreach (dsDeviceStatus.StreamsRow rStream in deviceStatus.Streams)
            //{
            //    rStream.Data = this.GetRandomStreamData(rStream);
            //    rStream.Description = this.GetRandomStreamMessage(rStream);
            //}

            return deviceStatus;
        }
        
        private dsDeviceStatus UpdateTestDeviceStatus(dsDeviceStatus dsDeviceStatusBase)
        {
            dsDeviceStatus dsChanged = new dsDeviceStatus();

            // update device status
            UpdateDeviceStatus(dsDeviceStatusBase, dsChanged);
            UpdateStreamFields(dsDeviceStatusBase, dsChanged);
            UpdateStreams(dsDeviceStatusBase, dsChanged);

            return dsChanged;
        }

        private void UpdateDeviceStatus(dsDeviceStatus dsDeviceStatusBase, dsDeviceStatus dsChanged)
        {
            // update device status
            int count = dsDeviceStatusBase.DeviceStatus.Rows.Count;
            int countUpdate = (int)Math.Round(count * (this._testAgentParameters.PercUpdateDevicesStatus / (decimal)100));

            RandomSequence r = new RandomSequence(0, count - 1);


            for (int i = 1; i <= countUpdate; i++)
            {
                dsDeviceStatus.DeviceStatusRow rDevStatus = (dsDeviceStatus.DeviceStatusRow)dsDeviceStatusBase.DeviceStatus.Rows[r.Next()];
                rDevStatus.Offline = (rDevStatus.Offline == 1 ? (byte)0 : (byte)1);
                rDevStatus.SevLevel = GetRandomSevLevel();
                dsDeviceStatusBase.AcceptChanges();

                dsChanged.DeviceStatus.ImportRow(rDevStatus);
            }
        }

        private void UpdateStreams(dsDeviceStatus dsDeviceStatusBase, dsDeviceStatus dsChanged)
        {
            // update device status
            int count = dsDeviceStatusBase.Streams.Rows.Count;
            int countUpdate = (int)Math.Round(count * (this._testAgentParameters.PercUpdateStreams / (decimal)100));

            RandomSequence r = new RandomSequence(0, count - 1);

            for (int i = 1; i <= countUpdate; i++)
            {
                dsDeviceStatus.StreamsRow rStream = (dsDeviceStatus.StreamsRow)dsDeviceStatusBase.Streams.Rows[r.Next()];
                //rStream.Description = this.GetRandomStreamMessage(rStream);
                rStream.SevLevel = GetRandomSevLevel();
                dsDeviceStatusBase.AcceptChanges();
                dsChanged.Streams.ImportRow(rStream);
            }
        }

        private void UpdateStreamFields(dsDeviceStatus dsDeviceStatusBase, dsDeviceStatus dsChanged)
        {
            // update device status            
            int count = dsDeviceStatusBase.StreamFields.Rows.Count;
            int countUpdate = (int)Math.Round(count * (this._testAgentParameters.PercUpdateStreamsFields / (decimal)100));

            RandomSequence r = new RandomSequence(0, count - 1);

            for (int i = 1; i <= countUpdate; i++)
            {
                dsDeviceStatus.StreamFieldsRow rStreamFields = (dsDeviceStatus.StreamFieldsRow)dsDeviceStatusBase.StreamFields.Rows[r.Next()];
                DefinitionDS.FieldTypeRow rStreamType = _dsDefinition.FieldType.FindByFieldTypeId(rStreamFields.FieldID);
                rStreamFields.Description = GetRandomFieldMessage(rStreamType);
                rStreamFields.Value = GetRandomFieldValue(rStreamType);
                rStreamFields.SevLevel = GetRandomSevLevel();
                dsDeviceStatusBase.AcceptChanges();

                dsChanged.StreamFields.ImportRow(rStreamFields);
            }
        }

        private dsDeviceStatus CloneDeviceStatus(dsDeviceStatus deviceStatus, string[] tables)
        {
            dsDeviceStatus dsNew = new dsDeviceStatus();

            if (Array.IndexOf<string>(tables, deviceStatus.DeviceStatus.TableName.ToLower()) >= 0)
                dsNew.DeviceStatus.Merge(deviceStatus.DeviceStatus);

            if (Array.IndexOf<string>(tables, deviceStatus.Streams.TableName.ToLower()) >= 0)
                dsNew.Streams.Merge(deviceStatus.Streams);

            if (Array.IndexOf<string>(tables, deviceStatus.StreamFields.TableName.ToLower()) >= 0)
                dsNew.StreamFields.Merge(deviceStatus.StreamFields);

            if (Array.IndexOf<string>(tables, deviceStatus.Reference.TableName.ToLower()) >= 0)
                dsNew.Reference.Merge(deviceStatus.Reference);

            dsNew.AcceptChanges();

            return dsNew;
        }

        private dsConfig CloneConfig(dsConfig Config, string[] tables)
        {
            dsConfig dsNew = new dsConfig();

            if (Array.IndexOf<string>(tables, Config.Port.TableName.ToLower()) >= 0)
                dsNew.Port.Merge(Config.Port);
            if (Array.IndexOf<string>(tables, Config.Servers.TableName.ToLower()) >= 0)
                dsNew.Servers.Merge(Config.Servers);
            if (Array.IndexOf<string>(tables, Config.Station.TableName.ToLower()) >= 0)
                dsNew.Station.Merge(Config.Station);
            if (Array.IndexOf<string>(tables, Config.Building.TableName.ToLower()) >= 0)
                dsNew.Building.Merge(Config.Building);
            if (Array.IndexOf<string>(tables, Config.Rack.TableName.ToLower()) >= 0)
                dsNew.Rack.Merge(Config.Rack);
            if (Array.IndexOf<string>(tables, Config.Regions.TableName.ToLower()) >= 0)
                dsNew.Regions.Merge(Config.Regions);
            if (Array.IndexOf<string>(tables, Config.Zones.TableName.ToLower()) >= 0)
                dsNew.Zones.Merge(Config.Zones);
            if (Array.IndexOf<string>(tables, Config.Nodes.TableName.ToLower()) >= 0)
                dsNew.Nodes.Merge(Config.Nodes);
            if (Array.IndexOf<string>(tables, Config.Devices.TableName.ToLower()) >= 0)
                dsNew.Devices.Merge(Config.Devices);

            dsNew.AcceptChanges();

            return dsNew;
        }

        private string GetRandomFieldValue(DefinitionDS.FieldTypeRow fieldType)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            StringBuilder sbOut = new StringBuilder();

            int maxlen = 5;
            int.TryParse(fieldType.Len, out maxlen);

            switch (fieldType.Type)
            {
                case "t_u_hex_8":
                    sbOut.Append(r.Next((int)byte.MinValue, (int)byte.MaxValue).ToString("{0:1x}"));
                    break;
                case "t_u_hex_16":
                    sbOut.Append(r.Next((int)short.MinValue, (int)short.MaxValue).ToString("{0:2x}"));
                    break;
                case "t_u_hex_32":
                    sbOut.Append(r.Next(int.MinValue, int.MaxValue).ToString("{0:4x}"));
                    break;
                default:
                    sbOut.Append(GetRandomString(r, r.Next(maxlen)));
                    break;
            }
            return sbOut.ToString();
        }

        private string GetRandomFieldMessage(DefinitionDS.FieldTypeRow rFieldType)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            StringBuilder sbOut = new StringBuilder();

            DataRow[] rValueTypeList = rFieldType.GetChildRows("FieldType_ValueType");

            int count = rValueTypeList.Length;
            int countToGen = (count / (int)4) + 1; // prendo 1/4 dei messaggi possibili

            for (int j = 0; j < countToGen; j++)
            {
                DefinitionDS.ValueTypeRow rValueType = (DefinitionDS.ValueTypeRow)rValueTypeList[r.Next(count - 1)];
                sbOut.AppendFormat("{0}={1};", rValueType.Des, rValueType.Sev);
            }

            return sbOut.ToString();
        }

        private string GetRandomStreamMessage(dsDeviceStatus.StreamsRow rStream)
        {
            DataRow[] rStreamFieldList = rStream.GetChildRows("Streams_Stream_fields");
            StringBuilder sbDescription = new StringBuilder();

            foreach (DataRow row in rStreamFieldList)
            {
                dsDeviceStatus.StreamFieldsRow rStreamField = (dsDeviceStatus.StreamFieldsRow)row;
                sbDescription.Append(rStreamField.Description);
            }
            return sbDescription.ToString();
        }

        private byte[] GetRandomStreamData(dsDeviceStatus.StreamsRow rStream)
        {
            DataRow[] rStreamFieldList = rStream.GetChildRows("Streams_Stream_fields");
            StringBuilder sbData = new StringBuilder();

            foreach (DataRow row in rStreamFieldList)
            {
                dsDeviceStatus.StreamFieldsRow rStreamField = (dsDeviceStatus.StreamFieldsRow)row;
                sbData.Append(rStreamField.Value);
            }
            return new System.Text.ASCIIEncoding().GetBytes(sbData.ToString());
        }

        private string GetRandomString(Random r, int len)
        {
            string s = string.Empty;
            for (int i = 0; i < len; i++)
                string.Concat(s, Convert.ToChar(r.Next(33, 95)).ToString());
            return s;

        }

        private void MergeDataSet(DataSet dsSource, DataSet dsTarget, DataSet dsChange)
        {
            dsChange.Clear();

            foreach (DataTable dtSource in dsSource.Tables)
            {
                string[] sColumns = GetColumnsToCompare(dtSource).Split(',');
                DataTable dtTarget = dsTarget.Tables[dtSource.TableName];
                DataTable dtChange = dsChange.Tables[dtSource.TableName];

                dtTarget.BeginLoadData();
                dtChange.BeginLoadData();

                // aggiungi e aggiorna le righe
                foreach (DataRow rowSource in dtSource.Rows)
                {
                    // find target row
                    DataRow rowTarget = dtTarget.Rows.Find(GetPKValues(rowSource));

                    if (rowTarget == null)
                    {
                        //LocalUtil.WriteLog(string.Format("NewRecord:{0}", dtSource.TableName));
                        //LocalUtil.WriteLog(null, rowSource);
                        dtTarget.Rows.Add(rowSource.ItemArray);
                        dtChange.Rows.Add(rowSource.ItemArray);
                    }
                    else
                    {
                        foreach (string colName in sColumns)
                        {
                            DataColumn colSource = dtSource.Columns[colName];

                            if (rowSource[colName].ToString() != rowTarget[colName].ToString())
                            {
                                //LocalUtil.WriteLog(string.Format("Column Changed:{0}.{1}", dtSource.TableName, colName));
                                //LocalUtil.WriteLog(rowTarget, rowSource);
                                dtChange.Rows.Add(rowSource.ItemArray);
                                break;
                            }
                        }
                        rowTarget.ItemArray = rowSource.ItemArray;
                    }

                }
                // cancella le righe in + o le marca come da cancellare se la tabella ha la colonna "IsDeleted"
                for (int i = dtTarget.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow rowTarget = dtTarget.Rows[i];
                    if (rowTarget.RowState == DataRowState.Unchanged)
                    {
                        //LocalUtil.WriteLog(string.Format("Deleted Record:{0}", rowTarget.Table.TableName));
                        DataRow deletedRow;
                        if (dtTarget.Columns.Contains("IsDeleted"))
                        {
                            deletedRow = dtChange.Rows.Add(rowTarget.ItemArray);
                            deletedRow["IsDeleted"] = true;
                        }
                        else
                        {
                            deletedRow = rowTarget;
                        }
                        //_testServerLog.WriteLog(deletedRow, string.Empty);
                        dtTarget.Rows.Remove(rowTarget);
                    }
                }
                dtTarget.EndLoadData();
                dtChange.EndLoadData();
            }
            dsChange.AcceptChanges();
            dsTarget.AcceptChanges();
        }

        private object[] GetPKValues(DataRow rowSource)
        {
            DataColumn[] pkColumns = rowSource.Table.PrimaryKey;
            object[] pkValues = new object[pkColumns.Length];

            for (int i = 0; i < pkColumns.Length; i++)
                pkValues[i] = rowSource[pkColumns[i]];

            return pkValues;
        }

        private string GetColumnsToCompare(DataTable table)
        {

            object o = global::Telefin.Properties.Settings.Default.PropertyValues[string.Format("Table_{0}_Columns", table.TableName)];

            if (o == null || ((SettingsPropertyValue)o).PropertyValue == null)
            {
                string s = string.Empty;
                foreach (DataColumn col in table.Columns)
                    s += col.ColumnName + ",";
                if (s.Length > 0)
                    return s.Substring(0, s.Length - 1);
                else
                    return string.Empty;
            }
            else
            {
                return ((SettingsPropertyValue)o).PropertyValue.ToString();
            }
        }

        private int GetDataSetRowsCount(DataSet ds)
        {
            int count = 0;
            foreach (DataTable dt in ds.Tables)
                count += dt.Rows.Count;
            return count;
        }

        private bool LoadFromCache(DataSet ds, string filename)
        {
            try
            {
                ds.Clear();
                if (File.Exists(filename))
                {
                    FileStream fs = new FileStream(filename, FileMode.Open);
                    ds.ReadXml(fs);
                    fs.Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _testServerLog.WriteLog(string.Format("Error on LoadCache File={0} Error={1}", filename, ex.Message), Guid.Empty);
                return false;
            }
        }

        private bool SaveCache(DataSet ds, string filename)
        {
            
            try
            {
                FileStream fs = new FileStream(filename, FileMode.Create);
                ds.WriteXml(fs);
                fs.Close();
                return true;
            }
            catch (Exception ex)
            {
                _testServerLog.WriteLog(string.Format("Error on SaveCache File={0} Error={1} ", filename, ex.Message), Guid.Empty);
                return false;
            }
        }

        private ICredentials GetCredentials()
        {
            if (string.IsNullOrEmpty(global::Telefin.WebServiceAccount.Default.Username))
                return CredentialCache.DefaultCredentials;
            else
            {
                string username = global::Telefin.WebServiceAccount.Default.Username;
                string password = global::Telefin.WebServiceAccount.Default.Password;
                string domain = global::Telefin.WebServiceAccount.Default.Domain;
                if (string.IsNullOrEmpty(domain))
                {
                    return new NetworkCredential(username, password);
                }
                else
                {
                    return new NetworkCredential(username, password, domain);
                }
            }
        }

        #endregion


    }

    public class TestAgentParameters
    {
        private int _serverId = 0;
        private int _intervalSeconds = 0;
        private int _updateCountMin = 0; // numero di volte minimo in cui viene simulato un update reale
        private int _updateCountMax = 1; // numero di volte massimo in cui viene simulato un update reale
        private int _numDevices = 0;
        private int _percUpdateDevices = 0;
        private int _percUpdateDevicesStatus = 0;
        private int _percUpdateStreams = 0;
        private int _percUpdateStreamsFields = 0;

        public int ServerId
        {
            get { return _serverId; }
            set { _serverId = value; }
        }
        
        public int IntervalSeconds
        {
            get { return _intervalSeconds; }
            set { _intervalSeconds = value; }
        }

        public int UpdateCountMin
        {
            get { return _updateCountMin; }
            set { _updateCountMin = value; }
        }

        public int UpdateCountMax
        {
            get { return _updateCountMax; }
            set { _updateCountMax = value; }
        }

        public int NumDevices
        {
            get { return _numDevices; }
            set { _numDevices = value; }
        }

        public int PercUpdateDevices
        {
            get { return _percUpdateDevices; }
            set { _percUpdateDevices = value; }
        }

        public int PercUpdateDevicesStatus
        {
            get { return _percUpdateDevicesStatus; }
            set { _percUpdateDevicesStatus = value; }
        }

        public int PercUpdateStreams
        {
            get { return _percUpdateStreams; }
            set { _percUpdateStreams = value; }
        }

        public int PercUpdateStreamsFields
        {
            get { return _percUpdateStreamsFields; }
            set { _percUpdateStreamsFields = value; }
        }

    }

    internal class RandomSequence
    {
        
        private List<int> _list;
        private Random _random;

        public RandomSequence(int from, int to)
        {
            _random = new Random(DateTime.Now.Millisecond);
            _list = new List<int>(Math.Abs(to - from) + 1);

            for (int j = from; j <= to; j++)
            {
                _list.Add(j);
            }
        }

        public int Next()
        {
            if (_list.Count > 0)
            {
                int i = _random.Next(0, _list.Count - 1);
                int v = _list[i];
                _list.RemoveAt(i);
                return v;
            }
            else 
            {
                return int.MinValue;
            }
        }

        public bool IsEmpty 
        {
            get {return (_list.Count == 0);}
        }
        

    }

    public class TestServerLog
    {
        private int _serverId = 0;


        public TestServerLog(int serverId)
        {
            _serverId = serverId;
        }

        public Guid WriteLog(string log)
        {
            return WriteLog(log, 0,0, Guid.Empty);
        }

        public Guid WriteLog(string log, Guid guidLog)
        {
            return WriteLog(log, 0,0, guidLog);
        }

        public Guid WriteLog(string log, int size,int rows,  Guid guidLog)
        {
            // datatime, machine, procedure, message, size
            if (guidLog == Guid.Empty)
                guidLog = Guid.NewGuid();

            log = log.Replace("\n", "\t");
            log = log.Replace("\r", "\t");
            log = log.Replace(";", ",");

            System.Diagnostics.Trace.WriteLine(string.Format("{0};{1}.{2}.{3};{4};{5};{6};{7};{8}",
                        DateTime.Now.ToString("o"),
                        System.Net.Dns.GetHostName(),
                        System.Diagnostics.Process.GetCurrentProcess().Id,
                        System.Threading.Thread.CurrentThread.ManagedThreadId,
                        _serverId,
                        log,
                        size,
                        rows,
                        guidLog));

            return guidLog;
        }

        public Guid WriteLog(DataRow rowOld, DataRow rowNew)
        {
            return Guid.Empty;
        }


        
    }


}



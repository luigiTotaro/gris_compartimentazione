using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite
{
    
    class RandomRegionList
    {
        XmlDocument _xDoc = null;
        private UInt16 _nodId = 0;
        private UInt16 _zonId = 0;
        private UInt16 _regId = 0;
        private string _nodName = "";
        private string _zonName = "";
        private string _regName = "";

        public RandomRegionList()
        {
            StreamReader sr = null;
            string fileName = System.AppDomain.CurrentDomain.BaseDirectory + "region_list.xml";
            try
            {
                if (File.Exists(fileName))
                {
                    _xDoc = new XmlDocument();
                    sr = new StreamReader(fileName, System.Text.ASCIIEncoding.ASCII);
                    _xDoc.Load(sr);
                }
                else
                {
                    throw new Exception(string.Format("File not found {0}", fileName));
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
            this.Next();
        }

        public void Next()
        {
            XmlNodeList xnl = _xDoc.SelectNodes("/telefin/system/region/zone/node[contains(@name,'sconosciut') = 0]");
            if (xnl.Count > 0)
            {
                int index = new Random().Next(xnl.Count);

                XmlNode node = xnl[index];
                XmlNode zone = node.ParentNode;
                XmlNode region = zone.ParentNode;

                _nodId = UInt16.Parse(node.Attributes["id"].Value);
                _nodName = node.Attributes["name"].Value;
                _zonId = UInt16.Parse(zone.Attributes["id"].Value);
                _zonName = zone.Attributes["name"].Value;
                _regId = UInt16.Parse(region.Attributes["id"].Value);
                _regName = region.Attributes["name"].Value;

            }
            else
            {
                throw new Exception("Erron on found next NodeId");
            }
 
        }


        public UInt16 RegionId
        {
            get 
            {
                return _regId;
            }
        }

        public string RegionName
        {
            get
            {
                return _regName;
            }
        }

        public UInt16 ZoneId
        {
            get
            {
                return _zonId;
            }
        }

        public string ZoneName
        {
            get
            {
                return _zonName;
            }
        }

        public UInt16 NodeId
        {
            get
            {
                return _nodId;
            }
        }

        public string NodeName
        {
            get
            {
                return _nodName;
            }
        }

        public Int64 NodeIdComplete
        {
            get
            {
                return this.EncodeNodId(_regId,_zonId,_nodId);
            }
        }

        public Int64 ZoneIdComplete
        {
            get
            {
                return this.EncodeZonId(_regId, _zonId);
            }
        }


        public Int64 RegionIdComplete
        {
            get
            {
                return this.EncodeRegId(_regId);
            }
        }


        private Int64 EncodeRegId(UInt16 regId)
        {
            UInt64 longValue = regId;
            longValue |= (UInt64)0xFFFF << 16;
            longValue |= (UInt64)0xFFFF << 32;

            return (Int64)longValue;
        }

        private Int64 EncodeZonId(UInt16 regId, UInt16 zonId)
        {
            UInt64 longValue = regId;
            longValue |= (UInt64)zonId << 16;

            return (Int64)longValue;
        }

        private Int64 EncodeNodId(UInt16 regId, UInt16 zonId, UInt16 nodId)
        {
            UInt64 longValue = regId;
            longValue |= (UInt64)zonId << 16;
            longValue |= (UInt64)nodId << 32;

            return (Int64)longValue;
        }



    }
}

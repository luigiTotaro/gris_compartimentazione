using System;
using System.Timers;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Telefin;



namespace GrisSuite
{
       // A delegate type for hooking up change notifications.
    public delegate void ServerStatedEventHandler(object sender, EventArgsTelefinServerStarted e);
    public delegate void CentralWSCalledEventHandler(object sender, EventArgsCentralWSCalled e);

    public class Simulator
    {
        private static TestDS _testDS;
        private static bool _isRunning = false;
        private static List<TestServer> _testSrvs = new List<TestServer>();
        public static event ServerStatedEventHandler ServerStarted;
        private static List<MethodCallStatistics> _methodCalls = new List<MethodCallStatistics>();
        private static object x = new object();


        public static void Start(TestDS testDS)
        {
            int serverIdOffet = global::Telefin.Properties.Settings.Default.ServerIdOffset;
            Random ran = new Random();
            _testDS = testDS;

            TestDS.TestPropertyRow testProperty = (TestDS.TestPropertyRow)testDS.TestProperty.Rows[0];
            int serverCount = 0;
            _testSrvs.Clear();

            for (int r = 1; r <= testDS.Simulation.Rows.Count; r++)
            {
                TestDS.SimulationRow simulationRow = (TestDS.SimulationRow)testDS.Simulation.Rows[r - 1];

                TestAgentParameters tap = new TestAgentParameters();
                tap.IntervalSeconds = testProperty.IntervalSeconds;
                tap.UpdateCountMin = testProperty.UpdateCountMin;
                tap.UpdateCountMax = testProperty.UpdateCountMax;
                tap.NumDevices = simulationRow.NumDevicesInServer;
                tap.PercUpdateDevices = simulationRow.PercUpdateDevices;
                tap.PercUpdateDevicesStatus = simulationRow.PercUpdateDevicesStatus;
                tap.PercUpdateStreams = simulationRow.PercUpdateStreams;
                tap.PercUpdateStreamsFields = simulationRow.PercUpdateStreamsFields;

                for (int i = 1; i <= simulationRow.NumStations; i++)
                {
                    for (int j = 1; j <= simulationRow.NumServersInStation; j++)
                    {
                        TestServer testSrv = new TestServer();
                        _testSrvs.Add(testSrv);

                        serverCount++;
                        tap.ServerId = serverIdOffet + serverCount;

                        ServerStarted(null,new EventArgsTelefinServerStarted(serverCount));

                        testSrv.CentralWSCalled += new CentralWSCalledEventHandler(testSrv_CentralWSCalled);
                        testSrv.Start(tap);

                        _isRunning = true;
                    }
                }
            }
        }

        static void testSrv_CentralWSCalled(object sender, EventArgsCentralWSCalled e)
        {
            lock (x)
            {
                MethodCallStatistics m = null;

                for (int i = 0; i < _methodCalls.Count; i++)
                {
                    if (_methodCalls[i].Name == e.MethodName)
                    {
                        m = _methodCalls[i];
                        break;
                    }
                }

                if (m != null)
                {
                    m.Count++;
                    m.Duration += e.Duration.Milliseconds;
                }
                else
                {
                    m = new MethodCallStatistics();
                    m.Name = e.MethodName;
                    m.Count = 1;
                    m.Duration = e.Duration.Milliseconds;
                    _methodCalls.Add(m);
                }
            }
        }

        public static List<MethodCallStatistics> MethodCalls
        {
            get { return _methodCalls; }
        }

        public static bool IsRunning
        {
            get { return _isRunning; }
        }

        public static void Stop()
        {
            foreach(TestServer testSrv in _testSrvs)
            {
                testSrv.Stop();
            }
            _testSrvs.Clear();
            _isRunning = false;

        }
    }

    public class EventArgsTelefinServerStarted : EventArgs
    {
        private int _serverStared = 0;

        public EventArgsTelefinServerStarted()
        { 
        }
        public EventArgsTelefinServerStarted(int serverStared)
        {
            _serverStared = serverStared;
        }

        public int ServerStared
        {
            get { return _serverStared; }
        }

    }

    public class EventArgsCentralWSCalled : EventArgs
    {
        private string _methodName = "";
        private TimeSpan _duration = TimeSpan.MinValue;

        public EventArgsCentralWSCalled()
        { 
        }
        public EventArgsCentralWSCalled(string methodName, TimeSpan duration)
        {
            _methodName = methodName;
            _duration = duration;
        }

        public string MethodName
        {
            get { return _methodName; }
        }

        public TimeSpan Duration
        {
            get { return _duration; }
        }

    }

    public class TestServer
    {
        private System.Timers.Timer _timer = null;
        private TestAgent _testAgent = null;
        private int _updateIndex = 0;
        private int _updateCount = 0;
        private int _updateCountMin = 1;
        private int _updateCountMax = 1;
        private object x = new object();
        private int _standardInterval = 1000;
        public event CentralWSCalledEventHandler CentralWSCalled;

        public void Start(TestAgentParameters tap)
        {
            _updateCountMin = tap.UpdateCountMin;
            _updateCountMax = tap.UpdateCountMax;

            _testAgent = new TestAgent(tap);


            _standardInterval = tap.IntervalSeconds * 1000;
            Random r = new Random(DateTime.Now.Millisecond);

            // alla prima partenza setto il timer con un incremento random per non cercare di sfasare le varie chiamate
            _timer = new System.Timers.Timer(_standardInterval + r.Next(5000,10000));
            _timer.AutoReset = true;
            _timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            _timer.Start();
            
        }

        public void Stop()
        {
            if (_timer != null && _timer.Enabled)
                _timer.Stop();
        }

        void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {

            if (Monitor.TryEnter(x))
            {
                try
                {
                    CentralWSCalled(this, new EventArgsCentralWSCalled("TimerEntered", TimeSpan.Zero));
                    if (!_testAgent.IsConfigValid)
                    {
                        DateTime t1 = DateTime.Now;
                        _testAgent.SendConfigToCentral(false);
                        CentralWSCalled(this, new EventArgsCentralWSCalled("SendConfigToCentral", (DateTime.Now - t1)));

                        DateTime t2 = DateTime.Now;
                        _testAgent.SendDeviceStatusToCentral(false);
                        CentralWSCalled(this, new EventArgsCentralWSCalled("SendDeviceStatusToCentralComplete", (DateTime.Now - t2)));

                        _updateCount = GetUpdateCount(_updateCountMin, _updateCountMax);
                        _updateIndex = 0;
                        _timer.Interval = _standardInterval;
                    }
                    else
                    {
                        _updateIndex++;
                        if (_updateIndex == _updateCount)
                        {
                            _updateCount = GetUpdateCount(_updateCountMin, _updateCountMax);
                            _updateIndex = 0;

                            DateTime t1 = DateTime.Now;
                            _testAgent.SendDeviceStatusToCentral(true);

                            CentralWSCalled(this, new EventArgsCentralWSCalled("SendDeviceStatusToCentral", (DateTime.Now - t1)));
                        }
                        else
                        {
                            DateTime t1 = DateTime.Now;
                            _testAgent.SendIsAlive();
                            CentralWSCalled(this, new EventArgsCentralWSCalled("SendIsAlive", (DateTime.Now - t1)));

                        }
                    }
                }
                catch
                {
                    CentralWSCalled(this, new EventArgsCentralWSCalled("TimerError", TimeSpan.Zero));
                }
                finally
                {
                    Monitor.Exit(x);
                }
            }
            else
            {
                CentralWSCalled(this, new EventArgsCentralWSCalled("TimerSkipped", TimeSpan.Zero)); 
            }
        }

        private int GetUpdateCount(int updateCountMin, int updateCountMax)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            return r.Next(updateCountMin, updateCountMax);
        }
    }

    public class MethodCallStatistics
    {
        string _name = string.Empty;
        long _duration = 0;
        int _count = 0;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public long Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
    }

}

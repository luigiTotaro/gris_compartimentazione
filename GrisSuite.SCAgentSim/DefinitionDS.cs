﻿using System;
using System.IO;


namespace GrisSuite {


    partial class DefinitionDS
    {
        public static void Fill(DefinitionDS ds)
        {
            if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "Definition.xml"))
            {
                ds.Clear();
                ds.ReadXml(System.AppDomain.CurrentDomain.BaseDirectory + "Definition.xml");
            }
            else
            {
                throw new Exception("Definition.xml not found");
            }
        }

        partial class DeviceTypeDataTable
        {

        }
    }
}

@echo off
sc stop "STLC Audia Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc delete "STLC Audia Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc create "STLC Audia Supervisor" binPath= "C:\Program Files\Telefin\AudiaSupervisorService\GrisSuite.AudiaSupervisorService.exe" start= auto DisplayName= "STLC Audia Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL
sc description "STLC Audia Supervisor" "STLC Audia Supervisor"

sc start "STLC Audia Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

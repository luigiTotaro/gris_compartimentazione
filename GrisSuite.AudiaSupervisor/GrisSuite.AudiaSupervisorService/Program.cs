﻿using System.ServiceProcess;

namespace GrisSuite.AudiaSupervisorService
{
    /// <summary>
    ///   Classe public avvio Supervisor
    /// </summary>
    public static class Program
    {
        private static void Main()
        {
#if (!DEBUG)
            // Se compilato in modalità Release è un servizio windows
            ServiceBase[] ServicesToRun = new ServiceBase[] {new MainService()};
            ServiceBase.Run(ServicesToRun);
#else
    // Se compilato in modalità Debug è una applicazione console (lo stop corrisponde con la chiusura dell'applicazione)
            using (MainService service = new MainService()) {
                service.Run();
            }
#endif
        }

#if (DEBUG)
    /// <summary>
    /// Esecuzione del programma in fase di strumentazione
    /// </summary>
        public static bool Run() {
            using (MainService service = new MainService()) {
                service.Run();
            }

            return true;
        }
#endif
    }
}
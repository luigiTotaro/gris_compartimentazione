﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GrisSuite.AudiaSupervisorLibrary.Database
{
	/// <summary>
	///     Rappresenta gli oggetti indicati per la decodifica dei valori
	/// </summary>
	public class DBStreamFieldValueDescription
	{
		/// <summary>
		///     Costruttore privato
		/// </summary>
		/// <param name="field">DBStreamField da valutare</param>
		/// <param name="isDefault">Indica se è il valore predefinito</param>
		/// <param name="valueDescription">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
		/// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
		/// <param name="exactStringValueToCompare">Valore di tipo stringa da confrontare con quello recuperato dalla query</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare una notifica per mail nel caso che questa sia la valutazione subentrata</param>
		public DBStreamFieldValueDescription(DBStreamField field,
		                                     bool isDefault,
		                                     string valueDescription,
		                                     Severity severityLevelIfMatch,
		                                     string exactStringValueToCompare,
		                                     bool shouldSendNotificationByEmail)
		{
			this.Field = field;
			this.IsDefault = isDefault;
			this.ValueDescription = valueDescription;
			this.SeverityLevelIfMatch = severityLevelIfMatch;
			this.ExactStringValueToCompare = exactStringValueToCompare;
			this.ShouldSendNotificationByEmailIfMatch = shouldSendNotificationByEmail;
			this.ParseDescriptions(valueDescription);
		}

		/// <summary>
		///     Field relativo alla decodifica dei valori
		/// </summary>
		public DBStreamField Field { get; private set; }

		/// <summary>
		///     Indica se il valore è quello predefinito
		/// </summary>
		public bool IsDefault { get; private set; }

		/// <summary>
		///     Indica se inviare una notifica per mail nel caso che questa sia la valutazione subentrata
		/// </summary>
		public bool ShouldSendNotificationByEmailIfMatch { get; private set; }

		/// <summary>
		///     Severità assegnata al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		public Severity SeverityLevelIfMatch { get; private set; }

		/// <summary>
		///     Valore statico di tipo stringa da confrontare con il valore recuperato
		/// </summary>
		public string ExactStringValueToCompare { get; private set; }

		/// <summary>
		///     Ritorna la descrizione assegnata al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		/// <param name="descriptionKey">Chiave per eseguire la lookup nell'elenco descrizioni</param>
		/// <returns></returns>
		public string DescriptionIfMatch(string descriptionKey)
		{
			if (this.DescriptionsIfMatch != null)
			{
				if (this.DescriptionsIfMatch.ContainsKey(descriptionKey))
				{
					return this.DescriptionsIfMatch[descriptionKey];
				}
			}
			else
			{
				return this.ValueDescription;
			}

			return string.Empty;
		}

		/// <summary>
		///     Descrizione assegnata al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		private string ValueDescription { get; set; }

		/// <summary>
		///     Dizionario di descrizioni assegnate al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		private Dictionary<string, string> DescriptionsIfMatch { get; set; }

		/// <summary>
		///     Elabora la stringa descrittiva, nel caso contenga un array di possibili valori, per decodifica enumerato
		/// </summary>
		/// <param name="descriptionIfMatch"></param>
		private void ParseDescriptions(string descriptionIfMatch)
		{
			if (!string.IsNullOrEmpty(descriptionIfMatch))
			{
				string[] descriptions = descriptionIfMatch.Split('§');

				if (descriptions.Length > 1)
				{
					this.DescriptionsIfMatch = new Dictionary<string, string>();

					Regex regex = new Regex("(?<key>.*?)=(?<description>.*)", RegexOptions.CultureInvariant | RegexOptions.Compiled);

					foreach (string valueDescription in descriptions)
					{
						Match data = regex.Match(valueDescription);
						if ((data.Groups.Count == 3) && (data.Groups["key"] != null) && (data.Groups["description"] != null) &&
						    (!string.IsNullOrEmpty(data.Groups["key"].Value)) && (!string.IsNullOrEmpty(data.Groups["description"].Value)))
						{
							this.DescriptionsIfMatch.Add(data.Groups["key"].Value, data.Groups["description"].Value);
						}
					}
				}
			}
		}
	}
}
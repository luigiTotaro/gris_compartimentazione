﻿using System;

namespace GrisSuite.AudiaSupervisorLibrary.Database
{
    public class DbDeviceAcknowledgement
    {
        /// <summary>
        /// Chiave primaria della tabella, ID della tacitazione
        /// </summary>
        public Guid DeviceAckID { get; private set; }

        /// <summary>
        /// Device ID
        /// </summary>
        public long DevID { get; private set; }

        /// <summary>
        /// Stream ID
        /// </summary>
        public int? StrID { get; private set; }

        /// <summary>
        /// Field ID
        /// </summary>
        public int? FieldID { get; private set; }

        /// <summary>
        /// Data e ora della tacitazione
        /// </summary>
        public DateTime AckDate { get; private set; }

        /// <summary>
        /// Durata in minuti della tacitazione
        /// </summary>
        public int AckDurationMinutes { get; private set; }

        /// <summary>
        /// Identificativo del Supervisore (2 Telnet), che gestisce la periferica
        /// </summary>
        public byte SupervisorID { get; private set; }

        /// <summary>
        /// Credenziali dell'utente che ha impostato la tacitazione
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="deviceAckID">Chiave primaria della tabella, ID della tacitazione</param>
        /// <param name="devID">Device ID</param>
        /// <param name="strID">Stream ID</param>
        /// <param name="fieldID">Field ID</param>
        /// <param name="ackDate">Data e ora della tacitazione</param>
        /// <param name="ackDurationMinutes">Durata in minuti della tacitazione</param>
		/// <param name="supervisorID">Identificativo del Supervisore (2 Telnet), che gestisce la periferica</param>
        /// <param name="username">Credenziali dell'utente che ha impostato la tacitazione</param>
        public DbDeviceAcknowledgement(Guid deviceAckID, long devID, int? strID, int? fieldID, DateTime ackDate, int ackDurationMinutes,
                                       byte supervisorID, string username)
        {
            this.DeviceAckID = deviceAckID;
            this.DevID = devID;
            this.StrID = strID;
            this.FieldID = fieldID;
            this.AckDate = ackDate;
            this.AckDurationMinutes = ackDurationMinutes;
            this.SupervisorID = supervisorID;
            this.Username = username;
        }
    }
}
﻿using System.Collections.ObjectModel;
using System.Text;

namespace GrisSuite.AudiaSupervisorLibrary.Database
{
	/// <summary>
	///     Rappresenta il singolo Stream da persistere su database
	/// </summary>
	public class DBStream
	{
		#region Variabili private

		private readonly Collection<DBStreamField> streamData;

		#endregion

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="streamId">Id dello Stream</param>
		/// <param name="name">Nome dello Stream</param>
		public DBStream(int streamId, string name)
		{
			this.StreamId = streamId;
			this.Name = name;
			this.ContainsStreamFieldWithError = false;
			this.streamData = new Collection<DBStreamField>();
		}

		#region Proprietà pubbliche

		/// <summary>
		///     Nome dello Stream
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		///     Lista dei Field contenuti nello Stream
		/// </summary>
		public Collection<DBStreamField> FieldData
		{
			get { return this.streamData; }
		}

		/// <summary>
		///     Id dello Stream
		/// </summary>
		public int StreamId { get; private set; }

		/// <summary>
		///     Indica se lo stream contiene almeno uno stream field che ha generato eccezione nel caricamento dei dati
		/// </summary>
		public bool ContainsStreamFieldWithError { get; private set; }

		/// <summary>
		///     Severità relativa allo Stream (come aggregata dei Field contenuti)
		/// </summary>
		public Severity SeverityLevel
		{
			get
			{
				Severity streamSeverity = Severity.Unknown;

				foreach (DBStreamField field in this.streamData)
				{
					// occorre escludere i campi che non saranno poi salvati sul database, perché la loro severità
					// sebbene effettiva, non deve essere contemplata
					if ((field.SeverityLevel != Severity.Unknown) && (field.PersistOnDatabase) &&
					    ((streamSeverity == Severity.Unknown) || ((int) field.SeverityLevel > (int) streamSeverity)))
					{
						streamSeverity = field.SeverityLevel;
					}

					if ((!this.ContainsStreamFieldWithError) && (field.LastError != null))
					{
						this.ContainsStreamFieldWithError = true;
					}
				}

				return streamSeverity;
			}
		}

		/// <summary>
		///     Decodifica descrittiva del valore dello Stream, come concatenazione delle descrizioni dei singoli Field, se la loro severità è non sconosciuta
		/// </summary>
		public string ValueDescriptionComplete
		{
			get
			{
				StringBuilder deviceDescription = new StringBuilder();

				int fieldCounter = 0;

				foreach (DBStreamField field in this.streamData)
				{
					// escludiamo dall'aggregazione i campi che non saranno salvati sul database
					if ((field.SeverityLevel != Severity.Unknown) && (field.PersistOnDatabase))
					{
						if (fieldCounter > 0)
						{
							deviceDescription.Append(";" + field.ValueDescriptionComplete);
						}
						else
						{
							deviceDescription.Append(field.ValueDescriptionComplete);
						}
						fieldCounter++;
					}
				}

				return deviceDescription.ToString();
			}
		}

		#endregion
	}
}
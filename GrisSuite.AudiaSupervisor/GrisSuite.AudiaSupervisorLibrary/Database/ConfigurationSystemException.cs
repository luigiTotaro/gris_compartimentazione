﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.AudiaSupervisorLibrary.Database
{
    [Serializable]
    public class ConfigurationSystemException : Exception
    {
        public ConfigurationSystemException()
        {
        }

        public ConfigurationSystemException(string message) : base(message)
        {
        }

        public ConfigurationSystemException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ConfigurationSystemException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
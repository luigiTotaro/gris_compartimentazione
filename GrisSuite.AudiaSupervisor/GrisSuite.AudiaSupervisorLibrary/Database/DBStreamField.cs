﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using GrisSuite.AudiaSupervisorLibrary.Properties;

namespace GrisSuite.AudiaSupervisorLibrary.Database
{
	/// <summary>
	///     Rappresenta il singolo Field da persistere su database
	/// </summary>
	public class DBStreamField
	{
		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="fieldId">Id del Field</param>
		/// <param name="arrayId">Posizione del valore nel vettore</param>
		/// <param name="fieldName">Nome del Field</param>
		/// <param name="telnetCommand">Comando Telnet da eseguire</param>
		public DBStreamField(int fieldId, int arrayId, string fieldName, string telnetCommand)
		{
			this.FieldId = fieldId;
			this.ArrayId = arrayId;
			this.Name = fieldName;
			this.TelnetCommand = telnetCommand;
			this.DbStreamFieldValueDescriptions = new Collection<DBStreamFieldValueDescription>();
			this.PersistOnDatabase = true;
		}

		#region Proprietà pubbliche

		/// <summary>
		///     Severità relativa al Field
		/// </summary>
		public Severity SeverityLevel { get; set; }

		/// <summary>
		///     Decodifica descrittiva del valore del Field
		/// </summary>
		public string ValueDescription { private get; set; }

		/// <summary>
		///     Decodifica descrittiva del valore del Field, concatenata alla severità (solo nel caso non sia sconosciuta)
		/// </summary>
		public string ValueDescriptionComplete
		{
			get
			{
				if (this.SeverityLevel == Severity.Unknown)
				{
					return this.ValueDescription;
				}

				return this.ValueDescription + "=" + (int) this.SeverityLevel;
			}
		}

		/// <summary>
		///     Id del Field
		/// </summary>
		public int FieldId { get; private set; }

		/// <summary>
		///     Comando Telnet da eseguire
		/// </summary>
		public String TelnetCommand { get; private set; }

		/// <summary>
		///     Valore del Field, come letto da comando Telnet
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		///     Lista di valori
		/// </summary>
		public IList<String> RawData { private get; set; }

		/// <summary>
		///     Posizione del valore nel vettore
		/// </summary>
		public int ArrayId { get; private set; }

		/// <summary>
		///     Nome del Field
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		///     Lista di descrizioni per la decodifica del valore e della severità
		/// </summary>
		public Collection<DBStreamFieldValueDescription> DbStreamFieldValueDescriptions { get; private set; }

		/// <summary>
		///     Indica se lo stream field deve essere salvato sul database
		/// </summary>
		/// <remarks>
		///     E' possibile che uno stream field di una tabella dinamica non abbia senso,
		///     a fronte di una valutazione del contatore che riguarda le righe della tabella
		/// </remarks>
		public bool PersistOnDatabase { get; set; }

		/// <summary>
		///     Contiene l'eventuale eccezione generata durante il popolamento con dati del field
		/// </summary>
		public Exception LastError { get; set; }

		/// <summary>
		///     Indica se, a seguito della valutazione dei casi, occorre inviare una notifica per mail, può essere resettato dall'esterno, durante la valutazione delle tacitazioni
		/// </summary>
		public bool ShouldSendNotificationByEmail { get; set; }

		#endregion

		private enum EvaluationNeeded
		{
			Unknown,
			StringComparison
		}

		private EvaluationNeeded ChooseEvaluationMethod()
		{
			EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

			if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
			{
				if (this.DbStreamFieldValueDescriptions.Count == 1)
				{
					evaluationNeeded = GetEvaluationNeeded(this.DbStreamFieldValueDescriptions[0]);
				}
				else
				{
					foreach (DBStreamFieldValueDescription dbStreamFieldValueDescription in
						this.DbStreamFieldValueDescriptions)
					{
						if (!dbStreamFieldValueDescription.IsDefault)
						{
							// Consideriamo solo il primo tipo - casi di tipi di valori etereogenei non sono supportati
							evaluationNeeded = GetEvaluationNeeded(dbStreamFieldValueDescription);
							break;
						}
					}
				}
			}

			return evaluationNeeded;
		}

		private static EvaluationNeeded GetEvaluationNeeded(DBStreamFieldValueDescription dbStreamFieldValueDescription)
		{
			EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

			if (!string.IsNullOrEmpty(dbStreamFieldValueDescription.ExactStringValueToCompare))
			{
				evaluationNeeded = EvaluationNeeded.StringComparison;
			}

			return evaluationNeeded;
		}

		/// <summary>
		///     Esegue la valutazione del valore contenuto nel campo, in base alle regole configurate
		/// </summary>
		public void Evaluate()
		{
			if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
			{
				if (this.DbStreamFieldValueDescriptions.Count == 1)
				{
					this.ParseDefaultSingleValue(this.DbStreamFieldValueDescriptions[0].DescriptionIfMatch(string.Empty),
					                             this.DbStreamFieldValueDescriptions[0].SeverityLevelIfMatch,
					                             this.DbStreamFieldValueDescriptions[0].ShouldSendNotificationByEmailIfMatch);
				}
				else
				{
					switch (this.ChooseEvaluationMethod())
					{
						case EvaluationNeeded.Unknown:
							this.ParseStringValue();
							break;
						case EvaluationNeeded.StringComparison:
							this.ParseStringValue();
							break;
					}
				}
			}
		}

		#region Metodi privati per l'elaborazione dei dati relativi ai vari Fields degli Stream, con decodifica di descrizione e severità

		/// <summary>
		///     Elabora i dati di tipo stringa relativi ai vari Fields degli Streams
		/// </summary>
		private void ParseStringValue()
		{
			if ((this.RawData != null) && (this.RawData.Count > 0))
			{
				if (this.ArrayId >= this.RawData.Count)
				{
					this.SetFieldStateToInvalidArrayIndex();
				}
				else
				{
					string valueString = this.RawData[this.ArrayId];

					if (valueString == null)
					{
						this.SetFieldStateToUnknown();
					}
					else
					{
						DBStreamFieldValueDescription defaultDescription = null;
						bool fieldValueSet = false;

						foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
						{
							if (String.Equals(valueString, description.ExactStringValueToCompare, StringComparison.OrdinalIgnoreCase))
							{
								this.SetFieldDataString(description.DescriptionIfMatch(valueString), valueString,
								                        description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch);
								fieldValueSet = true;
								break;
							}

							if (description.IsDefault)
							{
								// In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
								defaultDescription = description;
							}
						}

						if (!fieldValueSet)
						{
							if (defaultDescription != null)
							{
								this.SetFieldDataString(defaultDescription.DescriptionIfMatch(valueString), valueString,
								                        defaultDescription.SeverityLevelIfMatch,
								                        defaultDescription.ShouldSendNotificationByEmailIfMatch);
							}
							else
							{
								this.SetFieldStateToUnknown();
							}
						}
					}
				}
			}
			else
			{
				this.SetFieldStateToUnknown();
			}
		}

		/// <summary>
		///     Elabora un dato unico relativo al Field degli Streams
		/// </summary>
		/// <param name="valueDescription">Descrizione del valore passato</param>
		/// <param name="severity">Severità da attribuire allo stream field</param>
		/// <param name="shouldSendNotificationByEmailIfMatch">Indica se inviare mail di notifica, perché è subentrata questa condizione</param>
		private void ParseDefaultSingleValue(string valueDescription,
		                                     Severity severity,
		                                     bool shouldSendNotificationByEmailIfMatch)
		{
			if ((this.RawData != null) && (this.RawData.Count > 0))
			{
				if (this.ArrayId >= this.RawData.Count)
				{
					this.SetFieldStateToInvalidArrayIndex();
				}
				else
				{
					string value = this.RawData[this.ArrayId];

					if ((value != null) && (value.Trim().Length > 0))
					{
						this.SetFieldDataString(valueDescription, value, severity, shouldSendNotificationByEmailIfMatch);
					}
					else
					{
						this.SetFieldStateToUnknown();
					}
				}
			}
			else
			{
				this.SetFieldStateToUnknown();
			}
		}

		/// <summary>
		///     Imposta il valore, la descrizione e la severità del Field
		/// </summary>
		/// <param name="valueDescription">Descrizione del valore da impostare</param>
		/// <param name="value">Valore da impostare</param>
		/// <param name="severity">Severità da impostare</param>
		/// <param name="shouldSendNotificationByEmailIfMatch">Indica se inviare mail di notifica, perché è subentrata questa condizione</param>
		private void SetFieldDataString(string valueDescription,
		                                string value,
		                                Severity severity,
		                                bool shouldSendNotificationByEmailIfMatch)
		{
			this.SetFieldDataInternal(valueDescription, value, severity, shouldSendNotificationByEmailIfMatch);
		}

		/// <summary>
		///     Imposta il valore, la descrizione e la severità del Field
		/// </summary>
		/// <param name="valueDescription">Descrizione del valore da impostare</param>
		/// <param name="value">Valore da impostare</param>
		/// <param name="severity">Severità da impostare</param>
		/// <param name="shouldSendNotificationByEmailIfMatch">Indica se inviare mail di notifica, perché è subentrata questa condizione</param>
		private void SetFieldDataInternal(string valueDescription,
		                                  string value,
		                                  Severity severity,
		                                  bool shouldSendNotificationByEmailIfMatch)
		{
			this.SeverityLevel = severity;
			this.ValueDescription = valueDescription;
			this.Value = value;
			this.ShouldSendNotificationByEmail = shouldSendNotificationByEmailIfMatch;
		}

		/// <summary>
		///     Imposta il valore del Field nel caso il valore sia nullo, usando i dati di forzatura se presenti
		/// </summary>
		private void SetFieldStateToUnknown()
		{
			if (this.LastError != null)
			{
				this.SetFieldDataInternal(Settings.Default.ErrorOnDataRetrievalMessage, Settings.Default.ErrorOnDataRetrievalValue,
				                          Severity.Warning, false);
			}
			else
			{
				this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue,
				                          Severity.Unknown, false);
			}
		}

		/// <summary>
		///     Imposta il valore del Field nel caso si sia richiesto un valore con indice non disponibile
		/// </summary>
		private void SetFieldStateToInvalidArrayIndex()
		{
			this.SetFieldDataInternal(
				string.Format(CultureInfo.InvariantCulture,
				              "L'indice dell'array {0} non è valido per la dimensione dei dati disponibili {1}", this.ArrayId,
				              this.RawData.Count), Settings.Default.NoDataAvailableValue, Severity.Unknown, false);
		}

		#endregion
	}
}
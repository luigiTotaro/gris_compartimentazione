﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("GrisSuite.AudiaSupervisorLibrary - RC 1")]
[assembly: AssemblyDescription("Gestione recupero dati periferiche")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Telefin S.p.A.")]
[assembly: AssemblyProduct("GrisSuite.AudiaSupervisorLibrary")]
[assembly: AssemblyCopyright("Copyright © Telefin S.p.A. 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(false)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("6da1f534-1566-4f31-b5aa-bcbfd99f934c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading;
using GrisSuite.AudiaSupervisorLibrary.Devices.Telnet;
using GrisSuite.AudiaSupervisorLibrary.Properties;

namespace GrisSuite.AudiaSupervisorLibrary.Devices
{
	public static class AudiaDataRetriever
	{
		private const string bannerExpect = "Welcome to the Biamp Telnet server\r\n";

		/// <summary>
		///     Ritorna una serie di risultati dato un comando Telnet
		/// </summary>
		/// <param name="endpoint">Endpoint del server Telnet</param>
		/// <param name="telnetCommand">Comando Telnet da eseguire</param>
		/// <returns>Lista di stringhe con i valori di ritorno</returns>
		public static IList<String> Get(IPEndPoint endpoint, string telnetCommand)
		{
			if ((endpoint != null) && (!string.IsNullOrEmpty(bannerExpect)) && (!string.IsNullOrEmpty(telnetCommand)))
			{
				DateTime startRequest = DateTime.Now;

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Thread: {0} - Inizio richiesta Telnet, IP: {1}, comando: {2}",
					                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address,
					                                                             telnetCommand));
				}

				IList<String> data = null;

				try
				{
					TelnetClient telnetClient = new TelnetClient(Settings.Default.TelnetTimeoutMilliseconds);
					// I comandi di Audia devono rispondere solo con 0 e 1, ogni altra risposta è considerata una spuria
					string telnetData = telnetClient.ExpectAndExecute(endpoint, bannerExpect, telnetCommand, new List<char> { '0', '1' });

					if (!String.IsNullOrEmpty(telnetData))
					{
						data = new List<string>();
						data.Add(telnetData.Replace("\r", string.Empty).Replace("\n", string.Empty).Trim());
					}
				}
				catch (Exception ex)
				{
					if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Thread: {0} - Eccezione richiesta Telnet, IP: {1}, comando: {2}, Messaggio: {3}",
						                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address,
						                                                             telnetCommand, ex.Message));
					}
				}

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Thread: {0} - Fine richiesta Telnet, IP: {1}, comando: {2}, Durata richiesta: {3} (ms)",
					                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address,
					                                                             telnetCommand, requestDuration.TotalMilliseconds));
				}

				return data;
			}

			return null;
		}

		/// <summary>
		///     Verifica che l'endpoint Telnet sia raggiunbibile
		/// </summary>
		/// <param name="endpoint">Endpoint del server Telnet</param>
		/// <returns>Booleano che indica se l'endpoint risponde</returns>
		public static bool TestConnectivity(IPEndPoint endpoint)
		{
			bool canConnect = false;

			if (endpoint != null)
			{
				DateTime startRequest = DateTime.Now;

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Thread: {0} - Inizio richiesta Telnet raggiungibilità, IP: {1}",
					                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address));
				}

				try
				{
					TelnetClient telnetClient = new TelnetClient(Settings.Default.TelnetTimeoutMilliseconds);
					canConnect = telnetClient.TestConnectivity(endpoint);
				}
				catch (Exception ex)
				{
					if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Thread: {0} - Eccezione richiesta Telnet raggiungibilità, IP: {1}, Messaggio: {2}",
						                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address,
						                                                             ex.Message));
					}
				}

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Thread: {0} - Fine richiesta Telnet raggiungibilità, IP: {1}, Durata richiesta: {2} (ms)",
					                                                             Thread.CurrentThread.GetHashCode(), endpoint.Address,
					                                                             requestDuration.TotalMilliseconds));
				}

				return canConnect;
			}

			return false;
		}
	}
}
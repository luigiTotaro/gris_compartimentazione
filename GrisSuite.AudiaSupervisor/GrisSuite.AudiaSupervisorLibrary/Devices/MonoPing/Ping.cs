//
// System.Net.NetworkInformation.Ping
//
// Authors:
//	Gonzalo Paniagua Javier (gonzalo@novell.com)
//	Atsushi Enomoto (atsushi@ximian.com)
//
// Copyright (c) 2006-2007 Novell, Inc. (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
using System;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace GrisSuite.AudiaSupervisorLibrary.Devices.MonoPing
{
    public class Ping : Component
    {
        private const int DEFAULT_TIMEOUT = 4000; // 4 sec.
        private const int IDENTIFIER = 1; // no need to be const, but there's no place to change it.

        private static readonly byte[] default_buffer = new byte[0];

        // Sync
        public static PingReply Send(IPAddress address)
        {
            return Send(address, DEFAULT_TIMEOUT);
        }

        public static PingReply Send(IPAddress address, int timeout)
        {
            return Send(address, timeout, default_buffer);
        }

        public static PingReply Send(IPAddress address, int timeout, byte[] buffer)
        {
            return Send(address, timeout, buffer, new PingOptions());
        }

        public static PingReply Send(string hostNameOrAddress)
        {
            return Send(hostNameOrAddress, DEFAULT_TIMEOUT);
        }

        public static PingReply Send(string hostNameOrAddress, int timeout)
        {
            return Send(hostNameOrAddress, timeout, default_buffer);
        }

        public static PingReply Send(string hostNameOrAddress, int timeout, byte[] buffer)
        {
            return Send(hostNameOrAddress, timeout, buffer, new PingOptions());
        }

        public static PingReply Send(string hostNameOrAddress, int timeout, byte[] buffer, PingOptions options)
        {
            IPAddress[] addresses = Dns.GetHostAddresses(hostNameOrAddress);
            return Send(addresses[0], timeout, buffer, options);
        }

        private static IPAddress GetNonLoopbackIP()
        {
#pragma warning disable 618,612
            foreach (IPAddress addr in Dns.GetHostByName(Dns.GetHostName()).AddressList)
            {
#pragma warning restore 618,612
                if (!IPAddress.IsLoopback(addr))
                {
                    return addr;
                }
            }
            throw new InvalidOperationException("Could not resolve non-loopback IP address for localhost");
        }

        public static PingReply Send(IPAddress address, int timeout, byte[] buffer, PingOptions options)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }
            if (timeout < 0)
            {
                throw new ArgumentOutOfRangeException("timeout", "timeout must be non-negative integer");
            }
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }
            if (buffer.Length > 65500)
            {
                throw new ArgumentException("buffer length must be less than 65500 bytes", "buffer");
            }
            // options can be null.

            return SendPrivileged(address, timeout, buffer, options);
        }

        private static PingReply SendPrivileged(IPAddress address, int timeout, byte[] buffer, PingOptions options)
        {
            IPEndPoint target = new IPEndPoint(address, 0);
            IPEndPoint client = new IPEndPoint(GetNonLoopbackIP(), 0);

            using (Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Icmp))
            {
                if (options != null)
                {
                    s.DontFragment = options.DontFragment;
                    s.Ttl = (short) options.Ttl;
                }
                s.SendTimeout = timeout;
                s.ReceiveTimeout = timeout;
                // not sure why Identifier = 0 is unacceptable ...
                IcmpMessage send = new IcmpMessage(8, 0, IDENTIFIER, 0, buffer);
                byte[] bytes = send.GetBytes();
                s.SendBufferSize = bytes.Length;
                s.SendTo(bytes, bytes.Length, SocketFlags.None, target);

                DateTime sentTime = DateTime.Now;

                // receive
                bytes = new byte[100];
                do
                {
                    EndPoint endpoint = client;
                    int rc;

                    try
                    {
                        rc = s.ReceiveFrom(bytes, 0, 100, SocketFlags.None, ref endpoint);
                    }
                    catch (ArgumentNullException ex)
                    {
                        throw new NotSupportedException(String.Format(CultureInfo.InvariantCulture, "Unexpected socket error during ping request: {0}", ex.Message));
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        throw new NotSupportedException(String.Format(CultureInfo.InvariantCulture, "Unexpected socket error during ping request: {0}", ex.Message));
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode == SocketError.TimedOut)
                        {
                            return new PingReply(null, new byte[0], options, 0, IPStatus.TimedOut);
                        }
                        throw new NotSupportedException(String.Format(CultureInfo.InvariantCulture, "Unexpected socket error during ping request: {0}", ex.ErrorCode));
                    }
                    catch (ObjectDisposedException ex)
                    {
                        throw new NotSupportedException(String.Format(CultureInfo.InvariantCulture, "Unexpected socket error during ping request: {0}", ex.Message));
                    }

                    long rtt = (long) (DateTime.Now - sentTime).TotalMilliseconds;
                    int headerLength = (bytes[0] & 0xF) << 2;
                    int bodyLength = rc - headerLength;

                    // Ping reply to different request. discard it.
                    if (!((IPEndPoint) endpoint).Address.Equals(target.Address))
                    {
                        continue;
                    }

                    IcmpMessage recv = new IcmpMessage(bytes, headerLength, bodyLength);

                    /* discard ping reply to different request or echo requests if running on same host. */
                    if (recv.Identifier != IDENTIFIER || recv.Type == 8)
                    {
                        continue;
                    }

                    return new PingReply(address, recv.Data, options, rtt, recv.IPStatus);
                } while (true);
            }
        }

        // ICMP message
        private class IcmpMessage
        {
            private readonly byte[] bytes;

            // received
            public IcmpMessage(byte[] bytes, int offset, int size)
            {
                this.bytes = new byte[size];
                Buffer.BlockCopy(bytes, offset, this.bytes, 0, size);
            }

            // to be sent
            public IcmpMessage(byte type, byte code, short identifier, short sequence, byte[] data)
            {
                this.bytes = new byte[data.Length + 8];
                this.bytes[0] = type;
                this.bytes[1] = code;
                this.bytes[4] = (byte) (identifier & 0xFF);
                this.bytes[5] = (byte) (identifier >> 8);
                this.bytes[6] = (byte) (sequence & 0xFF);
                this.bytes[7] = (byte) (sequence >> 8);
                Buffer.BlockCopy(data, 0, this.bytes, 8, data.Length);

                ushort checksum = ComputeChecksum(this.bytes);
                this.bytes[2] = (byte) (checksum & 0xFF);
                this.bytes[3] = (byte) (checksum >> 8);
            }

            public byte Type
            {
                get { return this.bytes[0]; }
            }

            private byte Code
            {
                get { return this.bytes[1]; }
            }

            public byte Identifier
            {
                get { return (byte) (this.bytes[4] + (this.bytes[5] << 8)); }
            }

            public byte[] Data
            {
                get
                {
                    byte[] data = new byte[this.bytes.Length - 8];
                    Buffer.BlockCopy(this.bytes, 0, data, 0, data.Length);
                    return data;
                }
            }

            public byte[] GetBytes()
            {
                return this.bytes;
            }

            private static ushort ComputeChecksum(byte[] data)
            {
                uint ret = 0;
                for (int i = 0; i < data.Length; i += 2)
                {
                    ushort us = i + 1 < data.Length ? data[i + 1] : (byte) 0;
                    us <<= 8;
                    us += data[i];
                    ret += us;
                }
                ret = (ret >> 16) + (ret & 0xFFFF);
                return (ushort) ~ ret;
            }

            public IPStatus IPStatus
            {
                get
                {
                    switch (this.Type)
                    {
                        case 0:
                            return IPStatus.Success;
                        case 3: // destination unreacheable
                            switch (this.Code)
                            {
                                case 0:
                                    return IPStatus.DestinationNetworkUnreachable;
                                case 1:
                                    return IPStatus.DestinationHostUnreachable;
                                case 2:
                                    return IPStatus.DestinationProtocolUnreachable;
                                case 3:
                                    return IPStatus.DestinationPortUnreachable;
                                case 4:
                                    return IPStatus.BadOption; // FIXME: likely wrong
                                case 5:
                                    return IPStatus.BadRoute; // not sure if it is correct
                            }
                            break;
                        case 11:
                            switch (this.Code)
                            {
                                case 0:
                                    return IPStatus.TimeExceeded;
                                case 1:
                                    return IPStatus.TtlReassemblyTimeExceeded;
                            }
                            break;
                        case 12:
                            return IPStatus.ParameterProblem;
                        case 4:
                            return IPStatus.SourceQuench;
                        case 8:
                            return IPStatus.Success;
                    }
                    return IPStatus.Unknown;
                    //throw new NotSupportedException (String.Format ("Unexpected pair of ICMP message type and code: type is {0} and code is {1}", Type, Code));
                }
            }
        }
    }
}
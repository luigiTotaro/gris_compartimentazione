﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading;
using GrisSuite.AudiaSupervisorLibrary.Database;
using GrisSuite.AudiaSupervisorLibrary.Properties;

namespace GrisSuite.AudiaSupervisorLibrary.Devices
{
	/// <summary>
	///     <para>Rappresenta la classe base per i dispositivi Telnet.</para>
	/// </summary>
	public class TelnetDeviceBase : IDevice
	{
		#region Variabili private

		private bool isPopulated;
		private readonly Collection<DBStream> streamData;
		private readonly Collection<EventObject> eventsData;
		private readonly string deviceType;
		// Indica se la periferica contiene solamente stream in stato Info
		private bool containsOnlyInfoStreams;
		// Indica se esiste almeno uno stream field che ha generato eccezioni durante il caricamento dei dati
		private bool deviceContainsStreamWithError;
		// Severità calcolata della periferica
		private Severity severity;

		#endregion

		#region Costruttore

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
		/// <param name="deviceName">Nome della periferica</param>
		/// <param name="deviceType">Codice univoco della periferica</param>
		/// <param name="deviceIP">Indirizzo IP e porta della periferica</param>
		/// <param name="lastEvent">Informazioni dell'ultimo evento in ordine temporale associato alla periferica</param>
		/// <param name="deviceConfigurationHelperParameters">Classe helper che contiene i parametri base delle definizioni</param>
		/// <param name="device">Contenitore oggetto periferica, come letto da System.xml</param>
		public TelnetDeviceBase(bool fakeLocalData,
		                        string deviceName,
		                        string deviceType,
		                        IPEndPoint deviceIP,
		                        EventObject lastEvent,
		                        DeviceConfigurationHelperParameters deviceConfigurationHelperParameters,
		                        DeviceObject device)
		{
			if (String.IsNullOrEmpty(deviceName))
			{
				throw new ArgumentException("Il nome della periferica è obbligatorio.", "deviceName");
			}

			if (String.IsNullOrEmpty(deviceType))
			{
				throw new ArgumentException("Il tipo della periferica è obbligatorio.", "deviceType");
			}

			if ((deviceIP == null) || (deviceIP.Address == null))
			{
				throw new ArgumentException("L'indirizzo IP della periferica è obbligatorio.", "deviceIP");
			}

			if (device == null)
			{
				throw new ArgumentException("L'oggetto della periferica è obbligatorio.", "device");
			}

			this.DeviceName = deviceName;
			this.deviceType = deviceType;
			this.streamData = new Collection<DBStream>();
			this.eventsData = new Collection<EventObject>();
			this.DeviceIPEndPoint = deviceIP;
			this.LastEvent = lastEvent;
			this.LastError = null;
			this.DefinitionFileName = deviceConfigurationHelperParameters.DefinitionFileName;
			this.FakeLocalData = fakeLocalData;
			this.DeviceConfigurationParameters = deviceConfigurationHelperParameters;
			this.Device = device;
			this.containsOnlyInfoStreams = false;
			this.severity = Severity.Unknown;
			this.ShouldSendNotificationByEmail = false;

			try
			{
				DeviceConfigurationHelper deviceConfigurationHelper = new DeviceConfigurationHelper(this.FakeLocalData,
				                                                                                    this.DeviceIPEndPoint,
				                                                                                    this
					                                                                                    .DeviceConfigurationParameters);

				this.DefinitionFileName = deviceConfigurationHelper.DefinitionFileName;
				this.DeviceConfigurationParameters = deviceConfigurationHelper.DeviceConfigurationParameters;

				deviceConfigurationHelper.LoadConfiguration(this.streamData);
			}
			catch (DeviceConfigurationHelperException ex)
			{
				this.LastError = ex;
			}
			catch (ArgumentException ex)
			{
				this.LastError = ex;
			}

			if (this.LastError == null)
			{
				this.CheckStatus();
			}
		}

		#endregion

		#region Proprietà pubbliche

		/// <summary>
		///     Nome della periferica
		/// </summary>
		public string DeviceName { get; private set; }

		/// <summary>
		///     Codice univoco della periferica (forzato nel caso di definizioni dinamiche)
		/// </summary>
		public string DeviceType
		{
			get { return this.deviceType; }
		}

		/// <summary>
		///     Nome completo della definizione XML della periferica
		/// </summary>
		public string DefinitionFileName { get; private set; }

		/// <summary>
		///     EndPoint IP della periferica
		/// </summary>
		public IPEndPoint DeviceIPEndPoint { get; private set; }

		/// <summary>
		///     Lista degli Stream relativi alla periferica
		/// </summary>
		public Collection<DBStream> StreamData
		{
			get
			{
				if (this.LastError != null)
				{
					return new Collection<DBStream>();
				}

				return this.streamData;
			}
		}

		/// <summary>
		///     Classe helper che contiene i parametri base delle definizioni
		/// </summary>
		public DeviceConfigurationHelperParameters DeviceConfigurationParameters { get; private set; }

		/// <summary>
		///     Contenitore oggetto periferica, come letto da System.xml
		/// </summary>
		public DeviceObject Device { get; private set; }

		/// <summary>
		///     Indica se caricare i dati da file di risposte locali per simulazione
		/// </summary>
		public bool FakeLocalData { get; private set; }

		/// <summary>
		///     <para>Severità relativa al Device.</para>
		/// </summary>
		public Severity SeverityLevel
		{
			get { return this.severity; }
		}

		/// <summary>
		///     Stato del Device, inteso come Online (raggiungibile almeno via Telnet) oppure Offline (non raggiungibile e non monitorabile)
		/// </summary>
		public byte Offline
		{
			get
			{
				if (this.IsAliveIcmp && this.IsAliveTelnet)
				{
					return 0;
				}

				if (!this.IsAliveIcmp && this.IsAliveTelnet)
				{
					return 0;
				}

				if (this.IsAliveIcmp && !this.IsAliveTelnet)
				{
					return 0;
				}

				return 1;
			}
		}

		/// <summary>
		///     <para>Decodifica descrittiva dello stato della periferica</para>
		///     <para>0 = "In servizio" - Colore verde, significa che la periferica è in servizio e non presenta alcuna anomalia</para>
		///     <para>1 = "Anomalia lieve" - Colore giallo, significa che ci sono delle anomalie al funzionamento ma non pregiudicano il servizio del sistema</para>
		///     <para>2 = "Anomalia grave" - Colore rosso, le anomalie sono gravi e possono pregiudicare il servizio del sistema</para>
		///     <para>255 = "Sconosciuto" - Non è stato possibile determinare lo stato della periferica (dati ricevuti non coerenti, periferica non ancora raggiunta o mai raggiunta)</para>
		/// </summary>
		public string ValueDescriptionComplete
		{
			get
			{
				if (this.LastError != null)
				{
					return Settings.Default.DeviceStatusMessageUnknown;
				}

				if (this.IsAliveIcmp && !this.IsAliveTelnet)
				{
					return Settings.Default.DeviceStatusMessageMaintenanceMode;
				}

				if ((this.IsAliveIcmp && this.IsAliveTelnet) || (!this.IsAliveIcmp && this.IsAliveTelnet))
				{
					switch (this.SeverityLevel)
					{
						case Severity.Ok:
							return Settings.Default.DeviceStatusMessageOK;
						case Severity.Warning:
							if (this.deviceContainsStreamWithError)
							{
								return Settings.Default.DeviceStatusMessageIncompleteData;
							}

							return Settings.Default.DeviceStatusMessageWarning;
						case Severity.Error:
							return Settings.Default.DeviceStatusMessageError;
						case Severity.Unknown:
							return this.containsOnlyInfoStreams
								       ? Settings.Default.DeviceStatusMessageNotApplicable
								       : Settings.Default.DeviceStatusMessageUnknown;
					}
				}

				if (!this.IsAliveIcmp && !this.IsAliveTelnet)
				{
					return Settings.Default.DeviceStatusMessageUnreachable;
				}

				return Settings.Default.DeviceStatusMessageUnknown;
			}
		}

		/// <summary>
		///     Indica se la periferica è raggiungibile via ICMP (ping)
		/// </summary>
		public bool IsAliveIcmp { get; private set; }

		/// <summary>
		///     Indica se la periferica è raggiungibile via Telnet
		/// </summary>
		public bool IsAliveTelnet { get; private set; }

		/// <summary>
		///     Indica se c'è stata una eccezione durante il caricamento della periferica
		/// </summary>
		public Exception LastError { get; set; }

		/// <summary>
		///     Informazioni dell'ultimo evento in ordine temporale associato alla periferica
		/// </summary>
		public EventObject LastEvent { get; private set; }

		/// <summary>
		///     Lista degli Eventi relativi alla periferica
		/// </summary>
		public Collection<EventObject> EventsData
		{
			get { return this.eventsData; }
		}

		/// <summary>
		///     Indica se occorre inviare una notifica per mail. È un aggregato denormalizzato dei flag di tutti gli Stream Field contenuti
		/// </summary>
		public bool ShouldSendNotificationByEmail { get; private set; }

		#endregion

		#region Metodi privati

		private void RetrieveData()
		{
			foreach (DBStream stream in this.StreamData)
			{
				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Thread: {0} - Recupero dati Stream ID: {1} - Stream Name: {2}",
					                                                             Thread.CurrentThread.GetHashCode(), stream.StreamId,
					                                                             stream.Name));
				}

				foreach (DBStreamField field in stream.FieldData)
				{
					if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Thread: {0} - Recupero dati Stream Field ID: {1} - Stream Field Name: {2}",
						                                                             Thread.CurrentThread.GetHashCode(), field.FieldId,
						                                                             field.Name));
					}

					if (!String.IsNullOrEmpty(field.TelnetCommand))
					{
						#region Recupero dati Telnet

						IList<String> values = AudiaDataRetriever.Get(this.DeviceIPEndPoint, field.TelnetCommand);

						if ((values == null) || (values.Count == 0))
						{
							field.SeverityLevel = Severity.Unknown;
							field.ValueDescription = Settings.Default.NoDataAvailableMessage;
							field.Value = Settings.Default.NoDataAvailableValue;
						}
						else
						{
							field.RawData = values;
						}

						#endregion
					}
				}
			}
		}

		private void CheckStatus()
		{
			this.GetIsAliveIcmp();
			this.GetIsAliveTelnet();
		}

		/// <summary>
		///     Ottiene una risposta alla richiesta Ping ICMP
		/// </summary>
		/// <returns>True se l'endpoint risponde a ICMP, false altrimenti</returns>
		private void GetIsAliveIcmp()
		{
			this.IsAliveIcmp = IcmpDataRetriever.GetPingResponse(this.DeviceIPEndPoint);
		}

		/// <summary>
		///     Ottiene una risposta alla richiesta di connessione Telnet
		/// </summary>
		/// <returns>True se l'endpoint risponde su Telnet, false altrimenti</returns>
		private void GetIsAliveTelnet()
		{
			this.IsAliveTelnet = AudiaDataRetriever.TestConnectivity(this.DeviceIPEndPoint);
		}

		/// <summary>
		///     Popola i dati della periferica, in base agli Stream configurati
		/// </summary>
		private void PopulateInternal()
		{
			this.LastError = null;

			if (this.IsAliveTelnet)
			{
				this.RetrieveData();
			}

			if (this.LastError == null)
			{
				this.isPopulated = true;

				foreach (DBStream stream in this.StreamData)
				{
					foreach (DBStreamField field in stream.FieldData)
					{
						field.Evaluate();
					}
				}
			}
			else
			{
				this.isPopulated = false;
			}
		}

		/// <summary>
		///     Calcola la severità della periferica e ne valorizza gli stati possibili
		/// </summary>
		/// <returns></returns>
		private Severity CalculateSeverity()
		{
			if (this.LastError != null)
			{
				return Severity.Unknown;
			}

			if (this.IsAliveIcmp && !this.IsAliveTelnet)
			{
				return Severity.Warning;
			}

			Severity streamSeverity = Severity.Unknown;

			if (this.StreamData.Count > 0)
			{
				if (this.StreamData.Count == 1)
				{
					#region Esiste un solo stream

					// Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
					if ((this.StreamData[0].SeverityLevel != Severity.Unknown) &&
					    ((streamSeverity == Severity.Unknown) || ((int) this.StreamData[0].SeverityLevel > (int) streamSeverity)))
					{
						streamSeverity = this.StreamData[0].SeverityLevel;
					}

					if (streamSeverity == Severity.Info)
					{
						// Se la severità complessiva peggiore degli stream è rimasta ad info, la periferica va in stato sconosciuto,
						// perché non è possibile indicarne un vero stato
						streamSeverity = Severity.Unknown;
					}

					#endregion
				}
				else
				{
					#region Esiste più di uno stream

					Severity customMibStreamSeverity = Severity.Unknown;

					for (int streamCounter = 0; streamCounter < this.StreamData.Count; streamCounter++)
					{
						// Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
						if ((this.StreamData[streamCounter].SeverityLevel != Severity.Unknown) &&
						    ((customMibStreamSeverity == Severity.Unknown) ||
						     ((int) this.StreamData[streamCounter].SeverityLevel > (int) customMibStreamSeverity)))
						{
							customMibStreamSeverity = this.StreamData[streamCounter].SeverityLevel;
						}

						if ((!this.deviceContainsStreamWithError) && (this.StreamData[streamCounter].ContainsStreamFieldWithError))
						{
							this.deviceContainsStreamWithError = true;
						}
					}

					foreach (DBStream stream in this.StreamData)
					{
						// Escludiamo dal calcolo gli stream marcati come esclusi.
						if ((stream.SeverityLevel != Severity.Unknown) &&
						    ((streamSeverity == Severity.Unknown) || ((int) stream.SeverityLevel > (int) streamSeverity)))
						{
							streamSeverity = stream.SeverityLevel;
						}
					}

					if (streamSeverity == Severity.Info)
					{
						// Se la severità complessiva peggiore degli stream è rimasta ad info, la periferica va in stato sconosciuto,
						// perché non è possibile indicarne un vero stato
						streamSeverity = Severity.Unknown;
						this.containsOnlyInfoStreams = true;
					}

					// Valutiamo, come ultima cosa, la condizione di eccezione negli stream field.
					// Essendo la finale, prevale su qualsiasi altra, ma solo se la periferica non è già in errore critico
					// In caso di modifiche alla severità della condizione di eccezione, occorre adattare il codice
					// in modo che la severità forzata non nasconda severità più gravi calcolate
					if ((streamSeverity != Severity.Error) && (this.deviceContainsStreamWithError))
					{
						streamSeverity = Severity.Warning;
					}

					#endregion
				}
			}

			return streamSeverity;
		}

		#endregion

		#region Metodi pubblici

		/// <summary>
		///     Popola i dati della periferica, in base agli Stream configurati
		/// </summary>
		public void Populate()
		{
			this.LastError = null;

			if (this.IsAliveTelnet)
			{
				this.RetrieveData();
			}

			if (this.LastError == null)
			{
				this.isPopulated = true;

				foreach (DBStream stream in this.StreamData)
				{
					foreach (DBStreamField field in stream.FieldData)
					{
						field.Evaluate();
					}
				}

				if (Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem > 0)
				{
					// E' attivo il tentativo di recupero in caso di severità non normale della periferica
					// Tenta di recuperare una periferica in condizione di errore, ma non permanente (fluttuazione di stato)

					// C'è già stato un tentativo, quello principale, quindi il prossimo sarà il secondo
					ushort retryCounter = 2;

					while (retryCounter <= Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem)
					{
						if (this.SeverityLevel != Severity.Ok)
						{
							Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenRetriesToRecoverFromDeviceSeverityProblem);
							this.PopulateInternal();
							retryCounter++;
						}
						else
						{
							break;
						}
					}
				}
			}
			else
			{
				this.isPopulated = false;
			}

			// Il calcolo della severità permette di avere tutti gli stati possibili impostati e gli stream valutati
			this.severity = this.CalculateSeverity();
		}

		/// <summary>
		///     Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
		/// </summary>
		/// <returns>Rappresentazione completa e descrittiva della periferica a fini diagnostici</returns>
		public string Dump()
		{
			if ((!this.isPopulated) && (this.LastError == null))
			{
				// Se ci sono errori impostati, è già stato tentato un popolamento, quindi è inutile ritentare
				this.Populate();
			}

			StringBuilder dump = new StringBuilder();

			dump.AppendFormat(CultureInfo.InvariantCulture,
			                  "Device Name: {0}, Type: {1}, Offline: {2}, Severity: {3}, ReplyICMP: {4}, ReplyTelnet: {5}" +
			                  Environment.NewLine, this.DeviceName, this.DeviceType, this.Offline, (int) this.SeverityLevel,
			                  this.IsAliveIcmp, this.IsAliveTelnet);

			foreach (DBStream stream in this.StreamData)
			{
				dump.AppendFormat(CultureInfo.InvariantCulture,
				                  "Stream Id: {0}, Name: {1}, Severity: {2}, Description: {3}" + Environment.NewLine,
				                  stream.StreamId, stream.Name, (int) stream.SeverityLevel, stream.ValueDescriptionComplete);

				foreach (DBStreamField field in stream.FieldData)
				{
					if (field.PersistOnDatabase)
					{
						dump.AppendFormat(CultureInfo.InvariantCulture,
						                  "Field Id: {0}, Array Id: {1}, Name: {2}, Severity: {3}, Value: {4}, Description: {5}{6}" +
						                  Environment.NewLine, field.FieldId, field.ArrayId, field.Name, (int) field.SeverityLevel,
						                  field.Value, field.ValueDescriptionComplete,
						                  (field.LastError != null
							                   ? string.Format(CultureInfo.InvariantCulture, ", Exception: {0}, Inner Exception: {1}",
							                                   field.LastError.Message,
							                                   (field.LastError.InnerException != null)
								                                   ? field.LastError.InnerException.Message
								                                   : "N/A")
							                   : string.Empty));
					}
				}
			}

			return dump.ToString();
		}

		#endregion
	}
}
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GrisSuite.AudiaSupervisorLibrary.Devices.Telnet
{
	internal class TelnetClient
	{
		private bool isConnected;
		private int connectionTimeout;
		private TcpClient tcpClient;
		private NetworkStream netStream;

		#region Telnet Control Codes and Options

		private const byte IAC = 255;
		private const byte DONT = 254;
		private const byte DO = 253;
		private const byte WONT = 252;
		private const byte WILL = 251;
		private const byte OPTION_ECHO = 1;
		private const byte OPTION_SUPPRESS_GO_AHEAD = 3;

		/// <summary>
		///     END OF FILE
		/// </summary>
		private const byte EOF = 236;

		/// <summary>
		///     SUSPEND
		/// </summary>
		private const byte SUSP = 237;

		/// <summary>
		///     ABORT PROCESS
		/// </summary>
		private const byte ABORT = 238;

		/// <summary>
		///     END OF RECORD
		/// </summary>
		private const byte EOR = 239;

		/// <summary>
		///     START OF SUBNEGOTIATION PARAMETERS
		/// </summary>
		private const byte SB = 250;

		/// <summary>
		///     GO AHEAD
		/// </summary>
		private const byte GA = 249;

		/// <summary>
		///     ERASE LINE
		/// </summary>
		private const byte EL = 248;

		/// <summary>
		///     ERASE CHARACTER
		/// </summary>
		private const byte EC = 247;

		/// <summary>
		///     ARE YOU THERE
		/// </summary>
		private const byte AYT = 246;

		/// <summary>
		///     ABORT OUTPUT
		/// </summary>
		private const byte AO = 245;

		/// <summary>
		///     INTERRUPT PROCESS
		/// </summary>
		private const byte INTERRUPT_PROCESS = 244;

		/// <summary>
		///     NVT BREAK CHARACTER
		/// </summary>
		private const byte BRK = 243;

		/// <summary>
		///     DATA MARK
		/// </summary>
		private const byte DM = 242;

		/// <summary>
		///     NO OPERATION
		/// </summary>
		private const byte NOP = 241;

		/// <summary>
		///     END OF SUBNEGOTIATION PARAMETERS
		/// </summary>
		private const byte SE = 240;

		private enum ReplyTelnetOptions
		{
			Do_Echo,
			Dont_Echo,
			Do_SuppressGoAhead,
			Dont_SuppressGoAhead
		}

		#endregion

		public int ConnectionTimeout
		{
			get { return this.connectionTimeout; }
			set { this.connectionTimeout = value; }
		}

		public TelnetClient(int sendReceiveTimeout)
		{
			this.isConnected = false;
			this.connectionTimeout = sendReceiveTimeout;
		}

		public bool TestConnectivity(IPEndPoint deviceIP)
		{
			bool returnValue = false;

			if (deviceIP == null)
			{
				throw new TelnetException("Informazioni per la connessione Telnet mancanti (indirizzo IP e porta del dispositivo).");
			}

			this.tcpClient = new TcpClient {ReceiveTimeout = this.connectionTimeout, SendTimeout = this.connectionTimeout};

			this.Connect(deviceIP);

			if (this.tcpClient.Connected)
			{
				this.netStream = this.tcpClient.GetStream();

				if ((this.netStream.CanRead) && (this.netStream.CanWrite))
				{
					returnValue = true;
				}
			}

			this.Disconnect();

			return returnValue;
		}

		public string ExpectAndExecute(IPEndPoint deviceIP, string bannerExpect, string command, List<char> possibleResponse)
		{
			if (deviceIP == null)
			{
				throw new TelnetException("Informazioni per la connessione Telnet mancanti (indirizzo IP e porta del dispositivo).");
			}

			this.tcpClient = new TcpClient {ReceiveTimeout = this.connectionTimeout, SendTimeout = this.connectionTimeout};

			this.Connect(deviceIP);

			if (this.tcpClient.Connected)
			{
				this.netStream = this.tcpClient.GetStream();

				if ((this.netStream.CanRead) && (this.netStream.CanWrite))
				{
					string returnData = this.ReceiveData();

					this.ReplyTelnetOption(ReplyTelnetOptions.Dont_Echo);

					if (!String.IsNullOrEmpty(command))
					{
						if ((returnData.Length > 0) && (returnData.IndexOf(bannerExpect, StringComparison.OrdinalIgnoreCase) >= 0))
						{
							returnData = this.DoCommand(command, true);

							this.Disconnect();

							if ((possibleResponse != null) && (possibleResponse.Count > 0))
							{
								if (!String.IsNullOrEmpty(returnData))
								{
									foreach (char c in returnData)
									{
										if (possibleResponse.Contains(c))
										{
											return c.ToString();
										}
									}

									return null;
								}

								return returnData;
							}

							return returnData;
						}
					}
					else
					{
						this.Disconnect();

						return null;
					}
				}
				else
				{
					this.Disconnect();

					throw new TelnetException(
						String.Format("Impossibile leggere e scrivere nello stream dati della connessione Telnet, dispositivo {0}",
						              deviceIP));
				}
			}
			else
			{
				this.Disconnect();

				throw new TelnetException(String.Format("Impossibile stabilire una connessione Telnet, dispositivo {0}", deviceIP));
			}

			return null;
		}

		private void SendCommand(string command)
		{
			Byte[] sendBytes = Encoding.ASCII.GetBytes(command + "\r\n");
			this.netStream.Write(sendBytes, 0, sendBytes.Length);
		}

		private string DoCommand(string command, bool expectResponse)
		{
			if (command.Length > 0)
			{
				this.SendCommand(command);
			}

			if (expectResponse)
			{
				return this.ReceiveData();
			}

			return String.Empty;
		}

		private string ReceiveData()
		{
			byte[] returnDataBytes;

			this.ReceiveBytes(out returnDataBytes);

			return Encoding.ASCII.GetString(RemoveControlChars(returnDataBytes));
		}

		public static byte[] RemoveControlChars(byte[] response)
		{
			List<byte> outputBuffer = new List<byte>();

			for (int byteCounter = 0; byteCounter < response.Length; byteCounter++)
			{
				if (byteCounter == response.Length - 1)
				{
					// Siamo all'ultimo byte del buffer, non pu� essere un comando
					outputBuffer.Add(response[byteCounter]);
					continue;
				}

				if ((response[byteCounter] == IAC) && (response[byteCounter + 1] == IAC))
				{
					// IAC seguito da IAC � una sequenza di Escape che indica 0xFF 0xFF
					outputBuffer.Add(response[byteCounter]);
					outputBuffer.Add(response[byteCounter + 1]);
					byteCounter++;
					continue;
				}

				if (((response[byteCounter] == IAC) && (response[byteCounter + 1] == DONT)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == DO)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == WONT)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == WILL)))
				{
					// Comandi da 3 byte
					byteCounter = byteCounter + 2;
					continue;
				}

				if (((response[byteCounter] == IAC) && (response[byteCounter + 1] == SB)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == GA)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == EL)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == EC)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == AYT)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == AO)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == INTERRUPT_PROCESS)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == EOR)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == BRK)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == DM)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == NOP)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == ABORT)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == SUSP)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == EOF)) ||
				    ((response[byteCounter] == IAC) && (response[byteCounter + 1] == SE)))
				{
					// Comandi da 2 byte
					byteCounter++;
					continue;
				}

				outputBuffer.Add(response[byteCounter]);
			}

			return outputBuffer.ToArray();
		}

		private void ReplyTelnetOption(ReplyTelnetOptions reply)
		{
			switch (reply)
			{
				case ReplyTelnetOptions.Do_Echo:
					this.netStream.Write(new[] {IAC, DO, OPTION_ECHO}, 0, 3);
					break;
				case ReplyTelnetOptions.Dont_Echo:
					this.netStream.Write(new[] {IAC, DONT, OPTION_ECHO}, 0, 3);
					break;
				case ReplyTelnetOptions.Do_SuppressGoAhead:
					this.netStream.Write(new[] {IAC, DO, OPTION_SUPPRESS_GO_AHEAD}, 0, 3);
					break;
				case ReplyTelnetOptions.Dont_SuppressGoAhead:
					this.netStream.Write(new[] {IAC, DONT, OPTION_SUPPRESS_GO_AHEAD}, 0, 3);
					break;
			}

			Thread.Sleep(100);
		}

		private void ReceiveBytes(out byte[] returnDataBytes)
		{
			byte[] bytes = new byte[this.tcpClient.ReceiveBufferSize];

			List<byte> data = new List<byte>();

			do
			{
				Thread.Sleep(100);

				int bytesRead = this.netStream.Read(bytes, 0, this.tcpClient.ReceiveBufferSize);

				for (int byteCounter = 0; byteCounter < bytesRead; byteCounter++)
				{
					data.Add(bytes[byteCounter]);
				}
			} while (this.tcpClient.Available > 0);

			returnDataBytes = data.ToArray();
		}

		private void Connect(IPEndPoint deviceIP)
		{
			try
			{
				this.tcpClient.Connect(deviceIP);

				this.isConnected = this.tcpClient.Connected;
			}
			catch (SocketException)
			{
				this.Disconnect();

				throw new TelnetException(String.Format("Impossibile stabilire una connessione Telnet, dispositivo {0}", deviceIP));
			}
		}

		private void Disconnect()
		{
			if (this.isConnected)
			{
				if (this.netStream != null)
				{
					this.netStream.Close();
				}

				if (this.tcpClient != null)
				{
					this.tcpClient.Close();
				}

				this.isConnected = false;
			}
		}
	}
}
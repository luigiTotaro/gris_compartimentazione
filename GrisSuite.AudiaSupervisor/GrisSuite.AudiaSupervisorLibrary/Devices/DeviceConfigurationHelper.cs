﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security;
using System.Xml;
using GrisSuite.AudiaSupervisorLibrary.Database;

namespace GrisSuite.AudiaSupervisorLibrary.Devices
{
	/// <summary>
	///     Classe helper per il caricamento delle definizioni
	/// </summary>
	public class DeviceConfigurationHelper
	{
		#region Proprietà

		/// <summary>
		///     Definizione della periferica
		/// </summary>
		public string DefinitionFileName { get; private set; }

		/// <summary>
		///     Indica se caricare i dati da file di risposte locali per simulazione
		/// </summary>
		public bool FakeLocalData { get; private set; }

		/// <summary>
		///     Classe helper che contiene i parametri base delle definizioni
		/// </summary>
		public DeviceConfigurationHelperParameters DeviceConfigurationParameters { get; private set; }

		#endregion

		#region Costruttore

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
		/// <param name="ipEndPoint">Indirizzo IP e porta della periferica</param>
		/// <param name="deviceConfigurationHelperParameters">Classe helper che contiene i parametri base delle definizioni</param>
		public DeviceConfigurationHelper(bool fakeLocalData,
		                                 IPEndPoint ipEndPoint,
		                                 DeviceConfigurationHelperParameters deviceConfigurationHelperParameters)
		{
			if (deviceConfigurationHelperParameters == null)
			{
				throw new ArgumentException("La classe DeviceConfigurationHelperParameters è obbligatoria.",
				                            "deviceConfigurationHelperParameters");
			}

			if ((ipEndPoint == null) || (ipEndPoint.Address == null))
			{
				throw new ArgumentException("L'indirizzo IP e porta della periferica sono obbligatori.", "ipEndPoint");
			}

			this.DefinitionFileName = deviceConfigurationHelperParameters.DefinitionFileName;
			this.FakeLocalData = fakeLocalData;
			this.DeviceConfigurationParameters = deviceConfigurationHelperParameters;
		}

		#endregion

		#region LoadConfiguration - Carica la definizione, la valida e popola la struttura di Stream e StreamField

		/// <summary>
		///     Carica la definizione, la valida e popola la struttura di Stream e StreamField
		/// </summary>
		/// <param name="streamData">Collezione di DbStream che sarà popolata nel parsing della definizione</param>
		public void LoadConfiguration(Collection<DBStream> streamData)
		{
			if (streamData == null)
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Collezione degli Stream da caricare nulla. Impossibile caricare la configurazione della periferica."));
			}

			XmlNode root = this.GetDefinitionXmlRootElement();

			if (root != null)
			{
				bool streamsFound = false;

				foreach (XmlNode streams in root.ChildNodes)
				{
					if (streams.Name.Equals("streams", StringComparison.OrdinalIgnoreCase))
					{
						#region Stream

						bool streamFound = false;

						foreach (XmlNode stream in streams.ChildNodes)
						{
							if (stream.Name.Equals("stream", StringComparison.OrdinalIgnoreCase))
							{
								if ((stream.Attributes["ID"] == null) || (stream.Attributes["ID"].Value.Trim().Length == 0) ||
								    (stream.Attributes["Name"] == null) || (stream.Attributes["Name"].Value.Trim().Length == 0))
								{
									throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
									                                                           "Impossibile trovare l'attributo 'ID' o 'Name' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
									                                                           this.DefinitionFileName, stream.OuterXml));
								}

								#region ID

								int streamId;

								if (!int.TryParse(stream.Attributes["ID"].Value, out streamId))
								{
									throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
									                                                           "L'attributo 'ID' del nodo 'Stream' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
									                                                           this.DefinitionFileName,
									                                                           stream.Attributes["ID"].Value));
								}

								#endregion

								DBStream str = new DBStream(streamId, stream.Attributes["Name"].Value.Trim());

								#region StreamField

								bool streamFieldFound = false;

								foreach (XmlNode streamField in stream.ChildNodes)
								{
									if (streamField.Name.Equals("StreamField", StringComparison.OrdinalIgnoreCase))
									{
										#region Verifica presenza attributi ID e Name

										if ((streamField.Attributes["ID"] == null) || (streamField.Attributes["ID"].Value.Trim().Length == 0) ||
										    (streamField.Attributes["Name"] == null) || (streamField.Attributes["Name"].Value.Trim().Length == 0))
										{
											throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
											                                                           "Impossibile trovare l'attributo 'ID' o 'Name' sul nodo 'StreamField' nel file {0}, oppure nome dello StreamField nullo. Il contenuto del nodo è:\r\n{1}",
											                                                           this.DefinitionFileName, streamField.OuterXml));
										}

										#endregion

										if ((this.DeviceConfigurationParameters.DefinitionType == DeviceKind.Telnet) &&
										    ((streamField.Attributes["TelnetCommand"] == null) ||
										     (streamField.Attributes["TelnetCommand"].Value.Trim().Length == 0)))
										{
											throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
											                                                           "L'attributo 'TelnetCommand' è obbligatorio sul nodo 'StreamField' nel file {0}. Il contenuto del nodo è:\r\n{1}",
											                                                           this.DefinitionFileName, streamField.OuterXml));
										}

										#region ID

										int fieldId;

										if (!int.TryParse(streamField.Attributes["ID"].Value, out fieldId))
										{
											throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
											                                                           "L'attributo 'ID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
											                                                           this.DefinitionFileName,
											                                                           streamField.Attributes["ID"].Value));
										}

										#endregion

										#region ArrayID

										int arrayId = 0;

										if (streamField.Attributes["ArrayID"] != null)
										{
											if (!int.TryParse(streamField.Attributes["ArrayID"].Value, out arrayId))
											{
												throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
												                                                           "L'attributo 'ArrayID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
												                                                           this.DefinitionFileName,
												                                                           streamField.Attributes["ArrayID"].Value));
											}
										}

										#endregion

										#region TelnetCommand

										string telnetCommand = null;

										if (streamField.Attributes["TelnetCommand"] != null)
										{
											telnetCommand = streamField.Attributes["TelnetCommand"].Value.Trim();

											if (string.IsNullOrEmpty(telnetCommand))
											{
												throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
												                                                           "L'attributo 'TelnetCommand' del nodo 'StreamField' nel file {0}, non contiene un valore valido. Valore contenuto: '{1}'",
												                                                           this.DefinitionFileName,
												                                                           streamField.Attributes["TelnetCommand"].Value));
											}
										}

										#endregion

										DBStreamField strField = new DBStreamField(fieldId, arrayId, streamField.Attributes["Name"].Value.Trim(),
										                                           telnetCommand);

										if (strField != null)
										{
											#region FieldValueDescription

											bool streamFieldValueDescriptionFound = false;

											foreach (XmlNode fieldValueDescription in streamField.ChildNodes)
											{
												if (fieldValueDescription.Name.Equals("FieldValueDescription", StringComparison.OrdinalIgnoreCase))
												{
													if ((fieldValueDescription.Attributes["ValueDescription"] == null) ||
													    (fieldValueDescription.Attributes["ValueDescription"].Value.Trim().Length == 0))
													{
														throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
														                                                           "Impossibile trovare l'attributo 'ValueDescription' sul nodo 'FieldValueDescription', oppure descrizione del valore nulla nel file {0}. Il contenuto del nodo è:\r\n{1}",
														                                                           this.DefinitionFileName,
														                                                           fieldValueDescription.OuterXml));
													}

													#region Severity

													object severity = null;

													if (fieldValueDescription.Attributes["Severity"] != null)
													{
														try
														{
															string severityString = fieldValueDescription.Attributes["Severity"].Value.Trim();
															severity = Enum.Parse(typeof (Severity), severityString, true);
															if (severity == null)
															{
																throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
																                                                           "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
																                                                           this.DefinitionFileName,
																                                                           fieldValueDescription.Attributes["Severity"]
																	                                                           .Value.Trim()));
															}
														}
														catch (ArgumentException)
														{
															throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
															                                                           "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
															                                                           this.DefinitionFileName,
															                                                           fieldValueDescription.Attributes["Severity"]
																                                                           .Value.Trim()));
														}
													}

													if (severity == null)
													{
														severity = Severity.Ok;
													}

													#endregion

													#region ExactValueString

													string exactValueString = null;

													if (fieldValueDescription.Attributes["ExactValueString"] != null)
													{
														exactValueString = fieldValueDescription.Attributes["ExactValueString"].Value;
													}

													#endregion

													#region IsDefault

													bool isDefault = false;

													if (fieldValueDescription.Attributes["IsDefault"] != null)
													{
														string isDefaultString = fieldValueDescription.Attributes["IsDefault"].Value.Trim().ToLowerInvariant();
														if (!Boolean.TryParse(isDefaultString, out isDefault))
														{
															throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
															                                                           "L'attributo 'IsDefault' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
															                                                           this.DefinitionFileName,
															                                                           fieldValueDescription.Attributes["IsDefault"]
																                                                           .Value.Trim()));
														}
													}

													#endregion

													#region ShouldSendNotificationByEmail

													bool shouldSendNotificationByEmail = false;

													if (fieldValueDescription.Attributes["ShouldSendNotificationByEmail"] != null)
													{
														string shouldSendNotificationByEmailString =
															fieldValueDescription.Attributes["ShouldSendNotificationByEmail"].Value.Trim().ToLowerInvariant();
														if (!Boolean.TryParse(shouldSendNotificationByEmailString, out shouldSendNotificationByEmail))
														{
															throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
															                                                           "L'attributo 'ShouldSendNotificationByEmail' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
															                                                           this.DefinitionFileName,
															                                                           fieldValueDescription.Attributes[
																                                                           "ShouldSendNotificationByEmail"].Value.Trim()));
														}
													}

													#endregion

													strField.DbStreamFieldValueDescriptions.Add(new DBStreamFieldValueDescription(strField, isDefault,
													                                                                              fieldValueDescription
														                                                                              .Attributes[
															                                                                              "ValueDescription"].Value
															                                                                                                 .Trim(),
													                                                                              (Severity) severity,
													                                                                              exactValueString,
													                                                                              shouldSendNotificationByEmail));

													streamFieldValueDescriptionFound = true;
												}
											}

											if (!streamFieldValueDescriptionFound)
											{
												throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
												                                                           "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno una descrizione di decodifica dei valori dello stream field '{1}'.",
												                                                           this.DefinitionFileName,
												                                                           streamField.Attributes["Name"].Value.Trim()));
											}

											#endregion

											str.FieldData.Add(strField);
										}

										streamFieldFound = true;
									}
								}

								if (!streamFieldFound)
								{
									throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
									                                                           "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno uno stream field definito per lo stream '{1}' che sia relativo al tipo della periferica corrente.",
									                                                           this.DefinitionFileName,
									                                                           stream.Attributes["Name"].Value.Trim()));
								}

								#endregion

								if ((str != null) && (str.FieldData != null) && (str.FieldData.Count > 0))
								{
									// Lo Stream è aggiunto solo se contiene almeno uno Stream Field
									streamData.Add(str);
								}
							}

							streamFound = true;
						}

						if (!streamFound)
						{
							throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
							                                                           "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno uno Stream definito.",
							                                                           this.DefinitionFileName));
						}

						#endregion

						streamsFound = true;
						break;
					}
				}

				if (!streamsFound)
				{
					throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
					                                                           "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare il nodo 'Streams' al primo livello.",
					                                                           this.DefinitionFileName));
				}
			}
		}

		#endregion

		#region Metodi privati

		private XmlNode GetDefinitionXmlRootElement()
		{
			if (string.IsNullOrEmpty(this.DefinitionFileName))
			{
				throw new DeviceConfigurationHelperException("Nome del file di configurazione per la periferica non definito");
			}

			if (!FileUtility.CheckFileCanRead(this.DefinitionFileName))
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Impossibile leggere il file di configurazione: {0}",
				                                                           this.DefinitionFileName));
			}

			XmlDocument doc = new XmlDocument();
			XmlNode root;

			try
			{
				doc.Load(this.DefinitionFileName);
				root = doc.DocumentElement;
			}
			catch (XmlException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica",
					              this.DefinitionFileName), ex);
			}
			catch (ArgumentException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica",
					              this.DefinitionFileName), ex);
			}
			catch (IOException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica",
					              this.DefinitionFileName), ex);
			}
			catch (UnauthorizedAccessException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica",
					              this.DefinitionFileName), ex);
			}
			catch (NotSupportedException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica",
					              this.DefinitionFileName), ex);
			}
			catch (SecurityException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica",
					              this.DefinitionFileName), ex);
			}

			if (root == null)
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Configurazione non valida nel file {0}, con la definizione XML della periferica",
				                                                           this.DefinitionFileName));
			}

			if (!root.Name.Equals("Device", StringComparison.OrdinalIgnoreCase))
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Impossibile trovare l'elemento 'Device' come nodo radice del file di configurazione {0}.",
				                                                           this.DefinitionFileName));
			}

			return root;
		}

		#endregion
	}
}
﻿using System;
using System.Globalization;
using System.IO;
using System.Security;
using System.Xml;

namespace GrisSuite.AudiaSupervisorLibrary.Devices
{
	/// <summary>
	///     Classe helper che contiene i parametri base delle definizioni
	/// </summary>
	public class DeviceConfigurationHelperParameters
	{
		private const string DEFINITION_VERSION = "1.00";

		#region Proprietà

		/// <summary>
		///     Tipo della definizione
		/// </summary>
		public DeviceKind DefinitionType { get; private set; }

		/// <summary>
		///     Versione della definizione
		/// </summary>
		public String DefinitionVersion { get; private set; }

		/// <summary>
		///     Nome file completo della definizione
		/// </summary>
		public String DefinitionFileName { get; private set; }

		#endregion

		/// <summary>
		///     Costruttore privato
		/// </summary>
		/// <param name="definitionType">Tipo della definizione</param>
		/// <param name="definitionVersion">Versione della definizione</param>
		/// <param name="definitionFileName">Nome file completo della definizione</param>
		private DeviceConfigurationHelperParameters(DeviceKind definitionType,
		                                            string definitionVersion,
		                                            string definitionFileName)
		{
			this.DefinitionType = definitionType;
			this.DefinitionVersion = definitionVersion;
			this.DefinitionFileName = definitionFileName;
		}

		#region Precaricamento definizione, per poter configurare il caricamento dinamico delle sotto-definizioni, se presente

		/// <summary>
		///     Esegue un parsing preliminare della definizione, per caricare il tipo vari parametri, tra cui la versione,
		///     il tipo di definizione e verificare l'esistenza di definizioni per device
		/// </summary>
		/// <param name="deviceType">Tipo della periferica</param>
		/// <param name="deviceId">Device Id della periferica</param>
		/// <returns>Oggetto che contiene i dati di configurazione della periferica, in base alla definizione</returns>
		public static DeviceConfigurationHelperParameters PreloadConfigurationParameters(string deviceType, long deviceId)
		{
			string definitionFileName = FileUtility.GetDefinitionFile(deviceType, false);

			XmlNode root = GetDefinitionXmlRootElement(definitionFileName);

			if (root != null)
			{
				DeviceKind deviceKind;

				#region DefinitionType

				object definitionType = null;

				if (root.Attributes != null)
				{
					if (root.Attributes["DefinitionType"] != null)
					{
						try
						{
							string definitionTypeString = root.Attributes["DefinitionType"].Value.Trim();
							definitionType = Enum.Parse(typeof (DeviceKind), definitionTypeString, true);
						}
						catch (ArgumentException)
						{
							throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
							                                                           "L'attributo 'DefinitionType' del nodo 'Device' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
							                                                           definitionFileName,
							                                                           root.Attributes["DefinitionType"].Value.Trim()));
						}
					}
				}

				if (definitionType == null)
				{
					// Se non definito, il default per le definizioni è Telnet
					deviceKind = DeviceKind.Telnet;
				}
				else
				{
					deviceKind = (DeviceKind) definitionType;
				}

				#endregion

				String definitionVersion = null;

				#region DefinitionVersion

				if (root.Attributes != null)
				{
					if (root.Attributes["DefinitionVersion"] != null)
					{
						string definitionVersionString = root.Attributes["DefinitionVersion"].Value.Trim();

						if (!String.IsNullOrEmpty(definitionVersionString))
						{
							definitionVersion = definitionVersionString;
						}
					}
				}

				if (String.IsNullOrEmpty(definitionVersion))
				{
					definitionVersion = DEFINITION_VERSION;
				}

				#endregion

				DeviceConfigurationHelperParameters deviceConfigurationHelperParameters =
					new DeviceConfigurationHelperParameters(deviceKind, definitionVersion, definitionFileName);

				return deviceConfigurationHelperParameters;
			}

			// Non arriveremo ma qui
			return null;
		}

		#endregion

		#region Metodi privati

		private static XmlNode GetDefinitionXmlRootElement(string definitionFileName)
		{
			if (string.IsNullOrEmpty(definitionFileName))
			{
				throw new DeviceConfigurationHelperException("Nome del file di configurazione per la periferica non definito");
			}

			if (!FileUtility.CheckFileCanRead(definitionFileName))
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Impossibile leggere il file di configurazione: {0}",
				                                                           definitionFileName));
			}

			XmlDocument doc = new XmlDocument();
			XmlNode root;

			try
			{
				doc.Load(definitionFileName);
				root = doc.DocumentElement;
			}
			catch (XmlException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica", definitionFileName),
					ex);
			}
			catch (ArgumentException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica", definitionFileName),
					ex);
			}
			catch (IOException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica", definitionFileName),
					ex);
			}
			catch (UnauthorizedAccessException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica", definitionFileName),
					ex);
			}
			catch (NotSupportedException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica", definitionFileName),
					ex);
			}
			catch (SecurityException ex)
			{
				throw new DeviceConfigurationHelperException(
					string.Format(CultureInfo.InvariantCulture,
					              "Configurazione non valida nel file {0}, con la definizione XML della periferica", definitionFileName),
					ex);
			}

			if (root == null)
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Configurazione non valida nel file {0}, con la definizione XML della periferica",
				                                                           definitionFileName));
			}

			if (!root.Name.Equals("Device", StringComparison.OrdinalIgnoreCase))
			{
				throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
				                                                           "Impossibile trovare l'elemento 'Device' come nodo radice del file di configurazione {0}.",
				                                                           definitionFileName));
			}

			return root;
		}

		#endregion
	}
}
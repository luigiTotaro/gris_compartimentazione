﻿namespace GrisSuite.AudiaSupervisorLibrary.Devices
{
	/// <summary>
	///     Rappresenta il genere della periferica
	/// </summary>
	public enum DeviceKind
	{
		/// <summary>
		///     Periferica Telnet
		/// </summary>
		Telnet
	}
}
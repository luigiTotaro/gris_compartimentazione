﻿namespace GrisSuite.AudiaSupervisorLibrary.Devices
{
	public class DeviceTypeFactory
	{
		/// <summary>
		///     Ritorna una istanza di periferica, in base al tipo indicato
		/// </summary>
		/// <param name="deviceObject">Oggetto di stato con le informazioni relative al device</param>
		/// <returns></returns>
		public IDevice Create(DeviceObject deviceObject)
		{
			IDevice device = null;

			DeviceConfigurationHelperParameters deviceConfigurationHelperParameters =
				DeviceConfigurationHelperParameters.PreloadConfigurationParameters(deviceObject.DeviceType, deviceObject.DeviceId);

			switch (deviceConfigurationHelperParameters.DefinitionType)
			{
				case DeviceKind.Telnet:
					device = new TelnetDeviceBase(deviceObject.FakeLocalData, deviceObject.Name, deviceObject.DeviceType,
					                              deviceObject.EndPoint, deviceObject.LastEvent, deviceConfigurationHelperParameters,
					                              deviceObject);
					break;
			}

			return device;
		}
	}
}
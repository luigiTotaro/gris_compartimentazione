﻿namespace GrisSuite.AudiaSupervisorLibrary
{
    /// <summary>
    ///   Indica il livello di logging
    /// </summary>
    public enum LoggingLevel
    {
        /// <summary>
        ///   Logging disattivato
        /// </summary>
        None = 0,
        /// <summary>
        ///   Log solo degli eventi critici (eccezioni non gestite)
        /// </summary>
        Critical = 1,
        /// <summary>
        ///   Log degli errori (eccezioni gestite)
        /// </summary>
        Error = 2,
        /// <summary>
        ///   Log delle condizioni di allerta
        /// </summary>
        Warning = 3,
        /// <summary>
        ///   Log informativo
        /// </summary>
        Info = 4,
        /// <summary>
        ///   Log dettagliato
        /// </summary>
        Verbose = 5,
        /// <summary>
        ///   Log di diagnostica di rete
        /// </summary>
        Diagnostic = 6
    }
}
@echo off
sc stop "STLC SNMP Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc delete "STLC SNMP Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc create "STLC SNMP Supervisor" binPath= "D:\Projects\Telefin\GrisSuite\GrisSuite.SNMPSupervisor\GrisSuite.SNMPSupervisorService\bin\Release\GrisSuite.SnmpSupervisorService.exe" start= auto DisplayName= "STLC SNMP Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL
sc description "STLC SNMP Supervisor" "STLC SNMP Supervisor"

sc start "STLC SNMP Supervisor"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

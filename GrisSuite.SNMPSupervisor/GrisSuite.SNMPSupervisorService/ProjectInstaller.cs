﻿using System.ComponentModel;
using System.Configuration.Install;

namespace GrisSuite.SnmpSupervisorService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            this.InitializeComponent();
        }
    }
}
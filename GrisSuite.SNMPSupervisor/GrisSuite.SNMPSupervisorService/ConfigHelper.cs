﻿using System;
using System.IO;
using System.Security;
using GrisSuite.SnmpSupervisorService.Properties;
using Microsoft.Win32;

namespace GrisSuite.SnmpSupervisorService
{
	/// <summary>
	///     Classe helper per il caricamento delle configurazioni da Registry o file xml dei settings
	/// </summary>
	public class ConfigHelper
	{
		private static readonly ConfigHelper defaultInstance = new ConfigHelper();
        private String defaultSystemXml = null;

        private ConfigHelper()
        {
            defaultSystemXml = AppDomain.CurrentDomain.BaseDirectory + "System.xml";
        }

		/// <summary>
		///     Istanza della classe
		/// </summary>
		public static ConfigHelper Default
		{
			get { return defaultInstance; }
		}

		public string SystemXmlFile
		{
			get
			{
                // verifico se esiste il parametro nel file di configurazione
                string systemXml = Properties.Settings.Default.SystemXml;

                if (string.IsNullOrEmpty(systemXml))
                {
                    // provo a recuperare il dato da chiave di registro di windows
                    systemXml = this.GetRegistryKey(GrisSuite.SnmpSupervisorLibrary.ConfigHelper.Default.GlobalRegisterKeyPath, Settings.Default.SystemXmlRegisterKeyName);
                    if (string.IsNullOrEmpty(systemXml))
                    {
                        // dato che riesco ad ottenere il valore
                        systemXml = defaultSystemXml;
                    }
                }

                return systemXml;
			}
		}

		#region Metodi privati

		/// <summary>
		///     Aggiunge il backslash mancante a nome di cartella
		/// </summary>
		/// <param name="folderName">Nome della cartella</param>
		/// <returns>Nome della cartella decorato</returns>
		private static string AddMissingBackslash(string folderName)
		{
			if (!folderName.EndsWith("\\", StringComparison.OrdinalIgnoreCase))
			{
				return folderName + "\\";
			}

			return folderName;
		}

		/// <summary>
		///     Recupera la cartella in cui lavorare, espandendo eventuali path relativi
		/// </summary>
		/// <param name="relativeFolder">Nome cartella, relativa o assoluta</param>
		/// <returns>Path completo, terminato da backslash</returns>
		public static string GetAbsoluteFolderFromConfig(string relativeFolder)
		{
			try
			{
				if (string.IsNullOrEmpty(relativeFolder))
				{
					// Non è indicata una cartella base, quindi usiamo come punto di partenza il path dell'eseguibile
					return AddMissingBackslash(AppDomain.CurrentDomain.BaseDirectory);
				}

				if ((relativeFolder.StartsWith(".\\")) || (relativeFolder.StartsWith("..\\")))
				{
					string baseFolder = AppDomain.CurrentDomain.BaseDirectory;

					return AddMissingBackslash(Path.GetFullPath(Path.Combine(baseFolder, relativeFolder)));
				}

				return AddMissingBackslash(Path.GetFullPath(relativeFolder));
			}
			catch (ArgumentException)
			{
			}
			catch (SecurityException)
			{
			}
			catch (NotSupportedException)
			{
			}
			catch (PathTooLongException)
			{
			}
			catch (AppDomainUnloadedException)
			{
			}

			return null;
		}

		private string GetRegistryKey(string KeyName, string ValueName)
		{
			string keyValue = null;
			RegistryKey key = null;

			try
			{
				key = Registry.LocalMachine.OpenSubKey(KeyName);
				if (key != null)
				{
					keyValue = key.GetValue(ValueName) as String;
				}
			}
			catch (SecurityException)
			{
			}
			finally
			{
				if (key != null)
				{
					key.Close();
				}
			}

			return keyValue;
		}

		#endregion
	}
}
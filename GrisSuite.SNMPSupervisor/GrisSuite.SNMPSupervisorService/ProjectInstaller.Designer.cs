﻿namespace GrisSuite.SnmpSupervisorService {
    partial class ProjectInstaller {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.SnmpSupervisorServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.SnmpSupervisorServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // SnmpSupervisorServiceProcessInstaller
            // 
            this.SnmpSupervisorServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.SnmpSupervisorServiceProcessInstaller.Password = null;
            this.SnmpSupervisorServiceProcessInstaller.Username = null;
            // 
            // SnmpSupervisorServiceInstaller
            // 
            this.SnmpSupervisorServiceInstaller.Description = "GrisSuite SNMP Supervisor Service";
            this.SnmpSupervisorServiceInstaller.DisplayName = "STLC SNMP Supervisor Service";
            this.SnmpSupervisorServiceInstaller.ServiceName = "SnmpSupervisorService";
            this.SnmpSupervisorServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.SnmpSupervisorServiceProcessInstaller,
            this.SnmpSupervisorServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller SnmpSupervisorServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller SnmpSupervisorServiceInstaller;
    }
}
﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using GrisSuite.SnmpSupervisorService.Properties;
using System.Collections;
using Timer = System.Timers.Timer;

namespace GrisSuite.SnmpSupervisorService
{
	public partial class MainService : ServiceBase
	{
		private const string SYSTEM_XML_FILE_NAME = "System.xml";

		#region Campi Privati

		private static SqlConnection dbConnection;
		private static SqlConnection dbEventsConnection;
		private static ReadOnlyCollection<DeviceObject> devices;
		private static Timer pollingTimer;
		private static Timer startupTimer;
		private static bool processingInProgress;
		private static DatabaseFactory persistenceFactory;
		private static string configurationSystemFile;

		private enum DeviceWriteMode
		{
			Insert,
			Update
		};

		#endregion

		#region Costruttore

		public MainService()
		{
			this.InitializeComponent();
		}

		#endregion

		#region Start del servizio

		protected override void OnStart(string[] args)
		{
			configurationSystemFile = string.Empty;

            configurationSystemFile = ConfigHelper.Default.SystemXmlFile;

			// non inzializzando persistOnDatabase, lasciamo che si usi la configurazione da file
			InitializeStart(configurationSystemFile, null);
		}

		#endregion

		#region Inizializzazione, verifica configurazione ed avvio monitoraggio

		public static Exception InitializeStart(string configurationSystemFilename, bool? persistOnDatabase)
		{
			configurationSystemFile = configurationSystemFilename;

			ConnectionStringSettings localDatabaseConnectionString = null;
			ConnectionStringSettings localDatabaseEventsConnectionString = null;
			Exception lastException = null;
			persistenceFactory = new DatabaseFactory(persistOnDatabase);

			if (string.IsNullOrEmpty(configurationSystemFile))
			{
				lastException =
					new ConfigurationSystemException(
						"Nome del file di configurazione con la lista dei dispositivi da monitorare non definito");
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, lastException.Message);
				return lastException;
			}

			if (!FileUtility.CheckFileCanRead(configurationSystemFile))
			{
				lastException =
					new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					                                               "Impossibile leggere il file di configurazione con la lista dei dispositivi: {0}",
					                                               configurationSystemFile));
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, lastException.Message);
				return lastException;
			}

			try
			{
				localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
				localDatabaseEventsConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabaseEvents"];
			}
			catch (ConfigurationErrorsException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical, "Configurazione non corretta. " + ex.Message);
				lastException = ex;
			}

			if (localDatabaseConnectionString == null)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
				                                               "La stringa di connessione al database non è valida.");
			}
			else
			{
				try
				{
					dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					                                               "La stringa di connessione al database non è valida. " + ex.Message);
					lastException = ex;
				}
			}

			if (localDatabaseEventsConnectionString == null)
			{
				if (dbConnection != null)
				{
					dbEventsConnection = dbConnection;
				}
			}
			else
			{
				try
				{
					dbEventsConnection = new SqlConnection(localDatabaseEventsConnectionString.ConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					                                               "La stringa di connessione al database degli eventi non è valida. " +
					                                               ex.Message);
					lastException = ex;
				}
			}

			if ((lastException == null) && (dbConnection != null) && (dbEventsConnection != null))
			{
				if (Settings.Default.StartupDelayMinutes > 0)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Avvio servizio con attesa {0} minuti per primo monitoraggio.",
					                                                             Settings.Default.StartupDelayMinutes));

					startupTimer = new Timer {Interval = (Settings.Default.StartupDelayMinutes*60*1000)};
					startupTimer.Elapsed += OnStartupTimerElapsed;

					startupTimer.Start();
				}
				else
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
					                                               string.Format(CultureInfo.InvariantCulture, "Avvio servizio."));

					CleanupAndCreateDevices();
				}

#if (DEBUG)
				// In fase di debug lasciamo vari secondi per il completamento dei thread, da aumentare all'occorrenza
				Thread.Sleep(Settings.Default.StartupDelayMinutes*60*1000);
				Thread.Sleep(((int) Settings.Default.PollingIntervalSeconds - 1)*1000);
#endif
			}
			else
			{
				lastException =
					new ConfigurationErrorsException(
						"Le stringhe di connessione al database o al database degli eventi non sono disponibili.");
			}

			return lastException;
		}

		#endregion

		#region Gestione eventi tick del timer di avvio

		private static void OnStartupTimerElapsed(object source, ElapsedEventArgs e)
		{
			if (startupTimer != null)
			{
				startupTimer.Stop();
				startupTimer.Dispose();
			}

			CleanupAndCreateDevices();
		}

		#endregion

		#region CleanupAndCreateDevices - Carica le periferiche da configurazione, ripulisce la base dati e ricrea le periferiche

		private static void CleanupAndCreateDevices()
		{
			devices = SystemXmlHelper.GetMonitorDeviceList(configurationSystemFile);

			bool isMonitoring = false;

			if ((devices != null) && (devices.Count > 0))
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
				                                               string.Format(CultureInfo.InvariantCulture,
				                                                             "Caricate {0} periferiche da System.xml per il monitoraggio. Il polling impostato è di {1} secondi.",
				                                                             devices.Count, Settings.Default.PollingIntervalSeconds));

				foreach (DeviceObject deviceObject in devices)
				{
					if (deviceObject.Active == 1)
					{
						deviceObject.MonitoringEnabled = false;

						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose, deviceObject.ToString());

						bool? deviceExistence = persistenceFactory.CheckDeviceExistence(dbConnection.ConnectionString,
						                                                                deviceObject.DeviceId);

						if (deviceExistence.HasValue)
						{
							if (deviceExistence.Value)
							{
								// la periferica esiste in base dati
								if (Settings.Default.ShouldDeleteExistingDevicesAtStartup)
								{
									// la periferica esiste e deve essere cancellata
									if (DeleteDevice(deviceObject))
									{
										// la periferica è stata correttamente cancellata, possiamo reinserirla
										deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert);

										if (deviceObject.MonitoringEnabled)
										{
											isMonitoring = true;
										}
									}
									else
									{
										// non è stato possibile cancellare la periferica esistente
										FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
										                                               string.Format(CultureInfo.InvariantCulture,
										                                                             "Periferica presente in base dati e richiesta eliminazione all'avvio del servizio, ma eliminazione fallita. Ogni ulteriore operazione relativa alla periferica annullata. {0}",
										                                                             deviceObject));

										deviceObject.MonitoringEnabled = false;
									}
								}
								else
								{
									// aggiorna la periferica esistente
									deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Update);

									if (deviceObject.MonitoringEnabled)
									{
										isMonitoring = true;
									}
								}
							}
							else
							{
								// la periferica non esiste in base dati e dovrebbe essere creata
								if (Settings.Default.ShouldCreateNewDevices)
								{
									deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert);

									if (deviceObject.MonitoringEnabled)
									{
										isMonitoring = true;
									}
								}
								else
								{
									FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
									                                               string.Format(CultureInfo.InvariantCulture,
									                                                             "Periferica non presente in base dati, ma creazione di nuove periferiche disattivata da configurazione (modificare flag di configurazione 'ShouldCreateNewDevices'). Non sarà possibile creare gli Stream / Stream Fields per la periferica. {0}",
									                                                             deviceObject));
								}
							}
						}
					}
					else
					{
						// la periferica è indicata come non attiva
						deviceObject.MonitoringEnabled = false;

						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Periferica caricata da System.xml, ma non attiva. Ogni ulteriore operazione relativa alla periferica annullata. {0}",
						                                                             deviceObject));
					}
				}

				pollingTimer = new Timer {Interval = 1};
				pollingTimer.Elapsed += OnPollingTimerElapsed;
                // genera l'evento una sola volta
                pollingTimer.AutoReset = false;

				if (isMonitoring)
				{
#if (!DEBUG)
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Avvio monitoraggio.");

                    pollingTimer.Start();
#else
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Avvio monitoraggio.");

					if ((devices != null) && (devices.Count > 0))
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
						                                               "Esecuzione recupero singolo di dati (modalità debug).");
						PollDevices();
					}
#endif
				}
				else
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
					                                               "Tutte le periferiche da monitorare hanno generato errori in fase di aggiornamento/inserimento iniziale. Nessun monitoraggio in corso. Verificare i problemi delle periferiche e riavviare il servizio.");
				}
			}
			else
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
				                                               "Non sono disponibili periferiche da monitorare nel file di configurazione del sistema.");
			}
		}

		#endregion

		#region Gestione eventi tick del timer

		private static void OnPollingTimerElapsed(object source, ElapsedEventArgs e)
		{
			try
			{
				PollDevices();
			}
			catch (Exception ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
				                                               string.Format(CultureInfo.InvariantCulture, "{0}\r\n{1}\r\n{2}",
				                                                             ex.Message, ex.StackTrace, ex.InnerException));
            }

            #region gestione timer
            if (pollingTimer != null)
            {
                // l'evento viene scatenato ogni tot PollingIntervalSeconds secondi in automatico
                pollingTimer.Interval = (Settings.Default.PollingIntervalSeconds * 1000);
                pollingTimer.AutoReset = true;
            }
            #endregion
        }

		#endregion

		#region Elaborazione eventi tick del timer e accodamento dei thread

		private static void PollDevices()
		{
			// Evita le chiamate nidificate
			if (processingInProgress)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Warning,
				                                               string.Format(CultureInfo.InvariantCulture,
				                                                             "Saltato ciclo di elaborazione periferiche, perché ancora in esecuzione ciclo precedente. Aumentare il parametro di configurazione PollingIntervalSeconds."));
				return;
			}

			processingInProgress = true;

			FileUtility.RecycleLogFile();

			if ((dbConnection != null) && (dbEventsConnection != null))
			{
				if ((devices != null) && (devices.Count > 0))
				{
					if (FileUtility.LogLevel >= LoggingLevel.Verbose)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Monitoraggio {0} periferiche.", devices.Count));
					}

					#region Pulizia tacitazioni scadute da database

					bool expiredAcknowledgementsDeleted =
						persistenceFactory.DeleteExpiredAcknowledgements(dbConnection.ConnectionString);

					if (expiredAcknowledgementsDeleted)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Ripulite tacitazioni scadute da database."));
					}

					#endregion

					#region Caricamento tacitazioni da database

					ReadOnlyCollection<DbDeviceAcknowledgement> deviceAcknowledgementsList = null;

					// Carichiamo le tacitazioni solamente se il comando du pulizia ha funzionato, per evitare di avere dati spuri e riverificare la scadenza qui
					if (expiredAcknowledgementsDeleted)
					{
						deviceAcknowledgementsList = persistenceFactory.GetDeviceAcknowledgementsList(dbConnection.ConnectionString);

						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Caricat{0} {1} tacitazion{2} da database.",
						                                                             (deviceAcknowledgementsList.Count == 1 ? "a" : "e"),
						                                                             deviceAcknowledgementsList.Count,
						                                                             (deviceAcknowledgementsList.Count == 1 ? "e" : "i")));
					}

					#endregion

					using (ThreadPoolWait tpw = new ThreadPoolWait())
					{
						foreach (DeviceObject device in devices)
						{
							if (device.MonitoringEnabled)
							{
								device.LastEvent = null;

								#region Assegnazioni tacitazioni a periferica corrente

								DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(device.DeviceId);
								// Reimpostiamo l'oggetto che contiene gli Ack, in modo che, ad ogni iterazione, siano azzerati per la periferica
								device.DeviceAck = deviceAcknowledgement;

								if ((deviceAcknowledgementsList != null) && (deviceAcknowledgementsList.Count > 0))
								{
									bool existsAckByDevice = false;

									foreach (DbDeviceAcknowledgement dbDeviceAcknowledgement in deviceAcknowledgementsList)
									{
										if (dbDeviceAcknowledgement.DevID == device.DeviceId)
										{
											deviceAcknowledgement.AddDeviceAcknowledgement(dbDeviceAcknowledgement.StrID, dbDeviceAcknowledgement.FieldID);
											existsAckByDevice = true;
										}
									}

									if (existsAckByDevice)
									{
										device.DeviceAck = deviceAcknowledgement;

										FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
										                                               string.Format(CultureInfo.InvariantCulture,
										                                                             "Periferica {0},\r\nassegnate tacitazioni {1}.",
										                                                             device, deviceAcknowledgement));
									}
								}

								#endregion

								tpw.QueueUserWorkItem(ProcessMonitorDevice, device);
							}
						}

						// aspettiamo che tutti i thread abbiano completato la loro esecuzione
						tpw.WaitOne();
					}

					if (FileUtility.LogLevel >= LoggingLevel.Verbose)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Verbose,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "Fine monitoraggio {0} periferiche.", devices.Count));
					}
				}
			}

			processingInProgress = false;
		}

		#endregion

		#region Creazione istanze periferiche, recupero dati ed inserimento in base dati - Metodo lanciato dai thread

		private static void ProcessMonitorDevice(object state)
		{
			DeviceObject deviceObject = state as DeviceObject;

			if (deviceObject != null)
			{
				if ((String.IsNullOrEmpty(deviceObject.Name)) || (String.IsNullOrEmpty(deviceObject.DeviceType)) ||
				    (deviceObject.EndPoint == null) || (deviceObject.Address == null))
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Alcune informazioni obbligatorie mancanti per la periferica. Nome Periferica: {0}, Device Id: {1}, IP Address: {2}, Type: {3}",
					                                                             deviceObject.Name, deviceObject.DeviceId,
					                                                             deviceObject.Address, deviceObject.DeviceType));
					return;
				}

				IDevice device;
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();

				try
				{
					device = deviceTypeFactory.Create(deviceObject);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
					                                               string.Format(CultureInfo.InvariantCulture, "{0}, Eccezione: {1}",
					                                                             deviceObject, ex.Message));
					return;
				}
				catch (DeviceConfigurationHelperException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
					                                               string.Format(CultureInfo.InvariantCulture, "{0}, Eccezione: {1}",
					                                                             deviceObject, ex.Message));
					return;
				}

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						// Errori a questo livello significano errori nella definizione XML come correttezza sintattica
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
						                                               string.Format(CultureInfo.InvariantCulture,
						                                                             "{0}, Definizione: {1}\r\nEccezione: {2}\r\n{3}",
						                                                             deviceObject, device.DefinitionFileName,
						                                                             device.LastError.Message,
						                                                             ((device.LastError.InnerException != null) &&
						                                                              (device.LastError.InnerException.InnerException !=
						                                                               null)
							                                                              ? string.Format(CultureInfo.InvariantCulture,
							                                                                              ", Eccezione Interna: {0}",
							                                                                              device.LastError.InnerException
							                                                                                    .InnerException.Message)
							                                                              : string.Empty)));
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							// Errori a questo livello indicano problemi di caricamento dati SNMP,
							// come tabelle SNMP non corrette o incomplete
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
							                                               string.Format(CultureInfo.InvariantCulture,
							                                                             "{0}, Definizione: {1}\r\nEccezione: {2}{3}{4}",
							                                                             deviceObject, device.DefinitionFileName,
							                                                             device.LastError.Message,
							                                                             ((device.LastError.InnerException != null)
								                                                              ? string.Format(CultureInfo.InvariantCulture,
								                                                                              "\r\nEccezione Interna: {0}",
								                                                                              device.LastError.InnerException
								                                                                                    .Message)
								                                                              : string.Empty),
							                                                             ((device.LastError.InnerException != null) &&
							                                                              (device.LastError.InnerException.InnerException !=
							                                                               null)
								                                                              ? string.Format(CultureInfo.InvariantCulture,
								                                                                              "\r\nEccezione Interna 2: {0}",
								                                                                              device.LastError.InnerException
								                                                                                    .InnerException.Message)
								                                                              : string.Empty)));
						}
					}

					// La periferica è persistita comunque in base dati, anche se sono avvenuti errori in fase
					// di caricamento dati.
					bool? deviceExistence = persistenceFactory.CheckDeviceExistence(dbConnection.ConnectionString,
					                                                                deviceObject.DeviceId);

					if (deviceExistence.HasValue)
					{
                        if (deviceObject.DefinitionVersion == null)
                        {
                            // cerco nel db la versione
                            deviceObject.DefinitionVersion = persistenceFactory.GetDefinitionVersion(dbConnection.ConnectionString, deviceObject.DeviceId);
                        }
                        
                            // verifico se la versione della definizione è cambiato e in tal caso cancello e ricreo il device
                            if (deviceObject.DefinitionVersion != null && !deviceObject.DefinitionVersion.Equals(device.DeviceConfigurationParameters.DefinitionVersion))
                            {
                                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format("La periferiva {0} ha cambiato versione definizione da {1} a {2}", deviceObject.DeviceId, deviceObject.DefinitionVersion, device.DeviceConfigurationParameters.DefinitionVersion));
                                deviceObject.DefinitionVersion = device.DeviceConfigurationParameters.DefinitionVersion;
                                if (DeleteDevice(deviceObject))
                                {
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format("La periferica {0} è stata eliminata correttamente", deviceObject.DeviceId));
                                    // la periferica è stata correttamente cancellata, possiamo reinserirla
                                    deviceObject.MonitoringEnabled = UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert);
                                    if (deviceObject.MonitoringEnabled)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, string.Format("La periferica {0} è stata reinserita correttamente", deviceObject.DeviceId));
                                    }
                                    else
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, string.Format("La periferica {0} non è stata reinserita correttamente", deviceObject.DeviceId));
                                    }
                                }
                                else
                                {
                                    deviceObject.MonitoringEnabled = false;
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error, string.Format("La periferica {0} non è stata eliminata correttamente", deviceObject.DeviceId));
                                }
                            }
                        

						if (!deviceObject.DeviceType.Equals(device.DeviceType, StringComparison.OrdinalIgnoreCase))
						{
							// Il tipo della periferica è cambiato durante la discovery SNMP, quindi va aggiornato in base dati
							// In teoria l'operazione può essere fatta solo una volta, ma essendo la discovery sempre attiva, si preferisce mantenerla sempre
							persistenceFactory.UpdateDeviceType(dbConnection.ConnectionString, deviceObject.DeviceId, device.DeviceType);
						}

						if (deviceObject.IsSerialNumberForced)
						{
							// Se la periferica ha un numero di serie che è stato cambiato a runtime, cioè letto durante le richieste delle definizioni
							// occorre aggiornare ed eventualmente sovrascrivere il numero di serie caricato da System.xml e statico
							persistenceFactory.UpdateDeviceSerialNumber(dbConnection.ConnectionString, deviceObject.DeviceId,
							                                            deviceObject.SerialNumber);
						}

						// Se la periferica ha una versione di definizione che è stata cambiata durante la lettura della definizione,
						// occorre aggiornare ed eventualmente sovrascrivere quella di default statica sulle device
						persistenceFactory.UpdateDeviceDefinitionVersion(dbConnection.ConnectionString, deviceObject.DeviceId,
						                                                 device.DeviceConfigurationParameters.DefinitionVersion);

						persistenceFactory.UpdateDeviceStatus(dbConnection.ConnectionString, deviceObject.DeviceId,
						                                      (int) device.SeverityLevel, device.ValueDescriptionComplete, device.Offline,
						                                      (device.ShouldSendNotificationByEmail ? (Byte) 1 : (Byte) 0));

                        // Se la periferica non risponde su ICMP, SNMP... evitiamo di salvare i dati degli Stream, che quindi rimarranno quelli
                        // dell'ultimo salvataggio riuscito o database vuoto, se sempre rimasta offline
                        // Non aggiorna i dati neppure se nessun streamfield è stato aggiornato
                        int lastStreamFieldId;
                        Hashtable deleteByStreamField;
                        if (device.ShouldPersistStreamAndStreamFields && device.IsDataRetrieved == true)
					    {
					        foreach (DBStream stream in device.StreamData)
					        {
                                // elimino tutti gli streamfield legati ad uno stream
                                // persistenceFactory.DeleteStreamField(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId);

					            persistenceFactory.UpdateStream(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, stream.Name,
					                (int) stream.SeverityLevel, stream.ValueDescriptionComplete);
                                lastStreamFieldId = -1;
                                deleteByStreamField = new Hashtable();

					            foreach (DBStreamField field in stream.FieldData)
					            {
                                    // alloco un nuovo spazio di memodia
                                    if ( field.IsStreamFieldArray && field.MemoryBase.MemoryBase == -1 ) {
                                        int baseId = checkIncrementStreamFieldId(lastStreamFieldId, field.FieldId);
                                        lastStreamFieldId = baseId + field.MaxSizeArray - 1;
                                        field.MemoryBase.MemoryBase = baseId;
                                        generateEntryStreamFieldArray(deleteByStreamField, deviceObject.DeviceId, stream.StreamId, baseId, field.MaxSizeArray);
                                    }
                                    // SITUAZIONE STANDARD
                                    if (field.PersistOnDatabase && !field.isFather() && !field.isChild())
					                {
                                        // in questo caso alloco semplicemente un nuovo spazio di memoria
                                        lastStreamFieldId = checkIncrementStreamFieldId(lastStreamFieldId, field.FieldId);
                                        field.FieldId = lastStreamFieldId;

					                    persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, field.FieldId,
					                        field.ArrayId, field.Name, (int) field.SeverityLevel, field.Value, field.ValueDescriptionComplete,
					                        (field.ShouldSendNotificationByEmail ? (Byte) 1 : (Byte) 0), true, false);
                                    }
                                    // SE UN PADRE OPPURE E' UseToHeadOfStreamFieldArray
                                    else if (field.PersistOnDatabase && field.isFather() && ( !field.IsStreamFieldArray || field.UseToHeadOfStreamFieldArray ))
                                    {
                                        #region Gestione accorpamento di un streamfield( e relativi figli ) in un unica description
                                        String descriptions = "";
                                        String description;
                                        bool check = false;
                                        
                                        int fieldId;// = (field.StreamFieldFather != null ? field.StreamFieldFather.FieldId : field.FieldId);
                                        String name;
                                        int positionArray = 0;

                                        // assegno il field id corretto
                                        if (field.StreamFieldFather != null)
                                        {
                                            if (field.StreamFieldFather.MemoryBase.MemoryBase == -1)
                                            {
                                                lastStreamFieldId = checkIncrementStreamFieldId(lastStreamFieldId, field.StreamFieldFather.FieldId);
                                                fieldId = lastStreamFieldId + field.StreamFieldFather.Offset;
                                                field.StreamFieldFather.MemoryBase.MemoryBase = lastStreamFieldId;
                                            }
                                            else
                                            {
                                                fieldId = field.StreamFieldFather.MemoryBase.MemoryBase + field.StreamFieldFather.Offset; ;
                                            }
                                        }
                                        else {
                                            if (field.MemoryBase.MemoryBase == -1)
                                            {
                                                lastStreamFieldId = checkIncrementStreamFieldId(lastStreamFieldId, field.FieldId);
                                                fieldId = lastStreamFieldId + field.Offset;
                                                field.MemoryBase.MemoryBase = lastStreamFieldId;
                                            }
                                            else {
                                                fieldId = field.MemoryBase.MemoryBase + field.Offset;
                                            }
                                        }
 

                                        if (field.StreamFieldFather != null)
                                        {
                                            name = field.StreamFieldFather.Name + ": " + field.StreamFieldFather.Value;
                                            if (field.StreamFieldFather.ExpressionFormatShowRowId != null) {
                                                if (!field.StreamFieldFather.RowIdZeroBased)
                                                    field.StreamFieldFather.StreamFieldArrayid++;
                                                name = String.Format(field.StreamFieldFather.ExpressionFormatShowRowId, name, field.StreamFieldFather.StreamFieldArrayid);
                                            }
                                        }
                                        else {
                                            name = field.Name;
                                            if (field.ExpressionFormatShowRowId != null)
                                            {
                                                if (!field.RowIdZeroBased)
                                                    field.StreamFieldArrayid++;
                                                name = String.Format(field.ExpressionFormatShowRowId, name, field.StreamFieldArrayid);
                                            }
                                        }


                                        if (field.isDiscardData)
                                            continue;

                                        foreach (DBStreamField fieldChild in field.DbStreamFieldValueChildren)
                                        {
                                            if (fieldChild.isDiscardData)
                                                continue;

                                            if (fieldChild.IsAggregation)
                                            {
                                               #region Gestione stream field d'aggregazione
                                                String value = (fieldChild.IsShowValueDescription ? fieldChild.ValueDescription : fieldChild.Value);

                                                // il nome allocato nel db sarà quello del padre
                                                DeleteStreamField(deleteByStreamField, deviceObject.DeviceId, stream.StreamId, fieldId);

                                                persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, fieldId,
                                                    positionArray++, name, (int)fieldChild.SeverityLevel, !fieldChild.IsShowName ? value : fieldChild.Name + ": " + value, fieldChild.ValueDescriptionComplete,
                                                    (fieldChild.ShouldSendNotificationByEmail ? (Byte)1 : (Byte)0), true, true);
                                                #endregion
                                                continue;
                                            }
                                            description = fieldChild.Name + ": " + (!fieldChild.IsShowValueDescription ? fieldChild.Value : fieldChild.ValueDescription) + "=" + ((int)fieldChild.SeverityLevel);
                                            if (check)
                                            {
                                                descriptions += ";" + description;
                                            }
                                            else
                                            {
                                                descriptions = description;
                                                check = true;
                                            }
                                        }

                                        if (!descriptions.Equals(""))
                                        {
                                            DeleteStreamField(deleteByStreamField, deviceObject.DeviceId, stream.StreamId, fieldId);

                                            persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, fieldId,
                                                field.PositionArray, name, (int)field.SeverityLevel, !field.IsShowName ? field.Value : field.Name + ": " + field.Value, descriptions,
                                                (field.ShouldSendNotificationByEmail ? (Byte)1 : (Byte)0), true, true);
                                        }
                                        else if (field.DbStreamFieldValueChildren == null || field.DbStreamFieldValueChildren.Count == 0)
                                        {
                                            DeleteStreamField(deleteByStreamField, deviceObject.DeviceId, stream.StreamId, fieldId);

                                            persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceObject.DeviceId, stream.StreamId, fieldId,
                                                field.PositionArray, name, (int)field.SeverityLevel, !field.IsShowName ? field.Value : field.Name + ": " + field.Value, field.ValueDescription,
                                                (field.ShouldSendNotificationByEmail ? (Byte)1 : (Byte)0), true, true);
                                        }
                                        #endregion
                                    }
					            }
					        }
					    }
					}
				}
			}
		}

		#endregion

        // genera le entry per uno streamfiledArray tutte "invisibili"
        private static void generateEntryStreamFieldArray(Hashtable deleteByStreamField, long deviceId, int streamId, int fieldID, int numElements){
            for (int i = 0; i < numElements; i++)
            {
                // elimino tutti gli streamfield prima di ricrearli
                DeleteStreamField(deleteByStreamField, deviceId, streamId, fieldID + i);

                // ricreo lo streamfiled
                persistenceFactory.UpdateStreamField(dbConnection.ConnectionString, deviceId, streamId, fieldID + i,
                    0, "", 0, "", "", 0, false, true);
            }
        }

		#region Aggiornamento / inserimento di una periferica in base dati

		private static bool UpdateInsertDevice(DeviceObject deviceObject, DeviceWriteMode deviceWriteMode)
		{
			bool returnValue = false;

			DeviceDatabaseSupport deviceDatabaseSupport =
				persistenceFactory.GetStationBuildingRackPortData(dbConnection.ConnectionString, deviceObject.Topography, deviceObject.RegionId, deviceObject.RegionName, deviceObject.ZoneId, deviceObject.ZoneName, deviceObject.NodeId, deviceObject.NodeName,
																  deviceObject.ServerId, deviceObject.ServerHost, deviceObject.ServerName, deviceObject.Port, Settings.Default.ShouldCreateTopography);

			if (deviceDatabaseSupport != null)
			{
				if (persistenceFactory.UpdateDevice(dbConnection.ConnectionString, deviceObject.DeviceId, deviceObject.NodeId,
				                                    deviceObject.ServerId, deviceObject.Name, deviceObject.DeviceType,
				                                    deviceObject.SerialNumber, deviceObject.Address, deviceDatabaseSupport.PortId,
				                                    deviceObject.ProfileId, deviceObject.Active, deviceObject.Scheduled, 0,
				                                    deviceDatabaseSupport.RackId, deviceObject.RackPositionRow,
				                                    deviceObject.RackPositionColumn, deviceObject.MonitoringDeviceId))
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Periferica correttamente {0}. {1}, {2}",
					                                                             (deviceWriteMode == DeviceWriteMode.Insert
						                                                              ? "inserita"
						                                                              : "aggiornata"), deviceObject, deviceDatabaseSupport));

					returnValue = true;
				}
				else
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
					                                               string.Format(CultureInfo.InvariantCulture,
					                                                             "Errore nell'{0} della periferica su database. {1}, {2}",
					                                                             (deviceWriteMode == DeviceWriteMode.Insert
						                                                              ? "inserimento"
						                                                              : "aggiornamento"), deviceObject,
					                                                             deviceDatabaseSupport));
				}
			}
			else
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
				                                               string.Format(CultureInfo.InvariantCulture,
				                                                             "Impossibile trovare i dati della Stazione/Fabbricato/Armadio/Interfaccia/Server su Database, per poter {0} la periferica. {1}",
				                                                             (deviceWriteMode == DeviceWriteMode.Insert
					                                                              ? "creare"
					                                                              : "aggiornare"), deviceObject));
			}

			return returnValue;
		}

		#endregion

        // se lo streamfiled non è contenuto in hashtable,allora elimino il campo
        private static void DeleteStreamField(Hashtable deleteByStreamField, long deviceId, int streamId, int fieldId) {
            if (deleteByStreamField.ContainsKey(fieldId) == false) {
                persistenceFactory.DeleteStreamField(dbConnection.ConnectionString, deviceId, streamId, fieldId);
                deleteByStreamField.Add(fieldId, true);
            }
        }

		#region Eliminazione di una periferica da base dati

		private static bool DeleteDevice(DeviceObject deviceObject)
		{
			bool returnValue = false;

			if (persistenceFactory.DeleteDevice(dbConnection.ConnectionString, deviceObject.DeviceId))
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info,
				                                               string.Format(CultureInfo.InvariantCulture,
				                                                             "Periferica correttamente eliminata. {0}", deviceObject));

				returnValue = true;
			}
			else
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Critical,
				                                               string.Format(CultureInfo.InvariantCulture,
				                                                             "Errore nell'eliminazione della periferica su database. {0}",
				                                                             deviceObject));
			}

			return returnValue;
		}

		#endregion

		#region Stop del servizio

		protected override void OnStop()
		{
			if (pollingTimer != null)
			{
				pollingTimer.Stop();
				pollingTimer.Dispose();
			}

			FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Info, "Fine monitoraggio.");
		}

		#endregion

        #region Gestione incremento dello streamfield id
        // parametri ultimo streamfield id scritto
        // parametri streamfield id corrente
        // ritorna lo stream field da scrivere
        private static int checkIncrementStreamFieldId(int lastStreamFieldId, int streamfieldId) {
            return ++lastStreamFieldId;
        }
        #endregion

        #region Metodo di avvio forzato del servizio da usare per debug

#if (DEBUG)
        public void Run()
		{
			this.OnStart(null);
			this.OnStop();
		}
#endif

		#endregion
	}
}